//---------------------------------------------------------------------------
#ifndef Inp_segyH
#define Inp_segyH
#include "Profile.h"
#include <comctrls.hpp>

int Input_SEGY(char *Name, TProfile* Prof, TProgressBar *PB, bool SizeRestriction);
int Input_SEGY(char *Name, TProfile* Prof, TProgressBar *PB, bool SizeRestriction, int DevidingType, int StartTrace, int Size);
// DevidingType - 0, file opening without restrictions
// DevidingType - 1, open part of file from indicated StratTrace. The size of part (in MB) couldn't be more then Size
// DevidingType - 2, open part of file from indicated StratTrace. The size of part (in traces) couldn't be more then Size
// DevidingType - 3, open part of file from indicated StratTrace and up to the next Marker.

//---------------------------------------------------------------------------
#endif
