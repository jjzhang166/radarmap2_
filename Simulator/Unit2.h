//---------------------------------------------------------------------------

#ifndef Unit2H
#define Unit2H
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "IdBaseComponent.hpp"
#include "IdComponent.hpp"
#include "IdContext.hpp"
#include "IdCustomTCPServer.hpp"
#include "IdTCPServer.hpp"
#include <Dialogs.hpp>
#include <ExtCtrls.hpp>
#include "IdTCPClient.hpp"
#include "IdTCPConnection.hpp"
//---------------------------------------------------------------------------
enum TRadarType {SINGLE1=0, DOUBLE1=1, DOUBLE2=2, OtherCHM=3, DOUBLE9=4, PYTHON3=5, INFRARADAR=6};

struct TSetupTune
{
	TSetupTune() {Samples=512; ChannelsQ=1; ChannelN=0;}
	TSetupTune(int s, int q, int n) {Samples=s; ChannelsQ=q; ChannelN=n;}
	int Samples;
	int ChannelsQ;
	int ChannelN;
};

class TSetupThread: public TThread
{
private:
	virtual void __fastcall Execute();
	TRadarType RadarType;
	TIdIOHandler* IO;
	TStrings* Files;
	int FileIndex;
	bool GetNextFile;
	HANDLE hndl;
	short int samples;
	int traces, TraceIndex;
	char header[4096];
	unsigned char *TxBuf;
	bool DoubleStt;
	AnsiString tune;
	bool NewTune;
	TSetupTune TuneCommand;
protected:
	bool __fastcall OpenFile(UnicodeString Filename);
	bool __fastcall GetNextTrace();
	void __fastcall writeTuneStr(AnsiString value) {if(tune!=value) {tune=value; NewTune=true;}}
	TSetupTune __fastcall TuneCommandParser(AnsiString ATune);
public:
	__fastcall TSetupThread(bool CreateSuspended, TIdIOHandler *io, TStrings* f,
		TRadarType rt, AnsiString ATune);
	__property AnsiString TuneStr = {read=tune, write=writeTuneStr};
};

class TForm2 : public TForm
{
__published:	// IDE-managed Components
	TIdTCPServer *IdTCPServer1;
	TListBox *ListBox1;
	TButton *Button1;
	TOpenDialog *OpenDialog1;
	TButton *Button2;
	TButton *Button3;
	TButton *Button4;
	TLabel *Label1;
	TTimer *Timer1;
	TCheckBox *CheckBox1;
	TButton *Button5;
	TIdTCPServer *IdTCPServer2;
	TLabel *Label2;
	TButton *Button6;
	TRadioButton *SingleRButton;
	TRadioButton *InfraRButton;
	TLabel *Label3;
	TRadioButton *AdvancedRButton;
	TRadioButton *PythonRButton;
	void __fastcall Button4Click(TObject *Sender);
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall Button2Click(TObject *Sender);
	void __fastcall IdTCPServer1Execute(TIdContext *AContext);
	void __fastcall Timer1Timer(TObject *Sender);
	void __fastcall Button3Click(TObject *Sender);
	void __fastcall IdTCPServer1Connect(TIdContext *AContext);
	void __fastcall IdTCPServer1Disconnect(TIdContext *AContext);
	void __fastcall CheckBox1Click(TObject *Sender);
	void __fastcall Button5Click(TObject *Sender);
	void __fastcall IdTCPServer2Execute(TIdContext *AContext);
	void __fastcall IdTCPServer2Disconnect(TIdContext *AContext);
	void __fastcall Button6Click(TObject *Sender);

private:	// User declarations
public:		// User declarations
	__fastcall TForm2(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm2 *Form2;
//---------------------------------------------------------------------------
#endif
