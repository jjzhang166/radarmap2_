//---------------------------------------------------------------------------

#include <vcl.h>
#include <stdio.h>
#pragma hdrstop

#include "Unit2.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "IdBaseComponent"
#pragma link "IdComponent"
#pragma link "IdContext"
#pragma link "IdCustomTCPServer"
#pragma link "IdTCPServer"
#pragma link "IdTCPClient"
#pragma link "IdTCPConnection"
#pragma resource "*.dfm"
TForm2 *Form2;
TSetupThread *thrd;
bool Connected;
int tc;
AnsiString fff;
double Lat, Lon , Z;
TIdIOHandler *IO2;

//---------------------------------------------------------------------------
__fastcall TForm2::TForm2(TComponent* Owner)
	: TForm(Owner)
{
	thrd=NULL;
	Connected=false;
	IO2=NULL;
}
//---------------------------------------------------------------------------
void __fastcall TForm2::Button4Click(TObject *Sender)
{
	Close();
}
//---------------------------------------------------------------------------
void __fastcall TForm2::Button1Click(TObject *Sender)
{
	if(OpenDialog1->Execute())
	{
		for(int h=0; h<OpenDialog1->Files->Count; h++)
		{
			ListBox1->Items->Add(OpenDialog1->Files->Strings[h]);
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm2::Button2Click(TObject *Sender)
{
	if(!IdTCPServer1->Active)
	{
        Lat=Lon=Z=0;
		IdTCPServer1->Active=true;
		IdTCPServer2->Active=true;
		Timer1->Enabled=true;
		tc=0;
		Button2->Enabled=false;
		Button3->Enabled=true;
		Button6->Enabled=true;
		SingleRButton->Enabled=false;
		InfraRButton->Enabled=false;
		AdvancedRButton->Enabled=false;
		PythonRButton->Enabled=false;
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm2::Button3Click(TObject *Sender)
{
	if(IdTCPServer1->Active)
	{
		if(thrd!=NULL)
		{
			thrd->Terminate();
			Sleep(250);
			if(TerminateThread((HANDLE)thrd->Handle, 0)>0) delete thrd;
			thrd=NULL;
		}
		IdTCPServer1->Active=false;
		IdTCPServer2->Active=false;
		Timer1->Enabled=false;
		Button3->Enabled=false;
		Button2->Enabled=true;
		Button6->Enabled=false;
		SingleRButton->Enabled=true;
		InfraRButton->Enabled=true;
		AdvancedRButton->Enabled=true;
		PythonRButton->Enabled=true;
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm2::IdTCPServer1Execute(TIdContext *AContext)
{
	AnsiString Str;
	TEncoding* Encod;
	unsigned char buf2[11];
	char r, ch;
	int i;
	TBytes B;

	//AContext->Connection->IOHandler->ReadTimeout=
	if(AContext->Connection->Connected())// && AContext->Connection->IOHandler->CheckForDataOnSource())
	{

		r=AContext->Connection->IOHandler->ReadChar();
		/*Str=AContext->Connection->IOHandler->ReadLn();
		if(Str.Length()>0)
		{
			if(Str[1]=='W' || Str[1]=='w')*/
		if(r=='W' || r=='w')
		{
			 if(SingleRButton->Checked) sprintf((buf2+4), "%s", "SINGLE1");
			 else if(InfraRButton->Checked) sprintf((buf2+4), "%s", "DigAssi");
			 else if(AdvancedRButton->Checked) sprintf((buf2+4), "%s", "DUAL_10");
			 else if(PythonRButton->Checked) sprintf((buf2+4), "%s", "PYTHON3");
			 buf2[0]=0; buf2[1]=127; buf2[2]=0; buf2[3]=127;
			 B.Length=11;
			 for(int i=0; i<11; i++) B[i]=buf2[i];
			 AContext->Connection->IOHandler->Write(B, 11, 0);
		}
		else if(r=='T' || r=='t')
		{
			 r=AContext->Connection->IOHandler->ReadChar();
			 i=0;
			 Str="";
			 while(i<r)
			 {
				 ch=AContext->Connection->IOHandler->ReadChar();
				 Str+=ch;
				 i++;
			 }
			 if(AdvancedRButton->Checked || PythonRButton->Checked)
			 {
				 buf2[0]=0; buf2[1]=127; buf2[2]=0; buf2[3]=127;
				 buf2[4]=0;
				 B.Length=5;
				 for(int i=0; i<5; i++) B[i]=buf2[i];
				 AContext->Connection->IOHandler->Write(B, 5, 0);
             }
			 if(thrd==NULL)
			 {
				 TRadarType rt;
				 if(SingleRButton->Checked) rt=SINGLE1;
				 else if(InfraRButton->Checked) rt=INFRARADAR;
				 else if(AdvancedRButton->Checked) rt=DOUBLE9;
				 else if(PythonRButton->Checked) rt=PYTHON3;
				 else rt=SINGLE1;
				 thrd=new TSetupThread(false, AContext->Connection->IOHandler, ListBox1->Items, rt, Str);
			 }
			 else
			 {
				 thrd->TuneStr=Str;
             }
		}
		//}
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm2::Timer1Timer(TObject *Sender)
{
	if(Connected) Label1->Caption="File: "+fff+", Transmitted traces: "+(AnsiString) tc;
	else Label1->Caption="Disconnected";
}
//---------------------------------------------------------------------------

void __fastcall TForm2::IdTCPServer1Connect(TIdContext *AContext)
{
	Connected=true;
}
//---------------------------------------------------------------------------

void __fastcall TForm2::IdTCPServer1Disconnect(TIdContext *AContext)
{
	Connected=false;
	if(thrd!=NULL)
	{
		thrd->Terminate();
		Sleep(250);
		if(TerminateThread((HANDLE)thrd->Handle, 0)>0) delete thrd;
		thrd=NULL;
	}
}
//---------------------------------------------------------------------------

__fastcall TSetupThread::TSetupThread(bool CreateSuspended, TIdIOHandler *io,
	TStrings* f, TRadarType rt, AnsiString ATune) : TThread(CreateSuspended)
{
	IO=io;
	Files=f;
	GetNextFile=true;
	FileIndex=0;
	TraceIndex=0;
	TxBuf=NULL;
	RadarType=rt;
	tune=ATune;
	NewTune=false;
	if(rt==DOUBLE9)
	{
		TuneCommand=TuneCommandParser(tune);
	}
	else if(rt==PYTHON3) TuneCommand=TSetupTune(1024, 1, 0);
	else TuneCommand=TSetupTune();
}

TSetupTune __fastcall TSetupThread::TuneCommandParser(AnsiString str)
{
	TSetupTune res;

	try
	{
		if(str.Length()>31)
		{
			if(str[6]=='0' && str[7]=='0') res.Samples=128;
			else if(str[6]=='1' && str[7]=='0') res.Samples=256;
			else if(str[6]=='0' && str[7]=='1') res.Samples=512;
			else if(str[6]=='1' && str[7]=='1') res.Samples=1024;

			if(str[21]=='0' && str[22]=='0') { res.ChannelsQ=1; res.ChannelN=0;}
			else if(str[21]=='0' && str[22]=='1') { res.ChannelsQ=1; res.ChannelN=1;}
			else if(str[21]=='1' && str[22]=='0')
			{
				if(str[8]=='0')	res.ChannelsQ=2;
				else res.ChannelsQ=4;
				res.ChannelN=0;
			}
			else if(str[21]=='1' && str[22]=='1')
			{
				res.ChannelsQ=1;
				if(str[8]=='0')	res.ChannelN=2;
				else res.ChannelN=3;
			}
		}
		else res=TSetupTune();
	}
	catch(Exception &e)
	{
		res=TSetupTune();
	}

	return res;
}

bool __fastcall TSetupThread::OpenFile(UnicodeString Filename)
{
	unsigned long rd;
	int i;

	try
	{
		if(TxBuf!=NULL) delete[] TxBuf;
		TxBuf=NULL;
		hndl=CreateFile(Filename.t_str(), GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
		if(hndl==INVALID_HANDLE_VALUE ) return false;
		ReadFile(hndl,header,3200,&rd,NULL);
		header[10]=0;
		i=strcmp(header,"This SEG-Y");
		if(i!=0)
		{
			return false;
		}
		if(header[3199-83]=='D') DoubleStt=true;
		else DoubleStt=false;
		ReadFile(hndl,header,400,&rd,NULL);
		samples=*(short int*)(header+20);
		traces=(GetFileSize(hndl,NULL)-3200-400)/(240+samples*2);
		TxBuf=new unsigned char[samples*2];
		//TxBuf=new unsigned char[512*2];
		TraceIndex=0;
		return true;
	}
	catch(Exception &e)
	{
		if(TxBuf!=NULL) delete[] TxBuf;
		return false;
	}
}

bool __fastcall TSetupThread::GetNextTrace()
{
	unsigned long rd;
	short sh, txsamples;
	double dval;

	try
	{
		if(TraceIndex<traces)
		{
			ReadFile(hndl,header,240,&rd,NULL);
			if(DoubleStt)
			{
				dval=*(double*)(header+182);
				Lon=dval;
				dval=*(double*)(header+190);
				Lat=dval;
			}
			else
			{
				dval=*(float*)(header+72);
				Lon=dval;
				dval=*(float*)(header+76);
				Lat=dval;
			}
			dval=*(float*)(header+40);
			Z=dval;
			ReadFile(hndl,TxBuf,samples*2,&rd,NULL);

			for(int j=0; j<samples; j++)
			{
				sh=*((short int*)TxBuf+j);
				TxBuf[j*2]=(sh&0xFF00)>>8;
				TxBuf[j*2+1]=(sh&0x00FF);
			}
			TraceIndex++;
			return true;
		}
		else
		{
			return false;
		}
	}
	catch(Exception &e)
	{
		 if(TxBuf!=NULL) delete[] TxBuf;
		 return false;
	}
}

void __fastcall TSetupThread::Execute()
{
	TBytes B;
	char buf2[24];
	int s2s;

	while(!Terminated)
	{
		if(GetNextFile)
		{
			fff=Files->Strings[FileIndex];
			if(OpenFile(Files->Strings[FileIndex])) GetNextFile=false;
			FileIndex++;
			if(FileIndex>=Files->Count) FileIndex=0;
		}
		if(!GetNextFile)
		{
			if(!GetNextTrace())
			{
				GetNextFile=true;
				CloseHandle(hndl);
			}
			else
			{
				try
				{
					//B.Length=samples*2;
					//Move((void *)TxBuf, (void *)&B[0], samples*2);
					//IO->Write(B, samples*2, 0);
					if(RadarType==SINGLE1) s2s=512;
					else if(RadarType==INFRARADAR) s2s=512+32;
					else if(RadarType==DOUBLE9)
					{
						if(NewTune)
						{
							TuneCommand=TuneCommandParser(tune);
							NewTune=false;
						}
						s2s=TuneCommand.Samples;
					}
					else if(RadarType==PYTHON3) s2s=1024;
					B.Length=s2s*2;
					if(samples<s2s)
					{
						Move((void *)TxBuf, (void *)&B[0], samples*2);
						for(int j=samples*2; j<B.Length; j++)
							B[j]=0;
					}
					else Move((void *)TxBuf, (void *)&B[0], B.Length);
					if(RadarType==DOUBLE9)
					{
						B[((s2s-(s2s>>6))<<1)+3]=TuneCommand.ChannelN<<6;

						if(TuneCommand.ChannelsQ>1)
						{
							TuneCommand.ChannelN++;
							if(TuneCommand.ChannelN>TuneCommand.ChannelsQ-1) TuneCommand.ChannelN=0;
						}
					}
					IO->Write(B, B.Length, 0);
					if(IO2!=NULL)
					{
						*(double*)(buf2+0)=Lat;
						*(double*)(buf2+8)=Lon;
						*(double*)(buf2+16)=Z;
						B.Length=24;
						Move((void *)&buf2[0], (void *)&B[0], 24);
						IO2->Write(B, 24, 0);
						//Label2->Caption=FloatToStrF(Lat, ffFixed, 10, 8)+", "+FloatToStrF(Lon, ffFixed, 10, 8);
					}
					tc++;
				}
				catch(Exception &e)
				{
					;
				}
			}
		}
		Sleep(18);
	}
	if(hndl) CloseHandle(hndl);
}

void __fastcall TForm2::CheckBox1Click(TObject *Sender)
{
	if(CheckBox1->Checked) FormStyle=fsStayOnTop;
	else FormStyle=fsNormal;
}
//---------------------------------------------------------------------------

void __fastcall TForm2::Button5Click(TObject *Sender)
{
	ListBox1->Items->Clear();
}
//---------------------------------------------------------------------------

void __fastcall TForm2::IdTCPServer2Execute(TIdContext *AContext)
{
	if(AContext->Connection->Connected())
	{
		IO2=AContext->Connection->IOHandler;
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm2::IdTCPServer2Disconnect(TIdContext *AContext)
{
	IO2=NULL;
}
//---------------------------------------------------------------------------

void __fastcall TForm2::Button6Click(TObject *Sender)
{
    if(IdTCPServer1->Active)
	{
		if(thrd!=NULL)
		{
			if(Button6->Caption=="Pause")
			{
				thrd->Suspend();
				Button6->Caption="Continue";
			}
			else
			{
				thrd->Resume();
				Button6->Caption="Pause";
			}
		}
	}
}
//---------------------------------------------------------------------------


