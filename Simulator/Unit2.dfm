object Form2: TForm2
  Left = 0
  Top = 0
  BorderStyle = bsSizeToolWin
  Caption = 'RadarMap Simulator'
  ClientHeight = 335
  ClientWidth = 440
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 222
    Width = 424
    Height = 45
    Alignment = taCenter
    AutoSize = False
    Caption = 'Disconnected'
    WordWrap = True
  end
  object Label2: TLabel
    Left = 384
    Top = 278
    Width = 31
    Height = 13
    Caption = 'Label2'
    Visible = False
  end
  object Label3: TLabel
    Left = 390
    Top = 319
    Width = 42
    Height = 10
    Caption = 'v0.4.15.0831'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clGray
    Font.Height = -8
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object ListBox1: TListBox
    Left = 8
    Top = 39
    Width = 424
    Height = 146
    ItemHeight = 13
    TabOrder = 0
  end
  object Button1: TButton
    Left = 184
    Top = 8
    Width = 75
    Height = 25
    Caption = 'Open'
    TabOrder = 1
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 184
    Top = 191
    Width = 75
    Height = 25
    Caption = 'Start'
    TabOrder = 2
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 225
    Top = 273
    Width = 75
    Height = 25
    Caption = 'Stop'
    Enabled = False
    TabOrder = 3
    OnClick = Button3Click
  end
  object Button4: TButton
    Left = 184
    Top = 304
    Width = 75
    Height = 25
    Caption = 'Close'
    TabOrder = 4
    OnClick = Button4Click
  end
  object CheckBox1: TCheckBox
    Left = 8
    Top = 16
    Width = 73
    Height = 17
    Caption = 'Stay on top'
    TabOrder = 5
    OnClick = CheckBox1Click
  end
  object Button5: TButton
    Left = 357
    Top = 8
    Width = 75
    Height = 25
    Caption = 'Clear'
    TabOrder = 6
    OnClick = Button5Click
  end
  object Button6: TButton
    Left = 144
    Top = 273
    Width = 75
    Height = 25
    Caption = 'Pause'
    Enabled = False
    TabOrder = 7
    OnClick = Button6Click
  end
  object SingleRButton: TRadioButton
    Left = 8
    Top = 191
    Width = 89
    Height = 17
    Caption = 'Single channel'
    TabOrder = 8
  end
  object InfraRButton: TRadioButton
    Left = 103
    Top = 191
    Width = 75
    Height = 17
    Caption = 'InfraRadar'
    TabOrder = 9
  end
  object AdvancedRButton: TRadioButton
    Left = 265
    Top = 191
    Width = 75
    Height = 17
    Caption = 'Advanced'
    Checked = True
    TabOrder = 10
    TabStop = True
  end
  object PythonRButton: TRadioButton
    Left = 357
    Top = 191
    Width = 75
    Height = 17
    Caption = 'Python3'
    TabOrder = 11
  end
  object IdTCPServer1: TIdTCPServer
    Bindings = <>
    DefaultPort = 23
    MaxConnections = 1
    OnConnect = IdTCPServer1Connect
    OnDisconnect = IdTCPServer1Disconnect
    OnExecute = IdTCPServer1Execute
    Left = 208
    Top = 120
  end
  object OpenDialog1: TOpenDialog
    Filter = 'SEG-Y profiles|*.sgy'
    Options = [ofHideReadOnly, ofAllowMultiSelect, ofPathMustExist, ofFileMustExist, ofEnableSizing]
    Left = 208
    Top = 72
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 100
    OnTimer = Timer1Timer
    Left = 40
    Top = 184
  end
  object IdTCPServer2: TIdTCPServer
    Bindings = <>
    DefaultPort = 24
    MaxConnections = 1
    OnDisconnect = IdTCPServer2Disconnect
    OnExecute = IdTCPServer2Execute
    Left = 280
    Top = 120
  end
end
