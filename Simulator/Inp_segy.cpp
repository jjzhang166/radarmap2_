//---------------------------------------------------------------------------
#include <vcl.h>
#include <math.h>
#pragma hdrstop

#include "Inp_segy.h"
#include "Main.h"

bool Reverse2;

void FilterOff(TProfile *Prof);
void IntReverse2(int &ival);
void FloatReverse2(float &fval);
void ShortIntReverse2(short int &sval);
void DoubleReverse2(double &dval);
void CalculGainFunction(TList *Vs, float *GF, int Smpls);

int Input_SEGY(char *Name, TProfile* Prof, TProgressBar *PB, bool SizeRestriction)
{
    Input_SEGY(Name, Prof, PB, SizeRestriction, 0, 0, 0);
}

int Input_SEGY(char *Name, TProfile* Prof, TProgressBar *PB, bool SizeRestriction, int DevidingType, int StartTrace, int Size)
// DevidingType - 0, file opening without restrictions
// DevidingType - 1, open part of file from indicated StratTrace. The size of part (in MB) couldn't be more then Size
// DevidingType - 2, open part of file from indicated StratTrace. The size of part (in traces) couldn't be more then Size
// DevidingType - 3, open part of file from indicated StratTrace and up to the next Marker.
{
    HANDLE hndl;
    char *header=new char[3201];
    char *buf=NULL;
    short int sval;
    unsigned long rd;
    int traces,samples,m,i,k,j,val;
    float fval, OldVal=0., asd, X0=0., X9=0.;
    TTrace *Trace, *BackTrace;
    int bytes_to_read,sample_format;
    float smpl, dist=0.0;
    bool DoubleStt, ProfContainsBackwardMovement=false;
    double dval;
    int k_start, k_end, m_c;
    float DepthConstant;

    BackTrace=NULL;

    hndl=CreateFile(
     Name,
     GENERIC_READ,	// access (read-write) mode
     0,
     NULL,	// pointer to security attributes
     OPEN_EXISTING,	// how to create
     FILE_ATTRIBUTE_NORMAL,// file attributes
     NULL 	// handle to file with attributes to copy
     );

    if(hndl==INVALID_HANDLE_VALUE )
    {
      LPVOID lpMsgBuf;

      FormatMessage(
        FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
        NULL,
        GetLastError(),
        MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
        (LPTSTR) &lpMsgBuf,
        0,
        NULL
      );
      // Display the string.
      Application->MessageBox((LPTSTR)lpMsgBuf, "Prism2", MB_OK|MB_ICONINFORMATION );
      // Free the buffer.
      LocalFree( lpMsgBuf );
      delete[] header;
      return 1;
    }

    try
    {
  //----------- Read SEG-Y EDCDIC Reel Header ------------------
      ReadFile(hndl,header,3200,&rd,NULL);
      Prof->Permit=0;
      Prof->ZeroPoint=0;
      *(header+10)=0;
      if(strcmp(header,"This SEG-Y")==0)
       {
       for(k=11,j=0; k<3200-81; k++)
        {
        if(*(header+k)=='\n') j++;
        if(j==5 && Prof->Permit==0) // Finding permittivity
         {
         for(i=k; i<3200-81; i++)
          if(*(header+i)=='E' && *(header+i+1)=='=') break;
         i+=2;
         for(m=i; m<3200-81; m++)
         {
           if(*(header+m)==',' || *(header+m)=='.') *(header+m)=DecimalSeparator;
           if(*(header+m)=='\r') break;
         }
         *(header+m)=0;

         try
         {
           Prof->Permit=StrToFloat((AnsiString)(header+i));
         }
         catch (Exception &exception)
         {
           Prof->Permit=5;
         }
         }
        if(j==12) break;
        }
       k++;
       for(j=3199-104; j>=k; j--)
        if(*(header+j)!=' ') break;
       *(header+j+1)=0;
       Prof->TextInfo=(header+k);
       }
      if(header[3199-83]=='D') DoubleStt=true;
      else DoubleStt=false;
      if(header[3199-81]<11 && header[3199-81]>0)
      {
        union CharPoint
        {
          TMyPoint mp;
          char c[8];
        } ChP;
        TMyPoint *mp;
        int cc=header[3199-81]; // Number of gain function points (min=2, max=10)
        for(int k=0; k<Prof->Vertexes->Count; k++)
          delete (TMyPoint*)(Prof->Vertexes->Items[k]); // Clear Profile Gain Points
        Prof->Vertexes->Clear();
        for(int k=0; k<cc; k++)
        {
          for(int z=0; z<8; z++) ChP.c[z]=header[3199-81+k*8+z+1];
          mp=new TMyPoint;
          mp->x=ChP.mp.x; // Normalized point position (0..1) in samples, float
          mp->y=ChP.mp.y; // Normalized point gain (0..1) in dB, float
          Prof->Vertexes->Add(mp);
        }
      }
      // Filter points;
      if(header[3199-103]<=4 && header[3199-103]>=0)
        Prof->FILTER_TYPE=(TFilterType)header[3199-103];
      else Prof->FILTER_TYPE=FILTER_OFF;
      if(Prof->FILTER_TYPE==FILTER_CUSTOM)
      {
        union FPoint
        {
          float i;
          char c[4];
        } FP;
        for(int k=0; k<4; k++)
        {
          for(int z=0; z<4; z++) FP.c[z]=header[3199-103+k*4+z+1];
          if(FP.i<=1 && FP.i>=0) Prof->FilterPoints[k]=FP.i;
          else
          {
            FilterOff(Prof);
            break;
          }
        }
      }
      else FilterOff(Prof);

      ReadFile(hndl,header,400,&rd,NULL);

  // Data sample format code
      sval=*(short int*)(header+300);

      sval=*(short int*)(header+24);
      //sval=3; For Nogin
      if( (sval&0xFF00) ) Reverse2=true;
      else                Reverse2=false;

      ShortIntReverse2(sval);
      sample_format=sval;
  // Line number
      val=*(int*)(header+4);
      IntReverse2(val);
  // Reel number
      val=*(int*)(header+8);
      IntReverse2(val);
  // Number of data traces per second
      sval=*(short int*)(header+12);
      ShortIntReverse2(sval);
  // Number of auxiliary traces per second
      sval=*(short int*)(header+14);
      ShortIntReverse2(sval);
  // Number of samples per trace for this reel's data
      sval=*(short int*)(header+20);
      ShortIntReverse2(sval);
      samples=sval;
      //samples=1018; For Nogin
  // Sample interval of this reel's data in MICROseconds
      sval=*(short int*)(header+16);
      ShortIntReverse2(sval);
      Prof->TimeRange=(float)sval/1000*(float)(samples-1);
      if(Prof->TimeRange==0) Prof->TimeRange=100;
      //Prof->TimeRange=187;
  // CDP fold (expected number of data traces per ensemble)
      sval=*(short int*)(header+26);
      ShortIntReverse2(sval);
  // Measuring system (1-meters, 2-feets)
      sval=*(short int*)(header+54);
      ShortIntReverse2(sval);

      Prof->Samples=samples;

      bytes_to_read=0;
      switch(sample_format)
      {
        case 1: // 32-bit IBM floating point
        case 2: // 32-bit fixed point (integer)
        case 4: // 32-bit fixed point with gain code (integer)
          bytes_to_read=samples*4;
          break;
        case 3: // 16-bit fixed point (integer)
          bytes_to_read=samples*2;
          break;
        default: bytes_to_read=0;
      }

      if(bytes_to_read==0) throw Exception("Cannot open file!");

      buf=new char[bytes_to_read];

      traces=(GetFileSize(hndl,NULL)-3200-400)/(240+bytes_to_read);

      PB->Max=traces;
      PB->Step=1;
      PB->Position=0;

      if(traces==0)
      {
        delete[] header;
        delete[] buf;
        CloseHandle(hndl);
        return 2;
      }

      // Very big file
      int fs=(int)(GetFileSize(hndl, NULL)/(1024l*1024l));
      if(SizeRestriction && fs>MainForm->MaxFileSizeMB)
      {
        Prof->Traces=traces;
        CloseHandle(hndl);
        delete[] header;
        delete[] buf;
        return (-1)*fs;
      }

      // DevidingType - 0, file opening without restrictions
// DevidingType - 1, open part of file from indicated StratTrace. The size of part (in MB) couldn't be more then Size
// DevidingType - 2, open part of file from indicated StratTrace. The size of part (in traces) couldn't be more then Size
// DevidingType - 3, open part of file from indicated StratTrace and up to the next Marker.
      switch(DevidingType)
      {
        case 0:
          k_start=0;
          k_end=traces;
          break;
        case 1:
          if(StartTrace<0) StartTrace=0;
          if(StartTrace>traces) StartTrace=traces;
          k_start=StartTrace;
          k_end=k_start+(Size*1024*1024-3200-400)/(240+bytes_to_read);
          if(k_end>traces) k_end=traces;
          break;
        case 2:
          if(StartTrace<0) StartTrace=0;
          if(StartTrace>traces) StartTrace=traces;
          k_start=StartTrace;
          k_end=k_start+Size;
          if(k_end>traces) k_end=traces;
          break;
        case 3:
          if(StartTrace<0) StartTrace=0;
          if(StartTrace>traces) StartTrace=traces;
          k_start=StartTrace;
          k_end=traces;
          break;
      }
      SetFilePointer(hndl, (240+bytes_to_read)*k_start, NULL, FILE_CURRENT);
      m_c=0;

      DepthConstant=Prof->TimeRange*1e-9*3e8/2.0/sqrt(Prof->Permit);
      for(k=k_start; k<k_end; k++)
      {
         ReadFile(hndl,header,240,&rd,NULL);
         ReadFile(hndl,buf,bytes_to_read,&rd,NULL);
    // Check 'dead' trace
         val=*(short int*)(header+28);
         IntReverse2(val);
         if(val==2) continue;
    // Lag time between shot and recording start in PICOseconds
         if(Prof->ZeroPoint==0)
         {
            sval=*(short int*)(header+108);
            ShortIntReverse2(sval);
            if(Prof->TimeRange>0) Prof->ZeroPoint=(float)sval/1000/Prof->TimeRange*(float)(samples-1);
            else Prof->ZeroPoint=0;
         }

         Trace=new TTrace(samples);
    // Marks indicator
         sval=*(short int*)(header+236);
         ShortIntReverse2(sval);
         if(sval==0x5555)
         {
            // Mark
            sval=*(short int*)(header+238);
            ShortIntReverse2(sval);
            Trace->Mark=sval;
            if(sval>0) m_c++;
         }
         for(j=0; j<samples; j++)
         {
           switch(sample_format)
           {
             case 1: // 32-bit IBM floating point
               fval=*((float*)buf+j);
               FloatReverse2(fval);
               val=(int)fval;
               break;
             case 2: // 32-bit fixed point (integer)
               val=*((int*)buf+j);
               IntReverse2(val);
               break;
             case 3: // 16-bit fixed point (integer)
               sval=*((short int*)buf+j);
               ShortIntReverse2(sval);
               val=sval;
               break;
             case 4: // 32-bit fixed point with gain code (integer)
               val=*((int*)buf+j);
               IntReverse2(val);
               break;
           }
           Trace->SourceData[j]=(short int)val;
         }
         /*short int tH, tM, tS;
         tH=*(short int*)(header+160);
         tM=*(short int*)(header+162);
         tS=*(short int*)(header+164);
         Trace->GreenwichTm=TDateTime(tH, tM, tS, 0);*/
         val=*((int*)(header+48));
         if(val<0)
         {
           Trace->Backward=true;
         }
         else Trace->Backward=false;

         sval=*((short int*)(header+88));
         ShortIntReverse2(sval);
         if(sval==1) // if not arc, seconds
         {
            val=*(int*)(header+72);
            IntReverse2(val);
            asd=(float)val/1000.;
            Trace->TraceDistance=asd-OldVal;
            dist+=Trace->TraceDistance;
            OldVal=asd;
            val=*(int*)(header+80);
            IntReverse2(val);
            if(k==k_start) X0=val;
            if(k==k_end-1) X9=val;
            Trace->X=val;
            val=*(int*)(header+76);
            IntReverse2(val);
            Trace->Y=val;
         }
         else
         {
           val=*(int*)(header+80);
           IntReverse2(val);
           if(k==k_start) X0=val;
           if(k==k_end-1) X9=val;
           Trace->X=val;
           if(DoubleStt)
           {
             dval=*(double*)(header+182);
             DoubleReverse2(dval);
             Trace->Longitude=dval;
             dval=*(double*)(header+190);
             DoubleReverse2(dval);
             Trace->Latitude=dval;
           }
           else
           {
             fval=*(float*)(header+72);
             FloatReverse2(fval);
             Trace->Longitude=fval;
             fval=*(float*)(header+76);
             FloatReverse2(fval);
             Trace->Latitude=fval;
           }
           fval=*(float*)(header+40);
           FloatReverse2(fval);
           Trace->Z=fval;
           fval=*(float*)(header+44);
           FloatReverse2(fval);
           Trace->Z_Geoid=fval;
           Prof->Coordinated=true;
         }
         dval=*(double*)(header+198);
         DoubleReverse2(dval);
         Trace->UTMNorthing=dval;
         dval=*(double*)(header+206);
         DoubleReverse2(dval);
         Trace->UTMEasting=dval;
         Trace->UTMZone=*(header+214);
         fval=*(float*)(header+84);
         FloatReverse2(fval);
         Prof->YPosition=fval;

         fval=*(float*)(header+52);
         fval/=DepthConstant;
         fval*=(float)Prof->Samples;
         Trace->SamplesShifting=(int)fval;
         
         if(Trace->Backward)
         {
           if(BackTrace==NULL) BackTrace=Prof->LastTracePtr;
           else if(BackTrace!=NULL) BackTrace=BackTrace->PtrDn;
           if(BackTrace==NULL)
           {
             if(Prof->FirstTracePtr==NULL) Prof->Add(Trace);
             BackTrace=Prof->FirstTracePtr;
           }
           // else can't be used !!!
           if(BackTrace!=NULL) //Replace existing trace by bacward
           {
             BackTrace->CopyTrace(Trace);
             dist-=Trace->TraceDistance;
             delete Trace;
             ProfContainsBackwardMovement=true;
           }
         }
         else
         {
           if(BackTrace!=NULL)
           {
             BackTrace=BackTrace->PtrUp;
             if(BackTrace!=NULL)
             {
               dist-=BackTrace->TraceDistance;
               BackTrace->CopyTrace(Trace);
               delete Trace;
             }
             else Prof->Add(Trace);
           }
           else Prof->Add(Trace);
         }
         if(DevidingType==3 && m_c>0)
         {
           if(Prof->Traces>1) k_end=k;
           else m_c=0;
         }
         PB->StepIt();
      }
      traces=k_end-k_start;

      sval=*(short int*)(header+70);
      ShortIntReverse2(sval);
      Prof->HorRangeFrom=X0;
      Prof->HorRangeTo=X9;
      if(sval>=0)
      {
        Prof->HorRangeFrom*=(float)sval;
        Prof->HorRangeTo*=(float)sval;
      }
      else
      {
        Prof->HorRangeFrom/=(float)(-sval);
        Prof->HorRangeTo/=(float)(-sval);
      }
      Prof->HorRangeFrom-=Prof->FirstTracePtr->TraceDistance;
      if(Prof->HorRangeFrom<0) Prof->HorRangeFrom=0;

      if(ProfContainsBackwardMovement)
      {
        Prof->HorRangeTo=Prof->HorRangeFrom+dist;
        Prof->FileModified=true;
      }

      Prof->HorRangeFull=fabs(Prof->HorRangeTo-Prof->HorRangeFrom);

      if(Prof->Coordinated)
      {
        long double lat1, lon1, lat2, lon2, a, b, xx, dd, old_dd=0.;

        dist=0.0;
        Trace=Prof->FirstTracePtr;
        while(Trace!=NULL)
        {
          if(Trace->PtrDn!=NULL)
          {
            lat1=Trace->Latitude/100.;
            lon1=Trace->Longitude/100.;
            if(!MainForm->GPSTrue)
            {
              lat2=Prof->FirstTracePtr->Latitude/100.;
              lon2=Prof->FirstTracePtr->Longitude/100.;
            }
            else
            {
              lat2=Trace->PtrDn->Latitude/100.;
              lon2=Trace->PtrDn->Longitude/100.;
            }
            a=lat1-(int)lat1;
            lat1=(long double)100.*a/(long double)60.+(int)lat1;
            a=lat2-(int)lat2;
            lat2=(long double)100.*a/(long double)60.+(int)lat2;
            a=lon1-(int)lon1;
            lon1=(long double)100.*a/(long double)60.+(int)lon1;
            a=lon2-(int)lon2;
            lon2=(long double)100.*a/(long double)60.+(int)lon2;

            a=sinl(lat1/(long double)57.29577951)*sinl(lat2/(long double)57.29577951);
            b=cosl(lat1/(long double)57.29577951)*cosl(lat2/(long double)57.29577951)*cosl(lon2/(long double)57.29577951-lon1/(long double)57.29577951);
            xx=a+b;
            if((-xx*xx+(long double)1.)>0) dd=(long double)6367444.65*(atanl(-xx/sqrtl(-xx*xx+(long double)1.))+(long double)2.*atanl((long double)1.));
            else dd=0;

            if(!MainForm->GPSTrue)
            {
              Trace->TraceDistance=dd-old_dd;
              old_dd=dd;
            }
            else Trace->TraceDistance=dd;
            dist+=Trace->TraceDistance;
          }
          k++;
          Trace=Trace->PtrUp;
        }
        Prof->FirstTracePtr->TraceDistance=Prof->FirstTracePtr->PtrUp->TraceDistance/2.;
        Prof->FirstTracePtr->PtrUp->TraceDistance/=2.;
      }

      if(Prof->HorRangeFull<0.05)
      {
        Prof->HorRangeFull=Prof->HorRangeTo=dist;
        Prof->HorRangeFrom=0;
      }

      /**/
      CalculGainFunction(Prof->Vertexes, Prof->GainFunction, Prof->Samples);
      Trace=Prof->FirstTracePtr;
      while(Trace!=NULL)
      {
        for(int i=0; i<Prof->Samples; i++)
        {
          smpl=(float)Trace->SourceData[i]*Prof->GainFunction[i];
          if(smpl>32767) Trace->Data[i]=32767.;
          else if(smpl<-32768) Trace->Data[i]=-32768.;
          else Trace->Data[i]=(short int)smpl;
        }
        Trace=Trace->PtrUp;
      }
      CloseHandle(hndl);
      delete[] header;
      delete[] buf;
    }
    catch(Exception &exception)
    {
      CloseHandle(hndl);
      delete[] header;
      if(buf) delete[] buf;
      return 1;
    }
    return 0;
}
//---------------------------------------------------------------------------

void FilterOff(TProfile *Prof)
{
  Prof->FILTER_TYPE=FILTER_OFF;
  Prof->FilterPoints[0]=0.01;
  Prof->FilterPoints[1]=0.05;
  Prof->FilterPoints[2]=0.2;
  Prof->FilterPoints[3]=0.5;
}

void IntReverse2(int &ival)
{
    union Dat
     {
     int i;
     char c[4];
     }Dat;
    char ch;

    if(!Reverse2) return;

    Dat.i=ival;
    ch=Dat.c[0];
    Dat.c[0]=Dat.c[3];
    Dat.c[3]=ch;
    ch=Dat.c[1];
    Dat.c[1]=Dat.c[2];
    Dat.c[2]=ch;
    ival=Dat.i;
}

void ShortIntReverse2(short int &sval)
{
    union Dat
     {
     short int i;
     char c[2];
     }Dat;
    char ch;

    if(!Reverse2) return;

    Dat.i=sval;
    ch=Dat.c[0];
    Dat.c[0]=Dat.c[1];
    Dat.c[1]=ch;
    sval=Dat.i;
}

void FloatReverse2(float &fval)
{
    union Dat
     {
     float f;
     char c[4];
     }Dat;
    char ch;

    if(!Reverse2) return;

    Dat.f=fval;
    ch=Dat.c[0];
    Dat.c[0]=Dat.c[3];
    Dat.c[3]=ch;
    ch=Dat.c[1];
    Dat.c[1]=Dat.c[2];
    Dat.c[2]=ch;
    fval=Dat.f;
}

void CalculGainFunction(TList *Vs, float *GF, int Smpls)
{
   float delta, bp, ep, tmp;
   int bs, es;
   int k=0;

   while(k<Vs->Count-1)
   {
     tmp=((TMyPoint*)Vs->Items[k])->y*(float)84.;
     bp=pow(10., tmp/20.);
     tmp=((TMyPoint*)Vs->Items[k+1])->y*(float)84.;
     ep=pow(10., tmp/20.);
     bs=(int)(((TMyPoint*)Vs->Items[k])->x*(float)Smpls);
     es=(int)(((TMyPoint*)Vs->Items[k+1])->x*(float)Smpls);
     delta=(ep-bp)/(float)(es-bs);
     for(int i=bs; i<es; i++) GF[i]=(float)(i-bs)*delta+(float)bp;
     k++;
   }
}

void DoubleReverse2(double &dval)
{
    union Dat
     {
     double d;
     char c[8];
     }Dat;
    char ch;

    if(!Reverse2) return;

    Dat.d=dval;
    for(int i=0; i<4; i++)
    {
      ch=Dat.c[i];
      Dat.c[i]=Dat.c[7-i];
      Dat.c[i]=ch;
    }
    dval=Dat.d;
}

#pragma package(smart_init)
