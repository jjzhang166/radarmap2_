//---------------------------------------------------------------------------

#ifndef TCPClientH
#define TCPClientH

#include <IdBaseComponent.hpp>
#include <IdComponent.hpp>
#include <IdTCPClient.hpp>
#include <IdTCPConnection.hpp>
//---------------------------------------------------------------------------

class TTCPClient : public TObject
{
private:
	TIdTCPClient* _client;
protected:
	bool __fastcall readConnected();
	AnsiString __fastcallreadIPAddress();
	int __fastcall readPort();
	int __fastcall readReadTimeout();
	void __fastcall writeReadTimeout(int value);
	AnsiString __fastcall readIPaddress();
public:
	__property bool Connected = {read = readConnected };
	__property AnsiString IPaddress = {read = readIPaddress };
	__property int Port = {read = readPort };
	__property int ReadTimeout = {read=readReadTimeout, write=writeReadTimeout};

	__fastcall TTCPClient(Classes::TComponent* AOwner);
	__fastcall ~TTCPClient();

	bool __fastcall Connect(AnsiString IPaddr, int port, int Timeout);
	bool __fastcall Disconnect();
	char __fastcall ReadChar();
	System::Byte __fastcall ReadByte(void);
	void __fastcall ReadBytes(Sysutils::TBytes &VBuffer, int AByteCount, bool AAppend = true);
	AnsiString __fastcall ReadString(int N);
    AnsiString __fastcall ReadLn();
	void __fastcall WriteLn(AnsiString Str);
};

#endif
