//---------------------------------------------------------------------------

#ifndef PreviewH
#define PreviewH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <GR32_Image.hpp>
#include <ComCtrls.hpp>
#include <Dialogs.hpp>
#include <ExtCtrls.hpp>
#include "Defines.h"
#include "ImageMap.h"
#include "VectorMap.h"
#include "VisualToolObjects.h"

//---------------------------------------------------------------------------
class TPreviewThread;

class TPreviewForm : public TForm
{
__published:	// IDE-managed Components
	TOpenDialog *OpenDialog;
	TImgView32 *Image;
	TSaveDialog *SaveDialog;
	TOpenDialog *GMLOpenDialog;
	TSaveDialog *ExportDialog;
	void __fastcall FormResize(TObject *Sender);
	void __fastcall OpenDialogSelectionChange(TObject *Sender);
	void __fastcall OpenDialogClose(TObject *Sender);
	void __fastcall SaveDialogSelectionChange(TObject *Sender);
	void __fastcall OpenDialogShow(TObject *Sender);
	void __fastcall SaveDialogShow(TObject *Sender);
	void __fastcall GMLOpenDialogSelectionChange(TObject *Sender);
	void __fastcall FormShow(TObject *Sender);
	void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
private:	// User declarations
	bool NoPreview;
	TObject* Manager;
	int previewinprogress;
	unsigned previewthreadid;
	bool openorsave;
	TToolProgressBar *myprogress;
public:		// User declarations
	TPreviewThread* LastPreviewThrd;

	__fastcall TPreviewForm(TComponent* Owner, TObject* RadarMapManager);
	__fastcall ~TPreviewForm() {delete myprogress;}
	void __fastcall DoPreview(AnsiString FileName);

	__property int PreviewInProgress = {read = previewinprogress, write = previewinprogress};
	__property unsigned PreviewThreadID = {read = previewthreadid, write = previewthreadid};
	__property bool OpenOrSave = {write = openorsave};
	__property TToolProgressBar *MyProgress = {read=myprogress};
};

void PreviewWriteText(TImgView32 *Image, AnsiString str);

class TPreviewThread : public TThread
{
private:
	TImgView32 *Image;
	TMapsContainer *Container;
	AnsiString FileName, temp;
	TBitmap32 *Bitmap;
	bool *NoPreview;
	TObject* Owner;
	void* settings;
	HANDLE StopItEvent;

	void __fastcall Execute();
	void __fastcall Visualize();
	//void __fastcall StepIt() {if(Progress!=NULL && WaitForSingleObject(StopItEvent, 0)!=WAIT_OBJECT_0) {Progress->StepIt(); Progress->Repaint();}}
public:
	__fastcall TPreviewThread(TObject* AOwner, TImgView32 *img, AnsiString fn, void *ASettings, bool *np);
	__fastcall ~TPreviewThread() {CloseHandle(StopItEvent); if(Container) delete Container; Container=NULL;}
	void __fastcall StopIt();
};
//---------------------------------------------------------------------------
extern PACKAGE TPreviewForm *PreviewForm;
//---------------------------------------------------------------------------
#endif
