#ifndef ProfileH
#define ProfileH

#include <vcl.h>
#include "CacheManager.h"
#include "Spar.h"
#include "MyMath.h"

#define ALLOCATE_SAMPLES_NEW
#undef ALLOCATE_SAMPLES_NEW
#ifndef ALLOCATE_SAMPLES_NEW
	#define ALLOCATE_SAMPLES_HEAP
#endif

enum FKindEnum2 {Simple2=0, Ormsby2=1, Notch2=2};
enum TFilterType {FILTER_OFF=0, FILTER_WEAK=1, FILTER_STRONG=2, FILTER_SUPERSTRONG=3, FILTER_CUSTOM=4};
enum TTraceArrayType {tatData, tatSource, tatFilter};
enum TCharXUnits {cxuTraces='T', cxuDistance='D'};
enum TCharYUnits {cyuSamples='S', cyuTime='T', cyuDepth='D', cyuAltitude='A'};
enum TSampleFormat {sfFloat32bit = 1, sfInt32bit = 2, sfShort16bit = 3, sfIntGain32bit = 4};

TCharXUnits CheckXUnits(char ch) {if(ch=='T' || ch=='D') return (TCharXUnits)ch; else return cxuTraces;}
TCharYUnits CheckYUnits(char ch) {if(ch=='S' || ch=='T' || ch=='D' || ch=='A') return (TCharYUnits)ch; else return cyuTime;}

struct TMyPointStruct
{
    float x;
    float y;
};

class TMyPoint : public TObject
{
private:
	float xx;
	float yy;
public:
	__fastcall TMyPoint() : TObject() {xx=yy=0;}
	__fastcall TMyPoint(float ax, float ay) : TObject() {xx=ax; yy=ay;}
	__fastcall ~TMyPoint() {}
	__property float x = {read=xx, write=xx};
	__property float y = {read=yy, write=yy};
};

class TProfile;

//--- TTrace class definition
class TTrace
{
private:
	TTrace *ptrup, *ptrdn;
	unsigned __int64 validity;
	TProfile *owner;
	TBusyLocker *BusyLocker;
	int MaxInTrace;
	short int samples;
	TCacheArrayObject *DataCache, *SourceCache, *FilterCache;
	TSample *private_data, *private_sourcedata, *private_filterdata;
	unsigned long lastaccess;
	TFlagedValue<float> pitch, roll, heading;
	bool cachedbyowner;
	TDateTime timestamp;
	float horpos;
	TSample* __fastcall AllocateSamples(int N, TProfile* NewOwner=NULL);
	void __fastcall ReleaseSamples(TSample *array);
	//void __fastcall ReSample(short int *In, short int *Out);
	TSample* __fastcall readPrivateData();
	TSample* __fastcall readPrivateSourceData();
	TSample* __fastcall readPrivateFilterData();
	void __fastcall writePrivateData(TSample* value) {private_data=value;}
	void __fastcall writePrivateSourcedata(TSample* value) {private_sourcedata=value;}
	void __fastcall writePrivateFilterdata(TSample* value) {private_filterdata=value;}
	bool __fastcall readFilterDataExists() {return ((bool)private_filterdata | (bool)FilterCache);}
	unsigned __int64 __fastcall GetValidityValue() { return (unsigned __int64)private_data+(unsigned __int64)private_sourcedata+(unsigned __int64)private_filterdata+(unsigned __int64)ptrup+(unsigned __int64)ptrdn;}
	void __fastcall CalcValidity() {validity=GetValidityValue();}
	bool __fastcall CheckValidity() {return validity==GetValidityValue();}
protected:
	//TSample *data, *sourcedata, *filterdata;
	__property TSample *data = {read=readPrivateData, write=writePrivateData};
	__property TSample *sourcedata = {read=readPrivateSourceData, write=writePrivateSourcedata};
	__property TSample *filterdata = {read=readPrivateFilterData, write=writePrivateFilterdata};
	bool ApplyValidity;
	short int __fastcall readData(int Index) {if(data && Index>=0 && Index<samples) {if(Index==0) lastaccess=::GetTickCount(); return (short int)data[Index];} else return 0;}
	virtual short int __fastcall readSourceData(int Index) {if(sourcedata && Index>=0 && Index<samples) return (short int)sourcedata[Index]; else return 0;}
	short int __fastcall readFilterData(int Index) {if(filterdata && Index>=0 && Index<samples) return (short int)filterdata[Index]; else return 0;}
	void __fastcall writeData(int Index, short int value) {if(data && Index>=0 && Index<samples) data[Index]=value;}
	virtual void __fastcall writeSourceData(int Index, short int value) {if(sourcedata && Index>=0 && Index<samples) sourcedata[Index]=value;}
	void __fastcall writeFilterData(int Index, short int value) {if(filterdata && Index>=0 && Index<samples) filterdata[Index]=value;}/**/
	void __fastcall writeSamples(short int value);
	unsigned long __fastcall readSizeInMemory() {return sizeof(TTrace)+samples*sizeof(TSample)*(2+1*FilterDataExists);}
	void __fastcall writePtrUp(TTrace* value) {ptrup=value; CalcValidity();}
	void __fastcall writePtrDn(TTrace* value) {ptrdn=value; CalcValidity();}
public:
	__fastcall TTrace(int new_samples, TProfile* AOwner=NULL, bool aApplyValidity=true);
	__fastcall ~TTrace();
	int GetMaxInTrace(void);
	int GetMaxInSourceTrace(void);
	int Mark;
	float X;
	float Y;
	float Z;
	float Z_Geoid;
	double Latitude;
	double Longitude;
	float GPSConfidence;
	float LocalCsX;
	float LocalCsY;
    float LocalCsH;
	char LocalCsCh1;
	char LocalCsCh2;
	TDateTime GreenwichTm;
    float TraceDistance;
	bool GPSCoordinated, GPSInterpolated;
	int Channel;
	int PositiveMax, NegativeMax;
	bool Drawed, Drawed2, Backward, Saved;
	int SamplesShifting; // In case of Topography
	int Index;
	
	//SPAR
	Spar* SparDataPtr;
	//short int *Data, *SourceData, *FilterData

	bool __fastcall CopyTraceDataOnly(TTrace *Src);
	bool __fastcall CopyTrace(TTrace *Src, bool InvertData=false);
	void __fastcall ReSample(TSample *In, TSample *Out, int NewSamples);
	void __fastcall ReSample(TSample *In, TTraceArrayType t, int NewSamples) {TSample *out; switch(t) {case tatData: out=data; case tatSource: out=sourcedata; case tatFilter: out=filterdata; default: out=NULL;} ReSample(In, out, NewSamples);}
	void __fastcall ReSample(TTraceArrayType t, TSample *Out, int NewSamples) {TSample *in; switch(t) {case tatData: in=data; case tatSource: in=sourcedata; case tatFilter: in=filterdata; default: in=NULL;} ReSample(in, Out, NewSamples);}
	TSample* __fastcall GetArrayAddress(TTraceArrayType t); // Unsafe !!!
	void __fastcall CacheIt(bool ProfileCache=false);
	void __fastcall CacheIt(TTraceArrayType t, bool ProfileCache=false);
	void __fastcall AssignTraceArray(TTraceArrayType t, TSample *In);
	void __fastcall ReleaseFilterData();
	void __fastcall SetOwner(TProfile* AOwner);

	__property short int Samples = {read=samples, write=writeSamples};
	__property short int SourceData[int Index] = {read=readSourceData, write=writeSourceData};
	__property short int Data[int Index] = {read=readData, write=writeData};
	__property short int FilterData[int Index] = {read=readFilterData, write=writeFilterData};
	__property bool FilterDataExists = {read=readFilterDataExists};
	__property unsigned long LastAccess = {read=lastaccess}; //in Ticks
	__property unsigned long SizeInMemory = {read=readSizeInMemory};
	__property TFlagedValue<float> Pitch = {read=pitch, write=pitch}; //in radians;
	__property TFlagedValue<float> Roll = {read=roll, write=roll}; //in radians;
	__property TFlagedValue<float> Heading = {read=heading, write=heading}; //in radians
	__property TProfile* Owner = {read=owner};
	__property bool CachedByOwner = {read=cachedbyowner};
	__property TDateTime TimeStamp = {read=timestamp, write=timestamp};
	__property float HorPos = {read=horpos, write=horpos};
	__property TTrace* PtrUp = {read=ptrup, write=writePtrUp}; // Pointer to parent
	__property TTrace* PtrDn = {read=ptrdn, write=writePtrDn}; // Pointer to child
	__property bool Valid = {read=CheckValidity};
};
//--------------------------------------------------------------

class TTraceAccumulator : public TTrace
{
private:
	float accumulatedcoef;
	int AccumulationsQ;
	short int __fastcall readSourceData(int Index) {return TTrace::readSourceData(Index);}
	void __fastcall writeSourceData(int Index, short int value) {TTrace::writeSourceData(Index, value);}
public:
	__fastcall TTraceAccumulator(int new_samples, TProfile *AOwner=NULL);
	void __fastcall Accumulate(TTrace* ptr, double coef);
	void __fastcall Accumulate2(TTrace* ptr, double coef);
	void __fastcall AccumulateAndAverage(TTrace* ptr);
	void __fastcall Discharge();

	__property float AccumulatedCoef = {read=accumulatedcoef};
};
//--------------------------------------------------------------

class TTraceIterator
{
public:
	TTraceIterator(void);
	void SetFirstTracePtr(TTrace *ptr);
	TTrace* GetPtr(int index);
	TTrace* GetCurrentPtr(void);
	TTrace* GetNext(void);
	TTrace* GetPrev(void);
	int GetCurrentIndex(void);
private:
	TTrace* Ptr;
public:
	int Index;
};
//--------------------------------------------------------------

class TProfileWin : public TObject
{
private:
protected:
	int leftindex, rightindex;
	float leftpos, rightpos;
	TList *profiles, *lefttraces, *righttraces;
	__fastcall int readLeftIndex() {return leftindex;}
	__fastcall int readRightIndex() {return rightindex;}
	__fastcall float readLeftPos() {return leftpos;}
	__fastcall float readRightPos() {return rightpos;}
	TTrace* __fastcall readLeftTraces(int Index) {if(Index>=0 && Index<lefttraces->Count) return (TTrace*)lefttraces->Items[Index]; else return NULL;}
	TTrace* __fastcall readRightTraces(int Index) {if(Index>=0 && Index<righttraces->Count) return (TTrace*)righttraces->Items[Index]; else return NULL;}
	TTrace* __fastcall readLeftTrace() {return LeftTraces[0];}
	TTrace* __fastcall readRightTrace() {return RightTraces[0];}
	virtual void __fastcall writeLeftIndex(int value);
	virtual void __fastcall writeRightIndex(int value);
	void __fastcall writeLeftPos(float value);
	void __fastcall writeRightPos(float value);
	virtual void __fastcall writeLeftTraces(int Index, TTrace* value);
	virtual void __fastcall writeRightTraces(int Index, TTrace* value);
	void __fastcall writeLeftTrace(TTrace* value) {LeftTraces[0]=value;}
	void __fastcall writeRightTrace(TTrace* value) {RightTraces[0]=value;}
	int __fastcall readProfilesCount() {return profiles->Count;}
	TProfile* __fastcall readProfile();
	void __fastcall writeProfile(TProfile *value);
	TProfile* __fastcall readProfiles(int Index);
	bool __fastcall Init();
public:
	__fastcall TProfileWin(TProfile* Prof);
	__fastcall TProfileWin(TProfileWin* ProfWin);
	__fastcall ~TProfileWin();

	TTrace* __fastcall GetTraceByIndex(int Index, int ProfIndex, TTrace* LastGot);
	TTrace* __fastcall GetTraceByIndex(int Index, int ProfIndex) {return GetTraceByIndex(Index, ProfIndex, NULL);}
	TTrace* __fastcall GetTraceByIndex(int Index) {return GetTraceByIndex(Index, 0, NULL);}
	TTrace* __fastcall GetTraceByIndex(int Index, TProfile *prof) {return GetTraceByIndex(Index, profiles->IndexOf((void*)prof), NULL);}
	TTrace* __fastcall GetTraceByPos(float Pos, int ProfIndex, TTrace* LastGot);
	TTrace* __fastcall GetTraceByPos(float Pos, int ProfPos) {return GetTraceByPos(Pos, ProfPos, NULL);}
	TTrace* __fastcall GetTraceByPos(float Pos) {return GetTraceByPos(Pos, 0, NULL);}
	TTrace* __fastcall GetTraceByPos(float Pos, TProfile *prof) {return GetTraceByPos(Pos, profiles->IndexOf((void*)prof), NULL);}

	int __fastcall Width() {return rightindex-leftindex;}
	float __fastcall WidthInMeters() {return rightpos-leftpos;}
	virtual void __fastcall CenterAtPos(int Index) {int w=Width(); Index+=w>>1;}
	virtual int __fastcall AddProfile(TProfile *Prof);
	int __fastcall GetProfileIndex(TProfile *Prof) {return profiles->IndexOf((void*)Prof);}
	bool __fastcall IsInWin(int Index) {return (Index>=leftindex && Index<=rightindex);}

	__property int ProfilesCount = {read=readProfilesCount};
	__property TProfile* Profiles[int Index] = {read=readProfiles};
	__property TProfile* Profile = {read=readProfile};//, write=writeProfile};
	__property int LeftIndex = {read=readLeftIndex, write=writeLeftIndex};
	__property int RightIndex = {read=readRightIndex, write=writeRightIndex};
	__property float LeftPos = {read=readLeftPos, write=writeLeftPos};
	__property float RightPos = {read=readRightPos, write=writeRightPos};
	__property TTrace* LeftTrace = {read=readLeftTrace, write=writeLeftTrace};
	__property TTrace* RightTrace = {read=readRightTrace, write=writeRightTrace};
	__property TTrace* LeftTraces[int Index] = {read=readLeftTraces, write=writeLeftTraces};
	__property TTrace* RightTraces[int Index] = {read=readRightTraces, write=writeRightTraces};
};
//--------------------------------------------------------------

//All profiles has to have the same number of the traces!!!
class TProfileOutputView : public TProfileWin
{
private:
	//TTrace *lefttrace, *righttrace;
	int scrollingleft, scrollingright;
	bool inscrolling;
	TObject *path;
	TList *afterscrollingevents, *finishscrollingevents, *clearoutputviewevents;
protected:
	void __fastcall writeLeftIndex(int value) {TProfileWin::writeLeftIndex(value); scrollingleft=leftindex;}
	void __fastcall writeRightIndex(int value) {TProfileWin::writeRightIndex(value); scrollingright=rightindex;}
	void __fastcall writeLeftTraces(int Index, TTrace* value) {TProfileWin::writeLeftTraces(Index, value); InScrolling=false;}
	void __fastcall writeRightTraces(int Index, TTrace* value) {TProfileWin::writeRightTraces(Index, value); InScrolling=false;}
	void __fastcall writeScrollingLeft(int value);
	void __fastcall writeScrollingRight(int value);
	void __fastcall writeInScrolling(bool value) {inscrolling=value;}
	HANDLE __fastcall readAfterScrollingEvent(int Index) {if(Index>=0 && Index<afterscrollingevents->Count) return (HANDLE)afterscrollingevents->Items[Index]; else return NULL;}
	HANDLE __fastcall readFinishScrollingEvent(int Index) {if(Index>=0 && Index<finishscrollingevents->Count) return (HANDLE)finishscrollingevents->Items[Index]; else return NULL;}
	HANDLE __fastcall readClearOutputViewEvent(int Index) {if(Index>=0 && Index<clearoutputviewevents->Count) return (HANDLE)clearoutputviewevents->Items[Index]; else return NULL;}
public:
	__fastcall TProfileOutputView(TProfile* Prof);
	__fastcall ~TProfileOutputView();

	void __fastcall CenterAtPos(int Index);
	int __fastcall AddProfile(TProfile *Prof);
	void __fastcall ApplyScrolling();
	int __fastcall ScrollingWidth() {return scrollingright-scrollingleft;}
	bool __fastcall IsInOutputView(int Index) {return TProfileWin::IsInWin(Index);}
	void __fastcall ClearAllOutputViews();

	__property bool InScrolling = {read=inscrolling, write=writeInScrolling};
	__property int ScrollingLeftIndex = {read=scrollingleft, write=writeScrollingLeft};
	__property int ScrollingRightIndex = {read=scrollingright, write=writeScrollingRight};
	__property HANDLE AfterScrollingEvents[int Index] = {read=readAfterScrollingEvent};
	__property HANDLE ClearOutputViewEvents[int Index] = {read=readClearOutputViewEvent};
	__property HANDLE FinishScrollingEvents[int Index] = {read=readFinishScrollingEvent};
	__property TObject* Path={read=path, write=path};
};

//-------- TProfile class definition
class TProfile//: public TObject
{
private:
	int samples, MaxInFile;
	float permit, timerange, depth;
	float zeropoint;
	long traces;
	bool zeropointadjusted, InRestoring;
	HANDLE ProfileLockEvent, tracesaddedevent;
	TTrace *lasttraceptr, *firsttraceptr, *currenttraceptr;
	TTraceAccumulator *commontrace;
	AnsiString fullname, name;
	TProfileOutputView *outputview;
	TCacheProfileDataObject *ProfileCache;
	//TList *figures;
	bool outputviewowner;
	bool cachingisprohibited;
	unsigned long lastaccess;
	TSampleFormat sampleformat;
#ifdef ALLOCATE_SAMPLES_HEAP
	HANDLE hheap;
#endif
	void __fastcall DepthRecalcul();
	void __fastcall writeSamples(int value);
	void __fastcall writePermit(float value) {if(value>=1. && value<=100.) {permit=value; DepthRecalcul();}}
	void __fastcall writeTimeRange(float value) {timerange=value; DepthRecalcul();}
protected:
	float __fastcall readZeroPoint() {return zeropoint;}
	void __fastcall writeZeroPoint(float value) {zeropoint=value; zeropointadjusted=true;}
	void __fastcall writeFirstTracePtr(TTrace* ptr);
	TTrace* __fastcall readFirstTracePtr();
	void __fastcall writeLastTracePtr(TTrace* ptr);
	TTrace* __fastcall readLastTracePtr();
	void __fastcall writeCurrentTracePtr(TTrace* ptr);
	TTrace* __fastcall readCurrentTracePtr();
	void __fastcall writeFullName(AnsiString value);
	void __fastcall writeOutputView(TProfileOutputView *value);
	unsigned long __fastcall readSizeOnDiskAsSEGY() {return (3200+400+(240+samples*sizeof(short int))*traces);}
	unsigned long __fastcall readSizeInMemory() {unsigned long ul=sizeof(TProfile); if(LastTracePtr) ul+=LastTracePtr->SizeInMemory*traces; return ul;}
	bool __fastcall readTraceDataCached() { return (bool)ProfileCache;}
public:
	__fastcall TProfile();
	__fastcall TProfile(TObject *goc);
	__fastcall ~TProfile();
	void __fastcall ProfileInit(void);
	int __fastcall GetMaxInFile(void);
	int __fastcall GetMaxInSourceFile(void);
	void __fastcall Add(TTrace *ptr, bool WithLocking=false);
	void __fastcall AddNewTrace(TTrace* ptr, bool WithLocking=false);
	bool __fastcall DeleteFirstTrace(void);
	void __fastcall Clear(bool FullCleanining=false);
	int __fastcall GetTraceIndex(TTrace* ptr);
	void __fastcall ReAltitude(void);
	void __fastcall ReAltitude(TTrace* FromPtr, TTrace* ToPtr, bool Upstairs); // Find and store the highet point in MaxAltitude

	int SourceType;
	int SamplesBeforeChanging;
	float HorRangeFrom;
	float HorRangeTo;
	float HorRangeFull;
	//    float HorRange;  // in m

	bool NeedToScaleZero;
	TColor Colors[256]; // color palette
 //---Parameters for drawing---
	int SampleFrom;
	int SampleTo;
	int TraceFrom;
	int TraceTo;
	int SampleFromOld;
	int SampleToOld;
	int TraceFromOld;
	int TraceToOld;
	int Stacking;
	bool SamplesAutoFit;
	bool TracesAutoFit;
	bool ShowAnnotations;
	bool ShowOnlyAnnotations;
	bool ShowMarks;
	char XUnits; // 'T'races or 'D'istance
	char YUnits; // 'S'amples, 'T'ime, 'D'epth or 'A'ltitude
	bool IndividualRef;
	AnsiString TextInfo;
	float Contrast;
	bool FileSaved;
	bool FileModified;
	bool FileProcessed;
	bool AnnotationsModified;
	bool Coordinated;
	int NumberOfChannels;
	float WheelStep; // Wheel step (in m) per one wheel impulse
	float YPosition;
	TList *Vertexes;
	float *GainFunction;
	float FilterPoints[4];
	float *Filter;
	TFilterType FILTER_TYPE;
	float MaxAltitude, MinAltitude;
	TObject *ObjectsContainer;
//--Only for GSSI datas-----------------------------------------
	short int rh_zero;
	float rh_sps;
	float rh_spm;
	float rh_mpm;
	short int rh_spp;
	int rh_create;
	int rh_modif;
	short int rh_nchan;
	float rh_top;
	float rh_depth;
	short int rh_npass;
	char reserved[60];
	char variable[384];
	int __fastcall DateToGSSI(TDateTime Dt);
	TDateTime __fastcall GSSIToDate(int rh);
//--End of GSSI datas-------------------------------------------
	int PositiveMax, NegativeMax;
	TList *Figures; // Annotations, etc.

	bool __fastcall CheckLock() {try { return (ProfileLockEvent!=NULL && WaitForSingleObject(ProfileLockEvent, 0)==WAIT_OBJECT_0);} catch(...) {return false;}}
	bool __fastcall Lock() {bool b=!CheckLock(); if(b && ProfileLockEvent) SetEvent(ProfileLockEvent); else b=false; return b;}
	void __fastcall UnLock() {if(ProfileLockEvent) ResetEvent(ProfileLockEvent);}

	void __fastcall RefreshAccessTime() {lastaccess=::GetTickCount();}
	unsigned long __fastcall AccessTimeOut() {return (::GetTickCount()-lastaccess);}
	void __fastcall ReleaseFilter() {TTrace* ptr=FirstTracePtr; while(ptr) {ptr->ReleaseFilterData(); ptr=ptr->PtrUp;}}
	void __fastcall ClearZeroPoint() {zeropoint=0; zeropointadjusted=false;}
	void __fastcall StopProfiling() {TTrace* ptr=currenttraceptr; while(ptr) {ptr->Backward=false; ptr=ptr->PtrUp;} currenttraceptr=NULL;}

	void __fastcall CacheIt();
	void __fastcall Restore();

	float SamplesToTime(float sm) {float tm; if(samples>0) tm=timerange*(sm/(float)samples); else tm=0; return tm;}
	float TimeToSamples(float tm) {float sm; if(timerange>0) sm=(float)samples*(tm/timerange); else sm=0; return sm;}
    float TimeToDepth(float tm) {float d; if(timerange>0) d=depth*(tm/timerange); else d=0; return d;}
    float DepthToTime(float d) {float tm; if(depth>0) tm=timerange*(d/depth); else tm=0; return tm;}
    float SamplesToDepth(float sm) {float d; if(samples>0) d=depth*(sm/(float)samples); else d=0; return d;}
	float DepthToSamples(float d) {float sm; if(depth>0) sm=(float)samples*(d/depth); else sm=0; return sm;}

	__property TTrace *FirstTracePtr = {read=readFirstTracePtr, write=writeFirstTracePtr};  // Pointer to first trace int array
	__property TTrace *LastTracePtr = {read=readLastTracePtr, write=writeLastTracePtr};   // Pointer to last trace int array
	__property TTrace *CurrentTracePtr = {read=readCurrentTracePtr, write=writeCurrentTracePtr};
	__property TTraceAccumulator *CommonTrace = {read=commontrace};
	__property int Samples = {read=samples, write=writeSamples};
	__property float TimeRange = {read=timerange, write=writeTimeRange}; // in ns
	__property float Depth = {read=depth};
	__property float Permit = {read=permit, write=writePermit};    // average permittivity
	__property long Traces = {read=traces};
	__property HANDLE TracesAddedEvent = {read=tracesaddedevent};
	__property AnsiString FullName = {read=fullname, write=writeFullName};
	__property AnsiString Name = {read=name, write=name};
	__property bool CachingIsProhibited = {read=cachingisprohibited, write=cachingisprohibited};
	__property bool ProfileDataCached = {read=readTraceDataCached};
	__property TProfileOutputView *OutputView = {read=outputview, write=writeOutputView};
	__property bool OutputViewOwner = {read=outputviewowner, write=outputviewowner};
	__property unsigned long LastAccess = {read=lastaccess};
	__property TSampleFormat SampleFormat = {read=sampleformat, write=sampleformat};
	__property float ZeroPoint = {read=readZeroPoint, write=writeZeroPoint}; // offset to transmitter pulse (in samples)
	__property bool ZeroPointAdjusted = {read=zeropointadjusted};
	__property unsigned long SizeInMemory = {read=readSizeInMemory};

#ifdef ALLOCATE_SAMPLES_HEAP
	__property HANDLE hHeap = {read=hheap};
#endif
};

class TProfilesGroup : public TObject
{
private:
	int Count;
	TProfile** Items;
public:
	__fastcall TProfilesGroup(int N);
	__fastcall ~TProfilesGroup();
	void __fastcall AssignProfile(TProfile* p, int Index);
	TProfile* __fastcall GetProfile(int Index);
	TTrace* __fastcall GetLastTracePtr(int Index);
	TTrace* __fastcall GetFirstTracePtr(int Index);
	TTrace* __fastcall GetCurrentTracePtr(int Index);
	void __fastcall AddNewTrace(TTrace *ptr, int Index);
};

#endif
