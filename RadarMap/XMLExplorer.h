//---------------------------------------------------------------------------

#ifndef XMLExplorerH
#define XMLExplorerH
//---------------------------------------------------------------------------
#include "LibXmlComps.hpp"

class TXMLExplorer : public TObject
{
private:
	TXmlScanner *XmlScanner;
	AnsiString __fastcall readCurrentTag() {return XmlScanner->XmlParser->CurName;}
public:
	__fastcall TXMLExplorer(TComponent* AOwner, AnsiString FileName);
	__fastcall ~TXMLExplorer() {delete XmlScanner;}

	void __fastcall Restart();

	bool __fastcall FindTagStart(AnsiString TagName);
	bool __fastcall FindTagEnd(AnsiString TagName);
	bool __fastcall GetAttribute(AnsiString AttributeName, AnsiString &Str);
	bool __fastcall GetContent(UnicodeString &Str, bool NotNormalized=false);
	bool __fastcall GetContent(AnsiString &Str, bool NotNormalized=false) {UnicodeString ustr; bool b=GetContent(ustr, NotNormalized); Str=(AnsiString)ustr; return b;}
	bool __fastcall FindTagAndGetContent(AnsiString TagName, AnsiString &Str);
	bool __fastcall GetNextStartTag(AnsiString &Str);
	bool __fastcall GetNextStartTagInside(AnsiString Parent, AnsiString &Str);
	bool __fastcall GetNextEndTag(AnsiString &Str);

	__property AnsiString CurrentTag = {read = readCurrentTag};
};

#endif
