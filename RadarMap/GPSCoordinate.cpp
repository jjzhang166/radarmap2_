//---------------------------------------------------------------------------
#include <math.h>

#pragma hdrstop

//#ifdef RAD101
	#include "MyCompiler.h"
//#endif
#include "Defines.h"
#include "GPSCoordinate.h"
#include "WGS2UTM.h"
#include "DutchRD.h"
#include "rdnaptrans2008.h"
#include "Lambert.h"
//#include "LKS.h"
#include "PlugIns.h"
#include "Metric.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

//---------------------------------------------------------------------------
// Accompaning functions
//---------------------------------------------------------------------------

long double DegMinToDeg(long double lt)
{
	int n;
	long double t2;
	TFormatSettings fs;

	fs.CurrencyString="";
	n=(int)(lt/100.);
	t2=(long double)n+(lt-100.0*(long double)n)/60.;
	return t2;
}
//---------------------------------------------------------------------------

long double DegToDegMin(long double lt)
{
	int n;
	long double t2;

	n=100*(int)lt;
	t2=(long double)n+60.*(lt-(int)lt);
	return t2;
}
//---------------------------------------------------------------------------

float __fastcall CalculScaleX(TCoordinateSystem CS, int PixWidth,
	int PixHeight, TDoubleRect CoordinatesRect)
{
	double res, K;

	if(CS==csLatLon)
	{
		K=(double)PixWidth*fabs(CoordinatesRect.Height())/((double)PixHeight*fabs(CoordinatesRect.Width()));
		res=cos(M_PI*((CoordinatesRect.Top+CoordinatesRect.Bottom)/2.)/180.)/K;
	}
	else
	{
		K=(double)PixHeight*CoordinatesRect.Width()/CoordinatesRect.Height();
		res=(double)PixWidth/K;
	}
	return (float)fabs(res);
}

//---------------------------------------------------------------------------
// TNationalCoordinate
//---------------------------------------------------------------------------
__fastcall TNationalCoordinate::TNationalCoordinate(TCoordinateSystem AType, AnsiString APrefix,
	AnsiString AName, AnsiString AEllipsoid, AnsiString ACodeName, AnsiString ACrsURI,
	TNationalCoordinatesManager *AOwner, TCoordinateSystemDimension ADimension)
{
	Owner=AOwner;
	type=AType;
	prefix=APrefix;
	name=AName;
	codename=ACodeName;
	ellipsoid=AEllipsoid;
	crs_uri=ACrsURI;
	proj4="";
	x=y=h=0.;
	dimension=ADimension;
#ifdef UseEventAsFlag
	recalculneededevent=CreateEvent(NULL, true, true, NULL);
#else
	recalculneededevent=true;
#endif;
	CheckForChanges();
}
//---------------------------------------------------------------------------

__fastcall TNationalCoordinate::~TNationalCoordinate()
{
#ifdef UseEventAsFlag
	CloseHandle(recalculneededevent);
#endif;
}
//---------------------------------------------------------------------------

bool __fastcall TNationalCoordinate::readRecalculNeeded()
{
#ifdef UseEventAsFlag
	if(WaitForSingleObject(recalculneededevent, 0)==WAIT_OBJECT_0) return true;
	else return false;
#else
	return recalculneededevent;
#endif;
}
//---------------------------------------------------------------------------

void __fastcall TNationalCoordinate::writeRecalculNeeded(bool value)
{
#ifdef UseEventAsFlag
	if(value) SetEvent(recalculneededevent);
	else ResetEvent(recalculneededevent);
#else
	recalculneededevent=value;
#endif;
}
//---------------------------------------------------------------------------

void __fastcall TNationalCoordinate::CheckForChanges()
{
	double lat, lon, alt;

	if(Owner && Owner->GetLatLonAlt(this, lat, lon, alt))
	{
	   LatLonAltToXYH(lat, lon, alt);
	   recalculneeded=false;
	}
}
//---------------------------------------------------------------------------

void __fastcall TNationalCoordinate::writeX(double value)
{
	if(x!=value)
	{
		x=value;
		if(Owner) Owner->SetLastChanged(this);
	}
}
//---------------------------------------------------------------------------

void __fastcall TNationalCoordinate::writeY(double value)
{
	if(y!=value)
	{
		y=value;
		if(Owner) Owner->SetLastChanged(this);
	}
}
//---------------------------------------------------------------------------

void __fastcall TNationalCoordinate::writeXY(TDoublePoint value)
{
	if(xy.X!=value.X || xy.Y!=value.Y)
	{
		xy=value;
		num=value.Temp[0];
		ch=value.Temp[1];
		if(Owner) Owner->SetLastChanged(this);
	}
}
//---------------------------------------------------------------------------

void __fastcall TNationalCoordinate::writeXYH(TDouble3DPoint value)
{
	if(x!=value.X || y!=value.Y || h!=value.Z)
	{
		x=value.X;
		y=value.Y;
		h=value.Z;
		num=value.Temp[0];
        ch=value.Temp[1];
		if(Owner) Owner->SetLastChanged(this);
	}
}
//---------------------------------------------------------------------------

void __fastcall TNationalCoordinate::writeH(double value)
{
	if(h!=value)
	{
		h=value;
		if(Owner) Owner->SetLastChanged(this);
	}
}
//---------------------------------------------------------------------------

void __fastcall TNationalCoordinate::writeNum(char value)
{
	if(num!=value)
	{
		num=value;
		if(Owner) Owner->SetLastChanged(this);
	}
}
//---------------------------------------------------------------------------

void __fastcall TNationalCoordinate::writeCh(char value)
{
	if(ch!=value)
	{
		ch=value;
		if(Owner) Owner->SetLastChanged(this);
	}
}
//---------------------------------------------------------------------------

void __fastcall TNationalCoordinate::writeTemp(char* value)
{
	if(value)
	{
		*((int*)temp)=*((int*)value);
		if(Owner) Owner->SetLastChanged(this);
	}
}
//---------------------------------------------------------------------------

void __fastcall TNationalCoordinate::SetXYH(double Ax, double Ay, double Ah)
{
	if(x!=Ax || y!=Ay || h!=Ah)
	{
		x=Ax;
		y=Ay;
		h=Ah;
		if(Owner) Owner->SetLastChanged(this);
	}
}
//---------------------------------------------------------------------------

void __fastcall TNationalCoordinate::SetXY(double Ax, double Ay)
{
	if(x!=Ax || y!=Ay)
	{
		x=Ax;
		y=Ay;
		if(Owner) Owner->SetLastChanged(this);
	}
}
//---------------------------------------------------------------------------

AnsiString __fastcall TNationalCoordinate::AsString(bool Zoutput)
{
	AnsiString str=Prefix+" ";
	double lat, lon, f;

	CheckForChanges();

	switch(dimension)
	{
		case csdDegrees:
			lat=fabs(y);
			str+="Lat="+IntToStr((int)lat)+"�";
			f=(lat-(int)lat)*60.;
			str+=IntToStr((int)f)+"'";
			f=(f-(int)f)*60.;
			str+=FloatToStrF(f, ffFixed, 6, 4)+"''";
			if(y<0) str+="S";
			else str+="N";
			str+=" Lon=";
			lon=fabs(x);
			str+=IntToStr((int)lon)+"�";
			f=(lon-(int)lon)*60.;
			str+=IntToStr((int)f)+"'";
			f=(f-(int)f)*60.;
			str+=FloatToStrF(f, ffFixed, 6, 4)+"''";
			if(x<0) str+="W";
			else str+="E";
			break;
		case csdMeters:
		case csdFeets:
			str+=" X="+FloatToStrF(x, ffFixed, 10, 2)+" Y="+FloatToStrF(y, ffFixed, 10, 2);
			if(dimension==csdMeters) str+=" [m]";
			else str+=" [f]";
			break;
	}
	if(Zoutput) str+=" H="+FloatToStrF(h, ffFixed, 10, 2);
	if(dimension!=csdDegrees)
		str+=" ["+(AnsiString)CoordinateSystemDimensionStringsShort[dimension]+"]";
	return str;
}
//---------------------------------------------------------------------------

TDoublePoint __fastcall TNationalCoordinate::DistanceVector(double lat, double lon, double alt)
{
	double x0, y0, h0, dx, dy;
	TDoublePoint res;

	if(dimension==csdMeters || dimension==csdFeets)
	{
		CheckForChanges();
		res=UnitVector(lat, lon, alt);
		//res.X=1.;
		x0=x; y0=y; h0=h;
		LatLonAltToXYH(lat, lon, alt);
		dx=(x-x0)*res.X;
		dy=(y-y0)*res.Y;
		if(dimension==csdMeters) res=DoublePoint(dx, dy);
		else res=DoublePoint(dx*0.3048, dy*0.3048);
		x=x0; y=y0; h=h0;
	}
	return res;
}

//---------------------------------------------------------------------------
// TLatLonCoordinate
//---------------------------------------------------------------------------
/*void __fastcall TLatLonCoordinate::writeX(double value)
{
	if(longitude!=value)
	{
		longitude=value;
		if(Owner) Owner->SetLastChanged(this);
	}
}
//---------------------------------------------------------------------------

void __fastcall TLatLonCoordinate::writeY(double value)
{
	if(latitude!=value)
	{
		latitude=value;
		if(Owner) Owner->SetLastChanged(this);
	}
}
//---------------------------------------------------------------------------

void __fastcall TLatLonCoordinate::writeH(double value)
{
	if(altitude!=value)
	{
		altitude=value;
		if(Owner) Owner->SetLastChanged(this);
	}
}*/
//---------------------------------------------------------------------------

AnsiString __fastcall TLatLonCoordinate::AsString(bool Zoutput)
{
	AnsiString str=Prefix+" ";
	double lat, lon, f;

	lat=fabs(Latitude);
	str+="Lat="+IntToStr((int)lat)+"�";
	f=(lat-(int)lat)*60.;
	str+=IntToStr((int)f)+"'";
	f=(f-(int)f)*60.;
	str+=FloatToStrF(f, ffFixed, 6, 4)+"''";
	if(Latitude<0) str+="S";
	else str+="N";
	str+=" Lon=";
	lon=fabs(Longitude);
	str+=IntToStr((int)lon)+"�";
	f=(lon-(int)lon)*60.;
	str+=IntToStr((int)f)+"'";
	f=(f-(int)f)*60.;
	str+=FloatToStrF(f, ffFixed, 6, 4)+"''";
	if(Longitude<0) str+="W";
	else str+="E";
	if(Zoutput) str+=" H="+FloatToStrF(Altitude, ffFixed, 10, 2);
	return str;
}
//---------------------------------------------------------------------------

void __fastcall TLatLonCoordinate::SetLatLonAlt(double lat, double lon, double alt, bool UpdateNeighbors)
{
	y=lat;
	x=lon;
	h=alt;
	if(UpdateNeighbors && Owner) Owner->SetLastChanged(this);
}
//---------------------------------------------------------------------------

TDoublePoint __fastcall TLatLonCoordinate::DistanceVector(double lat, double lon, double alt)
{
	long double R = 6371032.; // m
	long double dLat, dLon, lat1, lat2, a, c;
	TDoublePoint dp;

	dLon=MyDegToRad(lon-x);
	lat1=MyDegToRad(lat);
	a=sinl(dLon/2)*sinl(dLon/2)*cosl(lat1)*cosl(lat1);
	if(a>0. && a<1.) dp.X=(double)(R*2.*atan2l(sqrtl(a), sqrtl(1.-a)));
	else dp.X=0.;
	dLat=MyDegToRad((long double)(lat-y));
	a=sinl(dLat/2)*sinl(dLat/2);
	if(a>0. && a<1.) dp.Y=(double)(R*2.*atan2l(sqrtl(a), sqrtl(1.-a)));
	else dp.Y=0.;

	return dp;
}

//---------------------------------------------------------------------------
// TDutchCoordinate
//---------------------------------------------------------------------------
void __fastcall TDutchCoordinate::XYHToLatLonAlt(double &lat, double &lon, double &alt)
{
#ifndef DutchRDNAPTRANS2008
	DutchRD::RDtoLL(x, y, lat, lon);
	alt=h;
#else
	RdNapTrans2008::rdnap2etrs(x, y, h, lat, lon, alt);
#endif
}
//---------------------------------------------------------------------------

void __fastcall TDutchCoordinate::LatLonAltToXYH(double lat, double lon, double alt)
{
#ifndef DutchRDNAPTRANS2008
	DutchRD::LLtoRD(lat, lon, x, y);
	h=alt;
#else
	RdNapTrans2008::etrs2rdnap(lat, lon, alt, x, y, h);
#endif
}

//---------------------------------------------------------------------------
// TBeLambert72Coordinate
//---------------------------------------------------------------------------
void __fastcall TBeLambert72Coordinate::XYHToLatLonAlt(double &lat, double &lon, double &alt)
{
#ifdef UseStaticDLL
	Lambert72::Lambert72ToGeoETRS89(x, y, h, &lat, &lon, &alt);
#else
	Lambert72::Lambert72ToGeoETRS89(x, y, h, lat, lon, alt);
#endif
}
//---------------------------------------------------------------------------

void __fastcall TBeLambert72Coordinate::LatLonAltToXYH(double lat, double lon, double alt)
{
#ifdef UseStaticDLL
	Lambert72::GeoETRS89ToLambert72(lat, lon, alt, &x, &y, &h);
#else
	Lambert72::GeoETRS89ToLambert72(lat, lon, alt, x, y, h);
#endif
}

//---------------------------------------------------------------------------
// TUTMCoordinate
//---------------------------------------------------------------------------
void __fastcall TUTMCoordinate::writeNum(char value)
{
	while(value>60) value-=60;
	while(value<1) value+=60;
	TNationalCoordinate::writeNum(value);
}
//---------------------------------------------------------------------------

void __fastcall TUTMCoordinate::writeCh(char value)
{
	if(value>='a' && value<='z') value-=32;
	if(value<'A') value='A';
	else if(value>'Z') value='Z';
	else
	{
		switch(value)
		{
			case 'I': value='J'; break;
			case 'O': value='P'; break;
		}
	}
	TNationalCoordinate::writeCh(value);
}
//---------------------------------------------------------------------------

void __fastcall TUTMCoordinate::XYHToLatLonAlt(double &lat, double &lon, double &alt)
{
	while(x<0)//500000)
	{
		num--;
		x+=500000;
	}
	while(y<0) y+=10000000;
	nsUTM::UTMtoLL(23, y, x, num, ch, lat, lon);
	alt=h;
}
//---------------------------------------------------------------------------

void __fastcall TUTMCoordinate::LatLonAltToXYH(double lat, double lon, double alt)
{
	nsUTM::LLtoUTM(23, lat, lon, y, x, num, ch);
	ch=nsUTM::UTMLetterDesignator(lat);
	h=alt;
}
//---------------------------------------------------------------------------

TDoublePoint __fastcall TUTMCoordinate::DistanceVector(double lat, double lon, double alt)
{
	double x0, y0, x1, y1, h0, dx, dy, w;
	char ch0, num0, num1;
	TDoublePoint res;

	CheckForChanges();
	x0=x1=x; y0=y1=y; h0=h; ch0=ch; num1=num0=num;
	LatLonAltToXYH(lat, lon, alt);
	if(ch<'N' && ch0>='N') y-=10000000;
	else if(ch0<'N' && ch>='N') y1+=10000000;
	if(num0!=num)
	{
		w=40075000./60.;
		x1-=500000;
		x-=500000;
		num1=num0-num;
		while(num1>60) num1-=59;
		while(num1<=-60) num1+=59;
		x1+=w*(double)num1*cos(lat);
    }
	dx=x-x1;
	dy=y-y1;
	if(dimension==csdMeters) res=DoublePoint(dx, dy);
	else res=DoublePoint(dx*0.3048, dy*0.3048);
	x=x0; y=y0; h=h0; ch=ch0; num=num0;

	return res;
}

/*
//---------------------------------------------------------------------------
// TLKSCoordinate
//---------------------------------------------------------------------------
void __fastcall TLKSCoordinate::XYHToLatLonAlt(double &lat, double &lon, double &alt)
{
	LKS92::LKSToLatLon(x, y, lat, lon);
	alt=h;
}
//---------------------------------------------------------------------------

void __fastcall TLKSCoordinate::LatLonAltToXYH(double lat, double lon, double alt)
{
	LKS92::LatLonToLKS(lat, lon, x, y);
	h=alt;
}
*/

//---------------------------------------------------------------------------
// TMetricCoordinate
//---------------------------------------------------------------------------
__fastcall TMetricCoordinate::TMetricCoordinate(TNationalCoordinatesManager *AOwner): TNationalCoordinate(csMetric, "Metric",
		"World Mercator", "WGS84", "csMetric", "EPSG:3395", AOwner, csdMeters)
{
	guid=_csMetricGUID;
	proj4="+proj=merc +lon_0=0 +k=1 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs";
	Metric_WGS84::InitPlugIn(0);
}
//---------------------------------------------------------------------------

__fastcall TMetricCoordinate::~TMetricCoordinate()
{
	Metric_WGS84::FreePlugIn(0);
}
//---------------------------------------------------------------------------

void __fastcall TMetricCoordinate::XYHToLatLonAlt(double &lat, double &lon, double &alt)
{
	double _lat, _lon, _alt;

	if(Metric_WGS84::_CRS_XYHToLatLonAlt(x, y, h, _lat, _lon, _alt))
	{
		lat=_lat;
		lon=_lon;
		alt=_alt;
	}
	else
	{
		Metric_WGS84::InitPlugIn(0);
		if(Metric_WGS84::_CRS_XYHToLatLonAlt(x, y, h, _lat, _lon, _alt))
		{
			lat=_lat;
			lon=_lon;
			alt=_alt;
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TMetricCoordinate::LatLonAltToXYH(double lat, double lon, double alt)
{
	double _x,_y, _h;

	if(Metric_WGS84::_CRS_LatLonAltToXYH(lat, lon, alt, _x, _y, _h))
	{
		x=_x;
		y=_y;
		h=_h;
	}
	else
	{
		Metric_WGS84::InitPlugIn(0);
		if(Metric_WGS84::_CRS_LatLonAltToXYH(lat, lon, alt, _x, _y, _h))
		{
			x=_x;
			y=_y;
			h=_h;
		}
	}
}/**/
//---------------------------------------------------------------------------

TDoublePoint __fastcall TMetricCoordinate::UnitVector(double lat, double lon, double alt)
{
	TDoublePoint dp=DoublePoint();

	CheckForChanges();
	if(!Metric_WGS84::_CRS_UnitVector(lat, lon, alt, dp.X, dp.Y))
	{
		Metric_WGS84::InitPlugIn(0);
		if(!Metric_WGS84::_CRS_UnitVector(lat, lon, alt, dp.X, dp.Y)) dp=DoublePoint();
    }
	return dp;
}

//---------------------------------------------------------------------------
// TDLLCoordinate
//---------------------------------------------------------------------------
__fastcall TDLLCoordinate::TDLLCoordinate(TNationalCoordinatesManager *AOwner) :
	TNationalCoordinate(csDLL, "DLL", "User Defined", "User Defined", "csDLL", "", AOwner, csdMeters)
{
	guid="";
	CheckForChanges();
}
//---------------------------------------------------------------------------

void __fastcall TDLLCoordinate::LatLonAltToXYH(double lat, double lon, double alt)
{
	if(PlugInManager && PlugInManager->ActiveCS)
	{
		PlugInManager->ActiveCS->LatLonAltToXYH(lat, lon, alt, x, y, h);
	}
}
//---------------------------------------------------------------------------

void __fastcall TDLLCoordinate::XYHToLatLonAlt(double &lat, double &lon, double &alt)
{
	if(PlugInManager && PlugInManager->ActiveCS)
	{
		PlugInManager->ActiveCS->XYHToLatLonAlt(x, y, h, lat, lon, alt);
	}
}
//---------------------------------------------------------------------------

TDoublePoint __fastcall TDLLCoordinate::UnitVector(double lat, double lon, double alt)
{
	TDoublePoint res=DoublePoint();

	if(PlugInManager && PlugInManager->ActiveCS)
	{
		PlugInManager->ActiveCS->UnitVector(lat, lon, alt, res);
	}
	else res=DoublePoint();
	return res;
}
//---------------------------------------------------------------------------

void __fastcall TDLLCoordinate::CheckForDLLChanges()
{
	if(PlugInManager && PlugInManager->ActiveCS)
	{
		if(guid!=PlugInManager->ActiveCS->GUID)
		{
			guid=PlugInManager->ActiveCS->GUID;
			prefix=PlugInManager->ActiveCS->Prefix;
			name=PlugInManager->ActiveCS->Name;
			//codename="dll_"+PlugInManager->ActiveCS->Code;
			//codename=PlugInManager->ActiveCS->Code;
			ellipsoid=PlugInManager->ActiveCS->EllipsoidName;
			crs_uri=PlugInManager->ActiveCS->EPSG;
			proj4=PlugInManager->ActiveCS->PROJ4;
			dimension=PlugInManager->ActiveCS->Dimension;
		}
	}
	else
	{
		guid="";
		prefix="DLL";
		name="User Defined";
		codename="csDLL";
		crs_uri="";
		proj4="";
		//dimension=csdMeters;
	}
}
//---------------------------------------------------------------------------

AnsiString __fastcall TDLLCoordinate::AsString(bool Zoutput)
{
	AnsiString str;

	CheckForChanges();
	CheckForDLLChanges();
	str=TNationalCoordinate::AsString(Zoutput);
	/*str=prefix+" X="+FloatToStrF(x, ffFixed, 10, 2)+" Y="+FloatToStrF(y, ffFixed, 10, 2);
	if(Zoutput) str+=" H="+FloatToStrF(h, ffFixed, 10, 2);*/
	return str;
}

//---------------------------------------------------------------------------
// TNationalCoordinatesManager
//---------------------------------------------------------------------------
__fastcall TNationalCoordinatesManager::TNationalCoordinatesManager()
{
	List=new TList();
	ncLLA=NULL;
	Add(csLatLon);
	LastChanged=csLatLon;
}
//---------------------------------------------------------------------------

__fastcall TNationalCoordinatesManager::~TNationalCoordinatesManager()
{
	Clear();
	delete List;
}
//---------------------------------------------------------------------------

void __fastcall TNationalCoordinatesManager::Clear()
{
	ncLLA=NULL;
	for(int i=0; i<Count; i++)
	{
		if(Items[i]) delete (TNationalCoordinate*)Items[i];
		List->Items[i]=NULL;
	}
	List->Clear();
}
//---------------------------------------------------------------------------

void __fastcall TNationalCoordinatesManager::Delete(int Index)
{
	if(Index>=0 && Index<List->Count)
	{
		if(Items[Index]->Type==csLatLon) ncLLA=NULL;
		delete (TNationalCoordinate*)Items[Index];
		List->Items[Index]=NULL;
		List->Delete(Index);
	}
}
//---------------------------------------------------------------------------

void __fastcall TNationalCoordinatesManager::Delete(TCoordinateSystem CS)
{
	int i=0;

	while(i<Count)
	{
		if(Items[i]->Type==CS) Delete(i);
		else i++;
	}
}
//---------------------------------------------------------------------------

int __fastcall TNationalCoordinatesManager::Add(TCoordinateSystem CS)
{
	TNationalCoordinate *nc;
	int res=-1;

	Delete(CS);
	switch (CS)
	{
		case csLatLon: nc=new TLatLonCoordinate(this); break;
		case csDutch: nc=new TDutchCoordinate(this); break;
		case csBeLambert08:
		case csBeLambert72: nc=new TBeLambert72Coordinate(this); break;
		case csUTM: nc=new TUTMCoordinate(this); break;
		//case csLKS: nc=new TLKSCoordinate(this); break;
		case csMetric: nc=new TMetricCoordinate(this); break;
		case csDLL: nc=new TDLLCoordinate(this); break;
		default: nc=NULL;
	}
	if(nc)
	{
		res=List->Add(nc);
		if(CS==csLatLon) ncLLA=(TLatLonCoordinate *)nc;
	}

	return res;
}
//---------------------------------------------------------------------------

TNationalCoordinate* __fastcall TNationalCoordinatesManager::Get(TCoordinateSystem CS, bool AutoCreate)
{
	int i=0;
	TNationalCoordinate *nc=NULL;

	try
	{
		while(i<Count)
		{
			if(Items[i]->Type==CS)
			{
				nc=Items[i];
				break;
			}
			else i++;
		}
		if(!nc && AutoCreate)
		{
			i=Add(CS);
			//nc=Get(CS, false);
			if(i>=0) nc=Items[i];
		}
	}
	catch(Exception &e)
	{
		nc=NULL;
	}

	return nc;
}
//---------------------------------------------------------------------------

bool __fastcall TNationalCoordinatesManager::GetLatLonAlt(TNationalCoordinate* NC,
	double &lat, double &lon, double &alt)
{
	bool res=true;
	TNationalCoordinate *nc;

	if(NC && NC->RecalculNeeded)
	{
		res=true;
		if((ncLLA && ncLLA->RecalculNeeded) || !ncLLA)
		{
			nc=Get(LastChanged, false);
			if(nc)
			{
				nc->XYHToLatLonAlt(lat, lon, alt);
				if(ncLLA)
				{
					ncLLA->SetLatLonAlt(lat, lon, alt, false);
					ncLLA->RecalculNeeded=false;
				}
			}
			else res=false; // ???????????
		}
		else if(ncLLA) ncLLA->XYHToLatLonAlt(lat, lon, alt);
		else res=false;
	}
	else res=false;

	return res;
}
//---------------------------------------------------------------------------

void __fastcall TNationalCoordinatesManager::SetLastChanged(TNationalCoordinate* NC)
{
	int i;
	for(i=0; i<Count; i++)
		if(Items[i]) Items[i]->RecalculNeeded=true;
	if(NC)
	{
		LastChanged=NC->Type;
		NC->RecalculNeeded=false;
	}
}
//---------------------------------------------------------------------------

void __fastcall TNationalCoordinatesManager::Copy(TNationalCoordinatesManager* NCC)
{
	TNationalCoordinate *nc;

	if(NCC)
	{
		Clear();
		for(int i=0; i<NCC->Count; i++)
		{
			Add(NCC->Items[i]->Type);
			nc=Get(NCC->Items[i]->Type);
			if(nc) nc->Copy(NCC->Items[i]);
		}
	}
}

//---------------------------------------------------------------------------
// TGPSCoordinate
//---------------------------------------------------------------------------
void __fastcall TGPSCoordinate::Initializing(void* p, double lt, double ln, double z, double geoid, double conf)
{
	//if(NationalCoordinates) delete NationalCoordinates;
	NationalCoordinates=new TNationalCoordinatesManager();
	//NationalCoordinates->Add(csDutch);
	//NationalCoordinates->Add(csBeLambert72);
	NationalCoordinates->SetLatLonAlt(lt, ln, geoid);
	parent=p;
	Z=z;
	confidence=conf;
	connectedptr=NULL;
}
//---------------------------------------------------------------------------

__fastcall TGPSCoordinate::TGPSCoordinate(void)
{
	NationalCoordinates=NULL;
	Initializing(NULL, 0, 0, 0, 0, 0);
	trace=NULL;
	LockEvent=CreateEvent(NULL, true, false, NULL);
}
//---------------------------------------------------------------------------

__fastcall TGPSCoordinate::TGPSCoordinate(void* p, double lt, double ln, double z, double geoid, double conf)
{
	NationalCoordinates=NULL;
	Initializing(p, lt, ln, z, geoid, conf);
	trace=NULL;
	LockEvent=CreateEvent(NULL, true, false, NULL);
}
//---------------------------------------------------------------------------

__fastcall TGPSCoordinate::TGPSCoordinate(void* p, double lt, double ln, double z, double geoid, TTrace* Ptr, double conf)
{
	NationalCoordinates=NULL;
	Initializing(p, lt, ln, z, geoid, conf);
	trace=Ptr;
	LockEvent=CreateEvent(NULL, true, false, NULL);
}
//---------------------------------------------------------------------------

__fastcall TGPSCoordinate::TGPSCoordinate(void* p, TTrace* Ptr)
{
	NationalCoordinates=NULL;
	Initializing(p, 0, 0, 0, 0, 0);
	Trace=Ptr;
	LockEvent=CreateEvent(NULL, true, false, NULL);
}
//---------------------------------------------------------------------------

__fastcall TGPSCoordinate::TGPSCoordinate(TGPSCoordinate* Src)
{
	NationalCoordinates=NULL;
	if(Src)
	{
		Initializing(Src->Parent, Src->Latitude, Src->Longitude, Src->Z, Src->Z_Geoid, Src->Confidence);
		trace=Src->Trace;
		greenwichtm=Src->GreenwichTm;
		xxx=Src->x;
		yyy=Src->y;
		NationalCoordinates->Copy(Src->NCC);
	}
	else
	{
        Initializing(NULL, 0, 0, 0, 0, 0);
		trace=NULL;
	}
	LockEvent=CreateEvent(NULL, true, false, NULL);
}
//---------------------------------------------------------------------------

__fastcall TGPSCoordinate::TGPSCoordinate(bool a)
{
	NationalCoordinates=NULL;
	Initializing(NULL, 0, 0, 0, 0, 0);
	trace=NULL;
	LockEvent=CreateEvent(NULL, true, false, NULL);
}
//---------------------------------------------------------------------------

__fastcall TGPSCoordinate::~TGPSCoordinate()
{
	UnLock();
	try
	{
		if(NationalCoordinates) delete NationalCoordinates;
	}
	catch(...) {}
	NationalCoordinates=NULL;
	if(LockEvent) CloseHandle(LockEvent);
    LockEvent=NULL;
}
//---------------------------------------------------------------------------

bool __fastcall TGPSCoordinate::CheckLock()
{
	try
	{
		return (WaitForSingleObject(LockEvent, 0)==WAIT_OBJECT_0);
	}
	catch(...)
	{
		return false;
	}
}
//---------------------------------------------------------------------------

void __fastcall TGPSCoordinate::Lock()
{
	bool b;
	if(locking)
	{
		while(CheckLock() && locking) ;
		SetEvent(LockEvent);
	}
}
//---------------------------------------------------------------------------

void __fastcall TGPSCoordinate::UnLock()
{
	if(locking) ResetEvent(LockEvent);
}
//---------------------------------------------------------------------------

void __fastcall TGPSCoordinate::writeTrace(TTrace* value)
{
	trace=value;

	if(trace!=NULL && (trace->Latitude!=0 || trace->Longitude!=0 || trace->Z!=0))
	{
		Lock();

		z=trace->Z;
		Confidence=trace->GPSConfidence;
		NationalCoordinates->SetLatLonAlt(trace->Latitude, trace->Longitude, trace->Z_Geoid);

		UnLock();
	}
}
//---------------------------------------------------------------------------

int __fastcall TGPSCoordinate::DistanceToPointInPix(int xx, int yy)
{
	int f=(xx-x)*(xx-x)+(yy-y)*(yy-y);
	if(f>=0) return (int)sqrt((float)f);
	else return 999999;
}
//---------------------------------------------------------------------------

double __fastcall TGPSCoordinate::DistanceToPointInDeg(double lt, double ln)
{
	double f;

	Lock();

	f=(lt-Latitude)*(lt-Latitude)+(ln-Longitude)*(ln-Longitude);

	UnLock();

	if(f>0) return sqrt(f);
	else return 0;
}
//---------------------------------------------------------------------------

double __fastcall TGPSCoordinate::DistanceToPoint(TDoublePoint dp, TCoordinateSystem cs)
{
	TDoublePoint xy;
	double res=-1;

	if(Valid)
	{
		xy=GetXY(cs);
		res=sqrt((xy.X-dp.X)*(xy.X-dp.X)+(xy.Y-dp.Y)*(xy.Y-dp.Y));
	}
	return res;
}

//---------------------------------------------------------------------------
// TGPSCoordinatesRect
//---------------------------------------------------------------------------
__fastcall TGPSCoordinatesRect::TGPSCoordinatesRect()
{
	valid=false;
	automaticupdate=true;
	lefttop=new TGPSCoordinate(automaticupdate);
	rightbottom=new TGPSCoordinate(automaticupdate);
}
//---------------------------------------------------------------------------

__fastcall TGPSCoordinatesRect::TGPSCoordinatesRect(bool au)
{
	automaticupdate=au;
	lefttop=new TGPSCoordinate(automaticupdate);
	rightbottom=new TGPSCoordinate(automaticupdate);
}
//---------------------------------------------------------------------------

__fastcall TGPSCoordinatesRect::TGPSCoordinatesRect(TGPSCoordinate *lt, TGPSCoordinate *rb)
{
	automaticupdate=true;
	lefttop=new TGPSCoordinate(automaticupdate);
	rightbottom=new TGPSCoordinate(automaticupdate);
	SetCorners(lt, rb);
}
//---------------------------------------------------------------------------

void __fastcall TGPSCoordinatesRect::Clear()
{
	valid=false;
	delete lefttop;
	delete rightbottom;
	lefttop=new TGPSCoordinate(automaticupdate);
	rightbottom=new TGPSCoordinate(automaticupdate);
}
//---------------------------------------------------------------------------

void __fastcall TGPSCoordinatesRect::SetCorners(TDoublePoint lt, TDoublePoint rb, bool Square, TCoordinateSystem cs)
{
	TDoublePoint LT, RB;

	if(lt.X==0 && lt.Y==0 && rb.X==0 && rb.Y==0) return;
	if(lt.Y>rb.Y) {LT.Y=lt.Y; RB.Y=rb.Y;}
	else {LT.Y=rb.Y; RB.Y=lt.Y;}
	if(lt.X<rb.X) {LT.X=lt.X; RB.X=rb.X;}
	else {LT.X=rb.X; RB.X=lt.X;}
	*((int*)LT.Temp)=*((int*)lt.Temp);
	*((int*)RB.Temp)=*((int*)rb.Temp);
	if(Square)
	{
		double w, h;

		w=fabs(RB.X-LT.X);
		h=fabs(LT.Y-RB.Y);
		if(w>h)	{LT.Y-=((w-h)/2.); RB.Y+=((w-h)/2.);}
		else if(h>w) {LT.X-=((h-w)/2.);	RB.X+=((h-w)/2.);}
	}
	lefttop->SetXY(cs, LT);
	rightbottom->SetXY(cs, RB);
	valid=true;
}
//---------------------------------------------------------------------------

void __fastcall TGPSCoordinatesRect::SetCorners(TGPSCoordinate *lt, TGPSCoordinate *rb, bool Square, TCoordinateSystem cs)
{
	if(lt && rb) SetCorners(lt->GetXY(cs), rb->GetXY(cs), Square, cs);
}
/*void __fastcall TGPSCoordinatesRect::SetCorners(TGPSCoordinate *lt, TGPSCoordinate *rb, bool Square, TCoordinateSystem cs)
{
	TDoublePoint LT, RB;

	if((lt->Latitude==0 && lt->Longitude==0) ||
		(rb->Latitude==0 && rb->Longitude==0)) return;
	if(lt->GetY(cs)>rb->GetY(cs))
	{
		LT.Y=lt->GetY(cs);
		RB.Y=rb->GetY(cs);
	}
	else
	{
		LT.Y=rb->GetY(cs);
		RB.Y=lt->GetY(cs);
	}
	if(lt->GetX(cs)<rb->GetX(cs))
	{
		LT.X=lt->GetX(cs);
		RB.X=rb->GetX(cs);
	}
	else
	{
		LT.X=rb->GetX(cs);
		RB.X=lt->GetX(cs);
	}

	if(Square)
	{
		double w, h;

		w=fabs(RB.X-LT.X);
		h=fabs(LT.Y-RB.Y);
		if(w>h)
		{
			LT.Y-=((w-h)/2.);
			RB.Y+=((w-h)/2.);
		}
		else if(h>w)
		{
			LT.X-=((h-w)/2.);
			RB.X+=((h-w)/2.);
		}
	}
	lefttop->SetXY(cs, LT);
	rightbottom->SetXY(cs, RB);
	valid=true;
}*/
//---------------------------------------------------------------------------

double __fastcall TGPSCoordinatesRect::Width(TCoordinateSystem cs)
{
	double res;

	try
	{
		if(cs==csLatLon) res=RightBottom->Longitude-LeftTop->Longitude;
		else res=RightBottom->GetX(cs)-LeftTop->GetX(cs);
	}
	catch(Exception &e)
	{
		res=0;
	}
	return res;
}
//---------------------------------------------------------------------------

double __fastcall TGPSCoordinatesRect::Height(TCoordinateSystem cs)
{
	double res;

	try
	{
		if(cs==csLatLon) res=LeftTop->Latitude-RightBottom->Latitude;
		else res=LeftTop->GetY(cs)-RightBottom->GetY(cs);
	}
	catch(Exception &e)
	{
		res=0;
	}
	return res;
}
//---------------------------------------------------------------------------

double __fastcall TGPSCoordinatesRect::WidthInMeters(TCoordinateSystem cs)
{
	double res;

	try
	{
		/*if(cs==csLatLon) res=RightBottom->Longitude-LeftTop->Longitude;
		else res=RightBottom->GetX(cs)-LeftTop->GetX(cs);*/
		res=LeftTop->DistanceVectorInMeters(cs, RightBottom).X;
	}
	catch(Exception &e)
	{
		res=0;
	}
	return res;
}
//---------------------------------------------------------------------------

double __fastcall TGPSCoordinatesRect::HeightInMeters(TCoordinateSystem cs)
{
	double res;

	try
	{
		/*if(cs==csLatLon) res=LeftTop->Latitude-RightBottom->Latitude;
		else res=LeftTop->GetY(cs)-RightBottom->GetY(cs);*/
		res=LeftTop->DistanceVectorInMeters(cs, RightBottom).Y;
	}
	catch(Exception &e)
	{
		res=0;
	}
	return res;
}
//---------------------------------------------------------------------------

TDoubleRect __fastcall TGPSCoordinatesRect::DoubleRect(TCoordinateSystem cs)
{
	TDoubleRect res;

	if(cs==csLatLon) res=TDoubleRect(LeftTop->Longitude, LeftTop->Latitude, RightBottom->Longitude, RightBottom->Latitude);
	else res=TDoubleRect(LeftTop->GetX(cs), LeftTop->GetY(cs), RightBottom->GetX(cs), RightBottom->GetY(cs));
	return res;
}
//---------------------------------------------------------------------------

bool __fastcall TGPSCoordinatesRect::Contains(TDoublePoint c, TCoordinateSystem cs)
{
	return (c.X>=LeftTop->GetX(cs) && c.X<=RightBottom->GetX(cs) &&
		c.Y<=LeftTop->GetY(cs) && c.Y>=RightBottom->GetY(cs));
}
//---------------------------------------------------------------------------

bool __fastcall TGPSCoordinatesRect::Contains(TGPSCoordinate *c)
{
	return (c->Longitude>=LeftTop->Longitude && c->Longitude<=RightBottom->Longitude &&
		c->Latitude<=LeftTop->Latitude && c->Latitude>=RightBottom->Latitude);
}
//---------------------------------------------------------------------------

__fastcall TGPSCoordinatesRect::~TGPSCoordinatesRect()
{
	delete LeftTop;
	delete RightBottom;
}

//---------------------------------------------------------------------------
// TGPSCoordinatesList
//---------------------------------------------------------------------------
void __fastcall TGPSCoordinatesList::Delete(int Index)
{
	if(Objects && Index>=0 && Index<Count)
	{
		if(itemsowner) delete Items[Index];
		Objects->Delete(Index);
	}
}
//---------------------------------------------------------------------------

void __fastcall TGPSCoordinatesList::Clear()
{
	if(Objects)
	{
		try
		{
			if(itemsowner)
			{
				for(int i=0; i<Count; i++)
					delete (TGPSCoordinate*)(Objects->Items[i]);
			}
		}
		__finally
		{
			Objects->Clear();
		}
	}
}
//---------------------------------------------------------------------------

TGPSCoordinate* __fastcall TGPSCoordinatesList::FindNearestPointAtXY(int x, int y)
{
	int d;

	return FindNearestPointAtXY(x, y, d);
}
//---------------------------------------------------------------------------

TGPSCoordinate* __fastcall TGPSCoordinatesList::FindNearestPointAtXY(int x, int y, int &distance)
{
	TGPSCoordinate *llp, *res=NULL;
	int d_old, d, k;

	if(Objects && Count>0)
	{
		llp=Items[0];
		d_old=llp->DistanceToPointInPix(x, y);
		k=0;
		for(int i=Count-1; i>=0; i--)
		{
			llp=Items[i];
			d=llp->DistanceToPointInPix(x, y);
			if(d<d_old)
			{
				d_old=d;
				k=i;
			}
		}
		distance=d_old;
		res=Items[k];
	}
	return res;
}
//---------------------------------------------------------------------------

TGPSCoordinate* __fastcall TGPSCoordinatesList::FindPointInRangeAtXY(int x, int y, int range)
{
	TGPSCoordinate* c=NULL;
	int d;

	c=FindNearestPointAtXY(x, y, d);
	if(c && d>range) c=NULL;

	return c;
}
//---------------------------------------------------------------------------

TGPSCoordinate* __fastcall TGPSCoordinatesList::FindNearestPointAtLatLon(double lat, double lon)
{
	TGPSCoordinate *llp, *res=NULL;
	double d_old, d, k;

	if(Objects && Count>0)
	{
		llp=Items[0];
		if(llp!=NULL) d_old=llp->DistanceToPointInDeg(lat, lon);
		else d_old=0;
		k=0;
		for(int i=0; i<Count; i++)
		{
			llp=Items[i];
			if(llp!=NULL) d=llp->DistanceToPointInDeg(lat, lon);
			else d=0;
			if(d<d_old)
			{
				d_old=d;
				k=i;
			}
		}
		res=Items[k];
	}
	return res;
}
//---------------------------------------------------------------------------

TGPSCoordinate* __fastcall TGPSCoordinatesList::FindNearestPointAtCoordinate(TGPSCoordinate* o, TCoordinateSystem CS, double &distance)
{
	TGPSCoordinate *llp=NULL;

	distance=-1;
	if(Objects && o)
	{
		llp=FindNearestPointAtLatLon(o->Latitude, o->Longitude);
		if(llp) distance=llp->DistanceToPoint(CS, o);
	}

	return llp;
}
//---------------------------------------------------------------------------

int __fastcall TGPSCoordinatesList::IndexOfUniq(TGPSCoordinate* o, TCoordinateSystem CS)
{
	double dist;
	TGPSCoordinate *llp;
	int res=-1;

	llp=FindNearestPointAtCoordinate(o, CS, dist);
	if(llp && dist<=0.001) res=TGPSCoordinatesList::IndexOf(llp);

	return res;
}
//---------------------------------------------------------------------------

