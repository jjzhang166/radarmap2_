//---------------------------------------------------------------------------
#pragma hdrstop

#include "Defines.h"
#include "DXFMap.h"
#include "GR32_Text.hpp"

//---------------------------------------------------------------------------

#pragma package(smart_init)

//---------------------------------------------------------------------------
// TDXFObject
//---------------------------------------------------------------------------
TColor32 __fastcall TDXFObject::readColorPX()
{
	if(color>=BYBLOCK && color<=BYLAYER)
		return DXFColors32[color];
	else return 0;
}
//---------------------------------------------------------------------------

void __fastcall TDXFObject::writeColorPX(TColor32 value)
{
	unsigned int dc=0x00FFFFFF, i=256, j, k;

	for(j=0; j<256; j++)
	{
		k=((DXFColors32[j]&0x00FF0000)-(value&0x00FF0000))>>16;
		k+=((DXFColors32[j]&0x0000FF00)-(value&0x0000FF00))>>8;
		k+=((DXFColors32[j]&0x000000FF)-(value&0x000000FF));
		if(k<dc)
		{
			dc=k;
			i=j;
			if(k==0) break;
		}
	}
	if(i<256) color=i;
}
//---------------------------------------------------------------------------

bool __fastcall TDXFObject::Import(TDXFObject *src)
{
	bool res=false;

	try
	{
		if(src)
		{
			objecttype=src->ObjectType;
			color=src->Color;
			name=src->Name;
			flags=src->Flags;
			description=src->Description;
			comment=src->Comment;
			textheightcoef=src->TextHeightCoef;
			handle=src->Handle;
			res=true;
		}
		else res=false;
	}
	catch(Exception &e)
	{
		res=false;
	}

	return res;
}

//---------------------------------------------------------------------------
// TDXFGroupList
//---------------------------------------------------------------------------
void __fastcall TDXFGroupList::Clear()
{
	try {
		for (int i = 0; i < this->Count; i++) {
			delete (TDXFGroup*)(this->Items[i]);
		}
	}
	__finally {
		TList::Clear();
	}
}
//---------------------------------------------------------------------------

bool __fastcall TDXFGroupList::Import(TDXFGroupList *src) {
	TDXFGroup *group = NULL;

	this->Clear();

	for (int i = 0; i < this->Count; i++) {
		group = new TDXFGroup((TDXFGroup*)(this->Items[i]));
		this->Add(group);
	}
	return true;
}

//---------------------------------------------------------------------------
// TDXFEntity
//---------------------------------------------------------------------------
TColor32 __fastcall TDXFEntity::readColorPX()
{
	if(owner)
	{
		if((owner->ObjectType==dotBlock && color==BYBLOCK) ||
			(owner->ObjectType==dotLayer && (color==BYLAYER || color==BYBLOCK)))
			return owner->ColorPX;//DXFColors32[owner->Color];
	}
	return TDXFObject::readColorPX();
}
//---------------------------------------------------------------------------

bool __fastcall TDXFEntity::Import(TDXFObject *src)
{
	bool res=false;

	try {
		if (src && src->ObjectType == dotEntity && TDXFObject::Import(src)) {
			TDXFEntity *src_entity = dynamic_cast<TDXFEntity *>(src);

			type = src_entity->Type;
			xyz = src_entity->XYZ;
			layername = src_entity->LayerName;

			if (src_entity->stringGroups) {
				if (NULL == this->stringGroups) {
					this->stringGroups = new TDXFGroupList();
				}
				this->stringGroups->Import(src_entity->stringGroups);
			}
			else {
				if (this->stringGroups) {
					this->stringGroups->Clear();
				}
			}

			res = true;
		}
		else {
			res = false;
        }
	}
	catch(Exception & e) {
		res = false;
	}

	return res;
}

//---------------------------------------------------------------------------
// TDXFVertex
//---------------------------------------------------------------------------
bool __fastcall TDXFVertex::Import(TDXFObject *src)
{
	bool res=false;

	try
	{
		if(src && src->ObjectType==dotEntity && ((TDXFVertex*)src)->Type==detVertex)
			res=TDXFEntity::Import(src);
		else res=false;
	}
	catch(Exception &e)
	{
		res=false;
	}

	return res;
}

//---------------------------------------------------------------------------
// TDXFLine
//---------------------------------------------------------------------------
bool __fastcall TDXFLine::Import(TDXFObject *src)
{
	bool res=false;

	try
	{
		if(src && src->ObjectType==dotEntity && ((TDXFLine*)src)->Type==detLine && TDXFEntity::Import(src))
		{
			point2=((TDXFLine*)src)->Point2;
			res=true;
		}
		else res=false;
	}
	catch(Exception &e)
	{
		res=false;
	}

	return res;
}

//---------------------------------------------------------------------------
// TDXFCircle
//---------------------------------------------------------------------------
bool __fastcall TDXFCircle::Import(TDXFObject *src)
{
	bool res=false;

	try
	{
		if(src && src->ObjectType==dotEntity && ((TDXFCircle*)src)->Type==detCircle && TDXFEntity::Import(src))
		{
			r=((TDXFCircle*)src)->R;
			res=true;
		}
		else res=false;
	}
	catch(Exception &e)
	{
		res=false;
	}

	return res;
}

//---------------------------------------------------------------------------
// TDXFArc
//---------------------------------------------------------------------------
bool __fastcall TDXFArc::Import(TDXFObject *src)
{
	bool res=false;

	try
	{
		if(src && src->ObjectType==dotEntity && ((TDXFArc*)src)->Type==detArc && TDXFEntity::Import(src))
		{
			r=((TDXFArc*)src)->R;
			start=((TDXFArc*)src)->Start;
			stop=((TDXFArc*)src)->Stop;
			res=true;
		}
		else res=false;
	}
	catch(Exception &e)
	{
		res=false;
	}

	return res;
}

//---------------------------------------------------------------------------
// TDXFEllipse
//---------------------------------------------------------------------------
bool __fastcall TDXFEllipse::Import(TDXFObject *src)
{
	bool res=false;

	try
	{
		if(src && src->ObjectType==dotEntity && ((TDXFEllipse*)src)->Type==detEllipse && TDXFEntity::Import(src))
		{
			mjaep=((TDXFEllipse*)src)->MjEndPoint;
			mn2mj=((TDXFEllipse*)src)->Mn2MjRatio;
			start=((TDXFEllipse*)src)->Start;
			stop=((TDXFEllipse*)src)->Stop;
			res=true;
		}
		else res=false;
	}
	catch(Exception &e)
	{
		res=false;
	}

	return res;
}

//---------------------------------------------------------------------------
// TDXFVertex
//---------------------------------------------------------------------------
bool __fastcall TDXFPoint::Import(TDXFObject *src)
{
	bool res=false;

	try
	{
		if(src && src->ObjectType==detPoint && ((TDXFPoint*)src)->Type==detPoint)
			res=TDXFEntity::Import(src);
		else res=false;
	}
	catch(Exception &e)
	{
		res=false;
	}

	return res;
}

//---------------------------------------------------------------------------
// TDXFText
//---------------------------------------------------------------------------
bool __fastcall TDXFText::Import(TDXFObject *src)
{
	bool res=false;

	try
	{
		if(src && src->ObjectType==dotEntity && ((TDXFText*)src)->Type==detText && TDXFEntity::Import(src))
		{
			text=((TDXFText*)src)->Text;
			style=((TDXFText*)src)->Style;
			height=((TDXFText*)src)->Height;
			angle=((TDXFText*)src)->Angle;
			alignmentpoint=((TDXFText*)src)->AlignmentPoint;
			hora=((TDXFText*)src)->HorA;
			vera=((TDXFText*)src)->VerA;
			enlargedrawingheight=((TDXFText*)src)->EnlargeDrawingHeight;
			res=true;
		}
		else res=false;
	}
	catch(Exception &e)
	{
		res=false;
	}

	return res;
}

//---------------------------------------------------------------------------
// TDXFInsert
//---------------------------------------------------------------------------
bool __fastcall TDXFInsert::Import(TDXFObject *src)
{
	bool res=false;

	try
	{
		if(src && src->ObjectType==dotEntity && ((TDXFInsert*)src)->Type==detInsert && TDXFEntity::Import(src))
		{
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
			block=NULL; //need to be applied by owner from outside
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
			x_scale=((TDXFInsert*)src)->X_scale;
			y_scale=((TDXFInsert*)src)->Y_scale;
			z_scale=((TDXFInsert*)src)->Z_scale;
			angle=((TDXFInsert*)src)->Angle;
			blockname=((TDXFInsert*)src)->BlockName;
			res=true;
		}
		else res=false;
	}
	catch(Exception &e)
	{
		res=false;
	}

	return res;
}

//---------------------------------------------------------------------------
// TDXFObjectsList
//---------------------------------------------------------------------------
__fastcall TDXFObjectsList::~TDXFObjectsList()
{
	Clear();
	delete Objects;
}
//---------------------------------------------------------------------------

void __fastcall TDXFObjectsList::Delete(int Index)
{
	if(Index>=0 && Index<Objects->Count)
	{
		delete (TDXFObject*)Objects->Items[Index];
		Objects->Delete(Index);
	}
}
//---------------------------------------------------------------------------

void __fastcall TDXFObjectsList::Clear()
{
	if(Objects!=NULL)
	{
		try
		{
			for(int i=0; i<Objects->Count; i++)
			{
				delete (TDXFObject*)(Objects->Items[i]);
			}
		}
		__finally
		{
			Objects->Clear();
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TDXFObjectsList::DeleteEmty()
{
	int i=0;

	if(Objects!=NULL)
	{
		try
		{
			while(i<Objects->Count)
			{
				if(((TDXFObject*)Objects->Items[i])->ObjectType & dotContainer &&
					((TDXFObjectsContainer*)Objects->Items[i])->List->Count==0)
						Objects->Delete(i);
				else i++;
			}
		}
		catch (Exception &e) {}
	}
}

//---------------------------------------------------------------------------
// TDXFObjectsContainer
//---------------------------------------------------------------------------
void __fastcall TDXFObjectsContainer::Add(TDXFEntity *o)
{
	if(o)
	{
		list->Add(o);
		o->Owner=this;
		if(handle_index && o->Handle==0) o->Handle=++(*handle_index);
	}
}
//---------------------------------------------------------------------------

bool __fastcall TDXFObjectsContainer::Import(TDXFObject *src)
{
	bool res=false;

	try
	{
		if(src && (src->ObjectType & dotContainer) && TDXFObject::Import(src))
		{
			TDXFObjectsContainer *Src=(TDXFObjectsContainer*)src;
			TDXFEntity *newe, *entity;

			for(int i=0; i<Src->List->Count; i++)
			{
				entity=(TDXFEntity*)Src->List->Items[i];
				switch(entity->Type)
				{
					case detLine: newe=new TDXFLine(this); break;
					case detCircle: newe=new TDXFCircle(this); break;
					case detArc: newe=new TDXFArc(this); break;
					case detEllipse: newe=new TDXFEllipse(this); break;
					case detPoint: newe=new TDXFPoint(this); break;
					case detText: newe=new TDXFText(this); break;
					case detInsert: newe=new TDXFInsert(this); break;
					case detPolyLine: newe=new TDXFPolyLine(this); break;
					case detLwPolyLine: newe=new TDXFLwPolyLine(this); break;
					//case detSpline: newe=new TDXFSpline(this); break;
					//case detHatch: newe=new TDXFHatch(this); break;
					case detVertex: newe=new TDXFVertex(this); break;
					default: newe=NULL; break;
				}
				if(newe && newe->Import((TDXFObject*)entity)) Add(newe);
				else if(newe) delete newe;
			}
			res=true;
		}
		else res=false;
	}
	catch(Exception &e)
	{
		res=false;
	}

	return res;
}
//---------------------------------------------------------------------------

void __fastcall TDXFObjectsContainer::writeEnlargeDrawingTextHeight(bool value)
{
	for(int i=0; i<list->Count; i++)
	{
		if(((TDXFEntity*)list->Items[i])->Type==detText)
			((TDXFText*)list->Items[i])->EnlargeDrawingHeight=value;
	}
}
//---------------------------------------------------------------------------

bool __fastcall TDXFObjectsContainer::Render(TBitmap32 *Dst, TColor32 bgColor, int InvertColor, TDoubleRect rct,
	TDouble3DPoint SourcePoint, double Scale_x, double Scale_y, double angle)
{
	int w, h, i, k, x, y;
	TDXFEntity *entity;
	TDXFLine *line;
	TDXFPolyLine *pline;
	TDXFLwPolyLine *lwpline;
	TDXFSpline *spline;
	TDXFHatch *hatch;
	TDXFVertex *vertex;
	TDXFInsert *insert;
	TDXFCircle *circle;
	TDXFArc *arc;
	TDXFEllipse *ellipse;
	TDXFPoint *point;
	TDXFText *text;
	TText32 *text32;
	TTrueTypeFont *font;
	double coef_X, coef_Y, text_sz, Ra, Rb, tw, th;
	TDouble3DPoint sp=SourcePoint, ep;
	TLine p1, p2;
	TFontStyles st;
	double da, db, xa, Deg2Rad;
	TColor32 color;
	unsigned char r, g, b;
	double spX;
	TDouble3DPoint p3d;
	TDoublePoint* pdp;
	bool bl;
	int InRectObjects=0;
	Types::TRect DstRect;

	spX=fabs(sp.X);
	//spX=sp.X;
	if(spX<rct.Left) ep.X=rct.Left;
	else if(spX>rct.Right) ep.X=rct.Right;
	else ep.X=spX;
	if(sp.Y<rct.Bottom) ep.Y=rct.Bottom;
	else if(sp.Y>rct.Top) ep.Y=rct.Top;
	else ep.Y=sp.Y;

	//angle+=45;

	Deg2Rad=M_PI/180.;

	try
	{
		if(Dst)
		{
			w=Dst->Width;
			h=Dst->Height;
			DstRect=Dst->BoundsRect();
			coef_X=(float)w/fabs(rct.Width());
			coef_Y=(float)h/fabs(rct.Height());
			for(i=0; i<list->Count; i++)
			{
				entity=(TDXFEntity*)list->Items[i];

				color=entity->ColorPX;
				r=abs((double)((color&0x00FF0000)>>16)-((bgColor&0x00FF0000)>>16));
				g=abs((double)((color&0x0000FF00)>>8)-((bgColor&0x0000FF00)>>8));
				b=abs((double)(color&0x000000FF)-(bgColor&0x000000FF));
				if(r<InvertColor && g<InvertColor && b<InvertColor)
				{
					color=Color32(255-((color&0x00FF0000)>>16), 255-((color&0x0000FF00)>>8), 255-(color&0x000000FF), (color&0xFF000000)>>24);
				}
				switch(entity->Type)
				{
					case detLine:
						line=(TDXFLine*)entity;
						p1=TLine((ep.X-rct.Left)*coef_X, (rct.Top-ep.Y)*coef_Y, (int)((spX+Scale_x*line->XYZ.X-rct.Left)*coef_X), (int)((rct.Top-Scale_y*line->XYZ.Y-sp.Y)*coef_Y));
						p2=TLine((ep.X-rct.Left)*coef_X, (rct.Top-ep.Y)*coef_Y, (int)((spX+Scale_x*line->Point2.X-rct.Left)*coef_X), (int)((rct.Top-Scale_y*line->Point2.Y-sp.Y)*coef_Y));
						p1.Rotate(-angle*Deg2Rad);
						p2.Rotate(-angle*Deg2Rad);
						if(DstRect.Contains(Types::TRect(p1.X2, p1.Y2, p2.X2, p2.Y2)))
						{
							Dst->LineAS(p1.X2, p1.Y2, p2.X2, p2.Y2, color, true);
							//InRectObjects+=(int)DstRect.Contains(Types::TPoint(p1.X2, p1.Y2));
							InRectObjects++;
						}
						break;
					case detCircle:
						circle=(TDXFCircle*)entity;
						da=M_PI2/(double)CirclePoints;
						p1.X1=(spX+circle->XYZ.X-rct.Left)*coef_X;
						p1.Y1=(rct.Top-circle->XYZ.Y-sp.Y)*coef_Y;
						p1.X2=(spX+circle->XYZ.X+Scale_x*circle->R-rct.Left)*coef_X;//p1.X1+Scale_x*circle->R*coef_X;//
						p1.Y2=p1.Y1;//(rct.Top-circle->XYZ.Y-sp.Y)*coef_Y;//
						if(DstRect.Contains(Types::TRect(p1.X1-p1.Length(), p1.Y1-p1.Length(), p2.X2, p2.Y2)))
						{
							Dst->PenColor=color;
							Dst->MoveTo(p1.X2, p1.Y2);
							if(p1.Length()<1.) Dst->LineToS(p1.X2, p1.Y2);
							else for(int j=0; j<CirclePoints; j++)
							{
								p1.Rotate(da);
								Dst->LineToAS(p1.X2, p1.Y2);
							}
							//InRectObjects+=(int)DstRect.Contains(Types::TPoint(p1.X1, p1.Y1));
							InRectObjects++;
						}
						break;
					case detPoint:
						point=(TDXFPoint*)entity;
						p1.X1=(spX+point->XYZ.X-rct.Left)*coef_X;
						p1.Y1=(rct.Top-point->XYZ.Y-sp.Y)*coef_Y;
						if(DstRect.Contains(Types::TPoint(p1.X1, p1.Y1)))
						{
							Dst->SetPixelT(p1.X1, p1.Y1, color);
							//InRectObjects+=(int)DstRect.Contains(Types::TPoint(p1.X1, p1.Y1));
							InRectObjects++;
						}
						break;
					case detArc:
						arc=(TDXFArc*)entity;
						da=(arc->Stop-arc->Start)*Deg2Rad;
						if(da>M_PI2 || da<-M_PI2) da=M_PI2;
						else if(da<0) da+=M_PI2;
						da/=(double)ArcPoints;
						xa=fabs(arc->XYZ.X);
						p1.X1=(spX+xa-rct.Left)*coef_X;
						p1.Y1=(rct.Top-arc->XYZ.Y-sp.Y)*coef_Y;
						p1.X2=(spX+arc->XYZ.X+Scale_x*arc->R-rct.Left)*coef_X;//p1.X1+Scale_x*arc->R*coef_X;
						p1.Y2=p1.Y1;//(rct.Top-arc->XYZ.Y-sp.Y)*coef_Y;//
						if(DstRect.Contains(Types::TRect(p1.X1-p1.Length(), p1.Y1-p1.Length(), p2.X2, p2.Y2)))
						{
							p1.Rotate(-arc->Start*Deg2Rad);
							Dst->PenColor=color;
							//Dst->MoveTo(p1.X2, p1.Y2);
							for(int j=0; j<ArcPoints; j++)
							{
								p1.X2=p1.X1+Scale_x*arc->R*cos(-arc->Start*Deg2Rad-(float)j*da)*coef_X;
								p1.Y2=p1.Y1+Scale_y*arc->R*sin(-arc->Start*Deg2Rad-(float)j*da)*coef_Y;
								if(arc->XYZ.X<0) p1.X2=2*p1.X1-p1.X2;
								if(angle!=0)
								{
									p2=TLine((spX-rct.Left)*coef_X, (rct.Top-sp.Y)*coef_Y, p1.X2, p1.Y2);
									p2.Rotate(-angle*Deg2Rad);
								}
								else p2=p1;
								if(j==0) Dst->MoveTo(p2.X2, p2.Y2);
								else Dst->LineToAS(p2.X2, p2.Y2);
							}
							//InRectObjects+=(int)DstRect.Contains(Types::TPoint(p1.X1, p1.Y1));
							InRectObjects++;
						}
						break;
					case detEllipse:
						ellipse=(TDXFEllipse*)entity;
						da=ellipse->Stop-ellipse->Start;
						if(da>M_PI2 || da<-M_PI2) da=M_PI2;
						else if(da<0) da+=M_PI2;
						p3d=ellipse->XYZ+ellipse->MjEndPoint;
						db=ellipse->XYZ.Angle(p3d);
						da/=(double)EllipsePoints;
						Ra=ellipse->XYZ.Radius(p3d);
						Rb=Ra*ellipse->Mn2MjRatio;

						xa=ellipse->XYZ.X; //fabs(ellipse->XYZ.X);

						p1.X1=(spX+xa-rct.Left)*coef_X;
						p1.Y1=(rct.Top-ellipse->XYZ.Y-sp.Y)*coef_Y;
						//p1.X2=p1.X1+Scale_x*Ra*cos(ellipse->Start)*coef_X;
						//p1.Y2=p1.Y1+Scale_y*Rb*sin(ellipse->Start)*coef_Y;
						p1.X2=p1.X1+Scale_x*(Ra+Rb)*coef_X;
						p1.Y2=p1.Y1+Scale_y*(Ra+Rb)*coef_Y;
						if(DstRect.Contains(Types::TRect(p1.X1-p1.Length(), p1.Y1-p1.Length(), p2.X2, p2.Y2)))
						{
							Dst->PenColor=color;
							for(int j=0; j<=EllipsePoints; j++)
							{
								p1.X2=p1.X1+Scale_x*Ra*cos(ellipse->Start+(float)j*da)*coef_X;
								p1.Y2=p1.Y1+Scale_y*Rb*sin(ellipse->Start+(float)j*da)*coef_Y;
								p1.Rotate(-db);
								if(angle!=0)
								{
									p2=TLine((spX-rct.Left)*coef_X, (rct.Top-sp.Y)*coef_Y, p1.X2, p1.Y2);
									p2.Rotate(-angle*Deg2Rad);
								}
								else p2=p1;
								if(j==0) Dst->MoveTo(p2.X2, p2.Y2);
								else Dst->LineToAS(p2.X2, p2.Y2);
								//Dst->LineToS(p1.X2, p1.Y2);
							}
							//InRectObjects+=(int)DstRect.Contains(Types::TPoint(p1.X1, p1.Y1));
							InRectObjects++;
						}
						//Dst->LineToS(p1.X2+1, p1.Y2);
						break;
					case detText:
						text=(TDXFText*)entity;
						text32=new TText32();
						text_sz=1.5*text->Height*coef_X;
						if(text_sz<1.5) text_sz=1.5;
						font=new TTrueTypeFont((TFontName)"Courier New", text_sz, st, (TFontCharset)0);
						try
						{
							x=(spX+text->XYZ.X-rct.Left)*coef_X;
							y=(rct.Top-text->XYZ.Y-sp.Y)*coef_Y;
							tw=text32->GetTextWidth(text->Text, font);
							th=text32->GetTextHeight(text->Text, font);
							if(th>=1 && tw>=1)
							{
								text32->Angle=text->Angle;
								text32->Draw(Dst, x, y, text->Text, font, color);
							}
							InRectObjects+=(int)DstRect.Contains(Types::TPoint(x, y));
						}
						__finally
						{
							delete text32;
							delete font;
						}
						break;
					case detInsert:
						insert=(TDXFInsert*)entity;
						if(insert && insert->Block)
						{
							/*if(insert->Block->Name=="APLAMP")
							{
								insert->Block->Render(Dst, bgColor, InvertColor, rct, insert->XYZ,
									insert->X_scale, insert->Y_scale, insert->Angle);
							}
							else */
							InRectObjects+=(int)insert->Block->Render(Dst, bgColor, InvertColor, rct, insert->XYZ,
								insert->X_scale, insert->Y_scale, insert->Angle);
						}
						break;
					case detPolyLine:
					case detLwPolyLine:
						if(entity->Type==detPolyLine) {pline=(TDXFPolyLine*)entity; k=pline->Count; bl=pline->Closed;}
						else if(entity->Type==detLwPolyLine) {lwpline=(TDXFLwPolyLine*)entity; k=lwpline->Count; bl=lwpline->Closed;}
						else k=0;
						Dst->PenColor=color;
						x=0;
						for(int j=0; j<k; j++)
						{
							if(entity->Type==detPolyLine)
							{
								vertex=pline->Vertexes[j];
								p1=TLine((ep.X-rct.Left)*coef_X, (rct.Top-ep.Y)*coef_Y, (int)((spX+Scale_x*vertex->XYZ.X-rct.Left)*coef_X), (int)((rct.Top-Scale_y*vertex->XYZ.Y-sp.Y)*coef_Y));
							}
							else if(entity->Type==detLwPolyLine)
							{
								pdp=lwpline->Vertexes[j];
								if(pdp) p1=TLine((ep.X-rct.Left)*coef_X, (rct.Top-ep.Y)*coef_Y, (int)((spX+Scale_x*pdp->X-rct.Left)*coef_X), (int)((rct.Top-Scale_y*pdp->Y-sp.Y)*coef_Y));
								else p1=TLine((ep.X-rct.Left)*coef_X, (rct.Top-ep.Y)*coef_Y, (ep.X-rct.Left)*coef_X, (rct.Top-ep.Y)*coef_Y);
							}
							else p1=TLine((ep.X-rct.Left)*coef_X, (rct.Top-ep.Y)*coef_Y, (ep.X-rct.Left)*coef_X, (rct.Top-ep.Y)*coef_Y);
							p1.Rotate(-angle*Deg2Rad);
							if(j==0)
							{
								Dst->MoveTo(p1.X2, p1.Y2);
								p2=p1;
							}
							else Dst->LineToS(p1.X2, p1.Y2);
							x+=(int)DstRect.Contains(Types::TPoint(p1.X2, p1.Y2));
						}
						InRectObjects+=(int)(x>(k>>1));
						if(k>0 && bl) Dst->LineToS(p2.X2, p2.Y2);
						break;
					case detSpline:
					case detHatch:
						//InRectObjects++;
						break;
				}
			}
		}
	}
	catch(Exception &e)
	{
		InRectObjects=0;
	}
	return (InRectObjects>0 && (InRectObjects>list->Count>>1));
}

//---------------------------------------------------------------------------
// TDXFBlock
//---------------------------------------------------------------------------
void __fastcall TDXFBlock::Add(TDXFEntity *o)
{
	if(o && o->Type!=detInsert) TDXFObjectsContainer::Add(o);
}
//---------------------------------------------------------------------------

bool __fastcall TDXFBlock::Import(TDXFObject *src)
{
	bool res=false;

	try
	{
		if(src && src->ObjectType==dotBlock) res=TDXFObjectsContainer::Import(src);
		else res=false;
	}
	catch(Exception &e)
	{
		res=false;
	}

	return res;
}

//---------------------------------------------------------------------------
// TDXFLayer
//---------------------------------------------------------------------------
bool __fastcall TDXFLayer::Import(TDXFObject *src)
{
	bool res=false;

	try
	{
		if(src && src->ObjectType==dotLayer && TDXFObjectsContainer::Import(src))
		{
			visible=((TDXFLayer*)src)->Visible;
			//utility=((TDXFLayer*)src)->Utility;
			layertype=((TDXFLayer*)src)->LayerType;
			res=true;
		}
		else res=false;
	}
	catch(Exception &e)
	{
		res=false;
	}

	return res;
}

//---------------------------------------------------------------------------
// TDXFObjectsList
//---------------------------------------------------------------------------
TDXFObject* __fastcall TDXFObjectsList::GetItemByName(AnsiString AName)
{
	if(Trim(AName)!="")
	{
		if(!LastFound || LastFound->Name!=AName)
		{
			LastFound=NULL;
			for(int i=0; i<Count; i++)
				if(Items[i]->Name==AName)
					LastFound=(TDXFObject*)Items[i];
		}
	}
	return LastFound;
}

//---------------------------------------------------------------------------
// TDXFPolyLine
//---------------------------------------------------------------------------
__fastcall TDXFPolyLine::TDXFPolyLine(TDXFObject *AOwner) : TDXFEntity(AOwner)
{
	type=detPolyLine;
	thickness=0;
	description="POLYLINE";
	closed=polyline3D=false;
	try
	{
		if(AOwner && (AOwner->ObjectType==dotContainer || AOwner->ObjectType==dotBlock || AOwner->ObjectType==dotLayer) &&
			((TDXFObjectsContainer*)AOwner)->Handle_Index)
				vertexes=new TDXFObjectsContainer(((TDXFObjectsContainer*)AOwner)->Handle_Index,
					description+" "+IntToStr(*((TDXFObjectsContainer*)AOwner)->Handle_Index));
		else vertexes=new TDXFObjectsContainer(NULL, description);
	}
	catch(Exception &e)
	{
		vertexes=new TDXFObjectsContainer(NULL, description);
	}
}
//---------------------------------------------------------------------------

void __fastcall TDXFPolyLine::writeOwner(TDXFObject *value)
{
	owner=value;
	if(owner && (owner->ObjectType==dotContainer || owner->ObjectType==dotBlock || owner->ObjectType==dotLayer) &&
		((TDXFObjectsContainer*)owner)->Handle_Index)
	{
		vertexes->Handle_Index=((TDXFObjectsContainer*)owner)->Handle_Index;
	}
	else vertexes->Handle_Index=NULL;
}
//---------------------------------------------------------------------------

bool __fastcall TDXFPolyLine::Import(TDXFObject *src)
{
	bool res=false;

	try
	{
		if(src && src->ObjectType==dotEntity && ((TDXFPolyLine*)src)->Type==detPolyLine && TDXFEntity::Import(src))
		{
			closed=((TDXFPolyLine*)src)->Closed;
			polyline3D=((TDXFPolyLine*)src)->Polyline3D;
			thickness=((TDXFPolyLine*)src)->Thickness3D;
			res=vertexes->Import(((TDXFPolyLine*)src)->VertexesContainer);
		}
		else res=false;
	}
	catch(Exception &e)
	{
		res=false;
	}

	return res;
}

//---------------------------------------------------------------------------
// TDXFLwPolyLine
//---------------------------------------------------------------------------
__fastcall TDXFLwPolyLine::TDXFLwPolyLine(TDXFObject *AOwner) : TDXFEntity(AOwner)
{
	type=detLwPolyLine;
	thickness=0;
	description="LWPOLYLINE";
	closed=false;
	plinegen=true;
	width=0.0;
	vertexes=new TList();
	addedX_cnt=0;
	addedY_cnt=0;
}
//---------------------------------------------------------------------------

void __fastcall TDXFLwPolyLine::AddedVertexX()
{
	addedX_cnt++;
	if(addedX_cnt>1) //Corrupted vertex
	{
		addedX_cnt=0;
		addedY_cnt=0;
	}
	else
	{
		if(addedY_cnt==1) //Add vertex
		{
			TDoublePoint *pdp;

			pdp=new TDoublePoint;
			pdp->X=XYZ.X;
			pdp->Y=XYZ.Y;
			AddVertex(pdp);
			addedX_cnt=0;
			addedY_cnt=0;
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TDXFLwPolyLine::AddedVertexY()
{
	addedY_cnt++;
	if(addedY_cnt>1) //Corrupted vertex
	{
		addedX_cnt=0;
		addedY_cnt=0;
	}
	else
	{
		if(addedX_cnt==1) //Add vertex
		{
			TDoublePoint *pdp;

			pdp=new TDoublePoint;
			pdp->X=XYZ.X;
			pdp->Y=XYZ.Y;
			AddVertex(pdp);
			addedX_cnt=0;
			addedY_cnt=0;
		}
	}
}
//---------------------------------------------------------------------------

bool __fastcall TDXFLwPolyLine::Import(TDXFObject *src)
{
	bool res=false;
	TDoublePoint *pdp;

	try
	{
		if(src && src->ObjectType==dotEntity && ((TDXFLwPolyLine*)src)->Type==detLwPolyLine && TDXFEntity::Import(src))
		{
			closed=((TDXFLwPolyLine*)src)->Closed;
			plinegen=((TDXFLwPolyLine*)src)->Plinegen;
			width=((TDXFLwPolyLine*)src)->Width;
			thickness=((TDXFLwPolyLine*)src)->Thickness3D;
			for(int i=0; i<vertexes->Count; i++)
				delete (TDoublePoint*)vertexes->Items[i];
			vertexes->Clear();
			for(int i=0; i<((TDXFLwPolyLine*)src)->Count; i++)
			{
				pdp=new TDoublePoint();
				*pdp=*((TDoublePoint*)((TDXFLwPolyLine*)src)->Vertexes[i]);
				AddVertex(pdp);
			}
			res=true;
		}
		else res=false;
	}
	catch(Exception &e)
	{
		res=false;
	}

	return res;
}

//---------------------------------------------------------------------------
// TDXFCollection
//---------------------------------------------------------------------------
__fastcall TDXFCollection::TDXFCollection()
{
	layers=new TDXFObjectsList();
	blocks=new TDXFObjectsList();
	FileName="";
	autocadversion="AC1009";
	handle_index=0;
	comments=new TStringList;
	enlargedrawingtextheight=false;
	lowercorner=DoublePoint(0.0, 0.0);
	uppercorner=DoublePoint(0.0, 0.0);
	maxcorner=NULL;
	mincorner=NULL;
}

__fastcall TDXFCollection::~TDXFCollection()
{
	delete layers;
	delete blocks;
	comments->Clear();
	delete comments;
	if(maxcorner) delete maxcorner;
	if(mincorner) delete mincorner;
}
//---------------------------------------------------------------------------

bool __fastcall TDXFCollection::AddLayer(TDXFLayer *l, bool ignoreWithEqualsName)
{
	//if(l && layers->GetItemByName(l->Name)==NULL)
	if(l && (ignoreWithEqualsName != true || layers->GetItemByName(l->Name)==NULL))
	{
		layers->Add(l);
		return true;
	}
	else return false;
}
//---------------------------------------------------------------------------

bool __fastcall TDXFCollection::AddBlock(TDXFBlock *b)
{
	if(b && blocks->GetItemByName(b->Name)==NULL)
	{
		blocks->Add(b);
		return true;
	}
	else return false;
}
//---------------------------------------------------------------------------

bool __fastcall TDXFCollection::ReadNextGroup(TBufferedROFile *cFile, TDXFGroup &Out, HANDLE AStopItEvent)
{
	char ch;
	unsigned long rd;
	bool reading=true;
	AnsiString str="";
	int i=0, z, readed=0;

	try
	{
		if(cFile && cFile->Opened)
		{
			/*while(reading)
			{
				rd=cFile->Read(&ch, 1);
				if(rd>0)
				{
					if(ch!='\n') str+=ch;
					else
					{
						try
						{
							if(str.Length()>0)
							{
								z=str.Length();
								if(z>0 && str[z]=='\r') str[z]='\0';
								if(i==0) Out.ID=StrToInt(Trim(str));
								else if(i==1)
								{
									Out.Value=Trim(str);
									reading=false;
								}
								str="";
								i++;
							}
						}
						catch(...)
						{
							break;
						}
					}
				}
				else reading=false;
				readed+=rd;
				if(AStopItEvent && WaitForSingleObject(AStopItEvent, 0)==WAIT_OBJECT_0) {reading=false; i=0;}
			}*/
			while(reading)
			{
				readed=cFile->ReadLn(&str);
				if(readed>0)
				{
					try
					{
						if(str.Length()>0)
						{
							z=str.Length();
							if(z>0 && str[z]=='\r') str[z]='\0';
							if(i==0) Out.ID=StrToInt(Trim(str));
							else if(i==1)
							{
								Out.Value=Trim(str);
								reading=false;
							}
							str="";
							i++;
						}
					}
					catch(...) {}
				}
				else reading=false;
				if(progress) progress->Position+=readed;
				if(AStopItEvent && WaitForSingleObject(AStopItEvent, 0)==WAIT_OBJECT_0) {reading=false; i=0;}
			}
		}
	}
	//catch(Exception &e)
	catch(...)
	{
		if(GetLastError()==ERROR_HANDLE_EOF)
		{
			//CloseHandle(hndl);
			//hndl=NULL;
			cFile->Close();
		}
		i=0;
	}
	if(i==2) return true;
	else return false;
}
//---------------------------------------------------------------------------

void __fastcall TDXFCollection::ReadHeader(TBufferedROFile *cFile, HANDLE AStopItEvent)
{
	TDXFGroup gr=TDXFGroup(), gr2;
	bool res;

	do
	{
		res=ReadNextGroup(cFile, gr, AStopItEvent);
	} while(res && (gr.ID!=0 || gr.Value.UpperCase()!="SECTION"));
	do
	{
		res=ReadNextGroup(cFile, gr, AStopItEvent);
	} while(res && (gr.ID!=2 || gr.Value.UpperCase()!="HEADER"));
	do
	{
		res=ReadNextGroup(cFile, gr, AStopItEvent);
		if(res)
		{
			if(gr.ID==9 && gr.Value.UpperCase()=="$ACADVER" && ReadNextGroup(cFile, gr2, AStopItEvent))
			{
				if(gr2.ID==1) autocadversion=gr2.Value;
			}
			else if(gr.ID==9 && gr.Value.UpperCase()=="$EXTMAX")
			{
				ReadNextGroup(cFile, gr2, AStopItEvent);
				if(res && gr2.ID==10) uppercorner.X=StrToFloat(gr2.Value);
				ReadNextGroup(cFile, gr2, AStopItEvent);
				if(res && gr2.ID==20) uppercorner.Y=StrToFloat(gr2.Value);
				//ReadNextGroup(cFile, gr2, AStopItEvent);
				//if(res && gr2.ID==30) uppercorner->Z=StrToFloat(gr2.Value);
			}
			else if(gr.ID==9 && gr.Value.UpperCase()=="$EXTMIN")
			{
				ReadNextGroup(cFile, gr2, AStopItEvent);
				if(res && gr2.ID==10) lowercorner.X=StrToFloat(gr2.Value);
				ReadNextGroup(cFile, gr2, AStopItEvent);
				if(res && gr2.ID==20) lowercorner.Y=StrToFloat(gr2.Value);
				//ReadNextGroup(cFile, gr2, AStopItEvent);
				//if(res && gr2.ID==30) lowercorner->Z=StrToFloat(gr2.Value);
			}
		}
	} while(res && (gr.ID!=0 || gr.Value.UpperCase()!="ENDSEC"));
}
//---------------------------------------------------------------------------

void __fastcall TDXFCollection::ReadTables(TBufferedROFile *cFile, HANDLE AStopItEvent)
{
	TDXFGroup gr=TDXFGroup(), gr2;
	TDXFLayer *layer;
	bool res;

	do
	{
		res=ReadNextGroup(cFile, gr, AStopItEvent);
	} while(res && (gr.ID!=0 || gr.Value.UpperCase()!="SECTION"));
	do
	{
		res=ReadNextGroup(cFile, gr, AStopItEvent);
	} while(res && (gr.ID!=2 || gr.Value.UpperCase()!="TABLES"));
	do
	{
		res=ReadNextGroup(cFile, gr, AStopItEvent);
		if(res)
		{
			if(gr.ID==0 && gr.Value.UpperCase()=="TABLE" && ReadNextGroup(cFile, gr2, AStopItEvent))
			{
				if(gr2.ID==2 && gr2.Value.UpperCase()=="LAYER") //Layers
				{
					do
					{
						if(gr2.ID!=0) res=ReadNextGroup(cFile, gr2, AStopItEvent);
						if(res && gr2.ID==0 && gr2.Value.UpperCase()=="LAYER")
						{
							do
							{
								res=ReadNextGroup(cFile, gr2, AStopItEvent);
							} while(res && !(gr2.ID==2 || gr2.ID==0));
							if(gr2.ID==2)
							{
								layer=new TDXFLayer(&handle_index, gr2.Value);
								AddLayer(layer);
								do
								{
									res=ReadNextGroup(cFile, gr2, AStopItEvent);
								} while(res && !(gr2.ID==62 || gr2.ID==0));
								if(gr2.ID==62) layer->Color=StrToInt(gr2.Value);
							}
						}
						else
						{
							if(res && !(gr2.ID==0 && gr2.Value.UpperCase()=="ENDTAB"))
								res=ReadNextGroup(cFile, gr2, AStopItEvent);
						}
					} while (res && !(gr2.ID==0 && gr2.Value.UpperCase()=="ENDTAB"));
				}
			}
		}
	} while(res && (gr.ID!=0 || gr.Value.UpperCase()!="ENDSEC"));
}
//---------------------------------------------------------------------------

TDXFLayer* __fastcall TDXFCollection::GetLayerByName(AnsiString name, bool CreateIfNotExists)
{
	TDXFLayer* layer;
	layer=(TDXFLayer*)layers->GetItemByName(name);
	if(!layer && CreateIfNotExists && (name!=NULL || name=="0") && name.Length()>0)
	{
		layer=new TDXFLayer(&Handle_index, name);
		AddLayer(layer);
	}
	return layer;
}
//---------------------------------------------------------------------------

TDXFLayer* __fastcall TDXFCollection::GetLayerByDbId(int LayerID)
{
	TDXFLayer* layer=NULL;

	if(LayerID>0)
	{
		layer=(TDXFLayer*)layers->GetLastFound();
		if(!layer || layer->IdDbRecord!=LayerID)
		{
			layer=NULL;
			for(int i=0; i<layers->Count; i++)
				if(((TDXFLayer*)layers->Items[i])->IdDbRecord==LayerID)
				{
					layer=(TDXFLayer*)layers->Items[i];
					layers->SetLastFound((TDXFObject*)layer);
					break;
				}
		}
	}
	return layer;
}
//---------------------------------------------------------------------------

bool __fastcall TDXFCollection::ReadEntity(TBufferedROFile *cFile, TDXFGroup &gr, TDXFObjectsContainer *Container, TDXFEntityType et,
	bool ReadFirst, HANDLE AStopItEvent)
{
	bool res;
	int f, x_cnt=0, y_cnt=0;
	float ff;
	TDXFEntity *Entity;
	TDXFGroup *n_gr = NULL;

	try
	{
		switch(et)
		{
			case detLine: Entity=new TDXFLine(Container); break;
			case detCircle: Entity=new TDXFCircle(Container); break;
			case detArc: Entity=new TDXFArc(Container); break;
			case detEllipse: Entity=new TDXFEllipse(Container); break;
			case detPoint: Entity=new TDXFPoint(Container); break;
			case detText: Entity=new TDXFText(Container); ((TDXFText*)Entity)->EnlargeDrawingHeight=enlargedrawingtextheight; break;
			case detInsert: Entity=new TDXFInsert(Container); break;
			case detPolyLine: Entity=new TDXFPolyLine(Container); break;
			case detLwPolyLine: Entity=new TDXFLwPolyLine(Container); break;
			//case detSpline: Entity=new TDXFSpline(Container); break;
			//case detHatch: Entity=new TDXFHatch(Container); break;
			case detVertex: Entity=new TDXFVertex(Container); break;
		}
		if(cFile && cFile->Opened && Entity)
		{
			do
			{
				res=ReadNextGroup(cFile, gr, AStopItEvent);
				//else ReadFirst=true;
				if(res)
				{
					try
					{
						switch(gr.ID)
						{
							case 0: res=false; break;
							case 5:	Entity->Handle=StrToInt64("$"+gr.Value); break;
							case 8:
								if(!Container) Container=GetLayerByName(gr.Value, true);
								Entity->LayerName=gr.Value;
								break;
							case 10:
								Entity->XYZ.X=StrToFloat(gr.Value);
								if(et==detLwPolyLine) ((TDXFLwPolyLine*)Entity)->AddedVertexX();
								x_cnt++;
								break;
							case 20:
								Entity->XYZ.Y=StrToFloat(gr.Value);
								if(et==detLwPolyLine) ((TDXFLwPolyLine*)Entity)->AddedVertexY();
								y_cnt++;
								break;
							case 30: Entity->XYZ.Z=StrToFloat(gr.Value); break;
							case 62: Entity->Color=StrToInt(gr.Value); break;
							case 1000:
								if (NULL == Entity->StringGroups) {
									Entity->StringGroups = new TDXFGroupList();
								}
								n_gr = new TDXFGroup(gr);
								Entity->StringGroups->Add(n_gr);
								break;
							default:
								switch(et)
								{
									case detLine:
										switch (gr.ID)
										{
											case 11: ((TDXFLine*)Entity)->Point2.X=StrToFloat(gr.Value); break;
											case 21: ((TDXFLine*)Entity)->Point2.Y=StrToFloat(gr.Value); break;
											//case 31: ((TDXFLine*)Entity)->Point2.Z=StrToFloat(gr.Value); break;
										}
										break;
									case detCircle:
										switch (gr.ID)
										{
											case 40: ((TDXFCircle*)Entity)->R=StrToFloat(gr.Value); break;
										}
										break;
									case detArc:
										switch (gr.ID)
										{
											case 40: ((TDXFArc*)Entity)->R=StrToFloat(gr.Value); break;
											case 50: ((TDXFArc*)Entity)->Start=StrToFloat(gr.Value); break;
											case 51: ((TDXFArc*)Entity)->Stop=StrToFloat(gr.Value); break;
										}
										break;
									case detEllipse:
										switch (gr.ID)
										{
											case 11: ((TDXFEllipse*)Entity)->MjEndPoint.X=StrToFloat(gr.Value); break;
											case 21: ((TDXFEllipse*)Entity)->MjEndPoint.Y=StrToFloat(gr.Value); break;
											case 31: ((TDXFEllipse*)Entity)->MjEndPoint.Z=StrToFloat(gr.Value); break;
											case 40: ((TDXFEllipse*)Entity)->Mn2MjRatio=StrToFloat(gr.Value); break;
											case 41: ((TDXFEllipse*)Entity)->Start=StrToFloat(gr.Value); break;
											case 42: ((TDXFEllipse*)Entity)->Stop=StrToFloat(gr.Value); break;
										}
										break;
									case detText:
										switch (gr.ID)
										{
											case 1: ((TDXFText*)Entity)->Text=gr.Value; break;
											case 7: ((TDXFText*)Entity)->Style=gr.Value; break;
											case 11: ((TDXFText*)Entity)->AlignmentPoint.X=StrToFloat(gr.Value); break;
											case 21: ((TDXFText*)Entity)->AlignmentPoint.Y=StrToFloat(gr.Value); break;
											//case 31: ((TDXFText*)Entity)->AlignmentPoint.Z=StrToFloat(gr.Value); break;
											case 40: ((TDXFText*)Entity)->Height=StrToFloat(gr.Value); break;
											case 50: ((TDXFText*)Entity)->Angle=StrToFloat(gr.Value); break;
											case 72: ((TDXFText*)Entity)->HorA=(TDXFHorizontalAlignment)StrToInt(gr.Value); break;
											case 73: ((TDXFText*)Entity)->VerA=(TDXFVerticalAlignment)StrToInt(gr.Value); break;
										}
										break;
									case detInsert:
										switch (gr.ID)
										{
											case 2:
												((TDXFInsert*)Entity)->Block=GetBlockByName(gr.Value);
												if(((TDXFInsert*)Entity)->Block)
													((TDXFInsert*)Entity)->BlockName=gr.Value;
											break;
											case 41: ((TDXFInsert*)Entity)->X_scale=StrToFloat(gr.Value); break;
											case 42: ((TDXFInsert*)Entity)->Y_scale=StrToFloat(gr.Value); break;
											case 43: ((TDXFInsert*)Entity)->Z_scale=StrToFloat(gr.Value); break;
											case 50: ((TDXFInsert*)Entity)->Angle=StrToFloat(gr.Value); break;
										}
										break;
									case detPolyLine:
										switch (gr.ID)
										{
											case 70:
												f=StrToInt(gr.Value);
												if(f&0x01) ((TDXFPolyLine*)Entity)->Closed=true;
												if(f&0x08) ((TDXFPolyLine*)Entity)->Polyline3D=true;
												break;
											case 39: ((TDXFPolyLine*)Entity)->Thickness3D=StrToFloat(gr.Value); break;
										}
										break;
									case detPoint:
									case detVertex: break;
									case detLwPolyLine:
										switch (gr.ID)
										{
											case 70:
												f=StrToInt(gr.Value);
												if(f&0x01) ((TDXFLwPolyLine*)Entity)->Closed=true;
												if(f&0x08) ((TDXFLwPolyLine*)Entity)->Plinegen=true;
												break;
											case 39: ((TDXFLwPolyLine*)Entity)->Thickness3D=StrToFloat(gr.Value); break;
											case 43: ((TDXFLwPolyLine*)Entity)->Width=StrToFloat(gr.Value); break;
										}
										break;
								}
								break;
						}
					}
					catch (Exception &e) {}
				}
			} while(res);
			if(AStopItEvent && WaitForSingleObject(AStopItEvent, 0)==WAIT_OBJECT_0)
			{
				if(Entity) delete Entity;
				res=false;
			}
			else
			{
				if(Entity->Type==detInsert && !((TDXFInsert*)Entity)->Block)
				{
					delete Entity;
					Entity=NULL;
				}
				else
				{
					if(Container) Container->Add(Entity);
					if(!Entity->Owner)
					{
						delete Entity;
						Entity=NULL;
					}
					else
					{
						if(Entity->Type==detPolyLine && gr.Value=="VERTEX")
						{
							do
							{
								res=ReadEntity(cFile, gr, ((TDXFPolyLine*)Entity)->Container, detVertex, false, AStopItEvent);
							} while(res && gr.ID==0 && gr.Value=="VERTEX");
						}
					}
				}
				if(AStopItEvent && WaitForSingleObject(AStopItEvent, 0)==WAIT_OBJECT_0) res=false;
				else res=true;
				if(res && Entity)
				{
					TDouble3DPoint ddp, ddp2, *max=NULL, *min=NULL;

					if((x_cnt>0 && y_cnt>0) || Entity->Type==detPolyLine)
					{
						ddp=Entity->XYZ;
						if(Entity->Type==detArc) ddp.X=fabs(ddp.X);
						ddp2=ddp;
						if(Entity->Type==detInsert && ((TDXFInsert*)Entity)->Block)
						{
							if(((TDXFInsert*)Entity)->Block->MinCorner)
								ddp2=ddp-(*((TDXFInsert*)Entity)->Block->MinCorner);
							if(((TDXFInsert*)Entity)->Block->MaxCorner)
								ddp=ddp+(*((TDXFInsert*)Entity)->Block->MaxCorner);
						}
						if(Entity->Type==detPolyLine)
						{
							if(x_cnt>0 && y_cnt>0)
							{
								if(((TDXFPolyLine*)Entity)->Container->MinCorner)
									ddp2=ddp-(*((TDXFPolyLine*)Entity)->Container->MinCorner);
								if(((TDXFPolyLine*)Entity)->Container->MaxCorner)
									ddp=ddp+(*((TDXFPolyLine*)Entity)->Container->MaxCorner);
							}
							else
							{
								max=((TDXFPolyLine*)Entity)->Container->MaxCorner;
								min=((TDXFPolyLine*)Entity)->Container->MinCorner;
							}
						}
						else
						{
							if(Container)
							{
								if(!Container->MaxCorner) Container->ApplyMaxCorner(ddp);
								max=Container->MaxCorner;
								if(!Container->MinCorner) Container->ApplyMinCorner(ddp2);
								min=Container->MinCorner;
							}
							else
							{
								if(!MaxCorner) ApplyMaxCorner(ddp);
								max=MaxCorner;
								if(!MinCorner) ApplyMinCorner(ddp);
								min=MinCorner;
							}
						}
						if(max && min)
						{
							if(max->X<ddp.X) max->X=ddp.X;
							if(max->Y<ddp.Y) max->Y=ddp.Y;
							if(max->Z<ddp.Z) max->Z=ddp.Z;
							if(min->X>ddp2.X) min->X=ddp2.X;
							if(min->Y>ddp2.Y) min->Y=ddp2.Y;
							if(min->Z>ddp2.Z) min->Z=ddp2.Z;
						}
					}
				}
			}
		}
		else
		{
			if(Entity) delete Entity;
			res=false;
		}
	}
	catch(...)
	{
		if(Entity) delete Entity;
		res=false;
	}
	return res;
}
//---------------------------------------------------------------------------

bool __fastcall TDXFCollection::LookForEntities(TBufferedROFile *cFile, TDXFGroup &gr, TDXFObjectsContainer *Container, HANDLE AStopItEvent)
{
	bool res;

	if(cFile && cFile->Opened)
	{
		if(gr.ID!=0) res=ReadNextGroup(cFile, gr, AStopItEvent);
		else res=true;
		do
		{
			if(res)
			{
				if(gr.ID==0)
				{
					if(gr.Value.UpperCase()=="LINE") res=ReadEntity(cFile, gr, Container, detLine, true, AStopItEvent);
					else if(gr.Value.UpperCase()=="POLYLINE") res=ReadEntity(cFile, gr, Container, detPolyLine, true, AStopItEvent);
					else if(gr.Value.UpperCase()=="LWPOLYLINE") res=ReadEntity(cFile, gr, Container, detLwPolyLine, true, AStopItEvent);
					else if(gr.Value.UpperCase()=="SPLINE") res=ReadEntity(cFile, gr, Container, detLwPolyLine, true, AStopItEvent); // detSpline);
					//else if(gr.Value.UpperCase()=="HATCH") res=ReadEntity(cFile, gr, Container, detHatch, true, AStopItEvent);
					else if(gr.Value.UpperCase()=="CIRCLE") res=ReadEntity(cFile, gr, Container, detCircle, true, AStopItEvent);
					else if(gr.Value.UpperCase()=="TEXT") res=ReadEntity(cFile, gr, Container, detText, true, AStopItEvent);
					else if(gr.Value.UpperCase()=="ARC") res=ReadEntity(cFile, gr, Container, detArc, true, AStopItEvent);
					else if(gr.Value.UpperCase()=="ELLIPSE") res=ReadEntity(cFile, gr, Container, detEllipse, true, AStopItEvent);
					else if(gr.Value.UpperCase()=="POINT") res=ReadEntity(cFile, gr, Container, detPoint, true, AStopItEvent);
					else if(gr.Value.UpperCase()=="INSERT") res=ReadEntity(cFile, gr, Container, detInsert, true, AStopItEvent);
					//else if(gr.Value.UpperCase()=="SEQEND") res=false;
					else if(gr.Value.UpperCase()=="ENDBLK" || gr.Value.UpperCase()=="ENDSEC" || gr.Value.UpperCase()=="EOF") res=false;
					else res=ReadNextGroup(cFile, gr, AStopItEvent);
				}
				else res=ReadNextGroup(cFile, gr, AStopItEvent);
				if(AStopItEvent && WaitForSingleObject(AStopItEvent, 0)==WAIT_OBJECT_0) return false;
			}
			else return false;
		} while(res);
		return true;
	}
	else return false;
}

void __fastcall TDXFCollection::ReadBlocks(TBufferedROFile *cFile, HANDLE AStopItEvent)
{
	TDXFGroup gr=TDXFGroup();
	bool res;

	do
	{
		res=ReadNextGroup(cFile, gr, AStopItEvent);
	} while(res && (gr.ID!=0 || gr.Value.UpperCase()!="SECTION"));
	do
	{
		res=ReadNextGroup(cFile, gr, AStopItEvent);
	} while(res && (gr.ID!=2 || gr.Value.UpperCase()!="BLOCKS"));
	do
	{
		res=ReadNextGroup(cFile, gr, AStopItEvent);
		if(res)
		{
			if(gr.ID==0 && gr.Value.UpperCase()=="BLOCK")
			{
				do
				{
					do
					{
						res=ReadNextGroup(cFile, gr, AStopItEvent);
					} while(res && gr.ID!=2);
					if(gr.ID==2)
					{
						TDXFBlock *block;

						block=new TDXFBlock(&handle_index, gr.Value);
						try
						{
							if(AddBlock(block))
							{
								do
								{
									res=ReadNextGroup(cFile, gr, AStopItEvent);
									if(res)
									{
										if(gr.ID==70)
										{
											block->Flags=gr.Value;
										}
										else if(gr.ID==62)
										{
											block->Flags=gr.Value;
										}
									}
								} while(res && !(gr.ID==4 || gr.ID==0));
								if(gr.ID==4) block->Description=gr.Value;
								res=LookForEntities(cFile, gr, block, AStopItEvent);
							}
							else throw Exception("Cannot add DXFBlock!");
						}
						catch(Exception &e)
						{
							delete block;
							res=false;
						}
					}
				} while (res && !(gr.ID==0 && gr.Value.UpperCase()=="ENDBLK"));
			}
			if(AStopItEvent && WaitForSingleObject(AStopItEvent, 0)==WAIT_OBJECT_0) res=false;
		}
	} while(res && !(gr.ID==0 && gr.Value.UpperCase()=="ENDSEC"));
}
//---------------------------------------------------------------------------

void __fastcall TDXFCollection::ReadEntities(TBufferedROFile *cFile, HANDLE AStopItEvent)
{
	TDXFGroup gr=TDXFGroup(), gr2;
	bool res;

	do
	{
		res=ReadNextGroup(cFile, gr, AStopItEvent);
	} while(res && (gr.ID!=0 || gr.Value.UpperCase()!="SECTION"));
	do
	{
		res=ReadNextGroup(cFile, gr, AStopItEvent);
	} while(res && (gr.ID!=2 || gr.Value.UpperCase()!="ENTITIES"));
	do
	{
		res=LookForEntities(cFile, gr, NULL, AStopItEvent);
	} while(res && (gr.ID!=0 || gr.Value.UpperCase()!="ENDSEC"));
}
//---------------------------------------------------------------------------

bool __fastcall TDXFCollection::LoadData(AnsiString filename, HANDLE AStopItEvent, bool aPreview)
{
	DWORD fSize=0;
	bool res;
	TDXFLayer* layer;
	//unsigned long ul;
	TBufferedROFile* cFile;

	if(FileExists(filename))
	{
		FileName=filename;
		//ul=::GetTickCount();

		cFile=new TBufferedROFile(NULL, filename, true);
		if(cFile && cFile->Open())
		{
			fSize=cFile->FileSize;
			if(fSize == 0xFFFFFFFF) fSize=0;
			if(fSize>0)
			{
				if(progress) progress->Show(fSize, !aPreview);
				Clear();
				try
				{
					ReadHeader(cFile, AStopItEvent);
					if(AStopItEvent && WaitForSingleObject(AStopItEvent, 0)==WAIT_OBJECT_0) res=false;
					else
					{
						ReadTables(cFile, AStopItEvent);
						if(AStopItEvent && WaitForSingleObject(AStopItEvent, 0)==WAIT_OBJECT_0) res=false;
						else
						{
							ReadBlocks(cFile, AStopItEvent);
							if(AStopItEvent && WaitForSingleObject(AStopItEvent, 0)==WAIT_OBJECT_0) res=false;
							else
							{
								blocks->DeleteEmty();
								ReadEntities(cFile, AStopItEvent);
								if(AStopItEvent && WaitForSingleObject(AStopItEvent, 0)==WAIT_OBJECT_0) res=false;
								{
									layers->DeleteEmty();
									for(int j=0; j<layers->Count; j++)
									{
										layer=(TDXFLayer*)layers->Items[j];

										if(layer->MaxCorner && layer->MinCorner)
										{
											if(!MaxCorner) ApplyMaxCorner((*layer->MaxCorner));
											if(!MinCorner) ApplyMinCorner((*layer->MinCorner));
											if(MaxCorner->X<layer->MaxCorner->X) MaxCorner->X=layer->MaxCorner->X;
											if(MaxCorner->Y<layer->MaxCorner->Y) MaxCorner->Y=layer->MaxCorner->Y;
											if(MaxCorner->Z<layer->MaxCorner->Z) MaxCorner->Z=layer->MaxCorner->Z;
											if(MinCorner->X>layer->MinCorner->X) MinCorner->X=layer->MinCorner->X;
											if(MinCorner->Y>layer->MinCorner->Y) MinCorner->Y=layer->MinCorner->Y;
											if(MinCorner->Z>layer->MinCorner->Z) MinCorner->Z=layer->MinCorner->Z;
										}
										if(AStopItEvent && WaitForSingleObject(AStopItEvent, 0)==WAIT_OBJECT_0)
										{
											res=false;
											break;
										}
									}
								}
							}
						}
					}
				}
				__finally
				{
					if(cFile)
					{
						cFile->Close();
						delete cFile;
					}
					//if(!aPreview) ShowMessage(FloatToStrF((float)(::GetTickCount()-ul)/1000., ffFixed, 10, 3));
				}
				if(progress) progress->Hide(!aPreview);
			}
			res=true;
		}
		else res=false;
	}
	else res=false;

	return res;
}
//---------------------------------------------------------------------------

void __fastcall TDXFCollection::WriteHeader(TStrings *strs)
{
	if(strs)
	{
		strs->Add("  0");
		strs->Add("SECTION");
		strs->Add("  2");
		strs->Add("HEADER");
		strs->Add("  9");
		strs->Add("$ACADVER");
		strs->Add("  1");
		strs->Add(autocadversion);
		strs->Add("  9");
		strs->Add("$EXTMIN");
		strs->Add("  10");
		strs->Add(FloatToStrF(lowercorner.X, ffFixed, 20, 14));
		strs->Add("  20");
		strs->Add(FloatToStrF(lowercorner.Y, ffFixed, 20, 14));
		strs->Add("  30");
		strs->Add("0.0");
		strs->Add("  9");
		strs->Add("$EXTMAX");
		strs->Add("  10");
		strs->Add(FloatToStrF(uppercorner.X, ffFixed, 20, 14));
		strs->Add("  20");
		strs->Add(FloatToStrF(uppercorner.Y, ffFixed, 20, 14));
		strs->Add("  30");
		strs->Add("0.0");
		strs->Add("  9");
		strs->Add("$LIMMIN");
		strs->Add("  10");
		strs->Add(FloatToStrF(lowercorner.X-fabs(uppercorner.X-lowercorner.X), ffFixed, 20, 14));
		strs->Add("  20");
		strs->Add(FloatToStrF(lowercorner.Y-fabs(uppercorner.Y-lowercorner.Y), ffFixed, 20, 14));
		strs->Add("  9");
		strs->Add("$LIMMAX");
		strs->Add("  10");
		strs->Add(FloatToStrF(uppercorner.X+fabs(uppercorner.X-lowercorner.X), ffFixed, 20, 14));
		strs->Add("  20");
		strs->Add(FloatToStrF(uppercorner.Y+fabs(uppercorner.Y-lowercorner.Y), ffFixed, 20, 14));
		if(Comments && Comments->Count>0)
		{
			for(int i=0; i<Comments->Count; i++)
			{
				strs->Add("  999");
				strs->Add(Comments->Strings[i]);
			}
		}
		strs->Add("  0");
		strs->Add("ENDSEC");
	}
}
//---------------------------------------------------------------------------

void __fastcall TDXFCollection::WriteTables(TStrings *strs)
{
	TDXFLayer* layer;

	if(strs)
	{
		strs->Add("  0");
		strs->Add("SECTION");
		strs->Add("  2");
		strs->Add("TABLES");
		strs->Add("  0");
		strs->Add("TABLE");
		strs->Add("  2");
		strs->Add("LAYER");
		strs->Add("  70");
		strs->Add(IntToStr(layers->Count));
		for(int i=0; i<layers->Count; i++)
		{
			layer=(TDXFLayer*)layers->Items[i];
			if(layer)
			{
				strs->Add("  0");
				strs->Add("LAYER");
				strs->Add("  2");
				strs->Add(layer->Name);
				strs->Add("  70");
				strs->Add("0");
				strs->Add("  62");
				if(layer->Color>0) strs->Add(IntToStr(layer->Color));
				else strs->Add("1");
				strs->Add("  6");
				strs->Add("CONTINUOUS");
				if(layer->Description!="")
				{
					strs->Add("  999");
					strs->Add(layer->Description);
				}
				if(layer->Comment!="")
				{
					strs->Add("  999");
					strs->Add(layer->Comment);
				}
			}
		}
//EmtyLayer
		if(!GetLayerByName(EmptyLayerName))
		{
			strs->Add("  0");
			strs->Add("LAYER");
			strs->Add("  2");
			strs->Add(EmptyLayerName);
			strs->Add("  70");
			strs->Add("0");
			strs->Add("  62");
			strs->Add("1");
			strs->Add("  6");
			strs->Add("CONTINUOUS");
			strs->Add("  999");
			strs->Add("!! RadarMap temporary empty layer for internal use !!");
		}
//End of EmptyLayer
		strs->Add("  0");
		strs->Add("ENDTAB");
		strs->Add("  0");
		strs->Add("ENDSEC");
	}
}
//---------------------------------------------------------------------------

void __fastcall TDXFCollection::WriteEntities(TStrings *strs, TDXFObjectsContainer *Container)
{
	TDXFEntity *entity;
	TDoublePoint *pdp;

	if(strs && Container && Container->List)
	{
		for(int i=0; i<Container->List->Count; i++)
		{
			entity=(TDXFEntity*)Container->List->Items[i];
			strs->Add("  0");
			strs->Add(entity->Description);
			strs->Add("  5");
			strs->Add(IntToHex(entity->Handle, 1));
			strs->Add("  8");
			if(entity->Owner && entity->Owner->ObjectType==dotLayer)
				strs->Add(entity->Owner->Name);
			else if(entity->LayerName!="") strs->Add(entity->LayerName);
			else strs->Add(EmptyLayerName);
			strs->Add("  62");
			strs->Add(IntToStr(entity->Color));
			if(entity->Type!=detLwPolyLine)
			{
				strs->Add("  10");
				strs->Add(FloatToStrF(entity->XYZ.X, ffFixed, 20, 14));
				strs->Add("  20");
				strs->Add(FloatToStrF(entity->XYZ.Y, ffFixed, 20, 14));
				strs->Add("  30");
				strs->Add(FloatToStrF(entity->XYZ.Z, ffFixed, 20, 14));
			}
			switch(entity->Type)
			{
				case detLine:
				{
					strs->Add("  11");
					strs->Add(FloatToStrF(((TDXFLine*)entity)->Point2.X, ffFixed, 20, 14));
					strs->Add("  21");
					strs->Add(FloatToStrF(((TDXFLine*)entity)->Point2.Y, ffFixed, 20, 14));
					strs->Add("  31");
					strs->Add(FloatToStrF(((TDXFLine*)entity)->Point2.Z, ffFixed, 20, 14));
					break;
				}
				case detCircle:
				{
					strs->Add("  40");
					strs->Add(FloatToStrF(((TDXFCircle*)entity)->R, ffFixed, 20, 14));
					break;
				}
				case detArc:
				{
					strs->Add("  40");
					strs->Add(FloatToStrF(((TDXFArc*)entity)->R, ffFixed, 20, 14));
					strs->Add("  50");
					strs->Add(FloatToStrF(((TDXFArc*)entity)->Start, ffFixed, 20, 14));
					strs->Add("  51");
					strs->Add(FloatToStrF(((TDXFArc*)entity)->Stop, ffFixed, 20, 14));
					break;
				}
				case detEllipse:
				{
					strs->Add("  11");
					strs->Add(FloatToStrF(((TDXFEllipse*)entity)->MjEndPoint.X, ffFixed, 20, 14));
					strs->Add("  21");
					strs->Add(FloatToStrF(((TDXFEllipse*)entity)->MjEndPoint.Y, ffFixed, 20, 14));
					strs->Add("  31");
					strs->Add(FloatToStrF(((TDXFEllipse*)entity)->MjEndPoint.Z, ffFixed, 20, 14));
					strs->Add("  40");
					strs->Add(FloatToStrF(((TDXFEllipse*)entity)->Mn2MjRatio, ffFixed, 20, 14));
					strs->Add("  41");
					strs->Add(FloatToStrF(((TDXFEllipse*)entity)->Start, ffFixed, 20, 14));
					strs->Add("  42");
					strs->Add(FloatToStrF(((TDXFEllipse*)entity)->Stop, ffFixed, 20, 14));
					break;
				}
				case detText:
				{
					strs->Add("  1");
					strs->Add(((TDXFText*)entity)->Text);
					strs->Add("  7");
					strs->Add(((TDXFText*)entity)->Style);
					strs->Add("  11");
					strs->Add(FloatToStrF(((TDXFText*)entity)->AlignmentPoint.X, ffFixed, 20, 14));
					strs->Add("  21");
					strs->Add(FloatToStrF(((TDXFText*)entity)->AlignmentPoint.Y, ffFixed, 20, 14));
					strs->Add("  31");
					strs->Add(FloatToStrF(((TDXFText*)entity)->AlignmentPoint.Z, ffFixed, 20, 14));
					strs->Add("  40");
					strs->Add(FloatToStrF(((TDXFText*)entity)->Height, ffFixed, 20, 14));
					strs->Add("  50");
					strs->Add(FloatToStrF(((TDXFText*)entity)->Angle, ffFixed, 20, 14));
					strs->Add("  72");
					strs->Add(StrToInt(((TDXFText*)entity)->HorA));
					strs->Add("  73");
					strs->Add(StrToInt(((TDXFText*)entity)->VerA));
					break;
				}
				case detInsert:
				{
					if(((TDXFInsert*)entity)->Block)
					{
						strs->Add("  2");
						//strs->Add(((TDXFInsert*)entity)->BlockName);
						strs->Add(((TDXFInsert*)entity)->Block->Name);
						((TDXFInsert*)entity)->BlockName=((TDXFInsert*)entity)->Block->Name;
						strs->Add("  41");
						strs->Add(FloatToStrF(((TDXFInsert*)entity)->X_scale, ffFixed, 20, 14));
						strs->Add("  42");
						strs->Add(FloatToStrF(((TDXFInsert*)entity)->Y_scale, ffFixed, 20, 14));
						strs->Add("  43");
						strs->Add(FloatToStrF(((TDXFInsert*)entity)->Z_scale, ffFixed, 20, 14));
						strs->Add("  50");
						strs->Add(FloatToStrF(((TDXFInsert*)entity)->Angle, ffFixed, 20, 14));
					}
					break;
				}
				case detPolyLine:
				{
					strs->Add("  70");
					strs->Add(IntToStr(128+(int)((TDXFPolyLine*)entity)->Closed)+8*(int)((TDXFPolyLine*)entity)->Polyline3D);
					strs->Add("  39");
					if(((TDXFPolyLine*)entity)->Thickness3D>0) strs->Add(FloatToStrF(((TDXFPolyLine*)entity)->Thickness3D, ffFixed, 20, 14));
					else strs->Add("0.1");
					strs->Add("  66");
					if(((TDXFPolyLine*)entity)->Container->List->Count>0) strs->Add("1");
					else strs->Add("0");
					WriteEntities(strs, ((TDXFPolyLine*)entity)->Container);
					strs->Add("  0");
					strs->Add("SEQEND");
					break;
				}
				case detPoint:
				case detVertex: break;
				case detLwPolyLine:
				{
					strs->Add("  70");
					strs->Add(IntToStr(128+(int)((TDXFLwPolyLine*)entity)->Closed)+8*(int)((TDXFLwPolyLine*)entity)->Plinegen);
					strs->Add("  39");
					if(((TDXFLwPolyLine*)entity)->Thickness3D>0) strs->Add(FloatToStrF(((TDXFLwPolyLine*)entity)->Thickness3D, ffFixed, 20, 14));
					else strs->Add("0.1");
					strs->Add("  43");

					strs->Add(IntToStr(((TDXFLwPolyLine*)entity)->Count));
					for(int j=0; j<((TDXFLwPolyLine*)entity)->Count; j++)
					{
						pdp=(TDoublePoint*)((TDXFLwPolyLine*)entity)->Vertexes[j];
						if(pdp)
						{
							strs->Add("  10");
							strs->Add(FloatToStrF(pdp->X, ffFixed, 20, 14));
							strs->Add("  20");
							strs->Add(FloatToStrF(pdp->Y, ffFixed, 20, 14));
						}
					}
					break;
				}
			}
			if(entity->Comment!="")
			{
				strs->Add("  999");
				strs->Add(entity->Comment);
			}
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TDXFCollection::WriteBlocks(TStrings *strs)
{
	TDXFBlock* block;
	AnsiString s;
	int i;

	if(strs)
	{
		strs->Add("  0");
		strs->Add("SECTION");
		strs->Add("  2");
		strs->Add("BLOCKS");
		for(int i=0; i<blocks->Count; i++)
		{
			block=(TDXFBlock*)blocks->Items[i];
			if(block)
			{
				s=block->Name;
				if(s!=NULL && s!="")
				{
					for(int g=1; g<=s.Length(); g++)
						if(s[g]=='\r' || s[g]=='\n' || s[g]=='.' || s[g]==',' || s[g]=='\\' || s[g]=='/' || s[g]=='!' || s[g]=='?' ||
							s[g]==':' || s[g]==';' || s[g]=='\n' || s[g]=='"' || s[g]=='\'' || s[g]=='(' || s[g]==')' || s[g]==' ' ||
								s[g]=='{' || s[g]=='}' || s[g]=='[' || s[g]==']')
									s[g]='_';
					if(block->Name!=s)
					{
						if(GetBlockByName(s))
						{
							i=0;
							s+="_";
							while(GetBlockByName(s+IntToStr(i))) i++;
							s+=IntToStr(i);
							block->Name=s;
						}
					}
					strs->Add("  0");
					strs->Add("BLOCK");
					strs->Add("  2");
					strs->Add(s);
					strs->Add("  10");
					strs->Add("0.0");
					strs->Add("  20");
					strs->Add("0.0");
					strs->Add("  30");
					strs->Add("0.0");
					strs->Add("  70");
					if(block->Flags.Trim()!="") strs->Add(block->Flags);
					else strs->Add("0");
					//strs->Add("  4");
					//strs->Add(block->Description);
					if(block->Description!="")
					{
						strs->Add("  999");
						strs->Add(block->Description);
					}
					WriteEntities(strs, block);
					if(block->Comment!="")
					{
						strs->Add("  999");
						strs->Add(block->Comment);
					}
					strs->Add("  0");
					strs->Add("ENDBLK");
				}
			}
		}
		strs->Add("  0");
		strs->Add("ENDSEC");
	}
}
//---------------------------------------------------------------------------

bool __fastcall TDXFCollection::SaveData(AnsiString filename)
{
	TStrings *strs;
	TDXFLayer *layer;

	FileName=filename;
	strs=new TStringList;
	try
	{
		WriteHeader(strs);
		WriteTables(strs);
		WriteBlocks(strs);
		strs->Add("  0");
		strs->Add("SECTION");
		strs->Add("  2");
		strs->Add("ENTITIES");
		for(int i=0; i<layers->Count; i++)
		{
			layer=(TDXFLayer*)layers->Items[i];
			if(layer) WriteEntities(strs, layer);
		}
		strs->Add("  0");
		strs->Add("ENDSEC");
		strs->Add("  0");
		strs->Add("EOF");
	}
	__finally
	{
		strs->SaveToFile(filename);
		strs->Clear();
		delete strs;
	}
	return true;
}
//---------------------------------------------------------------------------

float __fastcall TDXFCollection::readAutoCadVersionNum()
{
	AnsiString str=" ", str2;
	int c=0;
	float f;

	if(autocadversion!=NULL && autocadversion.Length()>0)
	{
		for(int i=autocadversion.Length(); i>0; i--)
		{
			if((autocadversion[i]>47 && autocadversion[i]<58) ||
				autocadversion[i]=='.' || autocadversion[i]==',')
			{
				if(autocadversion[i]=='.' || autocadversion[i]==',')
				{
					if(c==0)
					{
						str2=DecimalSeparator;
						c++;
					}
					else
					{
						str="";
						break;
					}
				}
				else str2=autocadversion[i];
				if(str2!="" && str2.Length()>0)
					str=str2+str;
			}
			else break;
		}
	}
	try
	{
		if(str!=NULL && str.Length()>0)	f=StrToFloat(str);
		else f=0;
	}
	catch(Exception &e) {f=0;}

	return f;
}

bool __fastcall TDXFCollection::Import(TDXFCollection *In)
{
	bool res=false;
	int i;

	if(In)
	{
		try
		{
			TDXFBlock *block;
			TDXFLayer *layer;
			TDXFEntity *entity;

			if(AutoCadVersionNum<In->AutoCadVersionNum)
			{
				//autocadversion=In->AutoCadVersion;
			}
			for(i=0; i<In->Blocks->Count; i++)
			{
				block=GetBlockByName(In->Blocks->Items[i]->Name);
				if(!block)
				{
					block=new TDXFBlock(&handle_index, In->Blocks->Items[i]->Name);
					if(!AddBlock(block))
					{
						delete block;
						block=NULL;
					}
				}
				if(block) block->Import(In->Blocks->Items[i]);
			}
			blocks->DeleteEmty();
			for(i=0; i<In->Layers->Count; i++)
			{
				layer=GetLayerByName(In->Layers->Items[i]->Name);
				if(!layer)
				{
					layer=new TDXFLayer(&handle_index, In->Layers->Items[i]->Name);
					if(!AddLayer(layer))
					{
						delete layer;
						layer=NULL;
					}
				}
				if(layer && layer->Import(In->Layers->Items[i]))
				{
					for(int j=0; j<layer->List->Count; j++)
					{
						entity=(TDXFEntity*)layer->List->Items[j];
						if(entity->Type==detInsert)
							((TDXFInsert*)entity)->Block=GetBlockByName(((TDXFInsert*)entity)->BlockName);
                    }
				}
			}
			layers->DeleteEmty();
			res=true;
		}
		catch(Exception &e)
		{
			res=false;
		}
	}
	else res=false;

	return res;
}
//---------------------------------------------------------------------------

void __fastcall TDXFCollection::writeEnlargeDrawingTextHeight(bool value)
{
	int i;

	if(enlargedrawingtextheight!=value)
	{
		enlargedrawingtextheight=value;
		for(i=0; i<Blocks->Count; i++)
		{
			if(Blocks->Items[i] && Blocks->Items[i]->ObjectType==dotBlock)
				((TDXFBlock*)Blocks->Items[i])->EnlargeDrawingTextHeight=value;
		}
		for(i=0; i<Layers->Count; i++)
		{
			if(Layers->Items[i] && Layers->Items[i]->ObjectType==dotLayer)
				((TDXFLayer*)Layers->Items[i])->EnlargeDrawingTextHeight=value;
		}
	}
}
