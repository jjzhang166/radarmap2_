//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Splash.h"
#include "Defines.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "G32_ProgressBar"
#pragma link "GR32_Image"
#pragma resource "*.dfm"
TSplashForm *SplashForm;
//---------------------------------------------------------------------------
__fastcall TSplashForm::TSplashForm(TComponent* Owner)
	: TForm(Owner)
{
#ifdef RadarMap_TRIAL
	TrialLabel->Visible=true;
#else
	TrialLabel->Visible=false;
#endif
	Label1->Caption=(AnsiString)_RadarMapCopyRight+"\n v"+(AnsiString)_RadarMapVersion;
	Caption=_RadarMapName;
}
//---------------------------------------------------------------------------

