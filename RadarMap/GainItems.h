//---------------------------------------------------------------------------

#ifndef GainItemsH
#define GainItemsH

#include "Manager.h"
#include <GR32_Image.hpp>
#include <ImageButton.h>

//---------------------------------------------------------------------------

const int ColorTrackBarShift = 12;

enum TGainControlsId {gcNextPoint=-1, gcPrevPoint=-2, gcMoveLeft=-3, gcMoveRight=-4, gcMoveUp=-5, gcMoveDown=-6,
	gcLock=-7, gcTagShift=10000};

const float MinGainXShift = 0.025;
const float MinGainYShift = 2;

class TGainItem: public TObject
{
private:
	TObject* Owner;
	TGainPoint *gainpoint;
	int id;
	Types::TRect gainwin;
	bool enabled;
	TImage* activeimage;
	TImage* disabledimage;
	TImageButton* imagebutton;
	TBitmapLayer *TraceLayer, *GainLayer;
	bool visible;
	TControl *button;
protected:
	void writeID(int value) {if(value>=0 && value<=MaxGainPointsCount) id=value;}
	int readX() {if(gainpoint) return (int)(gainpoint->x*(float)gainwin.Width()); else return -1;}
	int readY() {if(gainpoint) return (int)(gainpoint->y*(float)gainwin.Height()/MaxGain); else return -1;}
	void writeX(value) {if(value<gainwin.Left) value=gainwin.Left; if(value>gainwin.Right) value=gainwin.Right; if(gainpoint) gainpoint->x=(float)gainwin.Width()/(float)(value-gainwin.Left);}
	void writeY(value) {if(value<gainwin.Top) value=gainwin.Top; if(value>gainwin.Bottom) value=gainwin.Bottom; if(gainpoint) gainpoint->y=MaxGain*(float)gainwin.Height()/(float)(value-gainwin.Top);}
	void writeEnabled(bool value) {if(enabled!=value) {enabled=value; if(visible) {if(ActiveImage) ActiveImage->Visible=enabled; if(DisabledImage) DisabledImage->Visible=!enabled;} if(ImageButton) ImageButton->Down=enabled;}}
	//void __fastcall OnOffGainItem(TGainItem *GainItem);
	void __fastcall GainPointImageClick(TObject *Sender);
	void __fastcall writeActiveImage(TImage* value) {if(activeimage!=value) {if(activeimage) activeimage->OnClick=NULL; if(value) value->OnClick=GainPointImageClick; activeimage=value; if(activeimage) activeimage->Visible=enabled; if(disabledimage) disabledimage->Visible=!enabled; button=value;}}
	void __fastcall writeDisabledImage(TImage* value) {if(disabledimage!=value) {if(disabledimage) disabledimage->OnClick=NULL; if(value) value->OnClick=GainPointImageClick; disabledimage=value; if(activeimage) activeimage->Visible=enabled; if(disabledimage) disabledimage->Visible=!enabled; button=value;}}
	void __fastcall writeImageButton(TImageButton* value) {if(imagebutton!=value) {if(imagebutton) imagebutton->OnClick=NULL; if(value) value->OnClick=GainPointImageClick; imagebutton=value; if(imagebutton) imagebutton->Down=enabled; button=value;}}
	void __fastcall writeVisible(bool value) {if(visible!=value) {visible=value; if(visible) {if(ActiveImage) ActiveImage->Visible=enabled; if(DisabledImage) DisabledImage->Visible=!enabled;} else {if(ActiveImage) ActiveImage->Visible=false; if(DisabledImage) DisabledImage->Visible=false;} if(ImageButton) ImageButton->Visible=visible;}}
public:
	__property TImage* ActiveImage = {read=activeimage, write=writeActiveImage};
	__property TImage* DisabledImage = {read=disabledimage, write=writeDisabledImage};
	__property TImageButton* ImageButton = {read=imagebutton, write=writeImageButton};

	__fastcall TGainItem(TObject* AOwner, int aID=0) {Owner=AOwner; gainpoint=NULL; id=aID; gainwin=Types::TRect(0,0,0,0); activeimage=NULL; disabledimage=NULL; imagebutton=NULL; button=NULL; enabled=false; visible=true;}
	__fastcall TGainItem(TObject* AOwner, TGainPoint *gp, int aID, Types::TRect rct) {Owner=AOwner; gainpoint=gp; id=aID; gainwin=rct; activeimage=NULL; disabledimage=NULL; imagebutton=NULL; button=NULL; enabled=false; visible=true;}
	__fastcall TGainItem(TObject* AOwner, TGainPoint *gp, int aID, Types::TRect rct, TImage *AI, TImage *DI, TImageButton *IB=NULL) {Owner=AOwner; gainpoint=gp; id=aID; gainwin=rct; activeimage=NULL; disabledimage=NULL; imagebutton=NULL; button=NULL; ActiveImage=AI; DisabledImage=DI; ImageButton=IB; enabled=false;}
	__fastcall ~TGainItem() {if(ActiveImage) ActiveImage->OnClick=NULL; if(DisabledImage) DisabledImage->OnClick=NULL; if(ImageButton) ImageButton->OnClick=NULL;}

	__property TGainPoint *GainPoint = {read=gainpoint, write=gainpoint};
	__property int ID = {read=id, write=writeID};
	__property Types::TRect GainWin = {read=gainwin, write=gainwin};
	__property int PxX = {read=readX, write=writeX};
	__property int PxY = {read=readY, write=writeY};
	__property bool Enabled = {read=enabled, write=writeEnabled};
	__property bool Visible = {read=visible, write=writeVisible};
	__property TControl *Button = {read=button};
};

class TReConnectThread: public TStartStopThread
{
private:
	TObject *Manager, *Owner;
protected:
	void __fastcall ExecuteLoopBody();
public:
	__fastcall TReConnectThread(TObject* AManager, TObject* AOwner) : TStartStopThread() {Manager=AManager; Owner=AOwner;}
	__fastcall ~TReConnectThread() {}
};

class TSetupThread: public TMyThread
{
private:
	TObject *Manager;
	TImgView32* Image;
	TBitmapLayer *Layer;
	TFixedSizeQueue *Frames;
	TBitmap32 *LastDrawed;
	TTrace *Trace;
	TGainFunction *Gain;
	bool allowsingletrace;
	TTrace* singletrace;
	char transparency;
	bool *ApplyGain;
	bool visible;
	Types::TRect *BordersRect, *TempRect;
protected:
	void __fastcall ExecuteLoopBody();
	void __fastcall LayerPaint(TObject *Sender, TBitmap32 *Bmp);
	void __fastcall writeTraceTransparency(char value) {try{ if(transparency!=value){ transparency=value; ProcessTrace();}} catch(Exception &e) {}}
	void __fastcall writeSingleTrace(TTrace *value) {if(singletrace!=value) {singletrace=value; if(allowsingletrace) ProcessTrace();}}
public:
	__fastcall TSetupThread(bool CreateSuspended, TObject* AManager, TImgView32 *AImage,
		bool AAllowSingleTrace, bool *AG, Types::TRect *BR, TGainFunction* GF=NULL);//, TBitmap32 *ABmp);
	__fastcall ~TSetupThread();

	void __fastcall ProcessTrace();

	__property char TraceTransparency = {write=writeTraceTransparency};
	__property bool Visible = {read=visible, write=visible};

	__property TTrace *SingleTrace = {read=singletrace, write=writeSingleTrace};
	__property bool AllowSingleTrace = {read=allowsingletrace};
};/**/

class TOscilloscopeGrid : public TObject
{
private:
	TImgView32 *GainImage;
	TBitmapLayer *Background;
	int rows, ChannelsPxOffset;
	TColor32 color;
	TGPRUnit *GPRUnit;
	TColor32 backgroundcolor;
	bool frame;
	void __fastcall BackgroundPaint(TObject *Sender, TBitmap32 *Bmp);
	bool visible;
	Types::TRect *BordersRect;
protected:
	__fastcall void writeColor(TColor32 value) {if(color!=value) {color=value; Background->Update();}}
	__fastcall void writeRows(int value) {if(rows!=value) {rows=value; Background->Update();}}
	__fastcall void writeFrame(bool value) {if(frame!=value) {frame=value; Background->Update();}}
public:
	__fastcall TOscilloscopeGrid(TImgView32 *AImage, TGPRUnit *AGPRUnit,
		int AChannelsPxOffset, Types::TRect *BR, TColor32 ABackgroundColor=clWhite32);
	__fastcall ~TOscilloscopeGrid();
	void __fastcall Refresh() {Background->Update();}

	__property TColor32 Color = {read=color, write=writeColor};
	__property int Rows = {read=rows, write=writeRows};
	__property bool Frame = {read=frame, write=writeFrame};
	__property bool Visible = {read=visible, write=visible};
};

class TGainItems : public TObject
{
private:
	//TImage32 *GainImage;
	TImgView32 *GainImage;
	TObject* Manager;
	TGainItem** GainItems;
	TGainFunction *gainfunction, *oldgain;
	TBitmapLayer *GainLayer;//, *Background;//
	TBitmap32* LockBmp;
	TSetupThread *SetupThread;
	TReConnectThread *ConnectThread;
	TTrackBar* TransparencyTB;
	TImage *NextPointGC, *PrevPointGC, *MoveLeftGC, *MoveRightGC, *MoveUpGC,
		*MoveDownGC, *LockGC, *UnlockGC;
	int selectedgainitem;
	TGainPoint* SelectedGainPoint;
	bool selfconnect, selfconnected, Pressed;
	TGainControlsId LastId;
	TTimer *PressNHoldTimer;
	TPanel* PointButtonsPanel;
	Types::TRect *BordersRect;
	TChannel *channel, *oldchannel;
	TColor32 backgroundcolor;
	Types::TPoint LastXY;
	bool showdb, visible, allowsingletrace; //writeChannel,
	bool *ApplyGain;
	void __fastcall GainImageClick(TObject *Sender);
	void __fastcall GainImageMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
		  int X, int Y, TCustomLayer *Layer);
	void __fastcall GainImageMouseMove(TObject *Sender, TShiftState Shift, int X, int Y,
		  TCustomLayer *Layer);
	void __fastcall GainImageMouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift,
		  int X, int Y, TCustomLayer *Layer);
	void __fastcall GainImageDblClick(TObject *Sender);
	void __fastcall GainLayerPaint(TObject *Sender, TBitmap32 *Bmp);
	//void __fastcall BackgroundPaint(TObject *Sender, TBitmap32 *Bmp);
	void __fastcall TransparencyTBChange(TObject *Sender);
	void __fastcall ProcessControl(TGainControlsId Id);
	void __fastcall GainControlMouseDown(TObject *Sender, TMouseButton Button,
		  TShiftState Shift, int X, int Y);
	void __fastcall GainControlMouseUp(TObject *Sender, TMouseButton Button,
		  TShiftState Shift, int X, int Y);
	void __fastcall writeSelectedGainItem(int value);
	void __fastcall PressNHoldOnTimer(TObject *Sender);
	void __fastcall GetGainPointBorders(int x, int y, int &x1, int &x2, int &xx1, int &xx2,
		int &y1, int &y2, int &yy1, int &yy2, bool Activated=false);
protected:
	TGainItem* __fastcall readItems(int Index) {if(Index>=0 && Index<MaxGainPointsCount) return GainItems[Index]; else return NULL;}
	void __fastcall writeItems(int Index, TGainItem *value) {if(Index>=0 && Index<MaxGainPointsCount) {if(GainItems[Index]) delete GainItems[Index]; GainItems[Index]=value;}}
	TGPRUnit* __fastcall readGPRUnit() {if(Manager) return ((TRadarMapManager*)Manager)->ReceivingSettings; else return NULL;}
	bool __fastcall readReadyForGain();
	void __fastcall writeVisible(bool value);
	void __fastcall writeSelfConnect(bool value);
	void __fastcall writeAllowSingleTrace(bool value);
	void __fastcall writeChannel(TChannel* value);
	void __fastcall writeSingleTrace(TTrace* value) {if(SetupThread) SetupThread->SingleTrace=value;}
	TTrace* __fastcall readSingleTrace() {if(SetupThread) return SetupThread->SingleTrace; else return NULL;}
	int __fastcall readWw() {if(BordersRect) return BordersRect->Width(); else return GainImage->Width;}
	int __fastcall readHh() {if(BordersRect) return BordersRect->Height(); else return GainImage->Height;}
	int __fastcall readX0() {if(BordersRect) return BordersRect->Left; else return 0;}
	int __fastcall readY0() {if(BordersRect) return BordersRect->Top; else return 0;}

	__property int Ww = {read=readWw};
	__property int Hh = {read=readHh};
	__property int X0 = {read=readX0};
	__property int Y0 = {read=readY0};
public:
	//__fastcall TGainItems(TObject* AManager, TImage32 *AImage, bool SelfConnect, TTrackBar* ATransparencyTB, bool *AG, bool SetupThreadIsNeeded, TPanel *PBP=NULL);
	__fastcall TGainItems(TObject* AManager, TChannel* AChannel, TImgView32 *AImage, Types::TRect *BR,
		bool SelfConnect, bool AAllowSingleTrace, TTrackBar* ATransparencyTB, bool *AG, TPanel *PBP=NULL, TColor32 ABackgroundColor=clWhite32);
	__fastcall ~TGainItems();

	__property TGPRUnit* GPRUnit = {read=readGPRUnit};

	void __fastcall GetGainFunction();
	void __fastcall ReAssignGainPoints();
	void __fastcall SelectClosestItem();
	void __fastcall AssignControls(int Id, TImage *AI, TImage *DI);
	void __fastcall AssignControls(int Id, TImageButton* IB);
	bool __fastcall ApplyGainFunction();// {if(GPRUnit && GPRUnit->SelectedChannel && GPRUnit->SelectedChannel->Gain) {GPRUnit->SelectedChannel->Gain->Copy(gainfunction); return true;} else return false;}
	void __fastcall Redraw();
	int __fastcall FindItemArroundXY(int X, int Y);

	__property int SelectedGainItem = {read=selectedgainitem, write=writeSelectedGainItem}; // editing selection
	__property TGainItem* Items[int Index] = {read=readItems, write=writeItems};
	__property TGainFunction* GainFunction = {read=gainfunction};
	__property bool SelfConnect = {read=selfconnect, write=writeSelfConnect};
	__property bool SelfConnected = {read=selfconnected, write=selfconnected};
	__property bool ReadyForGain = {read=readReadyForGain};
	__property bool Visible = {read=visible, write=writeVisible};
	__property bool ShowDB = {read=showdb, write=showdb};
	__property TChannel* Channel = {write=writeChannel};
	__property bool AllowSingleTrace = {read=allowsingletrace, write=writeAllowSingleTrace};
	__property TTrace *SingleTrace = {read=readSingleTrace, write=writeSingleTrace};
};

#endif
