//---------------------------------------------------------------------------
#ifndef XMLMapH
#define XMLMapH
//---------------------------------------------------------------------------

#include "Defines.h"
#include "VisualToolObjects.h"
#include "LibXmlComps.hpp"
#include "GPSCoordinate.h"
#include "DXFMap.h"
#include "GpsListener.h"
#include "UPPredictor.h"
#include <gdiplus.h>
#include "XMLExplorer.h"
#include "Layers.h"
//#include "Manager.h"

#define TakeInAccountViewport32
//#undef TakeInAccountViewport32

typedef void __fastcall (__closure *TPredictorFoundObject)(TUPPFavoriteObject *favorite);//TMouseAction ma);

class TXMLMapObject : public TMyObject
{
private:
	TMyObject *owner;
	int index;
	bool visible;
	void __fastcall writeVisible(bool value) {visible=value; Update();}
protected:
	void __fastcall writeIndex(int value) {index=value;}
	void __fastcall writeOwner(TMyObject* value) {owner=value;}
public:
	__fastcall TXMLMapObject(TMyObject *AOwner);
	__fastcall ~TXMLMapObject() {}

	void __fastcall Update(TMyObjectType Type) {Update();}
	virtual void __fastcall Update() {if(Owner!=NULL) Owner->SetObjectChanged(gotNone);}

	__property int Index = {read=index, write=writeIndex};
	__property bool Visible = {read=visible, write=writeVisible};
	__property TMyObject* Owner = {read=owner, write=writeOwner};
};
//---------------------------------------------------------------------------

enum TXMLMapBitmapType {xmbtLocation, xmbtDimensions, xmbtAnnotation, xmbtTopo, xmbtElgenTopo};

class TXMLMapBitmap : public TXMLMapObject
{
private:
	AnsiString caption, filename;
	TXMLMapBitmapType type;
protected:
public:
	__fastcall TXMLMapBitmap(TMyObject *AOwner, TXMLMapBitmapType T, UnicodeString AFilename);
	__fastcall ~TXMLMapBitmap() {}
	void __fastcall Update(TMyObjectType Type) {Update();}
	void __fastcall Update();

	void __fastcall ChangeType(TXMLMapBitmapType T) {if(T!=type){ type=T; Update();}}

	__property AnsiString Caption = {read=caption};
	__property AnsiString FileName = {read=filename};
	__property TXMLMapBitmapType Type = {read=type};
};
//---------------------------------------------------------------------------

class TXMLMapObjectsList: public TMyObjectsList
{
private:
protected:
	TXMLMapObject* __fastcall readItem(int Index) {return (TXMLMapObject*)readMyObjectItem(Index);}
	void __fastcall writeItem(int Index, TXMLMapObject* value) {writeMyObjectItem(Index, (TMyObject*)value);}
public:
	__fastcall TXMLMapObjectsList(TMyObject *AOwner) : TMyObjectsList(AOwner) {}
	__fastcall ~TXMLMapObjectsList() {}

	int __fastcall Add(TMyObject* o) {if(o!=NULL) ((TXMLMapObject*)o)->Owner=Owner; return AddMyObject(o);}

	__property TXMLMapObject* Items[int Index] = {read=readItem, write=writeItem};
};
//---------------------------------------------------------------------------

class TXMLMapTheme : public TXMLMapObject 				//Thema
{
private:
	AnsiString name;
	TXMLMapObjectsList* Maps;
protected:
	int __fastcall readCount() {return Maps->Count;}
public:
	__fastcall TXMLMapTheme(TMyObject *AOwner) : TXMLMapObject(AOwner) {Maps=new TXMLMapObjectsList(this); name="";}
	__fastcall ~TXMLMapTheme() {delete Maps;}

	int __fastcall AddObject(TMyObject *o);
	TXMLMapBitmap* GetObject(TXMLMapBitmapType T);

	__property AnsiString ThemeName = {read=name, write=name}; 	//Themanaam
	__property int Count = {read=readCount};

};
//---------------------------------------------------------------------------

class TXMLMapThemesCollection : public TXMLMapObject  	//NetbeheerderLevering
{
private:
	AnsiString name, shortname;
	int id;
	TXMLMapObjectsList* Themes; 						//Themas
protected:
	int __fastcall readCount() {if(Themes!=NULL) return Themes->Count; else return -1;}
public:
	__fastcall TXMLMapThemesCollection(TMyObject *AOwner)
		: TXMLMapObject(AOwner) {Themes=new TXMLMapObjectsList(this); id=0; name=""; shortname="";}
	__fastcall ~TXMLMapThemesCollection() {delete Themes;}

	int __fastcall AddObject(TMyObject *o);
	TXMLMapTheme* __fastcall GetObject(int Index) {return (TXMLMapTheme*)Themes->Items[Index];}
	void __fastcall Delete(int Index) {Themes->Delete(Index);}

	__property AnsiString Name = {read=name, write=name};  			//Bedrijfsnaam
	__property AnsiString ShortName = {read=shortname, write=shortname};	//BedrijfsnaamAfkorting
	__property int Count = {read=readCount};
	__property int ID = {read=id, write=id}; 							//RelatienummerNetbeheerder
};
//---------------------------------------------------------------------------

enum TMapType {mtNone=0, mtBitmap=0x0001, mtVector=0x0002, mtFloating=0x0004, mtOnlineMap=0x0008,
	mtEmpty=0x0101, mtXML=0x0201, mtAdjustable=0x0301, //Truly bitmap maps
	mtDXF=0x0102, //Truly vector Maps
	//Mixed type Maps
	mtSQLiteDXF=0x0106, // Floating Vector Maps
	mtBingInet=0x020D, // Online Floating Bitmap Maps
	mtOsmInet=0x030D}; // Online Floating Bitmap Maps

class TMapsContainersCollection;

class TMapsContainer: public TXMLMapObject
{
private:
	TBitmapLayer* WatermarkLayer;
	bool __fastcall readFloatingMap() {return ((int)maptype & (int)mtFloating);}
	TMyObject* __fastcall readObjectsContainer();
protected:
	TMapsContainersCollection* Collection;
	float scalex;
	bool CornersChanged;
	bool showbackground;
	bool lockedcs;
	AnsiString klicnummer, filename, backupfilename;
	AnsiString location;
	AnsiString lowercorner, uppercorner;
	TDoublePoint lower, upper;
	TDoubleRect coordinatesrect, positionrect;
	TBitmap32 *UtilityBmp, *InfoBmp, *backgroundbmp;
	TToolProgressBar *progress;
	TXMLMapObjectsList* Collections;
	TCoordinateSystem coordinatesystem;
	TGPSCoordinatesRect *latlonrect;
	TMapType maptype;
	void *radarmapsettings;
	TObject *Manager;
	TVoidFunction ondisablebutton;
	TLockedDoubleRect *viewportarea;
	double scalingfactorx, scalingfactory;
	TGpsListener *GpsListener;
	bool copyrights;
	AnsiString copyrightsstr, copyrightslink;

	TGPSCoordinatesList *predictedcoordinateslist;
	TPredictorFoundObject onpredictorfoundobject;
	virtual void __fastcall ProcessPredictorResults(void *vMapPoint, unsigned long oLifetime);
	virtual void __fastcall AddCoordinate(TGPSCoordinate *c); //Callback function for TGpsListener, to process the each new coordinate, acquired by NMEA clients

	void __fastcall ConnectGpsListener();
	TGPSCoordinatesRect* __fastcall readLatLonRect() {return latlonrect;}
	void __fastcall writeBackground(TBitmap32* value) {if(backgroundbmp!=NULL) delete backgroundbmp; backgroundbmp=value;}
	void __fastcall writeLower(TDoublePoint value);
	void __fastcall writeUpper(TDoublePoint value);
	//void __fastcall LatLonRectUpdate();
	virtual void __fastcall LatLonRectScaling(TDoubleRect *drct) {}
	int __fastcall readCount() {return Collections->Count;}
	void __fastcall writeLowerCorner(AnsiString value);
	void __fastcall writeUpperCorner(AnsiString value);
	void __fastcall writeCoordinateSystem(TCoordinateSystem value);
	void __fastcall writeShowBackground(bool value) {if(showbackground!=value) {showbackground=value; Update();}}
	virtual void __fastcall WatermarkLayerPaint(TObject *Sender, TBitmap32 *Bmp);

	__property TBitmap32* background = {read=backgroundbmp, write=writeBackground};
	__property TDoublePoint Lower = {read=lower, write=writeLower};
	__property TDoublePoint Upper = {read=upper, write=writeUpper};
	__property TMyObject* ObjectsContainer = {read=readObjectsContainer};
public:
	__fastcall TMapsContainer(TMyObject *AOwner, void* ASettings, TLockedDoubleRect *AViewportArea);
	__fastcall ~TMapsContainer();
	int __fastcall AddObject(TMyObject *o);
	void __fastcall LatLonRectUpdate();
	virtual void __fastcall UtilityUpdate() {}
	virtual void __fastcall InfoUpdate() {}
	virtual void __fastcall Update(TMyObjectType Type); //overload from TMyObject
	virtual void __fastcall Update();
	virtual void __fastcall ZoomUpdate() {Update();}
	virtual void __fastcall ZoomUpdate(TMyObjectType Type) {Update(Type);};
	virtual void __fastcall RenderMap() {}
	//TDoublePoint __fastcall StringToDoublePoint(AnsiString value);
	//TDouble3DPoint __fastcall StringToDouble3DPoint(AnsiString pos);

	AnsiString __fastcall FindFileAtDir(AnsiString path, AnsiString Mask);

	virtual void __fastcall SaveData() {}

	virtual void __fastcall LoadData(AnsiString Filename, HANDLE AStopItEvent, bool WithUpdate, TCoordinateSystem CS, bool Preview=false) {}
	virtual void __fastcall LoadData(AnsiString Filename, HANDLE AStopItEvent, bool WithUpdate, bool Preview=false);// {LoadData(Filename, WithUpdate, csLatLon, Preview);}
	virtual void __fastcall LoadData(AnsiString Filename, HANDLE AStopItEvent) {LoadData(Filename, AStopItEvent, true);}

	void __fastcall LoadDataAsThread(AnsiString Filename, HANDLE AStopItEvent, bool WithUpdate, TCoordinateSystem CS, TVoidFunction SuccessCallBack, TVoidFunction CancelCallBack, bool Preview=false);
	void __fastcall LoadDataAsThread(AnsiString Filename, HANDLE AStopItEvent, bool WithUpdate, TVoidFunction SuccessCallBack, TVoidFunction CancelCallBack, bool Preview=false);
	void __fastcall LoadDataAsThread(AnsiString Filename, HANDLE AStopItEvent, TVoidFunction SuccessCallBack, TVoidFunction CancelCallBack) {LoadDataAsThread(Filename, AStopItEvent, true, SuccessCallBack, CancelCallBack);}

	void __fastcall GeneratePreview(AnsiString Filename, HANDLE AStopItEvent=NULL) {LoadData(Filename, AStopItEvent, false, true);}

	virtual void __fastcall ClearData();
	void __fastcall DisconnectGpsListener();

	virtual void __fastcall BackupEx(AnsiString aFileName);
	void __fastcall Backup() {BackupEx(backupfilename);}
	virtual void __fastcall RollbackBackupEx(AnsiString aFileName, bool AsThread);
	void __fastcall RollbackBackup(bool AsThread=false) {RollbackBackupEx(backupfilename, AsThread);}

	virtual void __fastcall GetLayers(TCheckBoxTreeView *TreeView, TTreeNode *Root) {}
	virtual void __fastcall SetLayers(TCheckBoxTreeView *TreeView, TTreeNode *Root) {}

	virtual Types::TRect __fastcall GetBitmapRect() {if(background) return Types::TRect(0, 0, background->Width, background->Height); else return Types::TRect(0, 0, 0, 0);}

	virtual void __fastcall GetFavotiteObjectsAtPoint(int X, int Y) {} //Objects finder around screen coordinates XY

	__property TMapType MapType = {read=maptype};
	__property AnsiString FileName = {read=filename};
	__property AnsiString BackupFileName = {read=backupfilename};
	__property AnsiString Klicnummer = {read=klicnummer};   //Klicnummer
	__property AnsiString Location = {read=location, write=location};       //Locatie - gml:polList, need to convert to points list
	__property AnsiString LowerCorner = {read=lowercorner, write=writeLowerCorner}; //Envelope Lower Corner
	__property AnsiString UpperCorner = {read=uppercorner, write=writeUpperCorner}; //Envelope Upper Corner
	__property TDoubleRect CoordinatesRect = {read=coordinatesrect};
	__property TDoubleRect PositionRect = {read=positionrect, write=positionrect};
	__property int Count = {read=readCount};
	__property TBitmap32* Background = {read=backgroundbmp};
	__property TBitmap32* Utility = {read=UtilityBmp};
	__property TBitmap32* Info = {read=InfoBmp};
	__property TToolProgressBar *Progress = {read=progress, write=progress};
	__property float ScaleX = {read=scalex};
	__property TCoordinateSystem CoordinateSystem = {read=coordinatesystem, write=writeCoordinateSystem};
	__property TGPSCoordinatesRect *LatLonRect = {read=readLatLonRect};
	__property bool ShowBackground = {read=showbackground, write=writeShowBackground};
	__property TVoidFunction OnDisableButton = {read=ondisablebutton, write=ondisablebutton};
	__property bool LockedCS = {read=lockedcs, write=lockedcs};
	__property TLockedDoubleRect *ViewportArea = {read=viewportarea};
	__property double ScalingFactorX = {read=scalingfactorx};
	__property double ScalingFactorY = {read=scalingfactory};
	__property bool IsFloatingMap = {read=readFloatingMap};

	__property TGPSCoordinatesList *PredictedCoordinatesList = {read=predictedcoordinateslist};
	__property TPredictorFoundObject OnPredictorFoundObject = {read=onpredictorfoundobject, write=onpredictorfoundobject};
	__property bool Copyrights = {read=copyrights};
	__property AnsiString CopyrightsStr = {read=copyrightsstr};
	__property AnsiString CopyrightsLink = {read=copyrightslink};
};

class TMapsContainerLoader: public TMyThread
{
private:
	TObject* Manager;
	TMapsContainer* Owner;
	AnsiString Filename;
	HANDLE StopItEvent;
	bool WithUpdate;
	TCoordinateSystem CS;
	TVoidFunction SuccessCallBack;
	TVoidFunction CancelCallBack;
	bool Preview;
protected:
	void __fastcall UpdateOwner();
	void __fastcall Execute();
public:
	__fastcall TMapsContainerLoader(TObject* aManager, TMapsContainer* aOwner,
		AnsiString aFilename, HANDLE aStopItEvent, bool aWithUpdate,
		TCoordinateSystem aCS, TVoidFunction aSuccessCallBack, TVoidFunction aCancelCallBack,
		bool aPreview);
};

class TMapsContainersCollection : public TMyObject
{
private:
	bool ZoomUpdateInProgress, NeedReZooming; //Crutches to avoid several runings of ZoomUpdate() while ProcessMessages() from progress windmill
	TMyObject *owner;
	TImgView32 *image;
	TBitmap32 *UtilityBmp, *InfoBmp, *BackgroundBmp, *Draw, *UtilityDraw, *InfoDraw;
	TBitmapLayer *UtilityLayer, *InfoLayer;
	TToolProgressBar *progress;
	TXMLMapObjectsList* Collection;
	TDoubleRect coordinatesrect;
	Types::TRect* realrect;
	void* RadarMapSettings;
	TGPSCoordinatesRect *latlonrect;
	TLockedDoubleRect* viewportarea;
	void __fastcall GetBackgroundDraw();
	//TCoordinateSystem coordinatesystem;
protected:
	int __fastcall readCount() {return Collection->Count;}
	void __fastcall writeProgress(TToolProgressBar *value);
	TBitmap32** __fastcall readUtilityPtr() {return &UtilityDraw;}
	TCoordinateSystem __fastcall readCoordinateSystem();
	void __fastcall writeCoordinateSystem(TCoordinateSystem value);
public:
	__fastcall TMapsContainersCollection(TMyObject *AOwner, TImgView32 *ImgView, void* RMS);
	__fastcall ~TMapsContainersCollection();
	int __fastcall AddObject(TMyObject *o);
	void __fastcall UtilityUpdate();
	void __fastcall InfoUpdate();
	void __fastcall BackgroundUpdate();
	void __fastcall Update(TMyObjectType Type); //overload from TMyObject
	void __fastcall Update();
	void __fastcall ZoomUpdate();
	void __fastcall ZoomUpdate(TMyObjectType Type) {Update(Type);};
	void __fastcall UpdateCoordinatesRect();
	void __fastcall RenderMaps();
	void __fastcall UtilityLayerPaint(TObject *Sender, TBitmap32 *Bmp);
	void __fastcall InfoLayerPaint(TObject *Sender, TBitmap32 *Bmp);
	void __fastcall GetLayers(TCheckBoxTreeView *TreeView);
	void __fastcall SetLayers(TCheckBoxTreeView *TreeView);
	void __fastcall GetMapFoldersContent(TTreeView *TreeView, int ItemImageIndex, AnsiString Extension);
	TMapsContainer* __fastcall GetObject(int Index) {if(Index>=0 && Index<Collection->Count) return (TMapsContainer*)Collection->Items[Index]; else return NULL;}
	void __fastcall Clear();

	Types::TRect __fastcall GetBitmapRect(); //return a size of total Bitmap in px
	TDoublePoint __fastcall ScreenToViewportArea(Types::TPoint P);
	Types::TPoint __fastcall ViewportAreaToScreen(TDoublePoint DP);
	TDoublePoint __fastcall CoordinatesRectToViewportArea(TDoublePoint DP);
	TDoublePoint __fastcall ViewportAreaToCoordinatesRect(TDoublePoint DP);
	TDoublePoint __fastcall ViewportAreaToCoordinatesRect(TDoubleRect pCoordinatesRect, TDoublePoint DP);
	TDoublePoint __fastcall ScreenToCoordinatesRect(Types::TPoint P) {return ViewportAreaToCoordinatesRect(ScreenToViewportArea(P));}
	Types::TPoint __fastcall CoordinatesRectToScreen(TDoublePoint DP) {return ViewportAreaToScreen(CoordinatesRectToViewportArea(DP));}

	bool __fastcall IsContains(TMapType aMapType);
	TMapsContainer* GetFirstObject(TMapType aMapType);

	__property TDoubleRect CoordinatesRect = {read=coordinatesrect};
	__property TImgView32* Image = {read=image};
	__property TBitmap32* Background = {read=BackgroundBmp};
	__property TToolProgressBar *Progress = {read=progress, write=writeProgress};
	__property int Count = {read=readCount};
	__property Types::TRect *RealRect = {read=realrect};
	__property TGPSCoordinatesRect *LatLonRect = {read=latlonrect};
	__property TBitmap32* Utility = {read=UtilityBmp};
	__property TBitmap32** UtilityPtr = {read=readUtilityPtr};
	__property TCoordinateSystem CoordinateSystem = {read=readCoordinateSystem, write=writeCoordinateSystem};
	__property TLockedDoubleRect *ViewportArea = {read=viewportarea};
	__property TMyObject* Owner = {read=owner};
};

//---------------------------------------------------------------------------
#endif
