//---------------------------------------------------------------------------


#pragma hdrstop

#include "CheckBoxTreeView.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

#define TVIS_CHECKED 0x2000
#define TVIS_UNCHECKED 0x1000

UINT GetItemState(HWND hwnd, HTREEITEM hItem)
{
	TVITEM tvi;

	tvi.mask=TVIF_HANDLE | TVIF_STATE;
	tvi.hItem=hItem;
	//SendMessage(hwnd, TVM_GETITEM, NULL, (long)&tvi);
	TreeView_GetItem(hwnd, &tvi);
	return tvi.state;
}

bool __fastcall TCheckBoxTreeView::Checked(TTreeNode *n)
{
	TVITEM tvi;

	if(n!=NULL)
	{
		tvi.state=GetItemState(Handle, n->ItemId);
		if(tvi.state & TVIS_CHECKED) return true;
		else return false;
	}
	else return false;
}

void __fastcall TCheckBoxTreeView::SetChecked(TTreeNode *n)
{
	TVITEM tvi;

	if(n)
	{
		tvi.mask=TVIF_HANDLE | TVIF_STATE;
		tvi.hItem=n->ItemId;
		tvi.state=GetItemState(Handle, tvi.hItem);
		tvi.state=tvi.state & ~TVIS_UNCHECKED;
		tvi.state|=TVIS_CHECKED;
		tvi.stateMask=tvi.stateMask | TVIS_UNCHECKED | TVIS_CHECKED;
		TreeView_SetItem(Handle, &tvi);
	}
}

void __fastcall TCheckBoxTreeView::SetChecking(TTreeNode *n, bool Checked)
{
	TVITEM tvi;

	if(n)
	{
		tvi.mask=TVIF_HANDLE | TVIF_STATE;
		tvi.hItem=n->ItemId;
		tvi.state=GetItemState(Handle, tvi.hItem);
		if(Checked)
		{
			tvi.state=tvi.state & ~TVIS_UNCHECKED;
			tvi.state|=TVIS_CHECKED;
		}
		else
		{
			tvi.state=tvi.state & ~TVIS_CHECKED;
			tvi.state|=TVIS_UNCHECKED;
		}
		tvi.stateMask=tvi.stateMask | TVIS_UNCHECKED | TVIS_CHECKED;
		TreeView_SetItem(Handle, &tvi);
	}
}

void __fastcall TCheckBoxTreeView::SetUnchecked(TTreeNode *n)
{
	TVITEM tvi;

	if(n)
	{
		tvi.mask=TVIF_HANDLE | TVIF_STATE;
		tvi.hItem=n->ItemId;
		tvi.state=GetItemState(Handle, tvi.hItem);
		tvi.state=tvi.state & ~TVIS_CHECKED;
		tvi.state|=TVIS_UNCHECKED;
		tvi.stateMask=tvi.stateMask | TVIS_UNCHECKED | TVIS_CHECKED;
		TreeView_SetItem(Handle, &tvi);
	}
}

void __fastcall TCheckBoxTreeView::ReverseChecking(TTreeNode *n)
{
	TVITEM tvi;

	if(n)
	{
		tvi.mask=TVIF_HANDLE | TVIF_STATE;
		tvi.hItem=n->ItemId;
		tvi.state=GetItemState(Handle, tvi.hItem);
		if(tvi.state & TVIS_UNCHECKED)
		{
			tvi.state=tvi.state & ~TVIS_UNCHECKED;
			tvi.state|=TVIS_CHECKED;
		}
		else
		{
			tvi.state=tvi.state & ~TVIS_CHECKED;
			tvi.state|=TVIS_UNCHECKED;
		}
		tvi.stateMask=tvi.stateMask | TVIS_UNCHECKED | TVIS_CHECKED;
		//tvi.state=(UINT)TVIS_CHECKED;//(UINT)8194;//
		TreeView_SetItem(Handle, &tvi);
	}
}