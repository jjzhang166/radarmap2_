//---------------------------------------------------------------------------

#pragma hdrstop

#include "MyFiles.h"
#include <Psapi.h>

//---------------------------------------------------------------------------

#pragma package(smart_init)

//---------------------------------------------------------------------------
// Process functions
//---------------------------------------------------------------------------
DWORD MyFiles::MyGetProcessId(HANDLE h)
{
/*    static pfnQueryInformationProcess ntQIP = (pfnQueryInformationProcess) GetProcAddress(GetModuleHandle("NTDLL.DLL"),"NtQueryInformationProcess");
	static pfnGetProcID getPId	= (pfnGetProcID) GetProcAddress(GetModuleHandle("KERNEL32.DLL"),"GetProcessId");

	if ((ntQIP == NULL) && (getPId == NULL))
		throw Exception("Can't retrieve process ID : GetProcessID not supported");

	if (getPId != NULL)
		return getPId(h);
	else
	{
		PROCESS_BASIC_INFORMATION info;
		ULONG returnSize;
		ntQIP(h, ProcessBasicInformation, &info, sizeof(info), &returnSize);  // Get basic information.
		return info.UniqueProcessId;
	}*/
	return GetProcessId(h);
}
//---------------------------------------------------------------------------

bool __fastcall MyFiles::FindProcess(AnsiString szExe, PROCESSENTRY32 &pe)
{
	HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	AnsiString found;
	bool res=false;

	pe.dwSize = sizeof(PROCESSENTRY32);
	Process32First(hSnapshot, &pe);
	do
	{
		found=pe.szExeFile;
		//if (!_wcsicmp((wchar_t *)&pe.szExeFile, szExe.c_))
		res=(found.LowerCase()==szExe.LowerCase());
	} while(!res && Process32Next(hSnapshot, &pe));
	return res;
}
//---------------------------------------------------------------------------

int __fastcall MyFiles::ProcessCopiesNumber(AnsiString szExe)
{
	HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	AnsiString found;
	int res=0;
	PROCESSENTRY32 pe;

	pe.dwSize = sizeof(PROCESSENTRY32);
	Process32First(hSnapshot, &pe);
	do
	{
		found=pe.szExeFile;
		//if (!_wcsicmp((wchar_t *)&pe.szExeFile, szExe.c_))
		if(found.LowerCase()==szExe.LowerCase()) res++;
	} while(Process32Next(hSnapshot, &pe));
	return res;
}
//---------------------------------------------------------------------------

bool __fastcall MyFiles::FindAndKillProcess(AnsiString szExe, bool Similar)
{
	HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	AnsiString found;
	bool res=false;
	PROCESSENTRY32 pe;

	pe.dwSize = sizeof(PROCESSENTRY32);
	Process32First(hSnapshot, &pe);
	do
	{
		found=pe.szExeFile;
		//if (!_wcsicmp((wchar_t *)&pe.szExeFile, szExe.c_))
		if(found.LowerCase()==szExe.LowerCase()) res=KillProcess(&pe);
	} while((Similar || !res) && Process32Next(hSnapshot, &pe));
	return res;
}
//---------------------------------------------------------------------------

unsigned long MyFiles::GetProcessSize(HANDLE hProcess)
{
    unsigned long ul=0;
    PROCESS_MEMORY_COUNTERS pmc;

    if(hProcess && GetProcessMemoryInfo(hProcess, &pmc, sizeof(pmc)))
        ul=pmc.WorkingSetSize;
    return ul;
}
//---------------------------------------------------------------------------

unsigned long MyFiles::GetProcessSize(AnsiString szExe)
{
    unsigned long res=0;
    PROCESSENTRY32 pe;
    HANDLE hProcess;

    if(FindProcess(szExe, pe))
    {
        hProcess=OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, false, pe.th32ProcessID);
        if(hProcess)
        {
            try
            {
                res=GetProcessSize(hProcess);
            }
            __finally
            {
                CloseHandle(hProcess);
            }
        }
    }
    return res;
}
//---------------------------------------------------------------------------

unsigned long MyFiles::FindAssociatedProcesses(ULONG pid, TList *List)
{
	/////////////////////////////////////////////////////////////////////////
	// Prepare for NtQuerySystemInformation and NtQueryInformationFile.
	//
	// The functions have no associated import library. You must use the
	// LoadLibrary and GetProcAddress functions to dynamically link to
	// ntdll.dll.
	AnsiString str;
	unsigned long ProcCount=0;
    DWORD id;
	HANDLE hCopy, hProcess;
	HINSTANCE hNtDll=LoadLibrary("ntdll.dll");
    const SYSTEM_INFORMATION_CLASS SystemHandleInformation = (SYSTEM_INFORMATION_CLASS)16;

	//assert(hNtDll != NULL);
	if(hNtDll && List)
	{
		try
		{
			PFN_NTQUERYSYSTEMINFORMATION NtQuerySystemInformation=
				(PFN_NTQUERYSYSTEMINFORMATION)GetProcAddress(hNtDll, "NtQuerySystemInformation");
			PFN_NTQUERYOBJECT NtQueryObject=
				(PFN_NTQUERYOBJECT)GetProcAddress(hNtDll, "NtQueryObject");
			if(NtQuerySystemInformation && NtQueryObject)
			{
				/////////////////////////////////////////////////////////////////////////
				// Get system handle information.
				//
				DWORD nSize = 4096, nReturn;
				PSYSTEM_HANDLE_INFORMATION pSysHandleInfo = (PSYSTEM_HANDLE_INFORMATION)
					HeapAlloc(GetProcessHeap(), 0, nSize);
				// NtQuerySystemInformation does not return the correct required buffer
				// size if the buffer passed is too small. Instead you must call the
				// function while increasing the buffer size until the function no longer
				// returns STATUS_INFO_LENGTH_MISMATCH.
				try
				{
					while (NtQuerySystemInformation(SystemHandleInformation, pSysHandleInfo,
						nSize, &nReturn) == STATUS_INFO_LENGTH_MISMATCH)
					{
						HeapFree(GetProcessHeap(), 0, pSysHandleInfo);
						nSize += 4096;
						pSysHandleInfo = (SYSTEM_HANDLE_INFORMATION*)HeapAlloc(
							GetProcessHeap(), 0, nSize);
					}
					// Get the handle of the target process. The handle will be used to
					// duplicate the file handles in the process.
					hProcess=OpenProcess(PROCESS_DUP_HANDLE | PROCESS_QUERY_INFORMATION, false, pid);
					if(hProcess)
					{
						try
						{
							for(ULONG i = 0; i < pSysHandleInfo->NumberOfHandles; i++)
							{
								PSYSTEM_HANDLE pHandle = &(pSysHandleInfo->Handles[i]);

								if(pHandle->ProcessId==pid && DuplicateHandle(hProcess, (HANDLE)pHandle->Handle,
									GetCurrentProcess(), &hCopy, PROCESS_QUERY_INFORMATION, false, 0) && hCopy)
								{
									try
									{
										nSize=4096;
										PPUBLIC_OBJECT_TYPE_INFORMATION pTypeInfo = (PPUBLIC_OBJECT_TYPE_INFORMATION)
											HeapAlloc(GetProcessHeap(), 0, nSize);
										try
										{
											while(NtQueryObject(hCopy, ObjectTypeInformation, pTypeInfo, nSize, &nReturn) == STATUS_INFO_LENGTH_MISMATCH)
											{
												HeapFree(GetProcessHeap(), 0, pTypeInfo);
												nSize+=4096;
												pTypeInfo=(PPUBLIC_OBJECT_TYPE_INFORMATION)HeapAlloc(GetProcessHeap(), 0, nSize);
											}
											str=pTypeInfo->TypeName.Buffer;
											if(str.Pos("Process")>0)
											{
												id=MyGetProcessId(hCopy);
												if(id>0)
												{
													ProcCount++;
													ProcCount+=FindAssociatedProcesses(id, List);
													DWORD *idp=new DWORD;
													*idp=id;
													List->Add(idp);
												}
											}
										}
										__finally
										{
											HeapFree(GetProcessHeap(), 0, pTypeInfo);
										}
									}
									__finally
									{
										CloseHandle(hCopy);
									}
								}
							}
						}
						__finally
						{
							CloseHandle(hProcess);
						}
					}
				}
				__finally
				{
					HeapFree(GetProcessHeap(), 0, pSysHandleInfo);
				}
			}
		}
		__finally
		{
			FreeLibrary(hNtDll);
		}
	}
	return ProcCount;
}

//---------------------------------------------------------------------------
// TUnsharedFile
//---------------------------------------------------------------------------
__fastcall TUnsharedFile::TUnsharedFile(TMyObject *AOwner, AnsiString AFileName, bool AOpenExisting): TMyObject()
{   owner=AOwner;

	filename=AFileName;
	hndl=INVALID_HANDLE_VALUE;
	opened=false;
	openexisting=AOpenExisting;
	BusyLocker=new TBusyLocker();
	filepointer=0;
	lasterror="";
}
//---------------------------------------------------------------------------

__fastcall TUnsharedFile::~TUnsharedFile()
{
	if(BusyLocker) BusyLocker->UnLock();
	Close();
	if(BusyLocker) delete BusyLocker;
	BusyLocker=NULL;
	if(owner && index>=0)
	{
		bool b=owner->FreeObjects;
		owner->FreeObjects=false;
		owner->RemoveChildFromList(this);
        owner->FreeObjects=b;
	}
}
//---------------------------------------------------------------------------

void __fastcall TUnsharedFile::GetError()
{
    LPVOID lpMsgBuf;

	FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM, NULL,
		GetLastError(), MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
		(LPTSTR) &lpMsgBuf, 0, NULL);
	lasterror=(LPTSTR)lpMsgBuf;
	LocalFree(lpMsgBuf);
}
//---------------------------------------------------------------------------

bool __fastcall TUnsharedFile::Open()
{
	if(!opened)
	{
		if(BusyLocker->SequentLock())
		{
			try
			{
				try
				{
                    DWORD HowCreate;

					if(OpenExisting) HowCreate=OPEN_EXISTING;
                    else HowCreate=OPEN_ALWAYS;
					hndl=CreateFile(filename.c_str(), GENERIC_READ | GENERIC_WRITE, 0, NULL,
						HowCreate,
						//FILE_ATTRIBUTE_NORMAL,
						FILE_FLAG_SEQUENTIAL_SCAN,
						NULL);
					if(hndl==INVALID_HANDLE_VALUE) GetError();
				}
				catch(...) {}
			}
			__finally
			{
				BusyLocker->UnLock();
			}
		}
	}
    opened=(hndl!=INVALID_HANDLE_VALUE);
	return opened;
}
//---------------------------------------------------------------------------

bool __fastcall TUnsharedFile::Close()
{
	if(opened)
	{
		if(BusyLocker->SequentLock())
		{
			try
			{
				try
				{
					CloseHandle(hndl);
					hndl=INVALID_HANDLE_VALUE;
					opened=false;
				}
				catch(...) {}
			}
			__finally
			{
				BusyLocker->UnLock();
			}
		}
	}
	return !opened;
}
//---------------------------------------------------------------------------

unsigned long __fastcall TUnsharedFile::UnlockedRead(void* buf, unsigned long size)
{
	unsigned long res, ul;

	res=0;
	if(opened && hndl!=INVALID_HANDLE_VALUE)
	{
		do
		{
			if(!ReadFile(hndl, (void*)((char*)buf+res), size-res, &ul, NULL))
			{
				GetError();
				break;
            }
			res+=ul;
		} while(res<size && ul>0);
	}
	return res;
}
//---------------------------------------------------------------------------

unsigned long __fastcall TUnsharedFile::Read(void* buf, unsigned long size)
{
	unsigned long res, rd;

	res=0;
	if(opened && hndl!=INVALID_HANDLE_VALUE)
	{
		if(BusyLocker->SequentLock())
		{
			try
			{
				try
				{
					res=UnlockedRead(buf, size);
					filepointer+=res;
				}
				catch(...) {}
			}
			__finally
			{
				BusyLocker->UnLock();
			}
		}
	}
	return res;
}
//---------------------------------------------------------------------------

unsigned long __fastcall TUnsharedFile::Read(void* buf, unsigned long size,
	unsigned long pointer, TFileMovementDirection dir)
{
	unsigned long res, ul, fp;

	res=0;
	if(opened && hndl!=INVALID_HANDLE_VALUE)
	{
		if(BusyLocker->SequentLock())
		{
			try
			{
				try
				{
					ul=GetFileSize(hndl, NULL);
					if(ul>=pointer)
					{
						fp=SetFilePointer(hndl, pointer, NULL, dir);
						if(fp!=INVALID_SET_FILE_POINTER)
						{
							res=UnlockedRead(buf, size);
							filepointer=fp+res;
						}
					}
				}
				catch(...) {}
			}
			__finally
			{
				BusyLocker->UnLock();
			}
		}
	}
	return res;
}
//---------------------------------------------------------------------------

unsigned long __fastcall TUnsharedFile::Write(void* buf, unsigned long size,
	unsigned long pointer, TFileMovementDirection dir, unsigned long &fp)
{
	unsigned long res, ul;

	res=0;
	fp=INVALID_SET_FILE_POINTER;
	if(opened && hndl!=INVALID_HANDLE_VALUE)
	{
		if(BusyLocker->SequentLock())
		{
			try
			{
				try
				{
					fp=SetFilePointer(hndl, pointer, NULL, dir);
					if(fp!=INVALID_SET_FILE_POINTER)
					{
						do
						{
							if(!WriteFile(hndl, (void*)((char*)buf+res), size-res, &ul, NULL))
							{
								GetError();
								break;
                            }
							res+=ul;
						} while(res<size && ul>0);
						fp+=res;
						filepointer=fp;
					}
				}
				catch(...) {}
			}
			__finally
			{
				BusyLocker->UnLock();
			}
		}
	}

	return res;
}
//---------------------------------------------------------------------------

unsigned long __fastcall TUnsharedFile::readFileSize()
{
	unsigned long res;

	res=INVALID_FILE_SIZE;
	if(opened && hndl!=INVALID_HANDLE_VALUE)
	{
		if(BusyLocker->SequentLock())
		{
			try
			{
				try
				{
					res=GetFileSize(hndl, NULL);
					if(res==INVALID_FILE_SIZE) GetError();
				}
				catch(...) {}
			}
			__finally
			{
				BusyLocker->UnLock();
			}
		}
	}

	return res;
}
//---------------------------------------------------------------------------

bool __fastcall TUnsharedFile::FileMemSet(unsigned long pointer, TFileMovementDirection dir, unsigned long size, char ch)
{
	unsigned long ul, fp, w;
	bool res=false;

	res=0;
	fp=INVALID_SET_FILE_POINTER;
	if(opened && hndl!=INVALID_HANDLE_VALUE)
	{
		if(BusyLocker->SequentLock())
		{
			try
			{
				try
				{
					fp=SetFilePointer(hndl, pointer, NULL, dir);
					if(fp!=INVALID_SET_FILE_POINTER)
					{
                        w=0;
						do
						{
							if(!WriteFile(hndl, &ch, size-w, &ul, NULL))
							{
								GetError();
								break;
                            }
							w+=ul;
						} while(w<size && ul>0);
						filepointer=fp+w;
                        res=(w==size);
					}
				}
				catch(...) {}
			}
			__finally
			{
				BusyLocker->UnLock();
			}
		}
	}

	return res;
}
//---------------------------------------------------------------------------

/*unsigned long __fastcall TUnsharedFile::SetFilePointer(unsigned long pointer, TFileMovementDirection dir)
{
    unsigned long fp=INVALID_SET_FILE_POINTER;

	if(opened && hndl!=INVALID_HANDLE_VALUE)
	{
		if(BusyLocker->SequentLock())
		{
			try
			{
				try
				{
					fp=SetFilePointer(hndl, pointer, NULL, dir);
					if(fp!=INVALID_SET_FILE_POINTER) filepointer=fp;
				}
				catch(...) {}
			}
			__finally
			{
				BusyLocker->UnLock();
			}
		}
	}

	return fp;
}*/
//---------------------------------------------------------------------------

bool __fastcall TUnsharedFile::SetEOF(unsigned long pointer, TFileMovementDirection dir)
{
    unsigned long fp;
	bool res=false;

	fp=INVALID_SET_FILE_POINTER;
	if(opened && hndl!=INVALID_HANDLE_VALUE)
	{
		if(BusyLocker->SequentLock())
		{
			try
			{
				try
				{
					fp=SetFilePointer(hndl, pointer, NULL, dir);
					if(fp!=INVALID_SET_FILE_POINTER)
                    {
                        filepointer=fp;
                        res=SetEndOfFile(hndl);
					}
					else GetError();
				}
				catch(...) {}
			}
			__finally
			{
				BusyLocker->UnLock();
			}
		}
	}

	return res;
}
//---------------------------------------------------------------------------

bool __fastcall TUnsharedFile::Delete()
{
    bool res=false;

    if(Close())
	{
        if(BusyLocker->SequentLock())
		{
			try
			{
				try
				{
					res=DeleteFile(filename);
					if(!res) GetError();
				}
				catch(...) {}
			}
			__finally
			{
				BusyLocker->UnLock();
			}
		}
	}
	return res;
}

//---------------------------------------------------------------------------
// TBufferedROFile
//---------------------------------------------------------------------------
__fastcall TBufferedROFile::TBufferedROFile(TMyObject* AOwner,
	AnsiString AFileName, bool AOpenExisting) :	TUnsharedFile(AOwner, AFileName, AOpenExisting)
{
	sizetoread=2*4096;//16384;//
	readedbuf=new char[2*sizetoread+1];
	ptr=readedbuf;
}
//---------------------------------------------------------------------------

__fastcall TBufferedROFile::~TBufferedROFile()
{
	delete[] readedbuf;
}
//---------------------------------------------------------------------------

bool __fastcall TBufferedROFile::Open()
{
	unsigned long res, ul;

	TUnsharedFile::Open();
	filesize=TUnsharedFile::FileSize;
	readedlength=0;
	if(opened && hndl!=INVALID_HANDLE_VALUE)
	{
		if(BusyLocker->SequentLock())
		{
			try
			{
				try
				{
					do
					{
						if(!ReadFile(hndl, (void*)(readedbuf+readedlength), sizetoread-readedlength, &ul, NULL))
						{
							GetError();
							break;
                        }
						readedlength+=ul;
					} while(readedlength<sizetoread && ul>0);
					filepointer+=readedlength;
				}
				catch(...) {}
			}
			__finally
			{
				BusyLocker->UnLock();
			}
		}
	}
	return res;
}
//---------------------------------------------------------------------------

unsigned long __fastcall TBufferedROFile::Read(void* buf, unsigned long size)
{
	unsigned long res, ul, sz;
	char *newbuf;

	res=0;
	if(opened && hndl!=INVALID_HANDLE_VALUE && buf && size>0)
	{
		try
		{
			if(sizetoread<size)
			{
				while(sizetoread<size) sizetoread*=2;
				newbuf=new char[2*sizetoread+1];
				memcpy((void*)newbuf, (void*)ptr, readedlength);
				delete[] readedbuf;
				readedbuf=newbuf;
				ptr=readedbuf;
			}
			/*if(readedlength<size)
			{
				do
				{
					ReadFile(hndl, (void*)(readedbuf+readedlength), size-readedlength, &ul, NULL);
					readedlength+=ul;
				} while(readedlength<size && ul>0);
				res=readedlength;
			}
			else res=size;
			memcpy(buf, (void*)ptr, res);
			readedlength-=res;
			if(readedlength>0) ptr+=res;
			if(readedlength<(sizetoread >> 1) && (filesize==INVALID_FILE_SIZE || (filesize-FilePointer)>readedlength))
			{
				memcpy((void*)readedbuf, (void*)ptr, readedlength);
				sz=readedlength+sizetoread;
				do
				{
					ReadFile(hndl, (void*)(readedbuf+readedlength), sz-readedlength, &ul, NULL);
					readedlength+=ul;
				} while(readedlength<sz && ul>0);
				ptr=readedbuf;
			}*/
			if(readedlength<size) res=readedlength;
			else res=size;
			memcpy(buf, (void*)ptr, res);
			readedlength-=res;
			if(readedlength==0)
			{
				do
				{
					if(!ReadFile(hndl, (void*)(readedbuf+readedlength), sizetoread-readedlength, &ul, NULL))
					{
						GetError();
						break;
                    }
					readedlength+=ul;
				} while(readedlength<sizetoread && ul>0);
				filepointer+=readedlength;
				ptr=readedbuf;
			}
			else ptr+=res;
		}
		catch(...) {}
	}
	return res;
}
//---------------------------------------------------------------------------

unsigned long __fastcall TBufferedROFile::ReadLn(AnsiString *Out)
{
	unsigned long res, ul, sz;
	char *newbuf;
	bool complete=false;

	res=0;
	*Out="";
	if(opened && hndl!=INVALID_HANDLE_VALUE && Out)
	{
		try
		{
			do
			{
				if(readedlength==0)
				{
					do
					{
						if(!ReadFile(hndl, (void*)(readedbuf+readedlength), sizetoread-readedlength, &ul, NULL))
						{
							GetError();
							break;
                        }
						readedlength+=ul;
					} while(readedlength<sizetoread && ul>0);
					filepointer+=readedlength;
					ptr=readedbuf;
				}
				if(readedlength>0 )
				{
					if(*ptr!='\n')
					{
						*Out=*Out+*ptr;
						res++;
					}
					else complete=true;
					ptr++;
					readedlength--;
				}
				else complete=true;
			} while(!complete);
		}
		catch(...) {}
	}
	return res;
}

//---------------------------------------------------------------------------
// TUnsharedFilesList
//---------------------------------------------------------------------------
TUnsharedFile* __fastcall TUnsharedFilesList::FindByFileName(AnsiString filename)
{
	TUnsharedFile *res=NULL;

	for(int i=0; i<Count; i++)
	{
		if(Items[i] && Items[i]->FileName==filename)
		{
			res=Items[i];
			break;
		}
	}
	return res;
}

//---------------------------------------------------------------------------
// Folder functions
//---------------------------------------------------------------------------
void __fastcall MyFiles::EmptyFolder(AnsiString PathName)
{
	WIN32_FIND_DATA FileData;
	HANDLE hSearch;
	AnsiString S;

	hSearch=FindFirstFile((PathName+"\\*").c_str(), &FileData);
	if(hSearch!=INVALID_HANDLE_VALUE)
	{
		do
		{
			S=FileData.cFileName;
			if(S!="." && S!="..")
			{
				if(FileData.dwFileAttributes==FILE_ATTRIBUTE_DIRECTORY)
				{
					EmptyFolder(PathName+"\\"+S);
					RemoveDir(PathName+"\\"+S);
				}
				else DeleteFile(PathName+"\\"+FileData.cFileName);
			}
		} 	while(FindNextFile(hSearch, &FileData));
		FindClose(hSearch);
	}
}
//---------------------------------------------------------------------------

void __fastcall MyFiles::CopyFolderFiles(AnsiString Src, AnsiString Dst)
{
	WIN32_FIND_DATA FileData;
	HANDLE hSearch;
	AnsiString S;

	hSearch=FindFirstFile((Src+"\\*").c_str(), &FileData);
	if(hSearch!=INVALID_HANDLE_VALUE)
	{
		do
		{
			S=FileData.cFileName;
			if(S!="." && S!="..")
			{
				if(FileData.dwFileAttributes==FILE_ATTRIBUTE_DIRECTORY)
				{
					if(!DirectoryExists(Dst+"\\"+S))
					{
						if(!CreateDirectory((Dst+"\\"+S).c_str(), NULL)) throw Exception("Cannot create Directory "+Dst+"\\"+S+"!");
					}
					CopyFolderFiles(Src+"\\"+S, Dst+"\\"+S);
				}
				else CopyFile((Src+"\\"+S).c_str(), (Dst+"\\"+S).c_str(), false);
			}
		} while(FindNextFile(hSearch, &FileData));
		FindClose(hSearch);
	}
}

//---------------------------------------------------------------------------
// TShellItem
//---------------------------------------------------------------------------
bool __fastcall TShellItem::Stop()
{
	bool res;
	DWORD cProcesses, *idp;
	HANDLE hProcess;
	TList *List;

	//if(hWnd) res=DestroyWindow(hWnd);
	//else res=false;
	if(pi.hProcess)
	{
		List=new TList;
		try
		{
			cProcesses=MyFiles::FindAssociatedProcesses(handleid, List);
			if(cProcesses>0)
			{
				for(int i=0; i<List->Count; i++)
				{
					idp=(DWORD*)List->Items[i];
					if(idp)
					{
						hProcess=OpenProcess(PROCESS_DUP_HANDLE | PROCESS_QUERY_INFORMATION | PROCESS_TERMINATE, false, *idp);
						if(hProcess) TerminateProcess(hProcess, -1);
						delete idp;
					}
				}
				List->Clear();
			}
		}
		__finally
		{
			delete List;
		}
		res=TerminateProcess(pi.hProcess, -1);
	}
	if(res)
	{
		ZeroMemory(&pi, sizeof(pi));
		handleid=0;
		hWnd=NULL;
	}
	return res;
}
//---------------------------------------------------------------------------

HWND FoundHWND;
BOOL __stdcall EnumWindowsProc(HWND hwnd, LPARAM lParam)
{
	unsigned long h;
	bool res;

	GetWindowThreadProcessId(hwnd, &h);
	if(h==(unsigned long)lParam)
	{
		FoundHWND=hwnd;
		return false;
	}
	else return true;
}

void __fastcall TShellItem::Run(bool CloseExistsing)
{
#ifdef UNICODE
	LPCWSTR lpCurrentDirectory;
#else
	LPCSTR lpCurrentDirectory;
#endif

	if(cmdline!=NULL && cmdline.Length()>0)
	{
		ZeroMemory(&pi, sizeof(pi));

		if(cmdlinepath!=NULL && cmdlinepath.Length()>0 && DirectoryExists(cmdlinepath))
#ifdef UNICODE
            lpCurrentDirectory=((UnicodeString)cmdlinepath).c_str();
#else
			lpCurrentDirectory=cmdlinepath.c_str();
#endif
		else lpCurrentDirectory=NULL;
		if(!CreateProcess(NULL, cmdline.c_str(), //"\"cmd.exe\" /K ping.exe 192.168.0.1 -t",
			NULL, NULL, false, procflags, NULL, lpCurrentDirectory, &si, &pi))
		{
			ZeroMemory(&pi, sizeof(pi));
		}
		else
		{
			Sleep(200);
			handleid=MyFiles::MyGetProcessId(pi.hProcess);
			if(handleid>0)
			{
				FoundHWND=NULL;
#ifdef prismAllH
				EnumWindows((int (__stdcall *)())EnumWindowsProc, handleid);
#else
				EnumWindows(EnumWindowsProc, handleid);
#endif
				if(FoundHWND) hWnd=FoundHWND;
			}
			if(OwnerForm)
			{
				HWND MainFormHWnd;
#ifdef prismAllH
				MainFormHWnd=FindWindow(((AnsiString)OwnerForm->ClassName()).c_str(), OwnerForm->Caption.c_str());
#else
				MainFormHWnd=FindWindow(((AnsiString)OwnerForm->ClassName()).c_str(), OwnerForm->Caption.t_str());
#endif
				SetForegroundWindow(MainFormHWnd);
			}
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TShellItem::ReSize()
{
	if(!hWnd)
	{
		FoundHWND=NULL;
#ifdef prismAllH
		EnumWindows((int (__stdcall *)())EnumWindowsProc, handleid);
#else
		EnumWindows(EnumWindowsProc, handleid);
#endif
		if(FoundHWND) hWnd=FoundHWND;
	}
	if(hWnd!=NULL)
	{
		MoveWindow(hWnd, si.dwX, si.dwY, si.dwXSize, si.dwYSize, true);
	}
}
//---------------------------------------------------------------------------
