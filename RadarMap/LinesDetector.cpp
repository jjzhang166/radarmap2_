//---------------------------------------------------------------------------

#pragma hdrstop

#include "LinesDetector.h"
#include "LocalMap.h"
#include "Manager.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

//---------------------------------------------------------------------------
// TLinesDetectorThread
//---------------------------------------------------------------------------
__fastcall TLinesDetectorThread::TLinesDetectorThread(TLinesDetector* AOwner, TObject* RadarMapManager, bool CreateSuspended)
	: TMyThread(CreateSuspended)
{
	Owner=AOwner;
	Manager=RadarMapManager;
	LastProcessedPoint=NULL;
	enteredonline=false;
	LastLineColor=clWhite32;
	InsertTargetEvent=CreateEvent(NULL, false, false, NULL);
	EnteredOnLineEvent=CreateEvent(NULL, false, false, NULL);
}
//---------------------------------------------------------------------------

__fastcall TLinesDetectorThread::~TLinesDetectorThread()
{
	CloseHandle(InsertTargetEvent);
	CloseHandle(EnteredOnLineEvent);
}

void __fastcall TLinesDetectorThread::writeEneteredOnLine(bool value)
{
	if(enteredonline) SetEvent(InsertTargetEvent);
	enteredonline=value;
	if(value) SetEvent(EnteredOnLineEvent);
}
//---------------------------------------------------------------------------

bool __fastcall TLinesDetectorThread::CheckPixel(int x, int y, bool CheckOut, TColor32 &cl)
{
	TMyPixel *mp;
	TBitmap32* UtilityDraw;
	TMapPathObject *Path;

	UtilityDraw = *(Owner->UtilityBmp);

	if(UtilityDraw && UtilityDraw->ClipRect.Contains(Types::TPoint(x, y)))
	{
		if(Owner->PixelList)
		{
			mp=new TMyPixel();
			mp->x=x;
			mp->y=y;
			mp->cl=clBlack32;
		}
		cl=UtilityDraw->Pixel[x][y];
		if((unsigned)(cl&0x00FFFFFF)>0)
		{
			if(!EnteredOnLine) //entering the line
			{
				EnteredOnLine=true;
			}
			else //allready entered the line
			{
				if(cl!=LastLineColor) // entering another line
				{
					EnteredOnLine=true; //to process the "write" procedure of __property
				}
			}
			if(Owner->PixelList) mp->cl=clLime32;
		}
		else if(EnteredOnLine && CheckOut) //leaving the line
		{
			EnteredOnLine=false;
		}
		if(Owner->PixelList) Owner->PixelList->Add(mp);
		return true;
	}
	else
	{
		if(EnteredOnLine && CheckOut) //leaving the line
		{
			EnteredOnLine=false;
		}
		cl=clBlack32;
		return false;
	}
}
//---------------------------------------------------------------------------

void __fastcall TLinesDetectorThread::InsertTarget(TGPSCoordinate *From, TDoublePoint from, float x, float y, float dT, float D)
{
	if(EnteredOnLinePixel.Valid)
	{
		//if(EnteredOnLinePixel.cl&0x00FFFFFF>0)
		//if(EnteredOnLinePixel.cl!=clBlack32)
		if((unsigned)(EnteredOnLinePixel.cl&0x00FFFFFF)>0)
		{
			try
			{
				TInsertingVars iv;
				bool b=false;
				float t;

				t=(double)From->Trace->Index+dT*sqrt((x-from.X)*(x-from.X)+(y-from.Y)*(y-from.Y))/(float)D;
				t=(EnteredOnLinePixel.TraceIndex+t)/2.;
				iv.Path=(TObject*)From->Parent;
				iv.TraceIndex=(int)(t+0.5);
				iv.Color=EnteredOnLinePixel.cl;
				EnteredOnLinePixel.Valid=false;
				while(!Terminated && !b)
				{
					if(WaitForSingleObject(Owner->ReadyForInsertingEvent, 0)==WAIT_OBJECT_0)
					{
						Owner->InsertingVars=iv;
						b=true;
					}
					MySleep(10);
					CheckAskForSuspend();
				}
			}
			catch(Exception &e) {}
		}
		else
		{
			EnteredOnLinePixel.Valid=false;
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TLinesDetectorThread::DetectLines(TGPSCoordinate *From, TGPSCoordinate *To)
{
	TDoublePoint from, to; //in pixels;
	int dX, dY, i, j;
	float d, s, x, y, xs, ys, D, dT, t;
	float xx, yy, tt;
	TColor32 cl;
	TBitmap32* UtilityDraw;
	TCoordinateSystem cs;

	if(Manager && ((TRadarMapManager*)Manager)->Settings)
	{
		if(((TRadarMapManager*)Manager)->MapObjects && ((TRadarMapManager*)Manager)->MapObjects->Container)
			cs=((TRadarMapManager*)Manager)->MapObjects->Container->CoordinateSystem;
		else cs=((TRadarMapManager*)Manager)->Settings->DefaultCoordinateSystem;
	}
	else cs=csLatLon;

	UtilityDraw = *(Owner->UtilityBmp);

	if(From==To || (From->Trace==NULL && To->Trace==NULL) || UtilityDraw==NULL) return;
	if(From->Trace==NULL) From->Trace=To->Trace;
	else if(To->Trace==NULL) To->Trace=From->Trace;

	if(Owner->LatLonRect->Width()!=0 && Owner->LatLonRect->Height()!=0)
	{
		if(cs==csLatLon)
		{
			from.X=From->Longitude-Owner->LatLonRect->LeftTop->Longitude;
			from.Y=Owner->LatLonRect->LeftTop->Latitude-From->Latitude;
			to.X=To->Longitude-Owner->LatLonRect->LeftTop->Longitude;
			to.Y=Owner->LatLonRect->LeftTop->Latitude-To->Latitude;
		}
		else
		{
			from.X=From->GetX(cs)-Owner->LatLonRect->LeftTop->GetX(cs);
			from.Y=Owner->LatLonRect->LeftTop->GetY(cs)-From->GetY(cs);
			to.X=To->GetX(cs)-Owner->LatLonRect->LeftTop->GetX(cs);
			to.Y=Owner->LatLonRect->LeftTop->GetY(cs)-To->GetY(cs);
		}
		/*switch(cs)
		{
			case csDutch:
				from.X=From->RdX-Owner->LatLonRect->LeftTop->RdX;
				from.Y=Owner->LatLonRect->LeftTop->RdY-From->RdY;
				to.X=To->RdX-Owner->LatLonRect->LeftTop->RdX;
				to.Y=Owner->LatLonRect->LeftTop->RdY-To->RdY;
				break;
			case csBeLambert08:
			case csBeLambert72:
				from.X=From->BeX-Owner->LatLonRect->LeftTop->BeX;
				from.Y=Owner->LatLonRect->LeftTop->BeY-From->BeY;
				to.X=To->BeX-Owner->LatLonRect->LeftTop->BeX;
				to.Y=Owner->LatLonRect->LeftTop->BeY-To->BeY;
				break;
			case csUTM:
				from.X=From->UtmX-Owner->LatLonRect->LeftTop->UtmX;
				from.Y=Owner->LatLonRect->LeftTop->UtmY-From->UtmY;
				to.X=To->UtmX-Owner->LatLonRect->LeftTop->UtmX;
				to.Y=Owner->LatLonRect->LeftTop->UtmY-To->UtmY;
				break;
			case csLKS:
				from.X=From->LksX-Owner->LatLonRect->LeftTop->LksX;
				from.Y=Owner->LatLonRect->LeftTop->LksY-From->LksY;
				to.X=To->LksX-Owner->LatLonRect->LeftTop->LksX;
				to.Y=Owner->LatLonRect->LeftTop->LksY-To->LksY;
				break;
			case csLatLon:
			default:
				from.X=From->Longitude-Owner->LatLonRect->LeftTop->Longitude;
				from.Y=Owner->LatLonRect->LeftTop->Latitude-From->Latitude;
				to.X=To->Longitude-Owner->LatLonRect->LeftTop->Longitude;
				to.Y=Owner->LatLonRect->LeftTop->Latitude-To->Latitude;
				break;
		}*/
		/*from.X=(float)UtilityDraw->Width*(From->Longitude-Owner->LatLonRect->LeftTop->Longitude)/Owner->LatLonRect->Width();
		from.Y=(float)UtilityDraw->Height*(Owner->LatLonRect->LeftTop->Latitude-From->Latitude)/Owner->LatLonRect->Height();
		to.X=(float)UtilityDraw->Width*(To->Longitude-Owner->LatLonRect->LeftTop->Longitude)/Owner->LatLonRect->Width();
		to.Y=(float)UtilityDraw->Height*(Owner->LatLonRect->LeftTop->Latitude-To->Latitude)/Owner->LatLonRect->Height();*/
		from.X*=(double)UtilityDraw->Width/Owner->LatLonRect->Width(cs);
		from.Y*=(double)UtilityDraw->Height/Owner->LatLonRect->Height(cs);
		to.X*=(double)UtilityDraw->Width/Owner->LatLonRect->Width(cs);
		to.Y*=(double)UtilityDraw->Height/Owner->LatLonRect->Height(cs);

		dX=(int)to.X-(int)from.X;
		dY=(int)to.Y-(int)from.Y;
		dT=To->Trace->Index-From->Trace->Index;
		if(dX!=0) xs=abs(dX)/dX;
		else xs=1;
		if(dY!=0) ys=abs(dY)/dY;
		else ys=1;
		dX+=xs;
		dY+=ys;

		try
		{
			D=sqrt((double)(dX*dX+dY*dY));
			d=fabs((float)dY/(float)dX);
			s=0;
			x=(int)from.X;
			y=(int)from.Y;
			xx=x;
			yy=y;
			for(i=0; i<abs(dX); i++)
			{
				s+=d;
				for(j=0; (j<(int)s) || (j==0); j++)
				{
					CheckPixel((int)x, (int)y, true, cl);
					if(WaitForSingleObject(InsertTargetEvent, 0)==WAIT_OBJECT_0 && EnteredOnLinePixel.Valid)
					{
						InsertTarget(From, from, x, y, dT, D);
					}
					if(WaitForSingleObject(EnteredOnLineEvent, 0)==WAIT_OBJECT_0)
					{
						EnteredOnLinePixel.x=x;
						EnteredOnLinePixel.y=y;
						EnteredOnLinePixel.cl=cl;
						EnteredOnLinePixel.TraceIndex=From->Trace->Index+dT*sqrt((x-from.X)*(x-from.X)+(y-from.Y)*(y-from.Y))/(float)D;
						EnteredOnLinePixel.Valid=true;
					}
					else
					{
						//!!!!!!!!!!! Are neighbors Suspicious? !!!!!!!!!!!!!!!!!
						if(UtilityDraw && UtilityDraw->ClipRect.Contains(Types::TPoint(x, y)) &&
							x!=xx && y!=yy && UtilityDraw->Pixel[xx][y]!=LastLineColor && UtilityDraw->Pixel[x][yy]==UtilityDraw->Pixel[xx][y])
						{
							if((unsigned)(cl&0x00FFFFFF)>0) InsertTarget(From, from, x, y, dT, D);
							if((unsigned)(UtilityDraw->Pixel[xx][y]&0x00FFFFFF)>0) // crossed the thin line
							{
								EnteredOnLinePixel.x=xx;
								EnteredOnLinePixel.y=yy;
								EnteredOnLinePixel.cl=UtilityDraw->Pixel[xx][y];
								EnteredOnLinePixel.TraceIndex=tt;
								EnteredOnLinePixel.Valid=true;
								InsertTarget(From, from, x, y, dT, D);
							}
							if(cl>0) //stayed on the same line or crosed the thin empty space
							{
								enteredonline=true;
								EnteredOnLinePixel.x=x;
								EnteredOnLinePixel.y=y;
								EnteredOnLinePixel.cl=cl;
								EnteredOnLinePixel.TraceIndex=From->Trace->Index+dT*sqrt((x-from.X)*(x-from.X)+(y-from.Y)*(y-from.Y))/(float)D;
								EnteredOnLinePixel.Valid=true;
							}
						}
					}
					LastLineColor=cl;
					yy=y;
					tt=From->Trace->Index+dT*sqrt((x-from.X)*(x-from.X)+(y-from.Y)*(y-from.Y))/(float)D;
					if(d>1)	y+=ys;
					else y+=d*(float)ys;
				}
				xx=x;
				x+=xs;
				s-=(int)s;
			}
		}
		catch(...) {}
	}
}
//---------------------------------------------------------------------------

void __fastcall TLinesDetectorThread::Execute()
{
	TMapPathObject *Path;
	TGPSCoordinate* AddedPoint;

	SetStarted();
	do
	{
		try
		{
			if(Owner && Owner->AddGPSPointEvent && WaitForSingleObject(Owner->AddGPSPointEvent, 10)==WAIT_OBJECT_0)
			{
				AddedPoint=Owner->AddedPoint;
				if(AddedPoint!=NULL && AddedPoint->Parent!=NULL && AddedPoint!=LastProcessedPoint)
				{
					Path=(TMapPathObject*)AddedPoint->Parent;
					if(LastProcessedPoint==NULL)
					{
						if(AddedPoint!=Path->FirstPoint)
						{
							LastProcessedPoint=Path->FirstPoint;
							DetectLines(LastProcessedPoint, AddedPoint);
						}
					}
					else
					{
						if(AddedPoint->Parent!=LastProcessedPoint->Parent)
						{
							if(LastProcessedPoint->Parent!=NULL)
								DetectLines(LastProcessedPoint, ((TMapPathObject*)LastProcessedPoint->Parent)->LastPoint);
							EnteredOnLine=false;
							ResetEvent(InsertTargetEvent);
							if(AddedPoint!=Path->FirstPoint)
							{
								LastProcessedPoint=Path->FirstPoint;
								DetectLines(LastProcessedPoint, AddedPoint);
							}
						}
						else DetectLines(LastProcessedPoint, AddedPoint);
					}
					LastProcessedPoint=AddedPoint;
				}
			}
		}
		catch(Exception &e)
		{
			if(LastProcessedPoint && LastProcessedPoint->Parent)
			{
				try
				{
					LastProcessedPoint=((TMapPathObject*)LastProcessedPoint->Parent)->LastPoint;
				}
				catch(Exception &e)
				{
					LastProcessedPoint=NULL;
				}
			}
			else LastProcessedPoint=NULL;
		}
		CheckAskForSuspend();
	} while(!Terminated);
	SetIsTerminated();
}

//---------------------------------------------------------------------------
// TLinesDetector
//---------------------------------------------------------------------------
__fastcall TLinesDetector::TLinesDetector(TImgView32 *image, TBitmap32** UBmp, TGPSCoordinatesRect* cRect,
	TObject* RadarMapManager)
{
	utilitybmp=UBmp;
	latlonrect=cRect;
	Manager=RadarMapManager;
	addgpspointevent=CreateEvent(NULL, false, false, NULL);
	readyforinsertingevent=CreateEvent(NULL, true, true, NULL);
	SetEvent(readyforinsertingevent);
	waitwhileinserting=false;

	if(((TRadarMapManager*)RadarMapManager)->Settings->LinesDetectorDebug && image!=NULL)
	{
		Image=image;
		pixellist=new TList;
		UtilityLayer = new TBitmapLayer(image->Layers);
		UtilityLayer->OnPaint = &UtilityLayerPaint;
		UtilityLayer->Bitmap->DrawMode = dmBlend;
		UtilityLayer->BringToFront();
		if(UtilityLayer->Index>0) UtilityLayer->Index--;
	}
	else
	{
		pixellist=NULL;
		UtilityLayer = NULL;
	}
	detector_thrd=new TLinesDetectorThread(this, Manager, false);
}
//---------------------------------------------------------------------------

__fastcall TLinesDetector::~TLinesDetector()
{
	TerminateThreads();
	if(PixelList!=NULL)
	{
		for(int i=0; i<PixelList->Count; i++) delete (TMyPixel*)PixelList->Items[i];
		PixelList->Clear();
		delete PixelList;
	}
	if(UtilityLayer!=NULL)
	{
		if(Image && Image->Layers) Image->Layers->Delete(UtilityLayer->Index);
		else delete UtilityLayer;
	}
	CloseHandle(addgpspointevent);
	CloseHandle(readyforinsertingevent);
}
//---------------------------------------------------------------------------

void __fastcall TLinesDetector::TerminateThreads(void)
{
	detector_thrd->AskForTerminate();
	WaitOthers();
	if(detector_thrd->ForceTerminate()) delete detector_thrd;
	detector_thrd=NULL;
}
//---------------------------------------------------------------------------

void __fastcall TLinesDetector::writeInsertingVars(TInsertingVars value)
{
	if(WaitForSingleObject(readyforinsertingevent, 0)==WAIT_OBJECT_0)
	{
		ResetEvent(readyforinsertingevent);
		InsertDetectedTarget(value.Path, value.TraceIndex, value.Color);
		SetEvent(readyforinsertingevent);
	}
}
//---------------------------------------------------------------------------

void __fastcall TLinesDetector::UtilityLayerPaint(TObject *Sender, TBitmap32 *Bmp)
{
	TMyPixel *mp;
	Types::TRect bmp, vpt;
	int i;
	float x, y;

	try
	{
		bmp=Image->GetBitmapRect();
		vpt=Image->GetViewportRect();

		Bmp->BeginUpdate();
		try
		{
			for(i=0; i<PixelList->Count; i++)
			{
				mp=(TMyPixel*)PixelList->Items[i];
				x=(float)bmp.Width()*(float)mp->x/(float)Image->Bitmap->Width;
				y=(float)bmp.Height()*(float)mp->y/(float)Image->Bitmap->Height;
				x+=bmp.left;
				y+=bmp.top;
				if(x>=1 && x<Bmp->Width-1 && y>0 && y<Bmp->Height-1)
				{
					Bmp->SetPixelT(x-1, y-1, mp->cl);Bmp->SetPixelT(x, y-1, mp->cl);Bmp->SetPixelT(x+1, y-1, mp->cl);
					Bmp->SetPixelT(x-1, y, mp->cl);  Bmp->SetPixelT(x, y, mp->cl);  Bmp->SetPixelT(x+1, y, mp->cl);
					Bmp->SetPixelT(x-1, y+1, mp->cl);Bmp->SetPixelT(x, y+1, mp->cl);Bmp->SetPixelT(x+1, y+1, mp->cl);
				}
			}
		}
		__finally
		{
			Bmp->EndUpdate();
		}
	}
	catch(Exception &e) {}
}
//---------------------------------------------------------------------------

void __fastcall TLinesDetector::InsertDetectedTarget(TObject *p, int TraceIndex, TColor32 color)
{
	TMapPathObject *Path;

	Path=(TMapPathObject*)p;
	if(Path!=NULL)
	{
		//Path->GetOutputView->Profile
		TProfileLabel *pl;
		TRadarMapManager* RadarMapManager;

		RadarMapManager=(TRadarMapManager*)Manager;

		if(RadarMapManager && RadarMapManager->MapObjects && RadarMapManager->ObjectsContainer && Path->OutputView)
		{
			pl=new TProfileLabel(RadarMapManager->ObjectsContainer, RadarMapManager->ObjectsContainer->Image->GetViewportRect(),
				Path->OutputView, TraceIndex, color, RadarMapManager);
			new TMapLabel(RadarMapManager->MapObjects, RadarMapManager->MapObjects->Image->GetViewportRect(),
				pl, RadarMapManager->MapObjects->Container->LatLonRect, Path, RadarMapManager);
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TLinesDetector::Rebuild(void)
{
    //??????????????????????????????????????
}
