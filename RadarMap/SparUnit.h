//---------------------------------------------------------------------------

#ifndef SparUnitH
#define SparUnitH
//---------------------------------------------------------------------------
#include "SerialComm.h"
#include "Profile.h"
#include "GPSCoordinate.h"

#define CollectSparData
#undef CollectSparData

#define UseCollectedSparData
#undef UseCollectedSparData

class TPipeThread : public TCustomSerialThread
{
private:
	long delay;
	long Timer;
	long __fastcall readTickCounter(void) {return Gr32_system::GetTickCount();}
protected:
	void __fastcall ExecuteLoopBody();
	__property long TickCounter = {read=readTickCounter};
public:
	__fastcall TPipeThread(TCustomSerialClient *AOwner, TObject *AManager) : TCustomSerialThread(AOwner, AManager) {delay=250; Timer=0;}
	__fastcall ~TPipeThread() {}

	__property long Delay = {read=delay, write=delay};
};

class TSparUnit;

class TSparParserThread : public TStartStopThread
{
private:
	TSparUnit* Owner;
	TObject *Manager;
	Spar* VisualData;
#ifdef CollectSparData
	HANDLE TempFile;
#endif
	//TTrace* ToPtr;
	void __fastcall VisualizeSparData();
    void __fastcall SparInterpolateTraces(TProfile* Prof, TTrace *FirstPtr, bool Backward);
protected:
	void __fastcall ExecuteLoopBody();
	bool __fastcall CheckSparData(Spar* Data);
public:
	__fastcall TSparParserThread(TSparUnit *AOwner, TObject *AManager);
	__fastcall ~TSparParserThread();
};

class TSparUnit : public TCustomSerialClient
{
private:
	TPipeThread *PipeThrd;
	TSparParserThread *ParserThrd;
	void __fastcall TerminateThreads();
	TSparData *spardata;
	bool activejob;
	HANDLE FieldSensViewProcess, FieldSensViewThread;
protected:
	long __fastcall readDelay() {return PipeThrd->Delay;}
	void __fastcall writeDelay(long value) {PipeThrd->Delay=value;}
public:
	__fastcall TSparUnit(TObject* AManager, AnsiString SparShell);
	__fastcall ~TSparUnit();

	void __fastcall Connect() {TCustomSerialClient::Connect();}
	void __fastcall Connect(AnsiString AHost);
	void __fastcall Disconnect();
	void __fastcall StartJob();
	void __fastcall StopJob();

	__property TSparData *SparData = {read=spardata};
	__property bool ActiveJob = {read = activejob};
	__property long Delay = {read=readDelay, write=writeDelay}; // in miliseconds
};

#endif
