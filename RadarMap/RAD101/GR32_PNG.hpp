﻿// CodeGear C++Builder
// Copyright (c) 1995, 2016 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'GR32_PNG.pas' rev: 31.00 (Windows)

#ifndef Gr32_pngHPP
#define Gr32_pngHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>
#include <SysInit.hpp>
#include <System.SysUtils.hpp>
#include <Winapi.Windows.hpp>
#include <System.Classes.hpp>
#include <GR32.hpp>
#include <Vcl.Graphics.hpp>

//-- user supplied -----------------------------------------------------------

namespace Gr32_png
{
//-- forward type declarations -----------------------------------------------
//-- type declarations -------------------------------------------------------
//-- var, const, procedure ---------------------------------------------------
extern DELPHI_PACKAGE void __fastcall LoadPNGintoBitmap32(Gr32::TBitmap32* DstBitmap, System::Classes::TStream* SrcStream, /* out */ bool &AlphaChannelUsed)/* overload */;
extern DELPHI_PACKAGE void __fastcall LoadPNGintoBitmap32(Gr32::TBitmap32* DstBitmap, System::UnicodeString Filename, /* out */ bool &AlphaChannelUsed)/* overload */;
}	/* namespace Gr32_png */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_GR32_PNG)
using namespace Gr32_png;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// Gr32_pngHPP
