// CodeGear C++Builder
// Copyright (c) 1995, 2016 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'LibXmlParser.pas' rev: 31.00 (Windows)

#ifndef LibxmlparserHPP
#define LibxmlparserHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>
#include <SysInit.hpp>
#include <System.SysUtils.hpp>
#include <System.Classes.hpp>
#include <System.Contnrs.hpp>
#include <System.Math.hpp>
#include <System.AnsiStrings.hpp>

//-- user supplied -----------------------------------------------------------

namespace Libxmlparser
{
//-- forward type declarations -----------------------------------------------
struct TDtdElementRec;
class DELPHICLASS TXmlParser;
class DELPHICLASS TNvpNode;
class DELPHICLASS TNvpList;
class DELPHICLASS TAttr;
class DELPHICLASS TAttrList;
class DELPHICLASS TEntityStack;
class DELPHICLASS TAttrDef;
class DELPHICLASS TElemDef;
class DELPHICLASS TElemList;
class DELPHICLASS TEntityDef;
class DELPHICLASS TNotationDef;
class DELPHICLASS TCustomXmlScanner;
//-- type declarations -------------------------------------------------------
enum DECLSPEC_DENUM TPartType : unsigned char { ptNone, ptXmlProlog, ptComment, ptPI, ptDtdc, ptStartTag, ptEmptyTag, ptEndTag, ptContent, ptCData };

enum DECLSPEC_DENUM TDtdElemType : unsigned char { deElement, deAttList, deEntity, deNotation, dePI, deComment, deError };

struct DECLSPEC_DRECORD TDtdElementRec
{
public:
	char *Start;
	char *Final;
	
public:
	TDtdElemType ElementType;
	union
	{
		struct 
		{
			char *Pos;
		};
		struct 
		{
			char *Target;
			char *Content;
			TAttrList* AttrList;
		};
		struct 
		{
			TNotationDef* NotationDef;
		};
		struct 
		{
			TEntityDef* EntityDef;
		};
		struct 
		{
			TElemDef* ElemDef;
		};
		
	};
};


#pragma pack(push,4)
class PASCALIMPLEMENTATION TXmlParser : public System::TObject
{
	typedef System::TObject inherited;
	
protected:
	char *FBuffer;
	int FBufferSize;
	System::UnicodeString FSource;
	System::AnsiString FXmlVersion;
	System::AnsiString FEncoding;
	bool FStandalone;
	System::AnsiString FRootName;
	char *FDtdcFinal;
	bool FNormalize;
	TEntityStack* EntityStack;
	System::AnsiString FCurEncoding;
	void __fastcall AnalyzeProlog(void);
	void __fastcall AnalyzeComment(char * Start, char * &Final);
	void __fastcall AnalyzePI(char * Start, char * &Final);
	void __fastcall AnalyzeDtdc(void);
	void __fastcall AnalyzeDtdElements(char * Start, char * &Final);
	void __fastcall AnalyzeTag(void);
	void __fastcall AnalyzeCData(void);
	void __fastcall AnalyzeText(bool &IsDone);
	void __fastcall AnalyzeElementDecl(char * Start, char * &Final);
	void __fastcall AnalyzeAttListDecl(char * Start, char * &Final);
	void __fastcall AnalyzeEntityDecl(char * Start, char * &Final);
	void __fastcall AnalyzeNotationDecl(char * Start, char * &Final);
	void __fastcall PushPE(char * &Start);
	void __fastcall ReplaceCharacterEntities(System::AnsiString &Str);
	void __fastcall ReplaceParameterEntities(System::AnsiString &Str);
	char * __fastcall GetDocBuffer(void);
	
public:
	__property System::AnsiString XmlVersion = {read=FXmlVersion};
	__property System::AnsiString Encoding = {read=FEncoding};
	__property bool Standalone = {read=FStandalone, nodefault};
	__property System::AnsiString RootName = {read=FRootName};
	__property bool Normalize = {read=FNormalize, write=FNormalize, nodefault};
	__property System::UnicodeString Source = {read=FSource};
	__property char * DocBuffer = {read=GetDocBuffer};
	TElemList* Elements;
	TNvpList* Entities;
	TNvpList* ParEntities;
	TNvpList* Notations;
	__fastcall TXmlParser(void);
	__fastcall virtual ~TXmlParser(void);
	bool __fastcall LoadFromFile(System::UnicodeString Filename, int FileMode = 0x40);
	bool __fastcall LoadFromBuffer(char * Buffer);
	void __fastcall SetBuffer(char * Buffer);
	void __fastcall Clear(void);
	TPartType CurPartType;
	System::AnsiString CurName;
	System::AnsiString CurContent;
	char *CurStart;
	char *CurFinal;
	TAttrList* CurAttr;
	__property System::AnsiString CurEncoding = {read=FCurEncoding};
	void __fastcall StartScan(void);
	bool __fastcall Scan(void);
	virtual TXmlParser* __fastcall LoadExternalEntity(System::AnsiString SystemId, System::AnsiString PublicId, System::AnsiString Notation);
	virtual System::AnsiString __fastcall TranslateEncoding(const System::AnsiString Source);
	virtual System::AnsiString __fastcall TranslateCharacter(const int UnicodeValue, const System::AnsiString UnknownChar = "�");
	virtual void __fastcall DtdElementFound(const TDtdElementRec &DtdElementRec);
};

#pragma pack(pop)

enum DECLSPEC_DENUM TValueType : unsigned char { vtNormal, vtImplied, vtFixed, vtDefault };

enum DECLSPEC_DENUM TAttrDefault : unsigned char { adDefault, adRequired, adImplied, adFixed };

enum DECLSPEC_DENUM TAttrType : unsigned char { atUnknown, atCData, atID, atIdRef, atIdRefs, atEntity, atEntities, atNmToken, atNmTokens, atNotation, atEnumeration };

enum DECLSPEC_DENUM TElemType : unsigned char { etEmpty, etAny, etChildren, etMixed };

;

#pragma pack(push,4)
class PASCALIMPLEMENTATION TNvpNode : public System::TObject
{
	typedef System::TObject inherited;
	
public:
	System::AnsiString Name;
	System::AnsiString Value;
	__fastcall TNvpNode(System::AnsiString TheName, System::AnsiString TheValue);
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TNvpNode(void) { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TNvpList : public System::Contnrs::TObjectList
{
	typedef System::Contnrs::TObjectList inherited;
	
public:
	HIDESBASE void __fastcall Add(TNvpNode* Node);
	TNvpNode* __fastcall Node(System::AnsiString Name)/* overload */;
	TNvpNode* __fastcall Node(int Index)/* overload */;
	System::AnsiString __fastcall Value(System::AnsiString Name)/* overload */;
	System::AnsiString __fastcall Value(int Index)/* overload */;
	System::AnsiString __fastcall Name(int Index);
public:
	/* TObjectList.Create */ inline __fastcall TNvpList(void)/* overload */ : System::Contnrs::TObjectList() { }
	/* TObjectList.Create */ inline __fastcall TNvpList(bool AOwnsObjects)/* overload */ : System::Contnrs::TObjectList(AOwnsObjects) { }
	
public:
	/* TList.Destroy */ inline __fastcall virtual ~TNvpList(void) { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TAttr : public TNvpNode
{
	typedef TNvpNode inherited;
	
public:
	TValueType ValueType;
	TAttrType AttrType;
public:
	/* TNvpNode.Create */ inline __fastcall TAttr(System::AnsiString TheName, System::AnsiString TheValue) : TNvpNode(TheName, TheValue) { }
	
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TAttr(void) { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TAttrList : public TNvpList
{
	typedef TNvpList inherited;
	
public:
	void __fastcall Analyze(char * Start, char * &Final);
public:
	/* TObjectList.Create */ inline __fastcall TAttrList(void)/* overload */ : TNvpList() { }
	/* TObjectList.Create */ inline __fastcall TAttrList(bool AOwnsObjects)/* overload */ : TNvpList(AOwnsObjects) { }
	
public:
	/* TList.Destroy */ inline __fastcall virtual ~TAttrList(void) { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TEntityStack : public System::Contnrs::TObjectList
{
	typedef System::Contnrs::TObjectList inherited;
	
protected:
	TXmlParser* Owner;
	
public:
	__fastcall TEntityStack(TXmlParser* TheOwner);
	void __fastcall Push(char * LastPos)/* overload */;
	void __fastcall Push(System::TObject* Instance, char * LastPos)/* overload */;
	char * __fastcall Pop(void);
public:
	/* TList.Destroy */ inline __fastcall virtual ~TEntityStack(void) { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TAttrDef : public TNvpNode
{
	typedef TNvpNode inherited;
	
public:
	System::AnsiString TypeDef;
	System::AnsiString Notations;
	TAttrType AttrType;
	TAttrDefault DefaultType;
public:
	/* TNvpNode.Create */ inline __fastcall TAttrDef(System::AnsiString TheName, System::AnsiString TheValue) : TNvpNode(TheName, TheValue) { }
	
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TAttrDef(void) { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TElemDef : public TNvpList
{
	typedef TNvpList inherited;
	
public:
	System::AnsiString Name;
	TElemType ElemType;
	System::AnsiString Definition;
public:
	/* TObjectList.Create */ inline __fastcall TElemDef(void)/* overload */ : TNvpList() { }
	/* TObjectList.Create */ inline __fastcall TElemDef(bool AOwnsObjects)/* overload */ : TNvpList(AOwnsObjects) { }
	
public:
	/* TList.Destroy */ inline __fastcall virtual ~TElemDef(void) { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TElemList : public System::Contnrs::TObjectList
{
	typedef System::Contnrs::TObjectList inherited;
	
public:
	TElemDef* __fastcall Node(System::AnsiString Name);
	HIDESBASE void __fastcall Add(TElemDef* Node);
public:
	/* TObjectList.Create */ inline __fastcall TElemList(void)/* overload */ : System::Contnrs::TObjectList() { }
	/* TObjectList.Create */ inline __fastcall TElemList(bool AOwnsObjects)/* overload */ : System::Contnrs::TObjectList(AOwnsObjects) { }
	
public:
	/* TList.Destroy */ inline __fastcall virtual ~TElemList(void) { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TEntityDef : public TNvpNode
{
	typedef TNvpNode inherited;
	
public:
	System::AnsiString SystemId;
	System::AnsiString PublicId;
	System::AnsiString NotationName;
public:
	/* TNvpNode.Create */ inline __fastcall TEntityDef(System::AnsiString TheName, System::AnsiString TheValue) : TNvpNode(TheName, TheValue) { }
	
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TEntityDef(void) { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TNotationDef : public TNvpNode
{
	typedef TNvpNode inherited;
	
public:
	System::AnsiString PublicId;
public:
	/* TNvpNode.Create */ inline __fastcall TNotationDef(System::AnsiString TheName, System::AnsiString TheValue) : TNvpNode(TheName, TheValue) { }
	
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TNotationDef(void) { }
	
};

#pragma pack(pop)

typedef System::Set<char, _DELPHI_SET_CHAR(0), _DELPHI_SET_CHAR(255)> TCharset;

typedef System::StaticArray<System::AnsiString, 10> Libxmlparser__21;

typedef System::StaticArray<System::AnsiString, 4> Libxmlparser__31;

typedef System::StaticArray<System::AnsiString, 4> Libxmlparser__41;

typedef System::StaticArray<System::AnsiString, 4> Libxmlparser__51;

typedef System::StaticArray<System::AnsiString, 11> Libxmlparser__61;

typedef void __fastcall (__closure *TXmlPrologEvent)(System::TObject* Sender, System::UnicodeString XmlVersion, System::UnicodeString Encoding, bool Standalone);

typedef void __fastcall (__closure *TCommentEvent)(System::TObject* Sender, System::UnicodeString Comment);

typedef void __fastcall (__closure *TPIEvent)(System::TObject* Sender, System::UnicodeString Target, System::UnicodeString Content, TAttrList* Attributes);

typedef void __fastcall (__closure *TDtdEvent)(System::TObject* Sender, System::UnicodeString RootElementName);

typedef void __fastcall (__closure *TStartTagEvent)(System::TObject* Sender, System::UnicodeString TagName, TAttrList* Attributes);

typedef void __fastcall (__closure *TEndTagEvent)(System::TObject* Sender, System::UnicodeString TagName);

typedef void __fastcall (__closure *TContentEvent)(System::TObject* Sender, System::UnicodeString Content);

typedef void __fastcall (__closure *TElementEvent)(System::TObject* Sender, TElemDef* ElemDef);

typedef void __fastcall (__closure *TEntityEvent)(System::TObject* Sender, TEntityDef* EntityDef);

typedef void __fastcall (__closure *TNotationEvent)(System::TObject* Sender, TNotationDef* NotationDef);

typedef void __fastcall (__closure *TErrorEvent)(System::TObject* Sender, char * ErrorPos);

typedef void __fastcall (__closure *TExternalEvent)(System::TObject* Sender, System::UnicodeString SystemId, System::UnicodeString PublicId, System::UnicodeString NotationId, TXmlParser* &Result);

typedef System::UnicodeString __fastcall (__closure *TEncodingEvent)(System::TObject* Sender, System::UnicodeString CurrentEncoding, System::UnicodeString Source);

typedef System::UnicodeString __fastcall (__closure *TEncodeCharEvent)(System::TObject* Sender, int UnicodeValue);

class PASCALIMPLEMENTATION TCustomXmlScanner : public System::Classes::TComponent
{
	typedef System::Classes::TComponent inherited;
	
protected:
	TXmlParser* FXmlParser;
	TXmlPrologEvent FOnXmlProlog;
	TCommentEvent FOnComment;
	TPIEvent FOnPI;
	TDtdEvent FOnDtdRead;
	TStartTagEvent FOnStartTag;
	TStartTagEvent FOnEmptyTag;
	TEndTagEvent FOnEndTag;
	TContentEvent FOnContent;
	TContentEvent FOnCData;
	TElementEvent FOnElement;
	TElementEvent FOnAttList;
	TEntityEvent FOnEntity;
	TNotationEvent FOnNotation;
	TErrorEvent FOnDtdError;
	TExternalEvent FOnLoadExternal;
	TEncodingEvent FOnTranslateEncoding;
	TEncodeCharEvent FOnTranslateCharacter;
	bool FStopParser;
	bool __fastcall GetNormalize(void);
	void __fastcall SetNormalize(bool Value);
	virtual void __fastcall WhenXmlProlog(System::UnicodeString XmlVersion, System::UnicodeString Encoding, bool Standalone);
	virtual void __fastcall WhenComment(System::UnicodeString Comment);
	virtual void __fastcall WhenPI(System::UnicodeString Target, System::UnicodeString Content, TAttrList* Attributes);
	virtual void __fastcall WhenDtdRead(System::UnicodeString RootElementName);
	virtual void __fastcall WhenStartTag(System::UnicodeString TagName, TAttrList* Attributes);
	virtual void __fastcall WhenEmptyTag(System::UnicodeString TagName, TAttrList* Attributes);
	virtual void __fastcall WhenEndTag(System::UnicodeString TagName);
	virtual void __fastcall WhenContent(System::UnicodeString Content);
	virtual void __fastcall WhenCData(System::UnicodeString Content);
	virtual void __fastcall WhenElement(TElemDef* ElemDef);
	virtual void __fastcall WhenAttList(TElemDef* ElemDef);
	virtual void __fastcall WhenEntity(TEntityDef* EntityDef);
	virtual void __fastcall WhenNotation(TNotationDef* NotationDef);
	virtual void __fastcall WhenDtdError(char * ErrorPos);
	
public:
	__fastcall virtual TCustomXmlScanner(System::Classes::TComponent* AOwner);
	__fastcall virtual ~TCustomXmlScanner(void);
	void __fastcall LoadFromFile(System::Sysutils::TFileName Filename);
	void __fastcall LoadFromBuffer(char * Buffer);
	void __fastcall SetBuffer(char * Buffer);
	System::Sysutils::TFileName __fastcall GetFilename(void);
	void __fastcall Execute(void);
	
protected:
	__property TXmlParser* XmlParser = {read=FXmlParser};
	__property bool StopParser = {read=FStopParser, write=FStopParser, nodefault};
	__property System::Sysutils::TFileName Filename = {read=GetFilename, write=LoadFromFile};
	__property bool Normalize = {read=GetNormalize, write=SetNormalize, nodefault};
	__property TXmlPrologEvent OnXmlProlog = {read=FOnXmlProlog, write=FOnXmlProlog};
	__property TCommentEvent OnComment = {read=FOnComment, write=FOnComment};
	__property TPIEvent OnPI = {read=FOnPI, write=FOnPI};
	__property TDtdEvent OnDtdRead = {read=FOnDtdRead, write=FOnDtdRead};
	__property TStartTagEvent OnStartTag = {read=FOnStartTag, write=FOnStartTag};
	__property TStartTagEvent OnEmptyTag = {read=FOnEmptyTag, write=FOnEmptyTag};
	__property TEndTagEvent OnEndTag = {read=FOnEndTag, write=FOnEndTag};
	__property TContentEvent OnContent = {read=FOnContent, write=FOnContent};
	__property TContentEvent OnCData = {read=FOnCData, write=FOnCData};
	__property TElementEvent OnElement = {read=FOnElement, write=FOnElement};
	__property TElementEvent OnAttList = {read=FOnAttList, write=FOnAttList};
	__property TEntityEvent OnEntity = {read=FOnEntity, write=FOnEntity};
	__property TNotationEvent OnNotation = {read=FOnNotation, write=FOnNotation};
	__property TErrorEvent OnDtdError = {read=FOnDtdError, write=FOnDtdError};
	__property TExternalEvent OnLoadExternal = {read=FOnLoadExternal, write=FOnLoadExternal};
	__property TEncodingEvent OnTranslateEncoding = {read=FOnTranslateEncoding, write=FOnTranslateEncoding};
	__property TEncodeCharEvent OnTranslateCharacter = {read=FOnTranslateCharacter, write=FOnTranslateCharacter};
};


//-- var, const, procedure ---------------------------------------------------
#define CVersion L"1.0.20"
static const char CUnknownChar = '\xbf';
#define CWhitespace (System::Set<char, _DELPHI_SET_CHAR(0), _DELPHI_SET_CHAR(255)>() << '\x9' << '\xa' << '\xd' << '\x20' )
#define CLetter (System::Set<char, _DELPHI_SET_CHAR(0), _DELPHI_SET_CHAR(255)>() << '\x41' << '\x42' << '\x43' << '\x44' << '\x45' << '\x46' << '\x47' << '\x48' << '\x49' << '\x4a' << '\x4b' << '\x4c' << '\x4d' << '\x4e' << '\x4f' << '\x50' << '\x51' << '\x52' << '\x53' << '\x54' << '\x55' << '\x56' << '\x57' << '\x58' << '\x59' << '\x5a' << '\x61' << '\x62' << '\x63' << '\x64' << '\x65' << '\x66' << '\x67' << '\x68' << '\x69' << '\x6a' << '\x6b' << '\x6c' << '\x6d' << '\x6e' << '\x6f' << '\x70' << '\x71' << '\x72' << '\x73' << '\x74' << '\x75' << '\x76' << '\x77' << '\x78' << '\x79' << '\x7a' << '\xc0' << '\xc1' << '\xc2' << '\xc3' << '\xc4' << '\xc5' << '\xc6' << '\xc7' << '\xc8' << '\xc9' << '\xca' << '\xcb' << '\xcc' << '\xcd' << '\xce' << '\xcf' \
	<< '\xd0' << '\xd1' << '\xd2' << '\xd3' << '\xd4' << '\xd5' << '\xd6' << '\xd8' << '\xd9' << '\xda' << '\xdb' << '\xdc' << '\xdd' << '\xde' << '\xdf' << '\xe0' << '\xe1' << '\xe2' << '\xe3' << '\xe4' << '\xe5' << '\xe6' << '\xe7' << '\xe8' << '\xe9' << '\xea' << '\xeb' << '\xec' << '\xed' << '\xee' << '\xef' << '\xf0' << '\xf1' << '\xf2' << '\xf3' << '\xf4' << '\xf5' << '\xf6' << '\xf8' << '\xf9' << '\xfa' << '\xfb' << '\xfc' << '\xfd' << '\xfe' << '\xff' )
#define CDigit (System::Set<char, _DELPHI_SET_CHAR(0), _DELPHI_SET_CHAR(255)>() << '\x30' << '\x31' << '\x32' << '\x33' << '\x34' << '\x35' << '\x36' << '\x37' << '\x38' << '\x39' )
#define CNameChar (System::Set<char, _DELPHI_SET_CHAR(0), _DELPHI_SET_CHAR(255)>() << '\x2d' << '\x2e' << '\x30' << '\x31' << '\x32' << '\x33' << '\x34' << '\x35' << '\x36' << '\x37' << '\x38' << '\x39' << '\x3a' << '\x41' << '\x42' << '\x43' << '\x44' << '\x45' << '\x46' << '\x47' << '\x48' << '\x49' << '\x4a' << '\x4b' << '\x4c' << '\x4d' << '\x4e' << '\x4f' << '\x50' << '\x51' << '\x52' << '\x53' << '\x54' << '\x55' << '\x56' << '\x57' << '\x58' << '\x59' << '\x5a' << '\x5f' << '\x61' << '\x62' << '\x63' << '\x64' << '\x65' << '\x66' << '\x67' << '\x68' << '\x69' << '\x6a' << '\x6b' << '\x6c' << '\x6d' << '\x6e' << '\x6f' << '\x70' << '\x71' << '\x72' << '\x73' << '\x74' << '\x75' << '\x76' << '\x77' << '\x78' << '\x79' << '\x7a' << '\xb7' \
	<< '\xc0' << '\xc1' << '\xc2' << '\xc3' << '\xc4' << '\xc5' << '\xc6' << '\xc7' << '\xc8' << '\xc9' << '\xca' << '\xcb' << '\xcc' << '\xcd' << '\xce' << '\xcf' << '\xd0' << '\xd1' << '\xd2' << '\xd3' << '\xd4' << '\xd5' << '\xd6' << '\xd8' << '\xd9' << '\xda' << '\xdb' << '\xdc' << '\xdd' << '\xde' << '\xdf' << '\xe0' << '\xe1' << '\xe2' << '\xe3' << '\xe4' << '\xe5' << '\xe6' << '\xe7' << '\xe8' << '\xe9' << '\xea' << '\xeb' << '\xec' << '\xed' << '\xee' << '\xef' << '\xf0' << '\xf1' << '\xf2' << '\xf3' << '\xf4' << '\xf5' << '\xf6' << '\xf8' << '\xf9' << '\xfa' << '\xfb' << '\xfc' << '\xfd' << '\xfe' << '\xff' )
#define CNameStart (System::Set<char, _DELPHI_SET_CHAR(0), _DELPHI_SET_CHAR(255)>() << '\x3a' << '\x41' << '\x42' << '\x43' << '\x44' << '\x45' << '\x46' << '\x47' << '\x48' << '\x49' << '\x4a' << '\x4b' << '\x4c' << '\x4d' << '\x4e' << '\x4f' << '\x50' << '\x51' << '\x52' << '\x53' << '\x54' << '\x55' << '\x56' << '\x57' << '\x58' << '\x59' << '\x5a' << '\x5f' << '\x61' << '\x62' << '\x63' << '\x64' << '\x65' << '\x66' << '\x67' << '\x68' << '\x69' << '\x6a' << '\x6b' << '\x6c' << '\x6d' << '\x6e' << '\x6f' << '\x70' << '\x71' << '\x72' << '\x73' << '\x74' << '\x75' << '\x76' << '\x77' << '\x78' << '\x79' << '\x7a' << '\xc0' << '\xc1' << '\xc2' << '\xc3' << '\xc4' << '\xc5' << '\xc6' << '\xc7' << '\xc8' << '\xc9' << '\xca' << '\xcb' << '\xcc' \
	<< '\xcd' << '\xce' << '\xcf' << '\xd0' << '\xd1' << '\xd2' << '\xd3' << '\xd4' << '\xd5' << '\xd6' << '\xd8' << '\xd9' << '\xda' << '\xdb' << '\xdc' << '\xdd' << '\xde' << '\xdf' << '\xe0' << '\xe1' << '\xe2' << '\xe3' << '\xe4' << '\xe5' << '\xe6' << '\xe7' << '\xe8' << '\xe9' << '\xea' << '\xeb' << '\xec' << '\xed' << '\xee' << '\xef' << '\xf0' << '\xf1' << '\xf2' << '\xf3' << '\xf4' << '\xf5' << '\xf6' << '\xf8' << '\xf9' << '\xfa' << '\xfb' << '\xfc' << '\xfd' << '\xfe' << '\xff' )
#define CQuoteChar (System::Set<char, _DELPHI_SET_CHAR(0), _DELPHI_SET_CHAR(255)>() << '\x22' << '\x27' )
#define CPubidChar (System::Set<char, _DELPHI_SET_CHAR(0), _DELPHI_SET_CHAR(255)>() << '\x9' << '\xa' << '\xd' << '\x20' << '\x21' << '\x23' << '\x24' << '\x25' << '\x27' << '\x28' << '\x29' << '\x2a' << '\x2b' << '\x2c' << '\x2d' << '\x2e' << '\x2f' << '\x30' << '\x31' << '\x32' << '\x33' << '\x34' << '\x35' << '\x36' << '\x37' << '\x38' << '\x39' << '\x3a' << '\x3b' << '\x3d' << '\x3f' << '\x40' << '\x41' << '\x42' << '\x43' << '\x44' << '\x45' << '\x46' << '\x47' << '\x48' << '\x49' << '\x4a' << '\x4b' << '\x4c' << '\x4d' << '\x4e' << '\x4f' << '\x50' << '\x51' << '\x52' << '\x53' << '\x54' << '\x55' << '\x56' << '\x57' << '\x58' << '\x59' << '\x5a' << '\x5f' << '\x61' << '\x62' << '\x63' << '\x64' << '\x65' << '\x66' << '\x67' << '\x68' << '\x69' \
	<< '\x6a' << '\x6b' << '\x6c' << '\x6d' << '\x6e' << '\x6f' << '\x70' << '\x71' << '\x72' << '\x73' << '\x74' << '\x75' << '\x76' << '\x77' << '\x78' << '\x79' << '\x7a' )
#define CDStart L"<![CDATA["
#define CDEnd L"]]>"
#define CUtf8BOM L"\u043f\u00bb\u0457"
extern DELPHI_PACKAGE Libxmlparser__21 CPartType_Name;
extern DELPHI_PACKAGE Libxmlparser__31 CValueType_Name;
extern DELPHI_PACKAGE Libxmlparser__41 CAttrDefault_Name;
extern DELPHI_PACKAGE Libxmlparser__51 CElemType_Name;
extern DELPHI_PACKAGE Libxmlparser__61 CAttrType_Name;
extern DELPHI_PACKAGE System::AnsiString __fastcall AnsiToUtf8(System::AnsiString Source);
extern DELPHI_PACKAGE System::AnsiString __fastcall Utf8ToAnsi(System::AnsiString Source, char UnknownChar = '\xbf');
extern DELPHI_PACKAGE System::AnsiString __fastcall TrimWs(System::AnsiString Source);
extern DELPHI_PACKAGE System::AnsiString __fastcall ConvertWs(System::AnsiString Source, bool PackWs);
extern DELPHI_PACKAGE void __fastcall SetStringSF(System::AnsiString &S, char * BufferStart, char * BufferFinal);
extern DELPHI_PACKAGE System::AnsiString __fastcall StrSFPas(char * Start, char * Finish);
}	/* namespace Libxmlparser */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_LIBXMLPARSER)
using namespace Libxmlparser;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// LibxmlparserHPP
