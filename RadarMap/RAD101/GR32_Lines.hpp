﻿// CodeGear C++Builder
// Copyright (c) 1995, 2016 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'GR32_Lines.pas' rev: 31.00 (Windows)

#ifndef Gr32_linesHPP
#define Gr32_linesHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>
#include <SysInit.hpp>
#include <Winapi.Windows.hpp>
#include <System.Classes.hpp>
#include <System.SysUtils.hpp>
#include <System.Math.hpp>
#include <GR32.hpp>
#include <GR32_LowLevel.hpp>
#include <GR32_Blend.hpp>
#include <GR32_Transforms.hpp>
#include <GR32_Math.hpp>
#include <GR32_Polygons_old.hpp>
#include <GR32_Misc.hpp>
#include <System.Types.hpp>

//-- user supplied -----------------------------------------------------------

namespace Gr32_lines
{
//-- forward type declarations -----------------------------------------------
class DELPHICLASS TArrowPen;
class DELPHICLASS TArrowHead;
class DELPHICLASS TLine32;
//-- type declarations -------------------------------------------------------
enum DECLSPEC_DENUM TJoinStyle : unsigned char { jsBevelled, jsRounded, jsMitered };

enum DECLSPEC_DENUM TEndStyle : unsigned char { esSquared, esRounded, esClosed, esButt };

enum DECLSPEC_DENUM TQuadrant : unsigned char { First, Second, Third, Forth };

enum DECLSPEC_DENUM TArrowHeadStyle : unsigned char { asNone, asThreePoint, asFourPoint, asSquare, asDiamond, asCircle, asCustom };

enum DECLSPEC_DENUM THitTestResult : unsigned char { htNone, htStartArrow, htEndArrow, htLine };

typedef Gr32::TArrayOfFixedPoint __fastcall (__closure *TCustomArrowHeadProc)(const Gr32::TFixedPoint &tipPt, const Gr32::TFixedPoint &tailPt, float HeadSize, float PenWidth, TArrowHeadStyle ArrowHeadStyle);

typedef Gr32::TColor32 __fastcall (*TColorProc)(float frac);

#pragma pack(push,4)
class PASCALIMPLEMENTATION TArrowPen : public System::TObject
{
	typedef System::TObject inherited;
	
private:
	TArrowHead* fOwnerArrowHead;
	Gr32::TColor32 fColor;
	float fWidth;
	void __fastcall SetWidth(float value);
	
public:
	__fastcall TArrowPen(TArrowHead* owner);
	__property Gr32::TColor32 Color = {read=fColor, write=fColor, nodefault};
	__property float Width = {read=fWidth, write=SetWidth};
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TArrowPen(void) { }
	
};

#pragma pack(pop)

class PASCALIMPLEMENTATION TArrowHead : public System::TObject
{
	typedef System::TObject inherited;
	
private:
	TLine32* fOwnerLine32;
	bool fIsStartArrow;
	TArrowHeadStyle fStyle;
	float fSize;
	Gr32::TColor32 fColor;
	TCustomArrowHeadProc fCustomProc;
	TArrowPen* fPen;
	Gr32::TFixedPoint fTipPoint;
	Gr32::TFixedPoint fBasePoint;
	int fBaseIdx;
	void __fastcall SetSize(float value);
	void __fastcall SetStyle(TArrowHeadStyle value);
	void __fastcall SetCustomProc(TCustomArrowHeadProc value);
	void __fastcall Draw(Gr32::TBitmap32* bitmap);
	
protected:
	bool __fastcall IsNeeded(void);
	bool __fastcall GetTipAndBase(void);
	__property Gr32::TFixedPoint Base = {read=fBasePoint};
	__property int BaseIdx = {read=fBaseIdx, nodefault};
	
public:
	__fastcall TArrowHead(TLine32* owner, bool IsStartArrow);
	__fastcall virtual ~TArrowHead(void);
	Gr32::TArrayOfFixedPoint __fastcall GetPoints(void);
	Gr32::TArrayOfFixedPoint __fastcall OutlinePoints(float delta);
	__property Gr32::TColor32 Color = {read=fColor, write=fColor, nodefault};
	__property TArrowHeadStyle Style = {read=fStyle, write=SetStyle, nodefault};
	__property float Size = {read=fSize, write=SetSize};
	__property TArrowPen* Pen = {read=fPen};
	__property TCustomArrowHeadProc CustomProc = {read=fCustomProc, write=SetCustomProc};
};


#pragma pack(push,4)
class PASCALIMPLEMENTATION TLine32 : public System::TObject
{
	typedef System::TObject inherited;
	
private:
	Gr32::TArrayOfFixedPoint fLinePoints;
	Gr32::TArrayOfFixedPoint fLeftPoints;
	Gr32::TArrayOfFixedPoint fRightPoints;
	Gr32_polygons_old::TPolygon32* fPolygon32;
	TArrowHead* fStartArrow;
	TArrowHead* fEndArrow;
	float fLineWidth;
	TEndStyle fEndStyle;
	Gr32_polygons_old::TPolyFillModeOld fFillMode;
	float fMiterLimit;
	TJoinStyle fJoinStyle;
	void __fastcall Build(void);
	void __fastcall SetWidth(float value);
	void __fastcall SetMiterLimit(float value);
	void __fastcall SetJoinStyle(TJoinStyle value);
	void __fastcall SetEndStyle(TEndStyle value);
	void __fastcall DrawArrows(Gr32::TBitmap32* bitmap);
	void __fastcall DrawGradientHorz(Gr32::TBitmap32* bitmap, float penWidth, const Gr32::TColor32 *colors, const int colors_High, Gr32::TColor32 edgeColor = (Gr32::TColor32)(0x0));
	void __fastcall DrawGradientVert(Gr32::TBitmap32* bitmap, float penWidth, const Gr32::TColor32 *colors, const int colors_High, Gr32::TColor32 edgeColor = (Gr32::TColor32)(0x0));
	Gr32_polygons_old::TAntialiasMode __fastcall GetAntialiasMode(void);
	void __fastcall SetAntialiasMode(Gr32_polygons_old::TAntialiasMode aaMode);
	
protected:
	void __fastcall ForceRebuild(void);
	
public:
	__fastcall TLine32(void);
	__fastcall virtual ~TLine32(void);
	void __fastcall Clear(void);
	void __fastcall SetPoints(const Gr32::TFixedPoint *pts, const int pts_High);
	unsigned __fastcall AddPoints(const Gr32::TFixedPoint *newPts, const int newPts_High, bool ToEnd = true)/* overload */;
	unsigned __fastcall AddPoints(Gr32::TFixedPoint &startPt, int count, bool ToEnd = true)/* overload */;
	void __fastcall DeletePoints(int count, bool FromEnd = true);
	void __fastcall Draw(Gr32::TBitmap32* bitmap, float penWidth, Gr32::TColor32 color, Gr32::TColor32 edgeColor = (Gr32::TColor32)(0x0))/* overload */;
	void __fastcall Draw(Gr32::TBitmap32* bitmap, float penWidth, Gr32::TBitmap32* pattern, Gr32::TColor32 edgeColor = (Gr32::TColor32)(0x0))/* overload */;
	void __fastcall Draw(Gr32::TBitmap32* bitmap, float penWidth, Gr32_polygons_old::TCustomPolygonFillerOld* filler, Gr32::TColor32 edgeColor = (Gr32::TColor32)(0x0))/* overload */;
	void __fastcall Draw(Gr32::TBitmap32* bitmap, float penWidth, Gr32::TColor32 *colors, const int colors_High)/* overload */;
	void __fastcall Draw(Gr32::TBitmap32* bitmap, float penWidth, Gr32::TColor32 *colors, const int colors_High, float StippleStep)/* overload */;
	void __fastcall Draw(Gr32::TBitmap32* bitmap, float penWidth, Gr32::TArrayOfFloat dashPattern, Gr32::TColor32 color, Gr32::TColor32 edgeColor = (Gr32::TColor32)(0x0))/* overload */;
	void __fastcall DrawGradient(Gr32::TBitmap32* bitmap, float penWidth, const Gr32::TColor32 *colors, const int colors_High, int angle_degrees, Gr32::TColor32 edgeColor = (Gr32::TColor32)(0x0));
	Gr32::TArrayOfFixedPoint __fastcall GetOutline(float penWidth = 0.000000E+00f);
	Gr32::TArrayOfFixedPoint __fastcall GetOuterEdge(float penWidth = 0.000000E+00f);
	Gr32::TArrayOfFixedPoint __fastcall GetInnerEdge(float penWidth = 0.000000E+00f);
	Gr32::TArrayOfFixedPoint __fastcall GetLeftPoints(void);
	Gr32::TArrayOfFixedPoint __fastcall GetRightPoints(void);
	Gr32::TFixedRect __fastcall GetBoundsFixedRect(float penWidth = 0.000000E+00f);
	System::Types::TRect __fastcall GetBoundsRect(float penWidth = 0.000000E+00f);
	void __fastcall Transform(const Gr32_transforms::TFloatMatrix &matrix);
	void __fastcall Translate(float dx, float dy);
	void __fastcall Scale(float dx, float dy);
	void __fastcall Rotate(const Gr32::TFloatPoint &origin, float radians);
	THitTestResult __fastcall DoHitTest(const Gr32::TFixedPoint &pt, float penWidth = 0.000000E+00f);
	Gr32::TArrayOfFixedPoint __fastcall Points(void);
	Gr32::TArrayOfFixedPoint __fastcall GetArrowTruncatedPoints(void);
	__property Gr32_polygons_old::TAntialiasMode AntialiasMode = {read=GetAntialiasMode, write=SetAntialiasMode, default=1};
	__property TArrowHead* ArrowStart = {read=fStartArrow};
	__property TArrowHead* ArrowEnd = {read=fEndArrow};
	__property TEndStyle EndStyle = {read=fEndStyle, write=SetEndStyle, nodefault};
	__property Gr32_polygons_old::TPolyFillModeOld FillMode = {read=fFillMode, write=fFillMode, nodefault};
	__property TJoinStyle JoinStyle = {read=fJoinStyle, write=SetJoinStyle, nodefault};
	__property float LineWidth = {read=fLineWidth, write=SetWidth};
	__property float MiterLimit = {read=fMiterLimit, write=SetMiterLimit};
};

#pragma pack(pop)

//-- var, const, procedure ---------------------------------------------------
extern DELPHI_PACKAGE void __fastcall SimpleLine(Gr32::TBitmap32* bitmap, const Gr32::TFixedPoint *pts, const int pts_High, Gr32::TColor32 color, float width, bool closed = false)/* overload */;
extern DELPHI_PACKAGE void __fastcall SimpleLine(Gr32::TBitmap32* bitmap, const Gr32::TArrayOfArrayOfFixedPoint ppts, Gr32::TColor32 color, float width, bool closed)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfFixedPoint __fastcall InflatePoints(const Gr32::TArrayOfFixedPoint pts, float delta, bool closed)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfArrayOfFixedPoint __fastcall InflatePoints(const Gr32::TArrayOfArrayOfFixedPoint polyPts, float delta, bool closed)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfFloatPoint __fastcall InflatePoints(const Gr32::TArrayOfFloatPoint pts, float delta, bool closed)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfArrayOfFloatPoint __fastcall InflatePoints(const Gr32::TArrayOfArrayOfFloatPoint polyPts, float delta, bool closed)/* overload */;
}	/* namespace Gr32_lines */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_GR32_LINES)
using namespace Gr32_lines;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// Gr32_linesHPP
