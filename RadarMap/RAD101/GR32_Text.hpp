﻿// CodeGear C++Builder
// Copyright (c) 1995, 2016 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'GR32_Text.pas' rev: 31.00 (Windows)

#ifndef Gr32_textHPP
#define Gr32_textHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>
#include <SysInit.hpp>
#include <Winapi.Windows.hpp>
#include <System.Classes.hpp>
#include <System.SysUtils.hpp>
#include <System.Math.hpp>
#include <Vcl.Graphics.hpp>
#include <System.Types.hpp>
#include <GR32.hpp>
#include <GR32_LowLevel.hpp>
#include <System.UITypes.hpp>
#include <GR32_Blend.hpp>
#include <GR32_Math.hpp>
#include <GR32_Polygons_old.hpp>
#include <GR32_Transforms.hpp>
#include <GR32_Backends.hpp>
#include <GR32_Misc.hpp>
#include <GR32_Lines.hpp>

//-- user supplied -----------------------------------------------------------

namespace Gr32_text
{
//-- forward type declarations -----------------------------------------------
struct TFloatSize;
struct TGlyphInfo;
class DELPHICLASS TTrueTypeFont;
class DELPHICLASS TTrueTypeFontAnsiCache;
class DELPHICLASS TText32;
//-- type declarations -------------------------------------------------------
enum DECLSPEC_DENUM TAlignH : unsigned char { aLeft, aRight, aCenter, aJustify };

enum DECLSPEC_DENUM TAlignV : unsigned char { aBottom, aMiddle, aTop };

struct DECLSPEC_DRECORD TFloatSize
{
public:
	float sx;
	float sy;
};


struct DECLSPEC_DRECORD TGlyphInfo
{
public:
	bool cached;
	_GLYPHMETRICS metrics;
	Gr32::TArrayOfArrayOfFixedPoint pts;
};


typedef System::TMetaClass* TTrueTypeFontClass;

#pragma pack(push,4)
class PASCALIMPLEMENTATION TTrueTypeFont : public System::Classes::TPersistent
{
	typedef System::Classes::TPersistent inherited;
	
private:
	Vcl::Graphics::TFont* fFont;
	HDC fMemDC;
	HFONT fOldFntHdl;
	bool fHinted;
	bool fFlushed;
	System::Uitypes::TFontName __fastcall GetFontName(void);
	void __fastcall SetFontName(const System::Uitypes::TFontName aFontName);
	int __fastcall GetHeight(void);
	void __fastcall SetHeight(int aHeight);
	int __fastcall GetSize(void);
	void __fastcall SetSize(int aSize);
	System::Uitypes::TFontStyles __fastcall GetStyle(void);
	void __fastcall SetStyle(System::Uitypes::TFontStyles aStyle);
	System::Uitypes::TFontCharset __fastcall GetCharSet(void);
	void __fastcall SetCharSet(System::Uitypes::TFontCharset aCharSet);
	void __fastcall SetHinted(bool value);
	
protected:
	__property HDC MemDC = {read=fMemDC, nodefault};
	
public:
	__fastcall virtual TTrueTypeFont(void)/* overload */;
	__fastcall virtual TTrueTypeFont(Vcl::Graphics::TFont* aFont)/* overload */;
	__fastcall virtual TTrueTypeFont(const System::Uitypes::TFontName aFontName, int aHeight, System::Uitypes::TFontStyles aStyle, System::Uitypes::TFontCharset aCharSet)/* overload */;
	__fastcall virtual ~TTrueTypeFont(void);
	__classmethod bool __fastcall IsValidTTFont(Vcl::Graphics::TFont* font);
	bool __fastcall Lock(void);
	bool __fastcall IsLocked(void);
	void __fastcall Unlock(void);
	virtual void __fastcall Assign(System::Classes::TPersistent* Source);
	virtual bool __fastcall GetGlyphInfo(System::WideChar wc, /* out */ _GLYPHMETRICS &gm, /* out */ Gr32::TArrayOfArrayOfFixedPoint &polyPts);
	bool __fastcall GetTextMetrics(/* out */ tagTEXTMETRICW &tm);
	bool __fastcall GetOutlineTextMetrics(/* out */ _OUTLINETEXTMETRICW &otm);
	void __fastcall Flush(void);
	__property bool Flushed = {read=fFlushed, write=fFlushed, nodefault};
	__property System::Uitypes::TFontName FontName = {read=GetFontName, write=SetFontName};
	__property int Height = {read=GetHeight, write=SetHeight, nodefault};
	__property int Size = {read=GetSize, write=SetSize, nodefault};
	__property System::Uitypes::TFontCharset CharSet = {read=GetCharSet, write=SetCharSet, nodefault};
	__property System::Uitypes::TFontStyles Style = {read=GetStyle, write=SetStyle, nodefault};
	__property bool Hinted = {read=fHinted, write=SetHinted, nodefault};
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TTrueTypeFontAnsiCache : public TTrueTypeFont
{
	typedef TTrueTypeFont inherited;
	
	
private:
	typedef System::StaticArray<TGlyphInfo, 96> _TTrueTypeFontAnsiCache__1;
	
	
private:
	_TTrueTypeFontAnsiCache__1 fGlyphInfo;
	
public:
	virtual bool __fastcall GetGlyphInfo(System::WideChar wc, /* out */ _GLYPHMETRICS &gm, /* out */ Gr32::TArrayOfArrayOfFixedPoint &polyPts);
public:
	/* TTrueTypeFont.Create */ inline __fastcall virtual TTrueTypeFontAnsiCache(void)/* overload */ : TTrueTypeFont() { }
	/* TTrueTypeFont.Create */ inline __fastcall virtual TTrueTypeFontAnsiCache(Vcl::Graphics::TFont* aFont)/* overload */ : TTrueTypeFont(aFont) { }
	/* TTrueTypeFont.Create */ inline __fastcall virtual TTrueTypeFontAnsiCache(const System::Uitypes::TFontName aFontName, int aHeight, System::Uitypes::TFontStyles aStyle, System::Uitypes::TFontCharset aCharSet)/* overload */ : TTrueTypeFont(aFontName, aHeight, aStyle, aCharSet) { }
	/* TTrueTypeFont.Destroy */ inline __fastcall virtual ~TTrueTypeFontAnsiCache(void) { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TText32 : public System::TObject
{
	typedef System::TObject inherited;
	
private:
	Gr32::TBitmap32* fTmpBmp;
	bool fLCDDraw;
	int fAngle;
	TFloatSize fSkew;
	TFloatSize fScale;
	TFloatSize fTranslate;
	float fPadding;
	bool fInverted;
	bool fMirrored;
	Gr32::TFixedPoint fCurrentPos;
	_MAT2 fGlyphMatrix;
	_MAT2 fCurrentPosMatrix;
	_MAT2 fUnrotatedMatrix;
	void __fastcall SetInverted(bool value);
	void __fastcall SetMirrored(bool value);
	void __fastcall SetAngle(int value);
	void __fastcall SetScaleX(float value);
	void __fastcall SetScaleY(float value);
	void __fastcall SetSkewX(float value);
	void __fastcall SetSkewY(float value);
	void __fastcall SetPadding(float value);
	void __fastcall PrepareMatrices(void);
	void __fastcall DrawInternal(Gr32::TBitmap32* bitmap, const System::UnicodeString text, TTrueTypeFont* ttFont, Gr32::TColor32 color);
	void __fastcall DrawInternalLCD(Gr32::TBitmap32* bitmap, const System::UnicodeString text, TTrueTypeFont* ttFont, Gr32::TColor32 color);
	Gr32::TFixedPoint __fastcall GetCurrentPos(void);
	void __fastcall SetCurrentPos(const Gr32::TFixedPoint &newPos);
	
protected:
	void __fastcall GetTextMetrics(const System::UnicodeString text, TTrueTypeFont* ttFont, const Gr32::TFixedPoint &InsertionPt, /* out */ Gr32::TFixedPoint &NextInsertionPt, /* out */ Gr32::TFixedRect &BoundsRect);
	void __fastcall GetDrawInfo(const System::UnicodeString text, TTrueTypeFont* ttFont, const Gr32::TFloatPoint &InsertionPt, /* out */ Gr32::TFloatPoint &NextInsertionPt, /* out */ Gr32_misc::TArrayOfArrayOfArrayOfFixedPoint &polyPolyPts);
	
public:
	__fastcall TText32(void);
	__fastcall virtual ~TText32(void);
	Gr32::TFixedRect __fastcall GetTextFixedRect(float X, float Y, const System::UnicodeString text, TTrueTypeFont* ttFont);
	float __fastcall GetTextHeight(const System::UnicodeString text, TTrueTypeFont* ttFont);
	float __fastcall GetTextWidth(const System::UnicodeString text, TTrueTypeFont* ttFont);
	int __fastcall CountCharsThatFit(float X, float Y, const System::UnicodeString text, TTrueTypeFont* ttFont, const Gr32::TFloatRect &boundsRect, bool forceWordBreak = false);
	void __fastcall Draw(Gr32::TBitmap32* bitmap, float X, float Y, const System::UnicodeString text, TTrueTypeFont* ttFont, Gr32::TColor32 color)/* overload */;
	void __fastcall Draw(Gr32::TBitmap32* bitmap, const System::UnicodeString text, TTrueTypeFont* ttFont, Gr32::TColor32 color)/* overload */;
	void __fastcall Draw(Gr32::TBitmap32* bitmap, const Gr32::TFixedPoint *path, const int path_High, const System::UnicodeString text, TTrueTypeFont* ttFont, Gr32::TColor32 color, TAlignH alignH = (TAlignH)(0x2), TAlignV alignV = (TAlignV)(0x1), float offsetFromLine = 0.000000E+00f)/* overload */;
	void __fastcall Draw(Gr32::TBitmap32* bitmap, const Gr32::TFloatRect &boundsRect, const System::UnicodeString text, TTrueTypeFont* ttFont, Gr32::TColor32 color, TAlignH alignH, TAlignV alignV, bool forceWordBreak = false)/* overload */;
	Gr32_misc::TArrayOfArrayOfArrayOfFixedPoint __fastcall GetEx(const Gr32::TFloatRect &boundsRect, const System::UnicodeString text, TTrueTypeFont* ttFont, TAlignH alignH, TAlignV alignV, bool forceWordBreak = false)/* overload */;
	void __fastcall DrawAndOutline(Gr32::TBitmap32* bitmap, float X, float Y, const System::UnicodeString text, TTrueTypeFont* ttFont, float outlinePenWidth, Gr32::TColor32 outlineColor, Gr32::TColor32 fillColor);
	Gr32::TArrayOfArrayOfFixedPoint __fastcall Get(float X, float Y, const System::UnicodeString text, TTrueTypeFont* ttFont, /* out */ Gr32::TFloatPoint &NextInsertionPt)/* overload */;
	Gr32_misc::TArrayOfArrayOfArrayOfFixedPoint __fastcall GetEx(float X, float Y, const System::UnicodeString text, TTrueTypeFont* ttFont, /* out */ Gr32::TFloatPoint &NextInsertionPt)/* overload */;
	Gr32::TArrayOfArrayOfFixedPoint __fastcall Get(const Gr32::TFixedPoint *path, const int path_High, const System::UnicodeString text, TTrueTypeFont* ttFont, TAlignH alignH, TAlignV alignV, bool rotateTextToPath, float offsetFromLine = 0.000000E+00f)/* overload */;
	Gr32_misc::TArrayOfArrayOfArrayOfFixedPoint __fastcall GetEx(const Gr32::TFixedPoint *path, const int path_High, const System::UnicodeString text, TTrueTypeFont* ttFont, TAlignH alignH, TAlignV alignV, bool rotateTextToPath, float offsetFromLine = 0.000000E+00f)/* overload */;
	Gr32::TArrayOfArrayOfFixedPoint __fastcall GetInflated(float X, float Y, float delta, const System::UnicodeString text, TTrueTypeFont* ttFont, /* out */ Gr32::TFloatPoint &NextInsertionPt);
	Gr32_misc::TArrayOfArrayOfArrayOfFixedPoint __fastcall GetInflatedEx(float X, float Y, float delta, const System::UnicodeString text, TTrueTypeFont* ttFont, /* out */ Gr32::TFloatPoint &NextInsertionPt);
	Gr32::TArrayOfArrayOfFixedPoint __fastcall GetBetweenPaths(const Gr32::TArrayOfFixedPoint bottomCurve, const Gr32::TArrayOfFixedPoint topCurve, const System::UnicodeString text, TTrueTypeFont* ttFont);
	Gr32_misc::TArrayOfArrayOfArrayOfFixedPoint __fastcall GetBetweenPathsEx(const Gr32::TArrayOfFixedPoint bottomCurve, const Gr32::TArrayOfFixedPoint topCurve, const System::UnicodeString text, TTrueTypeFont* ttFont);
	void __fastcall Scale(float sx, float sy);
	void __fastcall Skew(float sx, float sy);
	void __fastcall Translate(float dx, float dy);
	void __fastcall ClearTransformations(void);
	__property int Angle = {read=fAngle, write=SetAngle, nodefault};
	__property float ScaleX = {read=fScale.sx, write=SetScaleX};
	__property float ScaleY = {read=fScale.sy, write=SetScaleY};
	__property float SkewX = {read=fSkew.sx, write=SetSkewX};
	__property float SkewY = {read=fSkew.sy, write=SetSkewY};
	__property float TranslateX = {read=fTranslate.sx, write=fTranslate.sx};
	__property float TranslateY = {read=fTranslate.sy, write=fTranslate.sy};
	__property Gr32::TFixedPoint CurrentPos = {read=GetCurrentPos, write=SetCurrentPos};
	__property bool LCDDraw = {read=fLCDDraw, write=fLCDDraw, nodefault};
	__property bool Inverted = {read=fInverted, write=SetInverted, nodefault};
	__property bool Mirrored = {read=fMirrored, write=SetMirrored, nodefault};
	__property float Spacing = {read=fPadding, write=SetPadding};
};

#pragma pack(pop)

//-- var, const, procedure ---------------------------------------------------
extern DELPHI_PACKAGE TTrueTypeFontClass TrueTypeFontClass;
extern DELPHI_PACKAGE bool Text32LCDDrawDefault;
extern DELPHI_PACKAGE _MAT2 identity_mat2;
extern DELPHI_PACKAGE _MAT2 vert_flip_mat2;
extern DELPHI_PACKAGE _MAT2 horz_flip_mat2;
extern DELPHI_PACKAGE TFloatSize __fastcall FloatSize(float sx, float sy);
extern DELPHI_PACKAGE void __fastcall ScaleMat2(_MAT2 &mat, const float sx, const float sy);
extern DELPHI_PACKAGE void __fastcall SkewMat2(_MAT2 &mat, const float fx, const float fy);
extern DELPHI_PACKAGE void __fastcall RotateMat2(_MAT2 &mat, const float angle_radians);
extern DELPHI_PACKAGE bool __fastcall GetTTFontCharInfo(HDC MemDC, System::WideChar wc, float dx, float dy, const _MAT2 &mat2, bool Hinted, /* out */ _GLYPHMETRICS &gm, /* out */ Gr32::TArrayOfArrayOfFixedPoint &polyPts);
extern DELPHI_PACKAGE bool __fastcall GetTTFontCharMetrics(HDC MemDC, System::WideChar wc, float dx, float dy, const _MAT2 &mat2, /* out */ _GLYPHMETRICS &gm);
extern DELPHI_PACKAGE void __fastcall LCDPolygonSmoothing(Gr32::TBitmap32* bitmap, Gr32::TBitmap32* tmpBitmap, Gr32_misc::TArrayOfArrayOfArrayOfFixedPoint ppts, Gr32::TColor32 TextColor, Gr32::TColor32 BkColor = (Gr32::TColor32)(0x0));
extern DELPHI_PACKAGE void __fastcall SimpleText(Gr32::TBitmap32* bmp, Vcl::Graphics::TFont* font, int X, int Y, const System::WideString widetext, Gr32::TColor32 color);
extern DELPHI_PACKAGE void __fastcall SimpleTextLCD(Gr32::TBitmap32* bmp, Vcl::Graphics::TFont* font, int X, int Y, const System::UnicodeString wideText, Gr32::TColor32 color);
}	/* namespace Gr32_text */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_GR32_TEXT)
using namespace Gr32_text;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// Gr32_textHPP
