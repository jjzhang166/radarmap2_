﻿// CodeGear C++Builder
// Copyright (c) 1995, 2016 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'LibXmlComps.pas' rev: 31.00 (Windows)

#ifndef LibxmlcompsHPP
#define LibxmlcompsHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>
#include <SysInit.hpp>
#include <System.Classes.hpp>
#include <LibXmlParser.hpp>
#include <System.SysUtils.hpp>

//-- user supplied -----------------------------------------------------------

namespace Libxmlcomps
{
//-- forward type declarations -----------------------------------------------
class DELPHICLASS TXmlScanner;
class DELPHICLASS TEasyXmlScanner;
//-- type declarations -------------------------------------------------------
class PASCALIMPLEMENTATION TXmlScanner : public Libxmlparser::TCustomXmlScanner
{
	typedef Libxmlparser::TCustomXmlScanner inherited;
	
public:
	__property XmlParser;
	__property StopParser;
	
__published:
	__property Filename = {default=0};
	__property Normalize;
	__property OnXmlProlog;
	__property OnComment;
	__property OnPI;
	__property OnDtdRead;
	__property OnStartTag;
	__property OnEmptyTag;
	__property OnEndTag;
	__property OnContent;
	__property OnCData;
	__property OnElement;
	__property OnAttList;
	__property OnEntity;
	__property OnNotation;
	__property OnDtdError;
	__property OnLoadExternal;
	__property OnTranslateEncoding;
	__property OnTranslateCharacter;
public:
	/* TCustomXmlScanner.Create */ inline __fastcall virtual TXmlScanner(System::Classes::TComponent* AOwner) : Libxmlparser::TCustomXmlScanner(AOwner) { }
	/* TCustomXmlScanner.Destroy */ inline __fastcall virtual ~TXmlScanner(void) { }
	
};


class PASCALIMPLEMENTATION TEasyXmlScanner : public Libxmlparser::TCustomXmlScanner
{
	typedef Libxmlparser::TCustomXmlScanner inherited;
	
protected:
	virtual void __fastcall WhenCData(System::UnicodeString Content);
	
public:
	__property XmlParser;
	__property StopParser;
	
__published:
	__property Filename = {default=0};
	__property Normalize;
	__property OnComment;
	__property OnPI;
	__property OnStartTag;
	__property OnEmptyTag;
	__property OnEndTag;
	__property OnContent;
	__property OnLoadExternal;
	__property OnTranslateEncoding;
public:
	/* TCustomXmlScanner.Create */ inline __fastcall virtual TEasyXmlScanner(System::Classes::TComponent* AOwner) : Libxmlparser::TCustomXmlScanner(AOwner) { }
	/* TCustomXmlScanner.Destroy */ inline __fastcall virtual ~TEasyXmlScanner(void) { }
	
};


//-- var, const, procedure ---------------------------------------------------
extern DELPHI_PACKAGE void __fastcall Register(void);
}	/* namespace Libxmlcomps */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_LIBXMLCOMPS)
using namespace Libxmlcomps;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// LibxmlcompsHPP
