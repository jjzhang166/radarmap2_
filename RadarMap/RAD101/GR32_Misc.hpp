﻿// CodeGear C++Builder
// Copyright (c) 1995, 2016 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'GR32_Misc.pas' rev: 31.00 (Windows)

#ifndef Gr32_miscHPP
#define Gr32_miscHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>
#include <SysInit.hpp>
#include <Winapi.Windows.hpp>
#include <System.Types.hpp>
#include <System.Classes.hpp>
#include <System.SysUtils.hpp>
#include <GR32.hpp>
#include <GR32_LowLevel.hpp>
#include <GR32_Blend.hpp>
#include <GR32_Transforms.hpp>
#include <GR32_Math.hpp>
#include <GR32_Polygons_old.hpp>
#include <System.Math.hpp>

//-- user supplied -----------------------------------------------------------

namespace Gr32_misc
{
//-- forward type declarations -----------------------------------------------
//-- type declarations -------------------------------------------------------
enum DECLSPEC_DENUM TBalloonPos : unsigned char { bpNone, bpTopLeft, bpTopRight, bpBottomLeft, bpBottomRight };

enum DECLSPEC_DENUM TCorner : unsigned char { cTopLeft, cTopRight, cBottomLeft, cBottomRight };

typedef System::DynamicArray<Gr32::TArrayOfArrayOfFixedPoint> TArrayOfArrayOfArrayOfFixedPoint;

//-- var, const, procedure ---------------------------------------------------
static const System::Int8 MAXIMUM_SHADOW_FADE = System::Int8(0x0);
static const System::Int8 MEDIUM_SHADOW_FADE = System::Int8(0x5);
static const System::Int8 MINIMUM_SHADOW_FADE = System::Int8(0xa);
static const System::Int8 NO_SHADOW_FADE = System::Int8(0xb);
static const System::Extended rad01 = 1.745329E-02;
static const System::Extended rad30 = 5.235988E-01;
static const System::Extended rad45 = 7.853982E-01;
static const System::Extended rad60 = 1.047198E+00;
static const System::Extended rad90 = 1.570796E+00;
static const System::Extended rad180 = 3.141593E+00;
static const System::Extended rad270 = 4.712389E+00;
static const System::Extended rad360 = 6.283185E+00;
static const System::Extended DegToRad = 1.745329E-02;
static const System::Extended RadToDeg = 5.729578E+01;
#define SingleLineLimit  (1.500000E+00)
static const System::Extended SqrtTwo = 1.414214E+00;
#define half  (5.000000E-01)
#define cbezier_tolerance  (5.000000E-01)
#define qbezier_tolerance  (5.000000E-01)
extern DELPHI_PACKAGE void __fastcall OffsetPoint(Gr32::TFloatPoint &pt, float dx, float dy)/* overload */;
extern DELPHI_PACKAGE void __fastcall OffsetPoint(Gr32::TFixedPoint &pt, float dx, float dy)/* overload */;
extern DELPHI_PACKAGE void __fastcall OffsetPoints(Gr32::TArrayOfFixedPoint &pts, float dx, float dy)/* overload */;
extern DELPHI_PACKAGE void __fastcall OffsetPoints(Gr32::TArrayOfFloatPoint &pts, float dx, float dy)/* overload */;
extern DELPHI_PACKAGE void __fastcall OffsetPolyPoints(Gr32::TArrayOfArrayOfFixedPoint &polyPts, float dx, float dy);
extern DELPHI_PACKAGE void __fastcall OffsetPolyPolyPoints(TArrayOfArrayOfArrayOfFixedPoint &polyPolyPts, float dx, float dy);
extern DELPHI_PACKAGE Gr32::TArrayOfArrayOfFixedPoint __fastcall CopyPolyPoints(const Gr32::TArrayOfArrayOfFixedPoint polyPts);
extern DELPHI_PACKAGE Gr32::TArrayOfFixedPoint __fastcall ReversePoints(const Gr32::TArrayOfFixedPoint pts)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfFloatPoint __fastcall ReversePoints(const Gr32::TArrayOfFloatPoint pts)/* overload */;
extern DELPHI_PACKAGE void __fastcall ConcatenatePoints(Gr32::TArrayOfFixedPoint &pts, const Gr32::TArrayOfFixedPoint extras)/* overload */;
extern DELPHI_PACKAGE void __fastcall ConcatenatePoints(Gr32::TArrayOfFixedPoint &pts, const Gr32::TFixedPoint *extras, const int extras_High)/* overload */;
extern DELPHI_PACKAGE void __fastcall ConcatenatePolyPoints(Gr32::TArrayOfArrayOfFixedPoint &polyPts, const Gr32::TArrayOfArrayOfFixedPoint extras);
extern DELPHI_PACKAGE Gr32::TArrayOfFloatPoint __fastcall AFixedToAFloat(Gr32::TArrayOfFixedPoint pts);
extern DELPHI_PACKAGE Gr32::TArrayOfArrayOfFloatPoint __fastcall AAFixedToAAFloat(Gr32::TArrayOfArrayOfFixedPoint ppts);
extern DELPHI_PACKAGE Gr32::TArrayOfFixedPoint __fastcall AFloatToAFixed(Gr32::TArrayOfFloatPoint pts);
extern DELPHI_PACKAGE Gr32::TArrayOfArrayOfFixedPoint __fastcall AAFloatToAAFixed(Gr32::TArrayOfArrayOfFloatPoint ppts);
extern DELPHI_PACKAGE Gr32::TArrayOfFixedPoint __fastcall MakeArrayOfFixedPoints(const float *a, const int a_High)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfFixedPoint __fastcall MakeArrayOfFixedPoints(const System::Types::TRect &rec)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfFixedPoint __fastcall MakeArrayOfFixedPoints(const Gr32::TFloatRect &rec)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfFixedPoint __fastcall MakeArrayOfFixedPoints(const Gr32::TFixedRect &rec)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfFixedPoint __fastcall MakeArrayOfFixedPoints(const Gr32::TArrayOfFloatPoint a)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfFloatPoint __fastcall MakeArrayOfFloatPoints(const float *a, const int a_High)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfFloatPoint __fastcall MakeArrayOfFloatPoints(const Gr32::TArrayOfFixedPoint a)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfArrayOfFloatPoint __fastcall MakeArrayOfArrayOfFloatPoints(const Gr32::TArrayOfArrayOfFixedPoint a);
extern DELPHI_PACKAGE Gr32::TArrayOfFloat __fastcall MakeArrayOfFloat(const float *a, const int a_High);
extern DELPHI_PACKAGE void __fastcall OffsetFixedRect(Gr32::TFixedRect &rec, float dx, float dy);
extern DELPHI_PACKAGE void __fastcall OffsetFloatRect(Gr32::TFloatRect &rec, float dx, float dy);
extern DELPHI_PACKAGE bool __fastcall IsDuplicatePoint(const Gr32::TFixedPoint &p1, const Gr32::TFixedPoint &p2)/* overload */;
extern DELPHI_PACKAGE bool __fastcall IsDuplicatePoint(const Gr32::TFloatPoint &p1, const Gr32::TFloatPoint &p2)/* overload */;
extern DELPHI_PACKAGE bool __fastcall IsDuplicateRect(const Gr32::TFloatRect &rec1, const Gr32::TFloatRect &rec2);
extern DELPHI_PACKAGE Gr32::TArrayOfFixedPoint __fastcall StripDuplicatePoints(const Gr32::TFixedPoint *pts, const int pts_High)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfFixedPoint __fastcall StripDuplicatePoints(const Gr32::TArrayOfFixedPoint pts)/* overload */;
extern DELPHI_PACKAGE Gr32::TFixedPoint __fastcall GetPointAtAngleFromPoint(const Gr32::TFixedPoint &pt, const float dist, const float radians);
extern DELPHI_PACKAGE float __fastcall SquaredDistBetweenPoints(const Gr32::TFixedPoint &pt1, const Gr32::TFixedPoint &pt2)/* overload */;
extern DELPHI_PACKAGE float __fastcall SquaredDistBetweenPoints(const Gr32::TFloatPoint &pt1, const Gr32::TFloatPoint &pt2)/* overload */;
extern DELPHI_PACKAGE float __fastcall DistBetweenPoints(const Gr32::TFixedPoint &pt1, const Gr32::TFixedPoint &pt2)/* overload */;
extern DELPHI_PACKAGE float __fastcall DistBetweenPoints(const Gr32::TFloatPoint &pt1, const Gr32::TFloatPoint &pt2)/* overload */;
extern DELPHI_PACKAGE Gr32::TFloatPoint __fastcall ClosestPointOnLine(const Gr32::TFloatPoint &pt, const Gr32::TFloatPoint &lnA, const Gr32::TFloatPoint &lnB, bool ForceResultBetweenLinePts);
extern DELPHI_PACKAGE float __fastcall DistOfPointFromLine(const Gr32::TFloatPoint &pt, const Gr32::TFloatPoint &lnA, const Gr32::TFloatPoint &lnB, bool ForceResultBetweenLinePts)/* overload */;
extern DELPHI_PACKAGE float __fastcall DistOfPointFromLine(const Gr32::TFixedPoint &pt, const Gr32::TFixedPoint &lnA, const Gr32::TFixedPoint &lnB, bool ForceResultBetweenLinePts)/* overload */;
extern DELPHI_PACKAGE bool __fastcall IntersectionPoint(const Gr32::TFixedPoint &ln1A, const Gr32::TFixedPoint &ln1B, const Gr32::TFixedPoint &ln2A, const Gr32::TFixedPoint &ln2B, /* out */ Gr32::TFixedPoint &IntersectPoint);
extern DELPHI_PACKAGE Gr32::TFixedPoint __fastcall RotatePoint(const Gr32::TFixedPoint &pt, const Gr32::TFixedPoint &origin, const float radians);
extern DELPHI_PACKAGE Gr32::TArrayOfFixedPoint __fastcall RotatePoints(const Gr32::TFixedPoint *pts, const int pts_High, const Gr32::TFixedPoint &origin, float radians)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfFixedPoint __fastcall RotatePoints(const Gr32::TFixedPoint *pts, const int pts_High, float radians)/* overload */;
extern DELPHI_PACKAGE void __fastcall RotatePolyPoints(Gr32::TArrayOfArrayOfFixedPoint &pts, const Gr32::TFixedPoint &origin, float radians);
extern DELPHI_PACKAGE Gr32::TFixedPoint __fastcall MidPoint(const Gr32::TFixedPoint &pt1, const Gr32::TFixedPoint &pt2);
extern DELPHI_PACKAGE float __fastcall GetAngleOfPt2FromPt1(const Gr32::TFixedPoint &pt1, const Gr32::TFixedPoint &pt2)/* overload */;
extern DELPHI_PACKAGE float __fastcall GetAngleOfPt2FromPt1(const Gr32::TFloatPoint &pt1, const Gr32::TFloatPoint &pt2)/* overload */;
extern DELPHI_PACKAGE bool __fastcall PtInPolygon(const Gr32::TFixedPoint &Pt, const Gr32::TFixedPoint *Pts, const int Pts_High)/* overload */;
extern DELPHI_PACKAGE bool __fastcall PtInPolygon(const Gr32::TFloatPoint &pt, const Gr32::TArrayOfFloatPoint Pts)/* overload */;
extern DELPHI_PACKAGE bool __fastcall FixedPtInRect(const Gr32::TFixedRect &R, const Gr32::TFixedPoint &P);
extern DELPHI_PACKAGE System::Types::TPoint __fastcall MakePoint(const Gr32::TFixedPoint &pt)/* overload */;
extern DELPHI_PACKAGE System::Types::TPoint __fastcall MakePoint(const Gr32::TFloatPoint &pt)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfFloatPoint __fastcall FloatPoints(const Gr32::TArrayOfFixedPoint pts)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfFixedPoint __fastcall FixedPoints(const Gr32::TArrayOfFloatPoint pts)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfArrayOfFloatPoint __fastcall FloatPoints(const Gr32::TArrayOfArrayOfFixedPoint ppts)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfArrayOfFixedPoint __fastcall FixedPoints(const Gr32::TArrayOfArrayOfFloatPoint ppts)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfFixedPoint __fastcall CopyPoints(const Gr32::TArrayOfFixedPoint pts);
extern DELPHI_PACKAGE System::Types::TRect __fastcall GetBoundsRect(Gr32::TFixedPoint *pts, const int pts_High);
extern DELPHI_PACKAGE Gr32::TFixedRect __fastcall GetBoundsFixedRect(Gr32::TFixedPoint *pts, const int pts_High)/* overload */;
extern DELPHI_PACKAGE Gr32::TFixedRect __fastcall GetBoundsFixedRect(Gr32::TArrayOfArrayOfFixedPoint polyPts)/* overload */;
extern DELPHI_PACKAGE Gr32::TFloatRect __fastcall GetBoundsFloatRect(Gr32::TFixedPoint *pts, const int pts_High)/* overload */;
extern DELPHI_PACKAGE Gr32::TFloatRect __fastcall GetBoundsFloatRect(Gr32::TFloatPoint *pts, const int pts_High)/* overload */;
extern DELPHI_PACKAGE Gr32::TFixedRect __fastcall GetRectUnion(const Gr32::TFixedRect &rec1, const Gr32::TFixedRect &rec2);
extern DELPHI_PACKAGE Gr32::TArrayOfFixedPoint __fastcall GetConvexHull(const Gr32::TArrayOfFixedPoint polygon);
extern DELPHI_PACKAGE Gr32::TBitmap32* __fastcall CreateMaskFromPolygon(Gr32::TBitmap32* bitmap, const Gr32::TArrayOfFixedPoint polygon)/* overload */;
extern DELPHI_PACKAGE Gr32::TBitmap32* __fastcall CreateMaskFromPolygon(Gr32::TBitmap32* bitmap, const Gr32::TArrayOfArrayOfFixedPoint polygons)/* overload */;
extern DELPHI_PACKAGE void __fastcall ApplyMaskToAlpha(Gr32::TBitmap32* bitmap, Gr32::TBitmap32* mask, bool invertMask = false);
extern DELPHI_PACKAGE void __fastcall ApplyMask(Gr32::TBitmap32* modifiedBmp, Gr32::TBitmap32* originalBmp, Gr32::TBitmap32* maskBmp, bool invertMask = false);
extern DELPHI_PACKAGE void __fastcall InvertMask(Gr32::TBitmap32* maskBmp);
extern DELPHI_PACKAGE Gr32::TArrayOfColor32 __fastcall MakeArrayOfColor32(Gr32::TColor32 *colors, const int colors_High);
extern DELPHI_PACKAGE Gr32::TArrayOfFixedPoint __fastcall SmoothChart(const Gr32::TFixedPoint *chartPts, const int chartPts_High, int xStep);
extern DELPHI_PACKAGE Gr32::TArrayOfFixedPoint __fastcall GetCBezierPoints(const Gr32::TFixedPoint *control_points, const int control_points_High);
extern DELPHI_PACKAGE Gr32::TArrayOfFixedPoint __fastcall GetCSplinePoints(const Gr32::TFixedPoint *control_points, const int control_points_High);
extern DELPHI_PACKAGE Gr32::TArrayOfFixedPoint __fastcall GetQBezierPoints(const Gr32::TFixedPoint *control_points, const int control_points_High);
extern DELPHI_PACKAGE Gr32::TArrayOfFixedPoint __fastcall GetQSplinePoints(const Gr32::TFixedPoint *control_points, const int control_points_High);
extern DELPHI_PACKAGE Gr32::TArrayOfFixedPoint __fastcall GetEllipsePoints(const Gr32::TFloatRect &ellipseRect);
extern DELPHI_PACKAGE Gr32::TFloatPoint __fastcall GetPtOnEllipseFromAngleEccentric(const Gr32::TFloatRect &ellipseRect, float eccentric_angle_radians);
extern DELPHI_PACKAGE Gr32::TArrayOfFixedPoint __fastcall GetPointsAroundEllipse(const Gr32::TFloatRect &ellipseRect, int PointCount);
extern DELPHI_PACKAGE Gr32::TArrayOfFixedPoint __fastcall GetArcPoints(const Gr32::TFloatRect &ellipseRect, const Gr32::TFloatPoint &startPt, const Gr32::TFloatPoint &endPt)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfFixedPoint __fastcall GetArcPoints(const Gr32::TFloatRect &ellipseRect, float start_degrees, float end_degrees)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfFixedPoint __fastcall GetArcPointsEccentric(const Gr32::TFloatRect &ellipseRect, float start_eccentric, float end_eccentric);
extern DELPHI_PACKAGE Gr32::TArrayOfFixedPoint __fastcall GetPiePoints(const Gr32::TFloatRect &ellipseRect, const Gr32::TFloatPoint &startPt, const Gr32::TFloatPoint &endPt)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfFixedPoint __fastcall GetPiePoints(const Gr32::TFloatRect &ellipseRect, float start_degrees, float end_degrees)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfFixedPoint __fastcall GetPiePointsEccentric(const Gr32::TFloatRect &ellipseRect, float start_eccentric, float end_eccentric);
extern DELPHI_PACKAGE Gr32::TArrayOfFixedPoint __fastcall GetRoundedRectanglePoints(const Gr32::TFloatRect &rect, unsigned roundingPercent);
extern DELPHI_PACKAGE Gr32::TArrayOfFixedPoint __fastcall GetBalloonedEllipsePoints(const Gr32::TFloatRect &ellipseRect, TBalloonPos balloonPos);
extern DELPHI_PACKAGE Gr32::TArrayOfFixedPoint __fastcall GetPipeCornerPoints(const Gr32::TFloatRect &rec, float pipeWidth, TCorner corner);
extern DELPHI_PACKAGE Gr32::TArrayOfFixedPoint __fastcall GetStarPoints(const Gr32::TFixedPoint &Center, int PointCount, float radius1, float radius2);
extern DELPHI_PACKAGE Gr32::TColor32 __fastcall GradientColor(Gr32::TColor32 color1, Gr32::TColor32 color2, float frac);
extern DELPHI_PACKAGE Gr32::TColor32 __fastcall GetColor(const Gr32::TColor32 *colors, const int colors_High, float fraction);
extern DELPHI_PACKAGE bool __fastcall IsClockwise(const Gr32::TArrayOfFixedPoint pts)/* overload */;
extern DELPHI_PACKAGE bool __fastcall IsClockwise(const Gr32::TArrayOfFloatPoint pts)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfArrayOfFixedPoint __fastcall BuildDashedLine(const Gr32::TArrayOfFixedPoint Points, const float *DashArray, const int DashArray_High, float DashOffset = 0.000000E+00f);
extern DELPHI_PACKAGE Gr32::TFloatPoint __fastcall GetUnitVector(const Gr32::TFixedPoint &pt1, const Gr32::TFixedPoint &pt2);
extern DELPHI_PACKAGE Gr32::TFloatPoint __fastcall GetUnitNormal(const Gr32::TFixedPoint &pt1, const Gr32::TFixedPoint &pt2)/* overload */;
extern DELPHI_PACKAGE Gr32::TFloatPoint __fastcall GetUnitNormal(const Gr32::TFloatPoint &pt1, const Gr32::TFloatPoint &pt2)/* overload */;
extern DELPHI_PACKAGE void __fastcall SimpleLine(Gr32::TBitmap32* bitmap, const Gr32::TFixedPoint *pts, const int pts_High, Gr32::TColor32 color, bool closed = false)/* overload */;
extern DELPHI_PACKAGE void __fastcall SimpleLine(Gr32::TBitmap32* bitmap, const Gr32::TArrayOfArrayOfFixedPoint ppts, Gr32::TColor32 color, bool closed)/* overload */;
extern DELPHI_PACKAGE void __fastcall SimpleFill(Gr32::TBitmap32* bitmap, Gr32::TFixedPoint *pts, const int pts_High, Gr32::TColor32 edgeColor, Gr32::TColor32 fillColor, Gr32_polygons_old::TPolyFillModeOld aFillMode = (Gr32_polygons_old::TPolyFillModeOld)(0x1))/* overload */;
extern DELPHI_PACKAGE void __fastcall SimpleFill(Gr32::TBitmap32* bitmap, const Gr32::TArrayOfArrayOfFixedPoint pts, Gr32::TColor32 edgeColor, Gr32::TColor32 fillColor, Gr32_polygons_old::TPolyFillModeOld aFillMode = (Gr32_polygons_old::TPolyFillModeOld)(0x1))/* overload */;
extern DELPHI_PACKAGE void __fastcall SimpleFill(Gr32::TBitmap32* bitmap, const Gr32::TFloatPoint *pts, const int pts_High, Gr32::TColor32 edgeColor, Gr32::TColor32 fillColor, Gr32_polygons_old::TPolyFillModeOld aFillMode = (Gr32_polygons_old::TPolyFillModeOld)(0x1))/* overload */;
extern DELPHI_PACKAGE void __fastcall SimpleFill(Gr32::TBitmap32* bitmap, const Gr32::TArrayOfArrayOfFloatPoint pts, Gr32::TColor32 edgeColor, Gr32::TColor32 fillColor, Gr32_polygons_old::TPolyFillModeOld aFillMode = (Gr32_polygons_old::TPolyFillModeOld)(0x1))/* overload */;
extern DELPHI_PACKAGE void __fastcall SimpleFill(Gr32::TBitmap32* bitmap, const Gr32::TFixedPoint *pts, const int pts_High, Gr32::TColor32 edgeColor, Gr32::TBitmap32* pattern, Gr32_polygons_old::TPolyFillModeOld aFillMode = (Gr32_polygons_old::TPolyFillModeOld)(0x1))/* overload */;
extern DELPHI_PACKAGE void __fastcall SimpleFill(Gr32::TBitmap32* bitmap, const Gr32::TArrayOfArrayOfFixedPoint pts, Gr32::TColor32 edgeColor, Gr32::TBitmap32* pattern, Gr32_polygons_old::TPolyFillModeOld aFillMode = (Gr32_polygons_old::TPolyFillModeOld)(0x1))/* overload */;
extern DELPHI_PACKAGE void __fastcall SimpleGradientFill(Gr32::TBitmap32* bitmap, Gr32::TFixedPoint *pts, const int pts_High, Gr32::TColor32 edgeColor, const Gr32::TColor32 *colors, const int colors_High, int angle_degrees)/* overload */;
extern DELPHI_PACKAGE bool __fastcall NearlyMatch(const float s1, const float s2, const float tolerance);
extern DELPHI_PACKAGE void __fastcall SimpleGradientFill(Gr32::TBitmap32* bitmap, Gr32::TArrayOfArrayOfFixedPoint pts, Gr32::TColor32 edgeColor, const Gr32::TColor32 *colors, const int colors_High, int angle_degrees)/* overload */;
extern DELPHI_PACKAGE void __fastcall SimpleStippleFill(Gr32::TBitmap32* bitmap, Gr32::TFixedPoint *pts, const int pts_High, const Gr32::TColor32 *colors, const int colors_High, float step, int angle_degrees);
extern DELPHI_PACKAGE void __fastcall SimpleRadialFill(Gr32::TBitmap32* bitmap, const Gr32::TFixedPoint &pt, float radius, const Gr32::TColor32 *colors, const int colors_High)/* overload */;
extern DELPHI_PACKAGE void __fastcall SimpleRadialFill(Gr32::TBitmap32* bitmap, const Gr32::TFloatRect &rec, const Gr32::TColor32 *colors, const int colors_High)/* overload */;
extern DELPHI_PACKAGE void __fastcall SimpleRadialFill(Gr32::TBitmap32* bitmap, const Gr32::TFixedPoint *pts, const int pts_High, const Gr32::TColor32 *colors, const int colors_High, int angle_degrees = 0x0)/* overload */;
extern DELPHI_PACKAGE void __fastcall SimpleRadialFill(Gr32::TBitmap32* bitmap, const Gr32::TArrayOfArrayOfFixedPoint ppts, const Gr32::TColor32 *colors, const int colors_High, int angle_degrees = 0x0)/* overload */;
extern DELPHI_PACKAGE void __fastcall SimpleShadow(Gr32::TBitmap32* bitmap, const Gr32::TArrayOfFixedPoint pts, int dx, int dy, int fadeRate, Gr32::TColor32 shadowColor, bool closed = false, bool NoInteriorBleed = false)/* overload */;
extern DELPHI_PACKAGE void __fastcall SimpleShadow(Gr32::TBitmap32* bitmap, const Gr32::TArrayOfArrayOfFixedPoint pts, int dx, int dy, int fadeRate, Gr32::TColor32 shadowColor, bool closed = false, bool NoInteriorBleed = false)/* overload */;
extern DELPHI_PACKAGE void __fastcall Simple3D(Gr32::TBitmap32* bitmap, const Gr32::TArrayOfArrayOfFloatPoint ppts, int dx, int dy, int fadeRate, Gr32::TColor32 topLeftColor, Gr32::TColor32 bottomRightColor)/* overload */;
extern DELPHI_PACKAGE void __fastcall Simple3D(Gr32::TBitmap32* bitmap, const Gr32::TArrayOfFixedPoint pts, int dx, int dy, int fadeRate, Gr32::TColor32 topLeftColor, Gr32::TColor32 bottomRightColor)/* overload */;
extern DELPHI_PACKAGE void __fastcall Simple3D(Gr32::TBitmap32* bitmap, const Gr32::TArrayOfArrayOfFixedPoint pts, int dx, int dy, int fadeRate, Gr32::TColor32 topLeftColor, Gr32::TColor32 bottomRightColor)/* overload */;
extern DELPHI_PACKAGE void __fastcall SimpleBevel(Gr32::TBitmap32* bitmap, Gr32::TArrayOfArrayOfFixedPoint ppts, int strokeWidth, Gr32::TColor32 lightClr, Gr32::TColor32 darkClr, int angle_degrees);
}	/* namespace Gr32_misc */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_GR32_MISC)
using namespace Gr32_misc;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// Gr32_miscHPP
