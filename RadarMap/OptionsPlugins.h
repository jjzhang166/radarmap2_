//---------------------------------------------------------------------------

#ifndef OptionsPluginsH
#define OptionsPluginsH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <Graphics.hpp>
#include <pngimage.hpp>
#include <ComCtrls.hpp>
#include <ImgList.hpp>
#include <ImageButton.h>
#include "CheckBoxTreeView.h"
#include "PlugIns.h"
//---------------------------------------------------------------------------
class TOptionsItem : public TObject
{
private:
	TOption *option;
	TWinControl *owner;
	TPanel *panel, *panel2;
	TLabel *label;
	TWinControl* control;
	char value[OptionLength];
    AnsiString __fastcall ExtractLink(AnsiString str);
	void __fastcall OnLabelClick(TObject *Sender);
protected:
	int __fastcall readTop() {if(panel) return panel->Top; else return -100;}
	void __fastcall writeTop(int value) {if(panel) panel->Top=value;}
	char* __fastcall readValue();
	bool __fastcall readVisible() {if(panel) return panel->Visible; else return false;}
	int __fastcall readHeight() {if(panel) return panel->Height; else return 0;}
	void __fastcall writeVisible(bool value) {if(panel) panel->Visible=value;}
	void __fastcall EditKeyPress(TObject *Sender, wchar_t &Key);
	void __fastcall EditExit(TObject *Sender);
public:
	__fastcall TOptionsItem(TOption *aOption, TWinControl* aOwner);
	__fastcall ~TOptionsItem() {if(label) delete label; if(control) delete control; if(panel2) delete panel2; if(panel) delete panel;}

	void __fastcall Apply();

	__property TOption *Option = {read=option};
	__property TWinControl* Owner={read=owner};
	__property char* Value = {read=readValue};
	__property int Top = {read=readTop, write=writeTop};
	__property int Visible = {read=readVisible, write=writeVisible};
	__property int Height = {read=readHeight};
};

class TOptionsPluginsForm : public TForm
{
__published:	// IDE-managed Components
	TPanel *Panel1;
	TPanel *Panel2;
	TPanel *Panel3;
	TPanel *Panel4;
	TPanel *ClientPanel;
	TPanel *TitlePanel;
	TLabel *Label1;
	TPanel *Panel8;
	TPanel *Panel9;
	TImageButton *CloseIButton;
	TPanel *Panel5;
	TPanel *Panel6;
	TPanel *Panel7;
	TPanel *PagesPanel;
	TPanel *OkCancelPanel;
	TImageButton *CancelImageButton;
	TImageButton *OkImageButton;
	TEdit *TempEdit;
	TScrollBox *ScrollBox1;
	TPanel *Panel10;
	void __fastcall OkImageClick(TObject *Sender);
	void __fastcall CancelImageClick(TObject *Sender);
	void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
	void __fastcall FormShow(TObject *Sender);
	void __fastcall TitlePanelMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
	void __fastcall TitlePanelMouseMove(TObject *Sender, TShiftState Shift, int X, int Y);
	void __fastcall TitlePanelMouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);

private:	// User declarations
	int PanelsN;
	TList* Items;
	TPlugInObject* PlugIn;
	bool TitleDown;
	Types::TPoint TitleXY;
	void __fastcall Clear();
public:		// User declarations
	__fastcall TOptionsPluginsForm(TComponent* Owner);
	__fastcall ~TOptionsPluginsForm();
	void __fastcall GetPlugInOptions(TPlugInObject* aPlugIn);
	TCheckBoxTreeView *TreeView;
};
//---------------------------------------------------------------------------
extern PACKAGE TOptionsPluginsForm *OptionsPluginsForm;
//---------------------------------------------------------------------------
#endif
