#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <math.h>

#pragma hdrstop

#include "Profile.h"
#include "Defines.h"
#include "MyGraph.h"

//##############################################################
//---------- Constructor TTrace --------------
__fastcall TTrace::TTrace(int new_samples, TProfile* AOwner, bool aApplyValidity)
{
	owner=AOwner;
	cachedbyowner=false;
	samples=new_samples;
	//BusyLocker=new TBusyLocker();
	BusyLocker=NULL;
	private_data=NULL;
	private_sourcedata=NULL;
	private_filterdata=NULL;
	private_data=AllocateSamples(samples);
	private_sourcedata=AllocateSamples(samples);
	//filterdata=NULL;//new TSample[samples];
	DataCache=NULL;
	SourceCache=NULL;
	FilterCache=NULL;
	for(int i=0; i<samples; i++)
		data[i]=sourcedata[i]=0; // filterdata[i]=0;
	X=Y=0;
	Pitch=TFlagedValue<float>();
	Roll=TFlagedValue<float>();
	Heading=TFlagedValue<float>();
	TraceDistance=0.;
	Mark=0;
	MaxInTrace=0;
	Latitude=Longitude=Z_Geoid=Z=GPSConfidence=LocalCsX=LocalCsY=LocalCsH=0.0;
	LocalCsCh1=LocalCsCh2=0;
	GreenwichTm=Now();
	TimeStamp=Now();
	GPSCoordinated=GPSInterpolated=false;
	SparDataPtr=NULL;
	Drawed=Drawed2=Backward=Saved=false;
	SamplesShifting=0;
	Channel=0;
	Index=-1;
	horpos=0.;
	ptrup=ptrdn=NULL;
	PtrUp=NULL;
	PtrDn=NULL;
	ApplyValidity=aApplyValidity;
	if(ApplyValidity) CalcValidity();

	lastaccess=Gr32_system::GetTickCount();

	PositiveMax=(1<<15) - 1;
	NegativeMax=-((1<<15) - 1);
}

//--------- Destructor ~TScan ----------------
__fastcall TTrace::~TTrace()
{
	try
	{
		if(BusyLocker)
		{
			BusyLocker->UnLock();
			delete BusyLocker;
			BusyLocker=NULL;
		}
		ReleaseSamples(private_data);
		private_data=NULL;
		if(DataCache) delete DataCache;
		DataCache=NULL;
		ReleaseSamples(private_sourcedata);
		private_sourcedata=NULL;
		if(SourceCache) delete SourceCache;
		SourceCache=NULL;
		ReleaseFilterData();
		if(FilterCache) delete FilterCache;
		FilterCache=NULL;
		if(SparDataPtr) delete SparDataPtr;
		SparDataPtr=NULL;
	}
	catch(Exception &exception) {;}
}

TSample* __fastcall TTrace::readPrivateData()
{
	TSample* res=NULL;

	if(!BusyLocker || BusyLocker->SequentLock())
	{
		try
		{
			try
			{
				if(private_data) res=private_data;
				else if(CacheManager)
				{
					if(DataCache)
					{
						void* d=(TCacheArrayObject*)CacheManager->Restore(DataCache);
						if(d)
						{
							private_data=(TSample*)d;
							delete DataCache;
							DataCache=NULL;
							res=private_data;
						}
					}
					else if(owner && cachedbyowner)
					{
						private_data=AllocateSamples(samples);
						owner->Restore(); //!!! May be locked by BusyLocker...
						res=private_data;
					}
					CalcValidity();
				}
			}
			catch(...) {}
		}
		__finally
		{
			if(BusyLocker) BusyLocker->UnLock();
		}
	}
	return res;
}

TSample* __fastcall TTrace::readPrivateSourceData()
{
	TSample* res=NULL;

	if(!BusyLocker || BusyLocker->SequentLock())
	{
		try
		{
			try
			{
				if(private_sourcedata) res=private_sourcedata;
				else if(CacheManager)
				{
					if(SourceCache)
					{
						void* d=(TCacheArrayObject*)CacheManager->Restore(SourceCache);
						if(d)
						{
							private_sourcedata=(TSample*)d;
							delete SourceCache;
							SourceCache=NULL;
							res=private_sourcedata;
						}
					}
					else if(owner && cachedbyowner)
					{
						private_sourcedata=AllocateSamples(samples);
						owner->Restore(); //!!! May be locked by BusyLocker...
						res=private_sourcedata;
					}
					CalcValidity();
				}
			}
			catch(...) {}
		}
		__finally
		{
			if(BusyLocker) BusyLocker->UnLock();
		}
	}
	return res;
}

TSample* __fastcall TTrace::readPrivateFilterData()
{
	void* d;
	TSample* res=NULL;

	if(!private_filterdata)
	{
		if(CacheManager)
		{
			if(FilterCache)
			{
				d=(TCacheArrayObject*)CacheManager->Restore(FilterCache);
				if(d)
				{
					private_filterdata=(TSample*)d;
					delete FilterCache;
					FilterCache=NULL;
					res=private_filterdata;
				}
			}
			else
			{
				private_filterdata=AllocateSamples(samples);
				for(int i=0; i<samples; i++) private_filterdata[i]=0;
				res=private_filterdata;
			}
			CalcValidity();
		}
	}
	return res;
}

TSample* __fastcall TTrace::AllocateSamples(int N, TProfile* NewOwner)
{
	TSample *Out;
#ifdef ALLOCATE_SAMPLES_NEW
	Out=new TSample[N];
#else
  #ifdef ALLOCATE_SAMPLES_HEAP
	HANDLE h;
	if(NewOwner && NewOwner->hHeap) h=NewOwner->hHeap;
	else if(owner && owner->hHeap) h=owner->hHeap;
	else h=GetProcessHeap();
	Out=(TSample*)HeapAlloc(h, HEAP_GENERATE_EXCEPTIONS | HEAP_ZERO_MEMORY, (SIZE_T)N*sizeof(TSample));
  #else
	Out=NULL;
  #endif
#endif
	return Out;
}

void __fastcall TTrace::ReleaseSamples(TSample* Array)
{
	if(Array)
	{
#ifdef ALLOCATE_SAMPLES_NEW
		delete[] Array;
#else
		//VirtualFree(Array, 0, MEM_RELEASE);
		if(owner) HeapFree(owner->hHeap, 0, (LPVOID)Array);
		else HeapFree(GetProcessHeap(), 0, (LPVOID)Array);
#endif
	}
}

void __fastcall TTrace::SetOwner(TProfile* AOwner)
{
	if(owner!=AOwner)
	{
#ifdef ALLOCATE_SAMPLES_HEAP
		TSample *d;
		if(private_data)
		{
			d=private_data;
			private_data=AllocateSamples(samples, AOwner);
			memcpy(private_data, d, samples*sizeof(TSample));
			try	{ ReleaseSamples(d); } catch(...) {}
		}
		if(private_sourcedata)
		{
			d=private_sourcedata;
			private_sourcedata=AllocateSamples(samples, AOwner);
			memcpy(private_sourcedata, d, samples*sizeof(TSample));
			try	{ ReleaseSamples(d); } catch(...) {}
		}
		if(private_filterdata)
		{
			d=private_filterdata;
			private_filterdata=AllocateSamples(samples, AOwner);
			memcpy(private_filterdata, d, samples*sizeof(TSample));
			try	{ ReleaseSamples(d); } catch(...) {}
		}
		CalcValidity();
#endif
		owner=AOwner;
	}
}

void __fastcall TTrace::ReleaseFilterData()
{
	if(private_filterdata)
	{
		ReleaseSamples(private_filterdata);
		private_filterdata=NULL;
		CalcValidity();
	}
}

void __fastcall TTrace::CacheIt(bool ProfileCache)
{
	if(owner && owner->CachingIsProhibited) return;
	CacheIt(tatData, ProfileCache);
	CacheIt(tatSource, ProfileCache);
	CacheIt(tatFilter, ProfileCache);
}

void __fastcall TTrace::CacheIt(TTraceArrayType t, bool ProfileCache)
{
	if(owner && owner->CachingIsProhibited) return;
	if(CacheManager && (!BusyLocker || BusyLocker->SequentLock()))
	{
		try
		{
			try
			{
				switch(t)
				{
					case tatData:
						if(private_data && !DataCache)
						{
							if(!ProfileCache)
							{
								DataCache=(TCacheArrayObject*)CacheManager->CacheIt(cotArray, (void*)private_data, samples*sizeof(TSample), (TSynchronizeMethod)NULL);
								if(DataCache) private_data=NULL;
							}
							else
							{
								ReleaseSamples(private_data);
								private_data=NULL;
								cachedbyowner=true;
							}
						}
						break;
					case tatSource:
						if(private_sourcedata && !SourceCache)
						{
							if(!ProfileCache)
							{
								SourceCache=(TCacheArrayObject*)CacheManager->CacheIt(cotArray, (void*)private_sourcedata, samples*sizeof(TSample), (TSynchronizeMethod)NULL);
								if(SourceCache) private_sourcedata=NULL;
							}
							else
							{
								ReleaseSamples(private_sourcedata);
								private_sourcedata=NULL;
								cachedbyowner=true;
							}
						}
						break;
					case tatFilter:
						if(private_filterdata && !FilterCache)
						{
							if(!ProfileCache)
							{
								FilterCache=(TCacheArrayObject*)CacheManager->CacheIt(cotArray, (void*)private_filterdata, samples*sizeof(TSample), (TSynchronizeMethod)NULL);
								if(FilterCache)	private_filterdata=NULL;
							}
							else
							{
								ReleaseSamples(private_filterdata);
								private_filterdata=NULL;
								cachedbyowner=true;
							}
						}
						break;
				}
				CalcValidity();
			}
			catch(...) {}
		}
		__finally
		{
			if(BusyLocker) BusyLocker->UnLock();
		}
	}
}

void __fastcall TTrace::AssignTraceArray(TTraceArrayType t, TSample *In)
{
	switch(t)
	{
		case tatData: private_data=In; cachedbyowner=false; if(DataCache) delete DataCache; DataCache=NULL; break;
		case tatSource: private_sourcedata=In; cachedbyowner=false; if(SourceCache) delete SourceCache; SourceCache=NULL; break;
		case tatFilter: private_filterdata=In; cachedbyowner=false; if(FilterCache) delete FilterCache; FilterCache=NULL; break;
	}
	CalcValidity();
}

void __fastcall TTrace::writeSamples(short int value)
{
	if(samples!=value)
	{
		TSample *d, *s, *f;

		d=data;
		data=AllocateSamples(value);
		ReSample(d, data, value);
		try	{ ReleaseSamples(d); } catch(...) {}
		s=sourcedata;
		sourcedata=AllocateSamples(value);
		ReSample(s, sourcedata, value);
		try	{ ReleaseSamples(s); } catch(...) {}
		if(FilterDataExists)
		{
			f=filterdata;
			ReSample(f, filterdata, value);
			filterdata=AllocateSamples(value);
			try	{ ReleaseSamples(f); } catch(...) {}
		}
		if(SparDataPtr) delete SparDataPtr;
		samples=value;
	}
}

TSample* __fastcall TTrace::GetArrayAddress(TTraceArrayType t)  // Unsafe !!!
{
	switch(t)
	{
		case tatData: return data;
		case tatSource: return sourcedata;
		case tatFilter: return filterdata;
		default: return NULL;
	}
}

void __fastcall TTrace::ReSample(TSample *In, TSample *Out, int NewSamples)
{
	double v, coef, f;
	int i=0;

	if(In && Out && NewSamples>0 && NewSamples!=Samples)
	{
		try
		{
			coef=(double)Samples/(double)NewSamples;
			if(coef<1.) v=Out[0]=In[0];
			else v=0.;
			for(f=1, i=0; f<(double)Samples; f+=coef, i++)
			{
				if(coef<1.)
				{
					v+=(In[(int)f+1]-In[(int)f])*coef;
				}
				else
				{
					v+=(In[(int)f+1]-v)/coef;
					for(int j=1; j<coef; j++)
						v+=(In[(int)f+j+1]-In[(int)f+j])/coef;
					v+=(In[(int)(f+coef)+1]-In[(int)(f+coef)])/coef;
				}
				if(v>PositiveMax) v=PositiveMax;
				else if(v<NegativeMax) v=NegativeMax;
				Out[i]=v;
			}
		}
		catch(...) {}
	}
}

bool __fastcall TTrace::CopyTraceDataOnly(TTrace *Src)
{
	if(Samples==Src->Samples)
	{
		if(DataCache) delete DataCache;
		DataCache=NULL;
		ReleaseSamples(private_data);
		private_data=AllocateSamples(samples);
		memcpy(private_data, Src->GetArrayAddress(tatData), samples*sizeof(TSample));
		if(SourceCache) delete SourceCache;
		SourceCache=NULL;
		ReleaseSamples(private_sourcedata);
		private_sourcedata=AllocateSamples(samples);
		memcpy(private_sourcedata, Src->GetArrayAddress(tatSource), samples*sizeof(TSample));
		if(Src->FilterDataExists)
		{
			if(FilterCache) delete FilterCache;
			FilterCache=NULL;
			ReleaseFilterData();
			private_filterdata=AllocateSamples(samples);
			memcpy(private_filterdata, Src->GetArrayAddress(tatFilter), samples*sizeof(TSample));
		}
		CalcValidity();
		return true;
	}
	else return false;
}

bool __fastcall TTrace::CopyTrace(TTrace *Src, bool InvertData)
{
	int sn;
	if(Samples==Src->Samples)
	{
		sn=1-2*(int)InvertData;
		/*for(int i=0; i<Samples; i++)
		{
			Data[i]=Src->Data[i]*sn;
			SourceData[i]=Src->SourceData[i]*sn;
			if(Src->FilterDataExists)
				FilterData[i]=Src->FilterData[i]*sn;
		}*/
		CopyTraceDataOnly(Src);
		if(InvertData)
		{
			for(int i=0; i<Samples; i++)
			{
				if(private_data) private_data[i]*=sn;
				if(private_sourcedata) private_sourcedata[i]*=sn;
				if(private_filterdata) private_filterdata[i]*=sn;
			}
		}
		X=Src->X;
		Y=Src->Y;
		Z=Src->Z;
		Pitch=Src->Pitch;
		Roll=Src->Roll;
		Heading=Src->Heading;
		Z_Geoid=Src->Z_Geoid;
		TraceDistance=Src->TraceDistance;
		Mark=Src->Mark;
		MaxInTrace=Src->MaxInTrace;
		Latitude=Src->Latitude;
		Longitude=Src->Longitude;
		LocalCsX=Src->LocalCsX;
		LocalCsY=Src->LocalCsY;
		LocalCsH=Src->LocalCsH;
		LocalCsCh1=Src->LocalCsCh1;
		LocalCsCh2=Src->LocalCsCh2;
		GreenwichTm=Src->GreenwichTm;
		TimeStamp=Src->TimeStamp;
		GPSCoordinated=Src->GPSCoordinated;
		if(Src->SparDataPtr)
		{
			if(!SparDataPtr) SparDataPtr=new Spar;
			*SparDataPtr=*(Src->SparDataPtr);
		}
		else
		{
			if(SparDataPtr)
			{
				delete SparDataPtr;
				SparDataPtr=NULL;
			}
		}
		GPSInterpolated=Src->GPSInterpolated;
		SamplesShifting=Src->SamplesShifting;
		Drawed=Src->Drawed;
		Drawed2=Src->Drawed2;
		Backward=Src->Backward;
		Saved=Src->Saved;
		return true;
	}
	else return false;
}

int TTrace::GetMaxInTrace(void)
{
    short int k;
	double val=0;

	//if(MaxInTrace>0) return MaxInTrace;
    try
    {
	  for(k=1; k<Samples; k++) if(fabs((double)Data[k])>val) val=fabs((double)Data[k]);
	  if(val>PositiveMax) val=PositiveMax;
	  MaxInTrace=val;
	  return(val);
	}
	catch (Exception &exception)
	{
	  LogEvent(exception.Message, "TTrace::GetMaxInTrace()");
	  return(32767);
    }
}

int TTrace::GetMaxInSourceTrace(void)
{
	short int k;
	TSample val=0;

	for(k=1; k<Samples; k++) if(fabs((double)SourceData[k])>val) val=fabs((double)SourceData[k]);
	if(val>PositiveMax) val=PositiveMax;
    MaxInTrace=val;
	return val;
}

//##############################################################
// TTraceAccumulator
//##############################################################
__fastcall TTraceAccumulator::TTraceAccumulator(int new_samples, TProfile* AOwner) : TTrace(new_samples, AOwner, false)
{
	accumulatedcoef=0.0;
	AccumulationsQ=0;
}

void __fastcall TTraceAccumulator::Accumulate(TTrace* ptr, double coef)
{
	double v;

	if(ptr)
	{
		for(int k=0; k<Samples; k++)
		{
			v=ptr->GetArrayAddress(tatData)[k]*coef;
			v+=Data[k];
			if(v>(double)PositiveMax) v=(double)PositiveMax;
			else if(v<(double)NegativeMax) v=(double)NegativeMax;
			Data[k]=v;
			v=ptr->GetArrayAddress(tatSource)[k]*coef;
			v+=SourceData[k];
			if(v>(double)PositiveMax) v=(double)PositiveMax;
			else if(v<(double)NegativeMax) v=(double)NegativeMax;
			SourceData[k]=v;
			if(ptr->FilterDataExists)
			{
				v=ptr->GetArrayAddress(tatFilter)[k]*coef;
				v+=FilterData[k];
				if(v>(double)PositiveMax) v=(double)PositiveMax;
				else if(v<(double)NegativeMax) v=(double)NegativeMax;
				FilterData[k]=v;
			}
		}
		if(ptr->Mark>0) Mark=ptr->Mark;
		Drawed=ptr->Drawed;
		Drawed2=ptr->Drawed2;
		Backward=ptr->Backward;
		Saved=ptr->Saved;
		accumulatedcoef+=coef;
	}
}

void __fastcall TTraceAccumulator::Accumulate2(TTrace* ptr, double coef)
{
	double m, sn=1;

	if(ptr)
	{
		if(accumulatedcoef>=0)
		{
			if(ptr->Backward) m=-coef;
			else m=coef;
			if(accumulatedcoef+m<0) sn=-1;
		}
		else
		{
			if(ptr->Backward) m=coef;
			else m=-coef;
			if(accumulatedcoef-m>0) sn=-1;
		}
		for(int k=0; k<Samples; k++)
		{
			Data[k]+=(ptr->GetArrayAddress(tatData)[k]*m);
			Data[k]*=sn;
			SourceData[k]+=(ptr->GetArrayAddress(tatSource)[k]*m);
			SourceData[k]*=sn;
			if(ptr->FilterDataExists)
			{
				FilterData[k]+=(ptr->GetArrayAddress(tatFilter)[k]*m);
				FilterData[k]*=sn;
			}
		}
		accumulatedcoef+=(1.-2.*(double)ptr->Backward)*coef;
		if(ptr->Mark>0) Mark=ptr->Mark;
		Drawed=ptr->Drawed;
		Drawed2=ptr->Drawed2;
		Backward=ptr->Backward;
		Saved=ptr->Saved;
	}
}

void __fastcall TTraceAccumulator::AccumulateAndAverage(TTrace* ptr)
{
	double coef=1.;

	AccumulationsQ++;
	if(AccumulationsQ>1.)
	{
		coef-=1./(double)AccumulationsQ;
		for(int k=0; k<Samples; k++)
		{
			Data[k]*=coef;
			SourceData[k]*=coef;
			if(ptr && ptr->FilterDataExists) FilterData[k]*=coef;
		}
		accumulatedcoef*=coef;
	}
	else coef=0;
	Accumulate(ptr, 1.-coef);
}

void __fastcall TTraceAccumulator::Discharge()
{
	for(int k=0; k<Samples; k++)
	{
		Data[k]=0;
		SourceData[k]=0;
		if(FilterDataExists) FilterData[k]=0;
	}
	Mark=0;
	accumulatedcoef=0;
	AccumulationsQ=0;
}

//##############################################################
// TTraceIterator
TTraceIterator::TTraceIterator()
{
	Index=0;
	Ptr=NULL;
}

void TTraceIterator::SetFirstTracePtr(TTrace *ptr)
{
	Ptr=ptr;
}

TTrace* TTraceIterator::GetPtr(int index)
{
	int k, n;

	if(Ptr==NULL) return NULL;
	if(index==Index) return Ptr;
    try
    {
		if(index>Index)
		{
			n=index-Index;
			for(k=0; k<n; k++)
			{
				if(Ptr->PtrUp==NULL) break;
				Ptr=(Ptr->PtrUp);
				Index++;
			}
		}
		else
		{
			n=Index-index;
			for(k=0; k<n; k++)
			{
				if(Ptr->PtrDn==NULL) break;
				Ptr=(Ptr->PtrDn);
				Index--;
			}
		}
    }
	catch (Exception &exception) { LogEvent(exception.Message, "TTraceIterator::GetPtr()"); Ptr=NULL;}
	return Ptr;
}

TTrace* TTraceIterator::GetCurrentPtr(void)
{
    return Ptr;
}

TTrace* TTraceIterator::GetNext(void)
{
    if(Ptr==NULL) return NULL;
    if(Ptr->PtrUp==NULL) return Ptr;
    Ptr=(Ptr->PtrUp);
	Index++;
    return Ptr;
}

TTrace* TTraceIterator::GetPrev(void)
{
	if(Ptr==NULL) return NULL;
	if(Ptr->PtrDn==NULL) return Ptr;
	Ptr=(Ptr->PtrDn);
	Index--;
	return Ptr;
}

int TTraceIterator::GetCurrentIndex(void)
{
    return(Index);
}

//##############################################################
//--- Constructor of TProfileWin ---------
__fastcall TProfileWin::TProfileWin(TProfile *Prof)
{
	if(Init()) TProfileWin::AddProfile(Prof);
	else LogEvent("", "TProfileWin::TProfileWin(TProfile* Prof="+IntToHex((int)Prof, sizeof(int)*2)+")");
	//LogEvent("PC=3", "TProfileOutputView::TProfileOutputView("+IntToHex((int)Prof, sizeof(int)*2)+")");
}

__fastcall TProfileWin::TProfileWin(TProfileWin* ProfWin)
{
	if(Init())
	{
		for(int i=0; i<ProfWin->ProfilesCount; i++)
			if(ProfWin->Profiles[i]) TProfileWin::AddProfile(ProfWin->Profiles[i]);
		LeftIndex=ProfWin->LeftIndex;
		RightIndex=ProfWin->RightIndex;
	}
	else LogEvent("", "TProfileWin::TProfileWin(TProfileWin* ProfWin="+IntToHex((int)ProfWin, sizeof(int)*2)+")");
}

__fastcall TProfileWin::~TProfileWin()
{
	if(profiles)
	{
		profiles->Clear();
		delete profiles;
		profiles=NULL;
	}
	if(lefttraces)
	{
		lefttraces->Clear();
		delete lefttraces;
		lefttraces=NULL;
	}
	if(righttraces)
	{
		righttraces->Clear();
		delete righttraces;
		righttraces=NULL;
	}
}

bool __fastcall TProfileWin::Init()
{
	bool res;

	try
	{
		leftindex=rightindex=0;
		leftpos=rightpos=0.;
		profiles=new TList();
		lefttraces=new TList();
		righttraces=new TList();
		res=true;
	}
	catch(Exception &e)
	{
		LogEvent(e.Message, "TProfileWin::Init()");
		profiles=NULL;
		lefttraces=NULL;
		righttraces=NULL;
		res=false;
	}

	return res;
}

int __fastcall TProfileWin::AddProfile(TProfile *Prof)
{
	int i;

	if(Prof && profiles && profiles->IndexOf((void*)Prof)<0)
	{
		i=profiles->Add((void*)Prof);
		if(i>=0)
		{
			//LogEvent("PC=1", "TProfileOutputView::AddProfile("+IntToHex((int)Prof, sizeof(int)*2)+")");
			lefttraces->Add((void*)GetTraceByIndex(leftindex, i));
			//LogEvent("PC=2", "TProfileOutputView::AddProfile("+IntToHex((int)Prof, sizeof(int)*2)+")");
			righttraces->Add((void*)GetTraceByIndex(rightindex, i));
		}
	}
	else i=-1;

	return i;
}

TProfile* __fastcall TProfileWin::readProfile()
{
	TProfile* res=NULL;
	try
	{
		if(profiles && profiles->Count>0 && profiles->Items[0])
		{
			//((TProfile*)profiles->Items[0])->RefreshAccessTime();
			res=(TProfile*)profiles->Items[0];
		}
	}
	catch(...) {}
	return res;
}

void __fastcall TProfileWin::writeProfile(TProfile *value)
{
	if(profiles && profiles->Count==0)
	{
		AddProfile(value);
		value->RefreshAccessTime();
	}
	else profiles->Items[0]=value;
}

TProfile* __fastcall TProfileWin::readProfiles(int Index)
{
	if(Index>=0 && Index<profiles->Count && profiles && profiles->Items[Index])
	{
		((TProfile*)profiles->Items[Index])->RefreshAccessTime();
		return (TProfile*)profiles->Items[Index];
	}
	else return NULL;
}

TTrace* __fastcall TProfileWin::GetTraceByIndex(int Index, int ProfIndex, TTrace* LastGot)
{
	TTrace* ptr;
	TProfile *prof;
	int t2;

	prof=Profiles[ProfIndex];
	if(prof)
	{
		//LogEvent("PC=1 prof="+IntToHex((int)prof, sizeof(int)*2), "TProfileOutputView::GetTraceByIndex("+IntToStr(Index)+", "+IntToStr(ProfIndex)+")");
		if(Index<=0) ptr=prof->FirstTracePtr;
		else if(Index>=(prof->Traces-1)) ptr=prof->LastTracePtr;
		else
		{
			t2=prof->Traces>>1;
			if(LastGot)// && abs(LastGot->Index-Index)<t2)
			{
				if(LastGot->Index==Index) ptr=LastGot;
				else if(LastGot->Index<Index)
				{
					ptr=LastGot;
					while(ptr!=NULL && Index>ptr->Index) ptr=ptr->PtrUp;
					if(ptr==NULL) ptr=prof->LastTracePtr;
				}
				else
				{
					ptr=LastGot;
					while(ptr!=NULL && Index<ptr->Index) ptr=ptr->PtrDn;
					if(ptr==NULL) ptr=prof->FirstTracePtr;
				}
			}
			else
			{ /**/
				if(t2<Index)
				{
					//LogEvent("PC=2a prof="+IntToHex((int)prof, sizeof(int)*2), "TProfileOutputView::GetTraceByIndex("+IntToStr(Index)+", "+IntToStr(ProfIndex)+")");
					ptr=prof->LastTracePtr;
					//LogEvent("PC=3a prof="+IntToHex((int)prof, sizeof(int)*2), "TProfileOutputView::GetTraceByIndex("+IntToStr(Index)+", "+IntToStr(ProfIndex)+")");
					while(ptr!=NULL && Index<ptr->Index) ptr=ptr->PtrDn;
					//LogEvent("PC=4a prof="+IntToHex((int)prof, sizeof(int)*2), "TProfileOutputView::GetTraceByIndex("+IntToStr(Index)+", "+IntToStr(ProfIndex)+")");
					if(ptr==NULL) ptr=prof->FirstTracePtr;
				}
				else
				{
					//LogEvent("PC=2b prof="+IntToHex((int)prof, sizeof(int)*2), "TProfileOutputView::GetTraceByIndex("+IntToStr(Index)+", "+IntToStr(ProfIndex)+")");
					ptr=prof->FirstTracePtr;
					//LogEvent("PC=3b prof="+IntToHex((int)prof, sizeof(int)*2), "TProfileOutputView::GetTraceByIndex("+IntToStr(Index)+", "+IntToStr(ProfIndex)+")");
					while(ptr!=NULL && Index>ptr->Index) ptr=ptr->PtrUp;
					//LogEvent("PC=4b prof="+IntToHex((int)prof, sizeof(int)*2), "TProfileOutputView::GetTraceByIndex("+IntToStr(Index)+", "+IntToStr(ProfIndex)+")");
					if(ptr==NULL) ptr=prof->LastTracePtr;
				}
			}
		}
	}
	else ptr=NULL;
	//LogEvent("PC=5 prof="+IntToHex((int)prof, sizeof(int)*2), "TProfileOutputView::GetTraceByIndex("+IntToStr(Index)+", "+IntToStr(ProfIndex)+")");
	return ptr;
}

TTrace* __fastcall  TProfileWin::GetTraceByPos(float Pos, int ProfIndex, TTrace* LastGot)
{
	TTrace* ptr;
	TProfile *prof;
	float t2;

	prof=Profiles[ProfIndex];
	if(prof)
	{
		if(Pos<=0) ptr=prof->FirstTracePtr;
		else if(prof->LastTracePtr && Pos>=prof->LastTracePtr->HorPos) ptr=prof->LastTracePtr;
		else
		{
			t2=prof->HorRangeFull/2.;
			if(LastGot)// && abs(LastGot->Index-Index)<t2)
			{
				if(LastGot->HorPos==Pos) ptr=LastGot;
				else if(LastGot->HorPos<Pos)
				{
					ptr=LastGot;
					while(ptr!=NULL && Pos>ptr->HorPos) ptr=ptr->PtrUp;
					if(ptr==NULL) ptr=prof->LastTracePtr;
				}
				else
				{
					ptr=LastGot;
					while(ptr!=NULL && Pos<ptr->HorPos) ptr=ptr->PtrDn;
					if(ptr==NULL) ptr=prof->FirstTracePtr;
				}
			}
			else
			{ /**/
				if(t2<Pos)
				{
					//LogEvent("PC=2a prof="+IntToHex((int)prof, sizeof(int)*2), "TProfileOutputView::GetTraceByIndex("+IntToStr(Index)+", "+IntToStr(ProfIndex)+")");
					ptr=prof->LastTracePtr;
					//LogEvent("PC=3a prof="+IntToHex((int)prof, sizeof(int)*2), "TProfileOutputView::GetTraceByIndex("+IntToStr(Index)+", "+IntToStr(ProfIndex)+")");
					while(ptr!=NULL && Pos<ptr->HorPos) ptr=ptr->PtrDn;
					//LogEvent("PC=4a prof="+IntToHex((int)prof, sizeof(int)*2), "TProfileOutputView::GetTraceByIndex("+IntToStr(Index)+", "+IntToStr(ProfIndex)+")");
					if(ptr==NULL) ptr=prof->FirstTracePtr;
				}
				else
				{
					//LogEvent("PC=2b prof="+IntToHex((int)prof, sizeof(int)*2), "TProfileOutputView::GetTraceByIndex("+IntToStr(Index)+", "+IntToStr(ProfIndex)+")");
					ptr=prof->FirstTracePtr;
					//LogEvent("PC=3b prof="+IntToHex((int)prof, sizeof(int)*2), "TProfileOutputView::GetTraceByIndex("+IntToStr(Index)+", "+IntToStr(ProfIndex)+")");
					while(ptr!=NULL && Pos>ptr->HorPos) ptr=ptr->PtrUp;
					//LogEvent("PC=4b prof="+IntToHex((int)prof, sizeof(int)*2), "TProfileOutputView::GetTraceByIndex("+IntToStr(Index)+", "+IntToStr(ProfIndex)+")");
					if(ptr==NULL) ptr=prof->LastTracePtr;
				}
			}
		}
	}
	else ptr=NULL;

	return ptr;
}

void __fastcall TProfileWin::writeLeftIndex(int value)
{
	LeftTraces[0]=GetTraceByIndex(value, 0, LeftTraces[0]);
	for(int i=1; i<lefttraces->Count; i++) LeftTraces[i]=GetTraceByIndex(leftindex, i, LeftTraces[i]);
}

void __fastcall TProfileWin::writeRightIndex(int value)
{
	RightTraces[0]=GetTraceByIndex(value, 0, RightTraces[0]);
	for(int i=1; i<righttraces->Count; i++) RightTraces[i]=GetTraceByIndex(rightindex, i, RightTraces[i]);
}

void __fastcall TProfileWin::writeLeftPos(float value)
{
	LeftTraces[0]=GetTraceByPos(value, 0, LeftTraces[0]);
	for(int i=1; i<lefttraces->Count; i++) LeftTraces[i]=GetTraceByPos(leftindex, i, LeftTraces[i]);
}

void __fastcall TProfileWin::writeRightPos(float value)
{
	RightTraces[0]=GetTraceByPos(value, 0, RightTraces[0]);
	for(int i=1; i<righttraces->Count; i++) RightTraces[i]=GetTraceByPos(rightindex, i, RightTraces[i]);
}

void __fastcall TProfileWin::writeLeftTraces(int Index, TTrace* value)
{
	if(Index>=0 && Index<lefttraces->Count)
	{
		lefttraces->Items[Index]=(void*)value;
		if(value!=NULL)
		{
			leftindex=value->Index;
			leftpos=value->HorPos;
		}
		else
		{
			leftindex=0;
			leftpos=0.;
		}
		if(leftindex>rightindex || leftpos>rightpos) RightTraces[Index]=value;
	}
}

void __fastcall TProfileWin::writeRightTraces(int Index, TTrace* value)
{
	if(Index>=0 && Index<righttraces->Count)
	{
		righttraces->Items[Index]=(void*)value;
		if(value!=NULL)
		{
			rightindex=value->Index;
			rightpos=value->HorPos;
		}
		else
		{
			rightindex=0;
			rightpos=0.;
		}
		if(rightindex<leftindex || rightpos<leftpos) LeftTraces[Index]=value;
	}
}

//##############################################################
//--- Constructor of TProfileOutputView ---------
__fastcall TProfileOutputView::TProfileOutputView(TProfile *Prof) : TProfileWin(Prof)
{
	int i;

	try
	{
		scrollingleft=scrollingright=0;
		//LogEvent("PC=1", "TProfileOutputView::TProfileOutputView("+IntToHex((int)Prof, sizeof(int)*2)+")");
		afterscrollingevents=new TList();
		finishscrollingevents=new TList();
		clearoutputviewevents=new TList();
		//LogEvent("PC=2", "TProfileOutputView::TProfileOutputView("+IntToHex((int)Prof, sizeof(int)*2)+")");
		i=profiles->IndexOf(Prof);
		if(i>=0)
		{
			//LogEvent("PC=3", "TProfileOutputView::AddProfile("+IntToHex((int)Prof, sizeof(int)*2)+")");
			afterscrollingevents->Add((void*)CreateEvent(NULL, false, false, NULL));
			finishscrollingevents->Add((void*)CreateEvent(NULL, false, false, NULL));
			clearoutputviewevents->Add((void*)CreateEvent(NULL, false, false, NULL));
		}
		InScrolling=false;
	}
	catch(Exception &e)
	{
		LogEvent(e.Message, "TProfileOutputView::TProfileOutputView("+IntToHex((int)Prof, sizeof(int)*2)+")");
	}
	//LogEvent("PC=4", "TProfileOutputView::TProfileOutputView("+IntToHex((int)Prof, sizeof(int)*2)+")");
}

__fastcall TProfileOutputView::~TProfileOutputView()
{
	for(int i=0; i<afterscrollingevents->Count; i++)
		if(afterscrollingevents->Items[i]) CloseHandle((HANDLE)afterscrollingevents->Items[i]);
	afterscrollingevents->Clear();
	delete afterscrollingevents;
	for(int i=0; i<finishscrollingevents->Count; i++)
		if(finishscrollingevents->Items[i]) CloseHandle((HANDLE)finishscrollingevents->Items[i]);
	finishscrollingevents->Clear();
	delete finishscrollingevents;
	for(int i=0; i<clearoutputviewevents->Count; i++)
		if(clearoutputviewevents->Items[i]) CloseHandle((HANDLE)clearoutputviewevents->Items[i]);
	clearoutputviewevents->Clear();
	delete clearoutputviewevents;
}

void __fastcall TProfileOutputView::ClearAllOutputViews()
{
	for(int i=0; i<clearoutputviewevents->Count; i++)
		if(clearoutputviewevents->Items[i])
			SetEvent((HANDLE)clearoutputviewevents->Items[i]);
}

int __fastcall TProfileOutputView::AddProfile(TProfile *Prof)
{
	int i;

	i=TProfileWin::AddProfile(Prof);
	if(i>=0)
	{
		//LogEvent("PC=1", "TProfileOutputView::AddProfile("+IntToHex((int)Prof, sizeof(int)*2)+")");
		afterscrollingevents->Add((void*)CreateEvent(NULL, false, false, NULL));
		finishscrollingevents->Add((void*)CreateEvent(NULL, false, false, NULL));
		clearoutputviewevents->Add((void*)CreateEvent(NULL, false, false, NULL));
	}

	return i;
}

void __fastcall TProfileOutputView::writeScrollingLeft(int value)
{
	TTrace *ptr;

	ptr=GetTraceByIndex(value);
	if(ptr!=NULL) scrollingleft=ptr->Index;
	if(scrollingleft>scrollingright) scrollingright=scrollingleft;
}

void __fastcall TProfileOutputView::writeScrollingRight(int value)
{
	TTrace *ptr;

	ptr=GetTraceByIndex(value);
	if(ptr!=NULL) scrollingright=ptr->Index;
	if(scrollingright<scrollingleft) scrollingleft=scrollingright;
}

void __fastcall TProfileOutputView::CenterAtPos(int Index)
{
	int w=Width();

	TProfileWin::CenterAtPos(Index);

	ScrollingRightIndex=Index;
	ScrollingLeftIndex=ScrollingRightIndex-w;
	if(ScrollingLeftIndex==0) ScrollingRightIndex=w;
	ApplyScrolling();
}

void __fastcall TProfileOutputView::ApplyScrolling()
{
	TTrace *ptr;
	LeftIndex=ScrollingLeftIndex;
	RightIndex=ScrollingRightIndex;
	//Redraw Profile...
	for(int i=0; i<ProfilesCount; i++)
	{
		if(i==0) ptr=LeftTraces[i];
		else ptr=GetTraceByIndex(LeftIndex, i);
		while(ptr!=NULL && ptr->Index<RightIndex)
		{
			ptr->Drawed=false;
			ptr=ptr->PtrUp;
		}
		if(ptr) ptr->Drawed=true;
		ResetEvent(FinishScrollingEvents[i]);
		SetEvent(AfterScrollingEvents[i]);
		SetEvent(Profiles[i]->TracesAddedEvent);
	}
}

//##############################################################
//--- Constructor of TProfile ---------
__fastcall TProfile::TProfile(void)
{
	ProfileInit();
}

__fastcall TProfile::TProfile(TObject *goc)
{
	ProfileInit();
	ObjectsContainer=goc;
}

void __fastcall TProfile::ProfileInit(void)
{
	TMyPoint *mp;

	try
	{
#ifdef ALLOCATE_SAMPLES_HEAP
		hheap=HeapCreate(HEAP_GENERATE_EXCEPTIONS, 0, 0);
#endif
		tracesaddedevent=CreateEvent(NULL, false, false, NULL);
		ProfileLockEvent=CreateEvent(NULL, true, false, NULL);
		NumberOfChannels=1;
		name="";
		fullname="";
		TextInfo="";
		SourceType=_SEGY;
		FileSaved=false;
		FileModified=false;
		FileProcessed=false;
		AnnotationsModified=false;
		Contrast=0.5;
		samples=512;
		SamplesBeforeChanging=0;
		InterlockedExchange(&traces,0L);
		firsttraceptr=lasttraceptr=currenttraceptr=NULL;
		commontrace=new TTraceAccumulator(samples, this);
		timerange=0;
//      HorRange=0;
		HorRangeFrom=0;
		HorRangeTo=0;
		HorRangeFull=0;
		permit=1.0;
		zeropoint=0;
		zeropointadjusted=false;
		NeedToScaleZero=false;
		MaxInFile=0;
		XUnits=cxuDistance;
		YUnits=cyuDepth;
		ClearZeroPoint();
		DepthRecalcul();
		IndividualRef=false;
		ShowAnnotations=true;
		ShowOnlyAnnotations=false;
		ShowMarks=true;
		for(int k=0; k<=255; k++)
			Colors[255-k]=(TColor)RGB(k, k, k);
		SampleFrom=0;
		SampleTo=0;
		TraceFrom=0;
		TraceTo=0;
		SampleFromOld=0;
		SampleToOld=0;
		TraceFromOld=0;
		TraceToOld=0;
		Stacking=1;
		SamplesAutoFit=true;
		TracesAutoFit=true;
		Coordinated=false;
		Vertexes=new TList;
		GainFunction=new float[samples];
		mp=new TMyPoint();
		mp->x=0; mp->y=0;
		Vertexes->Add(mp);
		mp=new TMyPoint();
		mp->x=1; mp->y=0;
		Vertexes->Add(mp);
		Filter=new float[samples*2];
		FILTER_TYPE=FILTER_OFF;
		YPosition=0.;

		Figures=NULL;//figures=NULL;//new TList;

		FilterPoints[0]=0.05;
		FilterPoints[1]=0.1;
		FilterPoints[2]=0.2;
		FilterPoints[3]=0.5;
		MaxAltitude=0;
		MinAltitude=0;
		outputview=new TProfileOutputView(this);
		outputviewowner=true;
		ObjectsContainer=NULL;

		SampleFormat=sfShort16bit;
		lastaccess=Gr32_system::GetTickCount();
		cachingisprohibited=false;
		InRestoring=false;
		PositiveMax=(1<<15) - 1;
		NegativeMax=-((1<<15) - 1);
		ProfileCache=NULL;

	//--Only for GSSI data-----------------------------------------
		rh_zero=0;
		rh_sps=0;
		rh_spm=0;
		rh_mpm=0;
		rh_spp=0;
		rh_nchan=1;
		rh_top=0;
		rh_depth=0;
		rh_npass=0;
		for(int k=0; k<60; k++) reserved[k]=0;
		for(int k=0; k<384; k++) variable[k]=0;
		rh_create=DateToGSSI(Now());
		rh_modif=rh_create;
	}
	catch(Exception &e)
	{
		LogEvent(e.Message, "TProfile::TProfile() "+(AnsiString)__FILE__+" line:"+(AnsiString)__LINE__);
		ShowMessage(e.Message);
	}
}

__fastcall TProfile::~TProfile()
{
	TTrace *ptr;
	int k;

	UnLock();
	try
	{
		for(int i=0; i<Vertexes->Count; i++)
			delete (TMyPoint*)Vertexes->Items[i];
		delete Vertexes;
	}
	catch (Exception &exception) { LogEvent(exception.Message, "TProfile::~TProfile() Vertexes"); }
	if(Figures)
	{
		try
		{
			for(int i=0; i<Figures->Count; i++)
				delete Figures->Items[i];
			Figures->Clear();
			delete Figures;
			Figures=NULL;
		}
		catch (Exception &exception) { LogEvent(exception.Message, "TProfile::~TProfile() Figures"); }

	}
	delete[] GainFunction;
	delete[] Filter;
	if(ObjectsContainer!=NULL)
	{
		//Delete all connected to this profile objects...

		//...
	}
	if(outputview && OutputViewOwner) delete outputview;
	if(firsttraceptr)
	{
		for(k=0; k<Traces; k++)
		{
			ptr=firsttraceptr->PtrUp;
			delete firsttraceptr;
			firsttraceptr=ptr;
			if(ptr==NULL) break;
		}
	}
	delete commontrace;
	firsttraceptr=lasttraceptr=currenttraceptr=commontrace=NULL;
	if(ProfileCache) delete ProfileCache;
	ProfileCache=NULL;
	CloseHandle(tracesaddedevent);
	CloseHandle(ProfileLockEvent);
	ProfileLockEvent=NULL;
#ifdef ALLOCATE_SAMPLES_HEAP
	if(hHeap) HeapDestroy(hHeap);
#endif
}

void __fastcall TProfile::CacheIt()
{
	if(!ProfileCache)
		ProfileCache=(TCacheProfileDataObject*)CacheManager->CacheIt(cotProfileData, (void*)this, SizeInMemory, (TSynchronizeMethod)NULL);
}

void __fastcall TProfile::Restore()
{
	if(ProfileCache && CacheManager && !InRestoring)
	{
		InRestoring=true;
		try
		{
			try
			{
				if(CacheManager->Restore(ProfileCache, this))
				{
					TCacheProfileDataObject* tmp=ProfileCache;
					ProfileCache=NULL;
					delete tmp;
				}
			}
			catch(...) {}
		}
		__finally
		{
			InRestoring=false;
		}
	}
}

void __fastcall TProfile::writeSamples(int value)
{
	TTrace *ptr;
	int i;

	if(samples!=value)
	{
		if(firsttraceptr)
		{
			ptr=firsttraceptr;
			while(ptr)
			{
				ptr->Samples=value;
				ptr=ptr->PtrUp;
			}
		}
		commontrace->Samples=value;
		delete[] GainFunction;
		GainFunction=new float[value];
		delete[] Filter;
		Filter=new float[value*2];
		for(i=0; i<value; i++)
		{
			GainFunction[i]=1.;
			Filter[i]=1.;
		}
		for(; i<value*2; i++)
		{
			Filter[i]=1.;
		}
		samples=value;
	}
}

void __fastcall TProfile::writeOutputView(TProfileOutputView *value)
{
	if(outputview!=value)
	{
		if(OutputViewOwner) delete outputview;
		outputview=value;
		outputview->AddProfile(this);
		OutputViewOwner=false;
	}
}

void __fastcall TProfile::writeFullName(AnsiString value)
{
	if(value!=fullname)
	{
		fullname=value;
		if(FileExists(value))
		{
			AnsiString ext;

			name=ExtractFileName(value);
			ext=ExtractFileExt(value);
			if(name!=NULL && ext!=NULL && name.Length()>0 && ext.Length()>0)
				name.SetLength(name.Length()-ext.Length());
		}
	}
}

void __fastcall TProfile::DepthRecalcul()
{
	try
	{
		if(permit>=1) depth=timerange*(1e-9*3e8/2.0/sqrt(permit));
	}
	catch(Exception &e) {depth=0;}
}

void __fastcall TProfile::writeFirstTracePtr(TTrace* ptr)
{
	while(CheckLock()) ;
	RefreshAccessTime();
	if(ProfileLockEvent) firsttraceptr=ptr;
}

TTrace* __fastcall TProfile::readFirstTracePtr()
{
	while(CheckLock()) ;
	RefreshAccessTime();
	return firsttraceptr;
}

void __fastcall TProfile::writeLastTracePtr(TTrace* ptr)
{
	while(CheckLock()) ;
	//RefreshAccessTime();
	if(ProfileLockEvent) lasttraceptr=ptr;
}

TTrace* __fastcall TProfile::readLastTracePtr()
{
	while(CheckLock()) ;
	//RefreshAccessTime();
	return lasttraceptr;
}

void __fastcall TProfile::writeCurrentTracePtr(TTrace* ptr)
{
	while(CheckLock()) ;
	RefreshAccessTime();
	if(ProfileLockEvent) currenttraceptr=ptr;
}

TTrace* __fastcall TProfile::readCurrentTracePtr()
{
	while(CheckLock()) ;
	RefreshAccessTime();
	return currenttraceptr;
}

void __fastcall TProfile::Add(TTrace *ptr, bool WithLocking)
{
	if(ptr)
	{
		if(WithLocking) while(!Lock()) ;
		try
		{
			try
			{
				if(firsttraceptr==NULL)
				{
					ptr->PtrUp=NULL;
					ptr->PtrDn=NULL;
					firsttraceptr=ptr;
					lasttraceptr=ptr;
				}
				else
				{
					lasttraceptr->PtrUp=ptr;
					ptr->PtrDn=lasttraceptr;
					lasttraceptr=ptr;
					lasttraceptr->PtrUp=NULL;
				}
				currenttraceptr=ptr;
				if(ptr->Z>MaxAltitude) MaxAltitude=ptr->Z;
				if(ptr->Z<MinAltitude) MinAltitude=ptr->Z;
				ptr->Index=Traces;
				ptr->HorPos=HorRangeFull;
				ptr->SetOwner(this);
				ptr->TimeStamp=Now();
				commontrace->AccumulateAndAverage(ptr);
				InterlockedIncrement(&traces);
				HorRangeFull+=ptr->TraceDistance;
				HorRangeTo=HorRangeFrom+HorRangeFull;
				RefreshAccessTime();
			}
			catch(Exception &e) {}
		}
		__finally
		{
			if(WithLocking) UnLock();
		}
	}
}

void __fastcall TProfile::AddNewTrace(TTrace *ptr, bool WithLocking)
{
	int i;

	if(CurrentTracePtr==NULL)
	{
		if(!ptr->Backward) Add(ptr, WithLocking);
	}
	else
	{
		if(WithLocking) while(!Lock()) ;
		try
		{
			try
			{
				if(!ptr->Backward)
				{
					if(currenttraceptr==lasttraceptr || currenttraceptr->PtrUp==NULL) Add(ptr, false);
					else
					{
						currenttraceptr=currenttraceptr->PtrUp;
						currenttraceptr->CopyTrace(ptr);
						commontrace->AccumulateAndAverage(ptr);
						delete ptr;
						if(firsttraceptr->Backward) firsttraceptr->Backward=false;
					}
				}
				else
				{
					currenttraceptr->CopyTrace(ptr);
					commontrace->AccumulateAndAverage(ptr);
					if(currenttraceptr!=firsttraceptr || currenttraceptr->PtrDn!=NULL)
						currenttraceptr=currenttraceptr->PtrDn;
					delete ptr;
				}
			}
			catch(Exception &e) {}
		}
		__finally
		{
			if(WithLocking) UnLock();
		}
	}
	SetEvent(TracesAddedEvent);
	if(outputview)
	{
		i=outputview->GetProfileIndex(this);
		if(i>=0) SetEvent(outputview->FinishScrollingEvents[i]);
	}
	if(ObjectsContainer!=NULL) ((TGraphicObjectsContainer*)ObjectsContainer)->CustomUpdate();
}

bool __fastcall TProfile::DeleteFirstTrace(void)
{
	TTrace *ptr;

	if(Traces<=2) return false;
	if(FirstTracePtr->PtrUp==NULL) return false;

	ptr=FirstTracePtr;
	FirstTracePtr=(FirstTracePtr->PtrUp);
	InterlockedDecrement(&traces);
	if(CurrentTracePtr==ptr) CurrentTracePtr=FirstTracePtr;
	delete ptr;
	return true;
}

int __fastcall TProfile::GetMaxInFile(void)
{
	int k,max=0,val;
	TTraceIterator *iter;

	iter=new TTraceIterator;
	iter->SetFirstTracePtr(FirstTracePtr);
	iter->GetPtr(0);
	for(k=0; k<Traces; k++)
	{
		val=iter->GetCurrentPtr()->GetMaxInTrace();
		if(val>max) max=val;
		iter->GetNext();
	}
	MaxInFile=max;
	delete iter;
	RefreshAccessTime();
	return max;
}

int __fastcall TProfile::GetMaxInSourceFile(void)
{
	int k,max=0,val;
	TTraceIterator *iter;

	//if(MaxInFile>0) return MaxInFile;

	iter=new TTraceIterator;
	iter->SetFirstTracePtr(FirstTracePtr);
	iter->GetPtr(0);
	for(k=0; k<Traces; k++)
	{
		val=iter->GetCurrentPtr()->GetMaxInSourceTrace();
		if(val>max) max=val;
		iter->GetNext();
	}
	delete iter;
	RefreshAccessTime();
	return max;
}

void __fastcall TProfile::Clear(bool FullCleanining)
{
	int k;
	long z;
	TTrace *ptr;

	while(CheckLock()) ;
	Lock();
	try
	{
		try
		{
			if(FullCleanining) z=0;
			else z=2;

			if(Traces<=z) return;

			for(k=0; k<Traces-z; k++)
			{
				if(firsttraceptr!=NULL)
				{
					ptr=firsttraceptr->PtrUp;
					delete firsttraceptr;
					firsttraceptr=ptr;
					if(ptr) ptr->PtrDn=NULL;
				}
				else break;
			}
			InterlockedExchange(&traces,z);
			if(FullCleanining) lasttraceptr=ptr=NULL;
			currenttraceptr=firsttraceptr=ptr;
			MaxInFile=0;
			SampleFrom=0;
			SampleTo=Samples;
			TraceFrom=0;
			if(FullCleanining) TraceTo=0;
			else TraceTo=z-1;
			WheelStep=0.;
		}
		catch(Exception &e) {}
	}
	__finally
	{
		UnLock();
    }
	return;
}

//--Only for GSSI data-----------------------------------------
int __fastcall TProfile::DateToGSSI(TDateTime Dt)
{
	Word Year, Month, Day, Hour, Min, Sec, MSec;
	int tmp, rh;

	DecodeDate(Dt, Year, Month, Day);
	DecodeTime(Dt, Hour, Min, Sec, MSec);
	rh=0;
	tmp=(Year-1980)<<25;
	rh+=tmp;
	tmp=Month<<21;
	rh+=tmp;
	tmp=Day<<16;
	rh+=tmp;
	tmp=Hour<<11;
	rh+=tmp;
	tmp=Min<<5;
	rh+=tmp;
	tmp=(Sec/2);
	rh+=tmp;
	return rh;
}

//--Only for GSSI data-----------------------------------------
TDateTime __fastcall TProfile::GSSIToDate(int rh)
{
	Word Year, Month, Day, Hour, Min, Sec;
	int tmp;
	TDateTime Dt;

	tmp=(rh&0xFE000000)>>25;
	Year=(Word)(1980+tmp);
	tmp=(rh&0x1E00000)>>21;
	Month=(Word)tmp;
	if(Month==0) Month++;
    tmp=(rh&0x1F0000)>>16;
    Day=(Word)tmp;
	if(Day==0) Day++;
	tmp=(rh&0xF800)>>11;
    Hour=(Word)tmp;
    tmp=(rh&0x7E0)>>5;
	Min=(Word)tmp;
    tmp=rh&0x1F;
	Sec=(Word)(tmp*2);
	Dt=EncodeDate(Year, Month, Day);
	Dt+=EncodeTime(Hour, Min, Sec, 0);
    return Dt;
}

int __fastcall TProfile::GetTraceIndex(TTrace* ptr)
{
   int Index=-1, i=0;
   TTrace* Trace=FirstTracePtr;

   while(Trace!=NULL)
   {
	 if(Trace==ptr)
	 {
	   Index=i;
	   break;
	 }
	 Trace=Trace->PtrUp;
	 i++;
   }
   return Index;
}

void __fastcall TProfile::ReAltitude(void)
{
   MaxAltitude=FirstTracePtr->Z;
   MinAltitude=FirstTracePtr->Z;
   ReAltitude(FirstTracePtr, LastTracePtr, true);
}

void __fastcall TProfile::ReAltitude(TTrace* FromPtr, TTrace* ToPtr, bool Upstairs)
{
   TTrace* Trace=FromPtr;

   //MaxAltitude=Trace->Z;
   while(Trace!=ToPtr && Trace!=NULL)
   {
	 if(Trace->Z>MaxAltitude) MaxAltitude=Trace->Z;
	 if(Trace->Z>MinAltitude) MinAltitude=Trace->Z;
	 if(Upstairs) Trace=Trace->PtrUp;
	 else Trace=Trace->PtrDn;
   }
}

//##############################################################
//--- Constructor of TProfilesGroup ---------
__fastcall TProfilesGroup::TProfilesGroup(int N)
{
	Count=N;
	Items=new TProfile*[Count];
	for(int i=0; i<Count; i++) Items[i]=NULL;
}

__fastcall TProfilesGroup::~TProfilesGroup()
{
	for(int i=0; i<Count; i++)
		if(Items[i]!=NULL) delete Items[i];
	delete[] Items;
}

void __fastcall TProfilesGroup::AssignProfile(TProfile* p, int Index)
{
	if(Index<Count)
	{
		if(Items[Index]!=NULL) delete Items[Index];
		Items[Index]=p;
	}
}

TProfile* __fastcall TProfilesGroup::GetProfile(int Index)
{
	if(Index<Count) return Items[Index];
	else return NULL;
}

TTrace* __fastcall TProfilesGroup::GetLastTracePtr(int Index)
{
	if(Index<Count && Items[Index]!=NULL) return Items[Index]->LastTracePtr;
	else return NULL;
}

TTrace* __fastcall TProfilesGroup::GetFirstTracePtr(int Index)
{
	if(Index<Count && Items[Index]!=NULL) return Items[Index]->FirstTracePtr;
	else return NULL;
}

TTrace* __fastcall TProfilesGroup::GetCurrentTracePtr(int Index)
{
	if(Index<Count && Items[Index]!=NULL) return Items[Index]->CurrentTracePtr;
	else return NULL;
}

void __fastcall TProfilesGroup::AddNewTrace(TTrace *ptr, int Index)
{
	if(Index<Count && Items[Index]!=NULL) Items[Index]->AddNewTrace(ptr);
}

