//---------------------------------------------------------------------------

#ifndef MyGraphH
#define MyGraphH
//---------------------------------------------------------------------------
#include <Math.h>
#include "Profile.h"
#include <GR32.hpp>
#include <GR32_Image.hpp>
#include <GR32_Layers.hpp>
#include <GR32_Polygons.hpp>
#include "GR32_Lines.hpp"
#include "GR32_PNG_Load.h"
#include "GPSCoordinate.h"
#include "MyObjects.h"
//#include "Manager.h"

/*
#include <Math.h>

struct TFloatRect
{
	TFloatRect() {Left=Top=Right=Bottom=0;}
	TFloatRect(float a, float b, float c, float d) {Left=a; Top=b; Right=c; Bottom=d;}
	union
	{
		struct
		{
			TFloatPoint TopLeft;
			TFloatPoint BottomRight;
		};
		struct
		{
			float Left;
			float Top;
			float Right;
			float Bottom;
		};
	};
	float Width() {return Right-Left;}
	float Height() {return Bottom-Top;}

	bool operator ==(const TFloatRect& rc) const
	{
	   return Left ==  rc.Left  && Top==rc.Top &&
			  Right == rc.Right && Bottom==rc.Bottom;
	}
	bool operator !=(const TFloatRect& rc) const
	{  return !(rc==*this); }
};

//------------ TBitmap32 class --------------
public:
	inline void __fastcall  Draw(int DstX, int DstY, TCustomBitmap32* Src){ try{TCustomBitmap32::Draw(DstX, DstY, Src);} catch (Exception &e) {} }
	inline void __fastcall  Draw(int DstX, int DstY, const Types::TRect &SrcRect, TCustomBitmap32* Src){ try{TCustomBitmap32::Draw(DstX, DstY, SrcRect, Src);} catch (Exception &e) {} }
	inline void __fastcall  Draw(const Types::TRect &DstRect, const Types::TRect &SrcRect, TCustomBitmap32* Src){ try{ TCustomBitmap32::Draw(DstRect, SrcRect, Src);} catch (Exception &e) {} }
//-------------------------------------------

  struct TRect : public RECT
  {
    TRect() { left=top=right=bottom=0; }
	TRect(const TPoint& TL, const TPoint& BR) { left=TL.x; top=TL.y; right=BR.x; bottom=BR.y; }
    TRect(int l, int t, int r, int b)         { left=l;    top=t;    right=r;    bottom=b;    }
    TRect(const RECT& r)
    {
      left    = r.left;
      top     = r.top;
      right   = r.right;
	  bottom  = r.bottom;
	}
	int Width () const { return right  - left; }
    int Height() const { return bottom - top ; }
    bool operator ==(const TRect& rc) const
    {
	   return left ==  rc.left  && top==rc.top &&
              right == rc.right && bottom==rc.bottom;
    }
    bool operator !=(const TRect& rc) const
	{
		return !(rc==*this);
	}
	TRect operator *(float f) const
	{
	   return TRect((int)((float)left*f), (int)((float)top*f), (int)((float)right*f), (int)((float)bottom*f));
	}
	TRect operator *(int i) const
	{
	   return TRect((int)((float)left*i), (int)((float)top*i), (int)((float)right*i), (int)((float)bottom*i));
	}
	TRect operator /(float f) const
	{
	   return TRect((int)((float)left/f), (int)((float)top/f), (int)((float)right/f), (int)((float)bottom/f));
	}

	TRect Move(int x, int y)
	{
	   return TRect(left+x, top+y, right+x, bottom+y);
	}

	TRect Scale(float x, float y)
	{
	   return TRect((int)((float)left*x), (int)((float)top*x), (int)((float)right*y), (int)((float)bottom*y));
	}

	TRect Scale(int x, int y)
	{
	   return TRect(left*x, top*y, right*x, bottom*y);
	}

	bool Contains(const TPoint& p) const
	{
       return ((p.x >= left) && (p.y >= top) && (p.x < right) && (p.y < bottom));
	}

	bool Contains(const TRect& r) const
	{
		return (Contains(TPoint(r.left, r.top)) || Contains(TPoint(r.right, r.bottom)) ||
			Contains(TPoint(r.left, r.bottom)) || Contains(TPoint(r.right, r.top)) ||
			r.Contains(TPoint(left, top)) || r.Contains(TPoint(right, bottom)) ||
			r.Contains(TPoint(left, bottom)) || r.Contains(TPoint(right, bottom)) ||
			(left>r.left && right<r.right && top<r.top && bottom>r.bottom) ||
			(left<r.left && right>r.right && top>r.top && bottom<r.bottom));
	}

	bool IsInRect(const TRect& r)
	{
	   if(Contains(r))
	   {
		   if(left < r.left) left = r.left;
		   if(right > r.right) right = r.right;
		   if(top < r.top) top = r.top;
		   if(bottom > r.bottom) bottom = r.bottom;
		   return true;
	   }
	   else return false;
	}

       __property LONG Left    = { read=left,   write=left   };
       __property LONG Top     = { read=top,    write=top    };
       __property LONG Right   = { read=right,  write=right  };
       __property LONG Bottom  = { read=bottom, write=bottom };
  };
extern TBitmap32* __fastcall LoadPNG32(Classes::TStream* SrcStream)
{
	TBitmap32* bmp;
	bool b;

	bmp=new TBitmap32();
	LoadPNGintoBitmap32(bmp, SrcStream, b);
	if(b) bmp->DrawMode=dmBlend;
	else bmp->DrawMode=dmOpaque;

	return bmp;
}

extern TBitmap32* __fastcall LoadPNG32(System::UnicodeString Filename)
{
	TFileStream* fs;
	TBitmap32* bmp;

	try
	{
		fs=new TFileStream(Filename, fmOpenRead);
		bmp=LoadPNG32(fs);
	}
	__finally
	{
		delete fs;
	}
	return bmp;
}


extern TBitmap32* __fastcall LoadPNG32Resource(System::UnicodeString ResourceName)
{
	TBitmap32* bmp;
	TResourceStream *rs;

	try
	{
		rs=new TResourceStream((int)HInstance, ResourceName, MAKEINTRESOURCEW(10));// RT_RCDATA);
		bmp=LoadPNG32(rs);
	}
	__finally
	{
		delete rs;
	}
	return bmp;
}
*/

struct TLine
{
	int X1, Y1, X2, Y2;
	TLine() { X1=Y1=X2=Y2=0; }
	TLine(int x1, int y1, int x2, int y2) { X1=x1; Y1=y1; X2=x2; Y2=y2;}
	int Width() {return X2-X1;}
	int Height() {return Y2-Y1;}
	float Length() {double res; try { res=(double)Width()*(double)Width()+(double)Height()*(double)Height(); if(res>0) res=sqrt(res);} catch(Exception &e) {res=0;} return (float)res;}
	void Shift(int x, int y) {X1+=x; X2+=x; Y1+=y; Y2+=y;}
	void Move(int x1, int y1) {X2=Width()+x1; X1=x1; Y2=Height()+y1; Y1=y1;}

	float Angle() //between adjacent cathetus and hypotenuse in radians
	{
		float f, w, h;

		w=Width();
		h=Height();
		if(h==0)
		{
			if(w>=0) return 0.;
			else return M_PI;
		}
		else if(w==0)
		{
			if(h>0) return M_PI_2;
			else return 3.*M_PI_2;
		}
		f=fabs(atan2(fabs(h), fabs(w)));
		if(w<0)
		{
			if(h>0) f=M_PI-f;
			else f=f-M_PI;
		}
		else if(h<0) f=-f;

		return f;
	}

	void LinearFunction(double &k, double &b)
	{
		k=(double)(Y2-Y1)/(double)(X2-X1);
		b=(double)Y1-k*(double)X1;
	}

	void Reverse() {int x=X1, y=Y1; X1=X2; Y1=Y2; X2=x; Y2=y;}

	void SetLength(float L)
	{
		float a, h;

		a=Angle();
		h=Height();
		if(h==0) h=1;
		X2=(int)((float)X1+L*cos(a)+0.5);
		Y2=(int)((float)Y1+L*sin(a)+0.5);
		//Y2=(int)((float)Y1+L*sin(a)+0.5);
	}

	void Rotate(float r)
	{
		float L, h;

		r+=Angle();
		L=Length();
		h=Height();
		if(h==0) h=1;
		X2=(int)((float)X1+L*cos(r)+0.5);
		Y2=(int)((float)Y1+L*sin(r)+0.5);
		//Y2=(int)((float)Y1+L*sin(r)+0.5);
	}

	bool IsCrossing(TLine *line2, Types::TPoint *p)
	{
		int d;
		float dx;

		d=(line2->Y2-line2->Y1)*(X2-X1)-(line2->X2-line2->X1)*(Y2-Y1);
		if(d==0)
		{
			return false;
		}
		dx=(line2->X2-line2->X1)*(Y1-line2->Y1)-(line2->Y2-line2->Y1)*(X1-line2->X1);
		dx/=(float)d;
		if(dx<=1 && dx>=0)
		{
			p->x=X1+(X2-X1)*dx;
			p->y=Y1+(Y2-Y1)*dx;
			return true;
		}
		else
		{
			return false;
		}
	}

	bool IsInRect(Types::TRect tmp)
	{
		Types::TPoint p1, p2, *p;
		TLine line2, line;
		int i=0, j=0;

		p1=Types::TPoint(X1, Y1);
		p2=Types::TPoint(X2, Y2);
		p=&p1;
		/*float d=sqrt((p1.x-p2.x)*(p1.x-p2.x)+(p1.y-p2.y)*(p1.y-p2.y));
		if(d>100)
		{
			d=0;
		}*/
		if(p1.x>=tmp.Left && p1.x<tmp.Right && p1.y>=tmp.Top && p1.y<tmp.Bottom)
		{
		  p=&p2;
		  i++;
		}
		if(p2.x>=tmp.Left && p2.x<tmp.Right && p2.y>=tmp.Top && p2.y<tmp.Bottom)
		{
		  p=&p1;
		  i++;
		}
		if(i==2) return true;
		while(i<2 && j<4)
		{
		  switch(j)
		  {
			  case 0: line2=TLine(tmp.left, tmp.top, tmp.right-1, tmp.top); break;
			  case 1: line2=TLine(tmp.right-1, tmp.top, tmp.right-1, tmp.bottom-1); break;
			  case 2: line2=TLine(tmp.right-1, tmp.bottom-1, tmp.left, tmp.bottom-1); break;
			  case 3: line2=TLine(tmp.left, tmp.top, tmp.left, tmp.bottom-1); break;
		  }
		  line=TLine(X1, Y1, X2, Y2);
		  if(line.IsCrossing(&line2, p))
		  {
			  if(p->x>=tmp.Left && p->x<tmp.Right && p->y>=tmp.Top && p->y<tmp.Bottom)
			  {
				  if(i==0) p=&p2;
				  i++;
			  }
		  }
		  j++;
		}
		if(i<2) return false;
		else
		{
			X1=p1.x;
			Y1=p1.y;
			X2=p2.x;
			Y2=p2.y;
			return true;
		}
	}

	bool Contains(Types::TPoint p)
	{
		float cp;

		cp=(float)(p.x-X2)/(float)(X1-X2);
		if(cp<0 || cp>1) return false;
		cp=cp*(float)Y1+(1.-cp)*(float)Y2;
		if((int)cp==p.y) return true;
		else return false;
	}

	bool Near(Types::TPoint p, int R)
	{
		int i, j;
		Types::TPoint p2;

		if(R==0) return Contains(p);
		for(i=-R; i<=R; i++)
			for(j=-R; j<=R; j++)
			{
				p2.x=p.x+i;
				p2.y=p.y+j;
				if(Contains(p2)) return true;
			}
		return false;
	}

	bool operator ==(const TLine& rc) const
	{
	   return X1 ==  rc.X1  && Y1==rc.Y1 &&
			  X2 ==  rc.X2  && Y2==rc.Y2;
	}

	bool operator !=(const TLine& rc) const
	{
	   return !(rc==*this);
	}
};

struct TFloatLine
{
	union
	{
		TDoublePoint XY1;
		struct
		{
			double X1;
			double Y1;
			char Temp1[4];
		};
	};
	union
	{
		TDoublePoint XY2;
		struct
		{
			double X2;
			double Y2;
			char Temp2[4];
		};
	};
	TFloatLine() { X1=Y1=X2=Y2=0; }
	TFloatLine(double x1, double y1, double x2, double y2) { X1=x1; Y1=y1; X2=x2; Y2=y2; }
	double Width() {return X2-X1;}
	double Height() {return Y2-Y1;}
	double Length() {try {return sqrt(Width()*Width()+Height()*Height());} catch(Exception &e) {return 0.0;}}
	void Shift(double x, double y) {X1+=x; X2+=x; Y1+=y; Y2+=y;}
	void Move(double x1, double y1) {X2=Width()+x1; X1=x1; Y2=Height()+y1; Y1=y1;}

	double Angle() //between adjacent cathetus and hypotenuse in radians
	{
		double f, w, h;

		w=Width();
		h=Height();
		if(h==0)
		{
			if(w>=0) return 0.;
			else return M_PI;
		}
		else if(w==0)
		{
			if(h>0) return M_PI_2;
			else return 3.*M_PI_2;
		}
		f=fabs(atan2(fabs(h), fabs(w)));
		if(w<0)
		{
			if(h>0) f=M_PI-f;
			else f=f-M_PI;
		}
		else if(h<0) f=-f;

		return f;
	}

	void SetLength(double L, double A)
	{
		double h;

		h=Height();
		if(h==0) h=1;
		X2=X1+L*cos(A);
		Y2=Y1+L*sin(A);
	}

	void SetLength(double L) {SetLength(L, Angle());}

	void Rotate(double r) {SetLength(Length(), r+Angle());}

	bool operator ==(const TFloatLine& rc) const
	{
	   return X1 ==  rc.X1  && Y1==rc.Y1 &&
			  X2 ==  rc.X2  && Y2==rc.Y2;
	}

	bool operator !=(const TFloatLine& rc) const
	{
	   return !(rc==*this);
	}
};

TFixedPoint __fastcall MyFixedPoint(int X, int Y)
{
	TFixedPoint p;

	p.X=X;
	p.Y=Y;

	return p;
}

void __fastcall DrawPolyLine(TBitmap32 *Bmp, TColor32 Color, TColor32 Border,
	int Width, bool Shadow, Gr32::TFixedPoint *LineFP, int linefp_cnt)
{
	TLine32 *Line32;
	TColor32Entry ce;
	int a;

	if(linefp_cnt>0)
	{
		Line32=new TLine32();
		try
		{
			Line32->EndStyle=esRounded;
			Line32->JoinStyle=jsRounded;
			Line32->SetPoints(LineFP, linefp_cnt-1);
			if(Shadow)
			{
				Line32->Translate(Width/2, Width/2);
				Line32->Draw(Bmp, Width, Color32(64, 64, 64, 128));
			}
			Line32->SetPoints(LineFP, linefp_cnt-1);
			Line32->Draw(Bmp, Width, Color, Border);
		}
		__finally
		{
			delete Line32;
		}
	}
};

#define MaxEllipsePoints	30

enum TEllipse32Drawing {edOutline, edFill, edOutlineFill};

void DrawEllipse32(TBitmap32 *Bmp, int x, int y, float rx, float ry,
	Types::TRect ViewPort, TColor32 Outer, TColor32 Inner,
	TEllipse32Drawing Fill, int Points=MaxEllipsePoints)
{
	TPolygon32 *poly;
	float xi, yi;
	int np=Points;
	float step;

	poly=new TPolygon32();
	poly->Antialiased=true;
	poly->Closed=true;
	try
	{
		if(rx>=ry)
		{
			if(rx<np) np=(int)(rx+0.5);
			else np=(int)((float)np*(2.-(float)np/rx));
		}
		else if(ry<np) np=(int)(ry+0.5);
		if(np>0 && np<8) np=8;
		if(np>0)
		{
			step=M_PI2/(float)np;
			for(int i=0; i<np; i++)
			{
				xi=(float)x+rx*cos((float)i*step);
				yi=(float)y+ry*sin((float)i*step);
				if(ViewPort.Width()>0)
				{
					if(xi<ViewPort.left) xi=ViewPort.left;
					else if(xi>ViewPort.right-1) xi=ViewPort.right-1;
				}
				if(ViewPort.Height()>0)
				{
					if(yi<ViewPort.top) yi=ViewPort.top;
					else if(yi>ViewPort.bottom-1) yi=ViewPort.bottom-1;
				}
				poly->Add(FixedPoint(xi, yi));
			}
			switch(Fill)
			{
				case edOutlineFill: poly->Draw(Bmp, Outer, Inner); break;
				case edOutline: poly->DrawEdge(Bmp, Outer); break;
				case edFill: poly->DrawFill(Bmp, Inner); break;
			}
		}
	}
	__finally
	{
		delete poly;
	}
}

// Rotate3DPointAround000 - 0P Vector rotating
// pX, pY, pZ - point coordinates in 3D
// aX, aY, aZ - rotate angles for each axis, in radians
// &rX, &rY, &rZ - result of rotation in 3D
/*void __fastcall Rotate3DPointAround000(double pX, double pY, double pZ, double aX, double aY, double aZ, double &rX, double &rY, double &rZ)
{
	double r, a;

	r = pX*pX+pZ*pZ;
	if(r>0)
	{
		r=sqrt(r);
		a = acos(pZ/r);
		if(pX < 0) a = 2*M_PI-a;
		a+=aY;
		pZ = r*cos(a);
		pX = r*sin(a);
	}
	r = pZ*pZ+pY*pY;
	if(r>0)
	{
		r=sqrt(r);
		a = acos(pZ/r);
		if(pY < 0) a=2*M_PI-a;
		a+=aX;
		//pZ = r*sin(a);
		//pY = r*cos(a);
		pZ = r*cos(a);
		pY = r*sin(a);
	}
	r = pX*pX+pY*pY;
	if(r>0)
	{
		r=sqrt(r);
		a = acos(pY/r);
		if(pX < 0) a = 2*M_PI-a;
		a+=aZ;
		pX = r*sin(a);
		pY = r*cos(a);
	}

	rX=pX; rY=pY; rZ=pZ;
}*/

class TGraphicObject;

typedef void __fastcall (__closure *TMouseScroll)(System::TObject* Sender);

typedef void __fastcall (__closure *TMouseClickEvent)(System::TObject* Sender, int X, int Y);

typedef void __fastcall (__closure *TMouseDockDropEvent)(System::TObject* Sender, System::TObject* Fallen, int X, int Y);

typedef void __fastcall (__closure *TMouseUpDownEvent)(System::TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y);

typedef void __fastcall (__closure *TMouseMoveEvent32)(System::TObject *Sender, TShiftState Shift, int X, int Y, TCustomLayer *Layer);
typedef void __fastcall (__closure *TMouseUpDownEvent32)(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y, TCustomLayer *Layer);
typedef void __fastcall (__closure *TMouseWheelEvent32)(TObject *Sender, TShiftState Shift, int WheelDelta, const Types::TPoint &MousePos, bool &Handled);
typedef void __fastcall (__closure *TCanResize)(TObject *Sender, int &Width, int &Height, bool &Resize);

typedef void __fastcall (__closure *TMouseBool)(bool Value);

class TGraphicObjectsList: public TMyObjectsList
{
private:
protected:
	TGraphicObject* __fastcall readItem(int Index) {return (TGraphicObject*)readMyObjectItem(Index);}
	void __fastcall writeItem(int Index, TGraphicObject* value) {writeMyObjectItem(Index, (TMyObject*)value);}
public:
	__fastcall TGraphicObjectsList(TMyObject *AOwner) : TMyObjectsList(AOwner) {}
	__fastcall ~TGraphicObjectsList() {}
	int __fastcall Add(TGraphicObject* o);

	void __fastcall Update(TMyObjectType Type);
	void __fastcall Update();
	void __fastcall ZoomUpdate(TMyObjectType Type);
	void __fastcall ZoomUpdate();

	__property TGraphicObject* Items[int Index] = {read=readItem, write=writeItem};
};

#define MouseHoldDelay 500

class TMouseHoldTimer: public TTimerThread
{
private:
	TGraphicObject *sender;
	TMouseButton mousebutton;
	TCustomLayer *layer;
	TShiftState shift;
	int x;
	int y;
	void __fastcall OnMouseHold();
public:
	__fastcall TMouseHoldTimer();
	__fastcall ~TMouseHoldTimer() {}
	void __fastcall StartHold(TGraphicObject *Sender, TMouseButton MouseButton,
		TShiftState Shift, int X, int Y, TCustomLayer *Layer);
	void __fastcall Stop() {TTimerThread::Stop(); sender=NULL;}
};

class TResourcePng: public TMyObject
{
private:
	TBitmap32* bmp;
	AnsiString name;
	int count;
protected:
public:
	__fastcall TResourcePng(AnsiString AName) : TMyObject() {count=1; name=AName; try {bmp=LoadPNG32Resource((System::UnicodeString)name);} catch(...) {bmp=NULL;}}
	__fastcall ~TResourcePng() {if(bmp) delete bmp;}

	void __fastcall AddUser() {count++;}
	void __fastcall DelUser() {if(count>0) count--;}

	__property TBitmap32* Bmp = {read=bmp};
	__property AnsiString Name = {read=name};
	__property int Count = {read=count}; //Number of objects which use this Png
};

class TPngContainer: public TMyObject
{
private:
	TMyObject *Owner;
	TMyObjectsNamedList *bitmaps;
	int __fastcall readCount() {return bitmaps->Count;}
	TResourcePng* __fastcall readIndexBmp(int Index) {return (TResourcePng*)bitmaps->Items[Index];}
protected:
public:
	__fastcall TPngContainer(TMyObject *AOwner);
	__fastcall ~TPngContainer();

	TBitmap32* __fastcall AddPng(AnsiString AName);
	void __fastcall DelPng(AnsiString AName);

	__property int Count = {read=readCount};
	__property TResourcePng* Bitmaps[int Index] = {read=readIndexBmp};
};/**/

class TGraphicObjectsContainer: public TMyObject
{
private:
	TImgView32 *image;
	TBitmapLayer *CustomLayer, *ControlLayer, *ToolsLayer;
	TGraphicObjectsList *CustomObjects, *ControlObjects;
	Types::TRect DrawRect;
	TMouseClickEvent onmouseclick;
	bool entered, AfterScroll, lockcontrols;
	bool *CustomObjectsChanged;
	int NewWidth, NewHeight;
	TMouseHoldTimer *mouseholdtimer;
	Types::TPoint MouseDownPoint;
	TPngContainer *pngcontainer;
	TList *ModalObjectsList;
	TMouseMoveEvent32 prevmousemove;
	TMouseUpDownEvent32 prevmouseup, prevmousedown;
	TMouseWheelEvent32 prevmousewheel;
	TMouseScroll prevmouseclick, prevmousedblclick, prevscroll, prevresize;
	TCanResize prevcanresize;
	bool useevents;
// TGraphicObject Animation
	TMultiTimerThread* objectsglobaltimer;
	TMultiTimerThread* readObjectsGlobalTimer() {if(!objectsglobaltimer) objectsglobaltimer=new TMultiTimerThread(); return objectsglobaltimer;}
// End of Animation
	TGraphicObject* __fastcall readCurrentModalObject();
protected:
	TGraphicObjectsList *ToolsObjects;
	void* RadarMapSettings;
	TMouseAction *mouseaction;
    TVoidFunction disablemouseaction;
	int __fastcall readCustomObjectsCount() {return CustomObjects->Count;}
	int __fastcall readControlObjectsCount() {return ControlObjects->Count;}
	int __fastcall readToolsObjectsCount() {return ToolsObjects->Count;}
	TGraphicObjectsList* __fastcall readCustomObjects(void);
	TImgView32* __fastcall readImage() {return image;}
	Types::TRect __fastcall readViewPort() {return image->GetViewportRect();} // overload
	void __fastcall writeOnMouseClick(TMouseClickEvent value) {onmouseclick=value;}
	void __fastcall SmallResize();
	void __fastcall writeMouseAction(TMouseAction *value) {if(mouseaction!=NULL) *mouseaction=*value;}
	TMouseAction* __fastcall readMouseAction(void) {return mouseaction;}
public:
	__fastcall TGraphicObjectsContainer(TImgView32 *ImgView, bool UseEvents, TMouseAction* ma, void* RMS);
	__fastcall ~TGraphicObjectsContainer(void);
	int __fastcall AddObject(TMyObject* o);
	TGraphicObject* __fastcall GetObject(TMyObjectType Type, int Index);
	void __fastcall Delete(TMyObjectType Type, int Index);
	int __fastcall SendToBack(TMyObjectType Type, int Index);
	int __fastcall BringToFront(TMyObjectType Type, int Index);
	int __fastcall OneStepBack(TMyObjectType Type, int Index);
	int __fastcall OneStepForward(TMyObjectType Type, int Index);
	void __fastcall CustomUpdate() {if(CustomLayer!=NULL) CustomLayer->Update();}// AutomaticUpdate=true;}
	void __fastcall ControlUpdate() {if(ControlLayer!=NULL) ControlLayer->Update();}// AutomaticUpdate=true;}
	void __fastcall ToolsUpdate() {if(ToolsLayer!=NULL) ToolsLayer->Update();}// AutomaticUpdate=true;}
	virtual void __fastcall Update(TMyObjectType Type) {if(Type==gotCustom) CustomUpdate(); else if(Type==gotControl) ControlUpdate(); else if(Type==gotTool) ToolsUpdate();} //overload
	virtual void __fastcall Update() {CustomUpdate(); ToolsUpdate(); ControlUpdate();}
	void __fastcall Update(TMyObjectType Type, Types::TRect UpdateRect);
	void __fastcall RemoveChildFromList(TMyObject* o);
	void __fastcall ZoomUpdate(TMyObjectType Type) {if(Type==gotCustom) CustomObjects->ZoomUpdate(); else if(Type==gotControl) ControlObjects->ZoomUpdate(); else if(Type==gotTool) ToolsObjects->ZoomUpdate();}
	void __fastcall ZoomUpdate() {CustomObjects->ZoomUpdate(); ControlObjects->ZoomUpdate(); ToolsObjects->ZoomUpdate();}

	virtual void __fastcall MouseMove(System::TObject* Sender,
		  Classes::TShiftState Shift, int X, int Y, TCustomLayer *Layer);
	virtual void __fastcall MouseDown(TObject *Sender,
		  TMouseButton Button, TShiftState Shift, int X, int Y, TCustomLayer *Layer);
	virtual void __fastcall MouseUp(TObject *Sender,
		  TMouseButton Button, TShiftState Shift, int X, int Y, TCustomLayer *Layer);
	void __fastcall MouseWheel(TObject *Sender,
		  TShiftState Shift, int WheelDelta, const Types::TPoint &MousePos, bool &Handled);
	void __fastcall MouseClick(TObject *Sender);
	void __fastcall MouseDblClick(TObject *Sender);
	void __fastcall OnResize(TObject *Sender);
	void __fastcall OnCanResize(TObject *Sender, int &Width, int &Height, bool &Resize);
	virtual void __fastcall OnScroll(TObject *Sender) {AfterScroll=true;}
	void __fastcall CustomLayerPaint(TObject *Sender, TBitmap32 *Bmp);
	void __fastcall ControlLayerPaint(TObject *Sender, TBitmap32 *Bmp);
	void __fastcall ToolsLayerPaint(TObject *Sender, TBitmap32 *Bmp);
	void __fastcall ClearCustomObjects(TMyObject *PB=NULL) {CustomObjects->Clear(PB);}
	void __fastcall NewCustomObjects() {CustomObjects=new TGraphicObjectsList(this);}
	void __fastcall AssignCustomObjectsList(TGraphicObjectsList* value);
	TGraphicObjectsList* __fastcall GetToolsObjectsList() {return ToolsObjects;}
	void __fastcall SetModalObject(TGraphicObject *po);
	void __fastcall ClearModalObject(TGraphicObject *po);
	void __fastcall SetEntered(bool value) {entered=false;}

	__property int CustomObjectsCount = {read=readCustomObjectsCount};
	__property int ControlObjectsCount = {read=readControlObjectsCount};
	__property int ToolsObjectsCount = {read=readToolsObjectsCount};
	__property TImgView32 *Image = {read=readImage};
	__property TMouseClickEvent OnMouseClick = {read=onmouseclick, write=writeOnMouseClick};
	__property TBitmapLayer* LayerCustom = {read=CustomLayer};
	__property TBitmapLayer* LayerControl = {read=ControlLayer};
	__property bool Entered = {read=entered};
	__property TGraphicObjectsList *CustomObjectsList = {read=readCustomObjects};
	__property bool* ObjectsChanged = {read=CustomObjectsChanged, write=CustomObjectsChanged};
	__property TVoidFunction DisableMouseAction = {read=disablemouseaction, write=disablemouseaction};
	__property TMouseAction* MouseAction = {read=readMouseAction, write=writeMouseAction};
	__property Types::TRect ViewPort = {read=readViewPort};
	__property TPngContainer *PngContainer = {read=pngcontainer};
	__property bool LockControls = {read=lockcontrols, write=lockcontrols};
	__property TMultiTimerThread* ObjectsGlobalTimer = {read=readObjectsGlobalTimer};
	__property TGraphicObject* CurrentModalObject = {read=readCurrentModalObject};
};

enum TAnimationType {atCustom=0, atDisappear=-1, atAppear=1};

class TGraphicObject: public TMyObject
{
//private:
protected:
	TMyObject *owner;
	Types::TRect Rect;
	TMouseClickEvent onmouseclick, onmousedblclick;
	TMouseUpDownEvent onmousedown, onmouseup, onmousehold;
	TMouseDockDropEvent ondockdrop;
	TGraphicObject *draggingobject;
	int x1, y1;
	bool visible;
	bool fullmodality;
	bool resized;
protected:
	bool mouseable, forcemouseable, dockable;
	bool pressed, inrect, indragndrop, allowneighbormousemove; //, visibl
	Types::TRect output;
// Animation
	bool AnimationInProgress;
	TAnimationType AnimationType;
	TTimerThread* LocalAnimationTimer;
	TMultiTimerItem* AnimationTimerItem;
	int AnimationFrameInterval;
	TMultiTimerThread* globaltimer;
	virtual bool __fastcall StartAnimation(TAnimationType aType);
	virtual bool __fastcall StopAnimation();
	virtual void __fastcall OnAnimation() {}
// End of Animation
	virtual Types::TRect __fastcall readClientRect() {return Rect;}
	void __fastcall writeOwner(TMyObject* value) {owner=value;}
	virtual void __fastcall writePressed(bool value);
	virtual void __fastcall writeInRect(bool value);
	virtual void __fastcall writeInDragNDrop(bool value) {if(value!=indragndrop) indragndrop=value;}
	virtual void __fastcall writeVisible(bool value) {if(value!=visible) {visible=value; Update();}}
	void __fastcall writeOnMouseClick(TMouseClickEvent value) {onmouseclick=value;}
	void __fastcall writeOnMouseDown(TMouseUpDownEvent value) {onmousedown=value;}
	void __fastcall writeOnMouseUp(TMouseUpDownEvent value) {onmouseup=value;}
	void __fastcall writeOnMouseHold(TMouseUpDownEvent value) {onmousehold=value;}
	void __fastcall writeOnMouseDblClick(TMouseClickEvent value) {onmousedblclick=value;}
	void __fastcall writeOnDockDrop(TMouseDockDropEvent value) {ondockdrop=value;}
	virtual void __fastcall writeOutput(Types::TRect value);
	void __fastcall DecreaseGlyph(TBitmap32 *InOut);
	virtual bool __fastcall readMouseable() {return mouseable;}
	virtual bool __fastcall readForceMouseable() {return forcemouseable;}
	TBitmap32* __fastcall GetPngFromContainer(AnsiString AName);
	void __fastcall RemovePngFromContainer(AnsiString AName);
	virtual void __fastcall SelfUpdate() {}
public:
	__fastcall virtual TGraphicObject(TMyObject *AOwner, TMyObjectType Type, Types::TRect OutputRect);
	__fastcall virtual ~TGraphicObject();

	virtual void __fastcall Update(TMyObjectType Type); //overload
	virtual void __fastcall Update() {Update(objecttype);}
	virtual void __fastcall ZoomUpdate(TMyObjectType Type) {TMyObject::ZoomUpdate(Type);}
	virtual void __fastcall ZoomUpdate() {ZoomUpdate(objecttype);}

	virtual void __fastcall Redraw(TObject *Sender, TBitmap32 *Bmp) {}
	virtual void __fastcall MouseMove(System::TObject* Sender,
		  Classes::TShiftState Shift, int X, int Y, TCustomLayer *Layer) {}
	virtual void __fastcall MouseDown(TObject *Sender,
		  TMouseButton Button, TShiftState Shift, int X, int Y,
		  TCustomLayer *Layer) {if(OnMouseDown!=NULL) (OnMouseDown)((TObject*)this, Button, Shift, X, Y);}
	virtual void __fastcall MouseUp(TObject *Sender,
		  TMouseButton Button, TShiftState Shift, int X, int Y,
		  TCustomLayer *Layer) {if(OnMouseUp!=NULL) (OnMouseUp)((TObject*)this, Button, Shift, X, Y);}
	virtual void __fastcall MouseHold(TObject *Sender,
		  TMouseButton Button, TShiftState Shift, int X, int Y,
		  TCustomLayer *Layer) {if(OnMouseHold!=NULL) (OnMouseHold)((TObject*)this, Button, Shift, X, Y); Pressed=false;}
	virtual void __fastcall MouseWheel(TObject *Sender,
		  TShiftState Shift, int WheelDelta, const Types::TPoint &MousePos,
		  bool &Handled, TCustomLayer *Layer) {}
	virtual void __fastcall MouseClick(System::TObject* Sender, int X, int Y);
	virtual void __fastcall MouseDblClick(System::TObject* Sender, int X, int Y);
	virtual void __fastcall OnBeforeResize(TObject *Sender, int NewWidth, int NewHeight) {resized=false;}
	virtual void __fastcall OnResize(TObject *Sender, int NewWidth, int NewHeight);
	virtual void __fastcall DockDrop(System::TObject* Sender, System::TObject* Fallen, int X, int Y);
	virtual bool __fastcall IsInClientRect(Types::TRect value) {return ClientRect.Contains(value);}
	virtual bool __fastcall IsInClientRect(Types::TPoint value) {return ClientRect.Contains(value);}

	virtual void __fastcall Show() {Visible=true;}
	virtual void __fastcall Hide() {Visible=false; if(Owner && Owner->ObjectType==gotContainer) ((TGraphicObjectsContainer*)Owner)->ClearModalObject(this);}
	void __fastcall ShowModal() {if(Owner && Owner->ObjectType==gotContainer) ((TGraphicObjectsContainer*)Owner)->SetModalObject(this); Show();}

// Animation
	__property TMultiTimerThread *GlobalTimer = {read=globaltimer, write=globaltimer};
// End of Animation

	__property Types::TRect ClientRect = {read=readClientRect};
	__property TMyObject* Owner = {read=owner, write=writeOwner};
	__property bool Pressed = {read=pressed, write=writePressed};
	__property bool InRect = {read=inrect, write=writeInRect};
	__property bool InDragNDrop = {read=indragndrop, write=writeInDragNDrop};
	__property bool Visible = {read=visible, write=writeVisible};
	__property bool Dockable = {read=dockable, write=dockable};
	__property Types::TRect Output = {read=output, write=writeOutput};
	__property int X1 = {read=x1, write=x1}; //MouseDown Entrance point X
	__property int Y1 = {read=y1, write=y1}; //MouseDown Entrance point Y
	__property bool Mouseable = {read=readMouseable};
	__property bool ForceMouseable = {read=readForceMouseable};
	__property bool AllowNeighborMouseMove = {read=allowneighbormousemove};
	__property TGraphicObject* DraggingObject = {read=draggingobject, write=draggingobject};

	__property bool FullModality = {read=fullmodality, write=fullmodality}; //If true => all container mouse events work on the object only, otherwise OnMouseClick outside of the object hides the object
	__property bool Resized = {read=resized};

	__property TMouseClickEvent OnMouseClick = {read=onmouseclick, write=writeOnMouseClick};
	__property TMouseUpDownEvent OnMouseDown = {read=onmousedown, write=writeOnMouseDown};
	__property TMouseUpDownEvent OnMouseUp = {read=onmouseup, write=writeOnMouseUp};
	__property TMouseUpDownEvent OnMouseHold = {read=onmousehold, write=writeOnMouseHold};
	__property TMouseClickEvent OnMouseDblClick = {read=onmousedblclick, write=writeOnMouseDblClick};
	__property TMouseDockDropEvent OnDockDrop = {read=ondockdrop, write=writeOnDockDrop};
};

//Contol objects

enum TControlObjectType {cotItem, cotContainer};

class TControlObject: public TGraphicObject
{
private:
	TPngContainer *pngcontainer;
protected:
	TControlObjectType controltype;
public:
	__fastcall TControlObject(TMyObject *AOwner, Types::TRect Rect)
		: TGraphicObject(AOwner, gotControl, Rect) {controltype=cotItem; pngcontainer=new TPngContainer(this);};
	__fastcall ~TControlObject() {try {delete pngcontainer;} catch(...) {}}

	__property TControlObjectType ControlType = {read=controltype};
	__property TPngContainer *PngContainer = {read=pngcontainer};
};

class TManagerObject: public TGraphicObject
{
protected:
	TObject *Manager;
public:
	__fastcall TManagerObject(TMyObject *AOwner, TMyObjectType Type, Types::TRect OutputRect,
		TObject* RadarMapManager) : TGraphicObject(AOwner, Type, OutputRect) {Manager=RadarMapManager;}
	__fastcall ~TManagerObject() {}
};

/*class TProfileScroller: public TManagerObject
{
private:
	int dX;
	TProfileOutputView* outputview;
	void __fastcall DrawLine32(TBitmap32 *Bmp, TColor32 Color, TColor32 Border,
		int Width, bool Shadow, TLine line);
protected:
	TProfileOutputView* __fastcall readOutputView(void);
	void __fastcall writeOutputView(TProfileOutputView* value) {outputview=value; if(Owner!=NULL) Owner->SetObjectChanged(objecttype);}
public:
	__fastcall TProfileScroller(TMyObject *AOwner, Types::TRect OutputRect,
		TProfileOutputView *OV, TObject* RadarMapManager);
	__fastcall ~TProfileScroller() {}
	void __fastcall MouseClick(System::TObject* Sender, int X, int Y);
	void __fastcall Redraw(TObject *Sender, TBitmap32 *Bmp);
	void __fastcall MouseMove(System::TObject* Sender,
		  Classes::TShiftState Shift, int X, int Y, TCustomLayer *Layer);
	void __fastcall MouseUp(TObject *Sender,
		  TMouseButton Button, TShiftState Shift, int X, int Y,
		  TCustomLayer *Layer);
	void __fastcall OnResize(TObject *Sender, int NewWidth, int NewHeight);

	__property TProfileOutputView* OutputView = {read=readOutputView, write=writeOutputView};
};*/

class TProfileScroller: public TManagerObject
{
private:
	int dX, OnMouseMovePathUpdateCount;
	TList *List, *ScrollList;
	TBitmap32 *Background;
	bool showinfo;
	void __fastcall DrawLine32(TBitmap32 *Bmp, TColor32 Color, TColor32 Border,
		int Width, bool Shadow, TLine line);
	void __fastcall Draw(TBitmap32 *MainBmp, TBitmap32 *Bmp, TColor32 Color, TColor32 Border,
		int x, int y, TLine line, int h, int Alpha);
	TProfileOutputView* __fastcall readoutputview(void);
	void __fastcall writeoutputview(TProfileOutputView* value);
	TBitmap32* __fastcall readScrollBmp(int Index) {if(Index>=0 && Index<ScrollList->Count) return (TBitmap32*)ScrollList->Items[Index]; else return NULL;}
	void __fastcall writeShowInfo(bool value) {if(showinfo!=value) {showinfo=value; Update();}}
	__property TProfileOutputView* outputview = {read=readoutputview, write=writeoutputview};
	__property TBitmap32* ScrollBmp[int i] = {read=readScrollBmp};
protected:
	TProfileOutputView* __fastcall readOutputView(void);
	void __fastcall writeOutputView(TProfileOutputView* value) {outputview=value; if(Owner!=NULL) Owner->SetObjectChanged(objecttype);}
public:
	__fastcall TProfileScroller(TMyObject *AOwner, Types::TRect OutputRect,
		TProfileOutputView *OV, TObject* RadarMapManager);
	__fastcall ~TProfileScroller();

	void __fastcall MouseClick(System::TObject* Sender, int X, int Y);
	void __fastcall Redraw(TObject *Sender, TBitmap32 *Bmp);
	void __fastcall MouseMove(System::TObject* Sender,
		  Classes::TShiftState Shift, int X, int Y, TCustomLayer *Layer);
	void __fastcall MouseUp(TObject *Sender,
		  TMouseButton Button, TShiftState Shift, int X, int Y,
		  TCustomLayer *Layer);
	void __fastcall OnResize(TObject *Sender, int NewWidth, int NewHeight);

	__property TProfileOutputView* OutputView = {read=readOutputView, write=writeOutputView};
	__property bool ShowInfo = {read=showinfo, write=writeShowInfo};
};

//---------------------------------------------------------------------------
// Profile objects;
//---------------------------------------------------------------------------
enum TProfileObjectType {potNone, potProfileLabel, potMapLabel, potReferenceLabel,
	potStartNGoLabel, potStartNGoGPSLabel, potWaypointLabel, potMapPath,
	potMapCustomPath, potMapTrackPath, potMapLabelGeometry};

class TProfileObject: public TManagerObject
{
protected:
	TProfileObjectType profileobjecttype;
	TProfileOutputView* outputview;
	void __fastcall writeOutputView(TProfileOutputView* value) {outputview=value; if(Owner!=NULL) Owner->SetObjectChanged(objecttype);}
	virtual TProfileOutputView* __fastcall readOutputView(void) {return outputview;}
public:
	__fastcall TProfileObject(TMyObject *AOwner, Types::TRect OutputRect,
		TProfileOutputView *OV, TObject* RadarMapManager) : TManagerObject(AOwner, gotCustom, OutputRect, RadarMapManager) {outputview=OV; profileobjecttype=potNone;}
	__fastcall ~TProfileObject() {}

	__property TProfileOutputView* OutputView = {read=readOutputView, write=writeOutputView};
	__property TProfileObjectType ProfileObjectType = {read=profileobjecttype};
};

enum TProfileLabelType {   pltVertLine      = 0,  pltTransformed     = 1,
	pltCircleRed     = 2,  pltCircleGreen   = 3,  pltCircleBlue      = 4,
	pltSquareRed     = 5,  pltSquareGreen   = 6,  pltSquareBlue      = 7,
	pltTriangleRed   = 8,  pltTriangleGreen = 9,  pltTriangleBlue    = 10,
	pltNone          = 11, pltCustom        = 12,
	pltWaterCircle   = 13, pltWaterRect     = 14, pltWaterTriangle   = 15,
	pltElectroCircle = 16, pltElectroRect   = 17, pltElectroTriangle = 18,
	pltGasCircle 	 = 19, pltGasRect       = 20, pltGasTriangle     = 21,
	pltGasHighCircle = 22, pltGasHighRect   = 23, pltGasHighTriangle = 24,
	pltSewageCircle  = 25, pltSewageRect    = 26, pltSewageTriangle  = 27,
	pltTelecomCircle = 28, pltTelecomRect   = 29, pltTelecomTriangle = 30,

	pltAutoDetectCustom = 31,
	pltAutoDetect    = 32, pltAutoDetectPos = 33, pltAutoDetectNeg   = 34,

	pltFiberCircle   = 35, pltFiberRect     = 36, pltFiberTriangle   = 37};

const AnsiString TProfileLabelTypeStrings[]=
{
	"pltVertLine",
	"pltTransformed",
	"pltCircleRed",
	"pltCircleGreen",
	"pltCircleBlue",
	"pltSquareRed",
	"pltSquareGreen",
	"pltSquareBlue",
	"pltTriangleRed",
	"pltTriangleGreen",
	"pltTriangleBlue",
	"pltNone",
	"pltCustom",
	"pltWaterCircle",
	"pltWaterRect",
	"pltWaterTriangle",
	"pltElectroCircle",
	"pltElectroRect",
	"pltElectroTriangle",
	"pltGasCircle",
	"pltGasRect",
	"pltGasTriangle",
	"pltGasHighCircle",
	"pltGasHighRect",
	"pltGasHighTriangle",
	"pltSewageCircle",
	"pltSewageRect",
	"pltSewageTriangle",
	"pltTelecomCircle",
	"pltTelecomRect",
	"pltTelecomTriangle",
	"pltAutoDetectCustom",
	"pltAutoDetect",
	"pltAutoDetectPos",
	"pltAutoDetectNeg",
	"pltFiberCircle",
	"pltFiberRect",
	"pltFiberTriangle"
};

TProfileLabelType StrToTProfileLabelType(AnsiString str);

class TCustomProfileLabel: public TProfileObject
{
private:
	unsigned int id;
	TBitmap32 *normalglyph, *texticon, *hottexticon, *attachmenticon,
		*hotattachmenticon;
	AnsiString normalglyphname, texticonname, hottexticonname,
		attachmenticonname, hotattachmenticonname;
	AnsiString attachment;
	float z;
protected:
	int x, y;
	bool allwayshot;
	TColor32 color;
	TDateTime created, modified;
	TGPSCoordinate *coordinate;
	bool icons;
	TBitmap32 *Glyph;
	//TPngContainer *Glyph;
	TTrace *trace;
	int traceindex;
	Types::TPoint center;
	TCustomProfileLabel* connectedobject;
	//TBitmap32 *TextIcon, *HotTextIcon, *AttachmentIcon, *HotAttachmentIcon;
	float gpsconfidence;
	Types::TRect *ProfRect;
	int profileindex;
	TStrings *DescriptionStrs;
	TProfileLabelType labeltype;
	//void __fastcall writeNormalGlyph(TBitmap32 *value);
	AnsiString __fastcall readDescription() {if(DescriptionStrs) return DescriptionStrs->Text; else return "";}
	void __fastcall writeNormalGlyphName(AnsiString value);
	void __fastcall writeTextIconName(AnsiString value) {if(texticonname!=value) {if(texticonname!="") {RemovePngFromContainer(texticonname); if(Glyph==texticon) Glyph=NULL; texticon=NULL;} if(value!="") texticon=GetPngFromContainer(value); texticonname=value;}}
	void __fastcall writeHotTextIconName(AnsiString value) {if(hottexticonname!=value) {if(hottexticonname!="") {RemovePngFromContainer(hottexticonname); if(Glyph==hottexticon) Glyph=NULL; hottexticon=NULL;} if(value!="") hottexticon=GetPngFromContainer(value); hottexticonname=value;}}
	void __fastcall writeAttachmentIconName(AnsiString value) {if(attachmenticonname!=value) {if(attachmenticonname!="") {RemovePngFromContainer(attachmenticonname); if(Glyph==attachmenticon) Glyph=NULL; attachmenticon=NULL;} if(value!="") attachmenticon=GetPngFromContainer(value); attachmenticonname=value;}}
	void __fastcall writeHotAttachmentIconName(AnsiString value) {if(hotattachmenticonname!=value) {if(hotattachmenticonname!="") {RemovePngFromContainer(hotattachmenticonname); if(Glyph==hotattachmenticon) Glyph=NULL; hotattachmenticon=NULL;} if(value!="") hotattachmenticon=GetPngFromContainer(value); hotattachmenticonname=value;}}
	virtual void __fastcall writeTraceIndex(int value);
	virtual void __fastcall writeProfileIndex(int value);
	void __fastcall writeColor(TColor32 value);
	void __fastcall writeConnectedObject(TCustomProfileLabel* value);
	virtual void __fastcall writeDescription(AnsiString value);
	void __fastcall writeAttachment(AnsiString value);
	void __fastcall writeIcons(bool value) {if(value!=icons) {icons=value; Update();}}
	void __fastcall writeCenter(Types::TPoint value) {if(value!=center) {center=value; Update();}}
	void __fastcall writeAllwaysHot(bool value) {if(value!=allwayshot) {allwayshot=value; Update();}}
	virtual void __fastcall writeCoordinate(TGPSCoordinate *value);
	virtual TGPSCoordinate* __fastcall readCoordinate();
	virtual void __fastcall writeZ(float value) {z=value;}
	virtual float __fastcall readZ() {return z;}
	//virtual void __fastcall writeID(unsigned int value) {id=value;}
	virtual void __fastcall writeID(unsigned int value);
	__property TBitmap32* TextIcon = {read=texticon};
	__property AnsiString TextIconName = {read=texticonname, write=writeTextIconName};
	__property TBitmap32* HotTextIcon = {read=hottexticon};
	__property AnsiString HotTextIconName = {read=hottexticonname, write=writeHotTextIconName};
	__property TBitmap32* AttachmentIcon = {read=attachmenticon};
	__property AnsiString AttachmentIconName = {read=attachmenticonname, write=writeAttachmentIconName};
	__property TBitmap32* HotAttachmentIcon = {read=hotattachmenticon};
	__property AnsiString HotAttachmentIconName = {read=hotattachmenticonname, write=writeHotAttachmentIconName};
public:
	__fastcall TCustomProfileLabel(TMyObject *AOwner, Types::TRect OutputRect,
		TProfileOutputView *OV, int X, int Y, TObject* RadarMapManager);
	__fastcall ~TCustomProfileLabel();

	virtual void __fastcall MouseUp(TObject *Sender, TMouseButton Button,
		TShiftState Shift, int X, int Y, TCustomLayer *Layer);
	virtual void __fastcall MouseHold(TObject *Sender, TMouseButton Button,
		TShiftState Shift, int X, int Y, TCustomLayer *Layer);
	void __fastcall IconsDraw(TBitmap32 *Bmp);
	virtual void __fastcall MouseClick(System::TObject* Sender, int X, int Y);

	void __fastcall WriteCreatedTime(AnsiString Str) {created=MyStrToDateTime(Str);}
	void __fastcall WriteModifiedTime(AnsiString Str) {modified=MyStrToDateTime(Str);}

	__property int TraceIndex = {read=traceindex, write=writeTraceIndex};
	__property int ProfileIndex = {read=profileindex, write=writeProfileIndex};
	__property TBitmap32* NormalGlyph = {read=normalglyph};//, write=writeNormalGlyph};
	__property AnsiString NormalGlyphName = {read=normalglyphname, write=writeNormalGlyphName};
	__property TColor32 Color = {read=color, write=writeColor};
	__property TTrace* Trace = {read=trace};
	__property int Xx = {read=x};
	__property int Yy = {read=y};
	__property TCustomProfileLabel* ConnectedObject = {read=connectedobject, write=writeConnectedObject};
	__property AnsiString Description = {read=readDescription, write=writeDescription};
	__property AnsiString Attachment = {read=attachment, write=writeAttachment};
	__property bool Icons = {read=icons, write=writeIcons};
	__property Types::TPoint Center = {read=center, write=writeCenter};
	__property bool AllwaysHot = {read=allwayshot, write=writeAllwaysHot};
	__property TGPSCoordinate *Coordinate = {read=readCoordinate, write=writeCoordinate};
	__property TDateTime CreatedTime = {read = created};
	__property TDateTime ModifiedTime = {read = modified};
	__property float GPSConfidence = {read=gpsconfidence, write=gpsconfidence};
	__property float Z = {read=readZ, write=writeZ};
	__property unsigned int ID = {read=id, write=writeID};
	__property TProfileLabelType ProfileLabelType = {read=labeltype};
};

class TProfileLabel: public TCustomProfileLabel
{
private:
	int sample, dX, dY, old_traceindex, old_sample, old_profindex;
	int VertLineWidth;
	bool locked;
	TBitmap32 *hotglyph, *smallglyph, *HotLockedGlyph, *LockedGlyph;
	TBitmap32 *Glyph;
	AnsiString LockedGlyphName, HotLockedGlyphName,	hotglyphname, smallglyphname;
	void *data;
	void __fastcall Initialize(void);
protected:
	//void __fastcall writeHotGlyph(TBitmap32 *value);
	//void __fastcall writeSmallGlyph(TBitmap32 *value);
	void __fastcall writeHotGlyphName(AnsiString value) {if(hotglyphname!=value) {if(hotglyphname!="") {RemovePngFromContainer(hotglyphname); hotglyph=NULL;} if(value!="") hotglyph=GetPngFromContainer(value); hotglyphname=value;}}
	void __fastcall writeSmallGlyphName(AnsiString value) {if(smallglyphname!=value) {if(smallglyphname!="") {RemovePngFromContainer(smallglyphname); smallglyph=NULL;} if(value!="") smallglyph=GetPngFromContainer(value); smallglyphname=value;}}
	void __fastcall writeSample(int value);
	void __fastcall writeLocked(bool value);
	void __fastcall writeColor(TColor32 value);
	void __fastcall writeInRect(bool value); //overload
	float __fastcall readZ();
public:
	__fastcall TProfileLabel(TMyObject *AOwner, Types::TRect OutputRect,
		TProfileOutputView *OV, TProfileLabelType type, int X, int Y, TObject* RadarMapManager);
	__fastcall TProfileLabel(TMyObject *AOwner, Types::TRect OutputRect,
		TProfileOutputView *OV, TProfileLabelType type, int trcIndex, int smpl, TColor32 Cl, TObject* RadarMapManager);
	__fastcall TProfileLabel(TMyObject *AOwner, Types::TRect OutputRect,
		TProfileOutputView *OV, int trcIndex, TColor32 Cl, TObject* RadarMapManager);
	__fastcall ~TProfileLabel();

	void __fastcall Redraw(TObject *Sender, TBitmap32 *Bmp);
	void __fastcall MouseMove(System::TObject* Sender,
		  Classes::TShiftState Shift, int X, int Y, TCustomLayer *Layer);
	void __fastcall MouseDown(TObject *Sender,
		  TMouseButton Button, TShiftState Shift, int X, int Y, TCustomLayer *Layer);
	void __fastcall MouseUp(TObject *Sender,
		  TMouseButton Button, TShiftState Shift, int X, int Y, TCustomLayer *Layer);
	void __fastcall OnResize(TObject *Sender, int NewWidth, int NewHeight);
	void __fastcall MouseDblClick(System::TObject* Sender, int X, int Y); //Unlock
	void __fastcall MouseClick(System::TObject* Sender, int X, int Y) {}

	void __fastcall RestoreOldPos() {if(old_sample>=0 && old_traceindex>=0 && old_profindex>=0) {sample=old_sample; traceindex=old_traceindex; ProfileIndex=old_profindex; old_sample=old_traceindex=old_profindex=-1;}}

	__property int Sample = {read=sample, write=writeSample};
	__property TBitmap32* SmallGlyph = {read=smallglyph};//, write=writeSmallGlyph};
	__property TBitmap32* HotGlyph = {read=hotglyph};//, write=writeHotGlyph};
	__property AnsiString SmallGlyphName = {read=smallglyphname, write=writeSmallGlyphName};
	__property AnsiString HotGlyphName = {read=hotglyphname, write=writeHotGlyphName};
	__property bool Locked = {read=locked, write=writeLocked};
	__property TTrace* Trace = {read=trace};
	__property void* Data = {read=data, write=data};
};

class TToolObject: public TManagerObject
{
protected:
	TImgView32 *image;
	bool visual;
public:
	__fastcall TToolObject(TMyObject *AOwner, TImgView32 *ImgView, Types::TRect Rect, TObject *AManager, bool AVisual=false)
		: TManagerObject(AOwner, gotTool, Rect, AManager) {image=ImgView; visual=AVisual; visible=false;}
	__fastcall ~TToolObject() {}

	__property bool Visual = {read=visual};
};

#endif
