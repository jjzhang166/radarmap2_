//---------------------------------------------------------------------------

#ifndef SettingsH
#define SettingsH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <ImgList.hpp>
#include <ExtCtrls.hpp>
#include <pngimage.hpp>
#include <Mask.hpp>
#include <Dialogs.hpp>
#include <jpeg.hpp>
#include <Graphics.hpp>
#include "Manager.h"
#include <GR32_Image.hpp>
#include "GainItems.h"
#include <ImageButton.h>
#include <System.ImageList.hpp>
#ifdef _RAD101
	#include <System.ImageList.hpp>
	#include <Vcl.ComCtrls.hpp>
#endif

//---------------------------------------------------------------------------
typedef void __fastcall (__closure *TWriteCommandEvent)(char *cmd); //see GetGPSData.h line 72, TNmeaClient->WriteCommand
typedef void __fastcall (__closure *TPauseResumeEvent)(); //see GetGPSData.h line 73, 74, TNmeaClient->PauseTerminal, ResumeTerminal

/*void __fastcall CheckImage(TObject *Sender);
void __fastcall UncheckImage(TObject *Sender);
void __fastcall CheckUncheckImage(TObject *Sender);
void __fastcall CheckRadioImagesOnPanel(TObject *Sender);*/

struct TPaletteItem
{
private:
	int id;
	int position, min, max;
	TColor readColor() {if(Shape) return Shape->Brush->Color; else return clWhite;}
	bool readChecked() {if(CheckImage) return CheckImage->Down; else return false;}
	bool readSelected() {if(Panel) return Panel->BorderStyle!=bsNone; else return false;}
	void writeColor(TColor value) {if(Shape && Caption) {Shape->Brush->Color=value; Caption->Color=value; Caption->Font->Color=(TColor)(value^0xFFFFFF);}}
	void writeID(int value) {id=value; if(Caption) Caption->Caption=IntToStr(id);}
	void writeChecked(bool value) {if(CheckImage) {CheckImage->Down=!value; CheckImage->Toggle(); if(Cover) Cover->Visible=!value;}}
	void writeSelected(bool value) {if(Panel) {if(value) Panel->BorderStyle=bsSingle; else Panel->BorderStyle=bsNone;}}
	void writeMin(int value) {min=value; if(max<min) Max=min;}
	void writeMax(int value) {max=value; if(max<min) Min=max;}
	void writePosition(int value) {position=value;}
public:
	TPaletteItem() {Panel=NULL; CheckImage=NULL; Icon=NULL; Cover=NULL; Caption=NULL; Shape=NULL; ID=0; Min=0; Max=0; Position=0; Color=clWhite; Selected=false;}
	TPaletteItem(int aID, int aPos, TColor AColor, TPanel *APanel, TImageButton *ACheckImage, TImage *AIcon, TImage *ACover, TLabel *ACaption, TShape *AShape)
		{Panel=APanel; CheckImage=ACheckImage; Icon=AIcon; Cover=ACover; Caption=ACaption; Shape=AShape; ID=aID; Min=0; Max=PaletteMaxPosition; Position=aPos; Color=AColor; Selected=false;}

	TPanel* Panel;
	TImageButton* CheckImage;
	TImage* Icon;
	TImage* Cover;
	TLabel* Caption;
	TShape* Shape;

	__property TColor Color = {read=readColor, write=writeColor};
	__property int ID = {read=id, write=writeID};
	__property bool Checked = {read=readChecked, write=writeChecked};
	__property bool Selected = {read=readSelected, write=writeSelected};
	__property int Min = {read=min, write=writeMin};
	__property int Max = {read=max, write=writeMax};
	__property int Position = {read=position, write=writePosition};
};

class TSettingsForm : public TForm
{
__published:	// IDE-managed Components
	TImageList *ImageList1;
	TColorDialog *ColorDialog1;
	TOpenDialog *OpenDialog;
	TTimer *DblClickTimer;
	TImageList *ImageList2;
	TOpenDialog *PlugInDialog;
	TPanel *BluePanel1;
	TPanel *BluePanel2;
	TPanel *BluePanel3;
	TPanel *BluePanel4;
	TPanel *ClientPanel;
	TPanel *TitlePanel;
	TLabel *BlueLabel1;
	TPanel *BluePanel9;
	TImageButton *CloseIButton;
	TPanel *BluePanel5;
	TPanel *BluePanel6;
	TPanel *BluePanel7;
	TPanel *TabsPanel;
	TNotebook *ToolbarNotebook;
	TPanel *BluePanel13;
	TImageButton *GPSButton;
	TImageButton *GPRButton;
	TImageButton *TerminalButton;
	TImageButton *PlugInButton;
	TImageButton *PerformanceButton;
	TImageButton *PaletteButton;
	TImageButton *NextButton;
	TPanel *BluePanel12;
	TImageButton *DXFButton;
	TImageButton *MultiFileMapButton;
	TImageButton *XYZCButton;
	TImageButton *PreviousButton;
	TImageButton *SPARButton;
	TImageButton *GainButton;
	TImageButton *AboutButton;
	TPanel *PagesPanel;
	TPanel *OkCancelPanel;
	TImageButton *CancelImageButton;
	TImageButton *OkImageButton;
	TNotebook *Notebook1;
	TScrollBox *ScrollBox2;
	TPanel *Panel23;
	TScrollBox *ScrollBox4;
	TPanel *Panel5;
	TLabel *Label26;
	TLabel *Label87;
	TLabel *Label88;
	TLabel *Label105;
	TLabel *Label106;
	TLabel *Label107;
	TLabel *Label109;
	TLabel *Label110;
	TPanel *Panel25;
	TScrollBox *ScrollBox3;
	TPanel *Panel9;
	TLabel *Label127;
	TLabel *Label21;
	TLabel *Label19;
	TLabel *Label18;
	TLabel *Label17;
	TLabel *Label16;
	TLabel *Label15;
	TLabel *Label14;
	TLabel *Label65;
	TLabel *Label66;
	TLabel *Label67;
	TLabel *Label76;
	TLabel *Label77;
	TLabel *Label78;
	TLabel *Label101;
	TLabel *Label112;
	TLabel *Label113;
	TLabel *Label124;
	TLabel *Label126;
	TLabel *Label128;
	TLabel *Label132;
	TScrollBox *ScrollBox1;
	TPanel *Panel6;
	TLabel *Label29;
	TLabel *Label30;
	TLabel *Label31;
	TLabel *Label32;
	TLabel *Label33;
	TLabel *Label34;
	TLabel *Label35;
	TLabel *Label36;
	TLabel *Label37;
	TLabel *Label38;
	TLabel *Label39;
	TLabel *Label40;
	TLabel *Label41;
	TLabel *Label42;
	TLabel *Label43;
	TLabel *Label44;
	TLabel *Label45;
	TLabel *Label46;
	TLabel *Label47;
	TLabel *Label48;
	TLabel *Label49;
	TLabel *Label50;
	TLabel *Label51;
	TLabel *Label52;
	TLabel *Label53;
	TLabel *Label55;
	TEdit *SparShellEdit;
	TEdit *SparShellWaitEdit;
	TComboBox *SparCOMPortNumberCBox;
	TComboBox *SparFrequencyCBox;
	TComboBox *SparAverageTypeCBox;
	TComboBox *SparAverageTimeCBox;
	TComboBox *SparUpdateRateCBox;
	TEdit *SparElevationEdit;
	TEdit *SparToAntennaFwdEdit;
	TEdit *SparToAntennaRightEdit;
	TEdit *SparToAntennaUpEdit;
	TEdit *SparConfidenceMaxEdit;
	TEdit *SparMinDbFieldEdit;
	TPanel *Panel10;
	TLabel *Label68;
	TLabel *Label69;
	TLabel *Label70;
	TLabel *Label71;
	TLabel *Label72;
	TLabel *Label73;
	TLabel *Label74;
	TImage *Image3;
	TLabel *Label75;
	TImage *Image4;
	TLabel *Label79;
	TEdit *PitchCompensationEdit;
	TEdit *RollCompensationEdit;
	TEdit *HeadingCompensationEdit;
	TStaticText *StaticText4;
	TPanel *Panel11;
	TImage *Image6;
	TLabel *Label81;
	TLabel *Label82;
	TLabel *Label103;
	TPanel *Panel12;
	TImage32 *PaletteImage;
	TTrackBar *ColorTBar;
	TPanel *ColorPanel;
	TShape *ColorShape;
	TImage *PaletteColorImage;
	TLabel *ColorLabel;
	TPanel *ColorPanel1;
	TShape *Shape1;
	TImage *PaletteCanImage1;
	TLabel *Label89;
	TPanel *ColorPanel3;
	TShape *Shape3;
	TImage *PaletteCanImage3;
	TLabel *Label90;
	TImage *PaletteCanCoverImage3;
	TPanel *ColorPanel5;
	TShape *Shape5;
	TImage *PaletteCanImage5;
	TLabel *Label91;
	TImage *PaletteCanCoverImage5;
	TPanel *ColorPanel7;
	TShape *Shape7;
	TImage *PaletteCanImage7;
	TLabel *Label92;
	TImage *PaletteCanCoverImage7;
	TPanel *ColorPanel9;
	TShape *Shape9;
	TImage *PaletteCanImage9;
	TLabel *Label94;
	TImage *PaletteCanCoverImage9;
	TPanel *ColorPanel10;
	TShape *Shape10;
	TImage *PaletteCanImage10;
	TLabel *Label95;
	TPanel *ColorPanel8;
	TShape *Shape8;
	TImage *PaletteCanImage8;
	TLabel *Label93;
	TImage *PaletteCanCoverImage8;
	TPanel *ColorPanel6;
	TShape *Shape6;
	TImage *PaletteCanImage6;
	TLabel *Label98;
	TImage *PaletteCanCoverImage6;
	TPanel *ColorPanel4;
	TShape *Shape4;
	TImage *PaletteCanImage4;
	TLabel *Label99;
	TImage *PaletteCanCoverImage4;
	TPanel *ColorPanel2;
	TShape *Shape2;
	TImage *PaletteCanImage2;
	TLabel *Label100;
	TImage *PaletteCanCoverImage2;
	TPanel *Panel14;
	TImage *GPImage10d;
	TImage *GPImage8d;
	TImage *GPImage6d;
	TImage *GPImage4d;
	TImage *GPImage2d;
	TImage *Image8;
	TImage *Image9;
	TImage *Image11;
	TImage *Image12;
	TImage *Image16;
	TImage *Image10;
	TImage *Image13;
	TImage *Image7;
	TImage *GPImage1d;
	TImage *GPImage2;
	TImage *GPImage3d;
	TImage *GPImage3;
	TImage *GPImage4;
	TImage *GPImage5d;
	TImage *GPImage5;
	TImage *GPImage6;
	TImage *GPImage7d;
	TImage *GPImage7;
	TImage *GPImage8;
	TImage *GPImage9d;
	TImage *GPImage9;
	TImage *GPImage10;
	TImage *GPImage1;
	TLabel *Label104;
	TPanel *Panel17;
	TPanel *Panel15;
	TPanel *Panel16;
	TTrackBar *TrackBar2;
	TPanel *Panel19;
	TPanel *Panel20;
	TImage *TraceOffImage;
	TImage *PowerOffImage;
	TImage *AttenuationOffImage;
	TImage *TraceOnImage;
	TImage *PowerOnImage;
	TImage *AttenuationOnImage;
	TPanel *Panel21;
	TImgView32 *GainImage;
	TPanel *Panel24;
	TPanel *Panel22;
	TImageButton *AddPlugInBtn;
	TImageButton *DelPlugInBtn;
	TLabel *Label120;
	TLabel *Label121;
	TImage *Image14;
	TImage *Image15;
	TImage *Image1;
	TLabel *Label122;
	TImageButton *SettingsPlugInBtn;
	TListView *PlugInLView;
	TPanel *Panel2;
	TLabel *Label1;
	TLabel *Label2;
	TLabel *Label123;
	TImageButton *PosPlugInButton;
	TLabel *Label137;
	TImageButton *TwoWheelSettingsButton;
	TLabel *Label140;
	TLabel *Label141;
	TPanel *Panel1;
	TLabel *Label145;
	TLabel *Label5;
	TEdit *StepEdit;
	TPanel *Panel26;
	TLabel *Label144;
	TEdit *QEdit;
	TLabel *Label7;
	TPanel *Panel27;
	TLabel *Label146;
	TLabel *Label139;
	TPanel *Panel28;
	TLabel *Label148;
	TLabel *Label138;
	TComboBox *TwoWheelPortCBox;
	TComboBox *TwoWheelSpeedCBox;
	TPanel *Panel29;
	TComboBox *PosPlugInCBox;
	TLabel *Label6;
	TPanel *Panel30;
	TLabel *Label8;
	TEdit *DxEdit;
	TLabel *Label9;
	TPanel *Panel31;
	TLabel *Label147;
	TEdit *PermitEdit;
	TLabel *Label20;
	TPanel *Panel32;
	TLabel *Label149;
	TComboBox *BatteryTypeCBox;
	TLabel *Label102;
	TLabel *Label116;
	TLabel *Label119;
	TEdit *WSWindowWidthEdit;
	TLabel *Label118;
	TLabel *Label117;
	TScrollBox *ScrollBox5;
	TPanel *Panel33;
	TLabel *Label150;
	TImageButton *GpsUnitIButton;
	TLabel *Label24;
	TLabel *Label83;
	TLabel *Label108;
	TLabel *Label11;
	TLabel *Label12;
	TLabel *Label13;
	TLabel *Label64;
	TPanel *Panel18;
	TLabel *Label151;
	TLabel *Label152;
	TComboBox *GpsUtitCBox;
	TPanel *Panel34;
	TLabel *Label28;
	TLabel *Label153;
	TComboBox *PortCBox;
	TPanel *Panel35;
	TLabel *Label3;
	TLabel *Label22;
	TComboBox *SpeedCBox;
	TPanel *Panel36;
	TLabel *Label4;
	TEdit *AntennaHeightEdit;
	TPanel *Panel37;
	TLabel *Label27;
	TComboBox *DefaultCoordinateSystemCBox;
	TPanel *BluePanel8;
	TPanel *Panel3;
	TLabel *Label154;
	TPanel *Panel8;
	TLabel *Label155;
	TImageButton *ImageButton1;
	TLabel *Label157;
	TComboBox *SendEdit;
	TMemo *Memo1;
	TPanel *TerminalPausedPanel;
	TLabel *Label23;
	TPanel *Panel4;
	TLabel *Label156;
	TEdit *TextHeightSEdit;
	TUpDown *UpDown1;
	TTrackBar *TrackBar1;
	TLabel *Label111;
	TLabel *Label158;
	TLabel *Label159;
	TLabel *Label160;
	TPanel *Panel13;
	TLabel *Label161;
	TLabel *Label162;
	TEdit *txtBingInetMapAppKey;
	TImageButton *ImageButton2;
	TPanel *Panel40;
	TLabel *Label135;
	TLabel *Label136;
	TLabel *Label163;
	TLabel *Label125;
	TLabel *Label129p;
	TPanel *Panel41;
	TLabel *Label164;
	TPanel *DXFBackgroundColorPanel;
	TPanel *Panel42;
	TLabel *Label60;
	TEdit *DXFColorInvertThresholdEdit;
	TPanel *Panel43;
	TLabel *Label25;
	TEdit *EmptyMapSizeEdit;
	TImageButton *AutosaveBrowseImage;
	TLabel *Label114;
	TPanel *Panel44;
	TEdit *AutoStopProfileEdit;
	TPanel *Panel45;
	TLabel *Label115;
	TEdit *AutoSaveDirEdit;
	TLabel *Label130;
	TPanel *Panel46;
	TEdit *PredictionRadiusEdit;
	TLabel *Label131p;
	TLabel *Label80;
	TImageButton *ImageButton3;
	TScrollBox *ScrollBox6;
	TPanel *Panel7;
	TLabel *Label61;
	TLabel *Label62;
	TLabel *Label63;
	TLabel *Label84;
	TLabel *Label85;
	TLabel *Label86;
	TLabel *Label129;
	TPanel *Panel47;
	TLabel *Label131;
	TEdit *XYZCCellHeightEdit;
	TPanel *Panel48;
	TLabel *Label165;
	TEdit *XYZCCellWidthEdit;
	TImageButton *ImageButton4;
	TImageButton *ImageButton5;
	TLabel *Label56;
	TLabel *Label142;
	TPanel *XPanel;
	TLabel *Label143;
	TLabel *XLabel;
	TMaskEdit *GPRAddrEdit;
	TLabel *Label10;
	TImageButton *PauseTerminalIButton;
	TImageButton *ManualImage;
	TImageButton *WheelImage;
	TImageButton *TwoWheelImage;
	TImageButton *PosPlugInImage;
	TImageButton *TwoWheelRtsImage;
	TImageButton *UseGpsAsStartImage;
	TImageButton *WeightedSubtractionFilterImage;
	TImageButton *MoveoutCorrectionImage;
	TImageButton *RTSCTSImage;
	TImageButton *GpsUseZGeoidImage;
	TImageButton *GpsLogToFileImage;
	TImageButton *GpsSimFileImage;
	TImageButton *GpsTieCenterImage;
	TImageButton *GpsSimTcpImage;
	TImageButton *DXFMapExportImage;
	TImageButton *DXFPitchImage;
	TImageButton *DXFRollImage;
	TImageButton *DXFConfidenceImage;
	TImageButton *DXFPolyline3DImage;
	TImageButton *DXFIdToDescriptionImage;
	TImageButton *DXFDepthToDescriptionImage;
	TImageButton *DXFDescriptionAsTextImage;
	TImageButton *rbMap;
	TImageButton *rbAerial;
	TImageButton *rbAerialLabels;
	TImageButton *MapPreviewImage;
	TImageButton *FollowMeImage;
	TImageButton *LargeControlsImage;
	TImageButton *HideAnnotationLayerOnOpenImage;
	TImageButton *EnlargeEmptyMapImage;
	TImageButton *ProfilePipesDetectorImage;
	TImageButton *LinesDetectionImage;
	TImageButton *EasyRenderingImage;
	TImageButton *EasyZoomInImage;
	TImageButton *GpsTrackingImage;
	TImageButton *ShowLabelIconsImage;
	TImageButton *ShowLabelConfidenceImage;
	TImageButton *ShowLabelOnPinImage;
	TImageButton *CopyAttachmentsImage;
	TImageButton *BatteryImage;
	TImageButton *GyrocompassImage;
	TImageButton *RecycleBinImage;
	TImageButton *AutoStopProfileImage;
	TImageButton *AutoSaveImage;
	TImageButton *SparEnableImage;
	TImageButton *SparSimulationImage;
	TImageButton *SparUseBaselineImage;
	TImageButton *CompensationImage;
	TImageButton *XYZCUseZImage;
	TImageButton *XYZCUsePitchImage;
	TImageButton *XYZCUseRollImage;
	TImageButton *XYZCEnvelopeImage;
	TImageButton *XYZCAbsoluteImage;
	TImageButton *XYZCGainImage;
	TImageButton *PaletteCheckImage3;
	TImageButton *PaletteCheckImage5;
	TImageButton *PaletteCheckImage7;
	TImageButton *PaletteCheckImage9;
	TImageButton *PaletteCheckImage2;
	TImageButton *PaletteCheckImage4;
	TImageButton *PaletteCheckImage6;
	TImageButton *PaletteCheckImage8;
	TImageButton *ApplyGainImage;
	TImageButton *CacheTracesImage;
	TImageButton *CacheProfilesImage;
	TImageButton *PaletteCheckImage1;
	TImageButton *PaletteCheckImage10;
	TLabel *Label54;
	TImageButton *PredictionIButton;
	TLabel *Label57;
	TPanel *Panel49;
	TEdit *CacheTracesEdit;
	TPanel *Panel50;
	TLabel *Label58;
	TEdit *PredictionTimeoutEdit;
	TPanel *Panel51;
	TLabel *Label59;
	TImageButton *DbShowLayerFileNameIButton;
	TLabel *Label96;
	TImageButton *DbStoreLayersVisabilityIButton;
	TLabel *Label97;
	TImageButton *DbMergeLayersWithSameNameIButton;
	TLabel *Label166;
	TTrackBar *ZoomTrackBar;
	TLabel *ZoomLabel;
	TImageButton *SingleTypeIButton;
	TLabel *Label133;
	TLabel *BuildDateLabel;
	TLabel *TrialLabel;
	TLabel *Label167;
	TImageButton *WPSasWheelImage;
	TLabel *Label168;
	TImageButton *AutoCloseImage;
	TImageButton *rpoVerticalIButton;
	TLabel *Label134;
	TLabel *Label169;
	TImageButton *rpoHorizontalIButton;
	TImageButton *rpoAutoIButton;
	TLabel *Label170;
	TLabel *Label171;
	TImageButton *RMMaximizedIButton;
	TLabel *Label172;
	void __fastcall WheelImageForceClick(TObject *Sender);
	void __fastcall WheelImageClick(TObject *Sender);
	void __fastcall SetPositioningControls();
	void __fastcall Label1Click(TObject *Sender);
	void __fastcall Label2Click(TObject *Sender);
	void __fastcall Image1Click(TObject *Sender);
	void __fastcall StepEditKeyPress(TObject *Sender, wchar_t &Key);
	void __fastcall DxEditKeyPress(TObject *Sender, wchar_t &Key);
	void __fastcall GPRAddrEditExit(TObject *Sender);
	void __fastcall GpsSimTcpImageClick(TObject *Sender);
	void __fastcall Label11Click(TObject *Sender);
	void __fastcall Label12Click(TObject *Sender);
	void __fastcall Label13Click(TObject *Sender);
	void __fastcall Label19Click(TObject *Sender);
	void __fastcall Label18Click(TObject *Sender);
	void __fastcall Label17Click(TObject *Sender);
	void __fastcall Label16Click(TObject *Sender);
	void __fastcall Label15Click(TObject *Sender);
	void __fastcall Label14Click(TObject *Sender);
	void __fastcall FormShow(TObject *Sender);
	void __fastcall DxEditExit(TObject *Sender);
	void __fastcall StepEditExit(TObject *Sender);
	void __fastcall PermitEditExit(TObject *Sender);
	void __fastcall Label21Click(TObject *Sender);
	void __fastcall SendImageClick(TObject *Sender);
	void __fastcall Memo1Change(TObject *Sender);
	void __fastcall SendEditKeyPress(TObject *Sender, wchar_t &Key);
	void __fastcall Label24Click(TObject *Sender);
	void __fastcall Notebook1PageChanged(TObject *Sender);
	void __fastcall GpsUtitCBoxChange(TObject *Sender);
	void __fastcall DXFBackgroundColorPanelClick(TObject *Sender);
	void __fastcall Label26Click(TObject *Sender);
	void __fastcall DXFColorInvertThresholdEditExit(TObject *Sender);
	void __fastcall AntennaHeightEditExit(TObject *Sender);
	void __fastcall SparShellWaitEditExit(TObject *Sender);
	void __fastcall SparElevationEditExit(TObject *Sender);
	void __fastcall SparToAntennaFwdEditExit(TObject *Sender);
	void __fastcall SparToAntennaFwdEditKeyPress(TObject *Sender, wchar_t &Key);
	void __fastcall SparToAntennaRightEditExit(TObject *Sender);
	void __fastcall SparToAntennaUpEditExit(TObject *Sender);
	void __fastcall SparConfidenceMaxEditExit(TObject *Sender);
	void __fastcall SparMinDbFieldEditExit(TObject *Sender);
	void __fastcall SparShellEditExit(TObject *Sender);
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall XYZCCellWidthEditExit(TObject *Sender);
	void __fastcall XYZCCellHeightEditExit(TObject *Sender);
	void __fastcall Label65Click(TObject *Sender);
	void __fastcall Label66Click(TObject *Sender);
	void __fastcall Label67Click(TObject *Sender);
	void __fastcall HeadingCompensationEditExit(TObject *Sender);
	void __fastcall StaticText4Click(TObject *Sender);
	void __fastcall Image3Click(TObject *Sender);
	void __fastcall Label78Click(TObject *Sender);
	void __fastcall Image4Click(TObject *Sender);
	void __fastcall Label82Click(TObject *Sender);
	void __fastcall GpsUseZGeoidImageClick(TObject *Sender);
	void __fastcall Label86Click(TObject *Sender);
	void __fastcall XYZCAbsoluteImageClick(TObject *Sender);
	void __fastcall XYZCEnvelopeImageClick(TObject *Sender);
	void __fastcall Label87Click(TObject *Sender);
	void __fastcall Label88Click(TObject *Sender);
	void __fastcall Label61Click(TObject *Sender);
	void __fastcall Label62Click(TObject *Sender);
	void __fastcall Label63Click(TObject *Sender);
    void __fastcall PaletteCheckImageClick(TObject *Sender);
	void __fastcall PaletteCanImage1DblClick(TObject *Sender);
	void __fastcall ColorLabelClick(TObject *Sender);
	void __fastcall PaletteCanImage1Click(TObject *Sender);
	void __fastcall DblClickTimerTimer(TObject *Sender);
	void __fastcall ColorTBarChange(TObject *Sender);
	void __fastcall ColorTBarEnter(TObject *Sender);
	void __fastcall PaletteLoadImageClick(TObject *Sender);
	void __fastcall PaletteSaveImageClick(TObject *Sender);
	void __fastcall Label101Click(TObject *Sender);
	void __fastcall ApplyGainImageClick(TObject *Sender);
	void __fastcall Label105Click(TObject *Sender);
	void __fastcall Label106Click(TObject *Sender);
	void __fastcall Label107Click(TObject *Sender);
	void __fastcall GpsLogToFileImageClick(TObject *Sender);
	void __fastcall Label109Click(TObject *Sender);
	void __fastcall Label110Click(TObject *Sender);
	void __fastcall TextHeightSEditKeyPress(TObject *Sender, wchar_t &Key);
	void __fastcall TrackBar1Change(TObject *Sender);
	void __fastcall TextHeightSEditChange(TObject *Sender);
	void __fastcall UpDown1Click(TObject *Sender, TUDBtnType Button);
	void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
	void __fastcall Label112Click(TObject *Sender);
	void __fastcall EmptyMapSizeEditExit(TObject *Sender);
	void __fastcall Label113Click(TObject *Sender);
	void __fastcall WSWindowWidthEditExit(TObject *Sender);
	void __fastcall Label116Click(TObject *Sender);
	void __fastcall Label119Click(TObject *Sender);
	void __fastcall QEditExit(TObject *Sender);
	void __fastcall DelPlugInBtnClick(TObject *Sender);
	void __fastcall AddPlugInBtnClick(TObject *Sender);
	void __fastcall PlugInLViewChange(TObject *Sender, TListItem *Item, TItemChange Change);
	void __fastcall DefaultCoordinateSystemCBoxChange(TObject *Sender);
	void __fastcall PosPlugInImageClick(TObject *Sender);
	void __fastcall PosPlugInCBoxChange(TObject *Sender);
	void __fastcall PosPlugInButtonClick(TObject *Sender);
	void __fastcall SettingsPlugInBtnClick(TObject *Sender);
	void __fastcall Label124Click(TObject *Sender);
	void __fastcall Label126Click(TObject *Sender);
	void __fastcall PredictionRadiusEditExit(TObject *Sender);
	void __fastcall PauseTerminalIButtonClick(TObject *Sender);
	void __fastcall AutoStopProfileEditExit(TObject *Sender);
	void __fastcall AutosaveBrowseImageClick(TObject *Sender);
	void __fastcall Label132Click(TObject *Sender);
	void __fastcall ProfilePipesDetectorImageClick(TObject *Sender);
	void __fastcall GpsUnitIButtonClick(TObject *Sender);
	void __fastcall TwoWheelSettingsButtonClick(TObject *Sender);
	void __fastcall Label141Click(TObject *Sender);
	void __fastcall Label137Click(TObject *Sender);
	void __fastcall InfoBtnClick(TObject *Sender);
	void __fastcall NextButtonClick(TObject *Sender);
	void __fastcall PreviousButtonClick(TObject *Sender);
	void __fastcall TitlePanelMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
		  int X, int Y);
	void __fastcall TitlePanelMouseMove(TObject *Sender, TShiftState Shift, int X, int Y);
	void __fastcall TitlePanelMouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
	void __fastcall CancelImageButtonClick(TObject *Sender);
	void __fastcall Label140Click(TObject *Sender);
	void __fastcall Label123Click(TObject *Sender);
	void __fastcall ImageButton2Click(TObject *Sender);
	void __fastcall Panel9MouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
	void __fastcall Panel9MouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
	void __fastcall Panel9MouseMove(TObject *Sender, TShiftState Shift, int X, int Y);
	void __fastcall Panel6MouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
	void __fastcall Panel6MouseMove(TObject *Sender, TShiftState Shift, int X, int Y);
	void __fastcall Panel6MouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
	void __fastcall Panel2MouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
	void __fastcall Panel2MouseMove(TObject *Sender, TShiftState Shift, int X, int Y);
	void __fastcall Panel2MouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift,
		  int X, int Y);
	void __fastcall Panel5MouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
	void __fastcall Panel5MouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
	void __fastcall Panel5MouseMove(TObject *Sender, TShiftState Shift, int X, int Y);
	void __fastcall Panel33MouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
	void __fastcall Panel33MouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
	void __fastcall Panel33MouseMove(TObject *Sender, TShiftState Shift, int X, int Y);
	void __fastcall Panel7MouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
	void __fastcall Panel7MouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
	void __fastcall Panel7MouseMove(TObject *Sender, TShiftState Shift, int X, int Y);
	void __fastcall rbMapClick(TObject *Sender);
	void __fastcall Label77Click(TObject *Sender);
	void __fastcall Label76Click(TObject *Sender);
	void __fastcall Label127Click(TObject *Sender);
	void __fastcall Label128Click(TObject *Sender);
	void __fastcall Label158Click(TObject *Sender);
	void __fastcall Label159Click(TObject *Sender);
	void __fastcall Label160Click(TObject *Sender);
	void __fastcall PredictionTimeoutEditExit(TObject *Sender);
	void __fastcall CacheTracesEditExit(TObject *Sender);
	void __fastcall Label97Click(TObject *Sender);
	void __fastcall Label96Click(TObject *Sender);
	void __fastcall Label166Click(TObject *Sender);
	void __fastcall Label57Click(TObject *Sender);
	void __fastcall ZoomTrackBarChange(TObject *Sender);
	void __fastcall Label133Click(TObject *Sender);
	void __fastcall Label167Click(TObject *Sender);
	void __fastcall Label168Click(TObject *Sender);
	void __fastcall OkCancelPanelMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
	void __fastcall Label169Click(TObject *Sender);
	void __fastcall Label134Click(TObject *Sender);
	void __fastcall Label170Click(TObject *Sender);
	void __fastcall Label172Click(TObject *Sender);

private:	// User declarations
	TRadarMapManager *Manager;
	TRadarMapSettings settings;
	TPaletteItem *PaletteItems, *selectedpaletteitem;
	TOscilloscopeGrid* GainImageGrid;
	TGainItems* GainItems;
	TPalette* Palette;
	TObject *OnClickSender;
	TStrings *CRS_GUIDs, *Pos_GUIDs;
	bool TitleDown;
	Types::TPoint TitleXY;
	bool Panel9MousePressed, Panel6MousePressed, Panel2MousePressed,
		Panel5MousePressed, Panel33MousePressed, Panel7MousePressed;
	int Panel9Y, Panel6Y, Panel2Y, Panel5Y, Panel33Y, Panel7Y;
	void __fastcall CollectSettings(bool TryToApply);
	void __fastcall RebuildPaletteImage();
	void __fastcall SelectPaletteItem(TPaletteItem *PaletteItem);
	void __fastcall PlugInCBoxChange(TObject *Sender);
	__property TPaletteItem *SelectedPaletteItem = {read=selectedpaletteitem, write=SelectPaletteItem};
public:		// User declarations
	TWriteCommandEvent WriteCommand;
	TPauseResumeEvent PauseTerminal, ResumeTerminal;

	__fastcall TSettingsForm(TComponent* Owner);
	__fastcall ~TSettingsForm();
	int __fastcall MyWordBreakProc(LPTSTR pchar, int ichCurrent, int cch, int code) {return 0;}
	void __fastcall ApplyPalette(TPalette* APalette);

	__property TRadarMapSettings RadarMapSettings = {read=settings, write=settings};
	__property TRadarMapManager *RadarMapManager = {read=Manager, write=Manager};
};
//---------------------------------------------------------------------------
extern PACKAGE TSettingsForm *SettingsForm;
//---------------------------------------------------------------------------
#endif
