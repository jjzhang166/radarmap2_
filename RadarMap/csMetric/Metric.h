#ifndef MetricH
#define MetricH

namespace Metric_WGS84
{
	extern "C" _declspec(dllimport) bool __stdcall _CRS_LatLonAltToXYH(double lat,
		double lon, double alt, double &_x, double &_y, double &_h);
	extern "C" _declspec(dllimport) bool __stdcall _CRS_XYHToLatLonAlt(double _x,
		double _y, double _h, double &lat, double &lon, double &alt);
	extern "C" _declspec(dllimport) bool __stdcall _CRS_UnitVector(double lat,
		double lon, double alt, double &_vx, double &_vy);
	extern "C" _declspec(dllimport) void InitPlugIn(int CRS_Id);
	extern "C" _declspec(dllimport) void FreePlugIn(int CRS_Id);
}
#endif MetricH
