//---------------------------------------------------------------------------


#pragma hdrstop

#include "Drawing.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

namespace Drawing
{

Gr32::TFloatPoint __fastcall FloatPointCast(Drawing::TFloatPoint p)
{
	Gr32::TFloatPoint g;

	g.X=p.x;
	g.Y=p.y;
	return g;
}

Drawing::TFloatPoint __fastcall FloatPointCast(Gr32::TFloatPoint p)
{
	Drawing::TFloatPoint g;

	g.x=p.X;
	g.y=p.Y;
	return g;
}

Gr32::TFloatRect __fastcall FloatRectCast(Drawing::TFloatRect p)
{
	Gr32::TFloatRect g;

	g.Top=p.Top;
	g.Left=p.Left;
	g.Bottom=p.Bottom;
	g.Right=p.Right;
	return g;
}

Drawing::TFloatRect __fastcall FloatRectCast(Gr32::TFloatRect p)
{
	Drawing::TFloatRect g;

	g.Top=p.Top;
	g.Left=p.Left;
	g.Bottom=p.Bottom;
	g.Right=p.Right;
	return g;
}

// ---------------------------------------------------------------------------
// TMyLayerCollection
// ---------------------------------------------------------------------------
Drawing::TMyLayer* __fastcall TMyLayerCollection::readMouseListener()
{
	if(mll!=NULL) delete mll;
	mll = new TMyLayer(_layers->MouseListener);
	return mll;
}

TMyLayer* __fastcall TMyLayerCollection::readItem(int Index)
{
	if(il!=NULL) delete il;
	il = new TMyLayer(_layers->Items[Index]);
	return il;
}

void __fastcall TMyLayerCollection::writeItem(int Index, TMyLayer *value)
{
	_layers->Items[Index]=value->Layer;
}

void __fastcall TMyLayerCollection::writeMouseListener(TMyLayer *value)
{
	_layers->MouseListener=value->Layer;
}

// ---------------------------------------------------------------------------
// TMyLayer
// ---------------------------------------------------------------------------
void __fastcall TMyLayer::tempOnPaint(System::TObject* Sender, Gr32::TBitmap32* Buffer)
{
	if(myOnPaint!=NULL)
	{
		TMyBitmap *tmp;
		tmp=new TMyBitmap(Buffer);
		myOnPaint(Sender, tmp);
		delete tmp;
	}
}

__fastcall TMyLayer::TMyLayer(TMyLayerCollection *ptr)
{
	_layers=ptr;
	_layer=new TBitmapLayer(ptr->Layers);
	myOnPaint=NULL;
	_layer->OnPaint=tempOnPaint;
	SelfCreated=true;
}

__fastcall TMyLayer::TMyLayer(Gr32_layers::TCustomLayer* ptr)
{
	_layer=(Gr32_layers::TBitmapLayer*)ptr;
	_layers=new TMyLayerCollection(_layer->LayerCollection);
	 myOnPaint=NULL;
	_layer->OnPaint=tempOnPaint;
	SelfCreated=false;
}

__fastcall TMyLayer::~TMyLayer()
{
	if(SelfCreated) delete _layer;
	else delete _layers;
}

// ---------------------------------------------------------------------------
// TMyImage
// ---------------------------------------------------------------------------
void __fastcall TMyImage::tempMouseDown(System::TObject* Sender, Controls::TMouseButton Button, Classes::TShiftState Shift, int X, int Y, Gr32_layers::TCustomLayer* Layer)
{
	if(myMouseDown!=NULL && Layer!=NULL)
	{
		TMyLayer *tmp;
		tmp=new TMyLayer(Layer);
		myMouseDown(Sender, Button, Shift, X, Y, tmp);
		delete tmp;
	}
}

void __fastcall TMyImage::tempMouseMove(System::TObject* Sender, Classes::TShiftState Shift, int X, int Y, Gr32_layers::TCustomLayer* Layer)
{
	if(myMouseMove!=NULL && Layer!=NULL)
	{
		TMyLayer *tmp;
		tmp=new TMyLayer(Layer);
		myMouseMove(Sender, Shift, X, Y, tmp);
		delete tmp;
	}
}

void __fastcall TMyImage::tempMouseUp(System::TObject* Sender, Controls::TMouseButton Button, Classes::TShiftState Shift, int X, int Y, Gr32_layers::TCustomLayer* Layer)
{
	if(myMouseUp!=NULL && Layer!=NULL)
	{
		TMyLayer *tmp;
		tmp=new TMyLayer(Layer);
		myMouseUp(Sender, Button, Shift, X, Y, tmp);
		delete tmp;
	}
}

__fastcall TMyImage::TMyImage(Classes::TComponent* aowner)
{
	_image=new TImgView32(aowner);
	_bitmap=new TMyBitmap(_image->Bitmap);
	_layers=new TMyLayerCollection(_image->Layers);
	_image->ScaleMode=TScaleMode::smScale;
	_image->OnMouseDown=tempMouseDown;
	_image->OnMouseMove=tempMouseMove;
	_image->OnMouseUp=tempMouseUp;
}

__fastcall TMyImage::TMyImage(TImgView32* Image)
{
	_image=Image;
	_bitmap=new TMyBitmap(_image->Bitmap);
	_layers=new TMyLayerCollection(_image->Layers);
	_image->ScaleMode=TScaleMode::smScale;
	_image->OnMouseDown=tempMouseDown;
	_image->OnMouseMove=tempMouseMove;
	_image->OnMouseUp=tempMouseUp;
}

__fastcall TMyImage::~TMyImage()
{
	delete _image;
	delete _bitmap;
	delete _layers;
}

}

