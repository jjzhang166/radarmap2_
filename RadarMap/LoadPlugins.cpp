//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "LoadPlugins.h"
#include "PlugIns.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "ImageButton"
#pragma resource "*.dfm"
TLoadPluginsForm *LoadPluginsForm;
//---------------------------------------------------------------------------
__fastcall TLoadPluginsForm::TLoadPluginsForm(TComponent* Owner)
	: TForm(Owner)
{
	TreeView=new TCheckBoxTreeView(XPanel);
	TreeView->Left=TreeView1->Left;
	TreeView->Top=TreeView1->Top;
	TreeView->Width=TreeView1->Width;
	TreeView->Height=TreeView1->Height;
	TreeView->Font->Assign(TreeView1->Font);
	TreeView->Visible=true;
	TreeView->ShowButtons=true;
	TreeView->ShowLines=true;
	TreeView->ShowRoot=false;
	TreeView->ShowHint=true;
	TreeView->BorderStyle=bsSingle;
	TreeView->BorderWidth=1;
	TreeView->Ctl3D=false;
	TreeView->Images=ImageList1;
}
//---------------------------------------------------------------------------

__fastcall TLoadPluginsForm::~TLoadPluginsForm()
{
	for(int i=0; i<TreeView->Items->Count; i++)
	{
		if(TreeView->Items->Item[i]->Data) delete TreeView->Items->Item[i]->Data;
		TreeView->Items->Item[i]->Data=NULL;
	}
	TreeView->Items->Clear();
}
//---------------------------------------------------------------------------

void __fastcall TLoadPluginsForm::OkImageClick(TObject *Sender)
{
	ModalResult=mrOk;
}
//---------------------------------------------------------------------------

void __fastcall TLoadPluginsForm::CancelImageClick(TObject *Sender)
{
	ModalResult=mrCancel;
}
//---------------------------------------------------------------------------

void __fastcall TLoadPluginsForm::OutputPlugIns(AnsiString FileName, TPlugInType NeededType)
{
	int N;
	TPlugInListItem *Items, *plugin;
	TTreeNode *Root, *node;
	AnsiString str;

	for(int i=0; i<TreeView->Items->Count; i++)
	{
		if(TreeView->Items->Item[i]->Data) delete TreeView->Items->Item[i]->Data;
		TreeView->Items->Item[i]->Data=NULL;
	}
	TreeView->Items->Clear();
	if(FileName!=NULL && FileName.Length()>0 && FileExists(FileName) && PlugInManager)
	{
		Root=TreeView->Items->Add(NULL, ExtractFileName(FileName));
		Root->Data=NULL;
		Root->ImageIndex=0;
		Root->SelectedIndex=0;
		TreeView->SetChecking(Root, false);
		N=PlugInManager->GetPlugInsCountInLibrary(FileName);
		if(N>0)
		{
			Items=new TPlugInListItem[N];
			try
			{
				N=PlugInManager->GetPlugInsListInLibrary(FileName, Items, N, NeededType);
				if(N>0)
				{
					for(int i=0; i<N; i++)
					{
						switch(Items[i].Type)
						{
							case pitCRS: str="CRS"; break;
							case pitPos: str="POS"; break;
							default: str="UNK";
						}
						node=TreeView->Items->AddChild(Root, str+" - "+Items[i].Name+", "+Items[i].GUID);
						node->ImageIndex=(int)Items[i].Type+1;
						//if(Items[i].Type==pitCRS) node->ImageIndex=2;
						//else node->ImageIndex=1;
						node->SelectedIndex=node->ImageIndex;
						TreeView->SetChecking(node, false);
						plugin=new TPlugInListItem;
						plugin->DLLName=Items[i].DLLName;
						plugin->GUID=Items[i].GUID;
						plugin->Name=Items[i].Name;
						plugin->Type=Items[i].Type;
						plugin->About=Items[i].About;
						plugin->Version=Items[i].Version;
						plugin->Description=Items[i].Description;
						DescriptionLabel->Caption=Items[i].Description;
						CopyrightLabel->Caption=Items[i].About;
						VersionLabel->Caption=Items[i].Version;
						plugin->ID=Items[i].ID;
						node->Data=plugin;
					}
				}
			}
			__finally
			{
				delete[] Items;
			}
		}
		Root->Expand(true);
	}
}
//---------------------------------------------------------------------------


void __fastcall TLoadPluginsForm::CopyCheckBoxClick(TObject *Sender)
{
	((TImageButton*)Sender)->Toggle();
}
//---------------------------------------------------------------------------

void __fastcall TLoadPluginsForm::Label108Click(TObject *Sender)
{
	CopyCheckBox->Toggle();
}
//---------------------------------------------------------------------------

void __fastcall TLoadPluginsForm::TitlePanelMouseDown(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y)
{
	TitleDown=true;
	TitleXY=ClientToScreen(Types::TPoint(X, Y));
}
//---------------------------------------------------------------------------

void __fastcall TLoadPluginsForm::TitlePanelMouseMove(TObject *Sender, TShiftState Shift,
          int X, int Y)
{
	Types::TPoint tmp;

	if(TitleDown)
	{
		tmp=ClientToScreen(Types::TPoint(X, Y));
		Left+=tmp.x-TitleXY.x;
		Top+=tmp.y-TitleXY.y;
		TitleXY=tmp;
	}
}
//---------------------------------------------------------------------------

void __fastcall TLoadPluginsForm::TitlePanelMouseUp(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y)
{
	TitleDown=false;
}
//---------------------------------------------------------------------------

void __fastcall TLoadPluginsForm::FormShow(TObject *Sender)
{
	TitleDown=false;
}
//---------------------------------------------------------------------------

