//---------------------------------------------------------------------------
#ifndef CacheManagerH
#define CacheManagerH

#include <vcl.h>
#include "MyFiles.h"
#include "MyThreads.h"
#include <Classes.hpp>

#define AccessGapTime           20000 //msec, =30 sec
#define AccessMaxGapTime        10000 //msec, =10 sec
#define AccessCriticalGapTime   1000  //msec, =1 sec
#define CrashMemoryLevel        1950351360UL //1860 MB

//---------------------------------------------------------------------------
enum TCacheObjectType {cotNone = 0, cotArray = 1, cotProfileSGY = 2,
	cotAnnotations = 3, cotProfileData = 4};

class TCacheObjectsManager;

class TCacheObject: public TMyObject
{
private:
	bool rewritezeros;
protected:
	TCacheObjectType type;
	AnsiString filename;
	unsigned long size, filepointer;
	bool cachealone, cached, inrestore;
	void* source;
	TCacheObjectsManager *owner;
	TBusyLocker *BusyLocker;
	virtual unsigned long readSizeInMemory() {return sizeof(TCacheObject);}
	virtual void __fastcall ApproveRestore(TUnsharedFile *cFile, void* data);
	virtual void __fastcall ClearSource() {if(source) delete source; source=NULL;}
	virtual bool CacheToHDD(TUnsharedFile *cFile, unsigned long &pointer) {return true;}
	virtual void* RestoreFromHDD(TUnsharedFile *cFile, void* Param) {return NULL;}
public:
	__fastcall TCacheObject(TCacheObjectsManager *AOwner, TCacheObjectType t, void* ASource);
	__fastcall ~TCacheObject();

	virtual long __fastcall SizeOnDisk() {return size;}
	virtual bool __fastcall CacheIt(TUnsharedFile *cFile); //store on HDD Type corrsponding class, release memory
	virtual void* __fastcall Restore(TUnsharedFile *cFile, void* Param); //construct in memory Type corrsponding class, restore it from HDD data, return its pointer

	__property TCacheObjectType Type = {read=type};
	__property AnsiString FileName = {read=filename};
	__property unsigned long FilePointer = {read=filepointer};
	__property unsigned long ObjectSizeInMemory = {read=size};
	__property bool Cached = {read=cached};
	__property bool CacheAlone = {read=cachealone};
	__property bool RewriteZeros = {read=rewritezeros, write=rewritezeros};
    __property unsigned long SizeInMemory = {read=readSizeInMemory};
    __property bool InRestore = {read=inrestore};
};

class TCacheArrayObject: public TCacheObject
{
private:
protected:
    unsigned long readSizeInMemory() {unsigned long ul=sizeof(TCacheArrayObject); if(source) ul+=size; return ul;}
    void __fastcall ClearSource() {if(source) delete[] source; source=NULL;}
	 void* RestoreFromHDD(TUnsharedFile *cFile, void* Param);
public:
	__fastcall TCacheArrayObject(TCacheObjectsManager *AOwner, void* Source, long SizeBytes): TCacheObject(AOwner, cotArray, Source) {size=SizeBytes; cachealone=false;}
	__fastcall ~TCacheArrayObject() {}

	long __fastcall SizeOnDisk() {return size;}
};

class TCacheProfileSGYObject: public TCacheObject
{
private:
protected:
	virtual unsigned long readSizeInMemory();
	virtual void __fastcall ClearSource();
	virtual bool CacheToHDD(TUnsharedFile *cFile, unsigned long &pointer);
	virtual void* RestoreFromHDD(TUnsharedFile *cFile, void* Param);
public:
	__fastcall TCacheProfileSGYObject(TCacheObjectsManager *AOwner, void* Source): TCacheObject(AOwner, cotProfileSGY, Source) {size=SizeOnDisk(); cachealone=true;}
	__fastcall ~TCacheProfileSGYObject() {}

	virtual long __fastcall SizeOnDisk();
};

class TCacheAnnotationsObject: public TCacheObject
{
private:
protected:
	unsigned long readSizeInMemory();
	void __fastcall ClearSource();
	bool CacheToHDD(TUnsharedFile *cFile, unsigned long &pointer);
	void* RestoreFromHDD(TUnsharedFile *cFile, void* Param);
public:
	__fastcall TCacheAnnotationsObject(TCacheObjectsManager *AOwner, void* Source): TCacheObject(AOwner, cotAnnotations, Source) {size=SizeOnDisk(); cachealone=true;}
	__fastcall ~TCacheAnnotationsObject() {}

	long __fastcall SizeOnDisk();
};

class TCacheProfileDataObject: public TCacheProfileSGYObject
{
private:
protected:
	void __fastcall ClearSource();
	bool CacheToHDD(TUnsharedFile *cFile, unsigned long &pointer);
	void* RestoreFromHDD(TUnsharedFile *cFile, void* Param);
public:
	__fastcall TCacheProfileDataObject(TCacheObjectsManager *AOwner, void* Source): TCacheProfileSGYObject(AOwner, Source) {type=cotProfileData;}
	__fastcall ~TCacheProfileDataObject() {}

	long __fastcall SizeOnDisk() {return TCacheProfileSGYObject::SizeOnDisk();};
};

class TCacheObjectsList: public TMyObjectsList
{
private:
protected:
	TCacheObject* __fastcall readItem(int Index) {return (TCacheObject*)readMyObjectItem(Index);}
	void __fastcall writeItem(int Index, TCacheObject* value) {writeMyObjectItem(Index, (TMyObject*)value);}
public:
	__fastcall TCacheObjectsList(TMyObject *AOwner) : TMyObjectsList(AOwner) {}
	__fastcall ~TCacheObjectsList() {}

	int __fastcall Add(TCacheObject* o) { return TMyObjectsList::Add((TMyObject*)o);}

	__property TCacheObject* Items[int Index] = {read=readItem, write=writeItem};
};

class TCacheWriteThread: public TStartStopThread
{
private:
	TCacheObjectsManager *Owner;
	TEventedQueue *Queue;
protected:
	void __fastcall ExecuteLoopBody();
public:
	__fastcall TCacheWriteThread(TCacheObjectsManager* AOwner, TEventedQueue *AQueue);
	__fastcall ~TCacheWriteThread() {}
};

class TCacheWalkerThread: public TStartStopThread
{
private:
	TCacheObjectsManager *Owner;
	int PassedTime;
	TObject* Manager;
	void __fastcall ShowRDWarning();
	void __fastcall HideRDWarning();
protected:
	void __fastcall ExecuteLoopBody();
public:
	__fastcall TCacheWalkerThread(TCacheObjectsManager* AOwner, TObject* AManager=NULL);
	__fastcall ~TCacheWalkerThread();
};

typedef void __fastcall (__closure *TSynchronizeMethod)(TThreadMethod Method);

class TCacheObjectsManager: public TMyObject
{
private:
	bool CheckFreeSpaceOnHDD;
	AnsiString tempfoldermask, tempfilemask;
	TUnsharedFile* CommonTempFile;
	AnsiString temppath;
	int FileIndex;
	unsigned long maxffasize, memlvl2cache, maxmemlvl, criticalmemlvl;
	unsigned long criticalhddspace;
	TCacheObjectsList *List;
	TEventedQueue *CacheQueue; // Queue of Cache objects procedures, to protect sharing to the files.
	TCacheWriteThread *CacheThread;
	TCacheWalkerThread *CacheWalker;
	TUnsharedFilesList *TempFiles;
	TBusyLocker *RestoreLock;
	bool stopwalking;
	TObject* Manager;
protected:
	TUnsharedFile* __fastcall NewTempFile();
	unsigned long __fastcall readSizeInMemory();
	void __fastcall ShowHDDAlert();
public:
	__fastcall TCacheObjectsManager(AnsiString tmpFolderM, AnsiString tmpFileM, TObject* AManager=NULL);
	__fastcall ~TCacheObjectsManager();

	void __fastcall RemoveChildFromQueue(TMyObject* o) {if(o) {CacheQueue->Remove(o);}}
	void __fastcall RemoveChildFromList(TMyObject* o);

	TUnsharedFile* __fastcall OpenTempFile(TCacheObject *co);

    TUnsharedFile* __fastcall GetTempFile(TCacheObject *co) {if(co && List->IndexOf(co)>=0) return TempFiles->FindByFileName(co->FileName); else return NULL;}

	TCacheObject* __fastcall CacheIt(TCacheObjectType tT, void* Source, long Size, TSynchronizeMethod Synchro);
	TCacheObject* __fastcall CacheIt(TCacheObject* co, TSynchronizeMethod Synchro);
	void* __fastcall Restore(TCacheObject* co, void* Param=NULL);

	__property AnsiString TempFolderMask = {read=tempfoldermask};
	__property AnsiString TempFileMask = {read=tempfilemask};
	__property AnsiString TempPath = {read=temppath};
    __property unsigned long SizeInMemomory = {read=readSizeInMemory};
	__property unsigned long MaxFreeForAllSize = {read=maxffasize, write=maxffasize}; //in Bytes
    __property unsigned long MemoryLevelToStartCache = {read=memlvl2cache, write=memlvl2cache};
    __property unsigned long MaxMemoryLevel = {read=maxmemlvl, write=maxmemlvl};
    __property unsigned long CriticalMemoryLevel = {read=criticalmemlvl, write=criticalmemlvl};
    __property unsigned long CriticalFreeSpaceOnHDD = {read=criticalhddspace, write=criticalhddspace};
    __property bool StopWalking = {read=stopwalking, write=stopwalking};
};

extern PACKAGE TCacheObjectsManager *CacheManager;

#endif
