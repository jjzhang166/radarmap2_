//---------------------------------------------------------------------------

#pragma hdrstop

#include "XMLMap.h"
#include "Manager.h"
#include "Files.h"
//---------------------------------------------------------------------------

#pragma package(smart_init)

//---------------------------------------------------------------------------
// TXMLMapObject
//---------------------------------------------------------------------------
__fastcall TXMLMapObject::TXMLMapObject(TMyObject *AOwner) : TMyObject()
{
	owner=AOwner;
	if(owner!=NULL)
	{
		index=((TMyObject*)owner)->AddObject((TMyObject*)this);
	}
	else index=-1;
	visible=true;
}

//---------------------------------------------------------------------------
// TXMLMapBitmap
//---------------------------------------------------------------------------
__fastcall TXMLMapBitmap::TXMLMapBitmap(TMyObject *AOwner, TXMLMapBitmapType T, UnicodeString AFilename)
	: TXMLMapObject(AOwner)
{
	filename=AFilename;
	caption=ExtractFileName(AFilename);
	type=T;
}
//---------------------------------------------------------------------------

void __fastcall TXMLMapBitmap::Update()
{
	if(Owner!=NULL)
	{
		if(type==xmbtLocation) Owner->SetObjectChanged(gotUtility);
		else Owner->SetObjectChanged(gotInfo);
	}
}

//---------------------------------------------------------------------------
// TXMLMapTheme
//---------------------------------------------------------------------------
int __fastcall TXMLMapTheme::AddObject(TMyObject *o)
{
	int i;

	if(o!=NULL)
	{
		i=0;
		try
		{
			while(i<Maps->Count)
			{
				if(((TXMLMapBitmap*)o)->Type==((TXMLMapBitmap*)Maps->Items[i])->Type)
					Maps->Delete(i);
				else i++;
			}
			i=Maps->Add(o);
		}
		catch(Exception &e)
		{
			i=-1;
		}
	}
	return i;
}
//---------------------------------------------------------------------------

TXMLMapBitmap* TXMLMapTheme::GetObject(TXMLMapBitmapType T)
{
	int i;
	TXMLMapBitmap* po;

	i=0;
	po=NULL;
	try
	{
		while(i<Maps->Count)
		{
			po=(TXMLMapBitmap*)Maps->Items[i];
			if(po->Type==T) break;
			po=NULL;
			i++;
		}
	}
	catch(Exception &e)
	{
		po=NULL;
	}
	return po;
}

//---------------------------------------------------------------------------
// TXMLMapThemesCollection
//---------------------------------------------------------------------------
int __fastcall TXMLMapThemesCollection::AddObject(TMyObject *o)
{
	int i;

	if(o!=NULL)
	{
		try
		{
			((TXMLMapTheme*)o)->Owner=this;
			i=Themes->Add(o);
		}
		catch(Exception &e)
		{
			i=-1;
		}
	}
	return i;
}

//---------------------------------------------------------------------------
// TMapsContainerLoader
//---------------------------------------------------------------------------
__fastcall TMapsContainerLoader::TMapsContainerLoader(TObject* aManager, TMapsContainer* aOwner,
	AnsiString aFilename, HANDLE aStopItEvent, bool aWithUpdate, TCoordinateSystem aCS,
	TVoidFunction aSuccessCallBack, TVoidFunction aCancelCallBack, bool aPreview) : TMyThread(true)
{
	Manager=aManager;
	Owner=aOwner;
	Filename=aFilename;
	StopItEvent=aStopItEvent;
	WithUpdate=aWithUpdate;
	CS=aCS;
	SuccessCallBack=aSuccessCallBack;
	CancelCallBack=aCancelCallBack;
	Preview=aPreview;
	FreeOnTerminate=true;
	AskForResume();
}
//---------------------------------------------------------------------------

void __fastcall TMapsContainerLoader::Execute()
{
	bool err=false;

	SetStarted();
	if(Owner)
	{
		try
		{
			Owner->LoadData(Filename, StopItEvent, false, CS, Preview);
			Synchronize(UpdateOwner);
			if(StopItEvent && WaitForSingleObject(StopItEvent, 0)==WAIT_OBJECT_0) err=true;
			else if(Manager && ((TRadarMapManager*)Manager)->GpsUnit)
				((TRadarMapManager*)Manager)->GpsUnit->ReCenter();
		}
		catch(...) {err=true;}
		if(err && CancelCallBack) Synchronize(CancelCallBack);
		else if(!err && SuccessCallBack) Synchronize(SuccessCallBack);
	}
	SetIsTerminated();
}
//---------------------------------------------------------------------------

void __fastcall TMapsContainerLoader::UpdateOwner()
{
	if((!StopItEvent || WaitForSingleObject(StopItEvent, 0)!=WAIT_OBJECT_0) && Owner && WithUpdate) Owner->Update();
	if((!StopItEvent || WaitForSingleObject(StopItEvent, 0)!=WAIT_OBJECT_0) && Manager) ((TRadarMapManager*)Manager)->RenewMapTrackingPath();
}

//---------------------------------------------------------------------------
// TMapsContainer
//---------------------------------------------------------------------------
__fastcall TMapsContainer::TMapsContainer(TMyObject *AOwner, void* ASettings, TLockedDoubleRect *AViewportArea) :
	TXMLMapObject(AOwner)
{
	maptype=mtNone;
	Collection=(TMapsContainersCollection*)AOwner;
	showbackground=true;
	radarmapsettings=ASettings;
	viewportarea=AViewportArea;
	if(ASettings) Manager=((TRadarMapSettings*)ASettings)->Owner;
	Collections=new TXMLMapObjectsList(this);
	backgroundbmp=NULL;
	InfoBmp=new TBitmap32();
	InfoBmp->DrawMode=dmBlend;
	UtilityBmp=new TBitmap32();
	UtilityBmp->DrawMode=dmBlend;
	coordinatesrect=TDoubleRect(0,0,0,0);
	positionrect=TDoubleRect(0,0,0,0);
	if(AOwner==NULL || Index==-1) progress=NULL;
	klicnummer="0";
	filename="";
	backupfilename="";
	lower.X=0;
	lower.Y=0;
	upper.X=0;
	upper.Y=0;
	scalingfactorx=1.;
	scalingfactory=1.;
	lockedcs=false;
	if(radarmapsettings)
	{
		coordinatesystem=((TRadarMapSettings*)radarmapsettings)->DefaultCoordinateSystem;
	}
	else
	{
#ifdef _InfraRadarOnly
		coordinatesystem=csDutch;//csLatLon;
#else
		coordinatesystem=csLocalMetric();
#endif
	}
	if(Owner && ((TMapsContainersCollection*)Owner)->Image)
	{
		Types::TRect bmr=((TMapsContainersCollection*)Owner)->Image->GetBitmapRect();
		((TMapsContainersCollection*)Owner)->Image->ScrollToCenter(bmr.Width()>>1, bmr.Height()>>1);
		WatermarkLayer=new TBitmapLayer(((TMapsContainersCollection*)Owner)->Image->Layers);
		WatermarkLayer->OnPaint=&WatermarkLayerPaint;
		WatermarkLayer->Bitmap->DrawMode=dmBlend;
		WatermarkLayer->SendToBack();
	}
	CornersChanged=true;
	latlonrect=new TGPSCoordinatesRect();
	ondisablebutton=NULL;
	onpredictorfoundobject=NULL;
	GpsListener=NULL;
	predictedcoordinateslist=new TGPSCoordinatesList();
	copyrights=false;
	copyrightsstr="";
	copyrightslink="";
}
//---------------------------------------------------------------------------

__fastcall TMapsContainer::~TMapsContainer()
{
	if(Collections) delete Collections;
	Collections=NULL;
	if(InfoBmp) delete InfoBmp;
	InfoBmp=NULL;
	if(UtilityBmp) delete UtilityBmp;
	UtilityBmp=NULL;
	if(backgroundbmp) delete backgroundbmp;
	backgroundbmp=NULL;
	if(predictedcoordinateslist) delete predictedcoordinateslist;
	predictedcoordinateslist=NULL;
	DisconnectGpsListener();
	if(latlonrect) delete latlonrect;
	latlonrect=NULL;
	if(backupfilename!=NULL && backupfilename.Length()>0 && FileExists(backupfilename))
		DeleteFile(backupfilename);
	if(WatermarkLayer)
	{
		if(Owner && ((TMapsContainersCollection*)Owner)->Image && Owner && ((TMapsContainersCollection*)Owner)->Image->Layers)
			((TMapsContainersCollection*)Owner)->Image->Layers->Delete(WatermarkLayer->Index);
		else delete WatermarkLayer;
		WatermarkLayer=NULL;
	}
}
//---------------------------------------------------------------------------

TMyObject* __fastcall TMapsContainer::readObjectsContainer()
{
	TMyObject *res=NULL;

	if(Owner)
	{
    	res=((TMapsContainersCollection*)Owner)->Owner;
	}
	return res;
}
//---------------------------------------------------------------------------

void __fastcall TMapsContainer::ConnectGpsListener()
{
	if(Manager && ((TRadarMapManager*)Manager)->GpsListeners)
	{
		if(GpsListener) DisconnectGpsListener();
		GpsListener = new TGpsListener();
		GpsListener->OnAddCoordinate = AddCoordinate;
		((TRadarMapManager*)Manager)->GpsListeners->Add(this->GpsListener);
	}
	else GpsListener=NULL;
}
//---------------------------------------------------------------------------

void __fastcall TMapsContainer::DisconnectGpsListener()
{
	if(GpsListener && Manager && ((TRadarMapManager*)Manager)->GpsListeners)
	{
		((TRadarMapManager*)Manager)->GpsListeners->Delete(GpsListener);
		delete GpsListener;
	}
	GpsListener=NULL;
}
//---------------------------------------------------------------------------

void __fastcall TMapsContainer::AddCoordinate(TGPSCoordinate *c)
{
	TRadarMapManager *mngr = dynamic_cast<TRadarMapManager *>(this->Manager);

	try
	{
		if (c && c->Valid && mngr && mngr->Settings && mngr->Settings->MapObjectsPrediction)
		{
			new TUPPredictorThread(this, c->GetXYH(coordinatesystem), ProcessPredictorResults,
				((TRadarMapManager*)Manager)->Settings->LabelDestructionTimeByTrack, Manager);
		}
	}
	catch(...) {}
}
//---------------------------------------------------------------------------

void __fastcall TMapsContainer::ProcessPredictorResults(void *vMapPoint, unsigned long oLifetime)
{
	TUPPPoint<TUPPFavoriteObject> *pMapPoint;

	if(pMapPoint && pMapPoint->mFavorites.size()>0)
	{
		std::vector<TUPPFavoriteObject *>::iterator it;
		TUPPFavoriteObject *favorite=NULL;

		for (it=pMapPoint->mFavorites.begin(); it!=pMapPoint->mFavorites.end(); it++)
		{
			favorite = *it;
			if(OnPredictorFoundObject) (OnPredictorFoundObject)(favorite);
		}

		// ToDo finaly
	}
}
//---------------------------------------------------------------------------

void __fastcall TMapsContainer::LoadData(AnsiString Filename, HANDLE AStopItEvent, bool WithUpdate, bool Preview)
{
	TCoordinateSystem CS;

	if(Manager && ((TRadarMapManager*)Manager)->Settings)
		CS=((TRadarMapManager*)Manager)->Settings->DefaultCoordinateSystem;
//	else CS=csLatLon;
#ifdef _InfraRadarOnly
	else CS=csDutch;
#else
	else CS=csUTM;
#endif
	LoadData(Filename, AStopItEvent, WithUpdate, CS, Preview);
}
//---------------------------------------------------------------------------

void __fastcall TMapsContainer::LoadDataAsThread(AnsiString Filename, HANDLE AStopItEvent,
	bool WithUpdate, TVoidFunction SuccessCallBack, TVoidFunction CancelCallBack, bool Preview)
{
	TCoordinateSystem CS;

	if(Manager && ((TRadarMapManager*)Manager)->Settings)
		CS=((TRadarMapManager*)Manager)->Settings->DefaultCoordinateSystem;
//	else CS=csLatLon;
#ifdef _InfraRadarOnly
	else CS=csDutch;
#else
	else CS=csUTM;
#endif
	LoadDataAsThread(Filename, AStopItEvent, WithUpdate, CS, SuccessCallBack, CancelCallBack, Preview);
}
//---------------------------------------------------------------------------

void __fastcall TMapsContainer::LoadDataAsThread(AnsiString Filename, HANDLE AStopItEvent,
	bool WithUpdate, TCoordinateSystem CS, TVoidFunction SuccessCallBack, TVoidFunction CancelCallBack,
	bool Preview)
{
	new TMapsContainerLoader(Manager, this, Filename, AStopItEvent, WithUpdate, CS, SuccessCallBack, CancelCallBack, Preview);
}
//---------------------------------------------------------------------------

void __fastcall TMapsContainer::ClearData()
{
	Collections->Clear();
}
//---------------------------------------------------------------------------

void __fastcall TMapsContainer::BackupEx(AnsiString aFileName)
{
	if(filename!="" && FileExists(filename))
	{
		if(aFileName=="")
		{
			AnsiString dir, name, ext;

			dir=ExtractFilePath(filename);
			name=ExtractFileName(filename);
			ext=ExtractFileExt(filename);
			if(ext!=NULL && ext.Length()>0)
			{
				if(name!=NULL && name.Length()>0)
					name.SetLength(name.Length()-ext.Length());
				if(ext==".") ext="";
				else if(ext[1]=='.') ext=ext.SubString(2, ext.Length());
			}
			aFileName=dir+"\\"+name+".~"+ext;
		}
		else if(FileExists(aFileName))
		{
			DeleteFile(aFileName);
		}
		CopyFile(FileName.c_str(), aFileName.c_str(), false);
		backupfilename=aFileName;
	}
}
//---------------------------------------------------------------------------

void __fastcall TMapsContainer::RollbackBackupEx(AnsiString aFileName, bool AsThread)
{
	if(aFileName!="" && FileExists(aFileName) && filename!=aFileName)
	{
		try
		{
			HANDLE StopItEvent;

			ClearData();
			CopyFile(aFileName.c_str(), FileName.c_str(), false);
			if(Manager) StopItEvent=((TRadarMapManager*)Manager)->GlobalStopItEvent;
			else StopItEvent=NULL;
			if(AsThread) LoadDataAsThread(FileName, StopItEvent, true, coordinatesystem, (TVoidFunction)NULL, (TVoidFunction)NULL, false);
			else LoadData(FileName, StopItEvent, true, coordinatesystem, false);
		}
		__finally
		{
			DeleteFile(aFileName);
			if(backupfilename==aFileName) backupfilename="";
		}
	}
}
//---------------------------------------------------------------------------

/*TDoublePoint __fastcall TMapsContainer::StringToDoublePoint(AnsiString value)
{
	int i;
	TDoublePoint fp;
	AnsiString str;

	if(value.Trim()!="")
	{
		str="";
		i=1;
		try
		{
			while(i<=value.Length() && value[i]!=' ')
				str+=value[i++];
			fp.X=StrToFloat(str);
			i++;
			str="";
			while(i<=value.Length() && value[i]!=' ')
				str+=value[i++];
			fp.Y=StrToFloat(str);
		}
		catch(Exception &e)
		{
			fp.X=-9999;
			fp.Y=-9999;
		}
	}
	else
	{
		fp.X=-9999;
		fp.Y=-9999;
	}
	return fp;
}*/
//---------------------------------------------------------------------------

/*TDouble3DPoint __fastcall TMapsContainer::StringToDouble3DPoint(AnsiString pos)
{
	TDouble3DPoint fp;
	int j=0, i;
	double x, y, z, d;
	bool digit=false, err=false;
	AnsiString nr="";
	char ch;

	if(pos.Trim()!="")
	{
		x=y=z=0;
		i=1;
		try
		{
			while(i<=pos.Length())
			{
				if(pos[i]>='0' && pos[i]<='9') nr+=pos[i];
				else if(pos[i]=='.' || pos[i]==',') nr+=DecimalSeparator;
				else if(pos[i]=='-') nr+="-";
				else digit=true;
				if(digit || i==pos.Length())
				{
					digit=false;
					try
					{
						d=StrToFloat(nr);
					}
					catch(Exception &e) {j=-1;}
					nr="";
					j++;
					if(j==1) x=d;
					else if(j==2) y=d;
					else if(j==3) z=d;
				}
				i++;
			}
			if(j>1) fp=TDouble3DPoint(x, y, z);
			else err=true;
		}
		catch(Exception &e)
		{
			err=true;
		}
	}
	if(err)
	{
		fp.X=-9999;
		fp.Y=-9999;
		fp.Z=-9999;
	}
	return fp;
}*/
//---------------------------------------------------------------------------

void __fastcall TMapsContainer::writeLower(TDoublePoint value)
{
	lower=value;
	/*
	if(upper.X>lower.X || upper.Y>lower.Y)
	{
		coordinatesrect.BottomRight=upper;
		coordinatesrect.TopLeft=lower;
	}
	else
	{
		coordinatesrect.TopLeft=upper;
		coordinatesrect.BottomRight=lower;
	}*/
	coordinatesrect.RightBottom=lower;
}
//---------------------------------------------------------------------------

void __fastcall TMapsContainer::writeUpper(TDoublePoint value)
{
	upper=value;
	/*if(upper.X>lower.X || upper.Y>lower.Y)
	{
		coordinatesrect.BottomRight=upper;
		coordinatesrect.TopLeft=lower;
	}
	else
	{
		coordinatesrect.TopLeft=upper;
		coordinatesrect.BottomRight=lower;
	}*/
	coordinatesrect.LeftTop=upper;
}
//---------------------------------------------------------------------------

void __fastcall TMapsContainer::writeCoordinateSystem(TCoordinateSystem value)
{
	bool err;
	if(!lockedcs)
	{
		//Recalcul CoordinatesRect in correspondance to new CS
		TGPSCoordinate *c;

		c=new TGPSCoordinate();
		try
		{
			try
			{
				c->SetXY(coordinatesystem, coordinatesrect.LeftTop);
				coordinatesrect.LeftTop=c->GetXY(value);
				c->SetXY(coordinatesystem, coordinatesrect.RightBottom);
				coordinatesrect.RightBottom=c->GetXY(value);
				err=false;
			}
			catch(...) {err=true;}
		}
		__finally
		{
			delete c;
		}
		if(!err)
		{
			coordinatesystem=value;
			LatLonRectUpdate();
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TMapsContainer::writeLowerCorner(AnsiString value)
{
	TDoublePoint fp;

	fp=DoublePoint(value);//StringToDoublePoint(value);
	if(fp.X>-9999 && fp.Y>-9999)
	{
		lowercorner=value;
		Lower=fp;
	}
}
//---------------------------------------------------------------------------

void __fastcall TMapsContainer::writeUpperCorner(AnsiString value)
{
	TDoublePoint fp;

	fp=DoublePoint(value);//StringToDoublePoint(value);
	if(fp.X>-9999 && fp.Y>-9999)
	{
		uppercorner=value;
		Upper=fp;
	}
}
//---------------------------------------------------------------------------

int __fastcall TMapsContainer::AddObject(TMyObject *o)
{
	int i;

	if(o!=NULL)
	{
		try
		{
			((TXMLMapThemesCollection*)o)->Owner=this;
			i=Collections->Add(o);
		}
		catch(Exception &e)
		{
			i=-1;
		}
	}
	return i;
}
//---------------------------------------------------------------------------

void __fastcall TMapsContainer::LatLonRectUpdate()
{
	TGPSCoordinate *lt, *rb;
	TDoubleRect drct;
	double cw, ch, minx, maxy, left, top;
	TDoublePoint vpac, crc;

//	if(coordinatesrect.Left!=0 || coordinatesrect.Top!=0 ||
//		coordinatesrect.Right!=0 || coordinatesrect.Bottom!=0)
	if(coordinatesrect.Width()>0 && coordinatesrect.Height()>0)
	{
		if(ViewportArea)
		{
			cw=coordinatesrect.Width();
			ch=coordinatesrect.Height();

			vpac=ViewportArea->Center();
			//if(vpac.X<=0 || vpac.X>=1. || vpac.Y<=0 || vpac.Y>=1.)
			if(vpac.X<=0.25 || vpac.X>=0.75 || vpac.Y<=0.25 || vpac.Y>=0.75)
			{
				if(IsFloatingMap)
				{
					//crc.X=minx+(coordinatesrect.Right-coordinatesrect.Left)/2.;
					crc.X=coordinatesrect.Left+(coordinatesrect.Right-coordinatesrect.Left)/2.;
					crc.Y=coordinatesrect.Top+(coordinatesrect.Bottom-coordinatesrect.Top)/2.;

					if(coordinatesrect.Left<coordinatesrect.Right)
						left=crc.X-cw*(0.5-vpac.X);
					else left=crc.X+cw*(0.5-vpac.X);
					coordinatesrect.Left=left-cw/2.;
					coordinatesrect.Right=left+cw/2.;

					if(coordinatesrect.Top>coordinatesrect.Bottom)
						top=crc.Y+ch*(0.5-vpac.Y);
					else top=crc.Y-ch*(0.5-vpac.Y);
					coordinatesrect.Top=top+ch/2.;
					coordinatesrect.Bottom=top-ch/2.;

					ViewportArea->Left-=vpac.X-0.5;
					ViewportArea->Right-=vpac.X-0.5;
					ViewportArea->Top-=vpac.Y-0.5;
					ViewportArea->Bottom-=vpac.Y-0.5;

					/*left=crc.X-cw*(0.5-vpac.X);
					top=crc.Y-ch*(0.5-vpac.Y);
					if(coordinatesrect.Left<coordinatesrect.Right)
					{
						coordinatesrect.Left=left;
						coordinatesrect.Right=left+cw;
					}
					else
					{
						coordinatesrect.Right=left;
						coordinatesrect.Left=left+cw;
					}
					if(coordinatesrect.Top<coordinatesrect.Bottom)
					{
						coordinatesrect.Top=top;
						coordinatesrect.Bottom=top-ch;
					}
					else
					{
						coordinatesrect.Bottom=top;
						coordinatesrect.Top=top-ch;
					}*/
				}
				else
				{
					if(vpac.X<0 || vpac.X>1)
					{
						if(vpac.X>1.) vpac.X-=1.;
						ViewportArea->Left-=vpac.X;
						ViewportArea->Right-=vpac.X;
					}
					if(vpac.Y<=0 || vpac.Y>=1.)
					{
						if(vpac.Y>1.) vpac.Y-=1.;
						ViewportArea->Top-=vpac.Y;
						ViewportArea->Bottom-=vpac.Y;
					}
				}
			}
			if(coordinatesrect.Left<coordinatesrect.Right) minx=coordinatesrect.Left;
			else minx=coordinatesrect.Right;
			if(coordinatesrect.Top>coordinatesrect.Bottom) maxy=coordinatesrect.Top;
			else maxy=coordinatesrect.Bottom;

			drct.Left=minx+ViewportArea->Left*cw;//ScalingFactorX;
			drct.Bottom=maxy-ViewportArea->Top*ch;//ScalingFactorY;
			drct.Right=minx+ViewportArea->Right*cw;//ScalingFactorX;
			drct.Top=maxy-ViewportArea->Bottom*ch;//ScalingFactorY;
			*((int*)drct.LeftTopTemp)=*((int*)coordinatesrect.LeftTopTemp);
			*((int*)drct.RightBottomTemp)=*((int*)coordinatesrect.RightBottomTemp);
			/*
			if(coordinatesrect.Left<coordinatesrect.Right) minx=coordinatesrect.Left;
			else minx=coordinatesrect.Right;
			if(coordinatesrect.Top<coordinatesrect.Bottom) miny=coordinatesrect.Top;
			else miny=coordinatesrect.Bottom;
			drct.Left=minx+ViewportArea->Left*cw;//ScalingFactorX;
			drct.Top=miny+ViewportArea->Top*ch;//ScalingFactorY;
			drct.Right=minx+ViewportArea->Right*cw;//ScalingFactorX;
			drct.Bottom=miny+ViewportArea->Bottom*ch;//ScalingFactorY;*/

			LatLonRectScaling(&drct);
		}
		else drct=coordinatesrect;
		lt=new TGPSCoordinate();
		rb=new TGPSCoordinate();
		try
		{
			lt->AutomaticUpdate=false;
			rb->AutomaticUpdate=false;
			if(CornersChanged)
			{
//				if(coordinatesystem==csLatLon)
//				{
//					lt->Latitude=drct.Bottom;
//					lt->Longitude=drct.Left;
//					rb->Latitude=drct.Top;
//					rb->Longitude=drct.Right;
//				}
//				else
//				{
					/*lt->SetX(coordinatesystem, drct.Left);
					lt->SetY(coordinatesystem, drct.Bottom);
					rb->SetX(coordinatesystem, drct.Right);
					rb->SetY(coordinatesystem, drct.Top);*/
					lt->SetXY(coordinatesystem, drct.Left, drct.Bottom, coordinatesrect.LeftTopTemp);
					rb->SetXY(coordinatesystem, drct.Right, drct.Top, coordinatesrect.RightBottomTemp);
//				}
			}
			lt->AutomaticUpdate=true;
			rb->AutomaticUpdate=true;
			LatLonRect->SetCorners(lt, rb);
		}
		__finally
		{
			delete lt;
			delete rb;
		}
	}
	else
	{
		LatLonRect->Clear();
	}
}
//---------------------------------------------------------------------------

void __fastcall TMapsContainer::Update(TMyObjectType Type) //overload from TMyObject
{
	if(Type==gotNone) Update();
	else if(Type==gotUtility) UtilityUpdate();
	else InfoUpdate();
}
//---------------------------------------------------------------------------

void __fastcall TMapsContainer::Update()
{
	if(AutomaticUpdate)
	{
		try
		{
			if(progress) progress->Show(Collections->Count*2, false);
			try
			{
				LatLonRectUpdate();
				RenderMap();
				/*if(backgroundbmp && Owner && ((TMapsContainersCollection*)Owner)->Image)
				{
					if(Copyrights && CopyrightsStr!=NULL && CopyrightsStr.Length()>0)
					{
						TFont *font=new TFont();
						int tw, th, tx, ty;
						Types::TRect bmr;

						try
						{
							font->Name=backgroundbmp->Font->Name;
							font->Size=backgroundbmp->Font->Size;
							font->Style=backgroundbmp->Font->Style;
							backgroundbmp->Font->Name="Tahoma";
							backgroundbmp->Font->Size=8;
							backgroundbmp->Font->Style=TFontStyles();
							tw=backgroundbmp->TextWidth(CopyrightsStr);
							th=backgroundbmp->TextHeight(CopyrightsStr);
							bmr=((TMapsContainersCollection*)Owner)->Image->GetBitmapRect();
							tx=-bmr.Left+((TMapsContainersCollection*)Owner)->Image->Width-tw-5;
							ty=-bmr.Top+((TMapsContainersCollection*)Owner)->Image->Height-th*2-5;
							backgroundbmp->RenderText(tx, ty, CopyrightsStr, 2, clBlack32);//clGray32);
							if(CopyrightsLink!=NULL && CopyrightsLink.Length()>0)
							{
								backgroundbmp->Font->Style= TFontStyles() << fsUnderline;
								tw=backgroundbmp->TextWidth(CopyrightsLink);
								tx=-bmr.Left+((TMapsContainersCollection*)Owner)->Image->Width-tw-5;
								ty=-bmr.Top+((TMapsContainersCollection*)Owner)->Image->Height-th-5;
								backgroundbmp->RenderText(tx, ty, CopyrightsLink, 2, clBlue32);//clGray32);
							}
						}
						__finally
						{
							backgroundbmp->Font->Name=font->Name;
							backgroundbmp->Font->Size=font->Size;
							backgroundbmp->Font->Style=font->Style;
							delete font;
						}
					}
				}*/
			}
			catch(...) {}
			if(Owner!=NULL) Owner->SetObjectChanged(gotBackground);
			if(progress) progress->Hide(false);
		}
		catch(...) {}
	}
}
//---------------------------------------------------------------------------

void __fastcall TMapsContainer::WatermarkLayerPaint(TObject *Sender, TBitmap32 *Bmp)
{
	TFont *font;
	int tw, th, tx, ty, w, h;

	if(Owner && ((TMapsContainersCollection*)Owner)->Image && Bmp)
	{
		w=((TMapsContainersCollection*)Owner)->Image->Width;
		h=((TMapsContainersCollection*)Owner)->Image->Height;
		if(Copyrights && ((CopyrightsStr!=NULL && CopyrightsStr.Length()>0) ||
			(CopyrightsLink!=NULL && CopyrightsLink.Length()>0)))
		{
			Bmp->BeginUpdate();
			font=new TFont();
			try
			{
				try
				{
					font->Name=Bmp->Font->Name;
					font->Size=Bmp->Font->Size;
					font->Style=Bmp->Font->Style;
					Bmp->Font->Name="Tahoma";
					Bmp->Font->Size=8;
					Bmp->Font->Style=TFontStyles();
					if(CopyrightsStr!=NULL && CopyrightsStr.Length()>0)
					{
						tw=Bmp->TextWidth(CopyrightsStr);
						th=Bmp->TextHeight(CopyrightsStr);
						tx=w-tw-5;
						if(CopyrightsLink!=NULL && CopyrightsLink.Length()>0) th*=2;
						ty=h-th-5;
						Bmp->RenderText(tx, ty, CopyrightsStr, 2, clBlack32);//clGray32);
					}
					if(CopyrightsLink!=NULL && CopyrightsLink.Length()>0)
					{
						Bmp->Font->Style=TFontStyles() << fsUnderline;
						tw=Bmp->TextWidth(CopyrightsLink);
						th=Bmp->TextHeight(CopyrightsStr);
						tx=w-tw-5;
						ty=h-th-5;
						Bmp->RenderText(tx, ty, CopyrightsLink, 2, clBlue32);//clGray32);
					}
				}
				catch(...) {}
			}
			__finally
			{
				Bmp->Font->Name=font->Name;
				Bmp->Font->Size=font->Size;
				Bmp->Font->Style=font->Style;
				delete font;
				Bmp->EndUpdate();
			}
		}
	}
}
//---------------------------------------------------------------------------

AnsiString __fastcall TMapsContainer::FindFileAtDir(AnsiString path, AnsiString Mask)
{
	WIN32_FIND_DATA FileData;
	HANDLE hSearch;
	AnsiString S, Out="";

	hSearch=FindFirstFile((path+Mask).c_str(), &FileData);
	if(hSearch!=INVALID_HANDLE_VALUE)
	{
		try
		{
			do
			{
				S=FileData.cFileName;
				if(S!="." && S!="..")
				{
					if(FileData.dwFileAttributes!=FILE_ATTRIBUTE_DIRECTORY)
					{
						Out=path+S;
					}
				}
			} while(FindNextFile(hSearch, &FileData) && Out=="");
		}
		__finally
		{
			FindClose(hSearch);
		}
	}
	return Out;
}

//---------------------------------------------------------------------------
// TMapsContainersCollection
//---------------------------------------------------------------------------
__fastcall TMapsContainersCollection::TMapsContainersCollection(TMyObject *AOwner, TImgView32 *ImgView, void* RMS) : TMyObject()
{
	owner=AOwner;
	image=ImgView;
	objecttype=gotContainersCollection;
	Collection=new TXMLMapObjectsList(this);
	ZoomUpdateInProgress=false;
	NeedReZooming=false;
#ifndef OnlyOneMap
	BackgroundBmp=new TBitmap32();
	BackgroundBmp->DrawMode=dmBlend;
	if(image!=NULL) BackgroundBmp->SetSize(image->Width, image->Height);
	BackgroundBmp->BeginUpdate();
	BackgroundBmp->Clear(Color32(0,0,0,0));
	BackgroundBmp->EndUpdate();
	UtilityBmp=new TBitmap32();
	UtilityBmp->DrawMode=dmBlend;
	if(image!=NULL) UtilityBmp->SetSize(image->Width, image->Height);
	UtilityBmp->BeginUpdate();
	UtilityBmp->Clear(Color32(0,0,0,0));
	UtilityBmp->EndUpdate();
	InfoBmp=new TBitmap32();
	InfoBmp->DrawMode=dmBlend;
	if(image!=NULL) InfoBmp->SetSize(image->Width, image->Height);
	InfoBmp->BeginUpdate();
	InfoBmp->Clear(Color32(0,0,0,0));
	InfoBmp->EndUpdate();
#else
	BackgroundBmp=NULL;
	UtilityBmp=NULL;
	InfoBmp=NULL;
#endif
	Draw=NULL;
	UtilityDraw=NULL;
	InfoDraw=NULL;
	if(image!=NULL)
	{
		UtilityLayer=new TBitmapLayer(image->Layers);
		UtilityLayer->OnPaint=&UtilityLayerPaint;
		UtilityLayer->Bitmap->DrawMode=dmBlend;
		UtilityLayer->SendToBack();
		InfoLayer=new TBitmapLayer(image->Layers);
		InfoLayer->OnPaint=&InfoLayerPaint;
		InfoLayer->Bitmap->DrawMode=dmBlend;
		InfoLayer->SendToBack();
	}
	else
	{
		InfoLayer=NULL;
		UtilityLayer=NULL;
	}
	coordinatesrect=TDoubleRect(0,0,0,0);
	latlonrect=new TGPSCoordinatesRect();
	progress=NULL;
	realrect=new Types::TRect(0,0,0,0);
	RadarMapSettings=RMS;
	viewportarea=new TLockedDoubleRect(0,0,1,1); // 100% zoom
	//coordinatesystem=csLatLon;
}
//---------------------------------------------------------------------------

__fastcall TMapsContainersCollection::~TMapsContainersCollection()
{
	if(Collection) delete Collection;
	Collection=NULL;
	if(InfoLayer!=NULL)
	{
		if(Image && Image->Layers) image->Layers->Delete(InfoLayer->Index);
		else delete InfoLayer;
	}
	InfoLayer=NULL;
	if(UtilityLayer!=NULL)
	{
		if(Image && Image->Layers) image->Layers->Delete(UtilityLayer->Index);
		else delete UtilityLayer;
	}
	UtilityLayer=NULL;
#ifndef OnlyOneMap
	if(BackgroundBmp) delete BackgroundBmp;
	BackgroundBmp=NULL;
	if(UtilityBmp) delete UtilityBmp;
	UtilityBmp=NULL;
	if(InfoBmp) delete InfoBmp;
	InfoBmp=NULL;
#endif
	if(realrect) delete realrect;
	realrect=NULL;
	if(latlonrect) delete latlonrect;
	latlonrect=NULL;
	if(viewportarea) delete viewportarea;
	viewportarea=NULL;
}
//---------------------------------------------------------------------------

void __fastcall TMapsContainersCollection::Clear()
{
	TGPSCoordinatesRect *tmp;
	Types::TRect *rct;

	UtilityDraw=NULL;
	InfoDraw=NULL;
	image->Bitmap->BeginUpdate();
	image->Bitmap->Clear(clWhite32);
	image->Bitmap->EndUpdate();
	Collection->Clear();
	coordinatesrect=TDoubleRect(0,0,0,0);
	//tmp=latlonrect;
	//latlonrect=new TGPSCoordinatesRect();
	//delete tmp;
    latlonrect->Clear();
	rct=realrect;
	realrect=new Types::TRect(0,0,0,0);
	delete rct;
}
//---------------------------------------------------------------------------

void __fastcall TMapsContainersCollection::Update(TMyObjectType Type) //overload from TMyObject
{
	//if(Type==gotNone) Update();
	//else
	if(Type==gotUtility) UtilityUpdate();
	else if(Type==gotInfo) InfoUpdate();
	else Update();//BackgroundUpdate();
}
//---------------------------------------------------------------------------

void __fastcall TMapsContainersCollection::RenderMaps()
{
	for(int i=0; i<Collection->Count; i++)
		((TMapsContainer*)Collection->Items[i])->RenderMap();
}
//---------------------------------------------------------------------------

int __fastcall TMapsContainersCollection::AddObject(TMyObject *o)
{
	int i;
	TMapsContainer* mc;
	float w, ww, h, hh;

	if(o!=NULL)
	{
		try
		{
			mc=(TMapsContainer*)o;
			mc->Owner=this;
			mc->Progress=progress;
			//mc->RadarMapSettings=RadarMapSettings;
			i=Collection->Add(o);
		}
		catch(Exception &e)
		{
			i=-1;
		}
	}
	return i;
}
//---------------------------------------------------------------------------

void __fastcall TMapsContainersCollection::writeProgress(TToolProgressBar *value)
{
	progress=value;
	for(int i=0; i<Collection->Count; i++)
		((TMapsContainer*)Collection->Items[i])->Progress=value;
}
//---------------------------------------------------------------------------

TCoordinateSystem __fastcall TMapsContainersCollection::readCoordinateSystem()
{
	TCoordinateSystem res;

	try
	{
		if(Collection->Count>0 && Collection->Items[0])
			res=((TMapsContainer*)Collection->Items[0])->CoordinateSystem;
		else res=csLatLon;
	}
	catch(...) {res=csLatLon;}
	return res;
}
//---------------------------------------------------------------------------

void __fastcall TMapsContainersCollection::UtilityUpdate()
{
	TMapsContainer *Container;
	int i, j, w, h;

	if(Collection->Count==1 && Collection->Items[0]->Visible)
		UtilityDraw=((TMapsContainer*)Collection->Items[0])->Utility;
	else if(Collection->Count==0) UtilityDraw=NULL;
#ifndef OnlyOneMap
	else
	{
		UtilityBmp->BeginUpdate();
		try
		{
			UtilityBmp->Clear(Color32(0,0,0,0));
			w=UtilityBmp->Width;
			h=UtilityBmp->Height;
			if(progress) progress->Max+=Collections->Count;
			for(i=0; i<Collection->Count; i++)
			{
				Container=(TMapsContainer*)Collection->Items[i];
				//Container->UtilityUpdate();
				if(Container!=NULL && Container->Visible)
				{
					UtilityBmp->Draw((int)(Container->PositionRect.Left*(float)w+0.5),
						(int)(Container->PositionRect.Top*(float)h+0.5), Container->Utility);
				}
				if(progress) progress->StepIt();
			}
		}
		__finally
		{
			UtilityBmp->EndUpdate();
			UtilityDraw=UtilityBmp;
		}
	}
 #endif
	if(UtilityLayer!=NULL)  UtilityLayer->Update();
}
//---------------------------------------------------------------------------

void __fastcall TMapsContainersCollection::InfoUpdate()
{
	TMapsContainer *Container;
	int i, j, w, h;

	if(Collection->Count==1 && Collection->Items[0]->Visible)
		InfoDraw=((TMapsContainer*)Collection->Items[0])->Info;
	else if(Collection->Count==0) InfoDraw=NULL;
#ifndef OnlyOneMap
	else
	{
		InfoBmp->BeginUpdate();
		try
		{
			InfoBmp->Clear(Color32(0,0,0,0));
			w=InfoBmp->Width;
			h=InfoBmp->Height;
			if(progress) progress->Max+=Collections->Count;
			for(i=0; i<Collection->Count; i++)
			{
				Container=(TMapsContainer*)Collection->Items[i];
				//Container->InfoUpdate();
				if(Container!=NULL && Container->Visible)
				{
					InfoBmp->Draw((int)(Container->PositionRect.Left*(float)w+0.5),
						(int)(Container->PositionRect.Top*(float)h+0.5), Container->Info);
				}
				if(progress) progress->StepIt();
			}
		}
		__finally
		{
			InfoBmp->EndUpdate();
			InfoDraw=InfoBmp;
		}
	}
#endif
	if(InfoLayer!=NULL) InfoLayer->Update();
}
//---------------------------------------------------------------------------

void __fastcall TMapsContainersCollection::GetBackgroundDraw()
{
	if(Collection->Count==1) Draw=((TMapsContainer*)Collection->Items[0])->Background;
	else if(Collection->Count==0) Draw=NULL;
	else Draw=BackgroundBmp;
}
//---------------------------------------------------------------------------

void __fastcall TMapsContainersCollection::BackgroundUpdate()
{
	TMapsContainer *Container;
	int i, j, w, h;

	GetBackgroundDraw();
#ifndef OnlyOneMap
	if(Collection->Count>1)
	{
		BackgroundBmp->BeginUpdate();
		try
		{
			BackgroundBmp->Clear(Color32(0,0,0,0));
			w=BackgroundBmp->Width;
			h=BackgroundBmp->Height;
			if(progress) progress->Max+=Collections->Count;
			for(i=0; i<Collection->Count; i++)
			{
				Container=(TMapsContainer*)Collection->Items[i];
				if(Container!=NULL && Container->Background!=NULL && Container->Visible && Container->ShowBackground)
				{
					/*if(Container->MapType==mtDXF && ((TDXFMapsContainer*)Container)->DXFCollection->Layers->Count>0)
					{
						TDXFLayer *layer;
						layer=(TDXFLayer*)((TDXFMapsContainer*)Container)->DXFCollection->Layers->Items[0];
						if(layer)
						{
							Container->Background->Clear(((TRadarMapSettings*)RadarMapSettings)->DXFBackgroundColor);
							layer->Render(Container->Background, ((TRadarMapSettings*)RadarMapSettings)->DXFBackgroundColor,
								((TRadarMapSettings*)RadarMapSettings)->DXFColorInvertThreshold,
								TDoubleRect(Container->LatLonRect->LeftTop->RdX, Container->LatLonRect->LeftTop->RdY,
								Container->LatLonRect->RightBottom->RdX, Container->LatLonRect->RightBottom->RdY));
						}
					}*/
					BackgroundBmp->Draw((int)(Container->PositionRect.Left*(float)w+0.5),
						(int)(Container->PositionRect.Top*(float)h+0.5), Container->Background);
				}
				if(progress) progress->StepIt();
			}
		}
		__finally
		{
			BackgroundBmp->EndUpdate();
		}
	}
#endif
	if(image && Draw)
	{
		image->Bitmap->Assign(Draw);
	}
}
//---------------------------------------------------------------------------

Types::TRect __fastcall TMapsContainersCollection::GetBitmapRect()
{
	Types::TRect res;
	/*if(!Draw) GetBackgroundDraw();
	if(Draw) return Types::TRect(0, 0, Draw->Width, Draw->Height);
	else return Types::TRect(0, 0, 0, 0);*/

	if(Collection->Count==1) res=((TMapsContainer*)Collection->Items[0])->GetBitmapRect();
	else if(Collection->Count==0) res=Types::TRect(0, 0, 0, 0);
	else
	{
#ifndef OnlyOneMap
		Shell calculate the total BitmapRect on the sum of GetBitmapRect()'s of all Collections
#else
		res=Types::TRect(0, 0, 0, 0);
#endif
	}

	return res;
}
//---------------------------------------------------------------------------

TDoublePoint __fastcall TMapsContainersCollection::ScreenToViewportArea(Types::TPoint P)
{
	TDoublePoint res, vpac;
	Types::TRect mbr, bmr;
	double bmpw, bmph, w, h;
	int bmrw, bmrh;

	mbr=GetBitmapRect();
	bmpw=mbr.Width();
	bmph=mbr.Height();
	if(image && viewportarea && bmpw>0 && bmph>0)
	{
#ifdef TakeInAccountViewport32
		bmr=image->GetViewportRect();
#else
		bmr=image->GetBitmapRect();
#endif
		vpac=viewportarea->Center();
		bmrw=bmr.Width();
		bmrh=bmr.Height();
		w=viewportarea->Width();
		h=viewportarea->Height();
		/*P.x-=bmr.Left; //in BitmapRect coordinates relativily to LeftTop
		P.y-=bmr.Top;
		P.x-=(bmrw>>1); //in BitmapRect coordinates relativily to Center
		P.y-=(bmrh>>1);
		res.X=vpac.X+w*((double)P.x/(double)bmpw);
		res.Y=vpac.Y+h*((double)P.y/(double)bmph);*/
		res=DoublePoint(P.x, P.y);
		res.X-=(double)bmr.Left; //in BitmapRect coordinates relativily to LeftTop
		res.Y-=(double)bmr.Top;
		res.X-=(double)bmrw/2.; //in BitmapRect coordinates relativily to Center
		res.Y-=(double)bmrh/2.;
		res.X=vpac.X+w*((double)res.X/(double)bmpw);
		res.Y=vpac.Y+h*((double)res.Y/(double)bmph);
	}
	else res=DoublePoint(0, 0);

	return res;
}
//---------------------------------------------------------------------------

Types::TPoint __fastcall TMapsContainersCollection::ViewportAreaToScreen(TDoublePoint DP)
{
	Types::TPoint res;
	TDoublePoint vpac;
	Types::TRect mbr, bmr;
	double bmpw, bmph, w, h;
	int bmrw, bmrh;

	mbr=GetBitmapRect();
	bmpw=mbr.Width();
	bmph=mbr.Height();
	if(image && viewportarea && bmpw>0 && bmph>0)
	{
#ifdef TakeInAccountViewport32
		bmr=image->GetViewportRect();
#else
		bmr=image->GetBitmapRect();
#endif
		vpac=viewportarea->Center();
		bmrw=bmr.Width();
		bmrh=bmr.Height();
		w=viewportarea->Width();
		h=viewportarea->Height();
		DP.X-=vpac.X; //in ViewportArea coordinates relativily to Center
		DP.Y-=vpac.Y;
		DP.X=(double)bmrw/2.+(double)bmpw*DP.X/w;
		DP.Y=(double)bmrh/2.+(double)bmph*DP.Y/h;
		//DP.X=(double)(bmrw>>1)+(double)bmpw*DP.X/w;
		//DP.Y=(double)(bmrh>>1)+(double)bmph*DP.Y/h;
		if(DP.X>0) res.x=(int)(DP.X+0.5);
		else res.x=(int)(DP.X-0.5);
		if(DP.X>0) res.y=(int)(DP.Y+0.5);
		else res.y=(int)(DP.Y-0.5);
		//res.x=(int)((double)(bmrw>>1)+(double)bmpw*DP.X/w);
		//res.y=(int)((double)(bmrh>>1)+(double)bmph*DP.Y/h);
#ifndef TakeInAccountViewport32
		res.x+=bmr.Left;
		res.y+=bmr.Top;
#endif
	}
	else res=Types::TPoint(0, 0);

	return res;
}
//---------------------------------------------------------------------------

TDoublePoint __fastcall TMapsContainersCollection::CoordinatesRectToViewportArea(TDoublePoint DP)
{
	if(viewportarea && coordinatesrect.Width()>0 && coordinatesrect.Height()>0)
	{
		if(coordinatesrect.Left<coordinatesrect.Right)
			DP.X=(DP.X-coordinatesrect.Left)/coordinatesrect.Width();
		else DP.X=(DP.X-coordinatesrect.Right)/coordinatesrect.Width();
		if(coordinatesrect.Top<coordinatesrect.Bottom)
			DP.Y=(coordinatesrect.Bottom-DP.Y)/coordinatesrect.Height();
		else DP.Y=(coordinatesrect.Top-DP.Y)/coordinatesrect.Height();
	}
	else DP=DoublePoint(0, 0);

	return DP;
}
//---------------------------------------------------------------------------

TDoublePoint __fastcall TMapsContainersCollection::ViewportAreaToCoordinatesRect(TDoublePoint DP)
{
	if(viewportarea)
	{
		if(coordinatesrect.Left<coordinatesrect.Right)
			DP.X=coordinatesrect.Left+DP.X*coordinatesrect.Width();
		else DP.X=coordinatesrect.Right+DP.X*coordinatesrect.Width();
		if(coordinatesrect.Top<coordinatesrect.Bottom)
			DP.Y=coordinatesrect.Bottom-DP.Y*coordinatesrect.Height();
		else DP.Y=coordinatesrect.Top-DP.Y*coordinatesrect.Height();
		*((int*)DP.Temp)=*((int*)coordinatesrect.LeftTopTemp);
	}
	else DP=DoublePoint(0, 0);

	return DP;
}
//---------------------------------------------------------------------------

TDoublePoint __fastcall TMapsContainersCollection::ViewportAreaToCoordinatesRect(TDoubleRect pCoordinatesRect, TDoublePoint DP)
{
	if (viewportarea)
	{
		if (pCoordinatesRect.Left < pCoordinatesRect.Right)
			DP.X = pCoordinatesRect.Left + DP.X * pCoordinatesRect.Width();
		else DP.X = pCoordinatesRect.Right + DP.X * pCoordinatesRect.Width();
		if (pCoordinatesRect.Top < pCoordinatesRect.Bottom)
			DP.Y = pCoordinatesRect.Bottom - DP.Y * pCoordinatesRect.Height();
		else DP.Y = pCoordinatesRect.Top - DP.Y* pCoordinatesRect.Height();
		*((int*)DP.Temp)=*((int*)coordinatesrect.LeftTopTemp);
	}
	else DP = DoublePoint(0, 0);

	return DP;
}
//---------------------------------------------------------------------------

void __fastcall TMapsContainersCollection::ZoomUpdate()
{
	TMapsContainer *mc;
	TGPSCoordinatesRect *c1, *c2;
	int m, i;

	if(!ZoomUpdateInProgress)
	{
		ZoomUpdateInProgress=true;
		c1=new TGPSCoordinatesRect(false);
		c2=new TGPSCoordinatesRect(false);
		try
		{
			try
			{
				if(progress)
				{
					m=Collection->Count*3;
					for(i=0; i<Collection->Count; i++)
					{
						mc=(TMapsContainer*)Collection->Items[i];
						m+=mc->Count*2;
					}
					progress->Show(m, false);
				}
				for(i=0; i<Collection->Count; i++)
				{
					mc=(TMapsContainer*)Collection->Items[i];
					mc->ZoomUpdate();
					if(mc->LatLonRect->Valid)
					{
						if(i==0)
						{
							coordinatesrect=mc->CoordinatesRect;
							latlonrect->SetCorners(mc->LatLonRect->LeftTop, mc->LatLonRect->RightBottom);
						}
						else
						{
							c1->SetCorners(LatLonRect->LeftTop, mc->LatLonRect->LeftTop);
							c2->SetCorners(LatLonRect->RightBottom, mc->LatLonRect->RightBottom);
							if(c1->Width()>0 || c1->Height()>0 || c2->Width()>0 || c2->Height()>0)
								LatLonRect->SetCorners(c1->LeftTop, c2->RightBottom);
						}
					}
				}
			}
			catch(Exception &e) {}
		}
		__finally
		{
			delete c1;
			delete c2;
			if(progress) progress->Hide(false);
			if(Owner!=NULL)
			{
				// Only owner custom and tool objects are sencitive to maps coordinates
				Owner->SetObjectChanged(gotCustom);
				Owner->SetObjectChanged(gotTool);
				BackgroundUpdate();
			}/**/
			ZoomUpdateInProgress=false;
		}
		if(NeedReZooming)
		{
			NeedReZooming=false;
			ZoomUpdate();
		}
	}
	else
	{
		NeedReZooming=true;
	}
}
//---------------------------------------------------------------------------

void __fastcall TMapsContainersCollection::UpdateCoordinatesRect()
{
	TMapsContainer *mc;
	int i, k, j;
	double ww, hh; //,w, h;
	TGPSCoordinatesRect *c1, *c2;
	double minx, miny, maxx, maxy;
	char MinTemp[4], MaxTemp[4];
	int m;

	if(!AutomaticUpdate) return;

	c1=new TGPSCoordinatesRect(false);
	c2=new TGPSCoordinatesRect(false);
	minx=miny=maxx=maxy=0;
	try
	{
		try
		{
			ww=hh=0;
			k=0;
			if(!LatLonRect->Valid && CoordinatesRect.IsValid())
				LatLonRect->SetCorners(CoordinatesRect.LeftTop,	CoordinatesRect.RightBottom, false, CoordinateSystem);
			for(i=0; i<Collection->Count; i++)
			{
				mc=(TMapsContainer*)Collection->Items[i];
				c1->SetCorners(LatLonRect->LeftTop, mc->LatLonRect->LeftTop);
				c2->SetCorners(LatLonRect->RightBottom, mc->LatLonRect->RightBottom);
				if(!mc->LatLonRect->Valid && mc->CoordinatesRect.IsValid())
				{
					mc->LatLonRect->SetCorners(mc->CoordinatesRect.LeftTop, mc->CoordinatesRect.RightBottom, false, mc->CoordinateSystem);
				}
				if(mc->LatLonRect->Valid && (k==0 || (c1->Width()<(mc->LatLonRect->Width()+latlonrect->Width()) &&
					c1->Height()<(mc->LatLonRect->Height()+latlonrect->Height()))))
				{
					if(k==0)
					{
						//coordinatesrect=mc->CoordinatesRect;
						if(coordinatesrect.Left<coordinatesrect.Right)
						{
							minx=mc->CoordinatesRect.Left;
							maxx=mc->CoordinatesRect.Right;
						}
						else
						{
							minx=mc->CoordinatesRect.Right;
							maxx=mc->CoordinatesRect.Left;
						}
						if(mc->CoordinatesRect.Top<mc->CoordinatesRect.Bottom)
						{
							miny=mc->CoordinatesRect.Top;
							maxy=mc->CoordinatesRect.Bottom;
							*((int*)MinTemp)=*((int*)mc->CoordinatesRect.LeftTopTemp);
							*((int*)MaxTemp)=*((int*)mc->CoordinatesRect.RightBottomTemp);
						}
						else
						{
							miny=mc->CoordinatesRect.Bottom;
							maxy=mc->CoordinatesRect.Top;
							*((int*)MaxTemp)=*((int*)mc->CoordinatesRect.LeftTopTemp);
							*((int*)MinTemp)=*((int*)mc->CoordinatesRect.RightBottomTemp);
						}
						latlonrect->SetCorners(mc->LatLonRect->LeftTop, mc->LatLonRect->RightBottom);
						k++;
					}
					else
					{
						if(minx>mc->CoordinatesRect.Left) minx=mc->CoordinatesRect.Left;
						if(minx>mc->CoordinatesRect.Right) minx=mc->CoordinatesRect.Right;
						if(maxx<mc->CoordinatesRect.Left) maxx=mc->CoordinatesRect.Left;
						if(maxx<mc->CoordinatesRect.Right) maxx=mc->CoordinatesRect.Right;
						if(miny>mc->CoordinatesRect.Top) miny=mc->CoordinatesRect.Top;
						if(miny>mc->CoordinatesRect.Bottom) miny=mc->CoordinatesRect.Bottom;
						if(maxy<mc->CoordinatesRect.Top) maxy=mc->CoordinatesRect.Top;
						if(maxy<mc->CoordinatesRect.Bottom) maxy=mc->CoordinatesRect.Bottom;
						LatLonRect->SetCorners(c1->LeftTop, c2->RightBottom);
					}
					mc->PositionRect=TDoubleRect(0,0,1,1);
					/*if(mc->Background)
					{
						if(mc->LatLonRect->Width()!=0) w=(double)mc->Background->Width/mc->LatLonRect->Width();
						else w=mc->Background->Width;
						if(mc->LatLonRect->Height()!=0) h=(double)mc->Background->Height/mc->LatLonRect->Height();
						else h=mc->Background->Height;
					}
					if(w>ww) ww=w;
					if(h>hh) hh=h;*/
					if(mc->LatLonRect->Width()>ww) ww=mc->LatLonRect->Width();
					if(mc->LatLonRect->Height()>hh) hh=mc->LatLonRect->Height();
				}
				else mc->PositionRect=TDoubleRect(0,0,0,0);
			}
			if(ww==0 || hh==0) LatLonRect->Clear();
			coordinatesrect.Left=minx;
			coordinatesrect.Bottom=maxy;//ScalingFactorY;
			coordinatesrect.Right=maxx;
			coordinatesrect.Top=miny;
			*((int*)coordinatesrect.LeftTopTemp)=*((int*)MinTemp);
			*((int*)coordinatesrect.RightBottomTemp)=*((int*)MaxTemp);
			for(i=0; i<Collection->Count; i++)
			{
				mc=(TMapsContainer*)Collection->Items[i];
				if(mc->PositionRect.Width()>0.5 && LatLonRect->Width()>0 && LatLonRect->Height()>0)
				{
					mc->PositionRect.Left=(mc->LatLonRect->LeftTop->Longitude-LatLonRect->LeftTop->Longitude)/LatLonRect->Width();
					mc->PositionRect.Right=(mc->LatLonRect->RightBottom->Longitude-LatLonRect->LeftTop->Longitude)/LatLonRect->Width();
					mc->PositionRect.Top=(LatLonRect->LeftTop->Latitude-mc->LatLonRect->LeftTop->Latitude)/LatLonRect->Height();
					mc->PositionRect.Bottom=(LatLonRect->LeftTop->Latitude-mc->LatLonRect->RightBottom->Latitude)/LatLonRect->Height();
				}
			}
#ifndef OnlyOneMap
/*			w=ww*LatLonRect->Width()+0.5;
			h=hh*LatLonRect->Height()+0.5;
			if(BackgroundBmp!=NULL) BackgroundBmp->SetSize((int)w, (int)h);
			UtilityBmp->SetSize((int)w, (int)h);
			InfoBmp->SetSize((int)w, (int)h);
			realrect->Right=(int)w;
			realrect->Bottom=(int)h;*/
#endif
		}
		catch(Exception &e) {}
	}
	__finally
	{
		delete c1;
		delete c2;
	}
}
//---------------------------------------------------------------------------

void __fastcall TMapsContainersCollection::Update()
{
	TMapsContainer *mc;
	int i, m;

	if(!AutomaticUpdate) return;

	try
	{
		try
		{
			if(progress)
			{
				m=Collection->Count*3;
				for(i=0; i<Collection->Count; i++)
				{
					mc=(TMapsContainer*)Collection->Items[i];
					m+=mc->Count*2;
				}
				progress->Show(m, false);
			}

			UpdateCoordinatesRect();
			BackgroundUpdate();
			UtilityUpdate();
			InfoUpdate();
		}
		catch(Exception &e) {}
	}
	__finally
	{
		if(progress) progress->Hide(false);
		if(Owner!=NULL) Owner->SetObjectChanged(gotCustom); // Only owner custom objects depends on maps coordinates
	}
}
//---------------------------------------------------------------------------

void __fastcall TMapsContainersCollection::UtilityLayerPaint(TObject *Sender, TBitmap32 *Bmp)
{
	Types::TRect src, dst, bmp, vpt;

	if(UtilityDraw!=NULL)
	{
		bmp=image->GetBitmapRect();
		vpt=image->GetViewportRect();
		if(bmp.Width()>vpt.Width())
		{
			src.left=abs((double)bmp.left);
			src.right=src.left+vpt.Width();

			src.left=(int)((float)UtilityDraw->Width*(float)src.left/(float)bmp.Width());
			src.right=(int)((float)UtilityDraw->Width*(float)src.right/(float)bmp.Width());
			dst.left=vpt.left;
			dst.right=vpt.right;
		}
		else
		{
			src.left=0;
			src.right=UtilityDraw->Width;
			dst.left=(vpt.Width()>>1)-(bmp.Width()>>1);
			dst.right=dst.left+bmp.Width();
		}
		if(bmp.Height()>vpt.Height())
		{
			src.top=abs((double)bmp.top);
			src.bottom=src.top+vpt.Height();

			src.top=(int)((float)UtilityDraw->Height*(float)src.top/(float)bmp.Height());
			src.bottom=(int)((float)UtilityDraw->Height*(float)src.bottom/(float)bmp.Height());
			dst.top=vpt.top;
			dst.bottom=vpt.bottom;
		}
		else
		{
			src.top=0;
			src.bottom=UtilityDraw->Height;
			dst.top=(vpt.Height()>>1)-(bmp.Height()>>1);
			dst.bottom=dst.top+bmp.Height();
		}
		Bmp->Draw(dst, src, UtilityDraw);
	}
}
//---------------------------------------------------------------------------

void __fastcall TMapsContainersCollection::InfoLayerPaint(TObject *Sender, TBitmap32 *Bmp)
{
	Types::TRect src, dst, bmp, vpt;

	if(InfoDraw!=NULL)
	{
		bmp=image->GetBitmapRect();
		vpt=image->GetViewportRect();
		if(bmp.Width()>vpt.Width())
		{
			src.left=abs((double)bmp.left);
			src.right=src.left+vpt.Width();

			src.left=(int)((float)InfoDraw->Width*(float)src.left/(float)bmp.Width());
			src.right=(int)((float)InfoDraw->Width*(float)src.right/(float)bmp.Width());
			dst.left=vpt.left;
			dst.right=vpt.right;
		}
		else
		{
			src.left=0;
			src.right=InfoDraw->Width;
			dst.left=(vpt.Width()>>1)-(bmp.Width()>>1);
			dst.right=dst.left+bmp.Width();
		}
		if(bmp.Height()>vpt.Height())
		{
			src.top=abs((double)bmp.top);
			src.bottom=src.top+vpt.Height();

			src.top=(int)((float)InfoDraw->Height*(float)src.top/(float)bmp.Height());
			src.bottom=(int)((float)InfoDraw->Height*(float)src.bottom/(float)bmp.Height());
			dst.top=vpt.top;
			dst.bottom=vpt.bottom;
		}
		else
		{
			src.top=0;
			src.bottom=InfoDraw->Height;
			dst.top=(vpt.Height()>>1)-(bmp.Height()>>1);
			dst.bottom=dst.top+bmp.Height();
		}
		Bmp->Draw(dst, src, InfoDraw);
	}
}
//---------------------------------------------------------------------------

void __fastcall TMapsContainersCollection::GetLayers(TCheckBoxTreeView *TreeView)
{
	TMapsContainer *Container;
	TTreeNode *node;
	int i;

	TreeView->Items->Clear();
	for(i=0; i<Collection->Count; i++)
	{
		Container=(TMapsContainer*)Collection->Items[i];
		if(Container!=NULL)
		{
			node=TreeView->Items->Add(NULL, Container->Klicnummer);
			TreeView->SetChecking(node, Container->Visible);
			node->Data=(void*)Container;
			if(TreeView->Images)
			{
				node->ImageIndex=0;
				node->SelectedIndex=0;
			}
			Container->GetLayers(TreeView, node);
			node->Expand(false);
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TMapsContainersCollection::SetLayers(TCheckBoxTreeView *TreeView)
{
	TTreeNode *node;
	TMapsContainer *Container;

	node=TreeView->Items->GetFirstNode();
	AutomaticUpdate=false;
	try
	{
		try
		{
			while(node!=NULL)
			{
				if(node->getFirstChild()!=NULL && node->Data!=NULL)
				{
					Container=(TMapsContainer*)node->Data;
					Container->SetLayers(TreeView, node);
				}
				do
				{
					node=node->GetNext();
				} while(node!=NULL && node->getFirstChild()==NULL);
			}
		}
		catch (Exception &e) {}
	}
	__finally
	{
		AutomaticUpdate=true;
		Update();
	}
}
//---------------------------------------------------------------------------

void __fastcall TMapsContainersCollection::GetMapFoldersContent(TTreeView *TreeView, int ItemImageIndex, AnsiString Extension)
{
	TMapsContainer *Container;
	TTreeNode *node, *item;
	AnsiString path, S, fn;
	WIN32_FIND_DATA FileData;
	HANDLE hSearch;
	int i;
	char *data;

	for(i=0; i<TreeView->Items->Count; i++)
		if(TreeView->Items->Item[i] && TreeView->Items->Item[i]->Data!=NULL)
			TFilesForm::EmptyNodeData(TreeView->Items->Item[i]);
	TreeView->Items->Clear();
	for(i=0; i<Collection->Count; i++)
	{
		Container=(TMapsContainer*)Collection->Items[i];
		if(Container!=NULL)
		{
			node=TreeView->Items->Add(NULL, Container->Klicnummer);
			path=ExtractFilePath(Container->FileName);
			hSearch=FindFirstFile(((AnsiString)(path+"*"+Container->Klicnummer+"*"+Extension)).c_str(), &FileData);
			if(hSearch!=INVALID_HANDLE_VALUE)
			{
				do
				{
					S=FileData.cFileName;
					if(S!="." && S!="..")
					{
						if(FileData.dwFileAttributes!=FILE_ATTRIBUTE_DIRECTORY)
						{
							fn=path+S;
							item=TreeView->Items->AddChild(node, S);
							item->ImageIndex=ItemImageIndex;
							item->SelectedIndex=ItemImageIndex;
							data=new char[fn.Length()+sizeof(unsigned short int)];
							*((unsigned short int*)data)=(unsigned short int)fn.Length();
							memcpy((char*)(data+sizeof(unsigned short int)), fn.c_str(), fn.Length());
							item->Data=(void*)data;
						}
					}
				} while(FindNextFile(hSearch, &FileData));
				FindClose(hSearch);
			}
			node->Expand(false);
		}
	}

}
//---------------------------------------------------------------------------

void __fastcall TMapsContainersCollection::writeCoordinateSystem(TCoordinateSystem value)
{
	TMapsContainer *Container;
	int i;

	//coordinatesystem=value;
	for(i=0; i<Collection->Count; i++)
	{
		Container=(TMapsContainer*)Collection->Items[i];
		if(Container)// && Container->MapType==mtEmpty)
		{
			Container->CoordinateSystem=value;
		}
	}
}
//---------------------------------------------------------------------------

bool __fastcall TMapsContainersCollection::IsContains(TMapType aMapType)
{
	TMapsContainer *Container;
	int i;
	bool res=false;

	//coordinatesystem=value;
	for(i=0; i<Collection->Count; i++)
	{
		Container=(TMapsContainer*)Collection->Items[i];
		res=(Container && Container->MapType & aMapType);
		if(res) break;
	}

	return res;
}
//---------------------------------------------------------------------------

TMapsContainer* TMapsContainersCollection::GetFirstObject(TMapType aMapType)
{
	TMapsContainer *Container, *res=NULL;
	int i;


	//coordinatesystem=value;
	for(i=0; i<Collection->Count; i++)
	{
		Container=(TMapsContainer*)Collection->Items[i];
		if(Container && Container->MapType & aMapType)
		{
			res=Container;
			break;
		}
	}

	return res;
}
//---------------------------------------------------------------------------

