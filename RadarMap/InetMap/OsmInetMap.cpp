// -----------------------------------------------------------------------------
// OsmInetMap.cpp
// -----------------------------------------------------------------------------

#pragma hdrstop

#include "OsmInetMap.h"
#include "Manager.h"
#include <math.h>
#include <sys\stat.h>
#include "GR32_Text.hpp"
#include <Graphics.hpp>

#pragma package(smart_init)

#define MAX_READ_ATTEMPT 3
#define TILE_PIXEL_SIZE 256

// -------- long2tilex ---------------------------------------------------------
// http://wiki.openstreetmap.org/wiki/Slippy_map_tilenames
double long2tilex(double lon, int z) {
	return (lon + 180.0) / 360.0 * pow(2.0, z);
//	return(int)(floor((lon + 180.0) / 360.0 * pow(2.0, z)));
}

// -------- lat2tiley ----------------------------------------------------------
double lat2tiley(double lat, int z) {
	return (1.0 - log(tan(lat * M_PI / 180.0) + 1.0 / cos(lat * M_PI / 180.0)) / M_PI) / 2.0 * pow(2.0, z);
//	return(int)(floor((1.0 - log(tan(lat * M_PI / 180.0) + 1.0 / cos(lat * M_PI / 180.0)) / M_PI) / 2.0 * pow(2.0, z)));
}

// -------- tilex2long ---------------------------------------------------------
double tilex2long(int x, int z)
{
	return x / pow(2.0, z) * 360.0 - 180;
}

// -------- tiley2lat ----------------------------------------------------------
double tiley2lat(int y, int z)
{
	double n = M_PI - 2.0 * M_PI * y / pow(2.0, z);
	return 180.0 / M_PI * atan(0.5 * (exp(n) - exp(-n)));
}

// -----------------------------------------------------------------------------
// TOsmInetMap
// -----------------------------------------------------------------------------

// -------- CalcTilesCount -----------------------------------------------------
void __fastcall TOsmMapLoader::CalcTilesCount() {
	int half_width = (MaxBitmapWidth - TILE_PIXEL_SIZE) / 2;
	int half_height = (MaxBitmapHeight - TILE_PIXEL_SIZE) / 2;
	int count = half_width / TILE_PIXEL_SIZE;

	this->mOffsetWidth = TILE_PIXEL_SIZE - half_width % TILE_PIXEL_SIZE;
	if (this->mOffsetWidth < TILE_PIXEL_SIZE) {
		count++;
	}
	else this->mOffsetWidth = 0;

	this->mMapColumns = 2 * count + 1;

	count = half_height / TILE_PIXEL_SIZE;
	this->mOffsetHeight = TILE_PIXEL_SIZE - half_height % TILE_PIXEL_SIZE;
	if (this->mOffsetHeight < TILE_PIXEL_SIZE) {
		count++;
	}
	else this->mOffsetHeight = 0;

	this->mMapLines = 2 * count + 1;

	//tile center and coordinate map center compensation
	double x = long2tilex(this->mOsmMap->MapCenterLongitude, this->mOsmMap->MapImageZoom);
	double y = lat2tiley(this->mOsmMap->MapCenterLatitude, this->mOsmMap->MapImageZoom);
	this->mOffsetHeight -= (int) (floor(TILE_PIXEL_SIZE * (0.5 - (y - floor(y)))));
	this->mOffsetWidth -= (int) (floor(TILE_PIXEL_SIZE * (0.5 - (x - floor(x)))));
	if(this->mOffsetHeight<0) {
		this->mOffsetHeight += TILE_PIXEL_SIZE;
		this->mMapLines += 2;
	}
	else if(this->mOffsetHeight>TILE_PIXEL_SIZE) {
		//this->mOffsetHeight -= TILE_PIXEL_SIZE;
		//this->mMapLines += 2;
	}
	if(this->mOffsetWidth<0) {
		this->mOffsetWidth+=TILE_PIXEL_SIZE;
		this->mMapColumns += 2;
	}
	else if(this->mOffsetWidth>TILE_PIXEL_SIZE) {


		//this->mOffsetWidth -= TILE_PIXEL_SIZE;
		//this->mMapColumns += 2;
	}
}
// -----------------------------------------------------------------------------

// -------- Constructor / Destructor -------------------------------------------
__fastcall TOsmInetMap::TOsmInetMap(TMyObject *AOwner, void* ASettings, TLockedDoubleRect *AViewportArea) : TInetMapBase(AOwner, ASettings,
	AViewportArea) {
	this->maptype = mtOsmInet;
	this->mMapMatrix = NULL;
	this->copyrights=true;
	this->filename = _OpenStreetMapHTTP;
	this->copyrightsstr="� OpenStreetMap contributors";
	this->copyrightslink="http://www.openstreetmap.org/copyright";
}
// -----------------------------------------------------------------------------

// -------- MapViewTypeToImagerySet --------------------------------------------
UnicodeString __fastcall TOsmInetMap::MapViewTypeToImagerySet() {
	UnicodeString imagery_set = "mapnik";

	return imagery_set;
}
// -----------------------------------------------------------------------------

// -------- ParseMetaData ------------------------------------------------------
bool __fastcall TOsmInetMap::ParseMetaData(UnicodeString pXmlData) {
	bool result = false;

    if (this->WaitForMapCenter) {
		GetCoordinateFromGps();
	}
	this->LoadStatusCode = "200";
	this->LoadStatusDescription = "Ok";
	this->MapImageWidth = MaxBitmapWidth; // 256 * MAP_COLUMNS;
	this->MapImageHeight = MaxBitmapHeight; // 256 * MAP_LINES;

	double pixel_count = TILE_PIXEL_SIZE * pow(2.0, this->MapImageZoom);
	double degree_per_pixel = TILE_PIXEL_SIZE / pixel_count;
	double degree_half_width = (degree_per_pixel * this->MapImageWidth) / 2.0;

	this->WestLongitude = this->MapCenterLongitude - degree_half_width;
	this->EastLongitude = this->MapCenterLongitude + degree_half_width;

	degree_per_pixel = 170.0 / pixel_count; //	degree_per_pixel = 170.0 / pixel_count;
	double degree_half_height = (degree_per_pixel * this->MapImageHeight) / 2.0;

	this->SouthLatitude = this->MapCenterLatitude - degree_half_height;
	this->NorthLatitude = this->MapCenterLatitude + degree_half_height;

	this->CalcScale();
	result = true;

	return result;
}
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// TOsmMapLoader
// -----------------------------------------------------------------------------

// -------- TOsmMapLoader ------------------------------------------------------
__fastcall TOsmMapLoader::TOsmMapLoader(TInetMapLoader *pMyThread, TObject *pManager, TToolProgressBar *pProgressBar) {

	this->mMyThread = pMyThread;
	this->mManager = pManager;
	this->mOsmMap = NULL;
	this->mProgressBar = pProgressBar;
	this->mCacheDir = "TileCache";
	if(pManager) {
		TRadarMapManager *mngr = dynamic_cast<TRadarMapManager *>(this->mManager);

		this->mCacheDir = mngr->Settings->RadarMapDirectory + "\\"+this->mCacheDir;
	}
	this->mMapLines = 5;
	this->mMapColumns = 7;
	this->mOffsetWidth = mOffsetHeight = 0;
	this->LoadingTileImegeStr = "PngImage_LoadingTile";

	//this->CalcTilesCount();
	this->mTileThreads.reserve(MAX_LOAD_THREADS);

	Randomize();
}
// -----------------------------------------------------------------------------

__fastcall TOsmMapLoader::~TOsmMapLoader(){
	FreeTileThreads();
//	if(mManager && ((TRadarMapManager*)mManager)->MapObjects)
//		((TRadarMapManager*)mManager)->MapObjects->PngContainer->DelPng(this->LoadingTileImegeStr);
}

// -------- LoadMap ------------------------------------------------------------
TBitmap32 *__fastcall TOsmMapLoader::LoadMap(TInetMapBase *pOsmMap) {
    this->CreateCacheDirectory();

	if (mProgressBar) {
		mProgressBar->Show(35, true);
	}

	this->mOsmMap = dynamic_cast<TOsmInetMap*>(pOsmMap);

	this->CalcTilesCount();

	int tiles_to_load = this->mMapLines * this->mMapColumns;
	MapTileInfo *map_matrix = new MapTileInfo[tiles_to_load];
	TBitmap32 *image = new TBitmap32();

	this->mOsmMap->MapMatrix = new MapTileInfo[tiles_to_load];

    this->ClearMapImage(image);

	// ---- get tiles from current map or cache --------------------------------
	this->FillTileMatrix(map_matrix);
	this->CorrectMapCoordinate();
	this->mOsmMap->FillMapBoundaries();

	tiles_to_load -= this->GetTilesFromMapAndChache(this->mOsmMap->mMapMatrix, map_matrix, image);

	// ---- get tiles from server ----------------------------------------------
	if ((tiles_to_load > 0) && (this->mMyThread->CheckTerminated() != true)) {
		int threads_count = 0;
		time_t updt_time = time(NULL);
		time_t crnt_time = time(NULL);

		pOsmMap->FillMapImage(image);
		pOsmMap->Update();

		while (true) {

			if (this->mMyThread->CheckTerminated()) {
				break;
			}

			this->StartLoadTilesFromServer(map_matrix);

			threads_count = this->FillThreadHanlesArray();

			if (0 == threads_count) {
				break;
			}

			DWORD result = WaitForMultipleObjects(threads_count, this->mThreadHandles, false, 250);

			if (WAIT_TIMEOUT == result) {
				continue;
			}
			else if ((WAIT_OBJECT_0 <= result) && (result < WAIT_OBJECT_0 + threads_count)) {
				int thread_idx = result - WAIT_OBJECT_0;
				this->EndLoadingThreadProcessing(this->mThreadHandles[thread_idx], image);

//				Sleep(100); // TODO: Dbug

				crnt_time = time(NULL);
				if (crnt_time - updt_time > 5) {
					pOsmMap->FillMapImage(image);
					pOsmMap->Update();
					updt_time = crnt_time;
				}
			}
			else {
				//volatile long int ErrorCode = GetLastError();
			}
		}

		if (this->mMyThread->CheckTerminated() != true) {
			this->CopyTileMatrix(map_matrix, this->mOsmMap->mMapMatrix);
		}
	}

	// ---- wait canceling all thread ------------------------------------------
	if (this->mMyThread->CheckTerminated()) {
		this->StopAllThread();

		delete image;
		image = NULL;
	}

	if (this->mProgressBar) {
		this->mProgressBar->Hide(false);
	}
	delete[] map_matrix;

#ifdef _TileGrid_DEBUG
	image->SaveToFile(this->mCacheDir+"\\000.bmp");
#endif
	return image;
}
// -----------------------------------------------------------------------------

// -------- CorrectMapCoordinate -----------------------------------------------
void __fastcall TOsmMapLoader::CorrectMapCoordinate() {
	MapTileInfo center_tile_info = this->GetTileInfoAtCenter();

	double tile_lon = tilex2long(center_tile_info.xOsmTile, this->mOsmMap->MapImageZoom);
	double tile_lat = tiley2lat(center_tile_info.yOsmTile, this->mOsmMap->MapImageZoom);
	double right_lon = tilex2long(center_tile_info.xOsmTile + 1, this->mOsmMap->MapImageZoom);
	double top_lat = tiley2lat(center_tile_info.yOsmTile - 1, this->mOsmMap->MapImageZoom);

	double tile_height_degree = top_lat - tile_lat;
	double tile_width_degree = right_lon - tile_lon;

	double degree_per_pixel = tile_width_degree / TILE_PIXEL_SIZE;

	this->mOsmMap->WestLongitude = this->mOsmMap->MapCenterLongitude - degree_per_pixel * (double)this->mOsmMap->MapImageWidth / 2.;
	this->mOsmMap->EastLongitude = this->mOsmMap->WestLongitude + degree_per_pixel * this->mOsmMap->MapImageWidth;

	degree_per_pixel = tile_height_degree / TILE_PIXEL_SIZE;

	this->mOsmMap->SouthLatitude = this->mOsmMap->MapCenterLatitude - degree_per_pixel * (double)this->mOsmMap->MapImageHeight / 2.;
	this->mOsmMap->NorthLatitude = this->mOsmMap->SouthLatitude + degree_per_pixel * this->mOsmMap->MapImageHeight;

	this->mOsmMap->CalcScale();

	this->mOsmMap->ViewportArea->SetCenter(0.5, 0.5);
}
// -----------------------------------------------------------------------------

// -------- GetTileInfoAtCenter ------------------------------------------------
MapTileInfo __fastcall TOsmMapLoader::GetTileInfoAtCenter() {
	MapTileInfo result;

	result.tileState = mtsToLoad;
	result.xOsmTile = (int) floor(long2tilex(StrToFloat(this->mOsmMap->MapCenterLongitude), this->mOsmMap->MapImageZoom));
	result.yOsmTile = (int) floor(lat2tiley(StrToFloat(this->mOsmMap->MapCenterLatitude), this->mOsmMap->MapImageZoom));

	return result;
}
// -----------------------------------------------------------------------------

// -------- ClearMapImage -----------------------------------------------------
void __fastcall TOsmMapLoader::ClearMapImage(TBitmap32 *pImage) {
	TBitmap32 *empty_image = new TBitmap32();

	empty_image->BeginUpdate();
	try
	{
		TBitmap32 *tmp;

		empty_image->SetSize(TILE_PIXEL_SIZE, TILE_PIXEL_SIZE);
		empty_image->Clear(Color32(0, 0, 0, 0));
		if(mManager && ((TRadarMapManager*)mManager)->MapObjects)
		{
			//
			//empty_image->
			tmp = ((TRadarMapManager*)mManager)->MapObjects->PngContainer->AddPng(this->LoadingTileImegeStr);
			empty_image->Draw(0, 0, tmp);
		}
		else
		{
			int tw, th;
			AnsiString str = "Loading ...";
			//empty_image->Clear(clAqua32);
			empty_image->Font->Height = 20;
			empty_image->Font->Style = TFontStyles() << fsBold << fsItalic;
			// empty_image->RenderText(80, 118, "Loading ...", 2, clDimGray32);
			tw = (empty_image->Width - empty_image->TextWidth(str)) >> 1;
			th = (empty_image->Height - empty_image->TextHeight(str)) >> 1;
			empty_image->RenderText(tw, th, str, 2, clGray32);
		}
		// empty_image->SaveToFile("TileCache\\empty_image.bmp");
	}
	__finally
	{
		empty_image->EndUpdate();
	}
	//pImage->SetSize(256 * this->mMapColumns, 256 * this->mMapLines);
	pImage->SetSize(MaxBitmapWidth, MaxBitmapHeight);

	for (int line = 0; line < this->mMapLines; line++) {
		for (int col = 0; col < this->mMapColumns; col++) {
			this->ImageTileToMap(empty_image, line, col, pImage);
		}
	}

	delete empty_image;
}
// -----------------------------------------------------------------------------

// -------- GetTileRequest -----------------------------------------------------
UnicodeString __fastcall TOsmMapLoader::GetTileRequest(MapTileInfo *pTileInfo) {
	int prefix = Random(3);
	char c_prefix = ('a' + prefix);
	UnicodeString s_prefix = UnicodeString(c_prefix);

	UnicodeString result = UnicodeString::Format(L"http://%s.tile.osm.org/%d/%d/%d.png",
		ARRAYOFCONST((s_prefix, this->mOsmMap->MapImageZoom, pTileInfo->xOsmTile, pTileInfo->yOsmTile)));

	return result;
}
// -----------------------------------------------------------------------------

// -------- GetTileFileName ----------------------------------------------------
UnicodeString __fastcall TOsmMapLoader::GetTileFileName(MapTileInfo *pTileInfo) {
	UnicodeString result = UnicodeString::Format(L"o-temp_%d_%d_%d.png",
		ARRAYOFCONST((this->mOsmMap->MapImageZoom, pTileInfo->xOsmTile, pTileInfo->yOsmTile)));

	return result;
}
// -----------------------------------------------------------------------------

// -------- GetTileFileNameInCache ---------------------------------------------
UnicodeString __fastcall TOsmMapLoader::GetTileFileNameInCache(MapTileInfo *pTileInfo) {
	UnicodeString result = UnicodeString::Format(L"%s\\%s", ARRAYOFCONST((this->mCacheDir, this->GetTileFileName(pTileInfo))));

	return result;
}
// -----------------------------------------------------------------------------

// -------- GetTileFileNameInTempCache -----------------------------------------
UnicodeString __fastcall TOsmMapLoader::GetTileFileNameInTempCache(MapTileInfo *pTileInfo) {
	UnicodeString result = UnicodeString::Format(L"%s\\%s", ARRAYOFCONST((this->GetTempCacheDirName(), this->GetTileFileName(pTileInfo))));

	return result;
}
// -----------------------------------------------------------------------------

// -------- getTempCacheDirName ------------------------------------------------
UnicodeString __fastcall TOsmMapLoader::GetTempCacheDirName() {
	UnicodeString result = this->mCacheDir + "\\temp";

	return result;
}
// -----------------------------------------------------------------------------

// -------- GetTilesFromMapAndChache -------------------------------------------
int __fastcall TOsmMapLoader::GetTilesFromMapAndChache(MapTileInfo *pSrcMatrix, MapTileInfo *pMatrix, TBitmap32 *pImage) {
	MapTileInfo *tile_info = pMatrix;
	MapTileInfo *src_tile = NULL;
	bool is_terminated = false;
	int ready_count = 0;

	for (int line = 0; (line < this->mMapLines) && (is_terminated != true); line++) {

		for (int col = 0; col < this->mMapColumns; col++, tile_info++) {

			if (this->mMyThread->CheckTerminated()) {
				is_terminated = true;
				break;
			}

#ifdef _TileGrid_DEBUG
			src_tile = this->FindTileInCurrentImageOrCache(tile_info, pSrcMatrix);
#else
			src_tile = NULL;
#endif

			if (src_tile /* && (src_tile->errorCounter < MAX_READ_ATTEMPT)*/) {
				if (src_tile->pngImage) {
					this->TransferImageTile(src_tile, tile_info);
				}
				else {
					this->LoadPngTileFromCashe(tile_info);
				}

				if (mtsLoadError == tile_info->tileState) {
					tile_info->tileState = mtsToLoad;
				}
				else {
					this->ImageTileToMap(tile_info, pImage);
					tile_info->tileState = mtsReady;
					ready_count++;

					if (this->mProgressBar) {
						this->mProgressBar->Position++;
					}
				}
			}
			else if ((tile_info->xOsmTile < 0) || (tile_info->yOsmTile < 0)) {
				tile_info->tileState = mtsReady;
				ready_count++;
				this->EmptyPngToTile(tile_info);
				this->ImageTileToMap(tile_info, pImage);
			}
		}
	}
	return ready_count;
}
// -----------------------------------------------------------------------------

// -------- StartLoadTilesFromServer -------------------------------------------
void __fastcall TOsmMapLoader::StartLoadTilesFromServer(MapTileInfo *pMatrix) {
	MapTileInfo *tile_info = pMatrix;
	TMapTileLoader *thread = NULL;
	bool is_terminated = false;

	for (int line = 0; (line < this->mMapLines) && (false == is_terminated); line++) {

		for (int col = 0; col < this->mMapColumns; col++, tile_info++) {

			if (this->mMyThread->CheckTerminated() || (this->mTileThreads.size() >= MAX_LOAD_THREADS)) {
				is_terminated = true;
				break;
			}

			if (mtsToLoad == tile_info->tileState) {
				tile_info->tileState = mtsLoading;

				thread = new TMapTileLoader(this->mManager, this->mProgressBar, tile_info, this->GetTileRequest(tile_info),
					this->GetTileFileNameInCache(tile_info), this->GetTileFileNameInTempCache(tile_info));

				this->mTileThreads.push_back(thread);
			}
		}
	}
}
// -----------------------------------------------------------------------------

// -------- FillThreadHanlesArray ----------------------------------------------
int __fastcall TOsmMapLoader::FillThreadHanlesArray() {
	int threads_count = 0;
	std::vector<TMapTileLoader *>::iterator it;
	TMapTileLoader *thread = NULL;

	for (it = this->mTileThreads.begin(); it != this->mTileThreads.end(); it++) {
		thread = *it;

		if (thread) {
			this->mThreadHandles[threads_count] = (void *)(thread->Handle);
			threads_count++;
		}
	}

	return threads_count;
}
// -----------------------------------------------------------------------------

// -------- EndLoadingThreadProcessing -----------------------------------------
void __fastcall TOsmMapLoader::EndLoadingThreadProcessing(HANDLE pEndedThread, TBitmap32 *pImage) {
	std::vector<TMapTileLoader *>::iterator it;
	TMapTileLoader *thread = NULL;
	MapTileInfo *tile_info = NULL;

	for (it = this->mTileThreads.begin(); it != this->mTileThreads.end(); it++) {
		thread = *it;

		if (thread && (pEndedThread == (void *)thread->Handle)) {
			tile_info = thread->MapTile;

			switch(tile_info->tileState) {
			case mtsLoadedInCache:
				this->LoadPngTileFromCashe(tile_info);

				if (mtsLoadError != tile_info->tileState) {
					this->ImageTileToMap(tile_info, pImage);
					tile_info->tileState = mtsReady;

					if (this->mProgressBar) {
						this->mProgressBar->Position++;
					}
				}
				else {
					tile_info->tileState = mtsToLoad;
				}
				break;

			case mtsLoadError:
				if (tile_info->errorCounter < MAX_READ_ATTEMPT) {
					tile_info->tileState = mtsToLoad;
					tile_info->errorCounter++;
				} else {
					tile_info->tileState = mtsReady;
					this->ErrorPngToTile(tile_info);
				}
				break;

			default:
				break;
			}

			delete thread;
			this->mTileThreads.erase(it);
			break;
		}
	}
}
// -----------------------------------------------------------------------------

// -------- StopAllThread ------------------------------------------------------
void __fastcall TOsmMapLoader::StopAllThread() {
	std::vector<TMapTileLoader *>::iterator it;
	TMapTileLoader *thread = NULL;

	for (it = this->mTileThreads.begin(); it != this->mTileThreads.end(); it++) {
		thread = *it;

		if (thread) {
			if (!thread->SuccessfullyTerminated) {
				thread->ForceTerminate();
				thread->WaitFor();
				thread->MapTile->tileState = mtsCanceled;
			}
		}
	}
}
// -----------------------------------------------------------------------------

// -------- CopyTileMatrix -----------------------------------------------------
void __fastcall TOsmMapLoader::CopyTileMatrix(MapTileInfo *pSrcMatrix, MapTileInfo *pTrgMatrix) {
	MapTileInfo *src_ptr = pSrcMatrix;
	MapTileInfo *trg_ptr = pTrgMatrix;

	for (int i = 0; i < this->mMapLines; i++) {
		for (int j = 0; j < this->mMapColumns; j++) {
			*(trg_ptr++) = *(src_ptr++);
		}
	}
}
// -----------------------------------------------------------------------------

// -------- FillTileMatrix -----------------------------------------------------
void __fastcall TOsmMapLoader::FillTileMatrix(MapTileInfo *pMatrix) {
	int center_line = this->mMapLines / 2;
	int center_col = this->mMapColumns / 2;
	int zoom = this->mOsmMap->MapImageZoom;
	MapTileInfo *tile_info = pMatrix;
	MapTileInfo center_tile_info = this->GetTileInfoAtCenter();

	for (int line = 0; line < this->mMapLines; line++) {
		for (int col = 0; col < this->mMapColumns; col++, tile_info++) {
			tile_info->tileState = mtsToLoad;
			tile_info->xOsmTile = col - center_col + center_tile_info.xOsmTile;
			tile_info->yOsmTile = line - center_line + center_tile_info.yOsmTile;
			tile_info->imageTileLine = line;
			tile_info->imageTileCol = col;
			tile_info->errorCounter = 0;
			tile_info->lon = tilex2long(tile_info->xOsmTile, zoom);
			tile_info->lat = tiley2lat(tile_info->yOsmTile, zoom);
		}
	}
}
// -----------------------------------------------------------------------------

// -------- LoadPngTileFromCashe -----------------------------------------------
void __fastcall TOsmMapLoader::LoadPngTileFromCashe(MapTileInfo *pTileInfo) {
	TPngImage *png = new TPngImage();

	try {
		png->LoadFromFile(this->GetTileFileNameInCache(pTileInfo));
	}
	catch(...) {
		pTileInfo->tileState = mtsLoadError;
		delete png;
		png = NULL;
	}

	if (pTileInfo->pngImage) {
		delete pTileInfo->pngImage;
	}
	pTileInfo->pngImage = png;
}
// -----------------------------------------------------------------------------

// -------- EmptyPngToTile -----------------------------------------------------
void __fastcall TOsmMapLoader::EmptyPngToTile(MapTileInfo *pTileInfo) {
	TPngImage *png = new TPngImage();

	png->LoadFromResourceName((int)HInstance, "PngImage_EmptyTile");

	if (pTileInfo->pngImage) {
		delete pTileInfo->pngImage;
	}
	pTileInfo->pngImage = png;

}
// -----------------------------------------------------------------------------

// -------- ErrorPngToTile -----------------------------------------------------
void __fastcall TOsmMapLoader::ErrorPngToTile(MapTileInfo *pTileInfo) {
	this->EmptyPngToTile(pTileInfo);
}
// -----------------------------------------------------------------------------

// -------- ImageTileToMap -----------------------------------------------------
void __fastcall TOsmMapLoader::ImageTileToMap(MapTileInfo *pTileInfo, TBitmap32 *mapImage) {
	TBitmap32 *tile_image = new TBitmap32();
	// int pos_y = 256 * pTileInfo->imageTileLine - this->mOffsetHeight;
	// int pos_x = 256 * pTileInfo->imageTileCol - this->mOffsetWidth;

	tile_image->Assign(pTileInfo->pngImage);
#ifdef _TileGrid_DEBUG
	tile_image->FrameRectS(0, 0, tile_image->Width, tile_image->Height, clBlue32);
	tile_image->Font->Color=clRed;

	AnsiString str = "";
	double lat, lon, f;

	lat = fabs(pTileInfo->lat);
	str += "Lat=" + IntToStr((int)lat) + "�";
	f = (lat - (int)lat) * 60.;
	str += IntToStr((int)f) + "'";
	f = (f - (int)f) * 60.;
	str += FloatToStrF(f, ffFixed, 6, 4) + "''";
	if (pTileInfo->lat < 0) str += "S";
	else str += "N";
	//tile_image->Textout(20, 220, str);
	tile_image->RenderText(5, 5, str, 2, clRed32);

	str = "Lon=";
	lon = fabs(pTileInfo->lon);
	str += IntToStr((int)lon) + "�";
	f = (lon - (int)lon) * 60.;
	str += IntToStr((int)f) + "'";
	f = (f - (int)f) * 60.;
	str += FloatToStrF(f, ffFixed, 6, 4) + "''";
	if (pTileInfo->lon < 0) str += "W";
	else str += "E";
	tile_image->RenderText(5, 25, str, 2, clRed32);

#endif

	this->ImageTileToMap(tile_image, pTileInfo->imageTileLine, pTileInfo->imageTileCol, mapImage);

	// mapImage->Draw(pos_x, pos_y, tile_image);

	delete tile_image;
}

void __fastcall TOsmMapLoader::ImageTileToMap(TBitmap32 *pTileImage, int pLine, int pColumn, TBitmap32 *pMapImage) {
	int pos_y = TILE_PIXEL_SIZE * pLine - this->mOffsetHeight;
	int pos_x = TILE_PIXEL_SIZE * pColumn - this->mOffsetWidth;

	pMapImage->Draw(pos_x, pos_y, pTileImage);
}
// -----------------------------------------------------------------------------

// -------- FindTileInCurrentImageOrCache --------------------------------------
MapTileInfo * __fastcall TOsmMapLoader::FindTileInCurrentImageOrCache(MapTileInfo *pTileInfo, MapTileInfo *pMatrix) {
	MapTileInfo *result = NULL;
	MapTileInfo *ptr = pMatrix;

	for (int line = 0; line < this->mMapLines; line++) {
		for (int col = 0; col < this->mMapColumns; col++, ptr++) {

			if ((mtsReady == ptr->tileState) && (ptr->xOsmTile == pTileInfo->xOsmTile) && (ptr->yOsmTile == pTileInfo->yOsmTile)) {
				result = ptr;
				line = this->mMapLines;
				break;
			}
		}
	}

	if ((NULL == result) && (this->mCacheDir.IsEmpty() != true)) {
		UnicodeString f_name = this->GetTileFileNameInCache(pTileInfo);

		if (FileExists(f_name) && (this->IsExpireFileInCache(f_name) != true)) {
			result = pTileInfo;
		}
	}

	return result;
}
// -----------------------------------------------------------------------------

// -------- IsExpireFileInCache ------------------------------------------------
bool __fastcall TOsmMapLoader::IsExpireFileInCache(UnicodeString pFileName) {
	bool result = false;
	struct _stat statbuf;

	_wstat(pFileName.c_str(), &statbuf);

	time_t seconds = time(NULL);
	time_t diff = seconds - statbuf.st_mtime;
	long days = diff / 86400; // 24 * 60 *60

	if (days > 30) {
		result = true;
	}
	return result;
}
// -----------------------------------------------------------------------------

// -------- TransferImageTile --------------------------------------------------
void __fastcall TOsmMapLoader::TransferImageTile(MapTileInfo *srcTile, MapTileInfo *trgTile) {

	if (trgTile->pngImage) {
		delete trgTile->pngImage;
	}
	trgTile->pngImage = srcTile->pngImage;
	srcTile->pngImage = NULL;
}
// -----------------------------------------------------------------------------

// -------- CreateCacheDirectory -----------------------------------------------
void __fastcall TOsmMapLoader::CreateCacheDirectory() {

	if (!DirectoryExists(this->mCacheDir)) {
		if (!CreateDir(this->mCacheDir)) {
			TRadarMapManager *mngr = dynamic_cast<TRadarMapManager *>(this->mManager);

			mngr->ShowMessageObject(NULL, mngr->MapMessages, TMessageType::mtWarning, mvtDisapering, 10000,
				"Cannot create cache directory: " + this->mCacheDir, "Warning");
			this->mCacheDir = "";
		}
	}

	UnicodeString temp_cache_dir = this->GetTempCacheDirName();

	if (!DirectoryExists(temp_cache_dir)) {
		if (!CreateDir(temp_cache_dir)) {
			TRadarMapManager *mngr = dynamic_cast<TRadarMapManager *>(this->mManager);

			mngr->ShowMessageObject(NULL, mngr->MapMessages, TMessageType::mtWarning, mvtDisapering, 10000,
				"Cannot create cache directory: " + temp_cache_dir, "Warning");
			this->mCacheDir = "";
		}
	}
}
// -----------------------------------------------------------------------------

// -------- DeleteTileImageFromCache -------------------------------------------
void __fastcall TOsmMapLoader::DeleteTileImageFromCache(UnicodeString pFileName) {

	if (FileExists(pFileName)) {
		DeleteFile(pFileName);
	}
}
// -----------------------------------------------------------------------------

// -------- FreeTileThreads ----------------------------------------------------
void __fastcall TOsmMapLoader::FreeTileThreads() {
	std::vector<TMapTileLoader *>::iterator it;
	TMapTileLoader *thread = NULL;

	for (it = this->mTileThreads.begin(); it != this->mTileThreads.end(); it++) {
		thread = *it;
		*it = NULL;

		if (thread) {
			if (!thread->SuccessfullyTerminated) {
				thread->ForceTerminate();
				thread->WaitFor();
			}
			delete thread;
		}
	}
}
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// TMapTileLoader
// -----------------------------------------------------------------------------

// -------- ThreadBody ---------------------------------------------------------
void __fastcall TMapTileLoader::ThreadBody(TIdHTTP *pHttpConnect) {
	TFileStream *stream = NULL;

	try {
		try {
			stream = new TFileStream(this->mTempFileName, fmCreate);
			pHttpConnect->Get(this->mTileRequest, stream);

			delete stream;
			stream = NULL;

			if (this->CheckTerminated()) {
				this->mMapTile->tileState = mtsCanceled;
			}
			else {
				if (FileExists(this->mResultFileName)) {
					DeleteFile(this->mResultFileName);
				}
				RenameFile(this->mTempFileName, this->mResultFileName);
				this->mMapTile->tileState = mtsLoadedInCache;
			}
		}
		catch(const Exception & e) {
			this->mMapTile->tileState = mtsLoadError;
		}
	}
	__finally {
		if (stream) {
			delete stream;
		}
	}
}
// ------------------------------------------------------------------------------
