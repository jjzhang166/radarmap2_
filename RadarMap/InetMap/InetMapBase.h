// -----------------------------------------------------------------------------
// InetMapBase.h
// -----------------------------------------------------------------------------

#ifndef InetMapBaseH
#define InetMapBaseH

#include <IdHTTP.hpp>
#include <XMLIntf.hpp>
#include "GetGPSData.h"
#include "VisualToolObjects.h"

#include <jpeg.hpp>

#include "BitmapMap.h"

#define _HTTP				"http://"
#define _BingMapsHTTP 		((AnsiString)_HTTP+"www.bing.com/maps") //dev.virtualearth.net")
#define _OpenStreetMapHTTP  ((AnsiString)_HTTP+"www.openstreetmap.org")

enum TInetMapViewType {imvtMap, imvtSatellite, imvtSatelliteLabels};

class TInetMapLoader;

// -----------------------------------------------------------------------------
// TInetMapBase
// -----------------------------------------------------------------------------
class TInetMapBase : public TBitmapMapsContainer {

private:
	TJPEGImage *mImage;
	UnicodeString mStatusCode;
	UnicodeString mStatusDescription;
	UnicodeString mXmlMetaData;
	UnicodeString mAppKey;
	TInetMapViewType mImagerySet;

	double mSouthLatitude;
	double mNorthLatitude;
	double mWestLongitude;
	double mEastLongitude;
	double mCenterLatitude;
	double mCenterLongitude;
	double mScaleHight;
	double mScaleWidth;
	int mImageHeight;
	int mImageWidth;
	int mMapZoom;
	int mMapRequestMinZoom;
	int mMapRequestMaxZoom;
	bool mWaitForMapCenter;
	unsigned mMapLoaderThreadId;

	TGpsListener *GpsListener;
	TInetMapLoader *mMapLoader;

	void __fastcall SetMapImage(TJPEGImage *pImage) {
		if (mImage) {
			delete mImage;
		}
		mImage = pImage;
	}

protected:

	void __fastcall writeCenterLatitude(double value) {
		mCenterLatitude = value;
		mWaitForMapCenter = false;
	}

	void __fastcall writeCenterLongitude(double value) {
		mCenterLongitude = value;
		mWaitForMapCenter = false;
	}

	void __fastcall AddCoordinate(TGPSCoordinate *c);

	void __fastcall GetCoordinateFromGps();

	_di_IXMLNode __fastcall FindNode(_di_IXMLNode pNode, char *pNodeName);

	void __fastcall writeMapImageZoom(int value);
public:

	__fastcall TInetMapBase(TMyObject *AOwner, void* ASettings, TLockedDoubleRect *AViewportArea);
	__fastcall ~TInetMapBase();

	void __fastcall UtilityUpdate();
	void __fastcall InfoUpdate();

	void __fastcall Update(TMyObjectType Type) {
		TMapsContainer::Update(Type);
	}
	void __fastcall Update();
	void __fastcall RenderMap();

	void __fastcall CalcScale();

	virtual UnicodeString __fastcall GetInetRequest(bool pIsMeteData) = 0;

//	bool __fastcall LoadMapData(bool WithUpdate);
	virtual TPersistent *__fastcall StreamToImage(TStringStream *pStrStream) { return NULL; };
//	void __fastcall FillMapImage(TStringStream *pStrStream);
	void __fastcall FillMapImage(TPersistent *pImage);
	virtual bool __fastcall ParseMetaData(UnicodeString pXmlData) = 0;
	void __fastcall FillMapBoundaries();
	TInetMapViewType static __fastcall SettingsMapViewTypeToEnum(UnicodeString pSettingsMapView);

	void __fastcall GetLayers(TCheckBoxTreeView *TreeView, TTreeNode *Root);
	void __fastcall SetLayers(TCheckBoxTreeView *TreeView, TTreeNode *Root);

	void __fastcall ZoomUpdate();
	void __fastcall ZoomUpdate(TMyObjectType Type) {Update(Type);};

	__property UnicodeString LoadStatusCode = { read = mStatusCode, write = mStatusCode };
	__property UnicodeString LoadStatusDescription = { read = mStatusDescription, write = mStatusDescription };
	__property UnicodeString AppKey = { read = mAppKey, write = mAppKey };
	__property TJPEGImage * MapImage = { read = mImage, write = SetMapImage };

	__property double SouthLatitude = { read = mSouthLatitude, write = mSouthLatitude };
	__property double NorthLatitude = { read = mNorthLatitude, write = mNorthLatitude };
	__property double WestLongitude = { read = mWestLongitude, write = mWestLongitude };
	__property double EastLongitude = { read = mEastLongitude, write = mEastLongitude };

	__property double MapCenterLatitude = { read = mCenterLatitude, write = writeCenterLatitude };
	__property double MapCenterLongitude = { read = mCenterLongitude, write = writeCenterLongitude };

	__property double MapImageScaleHight = { read = mScaleHight };
	__property double MapImageScaleWidth = { read = mScaleWidth };

	__property int MapImageWidth = { read = mImageWidth, write = mImageWidth };
	__property int MapImageHeight = { read = mImageHeight, write = mImageHeight };
	__property int MapImageZoom = { read = mMapZoom, write = writeMapImageZoom };
	__property int MapRequestMinZoom = { read = mMapRequestMinZoom, write = mMapRequestMinZoom };
	__property int MapRequestMaxZoom = { read = mMapRequestMaxZoom, write = mMapRequestMaxZoom };
	__property unsigned MapLoaderThreadId = { read = mMapLoaderThreadId, write = mMapLoaderThreadId };

	__property TInetMapViewType MapViewType = { read = mImagerySet, write = mImagerySet };

	__property bool WaitForMapCenter = {read = mWaitForMapCenter, write = mWaitForMapCenter};
	__property TInetMapLoader *MapLoader = {read = mMapLoader, write = mMapLoader};
};
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// TInetThread
// -----------------------------------------------------------------------------
class TInetThread : public TMyThread {

private:
	TObject* mManager;
	__int64 WorkCountLast;

	void __fastcall HttpConnectWorkBegin(TObject *ASender, TWorkMode AWorkMode, __int64 AWorkCountMax);
	void __fastcall HttpConnectWork(TObject *ASender, TWorkMode AWorkMode, __int64 AWorkCount);
	void __fastcall HttpConnectWorkEnd(TObject *ASender, TWorkMode AWorkMode);

protected:
	TToolProgressBar *mProgressBar;

	void __fastcall Execute();
	virtual void __fastcall ThreadBody(TIdHTTP *pHttpConnect) = 0;

public:
	__fastcall TInetThread(TObject *pManager, TToolProgressBar *pProgressBar) : TMyThread(true) {
//		FreeOnTerminate = true;
		mProgressBar = pProgressBar;
		WorkCountLast = 0;
		ForceResume();
//		AskForResume();
	}

	__fastcall ~TInetThread() {}

	virtual bool __fastcall CheckTerminated();
};
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// TInetMapLoader
// -----------------------------------------------------------------------------
class TInetMapLoader : public TInetThread {

private:
	TObject* mManager;
	TInetMapBase *mInetMap;
	bool mNeedUpdate;

	bool __fastcall LoadMapMetaData(TIdHTTP *pHttpConnect, TStringStream *pStrStream);
	bool __fastcall LoadMapImage(TIdHTTP *pHttpConnect, TStringStream *pStrStream);
	void __fastcall BingMapLoad(TIdHTTP *httpConnect, TStringStream *strStream);
	void __fastcall OsmMapLoad(TIdHTTP *httpConnect, TStringStream *strStream);

protected:
	void __fastcall ThreadBody(TIdHTTP *pHttpConnect);
	void __fastcall UpdateMap();

public:
	__fastcall TInetMapLoader(TObject *pManager, TInetMapBase *pInetMap, TToolProgressBar *pProgressBar, bool pNeedUpdate = true) : TInetThread(pManager, pProgressBar) {
		FreeOnTerminate = true;
		mManager = pManager;
		mInetMap = pInetMap;
		pInetMap->MapLoaderThreadId = this->ThreadID;
		mNeedUpdate = pNeedUpdate;
		ForceResume();
	}

	__fastcall ~TInetMapLoader() {
		if(mInetMap->MapLoaderThreadId == this->ThreadID) {
			mInetMap->MapLoaderThreadId = 0;
		}
	}

	bool __fastcall CheckTerminated();
};
// -----------------------------------------------------------------------------

#endif
