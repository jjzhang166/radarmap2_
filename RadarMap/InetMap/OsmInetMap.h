// -----------------------------------------------------------------------------
// OsmInetMap.h
// -----------------------------------------------------------------------------

#ifndef OsmInetMapH
#define OsmInetMapH
#include "InetMapBase.h"
#include <vector>

#define _TileGrid_DEBUG
#undef _TileGrid_DEBUG

// -----------------------------------------------------------------------------
// MapTileInfo
// -----------------------------------------------------------------------------

enum MapTileState {mtsNone, mtsToLoad, mtsLoading, mtsLoadedInCache, mtsLoadError, mtsCanceled, mtsReady};

//const int MAP_LINES = 5;
//const int MAP_COLUMNS = 7;
const int MAX_LOAD_THREADS = 16;

struct MapTileInfo {

public:
	volatile MapTileState tileState;
	int xOsmTile;
	int yOsmTile;
	int imageTileLine;
	int imageTileCol;
	int errorCounter;
	double lat, lon;
	TPngImage *pngImage;

	MapTileInfo() {
		tileState = mtsNone;
		xOsmTile = 0;
		yOsmTile = 0;
		imageTileLine = 0;
		imageTileCol = 0;
		errorCounter = 0;
		lat = lon = 0.0;
		pngImage = NULL;
	}

	~MapTileInfo() {
		if (pngImage) {
			delete pngImage;
		}
	}

	MapTileInfo(const MapTileInfo &pRight) {
		tileState = pRight.tileState;
		xOsmTile = pRight.xOsmTile;
		yOsmTile = pRight.yOsmTile;
		imageTileLine = pRight.imageTileLine;
		imageTileCol = pRight.imageTileCol;
		errorCounter = pRight.errorCounter;
		lat = pRight.lat;
		lon = pRight.lon;

		if (pRight.pngImage) {
			pngImage = new TPngImage();
			pngImage->Assign(pRight.pngImage);
		}
		else {
			pngImage = NULL;
		}
	}

	MapTileInfo& operator= (const MapTileInfo &pRight) {
		tileState = pRight.tileState;
		xOsmTile = pRight.xOsmTile;
		yOsmTile = pRight.yOsmTile;
		imageTileLine = pRight.imageTileLine;
		imageTileCol = pRight.imageTileCol;
		errorCounter = pRight.errorCounter;
		lat = pRight.lat;
		lon = pRight.lon;

		if (pngImage) {
			delete pngImage;
			pngImage = NULL;
		}

		if (pRight.pngImage) {
			pngImage = new TPngImage();
			pngImage->Assign(pRight.pngImage);
		}
		return *this;
	}
};

class TOsmInetMap;
class TMapTileLoader;

// -----------------------------------------------------------------------------
// TOsmMapLoader
// -----------------------------------------------------------------------------
class TOsmMapLoader : TObject {

private:
	TInetMapLoader *mMyThread;
	TObject* mManager;
	TOsmInetMap *mOsmMap;
	TToolProgressBar *mProgressBar;
	UnicodeString mCacheDir;
	AnsiString LoadingTileImegeStr;
	std::vector<TMapTileLoader *> mTileThreads;
	HANDLE mThreadHandles[64];
	int mMapLines;
	int mMapColumns;
	int mOffsetWidth;
	int mOffsetHeight;
//	bool mNeedUpdate;

	void __fastcall CreateCacheDirectory();
	void __fastcall CalcTilesCount();
	void __fastcall ClearMapImage(TBitmap32 *pImage);

	MapTileInfo __fastcall GetTileInfoAtCenter();
	UnicodeString __fastcall GetTileFileName(MapTileInfo *pTileInfo);
	UnicodeString __fastcall GetTileFileNameInCache(MapTileInfo *pTileInfo);
	UnicodeString __fastcall GetTileFileNameInTempCache(MapTileInfo *pTileInfo);
	UnicodeString __fastcall GetTileRequest(MapTileInfo *pTileInfo);
	UnicodeString __fastcall GetTempCacheDirName();

	void __fastcall FillTileMatrix(MapTileInfo *pMatrix);
	void __fastcall CorrectMapCoordinate();
	void __fastcall CopyTileMatrix(MapTileInfo *pSrcMatrix, MapTileInfo *pTrgMatrix);

	int __fastcall GetTilesFromMapAndChache(MapTileInfo *pSrcMatrix, MapTileInfo *pMatrix, TBitmap32 *pImage);
	void __fastcall StartLoadTilesFromServer(MapTileInfo *pMatrix);
	int __fastcall FillThreadHanlesArray();
	void __fastcall EndLoadingThreadProcessing(HANDLE pEndedThread, TBitmap32 *pImage);
	void __fastcall LoadPngTileFromCashe(MapTileInfo *pTileInfo);
	void __fastcall EmptyPngToTile(MapTileInfo *pTileInfo);
	void __fastcall ErrorPngToTile(MapTileInfo *pTileInfo);
	void __fastcall ImageTileToMap(MapTileInfo *pTileInfo, TBitmap32 *mapImage);
	void __fastcall ImageTileToMap(TBitmap32 *pTileImage, int pLine, int pColumn, TBitmap32 *pMapImage);
	MapTileInfo *__fastcall FindTileInCurrentImageOrCache(MapTileInfo *pTileInfo, MapTileInfo *pMatrix);
	void __fastcall TransferImageTile(MapTileInfo *srcTile, MapTileInfo *trgTile);

	bool __fastcall IsExpireFileInCache(UnicodeString pFileName);
	void __fastcall DeleteTileImageFromCache(UnicodeString pFileName);

	void __fastcall StopAllThread();
	void __fastcall FreeTileThreads();


protected:
	void __fastcall UpdateMap();

public:
	__fastcall TOsmMapLoader(TInetMapLoader *pMyThread, TObject *pManager, TToolProgressBar *pProgressBar);
	__fastcall ~TOsmMapLoader();

	TBitmap32 *__fastcall LoadMap(TInetMapBase *pOsmMap);
	bool __fastcall CheckTerminated();
};
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// TMapTileLoader
// -----------------------------------------------------------------------------
class TMapTileLoader : public TInetThread {

private:
	MapTileInfo *mMapTile;
	UnicodeString mTileRequest;
	UnicodeString mResultFileName;
	UnicodeString mTempFileName;

protected:
	virtual void __fastcall ThreadBody(TIdHTTP *pHttpConnect);

public:
	__fastcall TMapTileLoader(TObject *pManager, TToolProgressBar *pProgressBar, MapTileInfo *pTile, UnicodeString pRequest,
		UnicodeString pResultFile, UnicodeString pTempFile) : TInetThread(pManager, pProgressBar) {

		mMapTile = pTile;
		mTileRequest = pRequest;
		mResultFileName = pResultFile;
		mTempFileName = pTempFile;
	};

	__fastcall ~TMapTileLoader() {
	};
	__property MapTileInfo *MapTile = {read = mMapTile, write = mMapTile};
};
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// TOsmInetMap
// -----------------------------------------------------------------------------
class TOsmInetMap : public TInetMapBase {

private:
	MapTileInfo *mMapMatrix;

	UnicodeString __fastcall MapViewTypeToImagerySet();
	friend void __fastcall TOsmMapLoader::LoadMap(TInetMapBase *pOsmMap);

	UnicodeString __fastcall GetInetRequest(bool pIsMeteData) { return ""; }

	void __fastcall SetMapMatrix(MapTileInfo *pMapMatrix) {
		if (mMapMatrix) {
			delete[] mMapMatrix;
		}
		mMapMatrix = pMapMatrix;
	}

protected:

public:
	__fastcall TOsmInetMap(TMyObject *AOwner, void *ASettings, TLockedDoubleRect *AViewportArea);
	__fastcall ~TOsmInetMap() { if (mMapMatrix) { delete[] mMapMatrix; } };

	bool __fastcall ParseMetaData(UnicodeString pXmlData);

	__property MapTileInfo *MapMatrix = {read = mMapMatrix, write = SetMapMatrix};
};
// -----------------------------------------------------------------------------

#endif
