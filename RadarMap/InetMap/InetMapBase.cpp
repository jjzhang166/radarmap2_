// -----------------------------------------------------------------------------
// InetMapBase.cpp
// -----------------------------------------------------------------------------

#pragma hdrstop

#include <XMLDoc.hpp>
#include "InetMapBase.h"
#include "Manager.h"
#include "OsmInetMap.h"
#include <math.h>

#pragma package(smart_init)

// ------------------------------------------------------------------------------
// TInetMapBase
// ------------------------------------------------------------------------------

// -------- Constructor / Destructor -------------------------------------------
__fastcall TInetMapBase::TInetMapBase(TMyObject *AOwner, void* ASettings, TLockedDoubleRect *AViewportArea) : TBitmapMapsContainer(AOwner,
	ASettings, AViewportArea) {
	TRadarMapSettings *rm_settings = static_cast<TRadarMapSettings *>(radarmapsettings);
	maptype = mtOnlineMap;

	this->mImage = NULL;
	this->mXmlMetaData = "";
	this->mScaleHight = 0.0;
	this->mScaleWidth = 0.0;

	if (rm_settings) {
		this->mAppKey = rm_settings->BingInetMapAppKey;
		this->mImagerySet = SettingsMapViewTypeToEnum(rm_settings->BingInetMapViewType); // AerialWithLabels, Road, Aerialthis->mMapZoom = rm_settings->BingInetMapDefaultZoom;
		this->mMapZoom = rm_settings->BingInetMapDefaultZoom;
		this->mMapRequestMinZoom = rm_settings->BingInetMapMinZoom;
		this->mMapRequestMaxZoom = rm_settings->BingInetMapMaxZoom;

		//if(rm_settings->Owner && rm_settings->Owner->MapObjects && rm_settings->Owner->MapObjects->MapScale)
		if (ObjectsContainer && ((TMapObjectsContainer*)ObjectsContainer)->MapScale)
			switch (mMapZoom) {
				case 1: ((TMapObjectsContainer*)ObjectsContainer)->MapScale->ZoomText="1st level";
				case 2: ((TMapObjectsContainer*)ObjectsContainer)->MapScale->ZoomText="2nd level";
				case 3: ((TMapObjectsContainer*)ObjectsContainer)->MapScale->ZoomText="3rd level";
				default: ((TMapObjectsContainer*)ObjectsContainer)->MapScale->ZoomText=IntToStr(mMapZoom)+"th level";
			}
	}

	this->mImageWidth = 0;
	this->mImageHeight = 0;

	this->mWaitForMapCenter = false;
	this->GpsListener = NULL;
	this->mCenterLatitude = 0.0;
	this->mCenterLongitude = 0.0;

	this->mMapLoaderThreadId = 0;

	//this->coordinatesystem = csLatLon; // CS;
	this->lockedcs = true;

	this->klicnummer = "OnlineMap";
	this->filename = _HTTP;

	this->mMapLoader = NULL;
}

__fastcall TInetMapBase::~TInetMapBase() {

	if (this->mImage) {
		delete this->mImage;
	}
}
// ------------------------------------------------------------------------------

// -------- writeMapImageZoom ---------------------------------------------------
void __fastcall TInetMapBase::writeMapImageZoom(int value)
{
	TRadarMapManager *manager = dynamic_cast<TRadarMapManager *>(this->Manager);
	bool w = false;

	if (value != mMapZoom) {
		if (value > MapRequestMaxZoom) {
			value = MapRequestMaxZoom;
			w = true;
		}
		else if (value < MapRequestMinZoom)	{
			value = MapRequestMinZoom;
			w = true;
		}
		mMapZoom = value;
		if (ObjectsContainer) {
			if (w && manager && ((TMapObjectsContainer*)ObjectsContainer)->Messages)
				manager->ShowMessageObject(NULL, ((TMapObjectsContainer*)ObjectsContainer)->Messages, TMessageType::mtWarning, mvtDisapering,
					5000, "Zoom limit is reached", "Warning");
			if (((TMapObjectsContainer*)ObjectsContainer)->MapScale)
				switch (mMapZoom) {
					case 1: ((TMapObjectsContainer*)ObjectsContainer)->MapScale->ZoomText="1st level";
					case 2: ((TMapObjectsContainer*)ObjectsContainer)->MapScale->ZoomText="2nd level";
					case 3: ((TMapObjectsContainer*)ObjectsContainer)->MapScale->ZoomText="3rd level";
					default: ((TMapObjectsContainer*)ObjectsContainer)->MapScale->ZoomText=IntToStr(mMapZoom)+"th level";
				}
		}
	}
}
// ------------------------------------------------------------------------------

// -------- SettingsMapViewTypeToEnum ------------------------------------------
TInetMapViewType __fastcall TInetMapBase::SettingsMapViewTypeToEnum(UnicodeString pSettingsMapView) {
	TInetMapViewType view_type = imvtMap;

	if ("Satellite" == pSettingsMapView) {
		view_type = imvtSatellite;
	}
	else if ("SatelliteLabels" == pSettingsMapView) {
		view_type = imvtSatelliteLabels;
	}

	return view_type;
}
// ------------------------------------------------------------------------------

// -------- GetCoordinateFromGps -----------------------------------------------
void __fastcall TInetMapBase::GetCoordinateFromGps() {

	TRadarMapManager *mngr = dynamic_cast<TRadarMapManager *>(this->Manager);

	if (mngr && mngr->GpsListeners) {
		if (!GpsListener){
			GpsListener = new TGpsListener();
			GpsListener->OnAddCoordinate = this->AddCoordinate;
			mngr->GpsListeners->Add(GpsListener);
		}

		while (this->WaitForMapCenter) {

			if (this->mMapLoader && this->mMapLoader->CheckTerminated()) {
				   break;
			}

			//ProcessMessages();
			Sleep(10);
		}

		if (GpsListener) {
			mngr->GpsListeners->Delete(GpsListener);
			delete GpsListener;
			GpsListener = NULL;
		}
	}
}
// ------------------------------------------------------------------------------

// -------- AddCoordinate ------------------------------------------------------
void __fastcall TInetMapBase::AddCoordinate(TGPSCoordinate *c) {

	if(c && c->Valid) {
		MapCenterLatitude = c->Latitude;
		MapCenterLongitude = c->Longitude;
	}
}
// ------------------------------------------------------------------------------

// -------- FillMapBoundaries --------------------------------------------------
void __fastcall TInetMapBase::FillMapBoundaries() {
	/* */
	double x, y, delta_height, delta_width;
	TDoublePoint pnt;
	TGPSCoordinate *c;

	delta_height = delta_width = 0.0;

	if (this->mImageHeight < MaxBitmapHeight) {
		double delta_pixel = MaxBitmapHeight - this->mImageHeight;
		double degree_per_pixel = (this->mNorthLatitude - this->mSouthLatitude) / this->mImageHeight;

		delta_height = degree_per_pixel * delta_pixel / 2.0;
	}

	if (this->mImageWidth < MaxBitmapWidth) {
		double delta_pixel = MaxBitmapWidth - this->mImageWidth;
		double degree_per_pixel = (this->mEastLongitude - this->mWestLongitude) / this->mImageWidth;

		delta_width = degree_per_pixel * delta_pixel / 2.0;
	}

	try
	{
		c = new TGPSCoordinate();

		pnt.X = this->mWestLongitude - delta_width;
		pnt.Y = this->mNorthLatitude + delta_height;
		c->SetLatLon(pnt.Y, pnt.X);

		this->Lower = c->GetXY(coordinatesystem); //pnt;

		pnt.X = this->mEastLongitude + delta_width;
		pnt.Y = this->mSouthLatitude - delta_height;
		c->SetLatLon(pnt.Y, pnt.X);

		this->Upper = c->GetXY(coordinatesystem); //pnt;
	}
	__finally
	{
		delete c;
		c = NULL;
    }
	this->LatLonRectUpdate();
	if(Collection) Collection->UpdateCoordinatesRect();
}
// -----------------------------------------------------------------------------

// -------- FillMapImage -------------------------------------------------------
void __fastcall TInetMapBase::FillMapImage(TPersistent *pImage) {
	TRadarMapSettings *rm_settings = static_cast<TRadarMapSettings *>(this->radarmapsettings);
	TBitmap32 *src_map = new TBitmap32();

	src_map->BeginUpdate();
	src_map->SetSize(this->mImageWidth, this->mImageHeight);

	if (rm_settings) {
		src_map->Clear(rm_settings->DXFBackgroundColor);
	}
	else {
		src_map->Clear(clWhite32);
	}
	src_map->Assign(pImage);
	src_map->EndUpdate();

	if (this->rawpxrect) {
		delete this->rawpxrect;
		this->rawpxrect = NULL;
	}

	if (src_map->Width < MaxBitmapWidth || src_map->Height < MaxBitmapHeight) {
		TBitmap32 *tmp = src_map;
		int w = (tmp->Width < MaxBitmapWidth) ? MaxBitmapWidth : tmp->Width;
		int h = (tmp->Height < MaxBitmapHeight) ? MaxBitmapHeight : tmp->Height;

		src_map = new TBitmap32();

		src_map->BeginUpdate();
		src_map->SetSize(w, h);

		if (rm_settings) {
			src_map->Clear(rm_settings->DXFBackgroundColor);
		}
		else {
			src_map->Clear(clWhite32);
		}

		this->rawpxrect = new Types::TRect((w - tmp->Width)>> 1, (h - tmp->Height)>> 1, (w + tmp->Width)>> 1, (h + tmp->Height)>> 1);
		src_map->Draw(RawPxRect->Left, RawPxRect->Top, tmp);
		src_map->EndUpdate();
		delete tmp;
	}
	else {
		this->rawpxrect = new Types::TRect(0, 0, src_map->Width, src_map->Height);
	}

	if (this->bmpGB) {
		delete this->bmpGB;
		this->bmpGB = NULL;
	}
	this->bmpGB = src_map;

	if (this->bmpUtil) {
		this->bmpUtil->SetSizeFrom(this->bmpGB);
	}
	if (this->bmpInf) {
		this->bmpInf->SetSizeFrom(this->bmpGB);
	}

	if (NULL == this->background) {
		this->background = new TBitmap32();
	}
	this->background->SetSize(this->bmpGB->Width, this->bmpGB->Height);
	this->Background->BeginUpdate();
	this->Background->Draw(0, 0, this->bmpGB);
	this->Background->EndUpdate();
}
// ------------------------------------------------------------------------------

// -------- FindNode -----------------------------------------------------------
_di_IXMLNode __fastcall TInetMapBase::FindNode(_di_IXMLNode pNode, char *pNodeName) {
	_di_IXMLNode node = NULL;

	if (pNode->NodeName == pNodeName) {
		node = pNode;
	}
	else {
		if (pNode->HasChildNodes) {
			_di_IXMLNode child = pNode->ChildNodes->GetNode(0);

			while (child) {
				node = this->FindNode(child, pNodeName);

				if (node) {
					break;
				}
				child = child->NextSibling();
			}
		}
	}
	return node;
}
// ------------------------------------------------------------------------------

// -------- CalcScale ----------------------------------------------------------
void __fastcall TInetMapBase::CalcScale() {

	try {
		double degree;

		degree = fabs(this->mNorthLatitude - this->mSouthLatitude);
		this->mScaleHight = degree / this->mImageHeight;

		degree = fabs(this->mEastLongitude - this->mWestLongitude);
		this->mScaleWidth = degree / this->mImageWidth;
	}
	catch(...) {
	}
}
// ------------------------------------------------------------------------------

// -------- Update -------------------------------------------------------------
void __fastcall TInetMapBase::Update() {

	TMapsContainer::Update();
}
// ------------------------------------------------------------------------------

// -------- InfoUpdate ---------------------------------------------------------
void __fastcall TInetMapBase::InfoUpdate() {

	if (this->bmpInf) {
		this->bmpInf->BeginUpdate();

		try {
			this->bmpInf->Clear(Color32(0, 0, 0, 0));
		}
		__finally {
			this->bmpInf->EndUpdate();
		}
		if (this->Owner != NULL) {
			this->Owner->SetObjectChanged(gotInfo);
		}
	}
}
// ------------------------------------------------------------------------------

// -------- UtilityUpdate ------------------------------------------------------
void __fastcall TInetMapBase::UtilityUpdate() {

	if (this->bmpUtil) {
		this->bmpUtil->BeginUpdate();

		try {
			bmpUtil->Clear(Color32(0, 0, 0, 0));
		}
		__finally {
			this->bmpUtil->EndUpdate();
		}
		if (this->Owner != NULL) {
			this->Owner->SetObjectChanged(gotUtility);
		}
	}
}
// ------------------------------------------------------------------------------

// -------- ZoomUpdate ---------------------------------------------------------
void __fastcall TInetMapBase::ZoomUpdate() {

	if (this->Owner != NULL) {
		TMapsContainersCollection *collect = static_cast<TMapsContainersCollection *>(this->Owner);
		TLockedDoubleRect *view_d_rect = collect->ViewportArea;
		//Types::TPoint view_center = collect->ViewportAreaToScreen(view_d_rect->Center());
		//TDoublePoint center_new = collect->ScreenToCoordinatesRect(view_center);
		TDoublePoint center_new = collect->ViewportAreaToCoordinatesRect(view_d_rect->Center());

		TGPSCoordinate *c = new TGPSCoordinate;

		try {
			c->SetXY(CoordinateSystem, center_new);
			this->mCenterLatitude = c->Latitude;
			this->mCenterLongitude = c->Longitude;
		}
		__finally {
			delete c;
		}

		new TInetMapLoader(this->Manager, this, collect->Progress, true);
	}
}
// -----------------------------------------------------------------------------

// -------- RenderMap ----------------------------------------------------------
void __fastcall TInetMapBase::RenderMap() {

	TBitmapMapsContainer::RenderMap();
}
// ------------------------------------------------------------------------------

// -------- GetLayers ----------------------------------------------------------
void __fastcall TInetMapBase::GetLayers(TCheckBoxTreeView *TreeView, TTreeNode *Root) {
	TTreeNode *node;

	if(TreeView == NULL)
		return;

	if(Root == NULL) {
		TreeView->Items->Clear();
		Root = TreeView->Items->Add(NULL, Klicnummer);
		TreeView->SetChecking(Root, Visible);
	}
	Root->Data =(void*)((TMapsContainer*)this);

	node = TreeView->Items->AddChild(Root, "Bing Map Image");
	TreeView->SetChecking(node, this->Visible);
//	node->Data =(void*)this;
}
// -----------------------------------------------------------------------------

// -------- SetLayers ----------------------------------------------------------
void __fastcall TInetMapBase::SetLayers(TCheckBoxTreeView *TreeView, TTreeNode *Root) {
	TTreeNode *node;
	TMapsContainer *mc;

	if (TreeView == NULL) {
		return;
	}
	AutomaticUpdate = false;

	try {
		try {
			if (!Root) {
				Root = TreeView->Items->GetFirstNode();
			}
			if (Root) {
				mc =(TMapsContainer*)Root->Data;

				if (mc->MapType == mtOnlineMap && Root->Text == Klicnummer) {
					Visible = TreeView->Checked(Root);
					node = Root->getFirstChild();

					while (node != NULL) {
						if(node->Data == NULL) {
							this->Visible = TreeView->Checked(node);
						}
						node = Root->GetNextChild(node);
					}
				}
			}
		}
		catch(Exception & e) {
		}
	}
	__finally {
		AutomaticUpdate = true;
		Update();
	}
}
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// TInetMapLoader
// -----------------------------------------------------------------------------

// -------- ThreadBody ---------------------------------------------------------
void __fastcall TInetMapLoader::ThreadBody(TIdHTTP *pHttpConnect) {
	TStringStream *str_stream = new TStringStream();
	//TRadarMapManager *rm = static_cast<TRadarMapManager *>(this->mManager);

	SetStarted();

	try {
		try {
			this->mInetMap->MapLoader = this;

			if (mtBingInet == this->mInetMap->MapType) {
				this->BingMapLoad(pHttpConnect, str_stream);
			} else if (mtOsmInet == this->mInetMap->MapType) {
				this->OsmMapLoad(pHttpConnect, str_stream);
			}
		}
		catch(const Exception & e) {
		}
	}
	__finally {
		delete str_stream;

		this->mInetMap->MapLoader = NULL;
		//if(rm) this->mInetMap->CoordinateSystem = rm->Settings->DefaultCoordinateSystem;
	}
}
// -----------------------------------------------------------------------------

// -------- BingMapLoad --------------------------------------------------------
void __fastcall TInetMapLoader::BingMapLoad(TIdHTTP *httpConnect, TStringStream *strStream) {
 	bool result = false;

	result = this->LoadMapMetaData(httpConnect, strStream);

	if (this->CheckTerminated()) {
		result = false;
	}

	if (result) {
		UnicodeString xml_data = strStream->DataString;

		strStream->Position = 0;
		result = this->LoadMapImage(httpConnect, strStream);

		// ---- Update map information --------------------------------------
		if (this->mNeedUpdate && (this->CheckTerminated() != true)) {
			TPersistent *new_image = this->mInetMap->StreamToImage(strStream);

			this->mInetMap->ParseMetaData(xml_data);
			this->mInetMap->FillMapImage(new_image);
			this->mInetMap->ViewportArea->SetCenter(0.5, 0.5);
			this->mInetMap->FillMapBoundaries();

			Synchronize(this->UpdateMap);

			if (this->mManager) {
				TRadarMapManager *manager = (TRadarMapManager*)this->mManager;
				if (manager->GpsUnit) {
					manager->GpsUnit->ReCenter();
				}
			}

			delete new_image;
		}
	}
}
// -----------------------------------------------------------------------------

// -------- OsmMapLoad ---------------------------------------------------------
void __fastcall TInetMapLoader::OsmMapLoad(TIdHTTP *httpConnect, TStringStream *strStream) {
	TOsmMapLoader *loader = new TOsmMapLoader(this, this->mManager, NULL);//this->mProgressBar);

	this->mInetMap->ParseMetaData("");
//	this->mInetMap->ViewportArea->SetCenter(0.5, 0.5);
//	this->mInetMap->FillMapBoundaries();

	TBitmap32 *image = loader->LoadMap(this->mInetMap);

	this->mInetMap->FillMapBoundaries();

	// ---- Update map information ------------------------------------------
	if (image && this->mNeedUpdate && (this->CheckTerminated() != true)) {
		this->mInetMap->FillMapImage(image);
		//this->mInetMap->ViewportArea->SetCenter(0.5, 0.5);
//		this->mInetMap->FillMapBoundaries();

		Synchronize(this->UpdateMap);

		if (this->mManager) {
			TRadarMapManager *manager = (TRadarMapManager*)this->mManager;
			if (manager->GpsUnit) {
				manager->GpsUnit->ReCenter();
			}
		}
	}

	if (image) {
		delete image;
	}
	delete loader;
}
// -----------------------------------------------------------------------------

// -------- LoadMapMetaData ----------------------------------------------------
bool __fastcall TInetMapLoader::LoadMapMetaData(TIdHTTP *pHttpConnect, TStringStream *pStrStream) {
	bool result = false;
	UnicodeString str_request = this->mInetMap->GetInetRequest(true);

	try {
		if ((pHttpConnect->IOHandler == NULL) || pHttpConnect->IOHandler->Opened) {
			pHttpConnect->Get(str_request, pStrStream);
			result = true;
		}
	}
	catch(const Exception & e) {
		result = false;

		if (this->mManager && (this->CheckTerminated() != true)) {
			((TRadarMapManager*)this->mManager)->ShowMessageObject(NULL, ((TRadarMapManager*)this->mManager)->MapMessages,
				TMessageType::mtStop, mvtDisapering, 10000, "You�re not connected to a network! (" + e.Message + ")", "Error");
		}
	}

	return result;
}
// -----------------------------------------------------------------------------

// -------- LoadMapImage -------------------------------------------------------
bool __fastcall TInetMapLoader::LoadMapImage(TIdHTTP *pHttpConnect, TStringStream *pStrStream) {
	bool result = false;
	UnicodeString str_request = this->mInetMap->GetInetRequest(false);

	try {
		if ((pHttpConnect->IOHandler == NULL) || pHttpConnect->IOHandler->Opened) {
			pHttpConnect->Get(str_request, pStrStream);
			result = true;
		}
	}
	catch(const Exception & e) {
		result = false;

		if (this->mManager && (this->CheckTerminated() != true)) {
			((TRadarMapManager*)this->mManager)->ShowMessageObject(NULL, ((TRadarMapManager*)this->mManager)->MapMessages,
				TMessageType::mtStop, mvtDisapering, 10000, "You�re not connected to a network! (" + e.Message + ")", "Error");
		}
	}

	return result;
}
// ------------------------------------------------------------------------------

// -------- UpdateMap ----------------------------------------------------------
void __fastcall TInetMapLoader::UpdateMap() {
	if (this->mInetMap && this->mInetMap->MapLoaderThreadId == this->ThreadID) {
		this->mInetMap->Update();
		if(this->mInetMap->Owner && ((TMapsContainersCollection*)this->mInetMap->Owner)->Image) {
			Types::TRect bmr;

			bmr =((TMapsContainersCollection*)this->mInetMap->Owner)->Image->GetBitmapRect();
			((TMapsContainersCollection*)this->mInetMap->Owner)->Image->ScrollToCenter((float)bmr.Width()/ 2., (float)bmr.Height()/ 2.);
		}
	}
}
// -----------------------------------------------------------------------------

// -------- CheckTerminated ----------------------------------------------------
bool __fastcall TInetMapLoader::CheckTerminated() {
	bool result = false;

	if ((this->mInetMap->MapLoaderThreadId != 0) && (this->mInetMap->MapLoaderThreadId != this->ThreadID)) {
		AskForTerminate();
		result = true;
	} else {
		result = TInetThread::CheckTerminated();
    }

	return result;
}
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// TInetThread
// -----------------------------------------------------------------------------
void __fastcall TInetThread::Execute() {
	TIdHTTP *http_connect = new TIdHTTP(NULL);

	SetStarted();

	try {
		try {
			http_connect->OnWorkBegin = HttpConnectWorkBegin;
			http_connect->OnWork = HttpConnectWork;
			http_connect->OnWorkEnd = HttpConnectWorkEnd;

			this->ThreadBody(http_connect);
		}
		catch(const Exception & e) {
			int a = 0;
			if (a > 1){

			}
		}
	}
	__finally {
		http_connect->Disconnect();
		delete http_connect;
	}

	SetIsTerminated();
}
// -----------------------------------------------------------------------------

// -------- CheckTerminated ----------------------------------------------------
bool __fastcall TInetThread::CheckTerminated() {
	bool result = false;

	if (Terminated || MyTerminated || (this->mProgressBar && this->mProgressBar->CancelPressed)) {
		result = true;
	}
	return result;
}
// -----------------------------------------------------------------------------

// -------- HttpConnectWorkBegin -----------------------------------------------
void __fastcall TInetThread::HttpConnectWorkBegin(TObject * ASender, TWorkMode AWorkMode, __int64 AWorkCountMax) {

	int m;

	if (this->mProgressBar) {
		TIdHTTP *IdHTTP1 = (TIdHTTP *)ASender;

		if (IdHTTP1->Response->ContentLength > 0)
			m = IdHTTP1->Response->ContentLength;
		// else m = this->mInetMap->MapImageWidth * this->mInetMap->MapImageHeight * 3 + 100;
		// else m = this->mInetMap->MapImageWidth * this->mInetMap->MapImageHeight * 3 / 40 + 100;
		else
			m = 100 * 1024;
		this->mProgressBar->Show(m, true);
	}
	this->WorkCountLast = 0;
}
// -----------------------------------------------------------------------------

// -------- HttpConnectWork ----------------------------------------------------
void __fastcall TInetThread::HttpConnectWork(TObject * ASender, TWorkMode AWorkMode, __int64 AWorkCount) {

	if (this->mProgressBar) {
		this->mProgressBar->StepBy(AWorkCount - this->WorkCountLast);
	}
	this->WorkCountLast = AWorkCount;

	if (this->CheckTerminated()) {
		TIdHTTP *idHTTP1 = (TIdHTTP *)ASender;

		idHTTP1->Disconnect();
	}
}
// -----------------------------------------------------------------------------

// -------- HttpConnectWorkEnd -------------------------------------------------
void __fastcall TInetThread::HttpConnectWorkEnd(TObject * ASender, TWorkMode AWorkMode) {

	if (this->mProgressBar) {
		this->mProgressBar->Hide(false);
	}
}
// -----------------------------------------------------------------------------
