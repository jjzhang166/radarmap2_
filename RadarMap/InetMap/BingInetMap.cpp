// -----------------------------------------------------------------------------
// BingInetMap.cpp
// -----------------------------------------------------------------------------

#pragma hdrstop

#include <XMLDoc.hpp>
#include "BingInetMap.h"
#include "Manager.h"
#include <math.h>

#pragma package(smart_init)

//------------------------------------------------------------------------------
// TBingInetMap
//------------------------------------------------------------------------------

// -------- Constructor / Destructor -------------------------------------------
__fastcall TBingInetMap::TBingInetMap(TMyObject *AOwner, void* ASettings, TLockedDoubleRect *AViewportArea) : TInetMapBase(AOwner, ASettings, AViewportArea) {
	maptype = mtBingInet;
	this->filename = _BingMapsHTTP;
}

__fastcall TBingInetMap::~TBingInetMap() {
}
//------------------------------------------------------------------------------

// -------- MapViewTypeToImagerySet --------------------------------------------
UnicodeString __fastcall TBingInetMap::MapViewTypeToImagerySet() {
	UnicodeString imagery_set = "unknown";

	switch (this->MapViewType) {
	case imvtMap:
		imagery_set = "Road";
		break;

	case imvtSatellite:
		imagery_set = "Aerial";
		break;

	case imvtSatelliteLabels:
		imagery_set = "AerialWithLabels";
		break;

	default: ;
		break;
	}
	return imagery_set;
}
//------------------------------------------------------------------------------

// -------- GetInetRequest -----------------------------------------------------
//  API - http://msdn.microsoft.com/en-us/library/ff701724.aspx
//
//  Calculation Meters to Pixels - https://msdn.microsoft.com/en-us/library/bb259689.aspx
//
//	strResult ="http://dev.virtualearth.net/REST/V1/Imagery/Map/"+imagerySet+"/"+centerPoint+"/"+ZoomLevel
//            +"?mapSize="+mapSize+"&mapVersion=v1&key=AkPES-CO6ZeMDRdT686WU4ncn8onOufX977CBuMj8f9XxbQrR_qNtfSeQqBdtX71";
//
UnicodeString __fastcall TBingInetMap::GetInetRequest(bool pIsMeteData) {

	if (this->WaitForMapCenter) {
		GetCoordinateFromGps();
	}
	int map_zoom = this->MapImageZoom;
	if (map_zoom > this->MapRequestMaxZoom) {
		map_zoom = this->MapRequestMaxZoom;
	}
	else if (map_zoom < this->MapRequestMinZoom) {
		map_zoom = this->MapRequestMinZoom;
	}


	UnicodeString url = "http://dev.virtualearth.net/REST/V1/Imagery/Map/" + this->MapViewTypeToImagerySet()
		+ "/" + this->GetMapCenterAsString() + "/" + IntToStr(map_zoom) + "?mapSize=" + this->GetMapSizeAsString();

	if (pIsMeteData) {
		 url += "&mapMetadata=1&o=xml";
	}
	url += "&mapVersion=v1&key=" + this->AppKey;  // "AkPES-CO6ZeMDRdT686WU4ncn8onOufX977CBuMj8f9XxbQrR_qNtfSeQqBdtX71";

	return url;
}
//------------------------------------------------------------------------------

// -------- GetMapCenterAsString -----------------------------------------------
UnicodeString __fastcall TBingInetMap::GetMapCenterAsString() {

	return FloatToStr(this->MapCenterLatitude) + "," + FloatToStr(this->MapCenterLongitude);
}
//------------------------------------------------------------------------------

// -------- GetMapSizeAsString -------------------------------------------------
UnicodeString __fastcall TBingInetMap::GetMapSizeAsString() {

	return IntToStr(this->MapImageWidth) + "," + IntToStr(this->MapImageHeight);
}
//------------------------------------------------------------------------------

// -------- ParseMetaData ------------------------------------------------------
bool __fastcall TBingInetMap::ParseMetaData(UnicodeString pXmlData) {
	bool result = false;
	_di_IXMLDocument xml_doc = Xmldoc::NewXMLDocument();
	int pos = pXmlData.Pos("<");

	if (pos > 1) {
		pXmlData = pXmlData.Delete(1, pos - 1);
	}
	xml_doc->LoadFromXML(pXmlData);

	if (xml_doc->GetActive()) {
		_di_IXMLNode root_node = xml_doc->DocumentElement;

		if ("Response" != root_node->NodeName) {
				this->LoadStatusCode = "10";
				this->LoadStatusDescription = "Error root tag name";
		}
		else {
			_di_IXMLNode node = this->FindNode(root_node, "StatusCode");

			if (node && node->IsTextElement) {
				this->LoadStatusCode = node->Text;
			}

			node = this->FindNode(root_node, "StatusDescription");
			if (node && node->IsTextElement) {
				this->LoadStatusDescription = node->Text;
			}

			if (this->LoadStatusCode == "200") {
				_di_IXMLNode meta_node = this->FindNode(root_node, "StaticMapMetadata");

				node = this->FindNode(meta_node, "SouthLatitude");
				if (node && node->IsTextElement) {
					this->SouthLatitude = StrToFloat(node->Text);
				}

				node = this->FindNode(meta_node, "WestLongitude");
				if (node && node->IsTextElement) {
					this->WestLongitude = StrToFloat(node->Text);
				}

				node = this->FindNode(meta_node, "NorthLatitude");
				if (node && node->IsTextElement) {
					this->NorthLatitude = StrToFloat(node->Text);
				}

				node = this->FindNode(meta_node, "EastLongitude");
				if (node && node->IsTextElement) {
					this->EastLongitude = StrToFloat(node->Text);
				}

				node = this->FindNode(meta_node, "Latitude");
				if (node && node->IsTextElement) {
					this->MapCenterLatitude = StrToFloat(node->Text);
				}

				node = this->FindNode(meta_node, "Longitude");
				if (node && node->IsTextElement) {
					this->MapCenterLongitude = StrToFloat(node->Text);
				}

				node = this->FindNode(meta_node, "ImageWidth");
				if (node && node->IsTextElement) {
					this->MapImageWidth = StrToInt(node->Text);
				}

				node = this->FindNode(meta_node, "ImageHeight");
				if (node && node->IsTextElement) {
					this->MapImageHeight = StrToInt(node->Text);
				}

				node = this->FindNode(meta_node, "Zoom");
				if (node && node->IsTextElement) {
					this->MapImageZoom = StrToInt(node->Text);
				}

				this->CalcScale();
				result = true;
			}
		}
	}
	delete xml_doc;

	return result;
}
//------------------------------------------------------------------------------

// -------- ParseMetaData ------------------------------------------------------
TPersistent *__fastcall TBingInetMap::StreamToImage(TStringStream *pStrStream) {
	TJPEGImage *image = new TJPEGImage();

	pStrStream->Position = 0;
	image->LoadFromStream(pStrStream);

	return image;
	// image->SaveToFile("bing_image.png");
}
//------------------------------------------------------------------------------

