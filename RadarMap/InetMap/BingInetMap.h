// -----------------------------------------------------------------------------
// BingInetMap.h
// -----------------------------------------------------------------------------

#ifndef BingInetMapH
#define BingInetMapH

#include <XMLIntf.hpp>
#include "InetMapBase.h"

// -----------------------------------------------------------------------------
// TBingInetMap
// -----------------------------------------------------------------------------
class TBingInetMap : public TInetMapBase {

private:
	UnicodeString __fastcall GetInetRequest(bool pIsMeteData);
	UnicodeString __fastcall GetMapCenterAsString();
	UnicodeString __fastcall GetMapSizeAsString();
	UnicodeString __fastcall MapViewTypeToImagerySet();

protected:

public:
	__fastcall TBingInetMap(TMyObject *AOwner, void* ASettings, TLockedDoubleRect *AViewportArea);
	__fastcall ~TBingInetMap();

	bool __fastcall ParseMetaData(UnicodeString pXmlData);
	TPersistent *__fastcall StreamToImage(TStringStream *pStrStream);
};
// -----------------------------------------------------------------------------

#endif
