//---------------------------------------------------------------------------

#pragma hdrstop

#include "MyGraph.h"
#include "Manager.h"
#include "LabelForm.h"

#pragma package(smart_init)

//---------------------------------------------------------------------------
// TGraphicObjectsList
//---------------------------------------------------------------------------
int __fastcall TGraphicObjectsList::Add(TGraphicObject* o)
{
	if(o!=NULL)
	{
		o->Owner=Owner;
		o->PresetSmallSize(Owner->SmallSize);
	}
	return AddMyObject((TMyObject*)o);
}
//---------------------------------------------------------------------------

void __fastcall TGraphicObjectsList::Update(TMyObjectType Type)
{
	for(int i=0; i<Count; i++)
		if(Items[i] && Items[i]->ObjectType==Type) Items[i]->Update();
}
//---------------------------------------------------------------------------

void __fastcall TGraphicObjectsList::Update()
{
	for(int i=0; i<Count; i++)
		if(Items[i]) Items[i]->Update();
}

void __fastcall TGraphicObjectsList::ZoomUpdate(TMyObjectType Type)
{
	for(int i=0; i<Count; i++)
		if(Items[i] && Items[i]->ObjectType==Type) Items[i]->ZoomUpdate();
}
//---------------------------------------------------------------------------

void __fastcall TGraphicObjectsList::ZoomUpdate()
{
	for(int i=0; i<Count; i++)
		if(Items[i]) Items[i]->ZoomUpdate();
}

//---------------------------------------------------------------------------
// TMouseHoldTimer
//---------------------------------------------------------------------------
__fastcall TMouseHoldTimer::TMouseHoldTimer() : TTimerThread(MouseHoldDelay)
{
	sender=NULL;
	mousebutton=mbLeft;
	shift= TShiftState()<< ssLeft;
	x=y=-1;
	layer=NULL;

	OnTimer=OnMouseHold;
}
//---------------------------------------------------------------------------

void __fastcall TMouseHoldTimer::OnMouseHold()
{
	if(sender)
	{
		try
		{
			TTimerThread::Stop();
			sender->MouseHold(sender, mousebutton, TShiftState(shift), x, y, layer);
			sender=NULL;
		}
		catch(Exception &e) {}
	}
}
//---------------------------------------------------------------------------

void __fastcall TMouseHoldTimer::StartHold(TGraphicObject *Sender, TMouseButton MouseButton,
	TShiftState Shift, int X, int Y, TCustomLayer *Layer)
{
	sender=Sender;
	mousebutton=MouseButton;
	shift=Shift;
	x=X;
	y=Y;
	layer=Layer;
	TTimerThread::Start();
}

//---------------------------------------------------------------------------
// TPngContainer
//---------------------------------------------------------------------------
__fastcall TPngContainer::TPngContainer(TMyObject *AOwner): TMyObject()
{
	Owner=AOwner;
	bitmaps=new TMyObjectsNamedList(this);
}
//---------------------------------------------------------------------------

__fastcall TPngContainer::~TPngContainer()
{
	if(bitmaps)
	{
		bitmaps->Clear();
		delete bitmaps;
		bitmaps=NULL;
	}
}
//---------------------------------------------------------------------------

TBitmap32* __fastcall TPngContainer::AddPng(AnsiString AName)
{
	TResourcePng *res;
	int i;

	if(bitmaps && AName!="" && AName!=NULL)
	{
		try
		{
			i=bitmaps->IndexOf(AName);
			if(i>=0)
			{
				res=(TResourcePng*)bitmaps->Items[i];
				if(res)	res->AddUser();
			}
			else
			{
				res=new TResourcePng(AName);
				bitmaps->AddMyObject(res, AName);
			}
		}
		catch(...)
		{
			res=NULL;
		}
	}
	else res=NULL;

	if(res)	return res->Bmp;
	else return NULL;
}
//---------------------------------------------------------------------------

void __fastcall TPngContainer::DelPng(AnsiString AName)
{
	TResourcePng *res;
	int i;

	if(bitmaps && AName!="" && AName!=NULL)
	{
		try
		{
			i=bitmaps->IndexOf(AName);
			if(i>=0)
			{
				res=(TResourcePng*)bitmaps->Items[i];
				if(res)
				{
					res->DelUser();
					if(res->Count==0) bitmaps->Delete(i);
				}
			}
		}
		catch(...) {}
	}
}

//---------------------------------------------------------------------------
// TGraphicObjectsContainer
//---------------------------------------------------------------------------
__fastcall TGraphicObjectsContainer::TGraphicObjectsContainer(TImgView32 *ImgView, bool UseEvents, TMouseAction* ma, void* RMS)
	: TMyObject()
{
	objecttype=gotContainer;
	image=ImgView;
	mouseaction=ma;
	disablemouseaction=NULL;
	useevents=UseEvents;
	if(image!=NULL)
	{
		if(UseEvents)
		{
			prevmousemove=image->OnMouseMove;
			image->OnMouseMove=&MouseMove;
			prevmousedown=image->OnMouseDown;
			image->OnMouseDown=&MouseDown;
			prevmouseup=image->OnMouseUp;
			image->OnMouseUp=&MouseUp;
			prevmousewheel=image->OnMouseWheel;
			image->OnMouseWheel=&MouseWheel;
			prevmouseclick=image->OnClick;
			image->OnClick=&MouseClick;
			prevmousedblclick=image->OnDblClick;
			image->OnDblClick=&MouseDblClick;
			prevcanresize=image->OnCanResize;
			image->OnCanResize=&OnCanResize;
			prevresize=image->OnResize;
			image->OnResize=&OnResize;
			prevscroll=image->OnScroll;
			image->OnScroll=&OnScroll;
		}
		else
		{
			prevmousemove=NULL;
			prevmousedown=NULL;
			prevmouseup=NULL;
			prevmousewheel=NULL;
			prevmouseclick=NULL;
			prevmousedblclick=NULL;
			prevcanresize=NULL;
			prevresize=NULL;
			prevscroll=NULL;
		}
		DrawRect=image->GetViewportRect();
		CustomLayer=new TBitmapLayer(image->Layers);
		CustomLayer->OnPaint=&CustomLayerPaint;
		CustomLayer->Bitmap->DrawMode=dmBlend;
		CustomLayer->BringToFront();
		ToolsLayer=new TBitmapLayer(image->Layers);
		ToolsLayer->OnPaint=&ToolsLayerPaint;
		ToolsLayer->Bitmap->DrawMode=dmBlend;
		ToolsLayer->BringToFront();
		ControlLayer=new TBitmapLayer(image->Layers);
		ControlLayer->OnPaint=&ControlLayerPaint;
		ControlLayer->Bitmap->DrawMode=dmBlend;
		ControlLayer->BringToFront();
	}
	else
	{
		CustomLayer=NULL;
		ToolsLayer=NULL;
		ControlLayer=NULL;
		DrawRect=Types::TRect(0,0,0,0);
		prevmousemove=NULL;
		prevmousedown=NULL;
		prevmouseup=NULL;
		prevmousewheel=NULL;
		prevmouseclick=NULL;
		prevmousedblclick=NULL;
		prevcanresize=NULL;
		prevresize=NULL;
		prevscroll=NULL;
	}
	lockcontrols=false;
	CustomObjects=new TGraphicObjectsList(this);
	ControlObjects=new TGraphicObjectsList(this);
	ToolsObjects=new TGraphicObjectsList(this);
	RadarMapSettings=RMS;
	onmouseclick=NULL;
	GlobalPressed=GlobalDragNDrop=AfterScroll=false;
	CustomObjectsChanged=NULL;
	mouseholdtimer=new TMouseHoldTimer();
	MouseDownPoint!=Types::TPoint(-1, -1);
	pngcontainer=new TPngContainer(this);
	ModalObjectsList=new TList();
	objectsglobaltimer=NULL;
}
//---------------------------------------------------------------------------

__fastcall TGraphicObjectsContainer::~TGraphicObjectsContainer(void)
{
	if(ModalObjectsList)
	{
		ModalObjectsList->Clear();
		delete ModalObjectsList;
		ModalObjectsList=NULL;
	}
	if(image && useevents)
	{
		image->OnMouseMove=prevmousemove;
		image->OnMouseDown=prevmousedown;
		image->OnMouseUp=prevmouseup;
		image->OnMouseWheel=prevmousewheel;
		image->OnClick=prevmouseclick;
		image->OnDblClick=prevmousedblclick;
		image->OnCanResize=prevcanresize;
		image->OnResize=prevresize;
		image->OnScroll=prevscroll;
	}
	if(mouseholdtimer)
	{
		mouseholdtimer->Stop();
		mouseholdtimer->AskForTerminate();
		WaitOthers();
		try
		{
			if(mouseholdtimer->ForceTerminate())
			{
				mouseholdtimer->FreeHandles();
				delete mouseholdtimer;
				mouseholdtimer=NULL;
			}
		}
		catch(...) {}
	}
	if(!CustomObjects->Linked) delete CustomObjects;
	else CustomObjects->Linked=false;
	delete ToolsObjects;
	delete ControlObjects;
	if(CustomLayer)
	{
		if(image && image->Layers) image->Layers->Delete(CustomLayer->Index);
		else delete CustomLayer;
		CustomLayer=NULL;
	}
	if(ControlLayer)
	{
		if(image && image->Layers) image->Layers->Delete(ControlLayer->Index);
		else delete ControlLayer;
		ControlLayer=NULL;
	}
	if(ToolsLayer)
	{
		if(image && image->Layers) image->Layers->Delete(ToolsLayer->Index);
		else delete ToolsLayer;
		ToolsLayer=NULL;
	}
	if(objectsglobaltimer)
	{
		DismissMyThread(objectsglobaltimer);
		objectsglobaltimer=NULL;
	}
}
//---------------------------------------------------------------------------

TGraphicObjectsList* __fastcall TGraphicObjectsContainer::readCustomObjects(void)
{
	//if(CustomObjects->Linked) CustomObjects=new TGraphicObjectsList(this);
	return CustomObjects;
}
//---------------------------------------------------------------------------

void __fastcall TGraphicObjectsContainer::AssignCustomObjectsList(TGraphicObjectsList* value)
{
	if(value!=NULL)
	{
		if(CustomObjects!=NULL)
		{
			if(!CustomObjects->Linked) delete CustomObjects;
			else CustomObjects->Linked=false;
		}
		CustomObjects=value;
		CustomObjects->Linked=true;
	}
	else
	{
		CustomObjects->Clear();
		CustomObjects->Linked=false;
	}
	Update(gotCustom);
}
//---------------------------------------------------------------------------

int __fastcall TGraphicObjectsContainer::AddObject(TMyObject* o)
{
	if(((TGraphicObject*)o)->ObjectType==gotCustom)
	{
		if(CustomObjectsChanged)
			*CustomObjectsChanged=true;
		return CustomObjects->Add((TGraphicObject*)o);
	}
	else if(((TGraphicObject*)o)->ObjectType==gotControl) return ControlObjects->Add((TGraphicObject*)o);
	else if(((TGraphicObject*)o)->ObjectType==gotTool) return ToolsObjects->Add((TGraphicObject*)o);
	else return -1;
}
//---------------------------------------------------------------------------

TGraphicObject* __fastcall TGraphicObjectsContainer::GetObject(TMyObjectType Type, int Index)
{
	TList *Objects;

	if(Type==gotCustom) return CustomObjects->Items[Index];
	else if(Type==gotControl) return ControlObjects->Items[Index];
	else if(Type==gotTool) return ToolsObjects->Items[Index];
	else return NULL;
}
//---------------------------------------------------------------------------

void __fastcall TGraphicObjectsContainer::Delete(TMyObjectType Type, int Index)
{
	TList *Objects;

	if(Type==gotCustom)
	{
		if(CustomObjectsChanged) *CustomObjectsChanged=true;
		CustomObjects->Delete(Index);
	}
	else if(Type==gotControl) ControlObjects->Delete(Index);
	else if(Type==gotTool) ToolsObjects->Delete(Index);
}
//---------------------------------------------------------------------------

int __fastcall TGraphicObjectsContainer::SendToBack(TMyObjectType Type, int Index)
{
	if(Type==gotCustom) return CustomObjects->SendToBack(Index);
	else if(Type==gotControl) return ControlObjects->SendToBack(Index);
	else if(Type==gotTool) return ToolsObjects->SendToBack(Index);
	else return -1;
}
//---------------------------------------------------------------------------

int __fastcall TGraphicObjectsContainer::BringToFront(TMyObjectType Type, int Index)
{
	if(Type==gotCustom) return CustomObjects->BringToFront(Index);
	else if(Type==gotControl) return ControlObjects->BringToFront(Index);
	else if(Type==gotTool) return ToolsObjects->BringToFront(Index);
	else return -1;
}
//---------------------------------------------------------------------------

int __fastcall TGraphicObjectsContainer::OneStepBack(TMyObjectType Type, int Index)
{
	if(Type==gotCustom) return CustomObjects->OneStepBack(Index);
	else if(Type==gotControl) return ControlObjects->OneStepBack(Index);
	else if(Type==gotTool) return ToolsObjects->OneStepBack(Index);
	else return -1;
}
//---------------------------------------------------------------------------

int __fastcall TGraphicObjectsContainer::OneStepForward(TMyObjectType Type, int Index)
{
	if(Type==gotCustom) return CustomObjects->OneStepForward(Index);
	else if(Type==gotControl) return ControlObjects->OneStepForward(Index);
	else if(Type==gotTool) return ToolsObjects->OneStepForward(Index);
	else return -1;
}
//---------------------------------------------------------------------------

void __fastcall TGraphicObjectsContainer::CustomLayerPaint(TObject *Sender, TBitmap32 *Bmp)
{
	TGraphicObject* po;
	Types::TRect vpt, bmr;

//	DrawRect=image->GetViewportRect();

	DrawRect=vpt=image->GetViewportRect();
	bmr=image->GetBitmapRect();
	vpt.right+=abs(bmr.left);
	vpt.bottom+=abs(bmr.top);
	for(int i=0; i<CustomObjects->Count; i++)
	{
		try
		{
			po=(TGraphicObject*)CustomObjects->Items[i];
			if(po && (AfterScroll || po->IsInClientRect(vpt)))
				po->Redraw(Sender, Bmp);
		}
		catch(...) {}
	}
	AfterScroll=false;
}
//---------------------------------------------------------------------------

void __fastcall TGraphicObjectsContainer::ControlLayerPaint(TObject *Sender, TBitmap32 *Bmp)
{
	TGraphicObject* po;

	DrawRect=image->GetViewportRect();
	for(int i=0; i<ControlObjects->Count; i++)
	{
		po=(TGraphicObject*)ControlObjects->Items[i];
		if(po && po->IsInClientRect(DrawRect)) po->Redraw(Sender, Bmp);
	}
}
//---------------------------------------------------------------------------

void __fastcall TGraphicObjectsContainer::ToolsLayerPaint(TObject *Sender, TBitmap32 *Bmp)
{
	TGraphicObject* po;

	DrawRect=image->GetViewportRect();
	for(int i=0; i<ToolsObjects->Count; i++)
	{
		po=(TGraphicObject*)ToolsObjects->Items[i];
		if(po && po->Visible && po->IsInClientRect(DrawRect)) po->Redraw(Sender, Bmp);
	}
}
//---------------------------------------------------------------------------

TGraphicObject* __fastcall TGraphicObjectsContainer::readCurrentModalObject()
{
	TGraphicObject *res=NULL;
	if(ModalObjectsList && ModalObjectsList->Count>0)
		res=(TGraphicObject*)ModalObjectsList->Items[ModalObjectsList->Count-1];
	return res;
}
//---------------------------------------------------------------------------

void __fastcall TGraphicObjectsContainer::SetModalObject(TGraphicObject *po)
{
	if(ModalObjectsList && po) ModalObjectsList->Add(po);
}
//---------------------------------------------------------------------------

void __fastcall TGraphicObjectsContainer::ClearModalObject(TGraphicObject *po)
{
	int i;

	if(ModalObjectsList && po)
	{
		i=ModalObjectsList->IndexOf(po);
		while(i>=0 && i<ModalObjectsList->Count)
			ModalObjectsList->Delete(i);//Remove(i);
	}
}
//---------------------------------------------------------------------------

void __fastcall TGraphicObjectsContainer::MouseMove(System::TObject* Sender,
	  Classes::TShiftState Shift, int X, int Y, TCustomLayer *Layer)
{
	Types::TPoint p;
	TGraphicObject* po, *pressedpo, *dockingpo;
	int draging=0;
	bool ControlChanged, ToolsChanged, CustomChanged;

	if(MouseDownPoint!=Types::TPoint(X, Y) && MouseDownPoint!=Types::TPoint(-1, -1)) mouseholdtimer->Stop();
	entered=false;
	pressedpo=dockingpo=NULL;
	p=Types::TPoint(X, Y);
	AutomaticUpdate=false;
	try
	{
		for(int i=ControlObjects->Count-1; i>=0; i--)
		{
			po=(TGraphicObject*)ControlObjects->Items[i];
			if(po)
			{
				if(!entered && po->Mouseable && po->Visible && ((GlobalPressed && po->Pressed) || (!GlobalPressed && po->IsInClientRect(p))) && (!CurrentModalObject || CurrentModalObject==po))
				{
					po->InRect=true;
					po->MouseMove(Sender, Shift, X, Y, ControlLayer);
					if(!po->AllowNeighborMouseMove) entered=true;
					if(po->Pressed)
					{
						pressedpo=po;
						GlobalDragNDrop=true;
						draging++;
					}
				}
				else
				{
					if(GlobalPressed && po->Mouseable && po->Visible && po->Dockable && po->IsInClientRect(p) && (!CurrentModalObject || CurrentModalObject==po))
					{
						dockingpo=po;
						po->InRect=true;
					}
					else po->InRect=false;
					//if(!entered && po->ForceMouseable) po->MouseMove(Sender, Shift, X, Y, ControlLayer);
				}
			}
		}
	}
	__finally
	{
		//if(ObjectChanged) ControlUpdate();
		ControlChanged=ObjectChanged;
	}
	try
	{
		for(int i=ToolsObjects->Count-1; i>=0; i--)
		{
			po=(TGraphicObject*)ToolsObjects->Items[i];
			if(po)
			{
				if(!entered && po->Mouseable && po->Visible && ((GlobalPressed && po->Pressed) || (!GlobalPressed && po->IsInClientRect(p))) && (!CurrentModalObject || CurrentModalObject==po))
				{
					po->InRect=true;
					po->MouseMove(Sender, Shift, X, Y, ToolsLayer);
					if(!po->AllowNeighborMouseMove) entered=true;
					if(po->Pressed)
					{
						pressedpo=po;
						GlobalDragNDrop=true;
						draging++;
					}
				}
				else
				{
					if(GlobalPressed && po->Mouseable && po->Visible && po->Dockable && po->IsInClientRect(p) && (!CurrentModalObject || CurrentModalObject==po))
					{
						dockingpo=po;
						po->InRect=true;
					}
					else po->InRect=false;
					//if(!entered && po->ForceMouseable) po->MouseMove(Sender, Shift, X, Y, ControlLayer);
				}
			}
		}
	}
	__finally
	{
		//if(ObjectChanged) ToolsUpdate();
		ToolsChanged=ObjectChanged;
	}
	try
	{
		for(int i=CustomObjects->Count-1; i>=0; i--)
		{
			po=(TGraphicObject*)CustomObjects->Items[i];
			if(po)
			{
				if(!entered && po->Mouseable && po->Visible && ((GlobalPressed && po->Pressed) || (!GlobalPressed && po->IsInClientRect(p))) && (!CurrentModalObject || CurrentModalObject==po))
				{
					po->InRect=true;
					po->MouseMove(Sender, Shift, X, Y, CustomLayer);
					if(!po->AllowNeighborMouseMove) entered=true;
					if(po->Pressed)
					{
						pressedpo=po;
						GlobalDragNDrop=true;
						draging++;
					}
				}
				else
				{
					if(GlobalPressed && po->Mouseable && po->Visible && po->Dockable && po->IsInClientRect(p) && (!CurrentModalObject || CurrentModalObject==po))
					{
						dockingpo=po;
						po->InRect=true;
					}
					else po->InRect=false;
					//if(!entered && po->ForceMouseable) po->MouseMove(Sender, Shift, X, Y, ControlLayer);
				}
			}
		}
	}
	__finally
	{
		//if(ObjectChanged) CustomUpdate();
		CustomChanged=ObjectChanged;
	}
	if(!entered)
	{
		for(int i=ControlObjects->Count-1; i>=0; i--)
		{
			po=(TGraphicObject*)ControlObjects->Items[i];
			if(po && po->ForceMouseable) po->MouseMove(Sender, Shift, X, Y, ControlLayer);
		}
		ControlChanged|=ObjectChanged;
		for(int i=ToolsObjects->Count-1; i>=0; i--)
		{
			po=(TGraphicObject*)ToolsObjects->Items[i];
			if(po && po->ForceMouseable) po->MouseMove(Sender, Shift, X, Y, ToolsLayer);
		}
		ToolsChanged|=ObjectChanged;
		for(int i=CustomObjects->Count-1; i>=0; i--)
		{
			po=(TGraphicObject*)CustomObjects->Items[i];
			if(po && po->ForceMouseable) po->MouseMove(Sender, Shift, X, Y, CustomLayer);
		}
		CustomChanged|=ObjectChanged;
	}
	AutomaticUpdate=true;
	try
	{
		if(GlobalDragNDrop && draging==0) GlobalDragNDrop=false;
		if(dockingpo) dockingpo->DraggingObject=pressedpo;
		if(ControlChanged) ControlUpdate();
		if(ToolsChanged) ToolsUpdate();
		if(CustomChanged) CustomUpdate();
	}
	catch (Exception &e) {}
}
//---------------------------------------------------------------------------

void __fastcall TGraphicObjectsContainer::MouseDown(TObject *Sender,
	  TMouseButton Button, TShiftState Shift, int X, int Y,
	  TCustomLayer *Layer)
{
	Types::TPoint p;
	TGraphicObject* po;

	mouseholdtimer->Stop();
	entered=false;
	MouseDownPoint=p=Types::TPoint(X, Y);
	GlobalPressed=true;
	try
	{
		if(!lockcontrols)
		{
			AutomaticUpdate=false;
			for(int i=ControlObjects->Count-1; i>=0; i--)
			{
				po=(TGraphicObject*)ControlObjects->Items[i];
				if(po)
				{
					if(!entered && po->Visible && po->Mouseable && po->IsInClientRect(p) && (!CurrentModalObject || CurrentModalObject==po))
					{
						po->X1=X;
						po->Y1=Y;
						po->InRect=true;
						po->Pressed=true;
						mouseholdtimer->StartHold(po, Button, Shift, X, Y, ControlLayer);
						po->MouseDown(Sender, Button, Shift, X, Y, ControlLayer);
						if(!po->AllowNeighborMouseMove) entered=true;
					}
					else
					{
						po->InRect=false;
						po->Pressed=false;
						//if(!entered && po->ForceMouseable) po->MouseDown(Sender, Button, Shift, X, Y, ToolsLayer);
					}
				}
			}
		}
	}
	__finally
	{
		if(ObjectChanged) ControlUpdate();
		AutomaticUpdate=true;
	}
	try
	{
		AutomaticUpdate=false;
		for(int i=ToolsObjects->Count-1; i>=0; i--)
		{
			po=(TGraphicObject*)ToolsObjects->Items[i];
			if(po)
			{
				if(!entered && po->Mouseable && po->Visible && po->IsInClientRect(p) && (!CurrentModalObject || CurrentModalObject==po))
				{
					po->X1=X;
					po->Y1=Y;
					po->InRect=true;
					po->Pressed=true;
					mouseholdtimer->StartHold(po, Button, Shift, X, Y, ToolsLayer);
					po->MouseDown(Sender, Button, Shift, X, Y, ToolsLayer);
					if(!po->AllowNeighborMouseMove) entered=true;
				}
				else
				{
					po->InRect=false;
					po->Pressed=false;
					//if(!entered && po->ForceMouseable) po->MouseDown(Sender, Button, Shift, X, Y, ToolsLayer);
				}
			}
		}
	}
	__finally
	{
		if(ObjectChanged) ToolsUpdate();
		AutomaticUpdate=true;
	}
	try
	{
		AutomaticUpdate=false;
		for(int i=CustomObjects->Count-1; i>=0; i--)
		{
			po=(TGraphicObject*)CustomObjects->Items[i];
			if(po)
			{
				if(!entered && ((mouseaction && *mouseaction>tmaGlobal) || !mouseaction) && po->Visible && po->Mouseable && po->IsInClientRect(p) && (!CurrentModalObject || CurrentModalObject==po))
				{
					po->X1=X;
					po->Y1=Y;
					po->InRect=true;
					po->Pressed=true;
					mouseholdtimer->StartHold(po, Button, Shift, X, Y, CustomLayer);
					po->MouseDown(Sender, Button, Shift, X, Y, CustomLayer);
					if(!po->AllowNeighborMouseMove) entered=true;
				}
				else
				{
					po->InRect=false;
					po->Pressed=false;
					//if(!entered && po->ForceMouseable) po->MouseDown(Sender, Button, Shift, X, Y, ToolsLayer);
				}
			}
		}
	}
	__finally
	{
		if(ObjectChanged) CustomUpdate();
		AutomaticUpdate=true;
	}
	if(!entered)
	{
		try
		{
			AutomaticUpdate=false;
			if(!lockcontrols)
			{
				for(int i=ControlObjects->Count-1; i>=0; i--)
				{
					po=(TGraphicObject*)ControlObjects->Items[i];
					if(po && po->ForceMouseable) po->MouseDown(Sender, Button, Shift, X, Y, ControlLayer);
				}
				if(ObjectChanged) ControlUpdate();
			}
			for(int i=ToolsObjects->Count-1; i>=0; i--)
			{
				po=(TGraphicObject*)ToolsObjects->Items[i];
				if(po && po->ForceMouseable) po->MouseDown(Sender, Button, Shift, X, Y, ToolsLayer);
			}
			if(ObjectChanged) ToolsUpdate();
			for(int i=CustomObjects->Count-1; i>=0; i--)
			{
				po=(TGraphicObject*)CustomObjects->Items[i];
				if(po && po->ForceMouseable) po->MouseDown(Sender, Button, Shift, X, Y, CustomLayer);
			}
			if(ObjectChanged) CustomUpdate();
		}
		__finally
		{
			AutomaticUpdate=true;
		}
	}
	if(!entered) GlobalPressed=false;
}
//---------------------------------------------------------------------------

void __fastcall TGraphicObjectsContainer::MouseUp(TObject *Sender,
	  TMouseButton Button, TShiftState Shift, int X, int Y,
	  TCustomLayer *Layer)
{
	Types::TPoint p;
	TGraphicObject *po, *pressedpo, *dockingpo;
	bool entered2;

	MouseDownPoint!=Types::TPoint(-1, -1);
	mouseholdtimer->Stop();
	entered=entered2=false;
	pressedpo=dockingpo=NULL;
	p=Types::TPoint(X, Y);
	try
	{
		if(!lockcontrols)
		{
			AutomaticUpdate=false;
			for(int i=ControlObjects->Count-1; i>=0; i--)
			{
				po=(TGraphicObject*)ControlObjects->Items[i];
				if(po)
				{
					if(!entered && po->Visible && po->Mouseable && ((GlobalPressed && po->Pressed) || (!GlobalPressed && po->IsInClientRect(p))) && (!CurrentModalObject || CurrentModalObject==po))
					{
						po->MouseUp(Sender, Button, Shift, X, Y, ControlLayer);
						pressedpo=po;
						if(!po->AllowNeighborMouseMove) entered=true;
					}
					//else if(!entered && po->ForceMouseable) po->MouseUp(Sender, Button, Shift, X, Y, ToolsLayer);
					po->Pressed=false;
					if(!entered2 && po->IsInClientRect(p) && (!CurrentModalObject || CurrentModalObject==po))
					{
						po->InRect=true;
						entered2=true;
					}
					else po->InRect=false;
					if(po->InDragNDrop && !dockingpo && po->IsInClientRect(p) && (!CurrentModalObject || CurrentModalObject==po))
					{
						dockingpo=po;
					}
					po->InDragNDrop=false;
				}
			}
		}
	}
	__finally
	{
		if(ObjectChanged) ControlUpdate();
		AutomaticUpdate=true;
	}
	try
	{
		AutomaticUpdate=false;
		for(int i=ToolsObjects->Count-1; i>=0; i--)
		{
			po=(TGraphicObject*)ToolsObjects->Items[i];
			if(po)
			{
				if(!entered && po->Visible && po->Mouseable && ((GlobalPressed && po->Pressed) || (!GlobalPressed && po->IsInClientRect(p))) && (!CurrentModalObject || CurrentModalObject==po))
				{
					po->MouseUp(Sender, Button, Shift, X, Y, ToolsLayer);
					pressedpo=po;
					if(!po->AllowNeighborMouseMove) entered=true;
				}
				//else if(!entered && po->ForceMouseable) po->MouseUp(Sender, Button, Shift, X, Y, ToolsLayer);
				po->Pressed=false;
				if(!entered2 && po->IsInClientRect(p) && (!CurrentModalObject || CurrentModalObject==po))
				{
					po->InRect=true;
					entered2=true;
				}
				else po->InRect=false;
				if(po->InDragNDrop && !dockingpo && po->IsInClientRect(p) && (!CurrentModalObject || CurrentModalObject==po))
				{
					dockingpo=po;
				}
				po->InDragNDrop=false;
			}
		}
	}
	__finally
	{
		if(ObjectChanged) ToolsUpdate();
		AutomaticUpdate=true;
	}
	try
	{
		AutomaticUpdate=false;
		for(int i=CustomObjects->Count-1; i>=0; i--)
		{
			po=(TGraphicObject*)CustomObjects->Items[i];
			if(po)
			{
				if(!entered && po->Visible && po->Mouseable && ((GlobalPressed && po->Pressed) || (!GlobalPressed && po->IsInClientRect(p))) && (!CurrentModalObject || CurrentModalObject==po))
				{
					po->MouseUp(Sender, Button, Shift, X, Y, CustomLayer);
					pressedpo=po;
					if(!po->AllowNeighborMouseMove) entered=true;
				}
				//else if(!entered && po->ForceMouseable) po->MouseUp(Sender, Button, Shift, X, Y, ToolsLayer);
				po->Pressed=false;
				if(!entered2 && po->IsInClientRect(p) && (!CurrentModalObject || CurrentModalObject==po))
				{
					po->InRect=true;
					entered2=true;
				}
				else po->InRect=false;
				if(po->InDragNDrop && !dockingpo && po->IsInClientRect(p) && (!CurrentModalObject || CurrentModalObject==po))
				{
					dockingpo=po;
				}
				po->InDragNDrop=false;
			}
		}
	}
	__finally
	{
		if(ObjectChanged) CustomUpdate();
		AutomaticUpdate=true;
	}
	if(!entered)
	{
		try
		{
			AutomaticUpdate=false;
			if(!lockcontrols)
			{
				for(int i=ControlObjects->Count-1; i>=0; i--)
				{
					po=(TGraphicObject*)ControlObjects->Items[i];
					if(po && po->ForceMouseable) po->MouseUp(Sender, Button, Shift, X, Y, ControlLayer);
				}
				if(ObjectChanged) ControlUpdate();
			}
			for(int i=ToolsObjects->Count-1; i>=0; i--)
			{
				po=(TGraphicObject*)ToolsObjects->Items[i];
				if(po && po->ForceMouseable) po->MouseUp(Sender, Button, Shift, X, Y, ToolsLayer);
			}
			if(ObjectChanged) ToolsUpdate();
			for(int i=CustomObjects->Count-1; i>=0; i--)
			{
				po=(TGraphicObject*)CustomObjects->Items[i];
				if(po && po->ForceMouseable) po->MouseUp(Sender, Button, Shift, X, Y, CustomLayer);
			}
			if(ObjectChanged) CustomUpdate();
		}
		__finally
		{
			AutomaticUpdate=true;
		}
	}
	if(GlobalDragNDrop && dockingpo && pressedpo)
	{
		try
		{
			dockingpo->DockDrop(this, pressedpo, X, Y);
			dockingpo->InRect=false;
		}
		__finally
		{
			if(ObjectChanged)
			{
				ControlUpdate();
				ToolsUpdate();
				CustomUpdate();
				AutomaticUpdate=true;
			}
		}
	}
	GlobalDragNDrop=GlobalPressed=false;
}
//---------------------------------------------------------------------------

void __fastcall TGraphicObjectsContainer::MouseWheel(TObject *Sender,
	  TShiftState Shift, int WheelDelta, const Types::TPoint &MousePos,
	  bool &Handled)
{
	TGraphicObject* po;

	if(!lockcontrols)
		for(int i=ControlObjects->Count-1; i>=0; i--)
		{
			po=(TGraphicObject*)ControlObjects->Items[i];
			if(po && po->Visible && po->Mouseable && po->IsInClientRect(MousePos) && (!CurrentModalObject || CurrentModalObject==po))
			{
				po->MouseWheel(Sender, Shift, WheelDelta, MousePos, Handled, ControlLayer);
				return;
			}
			//else if(!entered && po->ForceMouseable) po->MouseWheel(Sender, Shift, WheelDelta, MousePos, Handled, CustomLayer);
		}
	for(int i=ToolsObjects->Count-1; i>=0; i--)
	{
		po=(TGraphicObject*)ToolsObjects->Items[i];
		if(po && po->Visible && po->Mouseable && po->IsInClientRect(MousePos) && (!CurrentModalObject || CurrentModalObject==po))
		{
			po->MouseWheel(Sender, Shift, WheelDelta, MousePos, Handled, CustomLayer);
			return;
		}
		//else if(!entered && po->ForceMouseable) po->MouseWheel(Sender, Shift, WheelDelta, MousePos, Handled, CustomLayer);
	}
	for(int i=CustomObjects->Count-1; i>=0; i--)
	{
		po=(TGraphicObject*)CustomObjects->Items[i];
		if(po && po->Visible && po->Mouseable && po->IsInClientRect(MousePos) && (!CurrentModalObject || CurrentModalObject==po))
		{
			po->MouseWheel(Sender, Shift, WheelDelta, MousePos, Handled, CustomLayer);
			return;
		}
		//else if(!entered && po->ForceMouseable) po->MouseWheel(Sender, Shift, WheelDelta, MousePos, Handled, CustomLayer);
	}
	if(!entered)
	{
		try
		{
			AutomaticUpdate=false;
			if(!lockcontrols)
			{
				for(int i=ControlObjects->Count-1; i>=0; i--)
				{
					po=(TGraphicObject*)ControlObjects->Items[i];
					if(po && po->ForceMouseable) po->MouseWheel(Sender, Shift, WheelDelta, MousePos, Handled, ControlLayer);
				}
			}
			for(int i=ToolsObjects->Count-1; i>=0; i--)
			{
				po=(TGraphicObject*)ToolsObjects->Items[i];
				if(po && po->ForceMouseable) po->MouseWheel(Sender, Shift, WheelDelta, MousePos, Handled, ToolsLayer);
			}
			for(int i=CustomObjects->Count-1; i>=0; i--)
			{
				po=(TGraphicObject*)CustomObjects->Items[i];
				if(po && po->ForceMouseable) po->MouseWheel(Sender, Shift, WheelDelta, MousePos, Handled, CustomLayer);
			}
		}
		__finally
		{
			AutomaticUpdate=true;
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TGraphicObjectsContainer::MouseClick(TObject *Sender)
{
	Types::TPoint p;
	TGraphicObject* po;

	mouseholdtimer->Stop();
	GetCursorPos(&p);
	p=image->ScreenToClient(p);
	if(image->ClientRect.Contains(p))
	{
		if(CurrentModalObject && !CurrentModalObject->FullModality && !CurrentModalObject->IsInClientRect(p))
			CurrentModalObject->Hide();
		else
		{
			if(!lockcontrols)
				for(int i=ControlObjects->Count-1; i>=0; i--)
				{
					po=(TGraphicObject*)ControlObjects->Items[i];
					if(po && po->Visible && po->Mouseable && po->IsInClientRect(p) && (!CurrentModalObject || CurrentModalObject==po))
					{
						po->MouseClick(Sender, p.x, p.y);
						return;
					}
					//else if(!entered && po->ForceMouseable) po->MouseClick(Sender, p.x, p.y);
				}
			for(int i=ToolsObjects->Count-1; i>=0; i--)
			{
				po=(TGraphicObject*)ToolsObjects->Items[i];
				if(po && po->Visible && po->Mouseable && po->IsInClientRect(p) && (!CurrentModalObject || CurrentModalObject==po))
				{
					po->MouseClick(Sender, p.x, p.y);
					return;
				}
				//else if(!entered && po->ForceMouseable) po->MouseClick(Sender, p.x, p.y);
			}
			for(int i=CustomObjects->Count-1; i>=0; i--)
			{
				po=(TGraphicObject*)CustomObjects->Items[i];
				if(po && po->Visible && po->Mouseable && po->IsInClientRect(p) && (!CurrentModalObject || CurrentModalObject==po))
				{
					po->MouseClick(Sender, p.x, p.y);
					return;
				}
				//else if(!entered && po->ForceMouseable) po->MouseClick(Sender, p.x, p.y);
			}
		}
		if(OnMouseClick!=NULL)
		{
			(OnMouseClick)(Sender, p.x, p.y);
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TGraphicObjectsContainer::MouseDblClick(TObject *Sender)
{
	Types::TPoint p;
	TGraphicObject* po;

	GetCursorPos(&p);
	p=image->ScreenToClient(p);
	if(image->ClientRect.Contains(p))
	{
		if(!lockcontrols)
			for(int i=ControlObjects->Count-1; i>=0; i--)
			{
				po=(TGraphicObject*)ControlObjects->Items[i];
				if(po && po->Visible && po->Mouseable && po->IsInClientRect(p) && (!CurrentModalObject || CurrentModalObject==po))
				{
					po->MouseDblClick(Sender, p.x, p.y);
					return;
				}
				//else if(!entered && po->ForceMouseable) po->MouseDblClick(Sender, p.x, p.y);
			}
		for(int i=ToolsObjects->Count-1; i>=0; i--)
		{
			po=(TGraphicObject*)ToolsObjects->Items[i];
			if(po && po->Visible && po->Mouseable && po->IsInClientRect(p) && (!CurrentModalObject || CurrentModalObject==po))
			{
				po->MouseDblClick(Sender, p.x, p.y);
				return;
			}
			//else if(!entered && po->ForceMouseable) po->MouseDblClick(Sender, p.x, p.y);
		}
		for(int i=CustomObjects->Count-1; i>=0; i--)
		{
			po=(TGraphicObject*)CustomObjects->Items[i];
			if(po && po->Visible && po->Mouseable && po->IsInClientRect(p) && (!CurrentModalObject || CurrentModalObject==po))
			{
				po->MouseDblClick(Sender, p.x, p.y);
				return;
			}
			//else if(!entered && po->ForceMouseable) po->MouseDblClick(Sender, p.x, p.y);
		}
		if(!entered)
		{
			if(!lockcontrols)
				for(int i=ControlObjects->Count-1; i>=0; i--)
				{
					po=(TGraphicObject*)ControlObjects->Items[i];
					if(po && po->ForceMouseable) po->MouseDblClick(Sender, p.x, p.y);
				}
			for(int i=ToolsObjects->Count-1; i>=0; i--)
			{
				po=(TGraphicObject*)ToolsObjects->Items[i];
				if(po && po->ForceMouseable) po->MouseDblClick(Sender, p.x, p.y);
			}
			for(int i=CustomObjects->Count-1; i>=0; i--)
			{
				po=(TGraphicObject*)CustomObjects->Items[i];
				if(po && po->ForceMouseable) po->MouseDblClick(Sender, p.x, p.y);
			}
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TGraphicObjectsContainer::OnCanResize(TObject *Sender,
	int &Width, int &Height, bool &Resize)
{
	NewWidth=Width;
	NewHeight=Height;
}
//---------------------------------------------------------------------------

void __fastcall TGraphicObjectsContainer::OnResize(TObject *Sender)
{
	for(int i=ControlObjects->Count-1; i>=0; i--)
		((TGraphicObject*)ControlObjects->Items[i])->OnBeforeResize(Sender, NewWidth, NewHeight);
	for(int i=ControlObjects->Count-1; i>=0; i--)
		((TGraphicObject*)ControlObjects->Items[i])->OnResize(Sender, NewWidth, NewHeight);
	for(int i=ToolsObjects->Count-1; i>=0; i--)
		((TGraphicObject*)ToolsObjects->Items[i])->OnResize(Sender, NewWidth, NewHeight);
	for(int i=CustomObjects->Count-1; i>=0; i--)
		((TGraphicObject*)CustomObjects->Items[i])->OnResize(Sender, NewWidth, NewHeight);
}
//---------------------------------------------------------------------------

void __fastcall TGraphicObjectsContainer::Update(TMyObjectType Type, Types::TRect UpdateRect)
{
	DrawRect=UpdateRect;
	if(Type==gotControl) ControlUpdate();
	else if(Type==gotTool) ToolsUpdate();
	else if (Type==gotCustom) CustomUpdate();
}
//---------------------------------------------------------------------------

void __fastcall TGraphicObjectsContainer::RemoveChildFromList(TMyObject* o)
{
	TGraphicObject *po;

	try
	{
		po=(TGraphicObject*)o;
		if(po)
		{
			switch(po->ObjectType)
			{
				case gotControl: ControlObjects->Remove(po->Index); break;
				case gotCustom: CustomObjects->Remove(po->Index); break;
				case gotTool: ToolsObjects->Remove(po->Index); break;
			}
			if(CustomObjectsChanged) *CustomObjectsChanged=true;
		}
	}
	catch(Exception &e) {}
}
//---------------------------------------------------------------------------

void __fastcall TGraphicObjectsContainer::SmallResize()
{
	bool b=AutomaticUpdate;

	for(int i=ControlObjects->Count-1; i>=0; i--)
		((TGraphicObject*)ControlObjects->Items[i])->SmallSize=smallsize;
	for(int i=ToolsObjects->Count-1; i>=0; i--)
		((TGraphicObject*)ToolsObjects->Items[i])->SmallSize=smallsize;
	for(int i=CustomObjects->Count-1; i>=0; i--)
		((TGraphicObject*)CustomObjects->Items[i])->SmallSize=smallsize;
	AutomaticUpdate=b;
}

//---------------------------------------------------------------------------
// TGraphicObject
//---------------------------------------------------------------------------
__fastcall TGraphicObject::TGraphicObject(TMyObject *AOwner, TMyObjectType Type, Types::TRect OutputRect) : TMyObject()
{
	pressed=false;
	inrect=false;
	Rect=OutputRect;
	output=OutputRect;
	objecttype=Type;
	if(AOwner!=NULL)
	{
		index=((TMyObject*)AOwner)->AddObject((TMyObject*)this);
	}
	else Owner=NULL;
	onmouseclick=NULL;
	onmouseup=NULL;
	onmousedown=NULL;
	onmousehold=NULL;
	onmousedblclick=NULL;
	visible=true;
	mouseable=true;
	forcemouseable=false;
	dockable=false;
	allowneighbormousemove=false;
	X1=y1=0;
	fullmodality=true;
	resized=false;
// Animation
	AnimationInProgress=false;
	AnimationType=atCustom;
	LocalAnimationTimer=NULL;
	globaltimer=NULL;
	AnimationTimerItem=NULL;
	AnimationFrameInterval=25;
// End of Animation
}
//---------------------------------------------------------------------------

__fastcall TGraphicObject::~TGraphicObject()
{
// Animation
	StopAnimation();
	globaltimer=NULL;
// End of Animation
	if(owner!=NULL && index>=0)
	{
		owner->RemoveChildFromList(this);
		//Update();
	}
}
//---------------------------------------------------------------------------

bool __fastcall TGraphicObject::StartAnimation(TAnimationType aType)
{
	bool res=true;

	try
	{
		AnimationType=aType;
		if(GlobalTimer)
		{
			if(AnimationTimerItem) AnimationTimerItem->OnTimer=NULL;
			AnimationTimerItem=GlobalTimer->AddTimer(AnimationFrameInterval, OnAnimation); // and Start
			res=(AnimationTimerItem!=NULL);
		}
		else
		{
			if(LocalAnimationTimer)
			{
				LocalAnimationTimer->Stop();
				LocalAnimationTimer->OnTimer=NULL;
			}
			else LocalAnimationTimer=new TTimerThread(AnimationFrameInterval);
			LocalAnimationTimer->OnTimer=OnAnimation;
			LocalAnimationTimer->Start();
		}
		AnimationInProgress=true;
	}
	catch(...) {res=false;}
	return res;
}
//---------------------------------------------------------------------------

bool __fastcall TGraphicObject::StopAnimation()
{
	bool res=true;

	try
	{
		if(AnimationTimerItem) AnimationTimerItem->OnTimer=NULL;
		AnimationTimerItem=NULL;
		if(LocalAnimationTimer)
		{
			LocalAnimationTimer->Stop();
			LocalAnimationTimer->OnTimer=NULL;
			DismissMyThread(LocalAnimationTimer);
			LocalAnimationTimer=NULL;
		}
	}
	catch(...) {res=false;}
	return res;
}
//---------------------------------------------------------------------------

void __fastcall TGraphicObject::OnResize(TObject *Sender, int NewWidth, int NewHeight)
{
	//if(Image->Scroll
	TImgView32 *image;
	if(Owner && ((TGraphicObjectsContainer*)Owner)->Image)
	{
		image=((TGraphicObjectsContainer*)Owner)->Image;
		NewWidth-=(image->Width-image->GetViewportRect().Width());
		NewHeight-=(image->Height-image->GetViewportRect().Height());
	}
	Output=Types::TRect(0, 0, NewWidth, NewHeight);
	resized=true;
}
//---------------------------------------------------------------------------

void __fastcall TGraphicObject::writeOutput(Types::TRect value)
{
	output=value;
	Update();
}
//---------------------------------------------------------------------------

void __fastcall TGraphicObject::writePressed(bool value)
{
	if(pressed!=value)
	{
		pressed=value;
		if(Owner!=NULL) Owner->SetObjectChanged(objecttype);
	}
}
//---------------------------------------------------------------------------

void __fastcall TGraphicObject::writeInRect(bool value)
{
	if(inrect!=value)
	{
		inrect=value;
		if(indragndrop && !value) indragndrop=false;
		if(Owner!=NULL) Owner->SetObjectChanged(objecttype);
	}
}
//---------------------------------------------------------------------------

void __fastcall TGraphicObject::MouseClick(System::TObject* Sender, int X, int Y)
{
	if(OnMouseClick!=NULL) (OnMouseClick)((TObject*)this, X, Y);
}
//---------------------------------------------------------------------------

void __fastcall TGraphicObject::MouseDblClick(System::TObject* Sender, int X, int Y)
{
	if(OnMouseDblClick!=NULL) (OnMouseDblClick)((TObject*)this, X, Y);
}
//---------------------------------------------------------------------------

void __fastcall TGraphicObject::DockDrop(System::TObject* Sender, System::TObject* Fallen, int X, int Y)
{
	if(OnDockDrop!=NULL) (OnDockDrop)((TObject*)this, Fallen, X, Y);
}
//---------------------------------------------------------------------------

void __fastcall TGraphicObject::Update(TMyObjectType Type)
{
	if(Owner!=NULL)
	{
		Owner->SetObjectChanged(objecttype);
	}
	else SelfUpdate();
}
//---------------------------------------------------------------------------

void __fastcall TGraphicObject::DecreaseGlyph(TBitmap32 *InOut)
{
	TBitmap32 *bmp;

	if(InOut)
	{
		bmp=new TBitmap32();
		try
		{
			bmp->DrawMode=InOut->DrawMode;
			bmp->SetSize(InOut->Width>>1, InOut->Height>>1);
			bmp->Clear(Color32(0,0,0,0));
			bmp->Draw(Types::TRect(0, 0, bmp->Width, bmp->Height),
				Types::TRect(0, 0, InOut->Width, InOut->Height), InOut);
			//delete InOut;
			InOut->SetSizeFrom(bmp);
			InOut->Clear(Color32(0,0,0,0));
			InOut->Draw(Types::TRect(0, 0, bmp->Width, bmp->Height),
				Types::TRect(0, 0, bmp->Width, bmp->Height), bmp);
		}
		__finally
		{
			delete bmp;
		}
	}
}
//---------------------------------------------------------------------------

TBitmap32* __fastcall TGraphicObject::GetPngFromContainer(AnsiString AName)
{
	TBitmap32* res;

	if(Owner)
	{
		if(Owner->ObjectType==gotContainer &&
			((TGraphicObjectsContainer*)Owner)->PngContainer)
		{
			res=((TGraphicObjectsContainer*)Owner)->PngContainer->AddPng(AName);
		}
		else if(Owner->ObjectType==gotControl &&
			((TControlObject*)Owner)->ControlType==cotContainer &&
			((TControlObject*)Owner)->PngContainer)
		{
			res=((TControlObject*)Owner)->PngContainer->AddPng(AName);
		}
		else res=NULL;
	}
	else res=NULL;

	return res;
}
//---------------------------------------------------------------------------

void __fastcall TGraphicObject::RemovePngFromContainer(AnsiString AName)
{
	if(Owner)
	{
		if(Owner->ObjectType==gotContainer &&
			((TGraphicObjectsContainer*)Owner)->PngContainer)
		{
			((TGraphicObjectsContainer*)Owner)->PngContainer->DelPng(AName);
		}
		else if(Owner->ObjectType==gotControl &&
			((TControlObject*)Owner)->ControlType==cotContainer &&
			((TControlObject*)Owner)->PngContainer)
		{
			((TControlObject*)Owner)->PngContainer->DelPng(AName);
		}
	}
}

//---------------------------------------------------------------------------
// TProfileScroller
//---------------------------------------------------------------------------
/*
__fastcall TProfileScroller::TProfileScroller(TMyObject *AOwner, Types::TRect OutputRect,
		TProfileOutputView *OV, TObject* RadarMapManager) : TManagerObject(AOwner, gotControl, OutputRect, RadarMapManager)
{
	outputview=OV;
	OnResize((TObject*)AOwner, Rect.Width(), Rect.Height());
}
//---------------------------------------------------------------------------

TProfileOutputView* __fastcall TProfileScroller::readOutputView(void)
{
	if(Manager!=NULL && ((TRadarMapManager*)Manager)->Current!=NULL)
	{
		return ((TRadarMapManager*)Manager)->Current->OutputView;
	}
	else return NULL;
}
//---------------------------------------------------------------------------

void __fastcall TProfileScroller::MouseMove(System::TObject* Sender,
	Classes::TShiftState Shift, int X, int Y, TCustomLayer *Layer)
{
	int d, w, t;
	TProfileOutputView* OV;

	OV=OutputView;
	if(OV!=NULL && OV->Profile!=NULL)
	{
		w=Rect.Width();
		t=OV->Profile->Traces;
		if(Pressed)
		{
			if(!OV->InScrolling) OV->InScrolling=true;
			d=X1-X;
			d=(int)((float)t*(float)d/(float)w);
			OV->ScrollingRightIndex=OV->RightIndex-d;
			OV->ScrollingLeftIndex=
				OV->ScrollingRightIndex-OV->Width();
			if(OV->ScrollingLeftIndex==0)
				OV->ScrollingRightIndex=OV->Width();
			if(((TRadarMapManager*)Manager)->CurrentPath)
				((TRadarMapManager*)Manager)->CurrentPath->Update();
			//((TGraphicObjectsContainer*)Owner)->Update(ObjectType, Rect);
			Update();
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TProfileScroller::MouseUp(TObject *Sender, TMouseButton Button,
	TShiftState Shift, int X, int Y, TCustomLayer *Layer)
{
	if(Pressed && OutputView!=NULL) OutputView->ApplyScrolling();
}
//---------------------------------------------------------------------------

void __fastcall TProfileScroller::MouseClick(TObject *Sender, int X, int Y)
{
	int d, t;

	if(OutputView!=NULL && OutputView->Profile!=NULL && !OutputView->InScrolling)
	{
		t=OutputView->Profile->Traces;
		d=(int)((float)t*(float)(X-Rect.left)/(float)Rect.Width());
		//d+=OutputView->Width()>>1;
		//OutputView->ScrollingRightIndex=d;
		//OutputView->ScrollingLeftIndex=OutputView->ScrollingRightIndex-OutputView->Width();
		//if(OutputView->ScrollingLeftIndex==0) OutputView->ScrollingRightIndex=OutputView->Width();
		//((TGraphicObjectsContainer*)Owner)->Update(ObjectType, Rect);
		//OutputView->ApplyScrolling();
		OutputView->CenterAtPos(d);
		Pressed=false;
	}
}
//---------------------------------------------------------------------------

void __fastcall TProfileScroller::OnResize(TObject *Sender, int NewWidth, int NewHeight)
{
	int w, h, x, y;

	h=32; y=NewHeight-80;
	w=NewWidth*0.8;
	if(w<100) w=100;
	x=(NewWidth-w)>>1;
	Rect=Types::TRect(x, y, x+w, y+h);
}
//---------------------------------------------------------------------------

void __fastcall TProfileScroller::DrawLine32(TBitmap32 *Bmp, TColor32 Color,
		TColor32 Border, int Width, bool Shadow, TLine line)
{
	TLine32 *Line32;
	TFixedPoint LineFP[2];
	TColor32Entry ce;
	TColor32 Colors[2];

	Line32=new TLine32();
	try
	{
		Line32->EndStyle=esRounded;
		Line32->JoinStyle=jsRounded;
		LineFP[0]=FixedPoint(line.X1, line.Y1);
		LineFP[1]=FixedPoint(line.X2, line.Y2);
		Line32->SetPoints(LineFP, 1);
		if(Shadow)
		{
			Line32->Translate(Width/2, Width/2);
			Line32->Draw(Bmp, Width, Color32(64, 64, 64, 128));
		}
		Line32->SetPoints(LineFP, 1);
		if(InRect)
		{
			ce.ARGB=Color;
			if(ce.R>127) ce.R=255;
			else ce.R+=127;
			if(ce.G>127) ce.G=255;
			else ce.G+=127;
			if(ce.B>127) ce.B=255;
			else ce.B+=127;
			Colors[1]=ce.ARGB;
			Colors[0]=Color;
		}
		else
		{
			ce.ARGB=Color;
			ce.A=(Byte)((float)ce.A*0.25);
			Colors[1]=ce.ARGB;
			ce.R=(Byte)((float)ce.R*0.5);
			ce.G=(Byte)((float)ce.G*0.5);
			ce.B=(Byte)((float)ce.B*0.5);
			Colors[0]=ce.ARGB;

			ce.ARGB=Border;
			ce.A=(Byte)((float)ce.A*0.25);
			Border=ce.ARGB;
		}
		if(((TRadarMapManager*)Manager)->Settings->EasyRendering)
			Line32->Draw(Bmp, Width, Colors[0]);
		else Line32->DrawGradient(Bmp, Width, Colors, 2, 90, Border);
	}
	__finally
	{
		delete Line32;
    }
}
//---------------------------------------------------------------------------

void __fastcall TProfileScroller::Redraw(TObject *Sender, TBitmap32 *Bmp)
{
	int x, y, w, h, t, l, r;
	TLine line;
	Types::TRect tmp, rct;
	TProfileOutputView* OV;

	if(Bmp!=NULL && Rect.Width()!=0 && Rect.Height()!=0)
	{
		rct=Rect;
		y=rct.top+(rct.Height()>>1);
		w=rct.Width();
		x=rct.left;
		h=rct.Height();
		if(!InRect) h-=4;
		if(h<1) h=1;
		OV=OutputView;
		if(OV!=NULL && OV->Profile!=NULL)
		{
			t=OV->Profile->Traces;
			if(OV->InScrolling)
			{
				l=OV->ScrollingLeftIndex;
				r=OV->ScrollingRightIndex;
			}
			else
			{
				l=OV->LeftIndex;
				r=OV->RightIndex;
			}
		}
		else
		{
			l=0;
			r=1;
			t=1;
		}
		tmp=Owner->ViewPort;
		if(rct.IsInRect(tmp) && t>0)
		{
			l=(int)((float)w*(float)l/(float)t);
			r=(int)((float)w*(float)r/(float)t);
			//Bmp->FillRectT(rct.left, rct.top, rct.right, rct.bottom, Color32(255, 255, 255, 64));
			line.X1=x;
			line.X2=x+w;
			line.Y1=line.Y2=y;
			if(OV!=NULL && OV->Profile!=NULL)
			{
				if(line.IsInRect(rct))
				{
					DrawLine32(Bmp, clGreen32, clDimGray32, h, false, line);
				}
				line.X1=x+l;
				line.X2=x+r;
				line.Y1=line.Y2=y;
				if(line.IsInRect(rct))
				{
					DrawLine32(Bmp, clRed32, clDimGray32, h, false, line);
				}
			}
			else if(line.IsInRect(rct))
			{
				DrawLine32(Bmp, clDimGray32, clGray32, h, false, line);
			}
		}
	}
}*/

__fastcall TProfileScroller::TProfileScroller(TMyObject *AOwner, Types::TRect OutputRect,
		TProfileOutputView *OV, TObject* RadarMapManager) : TManagerObject(AOwner, gotControl, OutputRect, RadarMapManager)
{
	List=new TList();
	ScrollList=new TList();
	outputview=OV;
	showinfo=true;
	Background=new TBitmap32();
	Background->DrawMode=dmBlend;
	OnMouseMovePathUpdateCount=0;
	OnResize((TObject*)AOwner, Rect.Width(), Rect.Height());
}
//---------------------------------------------------------------------------

__fastcall TProfileScroller::~TProfileScroller()
{
	delete Background;
	List->Clear();
	delete List;
	for(int i=0; i<ScrollList->Count; i++)
		delete (TBitmap32*)ScrollList->Items[i];
	ScrollList->Clear();
	delete ScrollList;
}
//---------------------------------------------------------------------------

TProfileOutputView* __fastcall TProfileScroller::readOutputView(void)
{
	/*if(Manager!=NULL && ((TRadarMapManager*)Manager)->Current!=NULL)
	{
		return ((TRadarMapManager*)Manager)->Current->OutputView;
	}
	else return NULL;*/
	return outputview;
}
//---------------------------------------------------------------------------

TProfileOutputView* __fastcall TProfileScroller::readoutputview(void)
{
	TProfileOutputView* OV;

	if(List->Count>0)
	{
		OV=(TProfileOutputView*)List->Last();
	}
	else OV=NULL;
	if(OV==NULL)
	{
		if(Manager && ((TRadarMapManager*)Manager)->Current)
		{
			return ((TRadarMapManager*)Manager)->Current->OutputView;
		}
		else return NULL;
	}
	else return OV;
}
//---------------------------------------------------------------------------

void __fastcall TProfileScroller::writeoutputview(TProfileOutputView* value)
{
	TBitmap32* b;

	b=new TBitmap32();
	b->DrawMode=dmBlend;
	List->Add((TObject*)value);
	ScrollList->Add(b);
}
//---------------------------------------------------------------------------

void __fastcall TProfileScroller::MouseMove(System::TObject* Sender,
	Classes::TShiftState Shift, int X, int Y, TCustomLayer *Layer)
{
	int d, w, t;
	TProfileOutputView* OV;

	OV=OutputView;
	if(OV!=NULL && OV->Profile!=NULL)
	{
		w=Rect.Width();
		t=OV->Profile->Traces;
		if(Pressed)
		{
			if(!OV->InScrolling) OV->InScrolling=true;
			d=X1-X;
			d=(int)((float)t*(float)d/(float)w);
			OV->ScrollingRightIndex=OV->RightIndex-d;
			OV->ScrollingLeftIndex=OV->ScrollingRightIndex-OV->Width();
			if(OV->ScrollingLeftIndex==0)
				OV->ScrollingRightIndex=OV->Width();
			if(((TRadarMapManager*)Manager)->CurrentPath)
			{
				if(OnMouseMovePathUpdateCount>=OnMouseMovePathUpdateCountQty)
				{
					((TRadarMapManager*)Manager)->CurrentPath->Update();
					OnMouseMovePathUpdateCount=0;
				}
				OnMouseMovePathUpdateCount++;
			}
			Update();
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TProfileScroller::MouseUp(TObject *Sender, TMouseButton Button,
	TShiftState Shift, int X, int Y, TCustomLayer *Layer)
{
	if(Pressed && OutputView!=NULL) OutputView->ApplyScrolling();
	OnMouseMovePathUpdateCount=0;
}
//---------------------------------------------------------------------------

void __fastcall TProfileScroller::MouseClick(TObject *Sender, int X, int Y)
{
	int d, t;

	if(OutputView!=NULL && OutputView->Profile!=NULL && !OutputView->InScrolling)
	{
		t=OutputView->Profile->Traces;
		d=(int)((float)t*(float)(X-Rect.left)/(float)Rect.Width());
		/*d+=OutputView->Width()>>1;
		OutputView->ScrollingRightIndex=d;
		OutputView->ScrollingLeftIndex=OutputView->ScrollingRightIndex-OutputView->Width();
		if(OutputView->ScrollingLeftIndex==0) OutputView->ScrollingRightIndex=OutputView->Width();
		((TGraphicObjectsContainer*)Owner)->Update(ObjectType, Rect);
		OutputView->ApplyScrolling();*/
		OutputView->CenterAtPos(d);
		Pressed=false;
	}
}
//---------------------------------------------------------------------------

void __fastcall TProfileScroller::OnResize(TObject *Sender, int NewWidth, int NewHeight)
{
	int w, h, x, y;

	h=32; y=NewHeight-80;
	w=NewWidth*0.8;
	if(w<100) w=100;
	x=(NewWidth-w)>>1;
	Rect=Types::TRect(x, y, x+w, y+h);
}
//---------------------------------------------------------------------------

void __fastcall TProfileScroller::DrawLine32(TBitmap32 *Bmp, TColor32 Color,
		TColor32 Border, int Width, bool Shadow, TLine line)
{
	TLine32 *Line32;
	TFixedPoint LineFP[2];
	TColor32Entry ce;
	TColor32 Colors[2];

	Line32=new TLine32();
	try
	{
		Line32->EndStyle=esRounded;
		Line32->JoinStyle=jsRounded;
		LineFP[0]=FixedPoint(line.X1, line.Y1);
		LineFP[1]=FixedPoint(line.X2, line.Y2);
		Line32->SetPoints(LineFP, 1);
		if(Shadow)
		{
			Line32->Translate(Width/2, Width/2);
			Line32->Draw(Bmp, Width, Color32(64, 64, 64, 128));
		}
		Line32->SetPoints(LineFP, 1);
		if(InRect)
		{
			ce.ARGB=Color;
			if(ce.R>127) ce.R=255;
			else ce.R+=127;
			if(ce.G>127) ce.G=255;
			else ce.G+=127;
			if(ce.B>127) ce.B=255;
			else ce.B+=127;
			Colors[1]=ce.ARGB;
			Colors[0]=Color;
		}
		else
		{
			ce.ARGB=Color;
			ce.A=(Byte)((float)ce.A*0.35);
			Colors[1]=ce.ARGB;
			ce.R=(Byte)((float)ce.R*0.5);
			ce.G=(Byte)((float)ce.G*0.5);
			ce.B=(Byte)((float)ce.B*0.5);
			Colors[0]=ce.ARGB;

			ce.ARGB=Border;
			ce.A=(Byte)((float)ce.A*0.35);
			Border=ce.ARGB;
		}
		if(((TRadarMapManager*)Manager)->Settings->EasyRendering)
			Line32->Draw(Bmp, Width, Colors[0]);
		else Line32->DrawGradient(Bmp, Width, Colors, 2, 90, Border);
	}
	__finally
	{
		delete Line32;
    }
}
//---------------------------------------------------------------------------

void __fastcall TProfileScroller::Draw(TBitmap32 *MainBmp, TBitmap32 *Bmp, TColor32 Color, TColor32 Border,
	int x, int y, TLine line, int h, int Alpha)
{
	//return;
	try
	{
		if(MainBmp && Bmp)
		{
			if(line.Width()!=(Bmp->Width-h*2) || h!=(Bmp->Height-4))
			{
				line.X2=line.X2-line.X1+h;
				line.X1=h;
				line.Y1=line.Y2=(h >> 1)+2;
				Bmp->SetSize(line.Width()+h*2, h+4);
				Bmp->Clear(Color32(0,0,0,0));
				DrawLine32(Bmp, Color, Border, h, false, line);
			}
			if(Alpha<200) Alpha=200;
			if(!InRect) Bmp->MasterAlpha=(int)((float)Alpha*0.8);
			else Bmp->MasterAlpha=Alpha;
			try
			{
				MainBmp->Draw(x, y, Bmp);
			}
			catch(Exception &e) {}
		}
	}
	catch(Exception &e) {}
}
//---------------------------------------------------------------------------

void __fastcall TProfileScroller::Redraw(TObject *Sender, TBitmap32 *Bmp)
{
	int x, y, w, h, l, r, i, tw, th, ii;
	TLine line;
	Types::TRect tmp, rct;
	TProfileOutputView* OV;
	TColor32 c;
	TColor32Entry ce;
	AnsiString str;
	TMapPathObject *path;

	if(Bmp && Rect.Width()!=0 && Rect.Height()!=0)
	{
		rct=Rect;
		h=rct.Height();
		if(!InRect) h-=4;
		if(h<1) h=1;
		OV=OutputView;
		tmp=Owner->ViewPort;
		if(rct.IsInRect(tmp))
		{
			//Bmp->FillRectT(rct.left, rct.top, rct.right, rct.bottom, Color32(255, 255, 255, 64));
			y=rct.top+(rct.Height()>>1);
			w=rct.Width();
			x=rct.left;
			line.X1=x;
			line.X2=x+w;
			line.Y1=line.Y2=y;
			if(OV && OV->Profile)
			{
				Bmp->BeginUpdate();
				try
				{
					if(Manager && ((TRadarMapManager*)Manager)->Satellites)
					{
						ii=((TRadarMapManager*)Manager)->Satellites->IndexOf(OutputView);
						if(((TRadarMapManager*)Manager)->Satellites->Items[ii]->Path)
							path=((TRadarMapManager*)Manager)->Satellites->Items[ii]->Path;
						else path=NULL;
					}
					else path=NULL;
					if(path)
					{
						str=path->Name;
						ce.ARGB=path->Color;
						ce.R=ce.R >> 1;
						ce.G=ce.G >> 1;
						ce.B=ce.B >> 1;
						c=ce.ARGB;
					}
					else
					{
						str=OutputView->Profile->Name;
						c=clGreen32;
					}
					Draw(Bmp, Background, c, clDimGray32, line.X1-h, rct.top+(rct.Height()-h>>1), line, h, 0xFF);
					for(i=0; i<List->Count; i++)
					{
						OV=(TProfileOutputView*)List->Items[i];
						if(!OV && Manager && ((TRadarMapManager*)Manager)->Current)
							OV=((TRadarMapManager*)Manager)->Current->OutputView;
						if(OV && OV->Profile && OV->Profile->Traces>0)
						{
							if(OV->InScrolling)
							{
								l=OV->ScrollingLeftIndex;
								r=OV->ScrollingRightIndex;
							}
							else
							{
								l=OV->LeftIndex;
								r=OV->RightIndex;
							}
							l=(int)((float)w*(float)l/(float)OV->Profile->Traces);
							r=(int)((float)w*(float)r/(float)OV->Profile->Traces);
							line.X1=x+l;
							line.X2=x+r;
							//line.Y1=line.Y2=y;
							if(path)
							{
								ce.ARGB=path->SelectionColor;
								ce.R=ce.R >> 1;
								ce.G=ce.G >> 1;
								ce.B=ce.B >> 1;
								c=ce.ARGB;
							}
							else
							{
								if(i==0) c=clRed32;
								else if(i==1) c=clBlue32;
								else if(i==2) c=clYellow32;
								else c=clLime32;
							}
							Draw(Bmp, ScrollBmp[i], c, clDimGray32, line.X1-h, rct.top+(rct.Height()-h>>1), line, h, 220);
						}
					}
					if(showinfo && OutputView && OutputView->Profile)
					{
						bool b=false;

						if(str!="") str+=", ";
						str+="Traces: "+IntToStr((int)OutputView->Profile->Traces);
						str+=", Distance: ";
						if(Manager && ((TRadarMapManager*)Manager)->ReceivingProfile)
						{
							for(i=0; i<((TRadarMapManager*)Manager)->ReceivingProfilesQuantity; i++)
							{
								if(((TRadarMapManager*)Manager)->ReceivingProfiles[i]==OutputView->Profile)
								{
									b=true;
									break;
								}
							}
						}
						if(b && OutputView->Profile->CurrentTracePtr)
							str+=FloatToStrF(OutputView->Profile->CurrentTracePtr->HorPos, ffFixed, 10, 2);
						else str+=FloatToStrF(OutputView->Profile->HorRangeFull, ffFixed, 10, 2);
						b=false;
						str+=" m";
						h=rct.Height();
						Bmp->Font->Height=h*0.667;
						Bmp->Font->Color=clWhite;
						tw=Bmp->TextWidth(str);
						th=Bmp->TextHeight(str);
						while(tw>w && str.Length()>0)
						{
							str.SetLength(str.Length()-1);
							tw=Bmp->TextWidth(str+"...");
							b=true;
						}
						if(b) str+="...";
						if(tw<w && str.Length()>0 && str!="...")
							Bmp->Textout(x+((w-tw)>>1), rct.top+((h-th)>>1), str);
					}
				}
				__finally
				{
					Bmp->EndUpdate();
				}
			}
			else if(line.IsInRect(rct))
			{
				Draw(Bmp, Background, clDimGray32, clGray32, line.X1-h, rct.top+(rct.Height()-h>>1), line, h, 0xFF);
			}
		}
	}
}

//---------------------------------------------------------------------------
// TProfileLabelType
//---------------------------------------------------------------------------
TProfileLabelType StrToTProfileLabelType(AnsiString str)
{
	int n=sizeof(TProfileLabelTypeStrings);
	TProfileLabelType res=pltNone;

	for(int i=0; i<n; i++)
	{
		if(str==TProfileLabelTypeStrings[i])
		{
			res=(TProfileLabelType)i;
			break;
		}
	}
	return res;
}

//---------------------------------------------------------------------------
// TCustomProfileLabel
//---------------------------------------------------------------------------
__fastcall TCustomProfileLabel::TCustomProfileLabel(TMyObject *AOwner,
	Types::TRect OutputRect, TProfileOutputView *OV, int X, int Y, TObject* RadarMapManager)
	: TProfileObject(AOwner, OutputRect, OV, RadarMapManager)
{
	int i;

	x=X;
	y=Y;
	connectedobject=NULL;
	DescriptionStrs=new TStringList();
	attachment="";
	texticonname="";
	hottexticonname="";
	attachmenticonname="";
	hotattachmenticonname="";
	texticon=NULL;
	hottexticon=NULL;
	attachmenticon=NULL;
	hotattachmenticon=NULL;
	HotTextIconName="PNGIMAGE_148";
	HotAttachmentIconName="PNGIMAGE_147";
	TextIconName="PNGIMAGE_150";
	AttachmentIconName="PNGIMAGE_149";
	normalglyph=NULL;
	normalglyphname="";
	profileindex=0;
	labeltype=pltNone;
	if(OV && OV->Profile && Manager)
	{
		ProfRect=((TRadarMapManager*)Manager)->GetProfRect(x, y);
		i=((TRadarMapManager*)Manager)->GetProfRectIndex(ProfRect);
		if(i>=0) profileindex=i;
		if(ProfRect)
		{
			traceindex=x-ProfRect->left+OV->LeftIndex;
			trace=OV->GetTraceByIndex(traceindex, profileindex);
			if(trace) gpsconfidence=trace->GPSConfidence;
			else gpsconfidence=0;
		}
	}
	else
	{
		ProfRect=NULL;
		traceindex=-1;
		gpsconfidence=0;
		trace=NULL;
	}
	if(Manager)
	{
		do
		{
			id=((TRadarMapManager*)Manager)->LastLabelID+1;
		}
		while(((TRadarMapManager*)Manager)->MapObjects && ((TRadarMapManager*)Manager)->MapObjects->GetMapLabel(id));
		((TRadarMapManager*)Manager)->LastLabelID=id;
	}
	center=Types::TPoint(0,0);
	icons=true;
	allwayshot=false;
	created=modified=Now();
}
//---------------------------------------------------------------------------

__fastcall TCustomProfileLabel::~TCustomProfileLabel()
{
	if(connectedobject)
	{
		connectedobject->ConnectedObject=NULL;
		delete connectedobject;
		connectedobject=NULL;
	}
	//else if(normalglyph) delete normalglyph;
	RemovePngFromContainer(NormalGlyphName);
	normalglyph=NULL;
	RemovePngFromContainer(TextIconName);
	texticon=NULL;
	RemovePngFromContainer(AttachmentIconName);
	attachmenticon=NULL;
	RemovePngFromContainer(HotTextIconName);
	hottexticon=NULL;
	RemovePngFromContainer(HotAttachmentIconName);
	hotattachmenticon=NULL;
	if(DescriptionStrs) delete DescriptionStrs;
	DescriptionStrs=NULL;
	if(coordinate) delete coordinate;
	coordinate=NULL;
}
//---------------------------------------------------------------------------

/*void __fastcall TCustomProfileLabel::writeNormalGlyph(TBitmap32 *value)
{
	Glyph=NULL;
	if(normalglyph) delete normalglyph;
	normalglyph=value;
}*/
//---------------------------------------------------------------------------

void __fastcall TCustomProfileLabel::writeConnectedObject(TCustomProfileLabel* value)
{
	if(connectedobject!=value)
	{
		connectedobject=value;
		if(value!=NULL)
		{
			value->ConnectedObject=this;
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TCustomProfileLabel::writeID(unsigned int value)
{
	if(id!=value)
	{
		id=value;
		if(connectedobject) connectedobject->ID=value;
		if(Manager && ((TRadarMapManager*)Manager)->LastLabelID<id) ((TRadarMapManager*)Manager)->LastLabelID=id;
	}
}
//---------------------------------------------------------------------------

void __fastcall TCustomProfileLabel::writeProfileIndex(int value)
{
	if(profileindex!=value)
	{
		profileindex=value;
		if(OutputView!=NULL)
			trace=OutputView->GetTraceByIndex(traceindex, profileindex);
		if(trace) gpsconfidence=trace->GPSConfidence;
		else gpsconfidence=0;
		if(Manager)
		{
			ProfRect=((TRadarMapManager*)Manager)->ProfRect[profileindex];
			((TRadarMapManager*)Manager)->ResultsNotSaved=true;
		}
		if(connectedobject) connectedobject->ProfileIndex=value;
		Update();
		//if(connectedobject) connectedobject->Update();
	}
}

void __fastcall TCustomProfileLabel::writeTraceIndex(int value)
{
	if(traceindex!=value)
	{
		traceindex=value;
		if(OutputView!=NULL)
			trace=OutputView->GetTraceByIndex(traceindex, profileindex);
		if(trace) gpsconfidence=trace->GPSConfidence;
		else gpsconfidence=0;
		if(connectedobject) connectedobject->TraceIndex=value;
		Update();
		//if(connectedobject) connectedobject->Update();
		if(Manager) ((TRadarMapManager*)Manager)->ResultsNotSaved=true;
	}
}
//---------------------------------------------------------------------------

void __fastcall TCustomProfileLabel::writeColor(TColor32 value)
{
	if(color!=value)// && (Tag==(TProfileLabelType)pltVertLine || Tag==(TProfileLabelType)pltTransformed))
	{
		color=value;
		if(connectedobject) connectedobject->Color=value;
		Update();
		if(connectedobject) connectedobject->Update();
	}
}
//---------------------------------------------------------------------------

void __fastcall TCustomProfileLabel::IconsDraw(TBitmap32 *Bmp)
{
	int h=0, w;
	TBitmap32 *tmp, *texticon, *attachmenticon;

	if(icons && Bmp!=NULL && Manager && ((TRadarMapManager*)Manager)->Settings &&
		((TRadarMapManager*)Manager)->Settings->ShowLabelIcons)
	{
		try
		{
			if(InRect || AllwaysHot)
			{
				texticon=HotTextIcon;
				attachmenticon=HotAttachmentIcon;
			}
			else
			{
				texticon=TextIcon;
				attachmenticon=AttachmentIcon;
			}
			if(Description!="" && texticon) h+=texticon->Height;
			if(attachment!="" && attachmenticon) h+=attachmenticon->Height;
			tmp=new TBitmap32;
			try
			{
				tmp->DrawMode=dmBlend;
				w=0;
				if(texticon) w=texticon->Width;
				if(attachmenticon)
				{
					if(h>(Rect.Height()+1)) w+=attachmenticon->Width;
					else if(attachmenticon->Width>w) w=attachmenticon->Width;
				}
				tmp->SetSize(w, h);
				tmp->Clear(Color32(0,0,0,0));
				tmp->BeginUpdate();
				try
				{
					w=0;
					if(texticon && Description!="")
					{
						tmp->Draw(0, 0, texticon);
						if(h>(Rect.Height()+1))
						{
							w=texticon->Width+1;
							h=0;
						}
						else
						{
							w=0;
							h=texticon->Height+1;
						}
					}
					else w=h=0;
					if(attachmenticon && attachment!="") tmp->Draw(w, h, attachmenticon);
				}
				__finally
				{
					tmp->EndUpdate();
				}
				Bmp->BeginUpdate();
				try
				{
					Bmp->Draw(Rect.right+1, Rect.top, tmp);
				}
				__finally
				{
					Bmp->EndUpdate();
				}
			}
			__finally
			{
				delete tmp;
			}
		}
		catch(Exception &e) {}
	}
}
//---------------------------------------------------------------------------

void __fastcall TCustomProfileLabel::MouseUp(TObject *Sender,
	TMouseButton Button, TShiftState Shift, int X, int Y, TCustomLayer *Layer)
{
	if(icons && Button==mbRight)
	{
		LabelInfoForm->Manager=(TRadarMapManager*)Manager;
		/*if(Manager && ((TRadarMapManager*)Manager)->MapObjects &&
			((TRadarMapManager*)Manager)->MapObjects->Container)
				LabelInfoForm->CoordinateSystem=((TRadarMapManager*)Manager)->MapObjects->Container->CoordinateSystem;*/
		if(Manager && ((TRadarMapManager*)Manager)->Settings)
			LabelInfoForm->CoordinateSystem=((TRadarMapManager*)Manager)->Settings->DefaultCoordinateSystem;
		else LabelInfoForm->CoordinateSystem=csLatLon;
		LabelInfoForm->Label=this;
		LabelInfoForm->Show();
	}
}
//---------------------------------------------------------------------------

void __fastcall TCustomProfileLabel::MouseHold(TObject *Sender, TMouseButton Button,
	TShiftState Shift, int X, int Y, TCustomLayer *Layer)
{
	if(OnMouseHold!=NULL) (OnMouseHold)((TObject*)this, Button, Shift, X, Y);
	else MouseUp(Sender, mbRight, TShiftState(ssRight), X, Y, Layer);
	Pressed=false;
}
//---------------------------------------------------------------------------

void __fastcall TCustomProfileLabel::MouseClick(System::TObject* Sender, int X, int Y)
{
	if(outputview && outputview==((TRadarMapManager*)Manager)->CurrentOutputView)
		outputview->CenterAtPos(traceindex);
}
//---------------------------------------------------------------------------

TGPSCoordinate* __fastcall TCustomProfileLabel::readCoordinate()
{
	if(!coordinate || coordinate->Trace==NULL || coordinate->Trace->Index!=traceindex)
	{
		if(coordinate)
		{
			delete coordinate;
			coordinate=NULL;
		}
		if(OutputView!=NULL)
		{
			trace=OutputView->GetTraceByIndex(traceindex, profileindex);
			if(trace) coordinate=new TGPSCoordinate(NULL, trace);
		}
	}
	return coordinate;
}
//---------------------------------------------------------------------------

void __fastcall TCustomProfileLabel::writeDescription(AnsiString value)
{
	/*if(value!=description)
	{
		description=value;
		Update();
		if(connectedobject) connectedobject->Description=value;
		if(Manager) ((TRadarMapManager*)Manager)->ResultsNotSaved=true;
	}*/

	if(value!=DescriptionStrs->Text)
	{
		DescriptionStrs->Text=value;
		if(AutomaticUpdate) Update();
		if(connectedobject)
		{
			TCustomProfileLabel *tmp;

			tmp=connectedobject;
			connectedobject=NULL;
			tmp->ConnectedObject=NULL;
			tmp->Description=value;
			tmp->ConnectedObject=this;
		}
		if(Manager) ((TRadarMapManager*)Manager)->ResultsNotSaved=true;
	}
}
//---------------------------------------------------------------------------

void __fastcall TCustomProfileLabel::writeAttachment(AnsiString value)
{
	if(value!=attachment)
	{
		attachment=value;
		Update();
		if(connectedobject) connectedobject->Attachment=value;
		if(Manager) ((TRadarMapManager*)Manager)->ResultsNotSaved=true;
	}
}
//---------------------------------------------------------------------------

void __fastcall TCustomProfileLabel::writeCoordinate(TGPSCoordinate *value)
{
	if(coordinate!=value)
	{
		if(coordinate) delete coordinate;
		coordinate=value;
		Update();
		if(Manager) ((TRadarMapManager*)Manager)->ResultsNotSaved=true;
	}
}
//---------------------------------------------------------------------------

void __fastcall TCustomProfileLabel::writeNormalGlyphName(AnsiString value)
{
	if(normalglyphname!=value)
	{
		if(normalglyphname!="")
		{
			RemovePngFromContainer(normalglyphname);
			if(Glyph==normalglyph) Glyph=NULL;
			normalglyph=NULL;
		}
		if(value!="") normalglyph=GetPngFromContainer(value);
		normalglyphname=value;
	}
}

//---------------------------------------------------------------------------
// TProfileLabel
//---------------------------------------------------------------------------
__fastcall TProfileLabel::TProfileLabel(TMyObject *AOwner, Types::TRect OutputRect,
	TProfileOutputView *OV, TProfileLabelType type, int X, int Y, TObject* RadarMapManager)
	: TCustomProfileLabel(AOwner, OutputRect, OV, X, Y, RadarMapManager)
{
	Rect=Types::TRect(X-15, Y-16, X+15, Y+15);
	labeltype=type;
	Tag=(int)type;
	Initialize();
	if(OutputView && OutputView->Profiles[profileindex] && ProfRect)
	{
		sample=(int)((float)OutputView->Profiles[profileindex]->Samples*((float)(Y-ProfRect->Top)/(float)ProfRect->Height()));
		//z=OutputView->Profiles[i]->Depth*((float)sample)/(float)OutputView->Profiles[i]->Samples;
	}
	else
	{
		sample=0;//-1;
		//z=0;//-1;
	}
	if(connectedobject) connectedobject->Z=Z;
}
//---------------------------------------------------------------------------

__fastcall TProfileLabel::TProfileLabel(TMyObject *AOwner, Types::TRect OutputRect,
	TProfileOutputView *OV, TProfileLabelType type, int trcIndex, int smpl, TColor32 Cl, TObject* RadarMapManager)
	: TCustomProfileLabel(AOwner, OutputRect, OV, 0, 0, RadarMapManager)
{
	labeltype=type;
	Tag=(int)type;
	Initialize();
	AutomaticUpdate=false;
	try
	{
		Color=Cl;
		TraceIndex=trcIndex;
		sample=smpl;
		//z=aZ;
		if(connectedobject) connectedobject->Z=Z;
	}
	__finally
	{
		AutomaticUpdate=true;
	}
}
//---------------------------------------------------------------------------

__fastcall TProfileLabel::TProfileLabel(TMyObject *AOwner, Types::TRect OutputRect,
	TProfileOutputView *OV, int trcIndex, TColor32 Cl, TObject* RadarMapManager)
	: TCustomProfileLabel(AOwner, OutputRect, OV, 0, 0, RadarMapManager)
{
	Tag=(int)pltVertLine;
	AutomaticUpdate=false;
	try
	{
		Color=Cl;
		TraceIndex=trcIndex;
		sample=0;
		//z=0;
		if(connectedobject) connectedobject->Z=Z;
		Initialize();
	}
	__finally
	{
		AutomaticUpdate=true;
	}
}
//---------------------------------------------------------------------------

void __fastcall TProfileLabel::Initialize(void)
{
	//LockedGlyph=LoadPNG32Resource((System::UnicodeString)"PNGIMAGE_54");
	LockedGlyphName="PNGIMAGE_54";
	LockedGlyph=GetPngFromContainer(LockedGlyphName);
	//HotLockedGlyph=LoadPNG32Resource((System::UnicodeString)"PNGIMAGE_53");
	HotLockedGlyphName="PNGIMAGE_53";
	HotLockedGlyph=GetPngFromContainer(HotLockedGlyphName);
	profileobjecttype=potProfileLabel;
	locked=true;
	hotglyphname="";
	smallglyphname="";
	hotglyph=NULL;
	smallglyph=NULL;
	switch(ProfileLabelType)
	{
		case pltCircleRed:
			NormalGlyphName="PNGIMAGE_58";
			HotGlyphName="PNGIMAGE_56";
			SmallGlyphName="PNGIMAGE_55";
			Color=Color32(255, 0, 0, 255);
			break;
		case pltSquareRed:
			NormalGlyphName="PNGIMAGE_72";
			HotGlyphName="PNGIMAGE_71";
			SmallGlyphName="PNGIMAGE_70";
			Color=Color32(255, 0, 0, 255);
			break;
		case pltTriangleRed:
			NormalGlyphName="PNGIMAGE_81";
			HotGlyphName="PNGIMAGE_80";
			SmallGlyphName="PNGIMAGE_79";
			Color=Color32(255, 0, 0, 255);
			break;
		case pltCircleGreen:
			NormalGlyphName="PNGIMAGE_63";
			HotGlyphName="PNGIMAGE_62";
			SmallGlyphName="PNGIMAGE_61";
			Color=Color32(0, 255, 0, 255);
			break;
		case pltSquareGreen:
			NormalGlyphName="PNGIMAGE_69";
			HotGlyphName="PNGIMAGE_68";
			SmallGlyphName="PNGIMAGE_67";
			Color=Color32(0, 255, 0, 255);
			break;
		case pltTriangleGreen:
			NormalGlyphName="PNGIMAGE_78";
			HotGlyphName="PNGIMAGE_77";
			SmallGlyphName="PNGIMAGE_76";
			Color=Color32(0, 255, 0, 255);
			break;
		case pltCircleBlue:
			NormalGlyphName="PNGIMAGE_60";
			HotGlyphName="PNGIMAGE_59";
			SmallGlyphName="PNGIMAGE_57";
			Color=Color32(0, 0, 255, 255);
			break;
		case pltSquareBlue:
			NormalGlyphName="PNGIMAGE_66";
			HotGlyphName="PNGIMAGE_65";
			SmallGlyphName="PNGIMAGE_64";
			Color=Color32(0, 0, 255, 255);
			break;
		case pltTriangleBlue:
			NormalGlyphName="PNGIMAGE_75";
			HotGlyphName="PNGIMAGE_74";
			SmallGlyphName="PNGIMAGE_73";
			Color=Color32(0, 0, 255, 255);
			break;
		case pltWaterCircle:
			NormalGlyphName="PNGIMAGE_344";
			HotGlyphName="PNGIMAGE_345";
			SmallGlyphName="PNGIMAGE_343";
			Color=Color32(47, 158, 194);
			break;
		case pltWaterRect:
			NormalGlyphName="PNGIMAGE_348";
			HotGlyphName="PNGIMAGE_349";
			SmallGlyphName="PNGIMAGE_347";
			Color=Color32(47, 158, 194);
			break;
		case pltWaterTriangle:
			NormalGlyphName="PNGIMAGE_401";
			HotGlyphName="PNGIMAGE_402";
			SmallGlyphName="PNGIMAGE_400";
			Color=Color32(47, 158, 194);
			break;
		case pltElectroCircle:
			NormalGlyphName="PNGIMAGE_320";
			HotGlyphName="PNGIMAGE_321";
			SmallGlyphName="PNGIMAGE_319";
			Color=Color32(194, 47, 47);
			break;
		case pltElectroRect:
			NormalGlyphName="PNGIMAGE_324";
			HotGlyphName="PNGIMAGE_325";
			SmallGlyphName="PNGIMAGE_323";
			Color=Color32(194, 47, 47);
			break;
		case pltElectroTriangle:
			NormalGlyphName="PNGIMAGE_404";
			HotGlyphName="PNGIMAGE_405";
			SmallGlyphName="PNGIMAGE_403";
			Color=Color32(194, 47, 47);
			break;
		case pltGasCircle:
			NormalGlyphName="PNGIMAGE_328";
			HotGlyphName="PNGIMAGE_329";
			SmallGlyphName="PNGIMAGE_327";
			Color=Color32(194, 194, 47);
			break;
		case pltGasRect:
			NormalGlyphName="PNGIMAGE_332";
			HotGlyphName="PNGIMAGE_333";
			SmallGlyphName="PNGIMAGE_331";
			Color=Color32(194, 194, 47);
			break;
		case pltGasTriangle:
			NormalGlyphName="PNGIMAGE_407";
			HotGlyphName="PNGIMAGE_408";
			SmallGlyphName="PNGIMAGE_406";
			Color=Color32(194, 194, 47);
			break;
		case pltGasHighCircle:
			NormalGlyphName="PNGIMAGE_410";
			HotGlyphName="PNGIMAGE_411";
			SmallGlyphName="PNGIMAGE_409";
			Color=Color32(230, 115, 0);
			break;
		case pltGasHighRect:
			NormalGlyphName="PNGIMAGE_413";
			HotGlyphName="PNGIMAGE_414";
			SmallGlyphName="PNGIMAGE_412";
			Color=Color32(230, 115, 0);
			break;
		case pltGasHighTriangle:
			NormalGlyphName="PNGIMAGE_416";
			HotGlyphName="PNGIMAGE_417";
			SmallGlyphName="PNGIMAGE_415";
			Color=Color32(230, 115, 0);
			break;
		case pltSewageCircle:
			NormalGlyphName="PNGIMAGE_337";
			HotGlyphName="PNGIMAGE_334";
			SmallGlyphName="PNGIMAGE_336";
			Color=Color32(194, 47, 194);
			break;
		case pltSewageRect:
			NormalGlyphName="PNGIMAGE_340";
			HotGlyphName="PNGIMAGE_341";
			SmallGlyphName="PNGIMAGE_339";
			Color=Color32(194, 47, 194);
			break;
		case pltSewageTriangle:
			NormalGlyphName="PNGIMAGE_419";
			HotGlyphName="PNGIMAGE_420";
			SmallGlyphName="PNGIMAGE_418";
			Color=Color32(194, 47, 194);
			break;
		case pltTelecomCircle:
			NormalGlyphName="PNGIMAGE_430";
			HotGlyphName="PNGIMAGE_431";
			SmallGlyphName="PNGIMAGE_429";
			Color=Color32(120, 194, 47);
			break;
		case pltTelecomRect:
			NormalGlyphName="PNGIMAGE_433";
			HotGlyphName="PNGIMAGE_434";
			SmallGlyphName="PNGIMAGE_432";
			Color=Color32(120, 194, 47);
			break;
		case pltTelecomTriangle:
			NormalGlyphName="PNGIMAGE_436";
			HotGlyphName="PNGIMAGE_437";
			SmallGlyphName="PNGIMAGE_435";
			Color=Color32(120, 194, 47);
			break;
		case pltFiberCircle:
			NormalGlyphName="PNGIMAGE_422";
			HotGlyphName="PNGIMAGE_423";
			SmallGlyphName="PNGIMAGE_421";
			Color=Color32(47, 194, 120);
			break;
		case pltFiberRect:
			NormalGlyphName="PNGIMAGE_425";
			HotGlyphName="PNGIMAGE_426";
			SmallGlyphName="PNGIMAGE_424";
			Color=Color32(47, 194, 120);
			break;
		case pltFiberTriangle:
			NormalGlyphName="PNGIMAGE_427";
			HotGlyphName="PNGIMAGE_428";
			SmallGlyphName="PNGIMAGE_438";
			Color=Color32(47, 194, 120);
			break;
		case pltAutoDetectCustom:
		case pltAutoDetect:
		case pltAutoDetectPos:
			NormalGlyphName="PNGIMAGE_277";
			HotGlyphName="PNGIMAGE_275";
			SmallGlyphName="PNGIMAGE_274";
			Color=Color32(255, 0, 0, 255);
			break;
		case pltAutoDetectNeg:
			NormalGlyphName="PNGIMAGE_273";
			HotGlyphName="PNGIMAGE_271";
			SmallGlyphName="PNGIMAGE_270";
			Color=Color32(0, 0, 255, 255);
			break;
		case pltVertLine:
		case pltTransformed:
			NormalGlyphName="PNGIMAGE_84";
			//HotGlyph=LoadPNG32Resource((System::UnicodeString)"PNGIMAGE_83");
			//SmallGlyph=LoadPNG32Resource((System::UnicodeString)"PNGIMAGE_82");
			HotGlyphName="PNGIMAGE_83";
			SmallGlyphName="PNGIMAGE_82";
			break;
		case pltNone:
		case pltCustom:
			//NormalGlyph=NULL;
			//HotGlyph=NULL;
			//SmallGlyph=NULL;
			NormalGlyphName="";
			HotGlyphName="";
			SmallGlyphName="";
			break;
	}
	if(NormalGlyph) center=Types::TPoint(NormalGlyph->Width>>1, NormalGlyph->Height>>1);
	else center=Types::TPoint(0, 0);
	VertLineWidth=4;
	old_traceindex=old_sample=old_profindex=-1;
	data=NULL;
}
//---------------------------------------------------------------------------

__fastcall TProfileLabel::~TProfileLabel()
{
	//delete LockedGlyph;
	RemovePngFromContainer(LockedGlyphName);
	LockedGlyph=NULL;
	//delete HotLockedGlyph;
	RemovePngFromContainer(HotLockedGlyphName);
	HotLockedGlyph=NULL;
	//if(hotglyph!=NULL) //delete hotglyph;
	RemovePngFromContainer(HotGlyphName);
	hotglyph=NULL;
	//if(smallglyph!=NULL && ConnectedObject==NULL) //delete smallglyph;
	RemovePngFromContainer(SmallGlyphName);
	smallglyph=NULL;
	if(Tag & pltAutoDetect && data)
	{
		delete (TFoundObject*)data;
		data=NULL;
	}
}
//---------------------------------------------------------------------------

/*void __fastcall TProfileLabel::writeHotGlyph(TBitmap32 *value)
{
	Glyph=NULL;
	if(hotglyph!=NULL) delete hotglyph;
	hotglyph=value;
}
//---------------------------------------------------------------------------

void __fastcall TProfileLabel::writeSmallGlyph(TBitmap32 *value)
{
	if(smallglyph!=NULL && ConnectedObject==NULL) delete smallglyph;
	smallglyph=value;
	if(ConnectedObject!=NULL)
	{
		ConnectedObject->NormalGlyph=value;
		ConnectedObject->Update();
	}
}*/
//---------------------------------------------------------------------------

void __fastcall TProfileLabel::writeSample(int value)
{
	sample=value;
	Update();
	if(Manager) ((TRadarMapManager*)Manager)->ResultsNotSaved=true;
	//z=OutputView->Profiles[i]->Depth*((float)sample)/(float)OutputView->Profiles[i]->Samples;
	if(ConnectedObject)
	{
		ConnectedObject->Z=Z;
    }
}
//---------------------------------------------------------------------------

void __fastcall TProfileLabel::writeLocked(bool value)
{
	locked=value;
	Update();
	if(Manager) ((TRadarMapManager*)Manager)->ResultsNotSaved=true;
}
//---------------------------------------------------------------------------

void __fastcall TProfileLabel::writeInRect(bool value) //overload
{
	if(inrect!=value)
	{
		inrect=value;
		if(indragndrop && !value) indragndrop=false;
		if(inrect && hotglyph!=NULL) Glyph=hotglyph;
		else Glyph=NormalGlyph;
		Update();
	}
}
//---------------------------------------------------------------------------

void __fastcall TProfileLabel::Redraw(TObject *Sender, TBitmap32 *Bmp)
{
	Types::TRect rct, src;
	int X, Y;
	TBitmap32 *tmp;
	TColor32Entry ce;
	TLine32 *Line32;
	TFixedPoint LineFP[2];

	if(Visible && outputview!=NULL && Bmp!=NULL && Output.Width()!=0 && Output.Height()!=0)
	{
		if(!ProfRect) ProfRect=((TRadarMapManager*)Manager)->ProfRect[profileindex]; //((TRadarMapManager*)Manager)->GetProfRect(Xx, Yy);
		//i=((TRadarMapManager*)Manager)->GetProfRectIndex(ProfRect);
		if(TraceIndex>=outputview->LeftIndex && TraceIndex<outputview->RightIndex && outputview->Profiles[profileindex])
		{
			tmp=new TBitmap32();
			try
			{
				tmp->DrawMode=dmBlend;
				try
				{
					if(Glyph==NULL)
					{
						if(inrect)
						{
							if(hotglyph!=NULL) Glyph=hotglyph;
							else Glyph=NormalGlyph;
						}
						else Glyph=NormalGlyph;
					}
					if(ProfileLabelType!=pltVertLine)
					{
						if(Glyph!=NULL)
						{
							tmp->SetSize(Glyph->Width, Glyph->Height);
							tmp->BeginUpdate();
							if(ProfileLabelType==pltTransformed)
							{
								ce.ARGB=Color;
								if(inrect) ce.A=255;
								else ce.A=218;
								tmp->FillRectTS(9, 11, 22, 24, ce.ARGB);
							}
							tmp->Draw(0, 0, Glyph);
						}
						else
						{
							tmp->SetSize(32, 32);
							tmp->BeginUpdate();
							ce.ARGB=Color;
							if(inrect) ce.A=255;
							else ce.A=218;
							tmp->FillRectTS(0, 0, 32, 32, ce.ARGB);
						}
						src=tmp->ClipRect;
						if(Locked)
						{
							if(inrect && HotLockedGlyph)
								tmp->Draw(src.right-HotLockedGlyph->Width, src.bottom-HotLockedGlyph->Height, HotLockedGlyph);
							else if(LockedGlyph)
								tmp->Draw(src.right-LockedGlyph->Width, src.bottom-LockedGlyph->Height, LockedGlyph);
						}
					}
					else
					{
						tmp->SetSize(32, Output.Height());
						tmp->BeginUpdate();
						ce.ARGB=Color;
						if(inrect && !locked) ce.A=255;
						else ce.A=205;
						tmp->LineT(16, 0, 16, Output.Height(), ce.ARGB);
						tmp->FillRectTS(11, 13, 22, 24, ce.ARGB);
						if(Glyph!=NULL) tmp->Draw(0, 0, Glyph);
						src=tmp->ClipRect;
						if(Locked)
						{
							if(inrect && HotLockedGlyph)
								tmp->Draw(src.right-HotLockedGlyph->Width, 32-HotLockedGlyph->Height, HotLockedGlyph);
							else if(LockedGlyph)
								tmp->Draw(src.right-LockedGlyph->Width, 32-LockedGlyph->Height, LockedGlyph);
						}
					}
					if(Tag & pltAutoDetect && data)
					{
						TFoundObject* fo=(TFoundObject*)data;
						int tw, th;
						float f;
						AnsiString str;

						if(fo->Weight>0)
						{
							tmp->Font->Name="Tahoma";
							tmp->Font->Size=8;
							tmp->Font->Style=TFontStyles();
							str=FloatToStrF(fo->Weight*100., ffFixed, 3, 0)+"%";
							tmp->RenderText(2, 2, str, 2, clYellow32);
						}
					}
				}
				__finally
				{
					tmp->EndUpdate();
				}
				X=TraceIndex-outputview->LeftIndex+ProfRect->Left;
				X-=center.x;//tmp->Width>>1;
				if(ProfileLabelType!=pltVertLine)
				{
					Y=(((float)Sample/(float)outputview->Profiles[profileindex]->Samples)*(float)ProfRect->Height())+ProfRect->Top;
					Y-=center.y;//tmp->Height>>1;
					Rect=Types::TRect(X, Y, X+tmp->Width, Y+tmp->Height);
				}
				else
				{
					Y=0;
					Rect=Types::TRect(X, 0, X+tmp->Width, tmp->Width);//tmp->Height);
				}
				Bmp->BeginUpdate();
				try
				{
					Bmp->Draw(X, Y, tmp);
				}
				__finally
				{
					Bmp->EndUpdate();
				}
				if(icons) TCustomProfileLabel::IconsDraw(Bmp);
			}
			__finally
			{
				delete tmp;
			}
		}
		else
		{
			Rect=Types::TRect(0, 0, 0, 0);
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TProfileLabel::MouseMove(System::TObject* Sender,
	Classes::TShiftState Shift, int X, int Y, TCustomLayer *Layer)
{
	int ti, i;

	if(OutputView)
	{
		if(!Locked && Pressed)
		{
			if(ProfileLabelType==pltVertLine)
			{
				Tag=(int)pltTransformed;
				/*NormalGlyph=LoadPNG32Resource((System::UnicodeString)"PNGIMAGE_84");
				HotGlyph=LoadPNG32Resource((System::UnicodeString)"PNGIMAGE_83");
				SmallGlyph=LoadPNG32Resource((System::UnicodeString)"PNGIMAGE_82");*/
				NormalGlyphName="PNGIMAGE_84";
				HotGlyphName="PNGIMAGE_83";
				SmallGlyphName="PNGIMAGE_82";
				dY=0;
			}
			X-=dX;
			Y-=dY;
			x=X;
			y=Y;
			ProfRect=((TRadarMapManager*)Manager)->GetProfRect(Xx, Yy);
			i=((TRadarMapManager*)Manager)->GetProfRectIndex(ProfRect);
			if(i>=0)
			{
				ProfileIndex=i;
			}
			if(OutputView->Profiles[profileindex] && ProfRect)
			{
				ti=X-ProfRect->left+OutputView->LeftIndex;
				sample=(int)((float)OutputView->Profiles[profileindex]->Samples*((float)(Y-ProfRect->Top)/(float)ProfRect->Height()));
				if(ti>OutputView->RightIndex) TraceIndex=OutputView->RightIndex;
				else if(ti<OutputView->LeftIndex) TraceIndex=OutputView->LeftIndex;
				else TraceIndex=ti;
				if(sample<0) sample=0;
				else if(sample>OutputView->Profiles[profileindex]->Samples) sample=OutputView->Profiles[profileindex]->Samples;
				//z=OutputView->Profiles[profileindex]->Depth*((float)sample-OutputView->Profiles[profileindex]->ZeroPoint)/(float)OutputView->Profiles[profileindex]->Samples;
				if(ConnectedObject)
				{
					ConnectedObject->TraceIndex=TraceIndex;
					ConnectedObject->Z=Z;
					//ConnectedObject->Update();
				}
			}
		}
	}
}
//---------------------------------------------------------------------------

float __fastcall TProfileLabel::readZ()
{
	int i;
	float res=0.;

	if(Manager && OutputView)
	{
		if(OutputView->Profiles[profileindex])
			res=OutputView->Profiles[profileindex]->Depth*((float)sample-OutputView->Profiles[profileindex]->ZeroPoint)/
				(float)OutputView->Profiles[profileindex]->Samples;

	}
	return res;
}
//---------------------------------------------------------------------------

void __fastcall TProfileLabel::MouseDown(TObject *Sender,
	TMouseButton Button, TShiftState Shift, int X, int Y, TCustomLayer *Layer)
{
	dX=X1-(Rect.left+center.x);
	dY=Y1-(Rect.top+center.y);
	old_traceindex=TraceIndex;
	old_profindex=ProfileIndex;
	old_sample=Sample;
}
//---------------------------------------------------------------------------

void __fastcall TProfileLabel::MouseUp(TObject *Sender,
	TMouseButton Button, TShiftState Shift, int X, int Y, TCustomLayer *Layer)
{
	TCustomProfileLabel::MouseUp(Sender, Button, Shift, X, Y, Layer);
	if(!locked && Button==mbLeft)
	{
		modified=Now();
		if(Manager) ((TRadarMapManager*)Manager)->ResultsNotSaved=true;
	}
}
//---------------------------------------------------------------------------

void __fastcall TProfileLabel::OnResize(TObject *Sender, int NewWidth, int NewHeight)
{
	TImgView32 *image;
	if(Owner && ((TGraphicObjectsContainer*)Owner)->Image)
	{
		image=((TGraphicObjectsContainer*)Owner)->Image;
		NewWidth-=(image->Width-image->GetViewportRect().Width());
		NewHeight-=(image->Height-image->GetViewportRect().Height());
	}
	Rect=Types::TRect(0, 0, NewWidth, NewHeight);
	Update();
}
//---------------------------------------------------------------------------

void __fastcall TProfileLabel::MouseDblClick(System::TObject* Sender, int X, int Y) //Unlock
{
	Locked=!Locked;
	modified=Now();
}
//---------------------------------------------------------------------------
