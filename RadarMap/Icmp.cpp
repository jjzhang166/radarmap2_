//---------------------------------------------------------------------------
#pragma hdrstop

#include "Icmp.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

__fastcall Icmp::TPing::TPing(bool AThrowExceptions)
{
	int res;

	error=false;
	errorstring="";
	laststr="";
	newlineevent=CreateEvent(NULL, false, false, NULL);
	onrequestupdate=NULL;
	outputlines=NULL;
	lasttime=-1;
	laststatus=IP_STATUS_BASE;
	EndlessThrd=NULL;
	dwAddress=NULL;
	ipInfo.Ttl=255;
	ipInfo.Tos=0;
	ipInfo.Flags=0;
	ipInfo.OptionsSize=0;
	ipInfo.OptionsData=NULL;
	bufsize=32;
	timeout=1000;
	old_addr="";
	ValidIP=false;
	ValidAddr="";
	throwexceptions=AThrowExceptions;
	try
	{
		res=WSAStartup(0x0101, &wsaData);
		try
		{
			if(res)
			{
				ErrStr="Cannot initiates Winsock with error code: ";
				switch(res)
				{
					case WSASYSNOTREADY: ErrStr+="WSASYSNOTREADY"; break;
					case WSAVERNOTSUPPORTED: ErrStr+="WSAVERNOTSUPPORTED"; break;
					case WSAEINPROGRESS: ErrStr+="WSAEINPROGRESS"; break;
					case WSAEPROCLIM: ErrStr+="WSAEPROCLIM"; break;
					case WSAEFAULT: ErrStr+="WSAEFAULT"; break;
					default: ErrStr+=IntToStr(res);
				}
			}
			// Check WinSock version
			if (wsaData.wVersion!=0x0101) ErrStr="WinSock version 1.1 not supported";
			hndlFile=IcmpCreateFile();
			if(hndlFile==INVALID_HANDLE_VALUE)
			{
				ErrStr="Unable to open handle with error: "+IntToStr((int)GetLastError());
			}
		}
		__finally
		{
			if(Err) WSACleanup();
		}
	}
	catch(Exception &e)
	{
		ErrStr=e.Message;
	}
	if(Err)
	{
		if(hndlFile) IcmpCloseHandle(hndlFile);
		hndlFile=NULL;
		LastStr="Cannot initialize TPing class with error - "+ErrStr;
		if(throwexceptions) throw Exception(LastStr);
	}
}
//---------------------------------------------------------------------------

__fastcall Icmp::TPing::~TPing()
{
	StopEndlessPing();
	if(!Err)
	{
		if(hndlFile) IcmpCloseHandle(hndlFile);
		hndlFile=NULL;
		WSACleanup();
		//FreeLibrary(hndlIcmp);
	}
	CloseHandle(newlineevent);
}
//---------------------------------------------------------------------------

AnsiString __fastcall Icmp::TPing::DecodeIcmpResult(long Result, PICMP_ECHO_REPLY pEchoReply)
{
	AnsiString str;

	switch(Result)
	{
		case IP_SUCCESS:
			if(pEchoReply)
			{
				str="Reply from "+(AnsiString)inet_ntoa(ReplyAddr)+
				": bytes="+IntToStr(bufsize)+" time";
				if(pEchoReply->RoundTripTime<1) str+="<1";
				else str+="="+IntToStr((int)pEchoReply->RoundTripTime);
				str+="ms TTL="+IntToStr(pEchoReply->Options.Ttl);
			}
			else str="Reply successfull";
			break;
		case IP_BUF_TOO_SMALL: str="Reply buffer too small"; break;
		case IP_DEST_NET_UNREACHABLE: str="Destination network unreachable"; break;
		case IP_DEST_HOST_UNREACHABLE: str="Destination host unreachable"; break;
		case IP_DEST_PROT_UNREACHABLE: str="Destination protocol unreachable"; break;
		case IP_DEST_PORT_UNREACHABLE: str="Destination port unreachable"; break;
		case IP_NO_RESOURCES: str="Insufficient IP resources were available"; break;
		case IP_BAD_OPTION: str="Bad IP option specified"; break;
		case IP_HW_ERROR: str="Hardware error occurred"; break;
		case IP_PACKET_TOO_BIG: str="Packet was too big"; break;
		case IP_REQ_TIMED_OUT: str="Request timed out"; break;
		case IP_BAD_REQ: str="Bad request"; break;
		case IP_BAD_ROUTE: str="Bad route"; break;
		case IP_TTL_EXPIRED_TRANSIT: str="TTL expired in transit"; break;
		case IP_TTL_EXPIRED_REASSEM: str="TTL expired during fragment reassembly"; break;
		case IP_PARAM_PROBLEM: str="Parameter problem"; break;
		case IP_SOURCE_QUENCH: str="Datagrams are arriving too fast to be processed and datagrams may have been discarded"; break;
		case IP_OPTION_TOO_BIG: str="IP option too big"; break;
		case IP_BAD_DESTINATION: str="Bad destination"; break;
		case IP_GENERAL_FAILURE: str="General failure"; break;
		default: str="Unknown status";
	}
	return str;
}
//---------------------------------------------------------------------------

void __fastcall Icmp::TPing::StopEndlessPing()
{
	int t;
	TEndlessPingThread *Thrd=EndlessThrd;

	if(Thrd)
	{
		Thrd->DisconnectFromOwner();
		t=Thrd->Timeout;
		Thrd->Terminate();
		Sleep(t);//+1000);
		delete Thrd;
		if(EndlessThrd==Thrd) EndlessThrd=NULL;
	}
}
//---------------------------------------------------------------------------

int __fastcall Icmp::TPing::Ping(AnsiString Addr, bool Endless, bool ShowTitle)
{
	AnsiString str;
	char *SendData;
	LPVOID ReplyBuffer = NULL;
	LPHOSTENT pHost;
	DWORD ReplySize = 0;
	int result, i;
	unsigned long ipaddr = INADDR_NONE, ul;

	result=-1;
	if(!Err && Addr!="" && Addr.Length()>0)
	{
		i=Addr.Pos(".000");
		if(i>0)
		{
			Addr[i+1]=' ';
			Addr[i+2]=' ';
		}
		i=Addr.Pos(".00");
		if(i>0) Addr[i+1]=' ';
		i=Addr.Pos(".0");
		if(i>0 && Addr[i+2]!='.') Addr[i+1]=' ';
		while(Addr.Length()>0 && Addr.Pos(" ")>0)
		{
			Addr.Delete(Addr.Pos(" "), 1);
		}
		if(Endless)
		{
			if(EndlessThrd)
			{
				unsigned long t=EndlessThrd->Timeout;
				new TSingleMethodThread(&StopEndlessPing);
				ul=GetTickCount();
				while(!EndlessThrd->MyTerminated && (GetTickCount()-ul)<t*2) Sleep(10);
			}
			EndlessThrd=new TEndlessPingThread(this, Addr);
		}
		else
		{
			if(old_addr!=Addr)
			{
				iaDest.s_addr=inet_addr(Addr.c_str());
				if(iaDest.s_addr==INADDR_NONE) pHost=gethostbyname(Addr.c_str());
				else pHost=gethostbyaddr((const char *)&iaDest, sizeof(struct in_addr), AF_INET);
				if(pHost)
				{
					ValidAddr=(AnsiString)inet_ntoa((*(LPIN_ADDR)pHost->h_addr_list[0]));
					if(iaDest.s_addr==INADDR_NONE) ValidHost=(AnsiString)pHost->h_name+" ["+ValidAddr+"]";
					else ValidHost=(AnsiString)inet_ntoa((*(LPIN_ADDR)pHost->h_addr_list[0]));
				}
				else
				{
					ValidHost=ValidAddr=Addr;
					//ValidIP=true;
					//LastStr="Could not find host "+Addr;
				}
				ValidIP=true;
				old_addr=Addr;
			}
			if(ValidIP)
			{
				if(ShowTitle) LastStr="Pinging "+ValidHost+" with "+IntToStr(bufsize)+" bytes of data:";
				ReplySize=sizeof(ICMP_ECHO_REPLY)+2*bufsize;
				try
				{
					ReplyBuffer=(VOID*)malloc(ReplySize+bufsize);
					try
					{
						if(ReplyBuffer)
						{
							SendData=new char[bufsize];
							try
							{
								for(int i=0; i<bufsize; i++) SendData[i]=(char)i;
								ipaddr=inet_addr(ValidAddr.c_str());
								if(ipaddr!=INADDR_NONE)
								{
									dwRet=IcmpSendEcho(hndlFile, ipaddr, SendData, bufsize,
										&ipInfo, ReplyBuffer, ReplySize+bufsize, timeout);
									if(dwRet)
									{
										PICMP_ECHO_REPLY pEchoReply=(PICMP_ECHO_REPLY)ReplyBuffer;

										ReplyAddr.S_un.S_addr = pEchoReply->Address;
										LastStr=DecodeIcmpResult(pEchoReply->Status, pEchoReply);
										if(pEchoReply->Status==IP_SUCCESS) result=pEchoReply->RoundTripTime;
										else result=-(long)pEchoReply->Status;
										laststatus=pEchoReply->Status;
									}
									else
									{
										result=-1;
										laststatus=dwRet;
										LastStr=DecodeIcmpResult(GetLastError(), NULL);
									}
								}
								else
								{
									LastStr="Ping request could not find host "+Addr;
								}
							}
							__finally
							{
								delete[] SendData;
							}
						}
						else
						{
							LastStr="Unable to allocate memory";
						}
					}
					__finally
					{
						if(ReplyBuffer) free(ReplyBuffer);
					}
				}
				catch(Exception &e)
				{
					ErrStr=e.Message;
				}
				if(Err)
				{
					if(throwexceptions) throw Exception(ErrStr);
					LastStr=ErrStr;
					error=false;
				}
			}
		}
	}
	lasttime=result;
	return result;
}
//---------------------------------------------------------------------------

AnsiString __fastcall Icmp::TPing::GetPingCommandLine(AnsiString Addr, bool Endless)
{
	AnsiString res="";

	res="ping "+Addr+" -l "+IntToStr(BufSize)+" -i "+IntToStr(TTL)+" -v "+
		IntToStr(TOS)+" -w "+IntToStr(Timeout);
	if(FragmentFlag) res+=" -f";
	if(ReverseFlag) res+=" -a";
	if(Endless) res+=" -t";
	return res;
}
//---------------------------------------------------------------------------

void __fastcall Icmp::TPing::writeLastStr(AnsiString value)
{
	laststr=value;
	SetEvent(newlineevent);
	if(onrequestupdate) (onrequestupdate)();
	if(EndlessThrd) EndlessThrd->MySynchronize(VisualizeChanges);
	else VisualizeChanges();
}
