//---------------------------------------------------------------------------


#pragma hdrstop

#include "VectorMap.h"
#include "Manager.h"

//---------------------------------------------------------------------------
// TDXFMapsContainer
//---------------------------------------------------------------------------
void __fastcall TDXFMapsContainer::LoadData(AnsiString Filename, HANDLE AStopItEvent, bool WithUpdate, TCoordinateSystem CS, bool Preview)
{
	bool Terminated;
	double coef, max, ww, hh, dd;
	int q, qq;
	TDXFLayer *layer;

	if(CS==csLatLon && Manager)
	{
		((TRadarMapManager*)Manager)->ShowMessageObject(NULL, ((TRadarMapManager*)Manager)->MapMessages,
			TMessageType::mtWarning, mvtDisapering, 10000, "Latitude-Longitude Coordinate system is not aplicable for DXF Maps", "Warning");
	}
	DXF->EnlargeDrawingTextHeight=((TRadarMapSettings*)radarmapsettings)->DXFEnlargeText;
	DXF->Progress=progress;
	if(!DXF->LoadData(Filename, AStopItEvent, Preview))
	{
		background=NULL;
		throw Exception("File does not exists!");
	}
	else
	{
		try
		{
			filename=Filename;
			coordinatesystem=CS;
			klicnummer=ExtractFileName(Filename);
			if(DXF->MinCorner && DXF->MaxCorner && (DXF->UpperCorner.X-DXF->LowerCorner.X==0.0 || DXF->UpperCorner.Y-DXF->LowerCorner.Y==0.0))
			{
				Lower=DoublePoint(DXF->MinCorner->X, DXF->MinCorner->Y);
				Upper=DoublePoint(DXF->MaxCorner->X, DXF->MaxCorner->Y);
			}
			else
			{
				Lower=DXF->LowerCorner;
				Upper=DXF->UpperCorner;
			}
			LatLonRectUpdate();
			ww=LatLonRect->Width(coordinatesystem);
			hh=LatLonRect->Height(coordinatesystem);
			/*if(ww>300 || hh>300.) coef=7.;
			else coef=14.;
			//if(radarmapsettings) max=((TRadarMapSettings*)radarmapsettings)->MaximumLayerSizePX;
			//else max=3500;
			//max+=((TRadarMapSettings*)radarmapsettings)->DoubleMaximumLayerSizePX*max;
			if(Screen->DesktopWidth>Screen->DesktopHeight) max=Screen->DesktopWidth+(Screen->DesktopHeight>>1);
			else max=Screen->DesktopHeight+(Screen->DesktopWidth>>1);
			coef+=((TRadarMapSettings*)radarmapsettings)->DoubleMaximumLayerSizePX*coef;
			//while(ww*coef>max || hh*coef>max) coef/=2.;
			if(ww*coef>max || hh*coef>max)
			{
				if(hh>ww) coef=max/hh;
				else coef=max/ww;
			}
			else if(ww*coef<MaxBitmapWidth || hh*coef<MaxBitmapHeight)
			{
				if(hh>ww) coef=ww/Screen->DesktopWidth;
				else coef=hh/Screen->DesktopHeight;
				if(ww*coef>max || hh*coef>max)
				{
					if(hh>ww) coef=max/hh;
					else coef=max/ww;
				}
			}
			if(Preview) coef/=7.;*/
			if(MaxBitmapWidth>MaxBitmapHeight) max=MaxBitmapWidth;
			else max=MaxBitmapHeight;
			if(hh<ww)
			{
				coef=max/hh;
				if(Preview) coef*=0.5*(double)Screen->DesktopHeight/(double)max;
			}
			else
			{
				coef=max/ww;
				if(Preview) coef*=0.5*(double)Screen->DesktopWidth/(double)max;
			}
			background=new TBitmap32();
			background->DrawMode=dmBlend;
			background->BeginUpdate();
			try
			{
				background->SetSize((int)(ww*coef), (int)(hh*coef));
				background->Clear(((TRadarMapSettings*)radarmapsettings)->DXFBackgroundColor);//0xff212830);
				if(DXF->Layers->Count>0)
				{
					if(!Preview) q=1;
					else q=DXF->Layers->Count;
					qq=0;
					for(int j=0; j<q; j++)
					{
						layer=(TDXFLayer*)DXF->Layers->Items[j];
						if(Preview) layer->TextHeightCoef=coef*2;
						if(layer) qq+=(int)layer->Render(background, ((TRadarMapSettings*)radarmapsettings)->DXFBackgroundColor,
							((TRadarMapSettings*)radarmapsettings)->DXFColorInvertThreshold, LatLonRect->DoubleRect(coordinatesystem));
							//TDoubleRect(LatLonRect->LeftTop->RdX, LatLonRect->LeftTop->RdY,
							//LatLonRect->RightBottom->RdX, LatLonRect->RightBottom->RdY));
						if(AStopItEvent && WaitForSingleObject(AStopItEvent, 0)==WAIT_OBJECT_0)
						{
							if(qq==j+1) qq=q;
							break;
						}
					}
					/*if(qq<q) ((TRadarMapManager*)Manager)->ShowMessageObject(NULL, ((TRadarMapManager*)Manager)->MapMessages,
						TMessageType::mtWarning, mvtDisapering, 10000, "Wrong Coordinate system is chosen for this DXF Map", "Warning");*/
				}
			}
			__finally
			{
				background->EndUpdate();
			}
			//ApplyScaling(background); There is no sence to do it
			if(background!=NULL)
			{
				UtilityBmp->SetSize(background->Width, background->Height);
				InfoBmp->SetSize(background->Width, background->Height);
			}
		}
		__finally
		{
			if(WithUpdate) Update();

			LockedCS=true;
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TDXFMapsContainer::UtilityUpdate()
{
	TDXFLayer *layer;
	int i, j;

	UtilityBmp->BeginUpdate();
	try
	{
		UtilityBmp->Clear(Color32(0,0,0,0));
		if(Visible)
		{
			if(progress) progress->Max+=DXF->Layers->Count;
			for(i=1; i<DXF->Layers->Count; i++) // Layer 0 is Background
			{
				layer=(TDXFLayer*)DXF->Layers->Items[i];
				if(layer && layer->Utility && layer->Visible)
					layer->Render(UtilityBmp, ((TRadarMapSettings*)radarmapsettings)->DXFBackgroundColor,
						((TRadarMapSettings*)radarmapsettings)->DXFColorInvertThreshold,
							LatLonRect->DoubleRect(coordinatesystem));
						//TDoubleRect(LatLonRect->LeftTop->RdX, LatLonRect->LeftTop->RdY,
						//LatLonRect->RightBottom->RdX, LatLonRect->RightBottom->RdY));
				if(progress) progress->StepIt();
			}
		}
	}
	__finally
	{
		UtilityBmp->EndUpdate();
		//ApplyScaling(UtilityBmp); There is no sence to do it
	}
	if(Owner!=NULL) Owner->SetObjectChanged(gotUtility);
	//UtilityLayer->Update();
}
//---------------------------------------------------------------------------

void __fastcall TDXFMapsContainer::InfoUpdate()
{
	TDXFLayer *layer;
	int i, j;

	InfoBmp->BeginUpdate();
	try
	{
		InfoBmp->Clear(Color32(0,0,0,0));
		if(Visible)
		{
			if(progress) progress->Max+=DXF->Layers->Count;
			for(i=1; i<DXF->Layers->Count; i++) // Layer 0 is Background
			{
				layer=(TDXFLayer*)DXF->Layers->Items[i];
				if(layer && !layer->Utility && layer->Visible)
					layer->Render(InfoBmp, ((TRadarMapSettings*)radarmapsettings)->DXFBackgroundColor,
						((TRadarMapSettings*)radarmapsettings)->DXFColorInvertThreshold,
							LatLonRect->DoubleRect(coordinatesystem));
						//TDoubleRect(LatLonRect->LeftTop->RdX, LatLonRect->LeftTop->RdY,
						//LatLonRect->RightBottom->RdX, LatLonRect->RightBottom->RdY));
				if(progress) progress->StepIt();
			}
		}
	}
	__finally
	{
		InfoBmp->EndUpdate();
		//ApplyScaling(UtilityBmp); There is no sence to do it
	}
	if(Owner!=NULL) Owner->SetObjectChanged(gotInfo);
	//InfoLayer->Update();
}
//---------------------------------------------------------------------------

void __fastcall TDXFMapsContainer::Update()
{
    DXF->EnlargeDrawingTextHeight=((TRadarMapSettings*)radarmapsettings)->DXFEnlargeText;
	TMapsContainer::Update();
}
//---------------------------------------------------------------------------

void __fastcall TDXFMapsContainer::RenderMap()
{
	if(radarmapsettings && DXF->Layers->Count>0)
	{
		TDXFLayer *layer;

		Background->BeginUpdate();
		try
		{
			Background->Clear(((TRadarMapSettings*)radarmapsettings)->DXFBackgroundColor);
			if(Visible)
			{
				DXF->EnlargeDrawingTextHeight=((TRadarMapSettings*)radarmapsettings)->DXFEnlargeText;
				layer=(TDXFLayer*)DXF->Layers->Items[0];
				if(layer && layer->Visible)
				{
					layer->Render(Background, ((TRadarMapSettings*)radarmapsettings)->DXFBackgroundColor,
						((TRadarMapSettings*)radarmapsettings)->DXFColorInvertThreshold,
							LatLonRect->DoubleRect(coordinatesystem));
						//TDoubleRect(LatLonRect->LeftTop->RdX, LatLonRect->LeftTop->RdY,
						//LatLonRect->RightBottom->RdX, LatLonRect->RightBottom->RdY));
				}
			}
		}
		__finally
		{
			Background->EndUpdate();
		}
		UtilityUpdate();
		InfoUpdate();
	}
}
//---------------------------------------------------------------------------

void __fastcall TDXFMapsContainer::GetLayers(TCheckBoxTreeView *TreeView, TTreeNode *Root)
{
	TTreeNode *node;
	TDXFLayer *layer;
	int i, j, k;

	if(TreeView==NULL) return;

	if(Root==NULL)
	{
		TreeView->Items->Clear();
		Root=TreeView->Items->Add(NULL, Klicnummer);
		TreeView->SetChecking(Root, Visible);
	}
	Root->Data=(void*)((TMapsContainer*)this);
	for(int i=0; i<DXF->Layers->Count; i++)
	{
		layer=(TDXFLayer*)DXF->Layers->Items[i];
		if(layer)
		{
			node=TreeView->Items->AddChild(Root, layer->Name);
			TreeView->SetChecking(node, layer->Visible);
			node->Data=(void*)layer;
			if(TreeView->Images)
			{
				node->ImageIndex=(int)layer->LayerType;
				node->SelectedIndex=(int)layer->LayerType;
			}
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TDXFMapsContainer::SetLayers(TCheckBoxTreeView *TreeView, TTreeNode *Root)
{
	int i;
	TDXFLayer *layer;
	TTreeNode *node;
	TMapsContainer *mc;

	if(TreeView==NULL) return;

	AutomaticUpdate=false;
	try
	{
		try
		{
			if(!Root) Root=TreeView->Items->GetFirstNode();
			if(Root)
			{
				mc=(TMapsContainer*)Root->Data;
				if(mc->MapType==mtDXF && Root->Text==Klicnummer)
				{
					Visible=TreeView->Checked(Root);
					node=Root->getFirstChild();
					while(node!=NULL)
					{
						layer=(TDXFLayer*)node->Data;
						if(layer)
						{
							layer->ChangeLayerType((TDXFLayerType)node->ImageIndex);
							layer->Visible=TreeView->Checked(node);
						}
						node=Root->GetNextChild(node);
					}
				}
			}
		}
		catch(Exception &e) {}
	}
	__finally
	{
		AutomaticUpdate=true;
		Update();
	}
}

#pragma package(smart_init)
