//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Preview.h"
#include "Manager.h"
#include "DbDXFMapsContainer.h"
#include "BitmapGDI.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "GR32_Image"
#pragma resource "*.dfm"
TPreviewForm *PreviewForm;
bool PrevieFormVisible;
//---------------------------------------------------------------------------
__fastcall TPreviewForm::TPreviewForm(TComponent* Owner, TObject* RadarMapManager)
	: TForm(Owner)
{
	Manager=RadarMapManager;
	if(Manager && ((TRadarMapManager*)Manager)->Settings->MapPreview) NoPreview=true;
	else NoPreview=false;
	openorsave=true;
	previewinprogress=previewthreadid=0;
	LastPreviewThrd=NULL;
	myprogress=new TToolProgressBar(NULL, Image, NULL);
}
//---------------------------------------------------------------------------

void __fastcall TPreviewForm::FormResize(TObject *Sender)
{
	if(NoPreview) PreviewWriteText(Image, "No preview available.");
}
//---------------------------------------------------------------------------

void __fastcall MyGetClientRect(__in HWND hWnd, __out LPRECT lpRect)
{
	bool b;
	HWND hw;
	GetWindowRect(hWnd, lpRect);
}
//---------------------------------------------------------------------------

void __fastcall TPreviewForm::DoPreview(AnsiString FileName)
{
	if(Manager && ((TRadarMapManager*)Manager)->Settings->MapPreview)
	{
		AnsiString temp;

		if(!Visible)
		{
			Types::TRect Rect;
			if(openorsave) MyGetClientRect(OpenDialog->Handle, &Rect);
			else MyGetClientRect(SaveDialog->Handle, &Rect);
			Left=Rect.left+566;//Rect.left-Width-10;
			Top=Rect.top;
			Show();
		}
		ProcessMessages();
		//DeleteFile(temp);
		try
		{
			if(LastPreviewThrd)
			{
				LastPreviewThrd->StopIt();
				WaitOthers();
			}
		}
		catch(...) {}
		LastPreviewThrd=new TPreviewThread(this, Image, FileName, ((TRadarMapManager*)Manager)->Settings, &NoPreview);
	}
}
//---------------------------------------------------------------------------

void __fastcall TPreviewForm::OpenDialogSelectionChange(TObject *Sender)
{
	if (GetFileAttributes(this->OpenDialog->FileName.t_str())!=FILE_ATTRIBUTE_DIRECTORY)
	{
		UnicodeString ext = ExtractFileExt(this->OpenDialog->FileName).UpperCase();

		if(ext!=NULL && ext!=".DBSL" && ext!="")
		{
			DoPreview(OpenDialog->FileName);
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TPreviewForm::OpenDialogClose(TObject *Sender)
{
	Close();
	PrevieFormVisible=false;
}
//---------------------------------------------------------------------------

void __fastcall TPreviewForm::SaveDialogSelectionChange(TObject *Sender)
{
//	if(GetFileAttributes(SaveDialog->FileName.t_str())!=FILE_ATTRIBUTE_DIRECTORY)
//		DoPreview(SaveDialog->FileName);
}
//---------------------------------------------------------------------------

void __fastcall TPreviewForm::OpenDialogShow(TObject *Sender)
{
	openorsave=true;
}
//---------------------------------------------------------------------------

void __fastcall TPreviewForm::SaveDialogShow(TObject *Sender)
{
	openorsave=false;
}
//---------------------------------------------------------------------------

void __fastcall TPreviewForm::GMLOpenDialogSelectionChange(TObject *Sender)
{
//	if(GetFileAttributes(GMLOpenDialog->FileName.t_str())!=FILE_ATTRIBUTE_DIRECTORY)
//		DoPreview(GMLOpenDialog->FileName);
}
//---------------------------------------------------------------------------

void PreviewWriteText(TImgView32 *Image, AnsiString str)
{
	TBitmap32 *bmp;
	int w, h;

	bmp=Image->Bitmap;
	bmp->SetSize(Image->Width, Image->Height);
	bmp->Clear(clWhite32);
	//str="No preview available.";
	bmp->Font->Name="Tahoma";
	bmp->Font->Size=9;
	bmp->Font->Color=(TColor)RGB(128, 128, 128);
	w=bmp->TextWidth(str);
	h=bmp->TextHeight(str);
	bmp->Textout((bmp->Width-w)>>1, (bmp->Height-h)>>1, str);
}

//---------------------------------------------------------------------------
// TPreviewThread
//---------------------------------------------------------------------------
__fastcall TPreviewThread::TPreviewThread(TObject* AOwner, TImgView32 *img,
	AnsiString fn, void *ASettings, bool *np) : TThread(true)
{
	Owner=AOwner;
	Bitmap=NULL;
	Image=img;
	FileName=fn;
	settings=ASettings;
	if(settings) temp=((TRadarMapSettings*)settings)->RadarMapDirectory+"\\";
	temp+="temp"+IntToStr((int)ThreadID)+".tmp";
	FreeOnTerminate=true;
	NoPreview=np;
	Container=NULL;
	StopItEvent=CreateEvent(NULL, true, false, NULL);

	Resume();
}
//---------------------------------------------------------------------------

void __fastcall TPreviewThread::Execute()
{
	//Container=new TXMLMapsContainer(NULL);
	Container=NULL;
	try
	{
		try
		{
			((TPreviewForm*)Owner)->MyProgress->Show(0, false);
			if(FileExists(FileName))
			{
				((TPreviewForm*)Owner)->PreviewInProgress++;
				((TPreviewForm*)Owner)->PreviewThreadID=ThreadID;
				ProcessMessages();
				UnicodeString ext=ExtractFileExt(FileName);
				ResetEvent(StopItEvent);
				if(ext!=NULL) ext=ext.UpperCase();
				if(ext==".XML")
				{
					Container=new TXMLMapsContainer(NULL, settings, NULL);
					Container->Progress=((TPreviewForm*)Owner)->MyProgress;
					Container->GeneratePreview(FileName, StopItEvent);
					((TPreviewForm*)Owner)->PreviewInProgress--;
				}
				else if(ext==".DXF")
				{
					Container=new TDXFMapsContainer(NULL, settings, NULL);
					Container->Progress=((TPreviewForm*)Owner)->MyProgress;
					CopyFile(FileName.c_str(), temp.c_str(), false);
					Container->GeneratePreview(temp, StopItEvent);
					((TPreviewForm*)Owner)->PreviewInProgress--;
					DeleteFile(temp);
				}
				else if(ext==".PNG" || ext==".JPEG" || ext==".JPG" || ext==".BMP" || ext==".GIF" || ext==".TIF" || ext==".TIFF" || ext==".EMF" || ext==".WMF")
				{
					Container=new TImageMapsContainer(NULL, settings, NULL);
					Container->Progress=((TPreviewForm*)Owner)->MyProgress;
					CopyFile(FileName.c_str(), (temp+ext).c_str(), false);
					((TImageMapsContainer*)Container)->PreviewFileName=FileName;
					Container->GeneratePreview((temp+ext), StopItEvent);
					((TPreviewForm*)Owner)->PreviewInProgress--;
					DeleteFile((temp+ext));
				}
				else if(ext==".DBSL")
				{
					Container=new TDbDXFMapsContainer(NULL, settings, NULL);
					Container->Progress=((TPreviewForm*)Owner)->MyProgress;
					CopyFile(FileName.c_str(), temp.c_str(), false);
					Container->GeneratePreview(temp, StopItEvent);
					((TPreviewForm*)Owner)->PreviewInProgress--;
					DeleteFile(temp);
				}
				else
				{
					//Container=new T(NULL, settings);
					//Container->Progress=((TPreviewForm*)Owner)->ProgressBar;
					//CopyFile(FileName.c_str(), temp.c_str(), false);
					((TPreviewForm*)Owner)->PreviewInProgress--;
					//if(Container) Container->GeneratePreview(temp, StopItEvent);
					DeleteFile(temp);
				}
			}
			//ProcessMessages();
			if(Container) Bitmap=Container->Background;
			else Bitmap=NULL;
			//((TPreviewForm*)Owner)->PreviewInProgress--;
		}
		catch(Exception &e)	{;}
	}
	__finally
	{
		((TPreviewForm*)Owner)->MyProgress->Hide(false);
		if(PrevieFormVisible && (TPreviewForm*)Owner && ((TPreviewForm*)Owner)->Visible)
		{
			if(Image!=NULL)
			{
				if(((TPreviewForm*)Owner)->PreviewThreadID==ThreadID)
				{
					Synchronize(Visualize);
					((TPreviewForm*)Owner)->PreviewThreadID=0;
				}
			}
			if(Container) delete Container;
			Container=NULL;
			try
			{
				if(FileExists(temp)) DeleteFile(temp);
			}
			catch (Exception &e) {}
			if(((TPreviewForm*)Owner)->LastPreviewThrd==this)
				((TPreviewForm*)Owner)->LastPreviewThrd=NULL;
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TPreviewThread::StopIt()
{
	try
	{
		if(Container) Container->Progress=NULL;
		SetEvent(StopItEvent);
	}
	catch(...) {}
}
//---------------------------------------------------------------------------

void __fastcall TPreviewThread::Visualize()
{
	TBitmap32 *bmp, *tmp;
	AnsiString str;
	try
	{
		tmp=new TBitmap32;
		tmp->DrawMode=dmBlend;
		try
		{
			bmp=Image->Bitmap;
			bmp->BeginUpdate();
			if(Bitmap!=NULL && Bitmap->Width!=0 && Bitmap->Height!=0)
			{
				//bmp->SetSize(Bitmap->Width, Bitmap->Height);
				//bmp->Draw(0, 0, Bitmap);
				tmp->SetSize(Image->Width, Image->Height);
				ScaleBitmap32(Bitmap, tmp, (float)Image->Width/(float)Bitmap->Width,
					(float)Image->Height/(float)Bitmap->Height, false);
				bmp->SetSize(Image->Width, Image->Height);
				bmp->Clear(clWhite32);
				bmp->Draw(Types::TRect(0, 0, Image->Width, Image->Height),
					Types::TRect(0, 0, Image->Width, Image->Height), tmp);//Bitmap);
				*NoPreview=false;
			}
			else
			{
				PreviewWriteText(Image, "No preview available.");
				*NoPreview=true;
			}
			bmp->EndUpdate();
		}
		catch(Exception &e)	{}
	}
	__finally
	{
		delete tmp;
		Image->Refresh();
		ProcessMessages();
	}
}
//---------------------------------------------------------------------------

void __fastcall TPreviewForm::FormShow(TObject *Sender)
{
	PrevieFormVisible=true;
}
//---------------------------------------------------------------------------

void __fastcall TPreviewForm::FormClose(TObject *Sender, TCloseAction &Action)
{
	try
	{
		if(LastPreviewThrd) LastPreviewThrd->StopIt();
	}
	catch(...) {}
}
//---------------------------------------------------------------------------
