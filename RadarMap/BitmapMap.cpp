//---------------------------------------------------------------------------


#pragma hdrstop

#include "BitmapMap.h"
#include "Manager.h"
#include "BitmapGDI.h"

#pragma package(smart_init)

//---------------------------------------------------------------------------
// TBitmapMapsContainer
//---------------------------------------------------------------------------
__fastcall TBitmapMapsContainer::TBitmapMapsContainer(TMyObject *AOwner, void* ASettings, TLockedDoubleRect *AViewportArea)
	: TMapsContainer(AOwner, ASettings, AViewportArea)
{
	maptype=mtBitmap;
	bmpGBLocker=new TBusyLocker();
	bmpgb=NULL;
	bmpUtil=NULL;
	bmpInf=NULL;
	rawpxrect=NULL;
}
//---------------------------------------------------------------------------

__fastcall TBitmapMapsContainer::~TBitmapMapsContainer()
{
	bmpGBLocker->UnLock();
	if(bmpgb)
	{
		delete bmpgb;
		bmpgb=NULL;
	}
	if(bmpUtil) delete bmpUtil;
	if(bmpInf) delete bmpInf;
	if(rawpxrect) delete rawpxrect;
	delete bmpGBLocker;
}
//---------------------------------------------------------------------------

TBitmap32* __fastcall TBitmapMapsContainer::readBmpGB()
{
	TBitmap32* res;

	bmpGBLocker->SequentLock();
	try
	{
		try
		{
			res=bmpgb;
		}
		catch(...)
		{
			res=NULL;
		}
	}
	__finally
	{
		bmpGBLocker->UnLock();
	}
	return res;
}
//---------------------------------------------------------------------------

void __fastcall TBitmapMapsContainer::writeBmpGB(TBitmap32 *value)
{
	bmpGBLocker->SequentLock();
	try
	{
		try
		{
			bmpgb=value;
		}
		catch(...) {}
	}
	__finally
	{
		bmpGBLocker->UnLock();
	}
}
//---------------------------------------------------------------------------

void __fastcall TBitmapMapsContainer::DrawXMLBitmap(TBitmap32 *Dst, TXMLMapBitmap *map)
{
	TBitmap32 *bmp;

	if(map!=NULL && map->Visible)
	{
		if(FileExists(map->FileName))
		{
			bmp=NULL;
			try
			{
				bmp=LoadPNG32(map->FileName);
				if(bmp!=NULL)
				{
					if(rawpxrect) Dst->Draw(rawpxrect->Left, rawpxrect->Top, bmp);
					else Dst->Draw(0, 0, bmp);
				}
			}
			__finally
			{
				if(bmp!=NULL) delete bmp;
			}
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TBitmapMapsContainer::ApplyScaling(TBitmap32 *Src, TBitmap32 *Dst, TColor32 ClearColor)
{
	if(Src)
	{
		if(radarmapsettings)
		{
			if(ViewportArea) ScaleBitmap32(Src, Dst, ViewportArea,
				Types::TRect(0, 0, MaxBitmapWidth, MaxBitmapHeight), false);
			/*TBitmap32* tmp;
			float w, h, scaleX, vpw, vph, coef;
			Types::TRect rct, canvas;
			int d;

			w=Src->Width;
			h=Src->Height;
			if(((TRadarMapSettings*)radarmapsettings)->MapScaleApplying)
				scaleX=CalculScaleX(coordinatesystem, (int)w, (int)h, CoordinatesRect);
			else scaleX=1.;
			//if(ViewportArea) ScaleBitmap32(Src, Dst, ViewportArea,
			//	Types::TRect(0, 0, MaxBitmapWidth*scaleX, MaxBitmapHeight), false);
			if(ViewportArea)
			{
				if(MaxBitmapWidth>MaxBitmapHeight)
					coef=(float)MaxBitmapHeight/(float)MaxBitmapWidth;
				else coef=(float)MaxBitmapWidth/(float)MaxBitmapHeight;
				rct.Left=(int)(ViewportArea->Left*(float)Src->Width+0.5);
				rct.Top=(int)(ViewportArea->Top*(float)Src->Height+0.5);
				rct.Right=(int)(ViewportArea->Right*(float)Src->Width+0.5);
				rct.Bottom=(int)(ViewportArea->Bottom*(float)Src->Height+0.5);
				if(MaxBitmapWidth<=Src->Width || rct.Width()>=Src->Width)
				{
					canvas.Left=(MaxBitmapWidth-Src->Width)>>1;
					canvas.Right=canvas.Left+Src->Width;
				}
				else
				{
					canvas.Left=(MaxBitmapWidth-(int)((double)Src->Width/ViewportArea->Width()))>>1;
					canvas.Right=canvas.Left+(int)((double)Src->Width/ViewportArea->Width());
					d=(Src->Width-rct.Width())>>1;
					rct.Left-=d;
					rct.Right+=d;
				}
				if(MaxBitmapHeight<=Src->Height || rct.Height()>=Src->Height)
				{
					canvas.Top=(MaxBitmapHeight-Src->Height)>>1;
					canvas.Bottom=canvas.Top+Src->Height;
				}
				else
				{
					canvas.Top=(MaxBitmapHeight-(int)((double)Src->Height/ViewportArea->Height()))>>1;
					canvas.Bottom=canvas.Top+(int)((double)Src->Height/ViewportArea->Height());
					d=(Src->Height-rct.Height())>>1;
					rct.Top-=d;
					rct.Bottom+=d;
				}
			}
			else rct=Types::TRect(0, 0, Src->Width, Src->Height);
			if(!Dst) tmp=new TBitmap32();
			else tmp=Dst;
			try
			{
				tmp->BeginUpdate();
				try
				{
					tmp->SetSize(MaxBitmapWidth, MaxBitmapHeight);
					tmp->Clear(ClearColor);
					tmp->Draw(canvas, rct, Src);
				}
				catch(Exception &e)
				{
					if(!Dst) delete tmp;
					tmp=NULL;
				}
			}
			__finally
			{
				if(tmp)
				{
					tmp->EndUpdate();
					if(!Dst)
					{
						Src->SetSizeFrom(tmp);
						Src->Draw(0, 0, tmp);
						delete tmp;
					}
				}
			}/**/
		}
	}
	else if(Dst)
	{
		Dst->SetSize(MaxBitmapWidth, MaxBitmapHeight);
		Dst->Clear(ClearColor);
	}
}
//---------------------------------------------------------------------------

void __fastcall TBitmapMapsContainer::LatLonRectScaling(TDoubleRect *drct)
{
	double d;

	if(drct && bmpGB)
	{
		if(bmpGB->Width>MaxBitmapWidth)
		{
			d=drct->Width()*(double)((bmpGB->Width-MaxBitmapWidth)>>1)/(double)bmpGB->Width;
			drct->Left+=d;
			drct->Right-=d;
		}
		if(bmpGB->Height>MaxBitmapHeight)
		{
			d=drct->Height()*(double)((bmpGB->Height-MaxBitmapHeight)>>1)/(double)bmpGB->Height;
			drct->Bottom-=d;
			drct->Top+=d;
		}
	}
}
//---------------------------------------------------------------------------

Types::TRect __fastcall TBitmapMapsContainer::GetBitmapRect()
{
	if(bmpGB) return Types::TRect(0, 0, bmpGB->Width, bmpGB->Height);
	else if(Background) return Types::TRect(0, 0, Background->Width, Background->Height);
	else return Types::TRect(0, 0, 0, 0);
}

void __fastcall TBitmapMapsContainer::RenderMap()
{
	if(bmpGB)
	{
		if(!background)
		{
			background=new TBitmap32();
			background->SetSize(MaxBitmapWidth, MaxBitmapHeight);
			background->Clear(clWhite32);
		}
		Background->BeginUpdate();
		try
		{
			if(Visible)
			{
				if(radarmapsettings)
					ApplyScaling(bmpGB, Background, ((TRadarMapSettings*)radarmapsettings)->DXFBackgroundColor);
				else ApplyScaling(bmpGB, Background, clWhite32);
			}
		}
		__finally
		{
			Background->EndUpdate();
		}
		UtilityUpdate();
		InfoUpdate();
	}
}
//---------------------------------------------------------------------------

void __fastcall TBitmapMapsContainer::UtilityUpdate()
{
	TXMLMapThemesCollection *Collection;
	TXMLMapTheme *Theme;
	int i, j;

	if(!bmpUtil)
	{
		bmpUtil=new TBitmap32();
		bmpUtil->DrawMode=dmBlend;
		//bmpUtil->SetSize(UtilityBmp->Width, UtilityBmp->Height);
		bmpUtil->SetSize(bmpGB->Width, bmpGB->Height);
	}
	bmpUtil->BeginUpdate();
	try
	{
		bmpUtil->Clear(Color32(0,0,0,0));
		if(Visible)
		{
			if(progress) progress->Max+=Collections->Count;
			for(i=0; i<Collections->Count; i++)
			{
				Collection=(TXMLMapThemesCollection*)Collections->Items[i];
				if(Collection!=NULL)
					for(j=0; j<Collection->Count; j++)
					{
						Theme=(TXMLMapTheme*)Collection->GetObject(j);
						DrawXMLBitmap(bmpUtil, Theme->GetObject(xmbtLocation));
					}
				if(progress) progress->StepIt();
			}
		}
	}
	__finally
	{
		bmpUtil->EndUpdate();
		ApplyScaling(bmpUtil, UtilityBmp);
	}
	if(Owner!=NULL) Owner->SetObjectChanged(gotUtility);
}
//---------------------------------------------------------------------------

void __fastcall TBitmapMapsContainer::InfoUpdate()
{
	TXMLMapThemesCollection *Collection;
	TXMLMapTheme *Theme;
	int i, j, k;

	if(!bmpInf)
	{
		bmpInf=new TBitmap32();
		bmpInf->DrawMode=dmBlend;
		//bmpInf->SetSize(InfoBmp->Width, InfoBmp->Height);
		bmpInf->SetSize(bmpGB->Width, bmpGB->Height);
	}
	bmpInf->BeginUpdate();
	try
	{
		bmpInf->Clear(Color32(0,0,0,0));
		if(Visible)
		{
			if(progress) progress->Max+=Collections->Count;
			for(i=0; i<Collections->Count; i++)
			{
				Collection=(TXMLMapThemesCollection*)Collections->Items[i];
				if(Collection!=NULL)
					for(j=0; j<Collection->Count; j++)
					{
						Theme=(TXMLMapTheme*)Collection->GetObject(j);
						DrawXMLBitmap(bmpInf, Theme->GetObject(xmbtElgenTopo));
						DrawXMLBitmap(bmpInf, Theme->GetObject(xmbtTopo));
						DrawXMLBitmap(bmpInf, Theme->GetObject(xmbtDimensions));
						DrawXMLBitmap(bmpInf, Theme->GetObject(xmbtAnnotation));
					}
				if(progress) progress->StepIt();
			}
		}
	}
	__finally
	{
		bmpInf->EndUpdate();
		ApplyScaling(bmpInf, InfoBmp);
	}
	if(Owner!=NULL) Owner->SetObjectChanged(gotInfo);
}

//---------------------------------------------------------------------------
// TXMLMapsContainer
//---------------------------------------------------------------------------
void __fastcall TXMLMapsContainer::ZoomUpdate()
{
	LatLonRectUpdate();
	if(ViewportArea)
	{
		if(bmpGB)
		{
			if(radarmapsettings)
				ApplyScaling(bmpGB, Background, ((TRadarMapSettings*)radarmapsettings)->DXFBackgroundColor);
			else ApplyScaling(bmpGB, Background, clWhite32);
		}
		if(bmpUtil)	ApplyScaling(bmpUtil, UtilityBmp);
		if(bmpInf) ApplyScaling(bmpInf, InfoBmp);
	}
}
//---------------------------------------------------------------------------

void __fastcall TXMLMapsContainer::FindMapsForTheme(TXMLMapTheme *Theme, AnsiString path, AnsiString Mask, TXMLMapBitmapType T)
{
	WIN32_FIND_DATA FileData;
	HANDLE hSearch;
	AnsiString S;

	if(Theme)
	{
		hSearch=FindFirstFile((path+Mask).c_str(), &FileData);
		if(hSearch!=INVALID_HANDLE_VALUE)
		{
			try
			{
				do
				{
					S=FileData.cFileName;
					if(S!="." && S!="..")
					{
						if(FileData.dwFileAttributes!=FILE_ATTRIBUTE_DIRECTORY)
						{
							S=path+S;
							Theme->AddObject(new TXMLMapBitmap(NULL, T, S));
						}
					}
				} while(FindNextFile(hSearch, &FileData));
			}
			__finally
			{
				FindClose(hSearch);
			}
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TXMLMapsContainer::LoadData(AnsiString Filename, HANDLE AStopItEvent, bool WithUpdate, TCoordinateSystem CS, bool Preview)
{
	TXMLExplorer *XMLExplorer;
	AnsiString Dir;
	AnsiString str, str2, str3, str4;
	bool Terminated;
	TXMLMapThemesCollection *Collection;
	TXMLMapTheme *Theme;
	TXMLMapBitmap *map;
	TXMLMapBitmapType T;
	int w=0, h=0;

	Collection=NULL;
	Theme=NULL;
	map=NULL;
	XMLExplorer=new TXMLExplorer(NULL, Filename);
	try
	{
		if(radarmapsettings)
		{
			filename=Filename;
			coordinatesystem=CS;
			if(rawpxrect)
			{
				delete rawpxrect;
				rawpxrect=NULL;
			}
			Dir=ExtractFileDir(Filename);
			if(Dir!="") Dir+="\\";
			if(XMLExplorer->FindTagStart("lev:Leveringsinformatie"))
			{
				if(XMLExplorer->FindTagAndGetContent("lev:Klicnummer", str))
				{
					str2=Dir+"GB_"+str+".png";
					if(FileExists(str2))
					{
						AnsiString temp;

						if(bmpGB) delete bmpGB;
						bmpGB=NULL;
						if(Preview)
						{
							try
							{
								temp=((TRadarMapSettings*)radarmapsettings)->RadarMapDirectory+"\\";
								temp+="temp"+IntToStr((__int64)Gr32_system::GetTickCount())+".tmp";
								CopyFile(str2.c_str(), temp.c_str(), false);
								//background=LoadPNG32(temp);
								bmpGB=LoadPNG32(temp);
								DeleteFile(temp);
							}
							catch(Exception &e) {DeleteFile(temp);}
						}
						//else background=LoadPNG32(str2);
						else bmpGB=LoadPNG32(str2);
					}
					klicnummer=str;
				}
				if(AStopItEvent && WaitForSingleObject(AStopItEvent, 0)==WAIT_OBJECT_0) goto LoadEnd;
				if(XMLExplorer->FindTagStart("lev:Locatie") && XMLExplorer->FindTagAndGetContent("gml:posList", str)) Location=str;
				/*if(XMLExplorer->FindTagStart("lev:Pngformaat") && XMLExplorer->FindTagStart("lev:OmsluitendeRechthoek"))
				{
					if(XMLExplorer->FindTagAndGetContent("gml:lowerCorner", str)) LowerCorner=str;
					if(XMLExplorer->FindTagAndGetContent("gml:upperCorner", str)) UpperCorner=str;
					while(XMLExplorer->GetNextStartTagInside("lev:Pngformaat", str))
					{
						if(str=="lev:PixelsBreed")
						{
							if(XMLExplorer->GetContent(str)) w=StrToInt(str);
						}
						else if(str=="lev:PixelsHoog")
						{
							if(XMLExplorer->GetContent(str)) h=StrToInt(str);
						}
					}
					if(w>0 && h>0)
					{
						UtilityBmp->SetSize(w, h);
						InfoBmp->SetSize(w, h);
						if(background!=NULL) background->SetSize(w, h);
					}
				}*/
				if(XMLExplorer->FindTagStart("lev:Pngformaat"))
				{
					while(XMLExplorer->GetNextStartTagInside("lev:Pngformaat", str))
					{
						if(str=="gml:lowerCorner")
						{
							if(XMLExplorer->GetContent(str)) LowerCorner=str;
						}
						else if(str=="gml:upperCorner")
						{
							if(XMLExplorer->GetContent(str)) UpperCorner=str;
						}
						else if(str=="lev:PixelsBreed")
						{
							if(XMLExplorer->GetContent(str))
							{
								try {w=StrToInt(str);}
								catch(Exception &e) {w=0;}
							}
						}
						else if(str=="lev:PixelsHoog")
						{
							if(XMLExplorer->GetContent(str))
							{
								try {h=StrToInt(str);}
								catch(Exception &e) {h=0;}
							}
						}
						else if(str=="lev:CoordinatesSystem")
						{
							if(XMLExplorer->GetContent(str))
							{
								try {coordinatesystem=(TCoordinateSystem)StrToInt(str);}
								catch(Exception &e) {coordinatesystem=csDutch;}
							}
						}
						if(AStopItEvent && WaitForSingleObject(AStopItEvent, 0)==WAIT_OBJECT_0) break;
					}
					int nw=w, nh=h, rw, rh;

					if(bmpGB && nw<bmpGB->Width) nw=bmpGB->Width;
					rw=nw;
					if(nw<MaxBitmapWidth) nw=MaxBitmapWidth;
					if(bmpGB && nh<bmpGB->Height) nh=bmpGB->Height;
					rh=nh;
					if(nh<MaxBitmapHeight) nh=MaxBitmapHeight;
					if(w!=0 && h!=0) rawpxrect=new Types::TRect((nw-w)>>1, (nh-h)>>1, (nw+w)>>1, (nh+h)>>1);
					else if(bmpGB) rawpxrect=new Types::TRect((nw-bmpGB->Width)>>1, (nh-bmpGB->Height)>>1, (nw+bmpGB->Width)>>1, (nh+bmpGB->Height)>>1);

					if(!bmpGB || (rawpxrect->Left!=0 || rawpxrect->Top!=0))
					{
						TBitmap32 *tmp=new TBitmap32();

						tmp->BeginUpdate();
						tmp->SetSize(nw, nh);
						if(Manager && ((TRadarMapManager*)Manager)->Settings)
							tmp->Clear(((TRadarMapManager*)Manager)->Settings->DXFBackgroundColor);
						else tmp->Clear(clWhite32);
						if(bmpGB)
						{
							tmp->Draw(rawpxrect->Left, rawpxrect->Top, bmpGB);
							delete bmpGB;
						}
						tmp->EndUpdate();
						bmpGB=tmp;
					}
					if(rw!=nw || rh!=nh)
					{
						double cw=CoordinatesRect.Width(), ch=CoordinatesRect.Height(), dw, dh, ceny, cenx;

						dw=cw*0.5*(double)nw/(double)rw;
						dh=ch*0.5*(double)nh/(double)rh;
						cenx=(CoordinatesRect.Right+CoordinatesRect.Left)/2.;
						ceny=(CoordinatesRect.Top+CoordinatesRect.Bottom)/2.;
						CoordinatesRect.Right=cenx+dw;
						CoordinatesRect.Left=cenx-dw;
						CoordinatesRect.Top=ceny+dh;
						CoordinatesRect.Bottom=ceny-dh;
					}
					/*if(!Preview)
					{
						UtilityBmp->SetSize(bmpGB->Width, bmpGB->Height);
						InfoBmp->SetSize(bmpGB->Width, bmpGB->Height);
					}
					else*/
					if(Preview) goto LoadEnd;
				}
				if(AStopItEvent && WaitForSingleObject(AStopItEvent, 0)==WAIT_OBJECT_0) goto LoadEnd;
				if(XMLExplorer->FindTagStart("lev:NetbeheerderLeveringen"))
				{
					while(XMLExplorer->FindTagStart("lev:NetbeheerderLevering"))
					{
						Collection=new TXMLMapThemesCollection(this);
						try
						{
							while(XMLExplorer->GetNextStartTagInside("lev:NetbeheerderLevering", str))
							{
								if(str=="lev:RelatienummerNetbeheerder")
								{
									if(XMLExplorer->GetContent(str))
									{
										try	{Collection->ID=StrToInt(str);}
										catch(Exception &e) {Collection->ID=0;}
									}
								}
								else if(str=="lev:Bedrijfsnaam") XMLExplorer->GetContent(Collection->Name);
								else if(str=="lev:BedrijfsnaamAfkorting") XMLExplorer->GetContent(Collection->ShortName);
								else if(str=="lev:Thema" || str=="lev:EigenTopo" || str=="lev:PlanTopo")
								{
									if(Theme==NULL) Theme=new TXMLMapTheme(Collection);
									if(str=="lev:EigenTopo" || str=="lev:PlanTopo")
									{
										while(XMLExplorer->GetNextStartTagInside(str, str2))
										{
											if(str2=="lev:Bestandsnaam")
											{
												if(XMLExplorer->GetContent(str2, true))
												{
													if(str=="lev:EigenTopo") T=xmbtElgenTopo;
													else T=xmbtTopo;
													str2=Dir+str2;
													if(FileExists(str2))
													{
														map=new TXMLMapBitmap(NULL, T, str2);
														Theme->AddObject(map);
														map=NULL;
													}
												}
											}
											if(AStopItEvent && WaitForSingleObject(AStopItEvent, 0)==WAIT_OBJECT_0) break;
										}
										if(Theme->ThemeName=="") Theme->ThemeName="Topography";
									}
									else
									{
										while(XMLExplorer->GetNextStartTagInside(str, str2))
										{
											if(str2=="lev:Themanaam") XMLExplorer->GetContent(Theme->ThemeName);
											else if(str2=="lev:Ligging" || str2=="lev:Maatvoering" || str2=="lev:Annotatie")
											{
												while(XMLExplorer->GetNextStartTagInside(str2, str3))
												{
													if(str3=="lev:Bestandsnaam")
													{
														if(XMLExplorer->GetContent(str4, true))
														{
															if(str2=="lev:Ligging") T=xmbtLocation;
															else if(str2=="lev:Maatvoering") T=xmbtDimensions;
															else T=xmbtAnnotation;
															str4=Dir+str4;
															if(FileExists(str4))
															{
																map=new TXMLMapBitmap(NULL, T, str4);
																if(radarmapsettings && T!=xmbtLocation)
																	map->Visible=((TRadarMapSettings*)radarmapsettings)->ShowAnnotationLayerOnOpen;
																Theme->AddObject(map);
																map=NULL;
															}
														}
													}
													if(AStopItEvent && WaitForSingleObject(AStopItEvent, 0)==WAIT_OBJECT_0) break;
												}
											}
											if(AStopItEvent && WaitForSingleObject(AStopItEvent, 0)==WAIT_OBJECT_0) break;
										}
										Theme=NULL;
									}
								}
								if(AStopItEvent && WaitForSingleObject(AStopItEvent, 0)==WAIT_OBJECT_0) break;
							}
							if(Theme!=NULL && Theme->Count==0) Collection->Delete(Theme->Index);
							Theme=NULL;
						}
						__finally
						{
							if(Collection!=NULL && Collection->Count==0) Collections->Delete(Collection->Index);
							Collection=NULL;
						}
						if(AStopItEvent && WaitForSingleObject(AStopItEvent, 0)==WAIT_OBJECT_0) break;
					}
				}
			}
			else
			{
				XMLExplorer->Restart();
				if(XMLExplorer->FindTagStart("lev:Leveringsinfo"))
				{
					if(XMLExplorer->FindTagAndGetContent("lev:Klicnummer", str))
					{
						str2=FindFileAtDir(Dir, "GB_"+str+"*.png");
						AnsiString temp;
						if(bmpGB) delete bmpGB;
						bmpGB=NULL;
						if(FileExists(str2))
						{
							if(Preview)
							{
								try
								{
									temp=((TRadarMapSettings*)radarmapsettings)->RadarMapDirectory+"\\";
									temp+="temp"+IntToStr((__int64)Gr32_system::GetTickCount())+".tmp";
									CopyFile(str2.c_str(), temp.c_str(), false);
									//background=LoadPNG32(temp);
									bmpGB=LoadPNG32(temp);
									DeleteFile(temp);
								}
								catch(Exception &e) {DeleteFile(temp);}
							}
							//else background=LoadPNG32(str2);
							else bmpGB=LoadPNG32(str2);
						}

						klicnummer=str;
					}
					if(AStopItEvent && WaitForSingleObject(AStopItEvent, 0)==WAIT_OBJECT_0) goto LoadEnd;
					if(XMLExplorer->FindTagStart("lev:Locatie") && XMLExplorer->FindTagAndGetContent("gml:posList", str)) Location=str;
					if(XMLExplorer->FindTagStart("lev:Pngformaat"))
					{
						while(XMLExplorer->GetNextStartTagInside("lev:Pngformaat", str))
						{
							if(str=="ns3:lowerCorner")
							{
								if(XMLExplorer->GetContent(str)) LowerCorner=str;
							}
							else if(str=="ns3:upperCorner")
							{
								if(XMLExplorer->GetContent(str)) UpperCorner=str;
							}
							else if(str=="lev:PixelsBreed")
							{
								if(XMLExplorer->GetContent(str))
								{
									try {w=StrToInt(str);}
									catch(Exception &e) {w=0;}
								}
							}
							else if(str=="lev:PixelsHoog")
							{
								if(XMLExplorer->GetContent(str))
								{
									try {h=StrToInt(str);}
									catch(Exception &e) {h=0;}
								}
							}
							else if(str=="lev:CoordinatesSystem")
							{
								if(XMLExplorer->GetContent(str))
								{
									try {coordinatesystem=(TCoordinateSystem)StrToInt(str);}
									catch(Exception &e) {coordinatesystem=csLatLon;}
								}
							}
							if(AStopItEvent && WaitForSingleObject(AStopItEvent, 0)==WAIT_OBJECT_0) break;
						}
						int nw=w, nh=h, rw, rh;

						if(bmpGB && nw<bmpGB->Width) nw=bmpGB->Width;
						rw=nw;
						if(nw<MaxBitmapWidth) nw=MaxBitmapWidth;
						if(bmpGB && nh<bmpGB->Height) nh=bmpGB->Height;
						rh=nh;
						if(w!=0 && h!=0) rawpxrect=new Types::TRect((nw-w)>>1, (nh-h)>>1, (nw+w)>>1, (nh+h)>>1);
						else if(bmpGB) rawpxrect=new Types::TRect((nw-bmpGB->Width)>>1, (nh-bmpGB->Height)>>1, (nw+bmpGB->Width)>>1, (nh+bmpGB->Height)>>1);

						if(!bmpGB || (rawpxrect->Left!=0 || rawpxrect->Top!=0))
						{
							TBitmap32 *tmp=new TBitmap32();

							tmp->BeginUpdate();
							tmp->SetSize(nw, nh);
							if(Manager && ((TRadarMapManager*)Manager)->Settings)
								tmp->Clear(((TRadarMapManager*)Manager)->Settings->DXFBackgroundColor);
							else tmp->Clear(clWhite32);
							if(bmpGB)
							{
								tmp->Draw(rawpxrect->Left, rawpxrect->Top, bmpGB);
								delete bmpGB;
							}
							tmp->EndUpdate();
							bmpGB=tmp;
						}
						if(rw!=nw || rh!=nh)
						{
							double cw=CoordinatesRect.Width(), ch=CoordinatesRect.Height(), dw, dh, ceny, cenx;

							dw=cw*0.5*(double)nw/(double)rw;
							dh=ch*0.5*(double)nh/(double)rh;

							cenx=(CoordinatesRect.Right+CoordinatesRect.Left)/2.;
							ceny=(CoordinatesRect.Top+CoordinatesRect.Bottom)/2.;
							CoordinatesRect.Right=cenx+dw;
							CoordinatesRect.Left=cenx-dw;
							CoordinatesRect.Top=ceny+dh;
							CoordinatesRect.Bottom=ceny-dh;
						}
						/*if(!Preview)
						{
							UtilityBmp->SetSize(bmpGB->Width, bmpGB->Height);
							InfoBmp->SetSize(bmpGB->Width, bmpGB->Height);
						}
						else*/
						if(Preview) goto LoadEnd;
					}
					if(XMLExplorer->FindTagStart("lev:NetbeheerderLeveringen"))
					{
						while(XMLExplorer->FindTagStart("lev:NetbeheerderLevering"))
						{
							Collection=new TXMLMapThemesCollection(this);
							try
							{
								while(XMLExplorer->GetNextStartTagInside("lev:NetbeheerderLevering", str))
								{
									if(str=="lev:RelatienummerNetbeheerder")
									{
										if(XMLExplorer->GetContent(str))
										{
											try	{Collection->ID=StrToInt(str);}
											catch(Exception &e) {Collection->ID=0;}
										}
									}
									else if(str=="lev:Bedrijfsnaam") XMLExplorer->GetContent(Collection->Name);
									else if(str=="lev:BedrijfsnaamAfkorting") XMLExplorer->GetContent(Collection->ShortName);
									else if(str=="lev:Thema")
									{
										Theme=new TXMLMapTheme(Collection);
										try
										{
											while(XMLExplorer->GetNextStartTagInside(str, str2))
											{
												if(str2=="lev:Themanaam" &&	XMLExplorer->GetContent(str2) && str2!=NULL && str2!="")
												{
													Theme->ThemeName=str2;
													for(int g=1; g<=str2.Length(); g++)
														if(str2[g]==' ' || str2[g]=='.') str2[g]='*';
													FindMapsForTheme(Theme, Dir, "LG_*"+str2+"*"+Collection->Name+"*"+klicnummer+"*.png", xmbtLocation);
													FindMapsForTheme(Theme, Dir, "MV_*"+str2+"*"+Collection->Name+"*"+klicnummer+"*.png", xmbtDimensions);
													FindMapsForTheme(Theme, Dir, "AN_*"+str2+"*"+Collection->Name+"*"+klicnummer+"*.png", xmbtAnnotation);
												}
												if(AStopItEvent && WaitForSingleObject(AStopItEvent, 0)==WAIT_OBJECT_0) break;
											}
										}
										__finally
										{
											if(Theme!=NULL && Theme->Count==0) Collection->Delete(Theme->Index);
											Theme=NULL;
										}
									}
									if(AStopItEvent && WaitForSingleObject(AStopItEvent, 0)==WAIT_OBJECT_0) break;
								}
								Theme=new TXMLMapTheme(Collection);
								try
								{
									Theme->ThemeName="Topography";
									FindMapsForTheme(Theme, Dir, "ET_*"+Collection->Name+"*"+klicnummer+"*.png", xmbtElgenTopo);
								}
								__finally
								{
									if(Theme!=NULL && Theme->Count==0) Collection->Delete(Theme->Index);
								}
								Theme=new TXMLMapTheme(Collection);
								try
								{
									Theme->ThemeName="Topography";
									FindMapsForTheme(Theme, Dir, "PT_*"+Collection->Name+"*"+klicnummer+"*.png", xmbtTopo);
								}
								__finally
								{
									if(Theme!=NULL && Theme->Count==0) Collection->Delete(Theme->Index);
								}
								if(Theme!=NULL && Theme->Count==0) Collection->Delete(Theme->Index);
							}
							__finally
							{
								if(Collection!=NULL && Collection->Count==0) Collections->Delete(Collection->Index);
								Collection=NULL;
							}
							if(AStopItEvent && WaitForSingleObject(AStopItEvent, 0)==WAIT_OBJECT_0) break;
						}
					}
				}
			}
LoadEnd:
		}
	}
	__finally
	{
		delete XMLExplorer;
		if(!AStopItEvent || WaitForSingleObject(AStopItEvent, 0)!=WAIT_OBJECT_0)
		{
			if(WithUpdate)
			{
				Update();
				ZoomUpdate();
			}
			if(Preview)
			{
				if(bmpGB)
				{
					if(!background)
					{
						background=new TBitmap32();
						background->SetSize(bmpGB->Width, bmpGB->Height);
					}
					Background->BeginUpdate();
					Background->Draw(0, 0, bmpGB);
					Background->EndUpdate();
				}
			}
			else
			{
				if(bmpGB->Width>MaxBitmapWidth)
					scalingfactorx=(double)MaxBitmapWidth/(double)bmpGB->Width;
				if(bmpGB->Height>MaxBitmapHeight)
					scalingfactory=(double)MaxBitmapHeight/(double)bmpGB->Height;
			}
			LockedCS=true;
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TXMLMapsContainer::GetLayers(TCheckBoxTreeView *TreeView, TTreeNode *Root)
{
	TXMLMapBitmap *map;
	TXMLMapThemesCollection *Collection;
	TXMLMapTheme *Theme;
	TTreeNode *node;
	int i, j, k;

	if(TreeView==NULL) return;

	if(Root==NULL)
	{
		TreeView->Items->Clear();
		Root=TreeView->Items->Add(NULL, Klicnummer);
		TreeView->SetChecking(Root, Visible);
	}
	Root->Data=(void*)((TMapsContainer*)this);
	for(i=0; i<Collections->Count; i++)
	{
		Collection=(TXMLMapThemesCollection*)Collections->Items[i];
		if(Collection!=NULL)
		{
			for(j=0; j<Collection->Count; j++)
			{
				Theme=(TXMLMapTheme*)Collection->GetObject(j);
				map=Theme->GetObject(xmbtLocation);
				if(map)
				{
					node=TreeView->Items->AddChild(Root, map->Caption);
					TreeView->SetChecking(node, map->Visible);
					node->Data=(void*)map;
					if(TreeView->Images)
					{
						node->ImageIndex=2;
						node->SelectedIndex=2;
					}
				}
				map=Theme->GetObject(xmbtDimensions);
				if(map)
				{
					node=TreeView->Items->AddChild(Root, map->Caption);
					TreeView->SetChecking(node, map->Visible);
					node->Data=(void*)map;
					if(TreeView->Images)
					{
						node->ImageIndex=3;
						node->SelectedIndex=3;
					}
				}
				map=Theme->GetObject(xmbtAnnotation);
				if(map)
				{
					node=TreeView->Items->AddChild(Root, map->Caption);
					TreeView->SetChecking(node, map->Visible);
					node->Data=(void*)map;
					if(TreeView->Images)
					{
						node->ImageIndex=3;
						node->SelectedIndex=3;
					}
				}
				map=Theme->GetObject(xmbtElgenTopo);
				if(map)
				{
					node=TreeView->Items->AddChild(Root, map->Caption);
					TreeView->SetChecking(node, map->Visible);
					node->Data=(void*)map;
					if(TreeView->Images)
					{
						node->ImageIndex=1;
						node->SelectedIndex=1;
					}
				}
				map= Theme->GetObject(xmbtTopo);
				if(map)
				{
					node=TreeView->Items->AddChild(Root, map->Caption);
					TreeView->SetChecking(node, map->Visible);
					node->Data=(void*)map;
					if(TreeView->Images)
					{
						node->ImageIndex=1;
						node->SelectedIndex=1;
					}
				}
			}
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TXMLMapsContainer::SetLayers(TCheckBoxTreeView *TreeView, TTreeNode *Root)
{
	int i;
	TXMLMapBitmap *map;
	TTreeNode *node;
	TMapsContainer *mc;
	TXMLMapBitmapType tt;

	if(TreeView==NULL) return;

	AutomaticUpdate=false;
	try
	{
		if(Root==NULL) Root=TreeView->Items->GetFirstNode();
		if(Root!=NULL)
		{
			mc=(TMapsContainer*)Root->Data;
			if(mc->MapType==mtXML && Root->Text==Klicnummer)
			{
				Visible=TreeView->Checked(Root);
				node=Root->getFirstChild();
				while(node!=NULL)
				{
					map=(TXMLMapBitmap*)node->Data;
					if(map!=NULL)
					{
						switch(node->ImageIndex)
						{
							case 1: tt=xmbtTopo; break;
							case 2: tt=xmbtLocation; break;
							case 3: tt=xmbtAnnotation; break;
							default: tt=map->Type;
						}
						map->ChangeType(tt);
						map->Visible=TreeView->Checked(node);
					}
					node=Root->GetNextChild(node);
				}
			}
		}
	}
	__finally
	{
		AutomaticUpdate=true;
		Update();
	}
}

//---------------------------------------------------------------------------
// TEmptyMapsContainer
//---------------------------------------------------------------------------
__fastcall TEmptyMapsContainer::TEmptyMapsContainer(TMyObject *AOwner, void* ASettings, TLockedDoubleRect *AViewportArea)
	: TBitmapMapsContainer(AOwner, ASettings, AViewportArea)
{
	int sz;

	maptype=mtEmpty;
	background=new TBitmap32();
	background->DrawMode=dmBlend;
	//if(radarmapsettings) sz=(int)((float)((TRadarMapSettings*)radarmapsettings)->MaximumLayerSizePX*0.4);
	//else sz=2000;
	background->SetSize(MaxBitmapWidth, MaxBitmapHeight);
	UtilityBmp->SetSizeFrom(background);
	InfoBmp->SetSizeFrom(background);
	rawpxrect=new Types::TRect(0, 0, MaxBitmapWidth, MaxBitmapHeight);
	RenderMap();
	filename="empty";
	if(Manager && ((TRadarMapManager*)Manager)->Settings)
		coordinatesystem=((TRadarMapManager*)Manager)->Settings->DefaultCoordinateSystem;
	else coordinatesystem=csLatLon;
	klicnummer="EmptyMap";
	Lower=DoublePoint(0,0);
	Upper=DoublePoint(0,0);
	Update();
	//LatLonRectUpdate();
}
//---------------------------------------------------------------------------

#ifdef _RadarMap_Logo_BGND
void __fastcall TEmptyMapsContainer::WatermarkLayerPaint(TObject *Sender, TBitmap32 *Bmp)
{
	TBitmap32* tmp=NULL;
	float f, fh, fw;
	int x, y, w, h;

	TBitmapMapsContainer::WatermarkLayerPaint(Sender, Bmp);
	try
	{
		try
		{
			if(Owner && ((TMapsContainersCollection*)Owner)->Image)
			{
				w=((TMapsContainersCollection*)Owner)->Image->Width;
				h=((TMapsContainersCollection*)Owner)->Image->Height;
				tmp=LoadPNG32Resource((System::UnicodeString)"PngImage_ICON_BGND_COLOR");//PNGIMAGE_BGND");
				fh=(float)h*0.75/(float)tmp->Height;
				fw=(float)w*0.75/(float)tmp->Width;
				if(fh<fw) f=fh;
				else f=fw;
				Bmp->BeginUpdate();
				try
				{
					try
					{
						if(f<1.)
						{
							//ScaleBitmap32(tmp, f, f, false);
							x=(w-(int)(f*tmp->Width))>>1;
							y=(h-(int)(f*tmp->Height))>>1;
							Bmp->Draw(Types::TRect(x, y, x+f*tmp->Width-1, y+f*tmp->Height-1),
								Types::TRect(0, 0, tmp->Width-1, tmp->Height-1), tmp);
						}
						else Bmp->Draw((w-tmp->Width)>>1, (h-tmp->Height)>>1, tmp);
					}
					catch(...) {}
				}
				__finally
				{
					Bmp->EndUpdate();
					//Bmp->Changed();
				}
			}
		}
		catch(...) {}
	}
	__finally
	{
		if(tmp) delete tmp;
	}
}
//---------------------------------------------------------------------------
#endif

void __fastcall TEmptyMapsContainer::RenderMap()
{
	if(Background)
	{
		Background->BeginUpdate();
		try
		{
			try
			{
				Background->Clear(((TRadarMapSettings*)radarmapsettings)->DXFBackgroundColor);
			}
			catch(...) {}
		}
		__finally
		{
			Background->EndUpdate();
		}
		UtilityUpdate();
		InfoUpdate();
	}
}
//---------------------------------------------------------------------------

void __fastcall TEmptyMapsContainer::GetLayers(TCheckBoxTreeView *TreeView, TTreeNode *Root)
{
	TTreeNode *node;
	TDXFLayer *layer;
	int i, j, k;

	if(TreeView==NULL) return;

	if(Root==NULL)
	{
		TreeView->Items->Clear();
		Root=TreeView->Items->Add(NULL, Klicnummer);
		TreeView->SetChecking(Root, Visible);
	}
	Root->Data=(void*)((TMapsContainer*)this);
}
//---------------------------------------------------------------------------

void __fastcall TEmptyMapsContainer::SetLayers(TCheckBoxTreeView *TreeView, TTreeNode *Root)
{
	int i;
	TTreeNode *node;
	TMapsContainer *mc;

	if(TreeView==NULL) return;

	AutomaticUpdate=false;
	try
	{
		try
		{
			if(!Root) Root=TreeView->Items->GetFirstNode();
			if(Root)
			{
				mc=(TMapsContainer*)Root->Data;
				if(mc->MapType==mtEmpty && Root->Text==Klicnummer)
				{
					Visible=TreeView->Checked(Root);
				}
			}
		}
		catch(Exception &e) {}
	}
	__finally
	{
		AutomaticUpdate=true;
		Update();
	}
}
