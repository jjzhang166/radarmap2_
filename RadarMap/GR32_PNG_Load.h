//---------------------------------------------------------------------------

#ifndef GR32_PNG_LoadH
#define GR32_PNG_LoadH
//---------------------------------------------------------------------------
#include <GR32_PNG.hpp>

namespace Gr32_png
{
	extern TBitmap32* __fastcall LoadPNG32(Classes::TStream* SrcStream);
	extern TBitmap32* __fastcall LoadPNG32(System::UnicodeString Filename);
	extern TBitmap32* __fastcall LoadPNG32Resource(System::UnicodeString ResourceName);
}

#endif
