//---------------------------------------------------------------------------


#pragma hdrstop

#include "XMLExplorer.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

//---------------------------------------------------------------------------
// TXMLExplorer
//---------------------------------------------------------------------------
__fastcall TXMLExplorer::TXMLExplorer(TComponent* AOwner, AnsiString FileName)
{
	XmlScanner=new TXmlScanner(AOwner);
	XmlScanner->LoadFromFile(FileName);
	if(XmlScanner->XmlParser->DocBuffer==NULL ||
		XmlScanner->XmlParser->DocBuffer[0]==0) throw Exception("Cannot open file!");
	XmlScanner->XmlParser->StartScan();
}
//---------------------------------------------------------------------------

void __fastcall TXMLExplorer::Restart()
{
	XmlScanner->XmlParser->StartScan();
}
//---------------------------------------------------------------------------

bool __fastcall TXMLExplorer::FindTagStart(AnsiString TagName)
{
	AnsiString tag;
	bool res=true;

	tag="";
	while (tag!=TagName && res)
	{
		if(XmlScanner->XmlParser->Scan())
		{
			if(XmlScanner->XmlParser->CurPartType==ptStartTag)
				tag=XmlScanner->XmlParser->CurName;
		}
		else res=false;
	}
	return res;
}
//---------------------------------------------------------------------------

bool __fastcall TXMLExplorer::FindTagEnd(AnsiString TagName)
{
	AnsiString tag;
	bool res=true;

	tag="";
	while (tag!=TagName && res)
	{
		if(XmlScanner->XmlParser->Scan())
		{
			if(XmlScanner->XmlParser->CurPartType==ptEndTag)
				tag=XmlScanner->XmlParser->CurName;
		}
		else res=false;
	}
	return res;
}
//---------------------------------------------------------------------------

bool __fastcall TXMLExplorer::GetAttribute(AnsiString AttributeName, AnsiString &Str)
{
	bool res;

	if(XmlScanner->XmlParser->CurAttr && XmlScanner->XmlParser->CurAttr->Count>0)
	{
		Str=XmlScanner->XmlParser->CurAttr->Value(AttributeName);
		res=true;
	}
	else res=false;
	return res;
}
//---------------------------------------------------------------------------

bool __fastcall TXMLExplorer::GetContent(UnicodeString &Str, bool NotNormalized)
{
	if(XmlScanner->XmlParser->Scan() && (XmlScanner->XmlParser->CurPartType==ptContent ||
		XmlScanner->XmlParser->CurPartType==ptCData))
	{
		if(!NotNormalized) Str=(UnicodeString)XmlScanner->XmlParser->CurContent;
		else
		{
			int n=XmlScanner->XmlParser->CurFinal-XmlScanner->XmlParser->CurStart+1;
			if(n>0)
			{
				char *buf=new char[n+1];

				try
				{
					for(int i=0; i<n; i++)
						buf[i]=*((char*)(XmlScanner->XmlParser->CurStart+i));
					buf[n]='\0';
					Str=buf;
				}
				__finally
				{
					delete buf;
				}
			}
		}
		return true;
	}
	else return false;
}
//---------------------------------------------------------------------------

bool __fastcall TXMLExplorer::FindTagAndGetContent(AnsiString TagName, AnsiString &Str)
{
	if(FindTagStart(TagName) && GetContent(Str)) return true;
	else return false;
}
//---------------------------------------------------------------------------

bool __fastcall TXMLExplorer::GetNextStartTag(AnsiString &Str)
{
	bool res;

	do
	{
		res=XmlScanner->XmlParser->Scan();
	} while (res && XmlScanner->XmlParser->CurPartType!=ptStartTag);
	if(res) Str=XmlScanner->XmlParser->CurName;
	return res;
}
//---------------------------------------------------------------------------

bool __fastcall TXMLExplorer::GetNextStartTagInside(AnsiString Parent, AnsiString &Str)
{
	bool res;

	do
	{
		res=XmlScanner->XmlParser->Scan();
		if(XmlScanner->XmlParser->CurPartType==ptEndTag && XmlScanner->XmlParser->CurName==Parent) res=false;
	} while (res && XmlScanner->XmlParser->CurPartType!=ptStartTag);
	if(res) Str=XmlScanner->XmlParser->CurName;
	return res;
}
//---------------------------------------------------------------------------

bool __fastcall TXMLExplorer::GetNextEndTag(AnsiString &Str)
{
	bool res;

	do
	{
		res=XmlScanner->XmlParser->Scan();
	} while (res && XmlScanner->XmlParser->CurPartType!=ptEndTag);
	if(res) Str=XmlScanner->XmlParser->CurName;
	return res;
}