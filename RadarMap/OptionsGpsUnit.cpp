//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "OptionsGpsUnit.h"
#include "Manager.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "ImageButton"
#pragma resource "*.dfm"
TOptionsGpsUnitForm *OptionsGpsUnitForm;

//---------------------------------------------------------------------------
// TOptionsGpsUnitForm
//---------------------------------------------------------------------------
__fastcall TOptionsGpsUnitForm::TOptionsGpsUnitForm(TComponent* Owner)
	: TForm(Owner)
{
	Params=new TList();
}
//---------------------------------------------------------------------------

__fastcall TOptionsGpsUnitForm::~TOptionsGpsUnitForm()
{
	for(int i=0; i<Params->Count; i++)
		if(Params->Items[i]) delete (TGpsListenerCommandParam*)Params->Items[i];
	Params->Clear();
	delete Params;
}
//---------------------------------------------------------------------------

int __fastcall TOptionsGpsUnitForm::ShowOptions(AnsiString aGpsUnitFileName)
{
	TXMLExplorer *XMLExplorer;
	AnsiString str, str2, str3, str4, name="";
	TGpsListenerCommandParam *param;
	AnsiString regkey=_RadarMapRegKey+(AnsiString)_GpsUnitsRegKey;

	for(int i=0; i<Params->Count; i++)
		if(Params->Items[i]) delete (TGpsListenerCommandParam*)Params->Items[i];
	Params->Clear();
	ParamsLBox->Clear();
	GpsUnitLabel->Caption="";
	DescriptionLabel->Caption="";
	ValueEdit->Text="";
	if(FileExists(aGpsUnitFileName))
	{
		XMLExplorer=new TXMLExplorer(NULL, aGpsUnitFileName);
		TRegistry &regKey=*new TRegistry();
		try
		{
			if(XMLExplorer->FindTagStart("GpsUnit"))
			{
				if(XMLExplorer->FindTagAndGetContent("UnitName", str)) name=str;
				while(XMLExplorer->GetNextStartTagInside("Initializing", str))
				{
					if(str=="Command")
					{
						while(XMLExplorer->GetNextStartTagInside(str, str2))
						{
							if(str2=="Param")
							{
								str3=str2;
								param=NULL;
								while(XMLExplorer->GetNextStartTagInside(str3, str2))
								{
									if(str2=="Name")
									{
										if(XMLExplorer->GetContent(str4) && str4!="")
										{
											if(!param) param=new TGpsListenerCommandParam();
											param->Name=str4;
										}
									}
									else if(str2=="Description")
									{
										if(XMLExplorer->GetContent(str4) && str4!="")
										{
											if(!param) param=new TGpsListenerCommandParam();
											param->Description=str4;
										}
									}
									else if(str2=="Value")
									{
										if(XMLExplorer->GetContent(str4) && str4!="")
										{
											if(!param) param=new TGpsListenerCommandParam();
											param->Value=str4;
										}
									}
								}
								if(param && param->Name!="")
								{
									if(name!="")
									{
										if(regKey.OpenKey(regkey+"\\"+name, false))
										{
											if(regKey.ValueExists(param->Name))
												param->Value=regKey.ReadString(param->Name);
										}
										regKey.CloseKey();
										Params->Add(param);
										ParamsLBox->AddItem(param->Name, NULL);
									}
									else delete param;
								}
							}
						}
					}
				}
			}
		}
		__finally
		{
			GpsUnitLabel->Caption=name;
			delete XMLExplorer;
			delete &regKey;
		}
		return ShowModal();
	}
	return mrCancel;
}
//---------------------------------------------------------------------------

void __fastcall TOptionsGpsUnitForm::OkImageClick(TObject *Sender)
{
	TGpsListenerCommandParam *param;
	AnsiString regkey=_RadarMapRegKey+(AnsiString)_GpsUnitsRegKey;
	TRegistry &regKey=*new TRegistry();

	try
	{
		if(GpsUnitLabel->Caption!="")
		{
            if(regKey.OpenKey(regkey+"\\"+GpsUnitLabel->Caption, true))
			{
				for(int i=0; i<Params->Count; i++)
				{
					param=(TGpsListenerCommandParam*)Params->Items[i];
					if(param && param->Name!="") regKey.WriteString(param->Name, param->Value);
				}
				regKey.CloseKey();
			}
		}
	}
	__finally
	{
		delete &regKey;
	}
	ModalResult=mrOk;
}
//---------------------------------------------------------------------------

void __fastcall TOptionsGpsUnitForm::CancelImageClick(TObject *Sender)
{
	ModalResult=mrCancel;
}
//---------------------------------------------------------------------------

void __fastcall TOptionsGpsUnitForm::ParamsLBoxClick(TObject *Sender)
{
	TGpsListenerCommandParam *param;

	if(ParamsLBox->ItemIndex>=0 && ParamsLBox->ItemIndex<ParamsLBox->Count &&
		ParamsLBox->ItemIndex<Params->Count)
	{
		param=(TGpsListenerCommandParam*)Params->Items[ParamsLBox->ItemIndex];
		if(param)
		{
			DescriptionLabel->Caption=param->Description;
			ValueEdit->Text=param->Value;
		}
	}
	else
	{
		DescriptionLabel->Caption="";
		ValueEdit->Text="";
	}
}
//---------------------------------------------------------------------------

void __fastcall TOptionsGpsUnitForm::ValueEditChange(TObject *Sender)
{
	TGpsListenerCommandParam *param;

	if(ParamsLBox->ItemIndex>=0 && ParamsLBox->ItemIndex<ParamsLBox->Count &&
		ParamsLBox->ItemIndex<Params->Count)
	{
		param=(TGpsListenerCommandParam*)Params->Items[ParamsLBox->ItemIndex];
		if(param) param->Value=ValueEdit->Text;
	}
}
//---------------------------------------------------------------------------

void __fastcall TOptionsGpsUnitForm::FormClose(TObject *Sender, TCloseAction &Action)

{
	FormHide(Sender);
}
//---------------------------------------------------------------------------

void __fastcall TOptionsGpsUnitForm::FormHide(TObject *Sender)
{
	for(int i=0; i<Params->Count; i++)
		if(Params->Items[i]) delete (TGpsListenerCommandParam*)Params->Items[i];
	Params->Clear();
}
//---------------------------------------------------------------------------

void __fastcall TOptionsGpsUnitForm::TitlePanelMouseDown(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y)
{
	TitleDown=true;
	TitleXY=ClientToScreen(Types::TPoint(X, Y));
}
//---------------------------------------------------------------------------

void __fastcall TOptionsGpsUnitForm::TitlePanelMouseMove(TObject *Sender, TShiftState Shift,
          int X, int Y)
{
	Types::TPoint tmp;

	if(TitleDown)
	{
		tmp=ClientToScreen(Types::TPoint(X, Y));
		Left+=tmp.x-TitleXY.x;
		Top+=tmp.y-TitleXY.y;
		TitleXY=tmp;
	}
}
//---------------------------------------------------------------------------

void __fastcall TOptionsGpsUnitForm::TitlePanelMouseUp(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y)
{
	TitleDown=false;
}
//---------------------------------------------------------------------------

void __fastcall TOptionsGpsUnitForm::FormShow(TObject *Sender)
{
	TitleDown=false;
	if(ParamsLBox->Items->Count>0)
	{
		ParamsLBox->ItemIndex=0;
		ParamsLBoxClick(Sender);
	}
}
//---------------------------------------------------------------------------

