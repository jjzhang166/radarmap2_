//---------------------------------------------------------------------------

#ifndef PalettesRepositoryH
#define PalettesRepositoryH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <ImageButton.h>
#include <pngimage.hpp>
#include <GR32_Image.hpp>
#include <Graphics.hpp>
#include "Manager.h"
#include <Dialogs.hpp>
#include <ComCtrls.hpp>
#include <ImgList.hpp>
//---------------------------------------------------------------------------

#define PalettePanelColor (TColor)0x00F8F9FF

struct TPaletteUnit
{
private:
	int id;
	bool selected;
	bool readSelected() {if(Panel) return Panel->Color!=PalettePanelColor; else return false;}
	void writeSelected(bool value) {if(selected!=value && Panel) {selected=value; if(value) Panel->Color=clBlack; else Panel->Color=PalettePanelColor;}}
	void writeID(int value) {id=value; if(Caption) Caption->Caption=IntToStr(id);}
public:
	TPaletteUnit() {Panel=NULL; Image=NULL; Caption=NULL; ID=0; selected=true; Selected=false;}
	TPaletteUnit(int aID, TPanel *APanel, TImage32 *AImage, TStaticText *ACaption)
		{Panel=APanel; Image=AImage; Caption=ACaption; ID=aID; Selected=false;}

	TPanel *Panel;
	TImage32 *Image;
	TStaticText *Caption;

	__property int ID = {read=id, write=writeID};
	__property bool Selected = {read=readSelected, write=writeSelected};
};

class TPalettesForm : public TForm
{
__published:	// IDE-managed Components
	TPanel *Panel1;
	TPanel *Panel2;
	TPanel *Panel3;
	TPanel *Panel4;
	TPanel *TitlePanel;
	TPanel *Panel5;
	TPanel *Panel6;
	TPanel *Panel7;
	TPanel *PagesPanel;
	TPanel *OkCancelPanel;
	TLabel *CaptionST;
	TPanel *Panel8;
	TPanel *Panel9;
	TImageButton *CloseIButton;
	TImageButton *CancelImageButton;
	TEdit *TempEdit;
	TPanel *PalettePanel1;
	TImage32 *PaletteImage1;
	TPanel *PalettePanel2;
	TImage32 *PaletteImage2;
	TPanel *PalettePanel3;
	TImage32 *PaletteImage3;
	TPanel *PalettePanel4;
	TImage32 *PaletteImage4;
	TPanel *PalettePanel5;
	TImage32 *PaletteImage5;
	TPanel *PalettePanel6;
	TImage32 *PaletteImage6;
	TPanel *PalettePanel7;
	TImage32 *PaletteImage7;
	TPanel *PalettePanel8;
	TImage32 *PaletteImage8;
	TStaticText *StaticText8;
	TStaticText *StaticText7;
	TStaticText *StaticText6;
	TStaticText *StaticText5;
	TStaticText *StaticText4;
	TStaticText *StaticText3;
	TStaticText *StaticText2;
	TStaticText *StaticText1;
	void __fastcall CloseIButtonClick(TObject *Sender);
	void __fastcall FormShow(TObject *Sender);
	void __fastcall OkImageButtonClick(TObject *Sender);
	void __fastcall TitlePanelMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
		  int X, int Y);
	void __fastcall TitlePanelMouseMove(TObject *Sender, TShiftState Shift, int X, int Y);
	void __fastcall TitlePanelMouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift,
		  int X, int Y);

	void __fastcall BackgroundMouseMove(TObject *Sender, TShiftState Shift, int X, int Y);
	void __fastcall PaletteImage1Click(TObject *Sender);
	void __fastcall PaletteImage1MouseMove(TObject *Sender, TShiftState Shift, int X,
		  int Y, TCustomLayer *Layer);

private:	// User declarations
	TPaletteUnit *PaletteUnits;
	bool TitleDown;
	Types::TPoint TitleXY;
public:		// User declarations
	__fastcall TPalettesForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TPalettesForm *PalettesForm;
//---------------------------------------------------------------------------
#endif
