#ifndef MyThreadsH
#define MyThreadsH

#include <vcl.h>
#include <math.h>
//#include <Gr32_system.hpp>

//---------------------------------------------------------------------------
// Run'n'Forget, TMyThreads & Timer Threads
//---------------------------------------------------------------------------

#define MySleep(x) Sleep(x)
#define ProcessMessages() Application->ProcessMessages()
#define WaitOthers() MySleep(250); ProcessMessages();

const int MinimalStep = 25; //in msc
const int MinimalMessageTimeOut = 2000; //in msc;

//typedef void __fastcall (__closure *TMyThreadEvent)(void);//(TCustomTimerThread* Ptr);

#pragma warn -8111
//---------------------------------------------------------------------------
// Run'N'Forget with Delay
//---------------------------------------------------------------------------
class TRunNForgetThread : public TThread
{
private:
	TThreadMethod onrun;
	unsigned long Delay;
	bool wSynchro;
protected:
	void __fastcall Execute() {try {Sleep(Delay); if(onrun!=NULL) {if(wSynchro) Synchronize(onrun); else (onrun)();}} catch(Exception &e) {}}
public:
	__fastcall TRunNForgetThread(TThreadMethod aOnRun, bool aWithSynchronization, unsigned long aDelay=1000) : TThread(true) {FreeOnTerminate=true; onrun=aOnRun; Delay=aDelay; Resume();}
};/**/

class TMyThread : public TThread
{
private:
	bool successfully_terminated, started, handled;
	HANDLE askforsuspendevent, suspendedevent, askforterminateevent;
	void __fastcall Resume() {TThread::Resume();}
	void __fastcall Suspend() {TThread::Suspend();}
protected:
	HANDLE IdleEvent;
	bool MyTerminated;
	void __fastcall ForceResume() {Resume();}
	void __fastcall ForceSuspend() {Suspend();}
	void __fastcall CheckAskForSuspend() // Only for use indide of Execute method
	{
		if((!Terminated && !MyTerminated) && WaitForSingleObject(askforsuspendevent, 0)==WAIT_OBJECT_0)
		{
			//ResetEvent(AskForSuspendEvent); // bManualReset is FALSE in CreateEvent runction
			SetEvent(suspendedevent);
			Suspend();
			ResetEvent(suspendedevent);
		}
	}
	void __fastcall SetIsTerminated() // Only for use in the end of Execute method
	{
		successfully_terminated=true;
		SetEvent(askforterminateevent);
	}
	void __fastcall SetStarted() // Only for use in the begining of Execute method
	{
		started=true;
	}
	bool __fastcall IsStarted()
	{
		return started;
	}
	virtual void __fastcall MinStepSleep() {WaitForSingleObject(IdleEvent, 10);}
	virtual void __fastcall ExecuteLoopBody() {}
	virtual void __fastcall Execute()
	{
		SetStarted();
		do
		{
			ExecuteLoopBody();

			CheckAskForSuspend();
		} while((!Terminated && !MyTerminated));
		SetIsTerminated();
		ExitThread(99);
	}
public:
	__fastcall TMyThread(bool CreateSuspended) : TThread(true)
	{
		askforterminateevent=CreateEvent(NULL, true, false, NULL);
		askforsuspendevent=CreateEvent(NULL, false, false, NULL);
		suspendedevent=CreateEvent(NULL, true, CreateSuspended, NULL);
		IdleEvent=CreateEvent(NULL, true, false, NULL);
		handled=true;
		successfully_terminated=false;
		started=false;
		MyTerminated=false;
		if(!CreateSuspended) Resume();
	}
	void __fastcall FreeHandles()
	{
		try
		{
			if(handled)
			{
				if(IdleEvent) {CloseHandle(IdleEvent); IdleEvent=NULL;}
				if(askforsuspendevent) {CloseHandle(askforsuspendevent); askforsuspendevent=NULL;}
				if(suspendedevent) {CloseHandle(suspendedevent); suspendedevent=NULL;}
				if(askforterminateevent) {CloseHandle(askforterminateevent); askforterminateevent=NULL;}
			}
		}
		catch(Exception &e) {}
		handled=false;
	}
	__fastcall ~TMyThread()
	{
		FreeHandles();
	}
	void __fastcall AskForTerminate()
	{
		SetEvent(askforterminateevent);
		AskForResume();
		MyTerminated=true;
		Terminate();
	}
	bool __fastcall ForceTerminate()
	{
		if(!successfully_terminated)
		{
			AskForTerminate();//Terminate();
			WaitOthers();
		}
		if(!successfully_terminated)
		{
			//if(TerminateThread((HANDLE)Handle, 0)>0) return true;
			//else return false;
				//TerminateThread((HANDLE)Handle, 0);
			return false;
		}
		else return true;
	}
	bool FinalizeBeforeRelease()
    {
        bool res=false;
        AskForTerminate();
		WaitOthers();
        try
        {
            if(ForceTerminate())
			{
                FreeHandles();
                res=true;
            }
		}
        catch(...) {}
        return res;
	}
	void __fastcall AskForSuspend()
	{
		SetEvent(askforsuspendevent);
	}
	void __fastcall AskForResume()
	{
		ResetEvent(askforsuspendevent);
		ResetEvent(suspendedevent);
		if(Suspended) Resume();
	}
	bool __fastcall IsTerminated() {return Terminated | MyTerminated;}
	__property bool Started = {read=IsStarted};
	__property HANDLE SuspendedEvent = {read=suspendedevent};
	__property HANDLE AskForTerminateEvent = {read=askforterminateevent};
	__property bool SuccessfullyTerminated = {read=successfully_terminated};
};
#pragma warn +8111

void __fastcall DismissMyThread(TMyThread *aThread)
{
	if(aThread)
	{
		aThread->AskForTerminate();
		if(aThread->ForceTerminate()) delete aThread;
		else aThread->FreeOnTerminate=true;
	}
}

class TStartStopThread : public TMyThread
{
private:
	void __fastcall AskForResume() {TMyThread::AskForResume();}
	void __fastcall AskForSuspend() {TMyThread::AskForSuspend();}
	void __fastcall ForceResume() {TMyThread::ForceResume();}
	void __fastcall ForceSuspend() {TMyThread::ForceSuspend();}
	void __fastcall CheckAskForSuspend() {TMyThread::CheckAskForSuspend();}
	void __fastcall SetIsTerminated() {TMyThread::SetIsTerminated();}
	void __fastcall SetStarted() {TMyThread::SetStarted();}
	bool __fastcall readExecuting() {return !Suspended & !Terminated;}
protected:
	virtual void __fastcall ExecuteLoopBody() {}
public:
	__fastcall TStartStopThread() : TMyThread(true) {}
	__fastcall ~TStartStopThread() {}

	virtual void __fastcall Start() {AskForResume();}
	virtual void __fastcall Stop() {AskForSuspend();}

	void __fastcall WaitWhileStops() {if(!Suspended){Stop(); while(WaitForSingleObject(SuspendedEvent, 10)!=WAIT_OBJECT_0 && !Terminated);}}

	__property bool Executing = {read=readExecuting};
};

//---------------------------------------------------------------------------
// Timer Threads
//---------------------------------------------------------------------------
class TCustomTimerThread : public TStartStopThread
{
private:
	int step;
	long __fastcall readTickCounter(void) {return ::GetTickCount();}//Gr32_system::GetTickCount();}
	void __fastcall writeStep(int value) {if(value<MinimalStep) step=MinimalStep; else step=value;}
protected:
	void __fastcall MinStepSleep() {WaitForSingleObject(IdleEvent, step);}
public:
	__fastcall TCustomTimerThread() : TStartStopThread() {step=MinimalStep;}
	__fastcall ~TCustomTimerThread() {}

	__property int MinStep = {read=step, write=writeStep};
	__property unsigned long TickCounter = {read=readTickCounter};
};

class TTimerThread : public TCustomTimerThread
{
private:
	unsigned long interval;
	unsigned long start_tick;
	TThreadMethod ontimer;
	void __fastcall zeroTicks(unsigned long value) {start_tick=TickCounter+value;}
	unsigned long __fastcall readTicks(void) {return labs(TickCounter-start_tick);}
	__property unsigned long timer = {read=readTicks, write=zeroTicks};
protected:
	void __fastcall ExecuteLoopBody()
	{
		if(timer>=interval)
		{
			timer=timer-interval;//0;
			try	{ if(ontimer) Synchronize(ontimer); } catch(Exception &e) {}
		}
		//MySleep(MinStep);
		MinStepSleep();
	}
	void __fastcall writeInterval(int value) {if(value<MinimalStep) interval=MinimalStep; else interval=value;}
public:
	__fastcall TTimerThread(unsigned long AInterval) : TCustomTimerThread()
	{
		Interval=AInterval;
		timer=0;
		ontimer=NULL;
	}
	__fastcall ~TTimerThread() {}
	void __fastcall Start() {timer=0; TCustomTimerThread::Start();}

	__property unsigned long Interval = {read=interval, write=writeInterval};
	__property TThreadMethod OnTimer = {read=ontimer, write=ontimer};
};

class TMultiTimerThread;

struct TMultiTimerItem
{
public:
	TMultiTimerItem() {Owner=NULL;}
	TMultiTimerItem(TMultiTimerThread *AOwner) {Owner=AOwner;}
	int Index;
	unsigned long Interval;
	unsigned long Start_tick;
	bool Enabled;
	bool SingleRun;
	class TMultiTimerThread *Owner;
	TThreadMethod OnTimer;
};

class TMultiTimerThread : public TCustomTimerThread
{
private:
	TList *List;
protected:
	void __fastcall ExecuteLoopBody()
	{
		int i, cnt;
		unsigned long timer;//=TickCounter;
		TMultiTimerItem* mti;
		try
		{
			i=0; cnt=List->Count;
			while(i<List->Count)
			{
				try
				{
					mti=(TMultiTimerItem*)List->Items[i];
					timer=TickCounter;
					if(mti && mti->Enabled && (timer-mti->Start_tick)>=mti->Interval)
					{
						mti->Start_tick=timer;
						try { if(mti->OnTimer) Synchronize(mti->OnTimer); if(mti->SingleRun) mti->OnTimer=NULL;} catch(Exception &e) {}
					}
					if(mti && mti->OnTimer) i++;
					else
					{
						delete (TMultiTimerItem*)List->Items[i];
						List->Delete(i);
						cnt--;
					}
				}
				catch(Exception &e) {}
			}
			//MySleep(MinStep);
			MinStepSleep();
			if(List->Count==0) Stop();
		}
		catch(...) {}
	}
public:
	__fastcall TMultiTimerThread() : TCustomTimerThread() {List=new TList();}
	void __fastcall Clear() {Stop(); if(List) {for(int i=0; i<List->Count; i++) delete (TMultiTimerItem*)List->Items[i]; List->Clear();}}
	__fastcall ~TMultiTimerThread() {Clear(); if(List) delete List; List=NULL;}

	void __fastcall Start()
	{
		long tc=TickCounter;
		for(int i=0; i<List->Count; i++)
			((TMultiTimerItem*)List->Items[i])->Start_tick=tc;
		TCustomTimerThread::Start();
	}
	int __fastcall AddTimer(TMultiTimerItem* mti) {int i; if(mti) {i=List->Add(mti); mti->Index=i;} else i=-1; return i;}
	TMultiTimerItem* __fastcall AddTimer(int AInterval, TThreadMethod AOnTimer, bool aSingleRun=false)
	{
		TMultiTimerItem* res=NULL;
		if(AOnTimer!=NULL)
		{
			if(AInterval<MinimalStep) AInterval=MinimalStep;
			TMultiTimerItem* mti;
			mti=new TMultiTimerItem(this);
			mti->Interval=AInterval;
			mti->OnTimer=AOnTimer;
			mti->Enabled=true;
			mti->SingleRun=aSingleRun;
			mti->Start_tick=TickCounter;
			if(AddTimer(mti)>=0) res=mti;
		}
		if(Suspended && List->Count>0) Start();
		return res;
	}
	void __fastcall DeleteTimer(int Index){if(Index>=0 && Index<List->Count) {delete (TMultiTimerItem*)List->Items[Index]; List->Delete(Index);}}
};

#endif
