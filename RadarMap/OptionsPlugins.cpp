//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "OptionsPlugins.h"
#include "PlugIns.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "ImageButton"
#pragma resource "*.dfm"
TOptionsPluginsForm *OptionsPluginsForm;

//---------------------------------------------------------------------------
// TOptionsItem
//---------------------------------------------------------------------------
__fastcall TOptionsItem::TOptionsItem(TOption *aOption, TWinControl* aOwner)
{
	AnsiString str, lnk;

	owner=aOwner;
	option=aOption;
	if(owner && option)
	{
		panel=new TPanel(owner);
		panel->Parent=owner;
		panel->Left=42;
		panel->Top=0;
		panel->Width=476;
		panel->Height=103;
		panel->BevelInner=bvNone;
		panel->BevelOuter=bvNone;
		panel->Caption="";
		panel->Visible=false;
		panel->ParentColor=false;
		panel->ParentBackground=false;
		panel->Color=(TColor)0x00FFF9F8;
		label=new TLabel(panel);
		label->Parent=panel;
		label->Left=0;
		label->Top=18;
		label->Visible=true;
		label->AutoSize=true;
		str=((AnsiString)option->Name).UpperCase();
		lnk=ExtractLink(str);
		if(lnk!=NULL && lnk.Length()>0)
		{
			//ShellExecute(NULL, NULL, lnk.t_str(), NULL, NULL, SW_SHOW);
			label->OnClick=OnLabelClick;
			label->Font->Style = label->Font->Style << fsUnderline;
		}
		else label->OnClick=NULL;
		label->Caption=str;
		label->Font->Size=16;
		label->Font->Color=(TColor)0x00C0673F;
		panel2=new TPanel(panel);
		panel2->Parent=panel;
		panel2->Left=2;
		panel2->Top=61;
		panel2->Width=474;
		panel2->Height=42;
		panel2->BevelInner=bvNone;

		panel2->BevelOuter=bvNone;
		panel2->Caption="";
		panel2->Visible=true;
		panel2->ParentColor=false;
		panel2->ParentBackground=false;
		panel2->Color=(TColor)0x00E9E2E1;
		switch(option->Type)
		{
			case otInt32:
			case otDouble64:
			case otChar:
			case otString:
				control=(TWinControl*)new TEdit(panel2);
				control->Parent=panel2;
				if(option->Type==otInt32) ((TEdit*)control)->Text=IntToStr(*(int*)(option->Value));
				else if(option->Type==otDouble64) ((TEdit*)control)->Text=FloatToStrF(*(double*)(option->Value), ffFixed, 10, 3);
				else if(option->Type==otChar) ((TEdit*)control)->Text=*(char*)(option->Value);
				else if(option->Type==otString)
				{
					char* buf;

					buf=(char*)(option->Value);
					buf[option->Size]=0;
					((TEdit*)control)->Text=buf;
				}
				((TEdit*)control)->Font->Size=11;
				((TEdit*)control)->Font->Color=clBlack;
				((TEdit*)control)->Color=(TColor)0x00E9E2E1;
				((TEdit*)control)->OnKeyPress=EditKeyPress;
				((TEdit*)control)->OnExit=EditExit;
				break;
			case otBool:
				control=(TWinControl*)new TComboBox(panel2);
				control->Parent=panel2;
				((TComboBox*)control)->Style=csOwnerDrawFixed;
				((TComboBox*)control)->Ctl3D=false;
				((TComboBox*)control)->BevelInner=bvNone;
				((TComboBox*)control)->BevelOuter=bvNone;
				((TComboBox*)control)->Items->Add("Disabled");
				((TComboBox*)control)->Items->Add("Enabled");
				((TComboBox*)control)->ItemIndex=(int)*(bool*)(option->Value);
				((TComboBox*)control)->Font->Size=11;
				((TComboBox*)control)->Font->Color=clBlack;
				((TComboBox*)control)->Color=(TColor)0x00E9E2E1;
				break;
			case otArray:
			default:
				control=NULL;
		}
		if(control)
		{
			control->Left=10;
			control->Top=11;
			control->Width=454;
			control->Height=21;
			control->Enabled=!option->ReadOnly;
		}
	}
	else
	{
		panel=NULL;
		label=NULL;
		panel2=NULL;
		control=NULL;
	}
	strnset(value, 0, OptionLength);
}
//---------------------------------------------------------------------------

AnsiString __fastcall TOptionsItem::ExtractLink(AnsiString str)
{
	int i, k;
	AnsiString lnk="";

	i=str.Pos("HTTP://");
	if(i==0) i=str.Pos("WWW.");
	if(i>0)
	{
		k=i;
		while(k<str.Length() && str[k]!=' ' && str[k]!=')'  && str[k]!=']' &&
			str[k]!='}' && str[k]!='>') k++;
		lnk=str.SubString(i, k-i);
		if(lnk==NULL || lnk.Length()<4) lnk="";
	}
	return lnk;
}
//---------------------------------------------------------------------------

void __fastcall TOptionsItem::OnLabelClick(TObject *Sender)
{
	TLabel *lbl=dynamic_cast<TLabel*>(Sender);
	AnsiString lnk;

	if(lbl)
	{
		lnk=ExtractLink(lbl->Caption);
		if(lnk!=NULL && lnk.Length()>0)
			ShellExecute(NULL, NULL, lnk.c_str(), NULL, NULL, SW_SHOW);
	}
}
//---------------------------------------------------------------------------

void __fastcall TOptionsItem::EditKeyPress(TObject *Sender, wchar_t &Key)
{
	if(option && Sender)
	{
		switch(option->Type)
		{
			case otDouble64:
				if(Key=='.' || Key==',')
				{
					if(((TEdit*)Sender)->Text.Pos(DecimalSeparator)>0) Key=0;
					else Key=DecimalSeparator;
					break;
				}
			case otInt32:
				switch(Key)
				{
					case '0':
					case '1':
					case '2':
					case '3':
					case '4':
					case '5':
					case '6':
					case '7':
					case '8':
					case '9':
					case 8: break;
					default: Key=0;
				}
				break;
			case otChar:
			case otString:
			default: break;
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TOptionsItem::EditExit(TObject *Sender)
{
	if(option && Sender)
	{
		switch(option->Type)
		{
			case otDouble64:
				try
				{
					double d;
					d=StrToFloat(((TEdit*)Sender)->Text);
					if(d>option->Max) ((TEdit*)Sender)->Text=FloatToStrF(option->Max, ffFixed, 10, 3);
					else if(d<option->Min) ((TEdit*)Sender)->Text=FloatToStrF(option->Min, ffFixed, 10, 3);
				}
				catch(Exception &e)
				{
					((TEdit*)Sender)->Text=FloatToStrF(option->Min+((option->Max-option->Min)/2.), ffFixed, 10, 3);
				}
				break;
			case otInt32:
				try
				{
					int i;
					i=StrToInt(((TEdit*)Sender)->Text);
					if(i>option->Max) ((TEdit*)Sender)->Text=IntToStr((int)option->Max);
					else if(i<option->Min) ((TEdit*)Sender)->Text=IntToStr((int)option->Min);
				}
				catch(Exception &e)
				{
					((TEdit*)Sender)->Text=IntToStr((int)option->Min+((int)(option->Max-option->Min)>>1));
				}
				break;
			case otChar:
			case otString:
			default: break;
		}
	}
}
//---------------------------------------------------------------------------

char* __fastcall TOptionsItem::readValue()
{
	int i;
	double d;
	bool b;

	if(option && control)
	{
		strnset(value, 0, OptionLength);
		switch(option->Type)
		{
			case otInt32:
				i=StrToInt(((TEdit*)control)->Text);
				memcpy(value, (char*)(&i), option->Size);
				break;
			case otDouble64:
				d=StrToFloat(((TEdit*)control)->Text);
				memcpy(value, (char*)(&d), option->Size);
				break;
			case otChar:
			case otString:
				memcpy(value, ((TEdit*)control)->Text.t_str(), option->Size);
				break;
			case otBool:
				b=(bool)((TComboBox*)control)->ItemIndex;
				memcpy(value, (char*)(&b), option->Size);
				break;
			case otArray:
			default: break;
		}
	}

	return value;
}
//---------------------------------------------------------------------------

void __fastcall TOptionsItem::Apply()
{
	if(option) memcpy(option->Value, Value, OptionLength);
}

//---------------------------------------------------------------------------
// TOptionsPluginsForm
//---------------------------------------------------------------------------
__fastcall TOptionsPluginsForm::TOptionsPluginsForm(TComponent* Owner)
	: TForm(Owner)
{
	Items=new TList();
	PlugIn=NULL;
}
//---------------------------------------------------------------------------

__fastcall TOptionsPluginsForm::~TOptionsPluginsForm()
{
	Clear();
	delete Items;
}
//---------------------------------------------------------------------------

void __fastcall TOptionsPluginsForm::Clear()
{
	for(int i=0; i<Items->Count; i++)
	{
		if(Items->Items[i]) delete (TOptionsItem*)Items->Items[i];
	}
	Items->Clear();
}
//---------------------------------------------------------------------------

void __fastcall TOptionsPluginsForm::OkImageClick(TObject *Sender)
{
	TOptionsItem *item;

	if(PlugIn && PlugIn->OptionsCount>0)
	{
		for(int i=0; i<PlugIn->OptionsCount; i++)
		{
			item=(TOptionsItem*)Items->Items[i];
			if(item)
			{
				item->Apply();
            }
		}
	}
	if(PlugIn && PlugIn->OptionsCount>0)
	{
		PlugIn->SetOptions();
		PlugIn->SaveSettings();
	}
	ModalResult=mrOk;
}
//---------------------------------------------------------------------------

void __fastcall TOptionsPluginsForm::CancelImageClick(TObject *Sender)
{
	ModalResult=mrCancel;
}
//---------------------------------------------------------------------------

void __fastcall TOptionsPluginsForm::GetPlugInOptions(TPlugInObject* aPlugIn)
{
	TOption *option;
	TOptionsItem *item;
	Clear();
	PlugIn=aPlugIn;
	if(PlugIn && PlugIn->OptionsCount>0)
	{
		for(int i=0; i<PlugIn->OptionsCount; i++)
		{
			option=PlugIn->Options[i];
			if(option)
			{
				item=new TOptionsItem(option, ScrollBox1);
				item->Top=30+i*item->Height;
				Panel10->Top=item->Top+item->Height;
				item->Visible=true;
				Items->Add(item);
			}
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TOptionsPluginsForm::FormClose(TObject *Sender, TCloseAction &Action)
{
	Clear();
	PlugIn=NULL;
}
//---------------------------------------------------------------------------

void __fastcall TOptionsPluginsForm::FormShow(TObject *Sender)
{
	ScrollBox1->SetFocus();
	TitleDown=false;
}
//---------------------------------------------------------------------------

void __fastcall TOptionsPluginsForm::TitlePanelMouseDown(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y)
{
	TitleDown=true;
	TitleXY=ClientToScreen(Types::TPoint(X, Y));
}
//---------------------------------------------------------------------------

void __fastcall TOptionsPluginsForm::TitlePanelMouseMove(TObject *Sender, TShiftState Shift,
          int X, int Y)
{
	Types::TPoint tmp;

	if(TitleDown)
	{
		tmp=ClientToScreen(Types::TPoint(X, Y));
		Left+=tmp.x-TitleXY.x;
		Top+=tmp.y-TitleXY.y;
		TitleXY=tmp;
	}
}
//---------------------------------------------------------------------------

void __fastcall TOptionsPluginsForm::TitlePanelMouseUp(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y)
{
    TitleDown=false;
}
//---------------------------------------------------------------------------

