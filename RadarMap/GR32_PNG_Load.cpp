//---------------------------------------------------------------------------


#pragma hdrstop

#include "GR32_PNG_Load.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

extern TBitmap32* __fastcall Gr32_png::LoadPNG32(Classes::TStream* SrcStream)
{
	TBitmap32* bmp;
	bool b;

	bmp=new TBitmap32();
	LoadPNGintoBitmap32(bmp, SrcStream, b);
	if(b) bmp->DrawMode=dmBlend;
	else bmp->DrawMode=dmOpaque;

	return bmp;
}

extern TBitmap32* __fastcall Gr32_png::LoadPNG32(System::UnicodeString Filename)
{
	TFileStream* fs;
	TBitmap32* bmp;

	try
	{
		fs=new TFileStream(Filename, fmOpenRead);
		bmp=LoadPNG32(fs);
	}
	__finally
	{
		delete fs;
	}
	return bmp;
}


extern TBitmap32* __fastcall Gr32_png::LoadPNG32Resource(System::UnicodeString ResourceName)
{
	TBitmap32* bmp;
	TResourceStream *rs;

	try
	{
		rs=new TResourceStream((int)HInstance, ResourceName, MAKEINTRESOURCEW(10));// RT_RCDATA);
		bmp=LoadPNG32(rs);
	}
	__finally
	{
		delete rs;
	}
	return bmp;
}
