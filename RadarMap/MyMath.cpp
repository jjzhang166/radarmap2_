//---------------------------------------------------------------------------

#pragma hdrstop

#include "MyMath.h"
#include <math.h>
//---------------------------------------------------------------------------

#pragma package(smart_init)

unsigned int HexToInt(AnsiString str)
{
	int i = 0, v = 0, N;
	char *buf;

	if(str!=NULL && str.Length()>0)
	{
		N=str.Length();
		buf=str.c_str();
		if(buf[0] == '0' && buf[1] == 'x')
		{
			buf = (char*)(buf + 2);
			N -= 2;
		}
		while(N >= 0 && buf[N] == 0) N--;
		while(N >= 0 && buf[N] > 0)
		{
			if(buf[N] >= '0' && buf[N] <= '9') v += (int)(buf[N] - '0') * (int)pow(16., i);
			else if(buf[N] >= 'a' && buf[N] <= 'f') v += (int)(buf[N] - 'a' + 10) * (int)pow(16., i);
			N--;
			i++;
		}
	}
	return v;
}
//----------------------------------------------------------------------------

//---------------------------------------------------------------------------
// TFourierFamily
//---------------------------------------------------------------------------
__fastcall TFourierFamily::TFourierFamily(int ASamples)
{
	aSin=NULL;
	aCos=NULL;
	powers=NULL;
	samples=0;
	samplespower=0;
	Tab_cos=NULL;
	Tab_sin=NULL;
	ComplexData=NULL;

	Samples=ASamples;
}
//---------------------------------------------------------------------------

__fastcall TFourierFamily::~TFourierFamily()
{
	if(aSin) delete[] aSin;
	if(aCos) delete[] aCos;
	if(powers) delete[] powers;
	if(Tab_cos) delete[] Tab_cos;
	if(Tab_sin) delete[] Tab_sin;
	if(ComplexData) delete[] ComplexData;
}
//---------------------------------------------------------------------------

void __fastcall TFourierFamily::writeSamples(int value)
{
	if(value>samples || value<=(samples>>1))
	{
		samplespower=-1;
		do
		{
			samplespower++;
			samples=(int)ldexp(1., samplespower);
		} while(value>=samples);
		if(samples>0 && samplespower>0) Init();
	}
}
//---------------------------------------------------------------------------

void __fastcall TFourierFamily::Init()
{
	double h, x;
	int lv, le, lsx, i, j;

	if(aSin) delete[] aSin;
	if(aCos) delete[] aCos;
	if(powers) delete[] powers;
	if(Tab_cos) delete[] Tab_cos;
	if(Tab_sin) delete[] Tab_sin;
	if(ComplexData) delete[] ComplexData;

	aSin=new double[samples];
	aCos=new double[samples];
	powers=new int[samplespower];
	Tab_cos=new double[samplespower];
	Tab_sin=new double[samplespower];
	ComplexData=new Complex[samples*2];

	for(i=0, j=1; i<samplespower; i++, j=j<<1) powers[i]=j;//powers[i+1]=powers[i] << 1;

	// Fill cas tables
	h=M_PI/(double)(samples >> 1);
	x=0.;
	for(i=0; i<samples; i++)
	{
		x+=h;
		aSin[i]=Sin(x);
		aCos[i]=Cos(x);
	}

	for(i=0; i<samplespower; i++)
	{
		lv=i+1;
		for(le=1;lv>0;lv--)	le=le << 1;
		lsx=le >> 1;
		*(Tab_cos+i)=cos(M_PI/(double)lsx);
		*(Tab_sin+i)=sin(M_PI/(double)lsx);
	}

	for(i=0; i<samples*2; i++) ComplexData[i]=Complex();
}
//---------------------------------------------------------------------------

void __fastcall TFourierFamily::FHT(double *Outa, int sg)
{
	int q, u, u1, i, d, e, k, k1, k2, l;
	int v2, v3, t1, t3;
	double x, y, vf1, vf2, vf3, vf4;

	try
	{
		k=-1; i=-1;
		t1=samples-3;
		while(i<t1)
		{
			i++;
			k1=samplespower-1;
			k-=powers[k1];
			while(k>=-1 && k1>0)
			{
				k1--;
				k-=powers[k1];
			}
			k+=powers[k1+1];
			if(i>k && k>-2)
			{
				x=Outa[i+1];
				Outa[i+1]=Outa[k+1];
				Outa[k+1]=x;
			}
		}
		t1=samples-2;
		for(i=0; i<=t1; i+=2)
		{
			vf1=Outa[i]; vf2=Outa[i+1];
			Outa[i]=vf1+vf2;
			Outa[i+1]=vf1-vf2;
		}
		t1=samples-4;
		for(i=0; i<=t1;i+=4)
		{
			vf1=Outa[i]; vf2=Outa[i+1]; vf3=Outa[i+2]; vf4=Outa[i+3];
			Outa[i]=vf1+vf3;
			Outa[i+1]=vf2+vf4;
			Outa[i+2]=vf1-vf3;
			Outa[i+3]=vf2-vf4;
		}
		u=samplespower-1;
		u1=4;
		t1=samplespower-1;
		t3=samples >> 2;
		for(l=2; l<=t1; l++)
		{
			v2=2*u1;
			u--;
			k1=powers[u-1]-1; v3=k1+1;
			for(q=0; q<samples; q+=v2)
			{
				i=q; d=i+u1;
				vf1=Outa[i]; vf2=Outa[d];
				Outa[i]=vf1+vf2;
				Outa[d]=vf1-vf2;
				k2=d-1;
				for(k=k1; k<t3; k+=v3)
				{
					i++; d=i+u1;
					e=k2+u1;
					vf1=Outa[e]; vf2=aCos[k]; vf3=Outa[d]; vf4=aSin[k];
					x=vf1*vf2-vf3*vf4;
					y=vf3*vf2+vf1*vf4;
					vf1=Outa[i]; vf2=Outa[k2];
					vf4=vf2+x;
					vf3=vf2-x;
					vf2=vf1-y;
					vf1+=y;
					Outa[i]=vf1;
					Outa[d]=vf2;
					Outa[k2]=vf3;
					Outa[e]=vf4;
					k2--;
				}
				e=k2+u1;
			}
			u1=v2;
		}
		t1=samples+1-(samples-1)*sg;
		for(i=0; i<samples; i++) Outa[i]=(Outa[i]*2)/t1;
	}
	catch(Exception &e) {}
}
//---------------------------------------------------------------------------

void __fastcall TFourierFamily::Envelope(TSample *DataIn, TSample *DataOut, int N)
{
	int j;
	double *buf, si, max, tv;

	try
	{
		if(N>samples || N<=(samples>>1)) Samples=N;
		buf=new double[samples*2];
		try
		{
			max=DataIn[0];
			for(j=0; j<N; j++)
			{
				if(max<fabs(DataIn[j])) max=fabs(DataIn[j]);
				buf[j]=DataIn[j];
			}
			for(j=N; j<samples; j++) buf[j]=0;

			FHT(buf, -1);
			for(j=0; j<samples>>1; j++)
			{
				si=buf[j];
				buf[j]=(-1.)*buf[samples-j];
				buf[samples-j]=si;
			}
			buf[0]=0.; buf[samples>>1]=0.;
			FHT(buf, 1);
			for(int j=0; j<N; j++)
			{
				if(buf[j]>max) buf[j]=max;
				else if(buf[j]<-max) buf[j]=-max;
				tv=sqrt(DataIn[j]*DataIn[j]+buf[j]*buf[j]);
				if(fabs(tv)>max) tv=max;
				DataOut[j]=tv;
			}
		}
		__finally
		{
			delete[] buf;
		}
	}
	catch(Exception &e) {}
}
//---------------------------------------------------------------------------

void __fastcall TFourierFamily::FFT(Complex *Out, int sg)
{
	int id, lv, jg, i, l, le, lsx, ip, k;
	struct Complex t, u, w;

	jg=1+(samples >> 1);
	for(i=2;i<samples;i++)
	{
		if(i<jg)
		{
			t=*(Out+jg-1);
			*(Out+jg-1)=*(Out+i-1);
			*(Out+i-1)=t;
		}
		k=samples >> 1;
		while(k<jg)
		{
			jg-=k;
			k=k >> 1;
		}
		jg+=k;
	}
	for(l=1; l<=samplespower; l++)
	{
		lv=l;
		for(le=1; lv>0; lv--) le=le << 1;
		lsx=le >> 1;
		u=Complex(1., 0.);
		w=Complex(*(Tab_cos+l-1), *(Tab_sin+l-1)*sg);
		for(jg=1;jg<=lsx;jg++)
		{
			for(i=jg;i<=samples;i+=le)
			{
				ip=i+lsx-1;
				id=i-1;
				t=*(Out+ip)*u;
				*(Out+ip)=*(Out+id)-t;
				*(Out+id)+=t;
			}
			u*=w;
		}
		u*=w;
	}
	if(sg==1)
		for(k=0; k<samples; k++) Out[k]/=(double)samples;
}
//---------------------------------------------------------------------------

void __fastcall TFourierFamily::FrequencyFilter(TSample *DataIn, TSample *DataOut, double* FR, int N)
{
	int n=N>>4, d, j;

	if(Samples-N<n)
	{
		Samples=N+n;
		n=Samples-N;
		d=1;
	}
	else d=0;
	for(j=0; j<N; j++) ComplexData[j]=Complex(DataIn[j], 0.0);
	for(j=N; j<Samples; j++)
		ComplexData[j]=Complex((0.5*cos((double)(j-N)*M_PI/(double)N)+0.5)*DataIn[N-1], 0.0);
	FFT(ComplexData, 1);
	for(j=0; j<Samples; j++) ComplexData[j]=ComplexData[j]*FR[j>>d];
	FFT(ComplexData, -1);
	for(j=0; j<N; j++) DataOut[j]=ComplexData[j].x;
}
//---------------------------------------------------------------------------

