//---------------------------------------------------------------------------
#pragma hdrstop

#include "CacheManager.h"
#include "Inp_segy.h"
#include "Out_segy.h"
#include "Radar.h"

#ifdef prismAllH
	#include "Main.h"
	#include "Input.h"
	#include "Figures.h"
	#include "Child.h"
#else
	#include "Manager.h"
#endif

//---------------------------------------------------------------------------

#pragma package(smart_init)

TCacheObjectsManager *CacheManager;

//---------------------------------------------------------------------------
// TCacheObject
//---------------------------------------------------------------------------
__fastcall TCacheObject::TCacheObject(TCacheObjectsManager *AOwner, TCacheObjectType t,
	void* ASource): TMyObject()
{
	owner=AOwner;
	type=t;
    filepointer=0;
    source=ASource;
    size=SizeOnDisk();
	cachealone=false;
	rewritezeros=false;
    cached=false;
	BusyLocker=new TBusyLocker();
}

//---------------------------------------------------------------------------

__fastcall TCacheObject::~TCacheObject()
{
	BusyLocker->UnLock();
	if(owner)
    {
		if(Cached) ApproveRestore(owner->GetTempFile(this), source);
		ClearSource();
        if(index>=0) owner->RemoveChildFromList(this);
    }
	delete BusyLocker;
}
//---------------------------------------------------------------------------

void __fastcall TCacheObject::ApproveRestore(TUnsharedFile *cFile, void* data)
{
    source=data;
    cached=false;
	if(cFile)
	{
        if(cachealone)
        {
            if(cFile->Delete())
            {
                delete cFile;
                cFile=NULL;
            }
        }
        else if(cFile->Open() && size>0 && rewritezeros)
            cFile->FileMemSet(filepointer, fmdBegin, size, 0x00);
	}
}
//---------------------------------------------------------------------------

bool __fastcall TCacheObject::CacheIt(TUnsharedFile *cFile)
{
    bool res=false;
	unsigned long w, fp;

	if(source && SizeInMemory>0 && cFile && cFile->Open())
	{
		BusyLocker->SequentLock();
		try
		{
			try
			{
                if(!cachealone)
                {
                    w=cFile->Write(source, size, 0, fmdEnd, fp);
    				res=(w==size && fp!=INVALID_SET_FILE_POINTER); //success
                }
				else res=CacheToHDD(cFile, fp);
				if(res)
				{
					ClearSource();
					filename=cFile->FileName;
					filepointer=fp-size;
					res=cached=true;
				}
			}
			catch(...) {res=cached=false;}
		}
		__finally
		{
			BusyLocker->UnLock();
		}
	}

	return res;
}
//---------------------------------------------------------------------------

void* __fastcall TCacheObject::Restore(TUnsharedFile *cFile, void* Param)
{
    void* buf=NULL;
	unsigned long w;

	if(owner && !InRestore)
	{
        inrestore=true;
		BusyLocker->SequentLock();
		try
		{
			try
			{
                if(cached)
                {
					if(cFile && cFile->Open()) buf=RestoreFromHDD(cFile, Param);
                }
                else
                {
					owner->RemoveChildFromQueue(this);
                    buf=(char*)source;
                }
                if(buf)
                {
					ApproveRestore(cFile, (void*)buf);
                    source=NULL;
                }
            }
            catch(...) {}
        }
        __finally
        {
            BusyLocker->UnLock();
            inrestore=false;
        }
    }
	return buf;
}

//---------------------------------------------------------------------------
// TCacheArrayObject
//---------------------------------------------------------------------------
void* TCacheArrayObject::RestoreFromHDD(TUnsharedFile *cFile, void* Param)
{
    unsigned long w;
    char* buf=NULL;

    if(cFile)
    {
        w=cFile->FileSize;
        if(w!=INVALID_FILE_SIZE && w>filepointer)
		{
			buf=new char[size];
			w=cFile->Read(buf, size, filepointer, fmdBegin);
			if(w!=size)
			{
				delete[] buf;
				buf=NULL;
			}
        }
    }
    return buf;
}

//---------------------------------------------------------------------------
// TCacheProfileSGYObject
//---------------------------------------------------------------------------
long __fastcall TCacheProfileSGYObject::SizeOnDisk()
{
	TProfile* Prof=(TProfile*)source;
	long res;

	if(Prof) res=(3200+400+(240+Prof->Samples*sizeof(short int))*Prof->Traces);
	else res=size;

	return res;
}
//---------------------------------------------------------------------------

unsigned long TCacheProfileSGYObject::readSizeInMemory()
{
    unsigned long ul=sizeof(TCacheProfileSGYObject);

    if(source) ul+=((TProfile*)source)->SizeInMemory;

    return ul;
}
//---------------------------------------------------------------------------

bool TCacheProfileSGYObject::CacheToHDD(TUnsharedFile *cFile, unsigned long &pointer)
{
    bool res;

	res=(cFile && Output_SEGY_Unshared(cFile, (TProfile*)source, NULL)==0);
	if(res) cFile->Open();
	pointer=0;

    return res;
}
//---------------------------------------------------------------------------*/

void* TCacheProfileSGYObject::RestoreFromHDD(TUnsharedFile *cFile, void* Param)
{
	TProfile* Prof=new TProfile();

	if(size>0 && Input_SEGY_Unshared(cFile, Prof, NULL, false, 0, 0, 0, false, true)!=0)
	{
		delete Prof;
		Prof=NULL;
	}
	return Prof;
}
//---------------------------------------------------------------------------*/

void __fastcall TCacheProfileSGYObject::ClearSource()
{
    if(source)
    {
        delete (TProfile*)source;
        source=NULL;
    }
}

//---------------------------------------------------------------------------
// TCacheAnnotationsObject
//---------------------------------------------------------------------------
long __fastcall TCacheAnnotationsObject::SizeOnDisk()
{
	TList* List=(TList*)source;
	long res;

	if(List) res=110*List->Count;
	else res=size;

	return res;
}
//---------------------------------------------------------------------------

unsigned long TCacheAnnotationsObject::readSizeInMemory()
{
    unsigned long ul=sizeof(TCacheAnnotationsObject);

    if(source)
    {
        ul+=sizeof(TList);
 #ifdef prismAllH
		ul+=((TList*)source)->Count*sizeof(TFigures);
 #endif
    }
    return ul;
}
//---------------------------------------------------------------------------

bool TCacheAnnotationsObject::CacheToHDD(TUnsharedFile *cFile, unsigned long &pointer)
{
    bool res;

#ifdef prismAllH
	res=(cFile && SaveAnnotations(cFile, (TList*)source)==0);
#else
	res=true;
#endif
	pointer=0;

    return res;
}
//---------------------------------------------------------------------------*/

void* TCacheAnnotationsObject::RestoreFromHDD(TUnsharedFile *cFile, void* Param)
{
	TList* Figures=new TList();
#ifdef prismAllH
	if(size>0 && LoadAnnotations(cFile, Figures)!=0)
	{
		for(int i=0; i<Figures->Count; i++)
			if(Figures->Items[i])
				delete Figures->Items[i];
		Figures->Clear();
		delete Figures;
		Figures=NULL;
	}
#endif
	return Figures;
}
//---------------------------------------------------------------------------*/

void __fastcall TCacheAnnotationsObject::ClearSource()
{
	TList* Figures=(TList*)source;

    if(Figures)
	{
#ifdef prismAllH
		for(int i=0; i<Figures->Count; i++)
			if(Figures->Items[i])
				delete (TFigures*)Figures->Items[i];
#endif
		Figures->Clear();
        delete Figures;
        source=NULL;
    }
}

//---------------------------------------------------------------------------
// TCacheProfileDataObject
//---------------------------------------------------------------------------
bool TCacheProfileDataObject::CacheToHDD(TUnsharedFile *cFile, unsigned long &pointer)
{
	bool res;

	res=(cFile && Output_SEGY_Unshared(cFile, (TProfile*)source, NULL)==0);
	if(res) cFile->Open();
	pointer=0;

    return res;
}
//---------------------------------------------------------------------------*/

void* TCacheProfileDataObject::RestoreFromHDD(TUnsharedFile *cFile, void* Param)
{
	TProfile* Prof=(TProfile*)Param;

	if(size>0 && Prof && Input_SEGY_Unshared(cFile, Prof, NULL, false, 0, 0, 0, true, true)!=0)
	{
		Prof=NULL;
	}
	return Prof;
}
//---------------------------------------------------------------------------*/

void __fastcall TCacheProfileDataObject::ClearSource()
{
	TProfile* Prof=(TProfile*)source;
	TTrace *ptr;

	if(Prof)
	{
		ptr=Prof->FirstTracePtr;
		while(ptr)
		{
			ptr->CacheIt(true);
			ptr=ptr->PtrUp;
        }
    }
}
//---------------------------------------------------------------------------
// TCacheWriteThread
//---------------------------------------------------------------------------
__fastcall TCacheWriteThread::TCacheWriteThread(TCacheObjectsManager* AOwner,
	TEventedQueue *AQueue): TStartStopThread()
{
	Owner=AOwner;
	Queue=AQueue;
}
//---------------------------------------------------------------------------

void __fastcall TCacheWriteThread::ExecuteLoopBody()
{
	TCacheObject *co;
	HANDLE hndl;
	AnsiString str;
	TUnsharedFile* cFile;
    unsigned long ul;

	if(Owner && Queue && DirectoryExists(Owner->TempPath))
	{
		if(WaitForSingleObject(Queue->NotEmptyEvent, 10)==WAIT_OBJECT_0)
		{
            ul=MyFiles::GetCurrentProcessSize();
            if(ul==0 || ul>Owner->MemoryLevelToStartCache)
            {
                co=(TCacheObject*)Queue->Pop();
                if(co && !co->Cached)
                {
                    cFile=Owner->OpenTempFile(co);
                    if(cFile && cFile->Open() && !co->CacheIt(cFile)) Queue->Push(co);
                }
            }
		}
	}
	MySleep(10);
}

//---------------------------------------------------------------------------
// TCacheWalkerThread
//---------------------------------------------------------------------------
__fastcall TCacheWalkerThread::TCacheWalkerThread(TCacheObjectsManager* AOwner,
	TObject* AManager): TStartStopThread()
{
	Owner=AOwner;
	PassedTime=0;
	Manager=AManager;
}
//---------------------------------------------------------------------------

__fastcall TCacheWalkerThread::~TCacheWalkerThread()
{

}
//---------------------------------------------------------------------------

void __fastcall TCacheWalkerThread::ExecuteLoopBody()
{
	unsigned long ul, at, sec;
	div_t x;

	if(Owner && DirectoryExists(Owner->TempPath) && PassedTime>0)
	{
		ul=MyFiles::GetCurrentProcessSize();
		if(ul==0 || ul>Owner->CriticalMemoryLevel) sec=1;
		else if(ul>Owner->MaxMemoryLevel) sec=2;
		else sec=5;
		x=div(PassedTime, sec); //each 5 seconds
		if(ul==0 || ul>Owner->MemoryLevelToStartCache)
		{
#ifdef prismAllH
			TChild *child;
			if(x.quot>0 && x.rem==0 && MainForm->MDIChildCount>0 && !Owner->StopWalking)
			{

				for(int i=0; i<MainForm->MDIChildCount; i++)
				{
					child=dynamic_cast<TChild*>(MainForm->MDIChildren[i]);
					if(child && !Owner->StopWalking)
					{
						child->UndoList->CacheUnused();
						ul=MyFiles::GetCurrentProcessSize();
						if(ul>0 && ul<Owner->MemoryLevelToStartCache) break;
					}
				}
				if(ul==0 || ul>Owner->MaxMemoryLevel)
				{
					if(ul>0) ul=CrashMemoryLevel-ul;
					ul=ul>>20;
					if(ul>512) at=AccessGapTime;
					else if(ul>256) at=AccessMaxGapTime;
					else at=AccessCriticalGapTime;
					for(int i=0; i<MainForm->MDIChildCount; i++)
					{
						child=dynamic_cast<TChild*>(MainForm->MDIChildren[i]);
						if(child && !Owner->StopWalking)
						{
							if(!child->Cached && child->AccessTimeOut()>at && ((RadarDialog && RadarDialog->Visible) || child!=MainForm->ActiveMDIChild))
							{
								child->CacheIt();
								if(at!=AccessCriticalGapTime) break;
							}
						}
					}
				}
				ul=MyFiles::GetCurrentProcessSize();
			}
			if(RadarDialog && RadarDialog->Visible)
			{
				if(ul>Owner->CriticalMemoryLevel) Synchronize(ShowRDWarning);
				else if(RadarDialog->Warning->WarningState(rwtLowMemory))
					Synchronize(HideRDWarning);
			}
#else
			TProfileSatellites* Satellites;
			int uc;

			if(ul==0 || (CacheManager && ul>CacheManager->MaxMemoryLevel)) uc=0;
			else uc=1;
			if(ul>0) ul=CrashMemoryLevel-ul;
			ul=ul>>20;
			if(ul>512) at=AccessGapTime;
			else if(ul>256) at=AccessMaxGapTime;
			else at=AccessCriticalGapTime;
			if(Manager && x.quot>0 && x.rem==0 && ((TRadarMapManager*)Manager)->Satellites &&
				((TRadarMapManager*)Manager)->Satellites->Count>1)
			{
				for(int i=0; i<((TRadarMapManager*)Manager)->Satellites->Count; i++)
				{
					if(i<((TRadarMapManager*)Manager)->CurrentIndex-uc ||
						i>((TRadarMapManager*)Manager)->CurrentIndex+uc)
					{
						Satellites=((TRadarMapManager*)Manager)->Satellites->Items[i];
						if(Satellites)
						{
							for(int j=0; j<Satellites->ProfilesCount; j++)
							{
								if(Satellites->Profiles[j]!=((TRadarMapManager*)Manager)->ReceivingProfile &&
									!Satellites->Profiles[j]->ProfileDataCached &&
									Satellites->Profiles[j]->AccessTimeOut()>at)
										Satellites->Profiles[j]->CacheIt();
							}
							ul=MyFiles::GetCurrentProcessSize();
							if(ul>0 && ul<Owner->MemoryLevelToStartCache) break;
						}
					}
				}
			}
#endif
		}
	}
	MySleep(1000);
	PassedTime++;
	if(PassedTime>=86400) PassedTime=0; //DayNight time = 86400 sec
}
//---------------------------------------------------------------------------

void __fastcall TCacheWalkerThread::ShowRDWarning()
{
#ifdef prismAllH
	if(RadarDialog && RadarDialog->Warning && RadarDialog->Visible)
	{
		RadarDialog->Warning->ShowWarning(rwtLowMemory);
	}
#else

#endif
}
//---------------------------------------------------------------------------

void __fastcall TCacheWalkerThread::HideRDWarning()
{
#ifdef prismAllH
	if(RadarDialog && RadarDialog->Warning && RadarDialog->Visible)
	{
		RadarDialog->Warning->HideWarning(rwtLowMemory);
	}
#else

#endif
}

//---------------------------------------------------------------------------
// TCacheObjectsManager
//---------------------------------------------------------------------------
__fastcall TCacheObjectsManager::TCacheObjectsManager(AnsiString tmpFolderM,
	AnsiString tmpFileM, TObject* AManager)
{
	char temp[1024];
	AnsiString mask;
	TDateTime dt;
	unsigned short h, m, s, ms;

	Manager=AManager;
	if(tmpFolderM!="") tempfoldermask=tmpFolderM;
	else tempfoldermask=Application->Title+"_Temp";
	if(tmpFileM!="") tempfilemask=tmpFileM;
	else tempfilemask="temporary";
	if(GetTempPath(sizeof(temp), temp)) mask=temp;
	else mask=ExtractFilePath(Application->ExeName)+"Temp\\";
	if(MyFiles::ProcessCopiesNumber(ExtractFileName(Application->ExeName))<=1)
	{
		WIN32_FIND_DATA FileData;
		HANDLE hSearch;
		AnsiString S;

		hSearch=FindFirstFile((mask+tempfoldermask+"*").c_str(), &FileData);
		if(hSearch!=INVALID_HANDLE_VALUE)
		{
			do
			{
				S=FileData.cFileName;
				if(S!="." && S!="..")
				{
					if(FileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
					{
						MyFiles::EmptyFolder(mask+S);
						RemoveDirectory((mask+S).c_str());
					}
				}
			} while(FindNextFile(hSearch, &FileData));
			FindClose(hSearch);
		}
	}
	mask+=tempfoldermask;
	dt=Now();
	DecodeTime(dt, h, m, s, ms);
	temppath=mask+IntToStr((int)dt)+IntToStr(ms+s*1000+m*100000+h*10000000);
	if(!DirectoryExists(temppath))
	{
		if(!CreateDirectory(temppath.c_str(), NULL))
		{
			temppath="";
			List=NULL;
			CacheQueue=NULL;
			throw Exception("Cannot create Directory "+temppath+"!");
		}
	}
	TempFiles=new TUnsharedFilesList(this);
	CommonTempFile=NewTempFile();

	List=new TCacheObjectsList(this);
	List->DeleteItemOwnerAcknolegment=true;
	CacheQueue=new TObjectEventedQueue();
	CacheQueue->FreeOnDelete=false;
	CacheQueue->Locking=true;
	FileIndex=1;
	CheckFreeSpaceOnHDD=true;
	maxffasize=100*1024*1024; //Bytes = 100 MB
	memlvl2cache=512*1024*1024; //768*1024*1024; //128*1024*1024;//Bytes = 512 MB
	maxmemlvl=778*1024*1024; //1260*1024*1024; //Bytes = 778 MB
	criticalmemlvl=1024*1024*1024; //1534*1024*1024; //Bytes = 1 GB
	criticalhddspace=1024*1024*1024; // Bytes = 1 GB
	stopwalking=false;

	RestoreLock=new TBusyLocker();

	CacheThread=new TCacheWriteThread(this, CacheQueue);
	CacheThread->Start();
	CacheWalker=new TCacheWalkerThread(this, Manager);
	CacheWalker->Start();
}
//---------------------------------------------------------------------------

__fastcall TCacheObjectsManager::~TCacheObjectsManager()
{
	if(RestoreLock)
	{
		RestoreLock->UnLock();
		delete RestoreLock;
	}
	RestoreLock=NULL;
	if(CacheThread && CacheThread->FinalizeBeforeRelease()) try {delete CacheThread;} catch(...) {}
	CacheThread=NULL;
        if(CacheWalker && CacheWalker->FinalizeBeforeRelease()) try {delete CacheThread;} catch(...) {}
	CacheWalker=NULL;
	if(List) delete List;
	if(CacheQueue) delete CacheQueue;
	if(TempFiles) delete TempFiles;
	if(DirectoryExists(temppath))
	{
		MyFiles::EmptyFolder(temppath);
		RemoveDirectory(temppath.c_str());
	}
}
//---------------------------------------------------------------------------

TUnsharedFile* __fastcall TCacheObjectsManager::NewTempFile()
{
	AnsiString mask;
	TUnsharedFile *res;

	do
	{
		mask=temppath+"\\"+tempfilemask+IntToStr(FileIndex)+".tmp";
		FileIndex++;
	}
	while(FileIndex!=0 && FileExists(mask));
	res=new TUnsharedFile(TempFiles, mask, false);
	if(!res->Open())
	{
		delete res;
		res=NULL;
	}
	else TempFiles->Add(res);

	return res;
}
//---------------------------------------------------------------------------

TCacheObject* __fastcall TCacheObjectsManager::CacheIt(TCacheObjectType tT, void* Source, long Size, TSynchronizeMethod Synchro)
{
	TCacheObject* res=NULL;

	if(Source)
	{
		switch(tT)
		{
			case cotArray: if(Size>0) res=(TCacheObject*)(new TCacheArrayObject(this, Source, Size)); break;
			case cotProfileSGY: res=(TCacheObject*)(new TCacheProfileSGYObject(this, Source)); break;
			case cotAnnotations: res=(TCacheObject*)(new TCacheAnnotationsObject(this, Source)); break;
			case cotProfileData: res=(TCacheObject*)(new TCacheProfileDataObject(this, Source)); break;
		}
		if(!CacheIt(res, Synchro))
		{
			if(res) delete res;
			res=NULL;
		}
	}

	return res;
}
//---------------------------------------------------------------------------

void __fastcall TCacheObjectsManager::ShowHDDAlert()
{
#ifdef prismAllH
	Application->MessageBox(MainForm->FindLanguageStr(MainForm->NumLanguage, 5022,
		"Your free Hard Drive Disk space is very low! Please save your results, free disk space and restart the program.").c_str(), ((AnsiString)_RadarMapName).c_str(), MB_OK | MB_ICONWARNING);
#else

#endif
}
//---------------------------------------------------------------------------

TCacheObject* __fastcall TCacheObjectsManager::CacheIt(TCacheObject* co, TSynchronizeMethod Synchro)
{
	TCacheObject* res=NULL;

	if(List && CacheQueue && co)
	{
        if(CheckFreeSpaceOnHDD)
        {
			__int64 FreeBytesAvailable, TotalNumberOfBytes, TotalNumberOfFreeBytes;
#ifdef prismAllH
			if(Sysutils::GetDiskFreeSpaceEx(temppath.c_str(), FreeBytesAvailable, TotalNumberOfBytes, &TotalNumberOfFreeBytes))
#elif defined(_RAD101)
			ULARGE_INTEGER a, b, c;
	#pragma warn -8060
			if(GetDiskFreeSpaceExW(((UnicodeString)temppath).w_str(), &a, &b, &c) && (FreeBytesAvailable=a.QuadPart))
	#pragma warn +8060
#else
			if(Sysutils::GetDiskFreeSpaceEx(((UnicodeString)temppath).w_str(), FreeBytesAvailable, TotalNumberOfBytes, &TotalNumberOfFreeBytes))
#endif
			{
				if(FreeBytesAvailable<(__int64)criticalhddspace)
				{
                    CheckFreeSpaceOnHDD=false;
                    if(Synchro) (Synchro)(ShowHDDAlert);
                    else ShowHDDAlert();
                }
            }
        }
		List->Add(co);
		CacheQueue->Push(co);
		res=co;
	}

	return res;
}
//---------------------------------------------------------------------------

void* __fastcall TCacheObjectsManager::Restore(TCacheObject* co, void* Param)
{
	void* buf=NULL;
	TUnsharedFile *cFile;
    unsigned long ul;

	RestoreLock->SequentLock();
	if(co && List->IndexOf(co)>=0)
	{
        do
        {
            ul=MyFiles::GetCurrentProcessSize();
            if(CacheQueue->Count>0 && (ul==0 || ul>CriticalMemoryLevel)) MySleep(10);
        } while(CacheQueue->Count>0 && (ul==0 || ul>CriticalMemoryLevel));
		CacheThread->Stop();
		try
		{
			try
            {
				while(CacheThread->Executing) MySleep(0);
				buf=co->Restore(TempFiles->FindByFileName(co->FileName), Param);
            }
			catch(...) {}
		}
		__finally
		{
			CacheThread->Start();
			RestoreLock->UnLock();
        }
    }
	return buf;
}
//---------------------------------------------------------------------------

TUnsharedFile* __fastcall TCacheObjectsManager::OpenTempFile(TCacheObject *co)
{
	HANDLE hndl;
	unsigned long fSize=0;
	AnsiString res="";
	TUnsharedFile *cFile=NULL;

	if(co && List->IndexOf(co)>=0)
	{
		if(co->CacheAlone) cFile=NewTempFile();
		else
		{
			CommonTempFile->Open();
			fSize=CommonTempFile->FileSize;
			if(fSize>0 && fSize!=INVALID_FILE_SIZE)
			{
				fSize+=co->SizeOnDisk();
				if(fSize>MaxFreeForAllSize)
				{
					cFile=NewTempFile();
					if(cFile) CommonTempFile=cFile;
				}
			}
			cFile=CommonTempFile;
		}
	}
	return cFile;
}
//---------------------------------------------------------------------------

void __fastcall TCacheObjectsManager::RemoveChildFromList(TMyObject* o)
{
    if(o && List->IndexOf(o)>=0)
    {
        List->Remove(o->Index);
        RemoveChildFromQueue(o);
        /*if(((TCacheObject*)o)->Cached)
        {
			((TCacheObject*)o)->ApproveRestore(TempFiles->FindByFileName(((TCacheObject*)o)->FileName), NULL);
        }*/
    }
}
//---------------------------------------------------------------------------

unsigned long __fastcall TCacheObjectsManager::readSizeInMemory()
{
    unsigned long ul=sizeof(TCacheObjectsManager);

    ul=sizeof(TCacheObjectsManager);
    ul+=sizeof(TEventedQueue)+sizeof(TCacheObject)*CacheQueue->Count;
    ul+=sizeof(TUnsharedFilesList) +sizeof(TUnsharedFile)*TempFiles->Count;
    ul+=sizeof(CacheThread);
    for(int i=0; i<List->Count; i++)
        if(List->Items[i])
            ul+=List->Items[i]->SizeInMemory;

    return ul;
}
//---------------------------------------------------------------------------
