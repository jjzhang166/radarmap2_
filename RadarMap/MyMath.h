//---------------------------------------------------------------------------

#ifndef MyMathH
#define MyMathH

#include <vcl.h>
//---------------------------------------------------------------------------
typedef float TSample;

unsigned int HexToInt(AnsiString str);

double sqr(double x) {return x*x;}

struct Complex
{
	double x, y;

	Complex() {x=y=0.0;}
	Complex(double X, double Y) {x=X; y=Y;}

	Complex operator+(Complex A)
	{
		Complex Result;
		Result.x=x+A.x;
		Result.y=y+A.y;
		return Result;
	}
	Complex operator-(Complex A)
	{
		Complex Result;
		Result.x=x-A.x;
		Result.y=y-A.y;
		return Result;
	}
	Complex operator*(Complex A)
	{
		Complex Result;
		Result.x=x*A.x-y*A.y;
		Result.y=x*A.y+y*A.x;
		return Result;
	}
	Complex operator*(double C)
	{
		Complex Result;
		Result.x=x*C;
		Result.y=y*C;
		return Result;
	}
	Complex operator/(double C)
	{
		Complex Result;
		Result.x=x/C;
		Result.y=y/C;
		return Result;
	}
	void operator+=(Complex A)
	{
		x+=A.x;
		y+=A.y;
	}
	void operator-=(Complex A)
	{
		x-=A.x;
		y-=A.y;
	}
	void operator*=(double C)
	{
		x*=C;
		y*=C;
	}
	void operator*=(Complex A)
	{
		double t;

		t=x*A.x-y*A.y;
		y=x*A.y+y*A.x;
		x=t;
	}
	void operator/=(double C)
	{
		x/=C;
		y/=C;
	}
};

class TFourierFamily: public TObject
{
private:
	int samples, samplespower;
	//FHT
	int *powers;
	double *aSin, *aCos;
	//FFT
	double *Tab_cos, *Tab_sin;
	Complex *ComplexData;
	void __fastcall Init();
protected:
	void __fastcall writeSamples(int value);
	void __fastcall FHT(double *Outa, int sg);
	void __fastcall FFT(Complex *Outa, int sg);
public:
	__fastcall TFourierFamily(int ASamples);
	__fastcall ~TFourierFamily();

	void __fastcall Envelope(TSample *DataIn, TSample *DataOut, int N=512);
	void __fastcall FrequencyFilter(TSample *DataIn, TSample *DataOut, double* FR, int N=512);

	__property int Samples = {read=samples, write=writeSamples};
	__property int SamplesPower = {read=samplespower};

	//__property Complex Out[int i] = {read=readIn, write=writeIn};
	//__property Complex Out[int i] = {read=readIn, write=writeIn};
};

#endif
