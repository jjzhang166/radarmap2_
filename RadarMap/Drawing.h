//---------------------------------------------------------------------------

#ifndef DrawingH
#define DrawingH
//---------------------------------------------------------------------------
#include "GR32_Image.hpp"
//---------------------------------------------------------------------------

namespace Drawing
{

struct TFloatPoint
{
public:
	float x;
	float y;
};

Gr32::TFloatPoint __fastcall FloatPointCast(Drawing::TFloatPoint p);
Drawing::TFloatPoint __fastcall FloatPointCast(Gr32::TFloatPoint p);

struct TFloatRect
{
	TFloatRect() {}
		TFloatRect(float l, float t, float r, float b) { Left=l; Top=t; Right=r; Bottom=b; }
	union
	{
		struct
		{
			Drawing::TFloatPoint TopLeft;
			Drawing::TFloatPoint BottomRight;
		};
		struct
		{
			float Left;
			float Top;
			float Right;
			float Bottom;
		};
	};
	float Width () const { return Right  - Left; }
	float Height() const { return Bottom - Top ; }
};

Gr32::TFloatRect __fastcall FloatRectCast(Drawing::TFloatRect p);
Drawing::TFloatRect __fastcall FloatRectCast(Gr32::TFloatRect p);

struct TMyColor
{
	union
	{
		struct
		{
			System::Byte Items[4];

		};
		struct
		{
			TColor32 ARGB;
		};
		struct
		{
			System::Byte B;
			System::Byte G;
			System::Byte R;
			System::Byte A;
		};
	};
	TMyColor() {};
	TMyColor(System::Byte r, System::Byte g, System::Byte b, System::Byte a) {B=b; G=g; R=r; A=a;}
	TMyColor(TColor c) {B=c&0x00ff0000; G=c&0x0000ff00; R=c&0x000000ff; A=0xff;}
	TMyColor(__int32 c) {B=c&0x00ff0000; G=c&0x0000ff00; R=c&0x000000ff; A=c&0xff000000;}
	TMyColor(TColor32 c32) {ARGB=c32;}
};

typedef StaticArray<TMyColor, 1> TMyColorArray;
typedef TMyColorArray *PMyColorArray;

typedef TDrawMode TMyDrawMode;
typedef TCombineMode TMyCombineMode;
typedef TWrapMode TMyWrapMode;

const TMyColor MyBlack=TMyColor(clBlack);
const TMyColor MyWhite=TMyColor(clWhite);
const TMyColor MyRed=TMyColor(clRed);
const TMyColor MyGreen=TMyColor(clGreen);
const TMyColor MyBlue=TMyColor(clBlue);

class TMyBitmap : public TObject
{
private:
	TBitmap32* _bitmap;
	bool SelfCreated;
protected:
	int __fastcall readHeight() {return _bitmap->Height;}
	int __fastcall readWidth() {return _bitmap->Width;}
	TMyColor __fastcall readPenColor() {return TMyColor(_bitmap->PenColor);}
	TMyColor __fastcall readPixels(int X, int Y) {return TMyColor(_bitmap->Pixel[X][Y]);}
	PMyColorArray __fastcall readScanLine(int Y) {return (PMyColorArray)_bitmap->ScanLine[Y];}
	PMyColorArray __fastcall readBits() {return (PMyColorArray)_bitmap->Bits;}
	Types::TRect __fastcall readClipRect() {return _bitmap->ClipRect;}
	TMyDrawMode	__fastcall readDrawMode() {return _bitmap->DrawMode;}
	TMyCombineMode __fastcall readCombineMode() {return _bitmap->CombineMode;}
	TMyWrapMode __fastcall readWrapMode() {return _bitmap->WrapMode;}
	unsigned __fastcall readMasterAlpha() {return _bitmap->MasterAlpha;}
	TMyColor __fastcall readOuterColor() {return TMyColor(_bitmap->OuterColor);}
	Graphics::TFont* __fastcall readFont() {return _bitmap->Font;}
	HBITMAP __fastcall readHandle() {return _bitmap->BitmapHandle;}
	tagBITMAPINFO __fastcall readBitmapInfo() {return _bitmap->BitmapInfo;}
	HDC __fastcall readHDC() {return _bitmap->Handle;}
	Classes::TNotifyEvent __fastcall readOnResize() {return _bitmap->OnResize;}
	Classes::TNotifyEvent __fastcall readOnChange() {return _bitmap->OnChange;}
	Classes::TNotifyEvent __fastcall readOnHandleChanged() {return _bitmap->OnChange;}
	void __fastcall writeHeight(int value) {_bitmap->Height=value;}
	void __fastcall writeWidth(int value) {_bitmap->Width=value;}
	void __fastcall writePenColor(TMyColor value) {_bitmap->PenColor=value.ARGB;}
	void __fastcall writePixels(int X, int Y, TMyColor value) {_bitmap->Pixel[X][Y]=value.ARGB;}
	void __fastcall writeClipRect(Types::TRect value) {_bitmap->ClipRect=value;}
	void __fastcall writeDrawMode(TMyDrawMode value) {_bitmap->DrawMode=value;}
	void __fastcall writeCombineMode(TMyCombineMode value) {_bitmap->CombineMode=value;}
	void __fastcall writeWrapMode(TMyWrapMode value) {_bitmap->WrapMode=value;}
	void __fastcall writeMasterAlpha(unsigned value) {_bitmap->MasterAlpha=value;}
	void __fastcall writeOuterColor(TMyColor value) {_bitmap->OuterColor=value.ARGB;}
	void __fastcall writeFont(Graphics::TFont* value) {_bitmap->Font=value;}
	void __fastcall writeOnResize(Classes::TNotifyEvent value) {_bitmap->OnResize=value;}
	void __fastcall writeOnChange(Classes::TNotifyEvent value) {_bitmap->OnChange=value;}
	void __fastcall writeOnHandleChanged(Classes::TNotifyEvent value) {_bitmap->OnChange=value;}
public:
	__fastcall TMyBitmap() {_bitmap=new TBitmap32(); SelfCreated=true;}
	__fastcall TMyBitmap(TBitmap32* ptr) {_bitmap=ptr; SelfCreated=false;}
	__fastcall ~TMyBitmap() {if(SelfCreated) delete _bitmap;}
	__property TBitmap32* Bitmap = {read=_bitmap};

	void __fastcall Delete() {_bitmap->Delete();}
	bool __fastcall Empty() {return _bitmap->Empty();}
	void __fastcall Resized() {_bitmap->Resized();}
	bool __fastcall SetSize(int NewWidth, int NewHeight) {return _bitmap->SetSize(NewWidth, NewHeight);}
	void __fastcall Assign(Classes::TPersistent* Source) {_bitmap->Assign(Source);}
	Types::TRect __fastcall BoundsRect() {return _bitmap->BoundsRect();}
	void __fastcall Clear() {_bitmap->Clear();}
	void __fastcall Clear(TMyColor FillColor) {_bitmap->Clear(FillColor.ARGB);}
	void __fastcall BeginMeasuring(const TAreaChangedEvent Callback) {_bitmap->BeginMeasuring(Callback);}
	void __fastcall EndMeasuring() {_bitmap->EndMeasuring();}
	void __fastcall Changed() {_bitmap->Changed();}
	void __fastcall BeginUpdate() {_bitmap->BeginUpdate();}
	void __fastcall EndUpdate() {_bitmap->EndUpdate();}
	void __fastcall LoadFromFile(const System::UnicodeString FileName) {_bitmap->LoadFromFile(FileName);}
	void __fastcall SaveToFile(const System::UnicodeString FileName, bool SaveTopDown = false) {_bitmap->SaveToFile(FileName, SaveTopDown);}

	void __fastcall Draw(int DstX, int DstY, TMyBitmap* Src) {_bitmap->Draw(DstX, DstY, (TCustomBitmap32*)Src->Bitmap);}
	void __fastcall Draw(int DstX, int DstY, const Types::TRect &SrcRect, TMyBitmap* Src) {_bitmap->Draw(DstX, DstY, SrcRect, (TCustomBitmap32*)Src->Bitmap);}
	void __fastcall Draw(const Types::TRect &DstRect, const Types::TRect &SrcRect, TMyBitmap* Src) {_bitmap->Draw(DstRect, SrcRect, (TCustomBitmap32*)Src->Bitmap);}
	void __fastcall DrawTo(TMyBitmap* Dst) {_bitmap->DrawTo((TCustomBitmap32*)Dst->Bitmap);}
	void __fastcall DrawTo(TMyBitmap* Dst, int DstX, int DstY, const Types::TRect &SrcRect) {_bitmap->DrawTo((TCustomBitmap32*)Dst->Bitmap, DstX, DstY, SrcRect);}
	void __fastcall DrawTo(TMyBitmap* Dst, int DstX, int DstY) {_bitmap->DrawTo((TCustomBitmap32*)Dst->Bitmap, DstX, DstY);}
	void __fastcall DrawTo(TMyBitmap* Dst, const Types::TRect &DstRect) {_bitmap->DrawTo((TCustomBitmap32*)Dst->Bitmap, DstRect);}
	void __fastcall DrawTo(TMyBitmap* Dst, const Types::TRect &DstRect, const Types::TRect &SrcRect) {_bitmap->DrawTo((TCustomBitmap32*)Dst->Bitmap, DstRect, SrcRect);}
	void __fastcall HorzLine(int X1, int Y, int X2, TMyColor Value) {_bitmap->HorzLine(X1, Y, X2, Value.ARGB);}
	void __fastcall VertLine(int X, int Y1, int Y2, TMyColor Value) {_bitmap->VertLine(X, Y1, Y2, Value.ARGB);}
	void __fastcall Line(int X1, int Y1, int X2, int Y2, TMyColor Value, bool L = false) {_bitmap->Line(X1, Y1, X2, Y2, Value.ARGB, L);}
	void __fastcall LineA(int X1, int Y1, int X2, int Y2, TMyColor Value, bool L = false) {_bitmap->LineA(X1, Y1, X2, Y2, Value.ARGB, L);}
	void __fastcall MoveTo(int X, int Y) {_bitmap->MoveTo(X, Y);}
	void __fastcall LineToS(int X, int Y) {_bitmap->LineToS(X, Y);}
	void __fastcall FillRect(int X1, int Y1, int X2, int Y2, TMyColor Value) {_bitmap->FillRect(X1, Y1, X2, Y2, Value.ARGB);}
	void __fastcall FrameRectS(int X1, int Y1, int X2, int Y2, TMyColor Value) {_bitmap->FrameRectS(X1, Y1, X2, Y2, Value.ARGB);}
	void __fastcall FrameRectS(const Types::TRect &ARect, TMyColor Value) {_bitmap->FrameRectS(ARect, Value.ARGB);}
	void __fastcall Rotate90(TMyBitmap* Dst = (TMyBitmap*)(0x0)) {_bitmap->Rotate90((TCustomBitmap32*)Dst->Bitmap);}
	void __fastcall Rotate180(TMyBitmap* Dst = (TMyBitmap*)(0x0)) {_bitmap->Rotate180((TCustomBitmap32*)Dst->Bitmap);}
	void __fastcall Rotate270(TMyBitmap* Dst = (TMyBitmap*)(0x0)) {_bitmap->Rotate270((TCustomBitmap32*)Dst->Bitmap);}
	HIDESBASE void __fastcall Draw(const Types::TRect &DstRect, const Types::TRect &SrcRect, HDC hSrc) {_bitmap->Draw(DstRect, SrcRect, hSrc);}
	HIDESBASE void __fastcall DrawTo(HDC hDst, int DstX, int DstY) {_bitmap->DrawTo(hDst, DstX, DstY);}
	HIDESBASE void __fastcall DrawTo(HDC hDst, const Types::TRect &DstRect, const Types::TRect &SrcRect) {_bitmap->DrawTo(hDst, DstRect, SrcRect);}
	void __fastcall Textout(int X, int Y, const System::UnicodeString Text) {_bitmap->Textout(X, Y, Text);}
	void __fastcall Textout(int X, int Y, const Types::TRect &ClipRect, const System::UnicodeString Text) {_bitmap->Textout(X, Y, ClipRect, Text);}
	tagSIZE __fastcall TextExtent(const System::UnicodeString Text) {return _bitmap->TextExtent(Text);}
	int __fastcall TextHeight(const System::UnicodeString Text) {return _bitmap->TextHeight(Text);}
	int __fastcall TextWidth(const System::UnicodeString Text) {return _bitmap->TextWidth(Text);}
	void __fastcall RenderText(int X, int Y, const System::UnicodeString Text, int AALevel, TMyColor Color) {_bitmap->RenderText(X, Y, Text, AALevel, Color.ARGB);}

	__property int Height = {read=readHeight, write=writeHeight};
	__property int Width = {read=readWidth, write=writeWidth};
	__property TMyColor PenColor = {read=readPenColor, write=writePenColor};
	__property TMyColor Pixels[int X][int Y] = {read=readPixels, write=writePixels};
	__property PMyColorArray ScanLine[int Y] = {read=readScanLine};
	__property PMyColorArray Bits = {read=readBits};
	__property Types::TRect ClipRect = {read=readClipRect, write=writeClipRect};
	__property TMyDrawMode DrawMode = {read=readDrawMode, write=writeDrawMode, default=0};
	__property TMyCombineMode CombineMode = {read=readCombineMode, write=writeCombineMode, default=0};
	__property TMyWrapMode WrapMode = {read=readWrapMode, write=writeWrapMode, default=0};
	__property unsigned MasterAlpha = {read=readMasterAlpha, write=writeMasterAlpha, default=255};
	__property TMyColor OuterColor = {read=readOuterColor, write=writeOuterColor, default=0};
	__property Graphics::TFont* Font = {read=readFont, write=writeFont};
	__property HBITMAP BitmapHandle = {read=readHandle};
	__property tagBITMAPINFO BitmapInfo = {read=readBitmapInfo};
	__property HDC Handle = {read=readHDC};

	__property Classes::TNotifyEvent OnChange = {read=readOnChange, write=writeOnChange};
	__property Classes::TNotifyEvent OnResize = {read=readOnResize, write=writeOnResize};
	__property Classes::TNotifyEvent OnHandleChanged = {read=readOnHandleChanged, write=writeOnHandleChanged};
};

class TMyLayer;

class TMyLayerCollection : public TObject
{
private:
	Gr32_layers::TLayerCollection* _layers;
	TPersistent *AOwner;
	bool SelfCreated;
	TMyLayer *mll, *il;
protected:
	int __fastcall readCount() {return _layers->Count;}
	TMyLayer* __fastcall readMouseListener();
	TMyLayer* __fastcall readItem(int Index);

	void __fastcall writeItem(int Index, TMyLayer *value);
	void __fastcall writeMouseListener(TMyLayer *value);
public:
	__fastcall TMyLayerCollection(TComponent *ptr) {mll=NULL; AOwner=ptr; _layers=new TLayerCollection(ptr); SelfCreated=true;}
	__fastcall TMyLayerCollection(Gr32_layers::TLayerCollection* ptr) {mll=NULL; AOwner=ptr->Owner; _layers=ptr; SelfCreated=false;}
	__fastcall ~TMyLayerCollection() {if(mll!=NULL) delete mll; if(il!=NULL) delete il; if(SelfCreated) delete _layers;}
	__property Gr32_layers::TLayerCollection* Layers = {read=_layers};

	void __fastcall Clear(void) {_layers->Clear();}
	void __fastcall Delete(int Index) {_layers->Delete(Index);}
	Drawing::TFloatPoint __fastcall LocalToViewport(TFloatPoint &APoint, bool AScaled) {return FloatPointCast(_layers->LocalToViewport(FloatPointCast(APoint), AScaled));}
	Drawing::TFloatPoint __fastcall ViewportToLocal(TFloatPoint &APoint, bool AScaled) {return FloatPointCast(_layers->ViewportToLocal(FloatPointCast(APoint), AScaled));}
	void __fastcall GetViewportScale(float &ScaleX, float &ScaleY) {_layers->GetViewportScale(ScaleX, ScaleY);}
	void __fastcall GetViewportShift(float &ShiftX, float &ShiftY) {_layers->GetViewportShift(ShiftX, ShiftY);}

	__property int Count = {read=readCount};
	__property TMyLayer* Items[int Index] = {read=readItem, write=writeItem};
	__property TMyLayer* MouseListener = {read=readMouseListener, write=writeMouseListener};
};

typedef void __fastcall (__closure *TMyPaintLayerEvent)(System::TObject* Sender, TMyBitmap* Buffer);

class TMyLayer : public TObject
{
private:
	Gr32_layers::TBitmapLayer* _layer;
	bool SelfCreated;
	TMyLayerCollection* _layers;
	TMyBitmap *_bitmap;
	TMyPaintLayerEvent myOnPaint;
	void __fastcall tempOnPaint(System::TObject* Sender, Gr32::TBitmap32* Buffer);
protected:
	Controls::TCursor readCursor() {return _layer->Cursor;}
	int readIndex() {return _layer->Index;}
	TMyLayerCollection* readLayerCollection() {return _layers;}
	bool readMouseEvents() {return _layer->MouseEvents;}
	int readTag() {return _layer->Tag;}
	bool readVisible() {return _layer->Visible;}
	TMyBitmap* readBitmap() {return _bitmap;}
	Drawing::TFloatRect readLocation() {return FloatRectCast(_layer->Location);}
	bool readScaled() {return _layer->Scaled;}
	TMyPaintLayerEvent __fastcall readOnPaint() {return myOnPaint;}
	Controls::TMouseEvent __fastcall readOnMouseDown() {return _layer->OnMouseDown;}
	Controls::TMouseMoveEvent __fastcall readOnMouseMove() {return _layer->OnMouseMove;}
	Controls::TMouseEvent __fastcall readOnMouseUp() {return _layer->OnMouseUp;}

	void __fastcall writeCursor(Controls::TCursor value) {_layer->Cursor=value;}
	void __fastcall writeIndex(int value) {_layer->Index=value;}
	void __fastcall writeLayerCollection(TMyLayerCollection* value) {_layers=value; _layer->LayerCollection=_layers->Layers;}
	void __fastcall writeMouseEvents(bool value) {_layer->MouseEvents=value;}
	void __fastcall writeTag(int value) {_layer->Tag=value;}
	void __fastcall writeVisible(bool value) {_layer->Visible=value;}
	void __fastcall writeBitmap(TMyBitmap* value) {_bitmap=value; _layer->Bitmap=_bitmap->Bitmap;}
	void __fastcall writeLocation(Drawing::TFloatRect value) {_layer->Location=FloatRectCast(value);}
	void __fastcall writeScaled(bool value) {_layer->Scaled=value;}
	void __fastcall writeOnPaint(TMyPaintLayerEvent value) {myOnPaint=value;}
	void __fastcall writeOnMouseDown(Controls::TMouseEvent value) {_layer->OnMouseDown=value;}
	void __fastcall writeOnMouseMove(Controls::TMouseMoveEvent value) {_layer->OnMouseMove=value;}
	void __fastcall writeOnMouseUp(Controls::TMouseEvent value) {_layer->OnMouseUp=value;}
public:
	__fastcall TMyLayer(TMyLayerCollection *ptr);
	__fastcall TMyLayer(Gr32_layers::TCustomLayer* ptr);
	__fastcall ~TMyLayer();
	__property Gr32_layers::TBitmapLayer* Layer = {read=_layer};

	void __fastcall BringToFront(void) {_layer->BringToFront();}
	void __fastcall Changed(void) {_layer->Changed();}
	void __fastcall Changed(Types::TRect &Rect) {_layer->Changed(Rect);}
	void __fastcall Update(void) {_layer->Update();}
	void __fastcall Update(Types::TRect &Rect) {_layer->Update(Rect);}
	bool __fastcall HitTest(int X, int Y) {return _layer->HitTest(X, Y);}
	void __fastcall SendToBack(void) {_layer->SendToBack();}
	void __fastcall SetAsMouseListener(void) {_layer->SetAsMouseListener();}
	Drawing::TFloatRect __fastcall GetAdjustedRect(Drawing::TFloatRect &R) {return FloatRectCast(_layer->GetAdjustedRect((FloatRectCast(R))));}
	Drawing::TFloatRect __fastcall GetAdjustedLocation(void) {return FloatRectCast(_layer->GetAdjustedLocation());}

	__property Controls::TCursor Cursor = {read=readCursor, write=writeCursor};
	__property int Index = {read=readIndex, write=writeIndex};
	__property TMyLayerCollection* LayerCollection = {read=readLayerCollection, write=writeLayerCollection};
	__property bool MouseEvents = {read=readMouseEvents, write=writeMouseEvents};
	__property int Tag = {read=readTag, write=writeTag};
	__property bool Visible = {read=readVisible, write=writeVisible};
	__property TMyBitmap* Bitmap = {read=readBitmap, write=writeBitmap};
	__property Drawing::TFloatRect Location = {read=readLocation, write=writeLocation};
	__property bool Scaled = {read=readScaled, write=writeScaled};

	__property TMyPaintLayerEvent OnPaint = {read=readOnPaint, write=writeOnPaint};
	__property Controls::TMouseEvent OnMouseDown = {read=readOnMouseDown, write=writeOnMouseDown};
	__property Controls::TMouseMoveEvent OnMouseMove = {read=readOnMouseMove, write=writeOnMouseMove};
	__property Controls::TMouseEvent OnMouseUp = {read=readOnMouseUp, write=writeOnMouseUp};
};

typedef void __fastcall (__closure *TMyMouseEvent)(System::TObject* Sender, Controls::TMouseButton Button, Classes::TShiftState Shift, int X, int Y, TMyLayer* Layer);

typedef void __fastcall (__closure *TMyMouseMoveEvent)(System::TObject* Sender, Classes::TShiftState Shift, int X, int Y, TMyLayer* Layer);

enum TMyBitmapAlign { baTopLeft, baCenter, baTile, baCustom };

enum TMyScrollBarVisibility { svAlways, svHidden, svAuto };

enum TMyAlign { alNone, alTop, alBottom, alLeft, alRight, alClient, alCustom };

class TMyImage : public TObject
{
private:
	TImgView32* _image;
	TMyBitmap *_bitmap;
	TMyLayerCollection *_layers;
	TMyMouseEvent myMouseDown, myMouseUp;
	TMyMouseMoveEvent myMouseMove;

	void __fastcall tempMouseDown(System::TObject* Sender, Controls::TMouseButton Button, Classes::TShiftState Shift, int X, int Y, Gr32_layers::TCustomLayer* Layer);
	void __fastcall tempMouseMove(System::TObject* Sender, Classes::TShiftState Shift, int X, int Y, Gr32_layers::TCustomLayer* Layer);
	void __fastcall tempMouseUp(System::TObject* Sender, Controls::TMouseButton Button, Classes::TShiftState Shift, int X, int Y, Gr32_layers::TCustomLayer* Layer);
protected:
	bool readVisible() {return _image->Visible;}
	TCursor readCursor() {return _image->Cursor;}

	TMyBitmap* readBitmap() {return _bitmap;}

	TMyBitmapAlign readBitmapAlign() {return (TMyBitmapAlign)_image->BitmapAlign;}
	float readOffsetHorz() {return _image->OffsetHorz;}
	float readOffsetVert() {return _image->OffsetVert;}
	float readScale() {return _image->Scale;}
	float readScaleX() {return _image->ScaleX;}
	float readScaleY() {return _image->ScaleY;}
	int __fastcall readHeight() {return _image->Height;}
	int __fastcall readWidth() {return _image->Width;}
	TMyAlign readAlign() {return (TMyAlign)_image->Align;}
	TComponent* __fastcall readOwner() {return _image->Owner;}
	Types::TPoint __fastcall readClientOrigin() {return _image->ClientOrigin;}
	HWND __fastcall readHandle() {return _image->Handle;}
	TMyLayerCollection* readLayers() {return _layers;}
	Classes::TNotifyEvent __fastcall readOnResize() {return _image->OnResize;}
	Classes::TNotifyEvent __fastcall readOnChange() {return _image->OnChange;}
	Classes::TNotifyEvent __fastcall readOnBitmapResize() {return _image->OnBitmapResize;}
	Controls::TCanResizeEvent __fastcall readOnCanResize() {return _image->OnCanResize;}
	Classes::TNotifyEvent __fastcall readOnClick() {return _image->OnClick;}
	Classes::TNotifyEvent __fastcall readOnDblClick() {return _image->OnDblClick;}
	Controls::TKeyEvent __fastcall readOnKeyDown() {return _image->OnKeyDown;}
	Controls::TKeyPressEvent __fastcall readOnKeyPress() {return _image->OnKeyPress;}
	Controls::TKeyEvent __fastcall readOnKeyUp() {return _image->OnKeyUp;}
	TMyMouseEvent __fastcall readOnMouseDown() {return myMouseDown;}
	Classes::TNotifyEvent __fastcall readOnMouseEnter() {return _image->OnMouseEnter;}
	Classes::TNotifyEvent __fastcall readOnMouseLeave() {return _image->OnMouseLeave;}
	TMyMouseMoveEvent __fastcall readOnMouseMove() {return myMouseMove;}
	TMyMouseEvent __fastcall readOnMouseUp() {return myMouseUp;}
	TMyScrollBarVisibility __fastcall readScrollBarsVisiability() {return (TMyScrollBarVisibility)_image->ScrollBars->Visibility;}
	Controls::TMouseWheelEvent __fastcall readOnMouseWheel() {return _image->OnMouseWheel;}
	Controls::TMouseWheelUpDownEvent __fastcall readOnMouseWheelDown() {return _image->OnMouseWheelDown;}
	Controls::TMouseWheelUpDownEvent __fastcall readOnMouseWheelUp() {return _image->OnMouseWheelUp;}
	Classes::TNotifyEvent __fastcall readOnScroll() {return _image->OnScroll;}

	void __fastcall writeVisible(bool value) {_image->Visible=value;}
	void __fastcall writeCursor(TCursor value) {_image->Cursor=value;}
	void __fastcall writeBitmap(TMyBitmap* value) {delete _bitmap; _bitmap=value; _image->Bitmap=value->Bitmap;}
	void __fastcall writeLayers(TMyLayerCollection* value) {delete _layers; _layers=value; _image->Layers=_layers->Layers;}
	void __fastcall writeBitmapAlign(TMyBitmapAlign value) {_image->BitmapAlign=(TBitmapAlign)value;}
	void __fastcall writeOffsetHorz(float value) {_image->OffsetHorz=value;}
	void __fastcall writeOffsetVert(float value) {_image->OffsetVert=value;}
	void __fastcall writeScale(float value) {_image->Scale=value;}
	void __fastcall writeScaleX(float value) {_image->ScaleX=value;}
	void __fastcall writeScaleY(float value) {_image->ScaleY=value;}
	void __fastcall writeHeight(int value) {_image->Height=value;}
	void __fastcall writeWidth(int value) {_image->Width=value;}
	void __fastcall writeAlign(TMyAlign value) {_image->Align=(TAlign)value;}
	void __fastcall writeScrollBarsVisiability(TMyScrollBarVisibility value) {_image->ScrollBars->Visibility=(TScrollBarVisibility)value;}
	void __fastcall writeOnResize(Classes::TNotifyEvent value) {_image->OnResize=value;}
	void __fastcall writeOnChange(Classes::TNotifyEvent value) {_image->OnChange=value;}
	void __fastcall writeOnBitmapResize(Classes::TNotifyEvent value) {_image->OnBitmapResize=value;}
	void __fastcall writeOnCanResize(Controls::TCanResizeEvent value) {_image->OnCanResize=value;}
	void __fastcall writeOnClick(Classes::TNotifyEvent value) {_image->OnClick=value;}
	void __fastcall writeOnDblClick(Classes::TNotifyEvent value) {_image->OnDblClick=value;}
	void __fastcall writeOnKeyDown(Controls::TKeyEvent value) {_image->OnKeyDown=value;}
	void __fastcall writeOnKeyPress(Controls::TKeyPressEvent value) {_image->OnKeyPress=value;}
	void __fastcall writeOnKeyUp(Controls::TKeyEvent value) {_image->OnKeyUp=value;}
	void __fastcall writeOnMouseDown(TMyMouseEvent value) {myMouseDown=value;}
	void __fastcall writeOnMouseEnter(Classes::TNotifyEvent value) {_image->OnMouseEnter=value;}
	void __fastcall writeOnMouseLeave(Classes::TNotifyEvent value) {_image->OnMouseLeave=value;}
	void __fastcall writeOnMouseMove(TMyMouseMoveEvent value) {myMouseMove=value;}
	void __fastcall writeOnMouseUp(TMyMouseEvent value) {myMouseUp=value;}
	void __fastcall writeOnMouseWheel(Controls::TMouseWheelEvent value) {_image->OnMouseWheel=value;}
	void __fastcall writeOnMouseWheelDown(Controls::TMouseWheelUpDownEvent value) {_image->OnMouseWheelDown=value;}
	void __fastcall writeOnMouseWheelUp(Controls::TMouseWheelUpDownEvent value) {_image->OnMouseWheelUp=value;}
	void __fastcall writeOnScroll(Classes::TNotifyEvent value) {_image->OnScroll=value;}
public:
	__fastcall TMyImage(Classes::TComponent* aowner);
	__fastcall TMyImage(TImgView32* Image);
	__fastcall ~TMyImage();
	__property TImgView32* Image = {read=_image};
	__property TMyBitmap* Bitmap = {read=readBitmap, write=writeBitmap};

	Types::TRect __fastcall GetViewportRect(void) {return _image->GetViewportRect();}
	void __fastcall ScrollToCenter(int X, int Y) {_image->ScrollToCenter(X,Y);}
	void __fastcall Scroll(int Dx, int Dy) {_image->Scroll(Dx, Dy);}
	void __fastcall BeginUpdate(void) {_image->BeginUpdate();}
	void __fastcall Changed(void) {_image->Changed();}
	void __fastcall Update(const Types::TRect &Rect) {_image->Update(Rect);}
	void __fastcall EndUpdate(void) {_image->EndUpdate();}
	Types::TRect __fastcall GetBitmapRect(void) {return _image->GetBitmapRect();};
	tagSIZE __fastcall GetBitmapSize(void) {return _image->GetBitmapSize();}
	void __fastcall Invalidate(void) {_image->Invalidate();}
	void __fastcall Loaded(void) {_image->Loaded();}
	void __fastcall PaintTo(TMyBitmap* Dest, const Types::TRect &DestRect) {_image->PaintTo(Dest->Bitmap, DestRect);}
	void __fastcall Resize(void) {_image->Resize();}
	void __fastcall SetupBitmap(bool DoClear, TMyColor ClearColor) {_image->SetupBitmap(DoClear, ClearColor.ARGB);}

	__property bool Visible = {read=readVisible, write=writeVisible};
	__property TMyScrollBarVisibility ScrollBarsVisiability = {read=readScrollBarsVisiability, write=writeScrollBarsVisiability};
	__property TCursor Cursor = {read=readCursor, write=writeCursor};
	__property TBitmapAlign BitmapAlign = {read=readBitmapAlign, write=writeBitmapAlign};
	__property float OffsetHorz = {read=readOffsetHorz, write=writeOffsetHorz};
	__property float OffsetVert = {read=readOffsetVert, write=writeOffsetVert};
	__property float Scale = {read=readScale, write=writeScale};
	__property float ScaleX = {read=readScaleX, write=writeScaleX};
	__property float ScaleY = {read=readScaleY, write=writeScaleY};
	__property TMyAlign Align = {read=readAlign, write=writeAlign};
	__property int Height = {read=readHeight, write=writeHeight};
	__property int Width = {read=readWidth, write=writeWidth};
	__property TComponent *Owner = {read=readOwner};
	__property Types::TPoint ClientOrigin = {read=readClientOrigin};
	__property HWND Handle = {read = readHandle};

	__property TMyLayerCollection* Layers = {read=readLayers, write=writeLayers};

	__property Classes::TNotifyEvent OnBitmapResize = {read=readOnBitmapResize, write=writeOnBitmapResize};
	__property Controls::TCanResizeEvent OnCanResize = {read=readOnCanResize, write=writeOnCanResize};
	__property Classes::TNotifyEvent OnClick = {read=readOnClick, write=writeOnClick};
	__property Classes::TNotifyEvent OnChange = {read=readOnChange, write=writeOnChange};
	__property Classes::TNotifyEvent OnDblClick = {read=readOnDblClick, write=writeOnDblClick};
	__property Controls::TKeyEvent OnKeyDown = {read=readOnKeyDown, write=writeOnKeyDown};
	__property Controls::TKeyPressEvent OnKeyPress = {read=readOnKeyPress, write=writeOnKeyPress};
	__property Controls::TKeyEvent OnKeyUp = {read=readOnKeyUp, write=writeOnKeyUp};
	__property TMyMouseEvent OnMouseDown = {read=readOnMouseDown, write=writeOnMouseDown};
	__property Classes::TNotifyEvent OnMouseEnter = {read=readOnMouseEnter, write=writeOnMouseEnter};
	__property Classes::TNotifyEvent OnMouseLeave = {read=readOnMouseLeave, write=writeOnMouseLeave};
	__property TMyMouseMoveEvent OnMouseMove = {read=readOnMouseMove, write=writeOnMouseMove};
	__property TMyMouseEvent OnMouseUp = {read=readOnMouseUp, write=writeOnMouseUp};
	__property Controls::TMouseWheelEvent OnMouseWheel = {read=readOnMouseWheel, write=writeOnMouseWheel};
	__property Controls::TMouseWheelUpDownEvent OnMouseWheelDown = {read=readOnMouseWheelDown, write=writeOnMouseWheelDown};
	__property Controls::TMouseWheelUpDownEvent OnMouseWheelUp = {read=readOnMouseWheelUp, write=writeOnMouseWheelUp};
	__property Classes::TNotifyEvent OnResize = {read=readOnResize, write=writeOnResize};
	__property Classes::TNotifyEvent OnScroll = {read=readOnScroll, write=writeOnScroll};
};

}

#endif
