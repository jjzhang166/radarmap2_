// -----------------------------------------------------------------------------
// DbDxfStructures.h
// -----------------------------------------------------------------------------

#ifndef DbDxfStructuresH
#define DbDxfStructuresH

#include "Defines.h"
#include "GPSCoordinate.h"
#include <time.h>

#define DB_SCHEME_VERSION 1

// -----------------------------------------------------------------------------
// CoordinateChangeData
// -----------------------------------------------------------------------------
struct CoordinateChangeData {

public:
	TDouble3DPoint XYZ;
	float Scale_X, Scale_Y, Scale_Z;
	float Angle; // degrees
	int DbBlockRecordId;

	CoordinateChangeData() {
		XYZ = TDouble3DPoint();
		Scale_X = Scale_Y = Scale_Z = 1;
		Angle = 0;
		DbBlockRecordId = 0;
	}

	CoordinateChangeData(const CoordinateChangeData &pRight) {
		XYZ = pRight.XYZ;
		Scale_X = pRight.Scale_X;
		Scale_Y = pRight.Scale_Y;
		Scale_Z = pRight.Scale_Z;
		Angle = pRight.Angle;
		DbBlockRecordId = pRight.DbBlockRecordId;
	}
};

// -----------------------------------------------------------------------------
// EntityWriterData
// -----------------------------------------------------------------------------
class TDXFEntity;
class CoordinateChangeData;

class EntityWriterData {

private:
	TDXFEntity *entity;
	CoordinateChangeData *changeData;

	double min_X;
	double max_X;
	double min_Y;
	double max_Y;

public:
	__fastcall EntityWriterData(TDXFEntity *pEntity = NULL, CoordinateChangeData *pChangeData = NULL) {
		this->entity = pEntity;
		this->changeData = pChangeData;
		this->min_X = 0.0;
		this->max_X = 0.0;
		this->min_Y = 0.0;
		this->max_Y = 0.0;
	}
	__fastcall ~EntityWriterData() {};

	__property TDXFEntity *Entity = {
		read = entity, write = entity
	};
	__property CoordinateChangeData *ChangeData = {
		read = changeData, write = changeData
	};

	__property double Min_X = {
		read = min_X, write = min_X
	};
	__property double Max_X = {
		read = max_X, write = max_X
	};

	__property double Min_Y = {
		read = min_Y, write = min_Y
	};
	__property double Max_Y = {
		read = max_Y, write = max_Y
	};
};
//------------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// MapParametersRecord
// -----------------------------------------------------------------------------
struct MapParametersRecord {

public:
	int mIdDbRecord;
	int mDbShemeVersion;
	int mCS;
	TDoublePoint mLowerCorner, mUpperCorner;
	TDouble3DPoint mMinCorner, mMaxCorner;
	UnicodeString mChangeDate;

	MapParametersRecord() {
		mIdDbRecord = -1;
		mDbShemeVersion = DB_SCHEME_VERSION;
		mCS = csMetric;
		mLowerCorner = TDoublePoint();
		mUpperCorner = TDoublePoint();
		mMinCorner = TDouble3DPoint();
		mMaxCorner = TDouble3DPoint();
	}
};
//------------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// FileRecord
// -----------------------------------------------------------------------------
struct FileRecord {

public:
	int mIdDbRecord;
	UnicodeString mFileName;
	UnicodeString mFileDir;
	int mFileCreateDate;
	int mFileSize;
	UnicodeString mAutocadVersion;
	TDoublePoint mLowerCorner, mUpperCorner;
	TDouble3DPoint mMinCorner, mMaxCorner;
	bool mIsFillMinCorner, mIsFillMaxCorner;
	UnicodeString mLoadDate;

	FileRecord() {
		mIdDbRecord = -1;
		mFileCreateDate = 0;
		mFileSize = -1;
		mLowerCorner = TDoublePoint();
		mUpperCorner = TDoublePoint();
		mMinCorner = TDouble3DPoint();
		mMaxCorner = TDouble3DPoint();
		mIsFillMinCorner = mIsFillMaxCorner = false;
	}

	FileRecord(const FileRecord &pRight) {
		mIdDbRecord = pRight.mIdDbRecord;
		mFileName = pRight.mFileName;
		mFileDir = pRight.mFileDir;
		mFileCreateDate = pRight.mFileCreateDate;
		mFileSize = pRight.mFileSize;
		mAutocadVersion = pRight.mAutocadVersion;
		mLowerCorner = pRight.mLowerCorner;
		mUpperCorner = pRight.mUpperCorner;
		mIsFillMinCorner = pRight.mIsFillMinCorner;
		mMinCorner = pRight.mMinCorner;
		mIsFillMaxCorner = pRight.mIsFillMaxCorner;
		mMaxCorner = pRight.mMaxCorner;
		mLoadDate = pRight.mLoadDate;
	}

	static UnicodeString __fastcall TimestampToStrDateTime(time_t pTimestamp) {
		UnicodeString str_date_time = "";
		struct tm tblock;

		localtime_s(&pTimestamp, &tblock);
		str_date_time.sprintf(L"%02d.%02d.%02d %02d:%02d:%02d", tblock.tm_mday, tblock.tm_mon, 1900 + tblock.tm_year, tblock.tm_hour,
			tblock.tm_min, tblock.tm_sec);

		return str_date_time;
	}
};
//------------------------------------------------------------------------------



#endif
