// -----------------------------------------------------------------------------
// DbDxfThreads.h
// -----------------------------------------------------------------------------

#ifndef DbDxfThreadsH
#define DbDxfThreadsH

#include "Defines.h"
#include "MyThreads.h"
#include "GPSCoordinate.h"

class TToolProgressBar;

// -----------------------------------------------------------------------------
// TDbDxfBaseThread
// -----------------------------------------------------------------------------
class TDbDxfBaseThread : public TMyThread {

private:
protected:
	TObject* mManager;
	TCoordinateSystem mCoordinateSystem;
	AnsiString mFileName;
	AnsiString mDbName;
	TToolProgressBar *mProgressBar;
	TVoidFunction mSuccessCallBack;
	TVoidFunction mCancelCallBack;

//	virtual void __fastcall Execute() = 0;

public:

	__fastcall TDbDxfBaseThread(TObject* pManager, AnsiString pFileName, AnsiString pDbName, TCoordinateSystem pCS,
		TToolProgressBar *pProgressBar, TVoidFunction SuccessCallBack, TVoidFunction CancelCallBack) : TMyThread(true) {

		FreeOnTerminate = true;
		mManager = pManager;
		mFileName = pFileName;
		mDbName = pDbName;
		mCoordinateSystem = pCS;
		mSuccessCallBack = SuccessCallBack;
		mCancelCallBack = CancelCallBack;

		mProgressBar = pProgressBar;
//		if(mProgressBar) mProgressBar->Show(10, true);

		ForceResume();
	}

	__fastcall ~TDbDxfBaseThread() {
//		if(mProgressBar) {
//			mProgressBar->Hide(true);
//		}
	}

	bool __fastcall CheckTerminated();
	__property TCoordinateSystem CoordinateSystem = {read = mCoordinateSystem};
};
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// TDXFtoDbLoadThread
// -----------------------------------------------------------------------------
class TDXFtoDbLoadThread : public TDbDxfBaseThread {

private:
protected:
	void __fastcall Execute();

public:

	__fastcall TDXFtoDbLoadThread(TObject* pManager, AnsiString pFileName, AnsiString pDbName, TCoordinateSystem pCS,
		TToolProgressBar *pProgressBar, TVoidFunction SuccessCallBack, TVoidFunction CancelCallBack)
		: TDbDxfBaseThread(pManager, pFileName, pDbName, pCS, pProgressBar, SuccessCallBack, CancelCallBack) {
	}

	__fastcall ~TDXFtoDbLoadThread() {
	}
};
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// TDXFfromDbDeleteThread
// -----------------------------------------------------------------------------
class TDXFfromDbDeleteThread : public TDbDxfBaseThread {

private:
protected:
	void __fastcall Execute();

public:

	__fastcall TDXFfromDbDeleteThread(TObject* pManager, AnsiString pFileName, AnsiString pDbName, TCoordinateSystem pCS,
	  	TToolProgressBar *pProgressBar,	TVoidFunction SuccessCallBack, TVoidFunction CancelCallBack)
		: TDbDxfBaseThread(pManager, pFileName, pDbName, pCS, pProgressBar, SuccessCallBack, CancelCallBack) {
	}

	__fastcall ~TDXFfromDbDeleteThread() {
	}
};
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// TDXFinDbRefreshThread
// -----------------------------------------------------------------------------
class TDXFinDbRefreshThread : public TDbDxfBaseThread {

private:
	AnsiString mDbDxfFileName;

protected:
	void __fastcall Execute();

public:

	__fastcall TDXFinDbRefreshThread(TObject* pManager, AnsiString pDbDxfFileName, AnsiString pHddFileName, AnsiString pDbName,
		TCoordinateSystem pCS, TToolProgressBar *pProgressBar, TVoidFunction SuccessCallBack, TVoidFunction CancelCallBack)
		: TDbDxfBaseThread(pManager, pHddFileName, pDbName, pCS, pProgressBar, SuccessCallBack, CancelCallBack) {

		mDbDxfFileName = pDbDxfFileName;
	}

	__fastcall ~TDXFinDbRefreshThread() {
	}
};
// -----------------------------------------------------------------------------
#endif
