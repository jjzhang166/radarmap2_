// -----------------------------------------------------------------------------
// DbDxfThreads.cpp
// -----------------------------------------------------------------------------

#pragma hdrstop

#include "DbDxfThreads.h"
#include "VisualToolObjects.h"
#include "Manager.h"
#include "DXFtoDbLoader.h"

#pragma package(smart_init)

// -----------------------------------------------------------------------------
// TDbDxfBaseThread
// -----------------------------------------------------------------------------

// -------- CheckTerminated ----------------------------------------------------
bool __fastcall TDbDxfBaseThread::CheckTerminated() {
	bool result = false;

//	if ((this->mInetMap->MapLoaderThreadId != 0) && (this->mInetMap->MapLoaderThreadId != this->ThreadID)) {
//		AskForTerminate();
//		result = true;
//	}


	if (Terminated || MyTerminated || (this->mProgressBar && this->mProgressBar->CancelPressed)) {
		result = true;
	}
	return result;
}
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// TDXFtoDbLoadThread
// -----------------------------------------------------------------------------

// -------- Execute ------------------------------------------------------------
void __fastcall TDXFtoDbLoadThread::Execute() {
	TRadarMapManager *mngr = dynamic_cast<TRadarMapManager *>(this->mManager);
	TDXFtoDbLoader *loader = NULL;
	TDXFCollection *dxf = NULL;
	bool err = true;

	SetStarted();

	if (this->mCoordinateSystem == csLatLon) {
		mngr->ShowMessageObject(NULL, mngr->MapMessages, TMessageType::mtStop, mvtDisapering, 10000,
			"Latitude-Longitude Coordinate system is not aplicable for MultiFile Maps", "Error");
	}
	else {

		try {
			if (this->mProgressBar) {
				this->mProgressBar->Show(10, true);
			}

			loader = new TDXFtoDbLoader(this, this->mManager, this->mFileName, this->mDbName, this->mProgressBar);

			if (loader->MakeDatabase()) {

				dxf = loader->ReadDXFFile();

				if (dxf) {
					loader->WriteToDB(dxf);

					if (this->CheckTerminated() != true) {
						loader->CreateGeoIndex();
						err = false;
					}
				}
			}
		}
		catch(...) {
		}
		if(this->mProgressBar) {
			this->mProgressBar->Hide(false);
		}
		if (dxf) {
			delete dxf;
		}
		if (loader) {
			delete loader;
		}
	}
	if(err && mCancelCallBack) Synchronize(mCancelCallBack);
		else if(!err && mSuccessCallBack) Synchronize(mSuccessCallBack);
	SetIsTerminated();
}
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// TDXFfromDbDeleteThread
// -----------------------------------------------------------------------------

// -------- Execute ------------------------------------------------------------
void __fastcall TDXFfromDbDeleteThread::Execute() {
	//TRadarMapManager *mngr = dynamic_cast<TRadarMapManager *>(this->mManager);
	//bool result = false;
	TDXFtoDbLoader *loader = NULL;
	bool err = true;

	SetStarted();

	try {
		if (this->mProgressBar) {
			this->mProgressBar->Show(10, true);
		}

		loader = new TDXFtoDbLoader(this, this->mManager, this->mFileName, this->mDbName, this->mProgressBar);

		loader->DeleteDxfFromDB();

		if (this->CheckTerminated() != true) {
			loader->CreateGeoIndex();
			err = false;
		}
	}
	catch(...) {
	}

	if(this->mProgressBar) {
		this->mProgressBar->Hide(false);
	}
	if (loader) {
		delete loader;
	}
	if(err && mCancelCallBack) Synchronize(mCancelCallBack);
	else if(!err && mSuccessCallBack) Synchronize(mSuccessCallBack);

	SetIsTerminated();
}
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// TDXFinDbRefreshThread
// -----------------------------------------------------------------------------

// -------- Execute ------------------------------------------------------------
void __fastcall TDXFinDbRefreshThread::Execute() {
	TRadarMapManager *mngr = dynamic_cast<TRadarMapManager *>(this->mManager);
	TDXFtoDbLoader *loader = NULL;
	TDXFCollection *dxf = NULL;
	bool err = true;

	SetStarted();

	if (this->mCoordinateSystem == csLatLon) {
		mngr->ShowMessageObject(NULL, mngr->MapMessages, TMessageType::mtStop, mvtDisapering, 10000,
			"Latitude-Longitude Coordinate system is not aplicable for MultiFile Maps", "Error");
	}
	else {

		try {
			if (this->mProgressBar) {
				this->mProgressBar->Show(10, true);
			}

			loader = new TDXFtoDbLoader(this, this->mManager, this->mFileName, this->mDbName, this->mProgressBar);

			dxf = loader->ReadDXFFile();

			if (dxf && (this->CheckTerminated() != true)) {
				loader->DeleteDxfFromDB(this->mDbDxfFileName);

				loader->WriteToDB(dxf);

				if (this->CheckTerminated() != true) {
					loader->CreateGeoIndex();
					err = false;
				}
			}
		}
		catch(...) {
		}

		if(this->mProgressBar) {
			this->mProgressBar->Hide(false);
		}
		if (dxf) {
			delete dxf;
		}
		if (loader) {
			delete loader;
		}
	}
	if(err && mCancelCallBack) {
		Synchronize(mCancelCallBack);
	}
	else if(!err && mSuccessCallBack) {
		Synchronize(mSuccessCallBack);
	}
	SetIsTerminated();
}
// -----------------------------------------------------------------------------
