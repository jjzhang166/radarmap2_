// -----------------------------------------------------------------------------
// DbDXFMapsContainer.cpp
// -----------------------------------------------------------------------------

#pragma hdrstop

#include "DbDXFMapsContainer.h"
#include "DbDXFEntityRender.h"
#include "Manager.h"
//#include "GPSCoordinate.h"
#include "DbDxfThreads.h"
#include "DXFMap.h"
#include "GR32_Text.hpp"
#include <stack>
#ifdef _RAD101
	#include <dinkumware\hash_map>
#else
	#include <hash_map.h>
#endif


// -----------------------------------------------------------------------------

#pragma package(smart_init)

//------------------------------------------------------------------------------
// TDbDXFMapsContainer
//------------------------------------------------------------------------------

// -------- Constructor / Destructor -------------------------------------------
__fastcall TDbDXFMapsContainer::TDbDXFMapsContainer(TMyObject *AOwner, void* ASettings, TLockedDoubleRect *AViewportArea) : TVectorMapsContainer(AOwner, ASettings, AViewportArea) {
	this->maptype = TMapType::mtSQLiteDXF;

	//this->DXF = new TDXFCollection();
	this->DXF->IdDbRecord = -1;
	this->dbReader = new TDXFFromDbReader();

	/*TRadarMapManager *mngr = static_cast<TRadarMapManager *>(this->Manager);

	if (!this->GpsListener && mngr && mngr->GpsListeners) {
		this->GpsListener = new TGpsListener();
		this->GpsListener->OnAddCoordinate = AddCoordinate;
		mngr->GpsListeners->Add(this->GpsListener);
	}
	else { this->GpsListener = NULL; }*/
	ConnectGpsListener();
}

__fastcall TDbDXFMapsContainer::~TDbDXFMapsContainer() {
	//delete DXF;
	delete this->dbReader;

	/*TRadarMapManager *mngr = static_cast<TRadarMapManager *>(this->Manager);

	if (this->GpsListener && mngr && mngr->GpsListeners) {
		mngr->GpsListeners->Delete(this->GpsListener);
		delete this->GpsListener;
		this->GpsListener = NULL;
	}*/
}

// -------- SetupDlgFilter -----------------------------------------------------
void __fastcall TDbDXFMapsContainer::SetupDlgFilter(TOpenDialog *pDialog) {
	System::UnicodeString filter = pDialog->Filter;
	System::UnicodeString db_filter = "|Database files (*.dbsl)|*.dbsl";
	System::UnicodeString dxf_ext = "|*.dxf";
	int pos = filter.Pos((System::UnicodeString)"|*.dbsl");

	if (pos < 1) {
		pos = filter.Pos(dxf_ext);

		if (pos > 0) {
			filter.Insert(db_filter, pos + dxf_ext.Length());
		}
	}
	pDialog->Filter = filter;
}
//------------------------------------------------------------------------------

// -------- LoadData -----------------------------------------------------------
void __fastcall TDbDXFMapsContainer::LoadData(AnsiString Filename, HANDLE AStopItEvent, bool WithUpdate, TCoordinateSystem CS,
	bool Preview) {

	TRadarMapManager *mngr = dynamic_cast<TRadarMapManager *>(this->Manager);
	TRadarMapSettings *rm_settings = static_cast<TRadarMapSettings *>(this->radarmapsettings);

//	bool Terminated;
	double coef, max, ww, hh, dd;
	int q, qq;
	TDXFLayer *layer;

	this->filename = Filename;

	if (CS == csLatLon && Manager) {
		mngr->ShowMessageObject(NULL, mngr->MapMessages, TMessageType::mtWarning,
			mvtDisapering, 10000, "Latitude-Longitude Coordinate system is not aplicable for Vector Maps", "Warning");
	}
	this->DXF->EnlargeDrawingTextHeight = rm_settings->DXFEnlargeText;

	this->DXF->Progress = this->progress;

//	if (this->FillDXFFileInfoFromDB() != true) {
	if (this->FillIntegratedDXFInfoFromDB() != true) {
		this->background = NULL;
		throw Exception("File does not exists!");
	}
	else {
		if (this->progress) this->progress->Show(this->DXF->Layers->Count, true);
		this->DXF->EnlargeDrawingTextHeight = rm_settings->DXFEnlargeText;

		try {
			this->coordinatesystem = CS;
			this->klicnummer = ExtractFileName(Filename);

			if (DXF->MinCorner && DXF->MaxCorner &&
				(DXF->UpperCorner.X - DXF->LowerCorner.X == 0.0 || DXF->UpperCorner.Y - DXF->LowerCorner.Y == 0.0)) {

				this->Lower = DoublePoint(DXF->MinCorner->X, DXF->MinCorner->Y);
				this->Upper = DoublePoint(DXF->MaxCorner->X, DXF->MaxCorner->Y);
			}
			else {
				this->Lower = DoublePoint(DXF->UpperCorner.X, DXF->LowerCorner.Y);
				this->Upper = DoublePoint(DXF->LowerCorner.X, DXF->UpperCorner.Y);
//				this->Lower = DXF->LowerCorner;
//				this->Upper = DXF->UpperCorner;
			}

			this->LatLonRectUpdate();
			ww = this->LatLonRect->Width(this->coordinatesystem);
			hh = this->LatLonRect->Height(this->coordinatesystem);

			max = (MaxBitmapWidth > MaxBitmapHeight) ? MaxBitmapWidth : MaxBitmapHeight;

			if(hh!=0 && ww!=0 && max!=0) {

				if (hh < ww) {
					coef = max / hh;//ww;//
					//coef = MaxBitmapWidth / ww;
					//if(hh * coef > MaxBitmapHeight) coef = MaxBitmapHeight / hh;
					if (Preview) {
						coef *= 0.5 *(double)Screen->DesktopHeight /(double)max;
					}
				}
				else {
					coef = max / ww;//hh;//
					//coef = MaxBitmapHeight / hh;
					//if(ww * coef > MaxBitmapWidth) coef = MaxBitmapWidth / ww;
					if(Preview) {
						coef *= 0.5 *(double)Screen->DesktopWidth /(double)max;
					}
				}

				this->background = new TBitmap32();
				this->background->DrawMode = dmBlend;
				this->background->BeginUpdate();

				try {
					this->background->SetSize((int)(ww * coef), (int)(hh * coef));
					this->background->Clear(rm_settings->DXFBackgroundColor); // 0xff212830);

					if (this->DXF->Layers->Count > 0) {
						if(!Preview)
							q = 1;
						else
							q = this->DXF->Layers->Count;
						qq = 0;

						for (int j = 0; j < q; j++) {
							layer =(TDXFLayer*)this->DXF->Layers->Items[j];

							if (Preview) {
								layer->TextHeightCoef = coef * 2;
							}
							if (layer) {
								qq += (int)this->LayerRender(layer, this->Background, rm_settings->DXFBackgroundColor,
									rm_settings->DXFColorInvertThreshold, this->GetMapRenderArea());
							}
						}
					}
				}
				__finally {
					this->background->EndUpdate();
				}
				// ApplyScaling(background); There is no sence to do it
				if (this->background != NULL) {
					this->UtilityBmp->SetSize(this->background->Width, this->background->Height);
					this->InfoBmp->SetSize(this->background->Width, this->background->Height);
				}
			}
		}
		__finally {
			if (WithUpdate)
				this->Update();

			LockedCS = true;
			if (this->progress) this->progress->Hide(true);
		}
	}
}
//------------------------------------------------------------------------------

// -------- ReloadData ---------------------------------------------------------
//void __fastcall TDbDXFMapsContainer::ReloadData1(HANDLE AStopItEvent, bool WithUpdate) {
//	TRadarMapSettings *rm_settings = static_cast<TRadarMapSettings *>(this->radarmapsettings);
//	TDXFLayer *layer;
//
//	try {
//		this->LatLonRectUpdate();
//
//		if(this->progress) this->progress->Show(this->DXF->Layers->Count, true);
//
//		if (this->DXF->Layers->Count > 0) {
//			try {
//				this->background->BeginUpdate();
//
//				layer = (TDXFLayer*)this->DXF->Layers->Items[0];
//				if (layer) {
//					this->LayerRender(layer, this->Background, rm_settings->DXFBackgroundColor,
//						rm_settings->DXFColorInvertThreshold, this->GetMapRenderArea());
//				}
//			}
//			__finally {
//				this->background->EndUpdate();
//			}
//		}
//	}
//	__finally {
//		if (WithUpdate) {
//			this->Update();
//		}
//		LockedCS = true;
//		if(this->progress) this->progress->Hide(true);
//	}
//}
////------------------------------------------------------------------------------

// -------- GetMapRenderArea ---------------------------------------------------
TDoubleRect __fastcall TDbDXFMapsContainer::GetMapRenderArea() {
	//TRadarMapManager *mngr = dynamic_cast<TRadarMapManager *>(this->Manager);
	//TMapsContainersCollection *collect = //mngr->MapObjects->Container;
	TDoubleRect rct;

	rct.LeftTop = Collection->ViewportAreaToCoordinatesRect(this->coordinatesrect, ViewportArea->LeftTop);
	rct.RightBottom = Collection->ViewportAreaToCoordinatesRect(this->coordinatesrect, ViewportArea->RightBottom);

	return rct;
}
//------------------------------------------------------------------------------

// -------- FillIntegratedDXFInfoFromDB ----------------------------------------
bool __fastcall TDbDXFMapsContainer::FillIntegratedDXFInfoFromDB() {
	SQLiteConnection * conn = NULL;
	bool is_ok = false;

	try {
		conn = new SQLiteConnection();
		conn->Open(this->FileName);

		SQLiteReader reader = this->dbReader->GetMapParametersRecordReader(conn);

		try {
			// ---- Get DXF members (faile information) from database ----------
			if (reader.Read()) {
				delete this->DXF;

				this->DXF = this->dbReader->IntegratedMapInfoFromDb(&reader);

				this->FillDXFFileCommentsInfoFromDB(conn, 0);
				this->FillDXFLayersInfoFromDB(conn, 0);
			}
			is_ok = true;
		}
		__finally {
			reader.Close();
		}
	}
	__finally {
		if (conn) {
			delete conn;
		}
	}
	return is_ok;
}
//------------------------------------------------------------------------------

// -------- FillDXFFileInfoFromDB ----------------------------------------------
bool __fastcall TDbDXFMapsContainer::FillDXFFileInfoFromDB() {
	SQLiteConnection * conn = NULL;
	bool is_ok = false;

	try {
		conn = new SQLiteConnection();
		conn->Open(this->FileName);

		SQLiteReader reader = this->dbReader->GetFileInfoReader(conn);

		try {
			// ---- Get DXF members (faile information) from database ----------
			if (reader.Read()) {
				delete this->DXF;

				this->DXF = this->dbReader->FileInfoFromDb(&reader);

				this->FillDXFFileCommentsInfoFromDB(conn, this->DXF->IdDbRecord);
				this->FillDXFLayersInfoFromDB(conn, this->DXF->IdDbRecord);
			}
			is_ok = true;
		}
		__finally {
			reader.Close();
		}
	}
	__finally {
		if (conn) {
			delete conn;
		}
	}
	return is_ok;
}
//------------------------------------------------------------------------------

// -------- FillDXFFileCommentsInfoFromDB --------------------------------------
bool __fastcall TDbDXFMapsContainer::FillDXFFileCommentsInfoFromDB(SQLiteConnection *pConn, int pIdFile) {
	SQLiteCommand *cmd = NULL;
	bool is_ok = false;

	try {
		if (pIdFile > 0) {
			cmd = new SQLiteCommand(pConn, "SELECT * FROM property_data WHERE ParentTable = 'file' AND ParentId = :pId");
			cmd->BindIntByName(":pId", pIdFile);
		}
		else {
			cmd = new SQLiteCommand(pConn, "SELECT * FROM property_data WHERE ParentTable = 'file'");
		}

		SQLiteReader reader = cmd->ExecuteReader();
		AnsiString prty_name, prty_value, name_value;

		while (reader.Read()) {
			prty_name = reader.GetString("PropertyName");
			prty_value = reader.GetString("PropertyValue");
			name_value = prty_name + this->DXF->Comments->NameValueSeparator + prty_value;

			this->DXF->Comments->Append(name_value);
		}
		reader.Close();
		is_ok = true;
	}
	__finally {
		if (cmd) {
			delete cmd;
		}
	}
	return is_ok;
}
// -----------------------------------------------------------------------------

// -------- FillDXFLayersInfoFromDB --------------------------------------------
bool __fastcall TDbDXFMapsContainer::FillDXFLayersInfoFromDB(SQLiteConnection *pConn, int pIdFile) {
	bool is_ok = false;
	SQLiteReader reader = this->dbReader->GetLayerInfoReader(pConn, pIdFile);

	try {
		TDXFLayer *layer = NULL;

		while (reader.Read()) {
			layer = this->dbReader->LayerInfoFromDb(&reader);
			this->DXF->AddLayer(layer, false);
		}
		is_ok = true;
	}
	__finally {
		reader.Close();
	}
	return is_ok;
}
//------------------------------------------------------------------------------

// -------- GetDXFFilesList ----------------------------------------------------
void __fastcall TDbDXFMapsContainer::GetDXFFilesList(AnsiString pDbFile, std::vector<FileRecord> *pFiles) {
	SQLiteConnection * conn = NULL;
	TDXFFromDbReader *db_reader = NULL;
	FileRecord *file_record = NULL;

	if (pFiles) {

		try {
			conn = new SQLiteConnection();
			conn->Open(pDbFile);
			db_reader = new TDXFFromDbReader();

			SQLiteReader reader = db_reader->GetFileInfoReader(conn, "");
			AnsiString file_name, file_dir, full_name;

			try {
				// ---- Get DXF members (faile information) from database ------
				while (reader.Read()) {
					file_record = db_reader->FileRecordFromDb(&reader);
					pFiles->push_back(*file_record);
				}
			}
			__finally {
				reader.Close();
			}
		}
		__finally {
			if (db_reader) {
				delete db_reader;
			}
			if (conn) {
				delete conn;
			}
		}
	}
}
//------------------------------------------------------------------------------

// -------- AddDXFFile ---------------------------------------------------------
bool __fastcall TDbDXFMapsContainer::AddDXFFile(AnsiString pDxfFileName,
	TVoidFunction SuccessCallBack, TVoidFunction CancelCallBack) {
	bool res = false;

	if (pDxfFileName != NULL && pDxfFileName.Length()> 0) {
		TRadarMapManager *mngr = static_cast<TRadarMapManager *>(this->Manager);

		//TDXFtoDbLoadThread *db_loader =
		new TDXFtoDbLoadThread(mngr, pDxfFileName, this->FileName, this->CoordinateSystem, NULL, //mngr->MapProgress);
			SuccessCallBack, CancelCallBack);
		res = true;
	}
	return res;
}
//------------------------------------------------------------------------------

// -------- DeleteDXFFile ------------------------------------------------------
bool __fastcall TDbDXFMapsContainer::DeleteDXFFile(AnsiString pDxfFileName,
	TVoidFunction SuccessCallBack, TVoidFunction CancelCallBack) {
	bool res = false;

	if (pDxfFileName != NULL && pDxfFileName.Length()> 0) {
		TRadarMapManager *mngr = static_cast<TRadarMapManager *>(this->Manager);

		//TDXFfromDbDeleteThread *db_deleter =
		new TDXFfromDbDeleteThread(mngr, pDxfFileName, this->FileName, this->CoordinateSystem, NULL, //mngr->MapProgress);
			SuccessCallBack, CancelCallBack);
		res = true;
	}
	return res;
}
//------------------------------------------------------------------------------

// -------- RefreshDXFFile -----------------------------------------------------
bool __fastcall TDbDXFMapsContainer::RefreshDXFFile(AnsiString dbFileName, AnsiString hddFileName,
	TVoidFunction SuccessCallBack, TVoidFunction CancelCallBack) {
	bool res = false;

	if (dbFileName != NULL && dbFileName.Length() > 0 && hddFileName != NULL && hddFileName.Length() > 0) {
		TRadarMapManager *mngr = static_cast<TRadarMapManager *>(this->Manager);

		//TDXFinDbRefreshThread *db_refresher =
		new TDXFinDbRefreshThread(mngr, dbFileName, hddFileName, this->FileName, this->CoordinateSystem, NULL, //mngr->MapProgress);
			SuccessCallBack, CancelCallBack);
		res = true;
	}
	return res;
}
//------------------------------------------------------------------------------

// -------- UtilityUpdate ------------------------------------------------------
void __fastcall TDbDXFMapsContainer::UtilityUpdate() {

	this->UtilityBmp->BeginUpdate();

	try {
		UtilityBmp->Clear(Color32(0, 0, 0, 0));

		if (this->Visible) {
			TRadarMapSettings *rm_settings = static_cast<TRadarMapSettings*>(this->radarmapsettings);
			TDoubleRect render_area = this->GetMapRenderArea();
			TDXFLayer *layer;

			for (int i = 1; i < DXF->Layers->Count; i++) { // Layer 0 is Background

				if (this->progress && this->progress->CancelPressed) {
					break;
				}

				layer = static_cast<TDXFLayer*>(DXF->Layers->Items[i]);

				if (layer && layer->Utility && layer->Visible) {
					this->LayerRender(layer, UtilityBmp, rm_settings->DXFBackgroundColor, rm_settings->DXFColorInvertThreshold,
						render_area);
				}
			}
		}
	}
	__finally {
		this->UtilityBmp->EndUpdate();
		// ApplyScaling(UtilityBmp); There is no sence to do it
	}
	if (this->Owner != NULL)
		this->Owner->SetObjectChanged(gotUtility);
}
//------------------------------------------------------------------------------

// -------- InfoUpdate ---------------------------------------------------------
void __fastcall TDbDXFMapsContainer::InfoUpdate() {

	this->InfoBmp->BeginUpdate();

	try {
		this->InfoBmp->Clear(Color32(0, 0, 0, 0));

		if (Visible) {
			TRadarMapSettings *rm_settings = static_cast<TRadarMapSettings*>(this->radarmapsettings);
			TDoubleRect render_area = this->GetMapRenderArea();
			TDXFLayer *layer;

			for(int i = 1; i < this->DXF->Layers->Count; i++) { // Layer 0 is Background

				if (this->progress && this->progress->CancelPressed) {
					break;
				}

				layer = static_cast<TDXFLayer*>(this->DXF->Layers->Items[i]);

				if (layer && !layer->Utility && layer->Visible) {
					this->LayerRender(layer, this->InfoBmp, rm_settings->DXFBackgroundColor,
						rm_settings->DXFColorInvertThreshold, render_area);
				}
			}
		}
	}
	__finally {
		this->InfoBmp->EndUpdate();
		// ApplyScaling(UtilityBmp); There is no sence to do it
	}
	if (this->Owner != NULL)
		this->Owner->SetObjectChanged(gotInfo);
}
//------------------------------------------------------------------------------

// -------- Update -------------------------------------------------------------
void __fastcall TDbDXFMapsContainer::Update() {
	DXF->EnlargeDrawingTextHeight =((TRadarMapSettings*)radarmapsettings)->DXFEnlargeText;
	TMapsContainer::Update();
}
//------------------------------------------------------------------------------

// -------- RenderMap ----------------------------------------------------------
void __fastcall TDbDXFMapsContainer::RenderMap() {

	if (this->radarmapsettings && (this->DXF->Layers->Count > 0)) {
		TRadarMapSettings *rm_settings = static_cast<TRadarMapSettings*>(this->radarmapsettings);
		TDXFLayer *layer;
		DXF->EnlargeDrawingTextHeight = rm_settings->DXFEnlargeText;

		layer = static_cast<TDXFLayer*>(this->DXF->Layers->Items[0]);

		if (layer && layer->Visible) {
			this->Background->Clear(rm_settings->DXFBackgroundColor);

			this->LayerRender(layer, this->Background, rm_settings->DXFBackgroundColor, rm_settings->DXFColorInvertThreshold,
				this->GetMapRenderArea());
		}
		this->UtilityUpdate();
		this->InfoUpdate();
	}
}
//------------------------------------------------------------------------------

// -------- GetLayers ----------------------------------------------------------
void __fastcall TDbDXFMapsContainer::GetLayers(TCheckBoxTreeView *TreeView, TTreeNode *Root) {
	TRadarMapManager *mngr = dynamic_cast<TRadarMapManager *>(this->Manager);
	bool need_file_name = mngr->Settings->DbShowLayerFileName;
	bool need_merge_layer = mngr->Settings->DbMergeLayersWithSameName;
	std::hash_map<int, UnicodeString> layer_file_map;
	TTreeNode *node;
	TDXFLayer *layer;
	UnicodeString layer_name, f_name;

	if (TreeView == NULL) {
		return;
	}

	// ---- Fill map layer --> file name from database -------------------------
	if (need_file_name) {
		SQLiteConnection *conn = NULL;
		int layer_id;

		conn = new SQLiteConnection();
		conn->Open(this->FileName);

		SQLiteReader reader = this->dbReader->GetLayersFileNamesReader(conn);

		try {
			while (reader.Read()) {
				layer_id = reader.GetInt("LayerId");
				layer_file_map[layer_id] = reader.GetString("FileName");
			}
		}
		catch(Exception & e) {
		}

		reader.Close();
		if (conn) {
			delete conn;
		}
	}

	// ---- Fill layers tree ---------------------------------------------------
	if (Root == NULL) {
		TreeView->Items->Clear();
		Root = TreeView->Items->Add(NULL, Klicnummer);
		TreeView->SetChecking(Root, Visible);
	}
	Root->Data =(void*)((TMapsContainer*)this);

	for (int i = 0; i < DXF->Layers->Count; i++) {
		layer =(TDXFLayer*)DXF->Layers->Items[i];

		if (need_merge_layer) {
			layer = this->MergeDXFLayerInLayersTree(layer, TreeView);
		}

		if (layer) {
			layer_name = layer->Name;


			node = TreeView->Items->AddChild(Root, layer_name);
			TreeView->SetChecking(node, layer->Visible);
			node->Data =(void*)layer;

			if (TreeView->Images) {
				node->ImageIndex =(int)layer->LayerType;
				node->SelectedIndex =(int)layer->LayerType;
			}
		}
	}

	// ---- Add layer file name to tree ----------------------------------------
	if (need_file_name) {

		for (int i = 0; i < TreeView->Items->Count; i++) {
			node = TreeView->Items->Item[i];
			layer = static_cast<TDXFLayer*>(node->Data);
			f_name = layer_file_map[layer->IdDbRecord];

			if (f_name.Length() > 0) {
				node->Text += " (" + f_name + ")";
			}
		}
	}
}
//------------------------------------------------------------------------------

// -------- MergeDXFLayerInLayersTree ------------------------------------------
TDXFLayer * __fastcall TDbDXFMapsContainer::MergeDXFLayerInLayersTree(TDXFLayer *pLayer, TCheckBoxTreeView *TreeView) {
	TDXFLayer *result = pLayer;
	TTreeNode *node;

	for (int i = 0; i < TreeView->Items->Count; i++) {
		node = TreeView->Items->Item[i];

		if (node->Text == pLayer->Name) {
			result = NULL;
			break;
		}
	}

	return result;
}
//------------------------------------------------------------------------------

// -------- SetLayers ----------------------------------------------------------
void __fastcall TDbDXFMapsContainer::SetLayers(TCheckBoxTreeView *TreeView, TTreeNode *Root) {
	SQLiteConnection *conn = NULL;
	TDXFToDBWriter *writer = NULL;
	TRadarMapManager *mngr = dynamic_cast<TRadarMapManager *>(this->Manager);
	bool need_save_visible = mngr->Settings->DbStoreLayersVisability;
	bool need_merge_layer = mngr->Settings->DbMergeLayersWithSameName;
	TDXFLayer *layer, *node_layer;
	TTreeNode *node;
	TMapsContainer *mc;

	if(TreeView == NULL) {
		return;
	}

	AutomaticUpdate = false;
	try {
		try {
			if (!Root) {
				Root = TreeView->Items->GetFirstNode();
			}
			if (Root) {
				mc = static_cast<TMapsContainer *>(Root->Data);

				if (mc->MapType == mtSQLiteDXF && Root->Text == Klicnummer) {
					Visible = TreeView->Checked(Root);
					node = Root->getFirstChild();

					conn = new SQLiteConnection();
					conn->Open(this->FileName);
					writer = new TDXFToDBWriter(conn);
					conn->BeginTransaction();

					while (node != NULL) {
						node_layer =(TDXFLayer*)node->Data;

						if (node_layer) {
							if (need_merge_layer) {

								for (int i = 0; i < DXF->Layers->Count; i++) {
									layer =(TDXFLayer*)DXF->Layers->Items[i];

									if (node_layer->Name == layer->Name) {
										this->UpdateDXFLayerInfo(layer, TreeView, node, need_save_visible, writer);
									}
								}
							}
							else {
								this->UpdateDXFLayerInfo(node_layer, TreeView, node, need_save_visible, writer);
							}
						}
						node = Root->GetNextChild(node);
					}
					conn->Commit();
				}
			}
		}
		catch(Exception & e) {
			if (conn) {
				conn->Rollback();
			}
		}
	}
	__finally {
		if (conn) {
			delete conn;
			if (writer) {
				delete writer;
			}
		}
		AutomaticUpdate = true;
		Update();
	}
}
// -----------------------------------------------------------------------------

// -------- UpdateDXFLayerInfo -------------------------------------------------
void __fastcall TDbDXFMapsContainer::UpdateDXFLayerInfo(TDXFLayer *pLayer, TCheckBoxTreeView *TreeView, TTreeNode *pNode,
	bool pSaveVisible, TDXFToDBWriter *pWriter) {

	if (pLayer->LayerType != (TDXFLayerType)pNode->ImageIndex) {
		pLayer->ChangeLayerType((TDXFLayerType)pNode->ImageIndex);
		pWriter->LayerTypeInfoToDB(pLayer);
	}

	if (pLayer->Visible != TreeView->Checked(pNode)) {
		pLayer->Visible = TreeView->Checked(pNode);

		if (pSaveVisible) {
			pWriter->LayerVisibleInfoToDB(pLayer);
		}
	}
}
// -----------------------------------------------------------------------------

// -------- LayerRender --------------------------------------------------------
bool __fastcall TDbDXFMapsContainer::LayerRender(TDXFLayer *pLayer, TBitmap32 *Dst, TColor32 bgColor, int InvertColor, TDoubleRect rct,
	TDouble3DPoint SourcePoint, double Scale_x, double Scale_y, double angle) { // angle in degrees

	SQLiteConnection *conn = NULL;
	TDbDXFEntityRender *render = new TDbDXFEntityRender();
	bool ok_render = true;

	std::stack<CoordinateChangeData *> change_stack;
	CoordinateChangeData *cc_data = NULL;

	TDXFInsert *insert;
	TDXFLine *line = NULL;
	TDXFCircle *circle = NULL;
	TDXFArc *arc = NULL;
	TDXFEllipse *ellipse = NULL;
	TDXFPoint *point = NULL;
	TDXFText *text = NULL;

	try {
		conn = new SQLiteConnection();
		conn->Open(this->FileName);

		SQLiteReader reader = this->dbReader->GetLayerEntitiesReader(conn, pLayer->IdDbRecord, true, rct);

		try {
			cc_data = new CoordinateChangeData();
			cc_data->XYZ = SourcePoint;
			change_stack.push(cc_data);

			render->Init(Dst, bgColor, InvertColor, rct);
			render->SetCoordinateChangeData(cc_data);

			// ---- Read entities from database --------------------------------
			int i_type = -1, db_parent_id = -1, db_record_id = -1;

			while (reader.Read()) {

				if (this->progress && this->progress->CancelPressed) {
					ok_render = false;
					break;
				}

				i_type = reader.GetInt("EntityType");
				db_parent_id = reader.GetInt("ParentEntityId");

//				int ent_id = reader.GetInt("EntityId");   // for debug

				if ((change_stack.top()->DbBlockRecordId != db_parent_id)) {

					if (db_parent_id > change_stack.top()->DbBlockRecordId) {
						// ---- not parent data in srack, skip entity ----------
						continue;
					}

					do {
						cc_data = change_stack.top();
						change_stack.pop();
						delete cc_data;
					}
					while ((change_stack.top()->DbBlockRecordId != db_parent_id));

					render->SetCoordinateChangeData(change_stack.top());
				}

				switch(this->IntToEntityType(i_type)) {

				case detPolyLine:
					db_record_id = reader.GetInt("EntityId");

					// ---- Create new coordinate correction data --------------
					cc_data = new CoordinateChangeData(*change_stack.top());
					cc_data->DbBlockRecordId = db_record_id;
					change_stack.push(cc_data);
					break;

				case detArc:
					arc = this->dbReader->ArcInfoFromDb(&reader);
					arc->Owner = pLayer;
					render->DrawArc(arc);
					break;

				case detCircle:
					circle = this->dbReader->CircleInfoFromDb(&reader);
					circle->Owner = pLayer;
					render->DrawCircle(circle);
					break;

				case detEllipse:
					ellipse = this->dbReader->EllipseInfoFromDb(&reader);
					ellipse->Owner = pLayer;
					render->DrawEllipse(ellipse);
					break;

				case detInsert:
					insert = this->dbReader->InsertInfoFromDb(&reader);
					insert->Owner = pLayer;
					db_record_id = reader.GetInt("EntityId");

					// ---- Create new coordinate correction data --------------
					cc_data = new CoordinateChangeData(*change_stack.top());
					this->ModifyChangeData(cc_data, insert);
					cc_data->DbBlockRecordId = db_record_id;
					change_stack.push(cc_data);
					render->SetCoordinateChangeData(cc_data);
					break;

				case detLine:
					line = this->dbReader->LineInfoFromDb(&reader);
					line->Owner = pLayer;
					render->DrawLine(line);
					break;

				case detLwPolyLine:
					db_record_id = reader.GetInt("EntityId");

					// ---- Create new coordinate correction data --------------
					cc_data = new CoordinateChangeData(*change_stack.top());
					cc_data->DbBlockRecordId = db_record_id;
					change_stack.push(cc_data);
					break;

				case detPoint:
					point = this->dbReader->PointInfoFromDb(&reader);
					point->Owner = pLayer;
					render->DrawPoint(point);
					break;

				case detText:
					text = this->dbReader->TextInfoFromDb(&reader);
					text->Owner = pLayer;
					text->EnlargeDrawingHeight = this->DXF->EnlargeDrawingTextHeight;
					render->DrawText(text);
					break;

				case detVertex:
					// vertex = (TDXFVertex*)obj;
					break;

				default:
					break;
				}
			}
		}
		__finally {
			reader.Close();
		}
	}
	__finally {
		if (conn) {
			delete conn;
		}
		delete render;

		while (! change_stack.empty()) {
			cc_data = change_stack.top();
			change_stack.pop();
			delete cc_data;
		}

		if (this->progress) this->progress->StepIt();
	}

	return ok_render;
}
// -----------------------------------------------------------------------------

// -------- IntToEntityType ----------------------------------------------------
TDXFEntityType __fastcall TDbDXFMapsContainer::IntToEntityType(int srcEntityType) {
	TDXFEntityType e_type = TDXFEntityType::detNone;

	switch(srcEntityType) {
	case 1:
		e_type = TDXFEntityType::detPolyLine;
		break;
	case 2:
		e_type = TDXFEntityType::detLine;
		break;
	case 3:
		e_type = TDXFEntityType::detCircle;
		break;
	case 4:
		e_type = TDXFEntityType::detArc;
		break;
	case 5:
		e_type = TDXFEntityType::detText;
		break;
	case 6:
		e_type = TDXFEntityType::detInsert;
		break;
	case 7:
		e_type = TDXFEntityType::detVertex;
		break;
	case 8:
		e_type = TDXFEntityType::detEllipse;
		break;
	case 9:
		e_type = TDXFEntityType::detPoint;
		break;
	case 10:
		e_type = TDXFEntityType::detLwPolyLine;
		break;
	default:
		break;
	}

	return e_type;
}
//------------------------------------------------------------------------------

// -------- ModifyChangeData ---------------------------------------------------
void __fastcall TDbDXFMapsContainer::ModifyChangeData(CoordinateChangeData *pCcData, TDXFInsert *pInsert) {

	pCcData->XYZ = pCcData->XYZ + pInsert->XYZ;
	pCcData->Scale_X *= pInsert->X_scale;
	pCcData->Scale_Y *= pInsert->Y_scale;
	pCcData->Scale_Z *= pInsert->Z_scale;
	pCcData->Angle += pInsert->Angle;
}
//------------------------------------------------------------------------------

// -------- GetFavotiteObjectsAtPoint ------------------------------------------
// Upstairs to the TMapsContainer
void __fastcall TDbDXFMapsContainer::GetFavotiteObjectsAtPoint(int X, int Y) {

	TRadarMapManager *mngr = dynamic_cast<TRadarMapManager *>(this->Manager);
	if (this->HasUtilityLayers()) {
		try {
			TMapsContainersCollection *collect = static_cast<TMapsContainersCollection *>(this->Owner);
			Types::TPoint point = Types::TPoint(X, Y);
			TDoublePoint d_point = collect->ScreenToCoordinatesRect(point);
			TDouble3DPoint point_3d = TDouble3DPoint(d_point.X, d_point.Y, 0.0);

			if(mngr && mngr->Settings)
				new TUPPredictorDbDXFThread(this, point_3d, this->ProcessPredictorResults, mngr->Settings->LabelDestructionTimeByClick,
				this->Manager);
		}
		catch(...) {
		}
	}
	else {
		mngr->ShowMessageObject(NULL, mngr->MapMessages, TMessageType::mtWarning,
			mvtDisapering, 10000, "There are not utility layers on this map!", "Warning");
	}
}
//------------------------------------------------------------------------------

// -------- HasUtilityLayers ---------------------------------------------------
bool __fastcall TDbDXFMapsContainer::HasUtilityLayers() {
	bool result = false;
	TDXFLayer *layer;

	for (int i = 0; i < this->DXFCollection->Layers->Count; i++) {
		layer = static_cast<TDXFLayer*>(this->DXFCollection->Layers->Items[i]);

		if (dltUtility == layer->LayerType) {
			result = true;
			break;
		}
	}
	return result;
}
//------------------------------------------------------------------------------

// -------- AddCoordinate ------------------------------------------------------
void __fastcall TDbDXFMapsContainer::AddCoordinate(TGPSCoordinate *c) {

	if (this->HasUtilityLayers()) {
		TRadarMapManager *mngr = dynamic_cast<TRadarMapManager *>(this->Manager);

		try {
			if (c && c->Valid && mngr && mngr->Settings && mngr->Settings->MapObjectsPrediction && LatLonRect->Contains(c)) {
				new TUPPredictorDbDXFThread(this, c->GetXYH(this->coordinatesystem), this->ProcessPredictorResults,
					mngr->Settings->LabelDestructionTimeByTrack, this->Manager); /* */
			}
		}
		catch(...) {
		}
	}
}
//------------------------------------------------------------------------------

void __fastcall TDbDXFMapsContainer::ProcessPredictorResults(void *vMapPoint, unsigned long ObjectsLifetime) {
	TRadarMapManager *mngr = dynamic_cast<TRadarMapManager *>(this->Manager);
	std::vector<TUPPFavoriteDbDXFObject *>::iterator it;
	TUPPPoint<TUPPFavoriteDbDXFObject> *pMapPoint;
	TUPPFavoriteDbDXFObject *favorite = NULL;
	double dist, min_dist, *min_dists;
	TColorPredictedLabel *cpl, *cpl2;
	TGPSCoordinate *c, *c2;
	AnsiString result;
	TDXFLayer *layer;
	HANDLE IdleEvent;
	TColor32 color;
	int i, j;

	pMapPoint = (TUPPPoint<TUPPFavoriteDbDXFObject>*)vMapPoint;
	if (pMapPoint && pMapPoint->mFavorites.size() > 0 && mngr && mngr->Settings &&
		DXF && DXF->Layers && DXF->Layers->Count > 0) {
		IdleEvent = CreateEvent(NULL, false, false, NULL);
		for (it = pMapPoint->mFavorites.begin(); it != pMapPoint->mFavorites.end(); it++) {
			favorite = *it;
			if(OnPredictorFoundObject) (OnPredictorFoundObject)(favorite);
			else {
				if (result.Pos(favorite->mText) == 0) {
					if (result.Length() > 0) {
						result += "\n\n";
					}
					if(favorite->mEntity) {
						c = new TGPSCoordinate();
						try {
							c->SetXYH(((TMapsContainersCollection*)Owner)->CoordinateSystem, favorite->mFoXYZ);// favorite->mEntity->XYZ);
							if(Owner && ((TMapsContainersCollection*)Owner)->Image && c->Valid)	{
								i = PredictedCoordinatesList->IndexOfUniq(c, CoordinateSystem);
								if(i == -1) {
									layer=DXF->GetLayerByDbId(favorite->mLayerId);
									if(layer) color=layer->ColorPX;
									else color=favorite->mEntity->ColorPX;
									cpl = new TColorPredictedLabel(mngr->MapObjects, ((TMapsContainersCollection*)Owner)->Image->GetViewportRect(),
										LatLonRect, color, //Color32(138, 237, 138),
										c, mngr->MapObjects->ObjectsGlobalTimer, ObjectsLifetime, mngr);
									cpl->Description = favorite->mText;
									if(layer) cpl->LayerID = favorite->mLayerId;
									else cpl->LayerID = -1;
								}
								else {
									delete c;
									c = PredictedCoordinatesList->Items[i];
									if(c && c->Valid && c->ConnectedPtr && ((TColorPredictedLabel*)c->ConnectedPtr)->MapLabelType == mltColorPredictedLabel) {
										cpl = (TColorPredictedLabel*)c->ConnectedPtr;
										try	{
											cpl->PostponeSelfDestruction();
										}
										catch(...) {}
									}
								}
							}
							else delete c;
						} __finally {
							//Sleep(10);
							WaitForSingleObject(IdleEvent, 10);
						}
					}
					// ToDo for each found object
					result += favorite->mText;
				}
			}/**/
		}
		j = -1;
		min_dist = pMapPoint->sRadius;
		min_dists=new double[DXF->Layers->Count];
		for(i = 0; i < DXF->Layers->Count; i++)
			min_dists[i]=pMapPoint->sRadius;
		try
		{
			cpl = NULL;
			for(i = 0; i < PredictedCoordinatesList->Count; i++) {
				c2 = PredictedCoordinatesList->Items[i];
				if(c2 && c2->Valid && c2->ConnectedPtr)	{
					cpl2 = (TColorPredictedLabel*)c2->ConnectedPtr;
					if(cpl2->MapLabelType == mltColorPredictedLabel) {
						dist = c2->DistanceToPoint(pMapPoint->mXYZ.XY, CoordinateSystem);
						if(dist <= min_dist) {
							min_dist = dist;
							j = i;
							cpl = cpl2;
						}
						if(cpl2->LayerID >=0 && cpl2->LayerID < DXF->Layers->Count && dist <= min_dists[cpl2->LayerID])
							min_dists[cpl2->LayerID] = dist;
					}
				}
			}
			if(j >= 0 && cpl) {
				for(i = 0; i < PredictedCoordinatesList->Count; i++) {
					c2 = PredictedCoordinatesList->Items[i];
					if(i != j && c2->Valid && c2->ConnectedPtr)	{
						cpl2 = (TColorPredictedLabel*)c2->ConnectedPtr;
						dist = c2->DistanceToPoint(pMapPoint->mXYZ.XY, CoordinateSystem);
						if(cpl2->MapLabelType == mltColorPredictedLabel) {
							if(mngr->Settings->MapObjectsPredictionSinglePerLayer && cpl2->LayerID >=0 &&
								cpl2->LayerID < DXF->Layers->Count && dist > min_dists[cpl2->LayerID]) {
								delete cpl2;
								cpl2 = NULL;
							}
							if(cpl2 && cpl2->PopedUp &&	dist <= pMapPoint->sRadius*2) cpl2->PopDown();
						}
					}
				}
				if(!cpl->PopedUp) cpl->PopUp();
			}
		}
		__finally
		{
			delete[] min_dists;
			CloseHandle(IdleEvent);
		}
	}
}
