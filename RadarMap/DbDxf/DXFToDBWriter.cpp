// -----------------------------------------------------------------------------
// DXFToDBWriter.cpp
// -----------------------------------------------------------------------------

#pragma hdrstop

#include "DXFToDBWriter.h"
#include <sys\stat.h>

// -----------------------------------------------------------------------------

#pragma package(smart_init)

// -------- Constructor / Destructor -------------------------------------------
__fastcall TDXFToDBWriter::~TDXFToDBWriter() {

	if (this->cmdLayerInsert) {
		delete this->cmdLayerInsert;
	}
	if (this->cmdLayerTypeUpdate) {
		delete this->cmdLayerTypeUpdate;
	}
	if (this->cmdLayerVisibleUpdate) {
		delete this->cmdLayerVisibleUpdate;
	}
	if (this->cmdPropertyInsert) {
		delete this->cmdLayerInsert;
	}
	if (this->cmdPolyLineInsert) {
		delete this->cmdPolyLineInsert;
	}
	if (this->cmdLineInsert) {
		delete this->cmdLineInsert;
	}
	if (this->cmdArcInsert) {
		delete this->cmdArcInsert;
	}
	if (this->cmdCircleInsert) {
		delete this->cmdCircleInsert;
	}
	if (this->cmdEllipseInsert) {
		delete this->cmdEllipseInsert;
	}
	if (this->cmdInsertInsert) {
		delete this->cmdInsertInsert;
	}
	if (this->cmdLwPolyLineInsert) {
		delete this->cmdLwPolyLineInsert;
	}
	if (this->cmdVertexInsert) {
		delete this->cmdVertexInsert;
	}
	if (this->cmdExGroupInsert) {
		delete this->cmdExGroupInsert;
	}
	if (this->cmdPointInsert) {
		delete this->cmdPointInsert;
	}
	if (this->cmdTextInsert) {
		delete this->cmdTextInsert;
	}
	if (this->cmdGeoIndexInsert) {
		delete this->cmdGeoIndexInsert;
	}
	if (this->cmdEntityGeoDataUpdate) {
		delete this->cmdEntityGeoDataUpdate;
	}
	if (this->cmdEntityChildSelect) {
		delete this->cmdEntityChildSelect;
	}
}
// -----------------------------------------------------------------------------

// -------- MapParametersInfoToDB ----------------------------------------------
int __fastcall TDXFToDBWriter::EmptyMapParametersToDB(TCoordinateSystem pCS) {
	AnsiString sql_text = "";
	SQLiteCommand *cmd = NULL;
	int id_record = -1;

	sql_text = "INSERT INTO map_params (CoordinateSystem)";
	sql_text += " VALUES (:map_CS);";

	cmd = new SQLiteCommand(this->conn, sql_text);

	// ---- Write to database --------------------------------------------------
	cmd->BindInt64ByName(":map_CS", pCS);

	cmd->ExecuteNonQuery();

	delete cmd;

	id_record = this->conn->GetLastInsertId();

	return id_record;
}
// -----------------------------------------------------------------------------

// -------- MapParametersInfoToDB ----------------------------------------------
int __fastcall TDXFToDBWriter::MapParametersInfoToDB(MapParametersRecord *pMapParams) {
	AnsiString sql_text = "";
	SQLiteCommand *cmd = NULL;
	TDoublePoint lc = pMapParams->mLowerCorner;
	TDoublePoint uc = pMapParams->mUpperCorner;

	sql_text = "UPDATE map_params SET";
	sql_text += "   ChangeCornersDate = CURRENT_TIMESTAMP, CoordinateSystem = :map_CS,";
	sql_text += "   LowerCorner_X = :lc_X, LowerCorner_Y = :lc_Y, UpperCorner_X = :uc_X, UpperCorner_Y = :uc_Y,";
	sql_text += "   MinCorner_X = :min_X, MinCorner_Y = :min_Y, MinCorner_Z = :min_Z,";
	sql_text += "   MaxCorner_X = :max_X, MaxCorner_Y = :max_Y, MaxCorner_Z = :max_Z";
	sql_text += " WHERE MapParamsId = :record_id;";

	cmd = new SQLiteCommand(this->conn, sql_text);

	// ---- Write to database --------------------------------------------------
	cmd->BindInt64ByName(":map_CS", pMapParams->mCS);

	cmd->BindDoubleByName(":lc_X", lc.X);
	cmd->BindDoubleByName(":lc_Y", lc.Y);
	cmd->BindDoubleByName(":uc_X", uc.X);
	cmd->BindDoubleByName(":uc_Y", uc.Y);

	cmd->BindDoubleByName(":min_X", pMapParams->mMinCorner.X);
	cmd->BindDoubleByName(":min_Y", pMapParams->mMinCorner.Y);
	cmd->BindDoubleByName(":min_Z", pMapParams->mMinCorner.Z);

	cmd->BindDoubleByName(":max_X", pMapParams->mMaxCorner.X);
	cmd->BindDoubleByName(":max_Y", pMapParams->mMaxCorner.Y);
	cmd->BindDoubleByName(":max_Z", pMapParams->mMaxCorner.Z);

	cmd->BindInt64ByName(":record_id", pMapParams->mIdDbRecord);

	cmd->ExecuteNonQuery();

	delete cmd;

	return pMapParams->mIdDbRecord;
}
// -----------------------------------------------------------------------------

// -------- FileInfoToDB -------------------------------------------------------
int __fastcall TDXFToDBWriter::FileInfoToDB(TDXFCollection *pDxf) {
	AnsiString sql_text = "";
	SQLiteCommand *cmd = NULL;
	TDoublePoint lc = pDxf->LowerCorner;
	TDoublePoint uc = pDxf->UpperCorner;
	int id_record = -1;

	sql_text = "INSERT INTO file (FileName, FileDir, FileCreateDate, FileSize, AutocadVersion,";
	sql_text += " LowerCorner_X, LowerCorner_Y, UpperCorner_X, UpperCorner_Y,";
	sql_text += " MinCorner_X, MinCorner_Y, MinCorner_Z, MaxCorner_X, MaxCorner_Y, MaxCorner_Z)";
	sql_text += " VALUES (:f_name, :f_dir, :f_date, :f_size, :a_version, :lc_X, :lc_Y, :uc_X, :uc_Y,";
	sql_text += "         :min_X, :min_Y, :min_Z, :max_X, :max_Y, :max_Z);";

	cmd = new SQLiteCommand(this->conn, sql_text);

	// ---- Write to database --------------------------------------------------
	struct stat statbuf;
	//int rez =
	stat(pDxf->DXFFileName.c_str(), &statbuf);

	cmd->BindStringByName(":f_name", ExtractFileName(pDxf->DXFFileName));
	cmd->BindStringByName(":f_dir", ExtractFilePath(pDxf->DXFFileName));
	cmd->BindInt64ByName(":f_date", statbuf.st_atime);
	cmd->BindInt64ByName(":f_size", statbuf.st_size);
	cmd->BindStringByName(":a_version", pDxf->AutoCadVersion);
	cmd->BindStringByName(":lc_X", lc.X);
	cmd->BindStringByName(":lc_Y", lc.Y);
	cmd->BindStringByName(":uc_X", uc.X);
	cmd->BindStringByName(":uc_Y", uc.Y);

	if (pDxf->MinCorner) {
		cmd->BindDoubleByName(":min_X", pDxf->MinCorner->X);
		cmd->BindDoubleByName(":min_Y", pDxf->MinCorner->Y);
		cmd->BindDoubleByName(":min_Z", pDxf->MinCorner->Z);
	}
	else {
		cmd->BindNullByName(":min_X");
		cmd->BindNullByName(":min_Y");
		cmd->BindNullByName(":min_Z");
	}

	if (pDxf->MaxCorner) {
		cmd->BindDoubleByName(":max_X", pDxf->MaxCorner->X);
		cmd->BindDoubleByName(":max_Y", pDxf->MaxCorner->Y);
		cmd->BindDoubleByName(":max_Z", pDxf->MaxCorner->Z);
	}
	else {
		cmd->BindNullByName(":max_X");
		cmd->BindNullByName(":max_Y");
		cmd->BindNullByName(":max_Z");
	}

	cmd->ExecuteNonQuery();

	delete cmd;

	id_record = this->conn->GetLastInsertId();

	// ---- Write comments to database -----------------------------------------
	if (pDxf->Comments) {
		UnicodeString key, value;

		for (int i = 0; i < pDxf->Comments->Count; i++) {
			 key = pDxf->Comments->Names[i];
			 value = pDxf->Comments->ValueFromIndex[i];

			 this->PropertyInfoToDB(id_record, "file", key, value);
		}
	}

	return id_record;
}
// -----------------------------------------------------------------------------

// -------- PropertyInfoToDB ---------------------------------------------------
int __fastcall TDXFToDBWriter::PropertyInfoToDB(int pParentId, const char *pTable, AnsiString pKey, AnsiString pValue) {
	SQLiteCommand *cmd = this->cmdPropertyInsert;

	if (NULL == cmd) {
		AnsiString sql_text = "";

		sql_text = "INSERT INTO property_data (ParentId, ParentTable, PropertyName, PropertyValue)";
		sql_text += " VALUES (:p_id, :p_table, :key, :value);";

		cmd = new SQLiteCommand(this->conn, sql_text);
		this->cmdPropertyInsert = cmd;
	}
	else {
		cmd->Reset();
	}

	// ---- Write to database --------------------------------------------------
	cmd->BindIntByName(":p_id", pParentId);
	cmd->BindStringByName(":p_table", pTable);
	cmd->BindStringByName(":key", pKey);
	cmd->BindStringByName(":value", pValue);

	cmd->ExecuteNonQuery();

	return this->conn->GetLastInsertId();
}
// -----------------------------------------------------------------------------

// -------- LayerInfoToDB ------------------------------------------------------
int __fastcall TDXFToDBWriter::LayerInfoToDB(TDXFLayer *pLayer, int pFileId) {
	SQLiteCommand *cmd = this->cmdLayerInsert;

	if (NULL == cmd) {
		AnsiString sql_text = "";

		sql_text = "INSERT INTO layer (FileId, LayerType,";
		sql_text += " MinCorner_X, MinCorner_Y, MinCorner_Z, MaxCorner_X, MaxCorner_Y, MaxCorner_Z,";
		sql_text += " Handle, Color, TextHeightCoef, Name, Description, Comment, Flags)";
		sql_text += " VALUES (:f_id, :l_type, :min_X, :min_Y, :min_Z, :max_X, :max_Y, :max_Z,";
		sql_text += "         :handle, :color, :txt_coef, :name, :descr, :comment, :flag);";

		cmd = new SQLiteCommand(this->conn, sql_text);
		this->cmdLayerInsert = cmd;
	}
	else {
		cmd->Reset();
	}

	// ---- Write to database --------------------------------------------------
	cmd->BindIntByName(":f_id", pFileId);
	cmd->BindIntByName(":l_type", pLayer->LayerType);

	if (pLayer->MinCorner) {
		cmd->BindDoubleByName(":min_X", pLayer->MinCorner->X);
		cmd->BindDoubleByName(":min_Y", pLayer->MinCorner->Y);
		cmd->BindDoubleByName(":min_Z", pLayer->MinCorner->Z);
	}
	else {
		cmd->BindNullByName(":min_X");
		cmd->BindNullByName(":min_Y");
		cmd->BindNullByName(":min_Z");
	}

	if (pLayer->MaxCorner) {
		cmd->BindDoubleByName(":max_X", pLayer->MaxCorner->X);
		cmd->BindDoubleByName(":max_Y", pLayer->MaxCorner->Y);
		cmd->BindDoubleByName(":max_Z", pLayer->MaxCorner->Z);
	}
	else {
		cmd->BindNullByName(":max_X");
		cmd->BindNullByName(":max_Y");
		cmd->BindNullByName(":max_Z");
	}

	cmd->BindInt64ByName(":handle", *(pLayer->Handle_Index));
	cmd->BindInt64ByName(":color", pLayer->Color);
	cmd->BindDoubleByName(":txt_coef", pLayer->TextHeightCoef);
	cmd->BindStringByName(":name", pLayer->Name);
	cmd->BindStringByName(":descr", pLayer->Description);
	cmd->BindStringByName(":comment", pLayer->Comment);
	cmd->BindStringByName(":flag", pLayer->Flags);

	cmd->ExecuteNonQuery();

	return this->conn->GetLastInsertId();
}
// -----------------------------------------------------------------------------

// -------- LayerTypeInfoToDB --------------------------------------------------
void __fastcall TDXFToDBWriter::LayerTypeInfoToDB(TDXFLayer *pLayer) {
	SQLiteCommand *cmd = this->cmdLayerTypeUpdate;

	if (NULL == cmd) {
		AnsiString sql_text = "";

		sql_text = "UPDATE layer SET LayerType = :l_type WHERE LayerId = :l_id";

		cmd = new SQLiteCommand(this->conn, sql_text);
		this->cmdLayerTypeUpdate = cmd;
	}
	else {
		cmd->Reset();
	}

	// ---- Update to database -------------------------------------------------
	cmd->BindIntByName(":l_type", pLayer->LayerType);
	cmd->BindIntByName(":l_id", pLayer->IdDbRecord);

	cmd->ExecuteNonQuery();
}
// -----------------------------------------------------------------------------

// -------- LayerVisibleInfoToDB -----------------------------------------------
void __fastcall TDXFToDBWriter::LayerVisibleInfoToDB(TDXFLayer *pLayer) {
	SQLiteCommand *cmd = this->cmdLayerVisibleUpdate;

	if (NULL == cmd) {
		AnsiString sql_text = "";

		sql_text = "UPDATE layer SET IsVisible = :l_visible WHERE LayerId = :l_id";

		cmd = new SQLiteCommand(this->conn, sql_text);
		this->cmdLayerVisibleUpdate = cmd;
	}
	else {
		cmd->Reset();
	}

	// ---- Update to database -------------------------------------------------
	int visible = (pLayer->Visible) ? 1 : 0;

	cmd->BindIntByName(":l_visible", visible);
	cmd->BindIntByName(":l_id", pLayer->IdDbRecord);

	cmd->ExecuteNonQuery();
}
// -----------------------------------------------------------------------------

// -------- UpdateEntityGeoInfo ------------------------------------------------
SQLiteReader __fastcall TDXFToDBWriter::SelectEntityChildGeoInfo(int pDbRecordId) {
	SQLiteCommand *cmd = this->cmdEntityChildSelect;

	if (NULL == cmd) {
		AnsiString sql_text = "";

		sql_text = "SELECT EntityId, Min_X, Max_X, Min_Y, Max_Y FROM entity";
		sql_text += "   WHERE ParentEntityId = :parent_id";

		cmd = new SQLiteCommand(this->conn, sql_text);
		this->cmdEntityChildSelect = cmd;
	}
	else {
		cmd->Reset();
	}

	// ---- Select children Geo adta -------------------------------------------
	cmd->BindIntByName(":parent_id", pDbRecordId);

	return cmd->ExecuteReader();
}
// -----------------------------------------------------------------------------

// -------- UpdateEntityGeoInfo ------------------------------------------------
void __fastcall TDXFToDBWriter::UpdateEntityGeoInfo(EntityWriterData *pWriteData, int pDbRecordId) {
	SQLiteCommand *cmd = this->cmdEntityGeoDataUpdate;
//	TDXFEntity *entity = pWriteData->Entity;

	if (NULL == cmd) {
		AnsiString sql_text = "";

		sql_text = "UPDATE entity SET";
		sql_text += "    Min_X = :min_X, Max_X = :max_X, Min_Y = :min_Y, Max_Y = :max_Y";
		sql_text += " WHERE EntityId = :entity_id;";

		cmd = new SQLiteCommand(this->conn, sql_text);
		this->cmdEntityGeoDataUpdate = cmd;
	}
	else {
		cmd->Reset();
	}

	// ---- Write to database --------------------------------------------------
	cmd->BindDoubleByName(":max_X", pWriteData->Max_X);
	cmd->BindDoubleByName(":min_Y", pWriteData->Min_Y);
	cmd->BindDoubleByName(":max_Y", pWriteData->Max_Y);
	cmd->BindDoubleByName(":min_X", pWriteData->Min_X);
	cmd->BindIntByName(":entity_id", pDbRecordId);

	cmd->ExecuteNonQuery();
}
// -----------------------------------------------------------------------------

// -------- ArcInfoToDB --------------------------------------------------------
int __fastcall TDXFToDBWriter::ArcInfoToDB(EntityWriterData *pArc, int pLayerId, int pParentId, int pLevel) {
	SQLiteCommand *cmd = this->cmdArcInsert;
	TDXFArc *arc = static_cast<TDXFArc *>(pArc->Entity);

	if (NULL == cmd) {
		AnsiString sql_text = "";

		sql_text = "INSERT INTO entity (EntityType, LayerId, Level, ParentEntityId, Point_X, Point_Y, Point_Z,";
		sql_text += "    Handle, Color, TextHeightCoef, Name, Description, Comment, Flags,";
		sql_text += "    Radius, Start, Stop,";
		sql_text += "    Angle, Scale_X, Scale_Y, Scale_Z, ";
		sql_text += "    Min_X, Max_X, Min_Y, Max_Y)";
		sql_text += " VALUES (:e_type, :l_id, :level, :parent_id, :p_X, :p_Y, :p_Z,";
		sql_text += "         :handle, :color, :txt_coef, :name, :descr, :comment, :flag,";
		sql_text += "         :radius, :start, :stop,";
		sql_text += "         :angle, :scale_X, :scale_Y, :scale_Z,";
		sql_text += "         :min_X, :max_X, :min_Y, :max_Y);";

		cmd = new SQLiteCommand(this->conn, sql_text);
		this->cmdArcInsert = cmd;
	}
	else {
		cmd->Reset();
	}

	// ---- Write to database --------------------------------------------------
	this->SetEntityDataToCmdInsert(pArc, cmd, pLayerId, pParentId, pLevel);

	cmd->BindDoubleByName(":radius", arc->R);
	cmd->BindDoubleByName(":start", arc->Start);
	cmd->BindDoubleByName(":stop", arc->Stop);

	cmd->ExecuteNonQuery();

	return this->conn->GetLastInsertId();
}
// -----------------------------------------------------------------------------

// -------- CircleInfoToDB -----------------------------------------------------
int __fastcall TDXFToDBWriter::CircleInfoToDB(EntityWriterData *pCircle, int pLayerId, int pParentId, int pLevel) {
	SQLiteCommand *cmd = this->cmdCircleInsert;
	TDXFCircle *circle = static_cast<TDXFCircle *>(pCircle->Entity);

	if (NULL == cmd) {
		AnsiString sql_text = "";

		sql_text = "INSERT INTO entity (EntityType, LayerId, Level, ParentEntityId, Point_X, Point_Y, Point_Z,";
		sql_text += "    Handle, Color, TextHeightCoef, Name, Description, Comment, Flags,";
		sql_text += "    Radius,";
		sql_text += "    Angle, Scale_X, Scale_Y, Scale_Z, ";
		sql_text += "    Min_X, Max_X, Min_Y, Max_Y)";
		sql_text += " VALUES (:e_type, :l_id, :level, :parent_id, :p_X, :p_Y, :p_Z,";
		sql_text += "         :handle, :color, :txt_coef, :name, :descr, :comment, :flag,";
		sql_text += "         :radius,";
		sql_text += "         :angle, :scale_X, :scale_Y, :scale_Z,";
		sql_text += "         :min_X, :max_X, :min_Y, :max_Y);";

		cmd = new SQLiteCommand(this->conn, sql_text);
		this->cmdCircleInsert = cmd;
	}
	else {
		cmd->Reset();
	}

	// ---- Write to database --------------------------------------------------
	this->SetEntityDataToCmdInsert(pCircle, cmd, pLayerId, pParentId, pLevel);

	cmd->BindDoubleByName(":radius", circle->R);

	cmd->ExecuteNonQuery();

	return this->conn->GetLastInsertId();
}
// -----------------------------------------------------------------------------

// -------- EllipseInfoToDB ----------------------------------------------------
int __fastcall TDXFToDBWriter::EllipseInfoToDB(EntityWriterData *pEllipse, int pLayerId, int pParentId, int pLevel) {
	SQLiteCommand *cmd = this->cmdEllipseInsert;
	TDXFEllipse *ellipse = static_cast<TDXFEllipse *>(pEllipse->Entity);

	if (NULL == cmd) {
		AnsiString sql_text = "";

		sql_text = "INSERT INTO entity (EntityType, LayerId, Level, ParentEntityId, Point_X, Point_Y, Point_Z,";
		sql_text += "    Handle, Color, TextHeightCoef, Name, Description, Comment, Flags,";
		sql_text += "    ExtraPoint_X, ExtraPoint_Y, ExtraPoint_Z,";
		sql_text += "    Start, Stop, Mn2Mj,";
		sql_text += "    Angle, Scale_X, Scale_Y, Scale_Z, ";
		sql_text += "    Min_X, Max_X, Min_Y, Max_Y)";
		sql_text += " VALUES (:e_type, :l_id, :level, :parent_id, :p_X, :p_Y, :p_Z,";
		sql_text += "         :handle, :color, :txt_coef, :name, :descr, :comment, :flag,";
		sql_text += "         :ep_X, :ep_Y, :ep_Z,";
		sql_text += "         :start, :stop, :mn_2_mj,";
		sql_text += "         :angle, :scale_X, :scale_Y, :scale_Z,";
		sql_text += "         :min_X, :max_X, :min_Y, :max_Y);";

		cmd = new SQLiteCommand(this->conn, sql_text);
		this->cmdEllipseInsert = cmd;
	}
	else {
		cmd->Reset();
	}

	// ---- Write to database --------------------------------------------------
	this->SetEntityDataToCmdInsert(pEllipse, cmd, pLayerId, pParentId, pLevel);

	cmd->BindDoubleByName(":ep_X", ellipse->MjEndPoint.X);
	cmd->BindDoubleByName(":ep_Y", ellipse->MjEndPoint.Y);
	cmd->BindDoubleByName(":ep_Z", ellipse->MjEndPoint.Z);

	cmd->BindDoubleByName(":start", ellipse->Start);
	cmd->BindDoubleByName(":stop", ellipse->Stop);
	cmd->BindDoubleByName(":mn_2_mj", ellipse->Mn2MjRatio);

	cmd->ExecuteNonQuery();

	return this->conn->GetLastInsertId();
}
// -----------------------------------------------------------------------------

// -------- InsertInfoToDB -----------------------------------------------------
int __fastcall TDXFToDBWriter::InsertInfoToDB(EntityWriterData *pInsert, int pLayerId, int pParentId, int pLevel) {
	SQLiteCommand *cmd = this->cmdInsertInsert;
	TDXFInsert *insert = static_cast<TDXFInsert *>(pInsert->Entity);

	if (NULL == cmd) {
		AnsiString sql_text = "";

		sql_text = "INSERT INTO entity (EntityType, LayerId, Level, ParentEntityId, Point_X, Point_Y, Point_Z,";
		sql_text += "    Handle, Color, TextHeightCoef, Name, Description, Comment, Flags,";
		sql_text += "    Angle,";
		sql_text += "    BlockName, Scale_X, Scale_Y, Scale_Z, Min_X, Max_X, Min_Y, Max_Y)";
		sql_text += " VALUES (:e_type, :l_id, :level, :parent_id, :p_X, :p_Y, :p_Z,";
		sql_text += "         :handle, :color, :txt_coef, :name, :descr, :comment, :flag,";
		sql_text += "         :angle,";
		sql_text += "         :bl_name, :scale_X, :scale_Y, :scale_Z, :min_X, :max_X, :min_Y, :max_Y);";

		cmd = new SQLiteCommand(this->conn, sql_text);
		this->cmdInsertInsert = cmd;
	}
	else {
		cmd->Reset();
	}

	// ---- Write to database --------------------------------------------------
	this->SetEntityDataToCmdInsert(pInsert, cmd, pLayerId, pParentId, pLevel);

	cmd->BindDoubleByName(":angle", insert->Angle);
	cmd->BindStringByName(":bl_name", insert->BlockName);
	cmd->BindDoubleByName(":scale_X", insert->X_scale);
	cmd->BindDoubleByName(":scale_Y", insert->Y_scale);
	cmd->BindDoubleByName(":scale_Z", insert->Z_scale);

	cmd->ExecuteNonQuery();

	return this->conn->GetLastInsertId();
}
// -----------------------------------------------------------------------------

// -------- LineInfoToDB --------------------------------------------------
int __fastcall TDXFToDBWriter::LineInfoToDB(EntityWriterData *pLine, int pLayerId, int pParentId, int pLevel) {
	SQLiteCommand *cmd = this->cmdLineInsert;
	TDXFLine *line = static_cast<TDXFLine *>(pLine->Entity);

	if (NULL == cmd) {
		AnsiString sql_text = "";

		sql_text = "INSERT INTO entity (EntityType, LayerId, Level, ParentEntityId, Point_X, Point_Y, Point_Z,";
		sql_text += "    Handle, Color, TextHeightCoef, Name, Description, Comment, Flags,";
		sql_text += "    ExtraPoint_X, ExtraPoint_Y, ExtraPoint_Z,";
		sql_text += "    Angle, Scale_X, Scale_Y, Scale_Z, ";
		sql_text += "    Min_X, Max_X, Min_Y, Max_Y)";
		sql_text += " VALUES (:e_type, :l_id, :level, :parent_id, :p_X, :p_Y, :p_Z,";
		sql_text += "         :handle, :color, :txt_coef, :name, :descr, :comment, :flag,";
		sql_text += "         :ep_X, :ep_Y, :ep_Z,";
		sql_text += "         :angle, :scale_X, :scale_Y, :scale_Z,";
		sql_text += "         :min_X, :max_X, :min_Y, :max_Y);";

		cmd = new SQLiteCommand(this->conn, sql_text);
		this->cmdLineInsert = cmd;
	}
	else {
		cmd->Reset();
	}

	// ---- Write to database --------------------------------------------------
	this->SetEntityDataToCmdInsert(pLine, cmd, pLayerId, pParentId, pLevel);

	cmd->BindDoubleByName(":ep_X", line->Point2.X);
	cmd->BindDoubleByName(":ep_Y", line->Point2.Y);
	cmd->BindDoubleByName(":ep_Z", line->Point2.Z);

	cmd->ExecuteNonQuery();

	return this->conn->GetLastInsertId();
}
// -----------------------------------------------------------------------------

// -------- LwPolyLineInfoToDB -------------------------------------------------
int __fastcall TDXFToDBWriter::LwPolyLineInfoToDB(EntityWriterData *pLwPolyLine, int pLayerId, int pParentId, int pLevel) {
	SQLiteCommand *cmd = this->cmdLwPolyLineInsert;
	TDXFLwPolyLine *poly_line = static_cast<TDXFLwPolyLine *>(pLwPolyLine->Entity);

	if (NULL == cmd) {
		AnsiString sql_text = "";

		sql_text = "INSERT INTO entity (EntityType, LayerId, Level, ParentEntityId, Point_X, Point_Y, Point_Z,";
		sql_text += "    Handle, Color, TextHeightCoef, Name, Description, Comment, Flags,";
		sql_text += "    Closed, Thickness,  Plinegen, Width,";
		sql_text += "    Angle, Scale_X, Scale_Y, Scale_Z, ";
		sql_text += "    Min_X, Max_X, Min_Y, Max_Y)";
		sql_text += " VALUES (:e_type, :l_id, :level, :parent_id, :p_X, :p_Y, :p_Z,";
		sql_text += "         :handle, :color, :txt_coef, :name, :descr, :comment, :flag,";
		sql_text += "         :closed, :thick, :plinegen, :width,";
 		sql_text += "         :angle, :scale_X, :scale_Y, :scale_Z,";
		sql_text += "         :min_X, :max_X, :min_Y, :max_Y);";

		cmd = new SQLiteCommand(this->conn, sql_text);
		this->cmdLwPolyLineInsert = cmd;
	}
	else {
		cmd->Reset();
	}

	// ---- Write to database --------------------------------------------------
	this->SetEntityDataToCmdInsert(pLwPolyLine, cmd, pLayerId, pParentId, pLevel);

	cmd->BindIntByName(":closed", (poly_line->Closed) ? 1 : 0);
	cmd->BindIntByName(":plinegen", (poly_line->Plinegen) ? 1 : 0);
	cmd->BindDoubleByName(":thick", poly_line->Thickness3D);
	cmd->BindDoubleByName(":width", poly_line->Width);

	cmd->ExecuteNonQuery();

	return this->conn->GetLastInsertId();
}
// -----------------------------------------------------------------------------

// -------- PointInfoToDB ------------------------------------------------------
int __fastcall TDXFToDBWriter::PointInfoToDB(EntityWriterData *pPoint, int pLayerId, int pParentId, int pLevel) {
	SQLiteCommand *cmd = this->cmdPointInsert;

	if (NULL == cmd) {
		AnsiString sql_text = "";

		sql_text = "INSERT INTO entity (EntityType, LayerId, Level, ParentEntityId, Point_X, Point_Y, Point_Z,";
		sql_text += "    Handle, Color, TextHeightCoef, Name, Description, Comment, Flags,";
		sql_text += "    Angle, Scale_X, Scale_Y, Scale_Z, ";
		sql_text += "    Min_X, Max_X, Min_Y, Max_Y)";
		sql_text += " VALUES (:e_type, :l_id, :level, :parent_id, :p_X, :p_Y, :p_Z,";
		sql_text += "         :handle, :color, :txt_coef, :name, :descr, :comment, :flag,";
		sql_text += "         :angle, :scale_X, :scale_Y, :scale_Z,";
		sql_text += "         :min_X, :max_X, :min_Y, :max_Y);";

		cmd = new SQLiteCommand(this->conn, sql_text);
		this->cmdPointInsert = cmd;
	}
	else {
		cmd->Reset();
	}

	// ---- Write to database --------------------------------------------------
	this->SetEntityDataToCmdInsert(pPoint, cmd, pLayerId, pParentId, pLevel);

	cmd->ExecuteNonQuery();

	return this->conn->GetLastInsertId();
}
// -----------------------------------------------------------------------------

// -------- PolyLineInfoToDB ---------------------------------------------------
int __fastcall TDXFToDBWriter::PolyLineInfoToDB(EntityWriterData *pPolyLine, int pLayerId, int pParentId, int pLevel) {
	SQLiteCommand *cmd = this->cmdPolyLineInsert;
	TDXFPolyLine *poly_line = static_cast<TDXFPolyLine *>(pPolyLine->Entity);

	if (NULL == cmd) {
		AnsiString sql_text = "";

		sql_text = "INSERT INTO entity (EntityType, LayerId, Level, ParentEntityId, Point_X, Point_Y, Point_Z,";
		sql_text += "    Handle, Color, TextHeightCoef, Name, Description, Comment, Flags,";
		sql_text += "    Closed, PolyLine3D, Thickness,";
		sql_text += "    Angle, Scale_X, Scale_Y, Scale_Z, ";
		sql_text += "    Min_X, Max_X, Min_Y, Max_Y)";
		sql_text += " VALUES (:e_type, :l_id, :level, :parent_id, :p_X, :p_Y, :p_Z,";
		sql_text += "         :handle, :color, :txt_coef, :name, :descr, :comment, :flag,";
		sql_text += "         :closed, :pl_3D, :thick,";
		sql_text += "         :angle, :scale_X, :scale_Y, :scale_Z,";
		sql_text += "         :min_X, :max_X, :min_Y, :max_Y);";

		cmd = new SQLiteCommand(this->conn, sql_text);
		this->cmdPolyLineInsert = cmd;
	}
	else {
		cmd->Reset();
	}

	// ---- Write to database --------------------------------------------------
	this->SetEntityDataToCmdInsert(pPolyLine, cmd, pLayerId, pParentId, pLevel);

	cmd->BindIntByName(":closed", (poly_line->Closed) ? 1 : 0);
	cmd->BindIntByName(":pl_3D", (poly_line->Polyline3D) ? 1 : 0);
	cmd->BindDoubleByName(":thick", poly_line->Thickness3D);

	cmd->ExecuteNonQuery();

	return this->conn->GetLastInsertId();
}
// -----------------------------------------------------------------------------

// -------- TextInfoToDB -------------------------------------------------------
int __fastcall TDXFToDBWriter::TextInfoToDB(EntityWriterData *pText, int pLayerId, int pParentId, int pLevel) {
	SQLiteCommand *cmd = this->cmdTextInsert;
	TDXFText *text = static_cast<TDXFText *>(pText->Entity);

	if (NULL == cmd) {
		AnsiString sql_text = "";

		sql_text = "INSERT INTO entity (EntityType, LayerId, Level, ParentEntityId, Point_X, Point_Y, Point_Z,";
		sql_text += "    Handle, Color, TextHeightCoef, Name, Description, Comment, Flags,";
		sql_text += "    ExtraPoint_X, ExtraPoint_Y, ExtraPoint_Z,";
		sql_text += "    Text, Style, Height, TextAngle, HorAlignment, VertAlignment,";
		sql_text += "    Angle, Scale_X, Scale_Y, Scale_Z, ";
		sql_text += "    Min_X, Max_X, Min_Y, Max_Y)";
		sql_text += " VALUES (:e_type, :l_id, :level, :parent_id, :p_X, :p_Y, :p_Z,";
		sql_text += "         :handle, :color, :txt_coef, :name, :descr, :comment, :flag,";
		sql_text += "         :ep_X, :ep_Y, :ep_Z,";
		sql_text += "         :txt, :t_style, :height, :txt_angle, :h_align, :v_align,";
 		sql_text += "         :angle, :scale_X, :scale_Y, :scale_Z,";
		sql_text += "         :min_X, :max_X, :min_Y, :max_Y);";

		cmd = new SQLiteCommand(this->conn, sql_text);
		this->cmdTextInsert = cmd;
	}
	else {
		cmd->Reset();
	}

	// ---- Write to database --------------------------------------------------
	this->SetEntityDataToCmdInsert(pText, cmd, pLayerId, pParentId, pLevel);

	cmd->BindDoubleByName(":ep_X", text->AlignmentPoint.X);
	cmd->BindDoubleByName(":ep_Y", text->AlignmentPoint.Y);
	cmd->BindDoubleByName(":ep_Z", text->AlignmentPoint.Z);

	cmd->BindStringByName(":txt", text->Text);
	cmd->BindStringByName(":t_style", text->Style);
	cmd->BindDoubleByName(":height", text->Height);
	cmd->BindDoubleByName(":txt_angle", text->Angle);
	cmd->BindIntByName(":h_align", text->HorA);
	cmd->BindIntByName(":v_align", text->VerA);

	cmd->ExecuteNonQuery();

	return this->conn->GetLastInsertId();
}
// -----------------------------------------------------------------------------

// -------- VertexInfoToDB -----------------------------------------------------
int __fastcall TDXFToDBWriter::VertexInfoToDB(TDXFVertex *pVertex, int pParentId) {
	SQLiteCommand *cmd = this->cmdVertexInsert;

	if (NULL == cmd) {
		AnsiString sql_text = "";

		sql_text = "INSERT INTO entity_vertex (EntityId, Point_X, Point_Y, Point_Z)";
		sql_text += " VALUES (:e_id, :p_X, :p_Y, :p_Z);";

		cmd = new SQLiteCommand(this->conn, sql_text);
		this->cmdVertexInsert = cmd;
	}
	else {
		cmd->Reset();
	}

	// ---- Write to database --------------------------------------------------
	cmd->BindIntByName(":e_id", pParentId);

	cmd->BindDoubleByName(":p_X", pVertex->XYZ.X);
	cmd->BindDoubleByName(":p_Y", pVertex->XYZ.Y);
	cmd->BindDoubleByName(":p_Z", pVertex->XYZ.Z);

	cmd->ExecuteNonQuery();

	return this->conn->GetLastInsertId();
}
// -----------------------------------------------------------------------------

// -------- ExGroupInfoToDB -----------------------------------------------------
int __fastcall TDXFToDBWriter::ExGroupInfoToDB(TDXFGroup *pGroup, int pParentId, int pSeqNumber) {
	SQLiteCommand *cmd = this->cmdExGroupInsert;

	if (NULL == cmd) {
		AnsiString sql_text = "";

		sql_text = "INSERT INTO entity_ex_group (EntityId, SeqNumInGroup, GroupCode, Value)";
		sql_text += " VALUES (:e_id, :p_seq_num, :p_code, :p_value);";

		cmd = new SQLiteCommand(this->conn, sql_text);
		this->cmdExGroupInsert = cmd;
	}
	else {
		cmd->Reset();
	}

	// ---- Write to database --------------------------------------------------
	cmd->BindIntByName(":e_id", pParentId);

	cmd->BindIntByName(":p_seq_num", pSeqNumber);
	cmd->BindIntByName(":p_code", pGroup->ID);
	cmd->BindStringByName(":p_value", pGroup->Value);

	cmd->ExecuteNonQuery();

	return this->conn->GetLastInsertId();
}
// -----------------------------------------------------------------------------

// -------- SetEntityDataToCmdInsert -------------------------------------------
void __fastcall TDXFToDBWriter::SetEntityDataToCmdInsert(EntityWriterData *pWriteData, SQLiteCommand *pCmd, int pLayerId, int pParentId, int pLevel) {
	TDXFEntity *entity = pWriteData->Entity;

	pCmd->BindIntByName(":e_type", entity->Type);
	pCmd->BindIntByName(":l_id", pLayerId);
	pCmd->BindIntByName(":level", pLevel);
	pCmd->BindIntByName(":parent_id", pParentId);
	pCmd->BindDoubleByName(":p_X", entity->XYZ.X);
	pCmd->BindDoubleByName(":p_Y", entity->XYZ.Y);
	pCmd->BindDoubleByName(":p_Z", entity->XYZ.Z);

	pCmd->BindInt64ByName(":handle", entity->Handle);
	pCmd->BindInt64ByName(":color", entity->Color);
	pCmd->BindDoubleByName(":txt_coef", entity->TextHeightCoef);
	pCmd->BindStringByName(":name", entity->Name);
	pCmd->BindStringByName(":descr", entity->Description);
	pCmd->BindStringByName(":comment", entity->Comment);
	pCmd->BindStringByName(":flag", entity->Flags);

	pCmd->BindDoubleByName(":min_X", pWriteData->Min_X);
	pCmd->BindDoubleByName(":max_X", pWriteData->Max_X);
	pCmd->BindDoubleByName(":min_Y", pWriteData->Min_Y);
	pCmd->BindDoubleByName(":max_Y", pWriteData->Max_Y);

	if (detInsert != entity->Type) {
		CoordinateChangeData *coord_change = pWriteData->ChangeData;

		if (coord_change) {
			pCmd->BindDoubleByName(":angle", coord_change->Angle);
			pCmd->BindDoubleByName(":scale_X", coord_change->Scale_X);
			pCmd->BindDoubleByName(":scale_Y", coord_change->Scale_Y);
			pCmd->BindDoubleByName(":scale_Z", coord_change->Scale_Z);
		} else {
			pCmd->BindNullByName(":angle");
			pCmd->BindNullByName(":scale_X");
			pCmd->BindNullByName(":scale_Y");
			pCmd->BindNullByName(":scale_Z");
		}
	}
}
// -----------------------------------------------------------------------------

// -------- GeoIndexInfoToDB ---------------------------------------------------
void __fastcall TDXFToDBWriter::GeoIndexInfoToDB(SQLiteReader *pGeoData) {
	SQLiteCommand *cmd = this->cmdGeoIndexInsert;

	if (NULL == cmd) {
		AnsiString sql_text = "";

		sql_text = "INSERT INTO geo_index (EntityId, Min_X, Max_X, Min_Y, Max_Y)";
		sql_text += " VALUES (:e_id, :min_X, :max_X, :min_Y, :max_Y);";

		cmd = new SQLiteCommand(this->conn, sql_text);
		this->cmdGeoIndexInsert = cmd;
	}
	else {
		cmd->Reset();
	}

	cmd->BindIntByName(":e_id", pGeoData->GetInt("EntityId"));

	cmd->BindDoubleByName(":min_X", pGeoData->GetDouble("Min_X"));
	cmd->BindDoubleByName(":max_X", pGeoData->GetDouble("Max_X"));
	cmd->BindDoubleByName(":min_Y", pGeoData->GetDouble("Min_Y"));
	cmd->BindDoubleByName(":max_Y", pGeoData->GetDouble("Max_Y"));

	cmd->ExecuteNonQuery();
}
// -----------------------------------------------------------------------------
