// -----------------------------------------------------------------------------
// DbDXFEntityRender.cpp
// -----------------------------------------------------------------------------

#pragma hdrstop

#include "DbDXFEntityRender.h"
#include "MyGraph.h"
#include "GR32_Text.hpp"

//------------------------------------------------------------------------------

#pragma package(smart_init)

//------------------------------------------------------------------------------
// TDbDXFEntityRender
//------------------------------------------------------------------------------

// -------- Constructor / Destructor -------------------------------------------

// -------- Init ---------------------------------------------------------------
void __fastcall TDbDXFEntityRender::Init(TBitmap32 *pDst, TColor32 pBgColor, int pInvertColor, TDoubleRect pRect) {
	this->Dst = pDst;
	this->bgColor = pBgColor;
	this->invertColor = pInvertColor;
	this->rect = pRect;

	if (pDst) {
		this->dstRect = pDst->BoundsRect();
		this->coef_X = (float)pDst->Width / fabs(this->rect.Width());
		this->coef_Y = (float)pDst->Height / fabs(this->rect.Height());
	}

	this->inRectObjects = 0;
	this->RecalcRenderParameters();
}
//------------------------------------------------------------------------------

// -------- SetCoordinateChangeData --------------------------------------------
void __fastcall TDbDXFEntityRender::SetCoordinateChangeData(CoordinateChangeData *CcData) {
	this->sPoint = CcData->XYZ;
	this->scale_X = CcData->Scale_X;
	this->scale_Y = CcData->Scale_Y;
	this->scale_Z = CcData->Scale_Z;
	this->ccAngle = CcData->Angle;

	this->RecalcRenderParameters();
}
//------------------------------------------------------------------------------

// -------- LineDraw -----------------------------------------------------------
void __fastcall TDbDXFEntityRender::RecalcRenderParameters() {

	this->spX = fabs(this->sPoint.X);

	if (this->spX < this->rect.Left) {
		this->ePoint.X = this->rect.Left;
	}
	else if (this->spX > this->rect.Right) {
		this->ePoint.X = this->rect.Right;
	}
	else {
		this->ePoint.X = this->spX;
	}

	if (this->sPoint.Y < this->rect.Bottom) {
		this->ePoint.Y = this->rect.Bottom;
	}
	else if (this->sPoint.Y > this->rect.Top) {
		this->ePoint.Y = this->rect.Top;
	}
	else {
		this->ePoint.Y = this->sPoint.Y;
	}
}
//------------------------------------------------------------------------------

// -------- DrawArc ------------------------------------------------------------
void __fastcall TDbDXFEntityRender::DrawArc(TDXFArc *pArc) {
	TColor32 color = this->CalculateColor(pArc);
	TLine p1, p2;
	TDouble3DPoint p3d;
	double da, db, xa;

	da = (pArc->Stop - pArc->Start) * this->deg2Rad;

	if (da > M_PI2 || da < -M_PI2) {
		da = M_PI2;
	}
	else if(da < 0) {
		da += M_PI2;
	}

	da /= (double)ArcPoints;
	xa = fabs(pArc->XYZ.X);
	p1.X1 = (this->spX + xa - this->rect.Left) * this->coef_X;
	p1.Y1 = (this->rect.Top - pArc->XYZ.Y - this->sPoint.Y) * this->coef_Y;
	p1.X2 = (this->spX + pArc->XYZ.X + this->scale_X * pArc->R - this->rect.Left) * this->coef_X; // p1.X1+Scale_x*arc->R*coef_X;
	p1.Y2 = p1.Y1; // (rct.Top-arc->XYZ.Y-sp.Y)*coef_Y;//

	if (this->dstRect.Contains(Types::TRect(p1.X1 - p1.Length(), p1.Y1 - p1.Length(), p1.X1 + p1.Length(), p1.Y1 + p1.Length()))) {
		p1.Rotate(-pArc->Start * this->deg2Rad);
		this->Dst->PenColor = color;

		for (int j = 0; j < ArcPoints; j++) {
			p1.X2 = p1.X1 + this->scale_X * pArc->R * cos(-pArc->Start * this->deg2Rad - (float)j * da) * this->coef_X;
			p1.Y2 = p1.Y1 + this->scale_Y * pArc->R * sin(-pArc->Start * this->deg2Rad - (float)j * da) * this->coef_Y;

			if (pArc->XYZ.X < 0) {
				p1.X2 = 2 * p1.X1 - p1.X2;
			}
			if (this->ccAngle != 0) {
				p2 = TLine((this->spX - this->rect.Left) * this->coef_X, (this->rect.Top - this->sPoint.Y) * this->coef_Y, p1.X2, p1.Y2);
				p2.Rotate(-this->ccAngle * this->deg2Rad);
			}
			else {
				p2 = p1;
			}
			if (j == 0) {
				Dst->MoveTo(p2.X2, p2.Y2);
			}
			else {
				Dst->LineToAS(p2.X2, p2.Y2);
			}
		}
//		// InRectObjects+=(int)DstRect.Contains(Types::TPoint(p1.X1, p1.Y1));
		this->inRectObjects++;
	}
}
//------------------------------------------------------------------------------

// -------- DrawLine -----------------------------------------------------------
void __fastcall TDbDXFEntityRender::DrawLine(TDXFLine *pLine) {
	TColor32 color = this->CalculateColor(pLine);
	TLine p1, p2;

	p1 = TLine((this->ePoint.X - this->rect.Left) * this->coef_X, (this->rect.Top - this->ePoint.Y) * this->coef_Y,
		(int)((this->spX + this->scale_X * pLine->XYZ.X - this->rect.Left) * this->coef_X),
		(int)((this->rect.Top - this->scale_Y * pLine->XYZ.Y - this->sPoint.Y) * this->coef_Y));

	p2 = TLine((this->ePoint.X - this->rect.Left) * this->coef_X, (this->rect.Top - this->ePoint.Y) * this->coef_Y,
		(int)((this->spX + this->scale_X * pLine->Point2.X - this->rect.Left) * this->coef_X),
		(int)((this->rect.Top - this->scale_Y * pLine->Point2.Y - this->sPoint.Y) * this->coef_Y));

	if (this->ccAngle != 0.0) {
		p1.Rotate(-this->ccAngle * this->deg2Rad);
		p2.Rotate(-this->ccAngle * this->deg2Rad);
	}

	if (this->dstRect.Contains(Types::TRect(p1.X2, p1.Y2, p2.X2, p2.Y2))) {
		this->Dst->LineAS(p1.X2, p1.Y2, p2.X2, p2.Y2, color, true);
		// InRectObjects+=(int)DstRect.Contains(Types::TPoint(p1.X2, p1.Y2));
		this->inRectObjects++;
	}
}
//------------------------------------------------------------------------------

// -------- DrawCircle ---------------------------------------------------------
void __fastcall TDbDXFEntityRender::DrawCircle(TDXFCircle *pCircle) {
	TColor32 color = this->CalculateColor(pCircle);
	TLine p1;
	double da, db;

	da = M_PI2 /(double)CirclePoints;
	p1.X1 = (this->spX + pCircle->XYZ.X - this->rect.Left) * this->coef_X;
	p1.Y1 = (this->rect.Top - pCircle->XYZ.Y - this->sPoint.Y) * this->coef_Y;
	p1.X2 = (this->spX + pCircle->XYZ.X + this->scale_X * pCircle->R - this->rect.Left) * this->coef_X; // p1.X1+Scale_x*circle->R*coef_X;//
	p1.Y2 = p1.Y1; // (rct.Top-circle->XYZ.Y-sp.Y)*coef_Y;//

	if (this->dstRect.Contains(Types::TRect(p1.X1 - p1.Length(), p1.Y1 - p1.Length(), p1.X1 + p1.Length(), p1.Y1 + p1.Length()))) {
		this->Dst->PenColor = color;
		this->Dst->MoveTo(p1.X2, p1.Y2);

		if (p1.Length()< 1.) {
			Dst->LineToS(p1.X2, p1.Y2);
		}
		else {
			for(int j = 0; j < CirclePoints; j++) {
				p1.Rotate(da);
				this->Dst->LineToAS(p1.X2, p1.Y2);
			}
		}
//		// InRectObjects+=(int)DstRect.Contains(Types::TPoint(p1.X1, p1.Y1));
		this->inRectObjects++;
	}
}
//------------------------------------------------------------------------------

// -------- DrawEllipse --------------------------------------------------------
void __fastcall TDbDXFEntityRender::DrawEllipse(TDXFEllipse *pEllipse) {
	TColor32 color = this->CalculateColor(pEllipse);
	TLine p1, p2;
	TDouble3DPoint p3d;
	double da, db, xa, Ra, Rb;

	da = pEllipse->Stop - pEllipse->Start;
	if (da > M_PI2 || da < -M_PI2) {
		da = M_PI2;
	}
	else if (da < 0) {
		da += M_PI2;
	}

	p3d = pEllipse->XYZ + pEllipse->MjEndPoint;
	db = pEllipse->XYZ.Angle(p3d);
	da /= (double)EllipsePoints;
	Ra = pEllipse->XYZ.Radius(p3d);
	Rb = Ra * pEllipse->Mn2MjRatio;

	xa = pEllipse->XYZ.X; // fabs(ellipse->XYZ.X);

	p1.X1 = (this->spX + xa - this->rect.Left) * this->coef_X;
	p1.Y1 = (this->rect.Top - pEllipse->XYZ.Y - this->sPoint.Y) * this->coef_Y;
	p1.X2 = p1.X1 + this->scale_X * (Ra + Rb) * this->coef_X;
	p1.Y2 = p1.Y1 + this->scale_Y * (Ra + Rb) * this->coef_Y;

	if (this->dstRect.Contains(Types::TRect(p1.X1 - p1.Length(), p1.Y1 - p1.Length(), p2.X2, p2.Y2))) {
		this->Dst->PenColor = color;

		for (int j = 0; j <= EllipsePoints; j++) {
			p1.X2 = p1.X1 + this->scale_X * Ra * cos(pEllipse->Start + (float)j * da) * this->coef_X;
			p1.Y2 = p1.Y1 + this->scale_Y * Rb * sin(pEllipse->Start + (float)j * da) * this->coef_Y;
			p1.Rotate(-db);

			if (this->ccAngle != 0) {
				p2 = TLine((this->spX - this->rect.Left) * this->coef_X, (this->rect.Top - this->sPoint.Y) * this->coef_Y, p1.X2, p1.Y2);
				p2.Rotate(-this->ccAngle * this->deg2Rad);
			}
			else {
				p2 = p1;
			}
			if (j == 0) {
				Dst->MoveTo(p2.X2, p2.Y2);
			}
			else {
				Dst->LineToAS(p2.X2, p2.Y2);
			}
		}
//		// InRectObjects+=(int)DstRect.Contains(Types::TPoint(p1.X1, p1.Y1));
		this->inRectObjects++;
	}
}
//------------------------------------------------------------------------------

// -------- DrawPoint ----------------------------------------------------------
void __fastcall TDbDXFEntityRender::DrawPoint(TDXFPoint *pPoint) {
	TColor32 color = this->CalculateColor(pPoint);
	TLine p1, p2;

	p1.X1 = (this->spX + pPoint->XYZ.X - this->rect.Left) * this->coef_X;
	p1.Y1 = (this->rect.Top - pPoint->XYZ.Y - this->sPoint.Y) * this->coef_Y;

	if (this->ccAngle != 0) {
		p2 = TLine((this->spX - this->rect.Left) * this->coef_X, (this->rect.Top - this->sPoint.Y) * this->coef_Y, p1.X2, p1.Y2);
		p2.Rotate(-this->ccAngle * this->deg2Rad);
		p1.X1 = p2.X2;
		p1.Y1 = p2.Y2;
	}

	if (this->dstRect.Contains(Types::TPoint(p1.X1, p1.Y1))) {
		this->Dst->SetPixelT(p1.X1, p1.Y1, color);
//		// InRectObjects+=(int)DstRect.Contains(Types::TPoint(p1.X1, p1.Y1));
		this->inRectObjects++;
	}
}
//------------------------------------------------------------------------------

// -------- DrawText -----------------------------------------------------------
void __fastcall TDbDXFEntityRender::DrawText(TDXFText *pText) {
	TColor32 color = this->CalculateColor(pText);
	TText32 *text32 = new TText32();
	TFontStyles st;
	double text_sz = 1.5 * pText->Height * this->coef_X;

	if (text_sz < 1.5) text_sz = 1.5;

	TTrueTypeFont *font = new TTrueTypeFont((TFontName)"Courier New", text_sz, st, (TFontCharset)0);
	double tw, th;
	int x, y;

	try {
		x = (this->spX + pText->XYZ.X - this->rect.Left) * this->coef_X;
		y = (this->rect.Top - pText->XYZ.Y - this->sPoint.Y) * this->coef_Y;
		tw = text32->GetTextWidth(pText->Text, font);
		th = text32->GetTextHeight(pText->Text, font);

		if (th >= 1 && tw >= 1) {
			text32->Angle = pText->Angle;
			text32->Draw(this->Dst, x, y, pText->Text, font, color);
		}
		this->inRectObjects +=(int)this->dstRect.Contains(Types::TPoint(x, y));
	}
	__finally {
		delete text32;
		delete font;
	}
}
//------------------------------------------------------------------------------

// -------- CalculateColor -----------------------------------------------------
TColor32 __fastcall TDbDXFEntityRender::CalculateColor(TDXFEntity *pEntity) {
	TColor32 color = pEntity->ColorPX;
	unsigned char r, g, b;

	r = (((color & 0x00FF0000) >> 16) - ((this->bgColor & 0x00FF0000) >> 16));
	g = (((color & 0x0000FF00) >> 8) - ((this->bgColor & 0x0000FF00) >> 8));
	b = ((color & 0x000000FF) - (this->bgColor & 0x000000FF));

	if ((r < this->invertColor) && (g < this->invertColor) && (b < this->invertColor)) {
		color = Color32(255 - ((color & 0x00FF0000) >> 16), 255 - ((color & 0x0000FF00) >> 8), 255 - (color &0x000000FF), (color & 0xFF000000) >> 24);
	}

	return color;
}
//------------------------------------------------------------------------------

bool __fastcall TDbDXFEntityRender::Render(TDXFEntity *entity)
//	TDouble3DPoint SourcePoint, double Scale_x, double Scale_y, double angle)
{
//	TDoubleRect rct = this->rect;

//	TDouble3DPoint SourcePoint = this->cc_data->XYZ;
//	double Scale_x = this->ccData->Scale_X;
//	double Scale_y = this->ccData->Scale_Y;
//	double angle = this->ccData->Angle;

	int w, h, i, k, x, y;
//	TDXFEntity *entity;
	TDXFLine *line;
	TDXFPolyLine *pline;
	TDXFLwPolyLine *lwpline;
	TDXFSpline *spline;
	TDXFHatch *hatch;
	TDXFVertex *vertex;
	TDXFInsert *insert;
	TDXFCircle *circle;
	TDXFArc *arc;
	TDXFEllipse *ellipse;
	TDXFPoint *point;
	TDXFText *text;
	TText32 *text32;
	TTrueTypeFont *font;
//	double coef_X, coef_Y, text_sz, Ra, Rb, tw, th;
	double text_sz, Ra, Rb, tw, th;
//	TDouble3DPoint sp = SourcePoint, ep;
	TDouble3DPoint ep;
	TLine p1, p2;
	TFontStyles st;
//	double t_Scale_x = t_Scale_x, t_Scale_y = Scale_y, t_angle = angle;
	double da, db, xa; //, Deg2Rad;
	TColor32 color;
//	unsigned char r, g, b;
	double spX;
	TDouble3DPoint p3d;
	TDoublePoint* pdp;
	bool bl;
	int InRectObjects=0;
	Types::TRect DstRect;

	spX = fabs(this->sPoint.X);
	//spX=sp.X;
	if (spX < this->rect.Left) ep.X = this->rect.Left;
	else if (spX > this->rect.Right) ep.X = this->rect.Right;
	else ep.X = spX;
	if (this->sPoint.Y < this->rect.Bottom) ep.Y = this->rect.Bottom;
	else if (this->sPoint.Y > this->rect.Top) ep.Y = this->rect.Top;
	else ep.Y = this->sPoint.Y;

	//angle+=45;

//	Deg2Rad=M_PI/180.;

	try
	{
		if (this->Dst)
		{
//			w = this->Dst->Width;
//			h = this->Dst->Height;
			DstRect = this->Dst->BoundsRect();
//			coef_X = (float)w/fabs(this->rect.Width());
//			coef_Y = (float)h/fabs(this->rect.Height());

//			for(i=0; i<list->Count; i++)
//			{
//				entity=(TDXFEntity*)list->Items[i];

//				color=entity->ColorPX;
//				r=abs(((color&0x00FF0000)>>16)-((bgColor&0x00FF0000)>>16));
//				g=abs(((color&0x0000FF00)>>8)-((bgColor&0x0000FF00)>>8));
//				b=abs((color&0x000000FF)-(bgColor&0x000000FF));
//				if(r<InvertColor && g<InvertColor && b<InvertColor)
//				{
//					color=Color32(255-((color&0x00FF0000)>>16), 255-((color&0x0000FF00)>>8), 255-(color&0x000000FF), (color&0xFF000000)>>24);
//				}

				color=this->CalculateColor(entity);

				switch(entity->Type)
				{
					case detLine:
						line=(TDXFLine*)entity;
						p1=TLine((ep.X-this->rect.Left)*this->coef_X, (this->rect.Top-ep.Y)*this->coef_Y, (int)((spX+this->scale_X*line->XYZ.X-this->rect.Left)*this->coef_X), (int)((this->rect.Top-this->scale_Y*line->XYZ.Y - this->sPoint.Y)*this->coef_Y));
						p2=TLine((ep.X-this->rect.Left)*this->coef_X, (this->rect.Top-ep.Y)*this->coef_Y, (int)((spX+this->scale_X*line->Point2.X-this->rect.Left)*this->coef_X), (int)((this->rect.Top-this->scale_Y*line->Point2.Y - this->sPoint.Y)*this->coef_Y));
						if (this->ccAngle != 0.0) {
							p1.Rotate(-this->ccAngle * this->deg2Rad);
							p2.Rotate(-this->ccAngle * this->deg2Rad);
						}
						if(DstRect.Contains(Types::TRect(p1.X2, p1.Y2, p2.X2, p2.Y2)))
						{
							this->Dst->LineAS(p1.X2, p1.Y2, p2.X2, p2.Y2, color, true);
							//InRectObjects+=(int)DstRect.Contains(Types::TPoint(p1.X2, p1.Y2));
							InRectObjects++;
						}
						break;
					case detCircle:
						circle = (TDXFCircle*)entity;
						da = M_PI2/(double)CirclePoints;
						p1.X1 = (spX + circle->XYZ.X - this->rect.Left)*this->coef_X;
						p1.Y1 = (this->rect.Top - circle->XYZ.Y - this->sPoint.Y)*this->coef_Y;
						p1.X2 = (spX + circle->XYZ.X + this->scale_X*circle->R - this->rect.Left)*this->coef_X;//p1.X1+Scale_x*circle->R*coef_X;//
						p1.Y2 = p1.Y1;//(rct.Top-circle->XYZ.Y-sp.Y)*coef_Y;//
						if (DstRect.Contains(Types::TRect(p1.X1-p1.Length(), p1.Y1-p1.Length(), p2.X2, p2.Y2)))
						{
							this->Dst->PenColor = color;
							this->Dst->MoveTo(p1.X2, p1.Y2);

							if (p1.Length() < 1.) this->Dst->LineToS(p1.X2, p1.Y2);
							else for (int j = 0; j < CirclePoints; j++)
							{
								p1.Rotate(da);
								this->Dst->LineToAS(p1.X2, p1.Y2);
							}
							//InRectObjects+=(int)DstRect.Contains(Types::TPoint(p1.X1, p1.Y1));
							InRectObjects++;
						}
						break;
					case detPoint:
						point = (TDXFPoint*)entity;
						p1.X1 = (spX + point->XYZ.X - this->rect.Left)*this->coef_X;
						p1.Y1 = (this->rect.Top - point->XYZ.Y - this->sPoint.Y)*this->coef_Y;
						if (DstRect.Contains(Types::TPoint(p1.X1, p1.Y1)))
						{
							this->Dst->SetPixelT(p1.X1, p1.Y1, color);
							//InRectObjects+=(int)DstRect.Contains(Types::TPoint(p1.X1, p1.Y1));
							InRectObjects++;
						}
						break;
					case detArc:
						arc = (TDXFArc*)entity;
						da = (arc->Stop-arc->Start) * this->deg2Rad;
						if (da > M_PI2 || da < -M_PI2) da = M_PI2;
						else if (da < 0) da += M_PI2;
						da /= (double)ArcPoints;
						xa = fabs(arc->XYZ.X);
						p1.X1 = (spX + xa - this->rect.Left)*this->coef_X;
						p1.Y1 = (this->rect.Top - arc->XYZ.Y - this->sPoint.Y)*this->coef_Y;
						p1.X2 = (spX + arc->XYZ.X + this->scale_X*arc->R - this->rect.Left)*this->coef_X;//p1.X1+Scale_x*arc->R*coef_X;
						p1.Y2 = p1.Y1;//(rct.Top-arc->XYZ.Y-sp.Y)*coef_Y;//
						if (DstRect.Contains(Types::TRect(p1.X1-p1.Length(), p1.Y1-p1.Length(), p2.X2, p2.Y2)))
						{
							p1.Rotate(-arc->Start * this->deg2Rad);
							this->Dst->PenColor = color;
							//Dst->MoveTo(p1.X2, p1.Y2);
							for (int j = 0; j < ArcPoints; j++)
							{
								p1.X2 = p1.X1 + this->scale_X*arc->R*cos(-arc->Start * this->deg2Rad-(float)j*da)*this->coef_X;
								p1.Y2 = p1.Y1 + this->scale_Y*arc->R*sin(-arc->Start * this->deg2Rad-(float)j*da)*this->coef_Y;
								if (arc->XYZ.X <0 ) p1.X2 = 2*p1.X1 - p1.X2;
								if (this->ccAngle !=0 )
								{
									p2 = TLine((spX - this->rect.Left)*this->coef_X, (this->rect.Top - this->sPoint.Y)*this->coef_Y, p1.X2, p1.Y2);
									p2.Rotate(-this->ccAngle * this->deg2Rad);
								}
								else p2 = p1;
								if (j == 0) this->Dst->MoveTo(p2.X2, p2.Y2);
								else this->Dst->LineToAS(p2.X2, p2.Y2);
							}
							//InRectObjects+=(int)DstRect.Contains(Types::TPoint(p1.X1, p1.Y1));
							InRectObjects++;
						}
						break;
					case detEllipse:
						ellipse = (TDXFEllipse*)entity;
						da = ellipse->Stop-ellipse->Start;
						if (da > M_PI2 || da < -M_PI2) da = M_PI2;
						else if (da <0 ) da += M_PI2;
						p3d = ellipse->XYZ+ellipse->MjEndPoint;
						db = ellipse->XYZ.Angle(p3d);
						da /= (double)EllipsePoints;
						Ra = ellipse->XYZ.Radius(p3d);
						Rb = Ra*ellipse->Mn2MjRatio;

						xa = ellipse->XYZ.X; //fabs(ellipse->XYZ.X);

						p1.X1 = (spX + xa - this->rect.Left)*this->coef_X;
						p1.Y1 = (this->rect.Top - ellipse->XYZ.Y - this->sPoint.Y)*this->coef_Y;
						//p1.X2=p1.X1+Scale_x*Ra*cos(ellipse->Start)*coef_X;
						//p1.Y2=p1.Y1+Scale_y*Rb*sin(ellipse->Start)*coef_Y;
						p1.X2 = p1.X1 + this->scale_X*(Ra+Rb)*this->coef_X;
						p1.Y2 = p1.Y1 + this->scale_Y*(Ra+Rb)*this->coef_Y;
						if (DstRect.Contains(Types::TRect(p1.X1-p1.Length(), p1.Y1-p1.Length(), p2.X2, p2.Y2)))
						{
							this->Dst->PenColor = color;
							for (int j = 0; j <= EllipsePoints; j++)
							{
								p1.X2 = p1.X1 + this->scale_X*Ra*cos(ellipse->Start+(float)j*da)*this->coef_X;
								p1.Y2 = p1.Y1 + this->scale_Y*Rb*sin(ellipse->Start+(float)j*da)*this->coef_Y;
								p1.Rotate(-db);
								if (this->ccAngle != 0)
								{
									p2 = TLine((spX - this->rect.Left)*this->coef_X, (this->rect.Top - this->sPoint.Y)*this->coef_Y, p1.X2, p1.Y2);
									p2.Rotate(-this->ccAngle * this->deg2Rad);
								}
								else p2 = p1;
								if (j == 0) this->Dst->MoveTo(p2.X2, p2.Y2);
								else this->Dst->LineToAS(p2.X2, p2.Y2);
								//Dst->LineToS(p1.X2, p1.Y2);
							}
							//InRectObjects+=(int)DstRect.Contains(Types::TPoint(p1.X1, p1.Y1));
							InRectObjects++;
						}
						//Dst->LineToS(p1.X2+1, p1.Y2);
						break;
					case detText:
						text = (TDXFText*)entity;
						text32 = new TText32();
						text_sz = 1.5*text->Height*this->coef_X;
						if (text_sz < 1.5) text_sz = 1.5;
						font = new TTrueTypeFont((TFontName)"Courier New", text_sz, st, (TFontCharset)0);
						try
						{
							x = (spX + text->XYZ.X - this->rect.Left)*this->coef_X;
							y = (this->rect.Top - text->XYZ.Y - this->sPoint.Y)*this->coef_Y;
							tw = text32->GetTextWidth(text->Text, font);
							th = text32->GetTextHeight(text->Text, font);
							if (th >= 1 && tw >= 1)
							{
								text32->Angle = text->Angle;
								text32->Draw(Dst, x, y, text->Text, font, color);
							}
							InRectObjects += (int)DstRect.Contains(Types::TPoint(x, y));
						}
						__finally
						{
							delete text32;
							delete font;
						}
						break;
					case detInsert:
//						insert=(TDXFInsert*)entity;
//						if(insert && insert->Block)
//						{
//							/*if(insert->Block->Name=="APLAMP")
//							{
//								insert->Block->Render(Dst, bgColor, InvertColor, rct, insert->XYZ,
//									insert->X_scale, insert->Y_scale, insert->Angle);
//							}
//							else */
//							InRectObjects+=(int)insert->Block->Render(Dst, bgColor, InvertColor, rct, insert->XYZ,
//								insert->X_scale, insert->Y_scale, insert->Angle);
//						}
						break;
					case detPolyLine:
					case detLwPolyLine:
						if(entity->Type==detPolyLine) {pline=(TDXFPolyLine*)entity; k=pline->Count; bl=pline->Closed;}
						else if(entity->Type==detLwPolyLine) {lwpline=(TDXFLwPolyLine*)entity; k=lwpline->Count; bl=lwpline->Closed;}
						else k=0;
						Dst->PenColor=color;
						x=0;
						for(int j=0; j<k; j++)
						{
							if(entity->Type==detPolyLine)
							{
								vertex=pline->Vertexes[j];
								p1=TLine((ep.X-this->rect.Left)*this->coef_X, (this->rect.Top-ep.Y)*this->coef_Y, (int)((spX+this->scale_X*vertex->XYZ.X-this->rect.Left)*this->coef_X), (int)((this->rect.Top-this->scale_Y*vertex->XYZ.Y - this->sPoint.Y)*this->coef_Y));
							}
							else if(entity->Type==detLwPolyLine)
							{
								pdp=lwpline->Vertexes[j];
								if(pdp) p1=TLine((ep.X-this->rect.Left)*this->coef_X, (this->rect.Top-ep.Y)*this->coef_Y, (int)((spX+this->scale_X*pdp->X-this->rect.Left)*this->coef_X), (int)((this->rect.Top-this->scale_Y*pdp->Y - this->sPoint.Y)*this->coef_Y));
								else p1=TLine((ep.X-this->rect.Left)*this->coef_X, (this->rect.Top-ep.Y)*this->coef_Y, (ep.X-this->rect.Left)*this->coef_X, (this->rect.Top-ep.Y)*this->coef_Y);
							}
							else p1=TLine((ep.X-this->rect.Left)*this->coef_X, (this->rect.Top-ep.Y)*this->coef_Y, (ep.X-this->rect.Left)*this->coef_X, (this->rect.Top-ep.Y)*this->coef_Y);
							p1.Rotate(-this->ccAngle * this->deg2Rad);
							if(j==0)
							{
								Dst->MoveTo(p1.X2, p1.Y2);
								p2=p1;
							}
							else Dst->LineToS(p1.X2, p1.Y2);
							x+=(int)DstRect.Contains(Types::TPoint(p1.X2, p1.Y2));
						}
						InRectObjects+=(int)(x>(k>>1));
						if(k>0 && bl) this->Dst->LineToS(p2.X2, p2.Y2);
						break;
					case detSpline:
					case detHatch:
						//InRectObjects++;
						break;
				}
//			}
		}
	}
	catch(Exception &e)
	{
		InRectObjects=0;
	}
//	return (InRectObjects>0 && (InRectObjects>list->Count>>1));
	return (InRectObjects>0);
}

