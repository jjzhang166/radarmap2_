// -----------------------------------------------------------------------------
// DbDXFEntityRender.h
// -----------------------------------------------------------------------------

#ifndef DbDXFEntityRenderH
#define DbDXFEntityRenderH

#include "Defines.h"
#include "DXFMap.h"
#include "DbDxfStructures.h"

// -----------------------------------------------------------------------------
// TDbDXFEntityRender
// -----------------------------------------------------------------------------
class TDbDXFEntityRender {

private:
	TBitmap32 *Dst;
	TColor32 bgColor;
	int invertColor;
	TDoubleRect rect;
	double coef_X, coef_Y;
	Types::TRect dstRect;
	TDouble3DPoint sPoint, ePoint;
	float scale_X, scale_Y, scale_Z;
	float ccAngle; // degrees

	double deg2Rad;
	double spX;
	int inRectObjects;

	TColor32 __fastcall CalculateColor(TDXFEntity *pEntity);
	void __fastcall RecalcRenderParameters();

public:
	__fastcall TDbDXFEntityRender() {
		Dst = NULL;
		bgColor = clWindowText;
		invertColor = clRed32;
		scale_X = scale_Y = scale_Z = 1.0;
		ccAngle = 0.0;

		deg2Rad = M_PI / 180.;
		inRectObjects = 0;
	};
	__fastcall ~TDbDXFEntityRender() {};

	void __fastcall Init(TBitmap32 *pDst, TColor32 pBgColor, int pInvertColor, TDoubleRect pRect);
	void __fastcall DrawArc(TDXFArc *pArc);
	void __fastcall DrawCircle(TDXFCircle *pCircle);
	void __fastcall DrawEllipse(TDXFEllipse *pEllipse);
	void __fastcall DrawLine(TDXFLine *pLine);
	void __fastcall DrawPoint(TDXFPoint *pPoint);
	void __fastcall DrawText(TDXFText *pText);

	bool __fastcall Render(TDXFEntity *entity);

//	bool __fastcall Render(TDXFEntity *entity,
//		TDouble3DPoint SourcePoint, double Scale_x, double Scale_y, double angle);
	bool __fastcall Render(TDXFEntity *entity,  TBitmap32 *Dst, TColor32 bgColor, int InvertColor, TDoubleRect rct,
		TDouble3DPoint SourcePoint, double Scale_x, double Scale_y, double angle);

	void __fastcall SetCoordinateChangeData(CoordinateChangeData *CcData);

	__property int InRectObjects = {
		read = inRectObjects
	};
};
//---------------------------------------------------------------------------
#endif
