// -----------------------------------------------------------------------------
// CoordinateCorrector.cpp
// -----------------------------------------------------------------------------

#pragma hdrstop

#include "CoordinateCorrector.h"
#include "MyGraph.h"

// -----------------------------------------------------------------------------

#pragma package(smart_init)

// -------- Constructor / Destructor -------------------------------------------
__fastcall TCoordinateCorrector::~TCoordinateCorrector() {

	delete writerData;

	delete polyLineEntity;
	delete lineEntity;
	delete arcEntity;
	delete circleEntity;
	delete ellipseEntity;
	delete insertEntity;
	delete lwPolyLineEntity;
	delete pointEntity;
	delete textEntity;
	delete vertexEntity;
}
// -----------------------------------------------------------------------------

// -------- Init ---------------------------------------------------------------
void __fastcall TCoordinateCorrector::Init(TDXFCollection *pDxf, CoordinateChangeData *pChangeData) {

	if (pDxf->MinCorner && (pDxf->MinCorner->X != 0.0) && (pDxf->MinCorner->Y != 0.0)) {
		this->minCorner = *(pDxf->MinCorner);
	}
	else {
		this->minCorner.X = pDxf->LowerCorner.X;
		this->minCorner.Y = pDxf->LowerCorner.Y;
		this->minCorner.Z = 0.0;
	}

	if (pDxf->MaxCorner && (pDxf->MaxCorner->X != 0.0) && (pDxf->MaxCorner->Y != 0.0)) {
		this->maxCorner = *(pDxf->MaxCorner);
	}
	else {
		this->maxCorner.X = pDxf->UpperCorner.X;
		this->maxCorner.Y = pDxf->UpperCorner.Y;
		this->maxCorner.Z = 0.0;
	}

	this->ChangeData = pChangeData;
	this->writerData = new EntityWriterData();

	this->polyLineEntity = new TDXFPolyLine(NULL);
	this->lineEntity = new TDXFLine(NULL);
	this->arcEntity = new TDXFArc(NULL);
	this->circleEntity = new TDXFCircle(NULL);
	this->ellipseEntity = new TDXFEllipse(NULL);
	this->insertEntity = new TDXFInsert(NULL);
	this->lwPolyLineEntity = new TDXFLwPolyLine(NULL);
	this->pointEntity = new TDXFPoint(NULL);
	this->textEntity = new TDXFText(NULL);
	this->vertexEntity = new TDXFVertex(NULL);
}
// -----------------------------------------------------------------------------

// -------- CreateChangeData ---------------------------------------------------
CoordinateChangeData __fastcall TCoordinateCorrector::CreateChangeData(TDXFInsert *pInsert) {
	CoordinateChangeData result;

	result.XYZ = pInsert->XYZ;
	result.Scale_X = pInsert->X_scale;
	result.Scale_Y = pInsert->Y_scale;
	result.Scale_Z = pInsert->Z_scale;
	result.Angle = pInsert->Angle;

	return result;
}
// -----------------------------------------------------------------------------

// -------- CreateAsModifyChangeData -------------------------------------------
CoordinateChangeData __fastcall TCoordinateCorrector::CreateAsModifyChangeData(TDXFInsert *pInsert) {

	if (NULL == this->changeData) {
		return this->CreateChangeData(pInsert);
	}

	CoordinateChangeData result;

	result.XYZ = this->changeData->XYZ + pInsert->XYZ;
	result.Scale_X = this->changeData->Scale_X * pInsert->X_scale;
	result.Scale_Y = this->changeData->Scale_Y * pInsert->Y_scale;
	result.Scale_Z = this->changeData->Scale_Z * pInsert->Z_scale;
	result.Angle = this->changeData->Angle + pInsert->Angle;

	return result;
}
// -----------------------------------------------------------------------------

// -------- SetCoordinateChangeData --------------------------------------------
void __fastcall TCoordinateCorrector::SetCoordinateChangeData(CoordinateChangeData *value) {

	this->changeData = value;

	if (value) {
		this->RotatePoint.X = value->XYZ.X;
		this->RotatePoint.Y = value->XYZ.Y;
	}
	else {
		this->RotatePoint.X = this->minCorner.X;
		this->RotatePoint.Y = this->minCorner.Y;
	}
}
// -----------------------------------------------------------------------------

// -------- CalcRealCoordinate -------------------------------------------------
TDXFEntity *__fastcall TCoordinateCorrector::CalcRealCoordinate(TDXFEntity *pEntity, CoordinateChangeData *pCorrectData) {

	switch(pEntity->Type) {
	case detLine:
		TCoordinateCorrector::ChangeLineCoordinate(dynamic_cast<TDXFLine*>(pEntity), pCorrectData);
		break;

	case detCircle:
		TCoordinateCorrector::ChangeCircleCoordinate(dynamic_cast<TDXFCircle*>(pEntity), pCorrectData);
		break;

	case detArc:
		TCoordinateCorrector::ChangeArcCoordinate(dynamic_cast<TDXFArc*>(pEntity), pCorrectData);
		break;

	case detEllipse:
		TCoordinateCorrector::ChangeEllipseCoordinate(dynamic_cast<TDXFEllipse*>(pEntity), pCorrectData);
		break;

	case detPoint:
		TCoordinateCorrector::ChangePointCoordinate(dynamic_cast<TDXFPoint*>(pEntity), pCorrectData);
		break;

	case detText:
		break;

	case detInsert:
		break;

	case detPolyLine:
		break;

	case detLwPolyLine:
		break;

	case detVertex:
		TCoordinateCorrector::ChangeVertexCoordinate(dynamic_cast<TDXFVertex*>(pEntity), pCorrectData);
		break;
	}

	return pEntity;
}
// -----------------------------------------------------------------------------

// -------- CloneWithRealCoordinate --------------------------------------------
EntityWriterData *__fastcall TCoordinateCorrector::CloneWithRealCoordinate(TDXFEntity *pEntity) { // TDouble3DPoint *pPointsArray, int pCount) {
	TDXFEntity *clone = this->CloneEntity(pEntity);
	EntityWriterData *write_data = this->writerData;

	write_data->Entity = clone;
	write_data->ChangeData = this->changeData;
	write_data->Min_X = 0.0;
	write_data->Min_Y = 0.0;
	write_data->Max_X = 0.0;
	write_data->Max_Y = 0.0;

// FIXME - Debug
//	if (pEntity->Handle == 155114) {
//	   int a = 0;
//	   if (a == 1) {
//				  int b = 0;
//	   }
//	}

	switch(pEntity->Type) {
	case detLine:
		this->ChangeLineCoordinate(dynamic_cast<TDXFLine*>(pEntity), dynamic_cast<TDXFLine*>(clone), this->changeData);
		this->CalcLineMinMaxValue(write_data);
		write_data->Entity = pEntity;
		break;

	case detCircle:
		this->ChangeCircleCoordinate(dynamic_cast<TDXFCircle*>(pEntity), dynamic_cast<TDXFCircle*>(clone), this->changeData);
		this->CalcCircleMinMaxValue(write_data);
		write_data->Entity = pEntity;
		break;

	case detArc:
		this->ChangeArcCoordinate(dynamic_cast<TDXFArc*>(pEntity), dynamic_cast<TDXFArc*>(clone), this->changeData);
		this->CalcArcMinMaxValue(write_data);
		write_data->Entity = pEntity;
		break;

	case detEllipse:
		this->ChangeEllipseCoordinate(dynamic_cast<TDXFEllipse*>(pEntity), dynamic_cast<TDXFEllipse*>(clone), this->changeData);
		this->CalcEllipseMinMaxValue(write_data);
		write_data->Entity = pEntity;
		break;

	case detPoint:
		this->ChangePointCoordinate(dynamic_cast<TDXFPoint*>(pEntity), dynamic_cast<TDXFPoint*>(clone), this->changeData);
		this->CalcPointMinMaxValue(write_data);
		write_data->Entity = pEntity;
		break;

	case detText:
		this->ChangeTextCoordinate(dynamic_cast<TDXFText*>(pEntity), dynamic_cast<TDXFText*>(clone), this->changeData);
		this->CalcMinMaxValueText(write_data);
		write_data->Entity = pEntity;
		break;

	case detInsert:
		this->ChangeInsertCoordinate(dynamic_cast<TDXFInsert*>(pEntity), dynamic_cast<TDXFInsert*>(clone), this->changeData);
		write_data->Entity = pEntity;
		break;

	case detPolyLine:
		break;

	case detLwPolyLine:
		break;

	case detVertex:
		this->ChangeVertexCoordinate(dynamic_cast<TDXFVertex*>(pEntity), dynamic_cast<TDXFVertex*>(clone), this->changeData);
		this->CalcVertexMinMaxValue(write_data);
		write_data->Entity = pEntity;
		break;
	}

	return write_data;
}
// -----------------------------------------------------------------------------

// -------- CloneEntity --------------------------------------------------------
TDXFEntity * __fastcall TCoordinateCorrector::CloneEntity(TDXFEntity *pEntity) {
	TDXFEntity *clone = NULL;
	TDXFPolyLine *clone_poly_line = NULL;
	TDXFLwPolyLine *clone_lw_poly_line = NULL;

	switch(pEntity->Type) {
	case detLine:
		clone = this->lineEntity;
		clone->Import(pEntity);
		break;

	case detCircle:
		clone = this->circleEntity;
		clone->Import(pEntity);
		break;

	case detArc:
		clone = this->arcEntity;
		clone->Import(pEntity);
		break;

	case detEllipse:
		clone = this->ellipseEntity;
		clone->Import(pEntity);
		break;

	case detPoint:
		clone = this->pointEntity;
		clone->Import(pEntity);
		break;

	case detText:
		clone = this->textEntity;
		clone->Import(pEntity);
		break;

	case detInsert:
		clone = this->insertEntity;
		clone->Import(pEntity);
		break;

	case detPolyLine:
		clone_poly_line = this->polyLineEntity;
		clone = clone_poly_line;

		this->FillEntity(clone_poly_line, pEntity);

		clone_poly_line->Closed =((TDXFPolyLine*)pEntity)->Closed;
		clone_poly_line->Polyline3D =((TDXFPolyLine*)pEntity)->Polyline3D;
		clone_poly_line->Thickness3D =((TDXFPolyLine*)pEntity)->Thickness3D;
		break;

	case detLwPolyLine:
		clone_lw_poly_line = this->lwPolyLineEntity;
		clone = clone_lw_poly_line;

		this->FillEntity(clone_lw_poly_line, pEntity);

		clone_lw_poly_line->Closed =((TDXFLwPolyLine*)pEntity)->Closed;
		clone_lw_poly_line->Thickness3D =((TDXFLwPolyLine*)pEntity)->Thickness3D;
		clone_lw_poly_line->Plinegen =((TDXFLwPolyLine*)pEntity)->Plinegen;
		clone_lw_poly_line->Width =((TDXFLwPolyLine*)pEntity)->Width;
		break;

	case detVertex:
		clone = this->vertexEntity;
		clone->Import(pEntity);
		break;
	}

	return clone;
}
// -----------------------------------------------------------------------------

// -------- FillEntity ---------------------------------------------------------
TDXFEntity * __fastcall TCoordinateCorrector::FillEntity(TDXFEntity *pTrgEntity, TDXFEntity *pSrcEntity) {

	pTrgEntity->XYZ = pSrcEntity->XYZ;
	pTrgEntity->Handle = pSrcEntity->Handle;
	pTrgEntity->TextHeightCoef = pSrcEntity->TextHeightCoef;
	pTrgEntity->Name = pSrcEntity->Name;
	pTrgEntity->Description = pSrcEntity->Description;
	pTrgEntity->Comment = pSrcEntity->Comment;
	pTrgEntity->Flags = pSrcEntity->Flags;

	return pTrgEntity;
}
// -----------------------------------------------------------------------------

// -------- ChangeArcCoordinate ------------------------------------------------
void __fastcall TCoordinateCorrector::ChangeArcCoordinate(TDXFArc *pSrcArc, TDXFArc *pTrgArc, CoordinateChangeData *pCorrectData) {

	if (pCorrectData) {
		pTrgArc->XYZ.X = pCorrectData->Scale_X * pSrcArc->XYZ.X + pCorrectData->XYZ.X;
		pTrgArc->XYZ.Y = pCorrectData->Scale_Y * pSrcArc->XYZ.Y + pCorrectData->XYZ.Y;
		pTrgArc->XYZ.Z = pCorrectData->Scale_Z * pSrcArc->XYZ.Z + pCorrectData->XYZ.Z;

		if (pCorrectData->Angle != 0.0) {
			TFloatLine f_line = TFloatLine(this->RotatePoint.X, this->RotatePoint.Y, pTrgArc->XYZ.X, pTrgArc->XYZ.Y);
			double angle = pCorrectData->Angle * M_PI / 180.0;

			f_line.Rotate(angle);
			pTrgArc->XYZ.X = f_line.X1;
			pTrgArc->XYZ.Y = f_line.Y1;
		}
	}
}
// -----------------------------------------------------------------------------

// -------- ChangeArcCoordinate ------------------------------------------------
void __fastcall TCoordinateCorrector::ChangeArcCoordinate(TDXFArc *pArc, CoordinateChangeData *pCorrectData) {

	if (pCorrectData) {

		if (pCorrectData->Angle != 0.0) {
			TFloatLine f_line = TFloatLine(0.0, 0.0, pArc->XYZ.X, pArc->XYZ.Y);
			double angle = pCorrectData->Angle * M_PI / 180.0;

			f_line.Rotate(angle);
			pArc->XYZ.X = f_line.X1;
			pArc->XYZ.Y = f_line.Y1;
		}

		pArc->XYZ.X = pCorrectData->Scale_X * pArc->XYZ.X + pCorrectData->XYZ.X;
		pArc->XYZ.Y = pCorrectData->Scale_Y * pArc->XYZ.Y + pCorrectData->XYZ.Y;
		pArc->XYZ.Z = pCorrectData->Scale_Z * pArc->XYZ.Z + pCorrectData->XYZ.Z;
	}
}
// -----------------------------------------------------------------------------

// -------- CalcCircleMinMaxValue ----------------------------------------------
void __fastcall TCoordinateCorrector::CalcArcMinMaxValue(EntityWriterData *pWriteData) {
	TDXFArc *arc = (TDXFArc*)pWriteData->Entity;
	CoordinateChangeData *cc_data = pWriteData->ChangeData;

	if (cc_data) {
		pWriteData->Min_X = arc->XYZ.X - cc_data->Scale_X * arc->R;
		pWriteData->Max_X = arc->XYZ.X + cc_data->Scale_X * arc->R;

		pWriteData->Min_Y = arc->XYZ.Y - cc_data->Scale_Y * arc->R;
		pWriteData->Max_Y = arc->XYZ.Y + cc_data->Scale_Y * arc->R;
	}
	else {
		pWriteData->Min_X = arc->XYZ.X - arc->R;
		pWriteData->Max_X = arc->XYZ.X + arc->R;

		pWriteData->Min_Y = arc->XYZ.Y - arc->R;
		pWriteData->Max_Y = arc->XYZ.Y + arc->R;
	}
}

// -------- ChangeCircleCoordinate ---------------------------------------------
void __fastcall TCoordinateCorrector::ChangeCircleCoordinate(TDXFCircle *pSrcCircle, TDXFCircle *pTrgCircle,
	CoordinateChangeData *pCorrectData) {

	if (pCorrectData) {
		pTrgCircle->XYZ.X = pCorrectData->Scale_X * pSrcCircle->XYZ.X + pCorrectData->XYZ.X;
		pTrgCircle->XYZ.Y = pCorrectData->Scale_Y * pSrcCircle->XYZ.Y + pCorrectData->XYZ.Y;
		pTrgCircle->XYZ.Z = pCorrectData->Scale_Z * pSrcCircle->XYZ.Z + pCorrectData->XYZ.Z;

		if (pCorrectData->Angle != 0.0) {
			TFloatLine f_line = TFloatLine(this->RotatePoint.X, this->RotatePoint.Y, pTrgCircle->XYZ.X, pTrgCircle->XYZ.Y);
			double angle = pCorrectData->Angle * M_PI / 180.0;

			f_line.Rotate(angle);
			pTrgCircle->XYZ.X = f_line.X1;
			pTrgCircle->XYZ.Y = f_line.Y1;
		}
	}
}
// -----------------------------------------------------------------------------

// -------- ChangeCircleCoordinate ---------------------------------------------
void __fastcall TCoordinateCorrector::ChangeCircleCoordinate(TDXFCircle *pCircle, CoordinateChangeData *pCorrectData) {

	if (pCorrectData) {

		if (pCorrectData->Angle != 0.0) {
			TFloatLine f_line = TFloatLine(0.0, 0.0, pCircle->XYZ.X, pCircle->XYZ.Y);
			double angle = pCorrectData->Angle * M_PI / 180.0;

			f_line.Rotate(angle);
			pCircle->XYZ.X = f_line.X1;
			pCircle->XYZ.Y = f_line.Y1;
		}

		pCircle->XYZ.X = pCorrectData->Scale_X * pCircle->XYZ.X + pCorrectData->XYZ.X;
		pCircle->XYZ.Y = pCorrectData->Scale_Y * pCircle->XYZ.Y + pCorrectData->XYZ.Y;
		pCircle->XYZ.Z = pCorrectData->Scale_Z * pCircle->XYZ.Z + pCorrectData->XYZ.Z;
	}
}
// -----------------------------------------------------------------------------

// -------- CalcCircleMinMaxValue ----------------------------------------------
void __fastcall TCoordinateCorrector::CalcCircleMinMaxValue(EntityWriterData *pWriteData) {
	TDXFCircle *circle = (TDXFCircle*)pWriteData->Entity;
	CoordinateChangeData *cc_data = pWriteData->ChangeData;

	if (cc_data) {
		pWriteData->Min_X = circle->XYZ.X - cc_data->Scale_X * circle->R;
		pWriteData->Max_X = circle->XYZ.X + cc_data->Scale_X * circle->R;

		pWriteData->Min_Y = circle->XYZ.Y - cc_data->Scale_Y * circle->R;
		pWriteData->Max_Y = circle->XYZ.Y + cc_data->Scale_Y * circle->R;
	}
	else {
		pWriteData->Min_X = circle->XYZ.X - circle->R;
		pWriteData->Max_X = circle->XYZ.X + circle->R;

		pWriteData->Min_Y = circle->XYZ.Y - circle->R;
		pWriteData->Max_Y = circle->XYZ.Y + circle->R;
	}
}
// -----------------------------------------------------------------------------

// -------- ChangeEllipseCoordinate --------------------------------------------
void __fastcall TCoordinateCorrector::ChangeEllipseCoordinate(TDXFEllipse *pSrcEllipse, TDXFEllipse *pTrgEllipse,
	CoordinateChangeData *pCorrectData) {

	if (pCorrectData) {
		pTrgEllipse->XYZ.X = pCorrectData->Scale_X * pSrcEllipse->XYZ.X + pCorrectData->XYZ.X;
		pTrgEllipse->XYZ.Y = pCorrectData->Scale_Y * pSrcEllipse->XYZ.Y + pCorrectData->XYZ.Y;
		pTrgEllipse->XYZ.Z = pCorrectData->Scale_Z * pSrcEllipse->XYZ.Z + pCorrectData->XYZ.Z;

		if (pCorrectData->Angle != 0.0) {
			double rotate_angle = pCorrectData->Angle * M_PI / 180.0;
			TFloatLine p1 = TFloatLine(pCorrectData->XYZ.X, pCorrectData->XYZ.Y, pTrgEllipse->XYZ.X, pTrgEllipse->XYZ.Y);

			p1.Rotate(rotate_angle);

			pTrgEllipse->XYZ.X = p1.X2;
			pTrgEllipse->XYZ.Y = p1.Y2;
		}
	}
}
// -----------------------------------------------------------------------------

// -------- ChangeEllipseCoordinate --------------------------------------------
void __fastcall TCoordinateCorrector::ChangeEllipseCoordinate(TDXFEllipse *pEllipse, CoordinateChangeData *pCorrectData) {

	if (pCorrectData) {

		if (pCorrectData->Angle != 0.0) {
			double rotate_angle = pCorrectData->Angle * M_PI / 180.0;
			TFloatLine p1 = TFloatLine(0.0, 0.0, pEllipse->XYZ.X, pEllipse->XYZ.Y);

			p1.Rotate(rotate_angle);

			pEllipse->XYZ.X = p1.X2;
			pEllipse->XYZ.Y = p1.Y2;
		}

		pEllipse->XYZ.X = pCorrectData->Scale_X * pEllipse->XYZ.X + pCorrectData->XYZ.X;
		pEllipse->XYZ.Y = pCorrectData->Scale_Y * pEllipse->XYZ.Y + pCorrectData->XYZ.Y;
		pEllipse->XYZ.Z = pCorrectData->Scale_Z * pEllipse->XYZ.Z + pCorrectData->XYZ.Z;
	}
}
// -----------------------------------------------------------------------------

// -------- CalcEllipseMinMaxValue ---------------------------------------------
void __fastcall TCoordinateCorrector::CalcEllipseMinMaxValue(EntityWriterData *pWriteData) {
	TDXFEllipse *ellipse = (TDXFEllipse*)pWriteData->Entity;
	CoordinateChangeData *cc_data = pWriteData->ChangeData;
	TDouble3DPoint p3d = ellipse->XYZ + ellipse->MjEndPoint;
	double Ra = ellipse->XYZ.Radius(p3d);
	double Rb = Ra * ellipse->Mn2MjRatio;
	double r = sqrt(Ra * Ra + Rb * Rb);

	if (cc_data) {
		pWriteData->Min_X = ellipse->XYZ.X - cc_data->Scale_X * r;
		pWriteData->Max_X = ellipse->XYZ.X + cc_data->Scale_X * r;

		pWriteData->Min_Y = ellipse->XYZ.Y - cc_data->Scale_Y * r;
		pWriteData->Max_Y = ellipse->XYZ.Y + cc_data->Scale_Y * r;
	}
	else {
		pWriteData->Min_X = ellipse->XYZ.X - r;
		pWriteData->Max_X = ellipse->XYZ.X + r;

		pWriteData->Min_Y = ellipse->XYZ.Y - r;
		pWriteData->Max_Y = ellipse->XYZ.Y + r;
	}
}
// -----------------------------------------------------------------------------


// -------- ChangeInsertCoordinate ---------------------------------------------
void __fastcall TCoordinateCorrector::ChangeInsertCoordinate(TDXFInsert *pSrcInsert, TDXFInsert *pTrgInsert,
	CoordinateChangeData *pCorrectData) {

	if (pCorrectData) {
		pTrgInsert->XYZ.X = pCorrectData->Scale_X * pSrcInsert->XYZ.X + pCorrectData->XYZ.X;
		pTrgInsert->XYZ.Y = pCorrectData->Scale_Y * pSrcInsert->XYZ.Y + pCorrectData->XYZ.Y;
		pTrgInsert->XYZ.Z = pCorrectData->Scale_Z * pSrcInsert->XYZ.Z + pCorrectData->XYZ.Z;
	}
}
// -----------------------------------------------------------------------------

// -------- ChangeLineCoordinate -----------------------------------------------
void __fastcall TCoordinateCorrector::ChangeLineCoordinate(TDXFLine *pSrcLine, TDXFLine *pTrgLine, CoordinateChangeData *pCorrectData) {

	if (pCorrectData) {
		pTrgLine->XYZ.X = pCorrectData->Scale_X * pSrcLine->XYZ.X + pCorrectData->XYZ.X;
		pTrgLine->XYZ.Y = pCorrectData->Scale_Y * pSrcLine->XYZ.Y + pCorrectData->XYZ.Y;
		pTrgLine->XYZ.Z = pCorrectData->Scale_Z * pSrcLine->XYZ.Z + pCorrectData->XYZ.Z;

		pTrgLine->Point2.X = pCorrectData->Scale_X * pSrcLine->Point2.X + pCorrectData->XYZ.X;
		pTrgLine->Point2.Y = pCorrectData->Scale_Y * pSrcLine->Point2.Y + pCorrectData->XYZ.Y;
		pTrgLine->Point2.Z = pCorrectData->Scale_Z * pSrcLine->Point2.Z + pCorrectData->XYZ.Z;

		if (pCorrectData->Angle != 0.0) {
			TFloatLine f_line = TFloatLine(this->RotatePoint.X, this->RotatePoint.Y, pTrgLine->XYZ.X, pTrgLine->XYZ.Y);
			double angle = pCorrectData->Angle * M_PI / 180.0;

			f_line.Rotate(angle);
			pTrgLine->XYZ.X = f_line.X2;
			pTrgLine->XYZ.Y = f_line.Y2;

			f_line = TFloatLine(this->RotatePoint.X, this->RotatePoint.Y, pTrgLine->Point2.X, pTrgLine->Point2.Y);
			f_line.Rotate(angle);
			pTrgLine->Point2.X = f_line.X2;
			pTrgLine->Point2.Y = f_line.Y2;
		}
	}
}
// -----------------------------------------------------------------------------

// -------- ChangeLineCoordinate -----------------------------------------------
void __fastcall TCoordinateCorrector::ChangeLineCoordinate(TDXFLine *pLine, CoordinateChangeData *pCorrectData) {

	if (pCorrectData) {
		if (pCorrectData->Angle != 0.0) {
			TFloatLine f_line = TFloatLine(0.0, 0.0, pLine->XYZ.X, pLine->XYZ.Y);
			double angle = pCorrectData->Angle * M_PI / 180.0;

			f_line.Rotate(angle);
			pLine->XYZ.X = f_line.X2;
			pLine->XYZ.Y = f_line.Y2;

//			f_line = TFloatLine(this->RotatePoint.X, this->RotatePoint.Y, pTrgLine->Point2.X, pTrgLine->Point2.Y);
			f_line.X2 = pLine->Point2.X;
			f_line.Y2 = pLine->Point2.Y;

			f_line.Rotate(angle);
			pLine->Point2.X = f_line.X2;
			pLine->Point2.Y = f_line.Y2;
		}
		pLine->XYZ.X = pCorrectData->Scale_X * pLine->XYZ.X + pCorrectData->XYZ.X;
		pLine->XYZ.Y = pCorrectData->Scale_Y * pLine->XYZ.Y + pCorrectData->XYZ.Y;
		pLine->XYZ.Z = pCorrectData->Scale_Z * pLine->XYZ.Z + pCorrectData->XYZ.Z;

		pLine->Point2.X = pCorrectData->Scale_X * pLine->Point2.X + pCorrectData->XYZ.X;
		pLine->Point2.Y = pCorrectData->Scale_Y * pLine->Point2.Y + pCorrectData->XYZ.Y;
		pLine->Point2.Z = pCorrectData->Scale_Z * pLine->Point2.Z + pCorrectData->XYZ.Z;
	}
}
// -----------------------------------------------------------------------------

// -------- CalcLineMinMaxValue ------------------------------------------------
void __fastcall TCoordinateCorrector::CalcLineMinMaxValue(EntityWriterData *pWriteData) {
	TDXFLine *line = (TDXFLine*)pWriteData->Entity;

	if (line->XYZ.X < line->Point2.X) {
		pWriteData->Min_X = line->XYZ.X;
		pWriteData->Max_X = line->Point2.X;
	}
	else {
		pWriteData->Min_X = line->Point2.X;
		pWriteData->Max_X = line->XYZ.X;
	}

	if (line->XYZ.Y < line->Point2.Y) {
		pWriteData->Min_Y = line->XYZ.Y;
		pWriteData->Max_Y = line->Point2.Y;
	}
	else {
		pWriteData->Min_Y = line->Point2.Y;
		pWriteData->Max_Y = line->XYZ.Y;
	}
}
// -----------------------------------------------------------------------------

// -------- ChangePointCoordinate ----------------------------------------------
void __fastcall TCoordinateCorrector::ChangePointCoordinate(TDXFPoint *pSrcPoint, TDXFPoint *pTrgPoint, CoordinateChangeData *pCorrectData)
{
	if (pCorrectData) {
		pTrgPoint->XYZ.X = pCorrectData->Scale_X * pSrcPoint->XYZ.X + pCorrectData->XYZ.X;
		pTrgPoint->XYZ.Y = pCorrectData->Scale_Y * pSrcPoint->XYZ.Y + pCorrectData->XYZ.Y;
		pTrgPoint->XYZ.Z = pCorrectData->Scale_Z * pSrcPoint->XYZ.Z + pCorrectData->XYZ.Z;

		if (pCorrectData->Angle != 0.0) {
			TFloatLine f_line = TFloatLine(this->RotatePoint.X, this->RotatePoint.Y, pTrgPoint->XYZ.X, pTrgPoint->XYZ.Y);
			double angle = pCorrectData->Angle * M_PI / 180.0;

			f_line.Rotate(angle);
			pTrgPoint->XYZ.X = f_line.X1;
			pTrgPoint->XYZ.Y = f_line.Y1;
		}
	}
}
// -----------------------------------------------------------------------------

// -------- ChangePointCoordinate ----------------------------------------------
void __fastcall TCoordinateCorrector::ChangePointCoordinate(TDXFPoint *pPoint, CoordinateChangeData *pCorrectData) {

	if (pCorrectData) {

		if (pCorrectData->Angle != 0.0) {
			TFloatLine f_line = TFloatLine(0.0, 0.0, pPoint->XYZ.X, pPoint->XYZ.Y);
			double angle = pCorrectData->Angle * M_PI / 180.0;

			f_line.Rotate(angle);
			pPoint->XYZ.X = f_line.X1;
			pPoint->XYZ.Y = f_line.Y1;
		}

		pPoint->XYZ.X = pCorrectData->Scale_X * pPoint->XYZ.X + pCorrectData->XYZ.X;
		pPoint->XYZ.Y = pCorrectData->Scale_Y * pPoint->XYZ.Y + pCorrectData->XYZ.Y;
		pPoint->XYZ.Z = pCorrectData->Scale_Z * pPoint->XYZ.Z + pCorrectData->XYZ.Z;
	}
}
// -----------------------------------------------------------------------------

// -------- CalcPointMinMaxValue -----------------------------------------------
void __fastcall TCoordinateCorrector::CalcPointMinMaxValue(EntityWriterData *pWriteData) {
	TDXFPoint *point = (TDXFPoint *)pWriteData->Entity;

	pWriteData->Min_X = point->XYZ.X;
	pWriteData->Max_X = point->XYZ.X;
	pWriteData->Min_Y = point->XYZ.Y;
	pWriteData->Max_Y = point->XYZ.Y;
}
// -----------------------------------------------------------------------------

// -------- ChangeTextCoordinate -----------------------------------------------
void __fastcall TCoordinateCorrector::ChangeTextCoordinate(TDXFText *pSrcText, TDXFText *pTrgText, CoordinateChangeData *pCorrectData) {

	if (pCorrectData) {
		pTrgText->XYZ.X = pCorrectData->Scale_X * pSrcText->XYZ.X + pCorrectData->XYZ.X;
		pTrgText->XYZ.Y = pCorrectData->Scale_Y * pSrcText->XYZ.Y + pCorrectData->XYZ.Y;
		pTrgText->XYZ.Z = pCorrectData->Scale_Z * pSrcText->XYZ.Z + pCorrectData->XYZ.Z;

		pTrgText->Angle = pSrcText->Angle;

		pTrgText->AlignmentPoint.X = pCorrectData->Scale_X * pSrcText->AlignmentPoint.X + pCorrectData->XYZ.X;
		pTrgText->AlignmentPoint.Y = pCorrectData->Scale_Y * pSrcText->AlignmentPoint.Y + pCorrectData->XYZ.Y;
		pTrgText->AlignmentPoint.Z = pCorrectData->Scale_Z * pSrcText->AlignmentPoint.Z + pCorrectData->XYZ.Z;

		if (pCorrectData->Angle != 0.0) {
			TFloatLine f_line = TFloatLine(this->RotatePoint.X, this->RotatePoint.Y, pTrgText->XYZ.X, pTrgText->XYZ.Y);
			double angle = pCorrectData->Angle * M_PI / 180.0;

			f_line.Rotate(angle);
			pTrgText->XYZ.X = f_line.X1;
			pTrgText->XYZ.Y = f_line.Y1;

			f_line = TFloatLine(this->RotatePoint.X, this->RotatePoint.Y, pTrgText->AlignmentPoint.X, pTrgText->AlignmentPoint.Y);

			f_line.Rotate(angle);
			pTrgText->AlignmentPoint.X = f_line.X1;
			pTrgText->AlignmentPoint.Y = f_line.Y1;
		}
	}
}
// -----------------------------------------------------------------------------

// -------- CalcMinMaxValueText ------------------------------------------------
void __fastcall TCoordinateCorrector::CalcMinMaxValueText(EntityWriterData *pWriteData) {
	TDXFText *text = (TDXFText *)pWriteData->Entity;
	double min_x = 0.0, max_x = 0.0, min_y = 0.0, max_y = 0.0;

	if (text->AlignmentPoint.X > text->XYZ.X) {
		min_x = text->XYZ.X;
		max_x = text->AlignmentPoint.X;
	} else {
		min_x = text->AlignmentPoint.X;
		max_x = text->XYZ.X;
	}

	if (text->AlignmentPoint.Y > text->XYZ.Y) {
		min_y = text->XYZ.Y;
		max_y = text->AlignmentPoint.Y;
	} else {
		min_y = text->AlignmentPoint.Y;
		max_y = text->XYZ.Y;
	}

	pWriteData->Min_X = min_x;
	pWriteData->Max_X = max_x;
	pWriteData->Min_Y = min_y;
	pWriteData->Max_Y = max_y;
}
// -----------------------------------------------------------------------------

// -------- ChangeVertexCoordinate ---------------------------------------------
void __fastcall TCoordinateCorrector::ChangeVertexCoordinate(TDXFVertex *pSrcVertex, TDXFVertex *pTrgVertex,
	CoordinateChangeData *pCorrectData) {

	if (pCorrectData) {
		pTrgVertex->XYZ.X = pCorrectData->Scale_X * pSrcVertex->XYZ.X + pCorrectData->XYZ.X;
		pTrgVertex->XYZ.Y = pCorrectData->Scale_Y * pSrcVertex->XYZ.Y + pCorrectData->XYZ.Y;
		pTrgVertex->XYZ.Z = pCorrectData->Scale_Z * pSrcVertex->XYZ.Z + pCorrectData->XYZ.Z;

		if (pCorrectData->Angle != 0.0) {
			TFloatLine f_line = TFloatLine(this->RotatePoint.X, this->RotatePoint.Y, pTrgVertex->XYZ.X, pTrgVertex->XYZ.Y);
			double angle = pCorrectData->Angle * M_PI / 180.0;

			f_line.Rotate(angle);
			pTrgVertex->XYZ.X = f_line.X1;
			pTrgVertex->XYZ.Y = f_line.Y1;
		}
	}
}
// -----------------------------------------------------------------------------

// -------- ChangeVertexCoordinate ---------------------------------------------
void __fastcall TCoordinateCorrector::ChangeVertexCoordinate(TDXFVertex *pVertex, CoordinateChangeData *pCorrectData) {

	if (pCorrectData) {

		if (pCorrectData->Angle != 0.0) {
			TFloatLine f_line = TFloatLine(0.0, 0.0, pVertex->XYZ.X, pVertex->XYZ.Y);
			double angle = pCorrectData->Angle * M_PI / 180.0;

			f_line.Rotate(angle);
			pVertex->XYZ.X = f_line.X1;
			pVertex->XYZ.Y = f_line.Y1;
		}

		pVertex->XYZ.X = pCorrectData->Scale_X * pVertex->XYZ.X + pCorrectData->XYZ.X;
		pVertex->XYZ.Y = pCorrectData->Scale_Y * pVertex->XYZ.Y + pCorrectData->XYZ.Y;
		pVertex->XYZ.Z = pCorrectData->Scale_Z * pVertex->XYZ.Z + pCorrectData->XYZ.Z;
	}
}
// -----------------------------------------------------------------------------

// -------- VertexCalcMinMaxValue ----------------------------------------------
void __fastcall TCoordinateCorrector::CalcVertexMinMaxValue(EntityWriterData *pWriteData) {
	TDXFVertex *vertex = (TDXFVertex *)pWriteData->Entity;

	pWriteData->Min_X = vertex->XYZ.X;
	pWriteData->Max_X = vertex->XYZ.X;
	pWriteData->Min_Y = vertex->XYZ.Y;
	pWriteData->Max_Y = vertex->XYZ.Y;
}
// -----------------------------------------------------------------------------
