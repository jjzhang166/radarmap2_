// -----------------------------------------------------------------------------
// UPPStructures.h
// -----------------------------------------------------------------------------

#ifndef UPPStructuresH
#define UPPStructuresH

//#include "DXFMap.h"
#include "Defines.h"
#include <vector>

// -----------------------------------------------------------------------------
// TUPPFavoriteObject
// -----------------------------------------------------------------------------

struct TUPPFavoriteObject {
	float mDistance;
	AnsiString mText;
	int Tag;
	TDouble3DPoint mFoXYZ;

	__fastcall TUPPFavoriteObject() {
		mFoXYZ = TDouble3DPoint();
		mDistance = 0;
		mText = "";
		Tag = 0;
	}
	__fastcall ~TUPPFavoriteObject() {}
};

class TDXFEntity;

struct TUPPFavoriteDXFObject : public TUPPFavoriteObject {
public:
	TDXFEntity *mEntity;
	int mLayerId;
	std::vector<TUPPFavoriteDXFObject *> mChildEntity;

	__fastcall TUPPFavoriteDXFObject() {
		mEntity = NULL;
		mLayerId = 0;
	}
	__fastcall ~TUPPFavoriteDXFObject() {
		std::vector<TUPPFavoriteDXFObject *>::iterator it;
		TUPPFavoriteDXFObject *object = NULL;

		for (it = this->mChildEntity.begin(); it != this->mChildEntity.end(); it++) {
			object = *it;

			if (object) {
				delete object;
			}
		}
		if (mEntity) {
			 delete mEntity;
		}
	}
};

struct TUPPFavoriteDbDXFObject : TUPPFavoriteDXFObject {

public:
	int mDbRecordId;
	std::vector<AnsiString> mExGroupText;

	__fastcall TUPPFavoriteDbDXFObject() {
		mDbRecordId = 0;
	}
	__fastcall ~TUPPFavoriteDbDXFObject() {}
};

// -----------------------------------------------------------------------------
// TUPPPoint
// -----------------------------------------------------------------------------
template <typename TUPPFavorite_Type> struct TUPPPoint {

public:
	TDouble3DPoint mXYZ;
	std::vector<TUPPFavorite_Type *> mFavorites;
	AnsiString mText;
	TDouble3DPoint mFoXYZ;
	float mDistance;
	float sRadius;

	__fastcall TUPPPoint() {
		mDistance = 0;
		sRadius = 0;
	}

	__fastcall ~TUPPPoint() {
		std::vector<TUPPFavorite_Type *>::iterator it;
		TUPPFavorite_Type *favorite = NULL;

		for (it = this->mFavorites.begin(); it != this->mFavorites.end(); it++) {
			favorite = *it;

			if (favorite) {
				delete favorite;
			}
		}
	}
};

//------------------------------------------------------------------------------
#endif
