// -----------------------------------------------------------------------------
// SQLiteTools.h
// -----------------------------------------------------------------------------

#ifndef SQLiteToolsH
#define SQLiteToolsH

//#include <System.hpp>
//#include <SysUtils.hpp>
#include <vcl.h>
#include "sqlite3.h"

// -----------------------------------------------------------------------------
// SQLiteConnection
// -----------------------------------------------------------------------------
class SQLiteConnection : public TObject {

private:
	friend class SQLiteDbError;
	friend class SQLiteTransaction;

	sqlite3 *db;
	SQLiteTransaction *transaction;

	void __fastcall CheckDbOpen();

protected:

public:

	__fastcall SQLiteConnection() {
		this->db = NULL;
		this->transaction = NULL;
	}
	__fastcall ~SQLiteConnection();

	void __fastcall Open(const char *dbName);
	void __fastcall Open(const AnsiString &dbName) {
        this->Open(dbName.c_str());
    }
	void __fastcall Close();

	int sqlite3_changes() {
		return ::sqlite3_changes(db);
	}

	int __fastcall GetAutocommit() {
		return ::sqlite3_get_autocommit(db);
	}
	long long __fastcall GetLastInsertId();

	void __fastcall SetBusyTimeout(int ms);

	const char* __fastcall ExecuteNonQuery(const char *sql);
	void __fastcall ExecuteNonQuery(const AnsiString &sql);
	int __fastcall ExecuteInt(const char *sql);
	int __fastcall ExecuteInt(const AnsiString &sql);
	long long __fastcall ExecuteInt64(const char *sql);
	long long __fastcall ExecuteInt64(const AnsiString &sql);
	double __fastcall ExecuteDouble(const char *sql);
	double __fastcall ExecuteDouble(const AnsiString &sql);
	AnsiString __fastcall ExecuteString(const char *sql);
	AnsiString __fastcall ExecuteString(const AnsiString &sql);

	AnsiString __fastcall GetDbName();

	void __fastcall BeginTransaction();
	void __fastcall Commit();
	void __fastcall Rollback();

	bool __fastcall IsBeginTransaction() {
		return (this->transaction) ?  true : false;
	};

	__property sqlite3 * DataBase = {
		read = db
	};
};

// -----------------------------------------------------------------------------
// SQLiteTransaction
// -----------------------------------------------------------------------------
class SQLiteTransaction {

private:
	SQLiteConnection *dbConnect;
	bool isBeginTransaction;

public:
	__fastcall SQLiteTransaction(SQLiteConnection *pConnect, bool start = true);
	__fastcall ~SQLiteTransaction();

	void __fastcall Begin();
	void __fastcall Commit();
	void __fastcall Rollback();

	__property bool IsBeginTransaction = {
		read = isBeginTransaction
	};
};

// -----------------------------------------------------------------------------
// SQLiteCommand
// -----------------------------------------------------------------------------
class SQLiteCommand {

private:
	friend class SQLiteReader;

	SQLiteConnection *dbConnect;
	struct sqlite3_stmt *stmt;

//	const char** columns;
	AnsiString* columns;

	unsigned int refsCount;
	int columnCount;
	bool noDataFound;

	void __fastcall CheckNoDataFound();
	void __fastcall FillColumnInfo();
	int __fastcall GetBindIndexByName(const char *prmName);

public:
	__fastcall SQLiteCommand(SQLiteConnection *pConnect, const char *sql);
	__fastcall SQLiteCommand(SQLiteConnection *pConnect, const AnsiString &sql);
	__fastcall ~SQLiteCommand();

	void __fastcall Reset();
	void __fastcall Bind(int index);
	void __fastcall Bind(int index, int data);
	void __fastcall Bind(int index, long long data);
	void __fastcall Bind(int index, double data);
	void __fastcall Bind(int index, const char *data, int datalen, sqlite3_destructor_type p_destructor_type);
	void __fastcall Bind(int index, const AnsiString &data, sqlite3_destructor_type p_destructor_type);
	void __fastcall Bind(int index, const void *data, int datalen, sqlite3_destructor_type p_destructor_type);

	void __fastcall BindNullByName(const char *prmName);

	void __fastcall BindIntByName(const char *prmName, int data);
	void __fastcall BindIntByName(const char *prmName, const AnsiString &data);

	void __fastcall BindInt64ByName(const char *prmName, long long data);
	void __fastcall BindInt64ByName(const char *prmName, const AnsiString &data);

	void __fastcall BindDoubleByName(const char *prmName, double data);
	void __fastcall BindDoubleByName(const char *prmName, const AnsiString &data);

	void __fastcall BindStringByName(const char *prmName, const char* data);
	void __fastcall BindStringByName(const char *prmName, const AnsiString &data);

	SQLiteReader __fastcall ExecuteReader();
	SQLiteReader __fastcall ExecuteReaderFreeCommand();
	void __fastcall ExecuteNonQuery();
	int __fastcall ExecuteInt();
	long long __fastcall ExecuteInt64();
	long long __fastcall ExecuteInt64_NoThrow(); // [+]PPA
	double __fastcall ExecuteDouble();
	AnsiString __fastcall ExecuteString();

	bool IsNoDataFound()const {
		return this->noDataFound;
	}

//	int __fastcall GetColumnCount()const {
//		return this->columnCount;
//	}
	__property int ResultColumnCount = {
		read = columnCount
	};
	__property AnsiString * ResultColumnNames = {
		read = columns
	};
};

// -----------------------------------------------------------------------------
// SQLiteReader
// -----------------------------------------------------------------------------
class SQLiteReader {

private:
	friend class SQLiteCommand;

	SQLiteCommand *cmd;
	bool needFreeSQLiteCommand;

	__fastcall SQLiteReader(SQLiteCommand *cmd, bool needFreeComd = false);
	void __fastcall CheckReader(int p_index);

	int __fastcall GetColumnIndex(const AnsiString &pColumnName);
	int __fastcall GetColumnIndex(const char *pColumnName) {
		return this->GetColumnIndex(AnsiString(pColumnName));
	};

public:
	__fastcall SQLiteReader();
	__fastcall SQLiteReader(const SQLiteReader &copy);
	__fastcall ~SQLiteReader();

	SQLiteReader& __fastcall operator = (const SQLiteReader & copy);

	bool __fastcall Read();
	void __fastcall Reset();
	void __fastcall Close();

	bool __fastcall IsNull(int index);
	bool __fastcall IsNull(const char *pColumnName);
	bool __fastcall IsNull(const AnsiString &pColumnName);

	int __fastcall GetInt(int index);
	int __fastcall GetInt(const char *pColumnName);
	int __fastcall GetInt(const AnsiString &pColumnName);

	long long __fastcall GetInt64(int index);
	long long __fastcall GetInt64(const char *pColumnName);
	long long __fastcall GetInt64(const AnsiString &pColumnName);

	double __fastcall GetDouble(int index);
	double __fastcall GetDouble(const char *pColumnName);
	double __fastcall GetDouble(const AnsiString &pColumnName);

	AnsiString __fastcall GetString(int index);
	AnsiString __fastcall GetString(const char *pColumnName);
	AnsiString __fastcall GetString(const AnsiString &pColumnName);

	// void __fastcall GetBlob(int index, std::vector<uint8_t>& p_result);
	bool __fastcall GetBlob(int index, void* p_result, int p_size);
	bool __fastcall GetBlob(const char *pColumnName, void* p_result, int p_size);
	bool __fastcall GetBlob(const AnsiString &pColumnName, void* p_result, int p_size);

	AnsiString __fastcall GetColumnName(int index);

	__property SQLiteCommand *SQLcommand = {
		read = cmd
	};
};

// -----------------------------------------------------------------------------
// SQLiteDbError
// -----------------------------------------------------------------------------
class SQLiteDbError : public Exception {

public:
	__fastcall SQLiteDbError(const char *pMessage, const char *extraInfo = NULL) : Exception(pMessage) {
		AnsiString msg = AnsiString(pMessage);

		if (extraInfo && strlen(extraInfo) > 0) {
			msg += AnsiString(extraInfo);
		}
		this->Message = msg;
	}

	__fastcall SQLiteDbError(const AnsiString pMessage, const AnsiString extraInfo = "") : Exception(pMessage + extraInfo) {
	}

	SQLiteDbError(const SQLiteConnection* dbConnect) : Exception(sqlite3_errmsg(dbConnect->DataBase)) {
	}
};

// -----------------------------------------------------------------------------
#endif
