// -----------------------------------------------------------------------------
// UPPredictor.cpp
// -----------------------------------------------------------------------------

#pragma hdrstop

#include "UPPredictor.h"
#include "DXFFromDbReader.h"
#include "DbDXFMapsContainer.h"
#include "CoordinateCorrector.h"
#include "DbDxfStructures.h"
#include "SQLiteTools.h"
#include "Manager.h"

//------------------------------------------------------------------------------

#pragma package(smart_init)

//------------------------------------------------------------------------------
// TUPPredictor - underground pipeline predictor
//------------------------------------------------------------------------------

// -------- Constructor / Destructor -------------------------------------------

// -----------------------------------------------------------------------------

// -------- ClearCorrectorStack ------------------------------------------------
void __fastcall TUPPredictorDbDXF::ClearCorrectorStack() {
	std::vector<CoordinateChangeData *>::iterator it;
	CoordinateChangeData *cc_data = NULL;

	for (it = this->mCorrectorStack.begin(); it != this->mCorrectorStack.end(); it++) {
		cc_data = *it;

		if (cc_data) {
			delete cc_data;
		}
	}
}
// -----------------------------------------------------------------------------

// -------- Scan ---------------------------------------------------------------
void __fastcall TUPPredictorDbDXF::Scan(TUPPPoint<TUPPFavoriteObject> *aMapPoint) {
	TUPPPoint<TUPPFavoriteDbDXFObject> *pMapPoint = (TUPPPoint<TUPPFavoriteDbDXFObject>*)aMapPoint;
	SQLiteConnection *conn = NULL;
	TDXFFromDbReader *db_reader = new TDXFFromDbReader();
	TCoordinateCorrector *corrector = NULL;
	TDoubleRect scan_area = this->CalcScanRect(pMapPoint->mXYZ, this->mScanRadius);
	std::vector<TUPPFavoriteDbDXFObject *> *list = &(pMapPoint->mFavorites);
	TUPPFavoriteDbDXFObject *favorite = NULL;

	try {
		conn = new SQLiteConnection();
		conn->Open(this->mOwner->FileName);

		SQLiteReader sql_reader = db_reader->GetRectEntitiesReader(conn, scan_area, &(this->mPipelineLayers));

		// ---- Read entities from database ------------------------------------
		this->mEndSqlData = false;
		this->mEntityBuffer = NULL;
		this->ClearCorrectorStack();
		this->mWorkXYZ = pMapPoint->mXYZ;   // comment for DEBUG

		int i=0;
		while (this->mEndSqlData != true) {
			try
			{
				favorite = this->GetFavoriteEntity(db_reader, &sql_reader);
                i++;
			}
			catch(...) {favorite = NULL;}

			if (favorite) {
				list->push_back(favorite);
			}
		}

		// ---- Read strings groups from database ------------------------------
		if (list->size() > 0) {
			std::vector<int> fovorites_id;
			std::vector<TUPPFavoriteDbDXFObject *>::iterator it;

			for (it = list->begin(); it != list->end(); it++) {
				favorite = *it;
				fovorites_id.push_back(favorite->mDbRecordId);
			}

			sql_reader.Close();
			sql_reader = db_reader->GetExGroupsReader(conn, &fovorites_id);

			this->AddExGroupToFavorites(list, &sql_reader);

			pMapPoint->mText = this->GetAllFavoritesText(list);
			this->CalcMinDistance(pMapPoint);
		}
	}
	__finally {
		if (conn) {
			delete conn;
		}
		delete db_reader;

		if (corrector) {
			delete corrector;
		}
	}
}
//------------------------------------------------------------------------------

// -------- GetFavoriteEntity --------------------------------------------------
TUPPFavoriteDbDXFObject * __fastcall TUPPredictorDbDXF::GetFavoriteEntity(TDXFFromDbReader *pEntityReader, SQLiteReader *pSqlReader) {
	TUPPFavoriteDbDXFObject *favorite = NULL;
	TDXFEntity *entity = this->EntityRead(pEntityReader, pSqlReader);
	TDouble3DPoint cross_point_XYZ;
	float distance = 0.0;

	if (entity) {
		int db_record_id = pSqlReader->GetInt("EntityId");
		int db_layer_id = pSqlReader->GetInt("LayerId");

		switch(entity->Type) {

		case detPolyLine:
		case detLwPolyLine:
			favorite = this->GetLw_PolilineFavorite(entity, pEntityReader, pSqlReader);
			if (favorite) {
				favorite->mLayerId = db_layer_id;
			}
			break;

		case detInsert:
			favorite = this->GetInsertFavorite(static_cast<TDXFInsert *>(entity), pEntityReader, pSqlReader);
			if (favorite) {
				favorite->mLayerId = db_layer_id;
			}
			break;

		case detArc:
		case detCircle:
		case detEllipse:
		case detLine:
		case detPoint:
		case detVertex:
			if (this->mCorrectorStack.empty() != true) {
				this->CalcEntityRealCoordinate(entity);
			}

			distance = 0.0;
			cross_point_XYZ = entity->XYZ;
			if (this->IsParhtOfScanCircle(entity, &distance, &cross_point_XYZ)) {
				favorite = new TUPPFavoriteDbDXFObject();
				favorite->mDbRecordId = db_record_id;
				favorite->mEntity = entity;
				favorite->mFoXYZ = cross_point_XYZ;
				favorite->mDistance = distance;
				favorite->mLayerId = db_layer_id;
			}
			break;

		default:
			break;
		}
	}
	return favorite;
}
//------------------------------------------------------------------------------

// -------- CalcEntityRealCoordinate -------------------------------------------
TDXFEntity * __fastcall TUPPredictorDbDXF::CalcEntityRealCoordinate(TDXFEntity *pEntity) {
	std::vector<CoordinateChangeData *>::reverse_iterator it;
	CoordinateChangeData *cc_data = NULL;

	for (it = this->mCorrectorStack.rbegin(); it != this->mCorrectorStack.rend(); it++) {
		cc_data = *it;

		if (cc_data) {
			TCoordinateCorrector::CalcRealCoordinate(pEntity, cc_data);
		}
	}

	return pEntity;
}
//------------------------------------------------------------------------------

// -------- GetLw_PolilineFavorite ---------------------------------------------
TUPPFavoriteDbDXFObject * __fastcall TUPPredictorDbDXF::GetLw_PolilineFavorite(TDXFEntity *pEntity, TDXFFromDbReader *pEntityReader,
	SQLiteReader *pSqlReader) {

	TUPPFavoriteDbDXFObject *favorite = new TUPPFavoriteDbDXFObject();
	TUPPFavoriteDbDXFObject *favorite_line = NULL;
	TDXFEntity *entity_line = NULL;
	int polyline_record_id = pSqlReader->GetInt("EntityId");
	int entity_parent_record_id = 0;

	favorite->mDbRecordId = polyline_record_id;
	favorite->mEntity = pEntity;
	favorite->mFoXYZ = pEntity->XYZ;

	while (this->mEndSqlData != true) {
		entity_line = this->EntityRead(pEntityReader, pSqlReader);

		if (entity_line) {
			this->mEntityBuffer = entity_line;
			entity_parent_record_id = pSqlReader->GetInt("ParentEntityId");

			if (polyline_record_id == entity_parent_record_id) {

				// ---- Processing child line ----------------------------------
				favorite_line = this->GetFavoriteEntity(pEntityReader, pSqlReader);

				if (favorite_line) {
					favorite->mChildEntity.push_back(favorite_line);
				}
			}
			else {
				// ---- Child lines ended --------------------------------------
				break;
			}
		}
	}

	if (favorite->mChildEntity.size() == 0) {
		delete favorite;
		favorite = NULL;
	}
	else {
		this->CalcMinDistance(favorite);
	}

	return favorite;
}
//------------------------------------------------------------------------------

// -------- EntityRead ---------------------------------------------------------
TUPPFavoriteDbDXFObject * __fastcall TUPPredictorDbDXF::GetInsertFavorite(TDXFInsert *pInsert, TDXFFromDbReader *pEntityReader,
	SQLiteReader *pSqlReader) {

	TUPPFavoriteDbDXFObject *favorite = new TUPPFavoriteDbDXFObject();
	TUPPFavoriteDbDXFObject *favorite_item = NULL;
	TDXFEntity *entity_item = NULL;
	int insert_record_id = pSqlReader->GetInt("EntityId");
	int entity_parent_record_id = 0;

	favorite->mDbRecordId = insert_record_id;
	favorite->mEntity = pInsert;
	favorite->mFoXYZ = pInsert->XYZ;

	// ---- Create new coordinate correction data ------------------------------
	CoordinateChangeData *cc_data = this->CreateCoordinateChangeData(pInsert);

	cc_data->DbBlockRecordId = insert_record_id;
	this->mCorrectorStack.push_back(cc_data);

	try {
		// ---- Processing Insert child entities -------------------------------
		while (this->mEndSqlData != true) {
			entity_item = this->EntityRead(pEntityReader, pSqlReader);

			if (entity_item) {
				this->mEntityBuffer = entity_item;
				entity_parent_record_id = pSqlReader->GetInt("ParentEntityId");

				if (insert_record_id == entity_parent_record_id) {

					// ---- Processing child entity ----------------------------
					favorite_item = this->GetFavoriteEntity(pEntityReader, pSqlReader);

					if (favorite_item) {
						favorite->mChildEntity.push_back(favorite_item);
					}
				}
				else {
					// ---- Child entiies ended --------------------------------
					if (favorite->mChildEntity.size() == 0) {
						delete favorite;
						favorite = NULL;
					}
					else {
						this->CalcMinDistance(favorite);
					}
					break;
				}
			}
		}
	}
	catch(...) {
	}

	this->mCorrectorStack.pop_back();
	delete cc_data;

	return favorite;
}
//------------------------------------------------------------------------------

// -------- GetMinDistance -----------------------------------------------------
void __fastcall TUPPredictorDbDXF::CalcMinDistance(TUPPFavoriteDbDXFObject *pFavorite) {
	std::vector<TUPPFavoriteDXFObject *>::iterator it;
	TUPPFavoriteDXFObject *child = NULL;
	float min_distance = this->mScanRadius + 100.0;
	TDouble3DPoint min_XYZ;

	for (it = pFavorite->mChildEntity.begin(); it != pFavorite->mChildEntity.end(); it++) {
		child = *it;

		if (min_distance > child->mDistance) {
			min_distance = child->mDistance;
			min_XYZ = child->mFoXYZ;
		}
	}
	pFavorite->mDistance = min_distance;
	pFavorite->mFoXYZ = min_XYZ;
}

void __fastcall TUPPredictorDbDXF::CalcMinDistance(TUPPPoint<TUPPFavoriteDbDXFObject> *pMapPoint) {
	std::vector<TUPPFavoriteDbDXFObject *>::iterator it;
	TUPPFavoriteDbDXFObject *child = NULL;
	float min_distance = this->mScanRadius + 100.0;
	TDouble3DPoint min_XYZ;

	for (it = pMapPoint->mFavorites.begin(); it != pMapPoint->mFavorites.end(); it++) {
		child = *it;

		if (min_distance > child->mDistance) {
			min_distance = child->mDistance;
			min_XYZ = child->mFoXYZ;
		}
	}
	pMapPoint->mDistance = min_distance;
	pMapPoint->mFoXYZ = min_XYZ;
}
//------------------------------------------------------------------------------

// -------- CreateCoordinateChangeData -----------------------------------------
CoordinateChangeData * __fastcall TUPPredictorDbDXF::CreateCoordinateChangeData(TDXFInsert *pInsert) {
	CoordinateChangeData *cc_data = new CoordinateChangeData();

	cc_data->XYZ = pInsert->XYZ;
	cc_data->Scale_X = pInsert->X_scale;
	cc_data->Scale_Y = pInsert->Y_scale;
	cc_data->Scale_Z = pInsert->Z_scale;
	cc_data->Angle = pInsert->Angle;

	return cc_data;
}
//------------------------------------------------------------------------------

// -------- EntityRead ---------------------------------------------------------
TDXFEntity * __fastcall TUPPredictorDbDXF::EntityRead(TDXFFromDbReader *pEntityReader, SQLiteReader *pSqlReader) {
	TDXFEntity *entity = NULL;

	if (this->mEntityBuffer) {
		entity = this->mEntityBuffer;
		this->mEntityBuffer = NULL;
	}
	else {
		TDXFInsert *insert = NULL;
		TDXFLine *line = NULL;
		TDXFPolyLine *pline = NULL;
		TDXFLwPolyLine *lwpline = NULL;
		TDXFCircle *circle = NULL;
		TDXFArc *arc = NULL;
		TDXFEllipse *ellipse = NULL;
		TDXFPoint *point = NULL;

		if (pSqlReader->Read()) {
			int i_type = pSqlReader->GetInt("EntityType");

			switch(TDbDXFMapsContainer::IntToEntityType(i_type)) {
			case detPolyLine:
				pline = new TDXFPolyLine(NULL);
				pline->Import(pEntityReader->PolyLineInfoFromDb(pSqlReader));
				entity = pline;
				break;

			case detArc:
				arc = new TDXFArc(NULL);
				arc->Import(pEntityReader->ArcInfoFromDb(pSqlReader));
				entity = arc;
				break;

			case detCircle:
				circle = new TDXFCircle(NULL);
				circle->Import(pEntityReader->CircleInfoFromDb(pSqlReader));
				entity = circle;
				break;

			case detEllipse:
				ellipse = new TDXFEllipse(NULL);
				ellipse->Import(pEntityReader->EllipseInfoFromDb(pSqlReader));
				entity = ellipse;
				break;

			case detInsert:
				insert = new TDXFInsert(NULL);
				insert->Import(pEntityReader->InsertInfoFromDb(pSqlReader));
				entity = insert;
				break;

			case detLine:
				line = new TDXFLine(NULL);
				line->Import(pEntityReader->LineInfoFromDb(pSqlReader));
				entity = line;
				break;

			case detLwPolyLine:
				lwpline = new TDXFLwPolyLine(NULL);
				lwpline->Import(pEntityReader->LwPolyLineInfoFromDb(pSqlReader));
				entity = lwpline;
				break;

			case detPoint:
				point = new TDXFPoint(NULL);
				point->Import(pEntityReader->PointInfoFromDb(pSqlReader));
				entity = point;
				break;

			default:
				break;
			}
		}
		else {
			this->mEndSqlData = true;
		}
	}
	return entity;
}
//------------------------------------------------------------------------------

// -------- CalcScanRect -------------------------------------------------------
TDoubleRect __fastcall TUPPredictorDbDXF::CalcScanRect(TDouble3DPoint pPoint, float pRadius) {
	TDoubleRect scan_area;

	scan_area.Left = pPoint.X - pRadius;
	scan_area.Right = pPoint.X + pRadius;
	scan_area.Top = pPoint.Y + pRadius;
	scan_area.Bottom = pPoint.Y - pRadius;

// DEBUG
//	scan_area.Left = 216493.360032119;
//	scan_area.Right = 216493.380032119;
//	scan_area.Top = 466226.982930755;
//	scan_area.Bottom = 466226.962930755;

//	scan_area.Left = 216491.360032119;
//	scan_area.Right = 216495.380032119;
//	scan_area.Top = 466228.982930755;
//	scan_area.Bottom = 466224.962930755;
//
//	this->mWorkXYZ.X = 216493.370032119;
//	this->mWorkXYZ.Y = 466226.972930755;

	return scan_area;
}
//------------------------------------------------------------------------------

// -------- IsParhtOfScanCircle ------------------------------------------------
bool __fastcall TUPPredictorDbDXF::IsParhtOfScanCircle(TDXFEntity *pEntity, float *pDistance, TDouble3DPoint *pCrossPointXYZ) {
	bool result = false;
	TFloatLine p1, p2;

	switch (pEntity->Type) {
	case detArc:
		result = this->IsArcParhtOfScanCircle(static_cast<TDXFArc *>(pEntity), pDistance, pCrossPointXYZ);
		break;

	case detCircle:
		result = this->IsCircleParhtOfScanCircle(static_cast<TDXFCircle *>(pEntity), pDistance, pCrossPointXYZ);
		break;

	case detEllipse:
		result = false;
		break;

	case detLine:
		result = this->IsLineParhtOfScanCircle(static_cast<TDXFLine *>(pEntity), pDistance, pCrossPointXYZ);
		break;

	case detPoint:
		result = this->IsPointParhtOfScanCircle(static_cast<TDXFPoint *>(pEntity), pDistance, pCrossPointXYZ);
		break;

	default:
		*pDistance = this->mScanRadius;
		break;
	}
	return result;
}
//------------------------------------------------------------------------------

// -------- IsArcParhtOfScanCircle ---------------------------------------------
bool __fastcall TUPPredictorDbDXF::IsArcParhtOfScanCircle(TDXFArc *pArc, float *pDistance, TDouble3DPoint *pCrossPointXYZ) {
	bool result = false;
	TFloatLine ac;

	if ((pArc->XYZ.X == this->mWorkXYZ.X) && (pArc->XYZ.Y == this->mWorkXYZ.Y)) {
		// -------- arc center is equals work point ----------------------------
		if (pArc->R <= this->mScanRadius) {
			ac.X1 = pArc->XYZ.X;
			ac.Y1 = pArc->XYZ.Y;
			ac.X2 = pArc->XYZ.X + pArc->R;
			ac.Y2 = pArc->XYZ.Y;

			if (pArc->Start > 0.001) {
				ac.Rotate(-pArc->Start * M_PI / 180.);
			}
			pCrossPointXYZ->X = ac.X2;
			pCrossPointXYZ->Y = ac.Y2;
			*pDistance = pArc->R;
			result = true;
		}
	}
	else {
		ac.X1 = pArc->XYZ.X;
		ac.Y1 = pArc->XYZ.Y;
		ac.X2 = this->mWorkXYZ.X;
		ac.Y2 = this->mWorkXYZ.Y;

		double ac_len = ac.Length();

		if (ac_len != 0 && ac_len < pArc->R + this->mScanRadius) {
			// -------- calculate angle between arc center and work point ------
			/*double angle_rad = acos(fabs(ac.Width() / ac_len));

			if ((ac.Width() < 0.0) && (ac.Height() > 0.0)) {
				angle_rad = M_PI - angle_rad; // II quadrant
			}
			else if ((ac.Width() < 0.0) && (ac.Height() < 0.0)) {
				angle_rad = M_PI + angle_rad; // III quadrant
			}
			else if ((ac.Width() > 0.0) && (ac.Height() < 0.0)) {
				angle_rad = M_PI + angle_rad; // IV quadrant
			}
			double angle_degree = 180.0 * angle_rad / M_PI;*/
            double angle_degree = 180.0 * ac.Angle() / M_PI;

			if ((angle_degree >= pArc->Start) && (angle_degree <= pArc->Stop)) {
				// -------- cross point in arc sector --------------------------
				double proportion = pArc->R / ac_len;
				double delta = ac.Width() * proportion;

				pCrossPointXYZ->X = pArc->XYZ.X + delta;

				delta = ac.Height() * proportion;
				pCrossPointXYZ->Y = pArc->XYZ.Y + delta;

				*pDistance = fabs(ac_len - pArc->R);

				result = true;
			}
			else {
				// -------- cross point outside arc sector ---------------------
				TFloatLine tmp_line;
				TDouble3DPoint start_point, stop_point;
				double start_distance, stop_distance;

				// -------- calculate start point of arc -----------------------
				ac.X1 = pArc->XYZ.X;
				ac.Y1 = pArc->XYZ.Y;
				ac.X2 = pArc->XYZ.X + pArc->R;
				ac.Y2 = pArc->XYZ.Y;

				if (pArc->Start > 0.001) {
					ac.Rotate(-pArc->Start * M_PI / 180.);
				}
				start_point.X = ac.X2;
				start_point.Y = ac.Y2;

				tmp_line.X1 = this->mWorkXYZ.X;
				tmp_line.Y1 = this->mWorkXYZ.Y;
				tmp_line.X2 = start_point.X;
				tmp_line.Y2 = start_point.Y;
				start_distance = tmp_line.Length();

				// -------- calculate stop point of arc ------------------------
				ac.X2 = pArc->XYZ.X + pArc->R;
				ac.Y2 = pArc->XYZ.Y;

				if (pArc->Stop > 0.001) {
					ac.Rotate(-pArc->Stop * M_PI / 180.);
				}
				stop_point.X = ac.X2;
				stop_point.Y = ac.Y2;

				tmp_line.X2 = stop_point.X;
				tmp_line.Y2 = stop_point.Y;
				stop_distance = tmp_line.Length();

				// -------- calculate min distance -----------------------------
				if (stop_distance < start_distance) {
					start_distance = stop_distance;
					start_point = stop_point;
				}

				if (start_distance <= this->mScanRadius) {
					*pDistance = start_distance;
					*pCrossPointXYZ = start_point;

					result = true;
				}
			}
		}
	}

	return result;
}
//------------------------------------------------------------------------------

// -------- IsLineParhtOfScanCircle --------------------------------------------
bool __fastcall TUPPredictorDbDXF::IsCircleParhtOfScanCircle(TDXFCircle *pCircle, float *pDistance, TDouble3DPoint *pCrossPointXYZ) {
	bool result = false;
	TFloatLine ac;

	if ((pCircle->XYZ.X == this->mWorkXYZ.X) && (pCircle->XYZ.Y == this->mWorkXYZ.Y)) {
		// -------- circle center is equals work point -------------------------
		if (pCircle->R <= this->mScanRadius) {
			pCrossPointXYZ->X = pCircle->XYZ.X + pCircle->R;
			pCrossPointXYZ->Y = pCircle->XYZ.Y;
			*pDistance = pCircle->R;
			result = true;
		}
	}
	else {
		ac.X1 = this->mWorkXYZ.X;
		ac.Y1 = this->mWorkXYZ.Y;
		ac.X2 = pCircle->XYZ.X;
		ac.Y2 = pCircle->XYZ.Y;

		double ac_len = ac.Length();

		if (ac_len < pCircle->R + this->mScanRadius) {
			double proportion = pCircle->R / ac_len;
			double delta = ac.Width() * proportion;

			pCrossPointXYZ->X = pCircle->XYZ.X + delta;

			delta = ac.Height() * proportion;
			pCrossPointXYZ->Y = pCircle->XYZ.Y + delta;

			*pDistance = fabs(ac_len - pCircle->R);

			result = true;
		}
	}

	return result;
}
//------------------------------------------------------------------------------

// -------- IsEllipseParhtOfScanCircle ---------------------------------------------
bool __fastcall TUPPredictorDbDXF::IsEllipseParhtOfScanCircle(TDXFEllipse *pEllipse, float *pDistance, TDouble3DPoint *pCrossPointXYZ) {
	bool result = false;
	TFloatLine ac;

	ac.X1 = this->mWorkXYZ.X;
	ac.Y1 = this->mWorkXYZ.Y;
	ac.X2 = pEllipse->XYZ.X;
	ac.Y2 = pEllipse->XYZ.Y;

	double ac_len = ac.Length();

	if (ac_len < this->mScanRadius) {
		pCrossPointXYZ->Y = pEllipse->XYZ.Y;

		*pDistance = fabs(ac_len - this->mScanRadius);

		result = true;
	}

	return result;
}
//------------------------------------------------------------------------------

// -------- IsLineParhtOfScanCircle --------------------------------------------
//  Line A - B and map point C --> triangle ABC.  AB, AC, BC - sides of triangle
bool __fastcall TUPPredictorDbDXF::IsLineParhtOfScanCircle(TDXFLine *pLine, float *pDistance, TDouble3DPoint *pCrossPointXYZ) {
	bool result = false;
	TFloatLine ab, ac, bc;
	double ab_len, ac_len, bc_len;
	double ac_width, ac_height, bc_width, bc_height;

	ab.X1 = pLine->XYZ.X;
	ab.Y1 = pLine->XYZ.Y;
	ab.X2 = pLine->Point2.X;
	ab.Y2 = pLine->Point2.Y;

	ab_len = ab.Length();

	ac.X1 = this->mWorkXYZ.X;
	ac.Y1 = this->mWorkXYZ.Y;
	ac.X2 = pLine->XYZ.X;
	ac.Y2 = pLine->XYZ.Y;

	ac_width = ac.Width();
	ac_height = ac.Height();

	if (fabs(ac_width) < 0.1) {
		ac_len = fabs(ac_height);
	}
	else if (fabs(ac_height) < 0.1) {
		ac_len = fabs(ac_width);
	}
	else {
		ac_len = ac.Length();
	}

	bc.X1 = this->mWorkXYZ.X;
	bc.Y1 = this->mWorkXYZ.Y;
	bc.X2 = pLine->Point2.X;
	bc.Y2 = pLine->Point2.Y;

	bc_width = bc.Width();
	bc_height = bc.Height();

	if (fabs(bc_width) < 0.1) {
		bc_len = fabs(bc_height);
	}
	else if (fabs(bc_height) < 0.1) {
		bc_len = fabs(bc_width);
	}
	else {
		bc_len = bc.Length();
	}

	int sign_ac_width = (ac_width < 0.0) ? 0 : 1;
	int sign_ac_heigth = (ac_height < 0.0) ? 0 : 1;
	int sign_bc_width = (bc_width < 0.0) ? 0 : 1;
	int sign_bc_heigth = (bc_height < 0.0) ? 0 : 1;

	// -------- obtuse-angled triangle -----------------------------------------
	if ((sign_ac_width == sign_bc_width) && (sign_ac_heigth == sign_bc_heigth)) {

		if (ac_len < this->mScanRadius) {
			*pDistance = fabs(this->mScanRadius - ac_len);
			*pCrossPointXYZ = pLine->XYZ;
			result = true;
		}
		else if (bc_len < this->mScanRadius) {
			*pDistance = fabs(this->mScanRadius - bc_len);
			*pCrossPointXYZ = pLine->Point2;
			result = true;
		}
		else {
			result = false;
		}
	}
	else {

		// -------- triangle height --------------------------------------------
		// p = (ab + ac + bc)/2 - half-perimeter of triangle
		// h = 2 * sqrt( p*(p-ab)*(p-ac)*(p-bc) ) / ab
		double p = (ab_len + ac_len + bc_len) / 2;
		double tmp = p * (p - ab_len) * (p - ac_len) * (p - bc_len);
		double h = (tmp < 0.01) ? 0.0 : 2.0 * sqrt(tmp) / ab_len;

		if (h < this->mScanRadius) {
			*pDistance = h;
			result = true;

			double tmp = ac_len * ac_len - h * h;

			if (tmp > 0.0) {
				double a_cp_len = sqrt(tmp);
				double proportion = a_cp_len / ab_len;
				double delta = (pLine->Point2.X - pLine->XYZ.X) * proportion;

				pCrossPointXYZ->X = pLine->XYZ.X + delta;

				delta = (pLine->Point2.Y - pLine->XYZ.Y) * proportion;
				pCrossPointXYZ->Y = pLine->XYZ.Y + delta;
			}
			else {
				*pCrossPointXYZ = pLine->XYZ;
			}
		}
	}

	return result;
}
//------------------------------------------------------------------------------

// -------- IsPointParhtOfScanCircle -------------------------------------------
bool __fastcall TUPPredictorDbDXF::IsPointParhtOfScanCircle(TDXFPoint *pPoint, float *pDistance, TDouble3DPoint *pCrossPointXYZ) {
	bool result = false;
	TFloatLine ac;

	ac.X1 = this->mWorkXYZ.X;
	ac.Y1 = this->mWorkXYZ.Y;
	ac.X2 = pPoint->XYZ.X;
	ac.Y2 = pPoint->XYZ.Y;

	if ((fabs(ac.Width()) < 0.1) && (fabs(ac.Height()) < 0.1)) {
		*pDistance = 0.1;
		result = true;
	}
	else {
		double ac_len = ac.Length();

		if (ac_len < this->mScanRadius) {
			*pDistance = ac_len;
			result = true;
		}
	}
	if (result) {
		*pCrossPointXYZ = pPoint->XYZ;
	}

	return result;
}
//------------------------------------------------------------------------------

// -------- AddExGroupToFavorite -----------------------------------------------
 void __fastcall TUPPredictorDbDXF::AddExGroupToFavorites(std::vector<TUPPFavoriteDbDXFObject *> *pFavotitesList, SQLiteReader *pSqlReader) {
	AnsiString text;
	int favorite_id = 0;
	TUPPFavoriteDbDXFObject *favorite = NULL;

	while (pSqlReader->Read()) {
		favorite_id = pSqlReader->GetInt("EntityId");

		favorite = this->FindFavoriteById(pFavotitesList, favorite_id);

		if (favorite) {
			text = pSqlReader->GetString("Value");
			favorite->mExGroupText.push_back(text);

			if (favorite->mText.Length() > 0) {
				favorite->mText += "\n";
			}
			favorite->mText += text;
		}
	}
}
//------------------------------------------------------------------------------

// -------- FindFavoriteById ---------------------------------------------------
TUPPFavoriteDbDXFObject * __fastcall TUPPredictorDbDXF::FindFavoriteById(std::vector<TUPPFavoriteDbDXFObject *> *pFavotitesList, int pDbIdfavorite) {
	std::vector<TUPPFavoriteDbDXFObject *>::iterator it;
	TUPPFavoriteDbDXFObject *favorite = NULL;

	for (it = pFavotitesList->begin(); it != pFavotitesList->end(); it++) {

		if (pDbIdfavorite == (*it)->mDbRecordId) {
			favorite = *it;
			break;
		}
	}
	return favorite;
}
//------------------------------------------------------------------------------

// -------- GetAllFavoritesText ------------------------------------------------
AnsiString __fastcall TUPPredictorDbDXF::GetAllFavoritesText(std::vector<TUPPFavoriteDbDXFObject *> *pFavotitesList) {
	AnsiString result;
	std::vector<TUPPFavoriteDbDXFObject *>::iterator it;
	TUPPFavoriteDbDXFObject *favorite = NULL;

	for (it = pFavotitesList->begin(); it != pFavotitesList->end(); it++) {
		favorite = *it;

		if (result.Pos(favorite->mText) == 0) {
			if (result.Length() > 0) {
				result += "\n\n";
			}
			result += favorite->mText;
		}
	}
	return result;
}
//------------------------------------------------------------------------------


// -----------------------------------------------------------------------------
// TUPPredictorThread
// -----------------------------------------------------------------------------

// -------- Execute ------------------------------------------------------------
void __fastcall TUPPredictorDbDXFThread::ExecuteBody() {
	TDXFLayer *layer;
	TDbDXFMapsContainer *Owner = (TDbDXFMapsContainer*)mOwner;
	TUPPPoint<TUPPFavoriteDbDXFObject> mMapPoint;

	try {
		TRadarMapManager *mngr = static_cast<TRadarMapManager *>(this->mManager);

		if(mngr && mngr->Settings) {
			if(!this->mPredictor) this->mPredictor = (TUPPredictor*) new TUPPredictorDbDXF(Owner, mngr->Settings->MapObjectsPredictingRadius);

			for (int i = 0; i < Owner->DXFCollection->Layers->Count; i++) {
				layer = static_cast<TDXFLayer*>(Owner->DXFCollection->Layers->Items[i]);

				if (dltUtility == layer->LayerType) {
					this->mPredictor->AddPipelineLayer(layer->IdDbRecord);
				}
			}

			this->mPredictor->SetHoldingPercent(50.0);

			mMapPoint.mXYZ.X = this->mPoint.X;
			mMapPoint.mXYZ.Y = this->mPoint.Y;
			mMapPoint.sRadius = this->mPredictor->ScanningRadius;

			this->mPredictor->Scan((TUPPPoint<TUPPFavoriteObject>*)(&mMapPoint));

			if (this->mCallBack) {
				this->mCallBack((TUPPPoint<TUPPFavoriteObject>*)(&mMapPoint), mLifetime);
			}
		}
	}
	catch(...) {
	}
}
//------------------------------------------------------------------------------
