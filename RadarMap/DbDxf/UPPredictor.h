// -----------------------------------------------------------------------------
// UPPredictor.h
// -----------------------------------------------------------------------------

#ifndef UPPredictorH
#define UPPredictorH

#include "UPPStructures.h"
//#include "DXFMap.h"
#include <vector>

class TDXFFromDbReader;
class SQLiteReader;
class TCoordinateCorrector;
struct CoordinateChangeData;
class TMapsContainer;
class TDbDXFMapsContainer;
class TDXFArc;
class TDXFCircle;
class TDXFEllipse;
class TDXFLine;
class TDXFPoint;
class TDXFInsert;

typedef void __fastcall (__closure *TPredictorResultProcessing)(void *vMapPoint, unsigned long ObjectsLifetime);

// -----------------------------------------------------------------------------
// TUPPredictor - underground pipeline predictor
// -----------------------------------------------------------------------------
class TUPPredictor {

private:
protected:
	TMapsContainer *mOwner;
	float mScanRadius;
	float mHoldingRadius;
	float mHoldingPercent;
public:
	__fastcall TUPPredictor(TMapsContainer *pOwner, float pScanRadius) {
		mOwner = pOwner;
		mScanRadius = pScanRadius;
		mHoldingRadius = mScanRadius;
		mHoldingPercent = 0.0;
	};

	__fastcall ~TUPPredictor() {};
	virtual void __fastcall AddPipelineLayer(int pLayerId) {}
	virtual void __fastcall Scan(TUPPPoint<TUPPFavoriteObject> *aMapPoint) {}
	void __fastcall SetHoldingPercent(float pPercent) { mHoldingPercent = pPercent; mHoldingRadius = mScanRadius * (1.0 + pPercent / 100); };

	__property float ScanningRadius = {read=mScanRadius};
};

class TUPPredictorDbDXF : public TUPPredictor {

private:
	//TDbDXFMapsContainer *mOwner;

	TUPPPoint<TUPPFavoriteDbDXFObject> mCurrentPoint;
	std::vector<int> mPipelineLayers;
	std::vector<CoordinateChangeData *> mCorrectorStack;
	bool mEndSqlData;
	TDXFEntity *mEntityBuffer;
	TDouble3DPoint mWorkXYZ;

	TDoubleRect __fastcall CalcScanRect(TDouble3DPoint pPoint, float pRadius);
	TDXFEntity * __fastcall EntityRead(TDXFFromDbReader *pEntityReader, SQLiteReader *pSqlReader);

	bool __fastcall IsParhtOfScanCircle(TDXFEntity *pEntity, float *pDistance, TDouble3DPoint *pCrossPointXYZ);
	bool __fastcall IsArcParhtOfScanCircle(TDXFArc *pArc, float *pDistance, TDouble3DPoint *pCrossPointXYZ);
	bool __fastcall IsCircleParhtOfScanCircle(TDXFCircle *pCircle, float *pDistance, TDouble3DPoint *pCrossPointXYZ);
	bool __fastcall IsEllipseParhtOfScanCircle(TDXFEllipse *pEllipse, float *pDistance, TDouble3DPoint *pCrossPointXYZ);
	bool __fastcall IsLineParhtOfScanCircle(TDXFLine *pLine, float *pDistance, TDouble3DPoint *pCrossPointXYZ);
	bool __fastcall IsPointParhtOfScanCircle(TDXFPoint *pPoint, float *pDistance, TDouble3DPoint *pCrossPointXYZ);

	TUPPFavoriteDbDXFObject * __fastcall GetFavoriteEntity(TDXFFromDbReader *pEntityReader, SQLiteReader *pSqlReader);
	TUPPFavoriteDbDXFObject * __fastcall GetLw_PolilineFavorite(TDXFEntity *pEntity, TDXFFromDbReader *pEntityReader, SQLiteReader *pSqlReader);
	TUPPFavoriteDbDXFObject * __fastcall GetInsertFavorite(TDXFInsert *pInsert, TDXFFromDbReader *pEntityReader, SQLiteReader *pSqlReader);
	void __fastcall CalcMinDistance(TUPPFavoriteDbDXFObject *pFavorite);
	void __fastcall CalcMinDistance(TUPPPoint<TUPPFavoriteDbDXFObject> *pMapPoint);
	void __fastcall AddExGroupToFavorites(std::vector<TUPPFavoriteDbDXFObject *> *pFavotitesList, SQLiteReader *pSqlReader);
	TUPPFavoriteDbDXFObject * __fastcall FindFavoriteById(std::vector<TUPPFavoriteDbDXFObject *> *pFavotitesList, int pDbIdfavorite);
	AnsiString __fastcall GetAllFavoritesText(std::vector<TUPPFavoriteDbDXFObject *> *pFavotitesList);

	void __fastcall ClearCorrectorStack();
	CoordinateChangeData * __fastcall CreateCoordinateChangeData(TDXFInsert *pInsert);
	TDXFEntity * __fastcall CalcEntityRealCoordinate(TDXFEntity *pEntity);

public:

	__fastcall TUPPredictorDbDXF(TDbDXFMapsContainer *pOwner, float pScanRadius) :
		TUPPredictor((TMapsContainer*)pOwner, pScanRadius) {
		mEndSqlData = true;
		mEntityBuffer = NULL;
	};

	__fastcall ~TUPPredictorDbDXF() {
		ClearCorrectorStack();
		if (mEntityBuffer) {
			delete mEntityBuffer;
		}
	};

	void __fastcall AddPipelineLayer(int pLayerId) { mPipelineLayers.push_back(pLayerId); };
	void __fastcall Scan(TUPPPoint<TUPPFavoriteObject> *aMapPoint);

	__property TUPPPoint<TUPPFavoriteDbDXFObject> CurrentPoint = {
		read = mCurrentPoint, write = mCurrentPoint
	};
};
//------------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// TUPPredictorThread
// -----------------------------------------------------------------------------
class TUPPredictorThread : public TMyThread {

private:
protected:
	TUPPredictor *mPredictor;
	TMapsContainer *mOwner;
	TPredictorResultProcessing mCallBack;
	TDouble3DPoint mPoint;
	TObject *mManager;
	unsigned long mLifetime;
	virtual void __fastcall Execute() {Synchronize(ExecuteBody);}//ExecuteBody();}//
	virtual void __fastcall ExecuteBody() {}
public:
	__fastcall TUPPredictorThread(TMapsContainer *pOwner, TDouble3DPoint pPoint,
		TPredictorResultProcessing pCallBack, unsigned long ObjectsLifetime,
		TObject *pManager) : TMyThread(true)
		{mPredictor = NULL; mOwner = pOwner; mPoint = pPoint; mCallBack = pCallBack; mLifetime = ObjectsLifetime; mManager = pManager;}

	__fastcall ~TUPPredictorThread() {
		if (mPredictor) {
			delete mPredictor;
		}
	}
};
//------------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// TUPPredictorDbDXFThread
// -----------------------------------------------------------------------------
class TUPPredictorDbDXFThread : public TUPPredictorThread {

private:
protected:
	void __fastcall ExecuteBody();
public:
	__fastcall TUPPredictorDbDXFThread(TDbDXFMapsContainer *pOwner, TDouble3DPoint pPoint,
		TPredictorResultProcessing pCallBack, unsigned long ObjectsLifetime, TObject *pManager) :
		TUPPredictorThread((TMapsContainer*)pOwner, pPoint, pCallBack, ObjectsLifetime, pManager) {

		ForceResume();
	}

	__fastcall ~TUPPredictorDbDXFThread() {}
};
// -----------------------------------------------------------------------------

#endif
