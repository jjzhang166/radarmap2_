// -----------------------------------------------------------------------------
// DbDXFMapsContainer.h
// -----------------------------------------------------------------------------

#ifndef DbDXFMapsContainerH
#define DbDXFMapsContainerH

#include "VectorMap.h"
#include "SQLiteTools.h"
#include "DXFFromDbReader.h"
#include "DXFToDBWriter.h"
#include "DbDxfStructures.h"

#include "UPPredictor.h"

// -----------------------------------------------------------------------------
// TDbDXFMapsContainer
// -----------------------------------------------------------------------------
class TDbDXFMapsContainer : public TVectorMapsContainer {

private:
	TDXFFromDbReader *dbReader;

	void __fastcall ProcessPredictorResults(void *vMapPoint, unsigned long ObjectsLifetime);

	bool __fastcall FillIntegratedDXFInfoFromDB();
	bool __fastcall FillDXFFileInfoFromDB();
	bool __fastcall FillDXFFileCommentsInfoFromDB(SQLiteConnection *pConn, int pIdFile);
	bool __fastcall FillDXFLayersInfoFromDB(SQLiteConnection *pConn, int pIdFile);
	bool __fastcall HasUtilityLayers();
	TDXFLayer * __fastcall MergeDXFLayerInLayersTree(TDXFLayer *pLayer, TCheckBoxTreeView *TreeView);
	void __fastcall UpdateDXFLayerInfo(TDXFLayer *pLayer, TCheckBoxTreeView *TreeView, TTreeNode *pNode, bool pSaveVisible, TDXFToDBWriter *pWriter);

	TDoubleRect __fastcall GetMapRenderArea();
	bool __fastcall LayerRender(TDXFLayer *pLayer, TBitmap32 *Dst, TColor32 bgColor, int InvertColor, TDoubleRect rct,
		TDouble3DPoint SourcePoint, double Scale_x, double Scale_y, double angle); 	// angle in degrees
	bool __fastcall LayerRender(TDXFLayer *pLayer, TBitmap32 *Dst, TColor32 bgColor, int InvertColor, TDoubleRect rct) {
		TDouble3DPoint p;
		p = TDouble3DPoint();
		return this->LayerRender(pLayer, Dst, bgColor, InvertColor, rct, p, 1., 1., 0.);
	}
	void __fastcall ModifyChangeData(CoordinateChangeData *pCcData, TDXFInsert *pInsert);

protected:
	void __fastcall AddCoordinate(TGPSCoordinate *c);
public:
	__fastcall TDbDXFMapsContainer(TMyObject *AOwner, void* ASettings, TLockedDoubleRect *AViewportArea);

	__fastcall ~TDbDXFMapsContainer();

	void __fastcall UtilityUpdate();
	void __fastcall InfoUpdate();

	void __fastcall Update(TMyObjectType Type) {
		TMapsContainer::Update(Type);
	}
	void __fastcall Update();
	void __fastcall RenderMap();
	void __fastcall LoadData(AnsiString Filename, HANDLE AStopItEvent, bool WithUpdate, TCoordinateSystem CS, bool Preview = false);
	void __fastcall LoadData(AnsiString Filename, HANDLE AStopItEvent, bool WithUpdate, bool Preview = false) {
		TMapsContainer::LoadData(Filename, AStopItEvent, WithUpdate, Preview);
	}

	void __fastcall LoadData(AnsiString Filename, HANDLE AStopItEvent) {
		TMapsContainer::LoadData(Filename, AStopItEvent);//LoadData(Filename, AStopItEvent, true);
	}
	void __fastcall GetLayers(TCheckBoxTreeView *TreeView, TTreeNode *Root);
	void __fastcall SetLayers(TCheckBoxTreeView *TreeView, TTreeNode *Root);
	void __fastcall GetFavotiteObjectsAtPoint(int X, int Y);

//DXF Files operations

	static void __fastcall GetDXFFilesList(AnsiString pDbFile, std::vector<FileRecord> *pFiles); //Returns the full filenames Strings list
	bool __fastcall GetDXFFileInfo(AnsiString aFileName, struct stat  *__statbuf) {bool res=false; if(aFileName!=NULL && aFileName.Length()>0 && __statbuf) { /*ToDo with __statbuf*/ res=true;} return res;}
	bool __fastcall AddDXFFile(AnsiString pDxfFileName, TVoidFunction SuccessCallBack, TVoidFunction CancelCallBack);
	bool __fastcall DeleteDXFFile(AnsiString pDxfFileName, TVoidFunction SuccessCallBack, TVoidFunction CancelCallBack);
	bool __fastcall RefreshDXFFile(AnsiString dbFileName, AnsiString hddFileName, TVoidFunction SuccessCallBack, TVoidFunction CancelCallBack);
//

	static TDXFEntityType __fastcall IntToEntityType(int srcEntityType);
	static void __fastcall SetupDlgFilter(TOpenDialog *pOpenDialog);

	__property TDXFCollection *DXFCollection = {
		read = DXF
	};
};
// -----------------------------------------------------------------------------

#endif
