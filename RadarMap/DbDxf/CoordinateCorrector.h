// -----------------------------------------------------------------------------
// CoordinateCorrector.h
// -----------------------------------------------------------------------------

#ifndef CoordinateCorrectorH
#define CoordinateCorrectorH

#include "DbDxfStructures.h"
#include "DXFMap.h"

// -----------------------------------------------------------------------------
// TCoordinateCorrector
// -----------------------------------------------------------------------------
class TCoordinateCorrector : public TObject {

private:
	TDouble3DPoint minCorner, maxCorner;

	CoordinateChangeData *changeData;
	EntityWriterData *writerData;
	TDouble3DPoint RotatePoint;

	TDXFPolyLine *polyLineEntity;
	TDXFLine *lineEntity;
	TDXFArc *arcEntity;
	TDXFCircle *circleEntity;
	TDXFEllipse *ellipseEntity;
	TDXFInsert *insertEntity;
	TDXFLwPolyLine *lwPolyLineEntity;
	TDXFPoint *pointEntity;
	TDXFText *textEntity;
	TDXFVertex *vertexEntity;

	void __fastcall Init(TDXFCollection *pDxf, CoordinateChangeData *pChangeData = NULL);
	TDXFEntity * __fastcall CloneEntity(TDXFEntity *pEntity);
	TDXFEntity * __fastcall FillEntity(TDXFEntity *pTrgEntity, TDXFEntity *pSrcEntity);
	void __fastcall SetCoordinateChangeData(CoordinateChangeData *value);

	void __fastcall CalcArcMinMaxValue(EntityWriterData *pWriteData);
	void __fastcall CalcCircleMinMaxValue(EntityWriterData *pWriteData);
	void __fastcall CalcEllipseMinMaxValue(EntityWriterData *pWriteData);
	void __fastcall CalcLineMinMaxValue(EntityWriterData *pWriteData);
	void __fastcall CalcMinMaxValueText(EntityWriterData *pWriteData);
	void __fastcall CalcPointMinMaxValue(EntityWriterData *pWriteData);
	void __fastcall CalcVertexMinMaxValue(EntityWriterData *pWriteData);

	void __fastcall ChangeArcCoordinate(TDXFArc *pSrcArc, TDXFArc *pTrgArc, CoordinateChangeData *pCorrectData);
	void __fastcall ChangeCircleCoordinate(TDXFCircle *pSrcCircle, TDXFCircle *pTrgCircle, CoordinateChangeData *pCorrectData);
	void __fastcall ChangeEllipseCoordinate(TDXFEllipse *pSrcEllipse, TDXFEllipse *pTrgEllipse, CoordinateChangeData *pCorrectData);
	void __fastcall ChangeInsertCoordinate(TDXFInsert *pSrcInsert, TDXFInsert *pTrgInsert, CoordinateChangeData *pCorrectData);
	void __fastcall ChangeLineCoordinate(TDXFLine *pSrcLine, TDXFLine *pTrgLine, CoordinateChangeData *pCorrectData);
	void __fastcall ChangePointCoordinate(TDXFPoint *pSrcPoint, TDXFPoint *pTrgPoint, CoordinateChangeData *pCorrectData);
	void __fastcall ChangeTextCoordinate(TDXFText *pSrcText, TDXFText *pTrgText, CoordinateChangeData *pCorrectData);
	void __fastcall ChangeVertexCoordinate(TDXFVertex *pSrcVertex, TDXFVertex *pTrgVertex, CoordinateChangeData *pCorrectData);

	static void __fastcall ChangeArcCoordinate(TDXFArc *pArc, CoordinateChangeData *pCorrectData);
	static void __fastcall ChangeCircleCoordinate(TDXFCircle *pCircle, CoordinateChangeData *pCorrectData);
	static void __fastcall ChangeEllipseCoordinate(TDXFEllipse *pEllipse, CoordinateChangeData *pCorrectData);
	static void __fastcall ChangeLineCoordinate(TDXFLine *pLine, CoordinateChangeData *pCorrectData);
	static void __fastcall ChangePointCoordinate(TDXFPoint *pPoint, CoordinateChangeData *pCorrectData);
	static void __fastcall ChangeVertexCoordinate(TDXFVertex *pVertex, CoordinateChangeData *pCorrectData);

public:
	__fastcall TCoordinateCorrector(TDXFCollection *pDxf) {
		this->Init(pDxf);
	}
	__fastcall TCoordinateCorrector(TDXFCollection *pDxf, CoordinateChangeData *pChangeData) {
		this->Init(pDxf, pChangeData);
	}
	__fastcall ~TCoordinateCorrector();

	__property CoordinateChangeData *ChangeData = {
		read = changeData, write = SetCoordinateChangeData
	};

	CoordinateChangeData __fastcall CreateChangeData(TDXFInsert *pInsert);
	CoordinateChangeData __fastcall CreateAsModifyChangeData(TDXFInsert *pInsert);
	EntityWriterData * __fastcall CloneWithRealCoordinate(TDXFEntity *pEntity); //, TDouble3DPoint *pPointsArray = NULL, int pCount = 0);

	static TDXFEntity * __fastcall CalcRealCoordinate(TDXFEntity *pEntity, CoordinateChangeData *pCorrectData);
};
//------------------------------------------------------------------------------
#endif
