// -----------------------------------------------------------------------------
// DXFtoDbLoader.h
// -----------------------------------------------------------------------------

#ifndef DXFtoDbLoaderH
#define DXFtoDbLoaderH

#include "VisualToolObjects.h"
#include "SQLiteTools.h"
#include "CoordinateCorrector.h"
#include "DXFFromDbReader.h"
#include "DXFToDBWriter.h"
#include <stack>

class TDbDxfBaseThread;

// -----------------------------------------------------------------------------
// TDXFtoDbLoader
// -----------------------------------------------------------------------------
class TDXFtoDbLoader {

private:
	TDbDxfBaseThread *mThread;
	TObject* mManager;
	AnsiString mFileName;
	AnsiString mDbName;
	TToolProgressBar *mProgressBar;

	TDXFToDBWriter *mDbWriter;
	TDXFFromDbReader *mDbReader;
	TCoordinateCorrector *mCorrector;
	std::stack<int> hierarchyLevel;
	std::stack<CoordinateChangeData *> changeStack;

	SQLiteConnection* __fastcall GetDbConnect();
	int __fastcall CalcWritedItems(TDXFCollection *pDxf);
	int __fastcall CalcGeoIndexItems(SQLiteConnection *pConn);

	void __fastcall WriteLayersToDB(TDXFCollection *pDxf, SQLiteConnection *pConn, int pFileId);
	int __fastcall WriteEntityToDB(TDXFEntity *pEntity, int pLayerId, int pParentId);
	void __fastcall WritePolyLineToDB(TDXFPolyLine *pPolyLine, int pLayerId, int pParentId);
	void __fastcall WriteInsertToDB(TDXFInsert *pInsert, int pLayerId, int pParentId);
	void __fastcall WriteLwPolyLineToDB(TDXFLwPolyLine *pLwPolyLine, int pLayerId, int pParentId);
	void __fastcall WriteExGroupsToDB(TDXFEntity *pEntity, int pParentId);

	void __fastcall FreeDbObject(SQLiteConnection *pConn, TDXFToDBWriter *pDbWriter, SQLiteCommand *pCmd);

	void __fastcall DeleteFileRecordFromDb(SQLiteConnection *pConn, UnicodeString pFileName, UnicodeString pFileDir);
	void __fastcall UpdateContainerGeoInfo(EntityWriterData *pWriteData, int pDbRecordId);
	void __fastcall UpdateMapParams(SQLiteConnection *pConn);
	void __fastcall WritePointsAsLinesToDB(TDXFEntity *pEntity, TDouble3DPoint *pPointsArray, int pCount, int pLayerId, int pParentId);

	bool __fastcall CheckCoordinateSystem(SQLiteConnection *pConn, UnicodeString pDxfFileName);

	bool __fastcall CheckTerminated();
//	__int64 WorkCountLast;

//protected:
//	void __fastcall Execute();

public:
	__fastcall TDXFtoDbLoader(TDbDxfBaseThread *pThread, TObject* pManager, AnsiString pFileName, AnsiString pDbName, TToolProgressBar *pProgressBar) {
//		FreeOnTerminate = true;
		mThread = pThread;
		mManager = pManager;
		mFileName = pFileName;
		mDbName = pDbName;
		mDbWriter = NULL;
		mDbReader = new TDXFFromDbReader();
		mCorrector = NULL;

//		mInetMap = pInetMap;
//		pInetMap->MapLoaderThreadId = this->ThreadID;
		mProgressBar = pProgressBar;
		if(mProgressBar) mProgressBar->Show(10, true);
////		WorkCountLast = 0;
//		ForceResume();
////		AskForResume();
	}

	__fastcall ~TDXFtoDbLoader() {
//		if(mInetMap->MapLoaderThreadId == this->ThreadID) {
//			mInetMap->MapLoaderThreadId = 0;
//		}
		delete mDbReader;

		if(mProgressBar) {
			mProgressBar->Hide(true);
		}
		if (mDbWriter) {
			delete mDbWriter;
		}
		if (mCorrector) {
			delete mCorrector;
		}
	}

	static bool __fastcall CreateNewDatabase(AnsiString pDbFileName);
	static void __fastcall SplitString(UnicodeString &pString, TStringList *pList);
	static UnicodeString __fastcall GetGeoIndexSubTablePrefix(TStringList *pList);

	TDXFCollection * __fastcall ReadDXFFile();
	bool __fastcall MakeDatabase();
	void __fastcall WriteToDB(TDXFCollection *pDxf);
	void __fastcall DeleteDxfFromDB(AnsiString pFileName = NULL);
	bool  __fastcall CreateGeoIndex();
};
// -----------------------------------------------------------------------------
#endif
