// -----------------------------------------------------------------------------
// DXFtoDbLoader.cpp
// -----------------------------------------------------------------------------

#pragma hdrstop

#include "DXFtoDbLoader.h"
#include "DbDxfThreads.h"
#include "Manager.h"
#include "DXFMap.h"
#include <sys\stat.h>

#pragma package(smart_init)

// -----------------------------------------------------------------------------
// TDXFtoDbLoader
// -----------------------------------------------------------------------------

// -------- FillDXFFile --------------------------------------------------------
TDXFCollection * __fastcall TDXFtoDbLoader::ReadDXFFile() {
	TDXFCollection *dxf = NULL;
	bool result = false;

	if (this->mManager) {
		TRadarMapManager *manager = (TRadarMapManager*)this->mManager;

		dxf = new TDXFCollection();
		dxf->Progress = this->mProgressBar;
		result = dxf->LoadData(this->mFileName, manager->GlobalStopItEvent, false);

		if ((result != true) || this->CheckTerminated()) {
			 delete dxf;
			 dxf = NULL;
		}
	}
	return dxf;
}
// -----------------------------------------------------------------------------

// -------- MakeDatabase -------------------------------------------------------
bool __fastcall TDXFtoDbLoader::MakeDatabase() {
	bool is_ok = false;

	if (FileExists(this->mDbName) != true) {
		is_ok = this->CreateNewDatabase(this->mDbName);
	}
	else {
		SQLiteConnection* conn = NULL;
		UnicodeString f_name = ExtractFileName(this->mFileName);
		UnicodeString f_dir = ExtractFilePath(this->mFileName);

		try {
			try {
				conn = this->GetDbConnect();

				if (this->CheckCoordinateSystem(conn, f_name)) {
					SQLiteReader reader = this->mDbReader->GetFileInfoReader(conn, f_name);

					if (reader.Read()) {
						UnicodeString msg = "";
						int f_size = reader.GetInt("FileSize");
						long f_time = reader.GetInt("FileCreateDate");
						struct stat statbuf;
						//int rez = stat(this->mFileName.c_str(), &statbuf);

						msg.printf(L"Database contains this file which loaded from directory:\n%s\n", f_dir);
						msg.cat_printf(L"\nExisting:      %10d    %s", f_size, FileRecord::TimestampToStrDateTime(f_time));
						msg.cat_printf(L"\nNew:           %10d    %s", statbuf.st_size, FileRecord::TimestampToStrDateTime(statbuf.st_atime));
						msg += L"\n\nAre you you want to replase ?";

						if (ID_YES == Application->MessageBox(msg.w_str(), f_name.w_str(), MB_YESNO | MB_ICONQUESTION)) {
							int file_id = reader.GetInt("FileId");
							UnicodeString sql_text = "";

							sql_text.printf(L"DELETE FROM file WHERE FileId = %d;", file_id);
							conn->ExecuteNonQuery(sql_text);
							is_ok = true;
						}
					}
					else {
						is_ok = true;
					}
				}
			}
			catch(...) {
			}
		}
		__finally {
			if (conn) {
				conn->Close();
				delete conn;
			}
		}
	}
	return is_ok;
}
// -----------------------------------------------------------------------------

// -------- CheckCoordinateSystem ----------------------------------------------
bool __fastcall TDXFtoDbLoader::CheckCoordinateSystem(SQLiteConnection *pConn, UnicodeString pDxfFileName) {
	bool is_ok = true;
	TDXFFromDbReader *db_reader = new TDXFFromDbReader();

	try {
		SQLiteReader reader = db_reader->GetMapParametersRecordReader(pConn);

		if (reader.Read()) {
			MapParametersRecord *map_params = db_reader->MapParamsFromDb(&reader);

			if (map_params->mCS != this->mThread->CoordinateSystem) {
				UnicodeString msg = L"MultiFile Map coordinate system isn't equals to current coordinate system\\n";
				msg += L"\n\nAre you you want to load the data?";

				if (ID_YES != Application->MessageBox(msg.w_str(), pDxfFileName.w_str(), MB_YESNO | MB_ICONQUESTION)) {
					is_ok = false;
				}
			}
		}
	}
	catch(...) {
		delete db_reader;
		throw;
	}
	delete db_reader;
	return is_ok;
}
// -----------------------------------------------------------------------------

// -------- CreateNewDatabase --------------------------------------------------
bool __fastcall TDXFtoDbLoader::CreateNewDatabase(AnsiString pDbFileName) {
	bool ok_create = true;
	SQLiteConnection *conn = NULL;
	TResourceStream *stream = NULL;
	UnicodeString res_name = "CreateBaseSQL";
	char *buffer = NULL;

	try {
		try {
			conn = new SQLiteConnection();
			conn->Open(pDbFileName);

			stream = new TResourceStream((unsigned int)HInstance, res_name, (wchar_t*)RT_RCDATA);
			int len = stream->Size;

			buffer = new char[len + 1];

			stream->Read(buffer, len);
			buffer[len] = 0;

			UnicodeString sql_txt = buffer;
			std::auto_ptr<TStringList> sql_list(new TStringList());

			TDXFtoDbLoader::SplitString(sql_txt, sql_list.get());
			UnicodeString geo_prefix = TDXFtoDbLoader::GetGeoIndexSubTablePrefix(sql_list.get());

			TStringsEnumerator *it = sql_list->GetEnumerator();
			//int pos = 0;

			while (it->MoveNext()) {
				UnicodeString str = it->Current;

				if (0 == str.Pos(geo_prefix)) {
					conn->ExecuteNonQuery(str);
				}
			}

			conn->Close();
		}
		catch(...) {;
			ok_create = false;
		}
	}
	__finally {
		if (buffer) {
			delete buffer;
		}
		if (stream) {
			delete stream;
		}
		if (conn) {
			delete conn;
		}
	}

	return ok_create;
}
// -----------------------------------------------------------------------------

// -------- SplitString --------------------------------------------------------
void __fastcall TDXFtoDbLoader::SplitString(UnicodeString &pString, TStringList *pList) {
	UnicodeString tmp = pString;
	UnicodeString t_str;
	int pos;

	while (true) {
		pos = tmp.Pos(";");

		if (pos > 0) {
			t_str = tmp.SubString(0, pos).Trim();

			if (t_str.Length() > 0) {
				pList->Add(t_str);
			}
			tmp.Delete(1, pos);
		}
		else {
			if (tmp.Length() > 0) {
				t_str = tmp.Trim();
				if (t_str.Length() > 0) {
					pList->Add(t_str);
				}
			}
			break;
		}
	}
}
// -----------------------------------------------------------------------------

// -------- GetGeoIndexSubTablePrefix ------------------------------------------
UnicodeString __fastcall TDXFtoDbLoader::GetGeoIndexSubTablePrefix(TStringList *pList) {
	UnicodeString result = "";
	TStringsEnumerator *it = pList->GetEnumerator();
	int pos, pos1;

	while (it->MoveNext()) {
		UnicodeString str = it->Current.UpperCase();
		pos = str.Pos("USING");

		if (pos > 0) {
			pos1 = str.Pos("RTREE");

			if ((pos1 > 0) && (pos1 > pos)) {
			   pos1 = str.Pos("TABLE");

			   if (pos1 > 0) {
				   str = it->Current;
				   pos1 += 5;
				   result = str.SubString(pos1, pos - pos1).Trim();
				   break;
			   }
			}
		}
	}

	if (result.Length() > 0) {
		result += "_";
	}
	return result;
}
// -----------------------------------------------------------------------------

// -------- WriteToDB ----------------------------------------------------------
void __fastcall TDXFtoDbLoader::WriteToDB(TDXFCollection *pDxf) {
	SQLiteConnection* conn = NULL;
	int file_id = -1;

	if (this->mProgressBar) {
		this->mProgressBar->Max = this->CalcWritedItems(pDxf);
		this->mProgressBar->Position = 0;
	}

	try {
		conn = this->GetDbConnect();

		this->mDbWriter = new TDXFToDBWriter(conn);
		this->mCorrector = new TCoordinateCorrector(pDxf);

		// ---- Write DXF faile information to database ------------------------
		file_id = this->mDbWriter->FileInfoToDB(pDxf);

		// ---- Write layers to database ---------------------------------------
		if (this->CheckTerminated() != true) {
			this->WriteLayersToDB(pDxf, conn, file_id);
		}

		// ---- Update global map parameters in database -----------------------
		this->UpdateMapParams(conn);
	}
	catch(...) {}

	if (this->mCorrector) {
		delete this->mCorrector;
		this->mCorrector = NULL;
	}
	this->FreeDbObject(conn, this->mDbWriter, NULL);
}
// -----------------------------------------------------------------------------

// -------- GetDbConnect -------------------------------------------------------
SQLiteConnection* __fastcall TDXFtoDbLoader::GetDbConnect() {
	SQLiteConnection* conn = new SQLiteConnection();

	conn->Open(this->mDbName.c_str());
	conn->ExecuteNonQuery("PRAGMA foreign_keys=ON;");

	return conn;
}
// -----------------------------------------------------------------------------

// -------- CalcWritedItems ----------------------------------------------------
int __fastcall TDXFtoDbLoader::CalcWritedItems(TDXFCollection *pDxf) {
	int items_count = 0;
	TDXFObjectsList *lst = pDxf->Layers;
	TDXFLayer * layer = NULL;

	for (int i = 0; i < lst->Count; i++) {
		layer = (TDXFLayer*)lst->Items[i];

		if (layer != NULL) {
			items_count += layer->List->Count;
		}
	}
	return items_count;
}
// -----------------------------------------------------------------------------

// -------- WriteLayersToDB ----------------------------------------------------
void __fastcall TDXFtoDbLoader::WriteLayersToDB(TDXFCollection *pDxf, SQLiteConnection *pConn, int pFileId) {
	TDXFLayer *layer = NULL;
	TDXFObjectsList *list = NULL;
	int layer_id = -1;

	try {
		for (int i = 0; i < pDxf->Layers->Count; i++) {
			layer = (TDXFLayer*)pDxf->Layers->Items[i];

			if ((layer != NULL) && (this->CheckTerminated() != true)) {
				pConn->BeginTransaction();

				this->hierarchyLevel.c.clear();
				this->hierarchyLevel.push(1);

				this->changeStack.c.clear();
				this->mCorrector->ChangeData = NULL;

				layer_id = this->mDbWriter->LayerInfoToDB(layer, pFileId);

				this->mDbWriter->LayerId = layer_id;

				list = layer->List;

				// if (7 == layer_id) {
				// int a = 1;
				// if (a == 1) {
				//
				// }
				// }

				for (int j = 0; j < list->Count; j++) {

					if (this->CheckTerminated()) {
						break;
					}

					this->WriteEntityToDB((TDXFEntity*)list->Items[j], layer_id, 0);

					if (this->mProgressBar) {
						this->mProgressBar->Position++;
					}
				}
			}

			if (this->CheckTerminated()) {
				pConn->Rollback();
			}
			else {
				pConn->Commit();
			}
		}
	}
	catch(...) {
		pConn->Rollback();
		throw;
	}
}
// -----------------------------------------------------------------------------

// -------- WriteEntitiesToDB --------------------------------------------------
int __fastcall TDXFtoDbLoader::WriteEntityToDB(TDXFEntity *pEntity, int pLayerId, int pParentId) {
	EntityWriterData *writer_data = NULL;
	int entity_id = -1;
	int level = this->hierarchyLevel.top();


//	if (pEntity->Handle == 154471) {
//	if (pEntity->Handle == 158920) {
//	   int a = 0;
//	   if (a == 1) {
//				  int b = 0;
//	   }
//	}

	if (pEntity != NULL) {
		switch(pEntity->Type) {
		case detArc:
			writer_data = this->mCorrector->CloneWithRealCoordinate(pEntity);
			entity_id = this->mDbWriter->ArcInfoToDB(writer_data, pLayerId, pParentId, level);
			break;

		case detCircle:
			writer_data = this->mCorrector->CloneWithRealCoordinate((TDXFCircle*)pEntity);
			entity_id = this->mDbWriter->CircleInfoToDB(writer_data, pLayerId, pParentId, level);
			break;

		case detEllipse:
			writer_data = this->mCorrector->CloneWithRealCoordinate(pEntity);
			entity_id = this->mDbWriter->EllipseInfoToDB(writer_data, pLayerId, pParentId, level);
			break;

		case detInsert:
			this->WriteInsertToDB((TDXFInsert*)pEntity, pLayerId, pParentId);
			break;

		case detLine:
			writer_data = this->mCorrector->CloneWithRealCoordinate(pEntity);
			entity_id = this->mDbWriter->LineInfoToDB(writer_data, pLayerId, pParentId, level);
			break;

		case detPolyLine:
			this->WritePolyLineToDB((TDXFPolyLine*)pEntity, pLayerId, pParentId);
			break;

		case detLwPolyLine:
			this->WriteLwPolyLineToDB((TDXFLwPolyLine*)pEntity, pLayerId, pParentId);
			break;

		case detPoint:
			writer_data = this->mCorrector->CloneWithRealCoordinate(pEntity);
			entity_id = this->mDbWriter->PointInfoToDB(writer_data, pLayerId, pParentId, level);
			break;

		case detText:
			writer_data = this->mCorrector->CloneWithRealCoordinate(pEntity);
			entity_id = this->mDbWriter->TextInfoToDB(writer_data, pLayerId, pParentId, level);
			break;

		case detVertex:
			// vertex = (TDXFVertex*)obj;
			break;

		default:
			break;
		}

		this->WriteExGroupsToDB(pEntity, entity_id);
	}
	return entity_id;
}
// -----------------------------------------------------------------------------

// -------- WriteExGroupsToDB --------------------------------------------------
void __fastcall TDXFtoDbLoader::WriteExGroupsToDB(TDXFEntity *pEntity, int pParentId) {

	if (pEntity->StringGroups && (pParentId > 0)) {
		int seq_num = 1;

		for (int i = 0; i < pEntity->StringGroups->Count; i++) {
			this->mDbWriter->ExGroupInfoToDB((TDXFGroup *)(pEntity->StringGroups->Items[i]), pParentId, seq_num);
			seq_num++;
		}
	}
}
// -----------------------------------------------------------------------------

// -------- WritePointsAsLinesToDB ---------------------------------------------
void __fastcall TDXFtoDbLoader::WritePointsAsLinesToDB(TDXFEntity *pEntity, TDouble3DPoint *pPointsArray, int pCount, int pLayerId, int pParentId) {
	TDXFLine *line = new TDXFLine(pEntity);
	TDouble3DPoint *ptr = pPointsArray;
	//EntityWriterData *writer_data = NULL;

	try {
		// ---- Generate lines by points ---------------------------------------
		if (pCount > 0) {
			line->XYZ = *ptr;
			line->Color = pEntity->Color;

			for (int i = 1; i < pCount; i++) {
				ptr++;
				line->Point2.X = ptr->X;
				line->Point2.Y = ptr->Y;
				line->Point2.Z = ptr->Z;

				this->WriteEntityToDB(line, pLayerId, pParentId);
				line->XYZ = line->Point2;
			}
		}
	}
	__finally {
		delete line;
	}
}
// -----------------------------------------------------------------------------

// -------- WriteInsertToDB ----------------------------------------------------
void __fastcall TDXFtoDbLoader::WriteInsertToDB(TDXFInsert *pInsert, int pLayerId, int pParentId) {
	TDXFObjectsList *list = NULL;
	TDXFEntity *entity = NULL;
	EntityWriterData *writer_data = NULL;
	CoordinateChangeData *cc_data = NULL;
	unsigned int level_stack_size = this->hierarchyLevel.size();
	unsigned int cc_stack_size = this->changeStack.size();
	int level = this->hierarchyLevel.top();
	int insert_id = -1;
	int entity_count = 0;

	// FIXME - Debug
//	if (pInsert->Handle == 155208) {
//	   int a = 0;
//	   if (a == 1) {
//				  int b = 0;
//	   }
//	}

	try {
		writer_data = this->mCorrector->CloneWithRealCoordinate(pInsert);   /// ???
		writer_data->Entity = pInsert;

		insert_id = this->mDbWriter->InsertInfoToDB(writer_data, pLayerId, pParentId, level);
		this->WriteExGroupsToDB(pInsert, insert_id);

		++level;
		this->hierarchyLevel.push(level);

		// ---- Create new coordinate correction data --------------------------
		cc_data = new CoordinateChangeData();
		*cc_data = this->mCorrector->CreateAsModifyChangeData(pInsert);

		this->changeStack.push(cc_data);
		this->mCorrector->ChangeData = cc_data;

		// ---- Write entities block to database -------------------------------
		list = pInsert->Block->List;
		entity_count = list->Count;

		for (int i = 0; i < entity_count; i++) {
			entity = (TDXFEntity*)list->Items[i];

			this->WriteEntityToDB(entity, pLayerId, insert_id);
		}
		this->UpdateContainerGeoInfo(writer_data, insert_id);
	}
	__finally {
		while (level_stack_size < this->hierarchyLevel.size()) {
			this->hierarchyLevel.pop();
		}

		while (cc_stack_size < this->changeStack.size()) {
			cc_data = this->changeStack.top();
			delete cc_data;

			this->changeStack.pop();
		}
		this->mCorrector->ChangeData = (this->changeStack.size() > 0) ? this->changeStack.top() : NULL;
	}
}
// -----------------------------------------------------------------------------

// -------- UpdateContainerGeoInfo ---------------------------------------------
void __fastcall TDXFtoDbLoader::UpdateContainerGeoInfo(EntityWriterData *pWriteData, int pDbRecordId) {
	bool first = true;
	double min_x, max_x, min_y, max_y;
	SQLiteReader reader = this->mDbWriter->SelectEntityChildGeoInfo(pDbRecordId);

	while (reader.Read()) {
		min_x = reader.GetDouble("Min_X");
		max_x = reader.GetDouble("Max_X");
		min_y = reader.GetDouble("Min_Y");
		max_y = reader.GetDouble("Max_Y");

		if (first) {
			pWriteData->Min_X = min_x;
			pWriteData->Max_X = max_x;
			pWriteData->Min_Y = min_y;
			pWriteData->Max_Y = max_y;

			first = false;
		}
		else {
			if (pWriteData->Min_X > min_x) {
				pWriteData->Min_X = min_x;
			}
			if (pWriteData->Max_X < max_x) {
				pWriteData->Max_X = max_x;
			}
			if (pWriteData->Min_Y > min_y) {
				pWriteData->Min_Y = min_y;
			}
			if (pWriteData->Max_Y < max_y) {
				pWriteData->Max_Y = max_y;
			}
		}
	}
	reader.Close();

	this->mDbWriter->UpdateEntityGeoInfo(pWriteData, pDbRecordId);
}
// -----------------------------------------------------------------------------

// -------- WritePolyLineToDB --------------------------------------------------
void __fastcall TDXFtoDbLoader::WritePolyLineToDB(TDXFPolyLine *pPolyLine, int pLayerId, int pParentId) {
	CoordinateChangeData *crnt_change_data = this->mCorrector->ChangeData;
	unsigned int stack_size = this->hierarchyLevel.size();
	int level = this->hierarchyLevel.top();
	int points_cnt = (pPolyLine->Closed) ? pPolyLine->Count + 1 : pPolyLine->Count;
	TDouble3DPoint *points = new TDouble3DPoint[points_cnt];

	try {
		EntityWriterData *writer_data = this->mCorrector->CloneWithRealCoordinate(pPolyLine);
		int entity_id = this->mDbWriter->PolyLineInfoToDB(writer_data, pLayerId, pParentId, level);
 		this->WriteExGroupsToDB(pPolyLine, entity_id);

		// ---- Write vertexes to database -------------------------------------
		EntityWriterData *writer_vertex_data = NULL;
		TDXFVertex *vertex = NULL;
		TDouble3DPoint *ptr = points;

		for (int i = 0; i < pPolyLine->Count; i++) {
			vertex = pPolyLine->Vertexes[i];
			*ptr = vertex->XYZ;
			ptr++;

			writer_vertex_data = this->mCorrector->CloneWithRealCoordinate(vertex);
			this->mDbWriter->VertexInfoToDB(dynamic_cast<TDXFVertex*>(writer_vertex_data->Entity), entity_id);
		}

		if (pPolyLine->Closed) {
			*ptr = *points;
		}

		// ---- Generate lines by vertexes -------------------------------------
		if (pPolyLine->Count > 1) {
			level++;
			this->hierarchyLevel.push(level);

			this->WritePointsAsLinesToDB(writer_data->Entity, points, points_cnt, pLayerId, entity_id);
		}
		this->UpdateContainerGeoInfo(writer_data, entity_id);
	}
	__finally {
		delete[]points;

		this->mCorrector->ChangeData = crnt_change_data;

		while (stack_size < this->hierarchyLevel.size()) {
			this->hierarchyLevel.pop();
		}
	}
}
// -----------------------------------------------------------------------------

// -------- WriteLwPolyLineToDB ------------------------------------------------
void __fastcall TDXFtoDbLoader::WriteLwPolyLineToDB(TDXFLwPolyLine *pLwPolyLine, int pLayerId, int pParentId) {
	CoordinateChangeData *crnt_change_data = this->mCorrector->ChangeData;
	TDXFVertex *vertex = new TDXFVertex(pLwPolyLine);
	unsigned int stack_size = this->hierarchyLevel.size();
	int level = this->hierarchyLevel.top();
	int points_cnt = (pLwPolyLine->Closed) ? pLwPolyLine->Count + 1 : pLwPolyLine->Count;
	TDouble3DPoint *points = new TDouble3DPoint[points_cnt];

	try {
		EntityWriterData *writer_data = this->mCorrector->CloneWithRealCoordinate(pLwPolyLine);
		int entity_id = this->mDbWriter->LwPolyLineInfoToDB(writer_data, pLayerId, pParentId, level);
		this->WriteExGroupsToDB(pLwPolyLine, entity_id);

		// ---- Write vertexes to database -------------------------------------
		EntityWriterData *writer_vertex_data = NULL;
		TDoublePoint *v_point = NULL;
		TDouble3DPoint *ptr = points;

		for (int i = 0; i < pLwPolyLine->Count; i++) {
			v_point = pLwPolyLine->Vertexes[i];
			vertex->XYZ.X = v_point->X;
			vertex->XYZ.Y = v_point->Y;

			*ptr = vertex->XYZ;
			ptr++;

			writer_vertex_data = this->mCorrector->CloneWithRealCoordinate(vertex);
			this->mDbWriter->VertexInfoToDB(dynamic_cast<TDXFVertex *>(writer_vertex_data->Entity), entity_id);
		}

		if (pLwPolyLine->Closed) {
			*ptr = *points;
		}

		// ---- Generate lines by vertexes -------------------------------------
		if (pLwPolyLine->Count > 1) {
			++level;
			this->hierarchyLevel.push(level);

			this->WritePointsAsLinesToDB(writer_data->Entity, points, points_cnt, pLayerId, entity_id);
		}
		this->UpdateContainerGeoInfo(writer_data, entity_id);
	}
	__finally {
		delete[]points;
		delete vertex;

		this->mCorrector->ChangeData = crnt_change_data;

		while (stack_size < this->hierarchyLevel.size()) {
			this->hierarchyLevel.pop();
		}
	}
}
// -----------------------------------------------------------------------------

// -------- UpdateMapParams ----------------------------------------------------
void __fastcall TDXFtoDbLoader::UpdateMapParams(SQLiteConnection *pConn) {
	TDXFToDBWriter *db_writer = NULL;
	MapParametersRecord *map_params;

	try {
		SQLiteReader reader = this->mDbReader->GetMapParametersRecordReader(pConn);

		if (reader.Read()) {
			map_params = this->mDbReader->MapParamsFromDb(&reader);
		}
		else {
//			TRadarMapManager *mngr = dynamic_cast<TRadarMapManager *>(this->mManager);

			this->mDbWriter->EmptyMapParametersToDB(this->mThread->CoordinateSystem);
			reader.Reset();
			reader.Read();
			map_params = this->mDbReader->MapParamsFromDb(&reader);
		}

		reader.Close();

		reader = this->mDbReader->GetFilesIntegratedCornersReader(pConn);

		if (reader.Read()) {
			map_params->mLowerCorner.X = reader.GetDouble("lc_X");
			map_params->mLowerCorner.Y = reader.GetDouble("lc_Y");

			map_params->mUpperCorner.X = reader.GetDouble("uc_X");
			map_params->mUpperCorner.Y = reader.GetDouble("uc_Y");

			map_params->mMinCorner.X = reader.GetDouble("min_c_X");
			map_params->mMinCorner.Y = reader.GetDouble("min_c_Y");
			map_params->mMinCorner.Z = reader.GetDouble("min_c_Z");

			map_params->mMaxCorner.X = reader.GetDouble("max_c_X");
			map_params->mMaxCorner.Y = reader.GetDouble("max_c_Y");
			map_params->mMaxCorner.Z = reader.GetDouble("max_c_Z");

			map_params->mCS = this->mThread->CoordinateSystem;

			if (this->mDbWriter) {
				db_writer = this->mDbWriter;
			}
			else {
				db_writer = new TDXFToDBWriter(pConn);
			}
			db_writer->MapParametersInfoToDB(map_params);
		}
	}
	catch(...) {
		this->FreeDbObject(NULL, db_writer, NULL);
		throw;
	}

	this->FreeDbObject(NULL, db_writer, NULL);
}
// -----------------------------------------------------------------------------

// -------- DeleteDxfFromDB ----------------------------------------------------
void __fastcall TDXFtoDbLoader::DeleteDxfFromDB(AnsiString pFileName) {
	SQLiteConnection* conn = NULL;
	UnicodeString del_name = pFileName;

	try {
		conn = this->GetDbConnect();

		if (this->mProgressBar) {
			this->mProgressBar->StepIt();
		}

		conn->BeginTransaction();

		if ((del_name == NULL) || (del_name.Length() < 1)) {
			 del_name = this->mFileName;
		}

		this->DeleteFileRecordFromDb(conn, ExtractFileName(del_name), ExtractFilePath(del_name));

		if (this->mProgressBar) {
			this->mProgressBar->Position += 2;
		}

		this->UpdateMapParams(conn);

		if (this->mProgressBar) {
			this->mProgressBar->StepIt();
		}

		if (this->CheckTerminated()) {
			conn->Rollback();
		}
		else {
			conn->Commit();
		}
	}
	catch(...) {
		if (conn) {
			conn->Rollback();
		}
		this->FreeDbObject(conn, NULL, NULL);
		throw;
	}

	if (this->mProgressBar) {
		this->mProgressBar->Position += 2;
	}
	this->FreeDbObject(conn, NULL, NULL);
}
// -----------------------------------------------------------------------------

// -------- DeleteFileRecordFromDb ---------------------------------------------
void __fastcall TDXFtoDbLoader::DeleteFileRecordFromDb(SQLiteConnection *pConn, UnicodeString pFileName, UnicodeString pFileDir) {
	AnsiString sql_text = "DELETE FROM file WHERE FileName = :f_name AND FileDir = :f_dir";
	SQLiteCommand *cmd = NULL;

	try {
		cmd = new SQLiteCommand(pConn, sql_text);

		cmd->BindStringByName(":f_name", pFileName);
		cmd->BindStringByName(":f_dir", pFileDir);

		cmd->ExecuteNonQuery();

		this->FreeDbObject(NULL, NULL, cmd);
	}
	catch(...) {
		this->FreeDbObject(NULL, NULL, cmd);
		throw;
	}
}
// -----------------------------------------------------------------------------

// -------- CreateGeoIndex -----------------------------------------------------
bool __fastcall TDXFtoDbLoader::CreateGeoIndex() {
	SQLiteConnection* conn = NULL;
	SQLiteCommand *cmd_select = NULL;
	int step_progress = 0, geo_data_count = 0;
	bool res = false;

	try {
		conn = this->GetDbConnect();

		if (NULL == this->mDbWriter) {
			this->mDbWriter = new TDXFToDBWriter(conn);
		}

		if (this->mProgressBar) {
			geo_data_count = this->CalcGeoIndexItems(conn);
			step_progress = geo_data_count / 100;
			this->mProgressBar->Max += geo_data_count + 2 * step_progress;
		}

		// ---- Select geo data from table 'entity' ----------------------------
		AnsiString sql_text = "SELECT EntityId, Min_X, Max_X, Min_Y, Max_Y FROM entity;";

		cmd_select = new SQLiteCommand(conn, sql_text);
		SQLiteReader reader = cmd_select->ExecuteReader();

		if (this->mProgressBar) {
			this->mProgressBar->Position += step_progress;
		}

		// ---- Clear table 'geo_index' ----------------------------------------
		conn->BeginTransaction();

		sql_text = "DELETE FROM geo_index;";
		conn->ExecuteNonQuery(sql_text);

		if (this->mProgressBar) {
			this->mProgressBar->Position += step_progress;
		}

		// ---- Write "geo_index' records to database --------------------------
		if (this->CheckTerminated() != true) {

			while (reader.Read()) {
				if (this->CheckTerminated()) {
					break;
				}

				this->mDbWriter->GeoIndexInfoToDB(&reader);
				if (this->mProgressBar) {
					this->mProgressBar->Position++;
				}
			}
		}
		reader.Close();

		if (this->CheckTerminated()) {
			conn->Rollback();
		}
		else {
			conn->Commit();
			res = true;
		}
	}
	catch(...) {
		if (conn) {
			conn->Rollback();
		}
		this->FreeDbObject(conn, this->mDbWriter, cmd_select);
		throw;
	}

	this->FreeDbObject(conn, this->mDbWriter, cmd_select);

	return res;
}
// -----------------------------------------------------------------------------

// -------- CalcGeoIndexItems --------------------------------------------------
int __fastcall TDXFtoDbLoader::CalcGeoIndexItems(SQLiteConnection *pConn) {
	int items_count = 0;
	AnsiString sql_text = "SELECT COUNT(*) FROM entity WHERE entity.EntityType NOT IN (1, 10);";

	items_count = pConn->ExecuteInt(sql_text);

	return items_count;
}
// -----------------------------------------------------------------------------

// -------- FreeDbObject -------------------------------------------------------
void __fastcall TDXFtoDbLoader::FreeDbObject(SQLiteConnection *pConn, TDXFToDBWriter *pDbWriter, SQLiteCommand *pCmd) {

	if (pCmd) {
		delete pCmd;
	}

	if (pDbWriter) {
		if (pDbWriter == this->mDbWriter) {
			delete this->mDbWriter;
			this->mDbWriter = NULL;
		}
		else {
			delete pDbWriter;
		}
	}

	if (pConn) {
		delete pConn;
	}
}
// -----------------------------------------------------------------------------

// -------- CheckTerminated ----------------------------------------------------
bool __fastcall TDXFtoDbLoader::CheckTerminated() {
	return this->mThread->CheckTerminated();
}
// -----------------------------------------------------------------------------
