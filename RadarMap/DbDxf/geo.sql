BEGIN TRANSACTION;
CREATE TABLE `property_data` (
	`PropertyDataId`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`ParentId`	INTEGER,
	`ParentTable`	TEXT,
	`PropertyName`	TEXT,
	`PropertyValue`	TEXT
);
CREATE TABLE "map_params" (
	`MapParamsId`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`VersionDB`	INTEGER DEFAULT 1,
	`CoordinateSystem`	INTEGER,
	`LowerCorner_X`	REAL DEFAULT 0,
	`LowerCorner_Y`	REAL DEFAULT 0,
	`UpperCorner_X`	REAL DEFAULT 0,
	`UpperCorner_Y`	REAL DEFAULT 0,
	`MinCorner_X`	REAL DEFAULT 0,
	`MinCorner_Y`	REAL DEFAULT 0,
	`MinCorner_Z`	REAL DEFAULT 0,
	`MaxCorner_X`	REAL DEFAULT 0,
	`MaxCorner_Y`	REAL DEFAULT 0,
	`MaxCorner_Z`	REAL DEFAULT 0,
	`ChangeCornersDate`	TEXT DEFAULT CURRENT_TIMESTAMP
);
CREATE TABLE "layer" (
	`LayerId`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`FileId`	INTEGER,
	`LayerType`	INTEGER,
	`IsVisible`	INTEGER DEFAULT 1,
	`MinCorner_X`	REAL,
	`MinCorner_Y`	REAL DEFAULT 0,
	`MinCorner_Z`	REAL DEFAULT 0,
	`MaxCorner_X`	REAL DEFAULT 0,
	`MaxCorner_Y`	REAL DEFAULT 0,
	`MaxCorner_Z`	REAL DEFAULT 0,
	`Handle`	INTEGER,
	`Color`	INTEGER DEFAULT 0,
	`TextHeightCoef`	REAL,
	`Name`	TEXT,
	`Description`	TEXT,
	`Comment`	TEXT,
	`Flags`	TEXT,
	FOREIGN KEY(`FileId`) REFERENCES `file`(`FileId`) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE VIRTUAL TABLE geo_index USING rtree(
   EntityId,        -- Integer primary key
   Min_X, Max_X,    -- Minimum and maximum X coordinate
   Min_Y, Max_Y     -- Minimum and maximum Y coordinate
);
CREATE TABLE "file" (
	`FileId`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`FileName`	TEXT NOT NULL,
	`FileDir`	TEXT,
	`FileCreateDate`	INTEGER,
	`FileSize`	INTEGER,
	`AutocadVersion`	TEXT,
	`LowerCorner_X`	REAL,
	`LowerCorner_Y`	REAL,
	`UpperCorner_X`	REAL,
	`UpperCorner_Y`	REAL,
	`MinCorner_X`	REAL,
	`MinCorner_Y`	REAL,
	`MinCorner_Z`	REAL,
	`MaxCorner_X`	REAL,
	`MaxCorner_Y`	REAL,
	`MaxCorner_Z`	REAL,
	`LoadDate`	TEXT DEFAULT CURRENT_TIMESTAMP
);
CREATE TABLE "entity_vertex" (
	`EntityVertexId`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`EntityId`	INTEGER,
	`Point_X`	REAL,
	`Point_Y`	REAL,
	`Point_Z`	REAL,
            FOREIGN KEY(EntityId) REFERENCES `entity`(EntityId) ON DELETE CASCADE  ON UPDATE CASCADE
);
CREATE TABLE `entity_ex_group` (
	`EntityExGroupId`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	`EntityId`	INTEGER NOT NULL,
	`SeqNumInGroup`	INTEGER NOT NULL,
	`GroupCode`	INTEGER NOT NULL,
	`Value`	TEXT,
	FOREIGN KEY(`EntityId`) REFERENCES `entity`(`EntityId`) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE TABLE "entity" (
	`EntityId`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`EntityType`	INTEGER DEFAULT 0,
	`LayerId`	INTEGER,
	`Level`	INTEGER DEFAULT 0,
	`ParentEntityId`	INTEGER,
	`Handle`	INTEGER,
	`Point_X`	REAL DEFAULT 0,
	`Point_Y`	REAL DEFAULT 0,
	`Point_Z`	REAL DEFAULT 0,
	`Color`	INTEGER DEFAULT 0,
	`TextHeightCoef`	REAL,
	`Name`	TEXT,
	`Description`	TEXT,
	`Comment`	TEXT,
	`Flags`	TEXT,
	`ExtraPoint_X`	REAL,
	`ExtraPoint_Y`	REAL,
	`ExtraPoint_Z`	REAL,
	`Radius`	REAL,
	`Start`	REAL,
	`Stop`	REAL,
	`Mn2Mj`	REAL,
	`Text`	TEXT,
	`Style`	TEXT,
	`Height`	REAL,
	`TextAngle`	REAL,
	`HorAlignment`	INTEGER,
	`VertAlignment`	INTEGER,
	`Closed`	INTEGER,
	`PolyLine3D`	INTEGER,
	`Thickness`	REAL,
	`Plinegen`	INTEGER,
	`Width`	REAL,
	`BlockName`	TEXT,
	`Angle`	REAL,
	`Scale_X`	REAL,
	`Scale_Y`	REAL,
	`Scale_Z`	REAL,
	`Min_X`	REAL DEFAULT 0,
	`Max_X`	REAL DEFAULT 0,
	`Min_Y`	REAL DEFAULT 0,
	`Max_Y`	REAL DEFAULT 0,
	FOREIGN KEY(`LayerId`) REFERENCES `layer` ( LayerId ) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE INDEX `vertex_entity` ON "entity_vertex" (`EntityId` );
CREATE INDEX `property_Parent` ON `property_data` (`ParentId` ASC, `ParentTable` ASC);
CREATE INDEX `group_entity` ON `entity_ex_group` (`EntityId` ASC);
CREATE INDEX `entity_layer` ON `entity` (`LayerId` )













































;
COMMIT;
