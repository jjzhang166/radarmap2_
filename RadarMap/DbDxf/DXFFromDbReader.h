// -----------------------------------------------------------------------------
// DXFFromDBReader.h
// -----------------------------------------------------------------------------

#ifndef DXFFromDbReaderH
#define DXFFromDbReaderH

#include "DbDxfStructures.h"
#include "DXFMap.h"
#include "SQLiteTools.h"
#include <vector>

// -----------------------------------------------------------------------------
// TDXFFromDBReader
// -----------------------------------------------------------------------------
class TDXFFromDbReader {

private:
	MapParametersRecord mMapParamsRecord;
	bool mIsFillMapParams;
	FileRecord mFileRecord;

	TDXFArc *arc;
	TDXFCircle *circle;
	TDXFEllipse *ellipse;
	TDXFInsert *insert;
	TDXFLine *line;
	TDXFLwPolyLine *lwpline;
	TDXFPoint *point;
	TDXFPolyLine *pline;
	TDXFText *text;
	TDXFVertex *vertex;

	void __fastcall SetEntityData(SQLiteReader *pReader, TDXFEntity *pEntity);
	AnsiString __fastcall GetDbIdListAsString(std::vector<int> *pDbIdLayers);
	TDXFLayerType __fastcall IntToLayerType(int srcLayerType);

public:
	__fastcall TDXFFromDbReader() {
		mIsFillMapParams = false;

		arc = NULL;
		circle = NULL;
		ellipse = NULL;
		insert = NULL;
		line = NULL;
		lwpline = NULL;
		point = NULL;
		pline = NULL;
		text = NULL;
		vertex = NULL;
	};

	__fastcall ~TDXFFromDbReader();

	SQLiteReader __fastcall GetMapParametersRecordReader(SQLiteConnection *pConn);
	SQLiteReader __fastcall GetFilesIntegratedCornersReader(SQLiteConnection *pConn);
	SQLiteReader __fastcall GetFileInfoReader(SQLiteConnection *pConn, UnicodeString pFileName = "", UnicodeString pFileDir = "");
	SQLiteReader __fastcall GetLayersFileNamesReader(SQLiteConnection *pConn);
	SQLiteReader __fastcall GetLayerInfoReader(SQLiteConnection *pConn, int pIdFile);
	SQLiteReader __fastcall GetLayerEntitiesReader(SQLiteConnection *pConn, int pIdLayer, bool pUsedShowArea = false, TDoubleRect pShowArea = TDoubleRect());
	SQLiteReader __fastcall GetRectEntitiesReader(SQLiteConnection *pConn, TDoubleRect pShowArea, std::vector<int> *pDbIdLayers);
	SQLiteReader __fastcall GetExGroupsReader(SQLiteConnection *pConn, std::vector<int> *pDbIdRecords);

	MapParametersRecord * __fastcall MapParamsFromDb(SQLiteReader *pReader);
	TDXFCollection * __fastcall IntegratedMapInfoFromDb(SQLiteReader *pReader);
	TDXFCollection * __fastcall FileInfoFromDb(SQLiteReader *pReader);
	FileRecord * __fastcall FileRecordFromDb(SQLiteReader *pReader);
	TDXFArc * __fastcall ArcInfoFromDb(SQLiteReader *pReader);
	TDXFCircle * __fastcall CircleInfoFromDb(SQLiteReader *pReader);
	TDXFEllipse * __fastcall EllipseInfoFromDb(SQLiteReader *pReader);
	TDXFInsert * __fastcall InsertInfoFromDb(SQLiteReader *pReader);
	TDXFLayer * __fastcall LayerInfoFromDb(SQLiteReader *pReader);
	TDXFLine * __fastcall LineInfoFromDb(SQLiteReader *pReader);
	TDXFLwPolyLine * __fastcall LwPolyLineInfoFromDb(SQLiteReader *pReader);
	TDXFPoint * __fastcall PointInfoFromDb(SQLiteReader *pReader);
	TDXFPolyLine * __fastcall PolyLineInfoFromDb(SQLiteReader *pReader);
	TDXFText * __fastcall TextInfoFromDb(SQLiteReader *pReader);
};
//------------------------------------------------------------------------------
#endif
