// -----------------------------------------------------------------------------
// DXFToDBWriter.h
// -----------------------------------------------------------------------------

#ifndef DXFToDBWriterH
#define DXFToDBWriterH

// #include "Defines.h"
#include "DXFMap.h"
#include "DbDxfStructures.h"
#include "SQLiteTools.h"

// ------------------------------------------------------------------------------
class TDXFToDBWriter {

private:
	SQLiteConnection *conn;
	SQLiteCommand *cmdLayerInsert;
	SQLiteCommand *cmdLayerTypeUpdate;
	SQLiteCommand *cmdLayerVisibleUpdate;
	SQLiteCommand *cmdPropertyInsert;
	SQLiteCommand *cmdPolyLineInsert;
	SQLiteCommand *cmdLineInsert;
	SQLiteCommand *cmdArcInsert;
	SQLiteCommand *cmdCircleInsert;
	SQLiteCommand *cmdEllipseInsert;
	SQLiteCommand *cmdInsertInsert;
	SQLiteCommand *cmdLwPolyLineInsert;
	SQLiteCommand *cmdVertexInsert;
	SQLiteCommand *cmdExGroupInsert;
	SQLiteCommand *cmdPointInsert;
	SQLiteCommand *cmdTextInsert;
	SQLiteCommand *cmdGeoIndexInsert;
	SQLiteCommand *cmdEntityGeoDataUpdate;
	SQLiteCommand *cmdEntityChildSelect;
	int layerId;

	void __fastcall SetEntityDataToCmdInsert(EntityWriterData *pWriteData, SQLiteCommand *pCmd, int pLayerId, int pParentId, int pLevel);

public:
	__fastcall TDXFToDBWriter(SQLiteConnection *pConn) {
		conn = pConn;

		cmdLayerInsert = NULL;
		cmdLayerTypeUpdate = NULL;
		cmdLayerVisibleUpdate = NULL;
		cmdPropertyInsert = NULL;
		cmdPolyLineInsert = NULL;
		cmdLineInsert = NULL;
		cmdArcInsert = NULL;
		cmdCircleInsert = NULL;
		cmdEllipseInsert = NULL;
		cmdInsertInsert = NULL;
		cmdLwPolyLineInsert = NULL;
		cmdVertexInsert = NULL;
		cmdExGroupInsert = NULL;
		cmdPointInsert = NULL;
		cmdTextInsert = NULL;
		cmdGeoIndexInsert = NULL;
		cmdEntityGeoDataUpdate = NULL;
		cmdEntityChildSelect = NULL;
	};

	__fastcall ~TDXFToDBWriter();

	__property int LayerId = {
		read = layerId, write = layerId
	};

	int __fastcall EmptyMapParametersToDB(TCoordinateSystem pCS);
	int __fastcall MapParametersInfoToDB(MapParametersRecord *pMapParams);
	int __fastcall FileInfoToDB(TDXFCollection *pDxf);
	int __fastcall LayerInfoToDB(TDXFLayer *pLayer, int pFileId);
	void __fastcall LayerTypeInfoToDB(TDXFLayer *pLayer);
	void __fastcall LayerVisibleInfoToDB(TDXFLayer *pLayer);
	int __fastcall PropertyInfoToDB(int pParentId, const char *pTable, AnsiString pKey, AnsiString pValue);

	SQLiteReader __fastcall SelectEntityChildGeoInfo(int pDbRecordId);
	void __fastcall UpdateEntityGeoInfo(EntityWriterData *pWriteData, int pDbRecordId);

	int __fastcall ArcInfoToDB(EntityWriterData *pArc, int pLayerId, int pParentId, int pLevel);
	int __fastcall CircleInfoToDB(EntityWriterData *pCircle, int pLayerId, int pParentId, int pLevel);
	int __fastcall EllipseInfoToDB(EntityWriterData *pEllipse, int pLayerId, int pParentId, int pLevel);
	int __fastcall InsertInfoToDB(EntityWriterData *pInsert, int pLayerId, int pParentId, int pLevel);
	int __fastcall LineInfoToDB(EntityWriterData *pLine, int pLayerId, int pParentId, int pLevel);
	int __fastcall LwPolyLineInfoToDB(EntityWriterData *pLwPolyLine, int pLayerId, int pParentId, int pLevel);
	int __fastcall PointInfoToDB(EntityWriterData *pPoint, int pLayerId, int pParentId, int pLevel);
	int __fastcall PolyLineInfoToDB(EntityWriterData *pPolyLine, int pLayerId, int pParentId, int pLevel);
	int __fastcall TextInfoToDB(EntityWriterData *pText, int pLayerId, int pParentId, int pLevel);
	int __fastcall VertexInfoToDB(TDXFVertex *pVertex, int pParentId);
	int __fastcall ExGroupInfoToDB(TDXFGroup *pGroup, int pParentId, int pSeqNumber);

	void __fastcall GeoIndexInfoToDB(SQLiteReader *pGeoData);

};
// ------------------------------------------------------------------------------
#endif
