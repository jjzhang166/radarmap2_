
// -----------------------------------------------------------------------------
// DXFFromDbReader.cpp
// -----------------------------------------------------------------------------

#pragma hdrstop

#include "DXFFromDbReader.h"

// -----------------------------------------------------------------------------

#pragma package(smart_init)

//------------------------------------------------------------------------------
// TDXFFromDbReader
//------------------------------------------------------------------------------

// -------- Constructor / Destructor -------------------------------------------
__fastcall TDXFFromDbReader::~TDXFFromDbReader() {

	if (this->arc) {
		delete this->arc;
	}
	if (this->circle) {
		delete this->circle;
	}
	if (this->ellipse) {
		delete this->ellipse;
	}
	if (this->insert) {
		delete this->insert;
	}
	if (this->line) {
		delete this->line;
	}
	if (this->lwpline) {
		delete this->lwpline;
	}
	if (this->point) {
		delete this->point;
	}
	if (this->pline) {
		delete this->pline;
	}
	if (this->text) {
		delete this->text;
	}
	if (this->vertex) {
		delete this->vertex;
	}
}
// -----------------------------------------------------------------------------

// -------- GetMapParametersRecordReader ---------------------------------------
SQLiteReader __fastcall TDXFFromDbReader::GetMapParametersRecordReader(SQLiteConnection *pConn) {
	SQLiteCommand *cmd = new SQLiteCommand(pConn, "SELECT * FROM map_params");

	return cmd->ExecuteReaderFreeCommand();
}
// -----------------------------------------------------------------------------

// -------- MapParamsFromDb ----------------------------------------------------
MapParametersRecord * __fastcall TDXFFromDbReader::MapParamsFromDb(SQLiteReader *pReader) {
	MapParametersRecord *result = &this->mMapParamsRecord;

	result->mIdDbRecord = pReader->GetInt("MapParamsId");
	result->mDbShemeVersion = pReader->GetInt("VersionDB");
	result->mCS = pReader->GetInt("CoordinateSystem");

	result->mLowerCorner.X = pReader->GetDouble("LowerCorner_X");
	result->mLowerCorner.Y = pReader->GetDouble("LowerCorner_Y");

	result->mUpperCorner.X = pReader->GetDouble("UpperCorner_X");
	result->mUpperCorner.Y = pReader->GetDouble("UpperCorner_Y");

	if (pReader->IsNull("MinCorner_X") != true) {
		result->mMinCorner.X = pReader->GetDouble("MinCorner_X");
		result->mMinCorner.Y = pReader->GetDouble("MinCorner_Y");
		result->mMinCorner.Z = pReader->GetDouble("MinCorner_Z");
	}

	if (pReader->IsNull("MaxCorner_X") != true) {
		result->mMaxCorner.X = pReader->GetDouble("MaxCorner_X");
		result->mMaxCorner.Y = pReader->GetDouble("MaxCorner_Y");
		result->mMaxCorner.Z = pReader->GetDouble("MaxCorner_Z");
	}
	result->mChangeDate = pReader->GetString("ChangeCornersDate");

	this->mIsFillMapParams = true;

	return result;
}
// -----------------------------------------------------------------------------

// -------- IntegratedMapInfoFromDb --------------------------------------------
TDXFCollection * __fastcall TDXFFromDbReader::IntegratedMapInfoFromDb(SQLiteReader *pReader) {
	TDXFCollection *dxf = new TDXFCollection();

	if (this->mIsFillMapParams != true) {
		this->MapParamsFromDb(pReader);
	}

	dxf->IdDbRecord = this->mMapParamsRecord.mIdDbRecord;

	dxf->LowerCorner.X = this->mMapParamsRecord.mLowerCorner.X;
	dxf->LowerCorner.Y = this->mMapParamsRecord.mLowerCorner.Y;

	dxf->UpperCorner.X = this->mMapParamsRecord.mUpperCorner.X;
	dxf->UpperCorner.Y = this->mMapParamsRecord.mUpperCorner.Y;

	dxf->ApplyMinCorner(this->mMapParamsRecord.mMinCorner);
	dxf->ApplyMaxCorner(this->mMapParamsRecord.mMaxCorner);

	return dxf;
}
// -----------------------------------------------------------------------------

// -------- GetFilesIntegratedCornersReader ------------------------------------
SQLiteReader __fastcall TDXFFromDbReader::GetFilesIntegratedCornersReader(SQLiteConnection *pConn) {
	SQLiteCommand *cmd;
	UnicodeString sql_text = "SELECT";

	sql_text += "  MIN(LowerCorner_X) AS lc_X, MIN(LowerCorner_Y) AS lc_Y, MAX(UpperCorner_X) AS uc_X, Max(UpperCorner_Y) AS uc_Y,";
	sql_text += "  MIN(NullIf(MinCorner_X, 1.0E+300)) AS min_c_X, MIN(NullIf(MinCorner_Y, 1.0E+300)) AS min_c_Y,";
	sql_text += "  MIN(NullIf(MinCorner_Z, 1.0E+300)) AS min_c_Z,";
	sql_text += "  MAX(NullIf(MaxCorner_X, -1.0E+300)) AS max_c_X, Max(NullIf(MaxCorner_Y, -1.0E+300)) AS max_c_Y,";
	sql_text += "  Max(NullIf(MaxCorner_Z, -1E+300))  AS max_c_Z";
	sql_text += " FROM  file;";

	cmd = new SQLiteCommand(pConn, sql_text);

	return cmd->ExecuteReaderFreeCommand();
}
// -----------------------------------------------------------------------------

// -------- GetFileInfoReader --------------------------------------------------
SQLiteReader __fastcall TDXFFromDbReader::GetFileInfoReader(SQLiteConnection *pConn, UnicodeString pFileName, UnicodeString pFileDir) {
	SQLiteCommand *cmd = NULL;

	if ((pFileName.Length() > 0) && (pFileDir.Length() > 0)) {
		cmd = new SQLiteCommand(pConn, "SELECT * FROM file WHERE FileName = :f_name AND FileDir = :f_dir");
		cmd->BindStringByName(":f_name", pFileName);
		cmd->BindStringByName(":f_dir", pFileDir);
	}
	else if (pFileName.Length() > 0) {
		cmd = new SQLiteCommand(pConn, "SELECT * FROM file WHERE FileName = :f_name");
		cmd->BindStringByName(":f_name", pFileName);
	}
	else {
		cmd = new SQLiteCommand(pConn, "SELECT * FROM file");
	}

	return cmd->ExecuteReaderFreeCommand();
}
// -----------------------------------------------------------------------------

// -------- FileInfoFromDb -----------------------------------------------------
TDXFCollection * __fastcall TDXFFromDbReader::FileInfoFromDb(SQLiteReader *pReader) {
	TDXFCollection *dxf = new TDXFCollection();
	double x, y, z;

	dxf->IdDbRecord = pReader->GetInt("FileId");
	dxf->DXFFileName = pReader->GetString("FileName");

	dxf->LowerCorner.X = pReader->GetDouble("LowerCorner_X");
	dxf->LowerCorner.Y = pReader->GetDouble("LowerCorner_Y");

	dxf->UpperCorner.X = pReader->GetDouble("UpperCorner_X");
	dxf->UpperCorner.Y = pReader->GetDouble("UpperCorner_Y");

	if (pReader->IsNull("MinCorner_X") != true) {
		x = pReader->GetDouble("MinCorner_X");
		y = pReader->GetDouble("MinCorner_Y");
		z = pReader->GetDouble("MinCorner_Z");

		dxf->ApplyMinCorner(TDouble3DPoint(x, y, z));
	}

	if (pReader->IsNull("MaxCorner_X") != true) {
		x = pReader->GetDouble("MaxCorner_X");
		y = pReader->GetDouble("MaxCorner_Y");
		z = pReader->GetDouble("MaxCorner_Z");

		dxf->ApplyMaxCorner(TDouble3DPoint(x, y, z));
	}

	return dxf;
}
// -----------------------------------------------------------------------------

// -------- FileRecordFromDb ---------------------------------------------------
FileRecord * __fastcall TDXFFromDbReader::FileRecordFromDb(SQLiteReader *pReader) {
	FileRecord *result = &this->mFileRecord;
	double x, y, z;

	result->mIdDbRecord = pReader->GetInt("FileId");
	result->mFileName = pReader->GetString("FileName");
	result->mFileDir = pReader->GetString("FileDir");
	result->mFileCreateDate = pReader->GetInt("FileCreateDate");
	result->mFileSize = pReader->GetInt("FileSize");
	result->mAutocadVersion = pReader->GetString("FileName");
	result->mLoadDate = pReader->GetString("LoadDate");

	result->mLowerCorner.X = pReader->GetDouble("LowerCorner_X");
	result->mLowerCorner.Y = pReader->GetDouble("LowerCorner_Y");

	result->mUpperCorner.X = pReader->GetDouble("UpperCorner_X");
	result->mUpperCorner.Y = pReader->GetDouble("UpperCorner_Y");

	if (pReader->IsNull("MinCorner_X") != true) {
		x = pReader->GetDouble("MinCorner_X");
		y = pReader->GetDouble("MinCorner_Y");
		z = pReader->GetDouble("MinCorner_Z");

		result->mMinCorner = TDouble3DPoint(x, y, z);
		result->mIsFillMinCorner = true;
	}

	if (pReader->IsNull("MaxCorner_X") != true) {
		x = pReader->GetDouble("MaxCorner_X");
		y = pReader->GetDouble("MaxCorner_Y");
		z = pReader->GetDouble("MaxCorner_Z");

		result->mMaxCorner = TDouble3DPoint(x, y, z);
		result->mIsFillMaxCorner = true;
	}

	return result;
}
// -----------------------------------------------------------------------------

// -------- GetLayerInfoReader -------------------------------------------------
SQLiteReader __fastcall TDXFFromDbReader::GetLayerInfoReader(SQLiteConnection *pConn, int pIdFile) {
	SQLiteCommand *cmd = NULL;

	if (pIdFile > 0) {
		cmd = new SQLiteCommand(pConn, "SELECT * FROM layer WHERE FileId = :idFile");
		cmd->BindIntByName(":idFile", pIdFile);
	}
	else {
		cmd = new SQLiteCommand(pConn, "SELECT * FROM layer");
	}

	return cmd->ExecuteReaderFreeCommand();
}
// -----------------------------------------------------------------------------

// -------- LayerInfoFromDb ----------------------------------------------------
TDXFLayer * __fastcall TDXFFromDbReader::LayerInfoFromDb(SQLiteReader *pReader) {
	TDXFLayer *layer = new TDXFLayer(NULL, pReader->GetString("Name"));
	double x, y, z;

	layer->ChangeLayerType(this->IntToLayerType(pReader->GetInt("LayerType")));
	layer->Visible = (1 == pReader->GetInt("IsVisible")) ? true : false;

	layer->IdDbRecord = pReader->GetInt("LayerId");
	layer->Handle = pReader->GetInt("Handle");
	layer->Color = pReader->GetInt("Color");
	layer->TextHeightCoef = pReader->GetDouble("TextHeightCoef");
	layer->Description = pReader->GetString("Description");
	layer->Comment = pReader->GetString("Comment");
	layer->Flags = pReader->GetString("Flags");

	if (pReader->IsNull("MinCorner_X") != true) {
		x = pReader->GetDouble("MinCorner_X");
		y = pReader->GetDouble("MinCorner_Y");
		z = pReader->GetDouble("MinCorner_Z");
		layer->ApplyMinCorner(TDouble3DPoint(x, y, z));
	}

	if (pReader->IsNull("MaxCorner_X") != true) {
		x = pReader->GetDouble("MaxCorner_X");
		y = pReader->GetDouble("MaxCorner_Y");
		z = pReader->GetDouble("MaxCorner_Z");
		layer->ApplyMaxCorner(TDouble3DPoint(x, y, z));
	}

	return layer;
}
// -----------------------------------------------------------------------------

// -------- IntToLayerType -----------------------------------------------------
TDXFLayerType __fastcall TDXFFromDbReader::IntToLayerType(int srcLayerType) {
	TDXFLayerType l_type = dltNone;

	switch(srcLayerType) {
	case 1:
		l_type = dltTopography;
		break;
	case 2:
		l_type = dltUtility;
		break;
	case 3:
		l_type = dltAnnotation;
		break;
	case 4:
		l_type = dltAutoCAD;
		break;
	default:
		break;
	}

	return l_type;
}
//------------------------------------------------------------------------------

// -------- GetLayersFileNamesReader -------------------------------------------
SQLiteReader __fastcall TDXFFromDbReader::GetLayersFileNamesReader(SQLiteConnection *pConn) {
	AnsiString sql_text = "SELECT l.LayerId, f.FileName FROM layer l, file f WHERE l.FileId = f.FileId";
	SQLiteCommand *cmd = new SQLiteCommand(pConn, sql_text);

	return cmd->ExecuteReaderFreeCommand();
}
//------------------------------------------------------------------------------

// -------- GetLayerEntitiesReader ---------------------------------------------
SQLiteReader __fastcall TDXFFromDbReader::GetLayerEntitiesReader(SQLiteConnection *pConn, int pIdLayer, bool pUsedShowArea,
	TDoubleRect pShowArea) {

	SQLiteCommand *cmd = NULL;

	if (! pUsedShowArea) {
		cmd = new SQLiteCommand(pConn, "SELECT * FROM entity WHERE LayerId = :pId  ORDER BY EntityId");
		cmd->BindIntByName(":pId", pIdLayer);
	}
	else {
		AnsiString sql_text = "";

		sql_text = "SELECT e.* FROM entity e, geo_index g";
		sql_text += " WHERE e.LayerId = :pId AND e.EntityId = g.entityId";
		sql_text += "   AND g.max_X >= :area_min_X AND g.min_X <= :area_max_X";
		sql_text += "   AND g.max_Y >= :area_min_Y AND g.min_Y <= :area_max_Y";

		cmd = new SQLiteCommand(pConn, sql_text);
		cmd->BindIntByName(":pId", pIdLayer);
		cmd->BindDoubleByName(":area_min_X", pShowArea.Left);
		cmd->BindDoubleByName(":area_max_X", pShowArea.Right);
		cmd->BindDoubleByName(":area_min_Y", pShowArea.Bottom);
		cmd->BindDoubleByName(":area_max_Y", pShowArea.Top);
	}

	return cmd->ExecuteReaderFreeCommand();
}
//------------------------------------------------------------------------------

// -------- GetRectEntitiesReader ----------------------------------------------
SQLiteReader __fastcall TDXFFromDbReader::GetRectEntitiesReader(SQLiteConnection *pConn, TDoubleRect pShowArea, std::vector<int> *pDbIdLayers) {
	SQLiteCommand *cmd = NULL;
	AnsiString sql_text = "";

	sql_text = "SELECT e.*";
	sql_text += " FROM entity e NOT INDEXED,";
	sql_text += "      (SELECT g.entityId  FROM geo_index g";
	sql_text += "         WHERE g.max_X >= :area_min_X AND g.min_X <= :area_max_X";
	sql_text += "           AND g.max_Y >= :area_min_Y AND g.min_Y <= :area_max_Y) g1";
	sql_text += " WHERE e.EntityId = g1.entityId";

	if (pDbIdLayers->size() > 1) {
		sql_text += " AND e.LayerId IN (" + this->GetDbIdListAsString(pDbIdLayers) + ")";
	}
	else if (pDbIdLayers->size() == 1) {
		sql_text += " AND e.LayerId = :l_db_id";
	}
	sql_text += " ORDER BY EntityId";

	cmd = new SQLiteCommand(pConn, sql_text);

	if (pDbIdLayers->size() == 1) {
		cmd->BindIntByName(":l_db_id", pDbIdLayers->front());
	}
	cmd->BindDoubleByName(":area_min_X", pShowArea.Left);
	cmd->BindDoubleByName(":area_max_X", pShowArea.Right);
	cmd->BindDoubleByName(":area_min_Y", pShowArea.Bottom);
	cmd->BindDoubleByName(":area_max_Y", pShowArea.Top);

	return cmd->ExecuteReaderFreeCommand();
}
//------------------------------------------------------------------------------

// -------- GetRectEntitiesReader ----------------------------------------------
SQLiteReader __fastcall TDXFFromDbReader::GetExGroupsReader(SQLiteConnection *pConn, std::vector<int> *pDbIdRecords) {
	SQLiteCommand *cmd = NULL;
	AnsiString sql_text = "";

	sql_text = "SELECT e.* FROM entity_ex_group e WHERE";

	if (pDbIdRecords->size() > 1) {
		sql_text += " e.EntityId IN (" + this->GetDbIdListAsString(pDbIdRecords) + ")";
	}
	else {
		sql_text += " e.EntityId = :l_db_id";
	}
	sql_text += " ORDER BY EntityId, SeqNumInGroup";

	cmd = new SQLiteCommand(pConn, sql_text);

	if (pDbIdRecords->size() == 1) {
		cmd->BindIntByName(":l_db_id", pDbIdRecords->front());
	}

	return cmd->ExecuteReaderFreeCommand();
}
//------------------------------------------------------------------------------

// -------- GetRectEntitiesReader ----------------------------------------------
AnsiString __fastcall TDXFFromDbReader::GetDbIdListAsString(std::vector<int> *pDbIdLayers) {
	AnsiString id_list = "";
	std::vector<int>::iterator it;
	int id_layer = 0;

	for (it = pDbIdLayers->begin(); it != pDbIdLayers->end(); it++) {
		id_layer = *it;

		if (id_list.Length() > 0) {
            id_list += ",";
		}
		id_list += IntToStr(id_layer);
	}

	return id_list;
}
//------------------------------------------------------------------------------

// -------- ArcInfoFromDb ------------------------------------------------------
TDXFArc * __fastcall TDXFFromDbReader::ArcInfoFromDb(SQLiteReader * pReader) {

	if (NULL == this->arc) {
		this->arc = new TDXFArc(NULL);
	}

	// ---- Read from database -------------------------------------------------
	this->SetEntityData(pReader, this->arc);

	this->arc->R = pReader->GetDouble("Radius");
	this->arc->Start = pReader->GetDouble("Start");
	this->arc->Stop = pReader->GetDouble("Stop");

	return this->arc;
}
//------------------------------------------------------------------------------

// -------- CircleInfoFromDb ---------------------------------------------------
TDXFCircle * __fastcall TDXFFromDbReader::CircleInfoFromDb(SQLiteReader * pReader) {

	if (NULL == this->circle) {
		this->circle = new TDXFCircle(NULL);
	}

	// ---- Read from database -------------------------------------------------
	this->SetEntityData(pReader, this->circle);

	this->circle->R = pReader->GetDouble("Radius");

	return this->circle;
}
//------------------------------------------------------------------------------

// -------- EllipseInfoFromDb --------------------------------------------------
TDXFEllipse * __fastcall TDXFFromDbReader::EllipseInfoFromDb(SQLiteReader *pReader) {

	if (NULL == this->ellipse) {
		this->ellipse = new TDXFEllipse(NULL);
	}

	// ---- Read from database -------------------------------------------------
	this->SetEntityData(pReader, this->ellipse);

	this->ellipse->MjEndPoint.X = pReader->GetDouble("ExtraPoint_X");
	this->ellipse->MjEndPoint.Y = pReader->GetDouble("ExtraPoint_Y");
	this->ellipse->MjEndPoint.Z = pReader->GetDouble("ExtraPoint_Z");
	this->ellipse->Start = pReader->GetDouble("Start");
	this->ellipse->Stop = pReader->GetDouble("Stop");
	this->ellipse->Mn2MjRatio = pReader->GetDouble("Mn2Mj");

	return this->ellipse;
}
//------------------------------------------------------------------------------

// -------- InsertInfoFromDb ---------------------------------------------------
TDXFInsert * __fastcall TDXFFromDbReader::InsertInfoFromDb(SQLiteReader * pReader) {

	if (NULL == this->insert) {
		this->insert = new TDXFInsert(NULL);
	}

	// ---- Read from database -------------------------------------------------
	this->SetEntityData(pReader, this->insert);

	this->insert->Angle = pReader->GetDouble("Angle");
	this->insert->BlockName = pReader->GetString("BlockName");
	this->insert->X_scale = pReader->GetDouble("Scale_X");
	this->insert->Y_scale = pReader->GetDouble("Scale_Y");
	this->insert->Z_scale = pReader->GetDouble("Scale_Z");

	return this->insert;
}
//------------------------------------------------------------------------------

// -------- LineInfoFromDb -----------------------------------------------------
TDXFLine * __fastcall TDXFFromDbReader::LineInfoFromDb(SQLiteReader *pReader) {

	if (NULL == this->line) {
		this->line = new TDXFLine(NULL);
	}

	// ---- Read from database -------------------------------------------------
	this->SetEntityData(pReader, this->line);

	this->line->Point2.X = pReader->GetDouble("ExtraPoint_X");
	this->line->Point2.Y = pReader->GetDouble("ExtraPoint_Y");
	this->line->Point2.Z = pReader->GetDouble("ExtraPoint_Z");

	return this->line;
}
// -----------------------------------------------------------------------------

// -------- LwPolyLineInfoFromDb -----------------------------------------------
TDXFLwPolyLine * __fastcall TDXFFromDbReader::LwPolyLineInfoFromDb(SQLiteReader *pReader) {

	if (NULL == this->lwpline) {
		this->lwpline = new TDXFLwPolyLine(NULL);
	}

	// ---- Read from database -------------------------------------------------
	this->SetEntityData(pReader, this->lwpline);

	this->lwpline->Closed = (0 == pReader->GetInt("Closed"));
	this->lwpline->Plinegen = (0 == pReader->GetInt("Plinegen"));
	this->lwpline->Thickness3D = pReader->GetDouble("Thickness");
	this->lwpline->Width = pReader->GetDouble("Width");

	return this->lwpline;
}
// -----------------------------------------------------------------------------

// -------- PointInfoFromDb ----------------------------------------------------
TDXFPoint * __fastcall TDXFFromDbReader::PointInfoFromDb(SQLiteReader * pReader) {

	if (NULL == this->point) {
		this->point = new TDXFPoint(NULL);
	}

	// ---- Read from database -------------------------------------------------
	this->SetEntityData(pReader, this->point);

	return this->point;
}
// -----------------------------------------------------------------------------

// -------- PolyLineInfoFromDb -------------------------------------------------
TDXFPolyLine * __fastcall TDXFFromDbReader::PolyLineInfoFromDb(SQLiteReader *pReader) {

	if (NULL == this->pline) {
		this->pline = new TDXFPolyLine(NULL);
	}

	// ---- Read from database -------------------------------------------------
	this->SetEntityData(pReader, this->pline);

	this->pline->Thickness3D = pReader->GetDouble("Thickness");
	this->pline->Closed = (0 == pReader->GetInt("Closed"));
	this->pline->Polyline3D = (0 == pReader->GetInt("PolyLine3D"));

	return this->pline;
}
// -----------------------------------------------------------------------------

// -------- TextInfoFromDB -----------------------------------------------------
TDXFText * __fastcall TDXFFromDbReader::TextInfoFromDb(SQLiteReader *pReader) {

	if (NULL == this->text) {
		this->text = new TDXFText(NULL);
	}

	// ---- Read from database -------------------------------------------------
	this->SetEntityData(pReader, this->text);

	this->text->AlignmentPoint.X = pReader->GetDouble("ExtraPoint_X");
	this->text->AlignmentPoint.Y = pReader->GetDouble("ExtraPoint_Y");
	this->text->AlignmentPoint.Z = pReader->GetDouble("ExtraPoint_Z");

	this->text->Text = pReader->GetString("Text");
	this->text->Style = pReader->GetString("Style");
	this->text->Height = pReader->GetDouble("Height");
//	this->text->EnlargeDrawingHeight = (1 == pReader->GetInt("EnlargeDrawingHeight"));
	this->text->Angle = pReader->GetDouble("TextAngle");
	this->text->HorA = (TDXFHorizontalAlignment)pReader->GetInt("HorAlignment");
	this->text->VerA = (TDXFVerticalAlignment)pReader->GetInt("VertAlignment");

	return this->text;
}
// -----------------------------------------------------------------------------

// -------- SetEntityData ------------------------------------------------------
void __fastcall TDXFFromDbReader::SetEntityData(SQLiteReader *pReader, TDXFEntity *pEntity) {

	pEntity->XYZ.X = pReader->GetDouble("Point_X");
	pEntity->XYZ.Y = pReader->GetDouble("Point_Y");
	pEntity->XYZ.Z = pReader->GetDouble("Point_Z");

	pEntity->Handle = pReader->GetInt64("Handle");
	pEntity->Color = pReader->GetInt64("Color");
	pEntity->TextHeightCoef = pReader->GetInt64("TextHeightCoef");
	pEntity->Name = pReader->GetString("Name");
	pEntity->Description = pReader->GetString("Description");
	pEntity->Comment = pReader->GetString("Comment");
	pEntity->Flags = pReader->GetString("Flags");
}
// -----------------------------------------------------------------------------
