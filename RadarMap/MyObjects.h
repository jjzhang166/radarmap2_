//---------------------------------------------------------------------------

#ifndef MyObjectsH
#define MyObjectsH
//---------------------------------------------------------------------------

#include "Defines.h"

enum TMyObjectType {gotNone, gotCustom, gotControl, gotTool, gotInfo, gotUtility,
	gotBackground, gotContainer, gotContainersCollection};

class TMyObject: public TObject
{
private:
	bool objectchanged;
	int tag;
	unsigned long creationtimetick;
	bool freeobjects;
protected:
	bool GlobalPressed, GlobalDragNDrop;
	int groupindex;
	bool AutomaticUpdate;
	int index;
	bool smallsize;
	TMyObjectType objecttype;

	bool __fastcall readObjectChanged() {bool b=objectchanged; objectchanged=false; return b;}
	virtual Types::TRect __fastcall readViewPort() {return Types::TRect(0, 0, 0, 0);}
	virtual void __fastcall writeGroupIndex(int value) {groupindex=value; ApplyGrouping(value, -1);}
	void __fastcall writeIndex(int value) {index=value;}
	virtual void __fastcall SmallResize() {}
	void __fastcall writeSmallSize(bool value) {if(smallsize!=value) {smallsize=value; SmallResize();}}
public:
	__fastcall TMyObject() : TObject() {creationtimetick=::GetTickCount(); objecttype=gotNone; objectchanged=false; GlobalPressed=false; smallsize=false; tag=0; groupindex=0; AutomaticUpdate=true; freeobjects=true;}
	__fastcall ~TMyObject() {}
	virtual void __fastcall Update(TMyObjectType Type) {};
	virtual void __fastcall ZoomUpdate(TMyObjectType Type) {Update(Type);};
	virtual void __fastcall SetObjectChanged(TMyObjectType Type) {objectchanged=true; if(AutomaticUpdate) Update(Type);}
	void __fastcall PresetSmallSize(bool value) {smallsize=value;}

	void __fastcall SetGlobalPressed(bool value) {GlobalPressed=value;}
	virtual int __fastcall AddObject(TMyObject* o) {return -1;}
	virtual void __fastcall RebuildObjects() {}
	virtual void __fastcall ApplyGrouping(int Groupindex, int index) {}
	virtual void __fastcall RemoveChildFromList(TMyObject* o) {}

	virtual void __fastcall TurnOffUpdates() {AutomaticUpdate=false;}
	virtual void __fastcall TurnOnUpdates() {AutomaticUpdate=true;}

	__property TMyObjectType ObjectType = {read=objecttype};
	__property bool ObjectChanged = {read=readObjectChanged};
	__property Types::TRect ViewPort = {read=readViewPort};
	__property int GroupIndex = {read=groupindex, write=writeGroupIndex};
	__property int Index = {read=index, write=writeIndex};
	__property bool SmallSize = {read=smallsize, write=writeSmallSize};
	__property int Tag = {read=tag, write=tag};
	__property unsigned long CreationTimeTick = {read=creationtimetick};
	__property bool FreeObjects = {read=freeobjects, write=freeobjects};
};

class TMyObjectsList: public TMyObject
{
private:
	void __fastcall ArrangeIndexes();
	bool linked;
	bool deleteitemowneracknolegment;
	bool objectsreindexing;
	TBusyLocker *BusyLocker;
protected:
	TList *Objects;
	TMyObject *Owner;
	int __fastcall readObjectsCount();
	TMyObject* __fastcall readMyObjectItem(int Index);
	void __fastcall writeMyObjectItem(int Index, TMyObject* value);
public:
	__fastcall TMyObjectsList(TMyObject *AOwner);
	__fastcall ~TMyObjectsList();
	int __fastcall AddMyObject(TMyObject* o);
	int __fastcall Add(TMyObject* o);
	void __fastcall Delete(int Index);
	void __fastcall Delete(TMyObject* o) {int i=IndexOf(o); if(i>=0) Delete(i);}
	void __fastcall Remove(int Index);
	void __fastcall Remove(TMyObject* o) {int i=IndexOf(o); if(i>=0) Remove(i);}
	void __fastcall Clear(TMyObject *PB=NULL);
	int __fastcall SendToBack(int Index);
	int __fastcall SendToBack(TMyObject* o) {int i=IndexOf(o); if(i>=0) SendToBack(i); return i;}
	int __fastcall BringToFront(int Index);
	int __fastcall BringToFront(TMyObject* o) {int i=IndexOf(o); if(i>=0) BringToFront(i); return i;}
	int __fastcall OneStepBack(int Index);
	int __fastcall OneStepBack(TMyObject* o) {int i=IndexOf(o); if(i>=0) OneStepBack(i); return i;}
	int __fastcall OneStepForward(int Index);
	int __fastcall OneStepForward(TMyObject* o) {int i=IndexOf(o); if(i>=0) OneStepForward(i); return i;}
	int __fastcall IndexOf(TMyObject* o);

	__property TMyObject* Items[int Index] = {read=readMyObjectItem, write=writeMyObjectItem};
	__property int Count = {read=readObjectsCount};
	__property bool Linked = {read=linked, write=linked};
	__property bool DeleteItemOwnerAcknolegment = {read=deleteitemowneracknolegment, write=deleteitemowneracknolegment};
	__property bool ObjectsReIndexing = {read=objectsreindexing, write=objectsreindexing};

	TMyObject* operator[](int Index) { return Items[Index]; }
};

class TMyObjectsNamedList: public TMyObjectsList
{
private:
	TStrings *Strings;
protected:
	int __fastcall MoveNames(int oldi, int newi);
	AnsiString __fastcall readName(int Index) {if(Index>=0 && Index<Strings->Count) return Strings->Strings[Index]; else return "";}
	void __fastcall writeName(int Index, AnsiString AName) {if(Index>=0 && Index<Strings->Count) Strings->Strings[Index]=AName;}
public:
	__fastcall TMyObjectsNamedList(TMyObject *AOwner): TMyObjectsList(AOwner) {Strings=new TStringList();}
	__fastcall ~TMyObjectsNamedList() {delete Strings; Strings=NULL;}
	int __fastcall AddMyObject(TMyObject* o, AnsiString AName) {int i=TMyObjectsList::AddMyObject(o); if(i>=0) Strings->Add(AName); return i;}
	int __fastcall Add(TMyObject* o, AnsiString AName) {return AddMyObject(o, AName);}
	void __fastcall Delete(int Index) {TMyObjectsList::Delete(Index); Strings->Delete(Index);}
	void __fastcall Remove(int Index) {TMyObjectsList::Remove(Index); Strings->Delete(Index);}
	void __fastcall Clear() {TMyObjectsList::Clear(); Strings->Clear();}
	int __fastcall SendToBack(int Index) {return MoveNames(Index, TMyObjectsList::SendToBack(Index));}
	int __fastcall BringToFront(int Index) {return MoveNames(Index, TMyObjectsList::BringToFront(Index));}
	int __fastcall OneStepBack(int Index) {return MoveNames(Index, TMyObjectsList::OneStepBack(Index));}
	int __fastcall OneStepForward(int Index) {return MoveNames(Index, TMyObjectsList::OneStepForward(Index));}
	int __fastcall IndexOf(AnsiString AName);

	__property AnsiString Names[int Index] = {read=readName, write=writeName};
};
#endif
