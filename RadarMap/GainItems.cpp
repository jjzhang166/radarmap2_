// ---------------------------------------------------------------------------

#pragma hdrstop

#include "GainItems.h"
#include "Radar.h"
// ---------------------------------------------------------------------------

#pragma package(smart_init)

// ---------------------------------------------------------------------------
// TGainItem
// ---------------------------------------------------------------------------
void __fastcall TGainItem::GainPointImageClick(TObject *Sender) {
	float x, y, y2;

	if (Owner) {
		// if(((TGainItems*)Owner)->GPRUnit && ((TGainItems*)Owner)->ReadyForGain)
		if (((TGainItems*)Owner)->ReadyForGain) {
			if (ID > 1 && ID < MaxGainPointsCount) {
				Enabled = !Enabled;
				if (Enabled) {
					x = (float)(ID - 1.5) / (float)(MaxGainPointsCount - 2);
					if (ID <= 1)
						y = y2 = 0;
					else {
						y2 = MaxGain;
						y = 0;
						for (int i = ID; i < MaxGainPointsCount; i++)
							if (((TGainItems*)Owner)->Items[i] &&
								((TGainItems*)Owner)->Items[i]->Enabled &&
								((TGainItems*)Owner)->Items[i]->GainPoint) {
								y2 = ((TGainItems*)Owner)
									->Items[i]->GainPoint->y;
								break;
							}
						for (int i = ID - 2; i >= 0; i--)
							if (((TGainItems*)Owner)->Items[i] &&
								((TGainItems*)Owner)->Items[i]->Enabled &&
								((TGainItems*)Owner)->Items[i]->GainPoint) {
								y = ((TGainItems*)Owner)
									->Items[i]->GainPoint->y;
								break;
							}
					}
					y = (y + y2) / 2;
					GainPoint = ((TGainItems*)Owner)
						->GainFunction->InsertGainPoint(TGainPoint(x, y));
					((TGainItems*)Owner)->SelectedGainItem = ID - 1;
					if (!GainPoint)
						Enabled = false;
				}
				else {
					if (GainPoint) {
						if (((TGainItems*)Owner)->GainFunction->DeleteGainPoint
							(*GainPoint)) {
							GainPoint = NULL;
							((TGainItems*)Owner)->SelectClosestItem();
						}
						else
							Enabled = true;
					}
					else
						GainPoint = NULL;
				}
			}
			else
				Enabled = true;
			((TGainItems*)Owner)->ReAssignGainPoints();
			((TGainItems*)Owner)->Redraw();
		}
		// else Enabled=false;
	}
	// else Enabled=false;
}

// ---------------------------------------------------------------------------
// TOscilloscopeGrid
// ---------------------------------------------------------------------------
__fastcall TOscilloscopeGrid::TOscilloscopeGrid(TImgView32 *AImage,
	TGPRUnit *AGPRUnit, int AChannelsPxOffset, Types::TRect *BR,
	TColor32 ABackgroundColor) {
	GainImage = AImage;
	rows = 8;
	//color = 0xffbfbfdf;
    color = 0xffbfdfbf;
	////0xffdfdfdf;//0xffcfdfcf;//
	frame = true;
	visible = true;
	BordersRect = BR;
	GPRUnit = AGPRUnit;
	ChannelsPxOffset = AChannelsPxOffset;
	backgroundcolor = ABackgroundColor;
	if (GainImage && GainImage->Bitmap) {
		if (GainImage->Bitmap->Width == 0 && GainImage->Bitmap->Height == 0) {
			GainImage->Bitmap->SetSizeFrom(GainImage);
			GainImage->Bitmap->Clear(backgroundcolor);
		}
		Background = new TBitmapLayer(GainImage->Layers);
		Background->Bitmap->DrawMode = dmBlend;
		Background->OnPaint = &BackgroundPaint;
		Background->SendToBack();
	}
}
// ---------------------------------------------------------------------------

__fastcall TOscilloscopeGrid::~TOscilloscopeGrid() {
	if (visible && GainImage && Background) {
		GainImage->Layers->Delete(Background->Index);
		Background = NULL;
	}
}
// ---------------------------------------------------------------------------

void __fastcall TOscilloscopeGrid::BackgroundPaint(TObject *Sender,
	TBitmap32 *Bmp) {
	int ww, hh, y, max, i, x0, y0, ChN, rows2, ChNPx, tw, th;
	float f, dx, dy;
	int fh;
	TColor32 wmc;

	if (visible && Bmp && GainImage) {
		Bmp->BeginUpdate();
		// Bmp->Clear(0x00000000);
		try {
			try {
				if (GPRUnit) {
					switch(GPRUnit->SelectedChannelMode) {
					case TChannelMode::
						Double : ChN = 2;
						break;
					case TChannelMode::
						Channel1 :
					case TChannelMode::
						Channel2 :
					case TChannelMode::
						Tx1Rx2 :
					case TChannelMode::
						Tx2Rx1 : ChN = 1;
						break;
					case TChannelMode::
						Circle :
					default:
						ChN = 1;
					}
				}
				else
					ChN = 1;
				ChNPx = (float)ChannelsPxOffset * (float)(ChN - 1) / (float)ChN;
				if (BordersRect) {
					ww = BordersRect->Width();
					hh = (int)((float)(BordersRect->Height() - 1) / (float)ChN)
						- ChNPx;
					x0 = BordersRect->Left;
					y0 = BordersRect->Top;
				}
				else {
					ww = GainImage->Width;
					hh = (int)((float)GainImage->Height / (float)ChN) - ChNPx;
					x0 = 0;
					y0 = 0;
				}
				rows2 = (int)((float)rows / (float)ChN);

				Bmp->Font->Name = "Tahoma";
				fh = Bmp->Font->Height;
				Bmp->Font->Height = hh;
				for (int j = 0; j < ChN; j++) {
					y0 += j * (hh + ChannelsPxOffset);
					if (ChN > 1) {
						tw = Bmp->TextWidth(IntToStr(j + 1));
						th = Bmp->TextHeight(IntToStr(j + 1));
						if(j==0) wmc=Color32(64, 64, 196, 50);
						else if(j==1) wmc=Color32(196, 64, 64, 50);
						else wmc=Color32(128, 128, 128, 50);
						Bmp->RenderText(x0 + ((ww - tw) >> 1), y0 + ((hh - th) >> 1), IntToStr(j + 1), 1, wmc);
					}
					dy = (float)hh / (float)rows2;
					dx = (float)ww / dy;
					if (dx - (int)dx < 0.5)
						dx = (float)ww / (float)((int)dx);
					else
						dx = (float)ww / (float)((int)dx + 1);
					// ww--;
					// hh--;
					if (frame)
						Bmp->FrameRectS(x0, y0, x0 + ww, y0 + hh, color);
					max = (hh >> 1);
					y = max + y0;
					for (i = 1; i < rows2; i++) {
						Bmp->Line(x0, y0 + i * dy, x0 + ww, y0 + i * dy, color);
					}
					// Bmp->Line(x0, y-1, x0+ww, y-1, color);
					// Bmp->Line(x0, y+1, x0+ww, y+1, color);
					Bmp->Line(x0, y, x0 + ww, y, color);
					for (f = dx; f <= ww; f += dx)
						Bmp->Line(x0 + f, y0, x0 + f, y0 + hh, color);
				}
				Bmp->Font->Height = fh;
			}
			catch(Exception & e) {
			}
		}
		__finally {
			Bmp->EndUpdate();
		}
	}
}

// ---------------------------------------------------------------------------
// TGainItems
// ---------------------------------------------------------------------------
__fastcall TGainItems::TGainItems(TObject* AManager, TChannel* AChannel,
	TImgView32 *AImage, Types::TRect *BR, bool ASelfConnect,
	bool AAllowSingleTrace, TTrackBar *ATransparencyTB, bool *AG,
	TPanel *PBP, TColor32 ABackgroundColor) {
	Types::TRect rct;
	TImageButton *ib;
	int i, j;
	TBitmap32 *bmp;

	GainImage = AImage;
	BordersRect = BR;
	Manager = AManager;
	oldchannel = NULL;
	oldgain = NULL;
	channel = AChannel;
	AllowSingleTrace = AAllowSingleTrace;
	gainfunction = new TGainFunction(MySamples);
	GainItems = new TGainItem*[MaxGainPointsCount];
	PointButtonsPanel = PBP;
	selfconnected = false;
	backgroundcolor = ABackgroundColor;
	if (GainImage) {
		rct = GainImage->GetBitmapRect();

		for (i = 0; i < MaxGainPointsCount; i++) {
			if (PBP) {
				ib = new TImageButton(PBP);
				ib->Parent = PBP;
				try {
					try {
						bmp = LoadPNG32Resource
							((System::UnicodeString)
							("PNGIMAGE_" + IntToStr(177 + i * 2)));
					}
					catch(Exception & e) {
						bmp = NULL;
					}
				}
				__finally {
					if (bmp) {
						ib->PicturePressed->Assign(bmp);
						ib->PictureHot->Assign(bmp);
						delete bmp;
					}
				}
				try {
					try {
						bmp = LoadPNG32Resource
							((System::UnicodeString)
							("PNGIMAGE_" + IntToStr(177 + i * 2 + 1)));
					}
					catch(Exception & e) {
						bmp = NULL;
					}
				}
				__finally {
					if (bmp) {
						ib->PictureNormal->Assign(bmp);
						delete bmp;
					}
				}
				ib->Top = 0; // 1;//PBP->Height-ib->PictureNormal->Height-1;
				j = ((float)i * (float)PBP->Width / (float)
					(MaxGainPointsCount - 1));
				j -= (ib->PictureNormal->Width >> 1);
				if (j < 2)
					j = 1;
				else if ((j + ib->PictureNormal->Width) > (PBP->Width - 2))
					j = PBP->Width - 2 - ib->PictureNormal->Width;
				ib->Left = j;
				ib->Refresh();
				GainItems[i] = new TGainItem(this, NULL, i + 1, rct, NULL,
					NULL, ib);
			}
			else GainItems[i] = new TGainItem(this, NULL, i + 1, rct);
		}
	}
	else
		for (i = 0; i < MaxGainPointsCount; i++)
			GainItems[i] = new TGainItem(this, i + 1);
	TransparencyTB = ATransparencyTB;
	if (TransparencyTB) {
		TransparencyTB->Min = 50;
		TransparencyTB->Max = 255;
		if (Manager)
			TransparencyTB->Position = ((TRadarMapManager*)Manager)
				->Settings->TraceTransparency;
		else
			TransparencyTB->Position = 128;
		TransparencyTB->OnChange = &TransparencyTBChange;
	}
	LockBmp = LoadPNG32Resource((System::UnicodeString)"PNGIMAGE_53");
	if (GainImage) {
		if (GainImage->Bitmap->Width == 0 && GainImage->Bitmap->Height == 0) {
			GainImage->Bitmap->SetSizeFrom(GainImage);
			GainImage->Bitmap->Clear(backgroundcolor);
		}
		if (!GainImage->OnMouseDown)
			GainImage->OnMouseDown = &GainImageMouseDown;
		if (!GainImage->OnMouseMove)
			GainImage->OnMouseMove = &GainImageMouseMove;
		if (!GainImage->OnMouseUp)
			GainImage->OnMouseUp = &GainImageMouseUp;
		if (!GainImage->OnDblClick)
			GainImage->OnDblClick = &GainImageDblClick;
		// if!(GainImage->OnClick) GainImage->OnClick=&GainImageClick;
		GainLayer = new TBitmapLayer(GainImage->Layers);
		GainLayer->Bitmap->DrawMode = dmBlend;
		GainLayer->OnPaint = &GainLayerPaint;
		GainLayer->BringToFront();
		// Background=new TBitmapLayer(GainImage->Layers);
		// Background->Bitmap->DrawMode=dmBlend;
		// Background->OnPaint=&BackgroundPaint;
		// Background->SendToBack();
	}
	GetGainFunction();
	ApplyGain = AG;
	if (AllowSingleTrace || (ASelfConnect && !((TRadarMapManager*)Manager)
			->Connection || (((TRadarMapManager*)Manager)->Connection &&
				(((TRadarMapManager*)Manager)
					->Connection->Status == csStarted ||
					((TRadarMapManager*)Manager)
					->Connection->Status == csDisconnected))))
		SetupThread = new TSetupThread(false, AManager, GainImage,
		AllowSingleTrace, ApplyGain, BR, gainfunction);
	else
		SetupThread = NULL;
	SelfConnect = ASelfConnect;
	if (SelfConnect && Manager) {
		ConnectThread = new TReConnectThread(Manager, this);
		if (!ConnectThread->Executing)
			ConnectThread->Start();
	}
	else
		ConnectThread = NULL;
	selectedgainitem = MaxGainPointsCount - 1;
	SelectedGainPoint = gainfunction->GetPointPtr
		(gainfunction->GainPointsCount - 1);
	NextPointGC = PrevPointGC = MoveLeftGC = MoveRightGC = MoveUpGC =
		MoveDownGC = LockGC = UnlockGC = NULL;
	PressNHoldTimer = new TTimer(NULL);
	PressNHoldTimer->Interval = 150;
	PressNHoldTimer->Enabled = false;
	PressNHoldTimer->OnTimer = &PressNHoldOnTimer;
	Pressed = false;
	visible = true;
	showdb = true;
}
// ---------------------------------------------------------------------------

__fastcall TGainItems::~TGainItems() {
	if (SetupThread) {
		SetupThread->AskForTerminate();
		if (SetupThread->ForceTerminate())
			delete SetupThread;
		SetupThread = NULL;
	}
	for (int i = 0; i < MaxGainPointsCount; i++) {
		if (GainItems && GainItems[i]) {
			if (PointButtonsPanel && GainItems[i]->ImageButton)
				delete GainItems[i]->ImageButton;
			delete GainItems[i];
			GainItems[i] = NULL;
		}
	}
	if (GainItems)
		delete[]GainItems;
	GainItems = NULL;
	if (gainfunction)
		delete gainfunction;
	gainfunction = NULL;
	if (GainImage && GainImage->Layers && GainLayer) {
		GainImage->Layers->Delete(GainLayer->Index);
		GainLayer = NULL;
	}
	// if(GainImage && GainImage->Layers && Background)
	// GainImage->Layers->Delete(Background->Index);
	if (ConnectThread) {
		ConnectThread->AskForTerminate();
		if (ConnectThread->ForceTerminate())
			delete ConnectThread;
		ConnectThread = NULL;
	}
	if (selfconnected && Manager) // SelfConnect && Manager)
	{
		((TRadarMapManager*)Manager)->CloseConnection();
		selfconnected = false;
	}
	if (LockBmp)
		delete LockBmp;
	LockBmp = NULL;
}
// ---------------------------------------------------------------------------

void __fastcall TGainItems::GetGainFunction() {
	if (channel && channel->Gain)
		gainfunction->Copy(channel->Gain);
	/* else if(GPRUnit && GPRUnit->SelectedChannel && GPRUnit->SelectedChannel->Gain)
	gainfunction->Copy(GPRUnit->SelectedChannel->Gain); */
	ReAssignGainPoints();
	Redraw();
}
// ---------------------------------------------------------------------------

void __fastcall TGainItems::writeChannel(TChannel* value) {
	if (channel != value) {
		if (oldchannel && oldgain) {
			oldgain->Copy(gainfunction);
			oldchannel->Gain = oldgain;
		}
		channel = value;
		/* if(!channel && GPRUnit && GPRUnit->SelectedChannel)
		channel=GPRUnit->SelectedChannel; */
		oldchannel = channel;
		if (channel) {
			oldgain = channel->Gain;
			GetGainFunction();
			channel->Gain = gainfunction;
		}
		else
			oldgain = NULL;
	}
}
// ---------------------------------------------------------------------------

void __fastcall TGainItems::writeVisible(bool value) {
	if (visible != value) {
		visible = value;
		if (SetupThread)
			SetupThread->Visible = value;
		for (int i = 0; i < MaxGainPointsCount; i++) {
			if (GainItems[i])
				GainItems[i]->Visible = visible;
		}
	}
}
// ---------------------------------------------------------------------------

void __fastcall TGainItems::writeSelfConnect(bool value) {
	if (value != selfconnect) {
		selfconnect = value;
		if (selfconnect && !SetupThread && !((TRadarMapManager*)Manager)
			->Connection || (((TRadarMapManager*)Manager)->Connection &&
				(((TRadarMapManager*)Manager)
					->Connection->Status == csStarted ||
					((TRadarMapManager*)Manager)
					->Connection->Status == csDisconnected)))
			SetupThread = new TSetupThread(false, Manager, GainImage,
			AllowSingleTrace, ApplyGain, BordersRect, gainfunction);
		else if (!selfconnect && !AllowSingleTrace && SetupThread) {
			SetupThread->AskForTerminate();
			if (SetupThread->ForceTerminate())
				delete SetupThread;
			SetupThread = NULL;
		}
	}
}
// ---------------------------------------------------------------------------

void __fastcall TGainItems::writeAllowSingleTrace(bool value) {
	if (value != allowsingletrace) {
		allowsingletrace = value;
		if (allowsingletrace && !SetupThread)
			SetupThread = new TSetupThread(false, Manager, GainImage,
			AllowSingleTrace, ApplyGain, BordersRect, gainfunction);
		else if (!selfconnect && !allowsingletrace && SetupThread) {
			SetupThread->AskForTerminate();
			if (SetupThread->ForceTerminate())
				delete SetupThread;
			SetupThread = NULL;
		}
	}
}
// ---------------------------------------------------------------------------

bool __fastcall TGainItems::readReadyForGain() {
	return(allowsingletrace || ((((TRadarMapManager*)Manager)->Connection &&
				(((TRadarMapManager*)Manager)
					->Connection->Status == csConnected ||
					((TRadarMapManager*)Manager)
					->Connection->Status == csReceiving)) &&
			(!SelfConnect || (SelfConnect && SetupThread != NULL))));
}
// ---------------------------------------------------------------------------

void __fastcall TGainItems::AssignControls(int Id, TImage *AI, TImage *DI) {
	TImage* gc = NULL;

	if (Id > 0 && Id <= MaxGainPointsCount && GainItems[Id - 1]) {
		GainItems[Id - 1]->ActiveImage = AI;
		GainItems[Id - 1]->DisabledImage = DI;
	}
	else if (Id < 0) {
		switch((TGainControlsId)Id) {
		case gcNextPoint:
			gc = NextPointGC = AI;
			break;
		case gcPrevPoint:
			gc = PrevPointGC = AI;
			break;
		case gcMoveLeft:
			gc = MoveLeftGC = AI;
			break;
		case gcMoveRight:
			gc = MoveRightGC = AI;
			break;
		case gcMoveUp:
			gc = MoveUpGC = AI;
			break;
		case gcMoveDown:
			gc = MoveDownGC = AI;
			break;
		case gcLock:
			gc = LockGC = AI;
			UnlockGC = DI;
			break;
		}
		if (gc) {
			gc->Tag = (int)gcTagShift + Id;
			gc->OnMouseDown = &GainControlMouseDown;
			gc->OnMouseUp = &GainControlMouseUp;
			if (gc == LockGC && UnlockGC) {
				UnlockGC->Tag = (int)gcTagShift + Id;
				UnlockGC->OnMouseDown = &GainControlMouseDown;
				UnlockGC->OnMouseUp = &GainControlMouseUp;
				writeSelectedGainItem(gcTagShift);
			}
		}
	}
}
// ---------------------------------------------------------------------------

void __fastcall TGainItems::GainControlMouseDown(TObject *Sender,
	TMouseButton Button, TShiftState Shift, int X, int Y) {
	TGainControlsId Id;

	if (visible) {
		PressNHoldTimer->Enabled = false;
		PressNHoldTimer->Interval = 200;
		LastId = Id = (TGainControlsId)(((TComponent*)Sender)->Tag - (int)
			gcTagShift);
		ProcessControl(Id);
		PressNHoldTimer->Enabled = true;
	}
}
// ---------------------------------------------------------------------------

void __fastcall TGainItems::PressNHoldOnTimer(TObject *Sender) {
	ProcessControl(LastId);
	PressNHoldTimer->Interval = 50;
}
// ---------------------------------------------------------------------------

void __fastcall TGainItems::ProcessControl(TGainControlsId Id) {
	int i, j;
	float z, d, dp;

	i = SelectedGainItem;
	switch(Id) {
	case gcNextPoint:
	case gcPrevPoint:
		i = j = SelectedGainItem;
		do {
			if (Id == gcNextPoint) {
				i++;
				if (i >= MaxGainPointsCount)
					i = 0;
			}
			else if (Id == gcPrevPoint) {
				i--;
				if (i < 0)
					i = MaxGainPointsCount - 1;
			}
		}
		while (Items[i] && !Items[i]->Enabled & i != j);
		if (Items[i] && Items[i]->Enabled)
			SelectedGainItem = i;
		break;
	case gcMoveLeft:
	case gcMoveRight:
		if (i > 0 && i < MaxGainPointsCount - 1 && Items[i] && Items[i]
			->Enabled && Items[i]->GainPoint && !Items[i]
			->GainPoint->Locked) {
			d = Items[i]->GainPoint->x;
			if (Ww > 0)
				dp = 1. / (float)Ww;
			else
				dp = 0.;
			if (Id == gcMoveLeft) {
				d -= MinGainXShift;
				z = 0;
				for (int k = i - 1; k >= 0; k--)
					if (Items[k] && Items[k]->Enabled && Items[k]->GainPoint) {
						z = Items[k]->GainPoint->x;
						break;
					}
				if (d < z + dp)
					d = z + dp;
			}
			else if (Id == gcMoveRight) {
				d += MinGainXShift;
				z = 1.;
				for (int k = i + 1; k < MaxGainPointsCount; k++)
					if (Items[k] && Items[k]->Enabled && Items[k]->GainPoint) {
						z = Items[k]->GainPoint->x;
						break;
					}
				if (d > z - dp)
					d = z - dp;
			}
			Items[i]->GainPoint->x = d;
			gainfunction->RebuildGainFunction();
			ReAssignGainPoints();
		}
		break;
	case gcMoveUp:
	case gcMoveDown:
		if (Items[i] && Items[i]->Enabled && Items[i]->GainPoint && !Items[i]
			->GainPoint->Locked) {
			d = Items[i]->GainPoint->y;
			if (Id == gcMoveDown) {
				d -= MinGainYShift;
				if (d < 0)
					d = 0;
			}
			else if (Id == gcMoveUp) {
				d += MinGainYShift;
				if (d > MaxGain)
					d = MaxGain;
			}
			Items[i]->GainPoint->y = d;
			gainfunction->RebuildGainFunction();
		}
		break;
	case gcLock:
		if (Items[i] && Items[i]->Enabled && Items[i]->GainPoint) {
			Items[i]->GainPoint->Locked = !Items[i]->GainPoint->Locked;
			writeSelectedGainItem(gcTagShift);
		}
		break;
	}
	LastId = Id;
}
// ---------------------------------------------------------------------------

void __fastcall TGainItems::writeSelectedGainItem(int value) {
	if (value == gcTagShift)
		value = selectedgainitem;
	if (value >= 0 && value < MaxGainPointsCount) {
		selectedgainitem = value;
		if (Items[value] && Items[value]->Enabled && Items[value]->GainPoint) {
			SelectedGainPoint = Items[value]->GainPoint;
			if (LockGC) {
				if (UnlockGC) {
					LockGC->Visible = !Items[value]->GainPoint->Locked;
					UnlockGC->Visible = Items[value]->GainPoint->Locked;
				}
				else
					LockGC->Visible = false;
			}
		}
	}
}
// ---------------------------------------------------------------------------

void __fastcall TGainItems::GainControlMouseUp(TObject *Sender,
	TMouseButton Button, TShiftState Shift, int X, int Y) {
	if (visible)
		PressNHoldTimer->Enabled = false;
}
// ---------------------------------------------------------------------------

void __fastcall TGainItems::SelectClosestItem() {
	int i, max, j;

	max = MaxGainPointsCount;
	j = MaxGainPointsCount - 1;
	for (int k = 0; k < MaxGainPointsCount; k++) {
		if (Items[k] && Items[k]->Enabled) {
			i = abs(selectedgainitem - k);
			if (i < max) {
				max = i;
				j = k;
			}
		}
	}
	SelectedGainItem = j;
}
// ---------------------------------------------------------------------------

void __fastcall TGainItems::ReAssignGainPoints() {
	float x, l, r;
	int i, j, z;

	for (int k = 1; k < MaxGainPointsCount - 1; k++)
		if (Items[k]) {
			Items[k]->Enabled = false;
			Items[k]->GainPoint = NULL;
		}
	Items[0]->GainPoint = gainfunction->GetPointPtr(0);
	Items[0]->Enabled = true;
	if (gainfunction->GainPointsCount > 1)
		Items[MaxGainPointsCount - 1]->GainPoint = gainfunction->GetPointPtr
			(gainfunction->GainPointsCount - 1);
	else {
		Items[MaxGainPointsCount - 1]->GainPoint = gainfunction->InsertGainPoint
			(TGainPoint(1, 0));
	}
	Items[MaxGainPointsCount - 1]->Enabled = true;
	/* for(int k=1; k<gainfunction->GainPointsCount-1; k++)
	{
	x=gainfunction->GainPoints[k].x;
	x/=1./(float)(MaxGainPointsCount-2);
	x++;
	if(x>MaxGainPointsCount-2) x=MaxGainPointsCount-2;
	if(!Items[(int)x]->Enabled)
	{
	Items[(int)x]->GainPoint=gainfunction->GetPointPtr(k);
	Items[(int)x]->Enabled=true;
	}
	} */
	i = 1;
	for (int k = 1; k < MaxGainPointsCount - 1; k++) {
		if (Items[k]) {
			r = (float)k / (float)(MaxGainPointsCount - 2);
			for (; i < gainfunction->GainPointsCount - 1; ) {
				x = gainfunction->GainPoints[i].x;
				if (x < r) {
					if (!Items[k]->Enabled || Items[k]->GainPoint ==
						gainfunction->GetPointPtr(i)) {
						Items[k]->GainPoint = gainfunction->GetPointPtr(i);
						Items[k]->Enabled = true;
						if (Items[k]->GainPoint == SelectedGainPoint)
							selectedgainitem = k;
						i++;
					}
					else {
						for (z = k - 1; z >= 1; z--) {
							if (Items[z] && !Items[z]->Enabled && Items[z + 1]
								->Enabled) {
								Items[z]->GainPoint = Items[z + 1]->GainPoint;
								Items[z]->Enabled = Items[z + 1]->Enabled;
								Items[z + 1]->GainPoint = NULL;
								Items[z + 1]->Enabled = false;
								if (Items[z]->GainPoint == SelectedGainPoint)
									selectedgainitem = z;
								break;
							}
						}
						if (z == 0)
							break;
					}
				}
				else
					break;
			}
			if (i >= gainfunction->GainPointsCount - 1)
				break;
		}
	}
}
// ---------------------------------------------------------------------------

bool __fastcall TGainItems::ApplyGainFunction() {
	if (channel && channel->Gain) {
		channel->Gain->Copy(gainfunction);
		return true;
	}
	/* else if(GPRUnit && GPRUnit->SelectedChannel && GPRUnit->SelectedChannel->Gain)
	{
	GPRUnit->SelectedChannel->Gain->Copy(gainfunction);
	return true;
	} */
	else
		return false;
}
// ---------------------------------------------------------------------------

void __fastcall TGainItems::Redraw() {
	if (visible && GainLayer)
		GainLayer->Update();
}
// ---------------------------------------------------------------------------

void __fastcall TGainItems::GainImageClick(TObject *Sender) {
	if (ConnectThread && !ConnectThread->Executing &&
		(!((TRadarMapManager*)Manager)->Connection || ((TRadarMapManager*)
				Manager)->Connection && !((TRadarMapManager*)Manager)
			->Connection->Connected)) {
		ConnectThread->Start();
	}
}
// ---------------------------------------------------------------------------

void __fastcall TGainItems::GetGainPointBorders(int x, int y, int &x1, int &x2,
	int &xx1, int &xx2, int &y1, int &y2, int &yy1, int &yy2, bool Activated) {
	int as, as2;

	if (Activated)
		as = 3;
	else
		as = 0;
	as2 = 0; // =as;

	x1 = x - (6 + as);
	x2 = x + (5 + as);
	xx1 = x1 - (7 + as2);
	xx2 = x2 + (6 + as2);
	if (x1 < 0) {
		xx1 = x1 = 0;
		x2 = x1 + (11 + as);
		xx2 = x2 + (6 + as2);
	}
	else if (x2 > GainImage->Width - 1) {
		xx2 = x2 = GainImage->Width - 1;
		x1 = x2 - (11 + 2 * as);
		xx1 = x1 - (7 + as2);
	}
	if (xx1 < 0)
		xx1 = 0;
	else if (xx2 > GainImage->Width - 1)
		xx2 = GainImage->Width - 1;
	y1 = y - (6 + as);
	y2 = y + (5 + as);
	yy1 = y1 - (7 + as2);
	yy2 = y2 + (6 + as2);
	if (y1 < 0) {
		yy1 = y1 = 0;
		y2 = y1 + (11 + 2 * as);
		yy2 = y2 + (6 + as2);
	}
	else if (y2 > GainImage->Height - 1) {
		yy2 = y2 = GainImage->Height - 1;
		y1 = y2 - (11 + 2 * as);
		yy1 = y1 - (7 + as2);
	}
	if (yy1 < 0)
		yy1 = 0;
	else if (yy2 > GainImage->Height - 1)
		yy2 = GainImage->Height - 1;
}
// ---------------------------------------------------------------------------

int __fastcall TGainItems::FindItemArroundXY(int X, int Y) {
	float x, y;
	int x1, x2, xx1, xx2, y1, y2, yy1, yy2, res = -1;

	for (int k = MaxGainPointsCount - 1; k >= 0; k--) {
		if (Items[k] && Items[k]->Enabled && Items[k]->GainPoint) {
			x = X0 + Items[k]->GainPoint->x * (float)(Ww - 1);
			y = Y0 + Hh - 1 - (Items[k]->GainPoint->y / (float)MaxGain) *
				(float)(Hh - 1);
			GetGainPointBorders(x, y, x1, x2, xx1, xx2, y1, y2, yy1, yy2);
			if (X >= xx1 && X <= xx2 && Y >= yy1 && Y <= yy2) {
				res = k;
				break;
			}
		}
	}

	return res;
}
// ---------------------------------------------------------------------------

void __fastcall TGainItems::GainImageDblClick(TObject *Sender) {
	int i = SelectedGainItem;

	if (i >= 0 && i < MaxGainPointsCount && LastXY.x >= 0 && LastXY.y >= 0) {
		if (Items[i] && Items[i]->Enabled && Items[i]->GainPoint) {
			Items[i]->GainPoint->Locked = !Items[i]->GainPoint->Locked;
			writeSelectedGainItem(gcTagShift);
		}
	}
}
// ---------------------------------------------------------------------------

void __fastcall TGainItems::GainImageMouseDown(TObject *Sender,
	TMouseButton Button, TShiftState Shift, int X, int Y,
	TCustomLayer *Layer) {
	int i;

	if (visible && Button == mbLeft) {
		i = FindItemArroundXY(X, Y);
		if (i >= 0 && i < MaxGainPointsCount) {
			SelectedGainItem = i;
			Pressed = true;
			LastXY = Types::TPoint(X, Y);
		}
		else
			LastXY = Types::TPoint(-1, -1);
	}
}
// ---------------------------------------------------------------------------

void __fastcall TGainItems::GainImageMouseMove(TObject *Sender,
	TShiftState Shift, int X, int Y, TCustomLayer *Layer) {
	float x, y, z, dp;
	int i;
	// Types::TRect tmp;

	// tmp=Types::TRect(X0, Y0, X0+Ww, Y0+Hh);
	if (visible && Pressed && SelectedGainPoint)
	// && tmp.Contains(Types::TPoint(X, Y)))
	{
		if (Ww > 0)
			dp = 1. / (float)Ww;
		else
			dp = 0.;
		x = (float)(X - X0) / (float)(Ww - 1);
		y = (1. - (float)(Y - Y0) / (float)(Hh - 1)) * (float)MaxGain;
		i = SelectedGainItem;
		if (i > 0 && i < MaxGainPointsCount - 1) {
			if (x - SelectedGainPoint->x < 0) // Moving To The Left
			{
				z = 0;
				for (int k = i - 1; k >= 0; k--)
					if (Items[k] && Items[k]->Enabled && Items[k]->GainPoint) {
						z = Items[k]->GainPoint->x;
						break;
					}
				if (x < z + dp)
					x = z + dp;
			}
			else {
				z = 1.;
				for (int k = i + 1; k < MaxGainPointsCount; k++)
					if (Items[k] && Items[k]->Enabled && Items[k]->GainPoint) {
						z = Items[k]->GainPoint->x;
						break;
					}
				if (x > z - dp)
					x = z - dp;
			}
			// SelectedGainPoint->x=x;
		}
		if (y < 0)
			y = 0;
		else if (y > MaxGain) {
			y = MaxGain;
		}
		if (SelectedGainPoint->y != y || ((i > 0 && i < MaxGainPointsCount - 1)
				&& SelectedGainPoint->x != x)) {
			if (i > 0 && i < MaxGainPointsCount - 1)
				SelectedGainPoint->x = x;
			SelectedGainPoint->y = y;
			gainfunction->RebuildGainFunction();
			ReAssignGainPoints();
			Redraw();
		}
	}
}
// ---------------------------------------------------------------------------

void __fastcall TGainItems::GainImageMouseUp(TObject *Sender,
	TMouseButton Button, TShiftState Shift, int X, int Y,
	TCustomLayer *Layer) {
	if (visible) {
		Pressed = false;
		GainImageClick(Sender);
		Redraw();
	}
}
// ---------------------------------------------------------------------------

void __fastcall TGainItems::TransparencyTBChange(TObject *Sender) {
	if (SetupThread)
		SetupThread->TraceTransparency = ((TTrackBar*)Sender)->Position;
}
// ---------------------------------------------------------------------------

void __fastcall TGainItems::GainLayerPaint(TObject *Sender, TBitmap32 *Bmp) {
	int ww, hh, x0, y0;

	if (visible && Bmp) {
		Bmp->BeginUpdate();
		// Bmp->Clear(0x00000000);
		try {
			if (AllowSingleTrace || (GPRUnit && Manager && ((TRadarMapManager*)
						Manager)->Connection && ((TRadarMapManager*)Manager)
					->Connection->Connected)) {
				TPolygon32 *poly;
				int x, y, x1, x2, xx1, xx2, y1, y2, yy1, yy2, lx, ly, tw, th;
				AnsiString str;

				poly = new TPolygon32();
				try {
					poly->AntialiasMode = am8times;
					poly->Antialiased = true;
					poly->Closed = true;
					Bmp->PenColor = Color32(0x64, 0x2A, 0x00, 0xCC);
					for (int k = 0; k < MaxGainPointsCount; k++) {
						if
							(Items[k] && Items[k]->Enabled && Items[k]
							->GainPoint) {
							x = (int)(Items[k]->GainPoint->x * (float)(Ww - 1));
							y = Hh - 1 - (int)
								((Items[k]->GainPoint->y / (float)MaxGain) *
								(float)(Hh - 1));
							if (x > Ww - 1)
								x = Ww - 1;
							if (y < 0)
								y = 0;
							x += X0;
							y += Y0;
							poly->Add(FixedPoint(x, y));
							if
								(k > 0 && k < MaxGainPointsCount - 1 && Items[k]
								->Button) {
								Types::TPoint p1, p2;

								p1 = Items[k]->Button->ClientToScreen
									(Types::TPoint(0, 0));
								p2 = GainImage->ClientToScreen
									(Types::TPoint(0, 0));
								x2 = p1.x - p2.x +
									(Items[k]->Button->Width >> 1);
								Bmp->MoveTo(x2, Y0);
								Bmp->LineToAS(x, y);
								Bmp->LineToAS(x2, Y0 + Hh - 1);
							}
						}
					}
					poly->Add(FixedPoint(X0 + Ww - 1, Y0 + Hh - 1));
					poly->Add(FixedPoint(X0, Y0 + Hh - 1));
					poly->Draw(Bmp, Color32(0xFF, 0xB0, 0x70, 0xFF),
						Color32(0xFF, 0xE0, 0xC0, 0x64), NULL);
				}
				__finally {
					delete poly;
				}
				for (int k = 0; k < MaxGainPointsCount; k++) {
					if (Items[k] && Items[k]->Enabled && Items[k]->GainPoint) {
						x = X0 + (int)(Items[k]->GainPoint->x * (float)(Ww - 1)
							);
						y = Y0 + Hh - 1 - (int)
							((Items[k]->GainPoint->y / (float)MaxGain) *
							(float)(Hh - 1));
						GetGainPointBorders(x, y, x1, x2, xx1, xx2, y1, y2,
							yy1, yy2, SelectedGainItem == k);
						poly = new TPolygon32();
						try {
							// poly->AntialiasMode=am8times;
							// poly->Antialiased=true;
							poly->Closed = true;
							poly->Add(FixedPoint(xx1, yy1));
							poly->Add(FixedPoint(xx2, yy1));
							poly->Add(FixedPoint(xx2, yy2));
							poly->Add(FixedPoint(xx1, yy2));
							if (SelectedGainItem == k) {
								if
									(SelectedGainPoint &&
									SelectedGainPoint->Locked)
									poly->Draw(Bmp,
									Color32(0x8F, 0x3F, 0xFF, 0xB0),
									Color32(0x8F, 0x3F, 0xFF, 0x64), NULL);
								else
									poly->Draw(Bmp,
									Color32(0x00, 0x7F, 0xFF, 0xB0),
									Color32(0x00, 0x7F, 0xFF, 0x64), NULL);
							}
							else if (Items[k] && Items[k]->GainPoint->Locked)
								poly->Draw(Bmp,
								Color32(0xFF, 0x00, 0x00, 0xB0),
								Color32(0xFF, 0x00, 0x00, 0x64), NULL);
							else
								poly->Draw(Bmp,
								Color32(0xFF, 0x7F, 0x00, 0xB0),
								Color32(0xFF, 0x7F, 0x00, 0x64), NULL);
						}
						__finally {
							delete poly;
						}
						if (SelectedGainItem == k) {
							if (SelectedGainPoint && SelectedGainPoint->Locked)
							{
								Bmp->FillRect(x1, y1, x2, y2,
									Color32(0x8F, 0x3F, 0xFF, 0xFF));
								Bmp->FrameRectS(x1, y1, x2, y2,
									Color32(0x7F, 0x38, 0xDF, 0xFF));
							}
							else {
								Bmp->FillRect(x1, y1, x2, y2,
									Color32(0x00, 0x7F, 0xFF, 0xFF));
								Bmp->FrameRectS(x1, y1, x2, y2,
									Color32(0x00, 0x6F, 0xDF, 0xFF));
							}
						}
						else if (Items[k] && Items[k]->GainPoint->Locked) {
							Bmp->FillRect(x1, y1, x2, y2,
								Color32(0xFF, 0x00, 0x00, 0xFF));
							Bmp->FrameRectS(x1, y1, x2, y2,
								Color32(0xDF, 0x00, 0x00, 0xFF));
						}
						else {
							Bmp->FillRect(x1, y1, x2, y2,
								Color32(0xFF, 0x7F, 0x00, 0xFF));
							Bmp->FrameRectS(x1, y1, x2, y2,
								Color32(0xDF, 0x6F, 0x00, 0xFF));
						}
						if (Items[k]->GainPoint->Locked) {
							lx = xx2 + 2;
							if (lx > X0 + Ww - LockBmp->Width)
								lx = xx1 - 2 - LockBmp->Width;
							if (ShowDB)
								ly = yy2 - LockBmp->Height;
							else
								ly = yy1 + ((yy2 - yy1 - LockBmp->Height) >> 1);
							Bmp->Draw(lx, ly, LockBmp);
						}
						if (ShowDB) {
							str = IntToStr((int)Items[k]->GainPoint->y) + " dB";
							tw = Bmp->TextWidth(str);
							th = Bmp->TextHeight(str);
							lx = xx2 + 2;
							if (lx > X0 + Ww - tw)
								lx = xx1 - 2 - tw;
							if (Items[k]->GainPoint->Locked)
								ly = yy1;
							else
								ly = yy1 + ((yy2 - yy1 - th) >> 1);
							Bmp->RenderText(lx, ly, str, 2, clGray32);
						}
					}
				}
			}
			else {
				TBitmap32* tmp;
				AnsiString str;
				int tw;

				Bmp->LineA(X0, Y0, X0 + Ww - 1, Y0 + Hh - 1, clLightGray32);
				Bmp->LineA(X0 + Ww - 1, X0, Y0, Y0 + Hh - 1, clLightGray32);
				try {
					tmp = LoadPNG32Resource((System::UnicodeString)
						"PNGIMAGE_165");
					if (tmp)
						Bmp->Draw((GainImage->Width - tmp->Width) >> 1,
						(GainImage->Height - tmp->Height) >> 1, tmp);
					if (SelfConnect) {
						str =
							"Georadar is switched off or not connected! Click to reconnect...";
						tw = Bmp->TextWidth(str);
						Bmp->RenderText((GainImage->Width - tw) >> 1,
							((GainImage->Height + tmp->Height) >> 1) + 5, str,
							2, clBlack32);
					}
				}
				__finally {
					if (tmp)
						delete tmp;
				}
			}
		}
		__finally {
			Bmp->EndUpdate();
		}
	}
}
// ---------------------------------------------------------------------------

// ---------------------------------------------------------------------------
// TSetupThread
// ---------------------------------------------------------------------------
__fastcall TSetupThread::TSetupThread(bool CreateSuspended, TObject* AManager,
	TImgView32 *AImage, bool AAllowSingleTrace, bool *AG, Types::TRect *BR,
	TGainFunction* GF) : TMyThread(CreateSuspended) // , TBitmap32 *ABmp) : TMyThread(CreateSuspended)
{
	Trace = NULL;
	Manager = AManager;
	ApplyGain = AG;
	singletrace = NULL;
	allowsingletrace = AAllowSingleTrace;
	if (Manager) {
		if (((TRadarMapManager*)Manager)->ReceivingSettings) {
			((TRadarMapManager*)Manager)->ReceivingSettings->Positioning =
				ManualP;
		}
		transparency = ((TRadarMapManager*)Manager)
			->Settings->TraceTransparency;
	}
	Gain = GF;
	Image = AImage;
	Frames = new TFixedSizeQueue(2);
	LastDrawed = NULL;
	if (Image) {
		Layer = new TBitmapLayer(Image->Layers);
		Layer->Bitmap->DrawMode = dmBlend;
		Layer->BringToFront();
		Layer->OnPaint = &LayerPaint;
		if (!BR) {
			TempRect = new Types::TRect(0, 0, Image->Width - 1,
				Image->Height - 1);
			BordersRect = TempRect;
		}
		else {
			BordersRect = BR;
			TempRect = NULL;
		}
	}
	else {
		Layer = NULL;
		BordersRect = NULL;
	}
	visible = true;
}
// ---------------------------------------------------------------------------

__fastcall TSetupThread::~TSetupThread() {
	try {
		if (Image && Layer)
			Image->Layers->Delete(Layer->Index);
		if (LastDrawed)
			delete LastDrawed;
		delete Frames;
		if (TempRect)
			delete TempRect;
	}
	catch(Exception & e) {
	}
}
// ---------------------------------------------------------------------------

void __fastcall TSetupThread::ExecuteLoopBody() {
	if (AllowSingleTrace) {
		Trace = SingleTrace;
		if (Trace)
			Trace->Drawed = false;
		ProcessTrace();
		// WaitForSingleObject(IdleEvent, 98);
	}
	else if (Manager && ((TRadarMapManager*)Manager)->ReceivingProfile) {
		Trace = ((TRadarMapManager*)Manager)->ReceivingProfile->LastTracePtr;
		ProcessTrace();
		((TRadarMapManager*)Manager)->ReceivingProfile->Clear();
	}
	WaitForSingleObject(IdleEvent, 2);
}
// ---------------------------------------------------------------------------

void __fastcall TSetupThread::ProcessTrace() {
	int ww, hh, y, max, i, x0, y0;
	float ds, f, d; // , dx, dy;
	TBitmap32* Bmp;

	if (visible && Trace && !Trace->Drawed && Image && Layer) {
		try {
			Bmp = new TBitmap32();
			try {
				Bmp->SetSizeFrom(Image);
				Bmp->Clear(0x00000000);
				/* =(float)h/8.;
				dx=(float)w/dy;
				if(dx-(int)dx<0.5) dx=(float)w/(float)((int)dx);
				else dx=(float)w/(float)((int)dx+1); */
				// Bmp->Clear(Color32( );
				Bmp->DrawMode = dmBlend;

				ww = BordersRect->Width() - 1;
				hh = BordersRect->Height() - 1;
				x0 = BordersRect->Left;
				y0 = BordersRect->Top;
				max = (hh >> 1);
				y = max + y0;
				/* r(i=1; i<8; i++)
				{
				Bmp->Line(x0, y0+i*dy, x0+ww, y0+i*dy, clLightGray32, true);
				}
				Bmp->Line(x0, y-1, x0+ww, y-1, clLightGray32, true);
				Bmp->Line(x0, y+1, x0+ww, y+1, clLightGray32, true);

				for(f=dx; f<w; f+=dx)
				Bmp->Line(x0+f, y0, x0+f, y0+hh, clGray32, true); */
				Bmp->PenColor = Color32(255, 0, 0, transparency);
				ww -= 1;
				ds = (float)Trace->Samples / (float)ww;
				f = 0;
				for (i = 0; i < ww; i++) {
					if (ApplyGain && !*ApplyGain)
						d = Trace->SourceData[(int)f];
					else if (Gain) {
						d = Trace->SourceData[(int)f] * Gain->GainFunction
							[(int)f];
						if (d > (double)PositiveMax)
							d = (double)PositiveMax;
						else if (d < (double)NegativeMax)
							d = (double)NegativeMax;
					}
					else
						d = Trace->Data[(int)f];
					if (d >= 0)
						d /= (float)PositiveMax;
					else
						d /= (float) - NegativeMax;
					d *= (float)max;
					if (i == 0)
						Bmp->MoveTo(x0 + i, y - d);
					else
						Bmp->LineToAS(x0 + i, y - d);
					f += ds;
				}
				Frames->Push(Bmp);
			}
			catch(Exception & e) {
				delete Bmp;
			}
			Layer->Update();
			// Trace->Drawed=true;
		}
		catch(Exception & e) {
		}
	}
}
// ---------------------------------------------------------------------------

void __fastcall TSetupThread::LayerPaint(TObject *Sender, TBitmap32 *Bmp) {
	TBitmap32* tmp;

	if (Bmp) {
		try {
			tmp = (TBitmap32*)Frames->Pop();
		}
		catch(Exception & e) {
			tmp = NULL;
		}
		try {
			try {
				if (tmp) {
					if (!LastDrawed) {
						LastDrawed = new TBitmap32();
					}
					LastDrawed->SetSizeFrom(tmp);
					LastDrawed->DrawMode = dmBlend;
					LastDrawed->Clear(0x00000000);
					LastDrawed->Draw(0, 0, tmp);
				}
				else
					tmp = LastDrawed;
				if (tmp) {
					Bmp->BeginUpdate();
					try {
						Bmp->Draw(0, 0, tmp);
					}
					__finally {
						Bmp->EndUpdate();
					}
				}
			}
			catch(Exception & e) {
			}
		}
		__finally {
			try {
				if (tmp && tmp != LastDrawed) {
					delete tmp;
				}
			}
			catch(Exception & e) {
			}
		}
	}
}
// ---------------------------------------------------------------------------

// ---------------------------------------------------------------------------
// TReConnectThread
// ---------------------------------------------------------------------------
void __fastcall TReConnectThread::ExecuteLoopBody() {
	int PC = 0;

	try {
		if (Manager) {
			if (!((TRadarMapManager*)Manager)->Connection ||
				(((TRadarMapManager*)Manager)->Connection &&
					(((TRadarMapManager*)Manager)
						->Connection->Status == csStarted ||
						((TRadarMapManager*)Manager)
						->Connection->Status == csDisconnected))) {
				if (((TRadarMapManager*)Manager)->OpenConnection()) {
					PC = 1;
					((TRadarMapManager*)Manager)
						->Connection->Settings->CreateChannelProfiles();
					PC = 2;
					((TRadarMapManager*)Manager)->Connection->StartReceiving();
					PC = 3;
					if (((TRadarMapManager*)Manager)->ReceivingSettings) {
						((TRadarMapManager*)Manager)
							->ReceivingSettings->Positioning = ManualP;
					}
					if (Owner)
						((TGainItems*)Owner)->SelfConnected = true;
				}
				else {
					((TRadarMapManager*)Manager)->CloseConnection();
					if (Owner)
						((TGainItems*)Owner)->SelfConnected = true;
				}
				PC = 4;
			}
			else if (Owner) {
				((TGainItems*)Owner)->SelfConnect = false;
			}
			if (((TRadarMapManager*)Manager)->Connection &&
				((TRadarMapManager*)Manager)->Connection->Connected && Owner) {
				((TGainItems*)Owner)->GetGainFunction();
				PC = 5;
			}
		}
	}
	catch(Exception & e) {
		LogEvent(e.Message, "TRadarDialog::SetupBtnClick() PC=" + IntToStr(PC));
	}
	Stop();
}
// ---------------------------------------------------------------------------
