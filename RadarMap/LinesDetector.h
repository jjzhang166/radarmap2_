//---------------------------------------------------------------------------

#ifndef LinesDetectorH
#define LinesDetectorH
//---------------------------------------------------------------------------

#include "GPSCoordinate.h"
#include "Defines.h"
#include "MyGraph.h"

class TLinesDetector;

struct TMyPixel
{
	int TraceIndex;
	int x, y;
	bool Valid;
	TColor32 cl;
};

class TLinesDetectorThread: public TMyThread
{
private:
	TObject *Manager;
	bool enteredonline;
	HANDLE InsertTargetEvent, EnteredOnLineEvent;
	TLinesDetector* Owner;
	TColor32 LastLineColor;
	TGPSCoordinate *LastProcessedPoint;
	TMyPixel EnteredOnLinePixel;
	bool __fastcall CheckPixel(int x, int y, bool CheckOut, TColor32 &cl);
	void __fastcall DetectLines(TGPSCoordinate *From, TGPSCoordinate *To);
	void __fastcall writeEneteredOnLine(bool value);
	void __fastcall InsertTarget(TGPSCoordinate *From, TDoublePoint from, float x, float y, float dT, float D);
	__property bool EnteredOnLine = {read=enteredonline, write=writeEneteredOnLine};
protected:
	void __fastcall Execute();
public:
	__fastcall TLinesDetectorThread(TLinesDetector* AOwner, TObject* RadarMapManager, bool CreateSuspended);
	__fastcall ~TLinesDetectorThread();
};

struct TInsertingVars
{
	TObject *Path;
	int TraceIndex;
	TColor32 Color;
};

class TLinesDetector: public TObject
{
private:
	TObject* Manager;
	TBitmap32** utilitybmp;
	TGPSCoordinatesRect* latlonrect;
	TLinesDetectorThread* detector_thrd;
	HANDLE addgpspointevent, readyforinsertingevent;
	TGPSCoordinate* addedpoint;
	TBitmapLayer *UtilityLayer;
	TImgView32 *Image;
	TList *pixellist;
	bool waitwhileinserting;
	void __fastcall UtilityLayerPaint(TObject *Sender, TBitmap32 *Bmp);
	void __fastcall writeInsertingVars(TInsertingVars value);
	void __fastcall InsertDetectedTarget(TObject *p, int TraceIndex, TColor32 color);
protected:
	TGPSCoordinate* __fastcall readAddedPoint(void) {ResetEvent(addgpspointevent); return addedpoint;}
	void __fastcall writeAddedPoint(TGPSCoordinate* value) {addedpoint=value; if(value!=NULL) SetEvent(addgpspointevent);}
public:
	__fastcall TLinesDetector(TImgView32 *image, TBitmap32** UBmp, TGPSCoordinatesRect* cRect, TObject* RadarMapManager);
	__fastcall ~TLinesDetector();

	void __fastcall TerminateThreads(void);
	void __fastcall Rebuild(void);

	__property TBitmap32** UtilityBmp = {read=utilitybmp};
	__property TGPSCoordinatesRect* LatLonRect = {read=latlonrect};
	__property HANDLE AddGPSPointEvent = {read=addgpspointevent};
	__property TGPSCoordinate* AddedPoint = {read=readAddedPoint, write=writeAddedPoint};
	__property TList* PixelList = {read=pixellist};
	__property HANDLE ReadyForInsertingEvent = {read=readyforinsertingevent};
	__property TInsertingVars InsertingVars = {write=writeInsertingVars};
};

#endif
