//---------------------------------------------------------------------------
#pragma hdrstop

#include "SparUnit.h"
#include "Manager.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

//---------------------------------------------------------------------------
// TPipeThread
//---------------------------------------------------------------------------
void __fastcall TPipeThread::ExecuteLoopBody()
{
	unsigned long wb;
	long t;
	bool res;
	int tries;
	Spar *Data, sent;

	if(((TSparUnit*)Owner)->SparData && Owner->Connected && Owner->WriteBuf && Owner->ReadBuf)
	{
		if(WaitForSingleObject(Owner->WriteBuf->NotEmptyEvent, 0)==WAIT_OBJECT_0)
		{
			SparCmd *cmd;

			cmd=(SparCmd*)Owner->WriteBuf->Pop();
			if(cmd)
			{
#ifndef UseCollectedSparData
				WriteFile(Owner->SerialHandle, (LPCVOID)(char*)cmd, sizeof(SparCmd), &wb, NULL);
#endif
				delete cmd;
			}
		}
		else if(((TSparUnit*)Owner)->ActiveJob && WaitForSingleObject(((TSparUnit*)Owner)->SparData->SparDataReadyEvent, 0)==WAIT_OBJECT_0)
		{
			t=TickCounter;
			if(t-Timer>=delay)
			{
				Timer=t;
				((TSparUnit*)Owner)->SparData->ReBuild();
				sent=((TSparUnit*)Owner)->SparData->LastRebuilded;
#ifndef UseCollectedSparData
				res=WriteFile(Owner->SerialHandle, (LPCVOID)(char*)&sent, sizeof(Spar), &wb, NULL);
#else
            	res=true;
#endif
				Data=new Spar;
				try
				{
					if(res)
					{
						tries=0;
						wb=0;
						do
						{
						   res=ReadFile(Owner->SerialHandle, (char*)Data, sizeof(Spar), &wb, NULL);
						   tries++;
						} while(res && wb<sizeof(Spar) && CheckAttemptsQuantity(tries));
					}
#ifdef UseCollectedSparData
					if(wb==0)
					{
						SetFilePointer(Owner->SerialHandle, 0, NULL, FILE_BEGIN);
					}
					Data->pointID=sent.pointID;
#endif
					if(res && tries<MaxSerialReadWriteAttempts && sent.pointID==Data->pointID)
					{
						Owner->ReadBuf->Push(Data);
						Data=NULL;
					}
				}
				__finally
				{
					if(Data) delete Data;
				}
			}
		}
	}
	WaitForSingleObject(IdleEvent, 50);
}

//---------------------------------------------------------------------------
// TSparParserThread
//---------------------------------------------------------------------------
__fastcall TSparParserThread::TSparParserThread(TSparUnit *AOwner, TObject *AManager) : TStartStopThread()
{
	Owner=AOwner;
	Manager=AManager;
	VisualData=NULL;
#ifdef CollectSparData
	TempFile=CreateFile("SparData.txt", GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
		FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
#endif
}

__fastcall TSparParserThread::~TSparParserThread()
{
#ifdef CollectSparData
	CloseHandle(TempFile);
#endif
}

bool __fastcall TSparParserThread::CheckSparData(Spar* Data)
{
	bool res;
	double p1, p2, p3;

	res=(Manager && ((TRadarMapManager*)Manager)->Settings);

	if(res)
	{
		if(fabs(Data->offset)>0) p1=100.0*fabs(Data->dOffset/2./Data->offset);
		else p1=0;
		if(fabs(Data->depth)>0) p2=100.0*fabs(Data->dDepth/2./Data->depth);
		else p2=0;
		if(fabs(Data->yaw)>0) p3=100.0*fabs(Data->dYaw/2./Data->yaw);
		else p3=0;
		if(Data->dBField<=((TRadarMapManager*)Manager)->Settings->SparMinDbField ||
			p1>((TRadarMapManager*)Manager)->Settings->SparConfidenceMax ||
			p3>((TRadarMapManager*)Manager)->Settings->SparConfidenceMax)
		{
			res=false;
			Data->utilLatitude=0.0;
			Data->utilLongitude=0.0;
			Data->utilAltitude=0.0;
			Data->offset=0.0;
			Data->dOffset=0.0;
			Data->depth=0.0;
			Data->dDepth=0.0;
			Data->yaw=0.0;
			Data->dYaw=0.0;
		}
		else if(p2>((TRadarMapManager*)Manager)->Settings->SparConfidenceMax)
		{
			Data->depth=0.0;
			Data->dDepth=0.0;
		}
	}

	return res;
}

void __fastcall TSparParserThread::VisualizeSparData()
{
	if(Manager && VisualData && Owner && Owner->SparData)
	{
		if(CheckSparData(VisualData))
		{
			if(((TRadarMapManager*)Manager)->SparOnMap)
			{
				((TRadarMapManager*)Manager)->SparOnMap->AddSpar(Owner->SparData->RadToDeg(VisualData->utilLongitude),
					Owner->SparData->RadToDeg(VisualData->utilLatitude), *VisualData);
			}
		}
		if(((TRadarMapManager*)Manager)->GyroCompass)
		{
			bool b;
			b=((TRadarMapManager*)Manager)->GyroCompass->AutomaticUpdate;
			((TRadarMapManager*)Manager)->GyroCompass->AutomaticUpdate=false;
			((TRadarMapManager*)Manager)->GyroCompass->Pitch=VisualData->sparPitch;
			((TRadarMapManager*)Manager)->GyroCompass->Roll=VisualData->sparRoll;
			((TRadarMapManager*)Manager)->GyroCompass->Heading=VisualData->sparHeading;
			((TRadarMapManager*)Manager)->GyroCompass->AutomaticUpdate=b;
		}
	}
	VisualData=NULL;
}

void __fastcall TSparParserThread::SparInterpolateTraces(TProfile* Prof, TTrace *FirstPtr, bool Backward)
{
	//long double z, c, dlt, dln, dz, td, a, b, xx, lat1, lon1, lat2, lon2, t, t2;
	//long double UTMe1, UTMn1, UTMe2, UTMn2, de, dn;
	int n;
	double c;
	//double Tm, dTm;
	TTrace *tmp, *PtrNext;
	Spar FirstSpar, *LastSpar, dSpar;

	try
	{
		if(Prof && FirstPtr && FirstPtr->SparDataPtr)
		{
			FirstSpar=*(FirstPtr->SparDataPtr);
			if(!Backward) PtrNext=FirstPtr->PtrDn;
			else PtrNext=FirstPtr->PtrUp;
			if(PtrNext)
			{
				//tmp=FirstPtr;
				c=1.0;
				//if(!Backward) PtrNext=tmp->PtrDn;
				//else PtrNext=tmp->PtrUp;
				do
				{
					tmp=PtrNext;
					if(!Backward) PtrNext=tmp->PtrDn;
					else PtrNext=tmp->PtrUp;
					c++;
				} while(PtrNext!=NULL && !tmp->SparDataPtr);

				if(tmp && tmp->SparDataPtr)
				{
					LastSpar=tmp->SparDataPtr;
					if((FirstSpar.utilLatitude==0 && FirstSpar.utilLongitude==0 && FirstSpar.utilAltitude==0) ||
						(LastSpar->utilLatitude==0 && LastSpar->utilLongitude==0 && LastSpar->utilAltitude==0))
					{
						FirstSpar.utilLatitude=0;
						FirstSpar.utilLongitude=0;
						FirstSpar.utilAltitude=0;
						dSpar.utilLatitude=0.0;
						dSpar.utilLongitude=0.0;
						dSpar.utilAltitude=0.0;
					}
					else
					{
						dSpar.utilLatitude=(FirstSpar.utilLatitude-LastSpar->utilLatitude)/c;
						dSpar.utilLongitude=(FirstSpar.utilLongitude-LastSpar->utilLongitude)/c;
						dSpar.utilAltitude=(FirstSpar.utilAltitude-LastSpar->utilAltitude)/c;
					}
					dSpar.offset=(FirstSpar.offset-LastSpar->offset)/c;
					dSpar.dOffset=(FirstSpar.dOffset-LastSpar->dOffset)/c;
					dSpar.yaw=(FirstSpar.yaw-LastSpar->yaw)/c;
					dSpar.dYaw=(FirstSpar.dYaw-LastSpar->dYaw)/c;
					if(FirstSpar.depth==0 || LastSpar->depth==0)
						dSpar.depth=0.0;
					else dSpar.depth=(FirstSpar.depth-LastSpar->depth)/c;
					dSpar.dDepth=(FirstSpar.dDepth-LastSpar->dDepth)/c;
					dSpar.sparHeading=(FirstSpar.sparHeading-LastSpar->sparHeading)/c;
					dSpar.sparPitch=(FirstSpar.sparPitch-LastSpar->sparPitch)/c;
					dSpar.sparRoll=(FirstSpar.sparRoll-LastSpar->sparRoll)/c;
					dSpar.dBField=(FirstSpar.dBField-LastSpar->dBField)/c;
				}
				else
				{
					dSpar.utilLatitude=0.0;
					dSpar.utilLongitude=0.0;
					dSpar.utilAltitude=0.0;
					dSpar.offset=0.0;
					dSpar.dOffset=0.0;
					dSpar.yaw=0.0;
					dSpar.dYaw=0.0;
					dSpar.depth=0.0;
					dSpar.dDepth=0.0;
					dSpar.sparHeading=0.0;
					dSpar.sparPitch=0.0;
					dSpar.sparRoll=0.0;
					dSpar.dBField=0.0;
				}
				tmp=FirstPtr;
				if(!Backward) PtrNext=tmp->PtrDn;
				else PtrNext=tmp->PtrUp;
				while(PtrNext && !PtrNext->SparDataPtr)
				{
					tmp=PtrNext;
					PtrNext->SparDataPtr=new Spar;
					try
					{
						FirstSpar.utilLatitude-=dSpar.utilLatitude;   //
						FirstSpar.utilLongitude-=dSpar.utilLongitude; //
						FirstSpar.utilAltitude-=dSpar.utilAltitude;   //
						FirstSpar.offset-=dSpar.offset;           //
						FirstSpar.dOffset-=dSpar.offset;          //
						FirstSpar.yaw-=dSpar.yaw;                 //
						FirstSpar.dYaw-=dSpar.dYaw;               //
						FirstSpar.depth-=dSpar.depth;             //
						FirstSpar.dDepth-=dSpar.dDepth;           //
						FirstSpar.sparHeading-=dSpar.sparHeading; //
						FirstSpar.sparPitch-=dSpar.sparPitch;     //
						FirstSpar.sparRoll-=dSpar.sparRoll;       //
						FirstSpar.dBField-=dSpar.dBField;         //
						*(PtrNext->SparDataPtr)=FirstSpar;
						PtrNext->Pitch=FirstSpar.sparPitch;
						PtrNext->Roll=FirstSpar.sparRoll;
						PtrNext->Heading=FirstSpar.sparHeading;
					}
					catch(Exception &e)
					{
						delete PtrNext->SparDataPtr;
						PtrNext->SparDataPtr=NULL;
					}
					if(!Backward) PtrNext=tmp->PtrDn;
					else PtrNext=tmp->PtrUp;
					c++;
				}
			}
		}
	}
	catch (Exception &exception) { ; }
}
//---------------------------------------------------------------------------

void __fastcall TSparParserThread::ExecuteLoopBody()
{
	TProfile* Profile;
	TTrace* Ptr;

	if(Owner && Owner->ReadBuf && WaitForSingleObject(Owner->ReadBuf->NotEmptyEvent, 10)==WAIT_OBJECT_0)
	{
		Spar *Data;

		try
		{
			Data=(Spar*)Owner->ReadBuf->Pop();
		}
		catch (Exception &e) {Data=NULL;}
		if(Data)
		{
			if(Manager && ((TRadarMapManager*)Manager)->Settings)
			{
				if(((TRadarMapManager*)Manager)->Settings->SparAnglesCompensation)
				{
					Data->sparPitch-=DegToRad(((TRadarMapManager*)Manager)->Settings->SparPitchCompensation);
					Data->sparRoll-=DegToRad(((TRadarMapManager*)Manager)->Settings->SparRollCompensation);
					Data->sparHeading-=DegToRad(((TRadarMapManager*)Manager)->Settings->SparHeadingCompensation);
				}
				if(((TRadarMapManager*)Manager)->Settings->SparEliminateFSVCorrection)
				{
					double dLt, dLn;

					dLt=Data->latitude-Data->sparLatitude;
					dLn=Data->longitude-Data->sparLongitude;
					Data->utilLatitude+=dLt;
					Data->utilLongitude+=dLn;
				}
			}
			VisualData=Data;
			try
			{
#ifdef CollectSparData
				unsigned long wr;
				WriteFile(TempFile, (char*)Data, sizeof(Spar), &wr, NULL);
#endif
				CheckSparData(VisualData);
				try
				{
					Profile=((TRadarMapManager*)Manager)->ReceivingProfile;
				}
				catch(Exception &e)
				{
					Profile=NULL;
				}
				if(Profile)
				{
					try
					{
						if(Profile->FirstTracePtr && !Profile->FirstTracePtr->SparDataPtr)
							Ptr=Profile->FirstTracePtr;
						else if(Profile->CurrentTracePtr && !Profile->CurrentTracePtr->SparDataPtr)
							Ptr=Profile->CurrentTracePtr;
						else if(Profile->LastTracePtr && !Profile->LastTracePtr->SparDataPtr)
							Ptr=Profile->LastTracePtr;
						else Ptr=NULL;
					}
					catch (Exception &exception) { Ptr=NULL;}
				}
				else Ptr=NULL;
				if(Ptr)
				{
					if(!Ptr->SparDataPtr) Ptr->SparDataPtr=new Spar;
					*(Ptr->SparDataPtr)=*(VisualData);
					Ptr->Pitch=VisualData->sparPitch;
					Ptr->Roll=VisualData->sparRoll;
					Ptr->Heading=VisualData->sparHeading;
					SparInterpolateTraces(Profile, Ptr, Ptr->Backward);
				}
				Synchronize(VisualizeSparData);
			}
			__finally
			{
				delete VisualData;
				VisualData=NULL;
			}
		}
		else VisualData=NULL;
	}
	WaitForSingleObject(IdleEvent, 50);
	//Sleep(100);
}

//---------------------------------------------------------------------------
// TSparUnit
//---------------------------------------------------------------------------
__fastcall TSparUnit::TSparUnit(TObject* AManager, AnsiString SparShell) : TCustomSerialClient (AManager)
{
	if(FileExists(SparShell))
	{
		STARTUPINFO si;
		PROCESS_INFORMATION pi;
		bool SparAlreadyRun=false;

		ZeroMemory( &si, sizeof(si) );
		si.cb = sizeof(si);
		si.dwFlags=STARTF_FORCEOFFFEEDBACK;//STARTF_USESHOWWINDOW;
		si.wShowWindow=SW_MINIMIZE;
		ZeroMemory( &pi, sizeof(pi) );


		/*
		#include <psapi.h>

		DWORD aProcesses[1024], cbNeeded, cProcesses;
		HANDLE hProcess;
		HMODULE hMod;
		char Name[256];
		AnsiString name;

		if(EnumProcesses(aProcesses, sizeof(aProcesses), &cbNeeded ))
		{
			cProcesses=cbNeeded/sizeof(DWORD);
			for(int i=0; i<cProcesses; i++)
			{
				if(aProcesses[i]!=0)
				{
					aProcesses[i];
					hProcess=OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, false, aProcesses[i]);
					if(hProcess!=NULL)
					{
						if(EnumProcessModules(hProcess, &hMod, sizeof(hMod), &cbNeeded))
						{
							GetModuleBaseName(hProcess, hMod, Name, sizeof(Name)/sizeof(char));
							name=Name;
							if(name==ExtractFileName(SparShell))
							{
								SparAlreadyRun=true;
								break;
							}
						}
					}
				}
			}
		}*/
		if(!SparAlreadyRun)
		{
			if(!CreateProcess(NULL, SparShell.c_str(), NULL, NULL, false, 0, NULL, NULL, &si, &pi))
			{
				FieldSensViewProcess=INVALID_HANDLE_VALUE;
				ShowConnectionError("Cannot find SPAR Executable module!");
			}
			else
			{
				HWND h=FindWindow("TSplashForm", _RadarMapName);
				h=SetFocus(h);
				FieldSensViewThread=pi.hThread;
				FieldSensViewProcess=pi.hProcess;
				long t1, t0=Gr32_system::GetTickCount();
				WaitForSingleObject(pi.hProcess, ((TRadarMapManager*)Manager)->Settings->SparShellWait);
				t1=Gr32_system::GetTickCount();
				if(t1-t0<((TRadarMapManager*)Manager)->Settings->SparShellWait)
				{
					Sleep(((TRadarMapManager*)Manager)->Settings->SparShellWait-(t1-t0));
				}
				//SetFocus(FindWindow("RadarMapForm", "RadarMap"));
			}
		}
		else
		{
			FieldSensViewProcess=INVALID_HANDLE_VALUE;
		}
	}
	spardata=new TSparData();
	PipeThrd=new TPipeThread(this, Manager);
	ParserThrd=new TSparParserThread(this, Manager);
	Host="";
	//Connect();
}
//---------------------------------------------------------------------------

__fastcall TSparUnit::~TSparUnit()
{
	Disconnect();
	TerminateThreads();
	delete spardata;
	spardata=NULL;
	try
	{
		if(FieldSensViewProcess!=INVALID_HANDLE_VALUE)
		{
			TerminateProcess(FieldSensViewProcess, 0);
			CloseHandle(FieldSensViewProcess);
			CloseHandle(FieldSensViewThread);
		}
	}
	catch (Exception &e) {}
}
//---------------------------------------------------------------------------

void __fastcall TSparUnit::TerminateThreads()
{
	if(PipeThrd)
	{
		PipeThrd->Stop();
		PipeThrd->AskForTerminate();
		WaitOthers();
		if(PipeThrd->ForceTerminate()) delete PipeThrd;
		PipeThrd=NULL;
	}
	if(ParserThrd)
	{
		ParserThrd->Stop();
		ParserThrd->AskForTerminate();
		WaitOthers();
		if(ParserThrd->ForceTerminate()) delete ParserThrd;
		ParserThrd=NULL;
	}
}
//---------------------------------------------------------------------------

void __fastcall TSparUnit::Connect(AnsiString AHost)
{
	SparCmd* cmd;

    Name="Spar 300";
#ifndef UseCollectedSparData
	Host="\\\\.\\pipe\\"+AHost;
#else
	Host="SparData.txt";
#endif
	TCustomSerialClient::Connect();
	if(connected)
	{
		COMMTIMEOUTS CommTO;

		GetCommTimeouts(CommHandle, &CommTO);
		CommTO.ReadIntervalTimeout=1;
		CommTO.ReadTotalTimeoutConstant=10;
		CommTO.ReadTotalTimeoutMultiplier=1;
		CommTO.WriteTotalTimeoutMultiplier=0;
		CommTO.WriteTotalTimeoutConstant=10;
		SetCommTimeouts(CommHandle, &CommTO);

		//Start Job
		PipeThrd->Start();
		ParserThrd->Start();

		cmd=new SparCmd;
		cmd->message=SPAR_CMD_SETUP;
		if(((TRadarMapManager*)Manager)->Settings->SparSimulation)
			cmd->sparPort=999;
		else cmd->sparPort=((TRadarMapManager*)Manager)->Settings->SparCOMPortNumber;
		cmd->GNSSType=GNSS_TYPE_RADARMAP;
		cmd->panID=0;
		cmd->channelMask=0;
		cmd->frequency=((TRadarMapManager*)Manager)->Settings->SparFrequency;	//491
		cmd->avgType=((TRadarMapManager*)Manager)->Settings->SparAverageType;	//0
		cmd->avgTime=((TRadarMapManager*)Manager)->Settings->SparAverageTime;	//0
		cmd->updateRate=((TRadarMapManager*)Manager)->Settings->SparUpdateRate; //0
		cmd->distanceUnit=1; //Meters;
		cmd->depthTo=0; //top
		cmd->movingBase=((TRadarMapManager*)Manager)->Settings->SparUseBaseline;
		cmd->heightOverGround=((TRadarMapManager*)Manager)->Settings->SparElevation;	// Spar height over ground (m)
		cmd->sparToAntennaX=((TRadarMapManager*)Manager)->Settings->SparToAntennaFwd;	// Spar to antenna fwd (m)
		cmd->sparToAntennaY=((TRadarMapManager*)Manager)->Settings->SparToAntennaRight;	// Spar to antenna right (m)
		cmd->sparToAntennaZ=((TRadarMapManager*)Manager)->Settings->SparToAntennaUp;	// Spar to antenna up (m)
        WriteBuf->Push(cmd);

		StartJob();
	}
	else ShowConnectionError("Cannot connect to SPAR FieldSensView software!");
}
//---------------------------------------------------------------------------

void __fastcall TSparUnit::Disconnect()
{
	StopJob();
	WaitForSingleObject(WriteBuf->EmptyEvent, ((TRadarMapManager*)Manager)->Settings->SparShellWait);
	TCustomSerialClient::Disconnect();
	PipeThrd->Stop();
	ParserThrd->Stop();
	WriteBuf->Clear();
	ReadBuf->Clear();
}
//---------------------------------------------------------------------------

void __fastcall TSparUnit::StartJob()
{
	SparCmd* cmd=new SparCmd;

	cmd=new SparCmd;
	cmd->message=SPAR_CMD_START;
	cmd->mode=SPAR_MODE_LINE;
	cmd->surveyType = 3; //RTK
	strcpy_s(cmd->stringBuf, SPAR_STRING_BUF_MAX, ((AnsiString)("RadarMap "+MyDateTimeToStr(Now(), "yyyy-mm-dd", '-', "hh-nn-ss", '-'))).c_str());
	WriteBuf->Push(cmd);
	activejob=true;
}
//---------------------------------------------------------------------------

void __fastcall TSparUnit::StopJob()
{
	SparCmd* cmd=new SparCmd;

	activejob=false;
	cmd->message=SPAR_CMD_STOP;
	WriteBuf->Push(cmd);
}