//---------------------------------------------------------------------------

#ifndef CheckBoxTreeViewH
#define CheckBoxTreeViewH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <CheckLst.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>

UINT GetItemState(HWND hwnd, HTREEITEM hItem);

class TCheckBoxTreeView : public TTreeView
{
protected:
	void __fastcall CreateParams(Controls::TCreateParams &Params)
		{TTreeView::CreateParams(Params); Params.Style=Params.Style | TVS_CHECKBOXES;}
public:
	__fastcall virtual TCheckBoxTreeView(TWinControl* AOwner) : TTreeView(AOwner) {Parent=AOwner;}
	__fastcall virtual ~TCheckBoxTreeView(void) {;}

	bool __fastcall Checked(TTreeNode *n);
	bool __fastcall Checked(i) {return Checked(Items->Item[i]);}
	void __fastcall SetChecked(TTreeNode *n);
	void __fastcall SetChecking(TTreeNode *n, bool Checked);
	void __fastcall SetUnchecked(TTreeNode *n);
	void __fastcall ReverseChecking(TTreeNode *n);
};

#endif
