object PreviewForm: TPreviewForm
  Left = 0
  Top = 0
  BorderStyle = bsSizeToolWin
  Caption = 'Preview'
  ClientHeight = 295
  ClientWidth = 325
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Scaled = False
  OnClose = FormClose
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Image: TImgView32
    Left = 0
    Top = 0
    Width = 325
    Height = 295
    Align = alClient
    Bitmap.ResamplerClassName = 'TNearestResampler'
    BitmapAlign = baCustom
    Scale = 1.000000000000000000
    ScaleMode = smOptimalScaled
    ScrollBars.ShowHandleGrip = True
    ScrollBars.Style = rbsDefault
    ScrollBars.Visibility = svHidden
    OverSize = 0
    TabOrder = 0
  end
  object OpenDialog: TOpenDialog
    OnClose = OpenDialogClose
    OnShow = OpenDialogShow
    Filter = 
      'Kadaster.nl maps (*.xml)|*.xml|AutoCAD Drawing Interchane Files ' +
      '(*.dxf)|*.dxf|Images|*.jpg;*.jpeg;*.bmp;*.png;*.gif;*.tif;*.tiff' +
      ';*.emf;*.wmf|RadarMap Vector maps|*.dbsl|All files (*.*)|*.*'
    FilterIndex = 0
    Options = [ofFileMustExist]
    Title = 'Open map'
    OnSelectionChange = OpenDialogSelectionChange
    Left = 16
    Top = 8
  end
  object SaveDialog: TSaveDialog
    OnClose = OpenDialogClose
    OnShow = SaveDialogShow
    Filter = 'RadarMap GML data (*.gml)|*.gml'
    FilterIndex = 0
    Options = [ofOverwritePrompt, ofHideReadOnly, ofEnableSizing]
    Title = 'Save RadarMap data'
    OnSelectionChange = SaveDialogSelectionChange
    Left = 80
    Top = 8
  end
  object GMLOpenDialog: TOpenDialog
    OnClose = OpenDialogClose
    OnShow = SaveDialogShow
    Filter = 'RadarMap GML data (*.gml)|*.gml'
    FilterIndex = 0
    Options = [ofHideReadOnly, ofFileMustExist]
    Title = 'Open RadarMap data'
    OnSelectionChange = GMLOpenDialogSelectionChange
    Left = 152
    Top = 8
  end
  object ExportDialog: TSaveDialog
    OnClose = OpenDialogClose
    OnShow = SaveDialogShow
    Filter = 'AutoCAD Drawing Interchane Files (*.dxf)|*.dxf'
    FilterIndex = 0
    Options = [ofOverwritePrompt, ofHideReadOnly, ofEnableSizing]
    Title = 'XYZC'
    OnSelectionChange = SaveDialogSelectionChange
    Left = 224
    Top = 8
  end
end
