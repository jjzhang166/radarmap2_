//---------------------------------------------------------------------------

#ifndef Control_unitH
#define Control_unitH
//---------------------------------------------------------------------------

#include <Classes.hpp>
#include "Defines.h"
#include "Profile.h"
#include "SliderTools.h"
#include "MyMath.h"
//---------------------------------------------------------------------------

typedef signed char word;

const int MaxGainPointsCount = 10;
const float MaxGain = 84; // in dB
enum TBatteryStatusType {bstUnknown = -1, bstNone = 0, bstCharging = 1, bstDischarging = 2, bstDischarged = 3};
enum TBatteryType {btDefault =-1, btCustom = 0, btNiCd = 1, btNiMh = 2, btLeadAcid = 3, btLithium = 4};
enum TParsedDataType {pdtNone =-1, pdtTrace = 0, pdtInfo = 1};
enum TBytesPerSample {bpsNone = 0, bpsInt16 = 2, bpsInt24 = 3, bpsInt32 = 4};

struct TCommand
{
	AnsiString Cmd;
	unsigned char Buf[256];
	int BytesLength;
	bool ByteCommand;
	bool NeedSynchroReply;
	int CommandReplyLength;
};

struct TGainPoint
{
private:
	float X, Y;
	bool locked;

	void writeX(float value) {if(!locked && value>=0 && value<=1.) X=value;}
	void writeY(float value) {if(!locked && value>=0 && value<=MaxGain) Y=value;}
public:
	TGainPoint() {locked=false; X=Y=0;}
	TGainPoint(float _x, float _y) {locked=false; x=_x; y=_y;}

	__property float x = {read=X, write=writeX}; // Normalized samples (0..1)
	__property float y = {read=Y, write=writeY}; // Amplitude in dB
	__property bool Locked = {read=locked, write=locked};

	bool operator ==(const TGainPoint& rc) const {return X==rc.x && Y==rc.y;}
	bool operator !=(const TGainPoint& rc) const {return X!=rc.x || Y!=rc.y;}
};

class TParsedData
{
private:
protected:
	int n;
	void* owner;
	TParsedDataType type;
public:
	__fastcall TParsedData(int aN, void* AOwner, TParsedDataType AType) {type=AType; n=aN; owner=AOwner;}
	__fastcall ~TParsedData() {}

	__property int N = {read=n};
	__property void* Settings = {read=owner};
	__property TParsedDataType Type = {read=type};
};

class TParsedTraceData : public TParsedData
{
private:
	double* rxdata;
	float ds;
	float wheelpulses; // + - Toward, - - Backward
	float batteryvalue;
	int channelnumber;
public:
	__fastcall TParsedTraceData(int aN, void* AOwner) : TParsedData(aN, AOwner, TParsedDataType::pdtTrace) {rxdata=new double[aN]; wheelpulses=0; ds=0; batteryvalue=0; channelnumber=0;}
	__fastcall ~TParsedTraceData() {if(rxdata) delete[] rxdata; rxdata=NULL;}

	__property double* RxData = {read=rxdata};
	__property float dS = {read=ds, write=ds};
	__property float WheelPulses = {read=wheelpulses, write=wheelpulses}; // + - Toward, - - Backward
	__property float BatteryValue = {read=batteryvalue, write=batteryvalue};
	__property int ChannelNumber = {read=channelnumber, write=channelnumber};
};

class TGainFunction : public TObject
{
private:
	int gainpointscount;
	int realsamples;
	TGainPoint *gainpoints;
	double *gainfunction;
	HANDLE WaitWhileCopyEvent;
	bool applygain;
protected:
	void __fastcall writeGainPointsCount(int value);
	void __fastcall writeRealSamples(int value);
	TGainPoint __fastcall readGainPoint(int i);
	void __fastcall writeGainPoint(int i, TGainPoint value);
	double __fastcall readGainFunction(int i);
	void __fastcall writeApplyGain(bool value);
public:
	int *SavedGainPoints;
	void __fastcall Copy(TGainFunction* Source);
	__fastcall TGainFunction(int ms);
	__fastcall ~TGainFunction();
	void __fastcall RebuildGainFunction();
	void __fastcall CopyToProfile(TProfile *Prof, bool UpdateTraces=false);
	void __fastcall CopyFromProfile(TProfile *Prof);
	TGainPoint* __fastcall InsertGainPoint(TGainPoint gp) {return InsertGainPoint((int)(gp.x*(float)realsamples), gp.y);}
	TGainPoint* __fastcall InsertGainPoint(int sample, float dB);
	bool __fastcall DeleteGainPoint(int id);
	bool __fastcall DeleteGainPoint(TGainPoint gp);
	TGainPoint* GetPointPtr(int Index) {if(Index>=0 && Index<gainpointscount) return (TGainPoint*)(gainpoints+Index); else return NULL;}

	__property int GainPointsCount = {read=gainpointscount, write=writeGainPointsCount};
	__property int RealSamples = {write=writeRealSamples, read=realsamples};
	__property TGainPoint GainPoints[int i] = {read=readGainPoint, write=writeGainPoint};
	__property double GainFunction[int i] = {read=readGainFunction};
	__property bool ApplyGain = {read=applygain, write=writeApplyGain};
};

class TTuneData : public TObject
{
private:
	int t_delay;
protected:
	void __fastcall write_t_delay(int value);
public:
	__property int t_DELAY = {read=t_delay, write=write_t_delay, default=0};

	__fastcall TTuneData();
	//__fastcall void SaveData(TXMLDocument* wr);
	//__fastcall bool LoadData(TXmlDocument* rd);
};

class TFrequencyFilter: public TObject
{
private:
	int samples, samplespower;
	int timerange;
	double frequencyrange; //calculating while FilterReCalcul()
	double frontlow, fronthigh, cuthigh, cutlow; //!!! in MHz !!!
	double *fr;
	bool automaticupdate;
protected:
	void __fastcall FilterReCalcul();
	void __fastcall writeSamples(int value);
	void __fastcall writeTimeRange(int value) {if(timerange!=value) {timerange=value; if(automaticupdate) FilterReCalcul();}}
	double __fastcall readFRi(int i) {if(i<0) i=0; else if(i>=samples*2) i=samples*2-1; return fr[i];}
	void __fastcall writeFrontLow(double value) {if(frontlow!=value) {frontlow=value; if(automaticupdate) FilterReCalcul();}}
	void __fastcall writeFrontHigh(double value) {if(fronthigh!=value) {fronthigh=value; if(automaticupdate) FilterReCalcul();}}
	void __fastcall writeCutHigh(double value) {if(cuthigh!=value) {cuthigh=value; if(automaticupdate) FilterReCalcul();}}
	void __fastcall writeCutLow(double value) {if(cutlow!=value) {cutlow=value; if(automaticupdate) FilterReCalcul();}}
	void __fastcall writeAutomaticUpdate(bool value) {if(automaticupdate!=value) {automaticupdate=value; if(automaticupdate) FilterReCalcul();}}
public:
	__property int Samples = {read=samples, write=writeSamples};
	__property int TimeRange = {read=timerange, write=writeTimeRange}; //in nS
	__property double FrequencyRange = {read=frequencyrange};
	__property double FrontLow = {read=frontlow, write=writeFrontLow};
	__property double FrontHigh = {read=fronthigh, write=writeFrontHigh};
	__property double CutHigh = {read=cuthigh, write=writeCutHigh};
	__property double CutLow = {read=cutlow, write=writeCutLow};
	__property double *FR = {read=fr}; //Frequency Responce
	__property double FRi[int i] = {read=readFRi}; //Frequency Responce
	__property bool AutomaticUpdate = {read=automaticupdate, write=writeAutomaticUpdate};

	__fastcall TFrequencyFilter(int ASamples, int ATimeRange) {samples=ASamples; timerange=ATimeRange; frontlow=fronthigh=cuthigh=cutlow=0.; fr=new double[samples*2]; automaticupdate=true; FilterReCalcul();}
	__fastcall ~TFrequencyFilter() {delete[] fr;}
};

class TAntenna : public TObject
{
private:
	int timerange;
	int MaxTimeRange;
	int TimeRangeQuantity;
	int selectedtimerange, tunetimerange, localmaxtimerange;
	AnsiString name;
	double frequency; // central frequency in Mhz in free space
	TAntennaType type;
	TAntennaShielded shielded;
	double offset; // Offset between RX & TX antennas, m
	TTuneData** td;
	TFrequencyFilter *preset;
	int samples;
protected:
	void __fastcall writeSelectedTimeRangeIndex(int value);
	void __fastcall writeTimeRange(int value);
	void __fastcall writeSamples(int value);
	TTuneData* __fastcall readSelectedTD() {return TD[selectedtimerange];}
	TTuneData* __fastcall readTD(int i) {if(i>=0 && i<TimeRangeQuantity) return td[i]; else return NULL;}
public:
	__fastcall TAntenna(int trq, int mtr, AnsiString nm, int freq, TAntennaType at, TAntennaShielded ash, float dx, int ASamples);
	__fastcall ~TAntenna();
	void __fastcall SetPresetFilter();
	void __fastcall SetPresetFilter(double frontlow, double fronthigh, double cuthigh, double cutlow);

	__property int SelectedTimeRangeIndex = {read=selectedtimerange, write=writeSelectedTimeRangeIndex}; // Time's range number specified
	__property int TimeRange = {read=timerange, write=writeTimeRange};
	__property int TuneTimeRange = {read=tunetimerange};
	__property int LocalMaxTimeRange = {read=localmaxtimerange};
	__property TTuneData* 	SelectedTD = {read=readSelectedTD};
	__property AnsiString Name = {read=name, write=name};
	__property double Frequency = {read=frequency, write=frequency}; // central frequency in Mhz in free space
	__property TAntennaType Type = {read=type, write=type};
	__property TAntennaShielded Shielded = {read=shielded, write=shielded};
	__property double Offset = {read=offset, write=offset}; // Offset between RX & TX antennas, m
	__property TTuneData* TD[int i] = {read=readTD};
	__property TFrequencyFilter* Preset = {read=preset};
	__property int Samples = {read=samples, write=writeSamples};
};

class TChannel : public TObject
{
private:
	TObject* owner;
	int selectedindex;
	int channelnumber;
	TAntenna* selected;
	int MaxTimeRange;
	int TimeRangeQuantity;
	int realsamples;
	int HPFquantity;
	THPFState selectedHPF;
	THPFType HPFType;
	TStrings* antennanames;
// receiving variables
	double* StackingBuf;
	int stack;
	TTrace* currenttrace;
	TTraceAccumulator* traceaccumulator;
	HANDLE CurrentTraceAccessEvent;
	TProfile* profile;
	HANDLE markpressedevent;
	float wheelpulsessum;
	float wheelpulsesstack;
	float modbusdistsum;
	bool ProfileLinked;
	TFourierFamily *MyMath;
	TGainFunction* gain;
protected:
	void __fastcall writeSelectedAntennaIndex(int value);
	void __fastcall writeTimeRange(int value);
	int __fastcall readTimeRange();
	void __fastcall writeRealSamples(int value);
	void __fastcall writeSelectedHPF(THPFState value);
	void __fastcall writePulseDelay(int value);
	int __fastcall readPulseDelay();
	TStrings* __fastcall readAntennaNames();
	bool __fastcall LockCurrentTrace();
	void __fastcall UnlockCurrentTrace();
	TTrace* __fastcall readCurrentTrace();
	void __fastcall writeGain(TGainFunction* value) {if(gain!=value){ gain=value;}}
public:
	TList* Antennas;

	short *TraceBuf;
	__property int 					SelectedAntennaIndex = {read=selectedindex, write=writeSelectedAntennaIndex};
	__property int 					ChannelNumber = {read=channelnumber};
	__property TAntenna* 			SelectedAntenna = {read=selected};
	__property int 					TimeRange = {read=readTimeRange, write=writeTimeRange};
	__property int 					RealSamples = {read=realsamples, write=writeRealSamples};
	__property THPFState 			SelectedHPF = {read=selectedHPF, write=writeSelectedHPF};
	__property int 					PulseDelay = {read=readPulseDelay, write=writePulseDelay};
	__property TStrings* 			AntennaNames = {read=readAntennaNames};
	__property TTrace*				CurrentTrace = {read=readCurrentTrace}; // !!! Access to CurrentTrace could HangUp the process !!!
	__property TTraceAccumulator*	TraceAccumulator = {read=traceaccumulator}; // !!! Access to CurrentTrace could HangUp the process !!!
	__property TProfile*			Profile = {read=profile, write=profile, default=NULL};
	__property HANDLE				MarkPressedEvent = {read=markpressedevent, write=markpressedevent};
	__property float				WheelPulsesSum = {read=wheelpulsessum, write=wheelpulsessum};
	__property float				WheelPulsesStack = {read=wheelpulsesstack, write=wheelpulsesstack};
	__property float				ModBusDistSum = {read=modbusdistsum, write=modbusdistsum};
	__property TObject* 			Owner = {read=owner};
	__property TGainFunction* 		Gain = {read=gain, write=writeGain};

	__fastcall TChannel(TObject* AOwner, int ChN, THPFType ft, int maxtr, int trq, int ms);
	__fastcall ~TChannel();
	//void __fastcall SaveChannel(XmlWriter* wr);
	//__fastcall bool LoadChannel(XmlReader* rd);
	void __fastcall ClearAntennas();
	void __fastcall AddAntenna(TAntenna* ant);
	void __fastcall DeleteAntenna(int index);
	int __fastcall AntennasCount();
	TProfile* __fastcall LinkProfile(void) {ProfileLinked=true; return profile;}
// receiving methods
	void __fastcall RebuildAndStackData(TParsedTraceData* Data, bool Stack);
	bool __fastcall IsStacked(int N);
	void __fastcall ResetChannelCollecting() {stack=wheelpulsesstack=0; wheelpulsessum==0.0;}
};

class TGPRUnit : public TObject
{
private:
	AnsiString filename;
	int selectedchannel;
	int uschannelquantity;
	bool battery;
	AnsiString BufStr;
	AnsiString Regims[3];
	int stacking;
	TPositioning positioning;
	word markcounter;
	float pulsespertrace;
	float tracedistance;
	int wheelpulses;
	float wheeldiameter;
	float wheelstep;
	TChannelMode channelmode;
	int WSWinWidth, WSSamples, WSTracesCounter;
	long double *WSBuffer;
	bool needsynchro;
	int synchrolength;
	unsigned char *synchroword;
protected:
	TObject* manager;
	TBatteryType BatteryType;
	int samples, oldsamples, rxsamples, realsamples, rxpacketsize, spacecenter, sigmasamples, spacequart;
	bool SamplesChanged;
	short *SpaceBuf;
	int SpaceBufSize;
	int SkipedTQ, OldBV, Old_dBV;
	AnsiString name;
	TBytesPerSample bps;
	void __fastcall writeSamples(int value);
	void __fastcall writeChannelQuantity(int value);
	void __fastcall writeSelectedChannel(int value);
	void __fastcall writeBattery(bool value);
	TChannel* __fastcall readSelectedChannel();
	void __fastcall writeStacking(int value);
	void __fastcall writeTraceDistance(float value);
	void __fastcall writeWheelPulses(int value);
	void __fastcall writeWheelDiameter(float value);
	void __fastcall writeChannelMode(TChannelMode value);
	void __fastcall SaveSettingsByName(AnsiString Chapter);
	bool __fastcall LoadSettingsByName(AnsiString Chapter);
	void __fastcall writeSynchroLength(int value) {if(synchrolength!=value) {synchrolength=value; if(synchroword) delete[] synchroword; if(value>0) synchroword=new unsigned char[value]; else synchroword=NULL;}}
public:
	TChannel** Channels;
	AnsiString UnitName; //SetupDialog Caption
	TRadarType UnitType;
	TRegime REGIME;
	// SetupDialogFields
	int sdfSamplesQuantity; // How much different samples options
	THPFType sdfHPFType;
	bool sdfIsPulseDelay;
	int sdfTimeRangeQuantity, sdfMaxTimeRange; //
	bool sdfBattery;
	bool sdfCables; // To XML... !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	// UnitSettings
	int Medium;
	double Permitivity;
	TCables Cables; // To XML... !!!!!!!!!!!!!!!!!!!!!!!!!!!!
	int TracesPerSec; // To XML... !!!!!!!!!!!!!!!!!!!!!!!!!!!!
	bool ApplyConstantDeduct;

	__property int 			RxPacketSize = {read=rxpacketsize};
	__property int 			RxSamples = {read=rxsamples};
	__property int 			RealSamples = {read=realsamples};
	__property int 			SpaceCenter = {read=spacecenter};
	__property int 			SigmaSamples = {read=sigmasamples};
	__property int 			SpaceQuart = {read = spacequart};
	__property int 			Samples = {read=samples, write=writeSamples};
	__property int			OldSamples = {read=oldsamples, write=oldsamples};
	__property TChannel* 	SelectedChannel = {read=readSelectedChannel};
	__property int 			ChannelQuantity = {read=uschannelquantity, write=writeChannelQuantity};
	__property int 			SelectedChannelIndex = {read=selectedchannel, write=writeSelectedChannel};
	__property TChannelMode SelectedChannelMode = {read=channelmode, write=writeChannelMode};
	__property bool 		Battery = {read=battery, write=writeBattery};
	__property int 			Stacking = {read=stacking, write=writeStacking, default=1}; // Stacking
	__property AnsiString 	FileName = {read=filename, write=filename};
	__property TPositioning Positioning = {read=positioning, write=positioning};
	__property word			MarkCounter = {read=markcounter};
	__property float		PulsesPerTrace = {read=pulsespertrace};
	__property float 		TraceDistance = {read=tracedistance, write=writeTraceDistance}; // in Meters
	__property int 			WheelPulses = {read=wheelpulses, write=writeWheelPulses};
	__property float 		WheelDiameter = {read=wheeldiameter, write=writeWheelDiameter}; // in Meters
	__property float 		WheelStep = {read=wheelstep}; // in Meters
	__property AnsiString 	Name = {read=name};
	__property TObject*     Manager = {read=manager};
	__property bool NeedSynchroReply = {read=needsynchro, write=needsynchro};
	__property int SynchroLength = {read=synchrolength, write=writeSynchroLength};
	__property unsigned char *SynchroWord = {read=synchroword};
	__property TBytesPerSample BpS = {read=bps};

	__fastcall TGPRUnit(AnsiString fn, TRadarType ut, TRegime RG, TObject* RadarMapManager);
	__fastcall ~TGPRUnit();

	virtual AnsiString __fastcall GetTuneCommandString();
	virtual TCommand __fastcall GetTuneCommand();
	virtual void __fastcall ReInit(AnsiString fn, TRadarType ut, TRegime RG);
	virtual void __fastcall PrepareTransmitterOFF();
	virtual TCommand __fastcall GetStartCommand();
	virtual TCommand __fastcall GetStopCommand();
	virtual TParsedData* __fastcall ReceivedDataParser(signed char* Buf, int BufLength);
	virtual AnsiString __fastcall AsString();
	virtual void __fastcall ApplySamples(bool Force=false);

	virtual void __fastcall SaveSettings() {}
	virtual bool __fastcall LoadSettings() {return true;}

	AnsiString __fastcall GetSettingsInfo(int ChIndex);
	void __fastcall SetMarkPressetEvent();
	void __fastcall ResetMarkPressetEvent();
	void __fastcall ResetChannelCollecting();
	void __fastcall CreateChannelProfiles();
	void __fastcall DeleteChannelProfiles();

	//Filters
	void __fastcall WeightedSubtractionFilter(TSample* RxDataIn, TSample* RxDataOut, int N);
};

class TBattery: public TIndicator
{
private:
	int BatteryValuesQ;
	float *BatteryValues;
	int Index;
	float MaxBatteryVoltage;
	float MaxBatteryValue;
	TBatteryStatusType BatteryStatus;
	TBatteryType batterytype;
	float BatteryVoltage, BatteryPercent;
	float Model[11];
	int *IconIndex, MaxIndexes, PrevIndex;
	AnsiString UndefinedBatteryButton;
	void __fastcall ApplyIconOnButton();
	void __fastcall writeBatteryType(TBatteryType value);
protected:
public:
	__fastcall TBattery();
	__fastcall ~TBattery();

	void __fastcall AddValue(float BV);
	void __fastcall ApplyDischargeModel(float const In[11]);

	__property TBatteryStatusType Status = {read=BatteryStatus};
	__property float Voltage = {read=BatteryVoltage};
	__property float Percentage = {read=BatteryPercent};
	__property TBatteryType BatteryType = {read=batterytype, write=writeBatteryType};
};

class TLaptopBattery: public TIndicator
{
private:
	TTimerThread *CheckBatteryTmr;
	TBatteryStatusType BatteryStatus;
	float BatteryPercent;
	int BatteryLife; //in seconds
	int *IconIndex, MaxIndexes, PrevIndex;
	AnsiString DesktopBatteryButton;
	void __fastcall ApplyIconOnButton();
	void __fastcall OnTimer();
protected:
public:
	__fastcall TLaptopBattery();
	__fastcall ~TLaptopBattery();

	__property TBatteryStatusType Status = {read=BatteryStatus};
	__property float Percentage = {read=BatteryPercent};
	__property int LifeTime = {read=BatteryLife}; //in seconds;
};

class TGPR_Single1 : public TGPRUnit
{
private:
public:
	__fastcall TGPR_Single1(AnsiString fn, TRegime RG, TObject* Manager, TRadarType ut=TRadarType::SINGLE1) : TGPRUnit(fn, ut, RG, Manager) {name="SINGLE1"; BatteryType=btLeadAcid;}

	void __fastcall SaveSettings() {TGPRUnit::SaveSettingsByName("Single");}
	bool __fastcall LoadSettings() {return TGPRUnit::LoadSettingsByName("Single");}

	TParsedData* __fastcall ReceivedDataParser(signed char* Buf, int BufLength);
	void __fastcall AddUnknownName(AnsiString str) {name="UNKNOWN "+str;}
};

class TGPR_Single2 : public TGPR_Single1
{
private:
public:
	__fastcall TGPR_Single2(AnsiString fn, TRegime RG, TObject* Manager) : TGPR_Single1(fn, RG, Manager, TRadarType::SINGLE2) {name="SINGLE2"; BatteryType=btLithium;}

	void __fastcall SaveSettings() {TGPRUnit::SaveSettingsByName("Single2");}
	bool __fastcall LoadSettings() {return TGPRUnit::LoadSettingsByName("Single2");}
};

class TGPR_Double1 : public TGPRUnit
{
public:
	__fastcall TGPR_Double1(AnsiString fn, TRegime RG, TObject* Manager) : TGPRUnit(fn, TRadarType::DOUBLE1, RG, Manager) {name="DOUBLE1"; BatteryType=btLeadAcid;}

	void __fastcall SaveSettings() {TGPRUnit::SaveSettingsByName("Double");}
	bool __fastcall LoadSettings() {return TGPRUnit::LoadSettingsByName("Double");}
	TParsedData* __fastcall ReceivedDataParser(signed char* Buf, int BufLength);
};

class TGPR_FullDouble : public TGPRUnit
{
private:
public:
	__fastcall TGPR_FullDouble(AnsiString fn, TRegime RG, TObject* Manager) : TGPRUnit(fn, TRadarType::DOUBLE2, RG, Manager) {name="DOUBLE2"; BatteryType=btLeadAcid;}

	void __fastcall SaveSettings() {TGPRUnit::SaveSettingsByName("FullDouble");}
	bool __fastcall LoadSettings() {return TGPRUnit::LoadSettingsByName("FullDouble");}
	//virtual AnsiString __fastcall GetTuneCommand();
	//virtual void __fastcall PrepareTransmitterOFF();
	TParsedData* __fastcall ReceivedDataParser(signed char* Buf, int BufLength);
};

class TGPR_Advanced : public TGPRUnit
{
private:
public:
	__fastcall TGPR_Advanced(AnsiString fn, TRegime RG, TObject* Manager, TRadarType ut=TRadarType::DOUBLE9) : TGPRUnit(fn, ut, RG, Manager) {name="DOUBLE9"; BatteryType=btLithium;}

	virtual void __fastcall SaveSettings() {TGPRUnit::SaveSettingsByName("Advanced");}
	virtual bool __fastcall LoadSettings() {return TGPRUnit::LoadSettingsByName("Advanced");}
	//virtual AnsiString __fastcall GetTuneCommand();
	//virtual void __fastcall PrepareTransmitterOFF();
	virtual TParsedData* __fastcall ReceivedDataParser(signed char* Buf, int BufLength);
};

class TGPR_Dual10 : public TGPR_Advanced
{
private:
public:
	__fastcall TGPR_Dual10(AnsiString fn, TRegime RG, TObject* Manager) : TGPR_Advanced(fn, RG, Manager, TRadarType::DUAL10) {name="DUAL10"; BatteryType=btLithium;}

	void __fastcall SaveSettings() {TGPRUnit::SaveSettingsByName("Dual10");}
	bool __fastcall LoadSettings() {return TGPRUnit::LoadSettingsByName("Dual10");}
};

class TGPR_Cobra : public TGPR_Advanced
{
private:
public:
	__fastcall TGPR_Cobra(AnsiString fn, TRegime RG, TObject* Manager, TRadarType ut=TRadarType::COBRA) : TGPR_Advanced(fn, RG, Manager, ut) {name="COBRA"; BatteryType=btLithium;}

	void __fastcall SaveSettings() {TGPRUnit::SaveSettingsByName("Cobra");}
	bool __fastcall LoadSettings() {return TGPRUnit::LoadSettingsByName("Cobra");}
};

class TGPR_CobraCBD : public TGPR_Cobra
{
private:
public:
	__fastcall TGPR_CobraCBD(AnsiString fn, TRegime RG, TObject* Manager) : TGPR_Cobra(fn, RG, Manager, TRadarType::COBRA_CBD) {name="COBRA_CBD"; BatteryType=btLithium;}

	void __fastcall SaveSettings() {TGPRUnit::SaveSettingsByName("CobraCBD");}
	bool __fastcall LoadSettings() {return TGPRUnit::LoadSettingsByName("CobraCBD");}
};

class TGPR_Python3 : public TGPRUnit
{
public:
	__fastcall TGPR_Python3(AnsiString fn, TRegime RG, TObject* Manager, TRadarType RT=TRadarType::PYTHON3) : TGPRUnit(fn, RT, RG, Manager) {name="PYTHON3"; BatteryType=btNiMh;}

	virtual void __fastcall SaveSettings() {TGPRUnit::SaveSettingsByName("Python3");}
	virtual bool __fastcall LoadSettings() {return TGPRUnit::LoadSettingsByName("Python3");}
	//virtual AnsiString __fastcall GetTuneCommand();
	//virtual void __fastcall PrepareTransmitterOFF();
	TParsedData* __fastcall ReceivedDataParser(signed char* Buf, int BufLength);
};

class TGPR_Python3bis : public TGPR_Python3
{
public:
	__fastcall TGPR_Python3bis(AnsiString fn, TRegime RG, TObject* Manager) : TGPR_Python3(fn, RG, Manager, PYTHON3bis) {name="PYTHON3bis"; BatteryType=btLithium;}

	void __fastcall SaveSettings() {TGPRUnit::SaveSettingsByName("Python3bis");}
	bool __fastcall LoadSettings() {return TGPRUnit::LoadSettingsByName("Python3bis");}
};

class TGPR_InfraRadar : public TGPRUnit
{
private:
public:
	__fastcall TGPR_InfraRadar(AnsiString fn, TRegime RG, TObject* Manager, TRadarType RT=TRadarType::INFRARADAR) : TGPRUnit(fn, RT, RG, Manager) {name="INFRARADAR"; BatteryType=btLithium;}

	virtual void __fastcall SaveSettings() {TGPRUnit::SaveSettingsByName("InfraRadar");}
	virtual bool __fastcall LoadSettings() {return TGPRUnit::LoadSettingsByName("InfraRadar");}
	//virtual AnsiString __fastcall GetTuneCommand();
	//virtual void __fastcall PrepareTransmitterOFF();
	TParsedData* __fastcall ReceivedDataParser(signed char* Buf, int BufLength);
	void __fastcall ApplySamples(bool Force=false);
};

class TGPR_InfraRadar100 : public TGPR_InfraRadar
{
private:
public:
	__fastcall TGPR_InfraRadar100(AnsiString fn, TRegime RG, TObject* Manager) : TGPR_InfraRadar(fn, RG, Manager, TRadarType::INFRARADAR100) {name="INFRARADAR100";}

	void __fastcall SaveSettings() {TGPRUnit::SaveSettingsByName("InfraRadar100");}
	bool __fastcall LoadSettings() {return TGPRUnit::LoadSettingsByName("InfraRadar100");}
};

class TTracePart
{
private:
	int part, parts, samples, buflength;
	TBytesPerSample bps;
	char *buf;
protected:
public:
	__fastcall TTracePart(int aPart, int aParts, int aSamples, TBytesPerSample aBpS, int aBufLength) {part=aPart; parts=aParts; samples=aSamples; bps=aBpS; buflength=aBufLength; buf=new char[buflength];}
	__fastcall ~TTracePart() {delete[] buf;}
	void __fastcall FillBuf(char *Data) {memcpy(buf, Data, buflength);}
	__property int Part = {read=part};
	__property int Parts = {read=parts};
	__property int Samples = {read=samples};
	__property TBytesPerSample BpS = {read=bps};
	__property int BufLength = {read=buflength};
	__property char *Buf = {read=buf};
};

class TPartialTrace
{
private:
	TTracePart **traceparts;
	int parts, samples, buflength;
	TBytesPerSample bps;
	bool completed;
protected:
	void __fastcall Clear() {if(traceparts) for(int i=0; i<parts; i++) if(traceparts[i]) {delete traceparts[i]; traceparts[i]=NULL;} completed=false;}
public:
	__fastcall TPartialTrace();
	__fastcall ~TPartialTrace();

	void __fastcall AddTracePart(TTracePart *tp);
	bool __fastcall GetTraceData(char *Data);

	__property int Parts = {read=parts};
	__property int Samples = {read=samples};
	__property TBytesPerSample BpS = {read=bps};
	__property int BufLength = {read=buflength};
	__property bool Completed = {read=completed};
};

class TGPR_Zynq : public TGPRUnit
{
private:
protected:
	char *command_buf;
	int cmd_i;
	int ID_SIZE, PARAM_COUNT, PARAM_SIZE, COMMAND_NAME_SIZE, DATA_MAX_SIZE,
		CRC_SIZE, MAX_COMMAND_SIZE;
	char COMMAND_START, COMMAND_ID_CH, COMMAND_CRC_CH;
	AnsiString INVALID_COMMAND;
	bool crc;
	TTracePart *TracePart;
	TPartialTrace *PartialTrace;
	bool IsBufDigitsOnly(AnsiString buf, bool Hex);
	unsigned char Crc8(unsigned char *pcBlock, unsigned char len);
	bool parse_command(char *buf, int N, AnsiString &name, AnsiString &id, AnsiString *params, char *data, int *datasize);
public:
	__fastcall TGPR_Zynq(AnsiString fn, TRegime RG, TObject* Manager, TRadarType RT=TRadarType::ZYNQ_0);
	__fastcall ~TGPR_Zynq() {delete[] command_buf; command_buf=NULL;}

	virtual void __fastcall SaveSettings() {TGPRUnit::SaveSettingsByName("Zynq_0");}
	virtual bool __fastcall LoadSettings() {return TGPRUnit::LoadSettingsByName("Zynq_0");}

	//virtual AnsiString __fastcall GetTuneCommand();
	virtual TCommand __fastcall GetStartCommand();
	virtual TCommand __fastcall GetStopCommand();
	virtual TCommand __fastcall GetInvalidCommand();

	//virtual void __fastcall PrepareTransmitterOFF();
	TParsedData* __fastcall ReceivedDataParser(signed char* Buf, int BufLength);
	void __fastcall ApplySamples(bool Force=false);

	__property bool CRC = {read=crc, write=crc};
};

#endif
