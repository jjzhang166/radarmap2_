//---------------------------------------------------------------------------

#ifndef SliderToolsH
#define SliderToolsH
//---------------------------------------------------------------------------

#include "MyGraph.h"

enum TSliderToolButtonAlign {stbaDefault, stbaFirst, stbaLast};

class TSliderToolButton: public TControlObject
{
private:
	bool enabled, down, drawdownframe, specialplace, showcaption, freeglyphonchange;
	int initialwidth, initialheight;
	TSliderToolButtonAlign align;
	AnsiString caption;
	TFont *font;
	TBitmap32 *normalglyph, *disabledglyph, *hotglyph, *pressedglyph;
	AnsiString normalglyphname, disabledglyphname, hotglyphname, pressedglyphname;
	TBitmap32 *Glyph;
	TMouseBool onminimizing;
protected:
	void __fastcall writePressed(bool value); //overload
	void __fastcall writeInRect(bool value); //overload
	int __fastcall readLeft() {return Rect.left;}
	int __fastcall readTop() {return Rect.top;}
	int __fastcall readWidth() {return Rect.Width();}
	int __fastcall readHeight() {return Rect.Height();}
	void __fastcall writeEnabled(bool value);
	void __fastcall writeDown(bool value);
	void __fastcall writeDrawDownFrame(bool value);
	void __fastcall writeShowCaption(bool value) {showcaption=value; if(Owner!=NULL) Owner->SetObjectChanged(objecttype);}
	void __fastcall writeCaption(AnsiString value) {caption=value; if(Owner!=NULL) Owner->SetObjectChanged(objecttype);}
	void __fastcall writeLeft(int value) {int w=Rect.Width(); Rect.left=value; Rect.right=value+w;}
	void __fastcall writeTop(int value) {int h=Rect.Height(); Rect.top=value; Rect.bottom=value+h;}
	void __fastcall writeHeight(int value) {Rect.bottom=Rect.top+value; if(smallsize) initialheight=value*2; else initialheight=value;}
	void __fastcall writeWidth(int value) {Rect.right=Rect.left+value; 	if(smallsize) initialwidth=value*2; else initialwidth=value;}
	void __fastcall writeAlign(TSliderToolButtonAlign value);
	/*void __fastcall writeGlyph(TBitmap32 *value);
	void __fastcall writeHotGlyph(TBitmap32 *value);
	void __fastcall writeDisabledGlyph(TBitmap32 *value);
	void __fastcall writePressedGlyph(TBitmap32 *value);*/
	void __fastcall writeGlyphName(AnsiString value) {if(normalglyphname!=value) {if(normalglyphname!="") {RemovePngFromContainer(normalglyphname); normalglyph=NULL;} if(value!="") normalglyph=GetPngFromContainer(value); normalglyphname=value;}}
	void __fastcall writeHotGlyphName(AnsiString value) {if(hotglyphname!=value) {if(hotglyphname!="") {RemovePngFromContainer(hotglyphname); hotglyph=NULL;} if(value!="") hotglyph=GetPngFromContainer(value); hotglyphname=value;}}
	void __fastcall writeDisabledGlyphName(AnsiString value) {if(disabledglyphname!=value) {if(disabledglyphname!="") {RemovePngFromContainer(disabledglyphname); disabledglyph=NULL;} if(value!="") disabledglyph=GetPngFromContainer(value); disabledglyphname=value;}}
	void __fastcall writePressedGlyphName(AnsiString value) {if(pressedglyphname!=value) {if(pressedglyphname!="") {RemovePngFromContainer(pressedglyphname); pressedglyph=NULL;} if(value!="") pressedglyph=GetPngFromContainer(value); pressedglyphname=value;}}
	void __fastcall writeGroupIndex(int value);
	void __fastcall SmallResize();
	void __fastcall writeOnMinimizing(TMouseBool value) {onminimizing=value;}
public:
	__fastcall TSliderToolButton(TMyObject *AOwner, Types::TRect Rect);
	__fastcall ~TSliderToolButton();

	//void __fastcall DeleteItself();
	void __fastcall Redraw(TObject *Sender, TBitmap32 *Bmp);
	void __fastcall MouseMove(System::TObject* Sender,
		  Classes::TShiftState Shift, int X, int Y, TCustomLayer *Layer);
	void __fastcall MouseDown(TObject *Sender,
		  TMouseButton Button, TShiftState Shift, int X, int Y,
		  TCustomLayer *Layer);
	void __fastcall MouseUp(TObject *Sender,
		  TMouseButton Button, TShiftState Shift, int X, int Y,
		  TCustomLayer *Layer);
	void __fastcall MouseClick(System::TObject* Sender, int X, int Y);
	void __fastcall MouseDblClick(System::TObject* Sender, int X, int Y);

	//void __fastcall Hide();

	__property bool Enabled = {read=enabled, write=writeEnabled};
	__property bool Down = {read=down, write=writeDown};
	__property bool DrawDownFrame = {read=drawdownframe, write=writeDrawDownFrame};
	__property bool SpecialPlace = {read=specialplace};
	__property bool ShowCaption = {read=showcaption, write=writeShowCaption};
	__property bool FreeGlyphOnChange = {read=freeglyphonchange, write=freeglyphonchange};
	__property int Left = {read=readLeft, write=writeLeft};
	__property int Top = {read=readTop, write=writeTop};
	__property int Width = {read=readWidth, write=writeWidth};
	__property int Height = {read=readHeight, write=writeHeight};
	__property TSliderToolButtonAlign Align = {read=align, write=writeAlign};
	__property AnsiString Caption = {read=caption, write=writeCaption};
	__property TFont* Font = {read=font};

	__property TBitmap32* NormalGlyph = {read=normalglyph};//, write=writeGlyph};
	__property TBitmap32* HotGlyph = {read=hotglyph};//, write=writeHotGlyph};
	__property TBitmap32* DisabledGlyph = {read=disabledglyph};//, write=writeDisabledGlyph};
	__property TBitmap32* PressedGlyph = {read=pressedglyph};//, write=writePressedGlyph};
	__property AnsiString NormalGlyphName = {read=normalglyphname, write=writeGlyphName};
	__property AnsiString HotGlyphName = {read=hotglyphname, write=writeHotGlyphName};
	__property AnsiString DisabledGlyphName = {read=disabledglyphname, write=writeDisabledGlyphName};
	__property AnsiString PressedGlyphName = {read=pressedglyphname, write=writePressedGlyphName};
	__property TMouseBool OnMinimizing = {read=onminimizing, write=writeOnMinimizing};
};

enum TSliderToolbarAlign {staLeft, staTop, staRight, staBottom, staNone, staCenter};
enum TSliderToolbarButtonsAlign {stbsaCenter, stbsaFirst, stbsaLast};

class TSliderToolbar: public TControlObject
{
private:
	bool enabled, minimized;
	TSliderToolbarAlign align;
	TSliderToolbarButtonsAlign buttonsalign;
	AnsiString caption;
	TFont *font;
	TBitmap32 *background;
	TColor32 color, border;
	int buttonsize;
	bool transparent, showtitle;
	AnsiString title;
	bool rebuilded, fitbuttonsonly;
	Types::TRect MinMaxBtnRect;
protected:
	TGraphicObjectsList* Buttons;
	TGraphicObjectsList* Childs;
	int ButtonOutline, BorderWidth, Size, Columns, Rows, initialbuttonsize;
	TSliderToolButton* MinMaxBtn;
	virtual void __fastcall ApplyChanging();
	virtual void __fastcall MaximizeMinimize(System::TObject* Sender, int X, int Y);
	virtual void __fastcall SmallResize();
	int __fastcall readLeft() {return Rect.left;}
	int __fastcall readTop() {return Rect.top;}
	int __fastcall readWidth() {return Rect.Width();}
	int __fastcall readHeight() {return Rect.Height();}
	void __fastcall writeMinimized(bool value);
	void __fastcall writeEnabled(bool value) {enabled=value; ApplyChanging();}
	void __fastcall writeVisible(bool value) {visible=value; ApplyChanging();}
	void __fastcall writeAlign(TSliderToolbarAlign value) {align=value; ApplyChanging();}
	void __fastcall writeButtonsAlign(TSliderToolbarButtonsAlign value) {buttonsalign=value; ApplyChanging();}
	void __fastcall writeBackground(TBitmap32 *value);
	void __fastcall writeColor(TColor32 value) {color=value; if(Owner!=NULL) Owner->SetObjectChanged(objecttype);}
	void __fastcall writeBorder(TColor32 value) {border=value; if(Owner!=NULL) Owner->SetObjectChanged(objecttype);}
	void __fastcall writeButtonSize(int value) {initialbuttonsize=value; if(smallsize) buttonsize=value>>1; else buttonsize=value; ApplyChanging();}
	void __fastcall writeOutput(Types::TRect value);
	void __fastcall writeFitButtonsOnly(bool value);
	void __fastcall writeTransparent(bool value) {if(transparent!=value) {transparent=value; Update();}}
	void __fastcall writeTitle(AnsiString value) {if(title!=value) {title=value; Update();}}
	void __fastcall writeShowTitle(bool value) {if(showtitle!=value) {showtitle=value; Update();}}
	Types::TRect __fastcall readViewPort(); // overload
	void __fastcall writeInRect(bool value); //overload
	void __fastcall writePressed(bool value);
public:
	__fastcall TSliderToolbar(TMyObject *AOwner, Types::TRect OutputRect,
		int AButtonSize, TSliderToolbarAlign AAlign, bool Maximized);
	__fastcall ~TSliderToolbar();

	int __fastcall AddObject(TMyObject* o); // overload
	virtual void __fastcall RebuildObjects(); //overload
	void __fastcall ApplyGrouping(int Groupindex, int index); //overload
	void __fastcall RemoveChildFromList(TMyObject* o);

	virtual void __fastcall Redraw(TObject *Sender, TBitmap32 *Bmp);
	void __fastcall MouseMove(System::TObject* Sender,
		  Classes::TShiftState Shift, int X, int Y, TCustomLayer *Layer);
	void __fastcall MouseDown(TObject *Sender,
		  TMouseButton Button, TShiftState Shift, int X, int Y,
		  TCustomLayer *Layer);
	void __fastcall MouseUp(TObject *Sender,
		  TMouseButton Button, TShiftState Shift, int X, int Y,
		  TCustomLayer *Layer);
	virtual void __fastcall MouseClick(System::TObject* Sender, int X, int Y);
	void __fastcall MouseDblClick(System::TObject* Sender, int X, int Y);

	void __fastcall Show();
	virtual void __fastcall Hide();
	virtual void __fastcall Resize();
	void __fastcall AddToChilds(TSliderToolbar *tc) {if(tc && Childs->IndexOf(tc)==-1) Childs->AddMyObject(tc);} //!!! Childs->Add cannot be used because of Owner of child is ObjectsContainer, but Childs->Add replaces owner for Childs Owner (TSliderToolbar)
	void __fastcall RemoveFromChilds(TSliderToolbar *tc) {if(tc && Childs->IndexOf(tc)>=0) Childs->Delete(Childs->IndexOf(tc));}

	__property bool Minimized = {read=minimized, write=writeMinimized};
	__property bool Enabled = {read=enabled, write=writeEnabled};
	__property int Left = {read=readLeft};
	__property int Top = {read=readTop};
	__property int Width = {read=readWidth};
	__property int Height = {read=readHeight};
	__property TSliderToolbarAlign Align = {read=align, write=writeAlign};
	__property TSliderToolbarButtonsAlign ButtonsAlign = {read=buttonsalign, write=writeButtonsAlign};
	__property TBitmap32* Background = {read=background, write=writeBackground};
	__property TColor32 Color = {read=color, write=writeColor};
	__property TColor32 Border = {read=border, write=writeBorder};
	__property int ButtonSize = {read=buttonsize, write=writeButtonSize}; //in order button_width = button_height
	__property bool FitButtonsOnly = {read=fitbuttonsonly, write=writeFitButtonsOnly};
	__property bool Transparent = {read=transparent, write=writeTransparent};
	__property AnsiString Title = {read=title, write=writeTitle};
	__property bool ShowTitle = {read=showtitle, write=writeShowTitle};
};

class TSliderToolbarChild: public TSliderToolbar
{
private:
	TSliderToolbar* Parent;
	bool clickfirstonpopup;
protected:
	void __fastcall ApplyChanging() {Resize();}
	void __fastcall Resize();
	void __fastcall MaximizeMinimize(System::TObject* Sender, int X, int Y);
	void __fastcall SmallResize();
	void __fastcall Minimizing(bool minimizing);
	AnsiString __fastcall readNormalGlyphName() {if(MinMaxBtn) return MinMaxBtn->NormalGlyphName; else return "";}
	AnsiString __fastcall readHotGlyphName() {if(MinMaxBtn) return MinMaxBtn->HotGlyphName; else return "";}
	AnsiString __fastcall readDisabledGlyphName() {if(MinMaxBtn) return MinMaxBtn->DisabledGlyphName; else return "";}
	AnsiString __fastcall readPressedGlyphName() {if(MinMaxBtn) return MinMaxBtn->PressedGlyphName; else return "";}
	void __fastcall writeNormalGlyphName(AnsiString value) {if(MinMaxBtn) MinMaxBtn->NormalGlyphName=value;}
	void __fastcall writeHotGlyphName(AnsiString value) {if(MinMaxBtn) MinMaxBtn->HotGlyphName=value;}
	void __fastcall writeDisabledGlyphName(AnsiString value) {if(MinMaxBtn) MinMaxBtn->DisabledGlyphName=value;}
	void __fastcall writePressedGlyphName(AnsiString value) {if(MinMaxBtn) MinMaxBtn->PressedGlyphName=value;}
public:
	__fastcall TSliderToolbarChild(TMyObject *AOwner, TSliderToolbar *AParent,
		Types::TRect OutputRect, int AButtonSize, bool Maximized);
	__fastcall ~TSliderToolbarChild(){}

	void __fastcall RebuildObjects();
	void __fastcall Redraw(TObject *Sender, TBitmap32 *Bmp);
	void __fastcall Hide();
	void __fastcall DismissParent(TSliderToolbar *Caller) {if(Parent==Caller) Parent=NULL;}

	__property AnsiString NormalGlyphName = {read=readNormalGlyphName, write=writeNormalGlyphName};
	__property AnsiString HotGlyphName = {read=readHotGlyphName, write=writeHotGlyphName};
	__property AnsiString DisabledGlyphName = {read=readDisabledGlyphName, write=writeDisabledGlyphName};
	__property AnsiString PressedGlyphName = {read=readPressedGlyphName, write=writePressedGlyphName};
	__property TSliderToolButton* MinMaxButton = {read=MinMaxBtn};
	__property bool ClickFirstButtonOnPopUp = {read=clickfirstonpopup, write=clickfirstonpopup};
};

class TIndicator: public TObject
{
private:
protected:
	int MaxIcons;
	bool visible;
	TSliderToolButton* button;
	System::UnicodeString *IconPaths;
	System::UnicodeString EmptyButton;
	virtual void __fastcall ApplyIconOnButton() {}
	void __fastcall writeVisible(bool value) {if(visible!=value) {visible=value; ApplyIconOnButton();}}
	void __fastcall writeButton(TSliderToolButton* value) {button=value; ApplyIconOnButton();}
public:
	__fastcall TIndicator() {visible=true; IconPaths=NULL; button=NULL; EmptyButton="PNGIMAGE_115";}
	__fastcall ~TIndicator() {button=NULL; if(IconPaths) delete[] IconPaths; button=NULL;}
	void __fastcall Refresh() {ApplyIconOnButton();}

	__property TSliderToolButton* Button = {read=button, write=writeButton};
	__property bool Visible = {read=visible, write=writeVisible};
};

#endif
