//---------------------------------------------------------------------------

#ifndef TwoWheelsUnitH
#define TwoWheelsUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <Graphics.hpp>
#include <pngimage.hpp>
#include <ComCtrls.hpp>
#include <ImgList.hpp>
#include <ImageButton.h>
#include "CheckBoxTreeView.h"
#include "PlugIns.h"
#include <GR32_Image.hpp>
#include "ImageMovie.h"
#include "TwoWheel.h"
//---------------------------------------------------------------------------

enum TReceivedStrFlag {rsfNone = 0x0000, rsfLWheel = 0x0001, rsfRWheel = 0x0002,
	rsfWBase = 0x0004, rsfCenterXY = 0x0008, rsfWheels = 0x0010,
	rsfCalibDistStart = 0x0020, rsfCalibDistStop = 0x0040,
	rsfCalibAngleStart = 0x0080, rsfCalibAngleStop = 0x0100,
	rsfWrongCalibration = 0x2000};

enum TSetParamsWay {spwNone = 0, spwApply = 1, spwOK = 2};

class TTwoWheelsForm : public TForm
{
__published:	// IDE-managed Components
	TPanel *Panel1;
	TPanel *Panel2;
	TPanel *Panel3;
	TPanel *Panel4;
	TPanel *ClientPanel;
	TPanel *TitlePanel;
	TLabel *Label1;
	TPanel *Panel8;
	TPanel *Panel9;
	TImageButton *CloseIButton;
	TPanel *Panel5;
	TPanel *Panel6;
	TPanel *Panel7;
	TPanel *PagesPanel;
	TPanel *OkCancelPanel;
	TImageButton *CancelImageButton;
	TImageButton *OkImageButton;
	TEdit *TempEdit;
	TPanel *MoviePanel;
	TLabel *WaitLabel;
	TImage32 *Image321;
	TPanel *Panel14;
	TPanel *Panel15;
	TPanel *Panel16;
	TPanel *Panel18;
	TNotebook *Notebook1;
	TScrollBox *ScrollBox1;
	TPanel *Panel10;
	TLabel *Label3;
	TImageButton *ReloadButton;
	TLabel *Label14;
	TImageButton *ManualImage;
	TLabel *ManualLabel;
	TLabel *CalibLabel;
	TImageButton *CalibrationImage;
	TImageButton *CalibrateButton;
	TLabel *Label11;
	TPanel *PprPanel;
	TLabel *Label144;
	TLabel *LeftPprLabel;
	TPanel *LeftPanel;
	TLabel *Label145;
	TLabel *LeftWheelLabel;
	TEdit *LeftWheelDEdit;
	TPanel *RightPanel;
	TLabel *Label6;
	TLabel *RightWheelLabel;
	TEdit *RightWheelDEdit;
	TPanel *WheelBasePanel;
	TLabel *Label9;
	TLabel *WheelsBaseLabel;
	TEdit *WheelsBaseEdit;
	TPanel *XPanel;
	TLabel *XLabel;
	TLabel *Label13;
	TEdit *XEdit;
	TPanel *YPanel;
	TLabel *Label12;
	TLabel *YLabel;
	TEdit *YEdit;
	TImageButton *ManualPushButton;
	TLabel *ManualPushLabel;
	TImageButton *ManualPullButton;
	TLabel *ManualPullLabel;
	TImageButton *CloseMovieButton;
	TEdit *LeftPprEdit;
	TPanel *Panel11;
	TLabel *Label4;
	TLabel *RightPprLabel;
	TEdit *RightPprEdit;
	TPanel *Panel12;
	TLabel *Label5;
	TPanel *Panel17;
	TLabel *Label10;
	TLabel *Label7;
	TImage *Image1;
	TPanel *Panel13;
	TLabel *Label16;
	TLabel *Label17;
	TPanel *Panel19;
	TImage *Image2;
	TImageButton *CalibDistStartButton;
	TImageButton *CalibDistApplyButton;
	TPanel *Panel20;
	TLabel *Label8;
	TLabel *Label15;
	TEdit *CalibDistEdit;
	TPanel *Panel21;
	TLabel *Label18;
	TLabel *Label19;
	TPanel *Panel22;
	TLabel *Label20;
	TLabel *Label21;
	TPanel *Panel23;
	TLabel *Label22;
	TLabel *Label23;
	TEdit *CalibAngleEdit;
	TImageButton *CalibAngleApplyButton;
	TImageButton *CalibAngleStartButton;
	TLabel *Label24;
	TImageButton *StopButton;
	TImageButton *ReturnButton;
	TImageButton *ApplyButton;
	TLabel *OdometerLabel;
	TLabel *CalibNeedLabel;
	TLabel *CalibOdoLabel;
	TLabel *FWLabel;
	TLabel *Label25;
	TImageButton *FWUpdateButton;
	TLabel *SerialNumberLabel;
	void __fastcall OkImageClick(TObject *Sender);
	void __fastcall CancelImageClick(TObject *Sender);
	void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
	void __fastcall TitlePanelMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
	void __fastcall TitlePanelMouseMove(TObject *Sender, TShiftState Shift, int X, int Y);
	void __fastcall TitlePanelMouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
	void __fastcall FormShow(TObject *Sender);
	void __fastcall ManualImageClick(TObject *Sender);
	void __fastcall ManualPushButtonClick(TObject *Sender);
	void __fastcall ManualLabelClick(TObject *Sender);
	void __fastcall CalibLabelClick(TObject *Sender);
	void __fastcall ManualPushLabelClick(TObject *Sender);
	void __fastcall ManualPullLabelClick(TObject *Sender);
	void __fastcall RightWheelDEditKeyPress(TObject *Sender, wchar_t &Key);
	void __fastcall PprEditKeyPress(TObject *Sender, wchar_t &Key);
	void __fastcall CloseMovieButtonClick(TObject *Sender);
	void __fastcall ReloadButtonClick(TObject *Sender);
	void __fastcall CalibrateButtonClick(TObject *Sender);
	void __fastcall StopButtonClick(TObject *Sender);
	void __fastcall CalibDistStartButtonClick(TObject *Sender);
	void __fastcall CalibDistApplyButtonClick(TObject *Sender);
	void __fastcall ReturnButtonClick(TObject *Sender);
	void __fastcall CalibAngleStartButtonClick(TObject *Sender);
	void __fastcall CalibAngleApplyButtonClick(TObject *Sender);
	void __fastcall Notebook1PageChanged(TObject *Sender);
	void __fastcall WheelsBaseEditChange(TObject *Sender);
	void __fastcall ApplyButtonClick(TObject *Sender);
	void __fastcall Panel10MouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
		  int X, int Y);
	void __fastcall Panel10MouseMove(TObject *Sender, TShiftState Shift, int X, int Y);
	void __fastcall Panel10MouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift,
		  int X, int Y);

private:	// User declarations
	bool TitleDown;
	int ReceivedStrFlags;
	TObject *Manager;
	Types::TPoint TitleXY;
	TResourceMovie *ResMov;
	TTwoWheelCalibrationType CalibrationType;
	TSetParamsWay SetParamsWay;
	int CalibStartOK;
	bool Panel10MousePressed;
	int Panel10Y;
	void __fastcall ManualComponentsControl();
	void __fastcall StopMovie();
	void __fastcall StartMovie(bool WaitOrStop, TResourceMovieType tp=rmtNone);
	void __fastcall GetParams();
	void __fastcall SetParams();
    void __fastcall ParametersParser(AnsiString Reply);
	void __fastcall StartCalibDist();
	void __fastcall ApplyCalibDist();
	void __fastcall StartCalibAngle();
	void __fastcall ApplyCalibAngle();
	void __fastcall CancelCalibration();
	void __fastcall CalibParser(AnsiString Reply);
	void __fastcall SetParamsParser(AnsiString Reply);
public:		// User declarations
	__fastcall TTwoWheelsForm(TComponent* Owner, TObject *aManager);
	__fastcall ~TTwoWheelsForm();
	int __fastcall ShowOptions(AnsiString aGpsUnitFileName);
};
//---------------------------------------------------------------------------
extern PACKAGE TTwoWheelsForm *TwoWheelsForm;
//---------------------------------------------------------------------------
#endif
