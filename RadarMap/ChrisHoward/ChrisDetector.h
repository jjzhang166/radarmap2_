//---------------------------------------------------------------------------

#ifndef ChrisDetectorH
#define ChrisDetectorH

#include "Defines.h"
#include "Profile.h"
#include "hyperb_chg.h"
#include "HoughHyp.h"
#include "Manager.h"
//---------------------------------------------------------------------------

class TChrisDetectorThread: public TMyThread
{
private:
	TObject *Manager;
	TProfileSatellites* ProfSatellites;
	TProfile *Profile;
	HANDLE StopEvent;
	AnsiString Msg;
	void __fastcall PathNameUpdater();
	void __fastcall UpdatePathName(AnsiString str) {Msg=str; Synchronize(PathNameUpdater);}
	void __fastcall ShowManagerMsg();
	void __fastcall ShowManagerMessage(AnsiString str) {Msg=str; Synchronize(ShowManagerMsg);}
protected:
	void __fastcall Execute();
	bool __fastcall IsStopped();
	int __fastcall ChrisHyperbolas2FoundObjects(AnsiString OutputBinary,
		TFoundObjectList *AFoundObjects);
	void __fastcall ChrisHyperbolas2Labels(TFoundObjectList *AFoundObjects);
public:
	__fastcall TChrisDetectorThread(TProfileSatellites *AProfSatellites, HANDLE aStopEvent,
		TObject* RadarMapManager);
	__fastcall ~TChrisDetectorThread() {}
};

#endif
