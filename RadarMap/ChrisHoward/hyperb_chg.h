#include "ChrisTypes.h"

// Chebychev polynomials to express the horizontal darkness of the hyperbola
// set the nr of horizontal basis elements
#define	iCbyBasSz 20					// max order is 1 less
#define iCbyDomSz 256

// consider this many different models for the hyperbola
#define NrMaxHyperbola 5		// nr of alternative hypothesis to consider used during the hypothesis generation
#define NrMtcGps 5				// same thing, but saved in the final data passed on into the matching process
#define NrHypGroup 6			// eHyLsRHyp ..eHyLsAHyp, nr of elements in a hypothesis group

/*enum
{	eHyStart,						//	start of list for generic processing

	eHyGlobal=eHyStart,				//		when multiple hyperbolas have been matched, this is the global identifier for them
	eHyClsId,						//		after all properties have been evaluated, this is the class assignment id
	eHyLineNr,						// 0	quick access to relevant line numbers is ascii file
	eHyLsFile,						// 1	when processing multiple files, which file was it
	eHyLsObSq,						// 2	object sequence number as discovered
	eHyLsErr,						//   has entered the sorting process
	eHyLsSigm,						// 3
	eHyLsCRms,						// 3
	eHyLsCntX,						// 4
	eHyLsCntY,						// 5
	eHyLsTrace,						//		original SGY X location
	eHyLsSample,					//		original SGY Y location
	eHyLsRHyp,						// 6
	eHyLsIHyp,						// 7
	eHyLsMtXI,						// 8
	eHyLsMtYI,						// 9
	eHyLsSHyp,						//10
	eHyLsAHyp,						//10

	eHyLsRHyp1,						// 1
	eHyLsIHyp1,						// 2
	eHyLsMtXI1,						// 3
	eHyLsMtYI1,						// 4
	eHyLsSHyp1,						// 5
	eHyLsAHyp1,						//10

	eHyLsRHyp2,						// 6
	eHyLsIHyp2,						// 7
	eHyLsMtXI2,						// 8
	eHyLsMtYI2,						// 9
	eHyLsSHyp2,						//20
	eHyLsAHyp2,						//10

	eHyLsRHyp3,						// 1
	eHyLsIHyp3,						// 2
	eHyLsMtXI3,						// 3
	eHyLsMtYI3,						// 4
	eHyLsSHyp3,						// 5
	eHyLsAHyp3,						//10

	eHyLsRHyp4,						// 6
	eHyLsIHyp4,						// 7
	eHyLsMtXI4,						// 8
	eHyLsMtYI4,						// 9
	eHyLsSHyp4,						//30
	eHyLsAHyp4,						//10

	eHyLsMax,						// 1
	eHyLsMaj=eHyLsMax,				// 2
	eHyLsMin,						// 3
	eHySlope,						// 4
	eHyGpsX,						// 5
	eHyGpsY,
	eHyGpsZ,
	eHyGpsN,						// when repeatedly scanning same section, this is the pass number
	eHyGpsM,
	eHyGpsI=eHyGpsM+NrMtcGps,
	eHyPairR=eHyGpsI+NrMtcGps,		// after pairing, best matched
	eHyPairXI,						// after pairing, best matched
	eHyPairYI,
	eHyPairSg,
	eHyPairSa,
	eHyGpsMax,
	eHyMtcInd,						// record the match index to facilitate the Chebychev access
	eHyMtcAcp,						// match accepted, otherwise rejected
	eHyCbyCoef=eHyMtcAcp,
	eHyCbyEnd=eHyCbyCoef+(iCbyBasSz-1)*NrMaxHyperbola
};

typedef struct
{	union
	{
		struct {
			F64	fGlobal, fClsId;
			//fGlobal - found "pipe" uniq ID (different hyperbolas could be desribed by the same fGlobal)
			//fClsId - Clasification ID - RESERVED for the feauture
			F64	fLineNr;
			F64	fFile,fObSq,  fErr,  fSigm,fCRms,fCntX,fCntY,fTrace,fSample;						//		original SGY X,Y location
			// fTrace - trace number, counting from 0
			// fSample - 0..511

			F64 fRHyp,fIHyp,fMtXI,fMtYI,fSHyp,fAHyp;	// fRes1 is used for index of best match
			F64 fRHyp1,fIHyp1,fMtXI1,fMtYI1,fSHyp1,fAHyp1;
			F64 fRHyp2,fIHyp2,fMtXI2,fMtYI2,fSHyp2,fAHyp2;
			F64 fRHyp3,fIHyp3,fMtXI3,fMtYI3,fSHyp3,fAHyp3;
			F64 fRHyp4,fIHyp4,fMtXI4,fMtYI4,fSHyp4,fAHyp4;
			F64 fMaj,fMin,fSlope,fGpsX,fGpsY,fGpsZ,fGpsN,fGpsM[NrMtcGps],fGpsI[NrMtcGps];
			F64	fPairR,fPairXI,fPairYI,fPairSg,fPairSa;
			// note: the Chebychev coefficients are explicitly available
			F64	fMtcInd;
			F64	fMtcAcp;
		}	Nm;
		struct	{
			F64	fV[eHyCbyEnd];
		}	Ar;
	};
}	tHypList; // The single item (Hyperbola or found object)
*/

enum TChrisEnum
{	//t_obj value from conglomeration / subjugation process
	ePathMmX,		// path location mm
	ePathMmZ,		// central element depth in mm
	eMaxEnergy,		// image energy
	eColor,			// color 0 black 1 white
	eMajAxis,		// SVD major axis
	eMinAxis,		//     minor
	eForXBmp,		// X top of main hyperbola forward
	eForYBmp,		// Y
	eDiamMm,		// diameter
	eClusterId,		// cluster id
	eSymmetryId,	// internal usage
	eBckXBmp,		// X top of main hyperbola backward
	eSubjugated,	// -1 is king, otherwise subjugated by this cluster ID
	eCompact,		// compactness, used by subjugation
	eHghYBmp,		// highest cluster member
	eHypX,			// hyperbola X
	eHypY,			// hyperbola Y
	eLowYBmp,		// lowest
	eNrCluster,		// nr elements in clustuer
	eRotFrom90,		// rotation away from normal in pipe path
	// values for Sergi in trace-output
	eTrcForward,	// trace  nr forward	X coordinate in SGY file
	eTrcBckward,	//           backward
	eSmpForward,	// sample nr forward	Y coordinate in SGY
	eSmpBckward,	//           backard
	eMatchedHypMax
};

typedef struct
{
	union
	{
		struct
		{
			F64	fV[eMatchedHypMax];
		}Ar;
		struct
		{
			F64
				PathMmX,		// path location mm
				PathMmZ,		// central element depth in mm
				MaxEnergy,		// image energy
				Color,			// color 0 black 1 white
				MajAxis,		// SVD major axis
				MinAxis,		//     minor
				ForXBmp,		// X top of main hyperbola forward
				ForYBmp,		// Y
				DiamMm,			// diameter
				ClusterId,		// cluster id
				SymmetryId,		// forward backward pair ID from the tHypList processing
				BckXBmp,		// X top of main hyperbola backward
				Subjugated,		// -1 is king, otherwise subjugated by this cluster ID
				Compact,		// compactness, used by subjugation
				HghYBmp,		// highest cluster member
				HypX,			// hyperbola X
				HypY,			// hyperbola Y
				LowYBmp,		// lowest
				NrCluster,		// nr elements in clustuer
				RotFrom90,		// rotation away from normal in pipe path
								// values for Sergi in trace-output
								// values for Sergi in trace-output
				TrcForward,		// trace  nr forward	X coordinate in SGY file
				TrcBckward,		//           backward
				SmpForward,		// sample nr forward	Y coordinate in SGY
				SmpBckward;		//           backard
		}Nm;
	};
}	tHypList;//tHypMatchRsl;
