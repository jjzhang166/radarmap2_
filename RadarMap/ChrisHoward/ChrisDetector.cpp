//---------------------------------------------------------------------------

#pragma hdrstop

#include "ChrisDetector.h"
#include "Manager.h"
#include "Out_segy.h"
#include <IniFiles.hpp>

//---------------------------------------------------------------------------

#pragma package(smart_init)

__fastcall TChrisDetectorThread::TChrisDetectorThread(TProfileSatellites *AProfSatellites,
	HANDLE aStopEvent, TObject* aManager) : TMyThread(true)
{
	//Save the profile to the Temp SEGY
	//Initialize a Master Ini for Chris
	//Start Chris.exe via the ShellExecute
	//Wait until a Slave Ini appears from Chris
	//Check the Slave Ini and an internal binary output file (its size on disk)
	//Read the output binary and convert to TFoundObjects (by TPipesDector)
	//Integrate TFoundObjects as TProfileLabel by TProfWinProcess::Objects2Labels
	ProfSatellites=AProfSatellites;
	if(AProfSatellites)	Profile=AProfSatellites->Profiles[0];
	StopEvent=aStopEvent;
	Manager=aManager;
	FreeOnTerminate=true;
	AskForResume();
}
//---------------------------------------------------------------------------

#define _ChrisDetectorKeepAliving
//#undef _ChrisDetectorKeepAliving

void __fastcall TChrisDetectorThread::Execute()
{
	TRadarMapManager *rmm=(TRadarMapManager*)Manager;
	TIniFile *MasterIni, *SlaveIni;
	TFoundObjectList *FoundObjects;
	TShellItem *sh;

	SetStarted();
	if(rmm && rmm->Settings && Profile && Profile->Traces>0)
	{
		AnsiString ProfFN, Path, OutputBinary, OutputHuman, OutputError;
		AnsiString ChrisExePath, ChrisExe=rmm->Settings->ChrisExe;
		AnsiString MasterIniFN=rmm->Settings->ChrisMasterIni;
		AnsiString SlaveIniFN=rmm->Settings->ChrisSlaveIni;
		int ChrisExeTimeout=rmm->Settings->ChrisExeTimeout;
#ifdef _ChrisDetectorKeepAliving
		bool ChrisDetectorKeepAliving=rmm->Settings->ChrisExeKeepAliving;
		AnsiString KeepAliveStr, KeepAliveStr_old;
#endif
		ULONGLONG TickCount;

		int progress, progress_old, found;
		float TotalTime;

		try
		{
			if(FileExists(ChrisExe) && FileExists(MasterIniFN))
			{
				ChrisExePath=ExtractFilePath(ChrisExe);
				try
				{
					MasterIni=new TIniFile(MasterIniFN);
					ProfFN=MasterIni->ReadString("Files", "Input", "Radar_00001.sgy");
					Path=ExtractFilePath(ProfFN);
					if(Path=="" || Path=="\\" || !DirectoryExists(Path))
					{
						Path=ChrisExePath+"Data\\";
						if(!DirectoryExists(Path)) CreateDirectory(Path.c_str(), NULL);
						if(ExtractFileName(ProfFN)=="") ProfFN="Radar_00001.sgy";
						ProfFN=Path+ExtractFileName(ProfFN);
						MasterIni->WriteString("Files", "Input", ProfFN);
					}
					OutputHuman=MasterIni->ReadString("Files", "OutputHuman", "Eval_00001.txt");
					Path=ExtractFilePath(OutputHuman);
					if(Path=="" || Path=="\\" || !DirectoryExists(Path))
					{
						Path=ChrisExePath+"Data\\";
						if(!DirectoryExists(Path)) CreateDirectory(Path.c_str(), NULL);
						if(ExtractFileName(OutputHuman)=="") OutputHuman="Eval_00001.txt";
						OutputHuman=Path+ExtractFileName(OutputHuman);
						MasterIni->WriteString("Files", "OutputHuman", OutputHuman);
					}
					OutputBinary=MasterIni->ReadString("Files", "OutputBinary", "Eval_00001.hyb");
					Path=ExtractFilePath(OutputBinary);
					if(Path=="" || Path=="\\" || !DirectoryExists(Path))
					{
						Path=ChrisExePath+"Data\\";
						if(!DirectoryExists(Path)) CreateDirectory(Path.c_str(), NULL);
						if(ExtractFileName(OutputBinary)=="") OutputBinary="Eval_00001.hyb";
						OutputBinary=Path+ExtractFileName(OutputBinary);
						MasterIni->WriteString("Files", "OutputBinary", OutputBinary);
					}
					if(FileExists(ProfFN) && !DeleteFile(ProfFN))
					{
						ShowManagerMessage("Cannot access the Chris's files. The files are absent or last session is in progress still!");
						return;
					}
					if(FileExists(SlaveIniFN) && !DeleteFile(SlaveIniFN))
					{
						ShowManagerMessage("Cannot access the Chris's files. The files are absent or last session is in progress still!");
						return;
					}
					if(FileExists(OutputHuman) && !DeleteFile(OutputHuman))
					{
						ShowManagerMessage("Cannot access the Chris's files. The files are absent or last session is in progress still!");
						return;
					}
					if(FileExists(OutputBinary) && !DeleteFile(OutputBinary))
					{
						ShowManagerMessage("Cannot access the Chris's files. The files are absent or last session is in progress still!");
						return;
					}
					if(Output_SEGY(ProfFN.c_str(), Profile)!=0)
					{
						ShowManagerMessage("Cannot access the Chris's files. The files are absent or last session is in progress still!");
						return;
					}
				}
				__finally
				{
					delete MasterIni;
					MasterIni=NULL;
				}
				Sleep(100);
				SlaveIni=NULL;
				progress=progress_old=0;
				OutputError="";
				TotalTime=0;
#ifdef _ChrisDetectorKeepAliving
				TickCount=0;
				KeepAliveStr=KeepAliveStr_old;
#endif
				try
				{
					try
					{
						UpdatePathName("Detecting 0%");
						sh=new TShellItem(NULL);
						sh->CmdLine=ChrisExe;
						if(!rmm->Settings->ChrisShowExeOutput)
							sh->ProcessCreationFlags=CREATE_NO_WINDOW;
						sh->Run();
						do
						{
							if(FileExists(SlaveIniFN))
							{
								if(!SlaveIni) SlaveIni=new TIniFile(SlaveIniFN);
								else
								{
									progress=SlaveIni->ReadInteger("Activity", "ProgressPercent", 0);
									OutputError=SlaveIni->ReadString("Activity", "ErrorText", "");
#ifdef _ChrisDetectorKeepAliving
									//KeepAliveStr=SlaveIni->ReadString("Activity", "AliveTime", "");
									if(ChrisDetectorKeepAliving)
										KeepAliveStr=SlaveIni->ReadString("Activity", "TotalTime", "");
#endif
								}
							}
							else progress=0;
							if(progress_old!=progress)
								UpdatePathName("Detecting "+IntToStr(progress)+"%");
							progress_old=progress;
#ifdef _ChrisDetectorKeepAliving
							if(ChrisDetectorKeepAliving)
							{
								if(KeepAliveStr==KeepAliveStr_old)
								{
									if(TickCount==0) TickCount=::GetTickCount();//GetTickCount64();
									else
									{
										//if(GetTickCount64()-TickCount>5000)
										if(::GetTickCount()-TickCount>ChrisExeTimeout)
										{
											progress=-1;
											OutputError="Chris's files Timeout!";
										}
									}
								}
								else TickCount=0;
								KeepAliveStr_old=KeepAliveStr;
							}
#endif
							Sleep(100);
						} while(progress>=0 && progress<100 && !IsStopped());
					}
					catch(Exception &e) {progress=-1; OutputError="RadarMap Exception: "+(AnsiString)e.Message;}
				}
				__finally
				{
					if(SlaveIni)
					{
						TotalTime=SlaveIni->ReadFloat("Activity", "TotalTime", 0);
						delete SlaveIni;
						SlaveIni=NULL;
					}
					if(sh) delete sh;
					UpdatePathName("");
				}
				Sleep(100);

				if(!IsStopped())
				{
					if(progress>=100 && OutputError.Trim()=="")
					{
						if(FileExists(OutputBinary))
						{
							FoundObjects=new TFoundObjectList();
							try
							{
								found=ChrisHyperbolas2FoundObjects(OutputBinary, FoundObjects);
								if(found<=0)
									ShowManagerMessage("Cannot open ChrisDetector output binary file or it is empty");
								else
								{
									ChrisHyperbolas2Labels(FoundObjects);
									ShowManagerMessage("ChrisDetector found "+IntToStr(found)+
										" objects with elapsed time "+FloatToStrF(TotalTime, ffFixed, 8, 1)+" Sec.");
								}
							}
							__finally
							{
								delete FoundObjects;
							}
						}
						else ShowManagerMessage("ChrisDetector returned the Error:\r\n"+OutputError);
					}
					else
					{
						ShowManagerMessage("ChrisDetector returned the Error:\r\n"+OutputError);
					}
				}
			}
			else ShowManagerMessage("Cannot find Chris's module!");
		}
		catch(...) {}
	}
	SetIsTerminated();
	ExitThread(99);
}
//---------------------------------------------------------------------------

void __fastcall TChrisDetectorThread::ShowManagerMsg()
{
	if((TRadarMapManager*)Manager) ((TRadarMapManager*)Manager)->ShowMessageObject(NULL,
		((TRadarMapManager*)Manager)->MapMessages, TMessageType::mtWarning,
		TMessageVisibalityType::mvtDisapering, 10000, Msg, (AnsiString)_RadarMapName);
}
//---------------------------------------------------------------------------

void __fastcall TChrisDetectorThread::PathNameUpdater()
{
	if(ProfSatellites) ProfSatellites->Path->AdditionalName=Msg;
}
//---------------------------------------------------------------------------

bool __fastcall TChrisDetectorThread::IsStopped()
{
	bool res=(StopEvent && WaitForSingleObject(StopEvent, 0)==WAIT_OBJECT_0);

	if(res) AskForTerminate();

	return res;
}
//---------------------------------------------------------------------------

int __fastcall TChrisDetectorThread::ChrisHyperbolas2FoundObjects(AnsiString OutputBinary,
	TFoundObjectList *FoundObjects)
{
	int res=-1;
	HANDLE hndl;
	int Sz=sizeof(tHypList);
	unsigned long ul;
	tHypList *HypItem=NULL;
	TFoundObject *fo;
	TRadarMapManager *RadarMapManager=(TRadarMapManager*)Manager;

	if(RadarMapManager && FileExists(OutputBinary) && FoundObjects && Profile)
	{
		try
		{
			try
			{
				UpdatePathName("Processing...");
				HypItem=new tHypList;
				FoundObjects->Clear();
				hndl=CreateFile(OutputBinary.c_str(), GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
				if(hndl!=INVALID_HANDLE_VALUE)
				{
					try
					{
						try
						{
							do
							{
								ReadFile(hndl, (char*)HypItem, Sz, &ul, NULL);
								/*if(ul==(unsigned long)Sz && HypItem->Nm.fGlobal>=0 &&
									HypItem->Nm.fTrace>=0 && HypItem->Nm.fTrace<Profile->Traces &&
									HypItem->Nm.fSample>=0 && HypItem->Nm.fSample<Profile->Samples)
								{
									HypItem->Nm.fSample-=Profile->ZeroPoint;
									fo=new TFoundObject(HypItem->Nm.fTrace, HypItem->Nm.fSample+Profile->ZeroPoint, HypItem->Nm.fSlope, 1., HypItem->Nm.fSigm);
									FoundObjects->Add(fo);
								}*/
								if(ul==(unsigned long)Sz && HypItem->Nm.Subjugated<0 &&
									HypItem->Nm.TrcForward>=0 && HypItem->Nm.TrcForward<Profile->Traces &&
									HypItem->Nm.TrcBckward>=0 && HypItem->Nm.TrcBckward<Profile->Traces &&
									HypItem->Nm.SmpForward>=0 && HypItem->Nm.SmpForward<Profile->Samples &&
									HypItem->Nm.SmpBckward>=0 && HypItem->Nm.SmpBckward<Profile->Samples &&
									HypItem->Nm.MinAxis!=0)
								{
									//HypItem->Nm.fSample-=Profile->ZeroPoint;
									fo=new TFoundObject(HypItem->Nm.TrcForward, HypItem->Nm.SmpForward, HypItem->Nm.ClusterId, 1., HypItem->Nm.MaxEnergy);//HypItem->Nm.MajAxis/(-HypItem->Nm.MinAxis), 1., HypItem->Nm.MaxEnergy);
									fo->Positive=(HypItem->Nm.Color>=0);
									FoundObjects->Add(fo);
									fo=new TFoundObject(HypItem->Nm.TrcBckward, HypItem->Nm.SmpBckward, HypItem->Nm.ClusterId, 1., HypItem->Nm.MaxEnergy);//HypItem->Nm.MajAxis/(-HypItem->Nm.MinAxis), 1., HypItem->Nm.MaxEnergy);
									FoundObjects->Add(fo);
									fo->Positive=(HypItem->Nm.Color>=0);
								}
								res++;
							} while(ul==(unsigned long)Sz);
						}
						catch(...) {}
					}
					__finally
					{
						CloseHandle(hndl);
					}
				}
			}
			catch(...) {}
		}
		__finally
		{
			if(HypItem) delete HypItem;
			UpdatePathName("");
		}
	}

	return res;
}

void __fastcall TChrisDetectorThread::ChrisHyperbolas2Labels(TFoundObjectList *AFoundObjects)
{
	TFoundObject *fo;
	TTrace *ptr;
	TProfileLabel *pl;
	TMapLabel *ml;
	TRadarMapManager *RadarMapManager;
	TRadarMapSettings *Settings;
	float lb;

	RadarMapManager=(TRadarMapManager*)Manager;
	if(RadarMapManager && RadarMapManager->Settings && RadarMapManager->ObjectsContainer && ProfSatellites &&
		Profile && AFoundObjects && AFoundObjects->Count>0)
	{
		try
		{
			try
			{
				UpdatePathName("Rendering...");
				for(int i=0; i<AFoundObjects->Count; i++)
				{
					fo=AFoundObjects->Items[i];
					if(fo && fo->X>=0 && fo->X<Profile->Traces &&
						fo->Y>Profile->ZeroPoint && fo->Y<Profile->Samples)
					{
						ptr=Profile->OutputView->GetTraceByIndex(fo->X);

						if(ptr)
						{
							pl=new TProfileLabel(RadarMapManager->ObjectsContainer, RadarMapManager->ObjectsContainer->ViewPort,
								ProfSatellites->OutputView, (fo->Positive ? pltAutoDetectPos : pltAutoDetectNeg),
								fo->X, fo->Y, (fo->Positive ? clRed : clBlue), RadarMapManager);
							if(pl)
							{
								if(fo->ECoeff>0) lb=fo->ECoeff;
								else lb=1.;
								pl->Description="ID = "+IntToStr((int)(fo->E*lb))+"\n";
								pl->Description+="MaxEnergy = "+FloatToStrF(fo->Acc, ffFixed, 10, 2)+"\n";
								//if(fo->Weight>0) pl->Description+="Probability = "+FloatToStrF(fo->Weight*100., ffFixed, 10, 2)+" %"+"\n";
								//pl->Description+="HypValue = "+FloatToStrF(fo->HypValue, ffFixed, 10, 2)+"\n";

								pl->Data=fo;
								AFoundObjects->Items[i]=NULL;
								if(pl->Coordinate && pl->Coordinate->Valid &&
									RadarMapManager->MapObjects && RadarMapManager->MapObjects->Container)
								{
									//pl->GPSConfidence
									ml=new TMapLabel(RadarMapManager->MapObjects, RadarMapManager->MapObjects->ViewPort,
										pl, RadarMapManager->MapObjects->Container->LatLonRect, (TObject*)ProfSatellites->Path,
										RadarMapManager);
									ml->Description=pl->Description;
								}
							}
						}
					}
					if(IsStopped()) break;
				}
			}
			catch(...) {}
		}
		__finally
		{
			UpdatePathName("");
		}
	}
}

