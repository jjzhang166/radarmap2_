//---------------------------------------------------------------------------

#ifndef IcmpH
#define IcmpH
//---------------------------------------------------------------------------
//#include <windows.h>
//#include <winsock.h>
//#include <stdio.h>
//#include <string.h>
//---------------------------------------------------------------------------
#include <vcl.h>
#include <winsock2.h>
#include <iphlpapi.h>
#include <icmpapi.h>
#include <stdio.h>

#pragma comment(lib, "iphlpapi.lib")
#pragma comment(lib, "ws2_32.lib")

namespace Icmp
{
	class TEndlessPingThread;

	class TPing
	{
	private:
		AnsiString errorstring, old_addr;
		bool error, throwexceptions;
		WSADATA wsaData;                        // WSADATA
		HANDLE newlineevent;                        // LoadLibrary() handle to ICMP.DLL
		HANDLE hndlFile;                        // Handle for IcmpCreateFile()
		struct in_addr iaDest, ReplyAddr;      	// Internet address structure
		bool ValidIP;
		AnsiString ValidAddr, ValidHost;
		DWORD *dwAddress;                       // IP Address
		IP_OPTION_INFORMATION ipInfo;
		DWORD dwRet;                            // DWORD return code
		int bufsize, timeout, lasttime;
		AnsiString laststr;
		ULONG laststatus;
		TStrings *outputlines;
		TEndlessPingThread *EndlessThrd;
		TThreadMethod onrequestupdate;
		AnsiString __fastcall DecodeIcmpResult(long Result, PICMP_ECHO_REPLY pEchoReply);
		void __fastcall VisualizeChanges() {try {if(outputlines) outputlines->Add(laststr);} catch(Exception &e){ if(throwexceptions) throw Exception(e);}}
		void __fastcall writeErrStr(AnsiString value) {if(value!="" && value.Length()>0) {errorstring=value; error=true;}}
		void __fastcall writeLastStr(AnsiString value);
	protected:
		unsigned char __fastcall readTTL() {return ipInfo.Ttl;}
		unsigned char __fastcall readTOS() {return ipInfo.Tos;}
		bool __fastcall readFragmentFlag() {return ipInfo.Flags & IP_FLAG_REVERSE;}
		bool __fastcall readReverseFlag() {return ipInfo.Flags & IP_FLAG_DF;}
		void __fastcall writeBufSize(int value) {if(value<18) bufsize=18; else if(value>65500) bufsize=65500; else bufsize=value;}
		void __fastcall writeTTL(unsigned char value) {ipInfo.Ttl=value;}
		void __fastcall writeTOS(unsigned char value) {ipInfo.Tos=value;}
		void __fastcall writeReverseFlag(bool value) {if(value) ipInfo.Flags|=IP_FLAG_REVERSE; else ipInfo.Flags&=(0xFF-IP_FLAG_REVERSE);}
		void __fastcall writeFragmentFlag(bool value) {if(value) ipInfo.Flags|=IP_FLAG_DF; else ipInfo.Flags&=(0xFF-IP_FLAG_DF);}
		void __fastcall writeTimeout(int value) {if(value>=0) timeout=value;}
		void __fastcall writeOnRequestUpdate(TThreadMethod value) {onrequestupdate=value;}
		__property AnsiString ErrStr = {read=errorstring, write=writeErrStr};
		__property AnsiString LastStr = {read=laststr, write=writeLastStr};
		__property bool Err = {read=error};
	public:
		__fastcall TPing(bool AThrowExceptions=true);
		__fastcall ~TPing();

		int __fastcall Ping(AnsiString Addr, bool Endless, bool ShowTitle=true);
		void __fastcall StopEndlessPing();
		AnsiString __fastcall GetPingCommandLine(AnsiString Addr, bool Endless);

		__property int BufSize = {read=bufsize, write=writeBufSize};
		__property unsigned char TTL = {read=readTTL, write=writeTTL};
		__property unsigned char TOS = {read=readTOS, write=writeTOS};
		__property bool FragmentFlag = {read=readFragmentFlag, write=writeFragmentFlag};
		__property bool ReverseFlag = {read=readReverseFlag, write=writeReverseFlag};
		__property int Timeout = {read=timeout, write=writeTimeout};

		__property int LastRoundTripTime = {read=lasttime};
		__property AnsiString LastRequest = {read=laststr};
		__property ULONG LastStatus = {read=laststatus};
		__property bool ThrowExceptions = {read=throwexceptions, write=throwexceptions};
		__property TStrings *OutputLines = {read=outputlines, write=outputlines};
		__property HANDLE RequestUpdateEvent = {read=newlineevent};
		__property TThreadMethod OnRequestUpdate = {write=writeOnRequestUpdate};
	};

	class TEndlessPingThread : public TThread
	{
	private:
		TPing *Owner;
		AnsiString Addr;
		int timeout;
		bool ShowTitle;
		void __fastcall DoPing() {if(Owner) {timeout=Owner->Timeout; Owner->Ping(Addr, false, ShowTitle); ShowTitle=false;}}
	protected:
		void __fastcall Execute() {while(!Terminated){ DoPing(); Sleep(1000);}}
	public:
		__fastcall TEndlessPingThread(TPing *AOwner, AnsiString AAddr) : TThread(true) {Owner=AOwner; Addr=AAddr; timeout=0; ShowTitle=true; Resume();}
		__fastcall ~TEndlessPingThread() {}
		void __fastcall DisconnectFromOwner() {Owner=NULL;}

		void __fastcall MySynchronize(TThreadMethod AMethod) {Synchronize(AMethod);}

		__property int Timeout = {read=timeout};
		__property bool MyTerminated = {read=Terminated};
	};

	class TSingleMethodThread : public TThread
	{
	private:
		TThreadMethod Target;
		bool Synchro;
	protected:
		void __fastcall Execute() {if(Target) if (Synchro) Synchronize(Target); else (Target)();}
	public:
		__fastcall TSingleMethodThread(TThreadMethod ATarget, bool ASynchro=false) : TThread(true) {Target=ATarget; Synchro=ASynchro; FreeOnTerminate=true; Resume();}
	};

};

#endif
