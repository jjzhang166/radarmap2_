#ifndef MyQueuesH
#define MyQueuesH

#include <vcl.h>
#include <math.h>

//---------------------------------------------------------------------------
// TBusyLocker
//---------------------------------------------------------------------------
class TBusyLocker
{
private:
	HANDLE BusyEvent;
	bool endlesslock;
protected:
	void __fastcall writeEndlessLock(bool value) {endlesslock=value;}
public:
	__fastcall TBusyLocker() {BusyEvent=CreateEvent(NULL, true, false, NULL); endlesslock=true;}
	__fastcall ~TBusyLocker() {HANDLE e; UnLock(); e=BusyEvent; BusyEvent=NULL; CloseHandle(e);}

	bool __fastcall IsLocked() {try { return (BusyEvent && WaitForSingleObject(BusyEvent, 0)==WAIT_OBJECT_0);} catch(...) {return false;}}
	bool __fastcall Lock() {bool b; if(BusyEvent && WaitForSingleObject(BusyEvent, 0)!=WAIT_OBJECT_0) {SetEvent(BusyEvent); b=true;} else b=false; return b;}
	bool __fastcall SequentLock() {bool r; do {r=Lock();} while(BusyEvent && !r && endlesslock); return r;}
	void __fastcall UnLock() {if(BusyEvent) ResetEvent(BusyEvent);}

	__property bool EndlessLock = {read=endlesslock, write=writeEndlessLock};
};

//---------------------------------------------------------------------------
// Queues
//---------------------------------------------------------------------------
class TEventedQueue : public TQueue
{
private:
protected:
	HANDLE notemptyevent, emptyevent;
	bool freeondelete, locking;
	TBusyLocker *BusyLock;
	virtual int __fastcall readCount() {int i; if(locking) BusyLock->SequentLock(); i=TQueue::Count(); BusyLock->UnLock(); return i;}
	void __fastcall ReEvent() {if(TQueue::Count()==0) {ResetEvent(notemptyevent); SetEvent(emptyevent);} else {SetEvent(notemptyevent); ResetEvent(emptyevent);}}
public:
	__fastcall TEventedQueue() : TQueue() {notemptyevent=CreateEvent(NULL, true, false, NULL); emptyevent=CreateEvent(NULL, true, true, NULL); freeondelete=false; locking=false; BusyLock=new TBusyLocker();}
	__fastcall ~TEventedQueue() {locking=false; CloseHandle(notemptyevent); CloseHandle(emptyevent); Clear(); BusyLock->UnLock(); delete BusyLock; BusyLock=NULL;}

	virtual void __fastcall Clear() {void* d; if(locking) BusyLock->SequentLock(); while(TQueue::Count()>0) {d=TQueue::Pop(); if(d && freeondelete) delete d;} ReEvent(); BusyLock->UnLock();}
	void __fastcall Push(void* AItem) {void* n; if(locking) BusyLock->SequentLock(); n=TQueue::Push(AItem); ReEvent(); BusyLock->UnLock();}
	void* __fastcall Pop(void) {void* d=NULL; if(locking) BusyLock->SequentLock(); if(TQueue::Count()>0){ d=TQueue::Pop(); ReEvent();} BusyLock->UnLock(); return d;}
	void* __fastcall Peek(void) {void* d; if(locking) BusyLock->SequentLock(); d=TQueue::Peek(); BusyLock->UnLock(); return d;}
	//Removes the first coinsiding item in the Queue, but it doesn't free the memory occupied by the AItem
	bool __fastcall Remove(void* AItem) {bool r=false; int i=0; if(locking) BusyLock->SequentLock(); if(AItem) while(i<TQueue::Count()) {if(TQueue::List->Items[i]==AItem) {TQueue::List->Delete(i); ReEvent(); r=true; break;} i++;} BusyLock->UnLock(); return r;}
	void __fastcall Delete(void* AItem) {if(locking) BusyLock->SequentLock(); if(Remove(AItem) && freeondelete) delete AItem; BusyLock->UnLock();}

	__property int Count = {read=readCount};
	__property HANDLE NotEmptyEvent = {read=notemptyevent};
	__property HANDLE EmptyEvent = {read=emptyevent};
	__property bool FreeOnDelete = {read=freeondelete, write=freeondelete};
	__property bool Locking = {read=locking, write=locking};
};

class TObjectEventedQueue: public TEventedQueue
{
public:
	__fastcall TObjectEventedQueue() : TEventedQueue() {}
	__fastcall ~TObjectEventedQueue() {}

	virtual void __fastcall Clear() {TObject* d; if(locking) BusyLock->SequentLock(); while(TQueue::Count()>0) {d=(TObject*)TQueue::Pop(); if(d && freeondelete) delete d;} ReEvent(); BusyLock->UnLock();}
	virtual void __fastcall Push(TObject* AItem) {TEventedQueue::Push((void*)AItem);}
	TObject* __fastcall Pop(void) {return (TObject*)TEventedQueue::Pop();}
	TObject* __fastcall Peek(void) {return (TObject*)TEventedQueue::Peek();}
	//Removes the first coinsiding item in the Queue, but it doesn't free the memory occupied by the AItem
	bool __fastcall Remove(TObject* AItem) {return TEventedQueue::Remove((void*)AItem);}
	void __fastcall Delete(TObject* AItem) {if(locking) BusyLock->SequentLock(); if(Remove(AItem) && freeondelete) delete AItem; BusyLock->UnLock();}
};

class TFixedSizeQueue : public TObjectEventedQueue
{
private:
	int maxitems;
protected:
	void __fastcall CheckMax() {TObject *d; while(TObjectEventedQueue::Count>maxitems) {d=Pop(); if(d) delete d;}}
	void __fastcall writeMaxItems(int value) {maxitems=value;} //{if(maxitems!=value) {maxitems=value;}}// CheckMax();}}
public:
	__fastcall TFixedSizeQueue(unsigned int AMaxItems) : TObjectEventedQueue() {freeondelete=true; maxitems=AMaxItems; if (maxitems<1) maxitems=1;}
	__fastcall ~TFixedSizeQueue() {}

	void __fastcall Push(TObject* AItem) {TObjectEventedQueue::Push(AItem); CheckMax();}

	__property int MaxItems = {read=maxitems, write=writeMaxItems};
};

class TAnsiStringQueue : public TEventedQueue
{
public:
	__fastcall TAnsiStringQueue() : TEventedQueue() {}
	__fastcall ~TAnsiStringQueue() {}

	void __fastcall Push(AnsiString* AItem) {TEventedQueue::Push((void*)AItem);}
	AnsiString* __fastcall Pop(void) {return (AnsiString*)TEventedQueue::Pop();}
	AnsiString* __fastcall Peek(void) {return (AnsiString*)TEventedQueue::Peek();}
	void Remove(AnsiString* AItem) {TEventedQueue::Remove((void*)AItem);}
	void Delete(AnsiString* AItem) {TEventedQueue::Delete((void*)AItem);}
};

#endif