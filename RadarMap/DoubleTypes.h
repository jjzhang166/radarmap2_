#ifndef DoubleTypes_h
#define DoubleTypes_h

#include <math.h>
#include <Types.hpp>
#include <SysUtils.hpp>

struct TDoublePoint
{
public:
	double X;
	double Y;
	char Temp[4];

	bool operator == (const TDoublePoint& pt) const
	{
		return (X == pt.X) && (Y == pt.Y);
	}
	bool operator != (const TDoublePoint& pt) const
	{
		return !(pt == *this);
	}
	Types::TPoint AsTPoint() {return Types::TPoint((int)X, (int)Y);}
	double Length() {double d; try {d=X*X+Y*Y; if(d>0) d=sqrt(d);} catch(...) {d=0;} return d;}
};

TDoublePoint DoublePoint(double X, double Y, char *Temp=NULL) {TDoublePoint dp; dp.X=X; dp.Y=Y; if(Temp) *((int*)dp.Temp)=*((int*)Temp); else *((int*)dp.Temp)=0; return dp;}

TDoublePoint DoublePoint() {return DoublePoint(0, 0);}

TDoublePoint DoublePoint(Types::TPoint p) {return DoublePoint((double)p.x, (double)p.y);}

TDoublePoint DoublePoint(AnsiString value)
{
	int i;
	TDoublePoint dp;
	AnsiString str;

	if(value.Trim()!="")
	{
		str="";
		i=1;
		try
		{
			while(i<=value.Length() && value[i]!=' ')
				str+=value[i++];
			dp.X=StrToFloat(str);
			i++;
			str="";
			while(i<=value.Length() && value[i]!=' ')
				str+=value[i++];
			dp.Y=StrToFloat(str);
		}
		catch(Exception &e)
		{
			dp.X=-9999;
			dp.Y=-9999;
		}
	}
	else
	{
		dp.X=-9999;
		dp.Y=-9999;
	}
	*((int*)dp.Temp)=0;
	return dp;
};

AnsiString DoublePointToStr(TDoublePoint value) {return (FloatToStrF(value.X, ffFixed, 12, 4)+" "+FloatToStrF(value.Y, ffFixed, 12, 4));}

struct TDouble3DPoint
{
public:
	union
	{
        TDoublePoint XY;
		struct
		{
			double X;
			double Y;
			char Temp[4];
		};
	};
	double Z;
	TDouble3DPoint() {X=0.; Y=0.; Z=0.; *((int*)Temp)=0;}
	TDouble3DPoint(double a, double b, double c) {X=a; Y=b; Z=c; *((int*)Temp)=0;}
	TDouble3DPoint(double a, double b, double c, char ch1, char ch2, char ch3, char ch4) {X=a; Y=b; Z=c; Temp[0]=ch1; Temp[1]=ch2; Temp[2]=ch3; Temp[3]=ch4;}
	TDouble3DPoint(AnsiString pos)
	{
		int j=0, i;
		double x, y, z, d;
		bool digit=false, err=false;
		AnsiString nr="";
		char ch;

		if(pos.Trim()!="")
		{
			x=y=z=0;
			i=1;
			try
			{
				while(i<=pos.Length())
				{
					if(pos[i]>='0' && pos[i]<='9') nr+=pos[i];
					else if(pos[i]=='.' || pos[i]==',') nr+='.';//DecimalSeparator;
					else if(pos[i]=='-') nr+="-";
					else digit=true;
					if(digit || i==pos.Length())
					{
						digit=false;
						try
						{
							d=StrToFloat(nr);
						}
						catch(Exception &e) {j=-1;}
						nr="";
						j++;
						if(j==1) x=d;
						else if(j==2) y=d;
						else if(j==3) z=d;
					}
					i++;
				}
				if(j>1)
				{
					X=x;
					Y=y;
					Z=z;
				}
				else err=true;
			}
			catch(Exception &e)
			{
				err=true;
			}
		}
		if(err)
		{
			X=-9999;
			Y=-9999;
			Z=-9999;
		}
		*((int*)Temp)=0;
	}
	double Length()
	{
		double d;

		d=X*X+Y*Y+Z*Z;
		if(d!=0) d=sqrt(d);
		else d=0;
		return d;
	}
	TDouble3DPoint operator+(TDouble3DPoint A)
	{
		TDouble3DPoint Result;
		Result.X=X+A.X;
		Result.Y=Y+A.Y;
		Result.Z=Z+A.Z;
		return Result;
	}
	TDouble3DPoint operator-(TDouble3DPoint A)
	{
		TDouble3DPoint Result;
		Result.X=X-A.X;
		Result.Y=Y-A.Y;
		Result.Z=Z-A.Z;
		return Result;
	}
	double Radius(TDouble3DPoint A)
	{
		TDouble3DPoint d=TDouble3DPoint(X, Y, Z);
		d=A-d;
		return d.Length();
	}
	double Angle(TDouble3DPoint A) //between adjacent cathetus and hypotenuse in radians, for 2D(XY) only!!!!!!!!!!
	{
		TDouble3DPoint d=A-TDouble3DPoint(X, Y, Z);
		double f, w=d.X, h=d.Y;

		if(h==0)
		{
			if(w>=0) return 0.;
			else return M_PI;
		}
		else if(w==0)
		{
			if(h>0) return M_PI_2;
			else return 3.*M_PI_2;
		}
		f=fabs(atan2(fabs(h), fabs(w)));
		if(w<0)
		{
			if(h>0) f=M_PI-f;
			else f=f-M_PI;
		}
		else if(h<0) f=-f;

		return f;
	}
	double Angle()
	{
		TDouble3DPoint d=TDouble3DPoint();
		return d.Angle(*this);//TDouble3DPoint(X, Y, Z));
	}
	AnsiString AsString() {return (FloatToStrF(X, ffFixed, 12, 4)+" "+FloatToStrF(Y, ffFixed, 12, 4)+" "+FloatToStrF(Z, ffFixed, 12, 4));}
};

//TDouble3DPoint Double3DPoint(double X, double Y, double Z) {TDouble3DPoint dp; dp.X=X; dp.Y=Y; dp.Z=Z; return dp;}

struct TDoubleRect
{
	TDoubleRect() {Left=0; Top=0; Right=0; Bottom=0; *((int*)LeftTopTemp)=0; *((int*)RightBottomTemp)=0;}
	TDoubleRect(double a, double b, double c, double d) {Left=a; Top=b; Right=c; Bottom=d; *((int*)LeftTopTemp)=0; *((int*)RightBottomTemp)=0;}
	TDoubleRect(TDoublePoint ab, TDoublePoint cd) {LeftTop=ab; RightBottom=cd;}
	union
	{
		struct
		{
			TDoublePoint LeftTop;
			TDoublePoint RightBottom;
		};
		struct
		{
			double Left;
			double Top;
			char LeftTopTemp[4];
			double Right;
			double Bottom;
			char RightBottomTemp[4];
		};
	};
	void Set(double a, double b, double c, double d) {Left=a; Top=b; Right=c; Bottom=d; *((int*)LeftTopTemp)=0; *((int*)RightBottomTemp)=0;}
	double Width() {return fabs(Right-Left);}//{return Right-Left;} //
	double Height() {return fabs(Bottom-Top);}//{return Bottom-Top;} //
	TDoublePoint Center() {return DoublePoint((Right+Left)/2., (Bottom+Top)/2.);}
	bool Contains(double x, double y) {return (((x>=Left && x<Right) || (x>=Right && x<Left)) && ((y>=Top && y<Bottom) || (y>=Bottom && y<Top)));}
	bool Contains(TDoublePoint xy) {return Contains(xy.X, xy.Y);}
	bool IsValid() {return (Right!=Left && Top!=Bottom);}
	void SetCorners(TDoublePoint lt, TDoublePoint rb, bool Square)
	{
		if((lt.X==0 && lt.Y==0) || (rb.X==0 && rb.Y==0)) return;
		if(lt.Y>rb.Y) {LeftTop.Y=lt.Y; RightBottom.Y=rb.Y;}
		else {LeftTop.Y=rb.Y; RightBottom.Y=lt.Y;}
		if(lt.X<rb.X) {LeftTop.X=lt.X; RightBottom.X=rb.X;}
		else {LeftTop.X=rb.X; RightBottom.X=lt.X;}
		*((int*)LeftTop.Temp)=*((int*)lt.Temp);
		*((int*)RightBottom.Temp)=*((int*)rb.Temp);
		if(Square)
		{
			double w=fabs(RightBottom.X-LeftTop.X), h=fabs(LeftTop.Y-RightBottom.Y);
			if(w>h)	{LeftTop.Y-=((w-h)/2.);	RightBottom.Y+=((w-h)/2.);}
			else if(h>w) {LeftTop.X-=((h-w)/2.); RightBottom.X+=((h-w)/2.);}
		}
	}
};

class TLockedDoubleRect
{
private:
	HANDLE LockEvent;
	//int LockCounter;
	union
	{
		struct
		{
			TDoublePoint lefttop;
			TDoublePoint rightbottom;
		};
		struct
		{
			double left;
			double top;
			char LeftTopTemp[4];
			double right;
			double bottom;
			char RightBottomTemp[4];
		};
	};
	bool __fastcall CheckLock() {try { return (LockEvent!=NULL && WaitForSingleObject(LockEvent, 0)==WAIT_OBJECT_0);} catch(...) {return false;}}
	bool __fastcall Lock() {bool b=!CheckLock(); if(b && LockEvent) SetEvent(LockEvent); else b=true; return b;}
	void __fastcall UnLock() {if(LockEvent) ResetEvent(LockEvent);}
	void __fastcall HardUnLock() {UnLock();}// LockCounter=0;}
protected:
	double __fastcall readLeft() {double d; while(!Lock()); d=left; UnLock(); return d;}
	double __fastcall readTop() {double d; while(!Lock()); d=top; UnLock(); return d;}
	double __fastcall readRight() {double d; while(!Lock()); d=right; UnLock(); return d;}
	double __fastcall readBottom() {double d; while(!Lock()); d=bottom; UnLock(); return d;}
	TDoublePoint __fastcall readLeftTop() {TDoublePoint dp; while(!Lock()); dp=lefttop; UnLock(); return dp;}
	TDoublePoint __fastcall readRightBottom() {TDoublePoint dp; while(!Lock()); dp=rightbottom; UnLock(); return dp;}
	void __fastcall writeLeft(double value) {while(!Lock()); left=value; UnLock();}
	void __fastcall writeTop(double value) {while(!Lock()); top=value; UnLock();}
	void __fastcall writeRight(double value) {while(!Lock()); right=value; UnLock();}
	void __fastcall writeBottom(double value) {while(!Lock()); bottom=value; UnLock();}
	void __fastcall writeLeftTop(TDoublePoint value) {while(!Lock()); lefttop=value; UnLock();}
	void __fastcall writeRightBottom(TDoublePoint value) {while(!Lock()); rightbottom=value; UnLock();}
public:
	__fastcall TLockedDoubleRect() {LockEvent=CreateEvent(NULL, true, false, NULL); left=0; top=0; right=0; bottom=0;}// LockCounter=0;}
	__fastcall TLockedDoubleRect(double a, double b, double c, double d) {LockEvent=CreateEvent(NULL, true, false, NULL); LockEvent=CreateEvent(NULL, true, false, NULL); left=a; top=b; right=c; bottom=d;}// LockCounter=0;}
	__fastcall ~TLockedDoubleRect() {HardUnLock(); CloseHandle(LockEvent); LockEvent=NULL;}

	void __fastcall Set(double a, double b, double c, double d) {while(!Lock()); try {left=a; top=b; right=c; bottom=d;} __finally {UnLock();}}
	double __fastcall Width() {double f; while(!Lock()); try {f=fabs(right-left);} __finally {UnLock();} return f;}
	double __fastcall Height() {double f; while(!Lock()); try {f=fabs(bottom-top);} __finally {UnLock();} return f;}
	TDoublePoint __fastcall Center() {TDoublePoint dp; while(!Lock()); try {dp=DoublePoint((right+left)/2., (bottom+top)/2.);} __finally {UnLock();} return dp;}
	void __fastcall SetCenter(double x, double y) {double w, h; while(!Lock()); try {w=(right-left)/2.; h=(bottom-top)/2.; left=x-w; right=x+w; top=y-h; bottom=x+h;} __finally {UnLock();}}
	void __fastcall SetCenter(TDoublePoint dp) {SetCenter(dp.X , dp.Y);}
	bool __fastcall Contains(double x, double y) {bool b; while(!Lock()); try {b=(x>=left && x<right && y>=top && y<bottom);} __finally {UnLock();} return b;}

	// !!! UnSafe outside public access!!!
	//bool __fastcall Lock() {bool b=!CheckLock(); if(b && LockEvent) SetEvent(LockEvent); else if(LockCounter==0) b=false; else b=true; LockCounter++; return b;}
	//void __fastcall UnLock() {LockCounter--; if(LockCounter<=0 && LockEvent) {ResetEvent(LockEvent); LockCounter=0;}}
	// !!! End of UnSafe !!!

	__property double Left = {read=readLeft, write=writeLeft};
	__property double Top = {read=readTop, write=writeTop};
	__property double Right = {read=readRight, write=writeRight};
	__property double Bottom = {read=readBottom, write=writeBottom};
	__property TDoublePoint LeftTop = {read=readLeftTop, write=writeLeftTop};
	__property TDoublePoint RightBottom = {read=readRightBottom, write=writeRightBottom};
};

#endif
