//---------------------------------------------------------------------------
#pragma hdrstop

#include "TCPClient.h"

//---------------------------------------------------------------------------

#pragma link "IdBaseComponent"
#pragma link "IdComponent"
#pragma link "IdTCPClient"
#pragma link "IdTCPConnection"
#pragma package(smart_init)

bool __fastcall TTCPClient::readConnected()
{
	try
	{
		if(_client) return _client->Connected();
		else return false;
	}
	catch(Exception &e)
	{
		return false;
	}
}

AnsiString __fastcall TTCPClient::readIPaddress()
{
	if(_client) return _client->Host;
	else return "";
}

int __fastcall TTCPClient::readPort()
{
	if(_client) return _client->Port;
	else return -1;
}

int __fastcall TTCPClient::readReadTimeout()
{
	if(_client) return _client->ReadTimeout;
	else return -1;
}

void __fastcall TTCPClient::writeReadTimeout(int value)
{
	if(_client) _client->ReadTimeout=value;
}

bool __fastcall TTCPClient::Connect(AnsiString IPaddr, int port, int Timeout)
{
	if(_client)
	{
		_client->Host=IPaddr;
		_client->Port=port;
		try
		{
			_client->ConnectTimeout=Timeout;
			_client->Connect();
		}
		catch(Exception &exception)
		{
			return false;
		}
		return true;
	}
	else return false;
}

bool __fastcall TTCPClient::Disconnect()
{
	if(_client && _client->Connected())
	{
		_client->Disconnect();
		delete _client;
		_client=new Idtcpclient::TIdTCPClient(NULL);
		return true;
	}
	else return false;
}

char __fastcall TTCPClient::ReadChar()
{
	if(_client && _client->IOHandler) return _client->IOHandler->ReadChar();
	else return 0;
}

System::Byte __fastcall TTCPClient::ReadByte(void)
{
	if(_client && _client->IOHandler) return _client->IOHandler->ReadByte();
	else return 0;
}

void __fastcall TTCPClient::ReadBytes(Sysutils::TBytes &VBuffer, int AByteCount, bool AAppend)
{
	if(_client && _client->IOHandler) return _client->IOHandler->ReadBytes(VBuffer, AByteCount, AAppend);
}

AnsiString __fastcall TTCPClient::ReadString(int N)
{
	if(_client && _client->IOHandler) return _client->IOHandler->ReadString(N);
	else return "";
}

AnsiString __fastcall TTCPClient::ReadLn()
{
	if(_client && _client->IOHandler) return _client->IOHandler->ReadLn();
	else return "";
}

void __fastcall TTCPClient::WriteLn(AnsiString Str)
{
	if(_client && _client->IOHandler) _client->IOHandler->WriteLn(Str);
}

__fastcall TTCPClient::TTCPClient(Classes::TComponent* AOwner)
{
	_client=new Idtcpclient::TIdTCPClient(AOwner);
}

__fastcall TTCPClient::~TTCPClient()
{
	Disconnect();
	delete _client;
}
