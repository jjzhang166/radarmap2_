//---------------------------------------------------------------------------

#pragma hdrstop

#include "Manager.h"
#include "Out_segy.h"
#include <registry.hpp>
#include "Preview.h"
#include "Inp_segy.h"
#include "DXFMap.h"
#include "rdnaptrans2008.h"
#include "MyMath.h"
#include "BingInetMap.h"
#include "OsmInetMap.h"
#include <WideStrUtils.hpp>
#include "DbDXFMapsContainer.h"
#include "MainForm.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

TCoordinateSystem _defaultCS;
TCoordinateSystemDimension _defaultCSD;

//---------------------------------------------------------------------------
// TProfileSatellites
//---------------------------------------------------------------------------
__fastcall TProfileSatellites::TProfileSatellites(TRadarMapManager *AOwner, TProfile *Profile)
{
	Owner=AOwner;
	profiles=new TList();
	pipesdetector=NULL;
	if(Profile && Owner && Owner->MapObjects)
	{
		outputview=Profile->OutputView;
		AddProfile(Profile);
		if(Owner->MapObjects!=NULL)
			path=new TMapPathObject(Owner->MapObjects, outputview, Owner->MapObjects->Container->LatLonRect, (TObject*)Owner, this);
		if(Owner->ObjectsContainer!=NULL)
		{
			Owner->ObjectsContainer->AssignCustomObjectsList(new TGraphicObjectsList(Owner->ObjectsContainer));
			customobjects=Owner->ObjectsContainer->CustomObjectsList;
		}
		if(Owner->Connection!=NULL)
			connectionsettings=Owner->Connection->LinkSettings();
		if(Owner->Settings && Owner->Settings->ProfilePipesDetector) StartPipesDetector();
	}
	else
	{
		outputview=NULL;
		path=NULL;
		customobjects=NULL;
	}
}
//---------------------------------------------------------------------------

__fastcall TProfileSatellites::~TProfileSatellites()
{
	TProfile *prof;

	/*if(outputview!=NULL)
	{
		prof=outputview->Profile;
		if(prof!=NULL) delete prof;
	}*/
	StopPipesDetector();
	ClearProfiles();
	delete path;
	if(customobjects)
	{
		if(!customobjects->Linked) delete customobjects;
		else customobjects->Linked=false;
	}
	if(connectionsettings) delete connectionsettings;
}
//---------------------------------------------------------------------------

void __fastcall TProfileSatellites::AddProfile(TProfile *Profile)
{
	if(profiles->IndexOf(Profile)<0)
	{
		profiles->Add(Profile);
		if(!outputview) outputview=Profile->OutputView;
		Profile->OutputView=outputview;
		Profile->RefreshAccessTime();
	}
}
//---------------------------------------------------------------------------

void __fastcall TProfileSatellites::DeleteProfile(int Index)
{
	TProfile *prof, *prof2;
	int i;

	prof=Profiles[Index];
	if(prof)
	{
		if(prof->OutputViewOwner && profiles->Count>1)
		{
			prof->OutputViewOwner=false;
			i=Index+1;
			if(i>=profiles->Count) i=0;
			prof2=Profiles[i];
			if(prof2) prof2->OutputViewOwner=true;
		}
		delete prof;
		profiles->Delete(Index);
	}
}
//---------------------------------------------------------------------------

TRadarMapSettings* __fastcall TProfileSatellites::readSettings()
{
	if(Owner!=NULL) return Owner->Settings;
	else return NULL;
}

//---------------------------------------------------------------------------
// TProfileSatellitesList
//---------------------------------------------------------------------------
void __fastcall TProfileSatellitesList::GenerateInfo()
{
	TGPRUnit* settings;
	AnsiString str;
	TMapsContainer* mc;


	str="";
	if(Owner && Owner->Settings)
	{
		str="---=== RadarMap Settings ===---\r\n\r\n";
		str+=Owner->Settings->AsString();
		str+="\r\n---======---\r\n\r\n";
		str+="---=== Project ===---\r\n\r\n";
		if(Owner->MapObjects && Owner->MapObjects->Container)
		{
			str+="Coordinate system: ";
			TGPSCoordinate *c;
			c=new TGPSCoordinate();
			try
			{
				str+=c->GetCSName(Owner->MapObjects->Container->CoordinateSystem);
			}
			__finally
			{
				delete c;
			}
			/*switch(Owner->MapObjects->Container->CoordinateSystem)
			{
				case csDutch: str+="Dutch RD (Bessel 1841)\r\n"; break;
				case csBeLambert72: str+="Belgian Lambert72 (Hayford 1909)\r\n"; break;
				case csBeLambert08: str+="Belgian Lambert2008 (GRS 80)\r\n"; break;
				case csUTM: str+="UTM Universal Transverse Mercator (GRS 80)\r\n"; break;
				case csLKS: str+="Latvian Geodetic Coordinate System 1992 (GRS 80)\r\n"; break;
				default:
				case csLatLon: str+="Latitude - Longitude (WGS84)\r\n"; break;
			}*/
			for(int i=0; i<Owner->MapObjects->Container->Count; i++)
			{
				mc=Owner->MapObjects->Container->GetObject(i);
				if(mc)
				{
					switch(mc->MapType)
					{
						case mtXML: str+="XML "; break;
						case mtDXF: str+="DXF "; break;
						case mtEmpty: str+="DXF "; break;
					}
					str+="Map: "+mc->Klicnummer+"\r\n";
					str+="  Filename: "+mc->FileName+"\r\n";
					if(mc->LatLonRect)
					{
						str+="  Lower corner: "+mc->LatLonRect->LeftTop->AsString(mc->CoordinateSystem)+"\r\n";
						str+="  Upper corner: "+mc->LatLonRect->RightBottom->AsString(mc->CoordinateSystem)+"\r\n";
					}
					else
					{
						str+="  Lower corner: "+mc->LowerCorner+"\r\n";
						str+="  Upper corner: "+mc->UpperCorner+"\r\n";
					}
				}
			}
		}
		generatedinfo=str;
	}
}
//---------------------------------------------------------------------------

int __fastcall TProfileSatellitesList::Add(TProfileSatellites* o)
{
	try
	{
		if(CustomObjectsChanged) *CustomObjectsChanged=true;
		if(o && o->Path)
		{
			if(Owner && Owner->Settings)
			{
				o->Path->ChainedColors=false;//true;
				//o->Path->SelectionColor=Owner->Settings->GetNextPathColor();
				o->Path->SelectionColor=clRed32;
				/*if(Owner->Settings->PathColorIncIndex<=1)
				{
					o->Path->Color=clLime32;
					Owner->Settings->GetNextPathColor();
				}
				else*/ o->Path->Color=Owner->Settings->GetNextPathColor();
			}
			o->Path->Name="Path "+IntToStr(++pathindex);
		}
	}
	catch(Exception &e) {}
	return List->Add((TObject*)o);
}

//---------------------------------------------------------------------------
// TRadarMapManager
//---------------------------------------------------------------------------
__fastcall TRadarMapManager::TRadarMapManager(Classes::TComponent* AOwner)
{
	Owner=AOwner;
	int k;

	PlugInManager=new TPlugInManager();
	CacheManager=new TCacheObjectsManager((AnsiString)_RadarMapName+"Temp", "temporary", this);

	palette=new TPalette();

	gpslisteners=new TGpsListenersList();

	SatellitesList=new TProfileSatellitesList(this);
	current=NULL;
	currentindex=-1;
	settings=new TRadarMapSettings;
	settings->Owner=this;

	PrismDirectory=Application->ExeName;
	for(k=PrismDirectory.Length()-1; k>=0; k--)
		if(*(PrismDirectory.c_str()+k)=='\\') break;
	PrismDirectory.SetLength(k);

	DefaultSettings();
	LoadSettings();
	newsettingsavailableevent=CreateEvent(NULL, false, false, NULL);
	globalstopitevent=CreateEvent(NULL, true, false, NULL);

	connection=NULL;
	objectscontainer=NULL;
	mapobjects=NULL;
	linesdetector=NULL;
	trackingpath=NULL;
	sparonmap=NULL;
	gyrocompass=NULL;
	mapmessages=NULL;
	gpsmessages=NULL;
	coordinatesmessage=NULL;
	gpsmessage=NULL;
	laptopmessage=NULL;
	radarmessages=NULL;
	profrects=NULL;
	scales=NULL;
	gpsclient=new TNmeaClient(this, &Settings->GPS);
	//gpsclient=NULL;
	twowheelclient=NULL;
	battery=new TBattery();
	battery->BatteryType=settings->BatteryType;
	laptopbattery=new TLaptopBattery();
	resultsnotsaved=false;

	ProfileClearFunction=NULL;

	lastlabelid=0;

	lastGPSpoint=new TGPSCoordinate();
	lastGPSpoint->Locking=true;

	startstopwaypointbtn=NULL;
	startwaypoint=NULL;
	stopwaypoint=NULL;
}
//---------------------------------------------------------------------------

__fastcall TRadarMapManager::~TRadarMapManager()
{
	try
	{
        SaveSettings();

		SetEvent(GlobalStopItEvent);
		CloseConnection();
		if(gpsunit) delete gpsunit;
		gpsunit=NULL;
		if(sparunit) delete sparunit;
		if(linesdetector!=NULL) delete linesdetector;
		//if(trackingpath) delete trackingpath; //deleting by destructor of mapobjects
		//if(mapmessages) delete mapmessages; //deleting by destructor of mapobjects
		//if(radarmessages) delete radarmessages; //deleting by destructor of objectscontainer
		delete gpsclient;
		if(twowheelclient)
		{
			delete twowheelclient;
			twowheelclient=NULL;
		}
		delete battery;
		delete laptopbattery;
		delete SatellitesList;
		if(objectscontainer!=NULL) delete objectscontainer;
		if(mapobjects!=NULL) delete mapobjects;
		CloseHandle(newsettingsavailableevent);
		CloseHandle(globalstopitevent);
		globalstopitevent=NULL;
		gpslisteners=new TGpsListenersList();
		gpslisteners=NULL;
		delete palette;
		palette=NULL;
		delete PlugInManager;
		PlugInManager=NULL;
		delete CacheManager;
		CacheManager=NULL;
		delete lastGPSpoint;
		lastGPSpoint=NULL;
		delete settings;
	}
	catch(Exception &e) {}
}
//---------------------------------------------------------------------------

bool __fastcall TRadarMapManager::OpenConnection(void)
{
	if(connection==NULL)
	{
		connection=new TConnection(Owner, this);
		connection->IPaddress=settings->GeoradarTCPAddr;
		return connection->TryToConnect();
	}
	else return false;
}
//---------------------------------------------------------------------------

void __fastcall TRadarMapManager::CloseConnection(void)
{
	try
	{
		if(connection!=NULL)
		{
			try
			{
				connection->TryToDisconnect();
			}
			catch(Exception &e) {}
			delete connection;
			connection=NULL;
			if(pingtool) pingtool->ChangeIPAddress();
		}
	}
	catch(Exception &e)
	{
		connection=NULL;
	}
}
//---------------------------------------------------------------------------

void __fastcall TRadarMapManager::CreateObjectContainer(TImgView32* ProfImage, Types::TRect **profRect, TScale** AScales)
{
	profrects=profRect;
	scales=AScales;
	if(ProfImage!=NULL)
	{
		try
		{
			try
			{
				//if(profilescroller!=NULL) delete profilescroller; //deleting by destructor of objectscontainer
				//if(radarmessages!=NULL) delete radarmessages; //deleting by destructor of objectscontainer
				if(objectscontainer!=NULL) delete objectscontainer;
			}
			catch(Exception &e) {}
			objectscontainer=new TGraphicObjectsContainer(ProfImage, true, NULL, (TObject*)settings);
			objectscontainer->SmallSize=Settings->SmallSizeControls;
			objectscontainer->ObjectsChanged=&ResultsNotSaved;
			profilescroller=new TProfileScroller(objectscontainer, ProfImage->GetViewportRect(), NULL, (TObject*)this);
			if(settings->Messages)
			{
				radarmessages=new TMessagesScrollObject(objectscontainer, ProfImage, ProfImage->GetViewportRect(), msaTop, (TObject*)this);
			}
			else
			{
				radarmessages=NULL;
			}
			pingtool=new TToolPing(objectscontainer, ProfImage, this);
			memgraph=new TToolMemGraph(objectscontainer, ProfImage, this);
			recyclebin=new TRecycleBin(objectscontainer, ProfImage, this);
			//hyperbola=new THyperbolaTool(objectscontainer, ProfImage, this);
		}
		__finally
		{
			ResultsNotSaved=false;
        }
	}
}
//---------------------------------------------------------------------------

void __fastcall TRadarMapManager::CreateMapObjects(TImgView32* MapImage, TMouseAction *ma, TVoidFunction DBE)
{
	if(MapImage!=NULL)
	{
		try
		{
			try
			{
				if(mapobjects!=NULL)
				{
					coordinatesmessage=NULL;
					gpsmessage=NULL;
					laptopmessage=NULL;
					if(linesdetector) delete linesdetector;
					if(gpsunit) delete gpsunit;
					//if(twowheelclient) delete twowheelclient;
					if(sparunit) delete sparunit;
					delete mapobjects;
				}
			}
			catch(Exception &e) {}
			mapobjects=new TMapObjectsContainer(MapImage, true, ma, DBE, (TObject*)settings);
			mapobjects->SmallSize=Settings->SmallSizeControls;
			mapobjects->ObjectsChanged=&ResultsNotSaved;
			if(settings->CommunicationLinesDetector)
				linesdetector=new TLinesDetector(MapImage, mapobjects->Container->UtilityPtr, mapobjects->Container->LatLonRect, (TObject*)this);
			else linesdetector=NULL;
			if(settings->GPSTracking)
				trackingpath=new TMapTrackPathObject(mapobjects, mapobjects->Container->LatLonRect, (TObject*)this);
			else trackingpath=NULL;
			if(settings->Messages)
			{
				mapmessages=mapobjects->Messages;//new TMessagesScrollObject(mapobjects, MapImage, MapImage->GetViewportRect(), msaTop, (TObject*)this);
				gpsmessages=new TMessagesScrollObject(mapobjects, MapImage, MapImage->GetViewportRect(), msaBottom, (TObject*)this);
			}
			else
			{
				mapmessages=NULL;
				gpsmessages=NULL;
			}
			mapprogress=new TToolProgressBar(mapobjects, MapImage, this);
			mapobjects->Container->Progress=mapprogress;
			if(FileExists(settings->GPS.UnitFileName))
				gpsclient->LoadCommandsFromFile(settings->GPS.UnitFileName);
			if(settings->WheelPositioning==TPositioning::TwoWheel)
			{
				if(!twowheelclient)	twowheelclient=new TTwoWheelNmeaClient(this, &Settings->TwoWheelsDRS);
				if(FileExists(settings->TwoWheelsDRS.UnitFileName))
					twowheelclient->LoadCommandsFromFile(settings->TwoWheelsDRS.UnitFileName);
			}
			else if(twowheelclient)
			{
				delete twowheelclient;
				twowheelclient=NULL;
			}

			gpsunit=new TGpsUnit(gpsclient, gpslisteners, this);
			if(twowheelclient) gpsunit->AddNmeaClient(twowheelclient);
	#ifdef DutchRDNAPTRANS2008
			/*GRID_FILE_DX=PrismDirectory+"\\x2c.grd";
			GRID_FILE_DY=PrismDirectory+"\\y2c.grd";
			GRID_FILE_GEOID=PrismDirectory+"\\nlgeo04.grd";*/
			RdNapTrans2008::SetDirectory(PrismDirectory+"\\");
	#endif
			gpsunit->Initialize();
			if(settings->SparEnable)
			{
				sparunit=new TSparUnit(this, settings->SparShell);
				sparunit->Connect(settings->SparPipeName);
				sparonmap=new TMapSparObject(mapobjects, mapobjects->Container->LatLonRect, (TObject*)this);
			}
			else
			{
				sparunit=NULL;
				sparonmap=NULL;
			}
			if(settings->SparGyroCompass)
			{
				gyrocompass=new TGyroCompassObject(mapobjects, mapobjects->Image, (TObject*)this);
				gyrocompass->EasyRendering=settings->EasyRendering;
				gyrocompass->ShowGlass=false;
			}
			else gyrocompass=NULL;
		}
		__finally
		{
			ResultsNotSaved=false;
        }
	}
}
//---------------------------------------------------------------------------

void __fastcall TRadarMapManager::CreateProfileSatellites(void)
{
	if(connection && connection->Settings)
	{
		if(connection->Settings->SelectedChannelMode==TChannelMode::Double ||
			connection->Settings->SelectedChannelMode==TChannelMode::Circle)
		{
			if(connection->Settings->Channels && connection->Settings->Channels[0] &&
				connection->Settings->Channels[0]->Profile)
			{
				current=new TProfileSatellites(this, connection->Settings->Channels[0]->LinkProfile());
				currentindex=SatellitesList->Add(current);
				for(int i=1; i<connection->Settings->SelectedChannelMode; i++)
				{
					if(connection->Settings->Channels[i] && connection->Settings->Channels[i]->Profile)
						current->AddProfile(connection->Settings->Channels[i]->LinkProfile());
				}
			}
			else
			{
				current=NULL;
				currentindex=-1;
			}
		}
		else
		{
			if(connection->Settings->SelectedChannel && connection->Settings->SelectedChannel->Profile)
			{
				current=new TProfileSatellites(this, connection->Settings->SelectedChannel->LinkProfile());
				currentindex=SatellitesList->Add(current);
			}
			else
			{
				current=NULL;
				currentindex=-1;
			}
		}
	}
	else
	{
		current=NULL;
		currentindex=-1;
	}
}
//---------------------------------------------------------------------------

void __fastcall TRadarMapManager::SwitchToPath(TMapPathObject* APath)
{
	TProfileSatellites* ps;
	int j, i=SatellitesList->Count;

	if(APath)
	{
		for(i=0; i<SatellitesList->Count; i++)
		{
			ps=(TProfileSatellites*)SatellitesList->Items[i];
			if(ps && ps->Path==APath)
			{
				current=ps;
				currentindex=i;
				objectscontainer->AssignCustomObjectsList(ps->CustomObjects);
				profilescroller->Update(gotControl);
				mapobjects->Update(gotCustom);
				for(j=0; j<current->ProfilesCount; j++)
				{
					//current->Profiles[j]->CurrentTracePtr=NULL;
					SetEvent(current->Profiles[j]->TracesAddedEvent);
					current->Profiles[j]->RefreshAccessTime();
				}
				CurrentOutputView->ApplyScrolling();
				CurrentProfile->NeedToScaleZero=true;
				break;
			}
		}
	}
	if(i>=SatellitesList->Count) // Null path
	{
		current=NULL;
		currentindex=-1;
		objectscontainer->AssignCustomObjectsList(NULL);
		profilescroller->Update(gotControl);
		mapobjects->Update(gotCustom);
		if(ProfileClearFunction) (ProfileClearFunction)(true);
	}
}
//---------------------------------------------------------------------------

void __fastcall TRadarMapManager::SwitchToAvailablePath()
{

	if(SatellitesList->Count>0 && (TProfileSatellites*)SatellitesList->Items[SatellitesList->Count-1])
		SwitchToPath(SatellitesList->Items[SatellitesList->Count-1]->Path);
	else SwitchToPath(NULL);
}

void __fastcall TRadarMapManager::DeleteFilesInDirectory(AnsiString PathName, AnsiString FileMask)
{
	WIN32_FIND_DATA FileData;
	HANDLE hSearch;
	AnsiString S;

	hSearch=FindFirstFile((PathName+"\\"+FileMask).c_str(), &FileData);
	if(hSearch!=INVALID_HANDLE_VALUE)
	{
		try
		{
			do
			{
				S=FileData.cFileName;
				if(S!="." && S!="..")
				{
					if(FileData.dwFileAttributes!=FILE_ATTRIBUTE_DIRECTORY)
						DeleteFile(PathName+"\\"+FileData.cFileName);
				}
			} while(FindNextFile(hSearch, &FileData));
		}
		__finally
		{
			FindClose(hSearch);
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TRadarMapManager::DefaultSettings()
{
	int k;

	settings->RadarMapDirectory=PrismDirectory;
	DeleteFilesInDirectory(PrismDirectory, "temp*.tmp");
	settings->RadarMapOutputsCounter=0;
	settings->MapLabelGeometryIDs=0;
	settings->PathColorIncIndex=2;

	// additional ScaleX-only calculation
	settings->MapScaleApplying=false;//true;//
	// Detecting pixels vizualization
	settings->LinesDetectorDebug=false;//true;//

	//Not distributed still
	settings->Messages=true;
	settings->MessagesAnimation=true;

	//GPR
	settings->GeoradarTCPAddr="192.168.001.010";
	settings->WheelPositioning=TPositioning::Wheel;//TPositioning::ManualP;//
	settings->WheelTraceDistance=0.01;//0.01;
	settings->WheelPulses=128;
	settings->WheelDiameter=0.124;
	settings->TracesInPixel=1;
	settings->Permitivity=9;
	settings->ApplyConstantDeduct=true;
	settings->BatteryType=TBatteryType::btLeadAcid;
	settings->ApplyGain=true;
	settings->OneWheelForAllChannels=true;

	//Automatic profiling
	settings->AutoStopProfile=false;
	settings->AutoStopProfileSize=50; //in MB
	settings->AutoSaveProfile=false;
	settings->AutoCloseProfile=true;
	settings->AutoSaveDir=PrismDirectory;

	//Filters
	settings->WSWindowWidth=10;
	settings->WeightedSubtractionFilter=true;//false;//
	settings->MoveoutCorrection=true;

	//GPS
	settings->GPS.DeviceName="NMEA GPS receiver";
	settings->GPS.ComPort="COM1";
	settings->GPS.PortSpeed=4800;
	settings->GPS.MaxTerminalLines=300;
	settings->GPS.ReadReplyTimeOut=30000; //INFINITE; // 30 sec, also could be INFINITE
	settings->GPS.InitTimeOut=5*60000; // 5 min, also could be INFINITE
	settings->GPS.UnitName="NMEA GPS Receiver";
	settings->GPS.UnitFileName="";
	settings->GPS.RTSCTSEnable=true;
#ifdef _InfraRadarOnly
	settings->DefaultCoordinateSystem=csDutch;
#else
	settings->DefaultCoordinateSystem=csUTM;//csLatLon;
#endif
	settings->GPS.AntennaHeight=1.5;
	settings->GPS.UseZGeoid=false;
	settings->GPS.LogToFile=false;
	settings->GPS.InitOnStart=false;
	settings->GPS.UseGpsForStart=true;

	settings->UseGreenwichTime=false;

	//GPS Simulator
	settings->GPS.SimTcpSource=false;//true;//
	settings->GPS.SimTieCoordinatesToMapCenter=false;//false;//
	settings->GPS.SimTcpAddr="127.0.0.1";
	settings->GPS.SimFileSource=false;//true;//
	settings->GPS.SimFileName=PrismDirectory+"\\GPGGA.txt";

	settings->MaxDeviceSpeed=14; //meters per second ~50 kmph

	//TwoWheelsDRS
	settings->TwoWheelsDRS.DeviceName="WPS";
	settings->TwoWheelsDRS.ComPort="COM14";
	settings->TwoWheelsDRS.PortSpeed=115200;
	settings->TwoWheelsDRS.MaxTerminalLines=100;
	settings->TwoWheelsDRS.ReadReplyTimeOut=5000; //INFINITE; // 30 sec, also could be INFINITE
	settings->TwoWheelsDRS.InitTimeOut=60000; // 5 min, also could be INFINITE
	settings->TwoWheelsDRS.UnitName="NMEA WPS Receiver";
	settings->TwoWheelsDRS.UnitFileName=PrismDirectory+"\\twowheel.xml";
	settings->TwoWheelsDRS.RTSCTSEnable=true;
	settings->TwoWheelsDRS.AntennaHeight=0;
	settings->TwoWheelsDRS.UseZGeoid=true;
	settings->TwoWheelsDRS.LogToFile=false;
	settings->TwoWheelsDRS.InitOnStart=false;
	settings->TwoWheelsDRS.UseGpsForStart=false;
	settings->WPSasWheel=false;

	//TwoWheelsDRS Simulator
	settings->TwoWheelsDRS.SimTcpSource=false;//true;//
	settings->TwoWheelsDRS.SimTieCoordinatesToMapCenter=false;//false;//
	settings->TwoWheelsDRS.SimTcpAddr="127.0.0.1";
	settings->TwoWheelsDRS.SimFileSource=false;//true;//
	settings->TwoWheelsDRS.SimFileName=PrismDirectory+"\\TwoWheelsDRS.txt";

	//DXF
	settings->DXFIncludeMap=true;//true;//
	settings->DXFBackgroundColor=0xFFF8F8E0;
	settings->DXFColorInvertThreshold=60;
	settings->DXFPathEndsWithArrow=true;//true;//
	settings->DXFUsePitch=true;
	settings->DXFUseRoll=true;
	settings->DXFConfidence=true;
	settings->DXFDescriptionAsText=true;
	settings->DXFDescriptionTextHeight=1.;
	Settings->DXFDepthToDescription=true;
	Settings->DXFIdToDescription=true;
	settings->DXFPolyline3D=true;
	settings->DXFEnlargeText=true;

	//SQLiteDXF
//	settings->DbDXFAllowed=true;
	settings->DbStoreLayersVisability=true;
	settings->DbMergeLayersWithSameName=true;//false;
	settings->DbShowLayerFileName=true;

	//BingOnlineMap
	settings->BingInetMapAppKey = "";
	settings->BingInetMapViewType = "Road";
	settings->BingInetMapDefaultZoom = 18;
	settings->BingInetMapMinZoom = 0;//13;
	settings->BingInetMapMaxZoom = 19;

	//ImageMap
	settings->SaveImageMapChanges=true;

	//XYZC
	settings->XYZCCellWidth=0.05;
	settings->XYZCCellHeight=0.05;
	settings->XYZCUseZ=true;
	settings->XYZCUsePitch=true;
	settings->XYZCUseRoll=true;
	settings->XYZCGained=true;
	settings->XYZCAbsolute=false;
	settings->XYZCEnvelope=true;

	//SPAR
	settings->SparEnable=false;//true;//
	settings->SparPipeName="testpipe";
	settings->SparShell="C:\\Program Files\\FieldSens View\\FieldSensAppWin.exe";
	settings->SparShellWait=5000; //in miliseconds
	settings->SparSimulation=false;//true;//
	settings->SparCOMPortNumber=4;//27;//
	settings->SparFrequency=982;//491;
	settings->SparAverageType=0;
	settings->SparAverageTime=0;
	settings->SparUpdateRate=0;
	settings->SparElevation=0.4;
	settings->SparToAntennaFwd=1;
	settings->SparToAntennaRight=0;
	settings->SparToAntennaUp=1.1;
	settings->SparUseBaseline=true;
	settings->SparConfidenceMax=50; //in percents
	settings->SparMinDbField=20; //in dB
	settings->SparGyroCompass=true;
	settings->SparAnglesCompensation=true;
	settings->SparPitchCompensation=0;
	settings->SparRollCompensation=0;
	settings->SparHeadingCompensation=0;
	settings->SparEliminateFSVCorrection=true;

	//PipesDetector
	settings->ProfilePipesDetector=false;//true;//
	settings->PipesDetectionWinWidth=3.;//6.; //in meters
	settings->PipesDetectionWinOverlap=1;//3.; //in meters from the right edge of PipesDetectionWin
	settings->PipesDetectionWinResultsBorder=settings->PipesDetectionWinOverlap/2.;
	settings->PipesDetectionPermitFrom=4.;
	settings->PipesDetectionPermitRange=35.;
	settings->PipesDetectionHoughAccumMax=0;

	//Performance
	settings->MapPreview=true;
	settings->EasyRendering=false;
	settings->GPSTracking=true;
	settings->ShowGPSCoordinates=true;
	settings->ShowWindRose=true;
	settings->ShowClocks=true;
	settings->ShowPing=true;
	settings->ShowMemGraph=true;
	settings->FollowMe=false;//true;
	settings->CommunicationLinesDetector=true;
	settings->EasyZoomIn=false;
	settings->ShowBattery=true;
	settings->ShowRecycleBin=true;
	settings->RecycleBinTAlign=vtoaRightBottom;
	settings->ShowGyroCompass=true;
	settings->GyroCompassTAlign=vtoaRightBottom;
	settings->SmallSizeControls=false;
	settings->ShowLabelIcons=true;
	settings->CopyAttachments=true;
	settings->ShowLabelInformationOnPin=true;
	settings->ShowLabelConfidence=true;
	settings->TraceTransparency=192;//128;
	settings->ShowAnnotationLayerOnOpen=true;
	settings->EmptyMapSize=100;
	settings->EnlargeEmptyMap=true;
	settings->ShowMapLabelNotification=true;
	settings->DrawPathName=true;
	settings->ProfBackgroundColor=0xFFFFFFFF;
	settings->Gml3Dpoints=true;//false;
	settings->WindowState=wsMaximized;
	settings->WindowTop=0;
	settings->WindowLeft=0;
	settings->WindowWidth=Screen->Width;
	settings->WindowHeight=Screen->Height;
	settings->PanelsOrientation=rpoVertical;//rpoAuto;//rpoHorizontal;//

	settings->MapObjectsPrediction=true;
	settings->MapObjectsPredictingRadius=2.5;
	settings->LabelDestructionTimeByClick=15000;
	settings->LabelDestructionTimeByTrack=5000;
	settings->MapObjectsPredictionSinglePerLayer=true;

	//MapLabelsGeometry
	settings->MapLabelSpiderBeamsSector=15;
	settings->SingleTypeMapLabelsGeometry=true;//false;

	//Chris Howard
	/*settings->ChrisMasterIni="C:\\ChrisHoward\\Data\\GTFL_PrismInterfaceMaster.ini";//settings->RadarMapDirectory+"\\ChrisHoward\\Data\\GTFL_PrismInterfaceMaster.ini";
	settings->ChrisSlaveIni="C:\\ChrisHoward\\Data\\GTFL_PrismInterfaceSlave.ini";//settings->RadarMapDirectory+"\\ChrisHoward\\Data\\GTFL_PrismInterfaceSlave.ini";
	settings->ChrisExe_AVX1="C:\\ChrisHoward\\HyperbolaFinder_AVX1.exe";
	settings->ChrisExe_AVX2="C:\\ChrisHoward\\HyperbolaFinder.exe";
	settings->ChrisExe_SSE2="C:\\ChrisHoward\\HyperbolaFinder_SSE2.exe";
	settings->ChrisExe_Pent="C:\\ChrisHoward\\HyperbolaFinder_Pent.exe";*/

	settings->ChrisMasterIni=settings->RadarMapDirectory+"\\ChrisHoward\\Data\\GTFL_PrismInterfaceMaster.ini";
	settings->ChrisSlaveIni=settings->RadarMapDirectory+"\\ChrisHoward\\Data\\GTFL_PrismInterfaceSlave.ini";
	settings->ChrisExe_AVX1=settings->RadarMapDirectory+"\\ChrisHoward\\HyperbolaFinder_AVX1.exe";
	settings->ChrisExe_AVX2=settings->RadarMapDirectory+"\\ChrisHoward\\HyperbolaFinder.exe";
	settings->ChrisExe_SSE2=settings->RadarMapDirectory+"\\ChrisHoward\\HyperbolaFinder_SSE2.exe";
	settings->ChrisExe_Pent=settings->RadarMapDirectory+"\\ChrisHoward\\HyperbolaFinder_Pent.exe";
	settings->ChrisShowExeOutput=false;
	settings->ChrisExeKeepAliving=true;
	settings->ChrisExeTimeout=5000;

	settings->ChrisExe=settings->GetChrisExe();//"C:\\ChrisHoward\\HyperbolaFinder.exe";//"C:\\ChrisHoward_VS6\\matcher.exe";//settings->RadarMapDirectory+"\\ChrisHoward\\matcher.exe";

	//Constants
	settings->DoubleMaximumLayerSizePX=true;
	settings->MaximumLayerSizePX=3000;
	settings->OpenMapType=0;
	settings->ReferencePointCoordinatesQ=10;

	//Internal variables
	settings->LastMapCenter=DoublePoint(0, 0);
	settings->LastMapCS=csLatLon;
	settings->LastMapZoom=16;
}
//---------------------------------------------------------------------------

void __fastcall TRadarMapManager::LoadSettings()
{
	TRegistry& regKey=*new TRegistry();
	try
	{
		try
		{
			if(regKey.OpenKey(_RadarMapRegKey,true))
			{
				//Common parameters
				if(regKey.ValueExists("RadarMapOutputsCounter")) settings->RadarMapOutputsCounter=regKey.ReadInteger("RadarMapOutputsCounter");
				//GPR
				if(regKey.ValueExists("GeoradarTCPAddr")) settings->GeoradarTCPAddr=regKey.ReadString("GeoradarTCPAddr");
				if(regKey.ValueExists("WheelPositioning")) settings->WheelPositioning=(TPositioning)regKey.ReadInteger("WheelPositioning");
				if(regKey.ValueExists("WheelTraceDistance")) settings->WheelTraceDistance=regKey.ReadFloat("WheelTraceDistance");
				if(regKey.ValueExists("WheelPulses")) settings->WheelPulses=regKey.ReadInteger("WheelPulses");
				if(regKey.ValueExists("WheelDiameter")) settings->WheelDiameter=regKey.ReadFloat("WheelDiameter");
				if(regKey.ValueExists("TracesInPixel")) settings->TracesInPixel=regKey.ReadInteger("TracesInPixel");
				if(regKey.ValueExists("Permitivity")) settings->Permitivity=regKey.ReadFloat("Permitivity");
				if(regKey.ValueExists("ApplyConstantDeduct")) settings->ApplyConstantDeduct=regKey.ReadBool("ApplyConstantDeduct");
				if(regKey.ValueExists("BatteryType")) settings->BatteryType=(TBatteryType)regKey.ReadInteger("BatteryType");

				//???

				if(regKey.ValueExists("ApplyGain")) settings->ApplyGain=regKey.ReadBool("ApplyGain");
				if(regKey.ValueExists("OneWheelForAllChannels")) settings->OneWheelForAllChannels=regKey.ReadBool("OneWheelForAllChannels");

				//Filters
				if(regKey.ValueExists("WeightedSubtractionFilter")) settings->WeightedSubtractionFilter=regKey.ReadBool("WeightedSubtractionFilter");
				if(regKey.ValueExists("WSWindowWidth")) settings->WSWindowWidth=regKey.ReadInteger("WSWindowWidth");
				if(regKey.ValueExists("MoveoutCorrection")) settings->MoveoutCorrection=regKey.ReadBool("MoveoutCorrection");

				//GPS
				if(regKey.ValueExists("GpsComPort")) settings->GPS.ComPort=regKey.ReadString("GpsComPort");
				if(regKey.ValueExists("GpsPortSpeed")) settings->GPS.PortSpeed=regKey.ReadInteger("GpsPortSpeed");
				if(regKey.ValueExists("GpsMaxTerminalLines")) settings->GPS.MaxTerminalLines=regKey.ReadInteger("GpsMaxTerminalLines");
				//if(regKey.ValueExists("GpsUnitType")) settings->GPS.UnitType=regKey.ReadInteger("GpsUnitType");
				if(regKey.ValueExists("GpsUnitName")) settings->GPS.UnitName=regKey.ReadString("GpsUnitName");
				if(regKey.ValueExists("GpsUnitFileName")) settings->GPS.UnitFileName=regKey.ReadString("GpsUnitFileName");
				if(regKey.ValueExists("GpsRTSCTSEnable")) settings->GPS.RTSCTSEnable=regKey.ReadBool("GpsRTSCTSEnable");
				if(regKey.ValueExists("DefaultCoordinateSystem")) settings->DefaultCoordinateSystem=(TCoordinateSystem)regKey.ReadInteger("DefaultCoordinateSystem");
				if(regKey.ValueExists("GpsAntennaHeight")) settings->GPS.AntennaHeight=regKey.ReadFloat("GpsAntennaHeight");
				if(regKey.ValueExists("GpsUseZGeoid")) settings->GPS.UseZGeoid=regKey.ReadBool("GpsUseZGeoid");
				if(regKey.ValueExists("GpsLogToFile")) settings->GPS.LogToFile=regKey.ReadBool("GpsLogToFile");
				if(regKey.ValueExists("GpsInitOnStart")) settings->GPS.InitOnStart=regKey.ReadBool("GpsInitOnStart");
				if(regKey.ValueExists("GpsUseGpsForStart")) settings->GPS.UseGpsForStart=regKey.ReadBool("GpsUseGpsForStart");
				if(regKey.ValueExists("UseGreenwichTime")) settings->UseGreenwichTime=regKey.ReadBool("UseGreenwichTime");

				//GPS Simulator
				if(regKey.ValueExists("GpsSimTcpSource")) settings->GPS.SimTcpSource=regKey.ReadBool("GpsSimTcpSource");
				if(regKey.ValueExists("GpsSimTieCoordinatesToMapCenter")) settings->GPS.SimTieCoordinatesToMapCenter=regKey.ReadBool("GpsSimTieCoordinatesToMapCenter");
				//if(regKey.ValueExists("GpsSimTcpAddr")) settings->GPS.SimTcpAddr=regKey.ReadString("GpsSimTcpAddr");
				if(regKey.ValueExists("GpsSimFileSource")) settings->GPS.SimFileSource=regKey.ReadBool("GpsSimFileSource");
				//if(regKey.ValueExists("GpsSimFileName")) settings->GPS.SimFileName=regKey.ReadString("GpsSimFileName")

				if(regKey.ValueExists("MaxDeviceSpeed")) settings->MaxDeviceSpeed=regKey.ReadFloat("MaxDeviceSpeed");

				//TwoWheelsDRS
				if(regKey.ValueExists("TWDRSComPort")) settings->TwoWheelsDRS.ComPort=regKey.ReadString("TWDRSComPort");
				if(regKey.ValueExists("TWDRSPortSpeed")) settings->TwoWheelsDRS.PortSpeed=regKey.ReadInteger("TWDRSPortSpeed");
				if(regKey.ValueExists("TWDRSMaxTerminalLines")) settings->TwoWheelsDRS.MaxTerminalLines=regKey.ReadInteger("TWDRSMaxTerminalLines");
				if(regKey.ValueExists("TWDRSUnitName")) settings->TwoWheelsDRS.UnitName=regKey.ReadString("TWDRSUnitName");
				if(regKey.ValueExists("TWDRSUnitFileName")) settings->TwoWheelsDRS.UnitFileName=regKey.ReadString("TWDRSUnitFileName");
				if(regKey.ValueExists("TWDRSRTSCTSEnable")) settings->TwoWheelsDRS.RTSCTSEnable=regKey.ReadBool("TWDRSRTSCTSEnable");
				if(regKey.ValueExists("TWDRSAntennaHeight")) settings->TwoWheelsDRS.AntennaHeight=regKey.ReadFloat("TWDRSAntennaHeight");
				if(regKey.ValueExists("TWDRSUseZGeoid")) settings->TwoWheelsDRS.UseZGeoid=regKey.ReadBool("TWDRSUseZGeoid");
				if(regKey.ValueExists("TWDRSLogToFile")) settings->TwoWheelsDRS.LogToFile=regKey.ReadBool("TWDRSLogToFile");
				if(regKey.ValueExists("TWDRSInitOnStart")) settings->TwoWheelsDRS.InitOnStart=regKey.ReadBool("TWDRSInitOnStart");
				if(regKey.ValueExists("TWDRSUseGpsForStart")) settings->TwoWheelsDRS.UseGpsForStart=regKey.ReadBool("TWDRSUseGpsForStart");
				if(regKey.ValueExists("WPSasWheel")) settings->WPSasWheel=regKey.ReadBool("WPSasWheel");

				//TwoWheelsDRS Simulator
				//if(regKey.ValueExists("TWDRSSimTieCoordinatesToMapCenter")) settings->TwoWheelsDRS.SimTieCoordinatesToMapCenter=regKey.ReadBool("TWDRSSimTieCoordinatesToMapCenter");
				if(regKey.ValueExists("TWDRSSimFileSource")) settings->TwoWheelsDRS.SimFileSource=regKey.ReadBool("TWDRSSimFileSource");
				if(regKey.ValueExists("TWDRSSimFileName")) settings->TwoWheelsDRS.SimFileName=regKey.ReadString("TWDRSSimFileName");

				//DXF
				if(regKey.ValueExists("DXFIncludeMap")) settings->DXFIncludeMap=regKey.ReadBool("DXFIncludeMap");
				if(regKey.ValueExists("DXFBackgroundColor")) settings->DXFBackgroundColor=regKey.ReadInteger("DXFBackgroundColor");
				if(regKey.ValueExists("DXFColorInvertThreshold")) settings->DXFColorInvertThreshold=regKey.ReadInteger("DXFColorInvertThreshold");
				if(regKey.ValueExists("DXFPathEndsWithArrow")) settings->DXFPathEndsWithArrow=regKey.ReadBool("DXFPathEndsWithArrow");
				if(regKey.ValueExists("DXFUsePitch")) settings->DXFUsePitch=regKey.ReadBool("DXFUsePitch");
				if(regKey.ValueExists("DXFUseRoll")) settings->DXFUseRoll=regKey.ReadBool("DXFUseRoll");
				if(regKey.ValueExists("DXFConfidence")) settings->DXFConfidence=regKey.ReadBool("DXFConfidence");
				if(regKey.ValueExists("DXFDescriptionAsText")) settings->DXFDescriptionAsText=regKey.ReadBool("DXFDescriptionAsText");
				if(regKey.ValueExists("DXFDescriptionTextHeight")) settings->DXFDescriptionTextHeight=regKey.ReadFloat("DXFDescriptionTextHeight");
				if(regKey.ValueExists("DXFDepthToDescription")) settings->DXFDepthToDescription=regKey.ReadBool("DXFDepthToDescription");
				if(regKey.ValueExists("DXFIdToDescription")) settings->DXFIdToDescription=regKey.ReadBool("DXFIdToDescription");
				if(regKey.ValueExists("DXFPolyline3D")) settings->DXFPolyline3D=regKey.ReadBool("DXFPolyline3D");
				if(regKey.ValueExists("DXFEnlargeText")) settings->DXFEnlargeText=regKey.ReadBool("DXFEnlargeText");

				// BingOnlineMap
				if(regKey.ValueExists("BingInetMapAppKey")) settings->BingInetMapAppKey=regKey.ReadString("BingInetMapAppKey");
				if(regKey.ValueExists("BingInetMapViewType")) settings->BingInetMapViewType=regKey.ReadString("BingInetMapViewType");
				if(regKey.ValueExists("BingInetMapDefaultZoom")) settings->BingInetMapDefaultZoom=regKey.ReadInteger("BingInetMapDefaultZoom");
				//if(regKey.ValueExists("BingInetMapMinZoom")) settings->BingInetMapMinZoom=regKey.ReadInteger("BingInetMapMinZoom");
				if(regKey.ValueExists("BingInetMapMaxZoom")) settings->BingInetMapMaxZoom=regKey.ReadInteger("BingInetMapMaxZoom");

				if (settings->BingInetMapMinZoom < 1) {
					settings->BingInetMapMinZoom = 1;
				}
				if (settings->BingInetMapMaxZoom > 21) {
					settings->BingInetMapMaxZoom = 21;
				}
				if ((settings->BingInetMapDefaultZoom < settings->BingInetMapMinZoom) ||
					(settings->BingInetMapDefaultZoom > settings->BingInetMapMaxZoom)) {

					settings->BingInetMapDefaultZoom = (settings->BingInetMapMinZoom + settings->BingInetMapMaxZoom) / 2;
				}

				//ImageMap
				if(regKey.ValueExists("SaveImageMapChanges")) settings->SaveImageMapChanges=regKey.ReadBool("SaveImageMapChanges");

				//XYZC
				if(regKey.ValueExists("XYZCCellWidth")) settings->XYZCCellWidth=regKey.ReadFloat("XYZCCellWidth");
				if(regKey.ValueExists("XYZCCellHeight")) settings->XYZCCellHeight=regKey.ReadFloat("XYZCCellHeight");
				if(regKey.ValueExists("XYZCUseZ")) settings->XYZCUseZ=regKey.ReadBool("XYZCUseZ");
				if(regKey.ValueExists("XYZCUsePitch")) settings->XYZCUsePitch=regKey.ReadBool("XYZCUsePitch");
				if(regKey.ValueExists("XYZCUseRoll")) settings->XYZCUseRoll=regKey.ReadBool("XYZCUseRoll");
				if(regKey.ValueExists("XYZCGained")) settings->XYZCGained=regKey.ReadBool("XYZCGained");
				if(regKey.ValueExists("XYZCAbsolute")) settings->XYZCAbsolute=regKey.ReadBool("XYZCAbsolute");
				if(regKey.ValueExists("XYZCEnvelope")) settings->XYZCEnvelope=regKey.ReadBool("XYZCEnvelope");

#ifdef _SPAR
				//SPAR
				if(regKey.ValueExists("SparEnable")) settings->SparEnable=regKey.ReadBool("SparEnable");
				if(regKey.ValueExists("SparPipeName")) settings->SparPipeName=regKey.ReadString("SparPipeName");
				if(regKey.ValueExists("SparShell")) settings->SparShell=regKey.ReadString("SparShell");
				if(regKey.ValueExists("SparSimulation")) settings->SparSimulation=regKey.ReadBool("SparSimulation");
				if(regKey.ValueExists("SparCOMPortNumber")) settings->SparCOMPortNumber=regKey.ReadInteger("SparCOMPortNumber");
				if(regKey.ValueExists("SparFrequency")) settings->SparFrequency=regKey.ReadFloat("SparFrequency");
				if(regKey.ValueExists("SparAverageType")) settings->SparAverageType=regKey.ReadInteger("SparAverageType");
				if(regKey.ValueExists("SparAverageTime")) settings->SparAverageTime=regKey.ReadInteger("SparAverageTime");
				if(regKey.ValueExists("SparUpdateRate")) settings->SparUpdateRate=regKey.ReadInteger("SparUpdateRate");
				if(regKey.ValueExists("SparElevation")) settings->SparElevation=regKey.ReadFloat("SparElevation");
				if(regKey.ValueExists("SparToAntennaFwd")) settings->SparToAntennaFwd=regKey.ReadFloat("SparToAntennaFwd");
				if(regKey.ValueExists("SparToAntennaRight")) settings->SparToAntennaRight=regKey.ReadFloat("SparToAntennaRight");
				if(regKey.ValueExists("SparToAntennaUp")) settings->SparToAntennaUp=regKey.ReadFloat("SparToAntennaUp");
				if(regKey.ValueExists("SparUseBaseline")) settings->SparUseBaseline=regKey.ReadBool("SparUseBaseline");
				if(regKey.ValueExists("SparConfidenceMax")) settings->SparConfidenceMax=regKey.ReadInteger("SparConfidenceMax");
				if(regKey.ValueExists("SparMinDbField")) settings->SparMinDbField=regKey.ReadInteger("SparMinDbField");
				if(regKey.ValueExists("SparGyroCompass")) settings->SparGyroCompass=regKey.ReadBool("SparGyroCompass");
				if(regKey.ValueExists("SparAnglesCompensation")) settings->SparAnglesCompensation=regKey.ReadBool("SparAnglesCompensation");
				if(regKey.ValueExists("SparPitchCompensation")) settings->SparPitchCompensation=regKey.ReadFloat("SparPitchCompensation");
				if(regKey.ValueExists("SparRollCompensation")) settings->SparRollCompensation=regKey.ReadFloat("SparRollCompensation");
				if(regKey.ValueExists("SparHeadingCompensation")) settings->SparHeadingCompensation=regKey.ReadFloat("SparHeadingCompensation");
#endif
#ifndef _AutoSaveXYZCRestriction
				//Automatic profiling
				if(regKey.ValueExists("AutoStopProfile")) settings->AutoStopProfile=regKey.ReadBool("AutoStopProfile");
				if(regKey.ValueExists("AutoStopProfileSize")) settings->AutoStopProfileSize=(TBatteryType)regKey.ReadInteger("AutoStopProfileSize");
				if(regKey.ValueExists("AutoSaveProfile")) settings->AutoSaveProfile=regKey.ReadBool("AutoSaveProfile");
				if(regKey.ValueExists("AutoCloseProfile")) settings->AutoCloseProfile=regKey.ReadBool("AutoCloseProfile");
				if(regKey.ValueExists("AutoSaveDir")) settings->AutoSaveDir=regKey.ReadString("AutoSaveDir");
				//Pipes
				//if(regKey.ValueExists("ProfilePipesDetector")) settings->ProfilePipesDetector=regKey.ReadBool("ProfilePipesDetector");
				//if(regKey.ValueExists("PipesDetectionHoughAccumMax")) settings->PipesDetectionHoughAccumMax=regKey.ReadInteger("PipesDetectionHoughAccumMax");
				/*if(regKey.ValueExists("PipesDetectionWinWidth")) settings->PipesDetectionWinWidth=regKey.ReadFloat("PipesDetectionWinWidth");
				if(regKey.ValueExists("PipesDetectionWinOverlap")) settings->PipesDetectionWinOverlap=regKey.ReadFloat("PipesDetectionWinOverlap");
				if(regKey.ValueExists("PipesDetectionWinResultsBorder")) settings->PipesDetectionWinResultsBorder=regKey.ReadFloat("PipesDetectionWinResultsBorder");
				if(regKey.ValueExists("PipesDetectionPermitFrom")) settings->PipesDetectionPermitFrom=regKey.ReadFloat("PipesDetectionPermitFrom");
				if(regKey.ValueExists("PipesDetectionPermitRange")) settings->PipesDetectionPermitRange=regKey.ReadFloat("PipesDetectionPermitRange");
				*/
#endif
				//Performance
				if(regKey.ValueExists("MapPreview")) settings->MapPreview=regKey.ReadBool("MapPreview");
				if(regKey.ValueExists("EasyRendering")) settings->EasyRendering=regKey.ReadBool("EasyRendering");
				if(regKey.ValueExists("GPSTracking")) settings->GPSTracking=regKey.ReadBool("GPSTracking");
				if(regKey.ValueExists("ShowGPSCoordinates")) settings->ShowGPSCoordinates=regKey.ReadBool("ShowGPSCoordinates");
				if(regKey.ValueExists("FollowMe")) settings->FollowMe=regKey.ReadBool("FollowMe");
				if(regKey.ValueExists("CommunicationLinesDetector")) settings->CommunicationLinesDetector=regKey.ReadBool("CommunicationLinesDetector");
				if(regKey.ValueExists("EasyZoomIn")) settings->EasyZoomIn=regKey.ReadBool("EasyZoomIn");
				if(regKey.ValueExists("ShowBattery")) settings->ShowBattery=regKey.ReadBool("ShowBattery");
				if(regKey.ValueExists("ShowRecycleBin")) settings->ShowRecycleBin=regKey.ReadBool("ShowRecycleBin");
				if(regKey.ValueExists("RecycleBinTAlign")) settings->RecycleBinTAlign=(TVisualToolObjectAlign)regKey.ReadInteger("RecycleBinTAlign");
				if(regKey.ValueExists("ShowGyroCompass")) settings->ShowGyroCompass=regKey.ReadBool("ShowGyroCompass");
				if(regKey.ValueExists("ShowWindRose")) settings->ShowWindRose=regKey.ReadBool("ShowWindRose");
				if(regKey.ValueExists("ShowPing")) settings->ShowPing=regKey.ReadBool("ShowPing");
				if(regKey.ValueExists("ShowClocks")) settings->ShowClocks=regKey.ReadBool("ShowClocks");
				if(regKey.ValueExists("ShowMemGraph")) settings->ShowMemGraph=regKey.ReadBool("ShowMemGraph");
				if(regKey.ValueExists("GyroCompassTAlign")) settings->GyroCompassTAlign=(TVisualToolObjectAlign)regKey.ReadInteger("GyroCompassTAlign");
				if(regKey.ValueExists("SmallSizeControls")) settings->SmallSizeControls=regKey.ReadBool("SmallSizeControls");
				if(regKey.ValueExists("Messages")) settings->Messages=regKey.ReadBool("Messages");
				if(regKey.ValueExists("MessagesAnimation")) settings->MessagesAnimation=regKey.ReadBool("MessagesAnimation");
				if(regKey.ValueExists("ShowLabelIcons")) settings->ShowLabelIcons=regKey.ReadBool("ShowLabelIcons");
				if(regKey.ValueExists("CopyAttachments")) settings->CopyAttachments=regKey.ReadBool("CopyAttachments");
				if(regKey.ValueExists("ShowLabelInformationOnPin")) settings->ShowLabelInformationOnPin=regKey.ReadBool("ShowLabelInformationOnPin");
				if(regKey.ValueExists("ShowLabelConfidence")) settings->ShowLabelConfidence=regKey.ReadBool("ShowLabelConfidence");
//				if(regKey.ValueExists("TraceTransparency")) settings->TraceTransparency=regKey.ReadInteger("TraceTransparency");
				if(regKey.ValueExists("ShowAnnotationLayerOnOpen")) settings->ShowAnnotationLayerOnOpen=regKey.ReadBool("ShowAnnotationLayerOnOpen");
				if(regKey.ValueExists("EmptyMapSize")) settings->EmptyMapSize=regKey.ReadInteger("EmptyMapSize");
				if(regKey.ValueExists("EnlargeEmptyMap")) settings->EnlargeEmptyMap=regKey.ReadBool("EnlargeEmptyMap");
				if(regKey.ValueExists("ShowMapLabelNotification")) settings->ShowMapLabelNotification=regKey.ReadBool("ShowMapLabelNotification");
				if(regKey.ValueExists("DrawPathName")) settings->DrawPathName=regKey.ReadBool("DrawPathName");
				if(regKey.ValueExists("ProfBackgroundColor")) settings->ProfBackgroundColor=regKey.ReadInteger("ProfBackgroundColor");
				//if(regKey.ValueExists("Gml3Dpoints")) settings->Gml3Dpoints=regKey.ReadBool("Gml3Dpoints");
				if(regKey.ValueExists("WindowState")) settings->WindowState=(TWindowState)regKey.ReadInteger("WindowState");
				if(regKey.ValueExists("WindowTop")) settings->WindowTop=regKey.ReadInteger("WindowTop");
				if(regKey.ValueExists("WindowLeft")) settings->WindowLeft=regKey.ReadInteger("WindowLeft");
				if(regKey.ValueExists("WindowWidth")) settings->WindowWidth=regKey.ReadInteger("WindowWidth");
				if(regKey.ValueExists("WindowHeight")) settings->WindowHeight=regKey.ReadInteger("WindowHeight");
				if(regKey.ValueExists("PanelsOrientation")) settings->PanelsOrientation=(TRadarPanelsOrientation)regKey.ReadInteger("PanelsOrientation");

				if(regKey.ValueExists("MapObjectsPrediction")) settings->MapObjectsPrediction=regKey.ReadBool("MapObjectsPrediction");
				if(regKey.ValueExists("MapObjectsPredictingRadius")) settings->MapObjectsPredictingRadius=regKey.ReadFloat("MapObjectsPredictingRadius");
				if(regKey.ValueExists("LabelDestructionTimeByClick")) settings->LabelDestructionTimeByClick=(unsigned long)regKey.ReadInteger("LabelDestructionTimeByClick");
				if(regKey.ValueExists("LabelDestructionTimeByTrack")) settings->LabelDestructionTimeByTrack=(unsigned long)regKey.ReadInteger("LabelDestructionTimeByTrack");
				if(regKey.ValueExists("MapObjectsPredictionSinglePerLayer")) settings->MapObjectsPredictionSinglePerLayer=regKey.ReadBool("MapObjectsPredictionSinglePerLayer");

				//MapLabelsGeometry
				if(regKey.ValueExists("MapLabelSpiderBeamsSector")) settings->MapLabelSpiderBeamsSector=regKey.ReadInteger("MapLabelSpiderBeamsSector");
				//if(regKey.ValueExists("SingleTypeMapLabelsGeometry")) settings->SingleTypeMapLabelsGeometry=regKey.ReadBool("SingleTypeMapLabelsGeometry");

				//SQLiteDXF
				//if(regKey.ValueExists("DbDXFAllowed")) settings->DbDXFAllowed=regKey.ReadBool("DbDXFAllowed");
				if(regKey.ValueExists("DbStoreLayersVisability")) settings->DbStoreLayersVisability=regKey.ReadBool("DbStoreLayersVisability");
				if(regKey.ValueExists("DbMergeLayersWithSameName")) settings->DbMergeLayersWithSameName=regKey.ReadBool("DbMergeLayersWithSameName");
				if(regKey.ValueExists("DbShowLayerFileName")) settings->DbShowLayerFileName=regKey.ReadBool("DbShowLayerFileName");

				//Constants
				if(regKey.ValueExists("MaximumLayerSizePX")) settings->MaximumLayerSizePX=regKey.ReadInteger("MaximumLayerSizePX");
				if(settings->MaximumLayerSizePX>3000) settings->MaximumLayerSizePX=3000;
				if(regKey.ValueExists("DoubleMaximumLayerSizePX")) settings->DoubleMaximumLayerSizePX=regKey.ReadBool("DoubleMaximumLayerSizePX");
				if(regKey.ValueExists("OpenMapType")) settings->OpenMapType=regKey.ReadInteger("OpenMapType");
				if(regKey.ValueExists("ReferencePointCoordinatesQ")) settings->ReferencePointCoordinatesQ=regKey.ReadInteger("ReferencePointCoordinatesQ");

				//Chris Howard
				if(regKey.ValueExists("ChrisMasterIni")) settings->ChrisMasterIni=regKey.ReadString("ChrisMasterIni");
				if(regKey.ValueExists("ChrisSlaveIni")) settings->ChrisSlaveIni=regKey.ReadString("ChrisSlaveIni");
				if(regKey.ValueExists("ChrisExe_AVX1")) settings->ChrisExe=regKey.ReadString("ChrisExe_AVX1");
				if(regKey.ValueExists("ChrisExe_AVX2")) settings->ChrisExe=regKey.ReadString("ChrisExe_AVX2");
				if(regKey.ValueExists("ChrisExe_SSE2")) settings->ChrisExe=regKey.ReadString("ChrisExe_SSE2");
				if(regKey.ValueExists("ChrisExe_Pent")) settings->ChrisExe=regKey.ReadString("ChrisExe_Pent");
				if(regKey.ValueExists("ChrisShowExeOutput")) settings->ChrisShowExeOutput=regKey.ReadBool("ChrisShowExeOutput");
				if(regKey.ValueExists("ChrisExeKeepAliving")) settings->ChrisExeKeepAliving=regKey.ReadBool("ChrisExeKeepAliving");
				if(regKey.ValueExists("ChrisExeTimeout")) settings->ChrisExeTimeout=regKey.ReadInteger("ChrisExeTimeout");
				settings->ChrisExe=settings->GetChrisExe();

				//Internal variables
				if(regKey.ValueExists("LastMapCenterX")) settings->LastMapCenter.X=regKey.ReadFloat("LastMapCenterX");
				if(regKey.ValueExists("LastMapCenterY")) settings->LastMapCenter.Y=regKey.ReadFloat("LastMapCenterY");
				if(regKey.ValueExists("LastMapCenterTemp")) *((int*)settings->LastMapCenter.Temp)=regKey.ReadInteger("LastMapCenterTemp");
				if(regKey.ValueExists("LastMapCS")) settings->LastMapCS=(TCoordinateSystem)regKey.ReadInteger("LastMapCS");
				if(regKey.ValueExists("LastMapZoom")) settings->LastMapZoom=regKey.ReadInteger("LastMapZoom");
			}
			Palette->LoadPaletteFromRegistry((AnsiString)((AnsiString)_RadarMapRegKey+"\\Palettes").c_str(), "DefaultPalette");
		}
		catch(Exception &e)
		{
			DefaultSettings();
		}
	}
	__finally
	{
		delete &regKey;
	}
}
//---------------------------------------------------------------------------

void __fastcall TRadarMapManager::SaveSettings()
{
	TRegistry& regKey=*new TRegistry();
	try
	{
		try
		{
			if(regKey.OpenKey(_RadarMapRegKey,true))
			{
				//Common parameters
				regKey.WriteInteger("RadarMapOutputsCounter", settings->RadarMapOutputsCounter);
				//GPR
				regKey.WriteString("GeoradarTCPAddr", settings->GeoradarTCPAddr);
				regKey.WriteInteger("WheelPositioning", (int)settings->WheelPositioning);
				regKey.WriteFloat("WheelTraceDistance", settings->WheelTraceDistance);
				regKey.WriteInteger("WheelPulses", settings->WheelPulses);
				regKey.WriteFloat("WheelDiameter", settings->WheelDiameter);
				regKey.WriteInteger("TracesInPixel", settings->TracesInPixel);
				regKey.WriteFloat("Permitivity", settings->Permitivity);
				regKey.WriteBool("ApplyConstantDeduct", settings->ApplyConstantDeduct);
				regKey.WriteInteger("BatteryType", (int)settings->BatteryType);
				regKey.WriteBool("ApplyGain", settings->ApplyGain);
				regKey.WriteBool("OneWheelForAllChannels", settings->OneWheelForAllChannels);
				//Automatic profiling
				regKey.WriteBool("AutoStopProfile", settings->AutoStopProfile);
				regKey.WriteInteger("AutoStopProfileSize", (int)settings->AutoStopProfileSize);
				regKey.WriteBool("AutoSaveProfile", settings->AutoSaveProfile);
				regKey.WriteBool("AutoCloseProfile", settings->AutoCloseProfile);
				regKey.WriteString("AutoSaveDir", settings->AutoSaveDir);
				//Filters
				regKey.WriteBool("WeightedSubtractionFilter", settings->WeightedSubtractionFilter);
				regKey.WriteInteger("WSWindowWidth", settings->WSWindowWidth);
				regKey.WriteBool("MoveoutCorrection", settings->MoveoutCorrection);
				//GPS
				regKey.WriteString("GpsComPort", settings->GPS.ComPort);
				regKey.WriteInteger("GpsPortSpeed", settings->GPS.PortSpeed);
				regKey.WriteInteger("GpsMaxTerminalLines", settings->GPS.MaxTerminalLines);
				//regKey.WriteInteger("GpsUnitType", settings->GPS.UnitType);
				regKey.WriteString("GpsUnitName", settings->GPS.UnitName);
				regKey.WriteString("GpsUnitFileName", settings->GPS.UnitFileName);
				regKey.WriteBool("GpsRTSCTSEnable", settings->GPS.RTSCTSEnable);
				regKey.WriteInteger("DefaultCoordinateSystem", (int)settings->DefaultCoordinateSystem);
				regKey.WriteFloat("GpsAntennaHeight", settings->GPS.AntennaHeight);
				regKey.WriteBool("GpsUseZGeoid", settings->GPS.UseZGeoid);
				regKey.WriteBool("GpsLogToFile", settings->GPS.LogToFile);
				regKey.WriteBool("GpsInitOnStart", settings->GPS.InitOnStart);
				regKey.WriteBool("GpsUseGpsForStart", settings->GPS.UseGpsForStart);
				regKey.WriteBool("UseGreenwichTime", settings->UseGreenwichTime);
				//GPS Simulator
				regKey.WriteBool("GpsSimTcpSource", settings->GPS.SimTcpSource);
				regKey.WriteBool("GpsSimTieCoordinatesToMapCenter", settings->GPS.SimTieCoordinatesToMapCenter);//false;//
				regKey.WriteString("GpsSimTcpAddr", settings->GPS.SimTcpAddr);
				regKey.WriteBool("GpsSimFileSource", settings->GPS.SimFileSource);
				regKey.WriteString("GpsSimFileName", settings->GPS.SimFileName);

				regKey.WriteFloat("MaxDeviceSpeed", settings->MaxDeviceSpeed);
				//TwoWheelsDRS
				regKey.WriteString("TWDRSComPort", settings->TwoWheelsDRS.ComPort);
				regKey.WriteInteger("TWDRSPortSpeed", settings->TwoWheelsDRS.PortSpeed);
				regKey.WriteInteger("TWDRSMaxTerminalLines", settings->TwoWheelsDRS.MaxTerminalLines);
				//regKey.WriteInteger("TWDRSUnitType", settings->TwoWheelsDRS.UnitType);
				regKey.WriteString("TWDRSUnitName", settings->TwoWheelsDRS.UnitName);
				regKey.WriteString("TWDRSUnitFileName", settings->TwoWheelsDRS.UnitFileName);
				regKey.WriteBool("TWDRSRTSCTSEnable", settings->TwoWheelsDRS.RTSCTSEnable);
				regKey.WriteFloat("TWDRSAntennaHeight", settings->TwoWheelsDRS.AntennaHeight);
				regKey.WriteBool("TWDRSUseZGeoid", settings->TwoWheelsDRS.UseZGeoid);
				regKey.WriteBool("TWDRSLogToFile", settings->TwoWheelsDRS.LogToFile);
				regKey.WriteBool("TWDRSInitOnStart", settings->TwoWheelsDRS.InitOnStart);
				regKey.WriteBool("TWDRSUseGpsForStart", settings->TwoWheelsDRS.UseGpsForStart);
				regKey.WriteBool("WPSasWheel", settings->WPSasWheel);
				//TwoWheelsDRS Simulator
				regKey.WriteBool("TWDRSSimTcpSource", settings->TwoWheelsDRS.SimTcpSource);
				regKey.WriteBool("TWDRSSimTieCoordinatesToMapCenter", settings->TwoWheelsDRS.SimTieCoordinatesToMapCenter);//false;//
				regKey.WriteString("TWDRSSimTcpAddr", settings->TwoWheelsDRS.SimTcpAddr);
				regKey.WriteBool("TWDRSSimFileSource", settings->TwoWheelsDRS.SimFileSource);
				regKey.WriteString("TWDRSSimFileName", settings->TwoWheelsDRS.SimFileName);
				//DXF
				regKey.WriteBool("DXFIncludeMap", settings->DXFIncludeMap);
				regKey.WriteInteger("DXFBackgroundColor", settings->DXFBackgroundColor);
				regKey.WriteInteger("DXFColorInvertThreshold", settings->DXFColorInvertThreshold);
				regKey.WriteBool("DXFPathEndsWithArrow", settings->DXFPathEndsWithArrow);
				regKey.WriteBool("DXFUsePitch", settings->DXFUsePitch);
				regKey.WriteBool("DXFUseRoll", settings->DXFUseRoll);
				regKey.WriteBool("DXFConfidence", settings->DXFConfidence);
				regKey.WriteBool("DXFDescriptionAsText", settings->DXFDescriptionAsText);
				regKey.WriteFloat("DXFDescriptionTextHeight", settings->DXFDescriptionTextHeight);
				regKey.WriteBool("DXFDepthToDescription", settings->DXFDepthToDescription);
				regKey.WriteBool("DXFIdToDescription", settings->DXFIdToDescription);
				regKey.WriteBool("DXFPolyline3D", settings->DXFPolyline3D);
				regKey.WriteBool("DXFEnlargeText", settings->DXFEnlargeText);
				//SQLiteDXF
//				regKey.WriteBool("DbDXFAllowed", settings->DbDXFAllowed);
				regKey.WriteBool("DbStoreLayersVisability", settings->DbStoreLayersVisability);
				regKey.WriteBool("DbMergeLayersWithSameName", settings->DbMergeLayersWithSameName);
				regKey.WriteBool("DbShowLayerFileName", settings->DbShowLayerFileName);
				// BingOnlineMap
				regKey.WriteString("BingInetMapAppKey", settings->BingInetMapAppKey);
				regKey.WriteString("BingInetMapViewType", settings->BingInetMapViewType);
				regKey.WriteInteger("BingInetMapDefaultZoom", settings->BingInetMapDefaultZoom);
				regKey.WriteInteger("BingInetMapMinZoom", settings->BingInetMapMinZoom);
				regKey.WriteInteger("BingInetMapMaxZoom", settings->BingInetMapMaxZoom);
				//ImageMap
				regKey.WriteBool("SaveImageMapChanges", settings->SaveImageMapChanges);
				//XYZC
				regKey.WriteFloat("XYZCCellWidth", settings->XYZCCellWidth);
				regKey.WriteFloat("XYZCCellHeight", settings->XYZCCellHeight);
				regKey.WriteBool("XYZCUseZ", settings->XYZCUseZ);
				regKey.WriteBool("XYZCUsePitch", settings->XYZCUsePitch);
				regKey.WriteBool("XYZCUseRoll", settings->XYZCUseRoll);
				regKey.WriteBool("XYZCGained", settings->XYZCGained);
				regKey.WriteBool("XYZCAbsolute", settings->XYZCAbsolute);
				regKey.WriteBool("XYZCEnvelope", settings->XYZCEnvelope);
				//SPAR
				regKey.WriteBool("SparEnable", settings->SparEnable);
				regKey.WriteString("SparPipeName", settings->SparPipeName);
				regKey.WriteString("SparShell", settings->SparShell);
				regKey.WriteBool("SparSimulation", settings->SparSimulation);
				regKey.WriteInteger("SparCOMPortNumber", settings->SparCOMPortNumber);
				regKey.WriteFloat("SparFrequency", settings->SparFrequency);
				regKey.WriteInteger("SparAverageType", settings->SparAverageType);
				regKey.WriteInteger("SparAverageTime", settings->SparAverageTime);
				regKey.WriteInteger("SparUpdateRate", settings->SparUpdateRate);
				regKey.WriteFloat("SparElevation", settings->SparElevation);
				regKey.WriteFloat("SparToAntennaFwd", settings->SparToAntennaFwd);
				regKey.WriteFloat("SparToAntennaRight", settings->SparToAntennaRight);
				regKey.WriteFloat("SparToAntennaUp", settings->SparToAntennaUp);
				regKey.WriteBool("SparUseBaseline", settings->SparUseBaseline);
				regKey.WriteInteger("SparConfidenceMax", settings->SparConfidenceMax);
				regKey.WriteInteger("SparMinDbField", settings->SparMinDbField);
				regKey.WriteBool("SparGyroCompass", settings->SparGyroCompass);
				regKey.WriteBool("SparAnglesCompensation", settings->SparAnglesCompensation);
				regKey.WriteFloat("SparPitchCompensation", settings->SparPitchCompensation);
				regKey.WriteFloat("SparRollCompensation", settings->SparRollCompensation);
				regKey.WriteFloat("SparHeadingCompensation", settings->SparHeadingCompensation);
				//PipesDetection
				regKey.WriteBool("ProfilePipesDetector", settings->ProfilePipesDetector);
				regKey.WriteInteger("PipesDetectionHoughAccumMax", settings->PipesDetectionHoughAccumMax);
				regKey.WriteFloat("PipesDetectionWinWidth", settings->PipesDetectionWinWidth);
				regKey.WriteFloat("PipesDetectionWinOverlap", settings->PipesDetectionWinOverlap);
				regKey.WriteFloat("PipesDetectionWinResultsBorder", settings->PipesDetectionWinResultsBorder);
				regKey.WriteFloat("PipesDetectionPermitFrom", settings->PipesDetectionPermitFrom);
				regKey.WriteFloat("PipesDetectionPermitRange", settings->PipesDetectionPermitRange);
				//Performance
				regKey.WriteBool("MapPreview", settings->MapPreview);
				regKey.WriteBool("EasyRendering", settings->EasyRendering);
				regKey.WriteBool("GPSTracking", settings->GPSTracking);
				regKey.WriteBool("ShowGPSCoordinates", settings->ShowGPSCoordinates);
				regKey.WriteBool("FollowMe", settings->FollowMe);
				regKey.WriteBool("CommunicationLinesDetector", settings->CommunicationLinesDetector);
				regKey.WriteBool("EasyZoomIn", settings->EasyZoomIn);
				regKey.WriteBool("ShowBattery", settings->ShowBattery);
				regKey.WriteBool("ShowRecycleBin", settings->ShowRecycleBin);
				regKey.WriteInteger("RecycleBinTAlign", (int)settings->RecycleBinTAlign);
				regKey.WriteBool("ShowGyroCompass", settings->ShowGyroCompass);
				regKey.WriteBool("ShowWindRose", settings->ShowWindRose);
				regKey.WriteBool("ShowClocks", settings->ShowClocks);
				regKey.WriteBool("ShowMemGraph", settings->ShowMemGraph);
				regKey.WriteBool("ShowPing", settings->ShowPing);
				regKey.WriteInteger("GyroCompassTAlign", (int)settings->GyroCompassTAlign);
				regKey.WriteBool("SmallSizeControls", settings->SmallSizeControls);
				regKey.WriteBool("Messages", settings->Messages);
				regKey.WriteBool("MessagesAnimation", settings->MessagesAnimation);
				regKey.WriteBool("ShowLabelIcons", settings->ShowLabelIcons);
				regKey.WriteBool("CopyAttachments", settings->CopyAttachments);
				regKey.WriteBool("ShowLabelInformationOnPin", settings->ShowLabelInformationOnPin);
				regKey.WriteBool("ShowLabelConfidence", settings->ShowLabelConfidence);
				regKey.WriteInteger("TraceTransparency", (int)settings->TraceTransparency);
				regKey.WriteBool("ShowAnnotationLayerOnOpen", settings->ShowAnnotationLayerOnOpen);
				regKey.WriteInteger("EmptyMapSize", settings->EmptyMapSize);
				regKey.WriteBool("EnlargeEmptyMap", settings->EnlargeEmptyMap);
				regKey.WriteBool("ShowMapLabelNotification", settings->ShowMapLabelNotification);
				regKey.WriteBool("DrawPathName", settings->DrawPathName);
				regKey.WriteInteger("ProfBackgroundColor", (int)settings->ProfBackgroundColor);
				regKey.WriteBool("Gml3Dpoints", settings->Gml3Dpoints);
				regKey.WriteInteger("WindowState", (int)settings->WindowState);
				regKey.WriteInteger("WindowTop", (int)settings->WindowTop);
				regKey.WriteInteger("WindowLeft", (int)settings->WindowLeft);
				regKey.WriteInteger("WindowWidth", (int)settings->WindowWidth);
				regKey.WriteInteger("WindowHeight", (int)settings->WindowHeight);
				regKey.WriteInteger("PanelsOrientation", (int)settings->PanelsOrientation);

				regKey.WriteBool("MapObjectsPrediction", settings->MapObjectsPrediction);
				regKey.WriteFloat("MapObjectsPredictingRadius", settings->MapObjectsPredictingRadius);
				regKey.WriteInteger("LabelDestructionTimeByClick", (int)settings->LabelDestructionTimeByClick);
				regKey.WriteInteger("LabelDestructionTimeByTrack", (int)settings->LabelDestructionTimeByTrack);
				regKey.WriteBool("MapObjectsPredictionSinglePerLayer", settings->MapObjectsPredictionSinglePerLayer);
				//MapLabelsGeometry
				regKey.WriteInteger("MapLabelSpiderBeamsSector", settings->MapLabelSpiderBeamsSector);
				regKey.WriteBool("SingleTypeMapLabelsGeometry", settings->SingleTypeMapLabelsGeometry);
				//Constants
				regKey.WriteInteger("MaximumLayerSizePX", settings->MaximumLayerSizePX);
				regKey.WriteBool("DoubleMaximumLayerSizePX", settings->DoubleMaximumLayerSizePX);
				regKey.WriteInteger("OpenMapType", settings->OpenMapType);
				regKey.WriteInteger("ReferencePointCoordinatesQ", settings->ReferencePointCoordinatesQ);
				//Chris Howard
				regKey.WriteString("ChrisMasterIni", settings->ChrisMasterIni);
				regKey.WriteString("ChrisSlaveIni", settings->ChrisSlaveIni);
				regKey.WriteString("ChrisExe_AVX1", settings->ChrisExe_AVX1);
				regKey.WriteString("ChrisExe_AVX2", settings->ChrisExe_AVX2);
				regKey.WriteString("ChrisExe_SSE2", settings->ChrisExe_SSE2);
				regKey.WriteString("ChrisExe_Pent", settings->ChrisExe_Pent);
				regKey.WriteBool("ChrisShowExeOutput", settings->ChrisShowExeOutput);
				regKey.WriteBool("ChrisExeKeepAliving", settings->ChrisExeKeepAliving);
				regKey.WriteInteger("ChrisExeTimeout", settings->ChrisExeTimeout);
				regKey.WriteString("ChrisExe", settings->ChrisExe);
				//Internal variables
				regKey.WriteFloat("LastMapCenterX", settings->LastMapCenter.X);
				regKey.WriteFloat("LastMapCenterY", settings->LastMapCenter.Y);
				regKey.WriteInteger("LastMapCenterTemp", *((int*)settings->LastMapCenter.Temp));
				regKey.WriteInteger("LastMapCS", (int)settings->LastMapCS);
				regKey.WriteInteger("LastMapZoom", (int)settings->LastMapZoom);
			}
			Palette->SavePaletteToRegistry((AnsiString)((AnsiString)_RadarMapRegKey+"\\Palettes").c_str(), "DefaultPalette");
		}
		catch(Exception &e)	{}
	}
	__finally
	{
		delete &regKey;
	}
}
//---------------------------------------------------------------------------

void __fastcall TRadarMapManager::RenewMapTrackingPath()
{
	if(trackingpath)
	{
		if(mapobjects) mapobjects->Delete(trackingpath->ObjectType, trackingpath->Index);
		else delete trackingpath;
		trackingpath=NULL;
	}
	if(settings->GPSTracking && mapobjects)
		trackingpath=new TMapTrackPathObject(mapobjects, mapobjects->Container->LatLonRect, (TObject*)this);
}
//---------------------------------------------------------------------------

void __fastcall TRadarMapManager::ApplyNewSettings(TRadarMapSettings newSettings)
{
	if(connection)
	{
		int res;

		res=Application->MessageBox(L"Some settings changing while GPR data acquisition is in progress could cause of data loss! Do you want to continue (Yes) or rollback to previous settings(No)?",
			((UnicodeString)_RadarMapName).w_str(), MB_ICONWARNING | MB_YESNO);
		if(res==ID_NO) return;
	}
	try
	{
		//GPR
		settings->GeoradarTCPAddr=newSettings.GeoradarTCPAddr;
		settings->WheelTraceDistance=newSettings.WheelTraceDistance;
		settings->WheelPulses=newSettings.WheelPulses;
		settings->WheelDiameter=newSettings.WheelDiameter;
		settings->TracesInPixel=newSettings.TracesInPixel;
		settings->Permitivity=newSettings.Permitivity;
		settings->ApplyConstantDeduct=newSettings.ApplyConstantDeduct;
		settings->ApplyGain=newSettings.ApplyGain;
		settings->OneWheelForAllChannels=newSettings.OneWheelForAllChannels;
		if(CurrentSettings)
			for(int i=0; i<CurrentSettings->ChannelQuantity; i++)
			{
				if(CurrentSettings->Channels[i]->Gain)
					CurrentSettings->Channels[i]->Gain->ApplyGain=newSettings.ApplyGain;
			}
		if(newSettings.GPSTracking && !trackingpath && mapobjects)
			trackingpath=new TMapTrackPathObject(mapobjects, mapobjects->Container->LatLonRect, (TObject*)this);
		else if(!newSettings.GPSTracking && trackingpath)
		{
			if(mapobjects) mapobjects->Delete(trackingpath->ObjectType, trackingpath->Index);
			else delete trackingpath;
			trackingpath=NULL;
		}
		settings->GPSTracking=newSettings.GPSTracking;
		settings->ShowGPSCoordinates=newSettings.ShowGPSCoordinates;
		settings->BatteryType=newSettings.BatteryType;
		if(battery) battery->BatteryType=newSettings.BatteryType;
        //Automatic profiling
		settings->AutoStopProfile=newSettings.AutoStopProfile;
		settings->AutoStopProfileSize=newSettings.AutoStopProfileSize;
		settings->AutoSaveProfile=newSettings.AutoSaveProfile;
		settings->AutoCloseProfile=newSettings.AutoCloseProfile;
		settings->AutoSaveDir=newSettings.AutoSaveDir;
		//Filters
		settings->WeightedSubtractionFilter=newSettings.WeightedSubtractionFilter;
		settings->WSWindowWidth=newSettings.WSWindowWidth;
		settings->MoveoutCorrection=newSettings.MoveoutCorrection;


		int Changed;

		//GPS
		Changed=(settings->GPS.ComPort!=newSettings.GPS.ComPort);
		settings->GPS.ComPort=newSettings.GPS.ComPort;
		Changed+=(settings->GPS.PortSpeed!=newSettings.GPS.PortSpeed);
		settings->GPS.PortSpeed=newSettings.GPS.PortSpeed;
		//Changed+=(settings->GPS.UnitType!=newSettings.GPS.UnitType);
		Changed+=(settings->GPS.UnitName!=newSettings.GPS.UnitName);
		//settings->GPS.UnitType=newSettings.GPS.UnitType;
		settings->GPS.UnitName=newSettings.GPS.UnitName;
		settings->GPS.UnitFileName=newSettings.GPS.UnitFileName;
		settings->GPS.MaxTerminalLines=newSettings.GPS.MaxTerminalLines;
		Changed+=(settings->GPS.SimTcpSource!=newSettings.GPS.SimTcpSource);
		settings->GPS.SimTcpSource=newSettings.GPS.SimTcpSource;
		settings->GPS.UseZGeoid=newSettings.GPS.UseZGeoid;
		settings->GPS.UseGpsForStart=newSettings.GPS.UseGpsForStart;
		Changed+=(settings->GPS.UseGpsForStart!=newSettings.GPS.UseGpsForStart);
		if(gpsclient && gpsclient->Terminal)
		{
			gpsclient->Terminal->LogToFile=newSettings.GPS.LogToFile;
			settings->GPS.LogToFile=gpsclient->Terminal->LogToFile;
		}
		else settings->GPS.LogToFile=newSettings.GPS.LogToFile;
		settings->GPS.InitOnStart=newSettings.GPS.InitOnStart;
		Changed+=(settings->GPS.InitOnStart!=newSettings.GPS.InitOnStart);
		settings->UseGreenwichTime=newSettings.UseGreenwichTime;
		if(settings->GPS.SimTieCoordinatesToMapCenter!=newSettings.GPS.SimTieCoordinatesToMapCenter)
		{
			settings->GPS.SimTieCoordinatesToMapCenter=newSettings.GPS.SimTieCoordinatesToMapCenter;
			if(gpsunit) gpsunit->ReCenter();
		}
		//settings->GPS.SimTcpAddr=newSettings.GPS.SimTcpAddr;
		Changed+=(settings->GPS.SimFileSource!=newSettings.GPS.SimFileSource);
		settings->GPS.SimFileSource=newSettings.GPS.SimFileSource;
		Changed+=(settings->GPS.RTSCTSEnable!=newSettings.GPS.RTSCTSEnable);
		settings->GPS.RTSCTSEnable=newSettings.GPS.RTSCTSEnable;
		settings->GPS.AntennaHeight=newSettings.GPS.AntennaHeight;
		//settings->GPS.SimFileName=newSettings.GPS.SimFileName;
		if(Changed>0)
		{
			gpsclient->Reconnect();

			TSliderToolButton* button=gpsunit->Button;
			delete gpsunit;
			gpsunit=new TGpsUnit(gpsclient, gpslisteners, this);
			if(twowheelclient) gpsunit->AddNmeaClient(twowheelclient);
	#ifdef DutchRDNAPTRANS2008
			/*GRID_FILE_DX=PrismDirectory+"\\x2c.grd";
			GRID_FILE_DY=PrismDirectory+"\\y2c.grd";
			GRID_FILE_GEOID=PrismDirectory+"\\nlgeo04.grd";*/
			RdNapTrans2008::SetDirectory(PrismDirectory+"\\");
	#endif
			gpsunit->Initialize();
			if(FileExists(settings->GPS.UnitFileName))
				gpsclient->LoadCommandsFromFile(settings->GPS.UnitFileName);
			gpsunit->Button=button;

		}
		settings->MaxDeviceSpeed=newSettings.MaxDeviceSpeed;

		Changed=(settings->WheelPositioning!=newSettings.WheelPositioning);
		settings->WheelPositioning=newSettings.WheelPositioning;

		//TwoWheelsDRS
		Changed+=(settings->TwoWheelsDRS.ComPort!=newSettings.TwoWheelsDRS.ComPort);
		settings->TwoWheelsDRS.ComPort=newSettings.TwoWheelsDRS.ComPort;
		Changed+=(settings->TwoWheelsDRS.PortSpeed!=newSettings.TwoWheelsDRS.PortSpeed);
		settings->TwoWheelsDRS.PortSpeed=newSettings.TwoWheelsDRS.PortSpeed;
		//Changed+=(settings->TwoWheelsDRS.UnitType!=newSettings.TwoWheelsDRS.UnitType);
		Changed+=(settings->TwoWheelsDRS.UnitName!=newSettings.TwoWheelsDRS.UnitName);
		//settings->TwoWheelsDRS.UnitType=newSettings.TwoWheelsDRS.UnitType;
		settings->TwoWheelsDRS.UnitName=newSettings.TwoWheelsDRS.UnitName;
		settings->TwoWheelsDRS.UnitFileName=newSettings.TwoWheelsDRS.UnitFileName;
		settings->TwoWheelsDRS.MaxTerminalLines=newSettings.TwoWheelsDRS.MaxTerminalLines;
		Changed+=(settings->TwoWheelsDRS.SimTcpSource!=newSettings.TwoWheelsDRS.SimTcpSource);
		settings->TwoWheelsDRS.SimTcpSource=newSettings.TwoWheelsDRS.SimTcpSource;
		settings->TwoWheelsDRS.UseZGeoid=newSettings.TwoWheelsDRS.UseZGeoid;
		Changed+=(settings->TwoWheelsDRS.UseGpsForStart!=newSettings.TwoWheelsDRS.UseGpsForStart);
		settings->TwoWheelsDRS.UseGpsForStart=newSettings.TwoWheelsDRS.UseGpsForStart;
		Changed+=(settings->WPSasWheel!=newSettings.WPSasWheel);
		settings->WPSasWheel=newSettings.WPSasWheel;
		if(twowheelclient && twowheelclient->Terminal)
		{
			twowheelclient->Terminal->LogToFile=newSettings.TwoWheelsDRS.LogToFile;
			settings->TwoWheelsDRS.LogToFile=twowheelclient->Terminal->LogToFile;
		}
		else settings->TwoWheelsDRS.LogToFile=newSettings.TwoWheelsDRS.LogToFile;
		settings->TwoWheelsDRS.InitOnStart=newSettings.TwoWheelsDRS.InitOnStart;
		Changed+=(settings->TwoWheelsDRS.InitOnStart!=newSettings.TwoWheelsDRS.InitOnStart);
		settings->UseGreenwichTime=newSettings.UseGreenwichTime;
		//if(settings->TwoWheelsDRS.SimTieCoordinatesToMapCenter!=newSettings.TwoWheelsDRS.SimTieCoordinatesToMapCenter)
		//	settings->TwoWheelsDRS.SimTieCoordinatesToMapCenter=newSettings.TwoWheelsDRS.SimTieCoordinatesToMapCenter;
		//settings->TwoWheelsDRS.SimTcpAddr=newSettings.TwoWheelsDRS.SimTcpAddr;
		//Changed+=(settings->TwoWheelsDRS.SimFileSource!=newSettings.TwoWheelsDRS.SimFileSource);
		//settings->TwoWheelsDRS.SimFileSource=newSettings.TwoWheelsDRS.SimFileSource;
		Changed+=(settings->TwoWheelsDRS.RTSCTSEnable!=newSettings.TwoWheelsDRS.RTSCTSEnable);
		settings->TwoWheelsDRS.RTSCTSEnable=newSettings.TwoWheelsDRS.RTSCTSEnable;
		settings->TwoWheelsDRS.AntennaHeight=newSettings.TwoWheelsDRS.AntennaHeight;
		//settings->TwoWheelsDRS.SimFileName=newSettings.TwoWheelsDRS.SimFileName;
		if(Changed>0)
		{
			if(settings->WheelPositioning==TPositioning::TwoWheel)
			{
				if(twowheelclient) twowheelclient->Reconnect();
				else
				{
					twowheelclient=new TTwoWheelNmeaClient(this, &Settings->TwoWheelsDRS);
					gpsunit->AddNmeaClient(twowheelclient);
				}
				if(FileExists(settings->TwoWheelsDRS.UnitFileName))
					twowheelclient->LoadCommandsFromFile(settings->TwoWheelsDRS.UnitFileName);
			}
			else if(twowheelclient)
			{
				if(gpsunit) gpsunit->DeleteNmeaClient(twowheelclient);
				delete twowheelclient;
				twowheelclient=NULL;
			}
		}/**/
		if(Changed) gpsunit->Initialize();
		Changed=(settings->SparEnable!=newSettings.SparEnable);
		settings->SparEnable=newSettings.SparEnable;
		Changed+=(settings->SparPipeName!=newSettings.SparPipeName);
		settings->SparPipeName=newSettings.SparPipeName;
		Changed+=(settings->SparShell!=newSettings.SparShell);
		settings->SparShell=newSettings.SparShell;
		Changed+=(settings->SparSimulation!=newSettings.SparSimulation);
		settings->SparSimulation=newSettings.SparSimulation;
		Changed+=(settings->SparCOMPortNumber!=newSettings.SparCOMPortNumber);
		settings->SparCOMPortNumber=newSettings.SparCOMPortNumber;
		Changed+=(settings->SparFrequency!=newSettings.SparFrequency);
		settings->SparFrequency=newSettings.SparFrequency;
		Changed+=(settings->SparAverageType!=newSettings.SparAverageType);
		settings->SparAverageType=newSettings.SparAverageType;
		Changed+=(settings->SparAverageTime!=newSettings.SparAverageTime);
		settings->SparAverageTime=newSettings.SparAverageTime;
		Changed+=(settings->SparUpdateRate!=newSettings.SparUpdateRate);
		settings->SparUpdateRate=newSettings.SparUpdateRate;
		Changed+=(settings->SparElevation!=newSettings.SparElevation);
		settings->SparElevation=newSettings.SparElevation;
		Changed+=(settings->SparToAntennaFwd!=newSettings.SparToAntennaFwd);
		settings->SparToAntennaFwd=newSettings.SparToAntennaFwd;
		Changed+=(settings->SparToAntennaRight!=newSettings.SparToAntennaRight);
		settings->SparToAntennaRight=newSettings.SparToAntennaRight;
		Changed+=(settings->SparToAntennaUp!=newSettings.SparToAntennaUp);
		settings->SparToAntennaUp=newSettings.SparToAntennaUp;
		Changed+=(settings->SparUseBaseline!=newSettings.SparUseBaseline);
		settings->SparUseBaseline=newSettings.SparUseBaseline;
		settings->SparAnglesCompensation=newSettings.SparAnglesCompensation;
		settings->SparPitchCompensation=newSettings.SparPitchCompensation;
		settings->SparRollCompensation=newSettings.SparRollCompensation;
		settings->SparHeadingCompensation=newSettings.SparHeadingCompensation;
		if(Changed>0)
		{
			if(sparunit) delete sparunit;
			if(mapobjects)
			{
				if(sparonmap) mapobjects->Delete(sparonmap->ObjectType, sparonmap->Index);
			}
			else
			{
				if(sparonmap) delete sparonmap;
			}
			if(settings->SparEnable)
			{
				sparunit=new TSparUnit(this, settings->SparShell);
				sparunit->Connect(settings->SparPipeName);
				if(mapobjects) sparonmap=new TMapSparObject(mapobjects, mapobjects->Container->LatLonRect, (TObject*)this);
				else sparonmap=NULL;
			}
			else
			{
				sparunit=NULL;
				sparonmap=NULL;
			}
		}
		settings->SparConfidenceMax=newSettings.SparConfidenceMax;
		settings->SparMinDbField=newSettings.SparMinDbField;
		settings->SparGyroCompass=newSettings.SparGyroCompass;
		if(mapobjects && gyrocompass) mapobjects->Delete(gyrocompass->ObjectType, gyrocompass->Index);
		else if(gyrocompass) delete gyrocompass;
		if(settings->SparGyroCompass && mapobjects && mapobjects->Image)
		{
			gyrocompass=new TGyroCompassObject(mapobjects, mapobjects->Image, (TObject*)this);
			gyrocompass->EasyRendering=newSettings.EasyRendering;
			gyrocompass->ShowGlass=false;
		}
		else gyrocompass=NULL;
		settings->ShowWindRose=newSettings.ShowWindRose;
		settings->ShowClocks=newSettings.ShowClocks;
		settings->ShowPing=newSettings.ShowPing;
		settings->ShowMemGraph=newSettings.ShowMemGraph;

		//DXF
		settings->DXFIncludeMap=newSettings.DXFIncludeMap;
		settings->DXFBackgroundColor=newSettings.DXFBackgroundColor;
		settings->DXFColorInvertThreshold=newSettings.DXFColorInvertThreshold;
		settings->DXFPathEndsWithArrow=newSettings.DXFPathEndsWithArrow;
		settings->DXFUsePitch=newSettings.DXFUsePitch;
		settings->DXFUseRoll=newSettings.DXFUseRoll;
		settings->DXFConfidence=newSettings.DXFConfidence;
		settings->DXFDescriptionAsText=newSettings.DXFDescriptionAsText;
		settings->DXFDescriptionTextHeight=newSettings.DXFDescriptionTextHeight;
		settings->DXFDepthToDescription=newSettings.DXFDepthToDescription;
		settings->DXFIdToDescription=newSettings.DXFIdToDescription;
		settings->DXFPolyline3D=newSettings.DXFPolyline3D;
		settings->DXFEnlargeText=newSettings.DXFEnlargeText;

		//SQLiteDXF
//		settings->DbDXFAllowed=newSettings.DbDXFAllowed;
		settings->DbStoreLayersVisability=newSettings.DbStoreLayersVisability;
		settings->DbMergeLayersWithSameName=newSettings.DbMergeLayersWithSameName;
		settings->DbShowLayerFileName=newSettings.DbShowLayerFileName;

		// BingOnlineMap
		settings->BingInetMapAppKey=newSettings.BingInetMapAppKey;
		settings->BingInetMapViewType=newSettings.BingInetMapViewType;
		settings->BingInetMapDefaultZoom=newSettings.BingInetMapDefaultZoom;
		settings->BingInetMapMinZoom=newSettings.BingInetMapMinZoom;
		settings->BingInetMapMaxZoom=newSettings.BingInetMapMaxZoom;

		if(mapobjects && mapobjects->Container && mapobjects->Container->Count>0)
		{
			TMapsContainer *mc;

			for(int i=0; i<mapobjects->Container->Count; i++)
			{
				mc=mapobjects->Container->GetObject(i);
				if(mc && mc->MapType==mtBingInet)
				{
					((TBingInetMap*)mc)->AppKey=settings->BingInetMapAppKey;
					((TBingInetMap*)mc)->MapViewType=TInetMapBase::SettingsMapViewTypeToEnum(settings->BingInetMapViewType);
					((TBingInetMap*)mc)->MapImageZoom=settings->BingInetMapDefaultZoom;
					((TBingInetMap*)mc)->MapRequestMinZoom=settings->BingInetMapMinZoom;
					((TBingInetMap*)mc)->MapRequestMaxZoom=settings->BingInetMapMaxZoom;
				}
			}
		}
		//ImageMap
		settings->SaveImageMapChanges=newSettings.SaveImageMapChanges;

		//XYZC
		settings->XYZCCellWidth=newSettings.XYZCCellWidth;
		settings->XYZCCellHeight=newSettings.XYZCCellWidth;
		settings->XYZCUseZ=newSettings.XYZCUseZ;
		settings->XYZCUsePitch=newSettings.XYZCUsePitch;
		settings->XYZCUseRoll=newSettings.XYZCUseRoll;
		settings->XYZCGained=newSettings.XYZCGained;
		settings->XYZCAbsolute=newSettings.XYZCAbsolute;
		settings->XYZCEnvelope=newSettings.XYZCEnvelope;

		//Performance
		settings->MapPreview=newSettings.MapPreview;
		settings->EasyRendering=newSettings.EasyRendering;
		settings->EasyZoomIn=newSettings.EasyZoomIn;

		//TMouseAction ma=tmaHand;
		//mapobjects->MouseAction=&ma;

		if(mapobjects->DisableMouseAction) (mapobjects->DisableMouseAction)();
		if(mapobjects->MouseAction) *mapobjects->MouseAction=tmaHand;

		if(trackingpath && trackingpath->FollowMe) trackingpath->FollowMe=newSettings.FollowMe;
		settings->FollowMe=newSettings.FollowMe;

		if(newSettings.CommunicationLinesDetector && !linesdetector && mapobjects)
			linesdetector=new TLinesDetector(mapobjects->Image, mapobjects->Container->UtilityPtr, mapobjects->Container->LatLonRect, (TObject*)this);
		else if(!newSettings.CommunicationLinesDetector && linesdetector)
		{
			delete linesdetector;
			linesdetector=NULL;
		}
		settings->CommunicationLinesDetector=newSettings.CommunicationLinesDetector;
		if(SatellitesList)
		{
			for(int i=0; i<SatellitesList->Count; i++)
			{
				if(SatellitesList->Items[i])
				{
					if(newSettings.ProfilePipesDetector) SatellitesList->Items[i]->StartPipesDetector();
					else SatellitesList->Items[i]->StopPipesDetector();
				}
			}
		}
		settings->ProfilePipesDetector=newSettings.ProfilePipesDetector;
		if(battery)
		{
			battery->Visible=newSettings.ShowBattery;
			laptopbattery->Visible=newSettings.ShowBattery;
		}
		settings->ShowBattery=newSettings.ShowBattery;
		if(recyclebin) recyclebin->Visible=newSettings.ShowRecycleBin;
		settings->ShowRecycleBin=newSettings.ShowRecycleBin;
		if(gyrocompass) gyrocompass->Visible=newSettings.ShowGyroCompass;
		settings->RecycleBinTAlign=newSettings.RecycleBinTAlign;
		settings->ShowGyroCompass=newSettings.ShowGyroCompass;
		settings->GyroCompassTAlign=newSettings.GyroCompassTAlign;
		settings->SmallSizeControls=newSettings.SmallSizeControls;
		if(objectscontainer) objectscontainer->SmallSize=newSettings.SmallSizeControls;
		if(mapobjects) mapobjects->SmallSize=newSettings.SmallSizeControls;
		settings->CopyAttachments=newSettings.CopyAttachments;
		settings->ShowLabelIcons=newSettings.ShowLabelIcons;
		settings->ShowLabelInformationOnPin=newSettings.ShowLabelInformationOnPin;
		settings->ShowLabelConfidence=newSettings.ShowLabelConfidence;
		settings->TraceTransparency=newSettings.TraceTransparency;
		settings->ShowAnnotationLayerOnOpen=newSettings.ShowAnnotationLayerOnOpen;
		settings->EmptyMapSize=newSettings.EmptyMapSize;
		settings->EnlargeEmptyMap=newSettings.EnlargeEmptyMap;
		settings->ShowMapLabelNotification=newSettings.ShowMapLabelNotification;
		settings->DrawPathName=newSettings.DrawPathName;
		settings->ProfBackgroundColor=newSettings.ProfBackgroundColor;
		settings->Gml3Dpoints=newSettings.Gml3Dpoints;

		Changed=(settings->WindowState!=newSettings.WindowState);
		settings->WindowState=newSettings.WindowState;
		settings->WindowTop=newSettings.WindowTop;
		settings->WindowLeft=newSettings.WindowLeft;
		settings->WindowWidth=newSettings.WindowWidth;
		settings->WindowHeight=newSettings.WindowHeight;
		if(Changed) RadarMapForm->ReBorder(true);
		Changed=(settings->PanelsOrientation!=newSettings.PanelsOrientation);
		settings->PanelsOrientation=newSettings.PanelsOrientation;
		if(Changed) RadarMapForm->ReOrient();

		settings->MapObjectsPrediction=newSettings.MapObjectsPrediction;
		settings->MapObjectsPredictingRadius=newSettings.MapObjectsPredictingRadius;
		settings->LabelDestructionTimeByClick=newSettings.LabelDestructionTimeByClick;
		settings->LabelDestructionTimeByTrack=newSettings.LabelDestructionTimeByTrack;
		settings->MapObjectsPredictionSinglePerLayer=newSettings.MapObjectsPredictionSinglePerLayer;
		//MapLabelsGeometry
		settings->MapLabelSpiderBeamsSector=newSettings.MapLabelSpiderBeamsSector;
		settings->SingleTypeMapLabelsGeometry=newSettings.SingleTypeMapLabelsGeometry;
		//Constants
		settings->MaximumLayerSizePX=newSettings.MaximumLayerSizePX;
		settings->DoubleMaximumLayerSizePX=newSettings.DoubleMaximumLayerSizePX;
		settings->ReferencePointCoordinatesQ=newSettings.ReferencePointCoordinatesQ;
		//Chris Howard
		settings->ChrisMasterIni=newSettings.ChrisMasterIni;
		settings->ChrisSlaveIni=newSettings.ChrisSlaveIni;
		settings->ChrisExe_AVX1=newSettings.ChrisExe_AVX1;
		settings->ChrisExe_AVX2=newSettings.ChrisExe_AVX2;
		settings->ChrisExe_SSE2=newSettings.ChrisExe_SSE2;
		settings->ChrisExe_Pent=newSettings.ChrisExe_Pent;
		settings->ChrisShowExeOutput=newSettings.ChrisShowExeOutput;
		settings->ChrisExeKeepAliving=newSettings.ChrisExeKeepAliving;
		settings->ChrisExeTimeout=newSettings.ChrisExeTimeout;
		settings->ChrisExe=newSettings.ChrisExe;
		//Internal variables
		settings->LastMapCenter=newSettings.LastMapCenter;
		settings->LastMapCS=newSettings.LastMapCS;
		settings->LastMapZoom=newSettings.LastMapZoom;

		SaveSettings();
		SetEvent(newsettingsavailableevent);
		if(PlugInManager) PlugInManager->SaveSettings();
		if(connection)
			ShowMessageObject(NULL, radarmessages, TMessageType::mtWarning, mvtDisapering, 10000,
				"Some new settings, could be applied after data acquisition restart only!", (AnsiString)_RadarMapName);
		else if(pingtool) pingtool->ChangeIPAddress();
		settings->DefaultCoordinateSystem=newSettings.DefaultCoordinateSystem;
		if(mapobjects)
		{
			mapobjects->Container->CoordinateSystem=settings->DefaultCoordinateSystem;
			mapobjects->Container->ZoomUpdate();
			if(mapobjects->Image)
			{
				Types::TRect rct=mapobjects->Image->GetBitmapRect();

				mapobjects->Image->ScrollToCenter(rct.Width()>>1, rct.Height()>>1);
			}
			mapobjects->RenderMaps();
			mapobjects->Update();
		}
		if(objectscontainer) objectscontainer->Update();
	}
	catch(Exception &e)
	{
		LoadSettings();
	}
}
//---------------------------------------------------------------------------

TProfile* __fastcall TRadarMapManager::readReceivingProfile(void)
{
	try
	{
		if(Connection && Connection->Settings && Connection->Settings->SelectedChannel)
			return Connection->Settings->SelectedChannel->Profile;
		else return NULL;
	}
	catch(Exception &e)
	{
		CloseConnection();
		return NULL;
	}
}
//---------------------------------------------------------------------------

int __fastcall TRadarMapManager::readReceivingProfilesQuantity(void)
{
	try
	{
		if(Connection && Connection->Settings)
			return Connection->Settings->ChannelQuantity;
		else return 1;
	}
	catch(Exception &e)
	{
		return 1;
	}
}
//---------------------------------------------------------------------------

int __fastcall TRadarMapManager::readCurrentProfilesCount(void)
{
	try
	{
		if(Current)	return Current->ProfilesCount;
		else return 0;
	}
	catch(Exception &e)
	{
		return 0;
	}
}
//---------------------------------------------------------------------------

TProfile* __fastcall TRadarMapManager::readReceivingProfiles(int Index)
{
	try
	{
		if(Connection && Connection->Settings && Connection->Settings->Channels &&
			Index>=0 && Index<Connection->Settings->ChannelQuantity &&
				Connection->Settings->Channels[Index])
			return Connection->Settings->Channels[Index]->Profile;
		else return NULL;
	}
	catch(Exception &e)
	{
		return NULL;
	}
}
//---------------------------------------------------------------------------

TProfile* __fastcall TRadarMapManager::readCurrentProfile(void)
{
	if(Current && Current->OutputView)
		return Current->OutputView->Profile;
	else return NULL;
}
//---------------------------------------------------------------------------

TProfile* __fastcall TRadarMapManager::readCurrentProfiles(int Index)
{
	if(Current)
		return Current->Profiles[Index];
	else return NULL;
}
//---------------------------------------------------------------------------

TMessageObject* TRadarMapManager::ShowMessageObject(TMessageObject* InMsgObj, TMessagesScrollObject* mso,
	TMessageType mt, TMessageVisibalityType mvt, int ATimeOut, AnsiString Message,
	AnsiString Title, TVoidFunction SwitchOffCallback)
{
	try
	{
		try {if(InMsgObj) InMsgObj->SwitchOff();}
		catch(...) { InMsgObj=NULL;}
		if(mso!=NULL)
			InMsgObj=mso->AddMessage(mt, Message, mvt, ATimeOut, SwitchOffCallback);
		else if(mvt!=mvtConstant)
		{
			unsigned int ui;

			ui=MB_OK;
			switch (mt)
			{
				case(TMessageType::mtText):
				case(TMessageType::mtInfo): ui|=MB_ICONINFORMATION; break;
				case(TMessageType::mtWarning): ui|=MB_ICONWARNING; break;
				case(TMessageType::mtStop): ui|=MB_ICONSTOP; break;
			}
			//Application->MessageBox(((UnicodeString)Message).w_str(), ((UnicodeString)Title).w_str(), ui);
		}
	}
	catch(Exception &e)
	{
		try {if(InMsgObj) InMsgObj->SwitchOff();}
		catch(...) {}
		InMsgObj=NULL;
	}
	return InMsgObj;
}
//---------------------------------------------------------------------------

Types::TRect* __fastcall TRadarMapManager::GetProfRect(int X, int Y)
{
	Types::TRect* res=NULL;
	Types::TPoint p=Types::TPoint(X, Y);

	if(profrects)
	{
		for(int i=0; i<_MaxProfQuan; i++)
		{
			if(profrects[i] && profrects[i]->Contains(p))
			{
				res=profrects[i];
				break;
			}
		}
	}
	return res;
}
//---------------------------------------------------------------------------

int __fastcall TRadarMapManager::GetProfRectIndex(Types::TRect* pr)
{
	int res=-1;

	if(profrects && pr)
	{
		for(int i=0; i<_MaxProfQuan; i++)
		{
			if(pr==profrects[i])
			{
				res=i;
				break;
			}
		}
	}
	return res;
}

void __fastcall TRadarMapManager::NewProject()
{
	TEmptyMapsContainer *em;
	TToolProgressBar *progress;
	int i;

	try
	{
		if(mapobjects && MapObjects->Container)
		{
			progress=MapObjects->Container->Progress;
			i=7;
			if(objectscontainer) i+=objectscontainer->CustomObjectsCount;
			i+=SatellitesList->Count;
			i+=mapobjects->CustomObjectsCount;
			if(progress) progress->Show(i, false);
		}
		else progress=NULL;
		if(!connection)
		{
			SetEvent(GlobalStopItEvent);
			WaitOthers();
			lastlabelid=0;
			Settings->MapLabelGeometryIDs=0;
			if(sparunit) sparunit->Disconnect();
			if(progress) progress->StepIt();
			if(gpsunit) gpsunit->StopReceiving();
			if(progress) progress->StepIt();
			if(objectscontainer) objectscontainer->ClearCustomObjects((TMyObject*)progress);
			trackingpath=NULL;
			sparonmap=NULL;
			current=NULL;
			currentindex-1;
			SatellitesList->Clear(progress);
			if(mapobjects)
			{
				mapobjects->ClearCustomObjects(progress);
				delete linesdetector;
				linesdetector=NULL;
				mapobjects->Container->Clear();
				if(progress) progress->StepIt();
				mapobjects->Zoom->Clear();
				if(progress) progress->StepIt();
				em=new TEmptyMapsContainer(mapobjects->Container, (TObject*)settings, mapobjects->Container->ViewportArea);
				em->OnDisableButton=mapobjects->OnDisableButton;
				if(progress) progress->StepIt();
				if(settings->GPSTracking)
					trackingpath=new TMapTrackPathObject(mapobjects, mapobjects->Container->LatLonRect, (TObject*)this);
				if(sparunit) sparonmap=new TMapSparObject(mapobjects, mapobjects->Container->LatLonRect, (TObject*)this);
				if(settings->CommunicationLinesDetector)
					linesdetector=new TLinesDetector(mapobjects->Image, mapobjects->Container->UtilityPtr, mapobjects->Container->LatLonRect, (TObject*)this);
				if(progress) progress->StepIt();
			}
			if(gpsunit)
			{
				gpsunit->ReCenter();
				gpsunit->SetOffset(0, 0);
				gpsunit->StartReceiving();
			}
			if(progress) progress->StepIt();
			if(sparunit) sparunit->Connect(settings->SparPipeName);
			if(progress) progress->StepIt();
			if(ProfileClearFunction) (ProfileClearFunction)(true);
			if(progress) progress->StepIt();
			settings->PathColorIncIndex=2;
			ResetEvent(GlobalStopItEvent);
		}
	}
	__finally
	{
		resultsnotsaved=false;
		if(progress) progress->Hide(false);
	}
}
//---------------------------------------------------------------------------

void __fastcall TRadarMapManager::LoadResultsFromGML(AnsiString FileName)
{
	TXMLExplorer *XMLExplorer;
	AnsiString str, str2, str3, str4, str5, lc, uc, xmr;
	TCoordinateSystem CS;
	TGPSCoordinate *c=NULL;
	int i;
	TEmptyMapsContainer *em;

	NewProject();
	if(!mapobjects || !objectscontainer) return;
	if(FileName!="" && FileExists(FileName))
	{
		XMLExplorer=new TXMLExplorer(NULL, FileName);
		try
		{
			if(XMLExplorer->FindTagStart("xrm:RadarMap")) xmr="xrm:";
			else xmr="";
            XMLExplorer->Restart();
			while(XMLExplorer->GetNextStartTagInside(xmr+"RadarMap", str))
			{
				if(str=="gml:Envelope")
				{
					while(XMLExplorer->GetNextStartTagInside(str, str2))
					{
						if(str2=="gml:lowerCorner")
						{
							if(XMLExplorer->GetContent(str3) && str3!="") lc=str3;
						}
						else if(str2=="gml:upperCorner")
						{
							if(XMLExplorer->GetContent(str3) && str3!="") uc=str3;
						}
					}
				}
				else if(str=="map" || str==xmr+"map")
				{
					AnsiString fn="", kn="", cs="", guid="", plugin="", name="Unknown CRS";
					AnsiString proj4prefix="", proj4name="", proj4ellipsoid="",
						proj4epsg="", proj4="";
					TCoordinateSystemDimension proj4unit=csdNone;
					bool err=false;

					while(XMLExplorer->GetNextStartTagInside(str, str2))
					{
						if(str2=="filename" || str2==xmr+"filename")
						{
							if(XMLExplorer->GetContent(str3) && str3!="") fn=str3;
						}
						else if(str2=="Klicnummer" || str2==xmr+"Klicnummer")
						{
							if(XMLExplorer->GetContent(str3) && str3!="") kn=str3;
						}
						else if(str2=="CoordinateSystem" || str2==xmr+"CoordinateSystem")
						{
							if(XMLExplorer->GetContent(str3) && str3!="") cs=str3;
						}
						else if(str2=="csGUID" || str2==xmr+"csGUID")
						{
							if(XMLExplorer->GetContent(str3) && str3!="") guid=str3;
						}
						else if(str2=="csPROJ4Prefix" || str2==xmr+"csPROJ4Prefix")
						{
							if(XMLExplorer->GetContent(str3) && str3!="") proj4prefix=str3;
						}
						else if(str2=="csPROJ4Name" || str2==xmr+"csPROJ4Name")
						{
							if(XMLExplorer->GetContent(str3) && str3!="") proj4name=str3;
						}
						else if(str2=="csPROJ4Ellipsoid" || str2==xmr+"csPROJ4Ellipsoid")
						{
							if(XMLExplorer->GetContent(str3) && str3!="") proj4ellipsoid=str3;
						}
						else if(str2=="csPROJ4EPSG" || str2==xmr+"csPROJ4EPSG")
						{
							if(XMLExplorer->GetContent(str3) && str3!="") proj4epsg=str3;
						}
						else if(str2=="csPROJ4Unit" || str2==xmr+"csPROJ4Unit")
						{
							if(XMLExplorer->GetContent(str3) && str3!="")
							{
								try
								{
									proj4unit=(TCoordinateSystemDimension)StrToInt(str3);
								}
								catch(...) {proj4unit=csdNone;}
							}
						}
						else if(str2=="csPROJ4Definition" || str2==xmr+"csPROJ4Definition")
						{
							if(XMLExplorer->GetContent(str3) && str3!="") proj4=str3;
						}
						else if(str2=="csPlugIn" || str2==xmr+"csPlugIn")
						{
							if(XMLExplorer->GetContent(str3) && str3!="") plugin=str3;
						}
						else if(str2=="csPlugInName" || str2==xmr+"csPlugInName")
						{
							if(XMLExplorer->GetContent(str3) && str3!="") name=str3;
						}
					}
					try
					{
						AnsiString ext;

						if(cs!=NULL && cs.Length()>0)
						{
							if(cs=="csDutch") CS=csDutch;
							else if(cs=="csBeLambert72") CS=csBeLambert72;
							else if(cs=="csBeLambert08") CS=csBeLambert08;
							else if(cs=="csUTM") CS=csUTM;
							else if(cs=="csLatLon") CS=csLatLon;
							//else if(cs=="csLKS") CS=csLKS;
							//else if((cs.SubString(1, 4)=="dll_" || cs.SubString(1, 5)=="csDLL") && guid!="")
							else if(cs=="csPROJ4" && proj4!=NULL && proj4.Length()>0 &&	PlugInManager)
							{
								TPlugInCoordinateSystem *plugin;

								plugin=dynamic_cast<TPlugInCoordinateSystem*>(PlugInManager->GetByGUID(_csPROJ4GUID, pitCRS));
								if(plugin)
								{
									if(proj4prefix!=NULL && proj4prefix.Length()>0)
										plugin->Prefix=proj4prefix;
									if(proj4name!=NULL && proj4name.Length()>0)
										plugin->Name=proj4name;
									if(proj4ellipsoid!=NULL && proj4ellipsoid.Length()>0)
										plugin->EllipsoidName=proj4ellipsoid;
									if(proj4epsg!=NULL && proj4epsg.Length()>0)
										plugin->EPSG=proj4epsg;
									if(proj4unit!=csdNone) plugin->Dimension=proj4unit;
									plugin->PROJ4=proj4;
									CS=csDLL;
									PlugInManager->SetActive(_csPROJ4GUID, pitCRS);
								}
							}
							else if(guid!="" && IsValidGUID(guid))
							{
								if(PlugInManager && PlugInManager->GetByGUID(guid, pitCRS))
								{
									PlugInManager->SetActive(guid, pitCRS);
									if(guid==_csPROJ4GUID && proj4!=NULL && proj4.Length()>0) PlugInManager->ActiveCS->PROJ4=proj4;
									CS=csDLL;
								}
								else if(guid==_csLatLonGUID) cs=csLatLon;
								else if(guid==_csDutchGUID) cs=csDutch;
								else if(guid==_csBeLambert72GUID || guid==_csBeLambert08GUID) cs=csBeLambert72;
								else if(guid==_csUTMGUID) cs=csUTM;
								//else if(guid==_csLKSGUID) cs=csLKS;
								else
								{
									Application->MessageBox(((UnicodeString)"The unknown Coordinate System PlugIn ("+name+" - File: "+plugin+") is used. Please install it before opening the data!").w_str(), L"RadarMap", MB_OK | MB_ICONERROR);
									NewProject();
									return;
								}
							}
							else
							{
								Application->MessageBox(((UnicodeString)"The unknown Coordinate System ("+name+") is used!").w_str(), L"RadarMap", MB_OK | MB_ICONERROR);
								NewProject();
								return;
							}
						}
						else CS=Settings->DefaultCoordinateSystem;
						if(CS==csUTM) CS=csLatLon;
						if(!FileExists(fn))
						{
							if(FileExists(ExtractFileName(fn))) fn=ExtractFileName(fn);
						}
						if(FileExists(fn))
						{
							try
							{
								ext=ExtractFileExt(fn).UpperCase();
							}
							catch(Exception &e) {ext=".";}
							MapObjects->Container->Clear();
							mapobjects->Zoom->Clear();
							if(ext==".XML")
								(new TXMLMapsContainer(MapObjects->Container, settings, MapObjects->Container->ViewportArea))->LoadData(fn, NULL, true, CS);
							else if(ext==".DXF")
								(new TDXFMapsContainer(MapObjects->Container, settings, MapObjects->Container->ViewportArea))->LoadData(fn, NULL, true, CS);
							else if(ext==".PNG" || ext==".JPEG" || ext==".JPG" || ext==".BMP" || ext==".GIF" || ext==".TIF" || ext==".TIFF" || ext==".EMF" || ext==".WMF")
								(new TImageMapsContainer(MapObjects->Container, settings, MapObjects->Container->ViewportArea, MapObjects))->LoadData(fn, NULL, true, CS);
							else if(ext==".DBSL")
								(new TDbDXFMapsContainer(MapObjects->Container, Settings, MapObjects->Container->ViewportArea))->LoadData(fn, NULL, true, CS);
							else
							{
								ShowMessageObject(NULL, MapMessages,  TMessageType::mtWarning, mvtDisapering, 5000,
									"Unknown map format!", (AnsiString)_RadarMapName);
								err=true;
							}
						}
						else if(lc!=NULL && lc.Length()>0 && uc!=NULL && uc.Length()>0)
						{
							if(fn!=NULL && fn.Pos(_HTTP)>0)
							{
								TInetMapBase *bing_map=NULL;
								TDoublePoint dplc, dpuc, dp;

								MapObjects->Container->Clear();
								mapobjects->Zoom->Clear();
								if(fn.Pos(_BingMapsHTTP)>0)
									bing_map = new TBingInetMap(MapObjects->Container, Settings, MapObjects->Container->ViewportArea);
								else if(fn.Pos(_OpenStreetMapHTTP)>0)
									bing_map = new TOsmInetMap(MapObjects->Container, Settings, MapObjects->Container->ViewportArea);
								if(bing_map)
								{
									MapObjects->Container->CoordinateSystem = CS;
									bing_map->MapImageWidth = MaxBitmapWidth;
									bing_map->MapImageHeight = MaxBitmapHeight;
									if(CS==csUTM) CS=csLatLon;
									dplc=DoublePoint(lc);
									dpuc=DoublePoint(uc);
									dp.X=(dplc.X+dpuc.X)/2.;
									dp.Y=(dplc.Y+dpuc.Y)/2.;
									*((int*)dp.Temp)=*((int*)dplc.Temp);
									c=new TGPSCoordinate(false);
									try
									{
										if(CS==csLatLon || cs==csUTM)
											c->SetLatLonAlt(dp.X, dp.Y, 0);
										else c->SetXY(CS, dp);
										bing_map->MapCenterLatitude = c->Latitude;// OnlineLoadDlg->MapCenterLatitude;
										bing_map->MapCenterLongitude = c->Longitude;// OnlineLoadDlg->MapCenterLongitude;
									}
									__finally
									{
										delete c;
										c=NULL;
									}
									bing_map->MapImageZoom = Settings->LastMapZoom; //OnlineLoadDlg->MapImageZoom;
									bing_map->MapViewType = imvtMap;//OnlineLoadDlg->MapViewType;
									new TInetMapLoader(this, bing_map, bing_map->Progress);
								}
								else
								{
									ShowMessageObject(NULL, MapMessages,  TMessageType::mtWarning, mvtDisapering, 5000,
										"Unknown online map source!", (AnsiString)_RadarMapName);
									err=true;
								}
							}
							else if((fn=="empty" && kn=="EmptyMap") || fn=="")
							{
								try
								{
									if(MapObjects && MapObjects->Container && MapObjects->Container->Count>0)
									{
										TMapsContainer* mc=MapObjects->Container->GetObject(0);
	#ifndef OnlyOneMap
										int i=1;

										while(mc!=NULL && mc->MapType!=mtEmpty i<MapObjects->Container->Count)
										{
											mc=MapObjects->Container->GetObject(i);
											i++;
										}
	#endif
										if(mc && mc->MapType==mtEmpty)
										{
											bool b=mc->LockedCS;
											TDoublePoint dp;

											mc->LockedCS=false;
											mc->CoordinateSystem=CS;
											mc->LockedCS=b;
											if(CS==csLatLon || CS==csUTM) //UTM is represented as LatLon, but written csUTM in *.gml file
											{
												// Corners are represented as "Latitude Longitude" what correponds to "Y, X"
												dp=DoublePoint(lc);
												if(CS==csUTM)
												{
													c=new TGPSCoordinate(false);
													try
													{
														c->SetXY(csLatLon, DoublePoint(dp.Y, dp.X));
														dp.X=c->GetY(csUTM);
														dp.Y=c->GetX(csUTM);
													}
													__finally
													{
														delete c;
														c=NULL;
													}
												}
												mc->LowerCorner=DoublePointToStr(DoublePoint(dp.Y, dp.X));//lc;
												dp=DoublePoint(uc);
												if(CS==csUTM)
												{
													c=new TGPSCoordinate(false);
													try
													{
														c->SetXY(csLatLon, DoublePoint(dp.Y, dp.X));
														dp.X=c->GetY(csUTM);
														dp.Y=c->GetX(csUTM);
													}
													__finally
													{
														delete c;
														c=NULL;
													}
												}
												mc->UpperCorner=DoublePointToStr(DoublePoint(dp.Y, dp.X));//uc;
											}
											else
											{
												mc->LowerCorner=lc;
												mc->UpperCorner=uc;
											}
											mc->Update();
										}
//										if(mc && mc->MapType==mtEmpty && mc->LatLonRect && !mc->LatLonRect->Contains(c))
//										{
//											if(mc->LatLonRect->Valid)
//											{
//											}
//											else
//											{
//												o->Latitude=c->Latitude;
//												o->Longitude=c->Longitude;
//												o->AutomaticUpdate=true;
//												o2->Latitude=o->Latitude;
//												o2->Longitude=o->Longitude;
//												o->RdX-=50.;
//												o->RdY-=50.;
//												o->AutomaticUpdate=false;
//												o2->RdX+=50.;
//												o2->RdY+=50.;
//												mc->LatLonRect->SetCorners(o, o2, true, ((TMapObjectsContainer*)Owner)->Container->CoordinateSystem);
//											}
//											if(!LatLonRect->Contains(mc->LatLonRect->LeftTop) ||
//												!LatLonRect->Contains(mc->LatLonRect->RightBottom))
//											{
//												if(LatLonRect->Valid)
//												{
//													cr->SetCorners(mc->LatLonRect->LeftTop, LatLonRect->LeftTop);
//													o->Latitude=cr->LeftTop->Latitude;
//													o->Longitude=cr->LeftTop->Longitude;
//													cr->SetCorners(mc->LatLonRect->LeftTop, LatLonRect->RightBottom);
//													LatLonRect->SetCorners(o, cr->RightBottom);
//												}
//												else LatLonRect->SetCorners(mc->LatLonRect->LeftTop, mc->LatLonRect->RightBottom);
//											}
//										}
									}
									err=false;
								}
								catch(Exception &e) {err=true;}
							}
							else err=true;
						}
						else err=true;
					}
					catch(Exception &e)	{err=true;}
					if(err)
					{
						int res;

						NewProject();
						MapObjects->Container->Clear();
						mapobjects->Zoom->Clear();
						res=Application->MessageBox(((UnicodeString)"The Map "+kn+" ("+fn+") cannot be found, corrupted or has an unknown format. \r\n\r\nDo you want to find it manualy?").w_str(),
							((UnicodeString)_RadarMapName).w_str(), MB_ICONSTOP | MB_YESNO);
						if(res==IDYES)
						{
							TPreviewForm *PreviewForm;
							int ms=0;
							TTimeStamp t1, t2;
							AnsiString ext;

							PreviewForm=new TPreviewForm(NULL, (TObject*)this);
							try
							{
								try
								{
									ext=ExtractFileExt(fn).UpperCase();
								}
								catch(Exception &e) {ext=".";}
								if(ext==".XML") PreviewForm->OpenDialog->FilterIndex=0;
								else if(ext==".DXF") PreviewForm->OpenDialog->FilterIndex=1;
								else if(ext==".PNG" || ext==".JPEG" || ext==".JPG" || ext==".BMP" || ext==".GIF" || ext==".TIF" || ext==".TIFF" || ext==".EMF" || ext==".WMF") PreviewForm->OpenDialog->FilterIndex=2;
								else if(ext==".DBSL") PreviewForm->OpenDialog->FilterIndex=3;
								else PreviewForm->OpenDialog->FilterIndex=4;
								if(PreviewForm->OpenDialog->Execute())
								{
									try
									{
										ms=0;
										t1=DateTimeToTimeStamp(Now());
										while(PreviewForm->PreviewInProgress>0 && ms<2000)
										{
											t2=DateTimeToTimeStamp(Now());
											ms=t2.Time-t1.Time;
											ProcessMessages();
											MySleep(10);
										}
										if(PreviewForm->PreviewInProgress>0)
										{
											ShowMessageObject(NULL, MapMessages,  TMessageType::mtStop, mvtDisapering, 5000,
												"There are no enough resources! Please switch off Preview setting and try to open it again.", (AnsiString)_RadarMapName);
											return;
										}
										else
										{
											try
											{
												ext=ExtractFileExt(PreviewForm->OpenDialog->FileName).UpperCase();
											}
											catch(Exception &e) {ext=".";}
											if(ext==".XML")
												(new TXMLMapsContainer(mapobjects->Container, settings, mapobjects->Container->ViewportArea))->LoadData(PreviewForm->OpenDialog->FileName, NULL, true, CS);
											else if(ext==".DXF")
												(new TDXFMapsContainer(mapobjects->Container, settings, mapobjects->Container->ViewportArea))->LoadData(PreviewForm->OpenDialog->FileName, NULL, true, CS);
											else if(ext==".PNG" || ext==".JPEG" || ext==".JPG" || ext==".BMP" || ext==".GIF" || ext==".TIF" || ext==".TIFF" || ext==".EMF" || ext==".WMF")
												(new TImageMapsContainer(mapobjects->Container, settings, mapobjects->Container->ViewportArea, mapobjects))->LoadData(PreviewForm->OpenDialog->FileName, NULL, true, CS);
											else if(ext==".DBSL")
												(new TDbDXFMapsContainer(mapobjects->Container, settings, mapobjects->Container->ViewportArea))->LoadData(PreviewForm->OpenDialog->FileName, NULL, true, CS);
											else
											{
												ShowMessageObject(NULL, MapMessages,  TMessageType::mtStop, mvtDisapering, 5000,
													"Unknown map format!", (AnsiString)_RadarMapName);
												return;
											}
										}
									}
									catch(Exception &e)
									{
										MapObjects->Container->Clear();
										mapobjects->Zoom->Clear();
										em=new TEmptyMapsContainer(MapObjects->Container, Settings, MapObjects->Container->ViewportArea);
										em->OnDisableButton=MapObjects->OnDisableButton;
										ShowMessageObject(NULL, MapMessages,  TMessageType::mtWarning, mvtDisapering, 5000,
											"Cannot open file "+PreviewForm->GMLOpenDialog->FileName+"!", "Error");
										return;
									}
								}
								else return;
							}
							__finally
							{
								delete PreviewForm;
							}
						}
						else
						{
							em=new TEmptyMapsContainer(MapObjects->Container, Settings, MapObjects->Container->ViewportArea);
							em->OnDisableButton=MapObjects->OnDisableButton;
						}
					}
				}
				else if(str=="mapPath" || str==xmr+"mapPath")
				{
					TProfile* profile=NULL;
					TProfileSatellites *ps=NULL;
					AnsiString posList="", traceIndexes="";
					int srsDimension=2;

					while(XMLExplorer->GetNextStartTagInside(str, str2))
					{
						if(str2=="profile" || str2==xmr+"profile")
						{
							if(XMLExplorer->GetContent(str3, true) && str3!="")
							{
								bool err=false;

								try
								{
									if(ExtractFileDrive(str3)=="") str3=ExtractFilePath(FileName)+str3;
									if(FileExists(str3))
									{
										profile=new TProfile();
										profile->Permit=Settings->Permitivity;
										if(Palette) Palette->ConvertToWinColors(profile->Colors, 256);
										i=Input_SEGY(str3.c_str(), profile, mapobjects->Container->Progress, false, false, Settings->ApplyGain);
										if(i!=0)
										{
											if(profile)	delete profile;
											profile=NULL;
											break;
										}
										else if(profile)
										{
											if(profile->Traces>0 && profile->LastTracePtr)
											{
												profile->OutputView->RightIndex=profile->LastTracePtr->Index;
												profile->OutputView->LeftIndex=profile->OutputView->RightIndex-ProfRect[0]->Width()+4;//objectscontainer->Image->Width;
												if(ps) ps->AddProfile(profile);
												else
												{
													ps=new TProfileSatellites(this, profile);
													currentindex=SatellitesList->Add(ps);
													current=ps;
												}
											}
											else
											{
												if(profile)	delete profile;
												profile=NULL;
											}
										}
									}
									else err=true;
								}
								catch(Exception &e) {err=true;}
								if(err)
								{
									Application->MessageBox(((UnicodeString)"Cannot open profile "+str3+")!").w_str(),
										((UnicodeString)_RadarMapName).w_str(), MB_ICONSTOP | MB_OK);
									break;
								}
							}
						}
						else if((str2=="name" || str2==xmr+"name") && ps)
						{
							if(XMLExplorer->GetContent(str3, true)) ps->Path->Name=str3;
						}
						else if((str2=="points" || str2==xmr+"points" ||
							str2=="gml:LineString" ||
							str2=="gml:posList" ||
							str2=="traceindexes" || str2==xmr+"traceindexes") && ps)
						{
//							if((str2=="points" || str2==xmr+"points"))
//							{
//								while(XMLExplorer->GetNextStartTagInside(str2, str3))
//								{
//									if(str3=="gml:posList") XMLExplorer->GetContent(posList);
//									else if(str3=="traceindexes") XMLExplorer->GetContent(traceIndexes);
//								}
//							}
//							else if((str2=="gml:LineString" || str2==xmr+"gml:LineString"))
//							{
//								//if(XMLExplorer->GetNextStartTagInside(str2, str3) &&
//								//	(str3=="gml:LineString" || (str3=="gml:posList" &&
//								//	XMLExplorer->GetNextStartTagInside(str3, str3) && str3=="gml:posList")))
//								//	XMLExplorer->GetContent(posList);
//								while(XMLExplorer->GetNextStartTagInside(str2, str3))
//									if(str3=="gml:posList") XMLExplorer->GetContent(posList);
//							}
							if(str2=="gml:LineString")
							{
								if(XMLExplorer->GetAttribute("srsDimension", str3))
								{
									try
									{
										srsDimension=StrToInt(str3);
									}
									catch(Exception &e) {srsDimension=2;}
								}
								else srsDimension=2;
							}
							if(str2=="gml:posList") XMLExplorer->GetContent(posList);
							else if((str2=="traceindexes" || str2==xmr+"traceindexes"))
								XMLExplorer->GetContent(traceIndexes);
							if(posList!="" && traceIndexes!="")
							{
								int i=1, j=0, k=1, ti;
								double x, y, h, d;
								bool digit=false;
								AnsiString nr="";
								TTrace *ptr;

								c=NULL;
								x=y=h=0.;
								mapobjects->Container->Progress->Show(0, true);
								mapobjects->Container->Progress->Max+=CharInString(traceIndexes, ' ');
								while(i<=posList.Length())
								{
									if(posList[i]>='0' && posList[i]<='9') nr+=posList[i];
									else if(posList[i]=='.' || posList[i]==',') nr+=DecimalSeparator;
									else if(posList[i]=='-') nr+='-';
									else digit=true;
									if(digit || i==posList.Length())
									{
										digit=false;
										try
										{
											d=StrToFloat(nr);
										}
										catch(Exception &e) {j=-1;}
										nr="";
										j++;
										if(j==1) x=d;
										else if(j==2) y=d;
										else if(j==3) h=d;
										if(j==srsDimension)
										{
											while(k<=traceIndexes.Length() && ((traceIndexes[k]>='0' && traceIndexes[k]<='9') || traceIndexes[k]=='-'))
											{
												if(traceIndexes[k]=='-' && nr!="")
												{
													while(k<=traceIndexes.Length() && ((traceIndexes[k]>='0' && traceIndexes[k]<='9') || traceIndexes[k]=='-'))
														k++;
												}
												else
												{
													nr+=traceIndexes[k];
													k++;
												}
											}
											k++;
											c=new TGPSCoordinate(false);
											try
											{
												ti=StrToInt(nr);
											}
											catch(Exception &e) {ti=-1;}
											nr="";
											if(ti>=0 && ti<=profile->LastTracePtr->Index)
											{
												ptr=profile->OutputView->GetTraceByIndex(ti);
												if(ptr!=NULL) c->Trace=ptr;
											}
											if(CS==csLatLon || CS==csUTM)
											{
												/*c->Latitude=x;
												c->Longitude=y;
												if(srsDimension>2) c->Z_Geoid=h;*/
												if(srsDimension>2) c->SetLatLonAlt(x, y, h);
												else c->SetLatLon(x, y);
											}
											else
											{
												/*c->SetX(CS, x);
												c->SetY(CS, y);
												if(srsDimension>2) c->SetH(CS, h);*/
												if(srsDimension>2) c->SetXYH(CS, x, y, h);
												else c->SetXY(CS, x, y, NULL);
											}
											c->AutomaticUpdate=true;
											ps->Path->AddP(c);
											x=y=h=0.;
											j=0;
											mapobjects->Container->Progress->StepIt();
										}
									}
									i++;
								}
								if(trackingpath && c) trackingpath->ScrollToCoordinate(c);
								mapobjects->Container->Progress->Hide(true);
							}
						}
						else if((str2=="label" || str2==xmr+"label" ||
							str2=="mapPathLabel" || str2==xmr+"mapPathLabel") && ps)
						{
							TProfileLabelType lt=pltNone;
							TProfileLabel *pl;
							TColor32 color=clGreen32;
							float z=0;
							int traceindex=-1, profileindex=0, sample=-1;
							bool locked=true;
							AnsiString description="", attachment="", created="", modified="";
							double confidence=0;
							unsigned int id=0;

							while(XMLExplorer->GetNextStartTagInside(str2, str3))
							{
								if(str3=="type" || str3==xmr+"type")
								{
									if(XMLExplorer->GetContent(str4) && str4!="")
									{
										if(str4=="pltVertLine") lt=pltVertLine;
										else if(str4=="pltTransformed") lt=pltTransformed;
										else if(str4=="pltCircleRed") lt=pltCircleRed;
										else if(str4=="pltCircleGreen") lt=pltCircleGreen;
										else if(str4=="pltCircleBlue") lt=pltCircleBlue;
										else if(str4=="pltSquareRed") lt=pltSquareRed;
										else if(str4=="pltSquareGreen") lt=pltSquareGreen;
										else if(str4=="pltSquareBlue") lt=pltSquareBlue;
										else if(str4=="pltTriangleRed") lt=pltTriangleRed;
										else if(str4=="pltTriangleGreen") lt=pltTriangleGreen;
										else if(str4=="pltTriangleBlue") lt=pltTriangleBlue;
										else if(str4=="pltWaterCircle") lt=pltWaterCircle;
										else if(str4=="pltWaterRect") lt=pltWaterRect;
										else if(str4=="pltWaterTriangle") lt=pltWaterTriangle;
										else if(str4=="pltElectroCircle") lt=pltElectroCircle;
										else if(str4=="pltElectroRect") lt=pltElectroRect;
										else if(str4=="pltElectroTriangle") lt=pltElectroTriangle;
										else if(str4=="pltGasCircle") lt=pltGasCircle;
										else if(str4=="pltGasRect") lt=pltGasRect;
										else if(str4=="pltGasTriangle") lt=pltGasTriangle;
										else if(str4=="pltGasHighCircle") lt=pltGasHighCircle;
										else if(str4=="pltGasHighRect") lt=pltGasHighRect;
										else if(str4=="pltGasHighTriangle") lt=pltGasHighTriangle;
										else if(str4=="pltSewageCircle") lt=pltSewageCircle;
										else if(str4=="pltSewageRect") lt=pltSewageRect;
										else if(str4=="pltSewageTriangle") lt=pltSewageTriangle;
										else if(str4=="pltTelecomCircle") lt=pltTelecomCircle;
										else if(str4=="pltTelecomRect") lt=pltTelecomRect;
										else if(str4=="pltTelecomTriangle") lt=pltTelecomTriangle;
										else if(str4=="pltFiberCircle") lt=pltFiberCircle;
										else if(str4=="pltFiberRect") lt=pltFiberRect;
										else if(str4=="pltFiberTriangle") lt=pltFiberTriangle;
										else if(str4=="pltAutoDetect") lt=pltAutoDetect;
										else if(str4=="pltAutoDetectPos") lt=pltAutoDetectPos;
										else if(str4=="pltAutoDetectNeg") lt=pltAutoDetectNeg;
									}
								}
								else if(str3=="color" || str3==xmr+"color")
								{
									if(XMLExplorer->GetContent(str4) && str4!="")
									{
										try
										{
											color=TColor32(StrToInt(str4));
										}
										catch (Exception &e) {}
									}
								}
								else if(str3=="depth" || str3==xmr+"depth")
								{
									if(XMLExplorer->GetContent(str4) && str4!="")
									{
										try
										{
											z=StrToFloat(str4);
										}
										catch (Exception &e) {}
									}
								}
								else if(str3=="profileindex" || str3==xmr+"profileindex")
								{
									if(XMLExplorer->GetContent(str4) && str4!="")
									{
										try
										{
											profileindex=StrToInt(str4);
										}
										catch (Exception &e) {}
									}
								}
								else if(str3=="traceindex" || str3==xmr+"traceindex")
								{
									if(XMLExplorer->GetContent(str4) && str4!="")
									{
										try
										{
											traceindex=StrToInt(str4);
										}
										catch (Exception &e) {}
									}
								}
								else if(str3=="locked" || str3==xmr+"locked")
								{
									if(XMLExplorer->GetContent(str4) && str4!="")
									{
										try
										{
											locked=StrToInt(str4);
										}
										catch (Exception &e) {}
									}
								}
								else if(str3=="sample" || str3==xmr+"sample")
								{
									if(XMLExplorer->GetContent(str4) && str4!="")
									{
										try
										{
											sample=StrToInt(str4);
										}
										catch (Exception &e) {}
									}
								}
								else if(str3=="description" || str3==xmr+"description") XMLExplorer->GetContent(description, true);
								else if(str3=="linkedfile" || str3==xmr+"linkedfile")	 XMLExplorer->GetContent(attachment, true);
								else if(str3=="created" || str3==xmr+"created")	 XMLExplorer->GetContent(created);
								else if(str3=="modified" || str3==xmr+"modified")	 XMLExplorer->GetContent(modified);
								else if(str3=="confidence" || str3==xmr+"confidence")
								{
									if(XMLExplorer->GetContent(str4) && str4!="")
									{
										try
										{
											confidence=StrToFloat(str4);
										}
										catch (Exception &e) {}
									}
								}
								else if(str3=="ID" || str3==xmr+"ID")
								{
									if(XMLExplorer->GetContent(str4) && str4!="")
									{
										try
										{
											id=StrToInt(str4);
										}
										catch (Exception &e) {}
									}
								}
							}
							if(mapobjects && lt!=pltNone)
							{
								if(sample<0) sample=(int)((z/profile->Depth)*(float)profile->Samples);//+profile->ZeroPoint);
								pl=new TProfileLabel(objectscontainer, objectscontainer->ViewPort,
									profile->OutputView, lt, traceindex, sample, color, this);
								pl->ProfileIndex=profileindex;
								pl->Description=description;
								pl->Attachment=attachment;
								pl->WriteCreatedTime(created);
								pl->WriteModifiedTime(modified);
								pl->GPSConfidence=confidence;
								pl->ID=id;
								pl->Locked=locked;
								new TMapLabel(mapobjects, mapobjects->ViewPort,
									pl, mapobjects->Container->LatLonRect, ps->Path, this);
							}
						}
					}
				}
				else if(str==xmr+"mapLabel" || str=="mapLabel")
				{
					bool lt=false;
					TMapLabel *ml;
					TColor32 color=clRed32;
					AnsiString description="", attachment="", pos="", created="", modified="";
					double confidence=0;
					unsigned int id=0;
					int srsDimension=2;

					while(XMLExplorer->GetNextStartTagInside(str, str3))
					{
						if(str3=="type" || str3==xmr+"type")
						{
							if(XMLExplorer->GetContent(str4) && str4!="")
							{
								if(str4=="potMapLabel") lt=true;
							}
						}
						else if(str3=="color" || str3==xmr+"color")
						{
							if(XMLExplorer->GetContent(str4) && str4!="")
							{
								try
								{
									color=TColor32(StrToInt(str4));
								}
								catch (Exception &e) {}
							}
						}
						else if(str3=="description" || str3==xmr+"description") XMLExplorer->GetContent(description, true);
						else if(str3=="linkedfile" || str3==xmr+"linkedfile")	XMLExplorer->GetContent(attachment, true);
						else if(str3=="created" || str3==xmr+"created")	XMLExplorer->GetContent(created);
						else if(str3=="modified" || str3==xmr+"modified")	XMLExplorer->GetContent(modified);
						else if(str3=="point" || str3=="gml:Point")
						{
							if(XMLExplorer->GetAttribute("srsDimension", str5))
							{
								try
								{
									srsDimension=StrToInt(str5);
								}
								catch(Exception &e) {srsDimension=2;}
							}
							else srsDimension=2;
							while(XMLExplorer->GetNextStartTagInside(str3, str4))
								if(str4=="gml:pos" && XMLExplorer->GetContent(str5) && str5!="") pos=str5;
						}
						else if(str3=="confidence" || str3==xmr+"confidence")
						{
							if(XMLExplorer->GetContent(str4) && str4!="")
							{
								try
								{
									confidence=StrToFloat(str4);
								}
								catch (Exception &e) {}
							}
						}
						else if(str3=="ID" || str3==xmr+"ID")
						{
							if(XMLExplorer->GetContent(str4) && str4!="")
							{
								try
								{
									id=StrToInt(str4);
								}
								catch (Exception &e) {}
							}
						}
					}
					if(mapobjects && lt && pos!=NULL && pos.Length()>0)
					{
						TMapsContainer *mc=NULL;
						int j=0, ti, num;
						double x, y, z, d;
						bool digit=false;
						AnsiString nr="";
						char ch;

						i=1;
						x=y=z=0;
						while(i<=pos.Length() && j<srsDimension)
						{
							if(pos[i]>='0' && pos[i]<='9') nr+=pos[i];
							else if(pos[i]=='.' || pos[i]==',') nr+=DecimalSeparator;
							else if(pos[i]=='-') nr+='-';
							else digit=true;
							if(digit || i==pos.Length())
							{
								digit=false;
								try
								{
									d=StrToFloat(nr);
								}
								catch(Exception &e) {j=-1;}
								nr="";
								j++;
								if(j==1) x=d;
								else if(j==2) y=d;
								else if(j==3) z=d;
							}
							i++;
						}
						i=0;
						if(j>1)
						{
							do
							{
								mc=mapobjects->Container->GetObject(i);
								i++;
							} while(i<mapobjects->Container->Count && mc);// && mc->MapType==mtEmpty);
							if(mc && TrackingPath)// && mc->MapType!=mtEmpty)
							{
//								ml=new TMapLabel(MapObjects, MapObjects->ViewPort,
//									MapObjects->Container->LatLonRect, NULL, this);
//								ml->NormalGlyphName="PNGIMAGE_156";
//								if(ml->NormalGlyph) ml->Center=Types::TPoint(16, 28);//Types::TPoint(ml->NormalGlyph->Width>>1,  ml->NormalGlyph->Height);
//								else ml->Center=Types::TPoint(0, 0);
//								ml->AllwaysHot=true;
//								ml->Icons=true;
								ml=new TPinLabel(MapObjects, MapObjects->ViewPort,
									MapObjects->Container->LatLonRect, false, this);
								ml->Color=color;
								ml->Description=description;
								ml->Attachment=attachment;
								ml->WriteCreatedTime(created);
								ml->WriteModifiedTime(modified);
								ml->GPSConfidence=confidence;
								ml->ID=id;
								c=new TGPSCoordinate(false);
								/*if(CS==csLatLon || CS==csUTM) //MapObjects->Container->CoordinateSystem
								{
									c->Latitude=x;  // X and Y are changed, because standard format is used
									c->Longitude=y; // is used: (Latitude, Longitude)
									if(srsDimension>2 && j>2) c->Z_Geoid=z;
								}
								else
								{
									c->SetX(CS, x);
									c->SetY(CS, y);
									if(srsDimension>2 && j>2) c->SetH(CS, z);
								} */
								if(CS==csLatLon || CS==csUTM)
								{
									if(srsDimension>2 && j>2) c->SetLatLonAlt(x, y, z);
									else c->SetLatLon(x, y);
								}
								else
								{
									if(srsDimension>2 && j>2) c->SetXYH(CS, x, y, z);
									else c->SetXY(CS, x, y, NULL);
								}
								c->Confidence=confidence;
								c->AutomaticUpdate=true;
								ml->Coordinate=c;
							}
						}
					}
				}
				else if(str==xmr+"mapLabelGeometry" || str=="mapLabelGeometry")
				{
					TMapLabelGeometryType mlgt=mlgtNone;
					TProfileLabelType plt=pltNone;
					TMapLabelType mlt=mltNone;
					int thickness;
					bool singletype;
					TMapLabel *ml;
					TMapLabelGeometry *mlg;
					TColor32 color=clRed32, bordercolor=clRed32;
					AnsiString description="", name="", posList="", LabelsIDs="";;
					unsigned int id=0, li;
					//int srsDimension=2;
					int k=1;
					AnsiString nr;

					while(XMLExplorer->GetNextStartTagInside(str, str3))
					{
						if(str3=="mapLabelGeometryKind" || str3==xmr+"mapLabelGeometryKind")
						{
							if(XMLExplorer->GetContent(str4) && str4!="")
							{
								mlgt=StrToTMapLabelGeometryType(str4);
							}
						}
						else if(str3=="ID" || str3==xmr+"ID")
						{
							if(XMLExplorer->GetContent(str4) && str4!="")
							{
								try
								{
									id=StrToInt(str4);
								}
								catch (Exception &e) {}
							}
						}
						else if(str3=="name" || str3==xmr+"name")	XMLExplorer->GetContent(name, true);
						else if(str3=="description" || str3==xmr+"description") XMLExplorer->GetContent(description, true);
						else if(str3=="singletype" || str3==xmr+"singletype")
						{
							if(XMLExplorer->GetContent(str4) && str4=="1") singletype=true;
							else singletype=false;
						}
						else if(str3=="type" || str3==xmr+"type")
						{
							if(XMLExplorer->GetContent(str4) && str4!="")
							{
								if(str4.Pos("plt")==1)
								{
									plt=StrToTProfileLabelType(str4);
									mlt=mltMapLabel;
								}
								else if(str4.Pos("mlt")==1)
								{
									plt=pltNone;
									mlt=StrToTMapLabelType(str4);
								}
							}
						}
						else if(str3=="thickness" || str3==xmr+"thickness")
						{
							if(XMLExplorer->GetContent(str4) && str4!="")
							{
								try
								{
									thickness=StrToInt(str4);
								}
								catch (Exception &e) {}
							}
						}
						else if(str3=="color" || str3==xmr+"color")
						{
							if(XMLExplorer->GetContent(str4) && str4!="")
							{
								try
								{
									color=TColor32(StrToInt(str4));
								}
								catch (Exception &e) {}
							}
						}
						else if(str3=="bordercolor" || str3==xmr+"bordercolor")
						{
							if(XMLExplorer->GetContent(str4) && str4!="")
							{
								try
								{
									bordercolor=TColor32(StrToInt(str4));
								}
								catch (Exception &e) {}
							}
						}
						/*if(str3=="gml:LineString")
						{
							if(XMLExplorer->GetAttribute("srsDimension", str3))
							{
								try
								{
									srsDimension=StrToInt(str3);
								}
								catch(Exception &e) {srsDimension=2;}
							}
							else srsDimension=2;
							XMLExplorer->GetContent(posList);
						}*/
						else if((str3=="mapLabelsIDs" || str3==xmr+"mapLabelsIDs")) XMLExplorer->GetContent(LabelsIDs);
					}
					if(mapobjects && LabelsIDs!=NULL && LabelsIDs.Length()>0)
					{
						if(mlgt==mlgtPolyLine) mlg=new TMapLabelPolyLineGeometry(mapobjects, mapobjects->ViewPort, mapobjects->Container, this);
						else if(mlgt==mlgtLine) mlg=new TMapLabelLineGeometry(mapobjects, mapobjects->ViewPort, mapobjects->Container, this);
						else mlg=NULL;
						if(mlg)
						{
							mapobjects->CustomObjectsList->SendToBack(mlg->Index);
							try
							{
								k=1;
								while(k<=LabelsIDs.Length())
								{
									nr="";
									while(k<=LabelsIDs.Length() && LabelsIDs[k]>='0' && LabelsIDs[k]<='9') nr+=LabelsIDs[k++];
									k++;
									try {li=StrToInt(nr);}
									catch(Exception &e) {li=-1;}
									ml=mapobjects->GetMapLabel(li);
									if(ml) mlg->AddPoint(ml);
								}
							}
							__finally
							{
								if(mlg->VertexesCount<=1)
								{
									delete mlg;
									mlg=NULL;
								}
								else
								{
									mlg->ID=id;
									mlg->Name=name;
									mlg->Description=description;
									mlg->LabelType=plt;
									mlg->MapLabelType=mlt;
									mlg->SingleLabelType=singletype;
									mlg->ChangeDesign(thickness, color, bordercolor);
								}
							}
						}
					}

				}
				else if(str==xmr+"mapBriefInfo")
				{
					UnicodeString ustr, ustr2;
					int i;

					if(Satellites)
					{
						str3="";
						XMLExplorer->GetContent(ustr, true);
						if(ustr=="" && (XMLExplorer->CurrentTag == "info" || XMLExplorer->CurrentTag == xmr+"info"))
							XMLExplorer->GetContent(ustr, true);
						if(ustr!="")
						{
							if(Widestrutils::DetectUTF8Encoding((System::RawByteString)ustr)==TEncodeType::etUTF8)
								ustr2=(AnsiString)System::UTF8ToString((System::RawByteString)ustr);
							else ustr2=ustr;
							i=ustr2.Pos("<![CDATA[");
							while(i>0)
							{
								ustr2=ustr2.SubString(1, i-1)+ustr2.SubString(i+9, ustr2.Length()-(i+9)+1);
								i=ustr2.Pos("<![CDATA[");
							}
							i=ustr2.Pos("]]>");
							while(i>0)
							{
								ustr2=ustr2.SubString(1, i-1)+ustr2.SubString(i+3, ustr2.Length()-(i+3)+1);
								i=ustr2.Pos("]]>");
							}
							Satellites->BriefInfo=ustr2;
						}
					}
				}
				else if(str==xmr+"mapGeneratedInfo")
				{
					UnicodeString ustr, ustr2;
					int i;

					if(Satellites)
					{
						str3="";
						XMLExplorer->GetContent(ustr, true);
						if(ustr=="" && (XMLExplorer->CurrentTag == "info" || XMLExplorer->CurrentTag == xmr+"info"))
							XMLExplorer->GetContent(ustr, true);
						if(ustr!="")
						{
							if(Widestrutils::DetectUTF8Encoding((System::RawByteString)ustr)==TEncodeType::etUTF8)
								ustr2=(AnsiString)System::UTF8ToString((System::RawByteString)ustr);
							else ustr2=ustr;
							i=ustr2.Pos("<![CDATA[");
							while(i>0)
							{
								ustr2=ustr2.SubString(1, i-1)+ustr2.SubString(i+9, ustr2.Length()-(i+9)+1);
								i=ustr2.Pos("<![CDATA[");
							}
							i=ustr2.Pos("]]>");
							while(i>0)
							{
								ustr2=ustr2.SubString(1, i-1)+ustr2.SubString(i+3, ustr2.Length()-(i+3)+1);
								i=ustr2.Pos("]]>");
							}
							Satellites->GeneratedInfo=ustr2;
						}
					}
				}
			}
			ResultsNotSaved=false;
		}
		__finally
		{
			delete XMLExplorer;
		}
	}
	SwitchToPath(CurrentPath);
}
//---------------------------------------------------------------------------

void __fastcall TRadarMapManager::SaveResultsToGML(AnsiString FileName)
{
	TStrings* Strs;
	bool MapExists=false;
	bool Err=false;
	AnsiString str, crsURI;
	double depth;
	TCoordinateSystem CS;

	Strs=new TStringList();
	Strs->Add("<?xml version='1.0' encoding='UTF-8'?>");
	//Strs->Add("<RadarMap xsi:schemaLocation='http://www.radsys.lv radarmap.xsd' xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:gml=\"http://www.opengis.net/gml\">");
	Strs->Add("<xrm:RadarMap gml:id=\"xrm-RM-"+_RadarMapSmallVersion+"."+_RadarMapBuild+"-"+ //(AnsiString)_RadarMapVersion0+//
		IntToStr(Settings->RadarMapOutputsCounter)+"\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:gml=\"http://www.opengis.net/gml\" xmlns:fme=\"http://www.safe.com/gml/fme\" xsi:schemaLocation=\"xmlRadarMap http://"+((AnsiString)_RadarMapWeb).LowerCase() +"/gml/2_5_0/radarmap.xsd\" xmlns:xrm=\"xmlRadarMap\">");
	Strs->Add("  <gml:description>RadarMap output results. More details on "+(AnsiString)_RadarMapWeb+"</gml:description>");
	try
	{
		if(MapObjects && MapObjects->Container)
		{
			CS=MapObjects->Container->CoordinateSystem;
			if(CS==csUTM) CS=csLatLon;
			if(MapObjects->Container->Count>0)
			{
				TGPSCoordinate *c1, *c2;
				TMapsContainer *mc;

				c1=MapObjects->Container->LatLonRect->LeftTop;
				c2=MapObjects->Container->LatLonRect->RightBottom;
				if(!c1->AutomaticUpdate)
				{
					c1->AutomaticUpdate=true;
					c2->AutomaticUpdate=true;
					c1->AutomaticUpdate=false;
					c2->AutomaticUpdate=false;
				}
				Strs->Add("  <gml:boundedBy>");
				//Strs->Add("    <gml:Envelope>");
				if(CS==csUTM) crsURI=c1->GetCSURI(csLatLon);
				else crsURI=c1->GetCSURI(CS);
				Strs->Add("    <gml:Envelope srsName=\""+crsURI+"\" srsDimension=\"2\">");
				if(CS==csLatLon || CS==csUTM)
				{
					Strs->Add("      <gml:lowerCorner>"+FloatToStrF(c2->Latitude, ffFixed, 11, 6)+" "+
						FloatToStrF(c1->Longitude, ffFixed, 11, 6)+"</gml:lowerCorner>");
					Strs->Add("      <gml:upperCorner>"+FloatToStrF(c1->Latitude, ffFixed, 11, 6)+" "+
						FloatToStrF(c2->Longitude, ffFixed, 11, 6)+"</gml:upperCorner>");
				}
				else
				{
					Strs->Add("      <gml:lowerCorner>"+FloatToStrF(c1->GetX(CS), ffFixed, 10, 3)+" "+
						FloatToStrF(c2->GetY(CS), ffFixed, 10, 3)+"</gml:lowerCorner>");
					Strs->Add("      <gml:upperCorner>"+FloatToStrF(c2->GetX(CS), ffFixed, 10, 3)+" "+
						FloatToStrF(c1->GetY(CS), ffFixed, 10, 3)+"</gml:upperCorner>");
				}
				/*switch(CS)
				{
					case csDutch:
						Strs->Add("  <gml:lowerCorner>"+FloatToStrF(c1->RdX, ffFixed, 10, 3)+" "+
							FloatToStrF(c2->RdY, ffFixed, 10, 3)+"</gml:lowerCorner>");
						Strs->Add("  <gml:upperCorner>"+FloatToStrF(c2->RdX, ffFixed, 10, 3)+" "+
							FloatToStrF(c1->RdY, ffFixed, 10, 3)+"</gml:upperCorner>");
						break;
					case csBeLambert08:
					case csBeLambert72:
						Strs->Add("  <gml:lowerCorner>"+FloatToStrF(c1->BeX, ffFixed, 10, 3)+" "+
							FloatToStrF(c2->BeY, ffFixed, 10, 3)+"</gml:lowerCorner>");
						Strs->Add("  <gml:upperCorner>"+FloatToStrF(c2->BeX, ffFixed, 10, 3)+" "+
							FloatToStrF(c1->BeY, ffFixed, 10, 3)+"</gml:upperCorner>");
						break;
					//case csUTM:
					//	Strs->Add("  <gml:lowerCorner>"+FloatToStrF(c1->UtmX, ffFixed, 10, 3)+" "+
					//		FloatToStrF(c2->UtmY, ffFixed, 10, 3)+"</gml:lowerCorner>");
					//	Strs->Add("  <gml:upperCorner>"+FloatToStrF(c2->UtmX, ffFixed, 10, 3)+" "+
					//		FloatToStrF(c1->UtmY, ffFixed, 10, 3)+"</gml:upperCorner>");
					//	break;
					case csLKS:
						Strs->Add("  <gml:lowerCorner>"+FloatToStrF(c1->LksX, ffFixed, 10, 3)+" "+
							FloatToStrF(c2->LksY, ffFixed, 10, 3)+"</gml:lowerCorner>");
						Strs->Add("  <gml:upperCorner>"+FloatToStrF(c2->LksX, ffFixed, 10, 3)+" "+
							FloatToStrF(c1->LksY, ffFixed, 10, 3)+"</gml:upperCorner>");
						break;
					case csUTM:
					case csLatLon:
					default:
						Strs->Add("  <gml:lowerCorner>"+FloatToStrF(c2->Latitude, ffFixed, 11, 6)+" "+
							FloatToStrF(c1->Longitude, ffFixed, 11, 6)+"</gml:lowerCorner>");
						Strs->Add("  <gml:upperCorner>"+FloatToStrF(c1->Latitude, ffFixed, 11, 6)+" "+
							FloatToStrF(c2->Longitude, ffFixed, 11, 6)+"</gml:upperCorner>");
						break;
				}*/
				Strs->Add("    </gml:Envelope>");
				Strs->Add("  </gml:boundedBy>");
				for(int i=0; i<MapObjects->Container->Count; i++)
				{
					mc=MapObjects->Container->GetObject(i);
					if(mc)
					{
						Strs->Add("  <gml:featureMember>");
						Strs->Add("    <xrm:map  gml:id=\"xrm-map-"+mc->Klicnummer+"\">");
						Strs->Add("      <xrm:filename>"+mc->FileName+"</xrm:filename>");
						Strs->Add("      <xrm:Klicnummer>"+mc->Klicnummer+"</xrm:Klicnummer>");
						//if(CS==csUTM)
						//	Strs->Add("      <xrm:CoordinateSystem>"+c1->GetCSCodeName(csLatLon)+"</xrm:CoordinateSystem>");
						//else Strs->Add("      <xrm:CoordinateSystem>"+c1->GetCSCodeName(CS)+"</xrm:CoordinateSystem>");
						Strs->Add("      <xrm:CoordinateSystem>"+c1->GetCSCodeName(CS)+"</xrm:CoordinateSystem>");
						str=c1->GetCSGUID(CS);
						if(IsValidGUID(str))
						{
							Strs->Add("      <xrm:csGUID>"+str+"</xrm:csGUID>");
							if(CS==csDLL && PlugInManager)
							{
								TPlugInObject *plugin;

								plugin=PlugInManager->GetByGUID(str, pitCRS);
								if(plugin) Strs->Add("      <xrm:csPlugIn>"+ExtractFileName(plugin->DLLFileName)+", "+plugin->PlugInID+"</xrm:csPlugIn>");
								if(plugin) Strs->Add("      <xrm:csPlugInName>"+c1->GetCSName(CS)+"</xrm:csPlugInName>");
							}
						}
						str=c1->GetPROJ4(CS);
						if(str!=NULL && str.Length()>0)
						{
							Strs->Add("      <xrm:csPROJ4>");
							Strs->Add("        <xrm:csPROJ4Prefix>"+c1->GetCSPrefix(CS)+"</xrm:csPROJ4Prefix>");
							Strs->Add("        <xrm:csPROJ4Name>"+c1->GetCSName(CS)+"</xrm:csPROJ4Name>");
							Strs->Add("        <xrm:csPROJ4Ellipsoid>"+c1->GetCSEllipsoid(CS)+"</xrm:csPROJ4Ellipsoid>");
							Strs->Add("        <xrm:csPROJ4EPSG>"+c1->GetCSURI(CS)+"</xrm:csPROJ4EPSG>");
							Strs->Add("        <xrm:csPROJ4Unit>"+IntToStr((int)c1->GetCSDimension(CS))+"</xrm:csPROJ4Unit>");
							Strs->Add("        <xrm:csPROJ4Definition>"+str+"</xrm:csPROJ4Definition>");
							Strs->Add("      </xrm:csPROJ4>");
						}
						Strs->Add("    </xrm:map>");
						Strs->Add("  </gml:featureMember>");
						MapExists=true;
					}
				}
			}
			if(MapExists)
			{
				TProfileObject *po, *po2;
				TMapLabelGeometry *mlg;
				TMapLabel *ml;
				TMapPathObject *mpo;
				TStrings *maps;
				TGPSCoordinate *c;
				AnsiString s, s2;
				TProfileSatellites *ps;

				maps=new TStringList;
				try
				{
					for(int k=0; k<SatellitesList->Count; k++)
					{
						try
						{
							ps=(TProfileSatellites*)SatellitesList->Items[k];
							if(ps->Path)// && ps->Path->Count>0)
							{
								s2=FileName;
								if(s2.Length()>0)
								{
									if(s2[s2.Length()-3]=='.') s2.SetLength(s2.Length()-4);
									else FileName=FileName+".gml";
								}
								else throw(Exception("Wrong File Name!"));
								s2+="_Path"+IntToStr(k+1);
								for(int z=0; z<ps->ProfilesCount; z++)
								{
									if(ps->Profiles[z]) //<profile>
									{
										s=s2;
										if(ps->ProfilesCount>1) s+="_Ch"+IntToStr(z+1);
										s+=".sgy";
										if(Output_SEGY(s.c_str(), ps->Profiles[z])==0) //success
										{
											ps->Profiles[z]->FullName=s;
											s=ExtractFileName(s);
											maps->Add("      <xrm:profile>"+s+"</xrm:profile>");
										}
									}
								}
								maps->Add("      <xrm:name>"+ps->Path->Name+"</xrm:name>");
								s=""; s2="";
								for(int j=0; j<ps->Path->Count; j++) //<gml:LineString>
								{
									c=ps->Path->Points[j];
									if(!c->AutomaticUpdate)
									{
										c->AutomaticUpdate=true;
										c->AutomaticUpdate=false;
									}
									if(CS==csLatLon || CS==csUTM)
										s+=(FloatToStrF(c->Latitude, ffFixed, 11, 6)+" "+FloatToStrF(c->Longitude, ffFixed, 11, 6)+" ");
									else
										s+=(FloatToStrF(c->GetX(CS), ffFixed, 10, 3)+" "+
											FloatToStrF(c->GetY(CS), ffFixed, 10, 3)+" ");
									if(Settings->Gml3Dpoints) s+=FloatToStrF(c->GetH(CS), ffFixed, 10, 3)+" ";
									if(c->Trace) s2+=(IntToStr(c->Trace->Index)+" ");
									else s2+="-1 ";
								}
								s=s.Trim();
								s2=s2.Trim();
								if(s.Length()>0)
								{
									/*maps->Add("      <points>");
									maps->Add("        <coordinates>");
									maps->Add("          <gml:LineString><gml:posList>"+s+"</gml:posList></gml:LineString>");
									maps->Add("        </coordinates>");
									maps->Add("        <traceindexes>"+s2+"</traceindexes>");
									maps->Add("      </points>");*/
									maps->Add("      <gml:curveProperty>");
									maps->Add("        <gml:LineString srsName=\""+crsURI+"\" srsDimension=\""+(Settings->Gml3Dpoints ? "3" : "2")+"\">");
									maps->Add("          <gml:posList>"+s+"</gml:posList>");
									maps->Add("        </gml:LineString>");
									maps->Add("      </gml:curveProperty>");
									maps->Add("      <xrm:traceindexes>"+s2+"</xrm:traceindexes>");
								}
								for(int i=0; i<ps->CustomObjects->Count; i++)
								{
									try
									{
										po=dynamic_cast<TProfileObject*>(ps->CustomObjects->Items[i]);
										if(po && po->ProfileObjectType==potProfileLabel)
										{
											ml=(TMapLabel*)((TProfileLabel*)po)->ConnectedObject;
											if(ml && ml->ProfileObjectType==potMapLabel)
											{
												if(ml->Trace!=NULL) //<label>
												{
													//maps->Add("      <label>");
													maps->Add("      <xrm:mapPathLabel gml:id=\"xrm-label-"+IntToStr((int)ml->ID)+"\">");
													s="";
													switch((TProfileLabelType)ml->Tag)
													{
														case pltVertLine: 		 s="pltVertLine"; break;
														case pltTransformed: 	 s="pltTransformed"; break;
														case pltCircleRed: 		 s="pltCircleRed"; break;
														case pltCircleGreen: 	 s="pltCircleGreen"; break;
														case pltCircleBlue: 	 s="pltCircleBlue"; break;
														case pltSquareRed: 		 s="pltSquareRed"; break;
														case pltSquareGreen: 	 s="pltSquareGreen"; break;
														case pltSquareBlue: 	 s="pltSquareBlue"; break;
														case pltTriangleRed: 	 s="pltTriangleRed"; break;
														case pltTriangleGreen: 	 s="pltTriangleGreen"; break;
														case pltTriangleBlue: 	 s="pltTriangleBlue"; break;
														case pltWaterCircle:     s="pltWaterCircle"; break;
														case pltWaterRect:       s="pltWaterRect"; break;
														case pltWaterTriangle:   s="pltWaterTriangle"; break;
														case pltElectroCircle:   s="pltElectroCircle"; break;
														case pltElectroRect:     s="pltElectroRect"; break;
														case pltElectroTriangle: s="pltElectroTriangle"; break;
														case pltGasCircle:       s="pltGasCircle"; break;
														case pltGasRect:         s="pltGasRect"; break;
														case pltGasTriangle:     s="pltGasTriangle"; break;
														case pltGasHighCircle:   s="pltGasHighCircle"; break;
														case pltGasHighRect:     s="pltGasHighRect"; break;
														case pltGasHighTriangle: s="pltGasHighTriangle"; break;
														case pltSewageCircle:    s="pltSewageCircle"; break;
														case pltSewageRect:      s="pltSewageRect"; break;
														case pltSewageTriangle:  s="pltSewageTriangle"; break;
														case pltTelecomCircle:   s="pltTelecomCircle"; break;
														case pltTelecomRect:     s="pltTelecomRect"; break;
														case pltTelecomTriangle: s="pltTelecomTriangle"; break;
														case pltFiberCircle:     s="pltFiberCircle"; break;
														case pltFiberRect:       s="pltFiberRect"; break;
														case pltFiberTriangle:   s="pltFiberTriangle"; break;
														case pltAutoDetect:	     s="pltAutoDetect"; break;
														case pltAutoDetectPos:	 s="pltAutoDetectPos"; break;
														case pltAutoDetectNeg:	 s="pltAutoDetectNeg"; break;
														default: s="";
													}
													maps->Add("        <xrm:type>"+s+"</xrm:type>");
													maps->Add("        <xrm:color>"+IntToStr((int)ml->Color)+"</xrm:color>");
													if(ml->ConnectedObject && ml->ConnectedObject->ProfileObjectType==potProfileLabel)
														depth=((TProfileLabel*)ml->ConnectedObject)->Z;
													else depth=0.;
													maps->Add("        <xrm:depth>"+FloatToStrF(depth, ffFixed, 10, 2)+"</xrm:depth>");
													maps->Add("        <xrm:profileindex>"+IntToStr(ml->ProfileIndex)+"</xrm:profileindex>");
													maps->Add("        <xrm:traceindex>"+IntToStr(ml->TraceIndex)+"</xrm:traceindex>");
													maps->Add("        <xrm:locked>"+IntToStr((int)((TProfileLabel*)ml->ConnectedObject)->Locked)+"</xrm:locked>");
													maps->Add("        <xrm:sample>"+IntToStr(((TProfileLabel*)po)->Sample)+"</xrm:sample>");
													c=new TGPSCoordinate(NULL, ml->Trace);
													try
													{
														/*if(CS==csLatLon || CS==csUTM)
															maps->Add("        <point><gml:pos>"+FloatToStrF(c->Latitude, ffFixed, 11, 6)+" "+
																FloatToStrF(c->Longitude, ffFixed, 11, 6)+"</gml:pos></point>");
														else
															maps->Add("        <point><gml:pos>"+FloatToStrF(c->GetX(CS), ffFixed, 10, 3)+" "+
																FloatToStrF(c->GetY(CS), ffFixed, 10, 3)+"</gml:pos></point>");*/
														maps->Add("        <gml:pointProperty>");
														maps->Add("          <gml:Point srsName=\""+crsURI+"\" srsDimension=\""+(Settings->Gml3Dpoints ? "3" : "2")+"\">"); //srsDimension=\"3\">");
														depth*=-1.;
														if(CS==csLatLon || CS==csUTM)
															maps->Add("            <gml:pos>"+FloatToStrF(c->Latitude, ffFixed, 11, 6)+" "+
																FloatToStrF(c->Longitude, ffFixed, 11, 6)+" "+
																(Settings->Gml3Dpoints ? (AnsiString)FloatToStrF(depth, ffFixed, 10, 2) : (AnsiString)"")+"</gml:pos>");
														else
															maps->Add("            <gml:pos>"+FloatToStrF(c->GetX(CS), ffFixed, 10, 3)+" "+
																FloatToStrF(c->GetY(CS), ffFixed, 10, 3)+" "+
																(Settings->Gml3Dpoints ? (AnsiString)FloatToStrF(depth, ffFixed, 10, 2) : (AnsiString)"")+"</gml:pos>");
														maps->Add("          </gml:Point>");
														maps->Add("        </gml:pointProperty>");
													}
													__finally
													{
														delete c;
													}
													if(((TProfileLabel*)po)->Description!="")
														maps->Add("        <xrm:description>"+((TProfileLabel*)po)->Description+"</xrm:description>");
													if(((TProfileLabel*)po)->Attachment!="")
													{
														AnsiString nfn, fn=((TProfileLabel*)po)->Attachment;

														if(Settings->CopyAttachments)
														{
															AnsiString Dir=ExtractFilePath(FileName);

															if(Dir!=ExtractFilePath(fn))
															{
																try
																{
																	nfn=Dir+ExtractFileName(fn);
																	CopyFile(fn.c_str(), nfn.c_str(), false);
																	fn=nfn;
																}
																catch(Exception &e) {}
															}
														}
														maps->Add("        <xrm:linkedfile>"+fn+"</xrm:linkedfile>");
													}
													maps->Add("        <xrm:created>"+MyDateTimeToStr(((TProfileLabel*)po)->CreatedTime)+"</xrm:created>");
													maps->Add("        <xrm:modified>"+MyDateTimeToStr(((TProfileLabel*)po)->ModifiedTime)+"</xrm:modified>");
													maps->Add("        <xrm:confidence>"+FloatToStrF(((TProfileLabel*)po)->GPSConfidence, ffFixed, 10, 3)+"</xrm:confidence>");
													maps->Add("        <xrm:ID>"+IntToStr((int)((TProfileLabel*)po)->ID)+"</xrm:ID>");
													//maps->Add("      </label>");
													maps->Add("      </xrm:mapPathLabel>");
												}
											}
										}
									}
									catch(Exception &e) {maps->Clear();}
								}
								if(maps->Count>0)
								{
									Strs->Add("  <gml:featureMember>");
									Strs->Add("    <xrm:mapPath gml:id=\"xrm-path-"+IntToStr(k+1)+"\">");
									Strs->AddStrings(maps);
									Strs->Add("    </xrm:mapPath>");
									Strs->Add("  </gml:featureMember>");
									maps->Clear();
								}
							}
						}
						catch(Exception &e) {maps->Clear();}
					}
					if(mapobjects)
					{
						try
						{
							for(int k=0; k<mapobjects->CustomObjectsCount; k++)
							{
								po=dynamic_cast<TProfileObject*>(mapobjects->CustomObjectsList->Items[k]);
								if(po && po->ProfileObjectType==potMapLabel)
								{
									ml=dynamic_cast<TMapLabel*>(po);
									if(ml && !ml->ConnectedObject)
									{
										Err=false;
										maps->Clear();
										try
										{
											maps->Add("  <gml:featureMember>");
											maps->Add("    <xrm:mapLabel gml:id=\"xrm-label-"+IntToStr((int)ml->ID)+"\">");
											s="potMapLabel";
											maps->Add("      <xrm:type>"+s+"</xrm:type>");
											maps->Add("      <xrm:color>"+IntToStr((int)ml->Color)+"</xrm:color>");
											c=ml->Coordinate;
											maps->Add("      <gml:pointProperty>");
											maps->Add("        <gml:Point srsName=\""+crsURI+"\" srsDimension=\""+(Settings->Gml3Dpoints ? "3" : "2")+"\">");//srsDimension=\"3\">");
											if(CS==csLatLon || CS==csUTM)
												maps->Add("          <gml:pos>"+FloatToStrF(c->Latitude, ffFixed, 11, 6)+" "+
													FloatToStrF(c->Longitude, ffFixed, 11, 6)+" "+
													(Settings->Gml3Dpoints ? (AnsiString)FloatToStrF(c->Z_Geoid, ffFixed, 10, 2) : (AnsiString)"")+"</gml:pos>");
											else
												maps->Add("          <gml:pos>"+FloatToStrF(c->GetX(CS), ffFixed, 10, 3)+" "+
													FloatToStrF(c->GetY(CS), ffFixed, 10, 3)+" "+
													(Settings->Gml3Dpoints ? (AnsiString)FloatToStrF(c->GetH(CS), ffFixed, 10, 2) : (AnsiString)"")+"</gml:pos>");
											maps->Add("        </gml:Point>");
											maps->Add("      </gml:pointProperty>");
											if(ml->Description!="")
												maps->Add("      <xrm:description>"+ml->Description+"</xrm:description>");
											if(ml->Attachment!="")
											{
												AnsiString nfn, fn=ml->Attachment;

												if(Settings->CopyAttachments)
												{
													AnsiString Dir=ExtractFilePath(FileName);

													if(Dir!=ExtractFilePath(fn))
													{
														try
														{
															nfn=Dir+ExtractFileName(fn);
															CopyFile(fn.c_str(), nfn.c_str(), false);
															fn=nfn;
														}
														catch(Exception &e) {}
													}
												}
												maps->Add("      <xrm:linkedfile>"+fn+"</xrm:linkedfile>");
											}
											maps->Add("      <xrm:created>"+MyDateTimeToStr(ml->CreatedTime)+"</xrm:created>");
											maps->Add("      <xrm:modified>"+MyDateTimeToStr(ml->ModifiedTime)+"</xrm:modified>");
											maps->Add("      <xrm:confidence>"+FloatToStrF(ml->GPSConfidence, ffFixed, 10, 3)+"</xrm:confidence>");
											maps->Add("      <xrm:ID>"+IntToStr((int)ml->ID)+"</xrm:ID>");
											maps->Add("    </xrm:mapLabel>");
											maps->Add("  </gml:featureMember>");
										}
										catch(Exception &e) {Err=true;}
										if(!Err && maps->Count>0)
										{
											Strs->AddStrings(maps);
											maps->Clear();
										}
									}
								}
							}
							for(int k=0; k<mapobjects->CustomObjectsCount; k++)
							{
								po=dynamic_cast<TProfileObject*>(mapobjects->CustomObjectsList->Items[k]);
								if(po && po->ProfileObjectType==potMapLabelGeometry)
								{
									TMapLabelGeometry *mlg;
									mlg=dynamic_cast<TMapLabelGeometry*>(po);
									if(mlg && mlg->GeometryType==mlgtPolyLine)
									{
										Err=false;
										maps->Clear();
										try
										{
											maps->Add("  <gml:featureMember>");
											maps->Add("    <xrm:mapLabelGeometry gml:id=\"xrm-label-geometry-"+IntToStr((int)mlg->ID)+"\">");
											maps->Add("      <xrm:mapLabelGeometryKind>"+TMapLabelGeometryTypeStrings[mlg->GeometryType]+"</xrm:mapLabelGeometryKind>");
											maps->Add("      <xrm:ID>"+IntToStr((int)mlg->ID)+"</xrm:ID>");
											maps->Add("      <xrm:name>"+mlg->Name+"</xrm:name>");
											if(mlg->Description!="") maps->Add("      <xrm:description>"+mlg->Description+"</xrm:description>");
											maps->Add("      <xrm:singletype>"+IntToStr((int)mlg->SingleLabelType)+"</xrm:singletype>");
											if(mlg->LabelType==pltNone) s=TMapLabelTypeStrings[mlg->MapLabelType];
											else s=TProfileLabelTypeStrings[(int)mlg->LabelType];
											maps->Add("      <xrm:type>"+s+"</xrm:type>");
											maps->Add("      <xrm:thickness>"+IntToStr((int)mlg->Thickness)+"</xrm:thickness>");
											maps->Add("      <xrm:color>"+IntToStr((int)mlg->Color)+"</xrm:color>");
											maps->Add("      <xrm:bordercolor>"+IntToStr((int)mlg->BorderColor)+"</xrm:bordercolor>");
											s=""; s2="";
											for(int j=0; j<mlg->VertexesCount; j++) //<gml:LineString>
											{
												c=mlg->Vertexes[j];
												if(!c->AutomaticUpdate)
												{
													c->AutomaticUpdate=true;
													c->AutomaticUpdate=false;
												}
												if(CS==csLatLon || CS==csUTM)
													s+=(FloatToStrF(c->Latitude, ffFixed, 11, 6)+" "+FloatToStrF(c->Longitude, ffFixed, 11, 6)+" ");
												else
													s+=(FloatToStrF(c->GetX(CS), ffFixed, 10, 3)+" "+
														FloatToStrF(c->GetY(CS), ffFixed, 10, 3)+" ");
												if(Settings->Gml3Dpoints) s+=FloatToStrF(c->GetH(CS), ffFixed, 10, 3)+" ";
												if(mlg->Labels[j]) s2+=IntToStr((int)mlg->Labels[j]->ID)+" ";
												else s2+="-1 ";
											}
											s=s.Trim();
											s2=s2.Trim();
											if(s.Length()>0)
											{
												maps->Add("      <gml:curveProperty>");
												maps->Add("        <gml:LineString srsName=\""+crsURI+"\" srsDimension=\""+(Settings->Gml3Dpoints ? "3" : "2")+"\">");
												maps->Add("          <gml:posList>"+s+"</gml:posList>");
												maps->Add("        </gml:LineString>");
												maps->Add("      </gml:curveProperty>");
												maps->Add("      <xrm:mapLabelsIDs>"+s2+"</xrm:mapLabelsIDs>");
											}
											maps->Add("    </xrm:mapLabelGeometry>");
											maps->Add("  </gml:featureMember>");
										}
										catch(Exception &e) {Err=true;}
										if(!Err && maps->Count>0)
										{
											Strs->AddStrings(maps);
											maps->Clear();
										}
                                    }
								}
							}
						}
						catch(Exception &e) {}
					}
					if(Satellites)
					{
						try
						{
							Satellites->GenerateInfo();
							maps->Clear();
							Err=false;
							try
							{
								maps->Add("  <gml:featureMember>");
								maps->Add("    <xrm:mapBriefInfo gml:id=\"xrm-info-1\">");
								//maps->Add("      <xrm:info><![CDATA[");
								maps->Add(System::AnsiToUtf8(UnicodeString("      <xrm:info><![CDATA["+Satellites->BriefInfo+"]]></xrm:info>")));
								//maps->Add("      ]]></xrm:info>");
								maps->Add("    </xrm:mapBriefInfo>");
								maps->Add("  </gml:featureMember>");
								maps->Add("  <gml:featureMember>");
								maps->Add("    <xrm:mapGeneratedInfo gml:id=\"xrm-info-2\">");
								//maps->Add();
								maps->Add(System::AnsiToUtf8(UnicodeString("      <xrm:info><![CDATA["+Satellites->GeneratedInfo+"]]></xrm:info>")));
								//maps->Add("      ]]></xrm:info>");
								maps->Add("    </xrm:mapGeneratedInfo>");
								maps->Add("  </gml:featureMember>");
							}
							catch(Exception &e) {Err=true;}
							if(!Err && maps->Count>0)
							{
								Strs->AddStrings(maps);
								maps->Clear();
							}
						}
						catch(Exception &e) {}
					}
					ResultsNotSaved=false;
				}
				__finally
				{
					delete maps;
				}
			}
		}
	}
	__finally
	{
		Strs->Add("</xrm:RadarMap>");
		//Strs->SaveToFile(FileName);
		Strs->SaveToFile(FileName, TEncoding::UTF8);
		Settings->RadarMapOutputsCounter++;
		delete Strs;
	}
}
//---------------------------------------------------------------------------


TDXFBlock* __fastcall TRadarMapManager::CreateLabelBlock(__int64 *HIndex, AnsiString AName, float Coef)
{
	TDXFBlock* block;
	TDXFEntity *entity;

	block=new TDXFBlock(HIndex, AName);

	entity=new TDXFLine(block);
	entity->ColorPX=clBlack32;
	entity->XYZ.X=0;
	entity->XYZ.Y=Coef*-0.5;
	entity->XYZ.Z=0;
	((TDXFLine*)entity)->Point2.X=0;
	((TDXFLine*)entity)->Point2.Y=Coef*0.5;
	((TDXFLine*)entity)->Point2.Z=0;
	block->Add(entity);
	entity=new TDXFLine(block);
	entity->ColorPX=clBlack32;
	entity->XYZ.X=Coef*-0.5;
	entity->XYZ.Y=0;
	entity->XYZ.Z=0;
	((TDXFLine*)entity)->Point2.X=Coef*0.5;
	((TDXFLine*)entity)->Point2.Y=0;
	((TDXFLine*)entity)->Point2.Z=0;
	block->Add(entity);
	if(Settings->DXFPolyline3D)
	{
		entity=new TDXFLine(block);
		entity->ColorPX=clBlack32;
		entity->XYZ.Z=Coef*-0.5;
		entity->XYZ.Y=0;
		entity->XYZ.X=0;
		((TDXFLine*)entity)->Point2.Z=Coef*0.5;
		((TDXFLine*)entity)->Point2.Y=0;
		((TDXFLine*)entity)->Point2.X=0;
		block->Add(entity);
	}

	return block;
}

void __fastcall TRadarMapManager::ExportResultsToDXF(AnsiString FileName)
{
	bool MapExists=false;
	TDXFCollection *DXF;
	TProfileSatellites *ps;
	double pZ;

	DXF=new TDXFCollection();
	try
	{
		if(MapObjects)
		{
			if(MapObjects->Container && MapObjects->Container->Count>0)
			{
				TGPSCoordinate *c1, *c2;
				TGPSCoordinatesRect *cr1, *cr2;
				TMapsContainer *mc;

				c1=MapObjects->Container->LatLonRect->LeftTop;
				c2=MapObjects->Container->LatLonRect->RightBottom;
				if(!c1->AutomaticUpdate)
				{
					c1->AutomaticUpdate=true;
					c2->AutomaticUpdate=true;
					c1->AutomaticUpdate=false;
					c2->AutomaticUpdate=false;
				}
				cr1=new TGPSCoordinatesRect(true);
				cr2=new TGPSCoordinatesRect(true);
				try
				{
					for(int k=0; k<SatellitesList->Count; k++)
					{
						ps=(TProfileSatellites*)SatellitesList->Items[k];
						if(ps->Path && ps->Path->Count>0 && ps->Path->CoordinatesRect->Width()>0 &&
							ps->Path->CoordinatesRect->Height()>0)
						{
							cr1->SetCorners(c1, ps->Path->CoordinatesRect->LeftTop);
							cr2->SetCorners(c2, ps->Path->CoordinatesRect->RightBottom);
							c1=cr1->LeftTop;
							c2=cr2->RightBottom;
						}
					}
					if(MapObjects->Container->CoordinateSystem==csLatLon)
					{
						DXF->LowerCorner.Y=c2->Latitude;
						DXF->LowerCorner.X=c1->Longitude;
						DXF->UpperCorner.Y=c1->Latitude;
						DXF->UpperCorner.X=c2->Longitude;
					}
					else
					{
						DXF->LowerCorner.X=c1->GetX(MapObjects->Container->CoordinateSystem);
						DXF->LowerCorner.Y=c2->GetY(MapObjects->Container->CoordinateSystem);
						DXF->UpperCorner.X=c2->GetX(MapObjects->Container->CoordinateSystem);
						DXF->UpperCorner.Y=c1->GetY(MapObjects->Container->CoordinateSystem);
					}
				}
				__finally
				{
					delete cr1;
					delete cr2;
				}
				for(int i=0; i<MapObjects->Container->Count; i++)
				{
					mc=MapObjects->Container->GetObject(i);
					if(mc)
					{
						DXF->Comments->Add("Map: "+mc->FileName);
						DXF->Comments->Add("CoordinateSystem: "+c1->GetCSCodeName(MapObjects->Container->CoordinateSystem));
						/*switch(MapObjects->Container->CoordinateSystem)
						{
							case csDutch: DXF->Comments->Add("CoordinateSystem: csDutch"); break;
							case csBeLambert08: DXF->Comments->Add("CoordinateSystem: csBeLambert08"); break;
							case csBeLambert72: DXF->Comments->Add("CoordinateSystem: csBeLambert72"); break;
							case csUTM: DXF->Comments->Add("CoordinateSystem: csUTM"); break;
							case csLKS: DXF->Comments->Add("CoordinateSystem: csLKS"); break;
							case csLatLon:
							default:
								DXF->Comments->Add("CoordinateSystem: csLatLon"); break;
						}*/
						MapExists=true;
					}
					try
					{
						if(mc->MapType==mtDXF && settings->DXFIncludeMap)
						{
							DXF->Import(((TDXFMapsContainer*)mc)->DXFCollection);
						}
					}
					catch(Exception &e)
					{
						DXF->Clear();
					}
				}
			}
			//DXF->SaveData(FileName);

			if(MapExists)
			{
				//Adding Blocks
				TDXFBlock *block;
				TDXFLayer *layer;
				TDXFEntity *entity, *entity2;
				TDXFPolyLine *line;
				TDXFVertex *vertex;
				TDXFInsert *insert;
				TDXFText *text;
				TGPSCoordinate *c;
				TProfileObject *po;
				TMapLabel *ml;
				AnsiString s, s2;
				TDouble3DPoint *EndPoints;
				int N;
				double Coef;

				if(MapObjects->Container->CoordinateSystem==csLatLon) Coef=0.000009; //1/111111.111 // - degries in meter
				else Coef=1;
				/*switch(MapObjects->Container->CoordinateSystem)
				{
					case csDutch:
					case csBeLambert08:
					case csBeLambert72:
					case csUTM:
					case csLKS:
						Coef=1; break;
					case csLatLon:
					default:
						Coef=0.000009; break;// 1/111111.111 // - degries in meter
				}*/
				Coef*=2.;

				block=DXF->GetBlockByName("pltCircleRed");
				if(!block)
				{
					block=CreateLabelBlock(&(DXF->Handle_index), "pltCircleRed", Coef);
					entity=new TDXFCircle(block);
					entity->XYZ=TDouble3DPoint();
					entity->ColorPX=clRed32;
					block->Add(entity);
					DXF->AddBlock(block);
				}

				block=DXF->GetBlockByName("pltCircleGreen");
				if(!block)
				{
					block=CreateLabelBlock(&(DXF->Handle_index), "pltCircleGreen", Coef);
					entity2=new TDXFCircle(block);
					entity2->Import(entity);
					entity2->ColorPX=clGreen32;
					block->Add(entity2);
					DXF->AddBlock(block);
				}

				block=DXF->GetBlockByName("pltCircleBlue");
				if(!block)
				{
					block=CreateLabelBlock(&(DXF->Handle_index), "pltCircleBlue", Coef);
					entity2=new TDXFCircle(block);
					entity2->Import(entity);
					entity2->ColorPX=clBlue32;
					block->Add(entity2);
					DXF->AddBlock(block);
				}

				block=DXF->GetBlockByName("pltWaterCircle");
				if(!block)
				{
					block=CreateLabelBlock(&(DXF->Handle_index), "pltWaterCircle", Coef);
					entity2=new TDXFCircle(block);
					entity2->Import(entity);
					entity2->ColorPX=Color32(47, 158, 194);
					block->Add(entity2);
					DXF->AddBlock(block);
				}

				block=DXF->GetBlockByName("pltElectroCircle");
				if(!block)
				{
					block=CreateLabelBlock(&(DXF->Handle_index), "pltElectroCircle", Coef);
					entity2=new TDXFCircle(block);
					entity2->Import(entity);
					entity2->ColorPX=Color32(194, 47, 47);
					block->Add(entity2);
					DXF->AddBlock(block);
				}

				block=DXF->GetBlockByName("pltGasCircle");
				if(!block)
				{
					block=CreateLabelBlock(&(DXF->Handle_index), "pltGasCircle", Coef);
					entity2=new TDXFCircle(block);
					entity2->Import(entity);
					entity2->ColorPX=Color32(194, 194, 47);
					block->Add(entity2);
					DXF->AddBlock(block);
				}

				block=DXF->GetBlockByName("pltGasHighCircle");
				if(!block)
				{
					block=CreateLabelBlock(&(DXF->Handle_index), "pltGasHighCircle", Coef);
					entity2=new TDXFCircle(block);
					entity2->Import(entity);
					entity2->ColorPX=Color32(230, 115, 0);
					block->Add(entity2);
					DXF->AddBlock(block);
				}

				block=DXF->GetBlockByName("pltSewageCircle");
				if(!block)
				{
					block=CreateLabelBlock(&(DXF->Handle_index), "pltSewageCircle", Coef);
					entity2=new TDXFCircle(block);
					entity2->Import(entity);
					entity2->ColorPX=Color32(194, 47, 194);
					block->Add(entity2);
					DXF->AddBlock(block);
				}

				block=DXF->GetBlockByName("pltTelecomCircle");
				if(!block)
				{
					block=CreateLabelBlock(&(DXF->Handle_index), "pltTelecomCircle", Coef);
					entity2=new TDXFCircle(block);
					entity2->Import(entity);
					entity2->ColorPX=Color32(120, 194, 47);
					block->Add(entity2);
					DXF->AddBlock(block);
				}

				block=DXF->GetBlockByName("pltFiberCircle");
				if(!block)
				{
					block=CreateLabelBlock(&(DXF->Handle_index), "pltFiberCircle", Coef);
					entity2=new TDXFCircle(block);
					entity2->Import(entity);
					entity2->ColorPX=Color32(47, 194, 1207);
					block->Add(entity2);
					DXF->AddBlock(block);
				}

				block=DXF->GetBlockByName("pltSquareRed");
				if(!block)
				{
					block=CreateLabelBlock(&(DXF->Handle_index), "pltSquareRed", Coef);
					entity=new TDXFPolyLine(block);
					entity->ColorPX=clRed32;
					entity->XYZ=TDouble3DPoint();
					((TDXFPolyLine*)entity)->Closed=true;
					vertex=new TDXFVertex(((TDXFPolyLine*)entity)->VertexesContainer);
					vertex->XYZ.X=Coef*-0.4; vertex->XYZ.Y=Coef*-0.4;
					((TDXFPolyLine*)entity)->AddVertex(vertex);
					vertex=new TDXFVertex(((TDXFPolyLine*)entity)->VertexesContainer);
					vertex->XYZ.X=Coef* 0.4; vertex->XYZ.Y=Coef*-0.4;
					((TDXFPolyLine*)entity)->AddVertex(vertex);
					vertex=new TDXFVertex(((TDXFPolyLine*)entity)->VertexesContainer);
					vertex->XYZ.X=Coef* 0.4; vertex->XYZ.Y=Coef* 0.4;
					((TDXFPolyLine*)entity)->AddVertex(vertex);
					vertex=new TDXFVertex(((TDXFPolyLine*)entity)->VertexesContainer);
					vertex->XYZ.X=Coef*-0.4; vertex->XYZ.Y=Coef* 0.4;
					((TDXFPolyLine*)entity)->AddVertex(vertex);
					vertex=new TDXFVertex(((TDXFPolyLine*)entity)->VertexesContainer);
					vertex->XYZ.X=Coef*-0.4; vertex->XYZ.Y=Coef*-0.4;
					((TDXFPolyLine*)entity)->AddVertex(vertex);
					block->Add(entity);
					DXF->AddBlock(block);
				}

				block=DXF->GetBlockByName("pltSquareGreen");
				if(!block)
				{
					block=CreateLabelBlock(&(DXF->Handle_index), "pltSquareGreen", Coef);
					entity2=new TDXFPolyLine(block);
					entity2->Import(entity);
					entity2->ColorPX=clGreen32;
					block->Add(entity2);
					DXF->AddBlock(block);
				}

				block=DXF->GetBlockByName("pltSquareBlue");
				if(!block)
				{
					block=CreateLabelBlock(&(DXF->Handle_index), "pltSquareBlue", Coef);
					entity2=new TDXFPolyLine(block);
					entity2->Import(entity);
					entity2->ColorPX=clBlue32;
					block->Add(entity2);
					DXF->AddBlock(block);
				}

				block=DXF->GetBlockByName("pltWaterRect");
				if(!block)
				{
					block=CreateLabelBlock(&(DXF->Handle_index), "pltWaterRect", Coef);
					entity2=new TDXFPolyLine(block);
					entity2->Import(entity);
					entity2->ColorPX=Color32(47, 158, 194);
					block->Add(entity2);
					DXF->AddBlock(block);
				}

				block=DXF->GetBlockByName("pltElectroRect");
				if(!block)
				{
					block=CreateLabelBlock(&(DXF->Handle_index), "pltElectroRect", Coef);
					entity2=new TDXFPolyLine(block);
					entity2->Import(entity);
					entity2->ColorPX=Color32(194, 47, 47);
					block->Add(entity2);
					DXF->AddBlock(block);
				}

				block=DXF->GetBlockByName("pltGasRect");
				if(!block)
				{
					block=CreateLabelBlock(&(DXF->Handle_index), "pltGasRect", Coef);
					entity2=new TDXFPolyLine(block);
					entity2->Import(entity);
					entity2->ColorPX=Color32(194, 194, 47);
					block->Add(entity2);
					DXF->AddBlock(block);
				}

				block=DXF->GetBlockByName("pltGasHighRect");
				if(!block)
				{
					block=CreateLabelBlock(&(DXF->Handle_index), "pltGasHighRect", Coef);
					entity2=new TDXFPolyLine(block);
					entity2->Import(entity);
					entity2->ColorPX=Color32(230, 115, 0);
					block->Add(entity2);
					DXF->AddBlock(block);
				}

				block=DXF->GetBlockByName("pltSewageRect");
				if(!block)
				{
					block=CreateLabelBlock(&(DXF->Handle_index), "pltSewageRect", Coef);
					entity2=new TDXFPolyLine(block);
					entity2->Import(entity);
					entity2->ColorPX=Color32(194, 47, 194);
					block->Add(entity2);
					DXF->AddBlock(block);
				}

				block=DXF->GetBlockByName("pltTelecomRect");
				if(!block)
				{
					block=CreateLabelBlock(&(DXF->Handle_index), "pltTelecomRect", Coef);
					entity2=new TDXFPolyLine(block);
					entity2->Import(entity);
					entity2->ColorPX=Color32(120, 194, 47);
					block->Add(entity2);
					DXF->AddBlock(block);
				}

				block=DXF->GetBlockByName("pltFiberRect");
				if(!block)
				{
					block=CreateLabelBlock(&(DXF->Handle_index), "pltFiberRect", Coef);
					entity2=new TDXFPolyLine(block);
					entity2->Import(entity);
					entity2->ColorPX=Color32(47, 194, 1207);
					block->Add(entity2);
					DXF->AddBlock(block);
				}

				block=DXF->GetBlockByName("pltTriangleRed");
				if(!block)
				{
					block=CreateLabelBlock(&(DXF->Handle_index), "pltTriangleRed", Coef);

					entity=new TDXFPolyLine(block);
					entity->ColorPX=clRed32;
					entity->XYZ=TDouble3DPoint();
					((TDXFPolyLine*)entity)->Closed=true;
					vertex=new TDXFVertex(((TDXFPolyLine*)entity)->VertexesContainer);
					vertex->XYZ.X=Coef* 0;   vertex->XYZ.Y=Coef* 0.4;
					((TDXFPolyLine*)entity)->AddVertex(vertex);
					vertex=new TDXFVertex(((TDXFPolyLine*)entity)->VertexesContainer);
					vertex->XYZ.X=Coef* 0.4; vertex->XYZ.Y=Coef*-0.3;
					((TDXFPolyLine*)entity)->AddVertex(vertex);
					vertex=new TDXFVertex(((TDXFPolyLine*)entity)->VertexesContainer);
					vertex->XYZ.X=Coef*-0.4; vertex->XYZ.Y=Coef*-0.3;
					((TDXFPolyLine*)entity)->AddVertex(vertex);
					vertex=new TDXFVertex(((TDXFPolyLine*)entity)->VertexesContainer);
					vertex->XYZ.X=Coef* 0;   vertex->XYZ.Y=Coef* 0.4;
					((TDXFPolyLine*)entity)->AddVertex(vertex);
					block->Add(entity);

					DXF->AddBlock(block);
				}

				block=DXF->GetBlockByName("pltTriangleGreen");
				if(!block)
				{
					block=CreateLabelBlock(&(DXF->Handle_index), "pltTriangleGreen", Coef);
					entity2=new TDXFPolyLine(block);
					entity2->Import(entity);
					entity2->ColorPX=clGreen32;
					block->Add(entity2);
					DXF->AddBlock(block);
				}

				block=DXF->GetBlockByName("pltTriangleBlue");
				if(!block)
				{
					block=CreateLabelBlock(&(DXF->Handle_index), "pltTriangleBlue", Coef);
					entity2=new TDXFPolyLine(block);
					entity2->Import(entity);
					entity2->ColorPX=clBlue32;
					block->Add(entity2);
					DXF->AddBlock(block);
				}

				block=DXF->GetBlockByName("pltWaterTriangle");
				if(!block)
				{
					block=CreateLabelBlock(&(DXF->Handle_index), "pltWaterTriangle", Coef);
					entity2=new TDXFPolyLine(block);
					entity2->Import(entity);
					entity2->ColorPX=Color32(47, 158, 194);
					block->Add(entity2);
					DXF->AddBlock(block);
				}

				block=DXF->GetBlockByName("pltElectroTriangle");
				if(!block)
				{
					block=CreateLabelBlock(&(DXF->Handle_index), "pltElectroTriangle", Coef);
					entity2=new TDXFPolyLine(block);
					entity2->Import(entity);
					entity2->ColorPX=Color32(194, 47, 47);
					block->Add(entity2);
					DXF->AddBlock(block);
				}

				block=DXF->GetBlockByName("pltGasTriangle");
				if(!block)
				{
					block=CreateLabelBlock(&(DXF->Handle_index), "pltGasTriangle", Coef);
					entity2=new TDXFPolyLine(block);
					entity2->Import(entity);
					entity2->ColorPX=Color32(194, 194, 47);
					block->Add(entity2);
					DXF->AddBlock(block);
				}

				block=DXF->GetBlockByName("pltGasHighTriangle");
				if(!block)
				{
					block=CreateLabelBlock(&(DXF->Handle_index), "pltGasHighTriangle", Coef);
					entity2=new TDXFPolyLine(block);
					entity2->Import(entity);
					entity2->ColorPX=Color32(230, 115, 0);
					block->Add(entity2);
					DXF->AddBlock(block);
				}

				block=DXF->GetBlockByName("pltSewageTriangle");
				if(!block)
				{
					block=CreateLabelBlock(&(DXF->Handle_index), "pltSewageTriangle", Coef);
					entity2=new TDXFPolyLine(block);
					entity2->Import(entity);
					entity2->ColorPX=Color32(194, 47, 194);
					block->Add(entity2);
					DXF->AddBlock(block);
				}

				block=DXF->GetBlockByName("pltTelecomTriangle");
				if(!block)
				{
					block=CreateLabelBlock(&(DXF->Handle_index), "pltTelecomTriangle", Coef);
					entity2=new TDXFPolyLine(block);
					entity2->Import(entity);
					entity2->ColorPX=Color32(120, 194, 47);
					block->Add(entity2);
					DXF->AddBlock(block);
				}

				block=DXF->GetBlockByName("pltFiberTriangle");
				if(!block)
				{
					block=CreateLabelBlock(&(DXF->Handle_index), "pltFiberTriangle", Coef);
					entity2=new TDXFPolyLine(block);
					entity2->Import(entity);
					entity2->ColorPX=Color32(47, 194, 1207);
					block->Add(entity2);
					DXF->AddBlock(block);
				}

				block=DXF->GetBlockByName("pltVertLine");
				if(!block)
				{
					block=CreateLabelBlock(&(DXF->Handle_index), "pltVertLine", Coef);
					entity=new TDXFPolyLine(block);
					entity->ColorPX=clYellow32;
					entity->XYZ=TDouble3DPoint();
					((TDXFPolyLine*)entity)->Closed=true;
					vertex=new TDXFVertex(((TDXFPolyLine*)entity)->VertexesContainer);
					vertex->XYZ.X=Coef* 0.00; vertex->XYZ.Y=Coef* 0.40;
					((TDXFPolyLine*)entity)->AddVertex(vertex);
					vertex=new TDXFVertex(((TDXFPolyLine*)entity)->VertexesContainer);
					vertex->XYZ.X=Coef* 0.12; vertex->XYZ.Y=Coef* 0.16;
					((TDXFPolyLine*)entity)->AddVertex(vertex);
					vertex=new TDXFVertex(((TDXFPolyLine*)entity)->VertexesContainer);
					vertex->XYZ.X=Coef* 0.40; vertex->XYZ.Y=Coef* 0.12;
					((TDXFPolyLine*)entity)->AddVertex(vertex);
					vertex=new TDXFVertex(((TDXFPolyLine*)entity)->VertexesContainer);
					vertex->XYZ.X=Coef* 0.20; vertex->XYZ.Y=Coef*-0.08;
					((TDXFPolyLine*)entity)->AddVertex(vertex);
					vertex=new TDXFVertex(((TDXFPolyLine*)entity)->VertexesContainer);
					vertex->XYZ.X=Coef* 0.24; vertex->XYZ.Y=Coef*-0.32;
					((TDXFPolyLine*)entity)->AddVertex(vertex);
					vertex=new TDXFVertex(((TDXFPolyLine*)entity)->VertexesContainer);
					vertex->XYZ.X=Coef* 0.00; vertex->XYZ.Y=Coef*-0.20;
					((TDXFPolyLine*)entity)->AddVertex(vertex);
					vertex=new TDXFVertex(((TDXFPolyLine*)entity)->VertexesContainer);
					vertex->XYZ.X=Coef*-0.24; vertex->XYZ.Y=Coef*-0.32;
					((TDXFPolyLine*)entity)->AddVertex(vertex);
					vertex=new TDXFVertex(((TDXFPolyLine*)entity)->VertexesContainer);
					vertex->XYZ.X=Coef*-0.20; vertex->XYZ.Y=Coef*-0.08;
					((TDXFPolyLine*)entity)->AddVertex(vertex);
					vertex=new TDXFVertex(((TDXFPolyLine*)entity)->VertexesContainer);
					vertex->XYZ.X=Coef*-0.40; vertex->XYZ.Y=Coef* 0.12;
					((TDXFPolyLine*)entity)->AddVertex(vertex);
					vertex=new TDXFVertex(((TDXFPolyLine*)entity)->VertexesContainer);
					vertex->XYZ.X=Coef*-0.12; vertex->XYZ.Y=Coef* 0.16;
					((TDXFPolyLine*)entity)->AddVertex(vertex);
					vertex=new TDXFVertex(((TDXFPolyLine*)entity)->VertexesContainer);
					vertex->XYZ.X=Coef* 0.00; vertex->XYZ.Y=Coef* 0.40;
					((TDXFPolyLine*)entity)->AddVertex(vertex);
					block->Add(entity);
					DXF->AddBlock(block);
				}

				block=DXF->GetBlockByName("pltTransformed");
				if(!block)
				{
					block=CreateLabelBlock(&(DXF->Handle_index), "pltTransformed", Coef);
					entity2=new TDXFPolyLine(block);
					entity2->Import(entity);
					entity2->ColorPX=clFuchsia32;
					block->Add(entity2);
					DXF->AddBlock(block);
				}

				block=DXF->GetBlockByName("potMapLabel");
				if(!block)
				{
					block=CreateLabelBlock(&(DXF->Handle_index), "potMapLabel", Coef);
					entity=new TDXFLine(block);
					entity->ColorPX=clRed32;
					entity->XYZ.X=Coef*-0.4;
					entity->XYZ.Y=Coef*-0.4;
					((TDXFLine*)entity)->Point2.X=Coef*0.4;
					((TDXFLine*)entity)->Point2.Y=Coef*0.4;
					block->Add(entity);
					entity=new TDXFLine(block);
					entity->ColorPX=clRed32;
					entity->XYZ.X=Coef*-0.4;
					entity->XYZ.Y=Coef*0.4;
					((TDXFLine*)entity)->Point2.X=Coef*0.4;
					((TDXFLine*)entity)->Point2.Y=Coef*-0.4;
					block->Add(entity);
					DXF->AddBlock(block);
				}

				block=DXF->GetBlockByName("pltAutoDetectPos");
				if(!block)
				{
					block=CreateLabelBlock(&(DXF->Handle_index), "pltAutoDetectPos", Coef);
					entity=new TDXFCircle(block);
					entity->ColorPX=clRed32;
					entity->XYZ=TDouble3DPoint();
					((TDXFCircle*)entity)->R=0.5;
					block->Add(entity);
					entity=new TDXFCircle(block);
					entity->ColorPX=clRed32;
					entity->XYZ=TDouble3DPoint();
					((TDXFCircle*)entity)->R=0.3;
					block->Add(entity);
					entity=new TDXFCircle(block);
					entity->ColorPX=clRed32;
					entity->XYZ=TDouble3DPoint();
					((TDXFCircle*)entity)->R=0.1;
					block->Add(entity);
					DXF->AddBlock(block);
				}
				block=DXF->GetBlockByName("pltAutoDetectNeg");
				if(!block)
				{
					block=CreateLabelBlock(&(DXF->Handle_index), "pltAutoDetectNeg", Coef);
					entity=new TDXFCircle(block);
					entity->ColorPX=clBlue32;
					entity->XYZ=TDouble3DPoint();
					((TDXFCircle*)entity)->R=0.5;
					block->Add(entity);
					entity=new TDXFCircle(block);
					entity->ColorPX=clBlue32;
					entity->XYZ=TDouble3DPoint();
					((TDXFCircle*)entity)->R=0.3;
					block->Add(entity);
					entity=new TDXFCircle(block);
					entity->ColorPX=clBlue32;
					entity->XYZ=TDouble3DPoint();
					((TDXFCircle*)entity)->R=0.1;
					block->Add(entity);
					DXF->AddBlock(block);
				}

				for(int k=0; k<SatellitesList->Count; k++)
				{
					try
					{
						ps=(TProfileSatellites*)SatellitesList->Items[k];
						if(ps->Path && ps->Path->Count>0)
						{
							layer=DXF->GetLayerByName("radarmapPath"+IntToStr(k));
							if(!layer)
							{
								layer=new TDXFLayer(&(DXF->Handle_index), "radarmapPath"+IntToStr(k));
								layer->Description="Profile";
								if(ps->ProfilesCount>1) layer->Description+="s";
								layer->Description+=": ";
								s2="";
								for(int j=0; j<ps->ProfilesCount; j++)
								{
									if(j>0) s2+=", ";
									if(ps->Profiles[j] && ps->Profiles[j]->FullName!=NULL &&
										ps->Profiles[j]->FullName!="")
											s2+=ps->Profiles[j]->FullName;
									else s2+="<Noname>";
								}
								layer->Description+=s2;
								DXF->AddLayer(layer);
							}
							line=new TDXFPolyLine(layer);
							line->ColorPX=clGray32;
							//line->Thickness=GPSPathSelectionWidth;
							line->Closed=false;
							line->Polyline3D=Settings->DXFPolyline3D;
							if(ps->Path->Count<GPSPathSelectionArrowPointsQ) N=ps->Path->Count;
							else N=GPSPathSelectionArrowPointsQ;
							EndPoints=new TDouble3DPoint[N];
							try
							{
								for(int j=0; j<ps->Path->Count; j++) //<gml:LineString>
								{
									c=ps->Path->Points[j];
									if(!c->AutomaticUpdate)
									{
										c->AutomaticUpdate=true;
										c->AutomaticUpdate=false;
									}
									vertex=new TDXFVertex(line->VertexesContainer);
									if(MapObjects->Container->CoordinateSystem==csLatLon)
									{
										vertex->XYZ.Y=c->Latitude;
										vertex->XYZ.X=c->Longitude;
										vertex->XYZ.Z=c->Z_Geoid;
									}
									else
									{
										vertex->XYZ.X=c->GetX(MapObjects->Container->CoordinateSystem);
										vertex->XYZ.Y=c->GetY(MapObjects->Container->CoordinateSystem);
										vertex->XYZ.Z=c->GetH(MapObjects->Container->CoordinateSystem);
									}
									/*switch(MapObjects->Container->CoordinateSystem)
									{
										case csDutch:
											vertex->XYZ.X=c->RdX;
											vertex->XYZ.Y=c->RdY;
											vertex->XYZ.Z=c->RdH;
											break;
										case csBeLambert08:
										case csBeLambert72:
											vertex->XYZ.X=c->BeX;
											vertex->XYZ.Y=c->BeY;
											vertex->XYZ.Z=c->BeH;
											break;
										case csUTM:
											vertex->XYZ.X=c->UtmX;
											vertex->XYZ.Y=c->UtmY;
											vertex->XYZ.Z=c->UtmH;
											break;
										case csLKS:
											vertex->XYZ.X=c->LksX;
											vertex->XYZ.Y=c->LksY;
											vertex->XYZ.Z=c->LksH;
											break;
										case csLatLon:
										default:
											vertex->XYZ.Y=c->Latitude;
											vertex->XYZ.X=c->Longitude;
											vertex->XYZ.Z=c->Z_Geoid;
											break;
									}*/
									if(j==0) line->XYZ.Z=vertex->XYZ.Z;
									vertex->Comment="TraceIndex: ";
									if(c->Trace) vertex->Comment+=IntToStr(c->Trace->Index);
									else vertex->Comment+="-1";
									line->AddVertex(vertex);
									if(j<N) EndPoints[j]=vertex->XYZ;
									else
									{
										for(int z=0; z<N-1; z++) EndPoints[z]=EndPoints[z+1];
										EndPoints[N-1]=vertex->XYZ;
									}
								}
								layer->Add(line);
								if(settings->DXFPathEndsWithArrow)
								{
									int h, g;
									TFloatLine fline=TFloatLine();

									line=new TDXFPolyLine(layer);
									line->ColorPX=clGray32;
									//line->Thickness=GPSPathSelectionWidth;
									line->Closed=false;
									line->Polyline3D=Settings->DXFPolyline3D;
									for(h=0; h<(int)(N/2+0.5); h++)
									{
										fline.X1+=EndPoints[h].X;
										fline.Y1+=EndPoints[h].Y;
									}
									fline.X1=fline.X1/(float)h;
									fline.Y1=fline.Y1/(float)h;
									g=0;
									for(h=(int)(N/2+0.5); h<N; h++)
									{
										fline.X2+=EndPoints[h].X;
										fline.Y2+=EndPoints[h].Y;
										g++;
									}
									fline.X2=fline.X2/(float)g;
									fline.Y2=fline.Y2/(float)g;
									fline.Move(EndPoints[N-1].X, EndPoints[N-1].Y);
									fline.SetLength(Coef*1.5, fline.Angle()+5*M_PI/6.);
									vertex=new TDXFVertex(line->VertexesContainer);
									vertex->XYZ.X=fline.X2;
									vertex->XYZ.Y=fline.Y2;
									vertex->XYZ.Z=EndPoints[N-1].Z;
									line->AddVertex(vertex);
									vertex=new TDXFVertex(line->VertexesContainer);
									vertex->XYZ.X=fline.X1;
									vertex->XYZ.Y=fline.Y1;
									vertex->XYZ.Z=EndPoints[0].Z;
									line->AddVertex(vertex);
									line->XYZ.Z=vertex->XYZ.Z;
									fline.SetLength(Coef*1.5, fline.Angle()-2*5*M_PI/6.);
									vertex=new TDXFVertex(line->VertexesContainer);
									vertex->XYZ.X=fline.X2;
									vertex->XYZ.Y=fline.Y2;
									vertex->XYZ.Z=EndPoints[N-1].Z;
									line->AddVertex(vertex);

									layer->Add(line);
								}
							}
							__finally
							{
								delete EndPoints;
							}
							for(int i=0; i<ps->CustomObjects->Count; i++)
							{
								try
								{
									po=(TProfileObject*) ps->CustomObjects->Items[i];
									if(po->ProfileObjectType==potProfileLabel)
									{
										po=((TProfileLabel*)po)->ConnectedObject;
										if(po && po->ProfileObjectType==potMapLabel)
										{
											ml=(TMapLabel*)po;
											if(ml->Trace!=NULL) //<label>
											{
												s="";
												switch((TProfileLabelType)ml->Tag)
												{
													case pltVertLine: 		 s="pltVertLine"; break;
													case pltTransformed: 	 s="pltTransformed"; break;
													case pltCircleRed: 		 s="pltCircleRed"; break;
													case pltCircleGreen: 	 s="pltCircleGreen"; break;
													case pltCircleBlue: 	 s="pltCircleBlue"; break;
													case pltSquareRed: 		 s="pltSquareRed"; break;
													case pltSquareGreen: 	 s="pltSquareGreen"; break;
													case pltSquareBlue: 	 s="pltSquareBlue"; break;
													case pltTriangleRed: 	 s="pltTriangleRed"; break;
													case pltTriangleGreen: 	 s="pltTriangleGreen"; break;
													case pltTriangleBlue: 	 s="pltTriangleBlue"; break;
													case pltWaterCircle:     s="pltWaterCircle"; break;
													case pltWaterRect:       s="pltWaterRect"; break;
													case pltWaterTriangle:   s="pltWaterTriangle"; break;
													case pltElectroCircle:   s="pltElectroCircle"; break;
													case pltElectroRect:     s="pltElectroRect"; break;
													case pltElectroTriangle: s="pltElectroTriangle"; break;
													case pltGasCircle:       s="pltGasCircle"; break;
													case pltGasRect:         s="pltGasRect"; break;
													case pltGasTriangle:     s="pltGasTriangle"; break;
													case pltGasHighCircle:   s="pltGasHighCircle"; break;
													case pltGasHighRect:     s="pltGasHighRect"; break;
													case pltGasHighTriangle: s="pltGasHighTriangle"; break;
													case pltSewageCircle:    s="pltSewageCircle"; break;
													case pltSewageRect:      s="pltSewageRect"; break;
													case pltSewageTriangle:  s="pltSewageTriangle"; break;
													case pltTelecomCircle:   s="pltTelecomCircle"; break;
													case pltTelecomRect:     s="pltTelecomRect"; break;
													case pltTelecomTriangle: s="pltTelecomTriangle"; break;
													case pltFiberCircle:     s="pltFiberCircle"; break;
													case pltFiberRect:       s="pltFiberRect"; break;
													case pltFiberTriangle:   s="pltFiberTriangle"; break;
													case pltAutoDetect:	     s="pltAutoDetect"; break;
													case pltAutoDetectPos:	 s="pltAutoDetectPos"; break;
													case pltAutoDetectNeg:	 s="pltAutoDetectNeg"; break;
													default: s="";
												}
												if(s!="")
												{
													insert=new TDXFInsert(layer);
													insert->BlockName=s;
													insert->Block=DXF->GetBlockByName(s);
													if(insert->Block) insert->Color=insert->Block->Color;
													s=ml->Description;
													if(s!=NULL)
														for(int g=1; g<=s.Length(); g++)
															if(s[g]=='\r' || s[g]=='\n') s[g]=' ';
													c=new TGPSCoordinate(NULL, ml->Trace);
													try
													{
														if(MapObjects->Container->CoordinateSystem==csLatLon)
														{
															insert->XYZ.Y=c->Latitude;
															insert->XYZ.X=c->Longitude;
															insert->XYZ.Z=c->Z_Geoid;
														}
														else
														{
															insert->XYZ.X=c->GetX(MapObjects->Container->CoordinateSystem);
															insert->XYZ.Y=c->GetY(MapObjects->Container->CoordinateSystem);
															insert->XYZ.Z=c->GetH(MapObjects->Container->CoordinateSystem);
														}
														/*switch(MapObjects->Container->CoordinateSystem)
														{
															case csDutch:
																insert->XYZ.X=c->RdX;
																insert->XYZ.Y=c->RdY;
																insert->XYZ.Z=c->RdH;
																break;
															case csBeLambert08:
															case csBeLambert72:
																insert->XYZ.X=c->BeX;
																insert->XYZ.Y=c->BeY;
																insert->XYZ.Z=c->BeH;
																break;
															case csUTM:
																insert->XYZ.X=c->UtmX;
																insert->XYZ.Y=c->UtmY;
																insert->XYZ.Z=c->UtmH;
																break;
															case csLKS:
																insert->XYZ.X=c->LksX;
																insert->XYZ.Y=c->LksY;
																insert->XYZ.Z=c->LksH;
																break;
															case csLatLon:
															default:
																insert->XYZ.Y=c->Latitude;
																insert->XYZ.X=c->Longitude;
																insert->XYZ.Z=c->Z_Geoid;
																break;
														}*/
														if(ml->ConnectedObject && ml->ConnectedObject->ProfileObjectType==potProfileLabel)
														{
															pZ=-(double)((TProfileLabel*)ml->ConnectedObject)->Z;
															if(Settings->DXFUsePitch || Settings->DXFUseRoll)
															{
																double r, a, aZ, aX, aY, cosX, sinX, sinY, cosY, pX, pY;

																pX=pY=0;
																aX=(double)Settings->DXFUsePitch*ml->Trace->Pitch;
																aY=(double)Settings->DXFUseRoll*ml->Trace->Roll;
																aZ=ml->Trace->Heading;
																cosX=cos(aX);
																sinX=sin(aX);
																cosY=cos(aY);
																sinY=sin(aY);
																if(pX<0) pX=-pZ*sinY;
																else pX=pZ*sinY;
																pZ*=cosY;
																if(pY<0) pY=-pZ*sinX;
																else pY=pZ*sinX;
																pZ*=cosX;
																r = pX*pX+pY*pY;
																if(r>0)
																{
																	r=sqrt(r);
																	a = acos(pY/r);
																	if(pX < 0) a = 2*M_PI-a;
																	a+=aZ;
																	pX = r*sin(a);
																	pY = r*cos(a);
																}
																insert->XYZ.X+=pX;
																insert->XYZ.Y+=pY;
																insert->XYZ.Z+=pZ;
															}
														}
														else pZ=0;
														if(s==NULL || s=="")
														{
															if(ml->ConnectedObject) s=ml->ConnectedObject->Description;
															else s="";
															if(s!=NULL)
																for(int g=1; g<=s.Length(); g++)
																	if(s[g]=='\r' || s[g]=='\n') s[g]=' ';
														}
														if(ml->ConnectedObject && (Settings->DXFDescriptionAsText || Settings->DXFDepthToDescription || Settings->DXFIdToDescription))
														{
															text=new TDXFText(layer);
															text->Height=Settings->DXFDescriptionTextHeight;
															text->XYZ=insert->XYZ;
															text->XYZ.X+=0.55*Coef;
															text->Text="";
															if(Settings->DXFIdToDescription && ml->ConnectedObject->ID>0)
																text->Text+=" ID "+IntToStr((int)ml->ConnectedObject->ID);
															if(Settings->DXFDescriptionAsText && s!=NULL && s.Trim().Length()>0)
																text->Text+=" "+s.Trim();
															if(Settings->DXFDepthToDescription && pZ!=0 && (TProfileLabelType)ml->ProfileLabelType!=pltVertLine)
																text->Text+=" Depth="+FloatToStrF(-pZ, ffFixed, 10, 2)+" m";
															text->Text.Trim();
															if(text->Text!="" && text->Text.Length()>0)
																layer->Add(text);
															else delete text;
															//text->Height
														}
													}
													__finally
													{
														delete c;
													}
													insert->Comment="TraceIndex: "+IntToStr(ml->TraceIndex);
													insert->Comment+=" Depth: "+FloatToStrF(pZ, ffFixed, 10, 2)+" m";
													if(ml->ConnectedObject) insert->Comment+=" ID: "+IntToStr((int)ml->ConnectedObject->ID);
													//insert->Z=pZ;
													insert->Comment+=" "+s;
													layer->Add(insert);
													if(Settings->DXFConfidence && ml->GPSConfidence>0)
													{
														TDXFCircle *crcl;

														crcl=new TDXFCircle(layer);
														crcl->ColorPX=ml->Color;
														crcl->XYZ=insert->XYZ;
														crcl->R=ml->GPSConfidence;
														layer->Add(crcl);
													}
												}
											}
										}
									}
								}
								catch(Exception &e) {}
							}
						}
					}
					catch(Exception &e) {}
				}
				if(mapobjects)
				{
					try
					{
						layer=DXF->GetLayerByName("radarmapPins");
						if(!layer)
						{
							layer=new TDXFLayer(&(DXF->Handle_index), "radarmapPins");
							layer->Description="Tracking pins";
							DXF->AddLayer(layer);
						}
						for(int k=0; k<mapobjects->CustomObjectsCount; k++)
						{
							po=(TProfileObject*)mapobjects->CustomObjectsList->Items[k];
							if(po->ProfileObjectType==potMapLabel)
							{
								ml=(TMapLabel*)po;
								if(!ml->ConnectedObject)
								{
									s="potMapLabel";
									insert=new TDXFInsert(layer);
									insert->BlockName=s;
									insert->Block=DXF->GetBlockByName(s);
									if(insert->Block) insert->Color=insert->Block->Color;
									c=ml->Coordinate;
									if(MapObjects->Container->CoordinateSystem==csLatLon)
									{
										insert->XYZ.Y=c->Latitude;
										insert->XYZ.X=c->Longitude;
										insert->XYZ.Z=c->Z_Geoid;
									}
									else
									{
										insert->XYZ.X=c->GetX(MapObjects->Container->CoordinateSystem);
										insert->XYZ.Y=c->GetY(MapObjects->Container->CoordinateSystem);
										insert->XYZ.Z=c->GetH(MapObjects->Container->CoordinateSystem);
									}
									/*switch(MapObjects->Container->CoordinateSystem)
									{
										case csDutch:
											insert->XYZ.X=c->RdX;
											insert->XYZ.Y=c->RdY;
											insert->XYZ.Z=c->RdH;
											break;
										case csBeLambert08:
										case csBeLambert72:
											insert->XYZ.X=c->BeX;
											insert->XYZ.Y=c->BeY;
											insert->XYZ.Z=c->BeH;
											break;
										case csUTM:
											insert->XYZ.X=c->UtmX;
											insert->XYZ.Y=c->UtmY;
											insert->XYZ.Z=c->UtmH;
											break;
										case csLKS:
											insert->XYZ.X=c->LksX;
											insert->XYZ.Y=c->LksY;
											insert->XYZ.Z=c->LksH;
											break;
										case csLatLon:
										default:
											insert->XYZ.Y=c->Latitude;
											insert->XYZ.X=c->Longitude;
											insert->XYZ.Z=c->Z_Geoid;
											break;
									}*/
									s=ml->Description;
									if(s!=NULL)
										for(int g=1; g<=s.Length(); g++)
											if(s[g]=='\r' || s[g]=='\n') s[g]=' ';
									insert->Comment+=" "+s;
									layer->Add(insert);
									if((Settings->DXFDescriptionAsText && s!=NULL && s!="" && s.Length()>0)  || Settings->DXFIdToDescription)
									{
										text=new TDXFText(layer);
										text->Height=Settings->DXFDescriptionTextHeight;
										text->XYZ=insert->XYZ;
										text->XYZ.X+=0.55*Coef;
										//text->Text=s;
										//layer->Add(text);
										text->Text="";
										if(Settings->DXFIdToDescription && ml->ID>0)
											text->Text+=" ID "+IntToStr((int)ml->ID);
										if(Settings->DXFDescriptionAsText) text->Text+=" "+s;
										text->Text.Trim();
										if(text->Text!="" && text->Text.Length()>0)
											layer->Add(text);
									}
									if(Settings->DXFConfidence && ml->GPSConfidence>0)
									{
										TDXFCircle *crcl;

										crcl=new TDXFCircle(layer);
										crcl->ColorPX=ml->Color;
										crcl->XYZ=insert->XYZ;
										crcl->R=ml->GPSConfidence;
										layer->Add(crcl);
									}
								}
							}
						}
					}
					catch(Exception &e) {}
				}
			}
			DXF->SaveData(FileName);
		}
	}
	__finally
	{
		delete DXF;
	}
}
//---------------------------------------------------------------------------

void __fastcall TRadarMapManager::ExportResultsToXYZC(AnsiString FileName)
{
	TCoordinateSystem cs;
	TProfile* Profile;
	TStrings* Strs;
	float *Buf;
	TProfileSatellites *ps;
	AnsiString str;
	int TotalTraces;
	TToolProgressBar *progress;
	float d2, d, dX, dY, dZ, dP, dR, dH, dT;
	double X, Y, Z, P, R, H, pX, pY, pZ, aX, aY, aZ, r, cosX, sinX, cosY, sinY, a;//, X_old=0, Y_old=0;
	int i, j, k, ii, kk, kkk;
	TTrace *Trace;
	TGPSCoordinate *llp=NULL;
	TSample *EnvelopedData, *Data;
	TFourierFamily *FF;
	TFileStream* fs;

	fs=new TFileStream(FileName, fmCreate | fmShareExclusive);
	Strs=new TStringList();
	FF=NULL;
	try
	{
		if(MapObjects && MapObjects->Container)
		{
			cs=MapObjects->Container->CoordinateSystem;
			progress=MapObjects->Container->Progress;
		}
		else
		{
			cs=csLatLon;
			progress=NULL;
		}

		if(SatellitesList && Settings)
		{
			TotalTraces=0;
			for(int k=0; k<SatellitesList->Count; k++)
			{
				try
				{
					ps=(TProfileSatellites*)SatellitesList->Items[k];
					if(ps->Path)
					{
						if(ps->Path->OutputView && ps->Path->OutputView->Profile)
						{
						   TotalTraces+=ps->Path->OutputView->Profile->Traces;
						}
					}
				}
				catch (Exception &e) {}
			}
			if(progress) progress->Show(TotalTraces, true);
			try
			{
				for(int k=0; k<SatellitesList->Count; k++)
				{
					try
					{
						ps=(TProfileSatellites*)SatellitesList->Items[k];
						if(ps->Path)
						{
							if(ps->Path->OutputView && ps->Path->OutputView->Profile) //<profile>
							{
								Profile=ps->Path->OutputView->Profile;

								Buf=new float[Profile->Samples];
								EnvelopedData=new TSample[Profile->Samples];
								if(!FF) FF=new TFourierFamily(Profile->Samples);
								kkk=0;
								try
								{
									d2=Settings->XYZCCellHeight;
									if(Profile->HorRangeFull>0)
										dX=(int)((float)Profile->Traces*Settings->XYZCCellWidth/Profile->HorRangeFull);
									else dX=1;
									if(dX<1) dX=1;
									d=Profile->TimeRange;
									if(Profile->TimeRange>1E9) d/=1E9;
									else if(Profile->TimeRange>1000000) d/=1000000;
									else if(Profile->TimeRange>1000) d/=1000;
									d*=1e-9*3e8/2.0/sqrt(Profile->Permit);
									dY=(int)((float)Profile->Samples*d2/d);
									Trace=Profile->FirstTracePtr;
									while(Trace!=NULL)
									{
										X=Y=Z=P=R=H=0;
										for(i=0; i<Profile->Samples; i++) Buf[i]=0.0;
										dT=0;
										while(Trace!=NULL && dT<dX)
										{
											llp=new TGPSCoordinate(NULL, Trace);
											try
											{
												if(llp->Valid)
												{
													if(cs==csLatLon)
													{
														X+=llp->GetX(csMetric);
														Y+=llp->GetY(csMetric);
														if(Settings->GPS.UseZGeoid) Z+=llp->Z_Geoid;
														else Z+=llp->Z;
													}
													else
													{
														X+=llp->GetX(cs);
														Y+=llp->GetY(cs);
														Z+=llp->GetH(cs);
													}
													P+=Trace->Pitch;
													R+=Trace->Roll;
													H+=Trace->Heading;
													kkk=0;
													if(Settings->XYZCGained) Data=Trace->GetArrayAddress(tatData);
													else Data=Trace->GetArrayAddress(tatSource);
													if(Settings->XYZCEnvelope)
													{
														FF->Envelope(Data, EnvelopedData, Profile->Samples);
														Data=EnvelopedData;
													}
													for(i=0; i<Profile->Samples; i+=dY, kkk++)
													{
														kk=Profile->Samples-i;
														if(kk>dY) kk=dY;
														ii=0;
														for(j=0; j<kk; j++)
														{
															if(Settings->XYZCAbsolute) ii+=abs(Data[i+j]);
															else ii+=Data[i+j];
														}
														Buf[kkk]+=((float)ii/(float)kk);
													}
													dT++;
												}
												else
												{
													kk=0;
												}
												Trace=Trace->PtrUp;
												if(progress) progress->StepIt();
											}
											__finally
											{
												delete llp;
											}
										}
										if(!Settings->XYZCUseZ) Z=0;
										if(!Settings->XYZCUsePitch) P=0;
										if(!Settings->XYZCUseRoll) R=0;
										if(dT>0)
										{
											Y/=(float)dT;
											X/=(float)dT;
											Z/=(float)dT;
											P/=(float)dT;
											R/=(float)dT;
											H/=(float)dT;

											aX=(double)Settings->XYZCUsePitch*P;
											aY=(double)Settings->XYZCUseRoll*R;
											//aX=P;//-0.1*M_PI;
											//aY=R;//+0.2*M_PI;
											aZ=H;
											cosX=cos(aX);
											sinX=sin(aX);
											cosY=cos(aY);
											sinY=sin(aY);
											for(i=0; i<kkk; i++)
											{
												pX=pY=0;
												pZ=-(float)i*d2;
												//if(i>0 && (Settings->XYZCUsePitch || Settings->XYZCUseRoll))
												//	Rotate3DPoint(pX, pY, pZ, aX, aY, aZ, pX, pY, pZ);
												if(i>0 && (Settings->XYZCUsePitch || Settings->XYZCUseRoll))
												{
													/*//r = pX*pX+pZ*pZ;
													r=pZ;
													if(r>0)
													{
														//r=sqrt(r);
														//a = acos(pZ/r);
														if(pX < 0) a = 2*M_PI-a;
														a+=aY;
														pZ = r*cos(a);
														pX = r*sin(a);
													}
													//r = pZ*pZ+pY*pY;
													r=pZ;
													if(r>0)
													{
														//r=sqrt(r);
														//a = acos(pZ/r);
														if(pY < 0) a=2*M_PI-a;
														a+=aX;
														pZ = r*cos(a);
														pY = r*sin(a);
													}*/

													if(pX<0) pX=-pZ*sinY;
													else pX=pZ*sinY;
													pZ*=cosY;
													if(pY<0) pY=-pZ*sinX;
													else pY=pZ*sinX;
													pZ*=cosX;

													r = pX*pX+pY*pY;
													if(r>0)
													{
														r=sqrt(r);
														a = acos(pY/r);
														if(pX < 0) a = 2*M_PI-a;
														a+=aZ;
														pX = r*sin(a);
														pY = r*cos(a);
													}
												}
												//X
												str=FloatToStrF(X+pX, ffFixed, 15, 8)+"\t";
												//Y
												str+=FloatToStrF(Y+pY, ffFixed, 15, 8)+"\t";
												//Z
												str+=FloatToStrF(Z+pZ, ffFixed, 15, 8)+"\t";
												//C
												Buf[i]=Buf[i]/(float)dT;
												str+=IntToStr((int)Buf[i]);

												Strs->Add(str);
												if(Strs->Count>=kkk)
												{
													fs->Seek(0, soFromEnd);
													Strs->SaveToStream(fs);
													Strs->Clear();
												}
											}
										}
									}
								}
								__finally
								{
									delete[] Buf;
									delete[] EnvelopedData;
								}
							}
						}
					}
					catch (Exception &e) {}
				}
			}
			__finally
			{
				if(progress) progress->Hide(true);
			}
		}
	}
	__finally
	{
		//Strs->SaveToFile(FileName);
		if(Strs->Count>0)
		{
			fs->Seek(0, soFromEnd);
			Strs->SaveToStream(fs);
			Strs->Clear();
		}
		delete fs;
		if(FF) delete FF;
		delete Strs;
	}
}
//---------------------------------------------------------------------------

AnsiString __fastcall TRadarMapManager::LabelToText(TCoordinateSystem cs, TCustomProfileLabel *pl, AnsiString LabelType)
{
	AnsiString str, s;
	double lat, f, z;

	str="";
	if(pl->Coordinate!=NULL && MapObjects && MapObjects->Container) //<label>
	{
		if(LabelType=="")
		{
			switch((TProfileLabelType)pl->Tag)
			{
				case pltVertLine: 		 s="pltVertLine"; break;
				case pltTransformed: 	 s="pltTransformed"; break;
				case pltCircleRed: 		 s="pltCircleRed"; break;
				case pltCircleGreen: 	 s="pltCircleGreen"; break;
				case pltCircleBlue: 	 s="pltCircleBlue"; break;
				case pltSquareRed: 		 s="pltSquareRed"; break;
				case pltSquareGreen: 	 s="pltSquareGreen"; break;
				case pltSquareBlue: 	 s="pltSquareBlue"; break;
				case pltTriangleRed: 	 s="pltTriangleRed"; break;
				case pltTriangleGreen: 	 s="pltTriangleGreen"; break;
				case pltTriangleBlue: 	 s="pltTriangleBlue"; break;
				case pltWaterCircle:     s="pltWaterCircle"; break;
				case pltWaterRect:       s="pltWaterRect"; break;
				case pltWaterTriangle:   s="pltWaterTriangle"; break;
				case pltElectroCircle:   s="pltElectroCircle"; break;
				case pltElectroRect:     s="pltElectroRect"; break;
				case pltElectroTriangle: s="pltElectroTriangle"; break;
				case pltGasCircle:       s="pltGasCircle"; break;
				case pltGasRect:         s="pltGasRect"; break;
				case pltGasTriangle:     s="pltGasTriangle"; break;
				case pltGasHighCircle:   s="pltGasHighCircle"; break;
				case pltGasHighRect:     s="pltGasHighRect"; break;
				case pltGasHighTriangle: s="pltGasHighTriangle"; break;
				case pltSewageCircle:    s="pltSewageCircle"; break;
				case pltSewageRect:      s="pltSewageRect"; break;
				case pltSewageTriangle:  s="pltSewageTriangle"; break;
				case pltTelecomCircle:   s="pltTelecomCircle"; break;
				case pltTelecomRect:     s="pltTelecomRect"; break;
				case pltTelecomTriangle: s="pltTelecomTriangle"; break;
				case pltFiberCircle:     s="pltFiberCircle"; break;
				case pltFiberRect:       s="pltFiberRect"; break;
				case pltFiberTriangle:   s="pltFiberTriangle"; break;
				case pltAutoDetect:	     s="pltAutoDetect"; break;
				case pltAutoDetectPos:	 s="pltAutoDetectPos"; break;
				case pltAutoDetectNeg:	 s="pltAutoDetectNeg"; break;
				default: s="";
			}
		}
		else s=LabelType;
		if(s!="")
		{
			str+=IntToStr((int)pl->ID)+"\t";
			if(MapObjects->Container->CoordinateSystem==csLatLon)
			{
				lat=fabs(pl->Coordinate->Longitude);
				str+=IntToStr((int)lat)+"�";
				f=(lat-(int)lat)*60.;
				str+=IntToStr((int)f)+"'";
				f=(f-(int)f)*60.;
				str+=FloatToStrF(f, ffFixed, 6, 4)+"''";
				if(pl->Coordinate->Longitude<0) str+="W";
				else str+="E";
				str+="\t";
				lat=fabs(pl->Coordinate->Latitude);
				str+=IntToStr((int)lat)+"�";
				f=(lat-(int)lat)*60.;
				str+=IntToStr((int)f)+"'";
				f=(f-(int)f)*60.;
				str+=FloatToStrF(f, ffFixed, 6, 4)+"''";
				if(pl->Coordinate->Latitude<0) str+="W";
				else str+="E";
				str+="\t";
			}
			else
			{
				str+=FloatToStrF(pl->Coordinate->GetX(MapObjects->Container->CoordinateSystem), ffFixed, 10, 2)+"\t";
				str+=FloatToStrF(pl->Coordinate->GetY(MapObjects->Container->CoordinateSystem), ffFixed, 10, 2)+"\t";
			}
			/*switch(MapObjects->Container->CoordinateSystem)
			{
				case csDutch:
					str+=FloatToStrF(pl->Coordinate->RdX, ffFixed, 10, 2)+"\t";
					str+=FloatToStrF(pl->Coordinate->RdY, ffFixed, 10, 2)+"\t";
					break;
				case csBeLambert08:
				case csBeLambert72:
					str+=FloatToStrF(pl->Coordinate->BeX, ffFixed, 10, 2)+"\t";
					str+=FloatToStrF(pl->Coordinate->BeY, ffFixed, 10, 2)+"\t";
					break;
				case csUTM:
					str+=FloatToStrF(pl->Coordinate->UtmX, ffFixed, 10, 2)+"\t";
					str+=FloatToStrF(pl->Coordinate->UtmY, ffFixed, 10, 2)+"\t";
					str+=IntToStr(pl->Coordinate->UtmZN)+pl->Coordinate->UtmZL+"\t";
					break;
				case csLKS:
					str+=FloatToStrF(pl->Coordinate->LksX, ffFixed, 10, 2)+"\t";
					str+=FloatToStrF(pl->Coordinate->LksY, ffFixed, 10, 2)+"\t";
					break;
				case csLatLon:
				default:
					lat=fabs(pl->Coordinate->Longitude);
					str+=IntToStr((int)lat)+"�";
					f=(lat-(int)lat)*60.;
					str+=IntToStr((int)f)+"'";
					f=(f-(int)f)*60.;
					str+=FloatToStrF(f, ffFixed, 6, 4)+"''";
					if(pl->Coordinate->Longitude<0) str+="W";
					else str+="E";
					str+="\t";
					lat=fabs(pl->Coordinate->Latitude);
					str+=IntToStr((int)lat)+"�";
					f=(lat-(int)lat)*60.;
					str+=IntToStr((int)f)+"'";
					f=(f-(int)f)*60.;
					str+=FloatToStrF(f, ffFixed, 6, 4)+"''";
					if(pl->Coordinate->Latitude<0) str+="W";
					else str+="E";
					str+="\t";
					break;
			}*/
			str+=FloatToStrF(pl->Z, ffFixed, 10, 2)+"\t";
			z=pl->Coordinate->GetH(cs);
			//if(Settings) z-=Settings->GpsAntennaHeight;
			if(pl->Z>=0) z-=pl->Z;
			str+=FloatToStrF(z, ffFixed, 10, 2)+"\t";
			str+=FloatToStrF(pl->GPSConfidence, ffFixed, 10, 3)+"\t";
			str+=s+"\t";
			if((int)pl->CreatedTime>0) str+=MyDateTimeToStr(pl->CreatedTime);
			str+="\t";
			if((int)pl->ModifiedTime>0) str+=MyDateTimeToStr(pl->ModifiedTime);
			str+="\t";
			str+=pl->Attachment+"\t";
			str+=pl->Description;
		}
	}
	return str;
}
//---------------------------------------------------------------------------

void __fastcall TRadarMapManager::ExportResultsToTXT(AnsiString FileName)
{
	TCoordinateSystem cs;
	TProfile* Profile;
	TStrings* Strs;
	TProfileObject* po;
	TProfileSatellites *ps;
	AnsiString str;
	TToolProgressBar *progress;
	int total;

	Strs=new TStringList();
	str="ID\tX\tY\tZ mbs\tZ\tConfidence\tType\tCreated\tModified\tAttachment\tDescription";
	Strs->Add(str);
		if(MapObjects && MapObjects->Container)
	{
		cs=MapObjects->Container->CoordinateSystem;
		progress=MapObjects->Container->Progress;
	}
	else
	{
		cs=csLatLon;
		progress=NULL;
	}
	if(progress)
	{
		total=0;
		if(SatellitesList && Settings)
		{
			for(int k=0; k<SatellitesList->Count; k++)
			{
				try
				{
					ps=(TProfileSatellites*)SatellitesList->Items[k];
					total+=ps->CustomObjects->Count;
				}
				catch(Exception &e) {}
			}
		}
		if(mapobjects) total+=mapobjects->CustomObjectsCount;
		progress->Show(total, true);
	}
	try
	{	if(SatellitesList && Settings)
		{
			for(int k=0; k<SatellitesList->Count; k++)
			{
				try
				{
					ps=(TProfileSatellites*)SatellitesList->Items[k];
					for(int i=0; i<ps->CustomObjects->Count; i++)
					{
						try
						{
							po=(TProfileObject*)ps->CustomObjects->Items[i];
							if(po->ProfileObjectType==potProfileLabel)
							{
								str=LabelToText(cs, (TProfileLabel*)po);
								if(str!=NULL && str!="") Strs->Add(str);
							}
							if(progress) progress->StepIt();
						}
						catch(Exception &e) {}
					}
				}
				catch (Exception &e) {}
			}
		}
		if(mapobjects)
		{
			try
			{
				for(int k=0; k<mapobjects->CustomObjectsCount; k++)
				{
					po=(TProfileObject*)mapobjects->CustomObjectsList->Items[k];
					if(po->ProfileObjectType==potMapLabel && ((TMapLabel*)po)->ConnectedObject==NULL)
					{
						str=LabelToText(cs, (TMapLabel*)po, "potMapLabel");
						if(str!=NULL && str!="") Strs->Add(str);
					}
					if(progress) progress->StepIt();
				}
			}
			catch(Exception &e) {}
		}
	}
	__finally
	{
		Strs->SaveToFile(FileName);
		delete Strs;
		if(progress) progress->Hide(true);
	}
}
//---------------------------------------------------------------------------

bool __fastcall TRadarMapManager::LoadPathFromSGY(AnsiString FileName)
{
	TProfile* profile=NULL;
	TTrace *trace=NULL;
	TProfileSatellites *ps=NULL;
	TGPSCoordinate *c;
	int i;
	bool res=false;
	TToolProgressBar *progress=NULL;

	if(mapobjects && mapobjects->Container) progress=mapobjects->Container->Progress;
	if(FileExists(FileName) && SatellitesList)
	{
		profile=new TProfile();
		profile->Permit=Settings->Permitivity;
		if(Palette) Palette->ConvertToWinColors(profile->Colors, 256);
		i=Input_SEGY(FileName.c_str(), profile, progress, false, false, Settings->ApplyGain);
		if(i!=0)
		{
			delete profile;
			profile=NULL;
		}
		else
		{
			profile->OutputView->RightIndex=profile->LastTracePtr->Index;
			profile->OutputView->LeftIndex=profile->OutputView->RightIndex-ProfRect[0]->Width()+4;//objectscontainer->Image->Width;
			ps=new TProfileSatellites(this, profile);
			currentindex=SatellitesList->Add(ps);
			current=ps;
			trace=profile->FirstTracePtr;
			if(progress)
			{
				progress->Show(0, true);
				progress->Max+=profile->Traces;
			}
			while(trace!=NULL)
			{
				c=new TGPSCoordinate(NULL, trace);
				ps->Path->AddP(c);//, progress);
				if(trace->Index<profile->OutputView->RightIndex || trace->Index>profile->OutputView->RightIndex) trace->Drawed=true;
				trace=trace->PtrUp;
				progress->StepIt();
			}
			if(progress) progress->Hide(true);
			SwitchToPath(CurrentPath);
			if(trackingpath && c) trackingpath->ScrollToCoordinate(c);
			res=true;
		}
	}
	return res;
}
//---------------------------------------------------------------------------

void __fastcall TRadarMapManager::writeStartWaypoint(TStartStopWaypointLabel *value)
{
	if(startstopwaypointbtn)
	{
		if(value)
		{
			startstopwaypointbtn->Tag=swkStop;
			startstopwaypointbtn->NormalGlyphName="PNGIMAGE_260"; //StopWaypointBtn
			startstopwaypointbtn->HotGlyphName="PNGIMAGE_259";
			startstopwaypointbtn->PressedGlyphName="PNGIMAGE_261";
			startstopwaypointbtn->Down=false;
		}
		else
		{
			startstopwaypointbtn->Tag=swkStart;
			startstopwaypointbtn->NormalGlyphName="PNGIMAGE_256"; //StartWaypointBtn
			startstopwaypointbtn->HotGlyphName="PNGIMAGE_255";
			startstopwaypointbtn->PressedGlyphName="PNGIMAGE_257";
		}
	}
	if(value && MapObjects && MapObjects->MouseAction) *MapObjects->MouseAction=tmaHand;
	startwaypoint=value;
}
//---------------------------------------------------------------------------

void __fastcall TRadarMapManager::writeStopWaypoint(TStartStopWaypointLabel *value)
{
	if(startstopwaypointbtn)
	{
		startstopwaypointbtn->Tag=swkStart;
		startstopwaypointbtn->NormalGlyphName="PNGIMAGE_256"; //StartWaypointBtn
		startstopwaypointbtn->HotGlyphName="PNGIMAGE_255";
		startstopwaypointbtn->PressedGlyphName="PNGIMAGE_257";
		startstopwaypointbtn->Down=false;
	}
	if(value && MapObjects && MapObjects->MouseAction) *MapObjects->MouseAction=tmaHand;
	stopwaypoint=value;
}
