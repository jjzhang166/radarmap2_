#ifndef SparH
#define SparH

#include <vcl.h>
#include <math.h>

#pragma once
#define USING_PIPE
//#undef USING_PIPE

#define FULL_PIPE_NAME L"\\\\.\\pipe\\testpipe"

#define SHOW_PRECISE
#undef SHOW_PRECISE

enum sparMessage {
	SPAR_DATA           =  1, 
	SPAR_HEARTBEAT      =  2,
	SPAR_CMD_START      =  3, // +Job name, mode
	SPAR_CMD_STOP       =  4, 
	SPAR_CMD_REPLAY     =  5, 
	SPAR_CMD_EXIT       =  6,
	SPAR_CMD_SETUP      =  7, // +Spar port, internal GNSS, ZigBee PAN and mask, frequency, averaging, offsets, moving baseline, units
	SPAR_CMD_LAYERS     =  8, // +Mode and limits
	SPAR_CMD_SYSTEM     =  9, //
	SPAR_CMD_SONDE_CAL  = 10, // +Cal distance
	SPAR_CMD_FIND_COM   = 11, 
    SPAR_RESP_NONE      = 12,
    SPAR_RESP_SYSTEM    = 13, // +Serial number, firmware version, Jennic date string
    SPAR_RESP_SONDE_CAL = 14, // +Sonde strength
    SPAR_RESP_FIND_COM  = 15  // +Spar port
};

enum sparStatus
{
    FSV_STATUS_NO_RESP   = -3, // No response from spar
    FSV_STATUS_NO_CONN   = -2, // Cannot connect to spar
    FSV_STATUS_ERROR     = -1, // Unclassified error
    FSV_STATUS_IDLE      = 0,
    FSV_STATUS_RUNNING   = 1,
    FSV_STATUS_REPLAYING = 2,
    FSV_STATUS_EXPORTING = 3
};

enum sparAction
{
    FSV_ACTION_MEAS_POINT  = 1, 
    FSV_ACTION_MEAS_LINE   = 2, 
    FSV_ACTION_MEAS_PEAK   = 3, 
    FSV_ACTION_MEAS_GROUND = 4, 
    FSV_ACTION_CONT_TOPO   = 5,
    FSV_ACTION_SET_FREQ    = 6, // Setting in frequency slot
    FSV_STATUS_SET_RATE    = 7  // Setting in frequency slot
};

enum sparMode
{
    SPAR_MODE_LINE        = 1,
    SPAR_MODE_SONDE       = 2,
    SPAR_MODE_DUAL_LINE   = 3,
    SPAR_MODE_DUAL_SONDE  = 4,
    SPAR_MODE_MULTI_SONDE = 5,
    SPAR_MODE_PRECISE     = 6,
    SPAR_MODE_REPLAY      = 7
};

enum sparUnit
{
    SPAR_UNIT_METERS = 1,
    SPAR_UNIT_FEET   = 2,
    SPAR_UNIT_INCHES = 3
};

enum sparDepthTo
{
    SPAR_DEPTH_TO_TOP    = 1,
    SPAR_DEPTH_TO_CENTER = 2,
    SPAR_DEPTH_TO_BOTTOM = 3
};

enum storeType
{
	DONT_STORE  = 1, // Same as storePointState.NO_STORE
	USER_POINT	= 2,
	USER_LINE	= 3,
	AUTO_LINE	= 4,
	USER_PEAK	= 5
};

enum GNSSType
{
    GNSS_TYPE_TRIMBLE = 1,
    GNSS_TYPE_INTERNAL = 2,
    GNSS_TYPE_RADARMAP = 3
};

#define SPAR_STRING_BUF_MAX 256 // Max string buffer length

struct SparCmd
{
	char message;      // Message type, see enum sparMessage

	/***********************************************************************************
	 * COMMAND PARAMETERS: Start *
	 ***********************************************************************************/
    char mode;         // See enum sparMode

	/***********************************************************************************
	 * COMMAND PARAMETERS: Setup *
	 ***********************************************************************************/
    char avgType;      // 0=None, 1=Linear, 2=Exponential
    char avgTime;      // 0=0.2s, 1=0.4s, 2=0.6s, 3=0.8s, 4=1s, 5=2s, 6=5s, 7=10s
    char updateRate;   // 0=0.2s, 1=0.4s, 2=0.6s, 3=0.8s, 4=1s, 5=2s, 6=5s, 7=10s
    char distanceUnit; // M, ft, in
    char depthTo;      // Depth measured to where (top, center, bottom)
	char movingBase;   // Use moving baseline?

    float frequency;         // Set signal frequency

    float heightOverGround;  // Spar height over ground (m)
    float sparToAntennaX;    // Spar to antenna fwd (m)
    float sparToAntennaY;    // Spar to antenna right (m)
    float sparToAntennaZ;    // Spar to antenna up (m)
    float sparToSecondSparX; // Spar to end dev fwd (m)
    float sparToSecondSparY; // Spar to end dev right (m)
    float sparToSecondSparZ; // Spar to end dev up (m)

    float utilityDiameter;   // Diameter of utility (m)

    float sondeStrength;     // Stored/entered sonde strength

	/***********************************************************************************
	 * COMMAND PARAMETERS: Equipment *
	 ***********************************************************************************/
    int sparPort;      // COM port number (COMxx)

    int GNSSType;      // See enum GNSSType

    int panID;         // Settings for ZigBee
    int channelMask;

	/***********************************************************************************
     * COMMAND PARAMETERS: Layers *
     ***********************************************************************************/
    float layerSparH[5];     // SPAR_H upper limits on RMS
    float layerSparV[5];     //
    float layerUtilH[5];     // UTIL_H upper limits on total confidence
    float layerUtilV[5];     //

	/***********************************************************************************
     * COMMAND PARAMETERS: Transmitter *
     ***********************************************************************************/
    int transmitter;         // 

	/***********************************************************************************
     * COMMAND PARAMETERS: Sonde cal *
     ***********************************************************************************/
    float sondeCalDistance;  // Distance for sonde calibration (m)

    /***********************************************************************************
     * COMMAND PARAMETERS: Survey *
     ***********************************************************************************/
    int surveyType;      // Survey type: 0=None, 1-3=RTK, 4=Fast Static, 5=PP kin, 6-8=RT diff, 9=PP diff, 10=No GPS
    int tbd;

    /***********************************************************************************
     * COMMAND RESPONSE: Sonde cal *
	 ***********************************************************************************/
	float measSondeStrength;  // Measured sonde strength

	/***********************************************************************************
	 * COMMAND RESPONSE: Find port *
	 ***********************************************************************************/
	int foundSparPort;  // COM port number, or -1 for not found

	char     stringBuf[SPAR_STRING_BUF_MAX];   // Null-terminated job name or firmware date
};

struct Spar
{
	char    message;      // Message type, see sparMessage

	char    pointType;    // User point, user line or auto line
	char	flags;        // Bit 0: bApplyOffset
						  //        True indicates that the enclosed data is a stored
						  //	    point, and that the sender needs the most recent
						  //	    utility offsets and confidences to be merged,
						  //	    and the entire structur	e sent back. Otherwise,
						  //	    the passed structure is an observation for display
						  //	    and logging.
						  //        A REPLY TO THE SENT DATA IS ALWAYS PRESUMED, EVEN
						  //        IF bApplyOffset IS FALSE.
						  // Bit 1: bModelFix
						  //        Model fix required?

	char    measState;    // Copy of the monitor's measState
	char    pointName[4]; // Last 4 characters of the last stored point name

	/*************************************************************************
	 * GNSS MEASURED POSITION SECTION, OFFSET BY NOTED ANTENNA HEIGHT	 *
	 *************************************************************************/
	double	vAntHeight;	// Vertical offset of position to GPS antenna (user may set in Trimble Access)
	double	latitude;	// Latitude  in radians
	double	longitude;	// Longitude in radians
	double	altitude;	// WGS-84 altitude in meters

	double	utc;		// UTC time in seconds since 01-01-1970, or Greenwitch time
	double	rtkAge;		// Age of the base observations
	int		nEpochs;	// Number of epochs in the stored grid point (in case of averaging)

	int		fixType;	// 0:	gnsscm_NullPosition	- No solution.
						// 1:	gnsscm_Autonomous	- Autonomous solution.
						// 2:	gnsscm_SBAS 		- Satellite Based Augmentation System solution (EGNOS for Europe)
						// 3:	gnsscm_RTCM 		- Code solution.
						// 4:	gnsscm_RTKFloat 	- RTK float solution.
						// 5:	gnsscm_RTKFix 		- RTK fix solution.
						// 6:	gnsscm_Checking 	- Checking solution.
						// 7:	gnsscm_Inertial 	- Inertial solution.
						// 8:	gnsscm_RTKWAFloat 	- RTK float solution, Wide Area == RTK Network (ADV, VRS, CMR Net etc).
						// 9:	gnsscm_RTKWAFix 	- RTK fix solution, Wide Area == RTK Network (ADV, VRS, CMR Net etc).

	double	pdop;		// from GPGGA
	double	hdop;		// from GPGGA
	double	vdop;		// from GPGGA

	double	hsdv;		// Horizontal standard deviation in meters from GPGST
	double	vsdv;		// Vertical standard deviation in meters   from GPGST

	double  rms;        // RMS value from GPGST

	int		nSVs;		// Number of satellites used in solution from GPGGA

	int     pointID;    // Unique ID for the sent coordinates

	/***********************************************************************************
	 * SPAR POSITION SECTION, WITH APPLIED FIELDSENS VIEW OFFSET AND TILT COMPENSATION *
	 ***********************************************************************************/
	double	sparLatitude;	// Latitude  in radians
	double	sparLongitude;	// Longitude in radians
	double	sparAltitude;	// WGS-84 altitude in meters

	/***********************************************************************************
	 * END SPAR POSITION SECTION													   *
	 ***********************************************************************************/
	double	endSparEast;	// East offset of end spar w.r.t. base spar
	double	endSparNorth;	// North offset of end spar w.r.t. base spar
	double	endSparUp;		// Elevation difference of end spar w.r.t. base spar

	/***********************************************************************************
	 * UTILITY POSITION SECTION, WITH APPLIED FIELDSENS VIEW OFFSET AND TILT COMPENSATION *
	 ***********************************************************************************/
	double	utilLatitude;	// Latitude  in radians
	double	utilLongitude;	// Longitude in radians
	double	utilAltitude;	// WGS-84 altitude in meters
	double	utilHsdv;		// Horizontal standard deviation in meters
	double	utilVsdv;		// Vertical standard deviation in meters

	// Field Codes. If zero, indicates that the position or current value is not valid.
	int		cCode;	// Code indicating vertical spar confidence level (1=best, 5=worst) CHANGE THIS NAME!!
	int		hCode;	// Code indicating horizontal confidence level (1=best, 5=worst) of the utility position
	int		vCode;	// Code indicating vertical   confidence level (1=best, 5=worst) of the utility position
	int		sCode;	// Code indicating horizontal spar confidence level (1=best, 5=worst)

	/***********************************************************************************
	 * POINT IDs *
	 ***********************************************************************************/
	int     sparPointID;    // Coordinate ID used for the spar
	int     utilPointID;    // Coordinate ID used for the utility

	/***********************************************************************************
	 * LOCATE SOLUTION *
	 ***********************************************************************************/
    double offset;		// Measured offset in m
    double depth;		// Measured depth in m
    double range;		// Measured (sonde) range in m
    double current;		// Measured AC current in Amperes
    double yaw;			// Measured yaw in radians
    double pitch;		// Measured (sonde) pitch in radians
	double dOffset;		// Offset confidence in m
    double dDepth;		// Depth confidence in m
    double dRange;		// Range confidence in m
    double dCurrent;	// AC current confidence in Amperes
    double dYaw;		// Yaw confidence in radians
    double dPitch;		// Pitch confidence in radians
	double offsetCorr;  // Offset correction from solution to peak
	double sparHeading; // Measured spar heading in radians
	double sparPitch;   // Measured spar pitch in radians
	double sparRoll;    // Measured spar roll in radians

    double dBField;		// Total measured field in dB
    double frequency;	// Frequency used for this measurement (Hertz)

	char solutionMode;	// See sparMode
    char sparConnected;	// Boolean
    char batPercent;	// Battery percentage 0-100

	char action;        // User action, see sparAction

    /***********************************************************************************
     * SPAR SYSTEM *
     ***********************************************************************************/
    int serialNum;		// Spar serial number
    int firmwareRev;	// From EEPROM

	/***********************************************************************************
	 * STATUS OF FSV *
	 ***********************************************************************************/
	int    fsvStatus;  // Program status (running, failed, etc.)
};

class TSparData : public TObject
{
private:
	int pID;
	Spar* spardata;
	Spar lastrebuilded;
	HANDLE spardatareadyevent;
protected:

public:
	__fastcall TSparData()
	{
		spardata=new Spar;
		pID=0;
		spardatareadyevent=CreateEvent(NULL, true, false, NULL);
	}
	__fastcall ~TSparData() {CloseHandle(spardatareadyevent); delete spardata;}

	void __fastcall ReBuild()
	{
		if(spardata->latitude!=0 || spardata->longitude!=0 || spardata->altitude!=0)// && spardata->utc>0)
		{
			spardata->pointID=++pID;
			lastrebuilded=*spardata;
			SetEvent(spardatareadyevent);
		}
		else ResetEvent(spardatareadyevent);
	}

	long double DegToRad(long double src) {return M_PI*src/180.0;}
	long double RadToDeg(long double src) {return 180.0*src/M_PI;}
	long double DegMinToRad(long double lt){return DegToRad((long double)((int)(lt/100.))+(lt-100.0*(long double)((int)(lt/100.)))/60.);}
	long double RadToDegMin(long double lt){lt=RadToDeg(lt); return (long double)(100*(int)lt)+60.*(lt-(int)lt);}

	__property HANDLE SparDataReadyEvent = {read=spardatareadyevent};
	__property Spar* SparDataPtr = {read=spardata};
	__property Spar LastRebuilded = {read=lastrebuilded};
};

enum TxType 
{
	ORI=0, 
	VXMT,
	RD, 
	TX_3M, 
	SONDE, 
	POWER, 
	ELF,
	CUSTOM
};

enum RmsType
{
	HORZ=0,
	VERT,
	BOTH
};

/*
 *  Definition of Line form, and FieldSensSetupForm values
 */
struct FieldSensDefaults
{
	double	hsdv;			// spar horizontal standard deviation in meters
	double	vsdv;			// spar vertical standard deviation in meters
	double	maxOffset;		// max utiliy offset
	double	maxDepth;		// max utility depth
	double	maxRange;		// max utility depth
	bool	bLineFix;		// fix required (applies to point and line measurements)
	bool	bModelFix;		// model fix required
	int	    iStartingPointNum;
	int	    iMinEpochs;
	double	utilHsdv;		// util horizontal standard deviation in meters
	double	utilVsdv;		// util vertical standard deviation in meters;
	double	utilRsdv;		// util vertical standard deviation in meters;
	int		minDB;			// minimum required dB level for current (0-100)
	bool	bWithConstraints; // logging with constraints
};

/*
 *
 */
struct SparDefaults
{
	double	frequency;		// desired active frequency
	int		transmitterType; // see TxType above
	int		averageType;	// off, linear, or exponential
	int		averageTime;	// dialog index, to be later converted to scans by FS View
	int		updateRate;		// dialog index, to be later converted to scans by FS View
	int		hvRMS;			// see RmsType, selects what utility confidences are used to create line codes
	int		sparPort;
	int		panId;
	int		sparChannelMask;
	int		numSparsUsed;
	bool	bInternalRTK;
};

/*
 * Definition of Offset setup form values
 */
struct OffsetsDefaults
{
    double heightOverGround;  // Spar height over ground (m)
    double sparToAntX;		  // Spar to antenna fwd (m)
    double sparToAntY;		  // Spar to antenna right (m)
    double antennaHeight;		  // Spar to antenna up (m)
    double endSparToAntX;	  // Spar to antenna fwd (m)
    double endSparToAntY;	  // Spar to antenna right (m)
    double endSparAntHeight;	  // Spar to antenna up (m)
    double endSparX;		  // end dev easting (m)
    double endSparY;		  // end dev northing (m)
    double endSparZ;	      // end dev up (m)
    double baseSparX;		  // base easting (m)
    double baseSparY;		  // base northing (m)
    double baseSparZ;	      // base up (m)
    double utilityDiameter;   // Diameter of utility (m)
	int	   depthTo;			  // Depth measured to where (top, center, bottom)
	int	   displayUnits;      // M, ft, in
	bool   bUseMovingBaseline;
	bool   bRelativeBaseline; // treat spar-to-spar baseline as relative in spar coordinates	
};

// Management of the current spar status
struct SparStatus
{
    int serialNum;      // Spar serial number
    int firmwareRev;    // From EEPROM
    double frequency;	// current frequency
	char solutionMode;	// See sparMode
    char sparConnected;	// Boolean
    char batPercent;	// Battery percentage 0-100

	//bool baseSparSync;	// base spar PPS sync
	//bool endSparSync;	// end spar PPS sync
	//int sparSyncElapsedSec; // seconds since a spar PPS sync was invoked
	//int txSyncElapsedSec;	// seconds since a transmitter sync was invoked on Base Spar
	//int txPhaseOffset;	// smoothed phase offset of Base Spar to transmitter based
};

/*
 * Definition of Confidence threshold default values
 */
struct ConfidenceDefaults
{	
    float threshold[8][5];
};

/*
 *

struct FieldCodeKeys
{
    tsc_String		lCode[8][6];		// line codes
	tsc_String		cCode[8];			// line control
	tsc_String		aCode;				// antenna position code
};*/

/*
 *

struct FieldCodeDesc
{
	tsc_String		lCode[8][6];		// line codes
	tsc_String		cCode[8];			// line control
	tsc_String		aCode;				// antenna position code
};*/


// default values for all parameters that are saved in the Settings object
#define LOG_DEFAULT				false	// continuous
#define CONSTRAINTS_DEFAULT		false	// with tolerances
#define MIN_DB_DEFAULT			0
#define UTIL_HSDV_MAX_DEFAULT	1.0
#define UTIL_VSDV_MAX_DEFAULT	2.0
#define UTIL_RSDV_MAX_DEFAULT	1.0		// range SDV
#define UTIL_OFFSET_MAX_DEFAULT	10.0
#define UTIL_DEPTH_MAX_DEFAULT	10.0
#define UTIL_RANGE_MAX_DEFAULT	10.0
#define SPAR_HSDV_MAX_DEFAULT	0.5
#define SPAR_VSDV_MAX_DEFAULT	1.0

#define REQUIRE_FIX_DEFAULT		true
#define REQUIRE_MODELFIX_DEFAULT false
#define STARTING_POINT_NUM_DEFAULT 1
#define MIN_EPOCHS_DEFAULT		1

#define FREQ_DEFAULT			512.0
#define TX_TYPE_DEFAULT			ORI
#define AVG_TYPE_DEFAULT		0		// off
#define AVG_TIME_DEFAULT		0		// 1 scan
#define UPDATE_RATE_DEFAULT		0		// 1 scan
#define RMS_TYPE_DEFAULT		BOTH	
#define SPAR_COMPORT_DEFAULT	1
#define PANID_DEFAULT			56863
#define CHANNEL_MASK_DEFAULT	2048		
#define SPARS_USED_DEFAULT		1	
#define INTERNAL_RTK_DEFAULT	false		
#define HEIGHT_OVER_GND_DEFAULT	0.4
#define SPAR_TO_ANT_X_DEFAULT	-0.1	
#define SPAR_TO_ANT_Y_DEFAULT	0.0
#define ANT_HEIGHT_DEFAULT		2.0	
#define END_SPAR_TO_ANT_X_DEFAULT	-0.1	
#define END_SPAR_TO_ANT_Y_DEFAULT	0.0
#define END_ANT_HEIGHT_DEFAULT	1.0	
#define END_SPAR_X_DEFAULT		0
#define END_SPAR_Y_DEFAULT		0
#define END_SPAR_Z_DEFAULT		0
#define BASE_SPAR_X_DEFAULT		0
#define BASE_SPAR_Y_DEFAULT		0
#define BASE_SPAR_Z_DEFAULT		0
#define UTILITY_DIAMETER_DEFAULT 0	
#define DEPTH_TO_DEFAULT		1	
#define FSV_DISPLAY_UNITS_DEFAULT 1	
#define USE_MOVING_BASELINE_DEFAULT false
#define RELATIVE_BASELINE_DEFAULT false

#define NORM_SENS	200e-6	// Amps
#define NORM_FREQ	491		// Hz

#endif
