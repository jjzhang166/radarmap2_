//---------------------------------------------------------------------------

#ifndef VectorMapH
#define VectorMapH
//---------------------------------------------------------------------------

#include "XMLMap.h"

class TVectorMapsContainer: public TMapsContainer
{
private:
protected:
	TDXFCollection *DXF;
public:
	__fastcall TVectorMapsContainer(TMyObject *AOwner, void* ASettings, TLockedDoubleRect *AViewportArea) : TMapsContainer(AOwner, ASettings, AViewportArea) {maptype=mtVector; DXF=new TDXFCollection();}
	__fastcall ~TVectorMapsContainer() {if(DXF) delete DXF; DXF=NULL;}
};

class TDXFMapsContainer: public TVectorMapsContainer
{
private:
protected:
public:
	__fastcall TDXFMapsContainer(TMyObject *AOwner, void* ASettings, TLockedDoubleRect *AViewportArea) : TVectorMapsContainer(AOwner, ASettings, AViewportArea) {maptype=mtDXF;}
	__fastcall ~TDXFMapsContainer() {}

	void __fastcall UtilityUpdate();
	void __fastcall InfoUpdate();
	void __fastcall Update(TMyObjectType Type) {TMapsContainer::Update(Type);}
	void __fastcall Update();
	void __fastcall RenderMap();
	void __fastcall LoadData(AnsiString Filename, HANDLE AStopItEvent, bool WithUpdate, TCoordinateSystem CS, bool Preview=false);
	void __fastcall LoadData(AnsiString Filename, HANDLE AStopItEvent, bool WithUpdate, bool Preview=false) {TMapsContainer::LoadData(Filename, AStopItEvent, WithUpdate, Preview);}
	void __fastcall LoadData(AnsiString Filename, HANDLE AStopItEvent) {TMapsContainer::LoadData(Filename, AStopItEvent);}//LoadData(Filename, AStopItEvent, true);}
	void __fastcall GetLayers(TCheckBoxTreeView *TreeView, TTreeNode *Root);
	void __fastcall SetLayers(TCheckBoxTreeView *TreeView, TTreeNode *Root);

	__property TDXFCollection *DXFCollection = {read=DXF};
};



#endif
