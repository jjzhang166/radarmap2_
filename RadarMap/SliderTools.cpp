//---------------------------------------------------------------------------


#pragma hdrstop

#include "SliderTools.h"
#include "Manager.h"
#include "LabelForm.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

//---------------------------------------------------------------------------
// TSliderToolButton
//---------------------------------------------------------------------------
__fastcall TSliderToolButton::TSliderToolButton(TMyObject *AOwner, Types::TRect Rect)
	: TControlObject(AOwner, Rect)
{
	initialwidth=Rect.Width();
	initialheight=Rect.Height();
	enabled=true;
	down=false;
	drawdownframe=false;
	align=stbaDefault;
	showcaption=false;
	freeglyphonchange=true;
	caption="";
	font=new TFont();
	font->Name="Arial";
	font->Size=10;
	normalglyph=NULL;
	hotglyph=NULL;
	disabledglyph=NULL;
	pressedglyph=NULL;
	normalglyphname="";
	disabledglyphname="";
	hotglyphname="";
	pressedglyphname="";
	Glyph=NULL;
	onminimizing=NULL;
	SmallResize();
}
//---------------------------------------------------------------------------

__fastcall TSliderToolButton::~TSliderToolButton()
{
	if(font)
	{
		delete font;
		font=NULL;
	}
	if(freeglyphonchange)
	{
		/*if(normalglyph)
		{
			delete normalglyph;
			normalglyph=NULL;
		}
		if(hotglyph)
		{
			delete hotglyph;
			hotglyph=NULL;
		}
		if(disabledglyph)
		{
			delete disabledglyph;
			disabledglyph=NULL;
		}
		if(pressedglyph)
		{
			delete pressedglyph;
			pressedglyph=NULL;
		}*/
		RemovePngFromContainer(normalglyphname);
		normalglyph=NULL;
		RemovePngFromContainer(hotglyphname);
		hotglyph=NULL;
		RemovePngFromContainer(disabledglyphname);
		disabledglyph=NULL;
		RemovePngFromContainer(pressedglyphname);
		pressedglyph=NULL;
	}
}
//---------------------------------------------------------------------------

void __fastcall TSliderToolButton::SmallResize()
{
	if(smallsize)
	{
		Rect.bottom=Rect.top+(initialheight>>1);
		Rect.right=Rect.left+(initialwidth>>1);
	}
	else
	{
		Rect.bottom=Rect.top+initialheight;
		Rect.right=Rect.left+initialwidth;
	}
}
//---------------------------------------------------------------------------

void __fastcall TSliderToolButton::writeAlign(TSliderToolButtonAlign value)
{
	align=value;
	if(align==stbaDefault) specialplace=false;
	else specialplace=true;
	if(Owner!=NULL) Owner->RebuildObjects();
}
//---------------------------------------------------------------------------

/*void __fastcall TSliderToolButton::writeGlyph(TBitmap32 *value)
{
	Glyph=NULL;
	if(normalglyph!=NULL && freeglyphonchange) delete normalglyph;
	normalglyph=value;
	Update();
}
//---------------------------------------------------------------------------

void __fastcall TSliderToolButton::writeHotGlyph(TBitmap32 *value)
{
	Glyph=NULL;
	if(hotglyph!=NULL && freeglyphonchange) delete hotglyph;
	hotglyph=value;
	Update();
}
//---------------------------------------------------------------------------

void __fastcall TSliderToolButton::writeDisabledGlyph(TBitmap32 *value)
{
	Glyph=NULL;
	if(disabledglyph!=NULL && freeglyphonchange) delete disabledglyph;
	disabledglyph=value;
	Update();
}
//---------------------------------------------------------------------------

void __fastcall TSliderToolButton::writePressedGlyph(TBitmap32 *value)
{
	Glyph=NULL;
	if(pressedglyph!=NULL && freeglyphonchange) delete pressedglyph;
	pressedglyph=value;
	Update();
}*/
//---------------------------------------------------------------------------

void __fastcall TSliderToolButton::writePressed(bool value)
{
	if(pressed!=value)
	{
		pressed=value;
		if(!enabled) Glyph=disabledglyph;
		else if((pressed || down) && pressedglyph!=NULL) Glyph=pressedglyph;
		else if(down && hotglyph!=NULL) Glyph=hotglyph;
		else if(hotglyph!=NULL) Glyph=hotglyph;
		else Glyph=normalglyph;
		if(Owner!=NULL) Owner->SetObjectChanged(objecttype);
	}
}
//---------------------------------------------------------------------------

void __fastcall TSliderToolButton::writeInRect(bool value)
{
	if(inrect!=value)
	{
		inrect=value;
		if(indragndrop && !value) indragndrop=false;
		if(!enabled)
		{
			/*if(down && pressedglyph!=NULL) Glyph=pressedglyph;
			else if(down && hotglyph!=NULL) Glyph=hotglyph;
			else Glyph=disabledglyph;*/
			Glyph=disabledglyph;
		}
		else if(inrect && hotglyph!=NULL) Glyph=hotglyph;
		else if(down && pressedglyph!=NULL) Glyph=pressedglyph;
		else if(down && hotglyph!=NULL) Glyph=hotglyph;
		else Glyph=normalglyph;
		/*if(down && pressedglyph!=NULL) Glyph=pressedglyph;
		else if(inrect && hotglyph!=NULL) Glyph=hotglyph;
		else Glyph=normalglyph;*/
		if(Owner!=NULL) Owner->SetObjectChanged(objecttype);
	}
}
//---------------------------------------------------------------------------

void __fastcall TSliderToolButton::writeEnabled(bool value)
{
	if(enabled!=value)
	{
		enabled=value;
		if(!enabled) Glyph=disabledglyph;
		else if(down && pressedglyph!=NULL) Glyph=pressedglyph;
		else if(down && hotglyph!=NULL) Glyph=hotglyph;
		else if(inrect && hotglyph!=NULL) Glyph=hotglyph;
		else Glyph=normalglyph;
		if(Owner!=NULL) Owner->SetObjectChanged(objecttype);
	}
}
//---------------------------------------------------------------------------

void __fastcall TSliderToolButton::MouseClick(System::TObject* Sender, int X, int Y)
{
	if(enabled && OnMouseClick!=NULL) (OnMouseClick)((TObject*)this, X, Y);
}
//---------------------------------------------------------------------------

void __fastcall TSliderToolButton::MouseMove(System::TObject* Sender,
	Classes::TShiftState Shift, int X, int Y, TCustomLayer *Layer)
{

}
//---------------------------------------------------------------------------

void __fastcall TSliderToolButton::MouseDown(TObject *Sender,
	TMouseButton Button, TShiftState Shift, int X, int Y,
	TCustomLayer *Layer)
{
	if(enabled && OnMouseDown!=NULL) (OnMouseDown)((TObject*)this, Button, Shift, X, Y);
}
//---------------------------------------------------------------------------

void __fastcall TSliderToolButton::MouseUp(TObject *Sender,
	TMouseButton Button, TShiftState Shift, int X, int Y,
	TCustomLayer *Layer)
{
	if(enabled && OnMouseUp!=NULL) (OnMouseUp)((TObject*)this, Button, Shift, X, Y);
}
//---------------------------------------------------------------------------

void __fastcall TSliderToolButton::MouseDblClick(System::TObject* Sender, int X, int Y)
{
	if(enabled && OnMouseDblClick!=NULL) (OnMouseDblClick)((TObject*)this, X, Y);
}
//---------------------------------------------------------------------------

void __fastcall TSliderToolButton::Redraw(TObject *Sender, TBitmap32 *Bmp)
{
	Types::TRect rct, src;
	int i;

	try
	{
		if(Visible && Bmp!=NULL & Rect.Width()!=0 && Rect.Height()!=0)
		{
			rct=Rect;
			if(rct.IsInRect(Owner->ViewPort))
			{
				if(Glyph==NULL)
				{
					if(!enabled)
					{
						//if(pressedglyph!=NULL) Glyph=pressedglyph;
						//else Glyph=disabledglyph;
						Glyph=disabledglyph;
					}
					else if(pressed || down)
					{
						if(pressedglyph!=NULL) Glyph=pressedglyph;
						else if(hotglyph!=NULL) Glyph=hotglyph;
						else Glyph=normalglyph;
					}
					else if(inrect)
					{
						if(hotglyph!=NULL) Glyph=hotglyph;
						else Glyph=normalglyph;
					}
					else Glyph=normalglyph;
				}
				if(Glyph && Glyph->Bits)
				{
					src=Glyph->ClipRect;
					if(src.Width()<rct.Width())
					{
						i=(rct.Width()-src.Width())>>1;
						rct.left+=i;
						rct.right-=i;
					}
					if(src.Height()<rct.Height())
					{
						i=(rct.Height()-src.Height())>>1;
						rct.top+=i;
						rct.bottom-=i;
					}
					Bmp->Draw(rct, src, Glyph);
					if(down && drawdownframe)
					{
						if(enabled) Bmp->FrameRectS(rct, clRed32);
						else Bmp->FrameRectS(rct, clGray32);
					}
				}
				else
				{
					if(!enabled) Bmp->FrameRectS(rct, clGray32);
					else if(inrect)	Bmp->FrameRectS(rct, clRed32);
					else Bmp->FrameRectS(rct, clGreen32);
				}
			}
		}
	}
	catch(Exception &e) {}
}
//---------------------------------------------------------------------------

void __fastcall TSliderToolButton::writeDown(bool value)
{
	down=value;
	if(!enabled)
	{
		/*if(pressedglyph!=NULL) Glyph=pressedglyph;
		else Glyph=disabledglyph;*/
		Glyph=disabledglyph;
	}
	else if(pressed || down)
	{
		if(pressedglyph!=NULL) Glyph=pressedglyph;
		else if(hotglyph!=NULL) Glyph=hotglyph;
		else Glyph=normalglyph;
	}
	else if(inrect)
	{
		if(hotglyph!=NULL) Glyph=hotglyph;
		else Glyph=normalglyph;
	}
	else Glyph=normalglyph;
	if(Owner!=NULL)
	{
		if(down && groupindex>0) Owner->ApplyGrouping(groupindex, index);
		Owner->SetObjectChanged(objecttype);
	}
}
//---------------------------------------------------------------------------

void __fastcall TSliderToolButton::writeDrawDownFrame(bool value)
{
	drawdownframe=value;
	if(enabled && down && Owner!=NULL)
		Owner->SetObjectChanged(objecttype);
}
//---------------------------------------------------------------------------

void __fastcall TSliderToolButton::writeGroupIndex(int value)
{
	groupindex=value;
	if(groupindex>0) writeDown(down);
}

/*void __fastcall Hide()
{
	Visible=false;
	if(Owner && Owner->ObjectType==gotContainer) ((TGraphicObjectsContainer*)Owner)->ClearModalObject(this);
}*/

//---------------------------------------------------------------------------
// TSliderToolbar
//---------------------------------------------------------------------------
__fastcall TSliderToolbar::TSliderToolbar(TMyObject *AOwner, Types::TRect OutputRect,
	int AButtonSize, TSliderToolbarAlign AAlign, bool Maximized) : TControlObject(AOwner, OutputRect)
{
	controltype=cotContainer;

	initialbuttonsize=buttonsize=AButtonSize;
	minimized=!Maximized;
	transparent=false;
	Buttons=new TGraphicObjectsList((TMyObject*)this);
	Childs=new TGraphicObjectsList((TMyObject*)this);
	Childs->FreeObjects=false;
	align=AAlign;
	rebuilded=false;
	visible=true;
	enabled=true;
	buttonsalign=stbsaCenter;
	fitbuttonsonly=false;
	color=Color32(255, 255, 255, 210);
	//color=Color32(224, 224, 224, 224);
	border=Color32(63, 63, 63, 100);
	MinMaxBtnRect=Types::TRect(0, 0, 0, 0);
	if(AAlign==staCenter)
	{
		MinMaxBtn=new TSliderToolButton((TMyObject*)this, Types::TRect(0, 0, AButtonSize, AButtonSize));
		ButtonOutline=2;
		BorderWidth=1;
	}
	else
	{
		MinMaxBtn=new TSliderToolButton((TMyObject*)this, Types::TRect(0, 0, 32, 32));
		ButtonOutline=4;
		BorderWidth=1;
	}
	MinMaxBtn->OnMouseClick=MaximizeMinimize;
	SmallResize();
}
//---------------------------------------------------------------------------

__fastcall TSliderToolbar::~TSliderToolbar()
{
	delete Buttons;
	for(int i=0; i<Childs->Count; i++)
		((TSliderToolbarChild*)Childs->Items[i])->DismissParent(this);
	delete Childs;
}
//---------------------------------------------------------------------------

void __fastcall TSliderToolbar::Show()
{
	for(int i=1; i<Buttons->Count; i++)
	{
		//if(((TSliderToolButton*)Buttons->Items[i])->ControlType==cotContainer)
		((TSliderToolButton*)Buttons->Items[i])->Show();
	}
	GlobalPressed=false; TGraphicObject::Show();
}

void __fastcall TSliderToolbar::Hide()
{
	for(int i=1; i<Buttons->Count; i++)
	{
		//if(((TSliderToolButton*)Buttons->Items[i])->ControlType==cotContainer)
		((TGraphicObject*)Buttons->Items[i])->Hide();
		for(int i=0; i<Childs->Count; i++)
			Childs->Items[i]->Hide();
	}
	TGraphicObject::Hide();
	GlobalPressed=false;
}
//---------------------------------------------------------------------------

void __fastcall TSliderToolbar::MaximizeMinimize(System::TObject* Sender, int X, int Y)
{
	TSliderToolButton* po;

	try
	{
		if(((TGraphicObject*)Sender)->Pressed)
		{
			if(align==staCenter) Hide();
			else Minimized=!Minimized;
			for(int i=1; i<Buttons->Count; i++)
			{
				po=(TSliderToolButton*)Buttons->Items[i];
				if(po && po->OnMinimizing) (po->OnMinimizing)(Minimized);
			}
		}
	}
	catch(Exception &e) {}
}
//---------------------------------------------------------------------------

void __fastcall TSliderToolbar::writeMinimized(bool value)
{
	TSliderToolButton* po;

	minimized=value;
	if(minimized)
	{
		for(int i=1; i<Buttons->Count; i++)
		{
			if(Buttons->Items[i])
			{
				po=(TSliderToolButton*)Buttons->Items[i];
				po->InRect=false;
				po->Pressed=false;
				po->Down=false;
			}
		}
	}
	ApplyChanging();
	if(!minimized) RebuildObjects();
}
//---------------------------------------------------------------------------

void __fastcall TSliderToolbar::writeOutput(Types::TRect value)
{
	output=value;
	if(Buttons && Buttons->Count>1)
	{
		ApplyChanging();
		RebuildObjects();
	}
}
//---------------------------------------------------------------------------

void __fastcall TSliderToolbar::ApplyChanging()
{
	Resize();
	switch(align)
	{
		case staCenter:
			MinMaxBtn->NormalGlyphName="PNGIMAGE_290"; //close
			MinMaxBtn->HotGlyphName="PNGIMAGE_289";
			MinMaxBtn->DisabledGlyphName="PNGIMAGE_289";
			break;
		case staLeft:
			if(minimized)
			{
				MinMaxBtn->NormalGlyphName="PNGIMAGE_7";
				MinMaxBtn->HotGlyphName="PNGIMAGE_9";
				MinMaxBtn->DisabledGlyphName="PNGIMAGE_8";
			}
			else
			{
				MinMaxBtn->NormalGlyphName="PNGIMAGE_4";
				MinMaxBtn->HotGlyphName="PNGIMAGE_6";
				MinMaxBtn->DisabledGlyphName="PNGIMAGE_5";
			}
			break;
		case staTop:
			if(minimized)
			{
				MinMaxBtn->NormalGlyphName="PNGIMAGE_1";
				MinMaxBtn->HotGlyphName="PNGIMAGE_3";
				MinMaxBtn->DisabledGlyphName="PNGIMAGE_2";
			}
			else
			{
				MinMaxBtn->NormalGlyphName="PNGIMAGE_10";
				MinMaxBtn->HotGlyphName="PNGIMAGE_12";
				MinMaxBtn->DisabledGlyphName="PNGIMAGE_11";
			}
			break;
		case staRight:
			if(minimized)
			{
				MinMaxBtn->NormalGlyphName="PNGIMAGE_4";
				MinMaxBtn->HotGlyphName="PNGIMAGE_6";
				MinMaxBtn->DisabledGlyphName="PNGIMAGE_5";
			}
			else
			{
				MinMaxBtn->NormalGlyphName="PNGIMAGE_7";
				MinMaxBtn->HotGlyphName="PNGIMAGE_9";
				MinMaxBtn->DisabledGlyphName="PNGIMAGE_8";
			}
			break;
		case staBottom:
			if(minimized)
			{
				MinMaxBtn->NormalGlyphName="PNGIMAGE_10";
				MinMaxBtn->HotGlyphName="PNGIMAGE_12";
				MinMaxBtn->DisabledGlyphName="PNGIMAGE_11";
			}
			else
			{
				MinMaxBtn->NormalGlyphName="PNGIMAGE_1";
				MinMaxBtn->HotGlyphName="PNGIMAGE_3";
				MinMaxBtn->DisabledGlyphName="PNGIMAGE_2";
			}
			break;
	}
	MinMaxBtn->Width=MinMaxBtn->NormalGlyph->Width;
	MinMaxBtn->Height=MinMaxBtn->NormalGlyph->Height;
	if(align==staCenter)
	{
		MinMaxBtn->Left=Rect.Width()-MinMaxBtn->Width-ButtonOutline-BorderWidth;
		MinMaxBtn->Top=Rect.Height()-MinMaxBtn->Height-ButtonOutline-BorderWidth;
	}
	else
	{
		if(align==staLeft || align==staRight)
		{
			MinMaxBtn->Left=(Size-MinMaxBtn->Width)>>1;
			MinMaxBtn->Top=(Rect.Height()-MinMaxBtn->Height)>>1;
		}
		else
		{
			MinMaxBtn->Left=(Rect.Width()-MinMaxBtn->Width)>>1;
			MinMaxBtn->Top=(Size-MinMaxBtn->Height)>>1;
		}
		MinMaxBtnRect=Types::TRect(MinMaxBtn->Left-((Size-MinMaxBtn->Width)>>1),
			MinMaxBtn->Top-((buttonsize-MinMaxBtn->Height)>>1),
			MinMaxBtn->Left+MinMaxBtn->Width+((Size-MinMaxBtn->Width)>>1),
			MinMaxBtn->Top+MinMaxBtn->Height+((buttonsize-MinMaxBtn->Height)>>1));
	}
	if(Owner!=NULL) Owner->SetObjectChanged(objecttype);
}
//---------------------------------------------------------------------------

void __fastcall TSliderToolbar::SmallResize()
{
	bool b=AutomaticUpdate;

	AutomaticUpdate=false;
	if(smallsize)
	{
		buttonsize=initialbuttonsize>>1;
		//ButtonOutline=2;
	}
	else
	{
		buttonsize=initialbuttonsize;
		//ButtonOutline=4;
	}
	MinMaxBtn->SmallSize=smallsize;
	for(int i=1; i<Buttons->Count; i++)
		((TSliderToolButton*)Buttons->Items[i])->SmallSize=smallsize;
	ApplyChanging();
	RebuildObjects();
	AutomaticUpdate=b;
}
//---------------------------------------------------------------------------

void __fastcall TSliderToolbar::Resize()
{
	int x, y, w, h, oh, ow, th;
	float r;
	TBitmap32 *Bmp;
	/*Types::TRect output;

	if(Owner && Owner->ViewPort.Width()>0 && Owner->ViewPort.Height()>0)
		output=Owner->ViewPort;
	else output=Output;*/
	Size=buttonsize+2*ButtonOutline+2*BorderWidth;
	if(fitbuttonsonly && Buttons!=NULL && Buttons->Count>1)
	{
		oh=ow=(int)(((float)Buttons->Count+0.5)*(float)Size);
		if(ow>output.Width()) ow=output.Width();
		if(oh>output.Height()) oh=output.Height();
	}
	else
	{
		oh=output.Height();
		ow=output.Width();
	}
	switch(align)
	{
		case staCenter:
			Columns=sqrt((float)Buttons->Count)+1;
			w=Columns*(buttonsize+2*ButtonOutline)+2*BorderWidth+2*ButtonOutline;
			while(w>output.Width()-Size && Columns>1)
			{
				Columns--;
				w=Columns*(buttonsize+2*ButtonOutline)+2*BorderWidth;
			}
			r=(float)Buttons->Count/(float)Columns;
			if(r-(int)r>0.01) Rows=(int)r+1;
			else Rows=(int)r;
			h=Rows*(buttonsize+2*ButtonOutline)+2*BorderWidth+2*ButtonOutline;
			if(ShowTitle && Title!="")
			{
				Bmp=new TBitmap32;
				try
				{
					Bmp->Font->Name="Tahoma";
					Bmp->Font->Size=12;
					th=2*Bmp->TextHeight(Title)+BorderWidth+2*ButtonOutline;
					h+=th;
				}
				__finally
				{
					delete Bmp;
				}
			}
			else th=0;
			while(h>output.Height()-Size && Rows>1)
			{
				Rows--;
				h=Rows*(buttonsize+2*ButtonOutline)+2*BorderWidth+th;
			}
			x=(output.Width()-w)>>1;
			y=(output.Height()-h)>>1;
			break;
		case staLeft:
			Columns=1;
			if(minimized)
			{
				Rows=1;
				x=0;
				w=h=Size;
				y=(output.Height()-Size)>>1;
			}
			else
			{
				Rows=Buttons->Count;
				x=0;
				w=Size;
				//h=output.Height();
				h=oh;
				//y=0;
				y=(output.Height()-h)>>1;
			}
			break;
		case staTop:
			Rows=1;
			if(minimized)
			{
				Columns=1;
				x=(output.Width()-Size)>>1;
				y=0;
				w=h=Size;
			}
			else
			{
				Columns=Buttons->Count;
				y=0;
				//w=output.Width();
				w=ow;
				//x=0;
				x=(output.Width()-w)>>1;
				h=Size;
			}
			break;
		case staRight:
			Columns=1;
			if(minimized)
			{
				Rows=1;
				x=output.Width()-Size;
				w=h=Size;
				y=(output.Height()-Size)>>1;
			}
			else
			{
				Rows=Buttons->Count;
				x=output.Width()-Size;
				w=Size;
				//h=output.Height();
				h=oh;
				//y=0;
				y=(output.Height()-h)>>1;
			}
			break;
		case staBottom:
			Columns=Buttons->Count;
			Rows=1;
			if(minimized)
			{
				Columns=1;
				x=(output.Width()-Size)>>1;
				y=output.Height()-Size;
				w=h=Size;
			}
			else
			{
				Columns=Buttons->Count;
				y=output.Height()-Size;
				//w=output.Width();
				w=ow;
				//x=0;
				x=(output.Width()-w)>>1;
				h=Size;
			}
			break;
	}
	Rect=Types::TRect(x, y, x+w, y+h);
	if(Owner!=NULL) Owner->SetObjectChanged(objecttype);
	resized=true;
}
//---------------------------------------------------------------------------

void __fastcall TSliderToolbar::RebuildObjects()
{
	int first, last, cnt, i, j, e, cnt2;
	TSliderToolButton *po;

	if(align==staCenter)
	{
		int x0, y0, N, th;
		TBitmap32 *Bmp;

		x0=BorderWidth+ButtonOutline;
        if(ShowTitle && Title!="")
		{
			Bmp=new TBitmap32;
			try
			{
				Bmp->Font->Name="Tahoma";
				Bmp->Font->Size=12;
				th=2*Bmp->TextHeight(Title)+BorderWidth+2*ButtonOutline;
			}
			__finally
			{
				delete Bmp;
			}
		}
		else th=0;
		y0=BorderWidth+ButtonOutline+th;
		N=Columns*Rows;
		if(N>Buttons->Count) N=Buttons->Count;
		for(i=1, j=0, e=0; i<N; i++, j++)
		{
			if(j<0) j=0;
			if(j>=Columns)
			{
				j=0;
				e++;
            }
			po=(TSliderToolButton*)Buttons->Items[i];
			if(po)
			{
				po->Left=x0+j*Size;
				po->Top=y0+e*Size;
			}
			else j--;
		}
	}
	else if(!minimized && Rect.Width()>0 && Rect.Height()>0)
	{
		first=ButtonOutline;
		if(align==staLeft || align==staRight)
		{
			last=Rect.Height();
			e=MinMaxBtn->Top-((buttonsize-MinMaxBtn->Height)>>1)-ButtonOutline;
		}
		else
		{
			last=Rect.Width();
			e=MinMaxBtn->Left-((buttonsize-MinMaxBtn->Width)>>1)-ButtonOutline;
		}
		last-=ButtonOutline;
		cnt=0;
		for(i=1; i<Buttons->Count; i++)
		{
			po=(TSliderToolButton*)Buttons->Items[i];
			if(po->SpecialPlace)
			{
				if(po->Align==stbaFirst)
				{
					if(align==staLeft || align==staRight) po->Top=first;
					else po->Left=first;
					first+=(buttonsize+ButtonOutline);
				}
				else if(po->Align==stbaLast)
				{
					last-=buttonsize;
					if(align==staLeft || align==staRight) po->Top=last;
					else po->Left=last;
					last-=ButtonOutline;
				}
			}
			else cnt++;
		}
		cnt2=(int)((float)(e-first)/(float)(buttonsize+ButtonOutline));
		cnt2+=(int)((float)(last-e-buttonsize)/(float)(buttonsize+ButtonOutline));
		if(cnt>cnt2) cnt=cnt2;
		/*if((cnt+1)*(buttonsize+ButtonOutline)>(last-first))
			cnt=(int)((float)(last-first)/(float)(buttonsize+ButtonOutline))-1;*/
		switch(buttonsalign)
		{
			case stbsaCenter:
				//first+=((last-first)>>1)-(cnt*(buttonsize+ButtonOutline)>>1); break;
				first=e-(buttonsize+ButtonOutline)*(int)(0.5+(float)cnt/2.)-2*ButtonOutline; break; //
			case stbsaLast:	//first=last-cnt*(buttonsize+ButtonOutline); break;
				if((cnt*(buttonsize+ButtonOutline)>>1)<(last-e-buttonsize+ButtonOutline))
					first=last-cnt*(buttonsize+ButtonOutline);
				else first=e-(cnt-(last-e-buttonsize+ButtonOutline)/(buttonsize+ButtonOutline))*(buttonsize+ButtonOutline);
				break;
			case stbsaFirst:
				first=e-(cnt-(e-first)/(buttonsize+ButtonOutline))*(buttonsize+ButtonOutline);
				break;
		}
		if(first<0) first=0;
		for(i=1, j=0; i<Buttons->Count; i++, j++)
		{
			po=(TSliderToolButton*)Buttons->Items[i];
			if(align==staLeft || align==staRight)
			{
				po->Left=BorderWidth+ButtonOutline;
				if(!po->SpecialPlace)
				{
					if(j<cnt) po->Top=first;
					else po->Top=-buttonsize;
				}
			}
			else
			{
				po->Top=BorderWidth+ButtonOutline;
				if(!po->SpecialPlace)
				{
					if(j<cnt) po->Left=first;
					else po->Left=-buttonsize;
				}
			}
			if(!po->SpecialPlace) first+=(buttonsize+ButtonOutline);
			/*while ((first>=e && first<(e+buttonsize+2*ButtonOutline)) ||
				((first+buttonsize)>=e && (first+buttonsize)<(e+buttonsize+2*ButtonOutline)))
			{
				//first+=(buttonsize+4*ButtonOutline);
				first+=ButtonOutline;
			} */
			if((first>=e && first<(e+buttonsize+2*ButtonOutline)) ||
				((first+buttonsize)>=e && (first+buttonsize)<(e+buttonsize+ButtonOutline)))
					first=e+(buttonsize+4*ButtonOutline);
		}
	}
}
//---------------------------------------------------------------------------

Types::TRect __fastcall TSliderToolbar::readViewPort()
{
	//return ((TGraphicObjectsContainer*)Owner)->image->GetViewportRect();
	return Types::TRect(0, 0, Rect.Width(), Rect.Height());
}
//---------------------------------------------------------------------------

void __fastcall TSliderToolbar::Redraw(TObject *Sender, TBitmap32 *Bmp)
{
	Types::TRect rct, crt;
	TSliderToolButton *po;
	TBitmap32 *tmp;
	TPolygon32* poly;
	int th, tw;
	AnsiString str;

	if(Visible && Bmp!=NULL && Rect.Width()!=0 && Rect.Height()!=0)
	{
		rct=Rect;
		if(rct.IsInRect(Owner->ViewPort))
		{
			tmp=new TBitmap32();
			poly=new TPolygon32();
			try
			{
				tmp->SetSize(Rect.Width(), Rect.Height());
				tmp->DrawMode=dmBlend;
				tmp->BeginUpdate();
				crt=Types::TRect(0, 0, rct.Width(), rct.Height());
				if(!transparent)
				{
					poly->Antialiased=true;
					poly->Closed=true;
					switch(align)
					{
						case staCenter:
							poly->Add(FixedPoint((int)crt.left, (int)crt.top));
							poly->Add(FixedPoint((int)crt.right, (int)crt.top));
							poly->Add(FixedPoint((int)crt.right, (int)crt.bottom));
							poly->Add(FixedPoint((int)crt.left, (int)crt.bottom));
							break;
						case staRight:
							poly->Add(FixedPoint((int)crt.right, crt.top));
							poly->Add(FixedPoint((int)crt.left+2*ButtonOutline, crt.top));
							poly->Add(FixedPoint((int)crt.left, crt.top+2*ButtonOutline));
							if(!minimized)
							{
								poly->Add(FixedPoint((int)crt.left, MinMaxBtnRect.top-2*ButtonOutline-ButtonOutline));
								poly->Add(FixedPoint((int)crt.left+2*ButtonOutline, MinMaxBtnRect.top-ButtonOutline));
								poly->Add(FixedPoint((int)crt.right-4*ButtonOutline, MinMaxBtnRect.top-ButtonOutline));
								poly->Add(FixedPoint((int)crt.right-2*ButtonOutline, MinMaxBtnRect.top+2*ButtonOutline-ButtonOutline));
								poly->Add(FixedPoint((int)crt.right-2*ButtonOutline, MinMaxBtnRect.bottom-2*ButtonOutline));
								poly->Add(FixedPoint((int)crt.right-4*ButtonOutline, MinMaxBtnRect.bottom));
								poly->Add(FixedPoint((int)crt.left+2*ButtonOutline, MinMaxBtnRect.bottom));
								poly->Add(FixedPoint((int)crt.left, MinMaxBtnRect.bottom+2*ButtonOutline));
							}
							poly->Add(FixedPoint((int)crt.left, crt.bottom-2*ButtonOutline-1));
							poly->Add(FixedPoint((int)crt.left+2*ButtonOutline, crt.bottom-1));
							poly->Add(FixedPoint((int)crt.right, crt.bottom-1));
							break;
						case staLeft:
							poly->Add(FixedPoint((int)crt.left, crt.top));
							poly->Add(FixedPoint((int)crt.right-2*ButtonOutline, crt.top));
							poly->Add(FixedPoint((int)crt.right, crt.top+2*ButtonOutline));
							if(!minimized)
							{
								poly->Add(FixedPoint((int)crt.right-1, MinMaxBtnRect.top-2*ButtonOutline-ButtonOutline));
								poly->Add(FixedPoint((int)crt.right-2*ButtonOutline-1, MinMaxBtnRect.top-ButtonOutline));
								poly->Add(FixedPoint((int)crt.left+4*ButtonOutline, MinMaxBtnRect.top-ButtonOutline));
								poly->Add(FixedPoint((int)crt.left+2*ButtonOutline, MinMaxBtnRect.top+2*ButtonOutline-ButtonOutline));
								poly->Add(FixedPoint((int)crt.left+2*ButtonOutline, MinMaxBtnRect.bottom-2*ButtonOutline));
								poly->Add(FixedPoint((int)crt.left+4*ButtonOutline, MinMaxBtnRect.bottom));
								poly->Add(FixedPoint((int)crt.right-2*ButtonOutline-1, MinMaxBtnRect.bottom));
								poly->Add(FixedPoint((int)crt.right-1, MinMaxBtnRect.bottom+2*ButtonOutline));
							}
							poly->Add(FixedPoint((int)crt.right-1, crt.bottom-2*ButtonOutline-1));
							poly->Add(FixedPoint((int)crt.right-2*ButtonOutline-1, crt.bottom-1));
							poly->Add(FixedPoint((int)crt.left, crt.bottom-1));
							break;
						case staTop:
							poly->Add(FixedPoint((int)crt.left, crt.top));
							poly->Add(FixedPoint((int)crt.left, crt.bottom-2*ButtonOutline));
							poly->Add(FixedPoint((int)crt.left+2*ButtonOutline, crt.bottom));
							if(!minimized)
							{
								poly->Add(FixedPoint((int)MinMaxBtnRect.left-2*ButtonOutline, crt.bottom));
								poly->Add(FixedPoint((int)MinMaxBtnRect.left, crt.bottom-2*ButtonOutline));
								poly->Add(FixedPoint((int)MinMaxBtnRect.left, crt.top+4*ButtonOutline));
								poly->Add(FixedPoint((int)MinMaxBtnRect.left+2*ButtonOutline, crt.top+2*ButtonOutline));
								poly->Add(FixedPoint((int)MinMaxBtnRect.right-2*ButtonOutline, crt.top+2*ButtonOutline));
								poly->Add(FixedPoint((int)MinMaxBtnRect.right, crt.top+4*ButtonOutline));
								poly->Add(FixedPoint((int)MinMaxBtnRect.right, crt.bottom-2*ButtonOutline));
								poly->Add(FixedPoint((int)MinMaxBtnRect.right+2*ButtonOutline, crt.bottom));
							}
							poly->Add(FixedPoint((int)crt.right-2*ButtonOutline-1, crt.bottom));
							poly->Add(FixedPoint((int)crt.right-1, crt.bottom-2*ButtonOutline));
							poly->Add(FixedPoint((int)crt.right-1, crt.top));
							break;
						case staBottom:
							poly->Add(FixedPoint((int)crt.left, crt.bottom));
							poly->Add(FixedPoint((int)crt.left, crt.top+2*ButtonOutline));
							poly->Add(FixedPoint((int)crt.left+2*ButtonOutline, crt.top));
							if(!minimized)
							{
								poly->Add(FixedPoint((int)MinMaxBtnRect.left-2*ButtonOutline-ButtonOutline, crt.top));
								poly->Add(FixedPoint((int)MinMaxBtnRect.left-ButtonOutline, crt.top+2*ButtonOutline));
								poly->Add(FixedPoint((int)MinMaxBtnRect.left-ButtonOutline, crt.bottom-4*ButtonOutline));
								poly->Add(FixedPoint((int)MinMaxBtnRect.left+2*ButtonOutline-ButtonOutline, crt.bottom-2*ButtonOutline));
								poly->Add(FixedPoint((int)MinMaxBtnRect.right-2*ButtonOutline, crt.bottom-2*ButtonOutline));
								poly->Add(FixedPoint((int)MinMaxBtnRect.right, crt.bottom-4*ButtonOutline));
								poly->Add(FixedPoint((int)MinMaxBtnRect.right, crt.top+2*ButtonOutline));
								poly->Add(FixedPoint((int)MinMaxBtnRect.right+2*ButtonOutline, crt.top));
							}
							poly->Add(FixedPoint((int)crt.right-2*ButtonOutline-1, crt.top));
							poly->Add(FixedPoint((int)crt.right-1, crt.top+2*ButtonOutline));
							poly->Add(FixedPoint((int)crt.right-1, crt.bottom));
							break;
					}
					poly->Draw(tmp, border, color);
				}
				//tmp->FrameRectTS(crt, border);
				if(minimized && align!=staCenter) MinMaxBtn->Redraw(Sender, tmp);
				else
				{
					for(int i=0; i<Buttons->Count; i++)
					{
						po=(TSliderToolButton*)Buttons->Items[i];
						po->Redraw(Sender, tmp);
					}
				}
				if(align==staCenter && ShowTitle && Title!="")
				{
					tmp->Font->Name="Tahoma";
					tmp->Font->Size=12;
					tmp->Font->Style=TFontStyles();
					str=Title;
					th=2*tmp->TextHeight(str)+BorderWidth+2*ButtonOutline;
					tw=tmp->TextWidth(str);
					while(tw>crt.Width()-4*BorderWidth-4*ButtonOutline)
					{
						str.SetLength(str.Length()-4);
						str+="...";
						tw=tmp->TextWidth(str);
					}
					//tmp->RenderText((crt.Width()-tw)>>1, BorderWidth+ButtonOutline, str, 0, clBlack32);
					if(Transparent) tmp->FillRectTS(crt.Left+BorderWidth+ButtonOutline, crt.top, crt.right-BorderWidth-ButtonOutline, crt.top+th-ButtonOutline, border);
					else tmp->FillRectTS(crt.Left, crt.top, crt.right, crt.top+th, border);
					tmp->RenderText((BorderWidth+ButtonOutline)*Transparent+((th-tmp->TextHeight(str))>>1), (th-tmp->TextHeight(str))>>1, str, 2, clWhite32);
				}
				tmp->EndUpdate();
				Bmp->Draw(rct, crt, tmp);
			}
			__finally
			{
				poly->Clear();
				delete poly;
				delete tmp;
			}
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TSliderToolbar::writeInRect(bool value)
{
	TSliderToolButton *po;

	if(inrect!=value)
	{
		inrect=value;
		if(indragndrop && !value) indragndrop=false;
		if(!inrect)
		{
			for(int i=0; i<Buttons->Count; i++)
			{
				po=(TSliderToolButton*)Buttons->Items[i];
				po->InRect=false;
			}
		}
		if(Owner!=NULL) Owner->SetObjectChanged(objecttype);
	}
}
//---------------------------------------------------------------------------

void __fastcall TSliderToolbar::writePressed(bool value)
{
	TSliderToolButton *po;

	if(pressed!=value) pressed=value;
	if(!value)
	{
		for(int i=0; i<Buttons->Count; i++)
		{
			po=(TSliderToolButton*)Buttons->Items[i];
			if(po) po->Pressed=false;
		}
		GlobalPressed=false;
	}
}
//---------------------------------------------------------------------------

void __fastcall TSliderToolbar::MouseMove(System::TObject* Sender,
	Classes::TShiftState Shift, int X, int Y, TCustomLayer *Layer)
{
	TSliderToolButton *po;
	bool entered=false;
	Types::TPoint p;

	X-=Rect.Left;
	Y-=Rect.Top;
	p=Types::TPoint(X, Y);
	try
	{
		if(minimized)
		{
			if(!entered && ((GlobalPressed && MinMaxBtn->Pressed) || (!GlobalPressed && MinMaxBtn->IsInClientRect(p))))
			{
				MinMaxBtn->InRect=true;
				MinMaxBtn->MouseMove(Sender, Shift, X, Y, Layer);
				entered=true;
			}
		}
		else for(int i=Buttons->Count-1; i>=0; i--)
		{
			po=(TSliderToolButton*)Buttons->Items[i];
			if(!entered && ((GlobalPressed && po->Pressed) || (!GlobalPressed && po->IsInClientRect(p))))
			{
				po->InRect=true;
				po->MouseMove(Sender, Shift, X, Y, Layer);
				entered=true;
			}
			else po->InRect=false;
		}
	}
	__finally
	{
		if(ObjectChanged && Owner!=NULL) Owner->SetObjectChanged(objecttype);
	}
}
//---------------------------------------------------------------------------

void __fastcall TSliderToolbar::MouseDown(TObject *Sender,
	TMouseButton Button, TShiftState Shift, int X, int Y, TCustomLayer *Layer)
{
	Types::TPoint p;
	TSliderToolButton *po;
	bool entered=false;

	X-=Rect.Left;
	Y-=Rect.Top;
	p=Types::TPoint(X, Y);
	GlobalPressed=true;
	try
	{
		if(minimized)
		{
			if(!entered && MinMaxBtn->IsInClientRect(p))
			{
				MinMaxBtn->X1=X;
				MinMaxBtn->Y1=Y;
				MinMaxBtn->InRect=true;
				MinMaxBtn->Pressed=true;
				MinMaxBtn->MouseDown(Sender, Button, Shift, X, Y, Layer);
				entered=true;
			}
		}
		else for(int i=Buttons->Count-1; i>=0; i--)
		{
			po=(TSliderToolButton*)Buttons->Items[i];
			if(!entered && po->IsInClientRect(p))
			{
				po->X1=X;
				po->Y1=Y;
				po->InRect=true;
				po->Pressed=true;
				po->MouseDown(Sender, Button, Shift, X, Y, Layer);
				entered=true;
			}
			else
			{
				po->InRect=false;
				po->Pressed=false;
			}
		}
	}
	__finally
	{
		if(ObjectChanged && Owner!=NULL) Owner->SetObjectChanged(objecttype);
	}
}
//---------------------------------------------------------------------------

void __fastcall TSliderToolbar::MouseUp(TObject *Sender,
	TMouseButton Button, TShiftState Shift, int X, int Y, TCustomLayer *Layer)
{
	Types::TPoint p;
	TSliderToolButton *po;
	bool entered, entered2;

    X-=Rect.Left;
	Y-=Rect.Top;
	entered=entered2=false;
	p=Types::TPoint(X, Y);
	try
	{
		if(minimized)
		{
			if(!entered && ((GlobalPressed && MinMaxBtn->Pressed) || (!GlobalPressed && MinMaxBtn->IsInClientRect(p))))
			{
				MinMaxBtn->MouseUp(Sender, Button, Shift, X, Y, Layer);
				entered=true;
			}
		}
		else for(int i=Buttons->Count-1; i>=0; i--)
		{
			po=(TSliderToolButton*)Buttons->Items[i];
			if(!entered && ((GlobalPressed && po->Pressed) || (!GlobalPressed && po->IsInClientRect(p))))
			{
				po->MouseUp(Sender, Button, Shift, X, Y, Layer);
				entered=true;
			}
			po->Pressed=false;
			if(!entered2 && po->IsInClientRect(p))
			{
				po->InRect=true;
				entered2=true;
			}
			else po->InRect=false;
		}
	}
	__finally
	{
		GlobalPressed=false;
		if(ObjectChanged && Owner!=NULL) Owner->SetObjectChanged(objecttype);
	}
}
//---------------------------------------------------------------------------

void __fastcall TSliderToolbar::MouseClick(System::TObject* Sender, int X, int Y)
{
	Types::TPoint p;
	TSliderToolButton* po;

	p=Types::TPoint(X, Y);
	if(Rect.Contains(p))
	{
		p.x-=Rect.Left;
		p.y-=Rect.Top;
		if(minimized)
		{
			if(MinMaxBtn->IsInClientRect(p))
			{
				MinMaxBtn->MouseClick(Sender, p.x, p.y);
				return;
			}
		}
		else for(int i=Buttons->Count-1; i>=0; i--)
		{
			po=(TSliderToolButton*)Buttons->Items[i];
			if(po->IsInClientRect(p))
			{
				po->MouseClick(Sender, p.x, p.y);
				//return;
			}
			else if(po->OnMinimizing) (po->OnMinimizing)(true);
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TSliderToolbar::MouseDblClick(System::TObject* Sender, int X, int Y)
{
	Types::TPoint p;
	TSliderToolButton* po;

	p=Types::TPoint(X, Y);
	if(Rect.Contains(p))
	{
		p.x-=Rect.Left;
		p.y-=Rect.Top;
		if(minimized)
		{
			if(MinMaxBtn->IsInClientRect(p))
			{
				MinMaxBtn->MouseDblClick(Sender, p.x, p.y);
				return;
			}
		}
		else for(int i=Buttons->Count-1; i>=0; i--)
		{
			po=(TSliderToolButton*)Buttons->Items[i];
			if(po->IsInClientRect(p))
			{
				po->MouseDblClick(Sender, p.x, p.y);
				return;
			}
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TSliderToolbar::writeFitButtonsOnly(bool value)
{
	fitbuttonsonly=value;
	if(Buttons && Buttons->Count>1)
	{
		ApplyChanging();
		RebuildObjects();
	}
}
//---------------------------------------------------------------------------

void __fastcall TSliderToolbar::writeBackground(TBitmap32 *value)
{

}
//---------------------------------------------------------------------------

int __fastcall TSliderToolbar::AddObject(TMyObject* o)
{
	int i;

	o->SmallSize=smallsize;
	i=Buttons->Add((TGraphicObject*)o);
	if(Buttons->Count>1)
	{
		ApplyChanging();
		RebuildObjects();
	}
	return i;
}
//---------------------------------------------------------------------------

void __fastcall TSliderToolbar::ApplyGrouping(int Groupindex, int index)
{
	TSliderToolButton* po;

	if(Groupindex!=0)
		for(int i=0; i<Buttons->Count; i++)
		{
			po=(TSliderToolButton*)Buttons->Items[i];
			if(po->GroupIndex==Groupindex && po->Index!=index)
			{
				po->Down=false;
			}
		}
}
//---------------------------------------------------------------------------

void __fastcall TSliderToolbar::RemoveChildFromList(TMyObject* o)
{
	TSliderToolButton *po;

	try
	{
		po=(TSliderToolButton*)o;
		Buttons->Remove(po->Index);
	}
	catch(Exception &e) {}
}

//---------------------------------------------------------------------------
// TSliderToolbarChild
//---------------------------------------------------------------------------
__fastcall TSliderToolbarChild::TSliderToolbarChild(TMyObject *AOwner, TSliderToolbar *AParent,
	Types::TRect OutputRect, int AButtonSize, bool Maximized)
	: TSliderToolbar(AOwner, OutputRect, AButtonSize, staNone, Maximized)
{
	Parent=AParent;
	if(Parent) Parent->AddToChilds(this);
	clickfirstonpopup=false;
	if(MinMaxBtn) delete MinMaxBtn;
	MinMaxBtn=new TSliderToolButton(Parent, Types::TRect(0, 0, AButtonSize, AButtonSize));//48, 48));
	MinMaxBtn->OnMouseClick=MaximizeMinimize;
	MinMaxBtn->OnMinimizing=Minimizing;
}
//---------------------------------------------------------------------------

void __fastcall TSliderToolbarChild::Hide()
{
	try
	{
		Minimized=true;
		MinMaxBtn->Down=!Minimized;
		if(Owner) ((TGraphicObjectsContainer*)Owner)->ClearModalObject(this);
		if(OnMouseClick!=NULL) (OnMouseClick)(this, 0, 0);
		for(int i=0; i<Childs->Count; i++)
			Childs->Items[i]->Hide();
	}
	catch(Exception &e) {}
}
//---------------------------------------------------------------------------

void __fastcall TSliderToolbarChild::Minimizing(bool minimizing)
{
	TSliderToolButton* po;

	try
	{
		for(int i=1; i<Buttons->Count; i++)
		{
			po=(TSliderToolButton*)Buttons->Items[i];
			if(po && po->OnMinimizing) (po->OnMinimizing)(minimizing);
		}
		if(minimizing)
		{
			Minimized=true;
			MinMaxBtn->Down=!Minimized;
			if(OnMouseClick!=NULL) (OnMouseClick)(this, 0, 0);
		}
	}
	catch(Exception &e) {}
}
//---------------------------------------------------------------------------

void __fastcall TSliderToolbarChild::MaximizeMinimize(System::TObject* Sender, int X, int Y)
{
    TSliderToolButton* po;

	try
	{
		if(((TGraphicObject*)Sender)->Pressed)
		{
			Minimized=!Minimized;
			MinMaxBtn->Down=!Minimized;
			for(int i=1; i<Buttons->Count; i++)
			{
				po=(TSliderToolButton*)Buttons->Items[i];
				if(po && po->OnMinimizing) (po->OnMinimizing)(Minimized);
			}
			if(OnMouseClick!=NULL) (OnMouseClick)(Sender, X, Y);
			if(!Minimized && Buttons->Count>0) //Maximized
			{
				po=(TSliderToolButton*)Buttons->Items[0];
				if(Parent==((TGraphicObjectsContainer*)Owner)->CurrentModalObject)
					ShowModal();
				if(ClickFirstButtonOnPopUp && po && po->OnMouseClick)
				{
					po->Pressed=true;
					(po->OnMouseClick)(po, X, Y);
					//po->Pressed=false;
				}
			}
		}
	}
	catch(Exception &e) {}
}
//---------------------------------------------------------------------------

void __fastcall TSliderToolbarChild::Resize()
{
	int x, y, w, h, oh, ow;

	if(Parent && !Parent->Resized) Parent->OnResize(Owner, Output.Width(), Output.Height());
	Size=ButtonSize+2*ButtonOutline+2*BorderWidth;
	if(FitButtonsOnly && Buttons!=NULL && Buttons->Count>0)
	{
		oh=ow=(int)(((float)Buttons->Count+0.5)*(float)Size);
		if(ow>output.Width()) ow=output.Width();
		if(oh>output.Height()) oh=output.Height();
	}
	else
	{
		oh=output.Height();
		ow=output.Width();
	}
	if(Minimized)
	{
		x=y=w=h=0;
	}
	else
	{
		switch(Parent->Align)
		{
			/*case(staLeft):
				x=Parent->ClientRect.Right;
				w=Size;
				//h=output.Height();
				h=oh;
				//y=0;
				y=Parent->ClientRect.Top+MinMaxBtn->ClientRect.Top;//-ButtonOutline-BorderWidth;
				if(y<Parent->ClientRect.Top+2*ButtonOutline) y=Parent->ClientRect.Top+2*ButtonOutline;
				if(y+h>output.Height()) y=output.Height()-h-Size;
				break;
			case(staTop):
				y=Parent->ClientRect.Bottom;
				w=ow;
				h=Size;
				x=Parent->ClientRect.Left+MinMaxBtn->ClientRect.Left;//-ButtonOutline-BorderWidth;
				if(x>Parent->ClientRect.Left+2*ButtonOutline) x=ClientRect.Left+2*ButtonOutline;
				if(x+w>output.Width()) x=output.Width()-w-Size;
				break;
			case(staRight):
				x=Parent->ClientRect.Left-Size;
				w=Size;
				h=oh;
				y=Parent->ClientRect.Top+MinMaxBtn->ClientRect.Top;//-ButtonOutline-BorderWidth;
				if(y<Parent->ClientRect.Top+2*ButtonOutline) y=Parent->ClientRect.Top+2*ButtonOutline;
				if(y+h>output.Height()) y=output.Height()-h-Size;
				break;
			case(staBottom):
				y=Parent->ClientRect.Top-Size;
				w=ow;
				h=Size;
				x=Parent->ClientRect.Left+MinMaxBtn->ClientRect.Left;//-ButtonOutline-BorderWidth;
				if(x>Parent->ClientRect.Left+2*ButtonOutline) x=ClientRect.Left+2*ButtonOutline;
				if(x+w>output.Width()) x=output.Width()-w-Size;
				break;*/
			case(staLeft):
				x=Parent->ClientRect.Right;
				w=ow;
				h=Size;
				y=Parent->ClientRect.Top+MinMaxBtn->ClientRect.Top;
				if(y<Parent->ClientRect.Top+2*ButtonOutline) y=Parent->ClientRect.Top+2*ButtonOutline;
				if(y+h>output.Height()) y=output.Height()-h-Size;
				break;
			case staCenter:
				y=Parent->ClientRect.Top+MinMaxBtn->ClientRect.Top+(MinMaxBtn->ClientRect.Width()>>2);
				w=Size;
				h=oh;
				x=Parent->ClientRect.Left+MinMaxBtn->ClientRect.Left+(MinMaxBtn->ClientRect.Height()>>3);
				if(x+w>output.Right) x=output.Right-w;
				if(y+h>output.Height()) y=output.Height()-h;
				break;
			case(staTop):
				y=Parent->ClientRect.Bottom;
				w=Size;
				h=oh;
				x=Parent->ClientRect.Left+MinMaxBtn->ClientRect.Left;//-ButtonOutline-BorderWidth;
				if(x<Parent->ClientRect.Left+2*ButtonOutline) x=ClientRect.Left+2*ButtonOutline;
				if(x+w>output.Width()) x=output.Width()-w-Size;
				break;
			case(staRight):
				x=Parent->ClientRect.Left;
				w=ow;
				h=Size;
				y=Parent->ClientRect.Top+MinMaxBtn->ClientRect.Top;//-ButtonOutline-BorderWidth;
				if(y<Parent->ClientRect.Top+2*ButtonOutline) y=Parent->ClientRect.Top+2*ButtonOutline;
				if(y+h>output.Height()) y=output.Height()-h-Size;
				break;
			case(staBottom):
				y=Parent->ClientRect.Top;
				w=Size;
				h=oh;
				x=Parent->ClientRect.Left+MinMaxBtn->ClientRect.Left;//-ButtonOutline-BorderWidth;
				if(x<Parent->ClientRect.Left+2*ButtonOutline) x=ClientRect.Left+2*ButtonOutline;
				if(x+w>output.Width()) x=output.Width()-w-Size;
				break;
		}
	}
	Rect=Types::TRect(x, y, x+w, y+h);
	if(Owner!=NULL) Owner->SetObjectChanged(objecttype);
	resized=true;
}
//---------------------------------------------------------------------------

void __fastcall TSliderToolbarChild::RebuildObjects()
{
	int first, last, cnt, i, e;
	TSliderToolButton *po;

	if(!Minimized && Rect.Width()>0 && Rect.Height()>0)
	{
	/*	first=ButtonOutline;
		if(Parent->Align==staLeft || Parent->Align==staRight) last=Rect.Height();
		else last=Rect.Width();
		e=last>>1;
		last-=ButtonOutline;
		cnt=0;
		for(i=0; i<Buttons->Count; i++)
		{
			po=(TSliderToolButton*)Buttons->Items[i];
			if(po->SpecialPlace)
			{
				if(po->Align==stbaFirst)
				{
					if(Parent->Align==staLeft || Parent->Align==staRight) po->Top=first;
					else po->Left=first;
					first+=(ButtonSize+ButtonOutline);
				}
				else if(po->Align==stbaLast)
				{
					last-=ButtonSize;
					if(Parent->Align==staLeft || Parent->Align==staRight) po->Top=last;
					else po->Left=last;
					last-=ButtonOutline;
				}
			}
			else cnt++;
		}
		i=(int)((float)(last-first)/(float)(ButtonSize+ButtonOutline));
		if(cnt>i) cnt=i;
		switch(ButtonsAlign)
		{
			case stbsaCenter:
				//first+=((last-first)>>1)-(cnt*(buttonsize+ButtonOutline)>>1); break;
				first=e-((float)(ButtonSize+ButtonOutline)*(float)cnt/2.); break; //
			case stbsaLast:	//first=last-cnt*(buttonsize+ButtonOutline); break;
				first=last-cnt*(ButtonSize+ButtonOutline);
				break;
			case stbsaFirst:
				first=e-(cnt-(e-first)/(ButtonSize+ButtonOutline))*(ButtonSize+ButtonOutline);
				break;
		}
		for(i=0; i<Buttons->Count; i++)
		{
			po=(TSliderToolButton*)Buttons->Items[i];
			if(Parent->Align==staLeft || Parent->Align==staRight)
			{
				po->Left=BorderWidth+ButtonOutline;
				if(!po->SpecialPlace)
				{
					if(i<cnt) po->Top=first;
					else po->Top=-ButtonSize;
				}
			}
			else
			{
				po->Top=BorderWidth+ButtonOutline;
				if(!po->SpecialPlace)
				{
					if(i<cnt) po->Left=first;
					else po->Left=-ButtonSize;
				}
			}
			if(!po->SpecialPlace) first+=(ButtonSize+ButtonOutline);
		} */

		first=ButtonOutline;
		if(Parent->Align==staLeft || Parent->Align==staRight) last=Rect.Width();
		else last=Rect.Height();
        last-=ButtonOutline;
		cnt=Buttons->Count;
		i=(int)((float)(last-first)/(float)(ButtonSize+ButtonOutline));
		if(cnt>i) cnt=i;
		for(i=0; i<cnt; i++)
		{
			po=(TSliderToolButton*)Buttons->Items[i];
			if(Parent->Align==staLeft)
			{
				po->Left=ButtonOutline+i*(ButtonSize+ButtonOutline);
				po->Top=0;
			}
			else if(Parent->Align==staRight)
			{
				po->Left=last-ButtonOutline-i*(ButtonSize+ButtonOutline);
				po->Top=0;
			}
			else if(Parent->Align==staTop || Parent->Align==staCenter)
			{
				po->Top=ButtonOutline+i*(ButtonSize+ButtonOutline);
				po->Left=0;
			}
			else if(Parent->Align==staBottom)
			{
				po->Top=last-ButtonOutline-i*(ButtonSize+ButtonOutline);
				po->Left=0;
			}
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TSliderToolbarChild::Redraw(TObject *Sender, TBitmap32 *Bmp)
{
	Types::TRect rct, crt;
	TSliderToolButton *po;
	TBitmap32 *tmp;
	TPolygon32* poly;

	if(Visible && Bmp!=NULL && Rect.Width()!=0 && Rect.Height()!=0 && !Minimized)
	{
		rct=Rect;
		if(rct.IsInRect(Owner->ViewPort))
		{
			tmp=new TBitmap32();
			poly=new TPolygon32();
			try
			{
				tmp->SetSize(Rect.Width(), Rect.Height());
				tmp->DrawMode=dmBlend;
				tmp->BeginUpdate();
				crt=Types::TRect(0, 0, rct.Width(), rct.Height());

				if(!Transparent)
				{
					poly->Antialiased=true;
					poly->Closed=true;
					switch(Parent->Align)
					{
						case staRight:
							poly->Add(FixedPoint((int)crt.right, crt.top));
							poly->Add(FixedPoint((int)crt.left+2*ButtonOutline, crt.top));
							poly->Add(FixedPoint((int)crt.left, crt.top+2*ButtonOutline));
							poly->Add(FixedPoint((int)crt.left, crt.bottom-2*ButtonOutline-1));
							poly->Add(FixedPoint((int)crt.left+2*ButtonOutline, crt.bottom-1));
							poly->Add(FixedPoint((int)crt.right, crt.bottom-1));
							break;
						case staLeft:
							poly->Add(FixedPoint((int)crt.left, crt.top));
							poly->Add(FixedPoint((int)crt.right-2*ButtonOutline, crt.top));
							poly->Add(FixedPoint((int)crt.right, crt.top+2*ButtonOutline));
							poly->Add(FixedPoint((int)crt.right-1, crt.bottom-2*ButtonOutline-1));
							poly->Add(FixedPoint((int)crt.right-2*ButtonOutline-1, crt.bottom-1));
							poly->Add(FixedPoint((int)crt.left, crt.bottom-1));
							break;
						case staTop:
							poly->Add(FixedPoint((int)crt.left, crt.top));
							poly->Add(FixedPoint((int)crt.left, crt.bottom-2*ButtonOutline));
							poly->Add(FixedPoint((int)crt.left+2*ButtonOutline, crt.bottom));
							poly->Add(FixedPoint((int)crt.right-2*ButtonOutline-1, crt.bottom));
							poly->Add(FixedPoint((int)crt.right-1, crt.bottom-2*ButtonOutline));
							poly->Add(FixedPoint((int)crt.right-1, crt.top));
							break;
						case staBottom:
							poly->Add(FixedPoint((int)crt.left, crt.bottom));
							poly->Add(FixedPoint((int)crt.left, crt.top+2*ButtonOutline));
							poly->Add(FixedPoint((int)crt.left+2*ButtonOutline, crt.top));
							poly->Add(FixedPoint((int)crt.right-2*ButtonOutline-1, crt.top));
							poly->Add(FixedPoint((int)crt.right-1, crt.top+2*ButtonOutline));
							poly->Add(FixedPoint((int)crt.right-1, crt.bottom));
							break;
					}
					poly->Draw(tmp, Border, Color);
				}
				//tmp->FrameRectTS(crt, border);
				for(int i=0; i<Buttons->Count; i++)
				{
					po=(TSliderToolButton*)Buttons->Items[i];
					po->Redraw(Sender, tmp);
				}
				tmp->EndUpdate();
				Bmp->Draw(rct, crt, tmp);
			}
			__finally
			{
				poly->Clear();
				delete poly;
				delete tmp;
			}
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TSliderToolbarChild::SmallResize()
{
	bool b=AutomaticUpdate;

	AutomaticUpdate=false;
	if(smallsize)
	{
		ButtonSize=initialbuttonsize>>1;
		//ButtonOutline=2;
	}
	else
	{
		ButtonSize=initialbuttonsize;
		//ButtonOutline=4;
	}
	for(int i=0; i<Buttons->Count; i++)
		((TSliderToolButton*)Buttons->Items[i])->SmallSize=smallsize;
	ApplyChanging();
	RebuildObjects();
	AutomaticUpdate=b;
}
