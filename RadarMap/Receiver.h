//---------------------------------------------------------------------------

#ifndef ReceiverH
#define ReceiverH

#include "Defines.h"
#include "TCPClient.h"
#include "Control_unit.h"
//---------------------------------------------------------------------------

class TParsedDataQueue : public TEventedQueue
{
public:
	__fastcall TParsedDataQueue() : TEventedQueue() {}
	__fastcall ~TParsedDataQueue() {}

	void __fastcall Push(TParsedData* AItem) {TEventedQueue::Push((void*)AItem);}
	TParsedData* __fastcall Pop(void) {return (TParsedData*)TEventedQueue::Pop();}
	TParsedData* __fastcall Peek(void) {return (TParsedData*)TEventedQueue::Peek();}
	void __fastcall Clear() {TParsedData* d; while(TEventedQueue::Count>0) {d=(TParsedData*)TEventedQueue::Pop(); if(d) delete d;}}
};

class TTraceProcess : public TMyThread
{
private:
	int rdpc, rpdps, trcs, nrec;
	float coef, coef_sum;
	bool allreadystopped;
	float wpps;
	float wheelspeed;
	TTimeStamp t1, t2;
	long wt1, wt2;
	TParsedDataQueue* ParsedDataQueue;
	TObject *Manager;
	HANDLE RecoveredFromPauseEvent;
protected:
	void __fastcall Execute();
	void __fastcall CountPDataPerSec();
	void __fastcall CountWheelSpeed(TGPRUnit* Settings);
	void __fastcall writeAllreadyStopped(bool value);
	void __fastcall MoveoutCorrection(TProfile* Profile, TTrace* Trace, float Value);
public:
	__fastcall TTraceProcess(bool CreateSuspended, TParsedDataQueue* q, TObject* RadarMapManager);
	__fastcall ~TTraceProcess() {CloseHandle(RecoveredFromPauseEvent);}

	__property int RecPDataCount = {read=rdpc};
	__property int RecPDataPerSecond = {read=rpdps};
	__property bool AllreadyStopped = {write=writeAllreadyStopped};
	__property float WheelSpeed = {read=wheelspeed}; //meters per second
};

class TReceive : public TMyThread
{
private:
	char Buf[4096];
	double TracesOverspeed;
protected:
	TTCPClient* tcp_client;
	TParsedDataQueue* ParsedDataQueue;
	TGPRUnit* settings;
	HANDLE PausedEvent;

	void __fastcall ExecuteLoopBody();
public:
	__fastcall TReceive(bool CreateSuspended, TTCPClient* tc);
	__fastcall ~TReceive();

	void __fastcall AskForPause();
	void __fastcall AskForContinue();

	__property TGPRUnit* Settings = {read=settings, write=settings};
	__property TParsedDataQueue* Queue={read=ParsedDataQueue};
};

enum TConnectionStatus {csNone, csStarted, csConnected, csDisconnected, csReceiving, csStopped, csPaused};

class TConnection : public TObject
{
private:
	TTCPClient* tcp_client;
	TReceive* rec_thrd;
	TTraceProcess* proc_thrd;
	Classes::TComponent* Owner;
	AnsiString ipaddress;
	int port;
	TGPRUnit* settings;
	bool SettingsLinked;
	TObject* Manager;
	TConnectionStatus status;
protected:
	bool __fastcall readConnected();
	void __fastcall StartThreads();
	void __fastcall TerminateThreads();
	int __fastcall readReceivedTracesCount();
	float __fastcall readWheelSpeed() {if(proc_thrd) return proc_thrd->WheelSpeed; else return 0;}
	int __fastcall readRdps() {if(proc_thrd) return proc_thrd->RecPDataPerSecond; else return 0;}
	int __fastcall readTraffic() {if(settings) return readRdps()*settings->Samples*(int)settings->BpS*8; else return 0;} //Bits Per Second
public:
	__fastcall TConnection(Classes::TComponent* AOwner, TObject* RadarMapManager);
	__fastcall ~TConnection();
	bool __fastcall TryToConnect(); // Inside call of WhoIs and applying it to the RadarSettings
	bool __fastcall TryToDisconnect();
	bool __fastcall WriteCommand(TCommand cmd, AnsiString *Repl);
	bool __fastcall StartReceiving();
	bool __fastcall StopReceiving();
	void __fastcall PauseReceiving();
	void __fastcall ContinueReceiving();
	TGPRUnit* __fastcall LinkSettings(void);

	void __fastcall AssignSettings(TGPRUnit* ASettings) {settings=ASettings;}

	__property AnsiString IPaddress = {read=ipaddress, write=ipaddress};
	__property int Port = {read=port, write=port};
	__property TGPRUnit* Settings = {read=settings};
	__property bool Connected ={read=readConnected};
	__property TConnectionStatus Status = {read=status};
	__property float WheelSpeed = {read=readWheelSpeed};
	__property int RecPDataCount = {read=readReceivedTracesCount};
	__property int RecPDataPerSecond = {read=readRdps};
	__property int Traffic = {read=readTraffic};
};

#endif
