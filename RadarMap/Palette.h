//---------------------------------------------------------------------------

#ifndef PaletteH
#define PaletteH

#include <vcl.h>
#include <math.h>
#include <GR32.hpp>
//---------------------------------------------------------------------------

const int PaletteMaxPosition = 255;
const int PaletteMaxColors = 10;
const int PaletteColorBarOffset = 12;
const int PaletteMaxUnits = 8;

class TPaletteColor : public TObject
{
private:
	int position;
	unsigned char red, green, blue;
	TColor color;
	bool checked;
protected:
	void __fastcall writePosition(int value) {if(value<0) value=0; else if(value>PaletteMaxPosition) value=PaletteMaxPosition; position=value;}
	void __fastcall writeColor(TColor value) {color=value; red=value&0x0000FF; green=(value&0x00FF00)>>8; blue=(value&0xFF0000)>>16;}
	void __fastcall writeRed(unsigned char value) {if(red!=value){ red=value; color=(TColor)RGB(red, green, blue);}}
	void __fastcall writeGreen(unsigned char value) {if(green!=value){ green=value; color=(TColor)RGB(red, green, blue);}}
	void __fastcall writeBlue(unsigned char value) {if(blue!=value){ blue=value; color=(TColor)RGB(red, green, blue);}}
public:
	__fastcall TPaletteColor(TColor InPaletteColor, int InPosition, bool state=false) {Color=InPaletteColor; Position=InPosition; checked=state;}
	__fastcall TPaletteColor() {Color=clWhite; Position=0; checked=false;}

	__property int Position={read=position, write=writePosition};
	__property TColor Color = {read=color, write=writeColor};
	__property unsigned char Red = {read=red, write=writeRed};
	__property unsigned char Green = {read=green, write=writeGreen};
	__property unsigned char Blue = {read=blue, write=writeBlue};
	__property bool Checked = {read=checked, write=checked};
};

class TPaletteColorsList: public TObject
{
private:
	TList* List;
protected:
	TPaletteColor* __fastcall readListItem(int Index) {if(Index>=0 && Index<List->Count && List->Items[Index]) return (TPaletteColor*)List->Items[Index]; else return NULL;}
	void __fastcall writeListItem(int Index, TPaletteColor* value) {if(Index>=0 && Index<List->Count) {if(List->Items[Index]) delete List->Items[Index]; List->Items[Index]=value;}}
	int __fastcall readCount() {return List->Count;}
public:
	void __fastcall Delete(int Index) {if(Index>=0 && Index<List->Count && List->Items[Index]) {delete (TObject*)List->Items[Index]; List->Items[Index]=NULL;}}
	void __fastcall Clear() {for(int i=0; i<List->Count; i++) Delete(i);}
	void __fastcall SetCount(int N) {int n=List->Count; if(N>n) for(int i=n; i<N; i++) List->Add(new TPaletteColor()); else for(int i=N; i<n; i++) List->Delete(N);}

	__fastcall TPaletteColorsList(int AMax);
	__fastcall ~TPaletteColorsList() {Clear();}

	void __fastcall Add(TPaletteColor* AColor) {List->Add(AColor);}
	void __fastcall Insert(int Index, TPaletteColor* AColor) {if(Index<0) Index=0; else if(Index>List->Count) Index=List->Count; List->Insert(Index, AColor);}

	__property TPaletteColor* Items[int Index]={read=readListItem, write=writeListItem};
	__property int Count = {read=readCount};
};

class TPaletteColors: public TObject
{
private:
	int nextindex;
	TPaletteColorsList* colors;
protected:
	int __fastcall readCount() {if(colors) return colors->Count; else return 0;}
public:
	__fastcall TPaletteColors();
	__fastcall ~TPaletteColors();

	void __fastcall Assign(TPaletteColor* AColor, int Index);
	void __fastcall Check(int Index) {if(colors->Items[Index]) colors->Items[Index]->Checked=true;}
	void __fastcall Uncheck(int Index) {if(colors->Items[Index]) colors->Items[Index]->Checked=false;}
	TPaletteColor* __fastcall Get(int Index);
	TPaletteColor* __fastcall GetForce(int Index) {if (Index<colors->Count && Index>=0) return colors->Items[Index]; else return NULL;}
	TPaletteColor* __fastcall GetNext(int Index);
	TPaletteColor* __fastcall GetPrevious(int Index);
	void __fastcall Clear() {for(int i=0; i<colors->Count; i++) if(colors->Items[i]) colors->Items[i]->Checked=false;}
	void __fastcall SavePaletteColorsToRegistry(AnsiString keyName, AnsiString PaletteName);
	void __fastcall LoadPaletteColorsFromRegistry(AnsiString keyName, AnsiString PaletteName);
	bool __fastcall CheckState(int Index) {if(colors->Items[Index]) return colors->Items[Index]->Checked; else return false;}

	__property int NextIndex={read=nextindex};
	__property int Count = {read=readCount};
};

class TPalette: public TObject
{
private:
	TPaletteColors* colors;
	TPaletteColorsList* palettedata;
protected:
public:
	void __fastcall ApplyPaletteColors(TPaletteColors* PaletteColors);

	__fastcall TPalette(TPaletteColors* PaletteColors) {colors=NULL; palettedata=NULL; ApplyPaletteColors(PaletteColors);}
	__fastcall TPalette() {colors=new TPaletteColors(); palettedata=NULL; ApplyPaletteColors(NULL);}
	__fastcall ~TPalette() {if(colors) delete colors; if(palettedata) delete palettedata;}

	void __fastcall RebuildPalette(bool Apply=true);
	void __fastcall Assign(TPaletteColor* PaletteColor, int Index, bool Rebuild) {if(Colors) {Colors->Assign(PaletteColor, Index); RebuildPalette(Rebuild);}}
	void __fastcall Uncheck(byte i) {if(colors){colors->Uncheck(i); RebuildPalette();}}
	bool __fastcall CheckState(byte i) {if(colors){return colors->CheckState(i);} else return false;}
	void __fastcall ChangePosition(int Index, byte Pos);
	void __fastcall ChangeColor(int Index, TColor c);
	void __fastcall SavePaletteToRegistry(AnsiString keyName, AnsiString PaletteName) {if(Colors) Colors->SavePaletteColorsToRegistry(keyName, PaletteName);}
	void __fastcall LoadPaletteFromRegistry(AnsiString keyName, AnsiString PaletteName);
	bool __fastcall ConvertToWinColors(TColor *WinColors, int N);
	void __fastcall DrawPalette(TBitmap32* Bmp, Types::TRect Rct);
	void __fastcall DrawPalette(TBitmap32* Bmp, int Width, int Height, int Border=0, bool ShowPositions=true);

	__property TPaletteColorsList* Data={read=palettedata};
	__property TPaletteColors* Colors={read=colors};
};

#endif
