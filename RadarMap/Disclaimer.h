//---------------------------------------------------------------------------

#ifndef DisclaimerH
#define DisclaimerH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include "ImageButton.h"
#include <pngimage.hpp>
#include "G32_ProgressBar.hpp"
#include "GR32_Image.hpp"
#include <Graphics.hpp>
#include "Manager.h"
#include <Dialogs.hpp>
#include <ComCtrls.hpp>
#include <ImgList.hpp>
//---------------------------------------------------------------------------
class TDisclaimerForm : public TForm
{
__published:	// IDE-managed Components
	TPanel *Panel1;
	TPanel *Panel2;
	TPanel *Panel3;
	TPanel *Panel4;
	TPanel *TitlePanel;
	TPanel *Panel5;
	TPanel *Panel6;
	TPanel *Panel7;
	TPanel *PagesPanel;
	TPanel *OkCancelPanel;
	TLabel *Label1;
	TPanel *Panel8;
	TPanel *Panel9;
	TImageButton *CloseIButton;
	TImageButton *CancelImageButton;
	TImageButton *OkImageButton;
	TEdit *TempEdit;
	TLabel *Label3;
	TPanel *Panel10;
	TMemo *Memo1;
	void __fastcall CloseIButtonClick(TObject *Sender);
	void __fastcall FormShow(TObject *Sender);
	void __fastcall OkImageButtonClick(TObject *Sender);
	void __fastcall FormHide(TObject *Sender);
	void __fastcall TitlePanelMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
	void __fastcall TitlePanelMouseMove(TObject *Sender, TShiftState Shift, int X, int Y);
	void __fastcall TitlePanelMouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
	void __fastcall BriefMemoKeyPress(TObject *Sender, wchar_t &Key);
	void __fastcall BriefMemoChange(TObject *Sender);
private:	// User declarations
	bool TitleDown;
	Types::TPoint TitleXY;
public:		// User declarations
	__fastcall TDisclaimerForm(TComponent* Owner, TRadarMapManager *AManager);
};
//---------------------------------------------------------------------------
extern PACKAGE TDisclaimerForm *DisclaimerForm;
//---------------------------------------------------------------------------
#endif
