//---------------------------------------------------------------------------

#ifndef MyFilesH
#define MyFilesH
//---------------------------------------------------------------------------

#include <vcl.h>
#include <FileCtrl.hpp>
#include <windows.h>
#include <tlhelp32.h>
#include "MyObjects.h"
#include "MyQueues.h"
#include <winbase.h>
#include <windef.h>
#include <assert.h>
#include <ntsecapi.h>
#include <winternl.h>

#define BUFFER_SIZE					512
#define STATUS_SUCCESS				((NTSTATUS)0x00000000L)
#define STATUS_INFO_LENGTH_MISMATCH	((NTSTATUS)0xc0000004L)
#define HANDLE_TYPE_FILE			28

#define _GIGABYTE	1073741824
#define _MEGABYTE	1048576
#define _KILOBYTE	1024

namespace MyFiles
{
	typedef NTSTATUS (WINAPI * PFN_NTQUERYSYSTEMINFORMATION)(
		IN SYSTEM_INFORMATION_CLASS SystemInformationClass,
		OUT PVOID SystemInformation,
		IN ULONG SystemInformationLength,
		OUT PULONG ReturnLength OPTIONAL
	);

	typedef struct _SYSTEM_HANDLE
	{
		ULONG ProcessId;
		UCHAR ObjectTypeNumber;
		UCHAR Flags;
		USHORT Handle;
		PVOID Object;
		ACCESS_MASK GrantedAccess;
	} SYSTEM_HANDLE, *PSYSTEM_HANDLE;

	typedef struct _SYSTEM_HANDLE_INFORMATION
    {
		ULONG NumberOfHandles;
        SYSTEM_HANDLE Handles[1];
	} SYSTEM_HANDLE_INFORMATION, *PSYSTEM_HANDLE_INFORMATION;

	typedef DWORD (WINAPI* pfnGetProcID)(HANDLE h);

	typedef NTSTATUS (WINAPI* pfnQueryInformationProcess)(
        HANDLE ProcessHandle,
        PROCESSINFOCLASS ProcessInformationClass,
		PVOID ProcessInformation,
        ULONG ProcessInformationLength,
    PULONG ReturnLength);

	typedef NTSTATUS (WINAPI * PFN_NTQUERYOBJECT)(
        IN HANDLE Handle,
        IN OBJECT_INFORMATION_CLASS ObjectInformationClass,
		OUT PVOID ObjectInformation,
        IN ULONG ObjectInformationLength,
		OUT PULONG ReturnLength OPTIONAL
    );

	typedef struct _FILE_NAME_INFORMATION {
        ULONG FileNameLength;
		WCHAR FileName[1];
	} FILE_NAME_INFORMATION, *PFILE_NAME_INFORMATION;

	//Folder functions
	void __fastcall EmptyFolder(AnsiString PathName);
	void __fastcall CopyFolderFiles(AnsiString Src, AnsiString Dst);
	void __fastcall MoveFolderFiles(AnsiString Src, AnsiString Dst) {CopyFolderFiles(Src, Dst); EmptyFolder(Src);}
	//Process functions
	bool __fastcall FindProcess(AnsiString szExe, PROCESSENTRY32 &pe);
	bool __fastcall ProcessExists(AnsiString szExe) {PROCESSENTRY32 pe; return FindProcess(szExe, pe);}
	int __fastcall ProcessCopiesNumber(AnsiString szExe);
	bool __fastcall KillProcess(PROCESSENTRY32 *pe) {if(pe && pe->th32ProcessID>0) return TerminateProcess(OpenProcess(PROCESS_TERMINATE, BOOL(0), pe->th32ProcessID), 0); else return false;}
	bool __fastcall FindAndKillProcess(AnsiString szExe, bool Similar=true);
	DWORD MyGetProcessId(HANDLE h);
	unsigned long GetProcessSize(HANDLE hProcess);
	unsigned long GetProcessSize(AnsiString szExe);
	unsigned long GetCurrentProcessSize() {return GetProcessSize(GetCurrentProcess());}
	DWORD FindAssociatedProcesses(ULONG pid, TList *List);
};

enum TFileMovementDirection {fmdBegin=FILE_BEGIN, fmdCurrent=FILE_CURRENT, fmdEnd=FILE_END};

class TUnsharedFile: public TMyObject
{
private:
	AnsiString filename, lasterror;
	bool openexisting;
	TMyObject *owner;
	unsigned long __fastcall UnlockedRead(void* buf, unsigned long size);
protected:
	HANDLE hndl;
	bool opened;
	TBusyLocker *BusyLocker;
	unsigned long filepointer;
	unsigned long __fastcall readFileSize();
	bool __fastcall readEndlessLock() {return (BusyLocker && BusyLocker->EndlessLock);}
	void __fastcall writeEndlessLock(bool value) {if(BusyLocker) BusyLocker->EndlessLock=value;}
	void __fastcall GetError();
public:
	__fastcall TUnsharedFile(TMyObject* AOwner, AnsiString AFileName, bool AOpenExisting);
	__fastcall ~TUnsharedFile();

	virtual bool __fastcall Open();
	bool __fastcall Close();
	virtual unsigned long __fastcall Read(void* buf, unsigned long size, unsigned long pointer, TFileMovementDirection dir);
	virtual unsigned long __fastcall Read(void* buf, unsigned long size);// {return Read(buf, size, 0, fmdCurrent);}
	virtual unsigned long __fastcall Write(void* buf, unsigned long size, unsigned long pointer, TFileMovementDirection dir, unsigned long &fp);
	virtual unsigned long __fastcall Write(void* buf, unsigned long size, unsigned long &fp) {return Write(buf, size, 0, fmdCurrent, fp);}
	virtual bool __fastcall FileMemSet(unsigned long pointer, TFileMovementDirection dir, unsigned long size, char ch);
	virtual bool __fastcall FileMemSet(unsigned long size, char ch) {return FileMemSet(0, fmdCurrent, size, ch);}
	//unsigned long __fastcall SetFilePointer(unsigned long pointer, TFileMovementDirection dir);
	virtual bool __fastcall SetEOF(unsigned long pointer, TFileMovementDirection dir);
	virtual bool __fastcall SetEOF() {return SetEOF(0, fmdCurrent);}
	virtual bool __fastcall Delete();
	HANDLE GetHandleUnsafely() {return hndl;}

	__property AnsiString FileName = {read=filename};
	__property unsigned long FileSize = {read=readFileSize};
	__property unsigned long FilePointer = {read = filepointer}; //current file pointer
	__property bool Opened = {read=opened};
	__property bool OpenExisting = {read=openexisting};
	__property bool EndlessLock = {read=readEndlessLock, write=writeEndlessLock};
	__property TMyObject* Owner = {read=owner, write=owner};
	__property AnsiString LastError = {read=lasterror};
};

class TBufferedROFile: public TUnsharedFile
{
private:
	char *readedbuf, *ptr;
	unsigned long readedlength, sizetoread, filesize;
protected:
	unsigned long __fastcall Read(void* buf, unsigned long size, unsigned long pointer, TFileMovementDirection dir) {return TUnsharedFile::Read(buf, size, pointer, dir);}
	unsigned long __fastcall Write(void* buf, unsigned long size, unsigned long pointer, TFileMovementDirection dir, unsigned long &fp) {return TUnsharedFile::Write(buf, size, pointer, dir, fp);}
	unsigned long __fastcall Write(void* buf, unsigned long size, unsigned long &fp) {return TUnsharedFile::Write(buf, size, fp);}
	bool __fastcall FileMemSet(unsigned long pointer, TFileMovementDirection dir, unsigned long size, char ch) {return TUnsharedFile::FileMemSet(pointer, dir, size, ch);}
	bool __fastcall FileMemSet(unsigned long size, char ch) {return TUnsharedFile::FileMemSet(size, ch);}
	bool __fastcall SetEOF(unsigned long pointer, TFileMovementDirection dir) {return TUnsharedFile::SetEOF(pointer, dir);}
	bool __fastcall SetEOF() {return TUnsharedFile::SetEOF();}
	bool __fastcall Delete() {return TUnsharedFile::Delete();}
public:
	__fastcall TBufferedROFile(TMyObject* AOwner, AnsiString AFileName, bool AOpenExisting);
	__fastcall ~TBufferedROFile();

    bool __fastcall Open();
	unsigned long __fastcall Read(void* buf, unsigned long size);
	unsigned long __fastcall ReadLn(AnsiString *Out);
};

class TUnsharedFilesList: public TMyObjectsList
{
private:
protected:
	TUnsharedFile* __fastcall readItem(int Index) {return (TUnsharedFile*)readMyObjectItem(Index);}
	void __fastcall writeItem(int Index, TUnsharedFile* value) {writeMyObjectItem(Index, (TMyObject*)value);}
public:
	__fastcall TUnsharedFilesList(TMyObject *AOwner) : TMyObjectsList(AOwner) {}
	__fastcall ~TUnsharedFilesList() {}

	int __fastcall Add(TUnsharedFile* o) { int i=TMyObjectsList::Add((TMyObject*)o); if(i>=0) o->Owner=this; return i;}
	void __fastcall RemoveChildFromList(TMyObject* o) {if(o && o->Index>=0) Delete(o->Index);}
	TUnsharedFile* __fastcall FindByFileName(AnsiString filename);
	int __fastcall IndexOf(TUnsharedFile* o) {return TMyObjectsList::IndexOf((TMyObject*)o);}

	__property TUnsharedFile* Items[int Index] = {read=readItem, write=writeItem};
};

class TShellItem : public TObject
{
private:
	AnsiString cmdline, cmdlinepath;
	unsigned long handleid;
	HWND hWnd;
	PROCESS_INFORMATION pi;
	STARTUPINFO si;
	bool stopitemondestruct;
	TForm *OwnerForm;
	unsigned int procflags;
	//HWND FoundHWND;
	//BOOL __stdcall EnumWindowsProc(HWND hwnd, LPARAM lParam);
protected:
	bool __fastcall Stop();
	int __fastcall readX() {return si.dwX;}
	int __fastcall readY() {return si.dwY;}
	int __fastcall readWidth() {return si.dwXSize;}
	int __fastcall readHeight() {return si.dwYSize;}
	AnsiString __fastcall readTitle() {return (AnsiString)si.lpTitle;}
	void __fastcall writeCmdLine(AnsiString value) {if(cmdline!=value) {cmdline=value; cmdlinepath=ExtractFilePath(cmdline); if(handleid>0) Run();}}
	void __fastcall writeX(unsigned int value) {if(si.dwX!=value) {si.dwX=value; ReSize();} si.dwFlags|=STARTF_USEPOSITION;}
	void __fastcall writeY(unsigned int value) {if(si.dwY!=value) {si.dwY=value; ReSize();} si.dwFlags|=STARTF_USEPOSITION;}
	void __fastcall writeWidth(unsigned int value) {if(si.dwXSize!=value) {si.dwXSize=value; ReSize();} si.dwFlags|=STARTF_USESIZE;}
	void __fastcall writeHeight(unsigned int value) {if(si.dwYSize!=value) {si.dwYSize=value; ReSize();} si.dwFlags|=STARTF_USESIZE;}
	void __fastcall writeTitle(AnsiString value) {if(value=="0" || value!=NULL) {si.lpTitle=value.c_str(); if(handleid>0) Run();}}
public:
	__fastcall TShellItem(TForm *AOwnerForm) {OwnerForm=AOwnerForm; cmdline=cmdlinepath=""; procflags=0; ZeroMemory(&si, sizeof(si)); si.cb = sizeof(si); ZeroMemory(&pi, sizeof(pi)); handleid=0; hWnd=NULL; stopitemondestruct=true;}
	__fastcall ~TShellItem() {if(stopitemondestruct) Stop();}

	void __fastcall Run(bool CloseExistsing=true);
	void __fastcall ReSize();

	__property AnsiString CmdLine = {read=cmdline, write=writeCmdLine};
	__property AnsiString CmdLinePath = {read=cmdlinepath};
	__property AnsiString Title = {read=readTitle, write=writeTitle};
	__property unsigned int X = {read=readX, write=writeX};
	__property unsigned int Y = {read=readY, write=writeY};
	__property unsigned int Width = {read=readWidth, write=writeWidth};
	__property unsigned int Height = {read=readHeight, write=writeHeight};
	__property PROCESS_INFORMATION ProcInfo = {read=pi};
	__property unsigned long ProcessID = {read=handleid};
	__property HWND ProcessHWND = {read=hWnd};
	__property bool StopItemOnDestruct = {read=stopitemondestruct, write=stopitemondestruct};
	__property unsigned int ProcessCreationFlags = {read=procflags, write=procflags};
};

#endif
