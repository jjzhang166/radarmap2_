//---------------------------------------------------------------------------

#ifndef OptionsGpsUnitH
#define OptionsGpsUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <Graphics.hpp>
#include <pngimage.hpp>
#include <ComCtrls.hpp>
#include <ImgList.hpp>
#include <ImageButton.h>
#include "CheckBoxTreeView.h"
#include "PlugIns.h"
//---------------------------------------------------------------------------

class TOptionsGpsUnitForm : public TForm
{
__published:	// IDE-managed Components
	TPanel *Panel1;
	TPanel *Panel2;
	TPanel *Panel3;
	TPanel *Panel4;
	TPanel *ClientPanel;
	TPanel *TitlePanel;
	TLabel *Label1;
	TPanel *Panel8;
	TPanel *Panel9;
	TImageButton *CloseIButton;
	TPanel *Panel5;
	TPanel *Panel6;
	TPanel *Panel7;
	TPanel *PagesPanel;
	TLabel *Label3;
	TLabel *Label10;
	TLabel *Label4;
	TPanel *OkCancelPanel;
	TImageButton *CancelImageButton;
	TImageButton *OkImageButton;
	TEdit *TempEdit;
	TPanel *XPanel;
	TLabel *GpsUnitLabel;
	TPanel *Panel10;
	TLabel *DescriptionLabel;
	TPanel *Panel11;
	TEdit *ValueEdit;
	TPanel *TabsPanel;
	TListBox *ParamsLBox;
	void __fastcall OkImageClick(TObject *Sender);
	void __fastcall CancelImageClick(TObject *Sender);
	void __fastcall ParamsLBoxClick(TObject *Sender);
	void __fastcall ValueEditChange(TObject *Sender);
	void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
	void __fastcall FormHide(TObject *Sender);
	void __fastcall TitlePanelMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
	void __fastcall TitlePanelMouseMove(TObject *Sender, TShiftState Shift, int X, int Y);
	void __fastcall TitlePanelMouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
	void __fastcall FormShow(TObject *Sender);

private:	// User declarations
	TList *Params;
	bool TitleDown;
	Types::TPoint TitleXY;
public:		// User declarations
	__fastcall TOptionsGpsUnitForm(TComponent* Owner);
	__fastcall ~TOptionsGpsUnitForm();
	int __fastcall ShowOptions(AnsiString aGpsUnitFileName);
};
//---------------------------------------------------------------------------
extern PACKAGE TOptionsGpsUnitForm *OptionsGpsUnitForm;
//---------------------------------------------------------------------------
#endif
