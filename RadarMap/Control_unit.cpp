//---------------------------------------------------------------------------
#include <math.h>

#pragma hdrstop

#include "Control_unit.h"
#include "Manager.h"
#include <Registry.hpp>
#include "Defines.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

//---------------------------------------------------------------------------
// TGainFunction
//---------------------------------------------------------------------------

void __fastcall TGainFunction::writeGainPointsCount(int value)
{
	if(value>=2 && value<=MaxGainPointsCount)
	{
		gainpointscount = value;
		RebuildGainFunction();
	}
}
//---------------------------------------------------------------------------

void __fastcall TGainFunction::writeRealSamples(int value)
{
	if(realsamples!=value && value>0)
	{
		realsamples = value;
		if (gainfunction != NULL)
			delete[] gainfunction;
		gainfunction = new double[value];
		RebuildGainFunction();
	}
}
//---------------------------------------------------------------------------

TGainPoint __fastcall TGainFunction::readGainPoint(int i)
{
	while(WaitForSingleObject(WaitWhileCopyEvent, 0)==WAIT_OBJECT_0) ;

	if(i>=gainpointscount) i=gainpointscount-1;
	else if(i<0) i=0;

	return gainpoints[i];
}
//---------------------------------------------------------------------------

void __fastcall TGainFunction::writeGainPoint(int i, TGainPoint value)
{
	if(i>=gainpointscount) i=gainpointscount-1;
	else if(i<0) i=0;
	if(value.x<0 || i==0) value.x=0;
	else if(value.x>1 || i==gainpointscount-1) value.x=1;
	//if(i==0)
	if(value.y<0) value.y=0;
	else if(value.y>MaxGain) value.y=MaxGain;
	gainpoints[i]=value;
	RebuildGainFunction();
}
//---------------------------------------------------------------------------

__fastcall TGainFunction::TGainFunction(int ms)
{
	if(ms>0) realsamples = ms;
	else realsamples=512;
	gainpointscount=3;
	gainpoints = new TGainPoint[MaxGainPointsCount];
	gainpoints[0]=TGainPoint(0, 0);
	gainpoints[1]=TGainPoint(0.14, 0);
	gainpoints[2]=TGainPoint(1, 30);//MaxGain);
	SavedGainPoints = new int[4 * MaxGainPointsCount];
	gainfunction = new double[ms];
	WaitWhileCopyEvent=CreateEvent(NULL, true, false, NULL);
	applygain=true;
	RebuildGainFunction();
}
//---------------------------------------------------------------------------

__fastcall TGainFunction::~TGainFunction()
{
	delete[] gainpoints;
	delete[] SavedGainPoints;
	delete[] gainfunction;
}
//---------------------------------------------------------------------------

double __fastcall TGainFunction::readGainFunction(int i)
{
	while(WaitForSingleObject(WaitWhileCopyEvent, 0)==WAIT_OBJECT_0) ;

	if(i>=0 && i<realsamples) return gainfunction[i];
	else return 0;
}
//---------------------------------------------------------------------------

void __fastcall TGainFunction::writeApplyGain(bool value)
{
	if(applygain!=value)
	{
		applygain=value;
		RebuildGainFunction();
	}
}
//---------------------------------------------------------------------------

void __fastcall TGainFunction::RebuildGainFunction()
{
	double da, bp, ep;
	int i, j, N, r, k;

	if(applygain)
	{
		N=gainpointscount-1;
		for (i=0; i<N; i++)
		{
			j=(int)((double)realsamples*gainpoints[i].x);
			if (i==(N-1)) k=realsamples;
			else k=(int)((double)realsamples*gainpoints[i+1].x);
			bp=pow(10.0, (double)gainpoints[i].y/20.0);
			ep=pow(10.0, (double)gainpoints[i+1].y/20.0);
			if(k>j)
			{
				da=(ep-bp)/(double)(k-j);
				for (r=j; r<k; r++)
					gainfunction[r]=(double)bp+da*(double)(r-j);
			}
		}
	}
	else
	{
		for (i=0; i<realsamples; i++) gainfunction[i]=1.;
	}
}
//---------------------------------------------------------------------------

void __fastcall TGainFunction::Copy(TGainFunction* Source)
{
	if(this!=Source)
	{
		SetEvent(WaitWhileCopyEvent);
		try
		{
			RealSamples=Source->RealSamples;
			GainPointsCount=Source->GainPointsCount;
			for(int i=0; i<gainpointscount; i++)
			{
				GainPoints[i]=Source->GainPoints[i];
			}
		}
		__finally
		{
			ResetEvent(WaitWhileCopyEvent);
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TGainFunction::CopyToProfile(TProfile *Prof, bool UpdateTraces)
{
	TMyPoint *gp;

	SetEvent(WaitWhileCopyEvent);
	try
	{
		try
		{
			if(Prof)
			{
				if(Prof->Vertexes)
				{
					for(int i=0; i< Prof->Vertexes->Count; i++)
						delete (TMyPoint*)Prof->Vertexes->Items[i];
					Prof->Vertexes->Clear();
				}
				else Prof->Vertexes=new TList;
				if(Prof->Vertexes)
				{
					for(int i=0; i<gainpointscount; i++)
					{
						gp=new TMyPoint(gainpoints[i].x, gainpoints[i].y);
						gp->y/=MaxGain;
						Prof->Vertexes->Add(gp);
					}
				}
				if(UpdateTraces)
				{
					TTrace *ptr;
					double d;

					ptr=Prof->FirstTracePtr;
					while(ptr)
					{
						for(int i=0; i<Prof->Samples; i++)
						{
							d=(double)ptr->SourceData[i];
							d*=gainfunction[i];
							if(d>(double)ptr->PositiveMax) d=(double)ptr->PositiveMax;
							else if(d<(double)ptr->NegativeMax) d=(double)ptr->NegativeMax;
							ptr->Data[i]=(short)d;
						}
						ptr=ptr->PtrUp;
                    }
                }
			}
		}
		catch (Exception &e){}
	}
	__finally
	{
		ResetEvent(WaitWhileCopyEvent);
	}
}
//---------------------------------------------------------------------------

void __fastcall TGainFunction::CopyFromProfile(TProfile *Prof)
{
	TMyPoint *gp;
	bool old_style=true;

	SetEvent(WaitWhileCopyEvent);
	try
	{
		try
		{
			if(Prof && Prof->Vertexes)
			{
				RealSamples=Prof->Samples;
				GainPointsCount=Prof->Vertexes->Count;
				for(int i=0; i<gainpointscount; i++)
				{
					gp=(TMyPoint*)Prof->Vertexes->Items[i];
					if(gp->y>1.)
					{
						old_style=false;
						break;
					}
				}
				for(int i=0; i<gainpointscount; i++)
				{
					gp=(TMyPoint*)Prof->Vertexes->Items[i];
					if(old_style) gp->y*=MaxGain;
					GainPoints[i]=TGainPoint(gp->x, gp->y);
				}
			}
		}
		catch (Exception &e){}
	}
	__finally
	{
		ResetEvent(WaitWhileCopyEvent);
	}
}
//---------------------------------------------------------------------------

TGainPoint* __fastcall TGainFunction::InsertGainPoint(int sample, float dB)
{
	float x;
	TGainPoint* res=NULL;

	if(gainpointscount<MaxGainPointsCount)
	{
		if(sample==0) x=0;
		else if(sample>realsamples) x=1;
		else x=(float)sample/(float)realsamples;
		for(int i=0; i<gainpointscount-1; i++)
		{
			if(x>gainpoints[i].x && x<gainpoints[i+1].x)
			{
				for(int j=gainpointscount; j>i; j--)
					gainpoints[j]=gainpoints[j-1];
				gainpoints[i+1]=TGainPoint(x, dB);
				gainpointscount++;
				RebuildGainFunction();
				res=&gainpoints[i+1];
				break;
			}
		}
	}
	return res;
}
//---------------------------------------------------------------------------

bool __fastcall TGainFunction::DeleteGainPoint(int id)
{
	bool res=false;

	if(gainpointscount>2 && id>0 && id<gainpointscount-1)
	{
		for(int j=id; j<gainpointscount; j++)
			gainpoints[j]=gainpoints[j+1];
		gainpointscount--;
		RebuildGainFunction();
		res=true;
	}
	return res;
}
//---------------------------------------------------------------------------

bool __fastcall TGainFunction::DeleteGainPoint(TGainPoint gp)
{
	bool res=false;

	if(gainpointscount>2)
	{
		for(int j=1; j<gainpointscount-1; j++)
		{
			if(gainpoints[j]==gp)
			{
				res=DeleteGainPoint(j);
				break;
			}
		}
	}
	return res;
}

//---------------------------------------------------------------------------
// TTuneData
//---------------------------------------------------------------------------
void __fastcall TTuneData::write_t_delay(int value)
{
	if (value < 1024 && value >= 0) t_delay = value;
}

__fastcall TTuneData::TTuneData()
{
	t_DELAY = 500;
}
/* __fastcall void TTuneData::SaveData(TXMLDocument* wr)
{
}
__fastcall bool TTuneData::LoadData(TXmlDocument rd)
{
} */

//---------------------------------------------------------------------------
// TFrequencyFilter
//---------------------------------------------------------------------------
void __fastcall TFrequencyFilter::FilterReCalcul()
{
	float fp, delta1, sp, delta2;
	int k, kk, kkk, z, zz, zzz, tv, max, N;

	N=samples;
	frequencyrange=0.5/(timerange/1000.0/(float)(N-1));
	fp=FrontLow/frequencyrange;
	delta1=FrontHigh/frequencyrange-fp;
	sp=CutHigh/frequencyrange;
	delta2=CutLow/frequencyrange-sp;

	k=(int)(fp*(float)N);
	kk=(int)(delta1*(float)N);
	kkk=k+kk;
	z=(int)(sp*(float)N);
	zz=(int)(delta2*(float)N);
	zzz=z+zz;
	if(delta1>=0 && delta2>=0 && kkk<=z)
	{
		for(int j=0; j<N; j++)
		{
		  if(j<=k) fr[j]=0.;
		  if(j>k && j<=kkk) fr[j]=(-0.5)*cos((float)(j-k)*M_PI/(float)kk)+0.5;
		  if(j>kkk && j<=z) fr[j]=1.;
		  if(j>z && j<=zzz) fr[j]=0.5*cos((float)(j-z)*M_PI/(float)zz)+0.5;
		  if(j>zzz) fr[j]=0.;
		}
	}
	else for(int j=0; j<N; j++) fr[j]=1.;
	for(int j=N; j<N*2; j++) fr[j]=fr[N*2-j-1];
}
//---------------------------------------------------------------------------

void __fastcall TFrequencyFilter::writeSamples(int value)
{
	if(value>samples || value<=(samples>>1))
	{
		samplespower=-1;
		do
		{
			samplespower++;
			samples=(int)ldexp(1., samplespower);
		} while(value>=samples);
		delete[] fr;
		fr=new double[samples*2];
		FilterReCalcul();
	}
}
//---------------------------------------------------------------------------
// TAntenna
//---------------------------------------------------------------------------
void __fastcall TAntenna::writeSelectedTimeRangeIndex(int value)
{
	if (value < TimeRangeQuantity && value>=0)
	{
		selectedtimerange = value;
		if (value == 0)
		{
			if (TimeRangeQuantity == 2)
			{
				tunetimerange = 1;
				localmaxtimerange = MaxTimeRange;
			}
			else
			{
				if (TimeRange >= 0 && TimeRange < 50) { localmaxtimerange = 50; tunetimerange = 1; }
				else if (TimeRange >= 50 && TimeRange < 100) { localmaxtimerange = 100; tunetimerange = 2; }
				else if (TimeRange >= 100 && TimeRange < 200) { localmaxtimerange = 200; tunetimerange = 3; }
				else if (TimeRange >= 200 && TimeRange < 300) { localmaxtimerange = 300; tunetimerange = 4; }
				else if (TimeRange >= 300 && TimeRange < 500) { localmaxtimerange = 500; tunetimerange = 5; }
				else if (TimeRange >= 500 && TimeRange < 800) { localmaxtimerange = 800; tunetimerange = 6; }
				else if (TimeRange >= 800 && TimeRange < 1200) { localmaxtimerange = 1200; tunetimerange = 7; }
				else if (TimeRange >= 1200 && TimeRange < 2000) { localmaxtimerange = 2000; tunetimerange = 8; }
			}
		}
		else
		{
			tunetimerange = value;
			switch (value)
			{
				case 1: localmaxtimerange = 50; break;
				case 2: localmaxtimerange = 100; break;
				case 3: localmaxtimerange = 200; break;
				case 4: localmaxtimerange = 300; break;
				case 5: localmaxtimerange = 500; break;
				case 6: localmaxtimerange = 800; break;
				case 7: localmaxtimerange = 1200; break;
				case 8: localmaxtimerange = 2000; break;
			}
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TAntenna::writeTimeRange(int value)
{
	if(value<1) value=1;
	//else if(value>2000) value=2000;
	else if(value>MaxTimeRange) value=MaxTimeRange;

	timerange = value;
	if (TimeRangeQuantity == 2)
	{
		localmaxtimerange = MaxTimeRange;
		if (timerange == MaxTimeRange) SelectedTimeRangeIndex = 1;
		else SelectedTimeRangeIndex = 0;
	}
	else
	{
		localmaxtimerange = value;
		if (timerange == 50) SelectedTimeRangeIndex = 1;
		else if (timerange == 100) SelectedTimeRangeIndex = 2;
		else if (timerange == 200) SelectedTimeRangeIndex = 3;
		else if (timerange == 300) SelectedTimeRangeIndex = 4;
		else if (timerange == 500) SelectedTimeRangeIndex = 5;
		else if (timerange == 800) SelectedTimeRangeIndex = 6;
		else if (timerange == 1200)	SelectedTimeRangeIndex = 7;
		else if (timerange == 2000)	SelectedTimeRangeIndex = 8;
		else SelectedTimeRangeIndex = 0;
	}

	if(Preset) Preset->TimeRange=value;
}
//---------------------------------------------------------------------------

void __fastcall TAntenna::writeSamples(int value)
{
	if(value<0) value=0;
	else if(value>2048) value=2048;

	samples = value;

	if(Preset) Preset->Samples=value;
}
//---------------------------------------------------------------------------

__fastcall TAntenna::TAntenna(int trq, int mtr, AnsiString nm, int freq, TAntennaType at, TAntennaShielded ash, float dx, int ASamples)
{
	preset=NULL;
	TimeRangeQuantity = trq;
	MaxTimeRange = mtr;
	td = new TTuneData*[trq];
	if (nm == NULL || nm == "")	Name = "Noname";
	else Name=nm;
	Frequency = freq;
	Type = at;
	Shielded = ash;
	Offset = dx;
	td = new TTuneData*[trq];
	for (int i = 0; i < trq; i++)
		td[i] = new TTuneData();
	SelectedTimeRangeIndex = 0;
	TimeRange = 50;
	samples=ASamples;
}
//---------------------------------------------------------------------------

__fastcall TAntenna::~TAntenna()
{
	for (int i = 0; i < TimeRangeQuantity; i++)
		delete TD[i];
	delete[] td;
	if(Preset) delete Preset;
}
//---------------------------------------------------------------------------

void __fastcall TAntenna::SetPresetFilter()
{
	SetPresetFilter(0.45*Frequency, 0.75*Frequency, 1.*Frequency, 1.5*Frequency);
}
//---------------------------------------------------------------------------

void __fastcall TAntenna::SetPresetFilter(double frontlow, double fronthigh, double cuthigh, double cutlow)
{
	if(preset) delete preset;
	preset=new TFrequencyFilter(samples, timerange);
	preset->AutomaticUpdate=false;
	preset->FrontLow=frontlow;
	preset->FrontHigh=fronthigh;
	preset->CutHigh=cuthigh;
	preset->CutLow=cutlow;
	preset->AutomaticUpdate=true;
}
//---------------------------------------------------------------------------

/* void __fastcall TAntenna::SaveData(XmlWriter* wr)
{
}
//---------------------------------------------------------------------------

bool __fastcall TAntenna::LoadData(XmlReader* rd)
{
} */

//---------------------------------------------------------------------------
// TChannel
//---------------------------------------------------------------------------

void __fastcall TChannel::writeSelectedAntennaIndex(int value)
{
	if (Antennas != NULL && value < Antennas->Count)
	{
		selectedindex = value;
		selected = (TAntenna*)Antennas->Items[value];
	}
}
//---------------------------------------------------------------------------

int __fastcall TChannel::readTimeRange()
{
	if (selected != NULL) return selected->TimeRange;
	else return 0;
}
//---------------------------------------------------------------------------

void __fastcall TChannel::writeTimeRange(int value)
{
	if (value > 0 && value <= MaxTimeRange)
	{
		if (selected != NULL)
			selected->TimeRange = value;
	}
}
//---------------------------------------------------------------------------

void __fastcall TChannel::writeRealSamples(int value)
{
	if(realsamples!=value && value>0)
	{
		realsamples=value;
		if(StackingBuf) delete[] StackingBuf;
		StackingBuf=new double[realsamples];
		for(int i=0; i<realsamples; i++) StackingBuf[i]=0;
		if(TraceBuf) delete[] TraceBuf;
		TraceBuf=new short[realsamples];
		if(traceaccumulator) delete traceaccumulator;
		traceaccumulator=new TTraceAccumulator(realsamples);
		if(Profile)
		{
			Profile->Samples=value;
			Profile->Clear(true);
		}
		if (Gain) Gain->RealSamples=realsamples;
		for (int i = 0; i < Antennas->Count; i++)
			if(Antennas->Items[i] && ((TAntenna*)(Antennas->Items[i]))->Preset)
				((TAntenna*)(Antennas->Items[i]))->Samples=value;
		MyMath->Samples=value;
	}
}
//---------------------------------------------------------------------------

void __fastcall TChannel::writeSelectedHPF(THPFState value)
{
	// ((int)value < HPFquantity)
		selectedHPF = value;
	if(selectedHPF==HPFPreset)
		for (int i = 0; i < Antennas->Count; i++)
			if(Antennas->Items[i]) ((TAntenna*)(Antennas->Items[i]))->SetPresetFilter();
}
//---------------------------------------------------------------------------

void __fastcall TChannel::writePulseDelay(int value)
{
	if (value > 0 && value < 1024 && SelectedAntenna && SelectedAntenna->SelectedTD)
		SelectedAntenna->SelectedTD->t_DELAY = value;
}
//---------------------------------------------------------------------------

void __fastcall TChannel::AddAntenna(TAntenna* ant)
{
	Antennas->Add(ant);
}
//---------------------------------------------------------------------------

void __fastcall TChannel::DeleteAntenna(int index)
{
	delete(TAntenna*)(Antennas->Items[index]);
	Antennas->Delete(index);
}
//---------------------------------------------------------------------------

int __fastcall TChannel::AntennasCount()
{
	return Antennas->Count;
}
//---------------------------------------------------------------------------

__fastcall TChannel::TChannel(TObject* AOwner, int ChN, THPFType ft, int maxtr, int trq, int ms)
{
	owner=AOwner;
	channelnumber = ChN;
	MaxTimeRange = maxtr;
	TimeRangeQuantity = trq;
	realsamples=-1;
	gain = NULL;
	Antennas = new TList;
	MyMath=new TFourierFamily(realsamples);
	
	Antennas->Add(new TAntenna(TimeRangeQuantity, MaxTimeRange, "", 0, Surface, Shielded, 0, 512));
	SelectedAntennaIndex = 0;
	TimeRangeQuantity = trq;
	TimeRange = 50; // ns
	HPFType = ft;
	switch(ft)
	{
		case THPFType::BuildIn:
			HPFquantity = 4; // OFF, Weak, Strong, SuperStrong
			break;
		case THPFType::Custom:
			HPFquantity = 5; // OFF, Weak, Strong, SuperStrong, Custom
			break;
		case THPFType::DigitalOnly: // OFF, Preset - Piton only for now
			HPFquantity = 2;
			break;
	}
	SelectedHPF = THPFState::HPFStateOFF;
	PulseDelay = 647;
	antennanames = NULL;
// Receiving variables
	StackingBuf=NULL;
	TraceBuf=NULL;
	traceaccumulator=NULL;
	if(ms>0) RealSamples = ms;
	else RealSamples = 512;

	stack=0;
	currenttrace=NULL;
	wheelpulsessum=0.0;
	wheelpulsesstack=0;
	modbusdistsum=0;
	CurrentTraceAccessEvent=CreateEvent(NULL, true, true, NULL);
	profile=NULL;
	markpressedevent=CreateEvent(NULL, false, false, NULL);
	ProfileLinked=false;
}
//---------------------------------------------------------------------------

void __fastcall TChannel::ClearAntennas()
{
	for (int i = 0; i < Antennas->Count; i++)
		delete(TAntenna*)(Antennas->Items[i]);
	Antennas->Clear();
}
//---------------------------------------------------------------------------

__fastcall TChannel::~TChannel()
{
	ClearAntennas();
	delete Antennas;
	if (antennanames != NULL)
		delete antennanames;
	delete MyMath;
	delete[] StackingBuf;
	delete[] TraceBuf;
	if(Gain) delete Gain;
	if(currenttrace) delete currenttrace;
	if(traceaccumulator) delete traceaccumulator;
	if(!ProfileLinked && profile!=NULL) delete profile;
	CloseHandle(CurrentTraceAccessEvent);
	CloseHandle(markpressedevent);
}
//---------------------------------------------------------------------------

int __fastcall TChannel::readPulseDelay()
{
	return selected->SelectedTD->t_DELAY;
}
//---------------------------------------------------------------------------

TStrings* __fastcall TChannel::readAntennaNames()
{
	if (antennanames != NULL) delete antennanames;
	antennanames = NULL;
	if (Antennas && Antennas->Count > 0)
	{
		antennanames = new TStringList();
		for (int i = 0; i < Antennas->Count; i++)
			antennanames->Add(((TAntenna*)(Antennas->Items[i]))->Name);
	}
	return antennanames;
}
//---------------------------------------------------------------------------

void __fastcall TChannel::RebuildAndStackData(TParsedTraceData* Data, bool Stack)
{
	double f, s, j, sh;
	int k, sn, i;

	try
	{
		if(Data && Data->RxData)
		{
			for(int i=0; i<RealSamples; i++) Data->RxData[i]-=Data->dS;
			// OrmsbyBandPass
			//if(ToFilter) FFT(BufIn, BufIn, dSN, Channel-1, HFilter);
			if(selected->SelectedTimeRangeIndex==0) //Custom Timerange
			{
				f=(float)selected->LocalMaxTimeRange/(float)selected->TimeRange;
				s=f;
				k=0;
				j=0.;
				for(i=0; i<RealSamples; i++)
				{
					if((int)s<i)
					{
						k++;
						s+=f;
						j=0.;
					}
					sh=Data->RxData[k];
					sh=sh+(j*(Data->RxData[k+1]-Data->RxData[k])/f);//-dS; ?????????????
					if(sh>(double)PositiveMax) sh=(double)PositiveMax;
					else if(sh<(double)NegativeMax) sh=(double)NegativeMax;
					if(Stack) StackingBuf[i]+=sh;
					else StackingBuf[i]=sh;
					j=j+1.;
				}
			}
			else
			{
				for(i=0; i<RealSamples; i++)
				{
					if(Stack) StackingBuf[i]+=Data->RxData[i];
					else StackingBuf[i]=Data->RxData[i];
				}
			}
			//if(stack==0) wheelpulsesstack=0;
			wheelpulsesstack+=Data->WheelPulses;
		}
	}
	catch(...) {}
}
//---------------------------------------------------------------------------

bool __fastcall TChannel::IsStacked(int N)
{
	double d;
	int i;

	stack++;
	if(stack>=N)
	{
		if(LockCurrentTrace())
		{
			if(currenttrace!=NULL) delete currenttrace;
			currenttrace=new TTrace(RealSamples);
			for(i=0; i<RealSamples; i++)
			{
				StackingBuf[i]=StackingBuf[i]/(double)N;
			}
	//Filtering of StackingBuf(SourceData)
			if(Owner)
			{
				//((TGPRUnit*)Owner)->WeightedSubtractionFilter(StackingBuf, RealSamples);
			}
	//End of Filtering
			for(i=0; i<RealSamples; i++)
			{
				d=StackingBuf[i];
				if(d>(double)PositiveMax) d=(double)PositiveMax;
				else if(d<(double)NegativeMax) d=(double)NegativeMax;
				currenttrace->SourceData[i]=(short)d;
				currenttrace->Data[i]=(short)d;
				StackingBuf[i]=0;
			}
	//Filtering of Data
			if(SelectedAntenna && SelectedAntenna->Preset && SelectedHPF==HPFPreset)  //Frequency Preset filter
			{
				MyMath->FrequencyFilter(currenttrace->GetArrayAddress(tatSource),
					currenttrace->GetArrayAddress(tatData), SelectedAntenna->Preset->FR, RealSamples);
			}
			if(Owner) // WeightedSubtraction (horizontal high pass)
			{
				((TGPRUnit*)Owner)->WeightedSubtractionFilter(currenttrace->GetArrayAddress(tatSource),
					currenttrace->GetArrayAddress(tatData), RealSamples);
			}
	//End of Filtering
			//Gaining
			for(i=0; i<RealSamples; i++)
			{
				d=currenttrace->Data[i];
				d*=Gain->GainFunction[i];
				if(d>(double)PositiveMax) d=(double)PositiveMax;
				else if(d<(double)NegativeMax) d=(double)NegativeMax;
				currenttrace->Data[i]=TraceBuf[i]=(short)d;
			}
			UnlockCurrentTrace();
		}
		stack=0;
		return true;
	}
	else return false;
}
//---------------------------------------------------------------------------

// !!! Method which got CurrentTrace shoud release(delete) it by itself !!!
TTrace* __fastcall TChannel::readCurrentTrace()
{
	TTrace* tmp;
	if(LockCurrentTrace())
	{
		tmp=currenttrace;
		currenttrace=NULL;
	}
	else tmp=NULL;
    UnlockCurrentTrace();
	return tmp;
}
//---------------------------------------------------------------------------

bool __fastcall TChannel::LockCurrentTrace()
{
	if(WaitForSingleObject(CurrentTraceAccessEvent, INFINITE)==WAIT_OBJECT_0)
	{
		ResetEvent(CurrentTraceAccessEvent);
		return true;
	}
	else return false;
}
//---------------------------------------------------------------------------

void __fastcall TChannel::UnlockCurrentTrace()
{
	SetEvent(CurrentTraceAccessEvent);
}
//---------------------------------------------------------------------------

/* void __fastcall TChannel::SaveChannel(XmlWriter* wr)
{
}
//---------------------------------------------------------------------------

bool __fastcall TChannel::LoadChannel(XmlReader* rd)
{
} */

//---------------------------------------------------------------------------
// TGPRUnit
//---------------------------------------------------------------------------
void __fastcall TGPRUnit::writeSamples(int value)
{
	if (value > 0 && value < 2048)
	{
		SamplesChanged=true;
		oldsamples = samples;
		samples = value;
	}
}
//---------------------------------------------------------------------------

void __fastcall TGPRUnit::ApplySamples(bool Force)
{
	if(Force || SamplesChanged)
	{
		oldsamples = samples;
		rxsamples = samples;
		rxpacketsize = rxsamples * sizeof(short);
		realsamples = rxsamples - (rxsamples >> 4);
		spacecenter = realsamples + (rxsamples >> 5);
		sigmasamples = realsamples >> 1;
		spacequart = rxsamples - (rxsamples >> 6);
		SpaceBufSize=rxsamples-realsamples;
		if(SpaceBuf) delete[] SpaceBuf;
		SpaceBuf=new short[SpaceBufSize];
		if (Channels != NULL)
			for (int i = 0; i < uschannelquantity; i++)
				Channels[i]->RealSamples = realsamples;
		SamplesChanged=false;
	}
}
//---------------------------------------------------------------------------

void __fastcall TGPRUnit::writeChannelQuantity(int value)
{
	if (Channels != NULL)
	{
		for (int i = 0; i < uschannelquantity; i++)
			if (Channels[i] != NULL)
			{
				if(Channels[i]->Profile) Channels[i]->Profile->UnLock();
				delete Channels[i];
			}
		delete[] Channels;
	}
	uschannelquantity = value;
	Channels = new TChannel*[value];
	for (int i = 0; i < value; i++)
		Channels[i] = new TChannel(this, i, sdfHPFType, sdfMaxTimeRange,
			sdfTimeRangeQuantity, realsamples);
}
//---------------------------------------------------------------------------

void __fastcall TGPRUnit::writeSelectedChannel(int value)
{
	if (value < ChannelQuantity && value >= 0)
		selectedchannel = value;
}
//---------------------------------------------------------------------------

void __fastcall TGPRUnit::writeChannelMode(TChannelMode value)
{
	channelmode=value;
	switch(channelmode)
	{
		case TChannelMode::Tx1Rx2:
		case TChannelMode::Tx2Rx1:
		case TChannelMode::Channel1: SelectedChannelIndex=0; break;
		case TChannelMode::Channel2: SelectedChannelIndex=1; break;
		case TChannelMode::Double:
		case TChannelMode::Circle: break; //Apply Double or Cycle switching of SelectedChannelIndex
	}
}
//---------------------------------------------------------------------------

void __fastcall TGPRUnit::writeBattery(bool value)
{
	if (sdfBattery) battery = value;
	else battery = false;
}
//---------------------------------------------------------------------------

TChannel* __fastcall TGPRUnit::readSelectedChannel()
{
	return Channels[selectedchannel];
}
//---------------------------------------------------------------------------

void __fastcall TGPRUnit::writeStacking(int value)
{
	if(value>0) stacking = value;
}
//---------------------------------------------------------------------------

AnsiString __fastcall TGPRUnit::GetTuneCommandString()
{
	int k, val;

	//BufStr = "T "; // "T" tune command, " " - space=32 chars
	BufStr="";
	if (REGIME == TRegime::OFF)	BufStr += "1";
	else BufStr += "0";
	// Cables 0 - Separated, 1 - Combined
	if (sdfCables)
	{
		if (Cables == TCables::Combined) BufStr += "1";
		else BufStr += "0";
	}
	else BufStr += "1";
	// Time Range,
	// 000 - 50 ns,
	// 001 - 100 ns,
	// 010 - 200 ns,
	// 011 - 300 ns,
	// 100 - 500 ns,
	// 101 - 800 ns,
	// 110 - 1200 ns,
	// 111 - 2000 ns,
	for (k = 0, val = 1; k < 3; k++, val *= 2)
	{
		if (((SelectedChannel->SelectedAntenna->TuneTimeRange - 1) & val) == 0)
			BufStr += "0";
		else BufStr += "1";
	}
	// Samples 00 - 128, 01 - 256, 10 - 512, 11 - 1024
	switch(samples)
	{
		case 128:
			BufStr += "00";
			break;
		case 256:
			BufStr += "10";
			break;
		case 512:
			BufStr += "01";
			break;
		case 1024:
			BufStr += "11";
			break;
		default:
			BufStr += "01";
			break;
	}
	// Additional channel, "1" - Full double channel
	switch(SelectedChannelMode)
	{
		case TChannelMode::Channel1:
		case TChannelMode::Channel2:
		case TChannelMode::Double:
			BufStr += "0";
			break;
		case TChannelMode::Tx1Rx2:
			if(UnitType==TRadarType::DOUBLE2) BufStr += "1";
			else BufStr += "0";
			break;
		case TChannelMode::Circle:
		case TChannelMode::Tx2Rx1:
			BufStr += "1";
			break;
		default:
			BufStr += "0";
			break;
	}
	// Not used
	BufStr += "000";
	// HPF 00 - OFF, 01 - Weak, 10 - Strong, 11 - SuperStrong
	for (k = 0, val = 1; k < 2; k++, val *= 2)
	{
		if (((int)SelectedChannel->SelectedHPF & val) == 0)	BufStr = BufStr + "0";
		else BufStr = BufStr + "1";
	}
	// Not used
	BufStr += "000";
	// Regime
	// 0110 - Calibration
	// 1010 - Signal
	// 1001 - Test
	BufStr += Regims[(int)REGIME];
	// Channel Mode 00 - Ch1, 01 - Ch2, 10 - Double & Circle, 11 - Tx1Rx2 & Tx2Rx1
	switch(SelectedChannelMode)
	{
		case TChannelMode::Channel1:
			BufStr += "00";
			break;
		case TChannelMode::Channel2:
			BufStr += "01";
			break;
		case TChannelMode::Double:
		case TChannelMode::Circle:
			BufStr += "10";
			break;
		case TChannelMode::Tx1Rx2:
		case TChannelMode::Tx2Rx1:
			BufStr += "11";
			break;
		default:
			BufStr += "00";
			break;
	}
	// PulseDelay
	for (k = 0, val = 1; k < 10; k++, val *= 2)
	{
		if ((SelectedChannel->PulseDelay & val) == 0) BufStr += "0";
		else BufStr += "1";
	}

	/*
!!!!!!!!!!!!!!!!!!!!! Second channel !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	*/

	char a=(char)BufStr.Length();
	BufStr = "T"+(AnsiString)a+BufStr;

	//Test signal
	//BufStr = "T 01100010000000001001000101011001";
	// 1.5 GHz antenna on channel 1
	//BufStr = "T 01000010000110001010001100011010";
	// InfraRadar
	//BufStr = "T 01000000000000000000000000000000";
	// FullDouble - Channel1
	//BufStr = "T 01000010000000001010000001011100";


	return BufStr;
}
//---------------------------------------------------------------------------

TCommand __fastcall TGPRUnit::GetTuneCommand()
{
	TCommand cmd;

	cmd.Cmd = GetTuneCommandString();
	cmd.NeedSynchroReply = NeedSynchroReply; //false;
	cmd.CommandReplyLength = 0;//1;//0;
	cmd.ByteCommand=false;

	return cmd;
}
//---------------------------------------------------------------------------

__fastcall TGPRUnit::TGPRUnit(AnsiString fn, TRadarType ut, TRegime RG, TObject* RadarMapManager)
{
	Channels = NULL;
	manager=RadarMapManager;
	samples=512;
	rxsamples = samples;
	rxpacketsize = rxsamples * sizeof(short);
	realsamples = rxsamples - (rxsamples >> 4);
	spacecenter = realsamples + (rxsamples >> 5);
	sigmasamples = realsamples >> 1;
	spacequart = rxsamples - (rxsamples >> 6);
	SpaceBufSize=rxsamples-realsamples;
	SpaceBuf=new short[SpaceBufSize];
	synchrolength=0;
	synchroword=NULL;
	ReInit(fn, ut, RG);
	name="UNKNOWN";
	Regims[TRegime::OFF] = "0110";
	Regims[TRegime::Calibration] = "0110";
	Regims[TRegime::Signal] = "1010";
	Regims[TRegime::Test] = "1001";
	SkipedTQ=0;
	OldBV=0;
	Old_dBV=0;
	if(manager && ((TRadarMapManager*)manager)->Settings) BatteryType=((TRadarMapManager*)manager)->Settings->BatteryType;
	else BatteryType=btLeadAcid;
	bps=bpsInt16;

	WSTracesCounter=WSSamples=WSWinWidth=0;
	WSBuffer=NULL;
}
//---------------------------------------------------------------------------

__fastcall TGPRUnit::~TGPRUnit()
{
	if (Channels != NULL)
	{
		for (int i = 0; i < uschannelquantity; i++)
			if (Channels[i] != NULL)
				delete Channels[i];
		delete[] Channels;
	}
	if(SpaceBuf) delete[] SpaceBuf;

	if(WSBuffer) delete WSBuffer;
    WSBuffer=NULL;
}
//---------------------------------------------------------------------------

void __fastcall TGPRUnit::ReInit(AnsiString fn, TRadarType ut, TRegime RG)
{
	int ch;
	AnsiString fn2;
	TAntenna *ant;

	switch(ut)
	{
		case TRadarType::SINGLE1:
		case TRadarType::SINGLE2:
			UnitName = "Zond-12e GPR Single Channel";
			fn2 = "single1";
			sdfHPFType = THPFType::Custom;
			sdfMaxTimeRange = 2000; // ns
			sdfTimeRangeQuantity = 9; // Custom, 50, 100, 200, 300, 500, 800, 1200, 2000
			sdfIsPulseDelay = true;
			sdfBattery = false; //(ut == TRadarType::SINGLE2); //SINGLE2 is equipped with LithiumIon battery, but there is no any battery level reading possibility
			Cables = TCables::Combined;
			sdfCables = true;
			sdfSamplesQuantity = 1; // Only 512
			Samples = 512;
			TracesPerSec = 56;
			ch = 1;
			NeedSynchroReply = false;
			break;
		case TRadarType::PYTHON3:
			UnitName = "Python-3 GPR";
			fn2 = "python";
			sdfMaxTimeRange = 1500; // ns
		case TRadarType::PYTHON3bis:
			if(ut==TRadarType::PYTHON3bis)
			{
				UnitName = "Python-3bis GPR";
				fn2 = "python3bis";
				sdfMaxTimeRange = 2500; // ns
			}
			sdfHPFType = THPFType::DigitalOnly;
			sdfTimeRangeQuantity = 2; // Custom, 1500
			sdfIsPulseDelay = false;
			sdfBattery = true;//false;
			sdfCables = false;
			sdfSamplesQuantity = 1; // Only 1024
			Samples = 1024;
			TracesPerSec = 41;
			ch = 1;
			NeedSynchroReply = true;
			SynchroLength=4;
			SynchroWord[0]=0; SynchroWord[1]=127; SynchroWord[2]=0; SynchroWord[3]=127;
			break;
		case TRadarType::DOUBLE1:
			UnitName = "Zond-12e GPR Double Channel";
			fn2 = "double1";
			sdfHPFType = THPFType::Custom;
			sdfMaxTimeRange = 2000; // ns
			sdfTimeRangeQuantity = 9; // Custom, 50, 100, 200, 300, 500, 800, 1200, 2000
			sdfIsPulseDelay = true;
			sdfBattery = false;
			sdfCables = false;
			sdfSamplesQuantity = 1; // Only 512
			Samples = 512;
			TracesPerSec = 80;
			ch = 2;
			NeedSynchroReply = false;
			break;
		case TRadarType::DOUBLE2:
			UnitName = "Zond-12e GPR Double Channel";
			fn2 = "double2";
			sdfHPFType = THPFType::Custom;
			sdfMaxTimeRange = 2000; // ns
			sdfTimeRangeQuantity = 9; // Custom, 50, 100, 200, 300, 500, 800, 1200, 2000
			sdfIsPulseDelay = true;
			sdfBattery = false;
			sdfCables = false;
			sdfSamplesQuantity = 1; // Only 512
			Samples = 512;
			TracesPerSec = 80;
			ch = 2;
			NeedSynchroReply = false;
			break;
		case TRadarType::DOUBLE9:
			fn2 = "double9";
			sdfTimeRangeQuantity = 9; // Custom, 50, 100, 200, 300, 500, 800, 1200, 2000
		case TRadarType::DUAL10:
			UnitName = "Zond-12e GPR Advanced";
		case COBRA_CBD:
		case COBRA:
			if(ut==TRadarType::COBRA)
			{
				UnitName = "RadarTeam COBRA WiFi GPR";
				fn2 = "cobra";
				sdfTimeRangeQuantity = 9; // 11;// Custom, 12, 25, 50, 100, 200, 300, 500, 800, 1200, 2000
			}
			else if(ut==TRadarType::COBRA_CBD)
			{
				UnitName = "RadarTeam COBRA CBD WiFi GPR";
				fn2 = "cobra";
				sdfTimeRangeQuantity = 9; // 11;// Custom, 12, 25, 50, 100, 200, 300, 500, 800, 1200, 2000
			}
			else if(ut==TRadarType::DUAL10)
			{
				fn2 = "dual10";
				sdfTimeRangeQuantity = 9; // 11;// Custom, 12, 25, 50, 100, 200, 300, 500, 800, 1200, 2000
			}
			sdfHPFType = THPFType::Custom;
			sdfMaxTimeRange = 2000; // ns
			sdfIsPulseDelay = true;
			sdfBattery = true;
			sdfCables = false;
			sdfSamplesQuantity = 4; // 1024, 512, 256, 128
			Samples = 512;
			TracesPerSec = 80;
			ch = 4;
			NeedSynchroReply = true;
			SynchroLength=4;
			SynchroWord[0]=0; SynchroWord[1]=127; SynchroWord[2]=0; SynchroWord[3]=127;
			break;
		case TRadarType::INFRARADAR:
		case TRadarType::INFRARADAR100:
			if(ut==TRadarType::INFRARADAR)
			{
				UnitName = "InfraRadar";
				fn2 = "infraradar";
				sdfMaxTimeRange = 50; // ns
			}
			else if(ut==TRadarType::INFRARADAR100)
			{
				UnitName = "InfraRadar100";
				fn2 = "infraradar100";
				sdfMaxTimeRange = 100; // ns
			}
			sdfHPFType = THPFType::DigitalOnly;
			sdfTimeRangeQuantity = 2; // Custom, MaxTimeRange
			sdfIsPulseDelay = false;
			sdfBattery = true;
			sdfCables = false;
			sdfSamplesQuantity = 1; // Only 512
			Samples = 512;
			TracesPerSec = 56;
			ch = 1;
			NeedSynchroReply = false;//true;
			break;
		case TRadarType::ZYNQ_0:
			UnitName = "ZYNQ_0 Prototype";
			fn2 = "zynq_0";
			sdfHPFType = THPFType::Custom;
			sdfMaxTimeRange = 4000; // ns
			sdfTimeRangeQuantity = 2; // Custom, MaxTimeRange
			sdfIsPulseDelay = false;
			sdfBattery = false;
			sdfCables = false;
			sdfSamplesQuantity = 1; // Only 1024
			Samples = 1024;
			TracesPerSec = 1000;
			ch = 1;
			NeedSynchroReply = true;
			SynchroLength=2;
			SynchroWord[0]='\r'; SynchroWord[1]='\n';
			break;
	}
	ApplySamples(true);
	if (fn != NULL && fn != "")	FileName = fn;
	else FileName = fn2 + ".xml";
	oldsamples=0;
	ChannelQuantity = ch;
	SelectedChannelMode = TChannelMode::Channel1;
//	SelectedChannelIndex = 0;
	UnitType = ut;
	Medium = 3; // Dry Sand, 5
	Permitivity = ((TRadarMapManager*)manager)->Settings->Permitivity;
	stacking = 1;
	REGIME = RG;
	ApplyConstantDeduct=((TRadarMapManager*)manager)->Settings->ApplyConstantDeduct;
	Battery = true;
	for (int i = 0; i < ch; i++)
	{
		if (Channels[i] != NULL)
		{
			Channels[i]->ClearAntennas();
			switch(ut)
			{
				case TRadarType::SINGLE1:
				case TRadarType::SINGLE2:
				case TRadarType::DOUBLE1:
				case TRadarType::DOUBLE2:
				case TRadarType::DOUBLE9:
				case TRadarType::DUAL10:
				//case TRadarType::COBRA:
					Channels[i]->Antennas->Add(new TAntenna(sdfTimeRangeQuantity, sdfMaxTimeRange, "100 MHz Shielded", 100, Surface, Shielded, 1.13, realsamples));
					Channels[i]->Antennas->Add(new TAntenna(sdfTimeRangeQuantity, sdfMaxTimeRange, "300 MHz Shielded", 300, Surface, Shielded, 0.51, realsamples));
					Channels[i]->Antennas->Add(new TAntenna(sdfTimeRangeQuantity, sdfMaxTimeRange, "500 A Shielded", 500, Air, Shielded, 0.28, realsamples));
					Channels[i]->Antennas->Add(new TAntenna(sdfTimeRangeQuantity, sdfMaxTimeRange, "750 MHz Shielded", 750, Air, Shielded, 0.6, realsamples));
					Channels[i]->Antennas->Add(new TAntenna(sdfTimeRangeQuantity, sdfMaxTimeRange, "900 A Shielded", 900, Air, Shielded, 0.18, realsamples));
					Channels[i]->Antennas->Add(new TAntenna(sdfTimeRangeQuantity, sdfMaxTimeRange, "1 GHz Shielded", 1000, Surface, Shielded, 0.12, realsamples));
					Channels[i]->Antennas->Add(new TAntenna(sdfTimeRangeQuantity, sdfMaxTimeRange, "1.5 GHz Shielded", 1500, Surface, Shielded, 0.13, realsamples));
					break;
				case TRadarType::COBRA:
					Channels[i]->Antennas->Add(new TAntenna(sdfTimeRangeQuantity, sdfMaxTimeRange, "COBRA WiFi Shielded", 500, Surface, Shielded, 0.2, realsamples));
					break;
				case TRadarType::COBRA_CBD:
					Channels[i]->Antennas->Add(new TAntenna(sdfTimeRangeQuantity, sdfMaxTimeRange, "COBRA CBD Shielded", 500, Air, Shielded, 0.2, realsamples));
					break;
				case TRadarType::PYTHON3:
				case TRadarType::PYTHON3bis:
					ant=new TAntenna(sdfTimeRangeQuantity, sdfMaxTimeRange, "25 MHz Unshielded", 25, Air, Unshielded, 0, realsamples);
					ant->SetPresetFilter();
					Channels[i]->Antennas->Add(ant);
					ant=new TAntenna(sdfTimeRangeQuantity, sdfMaxTimeRange, "38 MHz Unshielded", 38, Air, Unshielded, 0, realsamples);
					ant->SetPresetFilter();
					Channels[i]->Antennas->Add(ant);
					ant=new TAntenna(sdfTimeRangeQuantity, sdfMaxTimeRange, "50 MHz Unshielded", 50, Air, Unshielded, 0, realsamples);
					ant->SetPresetFilter();
					Channels[i]->Antennas->Add(ant);
					ant=new TAntenna(sdfTimeRangeQuantity, sdfMaxTimeRange, "100 MHz Unshielded", 100, Air, Unshielded, 0, realsamples);
					ant->SetPresetFilter();
					Channels[i]->Antennas->Add(ant);
					break;
				case TRadarType::INFRARADAR:
				case TRadarType::INFRARADAR100:
					Channels[i]->Antennas->Add(new TAntenna(sdfTimeRangeQuantity, sdfMaxTimeRange, "InfraRadar", 500, Air, Shielded, 0.2, realsamples));
					break;
				case TRadarType::ZYNQ_0:
					Channels[i]->Antennas->Add(new TAntenna(sdfTimeRangeQuantity, sdfMaxTimeRange, "Zynq Antenna", 500, Surface, Shielded, 0.3, realsamples));
					break;
			}
			if (Channels[i]->Gain) delete Channels[i]->Gain;
			Channels[i]->Gain=new TGainFunction(realsamples);
			Channels[i]->Gain->ApplyGain=((TRadarMapManager*)manager)->Settings->ApplyGain;
		}
	}
	markcounter=0;
	positioning=((TRadarMapManager*)manager)->Settings->WheelPositioning;//TPositioning::Wheel; //TPositioning::ManualP; //
	tracedistance=((TRadarMapManager*)manager)->Settings->WheelTraceDistance;//0.01; //1 cm
	wheelpulses=((TRadarMapManager*)manager)->Settings->WheelPulses;//128;
	wheeldiameter=((TRadarMapManager*)manager)->Settings->WheelDiameter;//0.126; //126 mm
	wheelstep=wheeldiameter*M_PI/(float)wheelpulses;
	pulsespertrace=tracedistance/wheelstep; // ~16
}
//---------------------------------------------------------------------------

void __fastcall TGPRUnit::PrepareTransmitterOFF()
{
	REGIME = TRegime::OFF;
}
//---------------------------------------------------------------------------

TCommand __fastcall TGPRUnit::GetStartCommand()
{
	return GetTuneCommand();
}
//---------------------------------------------------------------------------

TCommand __fastcall TGPRUnit::GetStopCommand()
{
	PrepareTransmitterOFF();
	return GetTuneCommand();
}
//---------------------------------------------------------------------------

TParsedData* __fastcall TGPRUnit::ReceivedDataParser(signed char* Buf, int BufLength)
{
	return NULL;
}
//---------------------------------------------------------------------------

void __fastcall TGPRUnit::SaveSettingsByName(AnsiString Chapter)
{
	TRegistry& regKey=*new TRegistry();
	AnsiString regkey=_RadarMapRegKey;
	TAntenna *ant;

	try
	{
		try
		{
			if(regKey.OpenKey(regkey+"\\"+Chapter,true))
			{
				regKey.WriteInteger("Stacking", Stacking);
				regKey.WriteInteger("SelectedChannelMode", SelectedChannelMode);
				regKey.WriteInteger("Medium", Medium);
				regKey.WriteFloat("Permitivity", Permitivity);
				regKey.WriteInteger("Samples", Samples);

				for(int i=0; i<ChannelQuantity; i++)
				{
					if(Channels[i])
					{
						regKey.CloseKey();
						if(regKey.OpenKey(regkey+"\\"+Chapter+"\\Channel"+IntToStr(i),true))
						{
							regKey.WriteInteger("SelectedHPF", (int)Channels[i]->SelectedHPF);
							regKey.WriteInteger("SelectedAntennaIndex", Channels[i]->SelectedAntennaIndex);
							regKey.CloseKey();
							for(int j=0; j<Channels[i]->Antennas->Count; j++)
							{
								ant=(TAntenna*)Channels[i]->Antennas->Items[j];
								if(ant && regKey.OpenKey(regkey+"\\"+Chapter+"\\Channel"+IntToStr(i)+"\\Antenna"+IntToStr(j), true))
								{
		// REWRITE !!! there is it's own TimeRange for each Antnna, and own Pulsedelay for each TimeRange !!!

									regKey.WriteInteger("TimeRange", ant->TimeRange);
									for(int k=0; k<sdfTimeRangeQuantity; k++)
									{
									   regKey.WriteInteger("PulseDelay"+IntToStr(k), ant->TD[k]->t_DELAY);
									}
								}
								regKey.CloseKey();
							}
						}
						//regKey.CloseKey();
						if(regKey.OpenKey(regkey+"\\"+Chapter+"\\Channel"+IntToStr(i)+"\\Gain",true))
						{
							regKey.WriteInteger("GainPointsCount", Channels[i]->Gain->GainPointsCount);
							for(int k=0; k<MaxGainPointsCount; k++)
							{
								regKey.WriteFloat("x"+IntToStr(k), Channels[i]->Gain->GainPoints[k].x);
								regKey.WriteFloat("y"+IntToStr(k), Channels[i]->Gain->GainPoints[k].y);
								regKey.WriteBool("l"+IntToStr(k), Channels[i]->Gain->GainPoints[k].Locked);
							}
						}
					}
				}
			}
		}
		catch(Exception &e)	{}
	}
	__finally
	{
		regKey.CloseKey();
		delete &regKey;
	}
}
//---------------------------------------------------------------------------

bool __fastcall TGPRUnit::LoadSettingsByName(AnsiString Chapter)
{
	TRegistry& regKey=*new TRegistry();
	AnsiString regkey=_RadarMapRegKey;
	TGainPoint gp;
	TAntenna *ant;

	try
	{
		try
		{
			if(regKey.OpenKey(regkey+"\\"+Chapter,true))
			{
				if(regKey.ValueExists("Stacking")) Stacking=regKey.ReadInteger("Stacking");
				if(regKey.ValueExists("SelectedChannelMode")) SelectedChannelMode=(TChannelMode)regKey.ReadInteger("SelectedChannelMode");
				if(regKey.ValueExists("Permitivity")) Permitivity=regKey.ReadFloat("Permitivity");
				if(regKey.ValueExists("Medium")) Medium=regKey.ReadInteger("Medium");
				if(regKey.ValueExists("Samples")) Samples=regKey.ReadInteger("Samples");
				for(int i=0; i<ChannelQuantity; i++)
				{
					if(Channels[i])
					{
						regKey.CloseKey();
						if(regKey.OpenKey(regkey+"\\"+Chapter+"\\Channel"+IntToStr(i),true))
						{
							if(regKey.ValueExists("SelectedHPF"))
								Channels[i]->SelectedHPF=(THPFState)regKey.ReadInteger("SelectedHPF");
							if(regKey.ValueExists("SelectedAntennaIndex"))
								Channels[i]->SelectedAntennaIndex=regKey.ReadInteger("SelectedAntennaIndex");
							regKey.CloseKey();
							for(int j=0; j<Channels[i]->Antennas->Count; j++)
							{
								ant=(TAntenna*)Channels[i]->Antennas->Items[j];
								if(ant && regKey.OpenKey(regkey+"\\"+Chapter+"\\Channel"+IntToStr(i)+"\\Antenna"+IntToStr(j),true))
								{

		// REWRITE !!! there is it's own TimeRange for each Antnna, and own Pulsedelay for each TimeRange !!!

									if(regKey.ValueExists("TimeRange"))	ant->TimeRange=regKey.ReadInteger("TimeRange");
									for(int k=0; k<sdfTimeRangeQuantity; k++)
									{
										if(regKey.ValueExists("PulseDelay"+IntToStr(k)))
											ant->TD[k]->t_DELAY=regKey.ReadInteger("PulseDelay"+IntToStr(k));
									}
									regKey.CloseKey();
								}
							}
						}
						//regKey.CloseKey();
						if(regKey.OpenKey(regkey+"\\"+Chapter+"\\Channel"+IntToStr(i)+"\\Gain",true) && Channels[i]->Gain)
						{
							if(regKey.ValueExists("GainPointsCount"))
								Channels[i]->Gain->GainPointsCount=regKey.ReadInteger("GainPointsCount");
							for(int k=0; k<MaxGainPointsCount; k++)
							{
								if(regKey.ValueExists("x"+IntToStr(k)) && regKey.ValueExists("y"+IntToStr(k)))
								{
									gp=TGainPoint();
									//if(Chapter=="InfraRadar" && k==1) gp.x=0.14;
									//else
									gp.x=regKey.ReadFloat("x"+IntToStr(k));
									gp.y=regKey.ReadFloat("y"+IntToStr(k));
									if(regKey.ValueExists("l"+IntToStr(k)))
										gp.Locked=regKey.ReadBool("l"+IntToStr(k));
									else gp.Locked=false;
									Channels[i]->Gain->GainPoints[k]=gp;
									if(gp.x>=1 && k>0)
									{
										Channels[i]->Gain->GainPointsCount=k+1;
										break;
									}
								}
							}
						}
					}
				}
			}
		}
		catch(Exception &e)	{}
	}
	__finally
	{
		regKey.CloseKey();
		delete &regKey;
	}
	return true;
}
//---------------------------------------------------------------------------

AnsiString __fastcall TGPRUnit::AsString()
{
	AnsiString str;

	str = "Control unit: " + UnitName + "\r\n";
	//str += "Unit type: " + RadarTypeStrings[(int)UnitType] + "\r\n";
	str += "Unit type: " + Name + "\r\n";
	str += "Sounding medium: " + SetupMediumStrings[Medium] + "\r\n";
	str += "Samples: " + IntToStr(Samples) + "\r\n";
	str += "Stacking: " + IntToStr(Stacking) + "\r\n";
	str += "Time range: " + IntToStr(SelectedChannel->TimeRange) + " ns\r\n";
	if(UnitType==TRadarType::INFRARADAR || UnitType==TRadarType::INFRARADAR100)
	{

	}
	else
	{
		if (UnitType == TRadarType::PYTHON3 || UnitType == TRadarType::PYTHON3bis)
		{

		}
		else if (UnitType == TRadarType::SINGLE1 || UnitType == TRadarType::SINGLE2)
		{
			str += "Cables: " + CablesStrings[(int)Cables] + "\r\n";
		}
		else if (UnitType >= TRadarType::DOUBLE1 && UnitType <= TRadarType::DUAL10)
		{
			str += "Channel mode: " + ChannelModeStrings[SelectedChannelMode] + "\r\n";
			if (SelectedChannelMode == TChannelMode::Circle ||
				SelectedChannelMode == TChannelMode::Double)
					str += "Channel number: " + IntToStr(SelectedChannel->ChannelNumber) + "\r\n";
		}
		str += "Antenna: " + SelectedChannel->SelectedAntenna->Name + "\r\n";
		str += "High pass filter: " + HPFStateStrings[(int)SelectedChannel->SelectedHPF] + "\r\n";
		str += "Pulse delay: " + IntToStr(SelectedChannel->PulseDelay);
	}
	return str;
}
//---------------------------------------------------------------------------

void __fastcall TGPRUnit::SetMarkPressetEvent()
{
	if (Channels != NULL)
	{
		for (int i = 0; i < uschannelquantity; i++)
			if (Channels[i] != NULL)
				SetEvent(Channels[i]->MarkPressedEvent);
		markcounter++;
	}
}
//---------------------------------------------------------------------------

void __fastcall TGPRUnit::ResetMarkPressetEvent()
{
	if (Channels != NULL)
	{
		for (int i = 0; i < uschannelquantity; i++)
			if (Channels[i] != NULL)
				ResetEvent(Channels[i]->MarkPressedEvent);
	}
}
//---------------------------------------------------------------------------

void __fastcall TGPRUnit::ResetChannelCollecting()
{
	if (Channels != NULL)
	{
		for (int i = 0; i < uschannelquantity; i++)
			if (Channels[i] != NULL)
				Channels[i]->ResetChannelCollecting();
	}
}
//---------------------------------------------------------------------------

void __fastcall TGPRUnit::writeTraceDistance(float value)
{
	if(value>0) tracedistance=value;
	pulsespertrace=tracedistance/wheelstep;
}
//---------------------------------------------------------------------------

void __fastcall TGPRUnit::writeWheelPulses(int value)
{
	if(value>0 && value<2048) wheelpulses=value;

	wheelstep=wheeldiameter*M_PI/(float)wheelpulses;
	pulsespertrace=tracedistance/wheelstep;
}
//---------------------------------------------------------------------------

void __fastcall TGPRUnit::writeWheelDiameter(float value)
{
	if(value>0 && value<1) wheeldiameter=value;

	wheelstep=wheeldiameter*M_PI/(float)wheelpulses;
	pulsespertrace=tracedistance/wheelstep;
}
//---------------------------------------------------------------------------

void __fastcall TGPRUnit::CreateChannelProfiles()
{
	TMyPoint *mp;

	if (Channels != NULL)
	{
		for (int i = 0; i < uschannelquantity; i++)
			if (Channels[i] != NULL)
			{
				TProfile *Prof;
				float dp;

				//LogEvent("PC=1 Channel="+IntToStr(i), "TGPRUnit::CreateChannelProfiles");
				Prof=new TProfile();
				//LogEvent("PC=2 Channel="+IntToStr(i), "TGPRUnit::CreateChannelProfiles");
				Prof->Samples=Channels[i]->RealSamples;
				Prof->TimeRange=Channels[i]->TimeRange;
				Prof->Permit=Permitivity;
				Prof->Stacking=Stacking;
				Prof->NumberOfChannels=1;
				//LogEvent("PC=3 Channel="+IntToStr(i), "TGPRUnit::CreateChannelProfiles");
				Prof->TextInfo=AsString();
                Prof->TextInfo+="\r\n"+(AnsiString)_RadarMapName+" "+_RadarMapVersion0;
				if(uschannelquantity>1)
					Prof->Name="Channel "+IntToStr(i+1);
				else Prof->Name="Profile";
				//LogEvent("PC=4 Channel="+IntToStr(i), "TGPRUnit::CreateChannelProfiles");
				if(manager)
				{
					if(((TRadarMapManager*)manager)->Palette)
						((TRadarMapManager*)manager)->Palette->ConvertToWinColors(Prof->Colors, 256);
					if(((TRadarMapManager*)manager)->Settings->WheelPositioning!=TPositioning::ManualP)
						Prof->XUnits=cxuDistance;
					else Prof->XUnits=cxuTraces;
				}
				//LogEvent("PC=5 Channel="+IntToStr(i), "TGPRUnit::CreateChannelProfiles");
				for(int k=0; k<Prof->Vertexes->Count; k++)
					delete (TMyPoint*)Prof->Vertexes->Items[k];
				Prof->Vertexes->Clear();
				for(int k=0; k<=Channels[i]->Gain->GainPointsCount; k++)
				{
					mp=new TMyPoint();
					mp->x=Channels[i]->Gain->GainPoints[k].x;
					mp->y=Channels[i]->Gain->GainPoints[k].y/MaxGain;
					Prof->Vertexes->Add(mp);
				}
				for(int k=0; k<Prof->Samples; k++)
					Prof->GainFunction[k]=Channels[i]->Gain->GainFunction[k];
				/*for(int i=0; i<4; i++)
					Prof->FilterPoints[i]=t_FILTER_POINTS[N][i];
				for(int i=0; i<Prof->Samples*2; i++)
					Prof->Filter[i]=Filter[N][i];
				Prof->FILTER_TYPE=FILTER_TYPE[N];
				UserPaletteDialog->FillColors(LastPalettes[N], Prof->Colors, Prof->Contrast);
				*/
				//LogEvent("PC=7 Channel="+IntToStr(i), "TGPRUnit::CreateChannelProfiles");
				Channels[i]->Profile=Prof;
			}
	}
	//LogEvent("PC=8", "TGPRUnit::CreateChannelProfiles");
}
//---------------------------------------------------------------------------

void __fastcall TGPRUnit::DeleteChannelProfiles()
{
	if (Channels != NULL)
	{
		for (int i = 0; i < uschannelquantity; i++)
			if (Channels[i] != NULL && Channels[i]->Profile != NULL)
			{
				Channels[i]->Profile->UnLock();
				delete Channels[i]->Profile;
				Channels[i]->Profile=NULL;
			}
	}
}
//---------------------------------------------------------------------------

void __fastcall TGPRUnit::WeightedSubtractionFilter(TSample* RxDataIn, TSample* RxDataOut, int N)
{
	int i, WindowWidth;
	long double dw;

	try
	{
		if(Manager && ((TRadarMapManager*)Manager)->Settings && ((TRadarMapManager*)Manager)->Settings->WeightedSubtractionFilter &&
			RxDataIn && RxDataOut && N>0 && SelectedChannel &&	((TRadarMapManager*)Manager)->Connection &&
			((TRadarMapManager*)Manager)->Current && //if Current!=NULL means that profiling is started
			((TRadarMapManager*)Manager)->ReceivingProfile && ((TRadarMapManager*)Manager)->ReceivingProfile->ZeroPoint!=0)
		{
			WindowWidth=((TRadarMapManager*)Manager)->Settings->WSWindowWidth;

			if(WindowWidth>1)
			{
				if(WSSamples!=N || WindowWidth!=WSWinWidth || !WSBuffer)
				{
					WSWinWidth=WindowWidth;
					WSTracesCounter=0;
					WSSamples=N;
					if(WSBuffer) delete[] WSBuffer;
					WSBuffer=new long double[N];
					for(i=0; i<N; i++) WSBuffer[i]=0.0;
				}
				WSTracesCounter++;
				dw=1./(long double)WSTracesCounter;
				for(i=0; i<N; i++)
				{
					WSBuffer[i]*=(1.-dw);
					WSBuffer[i]+=(long double)RxDataIn[i]*dw;
				}
				if(WSTracesCounter>1)//WindowWidth)
				{
					for(i=0; i<N; i++)
					{
						dw=(long double)RxDataIn[i]-WSBuffer[i];
						if(dw>(long double)PositiveMax) dw=(long double)PositiveMax;
						else if(dw<(long double)NegativeMax) dw=(long double)NegativeMax;
						RxDataOut[i]=(TSample)dw;
					}
				}
			}
		}
	}
	catch(Exception &e) {}
}

//----------------------------------------------------------------------------
// TBattery
//----------------------------------------------------------------------------
__fastcall TBattery::TBattery() : TIndicator()
{
	BatteryValuesQ=100;
	MaxBatteryVoltage=12.91;
	MaxBatteryValue=12718;//18790;
	BatteryValues=new float[BatteryValuesQ];
	BatteryVoltage=15;
	BatteryPercent=0;//100;
	Index=0;
	button=NULL;
	MaxIcons=5;
	IconPaths=new System::UnicodeString[MaxIcons+1];
	IconPaths[0]="PNGIMAGE_13";
	IconPaths[1]="PNGIMAGE_14";
	IconPaths[2]="PNGIMAGE_15";
	IconPaths[3]="PNGIMAGE_16";
	IconPaths[4]="PNGIMAGE_17";
	IconPaths[5]="PNGIMAGE_18";
	UndefinedBatteryButton="PNGIMAGE_215";
	MaxIndexes=10;
	IconIndex=new int[MaxIndexes];
	for(int i=0; i<MaxIndexes; i++)
		IconIndex[i]=MaxIcons;
	PrevIndex=-2;//MaxIcons;
    BatteryStatus=bstNone;
	BatteryType=btLeadAcid;
}
//---------------------------------------------------------------------------

__fastcall TBattery::~TBattery()
{
	delete[] BatteryValues;
	delete[] IconIndex;
}
//---------------------------------------------------------------------------

void __fastcall TBattery::ApplyDischargeModel(float const In[11])
{
	BatteryType=btCustom;
	for(int i=0; i<11; i++) Model[i]=In[i];
}
//---------------------------------------------------------------------------

void __fastcall TBattery::writeBatteryType(TBatteryType value)
{
	if(batterytype!=value)
	{
		switch(value)
		{
			case btNiCd:
				Model[0]=14.5;
				Model[1]=13.2;
				Model[2]=12.7;
				Model[3]=12.4;
				Model[4]=12.2;
				Model[5]=12.05;
				Model[6]=11.95;
				Model[7]=11.9;
				Model[8]=11.8;
				Model[9]=11.5;
				Model[10]=11.0;
				break;
			case btNiMh:
				Model[0]=14.15;
				Model[1]=12.95;
				Model[2]=12.7;
				Model[3]=12.6;
				Model[4]=12.54;
				Model[5]=12.44;
				Model[6]=12.25;
				Model[7]=12.05;
				Model[8]=11.79;
				Model[9]=11.36;
				Model[10]=10.59;
				break;
			case btLeadAcid:
				Model[0]=13.5;
				Model[1]=12.50;
				Model[2]=12.31;
				Model[3]=12.08;
				Model[4]=11.84;
				Model[5]=11.61;
				Model[6]=11.38;
				Model[7]=11.15;
				Model[8]=10.91;
				Model[9]=10.68;
				Model[10]=10.50;
				break;
			case btLithium:
				Model[0]=12.15;
				Model[1]=11.925;
				Model[2]=11.7;
				Model[3]=11.475;
				Model[4]=11.25;
				Model[5]=11.025;
				Model[6]=10.8;
				Model[7]=10.575;
				Model[8]=10.35;
				Model[9]=10.125;
				Model[10]=9.9;
				break;
			default:
			case btCustom:
				Model[0]=13.5;
				Model[1]=13.2;
				Model[2]=12.9;
				Model[3]=12.6;
				Model[4]=12.3;
				Model[5]=12.0;
				Model[6]=11.7;
				Model[7]=11.4;
				Model[8]=11.1;
				Model[9]=10.8;
				Model[10]=10.5;
				break;
		}
		batterytype=value;
		Index=0;
	}
}
//---------------------------------------------------------------------------

void __fastcall TBattery::AddValue(float BV)
{
	float f, bp;
	int i;
	// { 13.5, 12.50, 12.31, 12.08, 11.84, 11.61, 11.38, 11.15, 10.91, 10.68, 10.50 }; //Lead Acid Pb measured
	// { 14.15, 12.95, 12.7, 12.6, 12.54, 12.44, 12.25, 12.05, 11.79, 11.36, 10.59 }; //Ni-MH measured
	// { 14.5, 13.2, 12.7, 12.4, 12.2, 12.05, 11.95, 11.9, 11.8, 11.5, 11.0 }; //Ni-Cd

	if(Index<BatteryValuesQ)
	{
		BatteryValues[Index]=BV;
		Index++;
	}
	else
	{
		for(i=1; i<BatteryValuesQ; i++)
			BatteryValues[i-1]=BatteryValues[i];
		BatteryValues[BatteryValuesQ-1]=BV;
	}
	f=0;
	for(i=0; i<Index; i++)
		f+=BatteryValues[i];
	f/=(float)Index;
	BatteryVoltage=(f/MaxBatteryValue)*MaxBatteryVoltage;
	if (BatteryVoltage > Model[0])
	{
		BatteryStatus = bstCharging;
		BatteryPercent = 100;
		//ii = 5;
	}
	else if (BatteryVoltage < Model[10])
	{
		BatteryStatus = bstDischarged;
		BatteryPercent = 0;
		//ii = 4;
	}
	else
	{
		for (int k = 0; k < 10; k++)
		{
			if (BatteryVoltage <= Model[k] && BatteryVoltage > Model[k + 1])
			{
				bp = (int)(10 * (BatteryVoltage - Model[k + 1]) / (Model[k] - Model[k + 1]));
				bp += 90 - k * 10;
				break;
			}
		}
		BatteryStatus = bstDischarging;
		if(bp<=BatteryPercent || bp-2.>BatteryPercent) BatteryPercent=bp;
		//ii = 3 - (int)(BatteryPercent / 25);
	}
	ApplyIconOnButton();
}
//----------------------------------------------------------------------------

void __fastcall TBattery::ApplyIconOnButton()
{
	float devider;
	int i, j;

	if(button!=NULL)
	{
		try
		{
			if(Visible)
			{
				if(BatteryStatus==bstNone && PrevIndex!=-1)
				{
					button->NormalGlyphName=UndefinedBatteryButton;
					PrevIndex=-1;
				}
				else
				{
					if(BatteryStatus==bstCharging) i=MaxIcons;
					else if(BatteryStatus==bstDischarged) i=0;
					else
					{
						devider=100.0/(float)(MaxIcons-1);
						i=(int)((BatteryPercent+devider/2.0)/devider);
						if(i<0) i=0;
						else if(i>MaxIcons) i=MaxIcons;
					}
					for(j=1; j<MaxIndexes; j++)
						IconIndex[j-1]=IconIndex[j];
					IconIndex[MaxIndexes-1]=i;
					for(j=0; j<MaxIndexes-1; j++)
						if(IconIndex[j]!=i) break;
					if((j==MaxIndexes-1 && PrevIndex!=i) || PrevIndex==-1)
					{
						button->NormalGlyphName=IconPaths[i];
						PrevIndex=i;
					}
				}
			}
			else if(PrevIndex!=-1)
			{
				button->NormalGlyphName=EmptyButton;
				PrevIndex=-1;
			}
		}
		catch(Exception &e) {}
	}
}

//----------------------------------------------------------------------------
// TLaptopBattery
//----------------------------------------------------------------------------
__fastcall TLaptopBattery::TLaptopBattery(void) : TIndicator()
{
	BatteryPercent=0;//100;
	BatteryLife=0;
	button=NULL;
	MaxIcons=5;
	IconPaths=new System::UnicodeString[MaxIcons+1];
	IconPaths[0]="PNGIMAGE_216";
	IconPaths[1]="PNGIMAGE_217";
	IconPaths[2]="PNGIMAGE_218";
	IconPaths[3]="PNGIMAGE_219";
	IconPaths[4]="PNGIMAGE_220";
	IconPaths[5]="PNGIMAGE_222";
	DesktopBatteryButton="PNGIMAGE_221";
	PrevIndex=-2;//MaxIcons;
	BatteryStatus=bstNone;
	OnTimer();
	CheckBatteryTmr=new TTimerThread(5000);
	CheckBatteryTmr->MinStep=1000;
	CheckBatteryTmr->OnTimer=OnTimer;
	CheckBatteryTmr->Start();
}
//---------------------------------------------------------------------------

__fastcall TLaptopBattery::~TLaptopBattery()
{
	if(CheckBatteryTmr!=NULL)
	{
		CheckBatteryTmr->AskForTerminate();
		WaitOthers();
		if(CheckBatteryTmr->ForceTerminate()) delete CheckBatteryTmr;
		else CheckBatteryTmr->FreeOnTerminate=true;
		CheckBatteryTmr=NULL;
	}
}
//---------------------------------------------------------------------------

void __fastcall TLaptopBattery::OnTimer()
{
	SYSTEM_POWER_STATUS sps;

	if(GetSystemPowerStatus(&sps)!=0)
	{
		BatteryPercent=sps.BatteryLifePercent;
		BatteryLife=sps.BatteryLifeTime;
		switch(sps.BatteryFlag)
		{
			case 1:
			case 2: BatteryStatus=bstDischarging; break;
			case 4: BatteryStatus=bstDischarged; break;
			case 8: BatteryStatus=bstCharging; break;
			case 128: BatteryStatus=bstNone; break;
			default:
				if(sps.ACLineStatus==1) BatteryStatus=bstCharging;
				else BatteryStatus=bstUnknown;
		}
		ApplyIconOnButton();
	}
}
//---------------------------------------------------------------------------

void __fastcall TLaptopBattery::ApplyIconOnButton()
{
	float devider;
	int i, j;

	if(button!=NULL)
	{
		try
		{
			if(Visible && BatteryStatus!=bstUnknown)
			{
				switch(BatteryStatus)
				{
					case bstNone:
						button->NormalGlyphName=DesktopBatteryButton;
						i=-1;
						break;
					case bstCharging: i=MaxIcons; break;
					case bstDischarged: i=0; break;
					case bstDischarging:
						devider=100.0/(float)(MaxIcons-1);
						i=(int)((BatteryPercent+devider/2.0)/devider);
						if(i<0) i=0;
						else if(i>MaxIcons) i=MaxIcons;
						break;
					default : i=-2;
				}
				if(i>=0 && (PrevIndex!=i || PrevIndex==-1)) button->NormalGlyphName=IconPaths[i];
				PrevIndex=i;
			}
			else if(PrevIndex!=-1)
			{
				button->NormalGlyphName=EmptyButton;
				PrevIndex=-1;
			}
		}
		catch(Exception &e) {}
	}
}

//----------------------------------------------------------------------------
// TGPR_Single1
//----------------------------------------------------------------------------
TParsedData* __fastcall TGPR_Single1::ReceivedDataParser(signed char* Buf, int BufLength)
{
	int Sigma;
	int z, h, l, i, N, BV, dBV;
	bool ZeroLine;
	double value;
	TParsedTraceData *Out;

	Sigma=0;
	ZeroLine=true;
	z=0;
	i=0;
	if(BufLength<=RxPacketSize) N=BufLength;
	else N=RxPacketSize;
	Out=new TParsedTraceData(RxSamples, (void*)this);
	while(z<N)
	{
		h=(int)Buf[z];
		l=(unsigned char)Buf[z+1];
		if(i<RealSamples)
		{
			value=(int)((h<<BitCount)+l);
			if(value>PositiveMax) value=PositiveMax;
			else if(value<NegativeMax) value=NegativeMax;
			Out->RxData[i]=value;
			if(i>=SigmaSamples)	Sigma=Sigma+Out->RxData[i];
			if(Out->RxData[i]!=0) ZeroLine=false;
		}
		else
		{
			if(i<SpaceCenter || i==(RxSamples-1)) SpaceBuf[i-RealSamples]=(h<<BitCount)+l;
			else SpaceBuf[i-RealSamples]=(unsigned char)h;
		}
		i++;
		z+=2;
	}
	if(ZeroLine)
	{
		delete Out;
		Out=NULL;
	}
	else if(SkipedTQ>=40)
	{
		// Constant deduction coefficient
		Out->dS=(float)Sigma/(float)SigmaSamples;
		Out->dS*=ApplyConstantDeduct;
		// Channel Number
// ChannelNumber is allways 0 for SINGLE1
		// Out->ChannelNumber=!SpaceBuf[SpaceBufSize-2]; // Double1 && Double2
		Out->ChannelNumber = 0;
		// Wheel pulses
		BV=SpaceBuf[SpaceCenter-RealSamples];
		if(OldBV>BV) BV+=256;
		dBV=(BV-OldBV);
		if(dBV>128 && dBV>Old_dBV*2) dBV=Old_dBV;
		Out->WheelPulses=dBV;
		if(BV>255) OldBV=BV-256;
		else OldBV=BV;
		Old_dBV=dBV;
		// Backward direction
		//if(SpaceBuf[SpaceBufSize-1]==-1) Out->WheelPulses*=(-1);
		if(SpaceBuf[SpaceBufSize-1]!=0) Out->WheelPulses*=(-1);
		// Battery value
		//Out->BatteryValue=SpaceBuf[1];
		Out->BatteryValue=0;
	}
	else
	{
		SkipedTQ++;
		OldBV=SpaceBuf[SpaceCenter-RealSamples];
		delete Out;
		Out=NULL;
	}

	return (TParsedData*)Out;
}

//----------------------------------------------------------------------------
// TGPR_Double1
//----------------------------------------------------------------------------
TParsedData* __fastcall TGPR_Double1::ReceivedDataParser(signed char* Buf, int BufLength)
{
	return TGPRUnit::ReceivedDataParser(Buf, BufLength);
}

//----------------------------------------------------------------------------
// TGPR_FullDouble
//----------------------------------------------------------------------------

TParsedData* __fastcall TGPR_FullDouble::ReceivedDataParser(signed char* Buf, int BufLength)
{
	//return TGPRUnit::ReceivedDataParser(Buf, BufLength);
	int Sigma;
	int z, h, l, i, N, BV, dBV;
	bool ZeroLine;
	float f;
	double value;
	TParsedTraceData *Out;

	Sigma=0;
	ZeroLine=true;
	z=0;
	i=0;
	if(BufLength<=RxPacketSize) N=BufLength;
	else N=RxPacketSize;
	Out=new TParsedTraceData(RxSamples, (void*)this);
	while(z<N)
	{
		h=(int)Buf[z];
		l=(unsigned char)Buf[z+1];
		if(i<RealSamples)
		{
			value=(int)((h<<BitCount)+l);
			if(value>PositiveMax) value=PositiveMax;
			else if(value<NegativeMax) value=NegativeMax;
			Out->RxData[i]=value;
			if(i>=SigmaSamples)	Sigma=Sigma+Out->RxData[i];
			if(Out->RxData[i]!=0) ZeroLine=false;
		}
		else
		{
			if(i<=SpaceCenter) SpaceBuf[i-RealSamples]=(h<<BitCount)+l;
			else SpaceBuf[i-RealSamples]=(unsigned char)h;
		}
		i++;
		z+=2;
	}
	if(ZeroLine)
	{
		delete Out;
		Out=NULL;
	}
	else if(SkipedTQ>=40)
	{
		// Constant deduction coefficient
		Out->dS=(float)Sigma/(float)SigmaSamples;
		Out->dS*=ApplyConstantDeduct;
		// Channel Number
// ChannelNumber is allways 0 for this version
		Out->ChannelNumber=!SpaceBuf[RxSamples-RealSamples-2];
		//Out->ChannelNumber = 0;
		// Wheel pulses
		BV=SpaceBuf[SpaceCenter-RealSamples+1];
		if(OldBV>BV) BV+=256;
		dBV=(BV-OldBV);
		if(dBV>128 && dBV>Old_dBV*2) dBV=Old_dBV;
		Out->WheelPulses=dBV;
		if(BV>255) OldBV=BV-256;
		else OldBV=BV;
		Old_dBV=dBV;
		// Backward direction
		if(SpaceBuf[SpaceBufSize-1]==255) Out->WheelPulses*=(-1);
	}
	else
	{
		SkipedTQ++;
		OldBV=SpaceBuf[SpaceCenter-RealSamples+1];
		delete Out;
		Out=NULL;
	}

	return (TParsedData*)Out;
}

//----------------------------------------------------------------------------
// TGPR_Advanced
//----------------------------------------------------------------------------
TParsedData* __fastcall TGPR_Advanced::ReceivedDataParser(signed char* Buf, int BufLength)
{
	int Sigma;
	int z, h, l, i, N, BV, dBV;
	bool ZeroLine;
	float f;
	double value;
	TParsedTraceData *Out;

	Sigma=0;
	ZeroLine=true;
	z=0;
	i=0;
	if(BufLength<=RxPacketSize) N=BufLength;
	else N=RxPacketSize;
	Out=new TParsedTraceData(RxSamples, (void*)this);
	while(z<N)
	{
		h=(int)Buf[z+1];
		l=(unsigned char)Buf[z+2];
		if(i<RealSamples)
		{
			value=(int)((h<<BitCount)+l);
			if(value>PositiveMax) value=PositiveMax;
			else if(value<NegativeMax) value=NegativeMax;
			Out->RxData[i]=value;
			if(i>=SigmaSamples)	Sigma=Sigma+Out->RxData[i];
			if(Out->RxData[i]!=0) ZeroLine=false;
		}
		else
		{
			if(i<=SpaceCenter || i>=SpaceQuart) SpaceBuf[i-RealSamples]=(h<<BitCount)+l;
			else SpaceBuf[i-RealSamples]=(unsigned char)h;
		}
		i++;
		z+=2;
	}
	if(ZeroLine)
	{
		delete Out;
		Out=NULL;
	}
	else if(SkipedTQ>=40)
	{
		// Constant deduction coefficient
		Out->dS=(float)Sigma/(float)SigmaSamples;
		Out->dS*=ApplyConstantDeduct;
		// Channel Number
		Out->ChannelNumber=(SpaceBuf[SpaceQuart-RealSamples+1]>>6)&0x03;
		//Out->ChannelNumber = 0;
		// Wheel pulses
		BV=SpaceBuf[SpaceCenter-RealSamples+1];
		if(OldBV>BV) BV+=256;
		dBV=(BV-OldBV);
		if(dBV>128 && dBV>Old_dBV*2) dBV=Old_dBV;
		Out->WheelPulses=dBV;
		if(BV>255) OldBV=BV-256;
		else OldBV=BV;
		Old_dBV=dBV;
		// Backward direction
		if((SpaceBuf[SpaceQuart-RealSamples+1]>>5)&0x01) Out->WheelPulses*=(-1);
		// Battery value
		f=0;
		z=((SpaceCenter-RealSamples)>>1)-1;
		if(z==0) z=1;
		for(i=0; i<z; i++)
			f+=(float)SpaceBuf[i+1];
		f/=(float)z;
		Out->BatteryValue=f;// SpaceBuf[3]; //[1];
	}
	else
	{
		SkipedTQ++;
		OldBV=SpaceBuf[SpaceCenter-RealSamples+1];
		delete Out;
		Out=NULL;
	}

	return (TParsedData*)Out;
}

//----------------------------------------------------------------------------
// TGPR_Python3
//----------------------------------------------------------------------------
TParsedData* __fastcall TGPR_Python3::ReceivedDataParser(signed char* Buf, int BufLength)
{
	int Sigma;
	int z, h, l, i, N, BV, dBV;
	bool ZeroLine;
	float f;
	double value;
	TParsedTraceData *Out;

	Sigma=0;
	ZeroLine=true;
	z=0;
	i=0;
	if(BufLength<=RxPacketSize) N=BufLength;
	else N=RxPacketSize;
	Out=new TParsedTraceData(RxSamples, (void*)this);
	while(z<N)
	{
		h=(int)Buf[z+1];
		l=(unsigned char)Buf[z+2];
		if(i<RealSamples)
		{
			value=(int)((h<<BitCount)+l);
			if(value>PositiveMax) value=PositiveMax;
			else if(value<NegativeMax) value=NegativeMax;
			Out->RxData[i]=value;
			if(i>=SigmaSamples)	Sigma=Sigma+Out->RxData[i];
			if(Out->RxData[i]!=0) ZeroLine=false;
		}
		else
		{
			if(i<=SpaceCenter || i>=SpaceQuart) SpaceBuf[i-RealSamples]=(h<<BitCount)+l;
			else SpaceBuf[i-RealSamples]=(unsigned char)h;
		}
		i++;
		z+=2;
	}
	if(ZeroLine)
	{
		delete Out;
		Out=NULL;
	}
	else if(SkipedTQ>=40)
	{
		// Constant deduction coefficient
		Out->dS=(float)Sigma/(float)SigmaSamples;
		Out->dS*=ApplyConstantDeduct;
		// Channel Number
// ChannelNumber is allways 0 for this version
		Out->ChannelNumber=(SpaceBuf[SpaceQuart-RealSamples+1]>>6)&0x03;
		Out->ChannelNumber = 0;
		// Wheel pulses
		BV=SpaceBuf[SpaceCenter-RealSamples+1];
		if(OldBV>BV) BV+=256;
		dBV=(BV-OldBV);
		if(dBV>128 && dBV>Old_dBV*2) dBV=Old_dBV;
		Out->WheelPulses=dBV;
		if(BV>255) OldBV=BV-256;
		else OldBV=BV;
		Old_dBV=dBV;
		// Backward direction
		if((SpaceBuf[SpaceQuart-RealSamples+1]>>5)&0x01) Out->WheelPulses*=(-1);
		// Battery value
		f=0;
		z=((SpaceCenter-RealSamples)>>1)-1;
		if(z==0) z=1;
		for(i=0; i<z; i++)
			f+=(float)SpaceBuf[i+1];
		f/=(float)z;
		Out->BatteryValue=f;// SpaceBuf[3]; //[1];
	}
	else
	{
		SkipedTQ++;
		OldBV=SpaceBuf[SpaceCenter-RealSamples+1];
		delete Out;
		Out=NULL;
	}

	return (TParsedData*)Out;
}

//----------------------------------------------------------------------------
// TGPR_InfraRadar
//----------------------------------------------------------------------------
TParsedData* __fastcall TGPR_InfraRadar::ReceivedDataParser(signed char* Buf, int BufLength)
{
	int Sigma;
	int z, h, l, i, N, BV, dBV;
	bool ZeroLine;
	double value;
	TParsedTraceData *Out;

	Sigma=0;
	ZeroLine=true;
	z=0;
	i=0;
	if(BufLength<=RxPacketSize) N=BufLength;
	else N=RxPacketSize;
	Out=new TParsedTraceData(RxSamples, (void*)this);
	while(z<N)
	{
		h=(int)Buf[z];
		l=(unsigned char)Buf[z+1];
		if(i<RealSamples)
		{
			value=(int)((h<<BitCount)+l);
			if(value>PositiveMax) value=PositiveMax;
			else if(value<NegativeMax) value=NegativeMax;
			Out->RxData[i]=value;
			if(i>=SigmaSamples)	Sigma=Sigma+Out->RxData[i];
			if(Out->RxData[i]!=0) ZeroLine=false;
		}
		else
		{
			if(i<(RealSamples+8)) SpaceBuf[i-RealSamples]=(h<<BitCount)+l;
			else SpaceBuf[i-RealSamples]=(unsigned char)h;
		}
		i++;
		z+=2;
	}
	if(ZeroLine)
	{
		delete Out;
		Out=NULL;
	}
	else if(SkipedTQ>=40)
	{
		// Constant deduction coefficient
		Out->dS=(float)Sigma/(float)SigmaSamples;
		Out->dS*=ApplyConstantDeduct;
		// Channel Number
// ChannelNumber is allways 0 for InfraRadar
		Out->ChannelNumber = 0;
		// Wheel pulses
		BV=SpaceBuf[SpaceCenter-RealSamples];
		if(OldBV>BV) BV+=256;
		dBV=(BV-OldBV);
		if(dBV>128 && dBV>Old_dBV*2) dBV=Old_dBV;
		Out->WheelPulses=dBV;
		if(BV>255) OldBV=BV-256;
		else OldBV=BV;
		Old_dBV=dBV;
		// Backward direction
		if(SpaceBuf[SpaceBufSize-1]==255) Out->WheelPulses*=(-1);
		// Battery value
		Out->BatteryValue=SpaceBuf[4];
	}
	else
	{
		SkipedTQ++;
		OldBV=SpaceBuf[SpaceCenter-RealSamples];
		delete Out;
		Out=NULL;
	}

	return (TParsedData*)Out;
}
//----------------------------------------------------------------------------

void __fastcall TGPR_InfraRadar::ApplySamples(bool Force)
{
	if(Force || SamplesChanged)
	{
		oldsamples=samples;
		rxsamples=samples+(samples >> 4);
		rxpacketsize=rxsamples*sizeof(short);
		realsamples=samples;
		spacecenter=realsamples+(realsamples >> 5);
		sigmasamples=realsamples >> 1;
		spacequart=rxsamples-(realsamples >> 6);
		SpaceBufSize=rxsamples-realsamples;
		if(SpaceBuf) delete[] SpaceBuf;
		SpaceBuf=new short[SpaceBufSize];
		if(Channels)
			for(int i=0; i<ChannelQuantity; i++)
				if(Channels[i]) Channels[i]->RealSamples=realsamples;
		SamplesChanged=false;
	}
}

//----------------------------------------------------------------------------
// TPartialTrace
//----------------------------------------------------------------------------
__fastcall TPartialTrace::TPartialTrace()
{
	completed=false;
	parts=samples=buflength=0;
	bps=bpsNone;
	traceparts=NULL;
}

//----------------------------------------------------------------------------
__fastcall TPartialTrace::~TPartialTrace()
{
	Clear();
	delete[] traceparts;
	traceparts=NULL;
}

//----------------------------------------------------------------------------

void __fastcall TPartialTrace::AddTracePart(TTracePart *tp)
{
	int i;

	if(tp)
	{
		if(!completed && tp->Parts==parts && tp->Samples==samples &&
			tp->BpS==bps && !traceparts[tp->Part])
				traceparts[tp->Part-1]=tp;
		else
		{
			Clear();
			if(!traceparts || parts!=tp->Parts)
			{
				if(traceparts) delete[] traceparts;
				traceparts=new TTracePart*[tp->Parts];
				for(int i=0; i<tp->Parts; i++)
					traceparts[i]=NULL;
			}
			parts=tp->Parts;
			samples=tp->Samples;
			bps=tp->BpS;
			buflength=samples*(int)bps;
			traceparts[tp->Part-1]=tp;
		}
		for(i=0; i<parts; i++)
			if(!traceparts[i]) break;
		completed=(i==parts);
	}
}
//----------------------------------------------------------------------------

bool __fastcall TPartialTrace::GetTraceData(char *Data)
{
	bool res=false;
	int p;

	if(completed)
	{
		p=0;
		try
		{
			res=true;
			for(int i=0; i<parts; i++)
			{
				if(traceparts[i])
				{
					memcpy((char*)(Data+p), traceparts[i]->Buf, traceparts[i]->BufLength);
					p=+traceparts[i]->BufLength;
				}
				else
				{
					res=false;
					break;
				}
			}
		}
		catch(...) {res=false;}
	}

	return res;
}

//----------------------------------------------------------------------------
// TGPR_Zynq
//----------------------------------------------------------------------------
__fastcall TGPR_Zynq::TGPR_Zynq(AnsiString fn, TRegime RG, TObject* Manager, TRadarType RT)
	:TGPRUnit(fn, RT, RG, Manager)
{
	name="ZYNQ_0";
	BatteryType=btLithium;
	Battery=false;
	crc=false;

	COMMAND_START = '$';
	COMMAND_ID_CH = '#';
	COMMAND_CRC_CH = '*';
	INVALID_COMMAND = "invalid";
	ID_SIZE = 6;
	PARAM_COUNT = 4;
	PARAM_SIZE = 32;
	COMMAND_NAME_SIZE = 8;
	DATA_MAX_SIZE = 2048;//1024;
	CRC_SIZE = 2;
	MAX_COMMAND_SIZE = (COMMAND_NAME_SIZE + CRC_SIZE) * 2 + ID_SIZE +
		PARAM_COUNT * (PARAM_SIZE + 3) + DATA_MAX_SIZE + (8 + CRC_SIZE);
	command_buf = new char[MAX_COMMAND_SIZE];
	cmd_i=0;
	TracePart=NULL;
	PartialTrace=NULL;
}
//----------------------------------------------------------------------------

bool TGPR_Zynq::IsBufDigitsOnly(AnsiString buf, bool Hex) // Hex number has to start from '0x'
{
	int j = 1;
	bool res = true;

	while(j<=buf.Length() && res)
	{
		if(buf[j] < 0x30) res = false;
		else if(buf[j] > 0x39) // check if for ASCII digits (from 1 to 64K or HEX in format 0xXXXX, uint16
		{
			if(Hex && buf.Length() >= 3 && buf[1] == '0' && buf[2] == 'x')
			{
				if(j > 2 && (buf[j] < 'a' || buf[j] > 'f')) res = false;
			}
			else res = false;
		}
		j++;
	}
	if(j == 1) res = false;

	return res;
}
//----------------------------------------------------------------------------

unsigned char TGPR_Zynq::Crc8(unsigned char *pcBlock, unsigned char len)
{
	unsigned char crc = 0xFF;
	const unsigned char Crc8Table[256] = {
		0x00, 0x31, 0x62, 0x53, 0xC4, 0xF5, 0xA6, 0x97,
		0xB9, 0x88, 0xDB, 0xEA, 0x7D, 0x4C, 0x1F, 0x2E,
		0x43, 0x72, 0x21, 0x10, 0x87, 0xB6, 0xE5, 0xD4,
		0xFA, 0xCB, 0x98, 0xA9, 0x3E, 0x0F, 0x5C, 0x6D,
		0x86, 0xB7, 0xE4, 0xD5, 0x42, 0x73, 0x20, 0x11,
		0x3F, 0x0E, 0x5D, 0x6C, 0xFB, 0xCA, 0x99, 0xA8,
		0xC5, 0xF4, 0xA7, 0x96, 0x01, 0x30, 0x63, 0x52,
		0x7C, 0x4D, 0x1E, 0x2F, 0xB8, 0x89, 0xDA, 0xEB,
		0x3D, 0x0C, 0x5F, 0x6E, 0xF9, 0xC8, 0x9B, 0xAA,
		0x84, 0xB5, 0xE6, 0xD7, 0x40, 0x71, 0x22, 0x13,
		0x7E, 0x4F, 0x1C, 0x2D, 0xBA, 0x8B, 0xD8, 0xE9,
		0xC7, 0xF6, 0xA5, 0x94, 0x03, 0x32, 0x61, 0x50,
		0xBB, 0x8A, 0xD9, 0xE8, 0x7F, 0x4E, 0x1D, 0x2C,
		0x02, 0x33, 0x60, 0x51, 0xC6, 0xF7, 0xA4, 0x95,
		0xF8, 0xC9, 0x9A, 0xAB, 0x3C, 0x0D, 0x5E, 0x6F,
		0x41, 0x70, 0x23, 0x12, 0x85, 0xB4, 0xE7, 0xD6,
		0x7A, 0x4B, 0x18, 0x29, 0xBE, 0x8F, 0xDC, 0xED,
		0xC3, 0xF2, 0xA1, 0x90, 0x07, 0x36, 0x65, 0x54,
		0x39, 0x08, 0x5B, 0x6A, 0xFD, 0xCC, 0x9F, 0xAE,
		0x80, 0xB1, 0xE2, 0xD3, 0x44, 0x75, 0x26, 0x17,
		0xFC, 0xCD, 0x9E, 0xAF, 0x38, 0x09, 0x5A, 0x6B,
		0x45, 0x74, 0x27, 0x16, 0x81, 0xB0, 0xE3, 0xD2,
		0xBF, 0x8E, 0xDD, 0xEC, 0x7B, 0x4A, 0x19, 0x28,
		0x06, 0x37, 0x64, 0x55, 0xC2, 0xF3, 0xA0, 0x91,
		0x47, 0x76, 0x25, 0x14, 0x83, 0xB2, 0xE1, 0xD0,
		0xFE, 0xCF, 0x9C, 0xAD, 0x3A, 0x0B, 0x58, 0x69,
		0x04, 0x35, 0x66, 0x57, 0xC0, 0xF1, 0xA2, 0x93,
		0xBD, 0x8C, 0xDF, 0xEE, 0x79, 0x48, 0x1B, 0x2A,
		0xC1, 0xF0, 0xA3, 0x92, 0x05, 0x34, 0x67, 0x56,
		0x78, 0x49, 0x1A, 0x2B, 0xBC, 0x8D, 0xDE, 0xEF,
		0x82, 0xB3, 0xE0, 0xD1, 0x46, 0x77, 0x24, 0x15,
		0x3B, 0x0A, 0x59, 0x68, 0xFF, 0xCE, 0x9D, 0xAC
	};
	while (len--)
		crc = Crc8Table[crc ^ *pcBlock++];

	return crc;
}
//----------------------------------------------------------------------------

bool TGPR_Zynq::parse_command(char *buf, int N, AnsiString &name, AnsiString &id, AnsiString *params, char *data, int *datasize)
{
	bool res;
	int i, j, d;
	bool nextParam=false, commas = false;
	char comma;

	if(crc) N -= (CRC_SIZE +1);
	if(buf[0] == COMMAND_START)
	{
		res = true;
		// parse Name of the command
		d = 0;
		i = 1; //0 - COMMAND_START '$'
		name = "";
		while(i < N && name.Length() < COMMAND_NAME_SIZE && buf[i] != ' ')
			name += buf[i++];
		if(i < N && buf[i] != ' ') res = false;
		else //valid Name
		{
			i++;
			id = "";
			if(i < N && buf[i] == '#') // parse ID of the command
			{
				i++;
				while(i < N && id.Length() < ID_SIZE && buf[i] != ' ')
					id += buf[i++];
				if(i < N && buf[i] != ' ') res = false;
				else //valid ID
				{
					if(!IsBufDigitsOnly(id, true)) res = false;
					i++;
				}
			}
			if(name == INVALID_COMMAND) return res;
		}
		j = 0;
		params[j] = "";
		while(res && i < N && j < PARAM_COUNT-1)
		{
			if(buf[i] == '"' || buf[i] == '\'')
			{
				if(commas)
				{
					if(comma != buf[i])
					{
						//params[j][k++] = buf[i];
						if(params[j].Length() < PARAM_SIZE) params[j] += buf[i];
						if(commas && d < DATA_MAX_SIZE) data[d++] = buf[i];
					}
					else
					{
						commas = false;
						nextParam = true;
						i++;
					}
				}
				else if(params[j].Length() == 0)
				{
					commas = true;
					comma = buf[i];
					d=0;
				}
				else res = false;
			}
			else if(!commas && buf[i] == ' ') nextParam = true;
			else
			{
				if(params[j].Length() < PARAM_SIZE) params[j] += buf[i];
				if(commas && d < DATA_MAX_SIZE) data[d++] = buf[i];
			}
			i++;
			if((nextParam || i >= N) && params[j].Length()>0)
			{
				if(d > 0)
				{
					data[d] = 0;
					*datasize = d;
				}
				nextParam=false;
				j++;
				if(j < PARAM_COUNT) params[j] = "";
			}
			else if(!commas && params[j].Length() >= PARAM_SIZE) res = false;
			else if(commas && d >= DATA_MAX_SIZE) res = false;
		}
		if(res)
		{
			if(i < N && j == PARAM_COUNT-1)
			{
				d = 0;
				if(buf[i] == '"' || buf[i] == '\'')
				{
					commas = true;
					comma = buf[i];
					data[d++] = buf[i];
					i++;
				}
				else commas = false;
				while(res && i < N)
				{
					if(!commas || (commas && buf[i] != comma))
					{
						if(params[j].Length() < PARAM_SIZE) params[j] += buf[i];
						if(d < DATA_MAX_SIZE) data[d++] = buf[i];
						else res = false;
					}
					else if(d < DATA_MAX_SIZE) data[d++] = buf[i];
					else res = false;
					i++;
				}
				if(i < N) res = false;
				else
				{
					data[d] = 0;
					*datasize = d;
				}
			}
		}
	}
	else res = false;
	if(CRC && res)
	{
		AnsiString crc_buf;
		int v;

		if(i == N && buf[N] == COMMAND_CRC_CH)
		{
			while(res && crc_buf.Length() < CRC_SIZE)
			{
				if((buf[i] >= '0' && buf[i] <= '9') ||
					(buf[i] >= 'a' && buf[i] <= 'f'))
						crc_buf += buf[i++];
				else res = false;
			}
			if(res)
			{
				v = HexToInt(crc_buf);
				if(v != Crc8((unsigned char*)buf, i)) res = false;
			}
		}
	}
	return res;
}
//----------------------------------------------------------------------------

TParsedData* __fastcall TGPR_Zynq::ReceivedDataParser(signed char* Buf, int BufLength)
{
	TParsedTraceData* Out=NULL;
	AnsiString Name, Id, *Params;
	char *Data;
	int DataSize, i, p, ps, pp, sm, n;
	short *RxData;
	TBytesPerSample bps;

	if(rxpacketsize>1)
	{
		if(TracePart)
		{
			TracePart->FillBuf(Buf);
			if(!PartialTrace) PartialTrace=new TPartialTrace();
			if(PartialTrace)
			{
				PartialTrace->AddTracePart(TracePart);
				TracePart=NULL;
				if(PartialTrace->Completed)
				{
					Out=new TParsedTraceData(PartialTrace->Samples, (void*)this);
					RxData=new short[PartialTrace->Samples];
					try
					{
						PartialTrace->GetTraceData((char*)RxData);
						for(i=0; i<PartialTrace->Samples; i++)
							Out->RxData[i]=(double)RxData[i];
					}
					__finally
					{
						delete[] RxData;
						delete PartialTrace;
						PartialTrace=NULL;
					}
				}
			}
		}
		rxpacketsize=1;
	}
	else
	{
		if(BufLength > 0)
		{
			i = 0;
			while(i < BufLength)
			{
				if(cmd_i > 0 && Buf[i] == '\n')// && command_buf[cmd_i-1] == '\r') // end of the line
				{
					if(command_buf[cmd_i-1] == '\r')
					{
					cmd_i--;
					command_buf[cmd_i] = 0;
					//if(i < BufLength - 1) recv_rest = (char*)(Buf + i + 1);
					//else recv_rest = NULL;
					//zz = z = n - i -1;
					Params=new AnsiString[PARAM_COUNT];
					Data=new char[DATA_MAX_SIZE];
					try
					{
						if(parse_command(command_buf, cmd_i, Name, Id, Params, Data, &DataSize))
						{
							if(Name=="trace")
							{
								try
								{
									if(Params[0]!=NULL && Params[0].Pos("_")>1)
									{
										pp=Params[0].Pos("_");
										p=StrToInt(Params[0].SubString(1, pp-1));
										ps=StrToInt(Params[0].SubString(pp+1, Params[0].Length()-pp));
										sm=StrToInt(Params[1]);
										bps=(TBytesPerSample)StrToInt(Params[2]);
										n=StrToInt(Params[3]);
										if(p>0 && ps>0 && p<=ps && sm>0 && bps!=bpsNone && n>0)
										{
											if(TracePart) delete TracePart;
											TracePart=new TTracePart(p, ps, sm, bps, n);
											rxpacketsize=n;
										}
									}
									else
									{
										p=ps=sm=n=0;
										bps=bpsNone;
									}
								}
								catch(...) {}
							}
						}
					}
					__finally
					{
						delete[] Params;
						delete[] Data;
					}
					cmd_i = 0;
					}
				}
				else
				{
					if(cmd_i > 0 || (cmd_i == 0 && Buf[i] == COMMAND_START))
						command_buf[cmd_i++] = Buf[i];
				}
				i++;
			}
		}
    }

	return (TParsedData*)Out;
}
//----------------------------------------------------------------------------

void __fastcall TGPR_Zynq::ApplySamples(bool Force)
{
	if(Force || SamplesChanged)
	{
		oldsamples=samples;
		rxsamples=samples;
		realsamples=samples;
		spacecenter=0;
		sigmasamples=realsamples >> 1;
		spacequart=0;
		SpaceBufSize=0;
		if(SpaceBuf) delete[] SpaceBuf;
		SpaceBuf=NULL;
		if(Channels)
			for(int i=0; i<ChannelQuantity; i++)
				if(Channels[i]) Channels[i]->RealSamples=realsamples;
		SamplesChanged=false;
		rxpacketsize=1;	//rxpacketsize=rxsamples*sizeof(short);
	}
}
//---------------------------------------------------------------------------

TCommand __fastcall TGPR_Zynq::GetStartCommand()
{
	TCommand cmd;

	cmd.Cmd = (AnsiString)COMMAND_START + "start";
	cmd.NeedSynchroReply = false; //false;
	cmd.CommandReplyLength = 0;//1;//0;
	cmd.ByteCommand = false;

	return cmd;
}
//---------------------------------------------------------------------------

TCommand __fastcall TGPR_Zynq::GetStopCommand()
{
	TCommand cmd;

	cmd.Cmd = (AnsiString)COMMAND_START + "stop";
	cmd.NeedSynchroReply = false; //false;
	cmd.CommandReplyLength = 0;//1;//0;
	cmd.ByteCommand = false;

	return cmd;
}
//---------------------------------------------------------------------------

TCommand __fastcall TGPR_Zynq::GetInvalidCommand()
{
	TCommand cmd;

	cmd.Cmd = (AnsiString)COMMAND_START + INVALID_COMMAND;
	cmd.NeedSynchroReply = false; //false;
	cmd.CommandReplyLength = 0;//1;//0;
	cmd.ByteCommand = false;

	return cmd;
}
//---------------------------------------------------------------------------

