//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "PalettesRepository.h"
#include "Palette.h"
#include "Defines.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "ImageButton"
#pragma link "G32_ProgressBar"
#pragma link "GR32_Image"
#pragma resource "*.dfm"
TPalettesForm *PalettesForm;

//---------------------------------------------------------------------------
__fastcall TPalettesForm::TPalettesForm(TComponent* Owner)
	: TForm(Owner)
{
	PaletteUnits=new TPaletteUnit[PaletteMaxUnits];
	PaletteUnits[0]=TPaletteUnit(1, PalettePanel1, PaletteImage1, StaticText1);
	PaletteUnits[1]=TPaletteUnit(2, PalettePanel2, PaletteImage2, StaticText2);
	PaletteUnits[2]=TPaletteUnit(3, PalettePanel3, PaletteImage3, StaticText3);
	PaletteUnits[3]=TPaletteUnit(4, PalettePanel4, PaletteImage4, StaticText4);
	PaletteUnits[4]=TPaletteUnit(5, PalettePanel5, PaletteImage5, StaticText5);
	PaletteUnits[5]=TPaletteUnit(6, PalettePanel6, PaletteImage6, StaticText6);
	PaletteUnits[6]=TPaletteUnit(7, PalettePanel7, PaletteImage7, StaticText7);
	PaletteUnits[7]=TPaletteUnit(8, PalettePanel8, PaletteImage8, StaticText8);
}
//---------------------------------------------------------------------------

void __fastcall TPalettesForm::BackgroundMouseMove(TObject *Sender, TShiftState Shift,
		  int X, int Y)
{
	Types::TPoint p=Types::TPoint(X, Y);

	for(int i=0; i<PaletteMaxUnits; i++)
	{
		PaletteUnits[i].Selected=(PaletteUnits[i].Panel && PaletteUnits[i].Panel->BoundsRect.Contains(p));
	}
}
//---------------------------------------------------------------------------

void __fastcall TPalettesForm::PaletteImage1MouseMove(TObject *Sender, TShiftState Shift,
		  int X, int Y, TCustomLayer *Layer)
{
	int i;

	if(Sender)
	{
		try
		{
			i=((TComponent*)Sender)->Tag-1;
			if(PaletteUnits[i].Panel && PaletteUnits[i].Image)
			{
				BackgroundMouseMove(Sender, Shift,
					X+PaletteUnits[i].Image->Left+PaletteUnits[i].Panel->Left,
					Y+PaletteUnits[i].Image->Top+PaletteUnits[i].Panel->Top);
			}
		}
		catch(Exception &e) {}
	}
}
//---------------------------------------------------------------------------

void __fastcall TPalettesForm::FormShow(TObject *Sender)
{
	TPalette* plt;

	for(int i=0; i<PaletteMaxUnits; i++)
	{
		if(PaletteUnits[0].Image)
		{
			plt=new TPalette();
			try
			{
				try
				{
					plt->LoadPaletteFromRegistry((AnsiString)((AnsiString)_RadarMapRegKey+"\\Palettes").c_str(), "Palette"+IntToStr(i+1));
					plt->DrawPalette(PaletteUnits[i].Image->Bitmap, PaletteUnits[i].Image->Width,
						PaletteUnits[i].Image->Height, 2, false);
				}
				catch(Exception &e) {}
			}
			__finally
			{
				delete plt;
			}
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TPalettesForm::PaletteImage1Click(TObject *Sender)
{
	if(Sender)
	{
		try
		{
			ModalResult=100+((TComponent*)Sender)->Tag;
		}
		catch(Exception &e) {}
	}
}
//---------------------------------------------------------------------------

void __fastcall TPalettesForm::CloseIButtonClick(TObject *Sender)
{
	ModalResult=mrCancel;
}

void __fastcall TPalettesForm::OkImageButtonClick(TObject *Sender)
{
	ModalResult=mrOk;
}
//---------------------------------------------------------------------------

void __fastcall TPalettesForm::TitlePanelMouseDown(TObject *Sender, TMouseButton Button,
		  TShiftState Shift, int X, int Y)
{
	TitleDown=true;
	TitleXY=ClientToScreen(Types::TPoint(X, Y));
}
//---------------------------------------------------------------------------

void __fastcall TPalettesForm::TitlePanelMouseMove(TObject *Sender, TShiftState Shift,
		  int X, int Y)
{
	Types::TPoint tmp;

	if(TitleDown)
	{
		tmp=ClientToScreen(Types::TPoint(X, Y));
		Left+=tmp.x-TitleXY.x;
		Top+=tmp.y-TitleXY.y;
		TitleXY=tmp;
	}
}
//---------------------------------------------------------------------------

void __fastcall TPalettesForm::TitlePanelMouseUp(TObject *Sender, TMouseButton Button,
		  TShiftState Shift, int X, int Y)
{
	TitleDown=false;
}
//---------------------------------------------------------------------------

