//---------------------------------------------------------------------------

#ifndef LoadPluginsH
#define LoadPluginsH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <Graphics.hpp>
#include <pngimage.hpp>
#include <ComCtrls.hpp>
#include <ImgList.hpp>
#include <ImageButton.h>
#include "CheckBoxTreeView.h"
#include "PlugIns.h"
//---------------------------------------------------------------------------
class TLoadPluginsForm : public TForm
{
__published:	// IDE-managed Components
	TPanel *Panel1;
	TPanel *Panel2;
	TPanel *Panel3;
	TPanel *Panel4;
	TPanel *ClientPanel;
	TPanel *TitlePanel;
	TLabel *Label1;
	TPanel *Panel8;
	TPanel *Panel9;
	TImageButton *CloseIButton;
	TPanel *Panel5;
	TPanel *Panel6;
	TPanel *Panel7;
	TPanel *PagesPanel;
	TLabel *Label3;
	TLabel *Label108;
	TPanel *OkCancelPanel;
	TImageButton *CancelImageButton;
	TImageButton *OkImageButton;
	TEdit *TempEdit;
	TPanel *XPanel;
	TLabel *GpsUnitLabel;
	TLabel *Label133;
	TLabel *Label2;
	TLabel *Label4;
	TLabel *Label5;
	TImage *Image1;
	TLabel *Label120;
	TImage *Image15;
	TLabel *Label6;
	TImage *Image2;
	TLabel *Label7;
	TImage *Image14;
	TLabel *Label121;
	TTreeView *TreeView1;
	TStaticText *CopyrightLabel;
	TStaticText *VersionLabel;
	TStaticText *DescriptionLabel;
	TImageList *ImageList2;
	TImageList *ImageList1;
	TImageButton *CopyCheckBox;
	void __fastcall OkImageClick(TObject *Sender);
	void __fastcall CancelImageClick(TObject *Sender);
	void __fastcall CopyCheckBoxClick(TObject *Sender);
	void __fastcall Label108Click(TObject *Sender);
	void __fastcall TitlePanelMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
	void __fastcall TitlePanelMouseMove(TObject *Sender, TShiftState Shift, int X, int Y);
	void __fastcall TitlePanelMouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
	void __fastcall FormShow(TObject *Sender);

private:	// User declarations
	bool TitleDown;
	Types::TPoint TitleXY;
public:		// User declarations
	__fastcall TLoadPluginsForm(TComponent* Owner);
	__fastcall ~TLoadPluginsForm();
	void __fastcall OutputPlugIns(AnsiString FileName, TPlugInType NeededType=pitUnknown);
	TCheckBoxTreeView *TreeView;
};
//---------------------------------------------------------------------------
extern PACKAGE TLoadPluginsForm *LoadPluginsForm;
//---------------------------------------------------------------------------
#endif
