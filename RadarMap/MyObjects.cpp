//---------------------------------------------------------------------------

#pragma hdrstop

#include "MyObjects.h"
#include "VisualToolObjects.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

//---------------------------------------------------------------------------
// TMyObjectsList
//---------------------------------------------------------------------------
__fastcall TMyObjectsList::TMyObjectsList(TMyObject *AOwner)
{
	Owner=AOwner;
	BusyLocker=new TBusyLocker();
	Objects=new TList();
	linked=false;
	ObjectsReIndexing=true;
	deleteitemowneracknolegment=false;
}
//---------------------------------------------------------------------------

__fastcall TMyObjectsList::~TMyObjectsList()
{
	BusyLocker->UnLock();
	Clear();
	delete Objects;
	delete BusyLocker;
}
//---------------------------------------------------------------------------

int __fastcall TMyObjectsList::AddMyObject(TMyObject* o)
{
	int i=-1;

	//BusyLocker->SequentLock();
	try
	{
		try
		{
			if(o!=NULL)
			{
				i=Objects->Add(o);
				if(ObjectsReIndexing) o->Index=i;
			}
		}
		catch(...) {}
	}
	__finally
	{
		//BusyLocker->UnLock();
	}
	return i;
}
//---------------------------------------------------------------------------

void __fastcall TMyObjectsList::ArrangeIndexes()
{
	//BusyLocker->SequentLock();
	if(ObjectsReIndexing)
	{
		try
		{
			try
			{
				for(int i=0; i<Objects->Count; i++)
				{
					((TMyObject*)Objects->Items[i])->Index=i;
				}
			}
			catch(...) {}
		}
		__finally
		{
			//BusyLocker->UnLock();
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TMyObjectsList::Delete(int Index)
{
	TMyObject *po;

	//BusyLocker->SequentLock();
	try
	{
		try
		{
			if(Index>=0 && Index<Objects->Count)
			{
				po=(TMyObject*)Objects->Items[Index];
				if(po && FreeObjects)
				{
					if(ObjectsReIndexing && !deleteitemowneracknolegment) po->Index=-1;
					delete (TMyObject*)po;
				}
				Objects->Delete(Index);
				ArrangeIndexes();
			}
		}
		catch(...) {}
	}
	__finally
	{
		//BusyLocker->UnLock();
	}
}
//---------------------------------------------------------------------------

void __fastcall TMyObjectsList::Remove(int Index)
{
	//TMyObject *po;
	//BusyLocker->SequentLock();
	try
	{
		try
		{
			if(Index>=0 && Index<Objects->Count)
			{
				Objects->Delete(Index);
				ArrangeIndexes();
			}
		}
		catch(...) {}
	}
	__finally
	{
		//BusyLocker->UnLock();
	}
}
//---------------------------------------------------------------------------

void __fastcall TMyObjectsList::Clear(TMyObject *PB)
{
	TMyObject *po;
	TToolProgressBar *progress=(TToolProgressBar*)PB;

	if(BusyLocker->Lock())
	{
		try
		{
			try
			{
				if(Objects!=NULL)
				{
					try
					{
						if(FreeObjects)
						{
							for(int i=0; i<Objects->Count; i++)
							{
								po=(TMyObject*)Objects->Items[i];
								if(po)
								{
									if(ObjectsReIndexing && !deleteitemowneracknolegment) po->Index=-1;
									delete po;
								}
								if(progress) progress->StepIt();
							}
						}
					}
					__finally
					{
						Objects->Clear();
					}
				}
			}
			catch(...) {}
		}
		__finally
		{
			BusyLocker->UnLock();
		}
	}
}
//---------------------------------------------------------------------------

int __fastcall TMyObjectsList::SendToBack(int Index)
{
	TMyObject* po;
	int res=-1;

	//BusyLocker->SequentLock();
	try
	{
		try
		{
			if(Index>=0 && Index<Objects->Count && Index!=0)
			{
				po=(TMyObject*)(Objects->Items[Index]);
				for(int i=Index; i>0; i--)
					Objects->Items[i]=Objects->Items[i-1];
				Objects->Items[0]=(void*)po;
				ArrangeIndexes();
				if(po) res=po->Index;
			}
		}
		catch(...) {}
	}
	__finally
	{
		//BusyLocker->UnLock();
	}

	return res;
}
//---------------------------------------------------------------------------

int __fastcall TMyObjectsList::BringToFront(int Index)
{
	TMyObject* po;
	int res=-1;

	//BusyLocker->SequentLock();
	try
	{
		try
		{
			if(Index>=0 && Index<Objects->Count && Index!=Objects->Count-1)
			{
				po=(TMyObject*)(Objects->Items[Index]);
				for(int i=Objects->Count-1; i>Index; i++)
					Objects->Items[i-1]=Objects->Items[i];
				Objects->Items[Objects->Count-1]=(void*)po;
				ArrangeIndexes();
				if(po) res=po->Index;
			}
		}
		catch(...) {}
	}
	__finally
	{
		//BusyLocker->UnLock();
	}

	return res;
}
//---------------------------------------------------------------------------

int __fastcall TMyObjectsList::OneStepBack(int Index)
{
	TMyObject* po;
	int res=-1;

	//BusyLocker->SequentLock();
	try
	{
		try
		{
			if(Index>=0 && Index<Objects->Count && Index!=0)
			{
				po=(TMyObject*)(Objects->Items[Index]);
				Objects->Items[Index]=Objects->Items[Index-1];
				Objects->Items[Index-1]=(void*)po;
				ArrangeIndexes();
				if(po) res=po->Index;
			}
		}
		catch(...) {}
	}
	__finally
	{
		//BusyLocker->UnLock();
	}
	return res;
}
//---------------------------------------------------------------------------

int __fastcall TMyObjectsList::OneStepForward(int Index)
{
	TMyObject* po;
	int res=-1;

	//BusyLocker->SequentLock();
	try
	{
		try
		{
			if(Index>=0 && Index<Objects->Count && Index!=Objects->Count-1)
			{
				po=(TMyObject*)(Objects->Items[Index]);
				Objects->Items[Index]=Objects->Items[Index+1];
				Objects->Items[Index+1]=(void*)po;
				ArrangeIndexes();
				if(po) res=po->Index;
			}
		}
		catch(...) {}
	}
	__finally
	{
		//BusyLocker->UnLock();
	}
	return res;
}
//---------------------------------------------------------------------------

int __fastcall TMyObjectsList::readObjectsCount()
{
	int res=-1;

	if(BusyLocker->Lock())
	{
		try
		{
			try
			{
				res=Objects->Count;
			}
			catch(...) {}
		}
		__finally
		{
			BusyLocker->UnLock();
		}
	}
	return res;
}
//---------------------------------------------------------------------------

TMyObject* __fastcall TMyObjectsList::readMyObjectItem(int Index)
{
	TMyObject* res=NULL;

	if(BusyLocker->Lock())
	{
		try
		{
			try
			{
				if(Index>=0 && Index<Objects->Count) res=(TMyObject*)Objects->Items[Index];
				else res=NULL;
			}
			catch(...) {}
		}
		__finally
		{
			BusyLocker->UnLock();
		}
	}
	return res;
}
//---------------------------------------------------------------------------

void __fastcall TMyObjectsList::writeMyObjectItem(int Index, TMyObject* value)
{
	if(BusyLocker->Lock())
	{
		try
		{
			try
			{
				if(Index>=0 && Index<Objects->Count) Objects->Items[Index]=(TObject*)value;
			}
			catch(...) {}
		}
		__finally
		{
			BusyLocker->UnLock();
		}
	}
}
//---------------------------------------------------------------------------

int __fastcall TMyObjectsList::IndexOf(TMyObject* o)
{
	int i=-1;

	//BusyLocker->SequentLock();
	try
	{
		try
		{
			if(Objects) i=Objects->IndexOf(o);
		}
		catch(...) {}
	}
	__finally
	{
		//BusyLocker->UnLock();
	}
	return i;
}
//---------------------------------------------------------------------------

int __fastcall TMyObjectsList::Add(TMyObject* o)
{
	int res=-1;

	//BusyLocker->SequentLock();
	try
	{
		try
		{
			res=AddMyObject(o);
		}
		catch(...) {}
	}
	__finally
	{
		//BusyLocker->UnLock();
	}
	return res;
}

//---------------------------------------------------------------------------
// TMyObjectsNamedList
//---------------------------------------------------------------------------
int __fastcall TMyObjectsNamedList::MoveNames(int oldi, int newi)
{
	AnsiString str;

	if(oldi>0 && oldi<Strings->Count && oldi!=newi &&
		newi>=0 && newi<Strings->Count)
	{
		str=Strings->Strings[oldi];
		if(oldi>newi)
		{
			for(int j=oldi; j>newi; j--)
				Strings->Strings[j]=Strings->Strings[j-1];
		}
		else
		{
			for(int j=newi; j>oldi; j++)
				Strings->Strings[j-1]=Strings->Strings[j];
		}
		Strings->Strings[newi]=str;
		return newi;
	}
	else return -1;
}
//---------------------------------------------------------------------------

int __fastcall TMyObjectsNamedList::IndexOf(AnsiString AName)
{
	int i;

	i=Strings->IndexOf(AName);
	return i;
}
