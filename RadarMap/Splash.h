//---------------------------------------------------------------------------

#ifndef SplashH
#define SplashH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <jpeg.hpp>
#include <Graphics.hpp>
#include <ComCtrls.hpp>
#include <G32_ProgressBar.hpp>
#include <GR32_Image.hpp>
#include <pngimage.hpp>
//---------------------------------------------------------------------------
const int AbyrvalgY1 = 783;
const int AbyrvalgM1 = 7;

class TSplashForm : public TForm
{
__published:	// IDE-managed Components
	TImage *Image1;
	TLabel *Label1;
	TG32_ProgressBar *PBar;
	TLabel *TrialLabel;
private:	// User declarations
public:		// User declarations
	__fastcall TSplashForm(TComponent* Owner);
};

const int AbyrvalgD1 = 8;
//---------------------------------------------------------------------------
extern PACKAGE TSplashForm *SplashForm;
//---------------------------------------------------------------------------
#endif
