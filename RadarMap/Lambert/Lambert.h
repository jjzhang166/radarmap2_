#ifndef LambertH
#define LambertH

#define UseStaticDLL
//#undef UseStaticDLL

namespace Lambert72
{
#ifdef UseStaticDLL
	extern "C" _declspec(dllimport) void __stdcall Lambert72ToGeoETRS89(double xi,
		double yi, double Hi, double *lat, double *lon, double *height);
	extern "C" _declspec(dllimport) void __stdcall GeoETRS89ToLambert72(double lat,
		double lon, double height, double *xo, double *yo, double *Ho);
#else
    void __stdcall GeoETRS89ToLambert72(double lat,	double lon, double height, double &xo, double &yo, double &Ho)
	{
		double x, y, h;
		void (*LatLonAlt2XYH) (double lat, double lon, double height, double *xo, double *yo, double *Ho);
		bool res=true;
		HINSTANCE hInst;

		try
		{
			hInst=LoadLibrary("Lambert_ETRS89.dll");
			//LatLonAlt2XYH=(void (*) (double lat, double lon, double height, double *xo, double *yo, double *Ho))
			//	GetProcAddress(GetModuleHandle(TEXT("Lambert_ETRS89.dll")), "GeoETRS89ToLambert72");
			if(hInst)
			{
				try
				{
					try
					{
						LatLonAlt2XYH=(void (*) (double lat, double lon, double height, double *xo, double *yo, double *Ho))
							GetProcAddress(hInst, "GeoETRS89ToLambert72");
						if(LatLonAlt2XYH)
						{
							LatLonAlt2XYH(lat, lon, height, &x, &y, &h);
							xo=x; yo=y; Ho=h;
						}
						else res=false;
					}
					catch(Exception &e) {res=false;}
				}
				__finally
				{
					if(!FreeLibrary(hInst)) UnmapViewOfFile(hInst);
				}
			}
			else res=false;
		}
		catch(Exception &e) {res=false;}

		if(!res) xo=yo=Ho=0.;
	}

	void __stdcall Lambert72ToGeoETRS89(double xi, double yi, double Hi, double &lat, double &lon, double &height)
	{
		void (*XYH2LatLonAlt) (double xi, double yi, double Hi, double *lat, double *lon, double *height);
		double lat2, lon2, height2;
		bool res=true;
		HINSTANCE hInst;

		try
		{
			hInst=LoadLibrary("Lambert_ETRS89.dll");
			//XYH2LatLonAlt=(void (*) (double xi, double yi, double Hi, double *lat, double *lon, double *height))
			//	GetProcAddress(GetModuleHandle(TEXT("Lambert_ETRS89.dll")), "Lambert72ToGeoETRS89");
			if(hInst)
			{
				try
				{
					try
					{
						XYH2LatLonAlt=(void (*) (double xi, double yi, double Hi, double *lat, double *lon, double *height))
							GetProcAddress(hInst, "Lambert72ToGeoETRS89");
						if(XYH2LatLonAlt)
						{
							XYH2LatLonAlt(xi, yi, Hi, &lat2, &lon2, &height2);
							lat=lat2; lon=lon2; height=height2;
						}
						else res=false;
					}
					catch(Exception &e) {res=false;}
				}
				__finally
				{
					if(!FreeLibrary(hInst)) UnmapViewOfFile(hInst);
				}
			}
			else res=false;
		}
		catch(Exception &e) {res=false;}

		if(!res) lat=lon=height=0.;
	}
#endif
	/*extern "C" _declspec(dllimport) void __stdcall Lambert08ToGeoETRS89(double xi,
		double yi, double Hi, double *lat, double *lon, double *height);
	extern "C" _declspec(dllimport) void __stdcall GeoETRS89ToLambert08(double lat,
		double lon, double height, double *xo, double *yo, double *Ho);
	extern "C" _declspec(dllimport) void __stdcall Lambert72ToLambert08(double xi,
		double yi, double Hi, double *xo, double *yo, double *Ho);
	extern "C" _declspec(dllimport) void __stdcall Lambert08ToLambert72(double xi,
		double yi, double Hi, double *xo, double *yo, double *Ho);*/
}

#endif