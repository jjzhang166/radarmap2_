//---------------------------------------------------------------------------

#include "MainDll.h"
//---------------------------------------------------------------------------
/*
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.

 Copyright (C) 2014 Radar Systems, Inc.
 All Rights Reserved.
*/
//---------------------------------------------------------------------------

#pragma argsused
//---------------------------------------------------------------------------
// DLL Main entry function
//---------------------------------------------------------------------------
int WINAPI DllMain(HINSTANCE hinst, unsigned long reason, void* lpReserved)
{
	double val;
	bool bval;

	switch(reason)
	{
		case DLL_PROCESS_ATTACH:
			// Constructor of the current session
			sprintf(PosItems[posGPS].GUID, "{D9D439FE-D114-4730-8D62-79FC6829BC3A}");
			sprintf(PosItems[posGPS].Name, "GPS Odometer");
			PosItems[posGPS].OptionsCount=pokTotal;
			//val=0.01;
			//Options[posGPS][pokDX]=Option(otDouble64, "Trace Distance (dX)\0", sizeof(double), (char*)(&val), 0.01, 1000);
			bval=true;
			Options[posGPS][pokUseZ]=Option(otBool, "Use Altitude in distance calculation", sizeof(bool), (char*)(&bval), 0, 1);
			val=0.005;
			Options[posGPS][pokMinStep]=Option(otDouble64, "Distance validity threshold [m]\0", sizeof(double), (char*)(&val), 0, 1000);
			bval=true;
			Options[posGPS][pokSpeedPredict]=Option(otBool, "Speed prediction\0", sizeof(bool), (char*)(&bval), 0, 1);
			//Functions
			sprintf(PosItems[posGPS].StartAcquisitionFunctionName, "_StartAcquisition");
			sprintf(PosItems[posGPS].StopAcquisitionFunctionName, "_StopAcquisition");
			sprintf(PosItems[posGPS].GetPassedDistanceFunctionName, "_GetPassedDistance");
			break;
		case DLL_PROCESS_DETACH:
			// Destructor of the current session
			LastTickCount=GetTickCount64();
			LastSpeed=0;
			PassedTickCount=0;
			Started=false;
			LastPoint.X=0;
			LastPoint.Y=0;
			LastPoint.Z=0;
			break;
	}
	return 1;
}

//---------------------------------------------------------------------------
// Obligatory functions of RadarMap PlugIns Library
//---------------------------------------------------------------------------
void GetLibrarySignature(char* buf, unsigned int buf_size)
{
	if(buf_size>=GUID_Length)
	{
		//sprintf(buf, "%s", RadarMapSignatureGUID);
		strncpy(buf, RadarMapSignatureGUID, sizeof(RadarMapSignatureGUID));
	}
}
//---------------------------------------------------------------------------

int GetPlugInsCount()
{
	return posTotal;
}
//---------------------------------------------------------------------------

int GetPlugInType(int Id)
{
	int res;

	switch((TPosType)Id)
	{
		case posGPS: res=(int)pitPos; break;
		default: res=(int)pitUnknown;
	}
	return res;
}
//---------------------------------------------------------------------------

void GetPlugInName(int Id, char* buf, unsigned int buf_size)
{
	if(Id<posTotal && Id>=0 && buf_size>=PlugInNameLength)
	{
		strncpy(buf, PosItems[Id].Name, PlugInNameLength);
	}
}
//---------------------------------------------------------------------------

void GetPlugInGUID(int Id, char* buf, unsigned int buf_size)
{
	if(Id<posTotal && Id>=0 && buf_size>=GUID_Length)
	{
		strncpy(buf, PosItems[Id].GUID, GUID_Length);
	}
}
//---------------------------------------------------------------------------

bool GetPlugInInfo(int Id, char* buf, unsigned int buf_size)
{
	bool res;

	if(Id<posTotal && Id>=0 && buf_size==sizeof(TPosItem))
	{
		memcpy(buf, &PosItems[Id], buf_size);
		res=true;
	}
	else res=false;

	return res;
}
//---------------------------------------------------------------------------

bool GetOption(int Id, int N, char* buf, unsigned int buf_size)
{
	bool res;
	
	if(Id<posTotal && Id>=0 && N<PosItems[Id].OptionsCount && N>=0 && buf_size==sizeof(TOption))
	{
		memcpy(buf, &Options[Id][N], sizeof(TOption));
		res=true;
	}
	else res=false;

	return res;
}
//---------------------------------------------------------------------------

void SetOption(int Id, int N, char* buf, unsigned int buf_size)
{
	TOption temp;

	if(Id<posTotal && Id>=0 && N<PosItems[Id].OptionsCount && N>=0 && buf_size==sizeof(TOption))
	{
		memcpy(&temp, buf, sizeof(TOption));
		if(temp.Type==Options[Id][N].Type)
		{
			Options[Id][N]=temp;
			/*switch((TPosOptionKind)N)
			{
				case pokMinStep:

					break;
			}*/
		}
	}
}
//---------------------------------------------------------------------------

void InitPlugIn(int Id)
{
	switch((TPosType)Id)
	{
		case posGPS:
			break;
	}
}
//---------------------------------------------------------------------------

void FreePlugIn(int Id)
{
	switch((TPosType)Id)
	{
		case posGPS:
			break;
	}
}
//---------------------------------------------------------------------------

void GetAbout(char* Copyright, char* Version, char* Description)
{
	strncpy(Copyright, _AboutStr, 255);
	strncpy(Version, _Version, 32);
	strncpy(Description, _Description, 255);
}

//---------------------------------------------------------------------------
// User defined fuctions
//---------------------------------------------------------------------------
void StartAcquisition(T3DDoublePoint XYH, TCoordinateSystemDimension Dimension)
{
	LastTickCount=GetTickCount64();
	LastSpeed=0;
	LastDimension=Dimension;
	LastPoint=XYH;
	Started=true;
	dTcnt=0;
	dTa=0;
}
//---------------------------------------------------------------------------

void StopAcquisition()
{
	Started=false;
	LastTickCount=GetTickCount64();
	PassedTickCount=0;
	LastSpeed=0;
	LastPoint.X=0;
	LastPoint.Y=0;
	LastPoint.Z=0;
	dTcnt=0;
	dTa=0;
}
//---------------------------------------------------------------------------

double GetPassedDistance(double WheelDistance, T3DDoublePoint XYH, bool XYHFreshness,
	TCoordinateSystemDimension Dimension)
{
	bool res=false, apply=false;
	double dX, dY, dZ, dS, dP, result;
	ULONGLONG dt, TickCount=GetTickCount64();

	if(Started && LastDimension==Dimension && !(LastPoint.X==0 && LastPoint.Y==0 && LastPoint.Z==0) && Dimension!=csdNone)
	{
		dt=TickCount-LastTickCount;
		if(dt>0 && dt<3600000)
		{
			res=true;
			result=0;
			if(!(XYH.X==0 && XYH.Y==0 && XYH.Z==0))
			{
				if(Dimension==csdDegrees)
				{
					long double a, b, xx;
					// Y = Latitude, X = Longitude
					a=sinl((long double)XYH.Y/(long double)57.29577951)*sinl((long double)LastPoint.Y/(long double)57.29577951);
					b=cosl((long double)XYH.Y/(long double)57.29577951)*cosl((long double)LastPoint.Y/(long double)57.29577951)*
						cosl((long double)LastPoint.X/(long double)57.29577951-(long double)XYH.Y/(long double)57.29577951);
					xx=a+b;
					if((-xx*xx+1.)>0) result=(double)((long double)6367444.65*(atanl(-xx/sqrtl(-xx*xx+1.))+2.*M_PI_4));//atanl(1.)));
					else
					{
						result=0;
						res=false;
					}
				}
				else if(Dimension==csdMeters || Dimension==csdFeets)
				{
					dX=XYH.X-LastPoint.X;
					dY=XYH.Y-LastPoint.Y;
					result=sqrt(dX*dX+dY*dY);
				}
				if(IfOption(Options[posGPS][pokUseZ], true))
				{
					dZ=XYH.Z-LastPoint.Z;
					result=sqrt(result*result+dZ*dZ);
				}
			}
			if(IfOption(Options[posGPS][pokSpeedPredict], true))
			{
				if(XYHFreshness && result>=*(double*)(Options[posGPS][pokMinStep].Value))
				{
					dS=result/(double)dt;
					dt-=PassedTickCount;
					apply=true;
					dTcnt++;
					dP=0;
				}
				else
				{
					if(dTcnt!=0 && dt>dTa)
					{
						dS=LastSpeed*(1.-(dt-dTa)/dTa);
						if(dS<0.) dS=0;
					}
					else dS=LastSpeed;
					dt-=PassedTickCount;
					dP=PassedTickCount+dt;
				}
				result=LastSpeed*(double)dt;
				PassedTickCount=dP;
				LastSpeed=dS;
			}
			else if(res)
			{
				if(result<*(double*)(Options[posGPS][pokMinStep].Value)) result=0;
				else
				{
					apply=true;
					PassedTickCount=0;
				}
			}
			if(apply)
			{
            	LastTickCount=TickCount;
				LastPoint=XYH;
				if(dTcnt==0) dTa=dt;
				else dTa=dTa*(dTcnt/(dTcnt+1.))+dt*(1./dTcnt+1.);
				dTcnt++;
				if(dTcnt>dTcnt_max) dTcnt=dTcnt_max;
			}
		}
		else
		{
			result=0;
			LastTickCount=TickCount;
			LastPoint=XYH;
		}
	}
	else
	{
		StartAcquisition(XYH, Dimension);
		result=0;
	}

	return result;
}
//---------------------------------------------------------------------------

void GetCorrectedXYH(T3DDoublePoint &XYH, bool XYHFreshness, TCoordinateSystemDimension Dimension)
{

}
