//---------------------------------------------------------------------------

#include "MainDLL_Metric.h"
//---------------------------------------------------------------------------
/*
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.

 Software based on "PROJ.4 - Cartographic Projections Library".

 PROJ.4 version is 4.8.0

 The Initial Developer of the Original Code - PROJ.4 Code is Frank Warmerdam,

 Copyright (C) 2014 Radar Systems, Inc.
 All Rights Reserved.
*/
//---------------------------------------------------------------------------

#pragma argsused
//---------------------------------------------------------------------------
// DLL Main entry function
//---------------------------------------------------------------------------
int WINAPI DllMain(HINSTANCE hinst, unsigned long reason, void* lpReserved)
{
	switch(reason)
	{
		case DLL_PROCESS_ATTACH:
			pj_latlon_WGS84=pj_init_plus("+proj=latlong +datum=WGS84");
			pj_CRS=NULL;
			sprintf(CRSItems.GUID, "{53E94BD8-FD21-47da-B26B-385CB20C4CEB}");
			sprintf(CRSItems.Prefix, "WGS84");
			sprintf(CRSItems.Name, "World Mercator");
			sprintf(CRSItems.Code, "csMetric");
			sprintf(CRSItems.EllipsoidName, "WGS84");
			sprintf(CRSItems.EPSG, "EPSG:3395");
			sprintf(CRSItems.LatLonAltToXYHFunctionName, "_CRS_LatLonAltToXYH");
			sprintf(CRSItems.XYHToLatLonAltFunctionName, "_CRS_XYHToLatLonAlt");
			sprintf(CRSItems.UnitVectorFunctionName, "_CRS_UnitVector");
			sprintf(CRSItems.PROJ4str, "+proj=merc +lon_0=0 +k=1 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs");
			CRSItems.Dimension=csdMeters;
			CRSItems.LatLonBounds.DoublePointRect(-180., -80., 180., 84.);
			CRSItems.XYBounds.DoublePointRect(-20026376.39, -15496570.74, 20026376.39, 18764656.23);
			LatLonAltToXYH=CRS_LatLonAltToXYH;
			XYHToLatLonAlt=CRS_XYHToLatLonAlt;
			CRSItems.OptionsCount=0;
			break;
		case DLL_PROCESS_DETACH:
			if(!lpReserved) //host process terminated with FreeLibrary call
			{
				pj_free(pj_latlon_WGS84);
				pj_latlon_WGS84=NULL;
				FreePlugIn(CRSTotal);
			}
			break;
	}
	return 1;
}

//---------------------------------------------------------------------------
// Obligatory functions of RadarMap PlugIns Library
//---------------------------------------------------------------------------
void GetLibrarySignature(char* buf, unsigned int buf_size)
{
	if(buf_size>=GUID_Length)
	{
		//sprintf(buf, "%s", RadarMapSignatureGUID);
		strncpy(buf, RadarMapSignatureGUID, sizeof(RadarMapSignatureGUID));
	}
}
//---------------------------------------------------------------------------

int GetPlugInsCount()
{
	return (int)CRSTotal;
}
//---------------------------------------------------------------------------

int GetPlugInType(int Id)
{
	int res;

	switch((TCRSType)Id)
	{
		case CurrentCRS: res=(int)pitCRS; break;
		default: res=(int)pitUnknown;
	}
	return res;
}
//---------------------------------------------------------------------------

void GetPlugInName(int Id, char* buf, unsigned int buf_size)
{
	if(Id<CRSTotal && Id>=0 && buf_size>=PlugInNameLength)
	{
		//sprintf(buf, "%s", CRSItems[Id].GUID);
		strncpy(buf, CRSItems.Name, PlugInNameLength);
	}
}
//---------------------------------------------------------------------------

void GetPlugInGUID(int Id, char* buf, unsigned int buf_size)
{
	if(Id<CRSTotal && Id>=0 && buf_size>=GUID_Length)
	{
		//sprintf(buf, "%s", CRSItems[Id].GUID);
		strncpy(buf, CRSItems.GUID, GUID_Length);
	}
}
//---------------------------------------------------------------------------

bool GetPlugInInfo(int Id, char* buf, unsigned int buf_size)
{
	bool res;

	if(Id<CRSTotal && Id>=0 && buf_size==sizeof(TCRSItem))
	{
		memcpy(buf, &CRSItems, buf_size);
		res=true;
	}
	else res=false;

	return res;
}
//---------------------------------------------------------------------------

void InitPlugIn(int Id)
{
	FreePlugIn(Id);
	switch((TCRSType)Id)
	{
		case CurrentCRS:
			pj_CRS = pj_init_plus(CRSItems.PROJ4str);
			break;
		case CRSTotal:
			for(int i=0; i<CRSTotal; i++) InitPlugIn(i);
			break;
	}
}
//---------------------------------------------------------------------------

void FreePlugIn(int Id)
{
	switch((TCRSType)Id)
	{
		case CurrentCRS:
			if(pj_CRS)
			{
				pj_free(pj_CRS);
				pj_CRS=NULL;
			}
			break;
		case CRSTotal:
			for(int i=0; i<CRSTotal; i++) FreePlugIn(i);
			break;
	}
}
//---------------------------------------------------------------------------

void GetAbout(char* Copyright, char* Version, char* Description)
{
	strncpy(Copyright, _AboutStr, 255);
	strncpy(Version, _Version, 32);
	strncpy(Description, _Description, 255);
}

//---------------------------------------------------------------------------
// User defined fuctions
//---------------------------------------------------------------------------
bool CRS_LatLonAltToXYH(double lat, double lon, double alt, double &x, double &y, double &h)
{
	if(pj_latlon_WGS84 && pj_CRS)
	{
		lat*=DEG_TO_RAD;
		lon*=DEG_TO_RAD;
		pj_transform(pj_latlon_WGS84, pj_CRS, 1, 1, &lon, &lat, &alt);
		x=lon;
		y=lat;
		h=alt;
		return true;
	}
	else return false;
}
//---------------------------------------------------------------------------

bool CRS_XYHToLatLonAlt(double x, double y, double h, double &lat, double &lon, double &alt)
{
	if(pj_latlon_WGS84 && pj_CRS)
	{
		pj_transform(pj_CRS, pj_latlon_WGS84, 1, 1, &x, &y, &h);
		lat=y/DEG_TO_RAD;
		lon=x/DEG_TO_RAD;
		alt=h;
		return true;
	}
	else return false;
}
//---------------------------------------------------------------------------

bool CRS_UnitVector(double lat, double lon, double alt, double &_vx, double &_vy)
{
	_vx=cos(MyDegToRad(lat));
	_vy=_vx;
	return true;
}
//---------------------------------------------------------------------------
