//---------------------------------------------------------------------------

#include "MainDLL.h"
//---------------------------------------------------------------------------
/*
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.

 Software based on "PROJ.4 - Cartographic Projections Library".

 PROJ.4 version is 4.8.0

 The Initial Developer of the Original Code - PROJ.4 Code is Frank Warmerdam,

 Copyright (C) 2014 Radar Systems, Inc.
 All Rights Reserved.
*/
//---------------------------------------------------------------------------

#pragma argsused
//---------------------------------------------------------------------------
// DLL Main entry function
//---------------------------------------------------------------------------
int WINAPI DllMain(HINSTANCE hinst, unsigned long reason, void* lpReserved)
{
	switch(reason)
	{
		case DLL_PROCESS_ATTACH:
			pj_GK2=pj_GK3=pj_GK4=pj_GK5=NULL;
			pj_latlon_WGS84=pj_init_plus("+proj=latlong +datum=WGS84");
			// DHDN / 3-degree Gauss-Kruger zone 2
			sprintf(CRSItems[GaussKrugerZ2].GUID, "{8440707C-8701-4282-B790-F2F216FF111C}");
			sprintf(CRSItems[GaussKrugerZ2].Prefix, "DE z2");
			sprintf(CRSItems[GaussKrugerZ2].Name, "German DHDN Gauss-Kruger zone 2");
			sprintf(CRSItems[GaussKrugerZ2].Code, "csDeutsch2");
			sprintf(CRSItems[GaussKrugerZ2].EllipsoidName, "Bessel 1841");
			sprintf(CRSItems[GaussKrugerZ2].EPSG, "EPSG:31466");
			sprintf(CRSItems[GaussKrugerZ2].LatLonAltToXYHFunctionName, "_LatLonAltToXYH_GK2");
			sprintf(CRSItems[GaussKrugerZ2].XYHToLatLonAltFunctionName, "_XYHToLatLonAlt_GK2");
			sprintf(CRSItems[GaussKrugerZ2].UnitVectorFunctionName, "_CRS_UnitVector");
			CRSItems[GaussKrugerZ2].Dimension=csdMeters;
			CRSItems[GaussKrugerZ2].LatLonBounds.DoublePointRect(5.8700, 49.1000, 7.5000, 53.7500);
			CRSItems[GaussKrugerZ2].XYBounds.DoublePointRect(2490547.1867, 5440321.7879, 2609576.6008, 5958700.0208);
			LatLonAltToXYH[GaussKrugerZ2]=LatLonAltToXYH_GK2;
			XYHToLatLonAlt[GaussKrugerZ2]=XYHToLatLonAlt_GK2;
			CRSItems[GaussKrugerZ2].OptionsCount=0;
			// DHDN / 3-degree Gauss-Kruger zone 3
			sprintf(CRSItems[GaussKrugerZ3].GUID, "{21D43F2E-3B61-40de-9F79-89799637E6D7}");
			sprintf(CRSItems[GaussKrugerZ3].Prefix, "DE z3");
			sprintf(CRSItems[GaussKrugerZ3].Name, "German DHDN Gauss-Kruger zone 3");
			sprintf(CRSItems[GaussKrugerZ3].Code, "csDeutsch3");
			sprintf(CRSItems[GaussKrugerZ3].EllipsoidName, "Bessel 1841");
			sprintf(CRSItems[GaussKrugerZ3].EPSG, "EPSG:31467");
			sprintf(CRSItems[GaussKrugerZ3].LatLonAltToXYHFunctionName, "_LatLonAltToXYH_GK3");
			sprintf(CRSItems[GaussKrugerZ3].XYHToLatLonAltFunctionName, "_XYHToLatLonAlt_GK3");
			sprintf(CRSItems[GaussKrugerZ3].UnitVectorFunctionName, "_CRS_UnitVector");
			CRSItems[GaussKrugerZ3].Dimension=csdMeters;
			CRSItems[GaussKrugerZ3].LatLonBounds.DoublePointRect(7.5000, 47.2700, 10.5000, 55.0600);
			CRSItems[GaussKrugerZ3].XYBounds.DoublePointRect(3386564.9400, 5237917.9109, 3613579.2251, 6104500.7393);
			LatLonAltToXYH[GaussKrugerZ3]=LatLonAltToXYH_GK3;
			XYHToLatLonAlt[GaussKrugerZ3]=XYHToLatLonAlt_GK3;
			CRSItems[GaussKrugerZ3].OptionsCount=0;
			// DHDN / 3-degree Gauss-Kruger zone 4
			sprintf(CRSItems[GaussKrugerZ4].GUID, "{FBFDD739-B2CE-4b21-8AA1-51EE020F3309}");
			sprintf(CRSItems[GaussKrugerZ4].Prefix, "DE z4");
			sprintf(CRSItems[GaussKrugerZ4].Name, "German DHDN Gauss-Kruger zone 4");
			sprintf(CRSItems[GaussKrugerZ4].Code, "csDeutsch4");
			sprintf(CRSItems[GaussKrugerZ4].EllipsoidName, "Bessel 1841");
			sprintf(CRSItems[GaussKrugerZ4].EPSG, "EPSG:31468");
			sprintf(CRSItems[GaussKrugerZ4].LatLonAltToXYHFunctionName, "_LatLonAltToXYH_GK4");
			sprintf(CRSItems[GaussKrugerZ4].XYHToLatLonAltFunctionName, "_XYHToLatLonAlt_GK4");
			sprintf(CRSItems[GaussKrugerZ4].UnitVectorFunctionName, "_CRS_UnitVector");
			CRSItems[GaussKrugerZ4].Dimension=csdMeters;
			CRSItems[GaussKrugerZ4].LatLonBounds.DoublePointRect(10.5000, 47.2700, 13.5000, 55.0600);
			CRSItems[GaussKrugerZ4].XYBounds.DoublePointRect(4386596.4101, 5237914.5325, 4613610.5843, 6104496.9694);
			LatLonAltToXYH[GaussKrugerZ4]=LatLonAltToXYH_GK4;
			XYHToLatLonAlt[GaussKrugerZ4]=XYHToLatLonAlt_GK4;
			CRSItems[GaussKrugerZ4].OptionsCount=0;
			// DHDN / 3-degree Gauss-Kruger zone 5
			sprintf(CRSItems[GaussKrugerZ5].GUID, "{2A8146B1-2BC2-482f-8966-4BB905636AE7}");
			sprintf(CRSItems[GaussKrugerZ5].Prefix, "DE z5");
			sprintf(CRSItems[GaussKrugerZ5].Name, "German DHDN Gauss-Kruger zone 5");
			sprintf(CRSItems[GaussKrugerZ5].Code, "csDeutsch5");
			sprintf(CRSItems[GaussKrugerZ5].EllipsoidName, "Bessel 1841");
			sprintf(CRSItems[GaussKrugerZ5].EPSG, "EPSG:31469");
			sprintf(CRSItems[GaussKrugerZ5].LatLonAltToXYHFunctionName, "_LatLonAltToXYH_GK5");
			sprintf(CRSItems[GaussKrugerZ5].XYHToLatLonAltFunctionName, "_XYHToLatLonAlt_GK5");
			sprintf(CRSItems[GaussKrugerZ5].UnitVectorFunctionName, "_CRS_UnitVector");
			CRSItems[GaussKrugerZ5].Dimension=csdMeters;
			CRSItems[GaussKrugerZ5].LatLonBounds.DoublePointRect(13.5000, 47.2700, 16.5000, 55.0600);
			CRSItems[GaussKrugerZ5].XYBounds.DoublePointRect(5386627.6162, 5237909.9506, 5613641.6401, 6104491.8566);
			LatLonAltToXYH[GaussKrugerZ5]=LatLonAltToXYH_GK5;
			XYHToLatLonAlt[GaussKrugerZ5]=XYHToLatLonAlt_GK5;
			CRSItems[GaussKrugerZ5].OptionsCount=0;
			// DHDN / 3-degree Gauss-Kruger zones 2-5
			sprintf(CRSItems[GaussKruger].GUID, "{F8427B6E-8A0F-49e6-A441-93B7E9C13A58}");
			sprintf(CRSItems[GaussKruger].Prefix, "DE");
			sprintf(CRSItems[GaussKruger].Name, "German DHDN Gauss-Kruger zones 2-5");
			sprintf(CRSItems[GaussKruger].Code, "csDeutsch");
			sprintf(CRSItems[GaussKruger].EllipsoidName, "Bessel 1841");
			sprintf(CRSItems[GaussKruger].EPSG, "EPSG:31466");//, 31467, 31468, 31469");
			sprintf(CRSItems[GaussKruger].LatLonAltToXYHFunctionName, "_LatLonAltToXYH_GK2_5");
			sprintf(CRSItems[GaussKruger].XYHToLatLonAltFunctionName, "_XYHToLatLonAlt_GK2_5");
			sprintf(CRSItems[GaussKruger].UnitVectorFunctionName, "_CRS_UnitVector");
			CRSItems[GaussKruger].Dimension=csdMeters;
			CRSItems[GaussKruger].LatLonBounds.DoublePointRect(5.8700, 47.2700, 16.5000, 55.0600);
			CRSItems[GaussKruger].XYBounds.DoublePointRect(2490547.1867, 5237917.9109, 5613641.6401, 6104500.7393);
			LatLonAltToXYH[GaussKruger]=LatLonAltToXYH_GK2_5;
			XYHToLatLonAlt[GaussKruger]=XYHToLatLonAlt_GK2_5;
			CRSItems[GaussKruger].OptionsCount=0;
			break;
		case DLL_PROCESS_DETACH:
			pj_free(pj_latlon_WGS84);
			pj_latlon_WGS84=NULL;
			if(lpReserved) //host process terminated without FreeLibrary call
			{
				FreePlugIn(GaussKruger);
			}
			break;
	}
	return 1;
}

//---------------------------------------------------------------------------
// Obligatory functions of RadarMap PlugIns Library
//---------------------------------------------------------------------------
void GetLibrarySignature(char* buf, unsigned int buf_size)
{
	if(buf_size>=GUID_Length)
	{
		//sprintf(buf, "%s", RadarMapSignatureGUID);
		strncpy(buf, RadarMapSignatureGUID, sizeof(RadarMapSignatureGUID));
	}
}
//---------------------------------------------------------------------------

int GetPlugInsCount()
{
	return (int)CRSTotal;
}
//---------------------------------------------------------------------------

int GetPlugInType(int Id)
{
	int res;

	switch((TCRSType)Id)
	{
		case GaussKrugerZ2:
		case GaussKrugerZ3:
		case GaussKrugerZ4:
		case GaussKrugerZ5:
		case GaussKruger: res=(int)pitCRS; break;
		default: res=(int)pitUnknown;
	}
	return res;
}
//---------------------------------------------------------------------------

void GetPlugInName(int Id, char* buf, unsigned int buf_size)
{
	if(Id<CRSTotal && Id>=0 && buf_size>=PlugInNameLength)
	{
		//sprintf(buf, "%s", CRSItems[Id].GUID);
		strncpy(buf, CRSItems[Id].Name, PlugInNameLength);
	}
}
//---------------------------------------------------------------------------

void GetPlugInGUID(int Id, char* buf, unsigned int buf_size)
{
	if(Id<CRSTotal && Id>=0 && buf_size>=GUID_Length)
	{
		//sprintf(buf, "%s", CRSItems[Id].GUID);
		strncpy(buf, CRSItems[Id].GUID, GUID_Length);
	}
}
//---------------------------------------------------------------------------

bool GetPlugInInfo(int Id, char* buf, unsigned int buf_size)
{
	bool res;

	if(Id<CRSTotal && Id>=0 && buf_size==sizeof(TCRSItem))
	{
		memcpy(buf, &CRSItems[Id], buf_size);
		res=true;
	}
	else res=false;

	return res;
}
//---------------------------------------------------------------------------

void InitPlugIn(int Id)
{
	switch((TCRSType)Id)
	{
		case GaussKrugerZ2:
			pj_GK2 = pj_init_plus("+proj=tmerc +lat_0=0 +lon_0=6 +k=1 +x_0=2500000 +y_0=0 +ellps=bessel +datum=potsdam +units=m +no_defs");
			break;
		case GaussKrugerZ3:
			pj_GK3 = pj_init_plus("+proj=tmerc +lat_0=0 +lon_0=9 +k=1 +x_0=3500000 +y_0=0 +ellps=bessel +datum=potsdam +units=m +no_defs");
			break;
		case GaussKrugerZ4:
			pj_GK4 = pj_init_plus("+proj=tmerc +lat_0=0 +lon_0=12 +k=1 +x_0=4500000 +y_0=0 +ellps=bessel +datum=potsdam +units=m +no_defs");
			break;
		case GaussKrugerZ5:
			pj_GK5 = pj_init_plus("+proj=tmerc +lat_0=0 +lon_0=15 +k=1 +x_0=5500000 +y_0=0 +ellps=bessel +datum=potsdam +units=m +no_defs");
			break;
		case GaussKruger:
			InitPlugIn(GaussKrugerZ2);
			InitPlugIn(GaussKrugerZ3);
			InitPlugIn(GaussKrugerZ4);
			InitPlugIn(GaussKrugerZ5);
			break;
	}
}
//---------------------------------------------------------------------------

void FreePlugIn(int Id)
{
	switch((TCRSType)Id)
	{
		case GaussKrugerZ2:
			if(pj_GK2)
			{
				pj_free(pj_GK2);
				pj_GK2=NULL;
			}
		break;
		case GaussKrugerZ3:
			if(pj_GK3)
			{
				pj_free(pj_GK3);
				pj_GK3=NULL;
			}
		break;
		case GaussKrugerZ4:
			if(pj_GK4)
			{
				pj_free(pj_GK4);
				pj_GK4=NULL;
			}
		break;
		case GaussKrugerZ5:
			if(pj_GK5)
			{
				pj_free(pj_GK5);
				pj_GK5=NULL;
			}
		break;
		case GaussKruger:
			FreePlugIn(GaussKrugerZ2);
			FreePlugIn(GaussKrugerZ3);
			FreePlugIn(GaussKrugerZ4);
			FreePlugIn(GaussKrugerZ5);
		break;
	}
}
//---------------------------------------------------------------------------

void GetAbout(char* Copyright, char* Version, char* Description)
{
	strncpy(Copyright, _AboutStr, 255);
	strncpy(Version, _Version, 32);
	strncpy(Description, _Description, 255);
}

//---------------------------------------------------------------------------
// User defined fuctions
//---------------------------------------------------------------------------
bool LatLonAltToXYH_GK2(double lat, double lon, double alt, double &x, double &y, double &h)
{
	if(pj_latlon_WGS84 && pj_GK2)
	{
		lat*=DEG_TO_RAD;
		lon*=DEG_TO_RAD;
		pj_transform(pj_latlon_WGS84, pj_GK2, 1, 1, &lon, &lat, &alt);
		x=lon;
		y=lat;
		h=alt;
		return true;
	}
	else return false;
}
//---------------------------------------------------------------------------

bool XYHToLatLonAlt_GK2(double x, double y, double h, double &lat, double &lon, double &alt)
{
	if(pj_latlon_WGS84 && pj_GK2)
	{
		pj_transform(pj_GK2, pj_latlon_WGS84, 1, 1, &x, &y, &h);
		lat=y/DEG_TO_RAD;
		lon=x/DEG_TO_RAD;
		alt=h;
		return true;
	}
	else return false;
}
//---------------------------------------------------------------------------

bool LatLonAltToXYH_GK3(double lat, double lon, double alt, double &x, double &y, double &h)
{
	if(pj_latlon_WGS84 && pj_GK3)
	{
		lat*=DEG_TO_RAD;
		lon*=DEG_TO_RAD;
		pj_transform(pj_latlon_WGS84, pj_GK3, 1, 1, &lon, &lat, &alt);
		x=lon;
		y=lat;
		h=alt;
		return true;
	}
	else return false;
}
//---------------------------------------------------------------------------

bool XYHToLatLonAlt_GK3(double x, double y, double h, double &lat, double &lon, double &alt)
{
	if(pj_latlon_WGS84 && pj_GK3)
	{
		pj_transform(pj_GK3, pj_latlon_WGS84, 1, 1, &x, &y, &h);
		lat=y/DEG_TO_RAD;
		lon=x/DEG_TO_RAD;
		alt=h;
		return true;
	}
	else return false;
}
//---------------------------------------------------------------------------

bool LatLonAltToXYH_GK4(double lat, double lon, double alt, double &x, double &y, double &h)
{
	if(pj_latlon_WGS84 && pj_GK4)
	{
		lat*=DEG_TO_RAD;
		lon*=DEG_TO_RAD;
		pj_transform(pj_latlon_WGS84, pj_GK4, 1, 1, &lon, &lat, &alt);
		x=lon;
		y=lat;
		h=alt;
		return true;
	}
	else return false;
}
//---------------------------------------------------------------------------

bool XYHToLatLonAlt_GK4(double x, double y, double h, double &lat, double &lon, double &alt)
{
	if(pj_latlon_WGS84 && pj_GK4)
	{
		pj_transform(pj_GK4, pj_latlon_WGS84, 1, 1, &x, &y, &h);
		lat=y/DEG_TO_RAD;
		lon=x/DEG_TO_RAD;
		alt=h;
		return true;
	}
	else return false;
}
//---------------------------------------------------------------------------

bool LatLonAltToXYH_GK5(double lat, double lon, double alt, double &x, double &y, double &h)
{
	if(pj_latlon_WGS84 && pj_GK5)
	{
		lat*=DEG_TO_RAD;
		lon*=DEG_TO_RAD;
		pj_transform(pj_latlon_WGS84, pj_GK5, 1, 1, &lon, &lat, &alt);
		x=lon;
		y=lat;
		h=alt;
		return true;
	}
	else return false;
}
//---------------------------------------------------------------------------

bool XYHToLatLonAlt_GK5(double x, double y, double h, double &lat, double &lon, double &alt)
{
	if(pj_latlon_WGS84 && pj_GK5)
	{
		pj_transform(pj_GK5, pj_latlon_WGS84, 1, 1, &x, &y, &h);
		lat=y/DEG_TO_RAD;
		lon=x/DEG_TO_RAD;
		alt=h;
		return true;
	}
	else return false;
}
//---------------------------------------------------------------------------

bool LatLonAltToXYH_GK2_5(double lat, double lon, double alt, double &x, double &y, double &h)
{
	bool res=false;

	for(int i=GaussKrugerZ2; i<=GaussKrugerZ5; i++)
	{
		if(CRSItems[i].LatLonBounds.Contains(lon, lat) && LatLonAltToXYH[i])
		{
			res=(LatLonAltToXYH[i])(lat, lon, alt, x, y, h);
			break;
		}
	}

	return res;
}
//---------------------------------------------------------------------------

bool XYHToLatLonAlt_GK2_5(double x, double y, double h, double &lat, double &lon, double &alt)
{
	bool res=false;

	for(int i=GaussKrugerZ2; i<=GaussKrugerZ5; i++)
	{
		if(CRSItems[i].XYBounds.Contains(x, y) && LatLonAltToXYH[i])
		{
			res=(XYHToLatLonAlt[i])(x, y, h, lat, lon, alt);
			break;
		}
	}

	return res;
}
//---------------------------------------------------------------------------

bool CRS_UnitVector(double lat, double lon, double alt, double &_vx, double &_vy)
{
	_vx=_vy=1.;
}
