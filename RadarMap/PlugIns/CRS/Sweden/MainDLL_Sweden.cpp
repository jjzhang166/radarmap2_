//---------------------------------------------------------------------------

#include "MainDLL_Sweden.h"
//---------------------------------------------------------------------------
/*
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.

 Software based on "PROJ.4 - Cartographic Projections Library".

 PROJ.4 version is 4.8.0

 The Initial Developer of the Original Code - PROJ.4 Code is Frank Warmerdam,

 Copyright (C) 2014 Radar Systems, Inc.
 All Rights Reserved.
*/
//---------------------------------------------------------------------------

#pragma argsused
//---------------------------------------------------------------------------
// DLL Main entry function
//---------------------------------------------------------------------------
int WINAPI DllMain(HINSTANCE hinst, unsigned long reason, void* lpReserved)
{
	switch(reason)
	{
		case DLL_PROCESS_ATTACH:
			pj_latlon_WGS84=pj_init_plus("+proj=latlong +datum=WGS84");
			PresetPlugIn(CRSTotal);
			break;
		case DLL_PROCESS_DETACH:
			if(!lpReserved) //host process terminated with FreeLibrary call
			{
				if(pj_latlon_WGS84) pj_free(pj_latlon_WGS84);
				pj_latlon_WGS84=NULL;
				FreePlugIn(CRSTotal);
			}
			break;
	}
	return 1;
}

//---------------------------------------------------------------------------
// Obligatory functions of RadarMap PlugIns Library
//---------------------------------------------------------------------------
void GetLibrarySignature(char* buf, unsigned int buf_size)
{
	if(buf_size>=GUID_Length)
	{
		//sprintf(buf, "%s", RadarMapSignatureGUID);
		strncpy(buf, RadarMapSignatureGUID, sizeof(RadarMapSignatureGUID));
	}
}
//---------------------------------------------------------------------------

int GetPlugInsCount()
{
	return (int)CRSTotal;
}
//---------------------------------------------------------------------------

void GetPlugInName(int Id, char* buf, unsigned int buf_size)
{
	if(Id<CRSTotal && Id>=0 && buf_size>=PlugInNameLength)
	{
		//sprintf(buf, "%s", CRSItems[Id].GUID);
		strncpy(buf, CRSItems[Id].Name, PlugInNameLength);
	}
}
//---------------------------------------------------------------------------

void GetPlugInGUID(int Id, char* buf, unsigned int buf_size)
{
	if(Id<CRSTotal && Id>=0 && buf_size>=GUID_Length)
	{
		//sprintf(buf, "%s", CRSItems[Id].GUID);
		strncpy(buf, CRSItems[Id].GUID, GUID_Length);
	}
}
//---------------------------------------------------------------------------

bool GetPlugInInfo(int Id, char* buf, unsigned int buf_size)
{
	bool res;

	if(Id<CRSTotal && Id>=0 && buf_size==sizeof(TCRSItem))
	{
		memcpy(buf, &CRSItems[Id], buf_size);
		res=true;
	}
	else res=false;

	return res;
}
//---------------------------------------------------------------------------

void GetAbout(char* Copyright, char* Version, char* Description)
{
	strncpy(Copyright, _AboutStr, 255);
	strncpy(Version, _Version, 32);
	strncpy(Description, _Description, 255);
}

//---------------------------------------------------------------------------
// Obligatory functions filled by User of RadarMap PlugIns Library
//---------------------------------------------------------------------------
int GetPlugInType(int Id)
{
	int res;

	switch((TCRSType)Id)
	{
		case SWEREF99_TM:
		case SWEREF99_1200:
		case SWEREF99_1330:
		case SWEREF99_1500:
		case SWEREF99_1630:
		case SWEREF99_1800:
		case SWEREF99_1415:
		case SWEREF99_1545:
		case SWEREF99_1715:
		case SWEREF99_1845:
		case SWEREF99_2015:
		case SWEREF99_2145:
		case SWEREF99_2315: res=(int)pitCRS; break;
		default: res=(int)pitUnknown;
	}
	return res;
}
//---------------------------------------------------------------------------

void PresetPlugIn(int Id)
{
	switch((TCRSType)Id)
	{
		case SWEREF99_TM:
			pj_TM=NULL;
			sprintf(CRSItems[SWEREF99_TM].GUID, "{18821E1F-9F80-4fe9-816B-6EB6FCCF4CAB}");
			sprintf(CRSItems[SWEREF99_TM].Prefix, "SE");
			sprintf(CRSItems[SWEREF99_TM].Name, "Swedish SWEREF99 TM");
			sprintf(CRSItems[SWEREF99_TM].Code, "csSWEREF99_TM");
			sprintf(CRSItems[SWEREF99_TM].EllipsoidName, "GRS 1980");
			sprintf(CRSItems[SWEREF99_TM].EPSG, "EPSG:3006");
			sprintf(CRSItems[SWEREF99_TM].LatLonAltToXYHFunctionName, "_LatLonAltToXYH_TM");
			sprintf(CRSItems[SWEREF99_TM].XYHToLatLonAltFunctionName, "_XYHToLatLonAlt_TM");
			sprintf(CRSItems[SWEREF99_TM].UnitVectorFunctionName, "_CRS_UnitVector");
			sprintf(CRSItems[SWEREF99_TM].PROJ4str, "+proj=utm +zone=33 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");
			CRSItems[SWEREF99_TM].Dimension=csdMeters;
			CRSItems[SWEREF99_TM].LatLonBounds.DoublePointRect(10.03, 54.96, 24.17, 69.07);
			CRSItems[SWEREF99_TM].XYBounds.DoublePointRect(181896.33, 6101648.07, 864416.00, 7689478.31);
			//LatLonAltToXYH[SWEREF99_TM]=LatLonAltToXYH_TM;
			//XYHToLatLonAlt[SWEREF99_TM]=XYHToLatLonAlt_TM;
			CRSItems[SWEREF99_TM].OptionsCount=0;
			break;
		case SWEREF99_1200:
			pj_1200=NULL;
			sprintf(CRSItems[SWEREF99_1200].GUID, "{D7F01420-271B-4192-ACC4-97D6A8DBAA65}");
			sprintf(CRSItems[SWEREF99_1200].Prefix, "SE z1200");
			sprintf(CRSItems[SWEREF99_1200].Name, "Swedish SWEREF99 1200");
			sprintf(CRSItems[SWEREF99_1200].Code, "csSWEREF99_1200");
			sprintf(CRSItems[SWEREF99_1200].EllipsoidName, "GRS 1980");
			sprintf(CRSItems[SWEREF99_1200].EPSG, "EPSG:3007");
			sprintf(CRSItems[SWEREF99_1200].LatLonAltToXYHFunctionName, "_LatLonAltToXYH_1200");
			sprintf(CRSItems[SWEREF99_1200].XYHToLatLonAltFunctionName, "_XYHToLatLonAlt_1200");
			sprintf(CRSItems[SWEREF99_1200].UnitVectorFunctionName, "_CRS_UnitVector");
			sprintf(CRSItems[SWEREF99_1200].PROJ4str, "+proj=tmerc +lat_0=0 +lon_0=12 +k=1 +x_0=150000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");
			CRSItems[SWEREF99_1200].Dimension=csdMeters;
			CRSItems[SWEREF99_1200].LatLonBounds.DoublePointRect(10.57, 55.85, 13.08, 60.25);
			CRSItems[SWEREF99_1200].XYBounds.DoublePointRect(60436.51, 6192786.97, 209806.95, 6682415.82);
			//LatLonAltToXYH[SWEREF99_1200]=LatLonAltToXYH_1200;
			//XYHToLatLonAlt[SWEREF99_1200]=XYHToLatLonAlt_1200;
			CRSItems[SWEREF99_1200].OptionsCount=0;
			break;
		case SWEREF99_1330:
			pj_1330=NULL;
			sprintf(CRSItems[SWEREF99_1330].GUID, "{383888F2-8702-4424-9789-5E3577251709}");
			sprintf(CRSItems[SWEREF99_1330].Prefix, "SE z1330");
			sprintf(CRSItems[SWEREF99_1330].Name, "Swedish SWEREF99 13 30");
			sprintf(CRSItems[SWEREF99_1330].Code, "csSWEREF99_1330");
			sprintf(CRSItems[SWEREF99_1330].EllipsoidName, "GRS 1980");
			sprintf(CRSItems[SWEREF99_1330].EPSG, "EPSG:3008");
			sprintf(CRSItems[SWEREF99_1330].LatLonAltToXYHFunctionName, "_LatLonAltToXYH_1330");
			sprintf(CRSItems[SWEREF99_1330].XYHToLatLonAltFunctionName, "_XYHToLatLonAlt_1330");
			sprintf(CRSItems[SWEREF99_1330].UnitVectorFunctionName, "_CRS_UnitVector");
			sprintf(CRSItems[SWEREF99_1330].PROJ4str, "+proj=tmerc +lat_0=0 +lon_0=13.5 +k=1 +x_0=150000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");
			CRSItems[SWEREF99_1330].Dimension=csdMeters;
			CRSItems[SWEREF99_1330].LatLonBounds.DoublePointRect(12.1000, 55.2000, 14.6500, 62.2600);
			CRSItems[SWEREF99_1330].XYBounds.DoublePointRect(60857.50, 6120389.76, 209741.58, 6906437.98);
			//LatLonAltToXYH[SWEREF99_1330]=LatLonAltToXYH_1330;
			//XYHToLatLonAlt[SWEREF99_1330]=XYHToLatLonAlt_1330;
			CRSItems[SWEREF99_1330].OptionsCount=0;
			break;
		case SWEREF99_1500:
			pj_1500=NULL;
			sprintf(CRSItems[SWEREF99_1500].GUID, "{E4174D18-4BFA-47e3-901E-9FA7C46590D5}");
			sprintf(CRSItems[SWEREF99_1500].Prefix, "SE z1500");
			sprintf(CRSItems[SWEREF99_1500].Name, "Swedish SWEREF99 15 00");
			sprintf(CRSItems[SWEREF99_1500].Code, "csSWEREF99_1500");
			sprintf(CRSItems[SWEREF99_1500].EllipsoidName, "GRS 1980");
			sprintf(CRSItems[SWEREF99_1500].EPSG, "EPSG:3009");
			sprintf(CRSItems[SWEREF99_1500].LatLonAltToXYHFunctionName, "_LatLonAltToXYH_1500");
			sprintf(CRSItems[SWEREF99_1500].XYHToLatLonAltFunctionName, "_XYHToLatLonAlt_1500");
			sprintf(CRSItems[SWEREF99_1500].UnitVectorFunctionName, "_CRS_UnitVector");
			sprintf(CRSItems[SWEREF99_1500].PROJ4str, "+proj=tmerc +lat_0=0 +lon_0=15 +k=1 +x_0=150000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");
			CRSItems[SWEREF99_1500].Dimension=csdMeters;
			CRSItems[SWEREF99_1500].LatLonBounds.DoublePointRect(13.5000, 55.9500, 16.1000, 61.6200);
			CRSItems[SWEREF99_1500].XYBounds.DoublePointRect(56294.04, 6204012.34, 208352.69, 6835075.63);
			//LatLonAltToXYH[SWEREF99_1500]=LatLonAltToXYH_1500;
			//XYHToLatLonAlt[SWEREF99_1500]=XYHToLatLonAlt_1500;
			CRSItems[SWEREF99_1500].OptionsCount=0;
			break;
		case SWEREF99_1630:
			pj_1630=NULL;
			sprintf(CRSItems[SWEREF99_1630].GUID, "{3EB39821-29F7-4076-A110-879D7D605AE5}");
			sprintf(CRSItems[SWEREF99_1630].Prefix, "SE z1630");
			sprintf(CRSItems[SWEREF99_1630].Name, "Swedish SWEREF99 16 30");
			sprintf(CRSItems[SWEREF99_1630].Code, "csSWEREF99_1630");
			sprintf(CRSItems[SWEREF99_1630].EllipsoidName, "GRS 1980");
			sprintf(CRSItems[SWEREF99_1630].EPSG, "EPSG:3010");
			sprintf(CRSItems[SWEREF99_1630].LatLonAltToXYHFunctionName, "_LatLonAltToXYH_1630");
			sprintf(CRSItems[SWEREF99_1630].XYHToLatLonAltFunctionName, "_XYHToLatLonAlt_1630");
			sprintf(CRSItems[SWEREF99_1630].UnitVectorFunctionName, "_CRS_UnitVector");
			sprintf(CRSItems[SWEREF99_1630].PROJ4str, "+proj=tmerc +lat_0=0 +lon_0=16.5 +k=1 +x_0=150000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");
			CRSItems[SWEREF99_1630].Dimension=csdMeters;
			CRSItems[SWEREF99_1630].LatLonBounds.DoublePointRect(15.6500, 56.1800, 17.7100, 62.3500);
			CRSItems[SWEREF99_1630].XYBounds.DoublePointRect(97213.6352, 6228930.1419, 225141.8681, 6916524.0785);
			//LatLonAltToXYH[SWEREF99_1630]=LatLonAltToXYH_1630;
			//XYHToLatLonAlt[SWEREF99_1630]=XYHToLatLonAlt_1630;
			CRSItems[SWEREF99_1630].OptionsCount=0;
			break;
		case SWEREF99_1800:
			pj_1800=NULL;
			sprintf(CRSItems[SWEREF99_1800].GUID, "{1D91280E-AF0D-4d0a-95BF-331B2E71CAEB}");
			sprintf(CRSItems[SWEREF99_1800].Prefix, "SE z1800");
			sprintf(CRSItems[SWEREF99_1800].Name, "Swedish SWEREF99 18 00");
			sprintf(CRSItems[SWEREF99_1800].Code, "csSWEREF99_1800");
			sprintf(CRSItems[SWEREF99_1800].EllipsoidName, "GRS 1980");
			sprintf(CRSItems[SWEREF99_1800].EPSG, "EPSG:3011");
			sprintf(CRSItems[SWEREF99_1800].LatLonAltToXYHFunctionName, "_LatLonAltToXYH_1800");
			sprintf(CRSItems[SWEREF99_1800].XYHToLatLonAltFunctionName, "_XYHToLatLonAlt_1800");
			sprintf(CRSItems[SWEREF99_1800].UnitVectorFunctionName, "_CRS_UnitVector");
			sprintf(CRSItems[SWEREF99_1800].PROJ4str, "+proj=tmerc +lat_0=0 +lon_0=18 +k=1 +x_0=150000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");
			CRSItems[SWEREF99_1800].Dimension=csdMeters;
			CRSItems[SWEREF99_1800].LatLonBounds.DoublePointRect(17.0800, 58.7000, 19.2100, 60.6500);
			CRSItems[SWEREF99_1800].XYBounds.DoublePointRect(96664.5565, 6509617.2232, 220146.6914, 6727103.5879);
			//LatLonAltToXYH[SWEREF99_1800]=LatLonAltToXYH_1800;
			//XYHToLatLonAlt[SWEREF99_1800]=XYHToLatLonAlt_1800;
			CRSItems[SWEREF99_1800].OptionsCount=0;
			break;
		case SWEREF99_1415:
			pj_1415=NULL;
			sprintf(CRSItems[SWEREF99_1415].GUID, "{E237B834-B2A4-4a41-BFB7-30306FB16D91}");
			sprintf(CRSItems[SWEREF99_1415].Prefix, "SE z1415");
			sprintf(CRSItems[SWEREF99_1415].Name, "Swedish SWEREF99 14 15");
			sprintf(CRSItems[SWEREF99_1415].Code, "csSWEREF99_1415");
			sprintf(CRSItems[SWEREF99_1415].EllipsoidName, "GRS 1980");
			sprintf(CRSItems[SWEREF99_1415].EPSG, "EPSG:3012");
			sprintf(CRSItems[SWEREF99_1415].LatLonAltToXYHFunctionName, "_LatLonAltToXYH_1415");
			sprintf(CRSItems[SWEREF99_1415].XYHToLatLonAltFunctionName, "_XYHToLatLonAlt_1415");
			sprintf(CRSItems[SWEREF99_1415].UnitVectorFunctionName, "_CRS_UnitVector");
			sprintf(CRSItems[SWEREF99_1415].PROJ4str, "+proj=tmerc +lat_0=0 +lon_0=14.25 +k=1 +x_0=150000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");
			CRSItems[SWEREF99_1415].Dimension=csdMeters;
			CRSItems[SWEREF99_1415].LatLonBounds.DoublePointRect(12.0000, 61.5700, 15.5000, 64.470);
			CRSItems[SWEREF99_1415].XYBounds.DoublePointRect(30462.5263, 6829647.9842, 216416.1584, 7154168.0208);
			//LatLonAltToXYH[SWEREF99_1415]=LatLonAltToXYH_1415;
			//XYHToLatLonAlt[SWEREF99_1415]=XYHToLatLonAlt_1415;
			CRSItems[SWEREF99_1415].OptionsCount=0;
			break;
		case SWEREF99_1545:
			pj_1545=NULL;
			sprintf(CRSItems[SWEREF99_1545].GUID, "{F162602D-D39D-413a-8AEB-85FB913664E9}");
			sprintf(CRSItems[SWEREF99_1545].Prefix, "SE z1545");
			sprintf(CRSItems[SWEREF99_1545].Name, "Swedish SWEREF99 15 45");
			sprintf(CRSItems[SWEREF99_1545].Code, "csSWEREF99_1545");
			sprintf(CRSItems[SWEREF99_1545].EllipsoidName, "GRS 1980");
			sprintf(CRSItems[SWEREF99_1545].EPSG, "EPSG:3013");
			sprintf(CRSItems[SWEREF99_1545].LatLonAltToXYHFunctionName, "_LatLonAltToXYH_1545");
			sprintf(CRSItems[SWEREF99_1545].XYHToLatLonAltFunctionName, "_XYHToLatLonAlt_1545");
			sprintf(CRSItems[SWEREF99_1545].UnitVectorFunctionName, "_CRS_UnitVector");
			sprintf(CRSItems[SWEREF99_1545].PROJ4str, "+proj=tmerc +lat_0=0 +lon_0=15.75 +k=1 +x_0=150000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");
			CRSItems[SWEREF99_1545].Dimension=csdMeters;
			CRSItems[SWEREF99_1545].LatLonBounds.DoublePointRect(13.6400, 60.5000, 17.0000, 65.1000);
			CRSItems[SWEREF99_1545].XYBounds.DoublePointRect(34056.6264, 6710433.2884, 218692.0214, 7224144.7320);
			//LatLonAltToXYH[SWEREF99_1545]=LatLonAltToXYH_1545;
			//XYHToLatLonAlt[SWEREF99_1545]=XYHToLatLonAlt_1545;
			CRSItems[SWEREF99_1545].OptionsCount=0;
			break;
		case SWEREF99_1715:
			pj_1715=NULL;
			sprintf(CRSItems[SWEREF99_1715].GUID, "{12425AA0-D58E-4681-BDC9-BF31631F96D9}");
			sprintf(CRSItems[SWEREF99_1715].Prefix, "SE z1715");
			sprintf(CRSItems[SWEREF99_1715].Name, "Swedish SWEREF99 17 15");
			sprintf(CRSItems[SWEREF99_1715].Code, "csSWEREF99_1715");
			sprintf(CRSItems[SWEREF99_1715].EllipsoidName, "GRS 1980");
			sprintf(CRSItems[SWEREF99_1715].EPSG, "EPSG:3014");
			sprintf(CRSItems[SWEREF99_1715].LatLonAltToXYHFunctionName, "_LatLonAltToXYH_1715");
			sprintf(CRSItems[SWEREF99_1715].XYHToLatLonAltFunctionName, "_XYHToLatLonAlt_1715");
			sprintf(CRSItems[SWEREF99_1715].UnitVectorFunctionName, "_CRS_UnitVector");
			sprintf(CRSItems[SWEREF99_1715].PROJ4str, "+proj=tmerc +lat_0=0 +lon_0=17.25 +k=1 +x_0=150000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");
			CRSItems[SWEREF99_1715].Dimension=csdMeters;
			CRSItems[SWEREF99_1715].LatLonBounds.DoublePointRect(14.3500, 62.1000, 18.4500, 67.2000);
			CRSItems[SWEREF99_1715].XYBounds.DoublePointRect(-1420.2800, 6888655.5779, 212669.1333, 7459585.3378);
			//LatLonAltToXYH[SWEREF99_1715]=LatLonAltToXYH_1715;
			//XYHToLatLonAlt[SWEREF99_1715]=XYHToLatLonAlt_1715;
			CRSItems[SWEREF99_1715].OptionsCount=0;
			break;
		case SWEREF99_1845:
			pj_1845=NULL;
			sprintf(CRSItems[SWEREF99_1845].GUID, "{24B08512-91F5-4cbf-ABC0-EF7005FC1D20}");
			sprintf(CRSItems[SWEREF99_1845].Prefix, "SE z1845");
			sprintf(CRSItems[SWEREF99_1845].Name, "Swedish SWEREF99 18 45");
			sprintf(CRSItems[SWEREF99_1845].Code, "csSWEREF99_1845");
			sprintf(CRSItems[SWEREF99_1845].EllipsoidName, "GRS 1980");
			sprintf(CRSItems[SWEREF99_1845].EPSG, "EPSG:3015");
			sprintf(CRSItems[SWEREF99_1845].LatLonAltToXYHFunctionName, "_LatLonAltToXYH_1845");
			sprintf(CRSItems[SWEREF99_1845].XYHToLatLonAltFunctionName, "_XYHToLatLonAlt_1845");
			sprintf(CRSItems[SWEREF99_1845].UnitVectorFunctionName, "_CRS_UnitVector");
			sprintf(CRSItems[SWEREF99_1845].PROJ4str, "+proj=tmerc +lat_0=0 +lon_0=18.75 +k=1 +x_0=150000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");
			CRSItems[SWEREF99_1845].Dimension=csdMeters;
			CRSItems[SWEREF99_1845].LatLonBounds.DoublePointRect(17.2500, 56.8500, 20.2500, 65.5800);
			CRSItems[SWEREF99_1845].XYBounds.DoublePointRect(58479.4774, 6304213.2147, 241520.5226, 7276832.4419);
			//LatLonAltToXYH[SWEREF99_1845]=LatLonAltToXYH_1845;
			//XYHToLatLonAlt[SWEREF99_1845]=XYHToLatLonAlt_1845;
			CRSItems[SWEREF99_1845].OptionsCount=0;
			break;
		case SWEREF99_2015:
			pj_2015=NULL;
			sprintf(CRSItems[SWEREF99_2015].GUID, "{61C650EE-F3ED-4c0b-9FAE-6DA03D102438}");
			sprintf(CRSItems[SWEREF99_2015].Prefix, "SE z2015");
			sprintf(CRSItems[SWEREF99_2015].Name, "Swedish SWEREF99 20 15");
			sprintf(CRSItems[SWEREF99_2015].Code, "csSWEREF99_2015");
			sprintf(CRSItems[SWEREF99_2015].EllipsoidName, "GRS 1980");
			sprintf(CRSItems[SWEREF99_2015].EPSG, "EPSG:3016");
			sprintf(CRSItems[SWEREF99_2015].LatLonAltToXYHFunctionName, "_LatLonAltToXYH_2015");
			sprintf(CRSItems[SWEREF99_2015].XYHToLatLonAltFunctionName, "_XYHToLatLonAlt_2015");
			sprintf(CRSItems[SWEREF99_2015].UnitVectorFunctionName, "_CRS_UnitVector");
			sprintf(CRSItems[SWEREF99_2015].PROJ4str, "+proj=tmerc +lat_0=0 +lon_0=20.25 +k=1 +x_0=150000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");
			CRSItems[SWEREF99_2015].Dimension=csdMeters;
			CRSItems[SWEREF99_2015].LatLonBounds.DoublePointRect(15.3800, 63.4000, 22.4800, 69.1000);
			CRSItems[SWEREF99_2015].XYBounds.DoublePointRect(-93218.3385, 7034909.8738, 261434.6246, 7676279.8691);
			//LatLonAltToXYH[SWEREF99_2015]=LatLonAltToXYH_2015;
			//XYHToLatLonAlt[SWEREF99_2015]=XYHToLatLonAlt_2015;
			CRSItems[SWEREF99_2015].OptionsCount=0;
			break;
		case SWEREF99_2145:
			pj_2145=NULL;
			sprintf(CRSItems[SWEREF99_2145].GUID, "{BECE1E1A-6CE5-424a-8FF2-47B0F5A1C2FC}");
			sprintf(CRSItems[SWEREF99_2145].Prefix, "SE z2145");
			sprintf(CRSItems[SWEREF99_2145].Name, "Swedish SWEREF99 21 45");
			sprintf(CRSItems[SWEREF99_2145].Code, "csSWEREF99_2145");
			sprintf(CRSItems[SWEREF99_2145].EllipsoidName, "GRS 1980");
			sprintf(CRSItems[SWEREF99_2145].EPSG, "EPSG:3017");
			sprintf(CRSItems[SWEREF99_2145].LatLonAltToXYHFunctionName, "_LatLonAltToXYH_2145");
			sprintf(CRSItems[SWEREF99_2145].XYHToLatLonAltFunctionName, "_XYHToLatLonAlt_2145");
			sprintf(CRSItems[SWEREF99_2145].UnitVectorFunctionName, "_CRS_UnitVector");
			sprintf(CRSItems[SWEREF99_2145].PROJ4str, "+proj=tmerc +lat_0=0 +lon_0=21.75 +k=1 +x_0=150000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");
			CRSItems[SWEREF99_2145].Dimension=csdMeters;
			CRSItems[SWEREF99_2145].LatLonBounds.DoublePointRect(19.35, 64.8, 22.5, 66.3);
			CRSItems[SWEREF99_2145].XYBounds.DoublePointRect(43872.59, 7213228.02, 183652.39, 7356493.75);
			//LatLonAltToXYH[SWEREF99_2145]=LatLonAltToXYH_2145;
			//XYHToLatLonAlt[SWEREF99_2145]=XYHToLatLonAlt_2145;
			CRSItems[SWEREF99_2145].OptionsCount=0;
			break;
		case SWEREF99_2315:
			pj_2315=NULL;
			sprintf(CRSItems[SWEREF99_2315].GUID, "{7A9516A8-FCBD-4318-BBD6-9F8A4C7A529E}");
			sprintf(CRSItems[SWEREF99_2315].Prefix, "SE z2315");
			sprintf(CRSItems[SWEREF99_2315].Name, "Swedish SWEREF99 23 15");
			sprintf(CRSItems[SWEREF99_2315].Code, "csSWEREF99_2315");
			sprintf(CRSItems[SWEREF99_2315].EllipsoidName, "GRS 1980");
			sprintf(CRSItems[SWEREF99_2315].EPSG, "EPSG:3018");
			sprintf(CRSItems[SWEREF99_2315].LatLonAltToXYHFunctionName, "_LatLonAltToXYH_2315");
			sprintf(CRSItems[SWEREF99_2315].XYHToLatLonAltFunctionName, "_XYHToLatLonAlt_2315");
			sprintf(CRSItems[SWEREF99_2315].UnitVectorFunctionName, "_CRS_UnitVector");
			sprintf(CRSItems[SWEREF99_2315].PROJ4str, "+proj=tmerc +lat_0=0 +lon_0=23.25 +k=1 +x_0=150000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");
			CRSItems[SWEREF99_2315].Dimension=csdMeters;
			CRSItems[SWEREF99_2315].LatLonBounds.DoublePointRect(20.8500, 65.5000, 24.1800, 68.4500);
			CRSItems[SWEREF99_2315].XYBounds.DoublePointRect(38920.7048, 7267405.2323, 193050.2460, 7597992.2419);
			//LatLonAltToXYH[SWEREF99_2315]=LatLonAltToXYH_2315;
			//XYHToLatLonAlt[SWEREF99_2315]=XYHToLatLonAlt_2315;
			CRSItems[SWEREF99_2315].OptionsCount=0;
			break;
		case CRSTotal:
			for(int i=0; i<CRSTotal; i++) PresetPlugIn(i);
			break;
	}
}
//---------------------------------------------------------------------------

void InitPlugIn(int Id)
{
	FreePlugIn(Id);
	switch((TCRSType)Id)
	{
		case SWEREF99_TM: pj_TM = pj_init_plus(CRSItems[SWEREF99_TM].PROJ4str);	break;
		case SWEREF99_1200:	pj_1200 = pj_init_plus(CRSItems[SWEREF99_1200].PROJ4str); break;
		case SWEREF99_1330:	pj_1330 = pj_init_plus(CRSItems[SWEREF99_1330].PROJ4str); break;
		case SWEREF99_1500:	pj_1500 = pj_init_plus(CRSItems[SWEREF99_1500].PROJ4str);	break;
		case SWEREF99_1630:	pj_1630 = pj_init_plus(CRSItems[SWEREF99_1630].PROJ4str); break;
		case SWEREF99_1800:	pj_1800 = pj_init_plus(CRSItems[SWEREF99_1800].PROJ4str); break;
		case SWEREF99_1415:	pj_1415 = pj_init_plus(CRSItems[SWEREF99_1415].PROJ4str); break;
		case SWEREF99_1545:	pj_1545 = pj_init_plus(CRSItems[SWEREF99_1545].PROJ4str); break;
		case SWEREF99_1715:	pj_1715 = pj_init_plus(CRSItems[SWEREF99_1715].PROJ4str); break;
		case SWEREF99_1845:	pj_1845 = pj_init_plus(CRSItems[SWEREF99_1845].PROJ4str); break;
		case SWEREF99_2015:	pj_2015 = pj_init_plus(CRSItems[SWEREF99_2015].PROJ4str); break;
		case SWEREF99_2145:	pj_2145 = pj_init_plus(CRSItems[SWEREF99_2145].PROJ4str); break;
		case SWEREF99_2315:	pj_2315 = pj_init_plus(CRSItems[SWEREF99_2315].PROJ4str); break;
		case CRSTotal:
			for(int i=0; i<CRSTotal; i++) InitPlugIn(i);
			break;
	}
}
//---------------------------------------------------------------------------

void FreePlugIn(int Id)
{
	switch((TCRSType)Id)
	{
		case SWEREF99_TM: if(pj_TM) pj_free(pj_TM); pj_TM=NULL; break;
		case SWEREF99_1200:	if(pj_1200)	pj_free(pj_1200); pj_1200=NULL; break;
		case SWEREF99_1330:	if(pj_1330) pj_free(pj_1330); pj_1330=NULL; break;
		case SWEREF99_1500: if(pj_1500) pj_free(pj_1500); pj_1500=NULL; break;
		case SWEREF99_1630:	if(pj_1630) pj_free(pj_1630); pj_1630=NULL; break;
		case SWEREF99_1800:	if(pj_1800) pj_free(pj_1800); pj_1800=NULL; break;
		case SWEREF99_1415: if(pj_1415) pj_free(pj_1415); pj_1415=NULL; break;
		case SWEREF99_1545: if(pj_1545) pj_free(pj_1545); pj_1545=NULL; break;
		case SWEREF99_1715: if(pj_1715) pj_free(pj_1715); pj_1715=NULL; break;
		case SWEREF99_1845: if(pj_1845) pj_free(pj_1845); pj_1845=NULL; break;
		case SWEREF99_2015: if(pj_2015) pj_free(pj_2015); pj_2015=NULL; break;
		case SWEREF99_2145:	if(pj_2145) pj_free(pj_2145); pj_2145=NULL; break;
		case SWEREF99_2315: if(pj_2315) pj_free(pj_2315); pj_2315=NULL; break;
		case CRSTotal:
			for(int i=0; i<CRSTotal; i++) FreePlugIn(i);
			break;
	}
}

//---------------------------------------------------------------------------
// User defined fuctions
//---------------------------------------------------------------------------
bool LatLonAltToXYH_pj(projPJ pj, double lat, double lon, double alt, double &x, double &y, double &h)
{
	if(pj_latlon_WGS84 && pj)
	{
		lat*=DEG_TO_RAD;
		lon*=DEG_TO_RAD;
		pj_transform(pj_latlon_WGS84, pj, 1, 1, &lon, &lat, &alt);
		x=lon;
		y=lat;
		h=alt;
		return true;
	}
	else return false;
}
//---------------------------------------------------------------------------

bool XYHToLatLonAlt_pj(projPJ pj, double x, double y, double h, double &lat, double &lon, double &alt)
{
	if(pj_latlon_WGS84 && pj)
	{
		pj_transform(pj, pj_latlon_WGS84, 1, 1, &x, &y, &h);
		lat=y/DEG_TO_RAD;
		lon=x/DEG_TO_RAD;
		alt=h;
		return true;
	}
	else return false;
}
//---------------------------------------------------------------------------

bool CRS_UnitVector(double lat, double lon, double alt, double &_vx, double &_vy)
{
	_vx=_vy=1.;
}
