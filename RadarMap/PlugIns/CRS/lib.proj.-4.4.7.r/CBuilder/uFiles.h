//=== File description =========================================================
/*!
\file uFiles.h
\brief Module <b>interface part</b><br> FileWriter and FileReader
 stream objects with type testing and casting.
\author  A. Weidauer; weidauer@huckfinn.de
\version 1.0-VI-2002
*/
#ifndef uFilesH
#define uFilesH
// --- Modeles needed ---------------------------------------------------------
#include <stdio.h>
#include <sys/stat.h>
#include "uDefs.h"

//------------------------------------------------------------------------------
//! Read a line form text input ignoring with # marked lines
/*! Read a line form text input ignoring with # marked lines
    hFile - FILE Variable, pChar - Buffer the string buffer and
    int - len the length of the buffer);
*/
void ReadLine(FILE *hFile, pChar Buffer, int len);

//---------------------------------------------------------------------------
//! FileWriter object 
class cFileWriter {
private:
 //! Filepointer 
 FILE*       fPort;
 //! Filename 
 pChar       fFileName;
 //! Writebuffer 
 pChar       fBuffer;
 //! Filestatus variable 
 struct stat fStatus;
 //! Buffer end pointer 
 tInteger fMaxBuffer;
 //! Buffer position pointer 
 tInteger fPosBuffer;
 //! send datas to buffer 
 void     WriteData(tChar Data);
public:
 //! standard constructor 
 cFileWriter  (const pChar aFileName, tInteger aBufferSize);
 //! standard destructor 
 ~cFileWriter ();
 //! obsolete by writing 
 tBoolean Eof();
 //! obsolete by writing 
 tCardinal Size();
 //! current file position 
 tInteger  Position();
 //! flush file buffer 
 tInteger Flush();
 //! write a data set of given size to the buffer 
 tInteger WriteBuf(Pointer Data, tCardinal Size);
 //! write bool 
 tInteger Write(tBoolean Data);
 //! write byte 
 tInteger Write(tByte Data);
 //! write char 
 tInteger Write(tChar Data);
 //! write word 2byte
 tInteger Write(tWord Data);
 //! write word 4byte 
 tInteger Write(tCardinal Data);
 //! write signed int 2byte 
 tInteger Write(tShortInteger Data);
 //! write signed int 4byte 
 tInteger Write(tInteger Data);
 //! write float 4byte 
 tInteger Write(tFloat Data);
 //! write float 8byte 
 tInteger Write(tDouble Data);
 //! write float ??byte 
 tInteger Write(tLargeDouble Data);
 //! write string 
 tInteger Write(pChar Data);
 //! textual output 
 void Debug(const pChar Prefix);
};
typedef cFileWriter* rFileWriter;

//---------------------------------------------------------------------------
//! FileReader object 
class cFileReader {
private:
 //! Filepointer 
 FILE*       fPort;
 //! Filestatus 
 struct stat fStatus;
 //! Filename 
 pChar       fFileName;
 //! Filebuffer 
 pChar       fBuffer;
 //! File buffer size
 tCardinal   fMaxBuffer;
 //! File current buffer size
 tCardinal   fInBuffer;
 //! File current buffer position
 tInteger    fPosBuffer;
 //! ?? 
 tInteger Fill();
 //! Read 1byte form buffer 
 tChar    ReadData();
public:
 //! Standard constructor 
 cFileReader  (const pChar aFileName, tInteger aBufferSize);
 //! Standard destructor 
 ~cFileReader ();
 //! Status end of file 
 tBoolean Eof();
 //! Move to file position 
 tBoolean Seek(tCardinal aPosition);
 //! Reset file pointer 
 tBoolean Reset();
 //! Total file size 
 tCardinal Size();
 //! Current file position 
 tInteger Position();
 //! Read adata buffer of a given size 
 tInteger ReadBuf(Pointer Data, tCardinal Size);
 //! Read bool 
 tInteger Read(tBoolean& Data);
 //! Read byte 
 tInteger Read(tByte& Data);
 //! Read char 
 tInteger Read(tChar& Data);
 //! Read word 2Byte 
 tInteger Read(tWord& Data);
 //! Read word 4Byte 
 tInteger Read(tCardinal& Data);
 //! Read signed int 2Byte 
 tInteger Read(tShortInteger& Data);
 //! Read signed int 4Byte 
 tInteger Read(tInteger& Data);
 //! Read float 4Byte 
 tInteger Read(tFloat& Data);
 //! Read float 8Byte 
 tInteger Read(tDouble& Data);
 //! Read float ??Byte 
 tInteger Read(tLargeDouble& Data);
 //! Read pChar 
 tInteger Read(pChar& Data);
 //! Textual output 
 void Debug(const pChar Prefix);
};
typedef cFileReader* rFileReader;

//------------------------------------------------------------------------------
// EndOf
//------------------------------------------------------------------------------

#endif
