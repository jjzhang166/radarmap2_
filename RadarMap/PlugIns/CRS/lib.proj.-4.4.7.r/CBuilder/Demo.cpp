//---------------------------------------------------------------------------
// Proj API for PROJ 4.4.5 Container Demo
// (c) VI/ 2002 Alexander Weidauer TriplexWare
//------------------------------------------------------------------------------
//   COPYRIGHT
//------------------------------------------------------------------------------
/*
 The contents of this files are used with permission, subject to
 the Mozilla Public License Version 1.1 (the "License"); you may
 not use this file except in compliance with the License. You may
 obtain a copy of the License at

 http://www.mozilla.org/MPL/MPL-1.1.html

 Software distributed under the License is distributed on an
 "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
 implied. See the License for the specific language governing
 rights and limitations under the License.

 The Initial Developer of the Original Code Proj Code is Frank Warmerdam,
 Portions for Delphi Code as well Modifications to make the Code
 industrial save created by Alexander Weidauer .
 Copyright (C) 2002 Alexander Weidauer.
 All Rights Reserved.

 Contributor A. Weidauer, TriplexWare
*/
//---------------------------------------------------------------------------
#pragma hdrstop
#include <condefs.h>
#include <stdio.h>
#include "uCoordinates.h"
//---------------------------------------------------------------------------
USEUNIT("uCoordinates.cpp");
USEUNIT("uDefs.cpp");
//---------------------------------------------------------------------------
#pragma link "proj447.lib"
#pragma link "cw32.lib"

//---------------------------------------------------------------------------
void getenter()
{ printf("-- next --"); getchar(); }
//---------------------------------------------------------------------------
int main()
{
 tDouble x,y,lo,la,lo1,la1,lo2,la2,ddeg,dkm,az;
 printf("--- test proj library ---\n\n");
 char Datum[]="+proj=tmerc +ellps=WGS84 +lon_0=12 +lat_0=12";
 rCoordinateTransformer TRFM = new cCoordinateTransformer(".."); // ".."where is the NAD directory
 TRFM->SetPlainLLSystem(Datum);
 printf("\n");
 lo=12;la=12;
 TRFM->cvsLLToPlain(lo,la,x,y);
 printf("LLtoPlain> input: lon=%f [deg] lat=%f [deg] \n",lo,la);
 printf("LLtoPlain> output: x=%f [km] y=%f [km] \n\n",x,y);
 TRFM->cvsPlainToLL(x,y,lo,la);
 printf("PlaintoLL> input: x=%f [km] y=%f [km] \n",x,y);
 printf("PlaintoLL> output: lon=%f [deg] lat=%f [deg] \n\n",lo,la);
 lo=12.5;la=12.5;
 TRFM->cvsLLToPlain(lo,la,x,y);
 printf("LLtoPlain> input: lon=%f [deg] lat=%f [deg] \n",lo,la);
 printf("LLtoPlain> output: x=%f [km] y=%f [km] \n\n",x,y);
 TRFM->cvsPlainToLL(x,y,lo,la);
 printf("PlaintoLL> input: x=%f [km] y=%f [km] \n",x,y);
 printf("PlaintoLL> output: lon=%f [deg] lat=%f [deg] \n\n",lo,la);
 lo1=0;la1=0; lo2=1;la2=0;
 GeodeticDistance(lo1,la1,lo2,la2,ddeg,dkm,az);
 printf("Geod> input: lon1=%f [deg] lat1=%f [deg]\n",lo1,la1);
 printf("Geod> input: lon2=%f [deg] lat2=%f [deg]\n",lo2,la2);
 printf("Geod> output: ddeg=%f [deg] dkm=%f [km] az=%f [deg]\n\n",ddeg,dkm,az);
 lo1=0;la1=0; lo2=-1;la2=0;
 GeodeticDistance(lo1,la1,lo2,la2,ddeg,dkm,az);
 printf("Geod> input: lon1=%f [deg] lat1=%f [deg]\n",lo1,la1);
 printf("Geod> input: lon2=%f [deg] lat2=%f [deg]\n",lo2,la2);
 printf("Geod> output: ddeg=%f [deg] dkm=%f [km] az=%f [deg]\n\n",ddeg,dkm,az);
 lo1=0;la1=0; lo2=0;la2=1;
 GeodeticDistance(lo1,la1,lo2,la2,ddeg,dkm,az);
 printf("Geod> input: lon1=%f [deg] lat1=%f [deg]\n",lo1,la1);
 printf("Geod> input: lon2=%f [deg] lat2=%f [deg]\n",lo2,la2);
 printf("Geod> output: ddeg=%f [deg] dkm=%f [km] az=%f [deg]\n\n",ddeg,dkm,az);
 lo1=0;la1=0; lo2=0;la2=-1;
 GeodeticDistance(lo1,la1,lo2,la2,ddeg,dkm,az);
 printf("Geod> input: lon1=%f [deg] lat1=%f [deg]\n",lo1,la1);
 printf("Geod> input: lon2=%f [deg] lat2=%f [deg]\n",lo2,la2);
 printf("Geod> output: ddeg=%f [deg] dkm=%f [km] az=%f [deg]\n\n",ddeg,dkm,az);
 lo1=0;la1=0; lo2=90;la2=0;
 GeodeticDistance(lo1,la1,lo2,la2,ddeg,dkm,az);
 printf("Geod> input: lon1=%f [deg] lat1=%f [deg]\n",lo1,la1);
 printf("Geod> input: lon2=%f [deg] lat2=%f [deg]\n",lo2,la2);
 printf("Geod> output: ddeg=%f [deg] dkm=%f [km] az=%f [deg]\n\n",ddeg,dkm,az);
 lo1=0;la1=0; lo2=0;la2=90;
 GeodeticDistance(lo1,la1,lo2,la2,ddeg,dkm,az);
 printf("Geod> input: lon1=%f [deg] lat1=%f [deg]\n",lo1,la1);
 printf("Geod> input: lon2=%f [deg] lat2=%f [deg]\n",lo2,la2);
 printf("Geod> output: ddeg=%f [deg] dkm=%f [km] az=%f [deg]\n\n",ddeg,dkm,az);
 delete TRFM;
 getenter();
 return 0;
}

