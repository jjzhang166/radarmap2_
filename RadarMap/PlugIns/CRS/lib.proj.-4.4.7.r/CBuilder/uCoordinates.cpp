//=== File description =========================================================
/*!
\file uCoordinates.cpp
\brief Module  <b>implementation part</b><br> to handle plain and
       spheroid coordinates.
 The routine GeodeticDistance is applied from the FORTRAN77 project hypoDD
 written by Feliux Waldhauser.
\author  A. Weidauer; weidauer@huckfinn.de
\version 1.0-VI-2002
*/
//---------------------------------------------------------------------------
#pragma hdrstop

#include "uCoordinates.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

// --- Modules -----------------------------------------------------------------
#include "uCoordinates.h"

static char *aProjDataPath = NULL;

static const char *CoordinateTransformerProjFileFinder( const char *aFile )
{
     static char *aLastPath = NULL;

     if( aProjDataPath == NULL )
         return aFile;

     if( aLastPath != NULL )
        free( aLastPath );
     aLastPath = (char *) malloc(strlen(aProjDataPath)+strlen(aFile)+2);
     strcpy( aLastPath, aProjDataPath );
     strcat( aLastPath, "\\" );
     strcat( aLastPath, aFile );

     return aLastPath;
}
/*
cCoordinateTransformer::cCoordinateTransformer(const char* aDataPath):
  cObject() {

  if (aDataPath!=NULL) {
   DataPath =(char*) malloc(strlen(aDataPath)+1);
   strcpy(DataPath,aDataPath);
  }

   PlainLL=NULL;  Src=NULL;  Dst=NULL;
   printf("..open proj 4.4.5 tool NADPATH =\"%s\" \n",
    DataPath);

   if( aDataPath != NULL )   {
      if( aProjDataPath != NULL )
          free( aProjDataPath );
      aProjDataPath = strdup(aDataPath);
   }
  pj_set_finder( CoordinateTransformerProjFileFinder );
  PlainLL=NULL;  Src=NULL;  Dst=NULL;


  }
*/

// --- Classes ---------------------------------------------------------------
 cCoordinateTransformer::cCoordinateTransformer(const char* aDataPath): cObject() {
  if (aDataPath!=NULL) {
   DataPath =(char*) malloc(strlen(aDataPath)+1);
   strcpy(DataPath,aDataPath);
  }
  printf("..open proj 4.4.5 tool NADPATH =\"%s\" \n", DataPath);
 }

 //-----------------------------------------------------------------------
 cCoordinateTransformer::~cCoordinateTransformer() {
  if (DataPath!=NULL) {free (DataPath);}
  if (PlainLL!=NULL) {  pj_free(PlainLL); }
  if (Src!=NULL)  {  pj_free(Src); }
  if (Dst!=NULL) {  pj_free(Dst); }
 }
//-----------------------------------------------------------------------
void cCoordinateTransformer::SetCoordSystem(const char* aProjection,
                                            projPJ& pj)
{ char buffer[512];
  if (pj!=NULL) { pj_free(pj);}
  pj = pj_init_plus_path(aProjection,DataPath);
  if (pj==NULL) {
   sprintf(buffer,"Cannot set projection \"%s\" !",aProjection);
   FatalError(-1,buffer);
  }
  printf("..set coordiate system parameters=\"%s\" \n",
   aProjection);
}

//-----------------------------------------------------------------------
void cCoordinateTransformer::SetPlainLLSystem(const char* aProjection)
{ SetCoordSystem(aProjection,PlainLL); }
//-----------------------------------------------------------------------
void cCoordinateTransformer::SetPlainSrcSystem(const char* aProjection)
{ SetCoordSystem(aProjection,Src); }
//-----------------------------------------------------------------------
void cCoordinateTransformer::SetPlainDestSystem(const char* aProjection)
{ SetCoordSystem(aProjection,Dst); }		

 //-----------------------------------------------------------------------
 void cCoordinateTransformer::cvsLLToPlain(tDouble Longitude,
                    tDouble Latitude,		
                    tDouble& XKM,
		    tDouble& YKM)    
{
    if (PlainLL==NULL) {
      FatalError(-1," lon lat to plain transformer ! Coordinate system not specified !");
    }
    p.u = DEG_TO_RAD * Longitude;
    p.v = DEG_TO_RAD * Latitude;
    p = pj_fwd(p, PlainLL);
    XKM = p.u/1000;
    YKM = p.v/1000;
}		
 //-----------------------------------------------------------------------
 void cCoordinateTransformer::cvsPlainToLL(tDouble XKM,
                    tDouble YKM,
                    tDouble& Longitude,
                    tDouble& Latitude)
 {
    if (PlainLL==NULL) {
      FatalError(-1," lon lat to plain transformer ! Coordinate system not specified !");
    }
    p.u = XKM*1000;
    p.v = YKM*1000;
    p = pj_inv(p, PlainLL);
    XKM = p.u * RAD_TO_DEG;
    YKM = p.v * RAD_TO_DEG;
 }
 //-----------------------------------------------------------------------
 void cCoordinateTransformer::cvsPlainToPlain(tDouble XSourceKM, tDouble YSourceKM,
                                              tDouble XDestKM,   tDouble YDestKM)
 {
    
    if (Src==NULL) {
      FatalError(-1," lon lat to plain transformer ! Source coordinate system not specified !");
    }
    if (Dst==NULL) {
      FatalError(-1," lon lat to plain transformer ! Source coordinate system not specified !");
    }
    p.u = XSourceKM*1000.0;
    p.v = YSourceKM*1000.0;
    p=pj_inv(p,Src);
    p=pj_fwd(p,Dst);
    XDestKM = p.u *1000.0;
    YDestKM = p.v *1000.0;
 }
 
		
// --- Functions ---------------------------------------------------------------

//------------------------------------------------------------------------------
//! Converstion radiant to decimal degree 
tDouble  cvsRadToDeg(tDouble aRadiant) {
 return aRadiant*dIRadiant;
}

//------------------------------------------------------------------------------
//! Converstion decimal degree to radiant
tDouble  cvsDegToRad(tDouble aDegree) {
  return aDegree*dRadiant;
}

//------------------------------------------------------------------------------
//! classical euclidic length
tDouble  L2Length(tDouble SX,tDouble SY, tDouble EX,tDouble EY)
{ return L2Norm(SX-EX,SY-EY);}
//------------------------------------------------------------------------------
//! classical euclidic length sqraes
tDouble  L2SQRLength(tDouble SX,tDouble SY, tDouble EX,tDouble EY)
{tDouble X = (SX-EX);
 tDouble Y = (SY-EY);
 return (X*X+Y*Y);
}

//------------------------------------------------------------------------------
/*! Euclidian norm */
tDouble  L2Norm(tDouble PlainX,tDouble PlainY) {
 return sqrt(PlainX*PlainX+PlainY*PlainY);
}


//------------------------------------------------------------------------------
tDouble TriArea(tDouble AX, tDouble AY,
                    tDouble BX, tDouble BY,
                    tDouble CX, tDouble CY)
{
  return (AX+CY)/2*(BX-AX)+(BY+CY)/2*(CX-BX)+(AY+CY)/2*(AX-CX);
}

//------------------------------------------------------------------------------
tDouble TriCos (tDouble SX, tDouble SY,
                tDouble CX, tDouble CY,
                tDouble EX, tDouble EY)
{ tDouble a,b,c; tDouble Result =0;
  a = L2Length(SX,SY,CX,CY);
  b = L2Length(CX,CY,EX,EY);
  c = L2Length(EX,EY,SX,SY);
  if (a * b == 0) Result = 9999;
   else Result = (c*c-a*a-b*b)/(-2*a*b);
  return Result;
}


//------------------------------------------------------------------------------
tDouble TriAngle(tDouble  SX, tDouble SY,
                    tDouble CX, tDouble CY,
                    tDouble EX, tDouble EY)
{
 tDouble AC,ACX,ACG,TR;
 tDouble Result= 9999;
  TR  = TriArea(SX,SY,CX,CY,EX,EY);
  AC  = TriCos (SX,SY,CX,CY,EX,EY);
  ACX = acos(AC);
  ACG = ACX / dPI * 180;
  if (TR >= 0)    Result = +ACG;
  if (TR <0)      Result = 360-ACG;
  if (AC == 9999) Result = 9999;
  return Result;
}


//------------------------------------------------------------------------------
/*! Converstion decimal minutes to decimal degrees
\param aDegree [�] the degree 0..360 in upper case 30
\param aMinute decimal minutes, in upper case 30.0
\param aDecDegree <b>return</b> [deg] like this 30.5
*/
tDouble  cvsDecMinToDecDeg(tInteger aDegree, tDouble aMinute) {
  return aDegree+aMinute/60;
}

//------------------------------------------------------------------------------
/*!
Converstion decimal degreesto decimal minutes
\param aDecDegree [deg] like this 30.5
\param aDegree [�] <b>return</b> the degree 0..360 in upper case 30
\param aMinute <b>return</b> decimal minutes, in upper case 30.0
*/
void  cvsDecDegToDecMin(tDouble aDecDegree, tInteger& aDegree, tDouble& aMinute){
 aDegree = (tInteger) floor(aDecDegree);
 aMinute = (aDecDegree-(tDouble) aDegree)*60.0;
}

//------------------------------------------------------------------------------
/*!
The sphere is referenced to WGS 72
\param LatA Latitude of LL Point A in decimal degree 
\param LonA Longitude of LL Point A in decimal degree 
\param LatB Latitude of LL Point B in decimal degree 
\param LonB Longitude of LL Point B in decimal degree 
\param DeltaDEG <b>return</b> Distance P(AB) in decimal degree
\param DeltaKM <b>return</b> Distance P(AB) in [km]
\param Azimut P(AB) in decimal degree
*/
void GeodeticDistance(
              tDouble LonA,tDouble LatA, /* LL Point A in decimal degree  */
              tDouble LonB,tDouble LatB, /* LL Point B in decimal degree  */
	      tDouble& DeltaDEG,         /* Dictance AB in decimal degree */
              tDouble& DeltaKM,          /* Dictance AB in kilometer      */ 
	      tDouble& AzimutDEG)        /* Azimut in kilometer           */
 {
  tDouble TanA,TanB,GeoA,GeoB,ColA,ColB;
  tDouble Sin_ColA,Cos_ColA,Sin_ColB,Cos_ColB;
  tDouble DLon,CosDelta,DeltaRad,xtop,xden;
  tDouble Cos_DLon,AzimutRad,CoLat,Cos_CoLat;

  // Conversation to radiants
  LatA=cvsDegToRad(LatA); LonA=cvsDegToRad(LonA);
  LatB=cvsDegToRad(LatB); LonB=cvsDegToRad(LonB);
  
  
  /* Determning latitutes in co latitudes Point A and Sine & Cosine values */
  TanA=tan(LatA)*dFlat;  GeoA=atan(TanA);
  ColA=dPI/2-GeoA;  Sin_ColA=sin(ColA); Cos_ColA=cos(ColA);
  
  /* Determning latitutes in co latitudes Point A and Sine & Cosine values */
  TanB=tan(LatB)*dFlat; GeoB=atan(TanB); 
  ColB=dPI/2-GeoB; Sin_ColB=sin(ColB); Cos_ColB=cos(ColB);

  // Determening Distance  between A and B  
  DLon=LonB-LonA;  Cos_DLon=cos(DLon);
  CosDelta=Sin_ColA*Sin_ColB*Cos_DLon
          +Cos_ColA*Cos_ColB;
  DeltaRad=acos(CosDelta);

  // Determing azimut from A to B
  xtop=sin(DLon);
  xden=Sin_ColA/tan(ColB)-cos(DLon)*Cos_ColA;
  AzimutRad=atan2(xtop,xden); 	  

  DeltaDEG=cvsRadToDeg(DeltaRad);
  AzimutDEG=cvsRadToDeg(AzimutRad);
  if (AzimutRad<0.0) {  AzimutDEG=AzimutDEG+360.0; }
  
  // Determening distance in kilometer
  CoLat=dPI/2-(LatA+LatB)/2;
  Cos_CoLat=cos(CoLat);   
  DeltaKM = DeltaRad *
   ((1/3 - Cos_CoLat*Cos_CoLat) * 0.00335278 + 1.0) * dEarthRadius;
  }  

//------------------------------------------------------------------------------
// EndOf
//------------------------------------------------------------------------------

