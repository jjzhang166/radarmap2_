//=== File description =====================================================
/*!
\file uDefs.h
\brief Module <b>implementation part</b><br> defines a unique nomenclature
for basic data types.
\author  A. Weidauer; weidauer@huckfinn.de
\version 1.0-VI-2002
*/
//---------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
//---------------------------------------------------------------------------
//! Errormessage Routine for fatal errors Numbe is the exit code, Message (dito)
void FatalError(int Number, const char* Message) {
 printf("FatalError %d !\n %s !\n",Number,Message);
 exit(Number);
}

