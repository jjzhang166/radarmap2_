//=== File description =========================================================
/*!
\file uObject.h
\brief Module <b>interface part</b><br> defines a the base set of objects.
\author  A. Weidauer; weidauer@huckfinn.de
\version 1.0-VI-2002
*/
//---------------------------------------------------------------------------
#ifndef uObjectH
#define uObjectH
#include "uDefs.h"
#include "uFiles.h"
//=============================================================================
//! Base object class for generic object containers.
/*! A simple example for a silly integer class:
<PRE>
class cInteger: public cObject {
public:
    int    Data;

    cInteger(): cObject(), Data(0)  {}
    cInteger(int aValue): cObject(), Data(aValue)  {}
    virtual ~cInteger()  {}
    virtual void Assign (cObject* aObject) {
      cInteger* Ref;
      Ref = (cInteger*) aObject;
      Data= Ref->Data;
    };
    virtual void Debug() { printf("%d",Data); } ;
};
</PRE>
*/
class cObject{
public:
  //! Standard constructor
  cObject() {};
  //! Standard destructor
  virtual ~cObject() {};
  //! Assignment of  contained datas.
  virtual void Assign(cObject* aObject) {};
  //! Print out contained datas.
  virtual void Debug(const char* Prefix) {};
  //! Standard Reader procedure
  virtual tInteger  Read(rFileReader Port) {return 0;};
  //! Standard Writer procedure
  virtual tInteger  Write(rFileWriter Port) {return 0;};

};
//! Pointer referenc to a generic object
typedef cObject* rObject;

//=============================================================================

#endif //uObjectH
//----------------------------------------------------------------------------------------
// EndOf
//----------------------------------------------------------------------------------------
