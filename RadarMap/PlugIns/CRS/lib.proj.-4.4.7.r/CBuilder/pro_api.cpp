/*!
 \file proj_api.c
 \brief Purpose:  Public (application) include file for PROJ.4 API, and constants.
 \author:   Frank Warmerdam, <warmerdam@pobox.com>
 \version PROJ.4 v 1.5 2002/01/09 14:36:22 warmerda Exp $
 \par Copyright:
  Copyright (c) 2001, Frank Warmerdam <warmerdam@pobox.com>
 \par
  Permission is hereby granted, free of charge, to any person obtaining a
  copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation
  the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the
  Software is furnished to do so, subject to the following conditions:
 \par
  The above copyright notice and this permission notice shall be included
  in all copies or substantial portions of the Software.
 \par
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
  DEALINGS IN THE SOFTWARE.
 \par ..this file should handle routines of compolex library
 \sa http://www.remotesensing.org
 */
