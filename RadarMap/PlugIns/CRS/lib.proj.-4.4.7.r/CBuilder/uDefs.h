//=== File description =========================================================
/*! 
\file uDefs.h
\brief Module <b>interface part</b><br> defines a unique nomenclature
for basic data types.
\author  A. Weidauer; weidauer@huckfinn.de
\version 1.0-VI-2002
*/
//-------------------------------------------------------------------------------
#ifndef uConstH
#define uConstH


//--- Modules -------------------------------------------------------------------
#include <stdlib.h>

//--- Constants -----------------------------------------------------------------
//! NULL Pointer 1. Variant
#define nil  0
//! NULL Pointer 2. Variant
#define NIL  0
//! Item nof found index
#define dIndexNIL -1
//--- Types ---------------------------------------------------------------------
//! Bool token in bin file
#define dBOOLEAN_TK       'B'
//! Bool token in bin file
#define dBOOL_TK          'B'
//! Char token in bin file
#define dCHAR_TK          'C'

//! Byte token in bin file
#define dBYTE_TK          'b'
//! 2Byte Word token in bin file
#define dWORD_TK          'w'
//! 4Byte Word token in bin file
#define dCARDINAL_TK      'c'

//! 2Byte Int token in bin file
#define dSHORTINT_TK      's'
//! 4Byte Int token in bin file
#define dINTEGER_TK       'i'
//! ??8Byte Int token in bin file
#define dLONGINT_TK       'l'

//! 4Byte Float token in bin file
#define dFLOAT_TK         'f'
//! 8Byte Float token in bin file
#define dDOUBLE_TK        'd'
//! ??Byte Float token in bin file
#define dLARGEDOUBLE_TK   'e'

//! String token in bin file
#define dPCHAR_TK         'S'
//! String token in bin file
#define dCHARARRAY_TK     'S'

//! Free token in bin file
#define dFREE_TK          180

//! common pointer type
typedef void*            Pointer;

//! common boolean type (false/true)
typedef bool             tBoolean;
//! Numeric text for bool true
#define dONE   "1"
//! Numeric text for bool false
#define dZERO  "0"
//! Logic text for bool true
#define dTRUE  "true"
//! Logic text for bool false
#define dFALSE "false"
//! Answer text for bool true
#define dYES   "yes"
//! Answer text for bool fasle
#define dNO    "no"
//! Switch text for bool true
#define dON    "on"
//! Switch text for bool false
#define dOFF   "off"
//! Max of macro
#define max(A,B) ((A)>(B) ? (A):(B))
//! Min of macro
#define min(A,B) ((A)<(B) ? (A):(B))

//! 32 bit real type
typedef float            tFloat;
//! 64 bit real type
typedef double           tDouble;
//! 128 bit real type
typedef long double      tLargeDouble;
//! 128 bit real type
typedef long double      tLongDouble;

//! signed 16 bit integer type
typedef short int        tShortInteger;
//! signed 16 bit integer type
typedef short int        tShortInt;
//! signed 32 bit integer type
typedef long int         tInteger;

/* long long doesnt work every where
//! signed 64 bit integer type
typedef long int         tLongInteger;
//! signed 64 bit integer type
typedef long int         tLongInt;
*/

//! unsigned 16 bit integer type
typedef unsigned short int tWord;
//! unsigned 32 bit integer type
typedef unsigned long int tCardinal;
//! unsigned 16 bit integer type
typedef unsigned char    tByte;
//! unsigned 8 bit integer type like byte
typedef char    tChar;

//! pointer to  32 bit real type
typedef tFloat*           pFloat;
//! pointer to 64 bit real type
typedef tDouble*          pDouble;
//! pointer to 128 bit real type
typedef tLargeDouble*    pLargeDouble;
//! pointer to 128 bit real type
typedef tLongDouble*     pLongDouble;

//! pointer to signed 16 bit integer type
typedef tShortInteger     pShortInteger;
//! pointer to signed 32 bit integer type
typedef tInteger         pInteger;

// ! pointer to signed 64 bit integer type
// typedef tLongInteger*    pLongInteger;

//! pointer to unsigned 32 bit integer type
typedef tWord*   pWord;
//! pointer to unsigned 64 bit integer type
typedef tCardinal*   pCardinal;
//! pointer to unsigned 16 bit integer type
typedef tByte*  pByte;
//! pointer to unsigned 8 bit integer type like byte
typedef tChar*   pChar;

//! pointer to a chracter array
typedef const char*    pConstChar;

//! open array to a 32 bit float
typedef tFloat*    tFloatArray;
//! open array to a 64 bit float
typedef tDouble*   tDoubleArray;
//! open array to a 128 bit float
typedef tLargeDouble*    tLargeDoubleArray;

//! open array to a 32 bit signed integer
typedef tInteger*  tIntegerArray;

//! open array to a 16 bit unsigned integer
typedef tWord*    tWordArray;
//! open array to a 32 bit unsigned integer
typedef tCardinal*   tCardinalArray;
//! open array to a 16 bit unsigned integer
typedef tByte*  tByteArray;
//! open array to a 8 bit unsigned integer like a byte
typedef tChar*   tCharArray;

typedef tInteger (*fListSortCompare)(void * aObject, void * bObject) ;

#define BCC 1 

#define BITSPERBYTE 8
#define MAXSHORT    0x7fff

#define MAXINT      0x7fffffff
#define HIBITS      0x80000000
#define HIBITI      0x80000000

#define MAXLONG     0x7fffffff
#define HIBITL      0x80000000

#define DMAXEXP     308
#define FMAXEXP     38
#define DMINEXP     -307
#define FMINEXP     -37

#define MAXDOUBLE   1.7976931348623158E+308
#define MAXFLOAT    3.40282347E+38F
#define MINDOUBLE   2.2250738585072014E-308
#define MINFLOAT    1.17549435E-38F
#define MAXLDOUBLE  1.1897314953572317649E+4932L
#define MINLDOUBLE  3.362103143112094E-4917L /* This isn't completely
                                                accurate, but it will do for
                                                now. The real value should
                                                be: 3.362103143112094E-4932L */

#define DSIGNIF     53
#define FSIGNIF     24

#define DMAXPOWTWO  0x3FF
#define FMAXPOWTWO  0x7F
#define DEXPLEN     11
#define FEXPLEN     8
#define EXPBASE     2
#define IEEE        1
#define LENBASE     1
#define HIDDENBIT   1
#define LN_MAXDOUBLE    7.0978E+2
#define LN_MINDOUBLE    -7.0840E+2

//--- Functions ----------------------------------------------------------------
//! Errormessage Routine for fatal errors Numbe is the exit code, Message (dito)
void FatalError(int Number, const char* Message);

//------------------------------------------------------------------------------
// EndOf
//------------------------------------------------------------------------------
#endif // uConstH

