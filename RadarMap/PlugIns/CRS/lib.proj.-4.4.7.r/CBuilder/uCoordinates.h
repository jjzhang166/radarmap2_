//=== File description =========================================================
/*!
\file uCoordinates.h
\brief Module  <b>interface part</b><br> to handle plain and
       spheroid coordinates.
 The routine GeodeticDistance is applied from the FORTRAN77 project hypoDD
 written by Felix Waldhauser.
\author  A. Weidauer; weidauer@huckfinn.de
\version 1.0-VI-2002
*/
//---------------------------------------------------------------------------
#ifndef uCoordinatesH
#define uCoordinatesH
// --- Modules ---------------------------------------------------------------------------------
#include <string.h>
#include <math.h>
#include "uDefs.h"
#include "uObject.h"
#include "uProjApi447.h"
// --- Constants ---------------------------------------------------------------
//! constant &PI;
#define dPI           3.1418926
//! Radiant constant
#define dRadiant   dPI/180.0
//! invers Radiant
#define dIRadiant      180.0/dPI
//! Earth radius given by WGS 72
#define dEarthRadius  6378.135
//! Earth abaltation parameter given by WGS 72
#define dEarthFlat     298.26
//! unknwon use by Geodetic from Felix. Waldhauser hypoDD
#define dFlat            0.993291


//----------------------------------------------------------------------------------------------
//! Class container to hole LL to XY and inverse transformations
/*! Class container to hole LL to XY and invers transformations via
    encapsulation of the proj 4.4.5 geodetic and geographic library.
   See www.remotesensing.org! */
class cCoordinateTransformer : private cObject {
private:
 //! Path to Proj ellipsoid and projection parameter database! 
 char* DataPath;
 //! help variable polar coordinates 
 projUV p;
 //! record of the LL to XY transformation parameter.  
 projPJ PlainLL;
 //! record of the inter coordinate system transformation source parameter.
 projPJ Src;
 //! record of the inter coordinate system transformation destination parameter.  
 projPJ Dst;
 //! initialize a coordinate system 
 void SetCoordSystem(const char* aProjection, projPJ& pj);
 public:
 //! Standard constructor 
 cCoordinateTransformer(const char* aDataPath);
 //! Standard destructor 
 ~cCoordinateTransformer();
 //! Initialize the LL to XY transformation parameter.  
 void SetPlainLLSystem(const char* aProjection);
 //! Initialize record of the inter coordinate system transformation source parameter.
 void SetPlainSrcSystem(const char* aProjection);
 //! Initialize record of the inter coordinate system transformation destination parameter.  
 void SetPlainDestSystem(const char* aProjection);
 //! Conversation LL to XY 
 void cvsLLToPlain (tDouble Longitude,
                    tDouble Latitude,
                    tDouble& XDistanceKM,
		    tDouble& YDistanceKM);
 //! Conversation XY to LL 
 void cvsPlainToLL(tDouble XDistanceKM,
                    tDouble YDistanceKM,
                    tDouble& Longitude,
                    tDouble& Latitude);

 //! Conversation inter coordinate systems XY to XY 
 void cvsPlainToPlain(tDouble XSourceKM, tDouble YSourceKM,
                      tDouble XDestKM,   tDouble YDestKM);

};
//! Pointer to a transsformer object 
typedef  cCoordinateTransformer*  rCoordinateTransformer;

//! Unit type descriptor 
typedef enum { tuDecimalDegree, tuRadiant, tuMeter, tuKilometer } tCoordUnits;

// --- Functions ---------------------------------------------------------------
//! Area of a traingle - positiv cw orientation, ccw negative oreintation 
tDouble TriArea(tDouble AX, tDouble AY, tDouble BX, tDouble BY, tDouble CX, tDouble CY);
//! Angle between pointe SCE means start-center-end 
tDouble TriAngle(tDouble  SX, tDouble SY, tDouble CX, tDouble CY, tDouble EX, tDouble EY);
//! Cosine between pointe SCE means start-center-end 
tDouble TriCos(tDouble SX, tDouble SY, tDouble CX, tDouble CY, tDouble EX, tDouble EY);

//! Converstion radiant to decimal degree
tDouble  cvsRadToDeg(tDouble aRadiant);
//! Converstion decimal degree to radiant
tDouble  cvsDegToRad(tDouble aDegree);
//! Converstion decimal minutes to decimal degrees
tDouble  cvsDecMinToDecDeg(tInteger aDegree, tDouble aMinute);
//! Converstion decimal degrees to decimal minutes
void     cvsDecDegToDecMin(tDouble aDecDegree, tInteger& aDegree, tDouble& aMinute);
//! classical euclidic length
tDouble  L2Length(tDouble SX,tDouble SY, tDouble EX,tDouble EY);
//! classical euclidic length sqraes
tDouble  L2SQRLength(tDouble SX,tDouble SY, tDouble EX,tDouble EY);
//! classical euclidic norm
tDouble  L2Norm(tDouble PlainX,tDouble PlainY);
//! Distance and Azimut for two given pointes on a sphere from hypoDd Felix Waldhauser
/*!
The sphere is referenced to WGS 72 radius
\param LonA Longitude of LL Point A in decimal degree
\param LatA Latitude of LL Point A in decimal degree
\param LonB Longitude of LL Point B in decimal degree
\param LatB Latitude of LL Point B in decimal degree
\param DeltaDEG <b>return</b> Distance P(AB) in decimal degree
\param DeltaKM <b>return</b> Distance P(AB) in [km]
\param Azimut <b>return</b> P(AB) in decimal degree
*/
void GeodeticDistance(
              tDouble LonA,tDouble LatA,
              tDouble LonB,tDouble LatB,
   	      tDouble& DeltaDEG,
              tDouble& DeltaKM,
	      tDouble& AzimutDeg);
//------------------------------------------------------------------------------
// EndOf
//------------------------------------------------------------------------------
#endif
