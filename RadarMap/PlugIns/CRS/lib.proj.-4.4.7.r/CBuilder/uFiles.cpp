//=== File description =========================================================
/*!
\file uFiles.cpp
\brief Module <b>implementation part</b><br> FileWriter and FileReader
 stream objects with type testing and casting.
\author  A. Weidauer; weidauer@huckfinn.de
\version 1.0-VI-2002
*/
//---------------------------------------------------------------------------
#pragma hdrstop
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <string.h>
#include "uError.h"
#include "uFiles.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
//---------------------------------------------------------------------------

//===========================================================================
// File Writer Class
//===========================================================================
cFileWriter::cFileWriter (const pChar aFileName, tInteger aBufferSize)
 {
  printf("..open file (w) %s\n",aFileName);
  fPort=fopen(aFileName,"w+b");
  if (fPort==NULL) {
    FatalError(mSystem+eFileOpen,"Cannot open file");
  }
  fFileName = (pChar) malloc(strlen(aFileName)+1);
  strcpy(fFileName,aFileName);
  fBuffer = (pChar) malloc(aBufferSize);
  fstat(fileno(fPort),&fStatus);
  fMaxBuffer = aBufferSize;
  fPosBuffer = 0;
 }
//---------------------------------------------------------------------------
 cFileWriter::~cFileWriter (){
  Flush();
  printf("..close file (w) %s\n",fFileName);
  fclose(fPort);
  free(fFileName);
  free(fBuffer);
 }
//---------------------------------------------------------------------------
 tInteger cFileWriter::Flush() {
  tInteger Result=0;
  // if (fPosBuffer==-1) return 0;
  Result = fwrite(fBuffer,1,fPosBuffer,fPort);
  fPosBuffer=0;
  // errorhandler
  return Result;
 }

//---------------------------------------------------------------------------
void cFileWriter::WriteData(tChar Data)
{
 if (fPosBuffer==fMaxBuffer) {
  Flush(); 
 }
 fBuffer[fPosBuffer]=Data; fPosBuffer++;
}
//---------------------------------------------------------------------------
tInteger cFileWriter::WriteBuf(Pointer Data, tCardinal Size)
 {
   tInteger Result=0;
   tCardinal i;
   pChar ptr;

   ptr = (pChar) Data;
   for (i=0;i<Size;i++) {
     WriteData(*ptr); ptr++;Result++;
    }
   return Result;
 }
//---------------------------------------------------------------------------
tInteger cFileWriter::Write(tBoolean Data) {
 return WriteBuf(&Data,sizeof(tBoolean));
}
//---------------------------------------------------------------------------
tInteger cFileWriter::Write(tChar Data) {
 return WriteBuf(&Data,sizeof(tChar));
}
//---------------------------------------------------------------------------
tInteger cFileWriter::Write(tByte Data) {
 return WriteBuf(&Data,sizeof(tByte));
}
//---------------------------------------------------------------------------
tInteger cFileWriter::Write(tWord Data) {
 return WriteBuf(&Data,sizeof(tWord));
}
//---------------------------------------------------------------------------
tInteger cFileWriter::Write(tCardinal Data) {
 return WriteBuf(&Data,sizeof(tCardinal));
}
//---------------------------------------------------------------------------
tInteger cFileWriter::Write(tShortInteger Data) {
 return WriteBuf(&Data,sizeof(tShortInteger));
}
//---------------------------------------------------------------------------
tInteger cFileWriter::Write(tInteger Data) {
 return WriteBuf(&Data,sizeof(tInteger));
}

//---------------------------------------------------------------------------
tInteger cFileWriter::Write(tFloat Data) {
 return WriteBuf(&Data,sizeof(tFloat));
}

//---------------------------------------------------------------------------
tInteger cFileWriter::Write(tDouble Data) {
 return WriteBuf(&Data,sizeof(tDouble));
}
//---------------------------------------------------------------------------
tInteger cFileWriter::Write(tLargeDouble Data) {
 return WriteBuf(&Data,sizeof(tLargeDouble));
}

//---------------------------------------------------------------------------
tInteger cFileWriter::Write(pChar Data)
 {
   tInteger Result=0;
   tCardinal slen=strlen(Data);
   tCardinal i;
   Result+=Write(slen);
   Result+=WriteBuf(Data,slen);
   return Result;
}

//---------------------------------------------------------------------------
 tBoolean cFileWriter::Eof(){
  return feof(fPort);
 }

//---------------------------------------------------------------------------
tCardinal cFileWriter::Size()
 {
   return fStatus.st_size;
 }

//---------------------------------------------------------------------------
 tInteger cFileWriter::Position()
  { return ftell(fPort); }
//---------------------------------------------------------------------------
void cFileWriter::Debug(const pChar Prefix)
{
 tChar c_eof[6];
 if (Eof()) strcpy(c_eof,"true"); else strcpy(c_eof,"false");

 printf("%s cFileReader {",Prefix);
 printf("\n%s // Common settings: \n",Prefix);
 printf("%s    Filename        = \"%s\"; \n",Prefix,fFileName );
 printf("%s    Filesize        = %d; // Bytes\n",Prefix,Size() );
 printf("%s    Fileposition    = %d; \n",Prefix,Position() );
 printf("%s    FileEndOf  )    = %s; \n",Prefix,c_eof );
 printf("%s    Filetime        = %s",Prefix,ctime(&fStatus.st_atime));
 printf("%s",Prefix);
 printf("\n%s // Buffer settings: \n",Prefix);
 printf("%s    Buffer size     = %d; \n",Prefix,fMaxBuffer);
 printf("%s    Buffer position = %d; \n",Prefix,fPosBuffer);
 printf("\n%s // File statistics:\n",Prefix);
 printf("%s    Handle            = %d;\n",Prefix,fileno(fPort));
 printf("%s    Device handle     = %d;\n",Prefix,fStatus.st_dev);
 printf("%s    Device handle (r) = %d;\n",Prefix,fStatus.st_rdev);
 printf("%s    INode  handle     = %d;\n",Prefix,fStatus.st_ino);
 printf("%s    File mode         = %d;\n",Prefix,fStatus.st_nlink);
 printf("%s    File user id      = %d;\n",Prefix,fStatus.st_uid);
 printf("%s    File group id     = %d;\n",Prefix,fStatus.st_gid);
 printf("%s    File time handle  = %d;\n",Prefix,fStatus.st_atime);
 printf("%s } \n\n",Prefix);

}

//===========================================================================
// File Reader Class
//===========================================================================
cFileReader::cFileReader (const pChar aFileName, tInteger aBufferSize)
 {
  printf("..open file (r) %s\n",aFileName);
  fPort=fopen(aFileName,"r+b");
  if (fPort==NULL) {
    FatalError(mSystem+eFileOpen,"Cannot open file");
  }
  fFileName = (pChar) malloc(strlen(aFileName)+1);
  strcpy(fFileName,aFileName);
  fstat(fileno(fPort),&fStatus);
  fBuffer = (pChar) malloc(aBufferSize);
  fMaxBuffer = aBufferSize;
  fPosBuffer=0;
  fInBuffer =0;
  Fill();
 }
//---------------------------------------------------------------------------
 cFileReader::~cFileReader (){
  printf("..close file (r) %s\n",fFileName);
  fflush(fPort);
  fclose(fPort);
  free(fFileName);
  free(fBuffer);
  fMaxBuffer=0;
  fPosBuffer=0;
 }
//---------------------------------------------------------------------------
 tInteger cFileReader::Fill() {
  if (feof(fPort)) {
   fInBuffer=0;
   fPosBuffer=-1;
  } else {
   fInBuffer = fread(fBuffer,1,fMaxBuffer,fPort);
   if (fInBuffer>0) fPosBuffer=0; else fPosBuffer=-1;
  }
  // errorhandler
  return fInBuffer;
 }
//---------------------------------------------------------------------------
tChar  cFileReader::ReadData()
{ tChar ch;
 ch=fBuffer[fPosBuffer];fPosBuffer++;
 if (fPosBuffer==fInBuffer) {
  Fill();
  // printf("\n");
 }
 // printf("%2X ",(tByte)ch);
 return ch;
}
//---------------------------------------------------------------------------
tInteger cFileReader::ReadBuf(Pointer Data, tCardinal Size)
 {
   tInteger Result=0;
   tCardinal i;
   pChar ptr = (pChar) Data;
   if (fPosBuffer==fInBuffer) Fill();
   for (i=0;i<Size;i++) {
     *ptr = ReadData();
     Result++;
     ptr++;
    }
   return Result;
 }
//---------------------------------------------------------------------------
tInteger cFileReader::Read(tBoolean& Data) {
 return ReadBuf(&Data,sizeof(tBoolean));
}
//---------------------------------------------------------------------------
tInteger cFileReader::Read(tChar& Data) {
 return ReadBuf(&Data,sizeof(tChar));
}
//---------------------------------------------------------------------------
tInteger cFileReader::Read(tByte& Data) {
 return ReadBuf(&Data,sizeof(tByte));
}
//---------------------------------------------------------------------------
tInteger cFileReader::Read(tWord& Data) {
 return ReadBuf(&Data,sizeof(tWord));
}
//---------------------------------------------------------------------------
tInteger cFileReader::Read(tCardinal& Data) {
 return ReadBuf(&Data,sizeof(tCardinal));
}

//---------------------------------------------------------------------------
tInteger cFileReader::Read(tShortInteger& Data) {
 return ReadBuf(&Data,sizeof(tShortInteger));
}
//---------------------------------------------------------------------------
tInteger cFileReader::Read(tInteger& Data) {
 return ReadBuf(&Data,sizeof(tInteger));
}

//---------------------------------------------------------------------------
tInteger cFileReader::Read(tFloat& Data) {
 return ReadBuf(&Data,sizeof(tFloat));
}

//---------------------------------------------------------------------------
tInteger cFileReader::Read(tDouble& Data) {
 return ReadBuf(&Data,sizeof(tDouble));
}

//---------------------------------------------------------------------------
tInteger cFileReader::Read(tLargeDouble& Data) {
 return ReadBuf(&Data,sizeof(tLargeDouble));
}

//---------------------------------------------------------------------------
tInteger cFileReader::Read(pChar& Data)
 { tInteger Result=0; tCardinal slen;  tCardinal i;
   Result+=Read(slen);
   if(Data!=NULL) free(Data);Data = (pChar) malloc(slen+1);
   pChar ptr = (pChar) Data;
   for (i=0;i<slen;i++) {
     *ptr = ReadData(); Result++;ptr++;
    }
   Data[slen]='\0';
   return Result;
 }
//---------------------------------------------------------------------------
 tBoolean cFileReader::Eof(){
  return (feof(fPort)&&(fPosBuffer==fInBuffer-1));
 }
//---------------------------------------------------------------------------
 tBoolean cFileReader::Seek(tCardinal aPosition) {
  tBoolean Done;
  Done = (fseek(fPort,aPosition,SEEK_SET)==0);
  if (Done) { // reset read buffer and read bytes
   fPosBuffer=-1;  fInBuffer = 0;  Fill();
  }
  return Done;
 }
//---------------------------------------------------------------------------
 tBoolean cFileReader::Reset() {
  return Seek(0);
 }

//---------------------------------------------------------------------------
tCardinal cFileReader::Size()
 {
   return fStatus.st_size;
 }

//---------------------------------------------------------------------------
tInteger cFileReader::Position()
  { return ftell(fPort); }

//---------------------------------------------------------------------------
void cFileReader::Debug(const pChar Prefix)
{
 tChar c_eof[6];
 if (Eof()) strcpy(c_eof,"true"); else strcpy(c_eof,"false");

 printf("%s cFileReader {",Prefix);
 printf("\n%s // Common settings: \n",Prefix);
 printf("%s    Filename        = \"%s\"; \n",Prefix,fFileName );
 printf("%s    Filesize        = %d; // Bytes\n",Prefix,Size() );
 printf("%s    Fileposition    = %d; \n",Prefix,Position() );
 printf("%s    FileEndOf  )    = %s; \n",Prefix,c_eof );
 printf("%s    Filetime        = %s",Prefix,ctime(&fStatus.st_atime));
 printf("%s",Prefix);
 printf("\n%s // Buffer settings: \n",Prefix);
 printf("%s    Buffer size     = %d; \n",Prefix,fMaxBuffer);
 printf("%s    Bytes in buffer = %d; \n",Prefix,fInBuffer);
 printf("%s    Buffer position = %d; \n",Prefix,fPosBuffer);
 printf("\n%s // File statistics:\n",Prefix);
 printf("%s    Handle            = %d;\n",Prefix,fileno(fPort));
 printf("%s    Device handle     = %d;\n",Prefix,fStatus.st_dev);
 printf("%s    Device handle (r) = %d;\n",Prefix,fStatus.st_rdev);
 printf("%s    INode  handle     = %d;\n",Prefix,fStatus.st_ino);
 printf("%s    File mode         = %d;\n",Prefix,fStatus.st_nlink);
 printf("%s    File user id      = %d;\n",Prefix,fStatus.st_uid);
 printf("%s    File group id     = %d;\n",Prefix,fStatus.st_gid);
 printf("%s    File time handle  = %d;\n",Prefix,fStatus.st_atime);
 printf("%s } \n\n",Prefix);

}

void ReadLine(FILE *hFile, pChar Buffer, int len) {
   strcpy(Buffer,"#\0");
   while (Buffer[0]=='#')
    fgets(Buffer,len,hFile);
}

