//=== File description =========================================================
/*!
\file uError.cpp
\brief Module  <b>implementation part</b> class registration numbers for error codes
and class base numbers (class registry)
\author  A. Weidauer; weidauer@huckfinn.de
\version 1.0-VI-2002
*/

//---------------------------------------------------------------------------
#pragma hdrstop

#include "uError.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)


//--- Modules ---------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>


//--- Functions ---------------------------------------------------------------------------
/*! Errormessage Routine for fatal errors
 \param  Number  contains the exit code class basebumber+errortype
 \param  Message the related error message
*/
void FatalError(int Number, const char* Message) {
 printf("FatalError %d !\n %s !\n",Number,Message);
 exit(Number);
} 

//------------------------------------------------------------------------------
// EndOf
//------------------------------------------------------------------------------
