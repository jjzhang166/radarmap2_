//---------------------------------------------------------------------------
#pragma hdrstop

#include "Palette.h"
#include <registry.hpp>

//---------------------------------------------------------------------------
// TPaletteColorsList
//---------------------------------------------------------------------------
__fastcall TPaletteColorsList::TPaletteColorsList(int AMax)
{
	List=new TList;
	SetCount(AMax);
	if(AMax>0)
	{
		((TPaletteColor*)List->Items[0])->Position=clWhite;
		((TPaletteColor*)List->Items[0])->Position=0;
		((TPaletteColor*)List->Items[0])->Checked=true;
		((TPaletteColor*)List->Items[AMax-1])->Position=clBlack;
		((TPaletteColor*)List->Items[AMax-1])->Position=PaletteMaxPosition;
		((TPaletteColor*)List->Items[AMax-1])->Checked=true;
	}
}

//---------------------------------------------------------------------------
// TPaletteColors
//---------------------------------------------------------------------------
__fastcall TPaletteColors::TPaletteColors()
{
	colors=new TPaletteColorsList(PaletteMaxColors);
	nextindex=0;
}
//---------------------------------------------------------------------------

__fastcall TPaletteColors::~TPaletteColors()
{
	delete colors;
}
//---------------------------------------------------------------------------

void __fastcall TPaletteColors::Assign(TPaletteColor* PaletteColor, int Index)
{
	if(PaletteColor)
	{
		if(Index>=colors->Count) Index=colors->Count-1;
		else if(Index<0) Index=0;
		if(Index==0) PaletteColor->Position=0;
		else if(Index==colors->Count-1) PaletteColor->Position=PaletteMaxPosition;
		else
		{
			for(int i=Index-1; i>=0; i--)
			{
				if(colors->Items[i] && colors->Items[i]->Checked)
				{
					if(colors->Items[i]->Position>PaletteColor->Position)
						PaletteColor->Position=colors->Items[i]->Position;
					break;
				}
			}
		}
		if(colors->Items[Index])
		{
			colors->Items[Index]=PaletteColor;
			//colors->Items[Index]->Checked=PaletteColor->;
		}
		else delete PaletteColor;
	}
}
//---------------------------------------------------------------------------

TPaletteColor* __fastcall TPaletteColors::Get(int Index)
{
	int i, j;

	if(Index<colors->Count && Index>=0)
	{
		i=j=Index;
		while(!colors->Items[i]->Checked && !colors->Items[j]->Checked)
		{
			if(i==0 && j==colors->Count-1) break;
			if(i>0) i--;
			if(j<colors->Count-1) j++;
		}
		if(colors->Items[i]->Checked)
		{
			nextindex=i;
			return colors->Items[i];
		}
		else if(colors->Items[j]->Checked)
		{
			nextindex=j;
			return colors->Items[j];
		}
		else return NULL;
	}
	else return NULL;
}
//---------------------------------------------------------------------------

TPaletteColor* __fastcall TPaletteColors::GetNext(int Index)
{
	int i;

	if(Index<nextindex) Index = nextindex;
	if(Index<PaletteMaxColors-1 && Index>=0)
	{
		i=Index+1;
		while(i<PaletteMaxColors && !colors->Items[i]->Checked) i++;
		if (i<PaletteMaxColors && colors->Items[i]->Checked)
		{
			nextindex=i;
			return colors->Items[i];
		}
		else return NULL;
	}
	else return NULL;
}
//---------------------------------------------------------------------------

TPaletteColor* __fastcall TPaletteColors::GetPrevious(int Index)
{
	int i;

	if (Index>nextindex) Index=nextindex;
	if (Index<colors->Count && Index>0)
	{
		i=Index-1;
		while(!colors->Items[i]->Checked && i>=0) i--;
		if(i>=0 && colors->Items[i]->Checked)
		{
			nextindex=i;
			return colors->Items[i];
		}
		else return NULL;
	}
	else return NULL;
}
//---------------------------------------------------------------------------

void __fastcall TPaletteColors::SavePaletteColorsToRegistry(AnsiString keyName, AnsiString PaletteName)
{
	int i;
	AnsiString s = keyName + "\\" + PaletteName;

	TRegistry& regKey=*new TRegistry();
	try
	{
		try
		{
			if(regKey.OpenKey(s,true))
			{
				regKey.WriteInteger("PaletteColors", colors->Count);
				for (i=0; i<colors->Count; i++)
				{
					if (colors->Items[i] != NULL)
					{
						regKey.WriteBool("PaletteState"+IntToStr(i), colors->Items[i]->Checked);
						regKey.WriteInteger("PaletteColor"+IntToStr(i), colors->Items[i]->Color);
						regKey.WriteInteger("PalettePosition"+IntToStr(i), colors->Items[i]->Position);
					}
					else regKey.WriteBool("PaletteState"+IntToStr(i), false);
				}
			}
		}
		catch(Exception &e) {}
	}
	__finally
	{
		delete &regKey;
	}
}
//---------------------------------------------------------------------------

void __fastcall TPaletteColors::LoadPaletteColorsFromRegistry(AnsiString keyName, AnsiString PaletteName)
{
	int i;
	AnsiString s = keyName + "\\" + PaletteName;

	TRegistry& regKey=*new TRegistry();
	try
	{
		try
		{
			if(regKey.OpenKey(s, true))
			{
				if(regKey.ValueExists("PaletteColors")) colors->SetCount(regKey.ReadInteger("PaletteColors"));
				else colors->SetCount(PaletteMaxColors);
				for (i=0; i<colors->Count; i++)
				{
					if(regKey.ValueExists("PaletteState"+IntToStr(i))) colors->Items[i]->Checked=regKey.ReadBool("PaletteState"+IntToStr(i));
					if(regKey.ValueExists("PaletteColor"+IntToStr(i))) colors->Items[i]->Color=(TColor)regKey.ReadInteger("PaletteColor"+IntToStr(i));
					if(regKey.ValueExists("PalettePosition"+IntToStr(i))) colors->Items[i]->Position=(unsigned char)regKey.ReadInteger("PalettePosition"+IntToStr(i));
				}
			}
	   }
	   catch(Exception &e) {}
	}
	__finally
	{
		delete &regKey;
	}
	if(!colors->Items[0]->Checked || !colors->Items[colors->Count-1]->Checked)
		throw new Exception("Corrupted Palette data in aregistry!");
}

//---------------------------------------------------------------------------
// TPalette
//---------------------------------------------------------------------------
void __fastcall TPalette::ApplyPaletteColors(TPaletteColors* APaletteColors)
{
	try
	{
		if(colors) delete colors;
		if(APaletteColors==NULL)
		{
			colors=new TPaletteColors();

			//Red - Black - White
			//colors.Assign(new TPaletteColor(Color.Red, 0), 0);
			//colors.Assign(new TPaletteColor(Color.Black, 30), 4);
			//colors.Assign(new TPaletteColor(Color.White, TPaletteColor.MaxPosition), TPaletteColors.Max - 1);

			colors->Assign(new TPaletteColor(clWhite, 0, true), 0);
			colors->Assign(new TPaletteColor(clBlack, PaletteMaxPosition, true), PaletteMaxColors-1);
		}
		else colors = APaletteColors;
		if(palettedata) delete palettedata;
		palettedata=new TPaletteColorsList(PaletteMaxPosition);

		RebuildPalette();
	}
	catch(Exception &e) {}
}
//---------------------------------------------------------------------------

void __fastcall TPalette::RebuildPalette(bool Apply)
{
	TPaletteColor *pc=NULL, *pc2=NULL, *pc3;
	double dr, dg, db, str, stg, stb, dpos;
	int i = 0;

	if(Apply && colors && palettedata)
	{
		while(i<colors->Count)
		{
			if(pc2) pc=pc2;
			else pc=colors->Get(i);
			pc2=colors->GetNext(i);
			i=colors->NextIndex;
			if(pc && pc2)
			{
				dpos=(double)(pc2->Position-pc->Position);
				if(fabs(dpos)>0)
				{
					dr=(double)(pc2->Red-pc->Red)/dpos;
					dg=(double)(pc2->Green-pc->Green)/dpos;
					db=(double)(pc2->Blue-pc->Blue)/dpos;
					str=stg=stb=0.;
					for (int j=pc->Position; j<pc2->Position; j++)
					{
						pc3=new TPaletteColor();
						pc3->Red=(byte)((double)pc->Red+str);
						pc3->Green=(byte)((double)pc->Green+stg);
						pc3->Blue=(byte)((double)pc->Blue+stb);
						pc3->Position=pc->Position;
						Data->Items[j]=pc3;
						str+=dr;
						stg+=dg;
						stb+=db;
					}
				}
			}
			else i=PaletteMaxColors;
		}
		if(pc) for(short j=pc->Position; j<PaletteMaxPosition; j++)
			Data->Items[j]=new TPaletteColor(pc->Color, pc->Position);
	}
}
//---------------------------------------------------------------------------

void __fastcall TPalette::ChangePosition(int Index, byte Pos)
{
	TPaletteColor* pc=Colors->GetForce(Index);
	if (pc)
	{
		pc->Position=Pos;
		RebuildPalette();
	}
}
//---------------------------------------------------------------------------

void __fastcall TPalette::ChangeColor(int Index, TColor c)
{
	TPaletteColor* pc=Colors->GetForce(Index);
	if (pc)
	{
		pc->Color=c;
		RebuildPalette();
	}
}
//---------------------------------------------------------------------------

void __fastcall TPalette::LoadPaletteFromRegistry(AnsiString keyName, AnsiString PaletteName)
{
	bool err=false;

	try
	{
		if(Colors)
		{
			Colors->LoadPaletteColorsFromRegistry(keyName, PaletteName);
			RebuildPalette();
		}
		else err=true;
	}
	catch (Exception &e)
	{
		err=true;
	}
	if(err) ApplyPaletteColors(NULL);
}
//---------------------------------------------------------------------------

void __fastcall TPalette::DrawPalette(TBitmap32* Bmp, Types::TRect Rct)
{
	if(Bmp && palettedata)
	{
		int h=Rct.Height();
		float f=0, df=(float)palettedata->Count/(float)h;

		for(int i=0; i<h; i++, f+=df)
		{
			Bmp->Line(Rct.Left, Rct.Top+i, Rct.Right, Rct.Top+i, Color32(palettedata->Items[(int)f]->Color));
		}
		Bmp->FrameRectS(Rct, clBlack32);
	}
}
//---------------------------------------------------------------------------

void __fastcall TPalette::DrawPalette(TBitmap32* Bmp, int Width, int Height, int Border, bool ShowPositions)
{
	int l, r, t, b;
	if(Bmp)
	{
		Bmp->BeginUpdate();
		try
		{
			Bmp->SetSize(Width, Height);
			Bmp->Clear((TColor32)0x00F8F9FF);
			if(ShowPositions)
			{
				l=0;
				r=Width-2*PaletteColorBarOffset;
			}
			else
			{
				l=PaletteColorBarOffset;
				r=Width-PaletteColorBarOffset;
			}
			l+=Border;
			r-=Border;
			t=Border;
			b=Height-Border;
			DrawPalette(Bmp, Types::TRect(l+1, t, r-1, b));
			Bmp->Font->Name="Tahoma";
			Bmp->Font->Height=PaletteColorBarOffset;
			if(colors)
			{
				float f, df=(float)palettedata->Count/(float)(Height-2*Border);
				int p, h, w;
				TPaletteColor* pc=colors->Get(0);

				while(pc)
				{
					f=t+(float)pc->Position/df;
					if(f<t+2) f=t+2;
					else if(f>b-3) f=b-3;
					Bmp->FillRect(l, (int)f-2, r, (int)f+3, Color32(pc->Color));
					Bmp->FrameRectS(l, (int)f-2, r, (int)f+3, clBlack32);
					if(ShowPositions)
					{
						p=(palettedata->Count>>1)-pc->Position;
						h=Bmp->TextHeight(IntToStr(p));
						f=t+((float)pc->Position/df)-(h>>1);
						if(f<t) f=t;
						else if(f>b-h) f=b-h;
						Bmp->Textout(r+2, f, IntToStr(p));
					}
					pc=colors->GetNext(colors->NextIndex);
				}
			}
		}
		__finally
		{
			Bmp->EndUpdate();
		}
	}
}
//---------------------------------------------------------------------------

bool __fastcall TPalette::ConvertToWinColors(TColor *WinColors, int N)
{
	bool res=false;
	try
	{
		if(WinColors && palettedata)
		{
			float f=0, df=(float)palettedata->Count/(float)N;

			for(int i=N-1; i>=0; i--, f+=df)
			{
				if(WinColors[i]!=palettedata->Items[(int)f]->Color) res=true;
				WinColors[i]=palettedata->Items[(int)f]->Color;
			}
		}
	}
	catch(Exception &e) {}

	return res;
}

#pragma package(smart_init)
