//---------------------------------------------------------------------------

#include <vcl.h>
#include <math.h>
#include "registry.hpp"
#include "mmsystem.h"
#pragma hdrstop

#include "Radar.h"
#include "Out_segy.h"
#include "BitmapGDI.h"
//#include "GR32_PNG_Load.h"
#include "MainForm.h"

//#ifndef _InfraRadarOnly
	#include "Setup.h"
//#endif

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "GR32_Image"
#pragma link "IdBaseComponent"
#pragma link "IdComponent"
#pragma link "IdTCPClient"
#pragma link "IdTCPConnection"
#pragma link "G32_ProgressBar"
#pragma resource "*.dfm"

//---------------------------------------------------------------------------
// TRadarDialog
//---------------------------------------------------------------------------

__fastcall TRadarDialog::TRadarDialog(TComponent* Owner, TRadarMapManager* RMM)
		: TForm(Owner)
{
	RadarMapManager=RMM;

	Permit=9.;
	DPC=0;
	Process=Initialized;
	TraceImage->SetupBitmap(false, 0xFF000000);
	ProfImage->SetupBitmap(false, 0xFF000000);
	PaletteImage->SetupBitmap(false, 0xFF000000);
	if(RMM && RMM->Settings) bgColor=&(RMM->Settings->ProfBackgroundColor);
	else
	{
		bgColorTemp=clWhite32;
		bgColor=&bgColorTemp;
	}
	for(int i=0; i<_MaxProfQuan; i++)
	{
		ProfRect[i]=new Types::TRect(0,0,0,0);
		ScaleL[i]=new TScale;
		ScaleL[i]->Bmp=ProfImage->Bitmap;
		ScaleL[i]->Color=clBlack;
		ScaleL[i]->BG_Color=WinColor(*bgColor);
		ScaleL[i]->HotSide=hsLeft;//hsRight;
		ScaleL[i]->Color=clBlack;
		ScaleL[i]->BG_Color=WinColor(*bgColor);
		ScaleL[i]->Caption="Depth, m";
		ScaleL[i]->Min=50*(1e-9*3e8/2.0/sqrt(Permit));
		ScaleL[i]->Max=0;
		ScaleB[i]=new TScale;
		ScaleB[i]->Bmp=ProfImage->Bitmap;
		ScaleB[i]->Color=clBlack;
		ScaleB[i]->BG_Color=WinColor(*bgColor);
		ScaleB[i]->Caption="Traces";
		ScaleB[i]->HotSide=hsTop;
		ScaleB[i]->Color=clBlack;
		ScaleB[i]->BG_Color=WinColor(*bgColor);
		ScaleB[i]->Min=0;
		ScaleB[i]->Max=ScaleB[i]->Rect.Width();
	}
	//EventDrawProfHndl=CreateEvent(NULL,true,false,NULL);
	//ResetEvent(EventDrawProfHndl);
	SoundsEnable=true;

	for(int i=0; i<_MaxProfQuan; i++) RightTraceX[i]=0;

	OldDelay=-0.1;
	Delay=11;
	DelayWin=20;
	DelayLevel=1024;
	AutoDelay=false;

	EN[0]=true;
	EN[1]=false;
	EN[2]=false;
	EN[3]=false;
	EN[4]=true;
#ifdef _InfraRadarOnly
	EN[0]=true;
	EN[1]=false;
	EN[2]=false;
	EN[3]=false;
	EN[4]=true;
#else
	EN[0]=true;
	EN[1]=false;
	EN[2]=false;
	EN[3]=false;
	EN[4]=false;
	EN[5]=false;
	EN[6]=true;
	EN[7]=true;
#endif

	AutoStop=new TAutoStopProfile(false, RadarMapManager, this);

	FormResize(this);

	//DrawOscThrd=NULL;
	DrawProfThrd=NULL;
	OldProfilesCount=-1;
	hyperbola=NULL;
	zeroline=NULL;
}
//---------------------------------------------------------------------------

__fastcall TRadarDialog::~TRadarDialog()
{
	//CloseHandle(EventDrawProfHndl);

	AutoStop->AskForTerminate();
	WaitOthers();
	if(AutoStop->ForceTerminate()) delete AutoStop;
	AutoStop=NULL;

	for(int i=0; i<_MaxProfQuan; i++)
	{
		delete ProfRect[i];
		delete ScaleL[i];
		delete ScaleB[i];
	}
	//if(hyperbola) delete hyperbola;
	//if(zeroline) delete zeroline;
}
//---------------------------------------------------------------------------

void __fastcall TRadarDialog::Initialize(void)
{
	RadarToolbar=new TSliderToolbar(RadarMapManager->ObjectsContainer, ProfImage->GetViewportRect(), 48, staLeft, true);
	RadarToolbar->FitButtonsOnly=true;
	RadarToolbar->Visible=false;
	RadarToolbar->Transparent=true;

#ifndef _InfraRadarOnly
	SetupBtn=new TSliderToolButton(RadarToolbar, Types::TRect(2, 202, 50, 250));
	SetupBtn->NormalGlyphName="PNGIMAGE_167";
	SetupBtn->HotGlyphName="PNGIMAGE_169";
	SetupBtn->DisabledGlyphName="PNGIMAGE_168";
	//SetupBtn->Align=stbaFirst;
	SetupBtn->Enabled=true;
	SetupBtn->OnMouseClick=SetupBtnClick;
#endif

	StartBtn=new TSliderToolButton(RadarToolbar, Types::TRect(2, 2, 50, 50));
	StartBtn->NormalGlyphName="PNGIMAGE_28";
	StartBtn->HotGlyphName="PNGIMAGE_30";
	StartBtn->DisabledGlyphName="PNGIMAGE_29";
	//StartBtn->PressedGlyphName="PNGIMAGE_31";
	StartBtn->Enabled=true;
	StartBtn->OnMouseClick=StartBtnPressed;

#ifndef _InfraRadarOnly
	PauseBtn=new TSliderToolButton(RadarToolbar, Types::TRect(2, 2, 50, 50));
	PauseBtn->NormalGlyphName="PNGIMAGE_173";
	PauseBtn->HotGlyphName="PNGIMAGE_175";
	PauseBtn->DisabledGlyphName="PNGIMAGE_174";
	//PauseBtn->PressedGlyphName="PNGIMAGE_176";
	PauseBtn->Enabled=false;
	PauseBtn->OnMouseClick=PauseBtnPressed;
#endif

	StopBtn=new TSliderToolButton(RadarToolbar, Types::TRect(2, 52, 50, 100));
	StopBtn->NormalGlyphName="PNGIMAGE_32";
	StopBtn->HotGlyphName="PNGIMAGE_34";
	StopBtn->DisabledGlyphName="PNGIMAGE_33";
	StopBtn->Enabled=false;
	StopBtn->OnMouseClick=StopBtnPressed;

	ToolsToolbar=new TSliderToolbarChild(RadarMapManager->ObjectsContainer, RadarToolbar,
		ProfImage->GetViewportRect(), 48, false);
	ToolsToolbar->FitButtonsOnly=true;
	ToolsToolbar->Visible=true;//false;
	ToolsToolbar->NormalGlyphName="PNGIMAGE_238";
	ToolsToolbar->HotGlyphName="PNGIMAGE_237";
	ToolsToolbar->DisabledGlyphName="PNGIMAGE_236";
	//ToolsToolbar->PressedGlyphName="PNGIMAGE_239";/**/
	ToolsToolbar->Transparent=true;

	MarkBtn=new TSliderToolButton(ToolsToolbar, Types::TRect(2, 52, 50, 100));//RadarToolbar, Types::TRect(2, 52, 50, 100));
	MarkBtn->NormalGlyphName="PNGIMAGE_170";
	MarkBtn->HotGlyphName="PNGIMAGE_172";
	MarkBtn->DisabledGlyphName="PNGIMAGE_171";
	MarkBtn->Enabled=false;
	MarkBtn->OnMouseClick=MarkBtnPressed;
#ifndef _InfraRadarOnly
#endif

	HyperbolaBtn=new TSliderToolButton(ToolsToolbar, Types::TRect(2, 52, 50, 100));
	HyperbolaBtn->NormalGlyphName="PNGIMAGE_230";
	HyperbolaBtn->HotGlyphName="PNGIMAGE_229";
	HyperbolaBtn->DisabledGlyphName="PNGIMAGE_228";
	//HyperbolaBtn->PressedGlyphName="PNGIMAGE_231";
	HyperbolaBtn->Enabled=true;
	HyperbolaBtn->OnMouseClick=HyperbolaBtnPressed;

	ZeroBtn=new TSliderToolButton(ToolsToolbar, Types::TRect(2, 52, 50, 100));
	ZeroBtn->NormalGlyphName="PNGIMAGE_234";
	ZeroBtn->HotGlyphName="PNGIMAGE_233";
	ZeroBtn->DisabledGlyphName="PNGIMAGE_232";
	//ZeroBtn->PressedGlyphName="PNGIMAGE_235";
	ZeroBtn->Enabled=true;
	ZeroBtn->OnMouseClick=ZeroBtnPressed;/**/

	PlusBtn=new TSliderToolButton(RadarToolbar, Types::TRect(2, 102, 50, 150));
	PlusBtn->NormalGlyphName="PNGIMAGE_25";
	PlusBtn->HotGlyphName="PNGIMAGE_27";
	PlusBtn->DisabledGlyphName="PNGIMAGE_26";
	PlusBtn->Enabled=false;
	PlusBtn->OnMouseClick=PlusBtnPressed;

	MinusBtn=new TSliderToolButton(RadarToolbar, Types::TRect(2, 152, 50, 200));
	MinusBtn->NormalGlyphName="PNGIMAGE_22";
	MinusBtn->HotGlyphName="PNGIMAGE_24";
	MinusBtn->DisabledGlyphName="PNGIMAGE_23";
	MinusBtn->Enabled=false;
	MinusBtn->OnMouseClick=MinusBtnPressed;

	ExitBtn=new TSliderToolButton(RadarToolbar, Types::TRect(2, 202, 50, 250));
	ExitBtn->NormalGlyphName="PNGIMAGE_19";
	ExitBtn->HotGlyphName="PNGIMAGE_21";
	ExitBtn->DisabledGlyphName="PNGIMAGE_20";
#ifdef _InfraRadarOnly
	ExitBtn->Align=stbaLast;
#endif
	ExitBtn->Enabled=false;
	ExitBtn->OnMouseClick=ExitBtnPressed;

	BatteryBtn=new TSliderToolButton(RadarToolbar, Types::TRect(2, 202, 50, 250));
	BatteryBtn->NormalGlyphName="PNGIMAGE_215";
	BatteryBtn->Align=stbaFirst;
	BatteryBtn->Enabled=true;
	BatteryBtn->OnMouseClick=BatteryPressed;
	if(RadarMapManager && RadarMapManager->Battery) RadarMapManager->Battery->Button=BatteryBtn;

	SYSTEM_POWER_STATUS sps;
	if(GetSystemPowerStatus(&sps)!=0 && sps.BatteryFlag!=128)
	{
		BatteryBtn2=new TSliderToolButton(RadarToolbar, Types::TRect(2, 202, 50, 250));
		BatteryBtn2->NormalGlyphName="PNGIMAGE_221";
		BatteryBtn2->Align=stbaLast;
		BatteryBtn2->Enabled=true;
		BatteryBtn2->OnMouseClick=LaptopBatteryPressed;
		if(RadarMapManager && RadarMapManager->LaptopBattery) RadarMapManager->LaptopBattery->Button=BatteryBtn2;
	}

	LabelsToolbar=new TSliderToolbar(RadarMapManager->ObjectsContainer, ProfImage->GetViewportRect(), 48, staTop, false);
	LabelsToolbar->FitButtonsOnly=true;
	LabelsToolbar->Visible=false;
	LabelsToolbar->Transparent=true;
	LabelsToolbar->Minimized=false;

	RgbFiguresBtn=new TSliderToolbarChild(RadarMapManager->ObjectsContainer, LabelsToolbar,
		ProfImage->GetViewportRect(), 48, false);
	RgbFiguresBtn->Transparent=true;
	RgbFiguresBtn->FitButtonsOnly=true;
	RgbFiguresBtn->Visible=true;
	RgbFiguresBtn->NormalGlyphName="PNGIMAGE_300";//"PNGIMAGE_352"; //"PNGIMAGE_350";
	RgbFiguresBtn->HotGlyphName="PNGIMAGE_301"; //"PNGIMAGE_351";

	CircRedBtn=new TSliderToolButton(RgbFiguresBtn, Types::TRect(0, 0, 48, 48));
	CircRedBtn->NormalGlyphName="PNGIMAGE_45";
	CircRedBtn->HotGlyphName="PNGIMAGE_46";
	CircRedBtn->Enabled=true;
	CircRedBtn->GroupIndex=1;
	CircRedBtn->Tag=(int)pltCircleRed;
	CircRedBtn->OnMouseClick=LabelBtnPressed;

	CircGreenBtn=new TSliderToolButton(RgbFiguresBtn, Types::TRect(0, 0, 48, 48));
	CircGreenBtn->NormalGlyphName="PNGIMAGE_37";
	CircGreenBtn->HotGlyphName="PNGIMAGE_38";
	CircGreenBtn->Enabled=true;
	CircGreenBtn->GroupIndex=1;
	CircGreenBtn->Tag=(int)pltCircleGreen;
	CircGreenBtn->OnMouseClick=LabelBtnPressed;

	CircBlueBtn=new TSliderToolButton(RgbFiguresBtn, Types::TRect(0, 0, 48, 48));
	CircBlueBtn->NormalGlyphName="PNGIMAGE_35";
	CircBlueBtn->HotGlyphName="PNGIMAGE_36";
	CircBlueBtn->Enabled=true;
	CircBlueBtn->GroupIndex=1;
	CircBlueBtn->Tag=(int)pltCircleBlue;
	CircBlueBtn->OnMouseClick=LabelBtnPressed;

	RectRedBtn=new TSliderToolButton(RgbFiguresBtn, Types::TRect(0, 0, 48, 48));
	RectRedBtn->NormalGlyphName="PNGIMAGE_43";
	RectRedBtn->HotGlyphName="PNGIMAGE_44";
	RectRedBtn->Enabled=true;
	RectRedBtn->GroupIndex=1;
	RectRedBtn->Tag=(int)pltSquareRed;
	RectRedBtn->OnMouseClick=LabelBtnPressed;

	RectGreenBtn=new TSliderToolButton(RgbFiguresBtn, Types::TRect(0, 0, 48, 48));
	RectGreenBtn->NormalGlyphName="PNGIMAGE_41";
	RectGreenBtn->HotGlyphName="PNGIMAGE_42";
	RectGreenBtn->Enabled=true;
	RectGreenBtn->GroupIndex=1;
	RectGreenBtn->Tag=(int)pltSquareGreen;
	RectGreenBtn->OnMouseClick=LabelBtnPressed;

	RectBlueBtn=new TSliderToolButton(RgbFiguresBtn, Types::TRect(0, 0, 48, 48));
	RectBlueBtn->NormalGlyphName="PNGIMAGE_39";
	RectBlueBtn->HotGlyphName="PNGIMAGE_40";
	RectBlueBtn->Enabled=true;
	RectBlueBtn->GroupIndex=1;
	RectBlueBtn->Tag=(int)pltSquareBlue;
	RectBlueBtn->OnMouseClick=LabelBtnPressed;

	TriaRedBtn=new TSliderToolButton(RgbFiguresBtn, Types::TRect(0, 0, 48, 48));
	TriaRedBtn->NormalGlyphName="PNGIMAGE_51";
	TriaRedBtn->HotGlyphName="PNGIMAGE_52";
	TriaRedBtn->Enabled=true;
	TriaRedBtn->GroupIndex=1;
	TriaRedBtn->Tag=(int)pltTriangleRed;
	TriaRedBtn->OnMouseClick=LabelBtnPressed;

	TriaGreenBtn=new TSliderToolButton(RgbFiguresBtn, Types::TRect(0, 0, 48, 48));
	TriaGreenBtn->NormalGlyphName="PNGIMAGE_49";
	TriaGreenBtn->HotGlyphName="PNGIMAGE_50";
	TriaGreenBtn->Enabled=true;
	TriaGreenBtn->GroupIndex=1;
	TriaGreenBtn->Tag=(int)pltTriangleGreen;
	TriaGreenBtn->OnMouseClick=LabelBtnPressed;

	TriaBlueBtn=new TSliderToolButton(RgbFiguresBtn, Types::TRect(0, 0, 48, 48));
	TriaBlueBtn->NormalGlyphName="PNGIMAGE_47";
	TriaBlueBtn->HotGlyphName="PNGIMAGE_48";
	TriaBlueBtn->Enabled=true;
	TriaBlueBtn->GroupIndex=1;
	TriaBlueBtn->Tag=(int)pltTriangleBlue;
	TriaBlueBtn->OnMouseClick=LabelBtnPressed;

	WaterBtn=new TSliderToolbarChild(RadarMapManager->ObjectsContainer, LabelsToolbar,
		ProfImage->GetViewportRect(), 48, false);
	WaterBtn->ClickFirstButtonOnPopUp=true;
	WaterBtn->Transparent=true;
	WaterBtn->FitButtonsOnly=true;
	WaterBtn->Visible=true;
	WaterBtn->NormalGlyphName="PNGIMAGE_373";
	WaterBtn->HotGlyphName="PNGIMAGE_374";

	WaterCircBtn=new TSliderToolButton(WaterBtn, Types::TRect(0, 0, 48, 48));
	WaterCircBtn->NormalGlyphName="PNGIMAGE_302";
	WaterCircBtn->HotGlyphName="PNGIMAGE_303";
	WaterCircBtn->Enabled=true;
	WaterCircBtn->GroupIndex=1;
	WaterCircBtn->Tag=(int)pltWaterCircle;
	WaterCircBtn->OnMouseClick=LabelBtnPressed;

	WaterRectBtn=new TSliderToolButton(WaterBtn, Types::TRect(0, 0, 48, 48));
	WaterRectBtn->NormalGlyphName="PNGIMAGE_310";
	WaterRectBtn->HotGlyphName="PNGIMAGE_311";
	WaterRectBtn->Enabled=true;
	WaterRectBtn->GroupIndex=1;
	WaterRectBtn->Tag=(int)pltWaterRect;
	WaterRectBtn->OnMouseClick=LabelBtnPressed;

	WaterTriaBtn=new TSliderToolButton(WaterBtn, Types::TRect(0, 0, 48, 48));
	WaterTriaBtn->NormalGlyphName="PNGIMAGE_353";
	WaterTriaBtn->HotGlyphName="PNGIMAGE_354";
	WaterTriaBtn->Enabled=true;
	WaterTriaBtn->GroupIndex=1;
	WaterTriaBtn->Tag=(int)pltWaterTriangle;
	WaterTriaBtn->OnMouseClick=LabelBtnPressed;

	ElectroBtn=new TSliderToolbarChild(RadarMapManager->ObjectsContainer, LabelsToolbar,
		ProfImage->GetViewportRect(), 48, false);
	ElectroBtn->ClickFirstButtonOnPopUp=true;
	ElectroBtn->Transparent=true;
	ElectroBtn->FitButtonsOnly=true;
	ElectroBtn->Visible=true;
	ElectroBtn->NormalGlyphName="PNGIMAGE_363";
	ElectroBtn->HotGlyphName="PNGIMAGE_364";

	ElectroCircBtn=new TSliderToolButton(ElectroBtn, Types::TRect(0, 0, 48, 48));
	ElectroCircBtn->NormalGlyphName="PNGIMAGE_308";
	ElectroCircBtn->HotGlyphName="PNGIMAGE_309";
	ElectroCircBtn->Enabled=true;
	ElectroCircBtn->GroupIndex=1;
	ElectroCircBtn->Tag=(int)pltElectroCircle;
	ElectroCircBtn->OnMouseClick=LabelBtnPressed;

	ElectroRectBtn=new TSliderToolButton(ElectroBtn, Types::TRect(0, 0, 48, 48));
	ElectroRectBtn->NormalGlyphName="PNGIMAGE_316";
	ElectroRectBtn->HotGlyphName="PNGIMAGE_317";
	ElectroRectBtn->Enabled=true;
	ElectroRectBtn->GroupIndex=1;
	ElectroRectBtn->Tag=(int)pltElectroRect;
	ElectroRectBtn->OnMouseClick=LabelBtnPressed;

	ElectroTriaBtn=new TSliderToolButton(ElectroBtn, Types::TRect(0, 0, 48, 48));
	ElectroTriaBtn->NormalGlyphName="PNGIMAGE_355";
	ElectroTriaBtn->HotGlyphName="PNGIMAGE_356";
	ElectroTriaBtn->Enabled=true;
	ElectroTriaBtn->GroupIndex=1;
	ElectroTriaBtn->Tag=(int)pltElectroTriangle;
	ElectroTriaBtn->OnMouseClick=LabelBtnPressed;

	GasLowBtn=new TSliderToolbarChild(RadarMapManager->ObjectsContainer, LabelsToolbar,
		ProfImage->GetViewportRect(), 48, false);
	GasLowBtn->ClickFirstButtonOnPopUp=true;
	GasLowBtn->Transparent=true;
	GasLowBtn->FitButtonsOnly=true;
	GasLowBtn->Visible=true;
	GasLowBtn->NormalGlyphName="PNGIMAGE_365";
	GasLowBtn->HotGlyphName="PNGIMAGE_366";

	GasLowCircBtn=new TSliderToolButton(GasLowBtn, Types::TRect(0, 0, 48, 48));
	GasLowCircBtn->NormalGlyphName="PNGIMAGE_306";
	GasLowCircBtn->HotGlyphName="PNGIMAGE_307";
	GasLowCircBtn->Enabled=true;
	GasLowCircBtn->GroupIndex=1;
	GasLowCircBtn->Tag=(int)pltGasCircle;
	GasLowCircBtn->OnMouseClick=LabelBtnPressed;

	GasLowRectBtn=new TSliderToolButton(GasLowBtn, Types::TRect(0, 0, 48, 48));
	GasLowRectBtn->NormalGlyphName="PNGIMAGE_314";
	GasLowRectBtn->HotGlyphName="PNGIMAGE_315";
	GasLowRectBtn->Enabled=true;
	GasLowRectBtn->GroupIndex=1;
	GasLowRectBtn->Tag=(int)pltGasRect;
	GasLowRectBtn->OnMouseClick=LabelBtnPressed;

	GasLowTriaBtn=new TSliderToolButton(GasLowBtn, Types::TRect(0, 0, 48, 48));
	GasLowTriaBtn->NormalGlyphName="PNGIMAGE_357";
	GasLowTriaBtn->HotGlyphName="PNGIMAGE_358";
	GasLowTriaBtn->Enabled=true;
	GasLowTriaBtn->GroupIndex=1;
	GasLowTriaBtn->Tag=(int)pltGasTriangle;
	GasLowTriaBtn->OnMouseClick=LabelBtnPressed;

	GasHighBtn=new TSliderToolbarChild(RadarMapManager->ObjectsContainer, LabelsToolbar,
		ProfImage->GetViewportRect(), 48, false);
	GasHighBtn->ClickFirstButtonOnPopUp=true;
	GasHighBtn->Transparent=true;
	GasHighBtn->FitButtonsOnly=true;
	GasHighBtn->Visible=true;
	GasHighBtn->NormalGlyphName="PNGIMAGE_367";
	GasHighBtn->HotGlyphName="PNGIMAGE_368";

    GasHighCircBtn=new TSliderToolButton(GasHighBtn, Types::TRect(0, 0, 48, 48));
	GasHighCircBtn->NormalGlyphName="PNGIMAGE_381";
	GasHighCircBtn->HotGlyphName="PNGIMAGE_382";
	GasHighCircBtn->Enabled=true;
	GasHighCircBtn->GroupIndex=1;
	GasHighCircBtn->Tag=(int)pltGasHighCircle;
	GasHighCircBtn->OnMouseClick=LabelBtnPressed;

	GasHighRectBtn=new TSliderToolButton(GasHighBtn, Types::TRect(0, 0, 48, 48));
	GasHighRectBtn->NormalGlyphName="PNGIMAGE_383";
	GasHighRectBtn->HotGlyphName="PNGIMAGE_384";
	GasHighRectBtn->Enabled=true;
	GasHighRectBtn->GroupIndex=1;
	GasHighRectBtn->Tag=(int)pltGasHighRect;
	GasHighRectBtn->OnMouseClick=LabelBtnPressed;

	GasHighTriaBtn=new TSliderToolButton(GasHighBtn, Types::TRect(0, 0, 48, 48));
	GasHighTriaBtn->NormalGlyphName="PNGIMAGE_385";
	GasHighTriaBtn->HotGlyphName="PNGIMAGE_386";
	GasHighTriaBtn->Enabled=true;
	GasHighTriaBtn->GroupIndex=1;
	GasHighTriaBtn->Tag=(int)pltGasHighTriangle;
	GasHighTriaBtn->OnMouseClick=LabelBtnPressed;

	SewageBtn=new TSliderToolbarChild(RadarMapManager->ObjectsContainer, LabelsToolbar,
		ProfImage->GetViewportRect(), 48, false);
	SewageBtn->ClickFirstButtonOnPopUp=true;
	SewageBtn->Transparent=true;
	SewageBtn->FitButtonsOnly=true;
	SewageBtn->Visible=true;
	SewageBtn->NormalGlyphName="PNGIMAGE_371";
	SewageBtn->HotGlyphName="PNGIMAGE_372";

	SewageCircBtn=new TSliderToolButton(SewageBtn, Types::TRect(0, 0, 48, 48));
	SewageCircBtn->NormalGlyphName="PNGIMAGE_304";
	SewageCircBtn->HotGlyphName="PNGIMAGE_305";
	SewageCircBtn->Enabled=true;
	SewageCircBtn->GroupIndex=1;
	SewageCircBtn->Tag=(int)pltSewageCircle;
	SewageCircBtn->OnMouseClick=LabelBtnPressed;

	SewageRectBtn=new TSliderToolButton(SewageBtn, Types::TRect(0, 0, 48, 48));
	SewageRectBtn->NormalGlyphName="PNGIMAGE_312";
	SewageRectBtn->HotGlyphName="PNGIMAGE_313";
	SewageRectBtn->Enabled=true;
	SewageRectBtn->GroupIndex=1;
	SewageRectBtn->Tag=(int)pltSewageRect;
	SewageRectBtn->OnMouseClick=LabelBtnPressed;

	SewageTriaBtn=new TSliderToolButton(SewageBtn, Types::TRect(0, 0, 48, 48));
	SewageTriaBtn->NormalGlyphName="PNGIMAGE_359";
	SewageTriaBtn->HotGlyphName="PNGIMAGE_360";
	SewageTriaBtn->Enabled=true;
	SewageTriaBtn->GroupIndex=1;
	SewageTriaBtn->Tag=(int)pltSewageTriangle;
	SewageTriaBtn->OnMouseClick=LabelBtnPressed;

	TelecomBtn=new TSliderToolbarChild(RadarMapManager->ObjectsContainer, LabelsToolbar,
		ProfImage->GetViewportRect(), 48, false);
	TelecomBtn->ClickFirstButtonOnPopUp=true;
	TelecomBtn->Transparent=true;
	TelecomBtn->FitButtonsOnly=true;
	TelecomBtn->Visible=true;
	TelecomBtn->NormalGlyphName="PNGIMAGE_369";
	TelecomBtn->HotGlyphName="PNGIMAGE_370";

	TelecomCircBtn=new TSliderToolButton(TelecomBtn, Types::TRect(0, 0, 48, 48));
	TelecomCircBtn->NormalGlyphName="PNGIMAGE_387";
	TelecomCircBtn->HotGlyphName="PNGIMAGE_388";
	TelecomCircBtn->Enabled=true;
	TelecomCircBtn->GroupIndex=1;
	TelecomCircBtn->Tag=(int)pltTelecomCircle;
	TelecomCircBtn->OnMouseClick=LabelBtnPressed;

	TelecomRectBtn=new TSliderToolButton(TelecomBtn, Types::TRect(0, 0, 48, 48));
	TelecomRectBtn->NormalGlyphName="PNGIMAGE_389";
	TelecomRectBtn->HotGlyphName="PNGIMAGE_390";
	TelecomRectBtn->Enabled=true;
	TelecomRectBtn->GroupIndex=1;
	TelecomRectBtn->Tag=(int)pltTelecomRect;
	TelecomRectBtn->OnMouseClick=LabelBtnPressed;

	TelecomTriaBtn=new TSliderToolButton(TelecomBtn, Types::TRect(0, 0, 48, 48));
	TelecomTriaBtn->NormalGlyphName="PNGIMAGE_391";
	TelecomTriaBtn->HotGlyphName="PNGIMAGE_392";
	TelecomTriaBtn->Enabled=true;
	TelecomTriaBtn->GroupIndex=1;
	TelecomTriaBtn->Tag=(int)pltTelecomTriangle;
	TelecomTriaBtn->OnMouseClick=LabelBtnPressed;

	FiberBtn=new TSliderToolbarChild(RadarMapManager->ObjectsContainer, LabelsToolbar,
		ProfImage->GetViewportRect(), 48, false);
	FiberBtn->ClickFirstButtonOnPopUp=true;
	FiberBtn->Transparent=true;
	FiberBtn->FitButtonsOnly=true;
	FiberBtn->Visible=true;
	FiberBtn->NormalGlyphName="PNGIMAGE_361";
	FiberBtn->HotGlyphName="PNGIMAGE_362";

	FiberCircBtn=new TSliderToolButton(FiberBtn, Types::TRect(0, 0, 48, 48));
	FiberCircBtn->NormalGlyphName="PNGIMAGE_375";
	FiberCircBtn->HotGlyphName="PNGIMAGE_376";
	FiberCircBtn->Enabled=true;
	FiberCircBtn->GroupIndex=1;
	FiberCircBtn->Tag=(int)pltFiberCircle;
	FiberCircBtn->OnMouseClick=LabelBtnPressed;

	FiberRectBtn=new TSliderToolButton(FiberBtn, Types::TRect(0, 0, 48, 48));
	FiberRectBtn->NormalGlyphName="PNGIMAGE_377";
	FiberRectBtn->HotGlyphName="PNGIMAGE_378";
	FiberRectBtn->Enabled=true;
	FiberRectBtn->GroupIndex=1;
	FiberRectBtn->Tag=(int)pltFiberRect;
	FiberRectBtn->OnMouseClick=LabelBtnPressed;

	FiberTriaBtn=new TSliderToolButton(FiberBtn, Types::TRect(0, 0, 48, 48));
	FiberTriaBtn->NormalGlyphName="PNGIMAGE_379";
	FiberTriaBtn->HotGlyphName="PNGIMAGE_380";
	FiberTriaBtn->Enabled=true;
	FiberTriaBtn->GroupIndex=1;
	FiberTriaBtn->Tag=(int)pltFiberTriangle;
	FiberTriaBtn->OnMouseClick=LabelBtnPressed;

#ifndef _InfraRadarOnly
	STB[0]=StartBtn;
	STB[1]=PauseBtn;
	STB[2]=StopBtn;
	STB[3]=MarkBtn;
	STB[4]=PlusBtn;
	STB[5]=MinusBtn;
	STB[6]=ExitBtn;
	STB[7]=SetupBtn;
#else
	STB[0]=StartBtn;
	STB[1]=StopBtn;
	STB[2]=PlusBtn;
	STB[3]=MinusBtn;
	STB[4]=ExitBtn;
#endif
	SetButtonsState();

	//DrawOscThrd=new TDrawOscThrd(true);
	DrawProfThrd=new TDrawProfThrd(true, RadarMapManager, ProfImage);
}
//---------------------------------------------------------------------------

void __fastcall TRadarDialog::LabelBtnPressed(System::TObject* Sender, int X, int Y)
{
	try
	{
		if(((TGraphicObject*)Sender)->Pressed)
		{
			LabelBtn=(TSliderToolButton*)Sender;
			LabelBtn->Down=!LabelBtn->Down;
		}
	}
	catch(Exception &e) {}
}
//---------------------------------------------------------------------------

void __fastcall TRadarDialog::MouseClick(System::TObject* Sender, int X, int Y)
{
	TProfileLabel *pl;
	bool passed=false;
	int x=X;

	if(RadarMapManager)
	{
		if(LabelBtn && LabelBtn->Down)
		{
			try
			{
				if(RadarMapManager->MapObjects!=NULL && RadarMapManager->CurrentPath!=NULL && RadarMapManager->CurrentOutputView!=NULL)
				{
					x+=(RadarMapManager->CurrentOutputView->LeftIndex-ProfRect[0]->left);
					for(int i=0; i<RadarMapManager->CurrentProfilesCount; i++)
					{
						if(x>=RadarMapManager->CurrentOutputView->LeftIndex && x<=RadarMapManager->CurrentOutputView->RightIndex &&
							Y>=ProfRect[i]->Top && Y<=ProfRect[i]->Bottom)
						{
							pl=new TProfileLabel(RadarMapManager->ObjectsContainer, ProfImage->GetViewportRect(),
								RadarMapManager->CurrentOutputView, (TProfileLabelType)LabelBtn->Tag, X, Y, RadarMapManager);
							if(pl->Coordinate && pl->Coordinate->Valid)
							{
								//pl->GPSConfidence
								new TMapLabel(RadarMapManager->MapObjects, RadarMapManager->MapObjects->Image->GetViewportRect(),
									pl, RadarMapManager->MapObjects->Container->LatLonRect, (TObject*)RadarMapManager->CurrentPath, RadarMapManager);
								passed=true;
							}
							else
							{
								delete pl;
								pl=NULL;
							}
							break;
						}
					}
				}
			}
			__finally
			{
				if(passed)
				{
					LabelBtn->Down=false;
					LabelBtn=NULL;
				}
			}
		}
		else if(hyperbola && hyperbola->Visible)
		{
			hyperbola->MouseClick(Sender, X, Y);
        }
	}
}
//---------------------------------------------------------------------------

void __fastcall TRadarDialog::StartBtnPressed(System::TObject* Sender, int X, int Y)
{
	try
	{
		if(((TGraphicObject*)Sender)->Pressed)
		{
			StartPressed();
		}
	}
	catch(Exception &e) {}
}
//---------------------------------------------------------------------------

#ifndef _InfraRadarOnly
void __fastcall TRadarDialog::PauseBtnPressed(System::TObject* Sender, int X, int Y)
{
	try
	{
		if(((TGraphicObject*)Sender)->Pressed)
		{
			PauseBtn->Down=true;
			PausePressed();
			if(Process==Paused) PauseBtn->Down=true;
			else PauseBtn->Down=false;
		}
	}
	catch(Exception &e) {}
}
//---------------------------------------------------------------------------

void __fastcall TRadarDialog::PausePressed(void)
{
	if(RadarMapManager->Connection)
	{
		if(Process==Running)
		{
			RadarMapManager->Connection->PauseReceiving();
			Process=Paused;
			if(SoundsEnable) sndPlaySound("PAUSED.WAV", SND_ASYNC);
		}
		else
		{
			if(SoundsEnable) sndPlaySound("LETSGO.WAV", SND_SYNC);
			RadarMapManager->Connection->ContinueReceiving();
			Process=Running;
		}
		SetButtonsState();
	}
}
//---------------------------------------------------------------------------

void __fastcall TRadarDialog::MarkBtnPressed(System::TObject* Sender, int X, int Y)
{
	try
	{
		if(((TGraphicObject*)Sender)->Pressed)
		{
			//StopPressed();
			//StartBtn->Down=false;
			MarkPressed();
		}
	}
	catch(Exception &e) {}
}
//---------------------------------------------------------------------------

#endif

void __fastcall TRadarDialog::StopBtnPressed(System::TObject* Sender, int X, int Y)
{
	try
	{
		if(((TGraphicObject*)Sender)->Pressed)
		{
			StopPressed();
			StartBtn->Down=false;
#ifndef _InfraRadarOnly
			PauseBtn->Down=false;
#endif
		}
	}
	catch(Exception &e) {}
}
//---------------------------------------------------------------------------

void __fastcall TRadarDialog::PlusBtnPressed(System::TObject* Sender, int X, int Y)
{
	try
	{
		if(((TGraphicObject*)Sender)->Pressed)
		{
			PlusPressed();
		}
	}
	catch(Exception &e) {}
}
//---------------------------------------------------------------------------

void __fastcall TRadarDialog::MinusBtnPressed(System::TObject* Sender, int X, int Y)
{
	try
	{
		if(((TGraphicObject*)Sender)->Pressed)
		{
			MinusPressed();
		}
	}
	catch(Exception &e) {}
}
//---------------------------------------------------------------------------

void __fastcall TRadarDialog::ExitBtnPressed(System::TObject* Sender, int X, int Y)
{
	try
	{
		if(((TGraphicObject*)Sender)->Pressed)
		{
			ExitPressed();
		}
	}
	catch(Exception &e) {}
}
//---------------------------------------------------------------------------

void __fastcall TRadarDialog::FormKeyDown(TObject *Sender, WORD &Key,
	  TShiftState Shift)
{
  switch(Key)
  {
	case VK_SPACE:
	case VK_RETURN:
	{
	  if(Shift.Contains(ssAlt) && Shift.Contains(ssCtrl))
	  {
		if(TraceImage->Visible)
		{
		  TraceImage->Visible=false;
		  TracePanel->Width=10;
		  if(RadarMapManager->Connection && RadarMapManager->Connection->Settings)
			RadarMapManager->Connection->Settings->Positioning=TPositioning::Wheel;
		}
		else
		{
		  TraceImage->Visible=true;
		  TracePanel->Width=100;
		  if(RadarMapManager->Connection && RadarMapManager->Connection->Settings)
			RadarMapManager->Connection->Settings->Positioning=TPositioning::ManualP;
		}
		FormResize(NULL);
		break;
	  }
	  break;
	}
  }
}
//---------------------------------------------------------------------------

#ifndef _InfraRadarOnly
void __fastcall TRadarDialog::SetupBtnClick(System::TObject* Sender, int X, int Y)
{
	int PC=0;
	try
	{
		if(((TGraphicObject*)Sender)->Pressed)
		{
			TSetupForm* SetupForm;
			TGPRUnit* Settings;

			PC=1;
			//LogEvent("PC="+IntToStr(PC), "TRadarDialog::SetupBtnClick()");
			if(RadarMapManager->Connection) RadarMapManager->CloseConnection();
			PC=2;
			//LogEvent("PC="+IntToStr(PC), "TRadarDialog::SetupBtnClick()");
			if(RadarMapManager->OpenConnection())
			{
				PC=3;
				//LogEvent("PC="+IntToStr(PC), "TRadarDialog::SetupBtnClick()");
				Settings=RadarMapManager->ReceivingSettings;
				if(Settings)
				{
					Settings->CreateChannelProfiles();
					PC=4;
					//LogEvent("PC="+IntToStr(PC), "TRadarDialog::SetupBtnClick()");
					SetupForm=new TSetupForm(this, RadarMapManager);
					PC=5;
					//LogEvent("PC="+IntToStr(PC), "TRadarDialog::SetupBtnClick()");
					RadarMapManager->Connection->StartReceiving();
					PC=6;
					//LogEvent("PC="+IntToStr(PC), "TRadarDialog::SetupBtnClick()");
					try
					{
						if(SetupForm->ShowModal()==mrOk)
						{
							//Settings->Stacking=1;
							PC=7;
							//LogEvent("PC="+IntToStr(PC), "TRadarDialog::SetupBtnClick()");
							Settings->SaveSettings();
						}
						else Settings->LoadSettings();
						PC=8;
						//LogEvent("PC="+IntToStr(PC), "TRadarDialog::SetupBtnClick()");
					}
					__finally
					{
						delete SetupForm;
					}
					PC=9;
					//LogEvent("PC="+IntToStr(PC), "TRadarDialog::SetupBtnClick()");
				}
				if(RadarMapManager->Connection) RadarMapManager->CloseConnection();
				PC=10;
				//LogEvent("PC="+IntToStr(PC), "TRadarDialog::SetupBtnClick()");
			}
			else
			{
				RadarMapManager->ShowMessageObject(NULL, RadarMapManager->RadarMessages,
					TMessageType::mtStop, mvtDisapering, 5000, "Radar is OFF or not connected!", "Error");
				PC=11;
				//LogEvent("PC="+IntToStr(PC), "TRadarDialog::SetupBtnClick()");
			}
		}
	}
	catch(Exception &e) {LogEvent(e.Message, "TRadarDialog::SetupBtnClick() PC="+IntToStr(PC));}
}
//---------------------------------------------------------------------------
#endif

void TRadarDialog::BottomScaleRefresh(int Index)
{
	int i0, j;
	if(Index<0)
	{
		i0=0;
		j=_MaxProfQuan;
	}
	else
	{
		i0=Index;
		j=Index+1;
    }
	for(int i=i0; i<j; i++)
	{
		if(RadarMapManager->Settings && RadarMapManager->Settings->WheelPositioning!=TPositioning::ManualP)
		{
			ScaleB[i]->Caption="Distance, m";
			ScaleB[i]->Max=ScaleB[i]->Min+ScaleB[i]->Rect.Width()*RadarMapManager->Settings->WheelTraceDistance;
		}
		else
		{
			ScaleB[i]->Caption="Traces";
			ScaleB[i]->Max=ScaleB[i]->Min+ScaleB[i]->Rect.Width();
		}
	}
}
//---------------------------------------------------------------------------

void TRadarDialog::StartPressed(void)
{
	int mr, PC=0;

	try
	{
		DrawWhileWheelIsStopped=false;
		if(RadarMapManager && (Process==Initialized || Process==Stopped))
		{
			StartBtn->Down=true;
			BatteryST->Caption="Battery --- V";

			if(RadarMapManager->Settings && RadarMapManager->Settings->WheelPositioning==TPositioning::TwoWheel &&
				RadarMapManager->TwoWheelClient && RadarMapManager->Settings->WPSasWheel)
			{
				if(!RadarMapManager->TwoWheelClient->StartPositioning(&StartWPSasWheelParser,
					0, 0, 0, 0, 1, 1, NULL))
				{
					if(RadarMapManager->RadarMessages)
					{
						RadarMapManager->ShowMessageObject(NULL, RadarMapManager->RadarMessages,
							TMessageType::mtStop, mvtDisapering, 5000, "WPS Communication error!", (AnsiString)_RadarMapName);
					}
					StartBtn->Down=false;
					return;
				}
			}
			if(RadarMapManager->OpenConnection())
			{
				//LogEvent("PC="+IntToStr(PC), "TRadarDialog::StartPressed()");
				DrawProfThrd->Suspend();
				PC=__LINE__;
				//LogEvent("PC="+IntToStr(PC), "TRadarDialog::StartPressed()");
				RadarMapManager->Connection->Settings->CreateChannelProfiles();
				PC=__LINE__;
				//LogEvent("PC="+IntToStr(PC), "TRadarDialog::StartPressed()");
				RadarMapManager->CreateProfileSatellites();
				PC=__LINE__;
				//LogEvent("PC="+IntToStr(PC), "TRadarDialog::StartPressed()");
				//RebuildRectsScales();
				RebuildAndRefresh(true);
				PC=__LINE__;
				//LogEvent("PC="+IntToStr(PC), "TRadarDialog::StartPressed()");
				RadarMapManager->CurrentOutputView->ClearAllOutputViews();
				DrawProfThrd->Resume();
				PC=__LINE__;
				//LogEvent("PC="+IntToStr(PC), "TRadarDialog::StartPressed()");
				Process=Running;
				//RadarMapManager->CurrentOutputView->ClearAllOutputViews();
				PC=__LINE__;
				//LogEvent("PC="+IntToStr(PC), "TRadarDialog::StartPressed()");
				RadarMapManager->CurrentOutputView->ApplyScrolling();
				PC=__LINE__;
				//LogEvent("PC="+IntToStr(PC), "TRadarDialog::StartPressed()");
				if(RadarMapManager->Connection->Settings->Positioning==TPositioning::PosDLL &&
					PlugInManager && PlugInManager->ActivePos)
				{
					TCoordinateSystem cs;
					TCoordinateSystemDimension csd;
					T3DDoublePoint dp;

					cs=RadarMapManager->Settings->DefaultCoordinateSystem;
					/*dp.X=RadarMapManager->LastGPSPoint->GetX(cs);
					dp.Y=RadarMapManager->LastGPSPoint->GetY(cs);
					dp.Z=RadarMapManager->LastGPSPoint->GetH(cs);*/
					RadarMapManager->LastGPSPoint->GetXYH(cs, dp.X, dp.Y, dp.Z);
					csd=RadarMapManager->LastGPSPoint->GetCSDimension(cs);
					PlugInManager->ActivePos->StartAcquisition(dp, csd);
				}
				PC=__LINE__;
				//LogEvent("PC="+IntToStr(PC), "TRadarDialog::StartPressed()");
				if(SoundsEnable) sndPlaySound("LETSGO.WAV",SND_SYNC);
				RadarMapManager->Connection->StartReceiving();
				PC=__LINE__;
				//LogEvent("PC="+IntToStr(PC), "TRadarDialog::StartPressed()");
			}
			else
			{
				StartBtn->Down=false;
				RadarMapManager->CloseConnection();
				PC=__LINE__;
				//LogEvent("PC="+IntToStr(PC), "TRadarDialog::StartPressed()");
				RadarMapManager->ShowMessageObject(NULL, RadarMapManager->RadarMessages,
					TMessageType::mtStop, mvtDisapering, 5000, "Radar is OFF or not connected!", "Error");
				PC=__LINE__;
				//LogEvent("PC="+IntToStr(PC), "TRadarDialog::StartPressed()");
			}
		}
	}
	catch(Exception &e) {LogEvent(e.Message, "file "+(AnsiString)__FILE__+" line "+IntToStr(PC));}
	SetButtonsState();
}
//---------------------------------------------------------------------------

void __fastcall TRadarDialog::StartWPSasWheelParser(AnsiString Reply)
{
	if(RadarMapManager && RadarMapManager->Settings &&
		RadarMapManager->Settings->WheelPositioning==TPositioning::TwoWheel &&
		RadarMapManager->TwoWheelClient && RadarMapManager->Settings->WPSasWheel)
	{
		int rsf;
		TStringList *strs;

		try
		{
			strs=new TStringList();
			if(Reply!=NULL && Reply!="" && Reply.Length()>0 &&
				RadarMapManager->TwoWheelClient->ParseStrings(Reply, strs, ',')>0)
			{
				if(strs->Strings[0].LowerCase()=="$invalid")
				{
					if(RadarMapManager->RadarMessages)
					{
						RadarMapManager->ShowMessageObject(NULL, RadarMapManager->RadarMessages,
							TMessageType::mtStop, mvtDisapering, 5000, "WPS Communication error!", (AnsiString)_RadarMapName);
					}
					StopPressed();
				}
			}
		}
		__finally
		{
			delete strs;
		}
	}
}

void TRadarDialog::StopPressed(void)
{
	int mr;
	TDateTime dt;
	unsigned short h, m, s, ms, y, mm, d;
	AnsiString str, s2;
	bool res;

	//if(!RadarMapManager->Connection) return;

	DrawWhileWheelIsStopped=false;
	if(RadarMapManager && (Process==Running || Process==Paused))
	{
		if(RadarMapManager->Settings &&
			RadarMapManager->Settings->WheelPositioning==TPositioning::TwoWheel &&
			RadarMapManager->TwoWheelClient && RadarMapManager->Settings->WPSasWheel)
				RadarMapManager->TwoWheelClient->StopPositioning(NULL, NULL);
		if(RadarMapManager->Connection)
		{
			if(RadarMapManager->Connection->Settings->SelectedChannelMode==TChannelMode::Double ||
				RadarMapManager->Connection->Settings->SelectedChannelMode==TChannelMode::Circle)
			{
				for(int i=0; i<RadarMapManager->ReceivingProfilesQuantity; i++)
				{
					if(RadarMapManager->ReceivingProfiles[i])
					{
						if(RadarMapManager->Connection->Settings)
							RadarMapManager->Connection->Settings->SelectedChannel->Gain->CopyToProfile(RadarMapManager->ReceivingProfiles[i]);
						RadarMapManager->ReceivingProfiles[i]->StopProfiling();
					}
				}
			}
			else if(RadarMapManager->ReceivingProfile)
			{
				if(RadarMapManager->Connection->Settings)
					RadarMapManager->Connection->Settings->SelectedChannel->Gain->CopyToProfile(RadarMapManager->ReceivingProfile);
				RadarMapManager->ReceivingProfile->StopProfiling();
			}
		}
		if(RadarMapManager->ReceivingSettings)
		{
			RadarMapManager->ReceivingSettings->SaveSettings();
			RadarMapManager->ReceivingSettings->REGIME=TRegime::OFF;
			/*if(RadarMapManager->Connection)
				RadarMapManager->Connection->WriteCommand(
					RadarMapManager->ReceivingSettings->GetTuneCommand(), NULL);*/
			if(RadarMapManager->ReceivingSettings->Positioning==TPositioning::PosDLL &&
				PlugInManager && PlugInManager->ActivePos)
					PlugInManager->ActivePos->StopAcquisition();
		}
		Process=Stopped;

		RadarMapManager->CloseConnection();

		if(RadarMapManager->Settings && RadarMapManager->Settings->AutoSaveProfile &&
			DirectoryExists(RadarMapManager->Settings->AutoSaveDir))
		{
			s2=RadarMapManager->Settings->AutoSaveDir+"\\RadarMap_Profile_";
			dt=Now();
			DecodeTime(dt, h, m, s, ms);
			DecodeDate(dt, y, mm, d);
			s2+=IntToStr(y);
			if(mm<10) s2+="0";
			s2+=IntToStr(mm);
			if(d<10) s2+="0";
			s2+=IntToStr(d)+"_";
			if(h<10) s2+="0";
			s2+=IntToStr(h);
			if(m<10) s2+="0";
			s2+=IntToStr(m);
			if(s<10) s2+="0";
			s2+=IntToStr(s);
			res=true;
			for(int i=0; i<RadarMapManager->CurrentProfilesCount; i++)
			{
				if(RadarMapManager->CurrentProfiles[i]) //<profile>
				{
					str=s2;
					if(RadarMapManager->CurrentProfilesCount>1) str+="_Ch"+IntToStr(i+1);
					str+=".sgy";
					if(Output_SEGY(str.c_str(), RadarMapManager->CurrentProfiles[i])!=0)
					{
						res=false;
					}
				}
			}
			if(!res)
			{
				RadarMapManager->ShowMessageObject(NULL, RadarMapManager->RadarMessages,
					TMessageType::mtStop, mvtDisapering, 5000, "Cannot autosave profile!", "Error");
			}
			else
			{
				if(RadarMapManager->Settings->AutoCloseProfile)
				{
					RadarMapManager->ShowMessageObject(NULL, RadarMapManager->RadarMessages,
						TMessageType::mtText, mvtDisapering, 5000, "Profile autosaved and closed", "Information");
					RadarMapManager->Satellites->Delete(RadarMapManager->Current);
					RadarMapManager->SwitchToAvailablePath();
				}
				else RadarMapManager->ShowMessageObject(NULL, RadarMapManager->RadarMessages,
					TMessageType::mtText, mvtDisapering, 5000, "Profile autosaved", "Information");
			}
		}
		if(SoundsEnable) sndPlaySound("stopped.wav", SND_ASYNC);
	}
	SetButtonsState();
}
//---------------------------------------------------------------------------

void TRadarDialog::PlusPressed(void)
{
	if(RadarMapManager && RadarMapManager->Connection && RadarMapManager->Connection->Settings)
	{
		TGainPoint gp;

		if(RadarMapManager->Connection->Settings->SelectedChannelMode==TChannelMode::Double ||
			RadarMapManager->Connection->Settings->SelectedChannelMode==TChannelMode::Circle)
		{
			for(int i=0; i<RadarMapManager->CurrentProfilesCount; i++)
			{
				if(RadarMapManager->Connection->Settings->Channels && RadarMapManager->Connection->Settings->Channels[i] &&
					RadarMapManager->Connection->Settings->Channels[i]->Gain)
				{
					for(int j=0; j<RadarMapManager->Connection->Settings->Channels[i]->Gain->GainPointsCount; j++)
					{
						gp=RadarMapManager->Connection->Settings->Channels[i]->Gain->GainPoints[j];
						gp.y+=6;
						RadarMapManager->Connection->Settings->Channels[i]->Gain->GainPoints[j]=gp;
					}
				}
			}
		}
		else
		{
			for(int i=0; i<RadarMapManager->Connection->Settings->SelectedChannel->Gain->GainPointsCount; i++)
			{
				gp=RadarMapManager->Connection->Settings->SelectedChannel->Gain->GainPoints[i];
				gp.y+=6;
				RadarMapManager->Connection->Settings->SelectedChannel->Gain->GainPoints[i]=gp;
			}
		}
	}
}
//---------------------------------------------------------------------------

void TRadarDialog::MinusPressed(void)
{
	if(RadarMapManager && RadarMapManager->Connection && RadarMapManager->Connection->Settings)
	{
		TGainPoint gp;

		if(RadarMapManager->Connection->Settings->SelectedChannelMode==TChannelMode::Double ||
			RadarMapManager->Connection->Settings->SelectedChannelMode==TChannelMode::Circle)
		{
			for(int i=0; i<RadarMapManager->CurrentProfilesCount; i++)
			{
				if(RadarMapManager->Connection->Settings->Channels && RadarMapManager->Connection->Settings->Channels[i] &&
					RadarMapManager->Connection->Settings->Channels[i]->Gain)
				{
					for(int j=0; j<RadarMapManager->Connection->Settings->Channels[i]->Gain->GainPointsCount; j++)
					{
						gp=RadarMapManager->Connection->Settings->Channels[i]->Gain->GainPoints[j];
						gp.y-=6;
						RadarMapManager->Connection->Settings->Channels[i]->Gain->GainPoints[j]=gp;
					}
				}
			}
		}
		else
		{
			for(int i=0; i<RadarMapManager->Connection->Settings->SelectedChannel->Gain->GainPointsCount; i++)
			{
				gp=RadarMapManager->Connection->Settings->SelectedChannel->Gain->GainPoints[i];
				gp.y-=6;
				RadarMapManager->Connection->Settings->SelectedChannel->Gain->GainPoints[i]=gp;
			}
		}
	}
}
//---------------------------------------------------------------------------

void TRadarDialog::MarkPressed(void)
{
	if(RadarMapManager && RadarMapManager->Connection && RadarMapManager->Connection->Settings)
	{
		RadarMapManager->Connection->Settings->SetMarkPressetEvent();
		if(SoundsEnable) sndPlaySound("mark.wav", SND_ASYNC);
	}
}
//---------------------------------------------------------------------------

void TRadarDialog::ExitPressed(void)
{
	/*int mr, mr1;

	if(RadarMapManager->ResultsNotSaved)
	{
		mr=Application->MessageBox(L"Your current results are not saved. Are you sure you want to Exit?",
			(L"Exit from "+(UnicodeString)_RadarMapName).w_str(), MB_YESNO | MB_ICONQUESTION);
		if(mr==ID_NO) return;
	}
	else mr=Application->MessageBox(L"Are you sure you want to Exit?",
		(L"Exit from "+(UnicodeString)_RadarMapName).w_str(), MB_YESNO);
	if(mr==IDYES)
	{
		if(RadarMapManager->GlobalStopItEvent)
			SetEvent(RadarMapManager->GlobalStopItEvent);
		WaitOthers();
		RadarMapForm->Close();
		//Application->Terminate();
	}*/
	RadarMapForm->Close();
}
//---------------------------------------------------------------------------

void __fastcall TRadarDialog::FormActivate(TObject *Sender)
{
	//ProfImageRefresh(true);
}
//---------------------------------------------------------------------------

void __fastcall TRadarDialog::ProfChannelRefresh(int Index, bool ZeroBottomScale, bool WithUpdate)
{
	try
	{
		try
		{
			if(ZeroBottomScale) ScaleB[Index]->Min=0;
			BottomScaleRefresh(Index);
			if(WithUpdate)
			{
				ProfImage->Bitmap->BeginUpdate();
				//ProfImage->Bitmap->FillRectS(*ProfRect[Index], *bgColor);
			}
			ProfImage->Bitmap->FillRectS(*ProfRect[Index], *bgColor);
#ifdef _RadarMap_Logo_BGND
			TBitmap32* tmp=NULL;
			float f, fh, fw;
			int x, y, h, w, x0, y0;

			try
			{
				try
				{
					w=ProfImage->Width;//ProfRect[Index]->Width();
					h=ProfImage->Height;//ProfRect[Index]->Height();
					x0=0;//ProfRect[Index]->Left;
					y0=0;//ProfRect[Index]->Top;
					tmp=LoadPNG32Resource((System::UnicodeString)"PngImage_ICON_BGND_COLOR");//"PNGIMAGE_BGND");
					fh=(float)h*0.75/(float)tmp->Height;
					fw=(float)w*0.75/(float)tmp->Width;
					if(fh<fw) f=fh;
					else f=fw;
					if(f<1.)
					{
						//ScaleBitmap32(tmp, f, f, false);
						x=x0+((w-(int)(f*tmp->Width))>>1);
						y=y0+((h-(int)(f*tmp->Height))>>1);
						ProfImage->Bitmap->Draw(Types::TRect(x, y, x+f*tmp->Width-1, y+f*tmp->Height-1),
							Types::TRect(0, 0, tmp->Width-1, tmp->Height-1), tmp);
					}
					else ProfImage->Bitmap->Draw(x0+((w-tmp->Width)>>1), y0+((h-tmp->Height)>>1), tmp);
				}
				catch(...) {}
			}
			__finally
			{
				if(tmp) delete tmp;
			}
#endif
			ProfImage->Bitmap->FrameRectS(*ProfRect[Index], clBlack32);
			ScaleB[Index]->Draw();
			ScaleL[Index]->Draw();
		}
		catch(...) {}
	}
	__finally
	{
		if(WithUpdate)
		{
			ProfImage->Bitmap->EndUpdate();
			ProfImage->Bitmap->Changed();
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TRadarDialog::RebuildAndRefresh(bool ZeroBottomScale)
{
	RebuildRectsScales();
	ProfImageRefresh(ZeroBottomScale);
}
//---------------------------------------------------------------------------

void __fastcall TRadarDialog::ProfImageRefresh(bool ZeroBottomScale)
{
	ProfImage->Bitmap->BeginUpdate();
	//ProfImage->Bitmap->Clear(*bgColor);
	if(RadarMapManager->CurrentProfilesCount==0) ProfChannelRefresh(0, ZeroBottomScale, false);
	else for(int i=0; i<RadarMapManager->CurrentProfilesCount; i++)
		ProfChannelRefresh(i, ZeroBottomScale, false);
	ProfImage->Bitmap->EndUpdate();
	ProfImage->Bitmap->Changed();
}
//---------------------------------------------------------------------------

#define ScaleHW 24

void __fastcall TRadarDialog::RebuildRectsScales()
{
	Types::TRect Rect;
	int rh, i, cpc;

	DataRect.Left=4;
	DataRect.Top=4;
	DataRect.Right=ProfImage->Width-4;
	DataRect.Bottom=ProfImage->Height-4;

	if(RadarMapManager && RadarMapManager->Current) cpc=RadarMapManager->CurrentProfilesCount;
	else cpc=1;

	rh=(int)((float)(DataRect.Height()-ScaleHW)/(float)cpc);
	rh-=DataRect.Top;

	for(i=0; i<cpc; i++)
	{
		Rect.Left=DataRect.Left;
		Rect.Right=DataRect.Right;
		Rect.Top=DataRect.Top+(DataRect.Top+rh)*i;//DataRect.Top*(q+1)+rh*q;
		Rect.Bottom=Rect.Top+rh;

		ScaleL[i]->Rect.Left=Rect.Right-ScaleHW;
		ScaleL[i]->Rect.Top=Rect.Top;
		ScaleL[i]->Rect.Right=Rect.Right;
		ScaleL[i]->Rect.Bottom=Rect.Bottom;
		
		ProfRect[i]->Left=Rect.Left;
		ProfRect[i]->Top=Rect.Top;
		ProfRect[i]->Right=ScaleL[i]->Rect.Left;
		ProfRect[i]->Bottom=Rect.Bottom;

		CopyRect[i].Left=ProfRect[i]->Left+1;
		CopyRect[i].Top=ProfRect[i]->Top+1;
		CopyRect[i].Right=ProfRect[i]->Right-1;
		CopyRect[i].Bottom=ProfRect[i]->Bottom-1;

		//ScaleB[i]->Rect.Top=DataRect.Bottom-ScaleHW;
		//ScaleB[i]->Rect.Left=DataRect.Left;
		//ScaleB[i]->Rect.Right=ScaleL[i]->Rect.Left;
		//ScaleB[i]->Rect.Bottom=DataRect.Bottom;
		ScaleB[i]->Rect=Types::TRect(0, 0, 0, 0);
		if(RightTraceX[i]>ProfRect[i]->Right) RightTraceX[i]=ProfRect[i]->Right-1;
	}
	for(;i<_MaxProfQuan; i++)
	{
		ScaleL[i]->Rect=Types::TRect(0, 0, 0, 0);
		ProfRect[i]->Left=0;
		ProfRect[i]->Top=0;
		ProfRect[i]->Right=0;
		ProfRect[i]->Bottom=0;
		CopyRect[i]=Types::TRect(0, 0, 0, 0);
		ScaleB[i]->Rect=Types::TRect(0, 0, 0, 0);
		if(RightTraceX[i]>ProfRect[i]->Right) RightTraceX[i]=ProfRect[i]->Right-1;
	}
	ScaleB[0]->Rect.Top=DataRect.Bottom-ScaleHW;
	ScaleB[0]->Rect.Left=DataRect.Left;
	ScaleB[0]->Rect.Right=ScaleL[0]->Rect.Left;
	ScaleB[0]->Rect.Bottom=DataRect.Bottom;

	//if(OldProfilesCount!=cpc)
	try
	{
		ProfImage->Bitmap->BeginUpdate();
		ProfImage->Bitmap->SetSize(ProfImage->Width, ProfImage->Height);
		ProfImage->Bitmap->Clear(*bgColor);
	}
	__finally
	{
		ProfImage->Bitmap->EndUpdate();
		ProfImage->Bitmap->Changed();
    }
	OldProfilesCount=cpc;
}
//---------------------------------------------------------------------------

void __fastcall TRadarDialog::FormResize(TObject *Sender)
{
	OldProfilesCount=-1;
	RebuildRectsScales();
	if(RadarMapManager)
	{
		if(RadarMapManager->CurrentOutputView!=NULL)
		{
			RadarMapManager->CurrentOutputView->ApplyScrolling();
		}
		else ProfImageRefresh(false);
	}
}
//---------------------------------------------------------------------------

void __fastcall TRadarDialog::FormClose(TObject *Sender, TCloseAction &Action)
{
	if(DrawProfThrd!=NULL)
	{
		try
		{
			DrawProfThrd->Terminate();
			WaitOthers();
			if(TerminateThread((HANDLE)DrawProfThrd->Handle, 0)>0) delete DrawProfThrd;
		}
		catch (Exception &e) {;}
	}
	if(RadarMapManager)	RadarMapManager->SaveSettings();
}
//---------------------------------------------------------------------------

int TRadarDialog::SetZeroPoint(TTrace* Ptr, int Smpls)
{
	int max, i=0;

	//max=Ptr->GetMaxInTrace();
	max=Ptr->GetMaxInSourceTrace();
	while(abs(Ptr->SourceData[i])<(int)((float)max/2.) && i<Smpls) i++;
	if(i<(Smpls-1))
	{
		while(abs(Ptr->SourceData[i])<=abs(Ptr->SourceData[i+1]) && (i+2)<(Smpls-1)) i++;
		return i;
	}
  	else return 0;
}
//---------------------------------------------------------------------------

void __fastcall TRadarDialog::FormCanResize(TObject *Sender, int &NewWidth,
	  int &NewHeight, bool &Resize)
{
	int nw;
	TProfileOutputView* ov;

	if(DrawProfThrd && DrawProfThrd->Suspended) DrawProfThrd->Resume();
	if(RadarMapManager!=NULL && RadarMapManager->CurrentOutputView!=NULL && //  Process!=Initialized &&
		Width!=NewWidth)
	{
		ov=RadarMapManager->CurrentOutputView;
		nw=NewWidth-(Width-ProfRect[0]->Width()+4);
		if(nw>0)
		{
			if(NewWidth<Width)
			{
				ov->ScrollingLeftIndex=ov->LeftIndex;
				ov->ScrollingRightIndex=ov->ScrollingLeftIndex+nw;
				if(ov->ScrollingWidth()!=nw) ov->ScrollingLeftIndex=ov->ScrollingRightIndex-nw;
			}
			else
			{
				ov->ScrollingRightIndex=ov->RightIndex;
				ov->ScrollingLeftIndex=ov->ScrollingRightIndex-nw;
				if(ov->ScrollingWidth()!=nw) ov->ScrollingRightIndex=ov->ScrollingLeftIndex+nw;
			}
		}
		else
		{
			ov->ScrollingLeftIndex=ov->LeftIndex;
			ov->ScrollingRightIndex=ov->ScrollingLeftIndex;
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TRadarDialog::FormShow(TObject *Sender)
{
  Top=0;
  Left=0;
  Resize();
}
//---------------------------------------------------------------------------

void TRadarDialog::SetButtonsState(void)
{
#ifdef _InfraRadarOnly
	if(Process==Initialized || Process==Stopped)
	{
		EN[0]=true;
		EN[1]=false;
		EN[2]=false;
		EN[3]=false;
		EN[4]=true;
	}
	else if(Process==Running || Process==Paused)
	{
		EN[0]=false;
		EN[1]=true;
		EN[2]=true;
		EN[3]=true;
		EN[4]=false;
	}
	for(int k=0; k<=4; k++)
	{
		STB[k]->Enabled=EN[k];
	}
#else
	if(Process==Initialized || Process==Stopped)
	{
		EN[0]=true;
		EN[1]=false;
		EN[2]=false;
		EN[3]=false;
		EN[4]=false;
		EN[5]=false;
		EN[6]=true;
		EN[7]=true;
	}
	else if(Process==Running || Process==Paused)
	{
		EN[0]=false;
		EN[1]=true;
		EN[2]=true;
		EN[3]=true;
		EN[4]=true;
		EN[5]=true;
		EN[6]=false;
		EN[7]=false;
	}
	for(int k=0; k<=7; k++)
	{
		STB[k]->Enabled=EN[k];
	}
#endif
}
//---------------------------------------------------------------------------

void __fastcall TRadarDialog::FormCloseQuery(TObject *Sender, bool &CanClose)
{
	CanClose=(Process!=Running);
}
//---------------------------------------------------------------------------

void __fastcall TRadarDialog::LaptopBatteryPressed(System::TObject* Sender, int X, int Y)
{
	try
	{
		if(((TGraphicObject*)Sender)->Pressed)
		{
			AnsiString str;
			float h, m;
			unsigned long color;

			if(RadarMapManager && RadarMapManager->LaptopBattery &&
				RadarMapManager->LaptopBattery->Status>bstNone)
			{
				str="";
				switch(RadarMapManager->LaptopBattery->Status)
				{
					case bstDischarging:
						if(RadarMapManager->LaptopBattery->LifeTime>0)
						{
							m=(float)RadarMapManager->LaptopBattery->LifeTime/60.;
							h=m/60.;
							m-=((int)h*60);
							if(h>0) str=IntToStr((int)h)+" hr ";
							str+=IntToStr((int)m)+" min ("+IntToStr((int)RadarMapManager->LaptopBattery->Percentage)+"%) remaining";
						}
						else str=IntToStr((int)RadarMapManager->LaptopBattery->Percentage)+"% remaining";
						break;
					case bstDischarged: str="Discharged. "+IntToStr((int)RadarMapManager->LaptopBattery->Percentage)+"% remaining"; break;
					case bstCharging: str="Charging. "+IntToStr((int)RadarMapManager->LaptopBattery->Percentage)+"% available"; break;
				}
				if(RadarMapManager->LaptopBattery->Percentage<10) color=mtStop;
				else color=mtText;
				if(str!=NULL && str.Length()>0)
					RadarMapManager->LaptopMessage=RadarMapManager->ShowMessageObject(
						RadarMapManager->LaptopMessage, RadarMapManager->RadarMessages,
						TMessageType::mtCustom, mvtDisapering, 5000, str,
						(AnsiString)_RadarMapName, SwitchOffLaptopBatteryMsg);
				if(RadarMapManager->LaptopMessage) RadarMapManager->LaptopMessage->Color=color;
			}
		}
	}
	catch(Exception &e) {}
}
//---------------------------------------------------------------------------

void __fastcall TRadarDialog::SwitchOffLaptopBatteryMsg()
{
	if(RadarMapManager && RadarMapManager->LaptopMessage)
	{
		RadarMapManager->LaptopMessage=NULL;
	}
}
//---------------------------------------------------------------------------

void __fastcall TRadarDialog::BatteryPressed(System::TObject* Sender, int X, int Y)
{
	try
	{
		if(((TGraphicObject*)Sender)->Pressed)
		{
			AnsiString str;
			float h, m;
			unsigned long color;

			if(RadarMapManager && RadarMapManager->Battery )
			{
				str="";
				if(RadarMapManager->LaptopBattery->Percentage<10) color=mtStop;
				else color=mtText;
				switch(RadarMapManager->Battery->Status )
				{
					case bstNone: color=mtStop; str="GPR is not connected!"; break;
					case bstDischarging: str="GPR Battery level is "+IntToStr((int)RadarMapManager->Battery->Percentage)+"% ("+FloatToStrF(RadarMapManager->Battery->Voltage, ffFixed, 5, 1)+" V)"; break;
					case bstDischarged: str="GPR Battery is discharged. "+IntToStr((int)RadarMapManager->Battery->Percentage)+"% remaining ("+FloatToStrF(RadarMapManager->Battery->Voltage, ffFixed, 5, 1)+" V)"; break;
					case bstCharging: str="GPR Battery is charging. "+IntToStr((int)RadarMapManager->Battery->Percentage)+"% available ("+FloatToStrF(RadarMapManager->Battery->Voltage, ffFixed, 5, 1)+" V)"; break;
					default:
					case bstUnknown: color=mtStop; str="GPR Battery is unknown!"; break;
				}

				if(str!=NULL && str.Length()>0)
					RadarMapManager->LaptopMessage=RadarMapManager->ShowMessageObject(
						RadarMapManager->LaptopMessage, RadarMapManager->RadarMessages,
						TMessageType::mtCustom, mvtDisapering, 5000, str, (AnsiString)_RadarMapName);
				if(RadarMapManager->LaptopMessage) RadarMapManager->LaptopMessage->Color=color;
			}
		}
	}
	catch(Exception &e) {}
}
//---------------------------------------------------------------------------

void __fastcall TRadarDialog::HyperbolaBtnPressed(System::TObject* Sender, int X, int Y)
{
	if(RadarMapManager && RadarMapManager->Settings)// && RadarMapManager->Hyperbola)
	{
		HyperbolaBtn->Down=!HyperbolaBtn->Down;
		if(HyperbolaBtn->Down)
		{
			if(!hyperbola) hyperbola=new THyperbolaTool(RadarMapManager->ObjectsContainer,
				ProfImage, RadarMapManager, HyperbolaBtn);
		}
		if(hyperbola)
		{
			hyperbola->Visible=HyperbolaBtn->Down;
            if(RadarMapManager->CurrentProfile)
				hyperbola->Permitivity=RadarMapManager->CurrentProfile->Permit;
			else hyperbola->Permitivity=RadarMapManager->Settings->Permitivity;
			HyperbolaBtn->Down=hyperbola->Visible;
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TRadarDialog::ZeroBtnPressed(System::TObject* Sender, int X, int Y)
{
	if(RadarMapManager && RadarMapManager->Settings)// && RadarMapManager->Hyperbola)
	{
		ZeroBtn->Down=!ZeroBtn->Down;
		if(ZeroBtn->Down)
		{
			if(!zeroline) zeroline=new TZeroPointTool(RadarMapManager->ObjectsContainer,
				ProfImage, RadarMapManager, ZeroBtn);
		}
		if(zeroline)
		{
			zeroline->Visible=ZeroBtn->Down;
			if(RadarMapManager->CurrentProfile)
				zeroline->ZeroPoint=(float)RadarMapManager->CurrentProfile->ZeroPoint/
					(float)RadarMapManager->CurrentProfile->Samples;
			else zeroline->ZeroPoint=zeroline->ZeroPoint;
			ZeroBtn->Down=zeroline->Visible;
		}
	}
}

//---------------------------------------------------------------------------
void __fastcall TAutoStopProfile::ExecuteLoopBody()
{
	int trcs;

	try
	{
		if(RadarDlg && Manager && Manager->Settings && Manager->Settings->AutoStopProfile &&
			Manager->ReceivingProfile)
		{
			trcs=Manager->Settings->AutoStopProfileSize << 20;//Bytes;
			trcs-=3200+400;
			trcs/=240+Manager->ReceivingProfile->Samples*sizeof(short int); //traces
			//for(int i=0; i<Manager->ReceivingProfilesQuantity; i++)
			if(Manager->ReceivingProfile->Traces>trcs)
			{
				RadarDlg->StopPressed();
				RadarDlg->StartPressed();
				Manager->ShowMessageObject(NULL, Manager->RadarMessages,
					TMessageType::mtInfo, mvtDisapering, 5000, "Profile reached maximum size and restarted", "Information");
				//break;
			}
		}
		MySleep(250);
	}
	catch(...) {}
}
