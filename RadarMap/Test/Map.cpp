//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Map.h"
#include "Radar.h"
#include "Preview.h"
#include "Settings.h"
#include "Files.h"
#include "Defines.h"
#include "InfoForm.h"
#include "Paths.h"
#include "DbDXFMapsContainer.h"
//#include "OnlineLoadDlg.h"
#include "OnlineMap.h"
#include "BingInetMap.h"
#include "OsmInetMap.h"
#include "DXFtoDbLoader.h"
#include "VectorDBForm.h"
#include "Layers.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "GR32_Image"
#pragma resource "*.dfm"

//---------------------------------------------------------------------------

__fastcall TMapForm::TMapForm(TComponent* Owner, TRadarMapManager* RMM)
	: TForm(Owner)
{
	//MapImage->Touch=Touch;
	//MapImage->OnGesture=OnGesture;

	MapImage->SetupBitmap(false, 0xFF000000u);
	MapImage->Bitmap->SetSize(MapImage->Width, MapImage->Height);
	MapImage->Bitmap->BeginUpdate();
	MapImage->Bitmap->Clear(Color32(Color));
	MapImage->Bitmap->FrameRectS(MapRect, clBlack32);
	MapImage->Bitmap->EndUpdate();
	MapImage->Bitmap->Changed();

	//myprogress=new TToolProgressBar(MapImage, NULL);

	MA=tmaHand;

	RadarMapManager=RMM;
	DblClick=false;
}
//---------------------------------------------------------------------------

__fastcall TMapForm::~TMapForm()
{
	if(RadarMapManager->StartStopWaypointBtn==StartStopWaypointBtn)
		RadarMapManager->StartStopWaypointBtn=NULL;
	//delete myprogress;
}

void __fastcall TMapForm::Initialize(void)
{
	MapToolbar=new TSliderToolbar(RadarMapManager->MapObjects, MapImage->GetViewportRect(), 48, staLeft, true);
	MapToolbar->FitButtonsOnly=true;
	MapToolbar->Visible=false;
	MapToolbar->Transparent=true;
	//MapToolbar->Align=staRight;

	/*SatellitesBtn=new TSliderToolButton(MapToolbar, Types::TRect(2, 2, 50, 50));
	SatellitesBtn->NormalGlyphName="PNGIMAGE_124";
	SatellitesBtn->Align=stbaFirst;
	SatellitesBtn->Enabled=true;
	SatellitesBtn->OnMouseClick=SatellitesClick;
	RadarMapManager->GpsUnit->Button=SatellitesBtn;*/

	SatellitesBtn=new TSliderToolbarChild(RadarMapManager->MapObjects, MapToolbar,
		MapImage->GetViewportRect(), 48, false);
	SatellitesBtn->Transparent=true;
	SatellitesBtn->FitButtonsOnly=true;
	SatellitesBtn->Visible=true;//false;
	SatellitesBtn->NormalGlyphName="PNGIMAGE_124";
	SatellitesBtn->OnMouseClick=SatellitesClick;
	RadarMapManager->GpsUnit->Button=SatellitesBtn->MinMaxButton;

	/*PosCorrectionBtn=new TSliderToolButton(SatellitesBtn, Types::TRect(2, 2, 50, 50));
	PosCorrectionBtn->NormalGlyphName="PNGIMAGE_246";
	PosCorrectionBtn->DisabledGlyphName="PNGIMAGE_250";
	PosCorrectionBtn->HotGlyphName="PNGIMAGE_247";
	PosCorrectionBtn->PressedGlyphName="PNGIMAGE_248";
	PosCorrectionBtn->Enabled=true;
	PosCorrectionBtn->OnMouseClick=PosCorrectionClick;

	PosRecoveryBtn=new TSliderToolButton(SatellitesBtn, Types::TRect(2, 2, 50, 50));
	PosRecoveryBtn->NormalGlyphName="PNGIMAGE_251";
	PosRecoveryBtn->DisabledGlyphName="PNGIMAGE_252";
	PosRecoveryBtn->HotGlyphName="PNGIMAGE_253";
	PosRecoveryBtn->PressedGlyphName="PNGIMAGE_254";
	PosRecoveryBtn->Enabled=true;
	PosRecoveryBtn->OnMouseClick=PosRecoveryClick;*/

	/*GPSToolbar->HotGlyphName="PNGIMAGE_237";
	GPSToolbar->DisabledGlyphName="PNGIMAGE_236";
	GPSToolbar->PressedGlyphName="PNGIMAGE_239";*/

	OpenBtn=new TSliderToolButton(MapToolbar, Types::TRect(2, 2, 50, 50));
	OpenBtn->NormalGlyphName="PNGIMAGE_85";
	OpenBtn->HotGlyphName="PNGIMAGE_86";
	//OpenBtn->PressedGlyphName="PNGIMAGE_93";
	OpenBtn->Enabled=true;
	OpenBtn->OnMouseClick=OpenClick;

	/*OnlineLoadBtn=new TSliderToolButton(MapToolbar, Types::TRect(2, 2, 50, 50));
	OnlineLoadBtn->NormalGlyphName="PNGIMAGE_85";
	OnlineLoadBtn->HotGlyphName="PNGIMAGE_86";
	OnlineLoadBtn->PressedGlyphName="PNGIMAGE_93";
	OnlineLoadBtn->Enabled=true;
	OnlineLoadBtn->OnMouseClick=OnlineLoadClick;*/

	LayersBtn=new TSliderToolButton(MapToolbar, Types::TRect(2, 52, 50, 100));
	LayersBtn->NormalGlyphName="PNGIMAGE_87";
	LayersBtn->DisabledGlyphName="PNGIMAGE_106";
	LayersBtn->HotGlyphName="PNGIMAGE_88";
	//LayersBtn->PressedGlyphName="PNGIMAGE_109";
	LayersBtn->Enabled=false;
	LayersBtn->OnMouseClick=LayersClick;

#ifdef _InfraRadarOnly
	AcrobatBtn=new TSliderToolButton(MapToolbar, Types::TRect(2, 52, 50, 100));
	AcrobatBtn->NormalGlyphName="PNGIMAGE_132";
	AcrobatBtn->DisabledGlyphName="PNGIMAGE_133";
	AcrobatBtn->HotGlyphName="PNGIMAGE_130";
	//AcrobatBtn->PressedGlyphName="PNGIMAGE_131";
	AcrobatBtn->Enabled=false;
	AcrobatBtn->OnMouseClick=AcrobatClick;

#else
	/* Obsolete
	AddPathBtn=new TSliderToolButton(MapToolbar, Types::TRect(2, 52, 50, 100));
	AddPathBtn->NormalGlyphName="PNGIMAGE_199";
	AddPathBtn->DisabledGlyphName="PNGIMAGE_197";
	AddPathBtn->HotGlyphName="PNGIMAGE_200";
	AddPathBtn->PressedGlyphName="PNGIMAGE_198";
	AddPathBtn->Enabled=true;
	AddPathBtn->OnMouseClick=AddPathClick;*/
#endif
	/* Obsolete
	InfoBtn=new TSliderToolButton(MapToolbar, Types::TRect(2, 52, 50, 100));
	InfoBtn->NormalGlyphName="PNGIMAGE_157";
	InfoBtn->DisabledGlyphName="PNGIMAGE_158";
	InfoBtn->HotGlyphName="PNGIMAGE_159";
	InfoBtn->PressedGlyphName="PNGIMAGE_160";
	InfoBtn->Enabled=true;
	InfoBtn->OnMouseClick=InfoClick;*/

	ZoomInBtn=new TSliderToolButton(MapToolbar, Types::TRect(2, 102, 50, 150));
	ZoomInBtn->NormalGlyphName="PNGIMAGE_89";
	ZoomInBtn->DisabledGlyphName="PNGIMAGE_96";
	ZoomInBtn->HotGlyphName="PNGIMAGE_90";
	//ZoomInBtn->PressedGlyphName="PNGIMAGE_94";
	ZoomInBtn->Enabled=true;//false;
	ZoomInBtn->OnMouseUp=ZoomInMouseUp;
//	ZoomInBtn->OnMouseDown=ZoomInMouseDown;
	//ZoomInBtn->OnMouseDblClick=ZoomInDblClick;

	ZoomOutBtn=new TSliderToolButton(MapToolbar, Types::TRect(2, 102, 50, 150));
	ZoomOutBtn->NormalGlyphName="PNGIMAGE_91";
	ZoomOutBtn->DisabledGlyphName="PNGIMAGE_97";
	ZoomOutBtn->HotGlyphName="PNGIMAGE_92";
	//ZoomOutBtn->PressedGlyphName="PNGIMAGE_95";
	ZoomOutBtn->Enabled=true;//false;
	ZoomOutBtn->OnMouseUp=ZoomOutMouseUp;
//	ZoomOutBtn->OnMouseDown=ZoomOutMouseDown;
	//ZoomOutBtn->OnMouseDblClick=ZoomOutDblClick;

	LabelsChildToolbar=new TSliderToolbarChild(RadarMapManager->MapObjects, MapToolbar,
		MapImage->GetViewportRect(), 48, false);
	LabelsChildToolbar->Transparent=true;
	LabelsChildToolbar->FitButtonsOnly=true;
	LabelsChildToolbar->Visible=true;//false;
	LabelsChildToolbar->NormalGlyphName="PNGIMAGE_268";
	LabelsChildToolbar->HotGlyphName="PNGIMAGE_267";
	//LabelsChildToolbar->"PNGIMAGE_269";
	LabelsChildToolbar->OnMouseClick=LabelsChildToolbarClick;

	PinBtn=new TSliderToolButton(LabelsChildToolbar, Types::TRect(2, 102, 50, 150));
	PinBtn->NormalGlyphName="PNGIMAGE_151";
	PinBtn->DisabledGlyphName="PNGIMAGE_152";
	PinBtn->HotGlyphName="PNGIMAGE_153";
	//PinBtn->PressedGlyphName="PNGIMAGE_154";
	PinBtn->Enabled=true;
	PinBtn->OnMouseClick=PinClick;

	StartNGoBtn=new TSliderToolButton(LabelsChildToolbar, Types::TRect(2, 102, 50, 150));
	StartNGoBtn->NormalGlyphName="PNGIMAGE_297";
	StartNGoBtn->DisabledGlyphName="PNGIMAGE_298";
	StartNGoBtn->HotGlyphName="PNGIMAGE_299";
	StartNGoBtn->Enabled=true;
	StartNGoBtn->OnMouseClick=StartNGoClick;

	StartNGoGPSBtn=new TSliderToolButton(LabelsChildToolbar, Types::TRect(2, 102, 50, 150));
	StartNGoGPSBtn->NormalGlyphName="PNGIMAGE_447";
	StartNGoGPSBtn->DisabledGlyphName="PNGIMAGE_448";
	StartNGoGPSBtn->HotGlyphName="PNGIMAGE_449";
	StartNGoGPSBtn->Enabled=true;//false;//
	StartNGoGPSBtn->OnMouseClick=StartNGoGPSClick;

	PredictionBtn=new TSliderToolButton(LabelsChildToolbar, Types::TRect(2, 102, 50, 150));
	PredictionBtn->NormalGlyphName="PNGIMAGE_472";
	PredictionBtn->DisabledGlyphName="PNGIMAGE_473";
	PredictionBtn->HotGlyphName="PNGIMAGE_474";
	PredictionBtn->Enabled=true;//false;//
	PredictionBtn->OnMouseClick=PredictionBtnClick;

	PinLineBtn=new TSliderToolButton(LabelsChildToolbar, Types::TRect(2, 102, 50, 150));
	PinLineBtn->NormalGlyphName="PNGIMAGE_475";
	PinLineBtn->DisabledGlyphName="PNGIMAGE_476";
	PinLineBtn->HotGlyphName="PNGIMAGE_477";
	PinLineBtn->Enabled=true;//false;//
	PinLineBtn->OnMouseClick=PinLineBtnClick;

	/*StartStopWaypointBtn=new TSliderToolButton(LabelsChildToolbar, Types::TRect(2, 2, 50, 50));
	StartStopWaypointBtn->NormalGlyphName="PNGIMAGE_256";
	StartStopWaypointBtn->HotGlyphName="PNGIMAGE_255";
	StartStopWaypointBtn->PressedGlyphName="PNGIMAGE_257";
	StartStopWaypointBtn->Enabled=true;
	StartStopWaypointBtn->OnMouseClick=StartStopWaypointClick;
	StartStopWaypointBtn->Tag=swkStart;
	RadarMapManager->StartStopWaypointBtn=StartStopWaypointBtn;*/

	FollowMeBtn=new TSliderToolButton(MapToolbar, Types::TRect(2, 2, 50, 50));
	FollowMeBtn->NormalGlyphName="PNGIMAGE_110";
	FollowMeBtn->DisabledGlyphName="PNGIMAGE_111";
	FollowMeBtn->HotGlyphName="PNGIMAGE_112";
	//FollowMeBtn->PressedGlyphName="PNGIMAGE_114";
	//FollowMeBtn->Align=stbaLast;
	FollowMeBtn->Enabled=true;//false;
	FollowMeBtn->OnMouseClick=FollowMeClick;

	FileToolbar=new TSliderToolbar(RadarMapManager->MapObjects, MapImage->GetViewportRect(), 48, staTop, true);
	FileToolbar->FitButtonsOnly=true;
	FileToolbar->Visible=false;
	FileToolbar->Transparent=true;

	NewBtn=new TSliderToolButton(FileToolbar, Types::TRect(2, 2, 50, 50));
	NewBtn->NormalGlyphName="PNGIMAGE_116";
	NewBtn->HotGlyphName="PNGIMAGE_117";
	NewBtn->Enabled=true;
	//NewBtn->Align=stbaFirst;
	NewBtn->OnMouseClick=NewClick;

	LoadBtn=new TSliderToolButton(FileToolbar, Types::TRect(2, 2, 50, 50));
	LoadBtn->NormalGlyphName="PNGIMAGE_98";
	LoadBtn->HotGlyphName="PNGIMAGE_99";
	//LoadBtn->PressedGlyphName="PNGIMAGE_108";
	LoadBtn->Enabled=true;
	LoadBtn->OnMouseClick=LoadClick;

	SaveBtn=new TSliderToolButton(FileToolbar, Types::TRect(52, 2, 100, 50));
	SaveBtn->NormalGlyphName="PNGIMAGE_100";
	SaveBtn->DisabledGlyphName="PNGIMAGE_101";
	SaveBtn->HotGlyphName="PNGIMAGE_102";
	//SaveBtn->PressedGlyphName="PNGIMAGE_107";
	SaveBtn->Enabled=true;//false;
	SaveBtn->OnMouseClick=SaveClick;

	PathsBtn=new TSliderToolButton(FileToolbar, Types::TRect(52, 2, 100, 50));
	PathsBtn->NormalGlyphName="PNGIMAGE_223";
	PathsBtn->DisabledGlyphName="PNGIMAGE_224";
	PathsBtn->HotGlyphName="PNGIMAGE_225";
	//PathsBtn->PressedGlyphName="PNGIMAGE_226";
	PathsBtn->Enabled=true;//false;
	PathsBtn->OnMouseClick=PathsClick;

	SettingsBtn=new TSliderToolButton(FileToolbar, Types::TRect(102, 2, 150, 50));
	SettingsBtn->NormalGlyphName="PNGIMAGE_103";
	SettingsBtn->HotGlyphName="PNGIMAGE_104";
	//SettingsBtn->PressedGlyphName="PNGIMAGE_105";
	SettingsBtn->Enabled=true;
	//SettingsBtn->Align=stbaLast;
	SettingsBtn->OnMouseClick=SettingsClick;

	ExportDXFBtn=new TSliderToolButton(FileToolbar, Types::TRect(52, 2, 100, 50));
	ExportDXFBtn->NormalGlyphName="PNGIMAGE_126";
	ExportDXFBtn->DisabledGlyphName="PNGIMAGE_127";
	ExportDXFBtn->HotGlyphName="PNGIMAGE_128";
	//ExportDXFBtn->PressedGlyphName="PNGIMAGE_129";
	ExportDXFBtn->Enabled=true;//false;
	ExportDXFBtn->OnMouseClick=ExportDXFBtnClick;

#ifndef _AutoSaveXYZCRestriction
	ExportXYZCBtn=new TSliderToolButton(FileToolbar, Types::TRect(52, 2, 100, 50));
	ExportXYZCBtn->NormalGlyphName="PNGIMAGE_134";
	ExportXYZCBtn->DisabledGlyphName="PNGIMAGE_135";
	ExportXYZCBtn->HotGlyphName="PNGIMAGE_136";
	//ExportXYZCBtn->PressedGlyphName="PNGIMAGE_137";
	ExportXYZCBtn->Enabled=true;//false;
	ExportXYZCBtn->OnMouseClick=ExportXYZCBtnClick;
#else
	ExportXYZCBtn=NULL;
#endif

	ExportTXTBtn=new TSliderToolButton(FileToolbar, Types::TRect(52, 2, 100, 50));
	ExportTXTBtn->NormalGlyphName="PNGIMAGE_162";
	ExportTXTBtn->DisabledGlyphName="PNGIMAGE_164";
	ExportTXTBtn->HotGlyphName="PNGIMAGE_161";
	//ExportTXTBtn->PressedGlyphName="PNGIMAGE_163";
	ExportTXTBtn->Enabled=true;//false;
	ExportTXTBtn->OnMouseClick=ExportTXTBtnClick;

	ToolsToolbar=new TSliderToolbar(RadarMapManager->MapObjects, MapImage->GetViewportRect(), 48, staBottom, true);
	ToolsToolbar->FitButtonsOnly=true;
	ToolsToolbar->Visible=false;
	ToolsToolbar->Transparent=true;

	ImageMapEditingBtn=new TSliderToolButton(ToolsToolbar, Types::TRect(52, 2, 100, 50));
	ImageMapEditingBtn->NormalGlyphName="PNGIMAGE_211";
	ImageMapEditingBtn->DisabledGlyphName="PNGIMAGE_212";
	ImageMapEditingBtn->HotGlyphName="PNGIMAGE_213";
	//ImageMapEditingBtn->PressedGlyphName="PNGIMAGE_214";
	ImageMapEditingBtn->Enabled=true;//false;
	ImageMapEditingBtn->DrawDownFrame=true;
	ImageMapEditingBtn->OnMouseClick=ImageMapEditingBtnClick;

	ReferenceBtn=new TSliderToolButton(ToolsToolbar, Types::TRect(52, 2, 100, 50));
	ReferenceBtn->NormalGlyphName="PNGIMAGE_202";
	ReferenceBtn->DisabledGlyphName="PNGIMAGE_201";
	ReferenceBtn->HotGlyphName="PNGIMAGE_203";
	//ReferenceBtn->PressedGlyphName="PNGIMAGE_204";
	ReferenceBtn->Enabled=true;//false;
	ReferenceBtn->DrawDownFrame=true;
	ReferenceBtn->OnMouseClick=ReferenceBtnClick;

	OpenMapToolbar=new TSliderToolbar(RadarMapManager->MapObjects, MapImage->GetViewportRect(), 128, staCenter, true);
	OpenMapToolbar->FitButtonsOnly=true;
	OpenMapToolbar->Visible=false;
	OpenMapToolbar->Title="OPEN MAP";
	OpenMapToolbar->ShowTitle=true;
	OpenMapToolbar->Transparent=true;
	OpenMapToolbar->Border=Color32(63, 103, 192, 255);
	OpenMapToolbar->Transparent=true;
	//OpenMapToolbar->Border=Color32(63, 103, 192, 230);

	KlicMapBtn=new TSliderToolButton(OpenMapToolbar, Types::TRect(2, 2, 130, 130));
	KlicMapBtn->NormalGlyphName="PNGIMAGE_286";
	KlicMapBtn->HotGlyphName="PNGIMAGE_285";
	KlicMapBtn->Enabled=true;
	KlicMapBtn->OnMouseClick=KlicMapBtnClick;/**/

	DXFMapBtn=new TSliderToolButton(OpenMapToolbar, Types::TRect(2, 2, 130, 130));
	DXFMapBtn->NormalGlyphName="PNGIMAGE_294";
	DXFMapBtn->HotGlyphName="PNGIMAGE_293";
	DXFMapBtn->Enabled=true;
	DXFMapBtn->OnMouseClick=DXFMapBtnClick;/**/

	ImageMapBtn=new TSliderToolButton(OpenMapToolbar, Types::TRect(2, 2, 130, 130));
	ImageMapBtn->NormalGlyphName="PNGIMAGE_296";
	ImageMapBtn->HotGlyphName="PNGIMAGE_295";
	ImageMapBtn->Enabled=true;
	ImageMapBtn->OnMouseClick=ImageMapBtnClick;/**/

	DBSLMapToolbar=new TSliderToolbarChild(RadarMapManager->MapObjects, OpenMapToolbar,
		MapImage->GetViewportRect(), 128, false);
	DBSLMapToolbar->Transparent=true;
	DBSLMapToolbar->FitButtonsOnly=true;
	DBSLMapToolbar->Visible=true;//false;
	DBSLMapToolbar->FullModality=false;
	DBSLMapToolbar->NormalGlyphName="PNGIMAGE_471";
	DBSLMapToolbar->HotGlyphName="PNGIMAGE_470";

	DBSLMapBtn=new TSliderToolButton(DBSLMapToolbar, Types::TRect(2, 2, 130, 130));
	DBSLMapBtn->NormalGlyphName="PNGIMAGE_292";
	DBSLMapBtn->HotGlyphName="PNGIMAGE_291";
	DBSLMapBtn->Enabled=true;
	DBSLMapBtn->OnMouseClick=DBSLMapBtnClick;

	DBSLMapSettingsBtn=new TSliderToolButton(DBSLMapToolbar, Types::TRect(2, 2, 130, 130));
	DBSLMapSettingsBtn->NormalGlyphName="PNGIMAGE_469";
	DBSLMapSettingsBtn->HotGlyphName="PNGIMAGE_468";
	DBSLMapSettingsBtn->Enabled=true;
	DBSLMapSettingsBtn->OnMouseClick=DBSLMapSettingsBtnClick;

	OnlineMapToolbar=new TSliderToolbarChild(RadarMapManager->MapObjects, OpenMapToolbar,
		MapImage->GetViewportRect(), 128, false);
	OnlineMapToolbar->Transparent=true;
	OnlineMapToolbar->FitButtonsOnly=true;
	OnlineMapToolbar->Visible=true;//false;
	OnlineMapToolbar->FullModality=false;
	OnlineMapToolbar->NormalGlyphName="PNGIMAGE_467";
	OnlineMapToolbar->HotGlyphName="PNGIMAGE_466";

	OnlineMapBtn=new TSliderToolButton(OnlineMapToolbar, Types::TRect(2, 2, 130, 130));
	OnlineMapBtn->NormalGlyphName="PNGIMAGE_288";
	OnlineMapBtn->HotGlyphName="PNGIMAGE_287";
	OnlineMapBtn->Enabled=true;
	OnlineMapBtn->OnMouseClick=OnlineMapBtnClick;

	OnlineMapXYBtn=new TSliderToolButton(OnlineMapToolbar, Types::TRect(2, 2, 130, 130));
	OnlineMapXYBtn->NormalGlyphName="PNGIMAGE_284";
	OnlineMapXYBtn->HotGlyphName="PNGIMAGE_283";
	OnlineMapXYBtn->Enabled=true;
	OnlineMapXYBtn->OnMouseClick=OnlineLoadClick;/**/
}
//---------------------------------------------------------------------------

void __fastcall TMapForm::OpenClick(System::TObject* Sender, int X, int Y)
{
	/*IFileDialog *pfd = NULL;
	HRESULT hr = CoCreateInstance(CLSID_FileOpenDialog, NULL, CLSCTX_INPROC_SERVER, IID_PPV_ARGS(&pfd));
	if (SUCCEEDED(hr))
	{
		IFileDialogEvents *pfde = NULL;
		hr = CDialogEventHandler_CreateInstance(IID_PPV_ARGS(&pfde));
        if (SUCCEEDED(hr))
		{
            // Hook up the event handler.
            DWORD dwCookie;
			hr = pfd->Advise(pfde, &dwCookie);
			if (SUCCEEDED(hr))
			{
				// Set up a Customization.
				IFileDialogCustomize *pfdc = NULL;
				hr = pfd->QueryInterface(IID_PPV_ARGS(&pfdc));
				if (SUCCEEDED(hr))
				{
					// Create a Visual Group.
					hr = pfdc->StartVisualGroup(2000, L"Sample Group");
					if (SUCCEEDED(hr))
					{
						// Add a radio-button list.
						hr = pfdc->AddControlItem() AddRadioButtonList(2);
						if (SUCCEEDED(hr))
						{
							// Set the state of the added radio-button list.
							hr = pfdc->SetControlState(2, CDCS_VISIBLE | CDCS_ENABLED);
							if (SUCCEEDED(hr))
							{
								// Add individual buttons to the radio-button list.
								hr = pfdc->AddControlItem(2,
														  1,
														  L"Change Title to Longhorn");
								if (SUCCEEDED(hr))
								{
									hr = pfdc->AddControlItem(2,
															  2,
															  L"Change Title to Vista");
									if (SUCCEEDED(hr))
                                    {
                                        // Set the default selection to option 1.
										hr = pfdc->SetSelectedControlItem(2,
																		  1);
                                    }
                                }
							}
                        }
						// End the visual group.
                        pfdc->EndVisualGroup();
                    }
					pfdc->Release();
				}
			}
		}

		if (SUCCEEDED(hr))
		{
			// Now show the dialog.
			hr = pfd->Show(NULL);
			if (SUCCEEDED(hr))
			{
				//
				// You can add your own code here to handle the results.
				//
			}
		}
		pfd->Release();
	}

	OPENFILENAME _ofn;
	UnicodeString _fileNameBuffer;

	_fileNameBuffer=UnicodeString();

	IFileDialog *pfd = NULL;
	HRESULT hr = CoCreateInstance(CLSID_FileOpenDialog,
					  NULL,
					  CLSCTX_INPROC_SERVER,
					  IID_PPV_ARGS(&pfd));
	if (SUCCEEDED(hr))
	{
		// Always use known folders instead of hard-coding physical file paths.
		// In this case we are using Public Music KnownFolder.
		IKnownFolderManager *pkfm = NULL;
		hr = CoCreateInstance(CLSID_KnownFolderManager,
					  NULL,
					  CLSCTX_INPROC_SERVER,
					  IID_PPV_ARGS(&pkfm));
		if (SUCCEEDED(hr))
		{
			// Get the known folder.
			IKnownFolder *pKnownFolder = NULL;
			hr = pkfm->FindFolderFromPath(((UnicodeString)"C:\\Windows").w_str(), FFFP_NEARESTPARENTMATCH, &pKnownFolder);
			if (SUCCEEDED(hr))
			{
				// File Dialog APIs need an IShellItem that represents the location.
				IShellItem *psi = NULL;
				hr = pKnownFolder->GetShellItem(0, IID_PPV_ARGS(&psi));
				if (SUCCEEDED(hr))
				{
					// Add the place to the bottom of default list in Common File Dialog.
					hr = pfd->AddPlace(psi, FDAP_BOTTOM);
					if (SUCCEEDED(hr))
					{
						// Show the File Dialog.
						_ofn.lStructSize = sizeof(OPENFILENAME);
						_ofn.lpstrFile = _fileNameBuffer.t_str();
						_ofn.nMaxFile = 260;
						_ofn.lpstrDefExt = "xml";
						_ofn.lpstrFileTitle = NULL;
						_ofn.nMaxFileTitle = 0;
						_ofn.lpstrFilter = "Maps";
						_ofn.Flags = OFN_EXPLORER | OFN_FILEMUSTEXIST;
						//_ofn.hInstance = _ipTemplate;
						//_ofn.lpfnHook = new OfnHookProc(MyHookProc);
						hr = pfd->Show(NULL);
						if (SUCCEEDED(hr))
						{
							//
							// You can add your own code here to handle the results.
							//
						}
					}
					psi->Release();
				}
				pKnownFolder->Release();
			}
			pkfm->Release();
		}
		pfd->Release();
	}*/

	TMapsContainer* mc=RadarMapManager->MapObjects->Container->GetObject(0);

	if(mc && mc->MapType!=mtEmpty)
	{
		RadarMapManager->ShowMessageObject(NULL, RadarMapManager->MapMessages,  TMessageType::mtWarning, mvtDisapering, 5000,
			"Sorry, this version could work with the one opened map at once only!", (AnsiString)_RadarMapName);
	}
	else OpenMapToolbar->ShowModal();
}
//---------------------------------------------------------------------------

void __fastcall TMapForm::FormResize(TObject *Sender)
{
	MapRect.Left=3;
	MapRect.Top=3;
	MapRect.Right=MapImage->Width-3;
	MapRect.Bottom=MapImage->Height-3;
}
//---------------------------------------------------------------------------

void __fastcall TMapForm::LayersClick(System::TObject* Sender, int X, int Y)
{
	try
	{
		if(Sender && ((TGraphicObject*)Sender)->Pressed)
		{
			ZoomInBtn->Down=false;
			DisableMouseAction();
			if(RadarMapManager->MapObjects && RadarMapManager->MapObjects->Container)
			{
				RadarMapManager->MapObjects->Container->GetLayers(LayersForm->TreeView);
				if(LayersForm->ShowModal()==mrOk)
				{
					RadarMapManager->MapObjects->Container->SetLayers(LayersForm->TreeView);
				}
			}
		}
	}
	catch(Exception &) {}
}
//---------------------------------------------------------------------------

#ifdef _InfraRadarOnly
void __fastcall TMapForm::AcrobatClick(System::TObject* Sender, int X, int Y)
{
	try
	{
		if(Sender && ((TGraphicObject*)Sender)->Pressed)
		{
			ZoomInBtn->Down=false;
			DisableMouseAction();
			TFilesForm *ff;

			ff=new TFilesForm(this);
			try
			{
				if(RadarMapManager->MapObjects && RadarMapManager->MapObjects->Container)
				{
					RadarMapManager->MapObjects->Container->GetMapFoldersContent(ff->TreeView, 1, ".pdf");
					ff->ShowModal();
				}
			}
			__finaly
			{
				delete ff;
			}
		}
	}
	catch(Exception &e) {}
}

#else
void __fastcall TMapForm::AddPathClick(System::TObject* Sender, int X, int Y)
{
	try
	{
		if(Sender && ((TGraphicObject*)Sender)->Pressed)
		{
			ZoomInBtn->Down=false;
			DisableMouseAction();

			TPreviewForm* PreviewForm;
			TCursor cur;
			bool res=false;

			if(RadarMapManager)
			{
				cur=Cursor;
				PreviewForm=new TPreviewForm(this, (TObject*)RadarMapManager);
				try
				{
					//PreviewForm->GMLOpenDialog->FilterIndex=RadarMapManager->Settings->OpenMapType;
					PreviewForm->GMLOpenDialog->Filter="SEG-Y Profile (*.sgy)|*.sgy|All files (*.*|*.*";
					PreviewForm->GMLOpenDialog->FilterIndex=0;
					PreviewForm->GMLOpenDialog->Options=PreviewForm->GMLOpenDialog->Options << ofAllowMultiSelect;
					if(PreviewForm->GMLOpenDialog->Execute())
					{
						Cursor=crHourGlass;
						//RadarMapManager->MapObjects->Container->Progress=RadarMapManager->MapProgress;//MyProgress;
						for(int i=0; i<PreviewForm->GMLOpenDialog->Files->Count; i++)
						{
							try
							{
								res=RadarMapManager->LoadPathFromSGY(PreviewForm->GMLOpenDialog->Files->Strings[i]);
							}
							catch(Exception &e)
							{
								res=false;
							}
							if(!res)
							{
								RadarMapManager->ShowMessageObject(NULL, RadarMapManager->MapMessages,  TMessageType::mtWarning, mvtDisapering, 5000,
									"Cannot open file "+PreviewForm->GMLOpenDialog->Files->Strings[i]+"!", "Error");
								break;
							}
						}
					}
				}
				__finally
				{
					delete PreviewForm;
					Cursor=cur;
				}
			}
		}
	}
	catch(Exception &e) {}
}
#endif
//---------------------------------------------------------------------------

void __fastcall TMapForm::NewClick(System::TObject* Sender, int X, int Y)
{
	int id;

	try
	{
		if(Sender && ((TGraphicObject*)Sender)->Pressed)
		{
			if(RadarMapManager->Connection)
				RadarMapManager->ShowMessageObject(NULL, RadarMapManager->MapMessages, TMessageType::mtWarning, mvtDisapering, 10000,
					"Stop data acquisition first!", (AnsiString)_RadarMapName);
			else
			{
				if(RadarMapManager->ResultsNotSaved)
				{
					id=Application->MessageBox(L"Your current results are not saved. Are you sure you want to continue?",
						((UnicodeString)_RadarMapName+" - New Project").w_str(), MB_YESNO | MB_ICONQUESTION);
					if(id==ID_NO) return;
				}
				else id=Application->MessageBox(L"Your current results will be lost. Are you sure you want to proceed?",
					((UnicodeString)_RadarMapName+" - New Project").w_str(), MB_ICONQUESTION | MB_YESNO);
				if(id==ID_YES)
				{
					RadarMapManager->NewProject();
					SaveBtn->Enabled=true;//false;
					ExportDXFBtn->Enabled=true;//false;
					if(ExportXYZCBtn) ExportXYZCBtn->Enabled=true;//false;
					ExportTXTBtn->Enabled=true;//false;

					FollowMeBtn->Enabled=true;//false;
					FollowMeBtn->Down=false;
					PinBtn->Enabled=true;//false;
					LayersBtn->Enabled=false;
#ifdef _InfraRadarOnly
					AcrobatBtn->Enabled=false;
#else
					//AddPathBtn->Enabled=true;
#endif
					//InfoBtn->Enabled=true;
					ZoomInBtn->Enabled=true;//false;
					ZoomOutBtn->Enabled=true;//false;
					ToolsToolbar->Visible=false;
					ImageMapEditingBtn->Pressed=false;
					DisableMouseAction();
				}
			}
		}
	}
	catch(Exception &e) {}
}
//---------------------------------------------------------------------------

void __fastcall TMapForm::LoadClick(System::TObject* Sender, int X, int Y)
{
	try
	{
		if(Sender && ((TGraphicObject*)Sender)->Pressed)
		{
			TPreviewForm* PreviewForm;
			TCursor cur;
			int ms=0, i;
			TTimeStamp t1, t2;

			if(RadarMapManager)
			{
				if(RadarMapManager->ResultsNotSaved)
				{
					int mi=Application->MessageBox(L"Your current results are not saved. Are you sure you want to continue?",
						((UnicodeString)_RadarMapName+" - Open Project").w_str(), MB_YESNO | MB_ICONQUESTION);
					if(mi==ID_NO) return;
				}
				cur=Cursor;
				PreviewForm=new TPreviewForm(this, (TObject*)RadarMapManager);
				try
				{
					//PreviewForm->GMLOpenDialog->FilterIndex=RadarMapManager->Settings->OpenMapType;
					if(PreviewForm->GMLOpenDialog->Execute())
					{
						try
						{
							Cursor=crHourGlass;
							//RadarMapManager->Settings->OpenMapType=PreviewForm->GMLOpenDialog->FilterIndex;
							t1=DateTimeToTimeStamp(Now());
							while(PreviewForm->PreviewInProgress>0 && ms<7500)
							{
								t2=DateTimeToTimeStamp(Now());
								ms=t2.Time-t1.Time;
								ProcessMessages();
								MySleep(100);
							}
							if(PreviewForm->PreviewInProgress>0)
							{
								RadarMapManager->ShowMessageObject(NULL, RadarMapManager->MapMessages,  TMessageType::mtWarning, mvtDisapering, 5000,
									"There are no enough resources! Please switch off Preview setting and try to open it again.", (AnsiString)_RadarMapName);
							}
							else
							{
								//RadarMapManager->MapObjects->Container->Progress=RadarMapManager->MapProgress;//MyProgress;
								RadarMapManager->LoadResultsFromGML(PreviewForm->GMLOpenDialog->FileName);
								LayersBtn->Enabled=true;
		#ifdef _InfraRadarOnly
								AcrobatBtn->Enabled=true;
		#else
								//AddPathBtn->Enabled=true;
		#endif
								//InfoBtn->Enabled=true;
								ZoomInBtn->Enabled=true;
								ZoomOutBtn->Enabled=true;
								PinBtn->Enabled=true;
								SaveBtn->Enabled=true;
								ExportDXFBtn->Enabled=true;
								if(ExportXYZCBtn) ExportXYZCBtn->Enabled=true;
								ExportTXTBtn->Enabled=true;
								i=1;
								TMapsContainer* mc=RadarMapManager->MapObjects->Container->GetObject(0);
								while(mc)
								{
									mc=RadarMapManager->MapObjects->Container->GetObject(i);
									if(mc && mc->MapType==mtAdjustable)
									{
										ToolsToolbar->Visible=true;
										if(((TImageMapsContainer*)mc)->ReferencesCount<ReferencesPointsMaxQ)
										{
											ImageMapEditingBtn->Down=false;
											ImageMapEditingBtn->Pressed=true;
											((TImageMapsContainer*)mc)->State=bfsDormancy;
											ImageMapEditingBtnClick(ImageMapEditingBtn, 0, 0);
										}
										else
										{
											ImageMapEditingBtn->Down=true;
											ImageMapEditingBtn->Pressed=true;
											((TImageMapsContainer*)mc)->State=bfsReferencing;
											ImageMapEditingBtnClick(ImageMapEditingBtn, 0, 0);
										}
										break;
									}
									i++;
								}
								if(RadarMapManager->GpsUnit) RadarMapManager->GpsUnit->ReCenter();
							}
						}
						catch(Exception &e)
						{
							RadarMapManager->ShowMessageObject(NULL, RadarMapManager->MapMessages,  TMessageType::mtWarning, mvtDisapering, 5000,
								"Cannot open file "+PreviewForm->GMLOpenDialog->FileName+"!", "Error");
							RadarMapManager->NewProject();
						}
					}
				}
				__finally
				{
					delete PreviewForm;
					Cursor=cur;
				}
			}
		}
	}
	catch(Exception &e) {}
}
//---------------------------------------------------------------------------

void __fastcall TMapForm::SaveClick(System::TObject* Sender, int X, int Y)
{
	try
	{
		if(Sender && ((TGraphicObject*)Sender)->Pressed)
		{
			TPreviewForm* PreviewForm;
			TCursor cur;
			int ms=0;
			TTimeStamp t1, t2;
			AnsiString ext, filename;
			TMapsContainer *mc;

			if(RadarMapManager && RadarMapManager->MapObjects && RadarMapManager->MapObjects->Container)
			{
				cur=Cursor;
				PreviewForm=new TPreviewForm(this, (TObject*)RadarMapManager);
				try
				{
					mc=RadarMapManager->MapObjects->Container->GetObject(0);
					if(mc)
					{
						PreviewForm->SaveDialog->InitialDir=ExtractFilePath(mc->FileName);
						ext=ExtractFileExt(mc->Klicnummer);
						filename=mc->Klicnummer;
						if(ext!=NULL && ext.Length()>0)
						{
							filename.SetLength(filename.Length()-ext.Length());
						}
						PreviewForm->SaveDialog->FileName=filename+"_out.gml";
					}
					else PreviewForm->SaveDialog->FileName="Results_out.gml";
					if(PreviewForm->SaveDialog->Execute(Handle))
					{
						Cursor=crHourGlass;

						t1=DateTimeToTimeStamp(Now());
						while(PreviewForm->PreviewInProgress>0 && ms<5000)
						{
							t2=DateTimeToTimeStamp(Now());
							ms=t2.Time-t1.Time;
							ProcessMessages();
							MySleep(10);
						}
						if(PreviewForm->PreviewInProgress>0)
						{
							RadarMapManager->ShowMessageObject(NULL, RadarMapManager->MapMessages,  TMessageType::mtWarning, mvtDisapering, 5000,
								"There are no enough resources! Please switch off Preview setting and try to save it again.", (AnsiString)_RadarMapName);
						}
						else
						{
							RadarMapManager->SaveResultsToGML(PreviewForm->SaveDialog->FileName);
						}
					}
				}
				__finally
				{
					delete PreviewForm;
					Cursor=cur;
				}
			}
		}
	}
	catch(Exception &e) {}
}
//---------------------------------------------------------------------------

void __fastcall TMapForm::PathsClick(System::TObject* Sender, int X, int Y)
{
	TPathsForm* PathsForm;

	if(RadarMapManager && RadarMapManager->Satellites)
	{
		PathsForm=new TPathsForm(this, RadarMapManager);
		try
		{
			PathsForm->ShowModal();
		}
		__finally
		{
			delete PathsForm;
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TMapForm::PauseTerminal()
{
	if(RadarMapManager->TwoWheelClient) RadarMapManager->TwoWheelClient->PauseTerminal();
	if(RadarMapManager->GpsClient) RadarMapManager->GpsClient->PauseTerminal();
}
//---------------------------------------------------------------------------

void __fastcall TMapForm::ResumeTerminal()
{
	if(RadarMapManager->TwoWheelClient) RadarMapManager->TwoWheelClient->ResumeTerminal();
	if(RadarMapManager->GpsClient) RadarMapManager->GpsClient->ResumeTerminal();
}
//---------------------------------------------------------------------------

void __fastcall TMapForm::WriteCommand(char *cmd)
{
	if(RadarMapManager->TwoWheelClient) RadarMapManager->TwoWheelClient->WriteCommand(cmd);
	if(RadarMapManager->GpsClient) RadarMapManager->GpsClient->WriteCommand(cmd);
}
//---------------------------------------------------------------------------

void __fastcall TMapForm::SettingsClick(System::TObject* Sender, int X, int Y)
{
	try
	{
		if(Sender && ((TGraphicObject*)Sender)->Pressed)
		{
			TRadarMapSettings _settings;
			TProfile* profile;
			int c=0;

			SettingsForm->RadarMapManager=RadarMapManager;
#ifdef _AssignRadarMapSettingsAsCopy
            SettingsForm->RadarMapSettings.Copy(RadarMapManager->Settings);
			_settings.Copy(&(SettingsForm->RadarMapSettings));
#else
			SettingsForm->RadarMapSettings=*RadarMapManager->Settings;
			_settings=SettingsForm->RadarMapSettings;
#endif
			if(RadarMapManager->GpsClient)
				RadarMapManager->GpsClient->Terminal->Memo=SettingsForm->Memo1;
			if(RadarMapManager->TwoWheelClient)
				RadarMapManager->TwoWheelClient->Terminal->Memo=SettingsForm->Memo1;
			//RadarMapManager->GpsClient->Terminal->Memo=NULL;
			if(RadarMapManager->TwoWheelClient)
			{
				SettingsForm->PauseTerminalIButton->Stretch=!RadarMapManager->TwoWheelClient->Terminal->ShowLines;
			}
			else
			{
				SettingsForm->PauseTerminalIButton->Stretch=!RadarMapManager->GpsClient->Terminal->ShowLines;
			}
			SettingsForm->WriteCommand=WriteCommand;
			SettingsForm->PauseTerminal=PauseTerminal;
			SettingsForm->ResumeTerminal=ResumeTerminal;
			SettingsForm->ApplyPalette(RadarMapManager->Palette);
			if(SettingsForm->ShowModal()==mrOk)
			{
				RadarMapManager->ApplyNewSettings(SettingsForm->RadarMapSettings);
				if(RadarMapManager->Settings->EasyZoomIn && ZoomInBtn->Down)
				{
					ZoomInBtn->Down=false;
					DisableMouseAction();
				}
			}
			else
			{
				if(!RadarMapManager->Connection) RadarMapManager->ApplyNewSettings(_settings);
			}
			RadarMapManager->Palette->LoadPaletteFromRegistry((AnsiString)((AnsiString)_RadarMapRegKey+"\\Palettes").c_str(), "DefaultPalette");
			for(int i=0; i<RadarMapManager->Satellites->Count; i++)
			{
				if(RadarMapManager->Satellites->Items[i])
					for(int j=0; j<RadarMapManager->Satellites->Items[i]->ProfilesCount; j++)
						if(RadarMapManager->Satellites->Items[i]->Profiles[j])
						{
							profile=RadarMapManager->Satellites->Items[i]->OutputView->Profile;
							c+=RadarMapManager->Palette->ConvertToWinColors(profile->Colors, 256);
						}
			}
			if(c>0) RadarMapManager->SwitchToPath(RadarMapManager->CurrentPath);
			SettingsForm->RadarMapManager=NULL;
			if(RadarMapManager->GpsClient)
				RadarMapManager->GpsClient->Terminal->Memo=NULL;
			if(RadarMapManager->TwoWheelClient)
				RadarMapManager->TwoWheelClient->Terminal->Memo=NULL;
		}
	}
	catch(Exception &e) {}
}
//---------------------------------------------------------------------------

void __fastcall TMapForm::ExitClick(System::TObject* Sender, int X, int Y)
{
	try
	{
		if(Sender && ((TGraphicObject*)Sender)->Pressed)
		{
			int mr, mr1;

			mr=Application->MessageBox(L"Do you want to proceed?", L"Exit from program", MB_YESNO);
			if(mr==IDYES) Application->Terminate();
		}
	}
	catch(Exception &e) {}
}
//---------------------------------------------------------------------------

void __fastcall TMapForm::FollowMeClick(System::TObject* Sender, int X, int Y)
{
	try
	{
		if(Sender && ((TGraphicObject*)Sender)->Pressed)
		{
			FollowMeBtn->Down=!FollowMeBtn->Down;
			if(RadarMapManager->Settings->GPSTracking && RadarMapManager->TrackingPath)
			{
				if(RadarMapManager->Settings->FollowMe)
					RadarMapManager->TrackingPath->FollowMe=FollowMeBtn->Down;
				else
				{
					RadarMapManager->ShowMessageObject(NULL, RadarMapManager->MapMessages,  TMessageType::mtWarning, mvtDisapering, 5000,
						"Function 'Follow Me' is switched OFF in the settings. Switch it ON and try again.", (AnsiString)_RadarMapName);
					FollowMeBtn->Down=false;
				}
			}
			else
			{
				RadarMapManager->ShowMessageObject(NULL, RadarMapManager->MapMessages,  TMessageType::mtWarning, mvtDisapering, 5000,
					"Function 'Permanent GPS tracking' is switched OFF in the settings. Switch it ON and try again.", (AnsiString)_RadarMapName);
				FollowMeBtn->Down=false;
			}
		}
	}
	catch(Exception &e) {}
}
//---------------------------------------------------------------------------

void __fastcall TMapForm::SatellitesClick(System::TObject* Sender, int X, int Y)
{
	try
	{
		if(Sender && ((TGraphicObject*)Sender)->Pressed)
		{
			AnsiString gga="", gsa="", llq="", gst="", str;

			if(RadarMapManager->GpsClient->NmeaSummary->Get(nstGGA))
				gga=RadarMapManager->GpsClient->NmeaSummary->Get(nstGGA)->AsString(false);
			if(RadarMapManager->GpsClient->NmeaSummary->Get(nstLLQ))
				llq=RadarMapManager->GpsClient->NmeaSummary->Get(nstLLQ)->AsString(false);
			if(RadarMapManager->GpsClient->NmeaSummary->Get(nstGSA))
				gsa=RadarMapManager->GpsClient->NmeaSummary->Get(nstGSA)->AsString(false);
			if(RadarMapManager->GpsClient->NmeaSummary->Get(nstGST))
				gst=RadarMapManager->GpsClient->NmeaSummary->Get(nstGST)->AsString(false);

			if(gga!="") str+=gga;
			if(llq!="")
			{
				if(str!="") str+=", ";
				str+=llq;
			}
			if(gsa!="")
			{
				if(str!="") str+=", ";
				str+=gsa;
			}
			if(gst!="")
			{
				if(str!="") str+=", ";
				str+=gst;
			}
			/*if(str!="")	RadarMapManager->ShowMessageObject(NULL, RadarMapManager->MapMessages,
				TMessageType::mtText, mvtDisapering, 10000, str, (AnsiString)_RadarMapName);*/
			if(str=="") str="Not connected or Initializing...";
			if(!RadarMapManager->GpsMessage)
			{
				RadarMapManager->GpsMessage=RadarMapManager->ShowMessageObject(
					RadarMapManager->GpsMessage, RadarMapManager->GpsMessages,
					TMessageType::mtCustom, mvtConstant, 0, str, (AnsiString)_RadarMapName);
				if(RadarMapManager->GpsMessage) RadarMapManager->GpsMessage->Color=mtRoutine;
			}
			else
			{
				RadarMapManager->GpsMessage->SwitchOff();
				RadarMapManager->GpsMessage=NULL;
			}
			DisableMouseAction();
		}
	}
	catch(Exception &e) {}
}
//---------------------------------------------------------------------------

void __fastcall TMapForm::ExportDXFBtnClick(System::TObject* Sender, int X, int Y)
{
	try
	{
		if(Sender && ((TGraphicObject*)Sender)->Pressed &&
			RadarMapManager && RadarMapManager->MapObjects && RadarMapManager->MapObjects->Container)
		{
			TPreviewForm* PreviewForm;
			TCursor cur;
			int ms=0;
			TTimeStamp t1, t2;
			AnsiString ext, filename;
			TMapsContainer *mc;

			cur=Cursor;
			PreviewForm=new TPreviewForm(this, (TObject*)RadarMapManager);
			try
			{
				PreviewForm->ExportDialog->Filter="AutoCAD Drawing eXchange Format Files (*.dxf)|*.dxf";
				mc=RadarMapManager->MapObjects->Container->GetObject(0);
				if(mc)
				{
					PreviewForm->ExportDialog->InitialDir=ExtractFilePath(mc->FileName);
					ext=ExtractFileExt(mc->Klicnummer);
					filename=mc->Klicnummer;
					if(ext!=NULL && ext.Length()>0)
					{
						filename.SetLength(filename.Length()-ext.Length());
					}
					PreviewForm->ExportDialog->FileName=filename+"_out.dxf";
				}
				else PreviewForm->ExportDialog->FileName="Results_out.dxf";
				if(PreviewForm->ExportDialog->Execute(Handle))
				{
					Cursor=crHourGlass;
					filename=PreviewForm->ExportDialog->FileName;
					ext=ExtractFileExt(filename);
					if(ext==NULL || ext.LowerCase()!=".dxf") filename+=".dxf";
					t1=DateTimeToTimeStamp(Now());
					while(PreviewForm->PreviewInProgress>0 && ms<5000)
					{
						t2=DateTimeToTimeStamp(Now());
						ms=t2.Time-t1.Time;
						ProcessMessages();
						MySleep(10);
					}
					if(PreviewForm->PreviewInProgress>0)
					{
						RadarMapManager->ShowMessageObject(NULL, RadarMapManager->MapMessages,  TMessageType::mtWarning, mvtDisapering, 5000,
							"There are no enough resources! Please switch off Preview setting and try to export it again.", (AnsiString)_RadarMapName);
					}
					else
					{
						RadarMapManager->ExportResultsToDXF(filename);
					}
				}
			}
			__finally
			{
				delete PreviewForm;
				Cursor=cur;
			}
		}
	}
	catch(Exception &e) {}
}
//---------------------------------------------------------------------------

void __fastcall TMapForm::ExportXYZCBtnClick(System::TObject* Sender, int X, int Y)
{
	try
	{
		if(Sender && ((TGraphicObject*)Sender)->Pressed &&
			RadarMapManager && RadarMapManager->MapObjects && RadarMapManager->MapObjects->Container)
		{
			TPreviewForm* PreviewForm;
			TCursor cur;
			int ms=0;
			TTimeStamp t1, t2;
			AnsiString ext, filename;
			TMapsContainer *mc;
			cur=Cursor;
			PreviewForm=new TPreviewForm(this, (TObject*)RadarMapManager);
			try
			{
				PreviewForm->ExportDialog->Filter="XYZC Data Text Files (*.txt)|*.txt";
				mc=RadarMapManager->MapObjects->Container->GetObject(0);
				if(mc)
				{
					PreviewForm->ExportDialog->InitialDir=ExtractFilePath(mc->FileName);
					ext=ExtractFileExt(mc->Klicnummer);
					filename=mc->Klicnummer;
					if(ext!=NULL && ext.Length()>0)
					{
						filename.SetLength(filename.Length()-ext.Length());
					}
					PreviewForm->ExportDialog->FileName=filename+"_XYZC_out.txt";
				}
				else PreviewForm->ExportDialog->FileName="Results_XYZC_out.txt";
				if(PreviewForm->ExportDialog->Execute(Handle))
				{
					Cursor=crHourGlass;
					filename=PreviewForm->ExportDialog->FileName;
					ext=ExtractFileExt(filename);
					if(ext==NULL || ext.LowerCase()!=".txt") filename+=".txt";
					t1=DateTimeToTimeStamp(Now());
					while(PreviewForm->PreviewInProgress>0 && ms<5000)
					{
						t2=DateTimeToTimeStamp(Now());
						ms=t2.Time-t1.Time;
						ProcessMessages();
						MySleep(10);
					}
					if(PreviewForm->PreviewInProgress>0)
					{
						RadarMapManager->ShowMessageObject(NULL, RadarMapManager->MapMessages,  TMessageType::mtWarning, mvtDisapering, 5000,
							"There are no enough resources! Please switch off Preview setting and try to export it again.", (AnsiString)_RadarMapName);
					}
					else
					{
						RadarMapManager->ExportResultsToXYZC(filename);
					}
				}
			}
			__finally
			{
				delete PreviewForm;
				Cursor=cur;
			}
		}
	}
	catch(Exception &e) {}
}
//---------------------------------------------------------------------------

void __fastcall TMapForm::ExportTXTBtnClick(System::TObject* Sender, int X, int Y)
{
	try
	{
		if(Sender && ((TGraphicObject*)Sender)->Pressed &&
			RadarMapManager && RadarMapManager->MapObjects && RadarMapManager->MapObjects->Container)
		{
			TPreviewForm* PreviewForm;
			TCursor cur;
			int ms=0;
			TTimeStamp t1, t2;
			AnsiString ext, filename;
			TMapsContainer *mc;

			cur=Cursor;
			PreviewForm=new TPreviewForm(this, (TObject*)RadarMapManager);
			try
			{
				PreviewForm->ExportDialog->Filter="ASCII Data Text Files (*.txt)|*.txt";
				mc=RadarMapManager->MapObjects->Container->GetObject(0);
				if(mc)
				{
					PreviewForm->ExportDialog->InitialDir=ExtractFilePath(mc->FileName);
					ext=ExtractFileExt(mc->Klicnummer);
					filename=mc->Klicnummer;
					if(ext!=NULL && ext.Length()>0)
					{
						filename.SetLength(filename.Length()-ext.Length());
					}
					PreviewForm->ExportDialog->FileName=filename+"_ASCII_out.txt";
				}
				else PreviewForm->ExportDialog->FileName="Results_ASCII_out.txt";
				if(PreviewForm->ExportDialog->Execute(Handle))
				{
					Cursor=crHourGlass;
					filename=PreviewForm->ExportDialog->FileName;
					ext=ExtractFileExt(filename);
					if(ext==NULL || ext.LowerCase()!=".txt") filename+=".txt";
					t1=DateTimeToTimeStamp(Now());
					while(PreviewForm->PreviewInProgress>0 && ms<5000)
					{
						t2=DateTimeToTimeStamp(Now());
						ms=t2.Time-t1.Time;
						ProcessMessages();
						MySleep(10);
					}
					if(PreviewForm->PreviewInProgress>0)
					{
						RadarMapManager->ShowMessageObject(NULL, RadarMapManager->MapMessages,  TMessageType::mtWarning, mvtDisapering, 5000,
							"There are no enough resources! Please switch off Preview setting and try to export it again.", (AnsiString)_RadarMapName);
					}
					else
					{
						RadarMapManager->ExportResultsToTXT(filename);
					}
				}
			}
			__finally
			{
				delete PreviewForm;
				Cursor=cur;
			}
		}
	}
	catch(Exception &e) {}
}
//---------------------------------------------------------------------------

void __fastcall TMapForm::InfoClick(System::TObject* Sender, int X, int Y)
{
	try
	{
		if(Sender && ((TGraphicObject*)Sender)->Pressed)
		{
			if(RadarMapManager && RadarMapManager->Satellites)
			{
				if(RadarMapManager->Satellites->GeneratedInfo=="")
					RadarMapManager->Satellites->GenerateInfo();
				InfoForm->GeneratedInfo=RadarMapManager->Satellites->GeneratedInfo;
				InfoForm->BriefInfo=RadarMapManager->Satellites->BriefInfo;
				if(InfoForm->ShowModal()==mrOk)
				{
					RadarMapManager->Satellites->BriefInfo=InfoForm->BriefInfo;
					RadarMapManager->ResultsNotSaved=true;
				}
			}
		}
	}
	catch(Exception &e) {}
}
//---------------------------------------------------------------------------

void __fastcall TMapForm::ReferenceBtnClick(System::TObject* Sender, int X, int Y)
{
	try
	{
		if(Sender && ((TGraphicObject*)Sender)->Pressed)
		{
			if(ReferenceBtn->Down)
			{
				ReferenceBtn->Down=false;
			}
			else
			{
				if(RadarMapManager && RadarMapManager->MapObjects && RadarMapManager->MapObjects->Container &&
					RadarMapManager->MapObjects->Container->Count>0)
				{
					TMapsContainer *mc=NULL;
					Types::TPoint sp;
					TMapLabel *ml;
					int i=0;

					try
					{
						do
						{
							mc=RadarMapManager->MapObjects->Container->GetObject(i);
							i++;
						} while(mc && mc->MapType!=mtAdjustable);
						if(mc && mc->MapType==mtAdjustable)// && mc->MapType!=mtEmpty)
						{
							ReferenceBtn->Down=true;
							MA=tmaReferenceFlag;
							/*
							ml=new TMapLabel(RadarMapManager->MapObjects, RadarMapManager->MapObjects->Image->GetViewportRect(),
								RadarMapManager->MapObjects->Container->LatLonRect, NULL, RadarMapManager);
							ml->NormalGlyph=LoadPNG32Resource((System::UnicodeString)"PNGIMAGE_156");
							if(ml->NormalGlyph) ml->Center=Types::TPoint(16, 28);//Types::TPoint(ml->NormalGlyph->Width>>1,  ml->NormalGlyph->Height);
							else ml->Center=Types::TPoint(0, 0);
							ml->AllwaysHot=true;
							ml->Icons=true;
							ml->Coordinate=new TGPSCoordinate(RadarMapManager->TrackingPath->LastPoint);
							ml->GPSConfidence=ml->Coordinate->Confidence;
							if(RadarMapManager->Settings && RadarMapManager->Settings->ShowLabelInformationOnPin)
								ml->MouseUp(Sender, mbRight, TShiftState(ssRight), 0, 0, NULL);
							ml->Color=clRed32;
							*/
						}
					}
					catch(Exception &e) {}
				}
			}
		}
	}
	catch(Exception &e) {}
}
//---------------------------------------------------------------------------

void __fastcall TMapForm::ImageMapEditingBtnClick(System::TObject* Sender, int X, int Y)
{
	try
	{
		if(Sender && ((TGraphicObject*)Sender)->Pressed)
		{
			if(RadarMapManager && RadarMapManager->Satellites)
			{
				int i=0;
				TMapsContainer* mc;
				do
				{
					mc=RadarMapManager->MapObjects->Container->GetObject(i);
					if(mc && mc->MapType==mtAdjustable)
					{
						if(((TImageMapsContainer*)mc)->State==bfsDormancy)
						{
							ReferenceBtn->Enabled=true;
							ImageMapEditingBtn->Down=true;
							((TImageMapsContainer*)mc)->State=bfsReferencing;
						}
						else
						{
							ReferenceBtn->Down=false;
							DisableMouseAction();
							ReferenceBtn->Enabled=false;
							ImageMapEditingBtn->Down=false;
							((TImageMapsContainer*)mc)->State=bfsDormancy;
						}
						break;
					}
					i++;
				} while(mc);
			}
		}
	}
	catch(Exception &e) {}
}
//---------------------------------------------------------------------------

void __fastcall TMapForm::DisableMouseAction()
{
	TList *list;

	switch(MA)
	{
		case tmaReferenceFlag: ReferenceBtn->Down=false; break;
		case tmaPosCorrection: PosCorrectionBtn->Down=false; break;
		case tmaStartStopWaypoint: StartStopWaypointBtn->Down=false; break;
		case tmaStartNGo: StartNGoBtn->Down=false; break;
		case tmaPrediction: PredictionBtn->Down=false; break;
		case tmaMapLabelPolyLineGraphics:
			try
			{
				TMapLabel *ml;

				list=new TList();
				if(RadarMapManager && RadarMapManager->MapObjects)
				{
					if(RadarMapManager->MapObjects->GetMapLabels(list)>0)
					{
                        for(int i=0; i<list->Count; i++)
						{
							ml=dynamic_cast<TMapLabel*>((TObject*)list->Items[i]);
							if(ml)
							{
								ml->SpiderWeb=false;
								ml->DeactiveGeometry();
							}
						}
                    }
				}
			}
			__finally
			{
				delete list;
            }
			PinLineBtn->Down=false;
			break;
		case tmaZoomIn:
		case tmaNone:
		case tmaHand:
		default:
			break;
	}
	MA=tmaHand;
}
//---------------------------------------------------------------------------

void __fastcall TMapForm::FormCanResize(TObject *Sender, int &NewWidth, int &NewHeight,
		  bool &Resize)
{
	if(NewWidth<50) Resize=false;
}
//---------------------------------------------------------------------------

void __fastcall TMapForm::PosCorrectionClick(System::TObject* Sender, int X, int Y)
{
	try
	{
		if(PosCorrectionBtn->Pressed)
		{
			PosCorrectionBtn->Down=!PosCorrectionBtn->Down;
			if(PosCorrectionBtn->Down)
			{
				if(RadarMapManager->TrackingPath && RadarMapManager->GpsUnit)
				{
					RadarMapManager->TrackingPath->GpsUnit=RadarMapManager->GpsUnit;
					MA=tmaPosCorrection;
				}
				else
				{
					RadarMapManager->ShowMessageObject(NULL, RadarMapManager->MapMessages,  TMessageType::mtWarning, mvtDisapering, 5000,
						"Cannot detect GPS unit!", (AnsiString)_RadarMapName);
					DisableMouseAction();
				}
			}
			else
			{
				DisableMouseAction();
				if(RadarMapManager->TrackingPath) RadarMapManager->TrackingPath->GpsUnit=NULL;
			}
		}
	}
	catch(Exception &e) {}
}
//---------------------------------------------------------------------------

void __fastcall TMapForm::PosRecoveryClick(System::TObject* Sender, int X, int Y)
{
	if(RadarMapManager->TrackingPath && RadarMapManager->GpsUnit)
	{
		RadarMapManager->GpsUnit->SetOffset(0, 0);
	}
}
//---------------------------------------------------------------------------

void __fastcall TMapForm::StartStopWaypointClick(System::TObject* Sender, int X, int Y)
{
	if(StartStopWaypointBtn->Pressed)
	{
		StartStopWaypointBtn->Down=!StartStopWaypointBtn->Down;
		if(StartStopWaypointBtn->Down) MA=tmaStartStopWaypoint;
		else DisableMouseAction(); //MA=tmaHand;
	}
}
//---------------------------------------------------------------------------

void __fastcall TMapForm::OnlineLoadClick(System::TObject* Sender, int X, int Y) {

	try {
		if(Sender && ((TGraphicObject*)Sender)->Pressed &&
			RadarMapManager && RadarMapManager->MapObjects && RadarMapManager->MapObjects->Container) {
			TCursor cur;
			TTimeStamp t1, t2;
			UnicodeString ext;
			TMapsContainer* mc;
			TOnlineMapForm *omf;

			ZoomInBtn->Down = false;
			MA = tmaHand;

			/*if (true) {
				TDXFtoDbLoader *db_loader = new TDXFtoDbLoader(RadarMapManager, "C:\\Users\\Radar Systems Inc\\Documents\\RAD Studio\\Projects\\IRView\\DbDxf\\database\\Zakusala_Georadaram.dxf",
					"C:\\Users\\Radar Systems Inc\\Documents\\RAD Studio\\Projects\\IRView\\DbDxf\\database\\geo2.dbsl", RadarMapManager->MapProgress);
					OpenMapToolbar->Hide();
				return;
			}*/


			/*if (RadarMapManager->Settings->BingInetMapAppKey.Length() == 0) {
				// RadarMapManager->Settings->BingInetMapAppKey = "AkPES-CO6ZeMDRdT686WU4ncn8onOufX977CBuMj8f9XxbQrR_qNtfSeQqBdtX71";

				RadarMapManager->ShowMessageObject(NULL, NULL, TMessageType::mtStop, mvtDisapering, 5000,
					"Sorry, You do not have access to the service (not available Bing Maps Key). "
					"See details on https://www.microsoft.com/maps/create-a-bing-maps-key.aspx."
					"\n\nBing Maps Key you need to specify in the program settings.", (AnsiString)_RadarMapName);

				OpenMapToolbar->Hide();
				return;
			}*/
			try
			{
				omf=new TOnlineMapForm(this, RadarMapManager, RadarMapManager->MapObjects->Container->CoordinateSystem);
#ifdef OnlyOneMap
				mc = RadarMapManager->MapObjects->Container->GetObject(0);
				if(mc && mc->MapType != mtEmpty) {
					RadarMapManager->ShowMessageObject(NULL, RadarMapManager->MapMessages, TMessageType::mtWarning, mvtDisapering, 5000,
						"Sorry, this version could work with the one opened map at once only!", (AnsiString)_RadarMapName);
					return;
				}
#endif
				cur = Cursor;
			/*if (NULL == OnlineLoadDlg) {
				OnlineLoadDlg = new TOnlineLoadDlg(this);
			} */
				//if ((OnlineMapForm->ShowModal() == mrOk) &&
				if (omf->ShowModal() == mrOk &&
					RadarMapManager && RadarMapManager->MapObjects &&
					RadarMapManager->MapObjects->Container) {

					//TBingInetMap *bing_map = new TBingInetMap(RadarMapManager->MapObjects->Container, RadarMapManager->Settings, RadarMapManager->MapObjects->Container->ViewportArea);
#ifdef OnlyOneMap
					RadarMapManager->MapObjects->Container->Clear();
					RadarMapManager->MapObjects->Zoom->Clear();
#endif
					RadarMapManager->Settings->DefaultCoordinateSystem = omf->CoordinateSystem;
					//RadarMapManager->MapObjects->Container->CoordinateSystem = omf->CoordinateSystem;

					TInetMapBase *bing_map = new TOsmInetMap(RadarMapManager->MapObjects->Container, RadarMapManager->Settings,
						RadarMapManager->MapObjects->Container->ViewportArea);

					bool b=bing_map->LockedCS;
					bing_map->LockedCS=false;
					bing_map->CoordinateSystem = omf->CoordinateSystem;
					bing_map->LockedCS=b;
					bing_map->MapImageWidth = MaxBitmapWidth;
					bing_map->MapImageHeight = MaxBitmapHeight;
					bing_map->MapCenterLatitude = omf->MapCenter->Latitude;// OnlineLoadDlg->MapCenterLatitude;
					bing_map->MapCenterLongitude = omf->MapCenter->Longitude;// OnlineLoadDlg->MapCenterLongitude;
					bing_map->MapImageZoom = omf->MapImageZoom; //OnlineLoadDlg->MapImageZoom;
					bing_map->MapViewType = imvtMap;//OnlineLoadDlg->MapViewType;

//					if (bing_map->LoadMapData(NULL, true)) {
//						this->ShowImageData(true);
//						this->Update();
//						this->LayersBtn->Enabled = true;
//					}
					new TInetMapLoader(RadarMapManager, bing_map, bing_map->Progress);
					OpenMapToolbar->Hide();
				}
			}
			__finally {
				Cursor = cur;
				if(RadarMapManager && RadarMapManager->Satellites) {
					RadarMapManager->Satellites->GeneratedInfo = "";
				}
				delete omf;
			}
		}
	}
	catch(Exception & e) {
	}
}
//---------------------------------------------------------------------------

bool __fastcall TMapForm::OpenMap(TMapType mt)
{
	bool res=false;
	TDbDXFMapsContainer *dxf_map;
	TImageMapsContainer *imc;
	Types::TRect vpt;//, bmr;
	float wc, hc, c;
	TMapsContainer *mc=NULL;

	try
	{
		if(RadarMapManager && RadarMapManager->MapObjects && RadarMapManager->MapObjects->Container)
		{
			TPreviewForm* PreviewForm;
			TCursor cur;
			int ms=0;
			TTimeStamp t1, t2;
			UnicodeString ext;

			ZoomInBtn->Down=false;
			DisableMouseAction();
#ifdef OnlyOneMap
			mc=RadarMapManager->MapObjects->Container->GetObject(0);
			if(mc && mc->MapType!=mtEmpty)
			{
				RadarMapManager->ShowMessageObject(NULL, RadarMapManager->MapMessages,  TMessageType::mtWarning, mvtDisapering, 5000,
					"Sorry, this version could work with the one opened map at once only!", (AnsiString)_RadarMapName);
				return false;
			}
			/*else
			{
				RadarMapManager->MapObjects->Container->Clear();
				RadarMapManager->MapObjects->Zoom->Clear();
			}*/
#endif
			cur=Cursor;
			PreviewForm=new TPreviewForm(this, (TObject*)RadarMapManager);
			try
			{
				/*if (this->RadarMapManager->Settings->DbDXFAllowed) {
					TDbDXFMapsContainer::SetupDlgFilter(PreviewForm->OpenDialog);
				}*/

				switch(mt)
				{
					case mtXML:
						PreviewForm->OpenDialog->Filter="Kadaster.nl maps (*.xml)|*.xml|All files (*.*)|*.*";
						break;
					case mtDXF:
						PreviewForm->OpenDialog->Filter="AutoCAD DDrawing eXchange Format Files (*.dxf)|*.dxf|All files (*.*)|*.*";
						break;
					case mtSQLiteDXF:
						PreviewForm->OpenDialog->Filter="Database files (*.dbsl)|*.dbsl|All files (*.*)|*.*";
						break;
					case mtAdjustable:
						PreviewForm->OpenDialog->Filter="Images|*.jpg;*.jpeg;*.bmp;*.png;*.gif;*.tif;*.tiff;*.emf;*.wmf|All files (*.*)|*.*";
						break;
					default:
						PreviewForm->OpenDialog->Filter="All files (*.*)|*.*";
				}
				PreviewForm->OpenDialog->FilterIndex=0;
				if(PreviewForm->OpenDialog->Execute(Handle) && RadarMapManager &&
					RadarMapManager->MapObjects && RadarMapManager->MapObjects->Container)
				{
					Cursor=crHourGlass;
#ifdef OnlyOneMap
					RadarMapManager->MapObjects->Container->Clear();
					RadarMapManager->MapObjects->Zoom->Clear();
#endif
					RadarMapManager->Settings->OpenMapType=PreviewForm->OpenDialog->FilterIndex;
					t1=DateTimeToTimeStamp(Now());
					while(PreviewForm->PreviewInProgress>0 && ms<2000)
					{
						t2=DateTimeToTimeStamp(Now());
						ms=t2.Time-t1.Time;
						ProcessMessages();
						MySleep(100);
					}
					if(PreviewForm->PreviewInProgress>0)
					{
						RadarMapManager->ShowMessageObject(NULL, RadarMapManager->MapMessages,  TMessageType::mtWarning, mvtDisapering, 5000,
							"There are no enough resources! Please switch off Preview setting and try to open it again.", (AnsiString)_RadarMapName);
					}
					else
					{
						//RadarMapManager->MapObjects->Container->Progress=RadarMapManager->MapProgress;//MyProgress;
						ToolsToolbar->Visible=false;
						try
						{
							if(RadarMapManager->GlobalStopItEvent)
								ResetEvent(RadarMapManager->GlobalStopItEvent);
							switch(mt)
							{
								case mtXML:
									mc=new TXMLMapsContainer(RadarMapManager->MapObjects->Container,
										RadarMapManager->Settings, RadarMapManager->MapObjects->Container->ViewportArea);
									mc->LoadDataAsThread(PreviewForm->OpenDialog->FileName, RadarMapManager->GlobalStopItEvent, (TVoidFunction)NULL, (TVoidFunction)NULL);
									break;
								case mtDXF:
									mc=new TDXFMapsContainer(RadarMapManager->MapObjects->Container,
										RadarMapManager->Settings, RadarMapManager->MapObjects->Container->ViewportArea);
									mc->LoadDataAsThread(PreviewForm->OpenDialog->FileName, RadarMapManager->GlobalStopItEvent, (TVoidFunction)NULL, (TVoidFunction)NULL);
									break;
								case mtSQLiteDXF:
	//								(new TDbDXFMapsContainer(RadarMapManager->MapObjects->Container, RadarMapManager->Settings))->LoadData(PreviewForm->OpenDialog->FileName, NULL);
									mc = new TDbDXFMapsContainer(RadarMapManager->MapObjects->Container,
										RadarMapManager->Settings, RadarMapManager->MapObjects->Container->ViewportArea);

	//                                dxf_map->ShowMapArea = TDoubleRect(507421.67, 309925.0685, 508262.7137, 309116.82);
//									dxf_map->ShowMapArea = TDoubleRect(507830.9, 309525.0685, 507940.7137, 309416.82);
//									dxf_map->ResetShowArea();
									mc->LoadDataAsThread(PreviewForm->OpenDialog->FileName, RadarMapManager->GlobalStopItEvent, (TVoidFunction)NULL, (TVoidFunction)NULL);
									break;
								case mtAdjustable:
								//else if(ext==".PNG" || ext==".JPEG" || ext==".JPG" || ext==".BMP" ||
								//	ext==".GIF" || ext==".TIF" || ext==".TIFF" || ext==".EMF" || ext==".WMF")
									mc=new TImageMapsContainer(RadarMapManager->MapObjects->Container,
										RadarMapManager->Settings, RadarMapManager->MapObjects->Container->ViewportArea, RadarMapManager->MapObjects);
									mc->LoadData(PreviewForm->OpenDialog->FileName, RadarMapManager->GlobalStopItEvent);
									if(RadarMapManager->MapObjects->Image)
									{
										if(RadarMapManager->MapObjects->Zoom)
										{
											vpt=RadarMapManager->MapObjects->Image->GetViewportRect();
											imc=(TImageMapsContainer*)mc;
											//bmr=RadarMapManager->MapObjects->Image->GetBitmapRect();
											if(imc->RawPxRect && imc->RawPxRect->Width()>0 && imc->RawPxRect->Height()>0 &&
												vpt.Width()>0 && vpt.Height()>0)
											{
												wc=(float)vpt.Width()/(float)imc->RawPxRect->Width();
												hc=(float)vpt.Height()/(float)imc->RawPxRect->Height();
												if(wc<hc) c=wc;
												else c=hc;
												c*=RadarMapManager->MapObjects->Zoom->CurrentZoom;
												if(c<1.0)
												{
													RadarMapManager->MapObjects->Zoom->SetCurrentZoomEx(c/1.2);
													RadarMapManager->MapObjects->Zoom->ApplyZoomAtXY(vpt.Width() >> 1, vpt.Height() >> 1);
												}
												ToolsToolbar->Visible=true;
												if(((TImageMapsContainer*)mc)->ReferencesCount<ReferencesPointsMaxQ)
												{
													ImageMapEditingBtn->Down=false;
													ImageMapEditingBtn->Pressed=true;
													((TImageMapsContainer*)mc)->State=bfsDormancy;
													ImageMapEditingBtnClick(ImageMapEditingBtn, 0, 0);
												}
												else
												{
													ImageMapEditingBtn->Down=true;
													ImageMapEditingBtn->Pressed=true;
													((TImageMapsContainer*)mc)->State=bfsReferencing;
													ImageMapEditingBtnClick(ImageMapEditingBtn, 0, 0);
												}
											}
										}
									}/**/
									break;
								default:
									mc=new TEmptyMapsContainer(RadarMapManager->MapObjects->Container, RadarMapManager->Settings, RadarMapManager->MapObjects->Container->ViewportArea);
									mc->OnDisableButton=DisableMouseAction;
									RadarMapManager->ShowMessageObject(NULL, RadarMapManager->MapMessages,  TMessageType::mtWarning, mvtDisapering, 5000,
										"Unknown map format!", (AnsiString)_RadarMapName);
									return res;
							}
							mc->OnDisableButton=DisableMouseAction;

							if(RadarMapManager->Settings->FollowMe)
								FollowMeBtn->Enabled=true;
							FollowMeBtn->Down=false;
							LayersBtn->Enabled=true;
		#ifdef _InfraRadarOnly
							AcrobatBtn->Enabled=true;
		#else
							//AddPathBtn->Enabled=true;
		#endif
							//InfoBtn->Enabled=true;
							ZoomInBtn->Enabled=true;
							ZoomOutBtn->Enabled=true;
							PinBtn->Enabled=true;
							SaveBtn->Enabled=true;
							ExportDXFBtn->Enabled=true;
							if(ExportXYZCBtn) ExportXYZCBtn->Enabled=true;
							ExportTXTBtn->Enabled=true;
							ReferenceBtn->Enabled=ToolsToolbar->Visible;
							ImageMapEditingBtn->Enabled=ToolsToolbar->Visible;
							if(RadarMapManager->GpsUnit) RadarMapManager->GpsUnit->ReCenter();
							res=true;
						}
						catch(Exception &e)
						{
							//RadarMapManager->MapObjects->Container->Clear();
							mc=new TEmptyMapsContainer(RadarMapManager->MapObjects->Container, RadarMapManager->Settings, RadarMapManager->MapObjects->Container->ViewportArea);
							mc->OnDisableButton=DisableMouseAction;
							RadarMapManager->ShowMessageObject(NULL, RadarMapManager->MapMessages,  TMessageType::mtWarning, mvtDisapering, 5000,
								"Cannot open the map!", (AnsiString)_RadarMapName);
						}
					}
				}
				/*else
				{
					mc=new TEmptyMapsContainer(RadarMapManager->MapObjects->Container, RadarMapManager->Settings, RadarMapManager->MapObjects->Container->ViewportArea);
					mc->OnDisableButton=DisableMouseAction;
				}*/
			}
			__finally
			{
				delete PreviewForm;
				Cursor=cur;
				if(RadarMapManager && RadarMapManager->Satellites) RadarMapManager->Satellites->GeneratedInfo="";
			}
		}
	}
	catch(Exception &e) {}

	return res;
}
//---------------------------------------------------------------------------

void __fastcall TMapForm::KlicMapBtnClick(System::TObject* Sender, int X, int Y)
{
	if(OpenMap(mtXML)) OpenMapToolbar->Hide();
}
//---------------------------------------------------------------------------

void __fastcall TMapForm::DXFMapBtnClick(System::TObject* Sender, int X, int Y)
{
	if(OpenMap(mtDXF)) OpenMapToolbar->Hide();
}
//---------------------------------------------------------------------------

void __fastcall TMapForm::ImageMapBtnClick(System::TObject* Sender, int X, int Y)
{
	if(OpenMap(mtAdjustable)) OpenMapToolbar->Hide();
}
//---------------------------------------------------------------------------

void __fastcall TMapForm::DBSLMapBtnClick(System::TObject* Sender, int X, int Y)
{
	if(OpenMap(mtSQLiteDXF)) OpenMapToolbar->Hide();
}
//---------------------------------------------------------------------------

void __fastcall TMapForm::DBSLMapSettingsBtnClick(System::TObject* Sender, int X, int Y)
{
	TVectorDBForm *vdbf;
	AnsiString FileName="";
	TCursor cur;

	if(RadarMapManager && RadarMapManager->MapObjects && RadarMapManager->MapObjects->Container)
	{
		vdbf=new TVectorDBForm(this, RadarMapManager, RadarMapManager->MapObjects->Container->CoordinateSystem);
		if(vdbf->ShowModal()==mrOk && vdbf->FileName!="" && FileExists(vdbf->FileName))
		{
			TMapsContainer* mc;

			FileName=vdbf->FileName;
			RadarMapManager->Settings->DefaultCoordinateSystem=vdbf->CoordinateSystem;
			RadarMapManager->MapObjects->Container->CoordinateSystem=vdbf->CoordinateSystem;
			delete vdbf;
			vdbf=NULL;
            ZoomInBtn->Down=false;
			DisableMouseAction();
#ifdef OnlyOneMap
			mc=RadarMapManager->MapObjects->Container->GetObject(0);
			if(mc && mc->MapType!=mtEmpty)
			{
				RadarMapManager->ShowMessageObject(NULL, RadarMapManager->MapMessages,  TMessageType::mtWarning, mvtDisapering, 5000,
					"Sorry, this version could work with the one opened map at once only!", (AnsiString)_RadarMapName);
				return;
			}
			else
			{
				RadarMapManager->MapObjects->Container->Clear();
				RadarMapManager->MapObjects->Zoom->Clear();
			}
#endif
			cur=Cursor;
			OpenMapToolbar->Hide();

			Cursor=crHourGlass;
			ToolsToolbar->Visible=false;
			try
			{
				if(RadarMapManager->GlobalStopItEvent)
					ResetEvent(RadarMapManager->GlobalStopItEvent);
				mc=new TDbDXFMapsContainer(RadarMapManager->MapObjects->Container,
					RadarMapManager->Settings, RadarMapManager->MapObjects->Container->ViewportArea);
				mc->LoadDataAsThread(FileName, RadarMapManager->GlobalStopItEvent, (TVoidFunction)NULL, (TVoidFunction)NULL);
				mc->OnDisableButton=DisableMouseAction;
			}
			catch(Exception &e)
			{
				//RadarMapManager->MapObjects->Container->Clear();
				mc=new TEmptyMapsContainer(RadarMapManager->MapObjects->Container, RadarMapManager->Settings, RadarMapManager->MapObjects->Container->ViewportArea);
				mc->OnDisableButton=DisableMouseAction;
				RadarMapManager->ShowMessageObject(NULL, RadarMapManager->MapMessages,  TMessageType::mtWarning, mvtDisapering, 5000,
					"Cannot open the map!", (AnsiString)_RadarMapName);
			}
		}
		else
		{
            delete vdbf;
			vdbf=NULL;
        }
		if(RadarMapManager->Settings->FollowMe)
			FollowMeBtn->Enabled=true;
		FollowMeBtn->Down=false;
		LayersBtn->Enabled=true;
#ifdef _InfraRadarOnly
		AcrobatBtn->Enabled=true;
#else
		//AddPathBtn->Enabled=true;
#endif
		//InfoBtn->Enabled=true;
		ZoomInBtn->Enabled=true;
		ZoomOutBtn->Enabled=true;
		PinBtn->Enabled=true;
		SaveBtn->Enabled=true;
		ExportDXFBtn->Enabled=true;
		if(ExportXYZCBtn) ExportXYZCBtn->Enabled=true;
		ExportTXTBtn->Enabled=true;
		ReferenceBtn->Enabled=ToolsToolbar->Visible;
		ImageMapEditingBtn->Enabled=ToolsToolbar->Visible;
		if(RadarMapManager->GpsUnit) RadarMapManager->GpsUnit->ReCenter();
		Cursor=cur;
	}
}
//---------------------------------------------------------------------------

void __fastcall TMapForm::OnlineMapBtnClick(System::TObject* Sender, int X, int Y)
{
	if(RadarMapManager && RadarMapManager->MapObjects && RadarMapManager->MapObjects->Container)
	{
		try {
			TCursor cur;
			TTimeStamp t1, t2;
			UnicodeString ext;
			TMapsContainer* mc;
	#ifdef OnlyOneMap
			mc = RadarMapManager->MapObjects->Container->GetObject(0);
			if (mc) {
				if(mc && mc->MapType != mtEmpty) {
					RadarMapManager->ShowMessageObject(NULL, RadarMapManager->MapMessages, TMessageType::mtWarning, mvtDisapering, 5000,
						"Sorry, this version could work with the one opened map at once only!", (AnsiString)_RadarMapName);
					return;
				}
				else {
					RadarMapManager->MapObjects->Container->Clear();
					RadarMapManager->MapObjects->Zoom->Clear();
				}
			}
	#endif
			ZoomInBtn->Down = false;
			MA = tmaHand;
			cur = Cursor;

			try {
				//TBingInetMap *bing_map = new TBingInetMap(RadarMapManager->MapObjects->Container, RadarMapManager->Settings,
				//	RadarMapManager->MapObjects->Container->ViewportArea);
				TInetMapBase *bing_map = new TOsmInetMap(RadarMapManager->MapObjects->Container, RadarMapManager->Settings,
						RadarMapManager->MapObjects->Container->ViewportArea);

				/*bool b=bing_map->LockedCS;
				bing_map->LockedCS=false;
				bing_map->CoordinateSystem = RadarMapManager->Settings->DefaultCoordinateSystem;
				bing_map->LockedCS=b;*/

				bing_map->MapImageWidth = MaxBitmapWidth;
				bing_map->MapImageHeight = MaxBitmapHeight;
				bing_map->WaitForMapCenter = true;

				// if (bing_map->LoadMapData(NULL, true)) {
				// this->ShowImageData(true);
				// this->Update();
				// this->LayersBtn->Enabled = true;
				// }
				new TInetMapLoader(RadarMapManager, bing_map, bing_map->Progress);
			}
			__finally {
				Cursor = cur;
				if(RadarMapManager && RadarMapManager->Satellites) {
					RadarMapManager->Satellites->GeneratedInfo = "";
				}
			}

		}
		catch(...) {}
	}
	OpenMapToolbar->Hide();
}
//---------------------------------------------------------------------------

void __fastcall TMapForm::CancelMapBtnClick(System::TObject* Sender, int X, int Y)
{
	OpenMapToolbar->Hide();
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
// ::ZoomIn
//---------------------------------------------------------------------------

void __fastcall TMapForm::ZoomInMouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y)
{
	if(!DblClick)
	{
		ButtonTimer->Enabled=false;
		if(!OnTime && RadarMapManager && RadarMapManager->MapObjects && RadarMapManager->MapObjects->Zoom &&
			RadarMapManager->MapObjects->Image)
		{
			bool std_zoom = this->ZoomBingMap(true);

			if (!std_zoom) {
				ZoomInBtn->Down = false;
			} else {
				if(RadarMapManager->Settings->EasyZoomIn) {
					RadarMapManager->MapObjects->Zoom->ZoomIn();
					ZoomInBtn->Down = false;
				}
				else {
					ZoomInBtn->Down =!ZoomInBtn->Down;
					if(ZoomInBtn->Down)
						MA = tmaZoomIn;
					else
						DisableMouseAction();
				}
			}
		}
		else ZoomInBtn->Down=false;
	}
	else DblClick=false;
}
//---------------------------------------------------------------------------

void __fastcall TMapForm::ZoomInDblClick(System::TObject* Sender, int X, int Y)
{
	ZoomInBtn->Down=false;
	if(!OnTime && RadarMapManager && RadarMapManager->MapObjects && RadarMapManager->MapObjects->Zoom) RadarMapManager->MapObjects->Zoom->ZoomFastIn();
	ButtonTimer->Enabled=false;
	DisableMouseAction();
	DblClick=true;
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
// ::ZoomOut
//---------------------------------------------------------------------------

void __fastcall TMapForm::ZoomOutMouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y) {

	if(!DblClick) {
		ZoomInBtn->Down = false;

		if(!OnTime && RadarMapManager && RadarMapManager->MapObjects && RadarMapManager->MapObjects->Zoom) {
			bool std_zoom = this->ZoomBingMap(false);

			if (!std_zoom) {
				this->Update();
			} else {
				RadarMapManager->MapObjects->Zoom->ZoomOut();
			}
		}
		ButtonTimer->Enabled = false;
		DisableMouseAction();
	}
	else {
		DblClick = false;
	}
}
//---------------------------------------------------------------------------

void __fastcall TMapForm::ZoomOutDblClick(System::TObject* Sender, int X, int Y)
{
	ZoomInBtn->Down=false;
	if(!OnTime && RadarMapManager && RadarMapManager->MapObjects && RadarMapManager->MapObjects->Zoom)
		RadarMapManager->MapObjects->Zoom->ZoomFastOut();
	ButtonTimer->Enabled=false;
	DisableMouseAction();
	DblClick=true;
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
// ::ZoomMap
//---------------------------------------------------------------------------
bool __fastcall TMapForm::ZoomBingMap(bool pIsZoomIn) {
	TMapsContainersCollection *map_list = RadarMapManager->MapObjects->Container;
	TMapsContainer *map = NULL;
	bool std_zoom = true;
	int mz;

	for (int i = 0; i < map_list->Count; i++) {
		map = map_list->GetObject(i);

		//if (mtBingInet == map->MapType) {
		if (map && mtOnlineMap & map->MapType) {
			//TBingInetMap *bing_map = static_cast<TBingInetMap *>(map);
			TInetMapBase *bing_map = static_cast<TBingInetMap *>(map);

			mz = bing_map->MapImageZoom;
			if (pIsZoomIn) {
				bing_map->MapImageZoom++;
			}
			else {
				bing_map->MapImageZoom--;
			}
			if (mz != bing_map->MapImageZoom) {
				new TInetMapLoader(RadarMapManager, bing_map, bing_map->Progress);
				this->RadarMapManager->MapObjects->Zoom->CurrentZoom = 1;
//				std_zoom = false;
			}
			switch(bing_map->MapImageZoom)
			{
				case 1: this->RadarMapManager->MapObjects->MapScale->ZoomText="1st level";
				case 2: this->RadarMapManager->MapObjects->MapScale->ZoomText="2nd level";
				case 3: this->RadarMapManager->MapObjects->MapScale->ZoomText="3rd level";
				default: this->RadarMapManager->MapObjects->MapScale->ZoomText=IntToStr(bing_map->MapImageZoom)+"th level";
			}
			std_zoom = false;
			break;
		}
	}
	return std_zoom;
}
//---------------------------------------------------------------------------

TDoublePoint __fastcall TMapForm::GetDbDxfMapViewCenterPosition(TDoubleRect pShowMapArea) {
	Types::TRect visible_area = this->MapImage->GetViewportRect();
	int center_x = (int) (fabs(this->MapImage->OffsetHorz) + visible_area.Width() / 2);
	int center_y = (int) (fabs(this->MapImage->OffsetVert) + visible_area.Height() / 2);
	double scale_pixel_x = pShowMapArea.Width() / this->MapImage->Bitmap->Width;
	double scale_pixel_y = pShowMapArea.Height() / this->MapImage->Bitmap->Height;
	double pos_x = scale_pixel_x * (double) center_x;
	double pos_y = scale_pixel_y * (double) center_y;

	return DoublePoint(pShowMapArea.Left + pos_x, pShowMapArea.Top - pos_y);
}
//---------------------------------------------------------------------------

void __fastcall TMapForm::PinClick(System::TObject* Sender, int X, int Y)
{
	try
	{
		if(Sender && ((TGraphicObject*)Sender)->Pressed)
		{
			if(RadarMapManager && RadarMapManager->MapObjects && RadarMapManager->MapObjects->Container &&
				RadarMapManager->MapObjects->Container->Count>0)
			{
				TMapsContainer *mc=NULL;
				TGPSCoordinate *c;
				int i=0;

				try
				{
					do
					{
						mc=RadarMapManager->MapObjects->Container->GetObject(i);
						i++;
					} while(!mc);// && mc->MapType==mtEmpty);
					if(mc && RadarMapManager->TrackingPath)// && mc->MapType!=mtEmpty)
					{
						/*TPinLabel *ml;
						ml=new TMapLabel(RadarMapManager->MapObjects, RadarMapManager->MapObjects->Image->GetViewportRect(),
							RadarMapManager->MapObjects->Container->LatLonRect, NULL, RadarMapManager);
						ml->NormalGlyphName="PNGIMAGE_156";
						if(ml->NormalGlyph) ml->Center=Types::TPoint(16, 28);//Types::TPoint(ml->NormalGlyph->Width>>1,  ml->NormalGlyph->Height);
						else ml->Center=Types::TPoint(0, 0);
						ml->AllwaysHot=true;
						ml->Icons=true;
						ml->Coordinate=new TGPSCoordinate(RadarMapManager->TrackingPath->LastPoint);
						ml->GPSConfidence=ml->Coordinate->Confidence;
						if(RadarMapManager->Settings && RadarMapManager->Settings->ShowLabelInformationOnPin)
							ml->MouseUp(Sender, mbRight, TShiftState(ssRight), 0, 0, NULL);
						ml->Color=clRed32;*/
						new TPinLabel(RadarMapManager->MapObjects, RadarMapManager->MapObjects->Image->GetViewportRect(),
							RadarMapManager->MapObjects->Container->LatLonRect, true, RadarMapManager);
					}
				}
				catch(Exception &e) {}
			}
		}
	}
	catch(Exception &e) {}
}
//---------------------------------------------------------------------------

void __fastcall TMapForm::StartNGoClick(System::TObject* Sender, int X, int Y)
{
	if(StartNGoBtn->Pressed)
	{
		StartNGoBtn->Down=!StartNGoBtn->Down;
		if(StartNGoBtn->Down) MA=tmaStartNGo;
		else DisableMouseAction(); //MA=tmaHand;
	}
}
//---------------------------------------------------------------------------

void __fastcall TMapForm::StartNGoGPSClick(System::TObject* Sender, int X, int Y)
{
	if(StartNGoGPSBtn->Pressed)
	{
		if(RadarMapManager && RadarMapManager->MapObjects && RadarMapManager->MapObjects->Container &&
				RadarMapManager->MapObjects->Container->Count>0)
		{
			//sgl=
			new TStartNGoGPSLabel(RadarMapManager->MapObjects, RadarMapManager->MapObjects->Image->GetViewportRect(),
				RadarMapManager->MapObjects->Container->LatLonRect, RadarMapManager->MapObjects->Container,
				Types::TPoint(RadarMapManager->MapObjects->Image->Width>>1, RadarMapManager->MapObjects->Image->Height>>1),
				RadarMapManager->Settings->ReferencePointCoordinatesQ, (TObject*)RadarMapManager);
		}

		StartNGoBtn->Down=false;
	}
}
//---------------------------------------------------------------------------

void __fastcall TMapForm::PredictionBtnClick(System::TObject* Sender, int X, int Y)
{
	if(PredictionBtn->Pressed)
	{
		TMapsContainer *mc = RadarMapManager->MapObjects->Container->GetObject(0);
		if(mc)
		{
			if(mc->MapType==mtSQLiteDXF)
			{
				PredictionBtn->Down=!PredictionBtn->Down;
				if(PredictionBtn->Down) MA=tmaPrediction;
				else DisableMouseAction(); //MA=tmaHand;
			}
			else
			{
				RadarMapManager->ShowMessageObject(NULL, RadarMapManager->MapMessages,
					TMessageType::mtWarning, mvtDisapering, 5000,
					"This option is working on MultiFile maps only!", (AnsiString)_RadarMapName);
			}
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TMapForm::LabelsChildToolbarClick(System::TObject* Sender, int X, int Y)
{
	if(!LabelsChildToolbar->Pressed)
	{
		if(MA==tmaStartNGo) DisableMouseAction(); //MA=tmaHand;
	}
}
//---------------------------------------------------------------------------

void __fastcall TMapForm::PinLineBtnClick(System::TObject* Sender, int X, int Y)
{
	if(PinLineBtn->Pressed)
	{
		PinLineBtn->Down=!PinLineBtn->Down;
		if(PinLineBtn->Down) MA=tmaMapLabelPolyLineGraphics;
		else DisableMouseAction(); //MA=tmaHand;
	}
}
