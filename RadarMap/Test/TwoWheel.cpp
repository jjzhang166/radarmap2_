//---------------------------------------------------------------------------


#pragma hdrstop

#include "TwoWheel.h"
#include "Manager.h"
//---------------------------------------------------------------------------

#pragma package(smart_init)

//---------------------------------------------------------------------------
// TNmeaTWPOS
//---------------------------------------------------------------------------
AnsiString __fastcall TNmeaTWPOS::AsString(bool Full)
{
	AnsiString str;

	str="";
	if(!Corrupted)
	{
		if(Full) str+="UTC="+TimeToStr(Tm)+", ";
		str+="X="+FloatToStrF(X, ffFixed, 10, 2)+" m, ";
		str+="Y="+FloatToStrF(Y, ffFixed, 10, 2)+" m, ";
		str+="Z="+FloatToStrF(Z, ffFixed, 10, 2)+" m, ";
		str+="D="+FloatToStrF(D, ffFixed, 10, 2)+" m, ";
		str+="S="+FloatToStrF(S, ffFixed, 10, 2)+" mps";
	}
	return str;
}

//---------------------------------------------------------------------------
// TNmeaTWHPR
//---------------------------------------------------------------------------
AnsiString __fastcall TNmeaTWHPR::AsString(bool Full)
{
	AnsiString str;

	str="";
	if(!Corrupted)
	{
		str+="Heading="+FloatToStrF(Heading, ffFixed, 5, 2)+"�, ";
		str+="Pitch="+FloatToStrF(Pitch, ffFixed, 5, 2)+"�, ";
		str+="Roll="+FloatToStrF(Roll, ffFixed, 5, 2)+"�";
	}
	return str;
}

//---------------------------------------------------------------------------
// TTwoWheelNmeaClient
//---------------------------------------------------------------------------
__fastcall TTwoWheelNmeaClient::TTwoWheelNmeaClient(TObject* AManager,
	TNmeaReceiverSettings *ANmeaSettings) : TNmeaClient(AManager, ANmeaSettings)
{
	azimuth=0.;
	useazimuth=false;
	startingpoint=NULL;
	LastTickCount=0;
	started=false;
	LastSuccessfulParsingTick=0;
	initiator=NULL;
}
//---------------------------------------------------------------------------

__fastcall TTwoWheelNmeaClient::~TTwoWheelNmeaClient()
{
	StopItselfByInitiator();
}
//---------------------------------------------------------------------------

void __fastcall TTwoWheelNmeaClient::StopItselfByInitiator()
{
	try
	{
		if(Started && initiator && (initiator->ProfileObjectType==potStartNGoLabel || Initiator->ProfileObjectType==potStartNGoGPSLabel))
		{
			if(StopPositioning(NULL, initiator))
				((TStartNGoLabel*)Initiator)->State=sgnStopped;
		}
	}
	catch(...) {}
}
//---------------------------------------------------------------------------

void __fastcall TTwoWheelNmeaClient::Disconnect()
{
	StopItselfByInitiator();
	TNmeaClient::Disconnect();
}
//---------------------------------------------------------------------------

void __fastcall TTwoWheelNmeaClient::Stop()
{
	StopItselfByInitiator();
	TNmeaClient::Stop();
}
//---------------------------------------------------------------------------

void __fastcall TTwoWheelNmeaClient::SetStartingPoint(TGPSCoordinate *sp, bool ua, double a)
{
	if(startingpoint)
	{
		delete startingpoint;
		startingpoint=NULL;
	}
	if(sp) startingpoint=new TGPSCoordinate(sp);
	else startingpoint=NULL;
	useyawsensor=false;
	useazimuth=ua;
	azimuth=a;
}
//---------------------------------------------------------------------------

void __fastcall TTwoWheelNmeaClient::Initialize()
{
	TNmeaClient::Initialize();
	if(nmeasummary)
	{
		nmeasummary->Set(nstTWPOS);
		nmeasummary->Set(nstTWHPR);
	}
}
//---------------------------------------------------------------------------

bool __fastcall TTwoWheelNmeaClient::StartAcquisition()
{
	TGpsListenerCommand *cmd;
	AnsiString str, str2;
	TCoordinateSystem cs;
	TDoublePoint uv;
	bool res=false;

	if(!NmeaSettings->UseGpsForStart || (startingpoint && startingpoint->Valid))
	{
		cs=((TRadarMapManager*)Manager)->Settings->DefaultCoordinateSystem;
		if(startingpoint->GetCSDimension(cs)==csdDegrees) cs=csLocalMetric();
		if(cs==csUTM) cs=csMetric;
		ClearCommandsSequence(false);
		str="$start";
		if(NmeaSettings->UseGpsForStart && startingpoint && startingpoint->Valid)
		{
			uv=startingpoint->UnitVector(cs);
			str+=","+FloatToStrF(startingpoint->GetX(cs), ffFixed, 12, 2);
			str+=","+FloatToStrF(startingpoint->GetY(cs), ffFixed, 12, 2);
			str+=","+FloatToStrF(startingpoint->GetH(cs), ffFixed, 12, 2);
		}
		else
		{
			uv=DoublePoint(1, 1);
			str+=",0,0,0";
		}
		if(UseAzimuth) str+=","+FloatToStrF(StartingAzimuth, ffFixed, 7, 3);
		else if(UseYawSensor) str+=",yaw";
		else str+=",0";//yaw
		str2="$correctc,"+FloatToStrF(uv.X, ffFixed, 12, 2)+","+FloatToStrF(uv.Y, ffFixed, 12, 2);
		cmd=new TGpsListenerCommand(str2, NULL);
		cmd->ParserProc=NULL;
		cmd->AddReply("$correctc,OK", rrACK, false);
		cmd->AddReply("$invalid", rrNAK, false); //");
		cmd->RepliesSequence=false;
		AddCommandWithReplyToSequence(cmd);
		//AddSingleCommandWithReply(cmd);
		cmd=new TGpsListenerCommand(str, NULL);
		cmd->ParserProc=NULL;
		cmd->AddReply("$start,OK", rrACK, false); //");
		cmd->AddReply("$invalid", rrNAK, false);
		cmd->RepliesSequence=false;
		StopPositioning(NULL);
		//AddSingleCommandWithReply(cmd);
		AddCommandWithReplyToSequence(cmd);
		res=StartCommandsSequence();
	}
	return res;
}
//---------------------------------------------------------------------------

bool __fastcall TTwoWheelNmeaClient::StopPositioning(TGpsListenerReplyParser parserproc,
	TCustomProfileLabel *aInitiator)
{
	TGpsListenerCommand *cmd;
	bool res=false;

	if(aInitiator && aInitiator==initiator) initiator=NULL;
    ClearCommandsSequence(false);//true);
	cmd=new TGpsListenerCommand("$stop", NULL);  //Hangs on Toshiba - ???
	cmd->ParserProc=parserproc;
	cmd->AddReply("$stop", rrACK, false);
	cmd->AddReply("$invalid", rrNAK, false);
	cmd->RepliesSequence=false;
	AddCommandWithReplyToSequence(cmd);
	res=StartCommandsSequence();

	return res;
}
//---------------------------------------------------------------------------

bool __fastcall TTwoWheelNmeaClient::GetParams(TGpsListenerReplyParser parserproc)
{
	bool res=false;
	TGpsListenerCommand *cmd;

	ClearCommandsSequence(false);//true);
	cmd=new TGpsListenerCommand("$nmeaout,shutup", NULL);  //Hangs on Toshiba - ???
	//cmd->ParserProc=parserproc;
	cmd->AddReply("$nmeaout,OK", rrACK, false);
	cmd->AddReply("$invalid", rrNAK, false);
	cmd->RepliesSequence=false;
	AddCommandWithReplyToSequence(cmd);
	cmd=new TGpsListenerCommand("$stop", NULL);  //Hangs on Toshiba - ???
	//cmd->ParserProc=parserproc;
	cmd->AddReply("$stop", rrACK, false);
	cmd->AddReply("$invalid", rrNAK, false);
	cmd->RepliesSequence=false;
	AddCommandWithReplyToSequence(cmd);
	cmd=new TGpsListenerCommand("$calibwhe,off", NULL);  //Hangs on Toshiba - ???
	//cmd->ParserProc=parserproc;
	cmd->AddReply("$calibwhe,OK", rrACK, false);
	cmd->AddReply("$invalid", rrNAK, false);
	cmd->RepliesSequence=false;
	AddCommandWithReplyToSequence(cmd);
	cmd=new TGpsListenerCommand("$nmeaout,pos,on", NULL);  //Hangs on Toshiba - ???
	//cmd->ParserProc=parserproc;
	cmd->AddReply("$nmeaout,OK", rrACK, false);
	cmd->AddReply("$invalid", rrNAK, false);
	cmd->RepliesSequence=false;
	AddCommandWithReplyToSequence(cmd);
	cmd=new TGpsListenerCommand("$lwheel,get", NULL);
	cmd->ParserProc=parserproc;
	cmd->AddReply("$lwheel", rrACK, false);
	cmd->AddReply("$invalid", rrNAK, false);
	cmd->RepliesSequence=false;
	AddCommandWithReplyToSequence(cmd);
	cmd=new TGpsListenerCommand("$rwheel,get", NULL);
	cmd->AddReply("$rwheel", rrACK, false);
	cmd->AddReply("$invalid", rrNAK, false);
	cmd->ParserProc=parserproc;
	cmd->RepliesSequence=false;
	AddCommandWithReplyToSequence(cmd);
	cmd=new TGpsListenerCommand("$wbase,get", NULL);
	cmd->AddReply("$wbase", rrACK, false);
	cmd->AddReply("$invalid", rrNAK, false);
	cmd->ParserProc=parserproc;
	cmd->RepliesSequence=false;
	AddCommandWithReplyToSequence(cmd);
	cmd=new TGpsListenerCommand("$centerxy,get", NULL);
	cmd->AddReply("$centerxy", rrACK, false);
	cmd->AddReply("$invalid", rrNAK, false);
	cmd->ParserProc=parserproc;
	cmd->RepliesSequence=false;
	AddCommandWithReplyToSequence(cmd);
	cmd=new TGpsListenerCommand("$odometer", NULL);
	cmd->AddReply("$odometer", rrACK, false);
	cmd->AddReply("$invalid", rrNAK, false);
	cmd->ParserProc=parserproc;
	cmd->RepliesSequence=false;
	AddCommandWithReplyToSequence(cmd);
	cmd=new TGpsListenerCommand("$calibwhe,need", NULL);
	cmd->AddReply("$calibwhe", rrACK, false);
	cmd->AddReply("$invalid", rrNAK, false);
	cmd->ParserProc=parserproc;
	cmd->RepliesSequence=false;
	AddCommandWithReplyToSequence(cmd);
	cmd=new TGpsListenerCommand("$about", NULL);
	cmd->AddReply("$about", rrACK, false);
	cmd->AddReply("$invalid", rrNAK, false);
	cmd->ParserProc=parserproc;
	cmd->RepliesSequence=false;
	AddCommandWithReplyToSequence(cmd);
	res=StartCommandsSequence();

	return res;
}
//---------------------------------------------------------------------------

bool __fastcall TTwoWheelNmeaClient::StartPositioning(TGpsListenerReplyParser parserproc,
	double startX, double startY, double startH, double startAzimuth, double startUVX, double startUVY,
	TCustomProfileLabel *aInitiator)
{
	TGpsListenerCommand *cmd;
	AnsiString str;
	bool res=false;

	try
	{
		if(aInitiator && Initiator && Initiator!=aInitiator &&
			(Initiator->ProfileObjectType==potStartNGoLabel || Initiator->ProfileObjectType==potStartNGoGPSLabel))
		{
			((TStartNGoLabel*)Initiator)->State=sgnStopped;
		}
	}
	catch(...) {initiator=NULL;}
	initiator=aInitiator;
	ClearCommandsSequence(false);
	cmd=new TGpsListenerCommand("$stop", NULL);
	//cmd->ParserProc=parserproc;
	cmd->AddReply("$stop", rrACK, false);
	cmd->AddReply("$invalid", rrNAK, false);
	cmd->RepliesSequence=false;
	AddCommandWithReplyToSequence(cmd);
	cmd=new TGpsListenerCommand("$nmeaout,pos,on", NULL);  //Hangs on Toshiba - ???
	//cmd->ParserProc=parserproc;
	cmd->AddReply("$nmeaout,OK", rrACK, false);
	cmd->AddReply("$invalid", rrNAK, false);
	cmd->RepliesSequence=false;
	AddCommandWithReplyToSequence(cmd);
	cmd=new TGpsListenerCommand("$nmeaout,frq,5", NULL);
	//cmd->ParserProc=parserproc;
	cmd->AddReply("$nmeaout,OK", rrACK, false);
	cmd->AddReply("$invalid", rrNAK, false);
	cmd->RepliesSequence=false;
	AddCommandWithReplyToSequence(cmd);
	if(startUVX==0) startUVX=1.;
    if(startUVY==0) startUVY=1.;
	str="$correctc,"+FloatToStrF(startUVX, ffFixed, 12, 2)+","+FloatToStrF(startUVY, ffFixed, 12, 2);
	cmd=new TGpsListenerCommand(str, NULL);
	cmd->ParserProc=NULL;
	cmd->AddReply("$correctc,OK", rrACK, false);
	cmd->AddReply("$invalid", rrNAK, false); //");
	cmd->RepliesSequence=false;
	AddCommandWithReplyToSequence(cmd);
	str="$start,"+FloatToStrF(startX, ffFixed, 10, 3)+","+FloatToStrF(startY, ffFixed, 10, 3)+","+
		FloatToStrF(startH, ffFixed, 10, 3)+","+FloatToStrF(startAzimuth, ffFixed, 10, 3);
	cmd=new TGpsListenerCommand(str, NULL);
	cmd->ParserProc=parserproc;
	cmd->AddReply("$start", rrACK, false);
	cmd->AddReply("$invalid", rrNAK, false);
	cmd->RepliesSequence=false;
	AddCommandWithReplyToSequence(cmd);
	res=StartCommandsSequence();

	return res;
}
//---------------------------------------------------------------------------

bool __fastcall TTwoWheelNmeaClient::CalibDistStart(TGpsListenerReplyParser parserproc)
{
	bool res=false;
	TGpsListenerCommand *cmd;

	ClearCommandsSequence(false);//true);
	cmd=new TGpsListenerCommand("$calibwhe,off", NULL);
	cmd->AddReply("$calibwhe,OK", rrACK, false);
	cmd->AddReply("$invalid", rrNAK, false);
	cmd->ParserProc=parserproc;
	cmd->RepliesSequence=false;
	AddCommandWithReplyToSequence(cmd);
	cmd=new TGpsListenerCommand("$calibwhe,dist,on", NULL);
	cmd->AddReply("$calibwhe,OK", rrACK, false);
	cmd->AddReply("$invalid", rrNAK, false);
	cmd->ParserProc=parserproc;
	cmd->RepliesSequence=false;
	AddCommandWithReplyToSequence(cmd);
	res=StartCommandsSequence();

	return res;
}
//---------------------------------------------------------------------------

bool __fastcall TTwoWheelNmeaClient::CalibDistStop(TGpsListenerReplyParser parserproc)
{
	TGpsListenerCommand *cmd;
	bool res=false;

	if(params.CalibDistValue!=0)
	{
		ClearCommandsSequence(false);//true);
		cmd=new TGpsListenerCommand("$calibwhe,dist,off,"+FloatToStrF(params.CalibDistValue, ffFixed, 10, 4), NULL);
		cmd->AddReply("$calibwhe,OK", rrACK, false);
		cmd->AddReply("$invalid", rrNAK, false);
		cmd->ParserProc=parserproc;
		cmd->RepliesSequence=false;
		AddCommandWithReplyToSequence(cmd);
		res=StartCommandsSequence();
	}
	return res;
}
//---------------------------------------------------------------------------

bool __fastcall TTwoWheelNmeaClient::CalibAngleStart(TGpsListenerReplyParser parserproc)
{
	TGpsListenerCommand *cmd;
	bool res=false;

	ClearCommandsSequence(false);//true);
	cmd=new TGpsListenerCommand("$calibwhe,off", NULL);
	cmd->AddReply("$calibwhe,OK", rrACK, false);
	cmd->AddReply("$invalid", rrNAK, false);
	cmd->ParserProc=parserproc;
	cmd->RepliesSequence=false;
	AddCommandWithReplyToSequence(cmd);
	cmd=new TGpsListenerCommand("$calibwhe,angle,on", NULL);
	cmd->AddReply("$calibwhe,OK", rrACK, false);
	cmd->AddReply("$invalid", rrNAK, false);
	cmd->ParserProc=parserproc;
	cmd->RepliesSequence=false;
	AddCommandWithReplyToSequence(cmd);
	res=StartCommandsSequence();

	return res;
}
//---------------------------------------------------------------------------

bool __fastcall TTwoWheelNmeaClient::CalibAngleStop(TGpsListenerReplyParser parserproc)
{
	TGpsListenerCommand *cmd;
	bool res=false;

	if(params.CalibAngleValue!=0)
	{
		ClearCommandsSequence(false);//true);
		cmd=new TGpsListenerCommand("$calibwhe,angle,off,"+IntToStr(params.CalibAngleValue), NULL);
		cmd->AddReply("$calibwhe,OK", rrACK, false);
		cmd->AddReply("$invalid", rrNAK, false);
		cmd->ParserProc=parserproc;
		cmd->RepliesSequence=false;
		AddCommandWithReplyToSequence(cmd);
		res=StartCommandsSequence();
	}
	return res;
}
//---------------------------------------------------------------------------

bool __fastcall TTwoWheelNmeaClient::CancelCalibration(TGpsListenerReplyParser parserproc)
{
	TGpsListenerCommand *cmd;
	bool res=false;

	ClearCommandsSequence(false);//true);
	cmd=new TGpsListenerCommand("$calibwhe,off", NULL);
	cmd->AddReply("$calibwhe,OK", rrACK, false);
	cmd->AddReply("$invalid", rrNAK, false);
	cmd->ParserProc=parserproc;
	cmd->RepliesSequence=false;
	AddCommandWithReplyToSequence(cmd);
	res=StartCommandsSequence();

	return res;
}
//---------------------------------------------------------------------------

bool __fastcall TTwoWheelNmeaClient::SetFullParams(TGpsListenerReplyParser parserproc)
{
	TGpsListenerCommand *cmd;
	AnsiString str;
	bool res=false;

	ClearCommandsSequence(false);//true);
	str="$lwheel,"+FloatToStrF(params.LeftWheelD, ffFixed, 10, 4)+","+
		IntToStr(params.LeftWheelPPR)+",";
	if(params.LeftWheelPP) str+="push";
	else str+="pull";
	cmd=new TGpsListenerCommand(str, NULL);
	cmd->ParserProc=parserproc;
	cmd->AddReply("$lwheel,OK", rrACK, false);
	cmd->AddReply("$invalid", rrNAK, false);
	cmd->RepliesSequence=false;
	AddCommandWithReplyToSequence(cmd);
	str="$rwheel,"+FloatToStrF(params.RightWheelD, ffFixed, 10, 4)+","+
		IntToStr(params.RightWheelPPR)+",";
	if(params.RightWheelPP) str+="push";
	else str+="pull";
	cmd=new TGpsListenerCommand(str, NULL);
	cmd->ParserProc=parserproc;
	cmd->AddReply("$rwheel,OK", rrACK, false);
	cmd->AddReply("$invalid", rrNAK, false);
	cmd->RepliesSequence=false;
	AddCommandWithReplyToSequence(cmd);
	cmd=new TGpsListenerCommand("$wbase,"+FloatToStrF(params.WBase, ffFixed, 10, 4), NULL);
	cmd->AddReply("$wbase,OK", rrACK, false);
	cmd->AddReply("$invalid", rrNAK, false);
	cmd->ParserProc=parserproc;
	cmd->RepliesSequence=false;
	AddCommandWithReplyToSequence(cmd);
	str="$centerxy,"+FloatToStrF(params.CenterX, ffFixed, 10, 4)+","+
		FloatToStrF(params.CenterY, ffFixed, 10, 4);
	cmd=new TGpsListenerCommand(str, NULL);
	cmd->AddReply("$centerxy,OK", rrACK, false);
	cmd->AddReply("$invalid", rrNAK, false);
	cmd->ParserProc=parserproc;
	cmd->RepliesSequence=false;
	AddCommandWithReplyToSequence(cmd);
	res=StartCommandsSequence();

	return res;
}
//---------------------------------------------------------------------------

bool __fastcall TTwoWheelNmeaClient::SetParams(TGpsListenerReplyParser parserproc)
{
	TGpsListenerCommand *cmd;
	AnsiString str;
	bool res=false;

	ClearCommandsSequence(false);//true);
	str="$lwheel,ppr,"+IntToStr(params.LeftWheelPPR);
	cmd=new TGpsListenerCommand(str, NULL);
	cmd->ParserProc=parserproc;
	cmd->AddReply("$lwheel,OK", rrACK, false);
	cmd->AddReply("$invalid", rrNAK, false);
	cmd->RepliesSequence=false;
	AddCommandWithReplyToSequence(cmd);
	str="$rwheel,ppr,"+IntToStr(params.RightWheelPPR);
	cmd=new TGpsListenerCommand(str, NULL);
	cmd->ParserProc=parserproc;
	cmd->AddReply("$rwheel,OK", rrACK, false);
	cmd->AddReply("$invalid", rrNAK, false);
	cmd->RepliesSequence=false;
	AddCommandWithReplyToSequence(cmd);
	str="$centerxy,"+FloatToStrF(params.CenterX, ffFixed, 10, 4)+","+
		FloatToStrF(params.CenterY, ffFixed, 10, 4);
	cmd=new TGpsListenerCommand(str, NULL);
	cmd->AddReply("$centerxy,OK", rrACK, false);
	cmd->AddReply("$invalid", rrNAK, false);
	cmd->ParserProc=parserproc;
	cmd->RepliesSequence=false;
	AddCommandWithReplyToSequence(cmd);
	res=StartCommandsSequence();

	return res;
}
//---------------------------------------------------------------------------

bool __fastcall TTwoWheelNmeaClient::CalibrationStart(TTwoWheelCalibrationType calib)
{
	TGpsListenerCommand *cmd;
	AnsiString CmdStr, ReStr, NegativeStr;
	bool res=false;

	if(currentcalib==twctNone && calib!=twctNone)
	{
		if(currentcalib==twctNone)
		{
			switch(calib)
			{
				case twctDistance:
					CmdStr = "$calibwhe,dist,on";
					ReStr = "$calibwhe,OK";
					break;
				case twctAngle:
					CmdStr = "$calibwhe,angle,on";
					ReStr = "$calibwhe,OK";
					break;
				case twctMotion:
					CmdStr = "$calibmpu,on";
					ReStr = "$calibmpu,OK";
					break;
				case twctNone:
					CmdStr = "";
					ReStr = "";
					break;
			}
			NegativeStr="$invalid";
		}
		currentcalib=calib;
		cmd=new TGpsListenerCommand(CmdStr, NULL);
		cmd->AddReply(ReStr);
		cmd->AddReply(NegativeStr, rrNAK);
		cmd->RepliesSequence=false;
		StopPositioning(NULL);
		res=AddSingleCommandWithReply(cmd);
		//res=true;
	}
	return res;
}
//---------------------------------------------------------------------------

bool __fastcall TTwoWheelNmeaClient::CalibrationStop(TTwoWheelCalibrationType calib, float value)
{
	TGpsListenerCommand *cmd;
	AnsiString CmdStr, ReStr, NegativeStr;
	bool res=false;

	switch(calib)
	{
		case twctNone:
			CmdStr = "$calibwhe,off";
			ReStr = "$calibwhe,OK";
			break;
		case twctDistance:
			CmdStr = "$calibwhe,dist,off,"+FloatToStrF(value, ffFixed, 10, 3);
			ReStr = "$calibwhe,OK";
			break;
		case twctAngle:
			CmdStr = "$calibwhe,angle,off,"+IntToStr((int)value);
			ReStr = "$calibwhe,OK";
			break;
		case twctMotion:
			CmdStr = "$calibmpu,off";
			ReStr = "$calibmpu,OK";
			break;
	}
	NegativeStr="$invalid";
	cmd=new TGpsListenerCommand(CmdStr, NULL);
	cmd->AddReply(ReStr);
	cmd->AddReply(NegativeStr, rrNAK);
	cmd->RepliesSequence=false;
	StopPositioning(NULL);
	res=AddSingleCommandWithReply(cmd);

	return res;
}
//---------------------------------------------------------------------------

void __fastcall TTwoWheelNmeaClient::CalibrationSet(char *Buf, int N)
{
	//$calibmpu,set,
}
//---------------------------------------------------------------------------

void __fastcall TTwoWheelNmeaClient::CalibrationGet(char *Buf, int &N)
{
	//$calibmpu,get,
}
//---------------------------------------------------------------------------

TNmeaSentenceType __fastcall TTwoWheelNmeaClient::NmeaParser(HANDLE ThreadIdleEvent)
{
	ULONGLONG dt, TickCount=::GetTickCount();//GetTickCount64();
	TNmeaSentenceType res;

	res=TNmeaClient::NmeaParser(ThreadIdleEvent);
	dt=TickCount-LastSuccessfulParsingTick;
	if(res>nstNone && res<nstUnknown)
	{
        started=true;
		LastSuccessfulParsingTick=TickCount;
	}
	else if(dt>1500 && !(LastSuccessfulParsingTick==0)) started=false;

	return res;
}
//---------------------------------------------------------------------------

TNmeaSentenceType __fastcall TTwoWheelNmeaClient::ParseNmeaSequence(AnsiString s1)
{
	TNmeaSentenceType res=nstNone;

	res=TNmeaClient::ParseNmeaSequence(s1);
	if(res==nstNone || res==nstUnknown)
	{
		if(s1.Pos("TWPOS,")>0)
		{
			res=nstTWPOS;
			ParseGPSDataTWPOS((TNmeaTWPOS*)NmeaSummary->Get(res), s1);
		}
		else if(s1.Pos("TWHPR,")>0)
		{
			res=nstTWHPR;
			ParseGPSDataTWHPR((TNmeaTWHPR*)NmeaSummary->Get(res), s1);
		}
		else res=nstUnknown;
		if(res!=nstUnknown)
		{
			NmeaSummary->Rebuild(res);
		}
	}
	return res;
}
//---------------------------------------------------------------------------

//$TWPOS,185828.152,1234.56,M,7890.12,M,34.56,M,78.9,M,3.456789,K*7C
void __fastcall TTwoWheelNmeaClient::ParseGPSDataTWPOS(TNmeaTWPOS *out, AnsiString str)
{
	int i, gi, zz;
	AnsiString s1, s2, Line;

	Line=str;
	i=Line.Pos("TWPOS,");
	if(out && i>0 && Line.Length()-i-6>0)
	{
		try
		{
			Line=Line.SubString(i+6, Line.Length()-i-3);
			out->Corrupted=false;
			out->EndFound=false;
			//UTC
			s2=GetNextNmeaParam(out->EndFound, &Line);
			if(s2!="" && s2.Length()>0)
			{
				gi=zz=1;
				s1="  .  .  .   ";
				while(gi<=s1.Length() && zz<=s2.Length() && ((s2[zz]>='0' && s2[zz]<='9') || s2[zz]=='.'))
				{
					if(s1[gi]=='.') gi++;
					s1[gi]=s2[zz];
					gi++;
					zz++;
					if(zz<=s2.Length() && s2[zz]=='.') zz++;
				}
				if(zz<7) out->Corrupted=true;
				else
				{
					try {out->Tm=MyStrToTime(s1, "hh.nn.ss.zzz", '.');}
					catch(Exception &exception) {out->Corrupted=true;}
				}
			}
			else out->Tm=0;
			//X
			if(!out->EndFound && !out->Corrupted)
			{
				s2=GetNextNmeaParam(out->EndFound, &Line);
				if(s2!="") try {out->X=StrToFloat(s2);} catch(Exception &exception) {out->Corrupted=true;}
				else out->Corrupted=true;
			}
			//unit of measure
			if(!out->EndFound && !out->Corrupted)
			{
				s2=GetNextNmeaParam(out->EndFound, &Line);
				if(s2=="F") out->X/=0.3048; //feets
				else if(s2=="N") out->X*=0.621371192237; //miles
				else if(s2=="K") out->X*=1000.; //kilometers
				else if(s2!="M") out->Corrupted=true; //meters
			}
			//Y
			if(!out->EndFound && !out->Corrupted)
			{
				s2=GetNextNmeaParam(out->EndFound, &Line);
				if(s2!="") try {out->Y=StrToFloat(s2);} catch (Exception &exception) {out->Corrupted=true;}
				else out->Corrupted=true;
			}
			//unit of measure
			if(!out->EndFound && !out->Corrupted)
			{
				s2=GetNextNmeaParam(out->EndFound, &Line);
				if(s2=="F") out->Y/=0.3048; //feets
				else if(s2=="N") out->Y/=0.621371192237; //miles
				else if(s2=="K") out->Y*=1000.; //kilometers
				else if(s2!="M") out->Corrupted=true; //meters
			}
			//Z
			if(!out->EndFound && !out->Corrupted)
			{
				s2=GetNextNmeaParam(out->EndFound, &Line);
				if(s2!="") try {out->Z=StrToFloat(s2);} catch (Exception &exception) {out->Corrupted=true;}
				else out->Corrupted=true;
			}
			//unit of measure
			if(!out->EndFound && !out->Corrupted)
			{
				s2=GetNextNmeaParam(out->EndFound, &Line);
				if(s2=="F") out->Z/=0.3048; //feets
				else if(s2=="N") out->Z/=0.621371192237; //miles
				else if(s2=="K") out->Y*=1000.; //kilometers
				else if(s2!="M") out->Corrupted=true; //meters
			}
			//D
			if(!out->EndFound && !out->Corrupted)
			{
				s2=GetNextNmeaParam(out->EndFound, &Line);
				if(s2!="") try {out->D=StrToFloat(s2);} catch (Exception &exception) {out->Corrupted=true;}
				else out->Corrupted=true;
			}
			//unit of measure
			if(!out->EndFound && !out->Corrupted)
			{
				s2=GetNextNmeaParam(out->EndFound, &Line);
				if(s2=="F") out->D/=0.3048; //feets
				else if(s2=="N") out->D/=0.621371192237; //miles
				else if(s2=="K") out->D*=1000.; //kilometers
				else if(s2!="M") out->Corrupted=true; //meters
			}
			//Speed
			if(!out->EndFound && !out->Corrupted)
			{
				s2=GetNextNmeaParam(out->EndFound, &Line);
				if(s2!="") try {out->S=StrToFloat(s2);} catch (Exception &exception) {out->Corrupted=true;}
				else out->Corrupted=true;
			}
			//unit of measure
			if(!out->EndFound && !out->Corrupted)
			{
				s2=GetNextNmeaParam(out->EndFound, &Line);
				if(s2=="N") out->S*=1.94384449244; //knots
				else if(s2=="K") out->S/=3.6; //kilometers per hour
				else if(s2!="M") out->Corrupted=true; //meters per second
			}
			out->Refreshed=true;
		}
		catch(Exception &e) {try {if(out) out->Corrupted=true;} catch(...) {}}
	}
}
//---------------------------------------------------------------------------

//$PTWHPR,161540.45,12.456,78.901,2.34,79.912,0.12*2C
void __fastcall TTwoWheelNmeaClient::ParseGPSDataTWHPR(TNmeaTWHPR *out, AnsiString str)
{
	int i, gi, zz;
	AnsiString s1, s2, Line;

	Line=str;
	i=Line.Pos("HPR,");
	if(out && i>0 && Line.Length()-i-4>0)
	{
		try
		{
			Line=Line.SubString(i+4, Line.Length()-i-3);
			out->Corrupted=false;
			out->EndFound=false;
			//Heading
			s2=GetNextNmeaParam(out->EndFound, &Line);
			if(s2!="") try {out->Heading=StrToFloat(s2);} catch (Exception &exception) {out->Corrupted=true;}
			else out->Corrupted=true;
			//Pitch
			if(!out->EndFound && !out->Corrupted)
			{
				s2=GetNextNmeaParam(out->EndFound, &Line);
				if(s2!="") try {out->Pitch=StrToFloat(s2);} catch(Exception &exception) {out->Corrupted=true;}
				else out->Corrupted=true;
			}
			//Roll
			if(!out->EndFound && !out->Corrupted)
			{
				s2=GetNextNmeaParam(out->EndFound, &Line);
				if(s2!="") try {out->Roll=StrToFloat(s2);} catch(Exception &exception) {out->Corrupted=true;}
				else out->Corrupted=true;
			}
			out->Refreshed=true;
		}
		catch(Exception &e) {try {if(out) out->Corrupted=true;} catch(...) {}}
	}
}
//---------------------------------------------------------------------------

double __fastcall TTwoWheelNmeaClient::GetPassedDistance(bool freshness)
{
	double result;
	ULONGLONG dt, TickCount=::GetTickCount();//GetTickCount64();

	dt=TickCount-LastTickCount;
	if(dt>0 && dt<3600000 && !(LastTickCount==0))
	{
		result=(double)NmeaSummary->Speed*((double)dt/1000.);
	}
	else result=0;
	LastTickCount=TickCount;

	return result;
}
//---------------------------------------------------------------------------

int TTwoWheelNmeaClient::ParseStrings(AnsiString InStr, TStrings *Out, char Separator)
{
	int res, i, n;
	AnsiString str;

	if(Out && InStr!=NULL)
	{
		Out->Clear();
		i=1;
		str="";
		while(i<=InStr.Length())
		{
			if(InStr[i]==Separator || InStr[i]=='\r' || InStr[i]=='\n')
			{
				Out->Add(str);
				str="";
			}
			else str+=InStr[i];
			i++;
		}
		if(str!="") Out->Add(str);
		res=Out->Count;
	}
	else res=-1;
	return res;
}
//---------------------------------------------------------------------------
