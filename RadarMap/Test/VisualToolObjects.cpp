//---------------------------------------------------------------------------

#pragma hdrstop

#include "VisualToolObjects.h"
#include "Manager.h"
#include <GR32_Filters.hpp>
#include "BitmapGDI.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

//---------------------------------------------------------------------------
// TVisualToolObject
//---------------------------------------------------------------------------
__fastcall TVisualToolObject::TVisualToolObject(TMyObject *AOwner, TImgView32 *ImgView,
	TObject *AManager, TSliderToolButton *AConnectedBtn) : TToolObject(AOwner, ImgView, Types::TRect(0,0,0,0), AManager, true)
{
	visible=true;
	ConnectedBtn=AConnectedBtn;
	showapplybtn=false;
	SmallOkBtn=new TSliderToolButton(this, Types::TRect(2, 2, 26, 26)); //AOwner
	SmallOkBtn->NormalGlyphName="PNGIMAGE_478";
	SmallOkBtn->HotGlyphName="PNGIMAGE_479";
	SmallOkBtn->Enabled=true;
	SmallOkBtn->Visible=(visible==true ? showapplybtn : false);
	SmallOkBtn->OnMouseClick=OKPressed;
	showclosebtn=true;
	SmallCloseBtn=new TSliderToolButton(this, Types::TRect(2, 2, 26, 26)); //AOwner
	SmallCloseBtn->NormalGlyphName="PNGIMAGE_398";//"PNGIMAGE_142";
	SmallCloseBtn->HotGlyphName="PNGIMAGE_399";
	SmallCloseBtn->Enabled=true;
	SmallCloseBtn->Visible=(visible==true ? showclosebtn : false);
	SmallCloseBtn->OnMouseClick=ClosePressed;
	fixedposition=false;
	easyrendering=false;
	CloseExternalSetting=NULL;
	ToolAlignExternalSetting=NULL;
	width=0;
	height=0;
	BorderWidth=60;
	toolalign=vtoaNone;
	alignwithneighbors=true;
	rebuilded=false;
	if(Owner && Owner->ObjectType==gotContainer)
	{
		SurroundingTools=((TGraphicObjectsContainer*)Owner)->GetToolsObjectsList();
	}
	else SurroundingTools=NULL;
	AutomaticUpdate=false;
	//SmallResize();
	AutomaticUpdate=true;
}
//---------------------------------------------------------------------------

void __fastcall TVisualToolObject::writeVisible(bool value)
{
	try
	{
		if(visible!=value)
		{
			SmallOkBtn->Visible=(value==true ? showapplybtn : false);
			SmallCloseBtn->Visible=(value==true ? showclosebtn : false);
		}
		TToolObject::writeVisible(value);
	}
	catch(...)
	{
		value=false;
	}
}
//---------------------------------------------------------------------------

void __fastcall TVisualToolObject::RebuildInternalRects(bool RebuildNeighbors)
{
	Types::TRect vpt;

	if(image)
	{
		vpt=image->GetViewportRect();
		RebuildInternalRects(vpt.Width(), vpt.Height(), RebuildNeighbors);
	}
}

void __fastcall TVisualToolObject::RebuildInternalRects(int NewWidth, int NewHeight, bool RebuildNeighbors)
{
	TToolObject *to;
	int mm, i;
	TStrings *strs;

	strs=new TStringList();
	try
	{
		if(image)
		{
			/*if(image->ScrollBars->Visibility==svAlways)
			{

			} */
			switch(toolalign)
			{
				case vtoaLeft: //Left Center
					ToolRect.left=BorderWidth;
					ToolRect.top=(NewHeight-height)>>1;
					break;
				case vtoaTop: //Top Center
					ToolRect.left=(NewWidth-width)>>1;
					ToolRect.top=(int)(1.5*(float)BorderWidth);
					break;
				case vtoaRight://Right Center
					ToolRect.left=NewWidth-width-BorderWidth;
					ToolRect.top=(NewHeight-height)>>1;
					break;
				case vtoaBottom: //Bottom Center
					ToolRect.left=(NewWidth-width)>>1;
					ToolRect.top=NewHeight-height-(int)(1.5*(float)BorderWidth);
					break;
				case vtoaLeftTop: //LeftTop Corner
					ToolRect.left=BorderWidth;
					ToolRect.top=(int)(1.5*(float)BorderWidth);
					break;
				case vtoaLeftBottom: //LeftBottom Corner
					ToolRect.left=BorderWidth;
					ToolRect.top=NewHeight-height-(int)(1.5*(float)BorderWidth);
					break;
				case vtoaRightTop: //RightTop Corner
					ToolRect.left=NewWidth-width-BorderWidth;
					ToolRect.top=(int)(1.5*(float)BorderWidth);
					break;
				case vtoaRightBottom: //RightTop Corner
					ToolRect.left=NewWidth-width-BorderWidth;
					ToolRect.top=NewHeight-height-(int)(1.5*(float)BorderWidth);
					break;
				case vtoaCenter: //Center
					ToolRect.left=(NewWidth-width)>>1;
					ToolRect.top=(NewHeight-height)>>1;
					break;
			}
			ToolRect.right=ToolRect.left+width;
			ToolRect.bottom=ToolRect.top+height;
			if(SurroundingTools && AlignWithNeighbors)// && (int)toolalign=>(int)vtoaLeft && toolalign<=vtoaRightBottom)
			{
				mm=0;

				//for(int i=0; i<SurroundingTools->Count; i++)
				i=0;
				while(i<SurroundingTools->Count)
				{
					if(strs->IndexOf(IntToStr(i))==-1)
					{
						if(SurroundingTools->Items[i] && SurroundingTools->Items[i]->ObjectType==gotTool)
						{
							to=(TToolObject*)SurroundingTools->Items[i];
							if(to->Visual && to!=this &&
								((TVisualToolObject*)to)->ToolAlign==toolalign && ((TVisualToolObject*)to)->Rebuilded &&
									((TVisualToolObject*)to)->ToolRect.Contains(ToolRect))
							{
								switch(toolalign)
								{
									case vtoaTop:
									case vtoaLeftTop:
									case vtoaRightTop:
										if(mm<((TVisualToolObject*)to)->ToolRect.Bottom) mm=((TVisualToolObject*)to)->ToolRect.Bottom;
										break;
									case vtoaBottom:
									case vtoaLeftBottom:
									case vtoaRightBottom:
										if(mm==0 || mm>((TVisualToolObject*)to)->ToolRect.Top) mm=((TVisualToolObject*)to)->ToolRect.Top;
										break;
									case vtoaLeft:
										if(mm<((TVisualToolObject*)to)->ToolRect.Right) mm=((TVisualToolObject*)to)->ToolRect.Right;
										break;
									case vtoaRight:
										if(mm==0 || mm>((TVisualToolObject*)to)->ToolRect.Left) mm=((TVisualToolObject*)to)->ToolRect.Left;
										break;
								}
							}
						}
						if(mm>0)
						{
							switch(toolalign)
							{
								case vtoaLeft: //Left Center
									ToolRect.left=mm+(int)(0.1*(float)BorderWidth);
									break;
								case vtoaRight://Right Center
									ToolRect.left=mm-width-(int)(0.1*(float)BorderWidth);
									break;
								case vtoaTop: //Top Center
								case vtoaLeftTop: //LeftTop Corner
								case vtoaRightTop: //RightTop Corner
									ToolRect.top=mm+(int)(0.1*(float)BorderWidth);
									break;
								case vtoaBottom: //Bottom Center
								case vtoaLeftBottom: //LeftBottom Corner
								case vtoaRightBottom: //RightTop Corner
									ToolRect.top=mm-height-(int)(0.1*(float)BorderWidth);
									break;
							}
							ToolRect.right=ToolRect.left+width;
							ToolRect.bottom=ToolRect.top+height;
							strs->Add(IntToStr(i));
							mm=0;
							i=0;
						}
					}
					i++;
				}
			
			}

			if(SmallCloseBtn)
			{
				SmallCloseBtn->Left=ToolRect.right-SmallCloseBtn->Width;
				if(ToolRect.top<0) SmallCloseBtn->Top=0;
				else SmallCloseBtn->Top=ToolRect.top;
			}
			if(SmallOkBtn)
			{
				SmallOkBtn->Left=SmallCloseBtn->Left-SmallOkBtn->Width-2;
				if(ToolRect.top<0) SmallOkBtn->Top=0;
				else SmallOkBtn->Top=ToolRect.top;
			}
			if(ToolRect.left<BorderWidth || ToolRect.top<BorderWidth ||
				ToolRect.right>(NewWidth-BorderWidth) || ToolRect.bottom>(NewHeight-BorderWidth))
					FitToImage=false;
			else FitToImage=true;
			Rect=ToolRect;
			if(RebuildNeighbors && SurroundingTools && !AlignWithNeighbors)// && toolalign=>vtoaLeft && toolalign<=vtoaRightBottom)
			{
				for(int i=0; i<SurroundingTools->Count; i++)
				{
					if(SurroundingTools->Items[i] && SurroundingTools->Items[i]->ObjectType==gotTool)
					{
						to=(TToolObject*)SurroundingTools->Items[i];
						if(to->Visual && to!=this && ((TVisualToolObject*)to)->Rebuilded)
						{
							((TVisualToolObject*)to)->RebuildInternalRects(false);
							to->Update();
						}
					}
				}
			}
			rebuilded=true;
		}
	}
	__finally
	{
		strs->Clear();
		delete strs;
	}
}
//---------------------------------------------------------------------------

void __fastcall TVisualToolObject::SmallResize()
{
	//if(smallsize) DecreaseGlyph(SmallCloseBtn);
	RebuildInternalRects();
	Update();
}
//---------------------------------------------------------------------------

void __fastcall TVisualToolObject::writeEasyRendering(bool value)
{
	if(easyrendering!=value)
	{
		easyrendering=value;
		SmallResize();
	}
}
//---------------------------------------------------------------------------

void __fastcall TVisualToolObject::Redraw(TObject *Sender, TBitmap32 *Bmp)
{
	/*if(Visible && FitToImage && Bmp && SmallCloseBtn && ShowCloseBtn)
	{
		Bmp->BeginUpdate();
		try
		{
			Bmp->Draw(CloseBtnRect, Types::TRect(0, 0, CloseBtnRect.Width(), CloseBtnRect.Height()),
				SmallCloseBtn);
		}
		__finally
		{
			Bmp->EndUpdate();
		}
	}*/
	if(Visible && FitToImage && Bmp)
	{
		if(showapplybtn && SmallOkBtn) SmallOkBtn->Redraw(Sender, Bmp);
		if(showclosebtn && SmallCloseBtn) SmallCloseBtn->Redraw(Sender, Bmp);
	}
}
//---------------------------------------------------------------------------

void __fastcall TVisualToolObject::MouseMove(System::TObject* Sender,
	Classes::TShiftState Shift, int X, int Y, TCustomLayer *Layer)
{
    Types::TRect vpt;

	if(pressed && image && !fixedposition)
	{
		vpt=image->GetViewportRect();
		if(toolalign==vtoaNone)
		{
			ToolRect.Left=X-dX;
			ToolRect.Top=Y-dY;
		}
		else
		{
			double w=vpt.Width(), h=vpt.Height();
			int xta;

			if(X<=(int)(w/3.)) xta=0;
			else if(X<=(int)(2.*w/3.)) xta=1;
			else xta=2;
			if(Y<=(int)(h/3.))
			{
				if(xta==0) toolalign=vtoaLeftTop;
				else if(xta==1) toolalign=vtoaTop;
				else toolalign=vtoaRightTop;
			}
			else if(Y<=(int)(2.*h/3.))
			{
				if(xta==0) toolalign=vtoaLeft;
				else if(xta==1) toolalign=vtoaCenter;
				else toolalign=vtoaRight;
			}
			else
			{
				if(xta==0) toolalign=vtoaLeftBottom;
				else if(xta==1) toolalign=vtoaBottom;
				else toolalign=vtoaRightBottom;
			}
		}
		if(toolalign==vtoaNone || dTA!=toolalign)
		{
			RebuildInternalRects(vpt.Width(), vpt.Height());
			Update();
			if(ToolAlignExternalSetting) (*ToolAlignExternalSetting)=toolalign;
			dTA=toolalign;
		}
	}
}

//---------------------------------------------------------------------------
// TRecycleBin
//---------------------------------------------------------------------------
__fastcall TRecycleBin::TRecycleBin(TMyObject *AOwner, TImgView32 *ImgView, TObject *AManager)
	: TVisualToolObject(AOwner, ImgView, AManager)
{
	bool v;

	NormalGlyph=HotGlyph=InUseGlyph=Glyph=NULL;
	//fixedposition=true;
	dockable=true;
	//width=Glyph->Width;
	//height=Glyph->Height;
	if(Manager && ((TRadarMapManager*)Manager)->Settings)
	{
		CloseExternalSetting=&(((TRadarMapManager*)Manager)->Settings->ShowRecycleBin);
		v=((TRadarMapManager*)Manager)->Settings->ShowRecycleBin;
		ToolAlignExternalSetting=&(((TRadarMapManager*)Manager)->Settings->RecycleBinTAlign);
		toolalign=((TRadarMapManager*)Manager)->Settings->RecycleBinTAlign;
	}
	else
	{
		toolalign=vtoaRightBottom;
		v=visible;
	}
	AutomaticUpdate=false;
	SmallResize();
	AutomaticUpdate=true;
	RebuildInternalRects();
	Visible=v;
}
//---------------------------------------------------------------------------

__fastcall TRecycleBin::~TRecycleBin()
{
	if(NormalGlyph) delete NormalGlyph;
	if(HotGlyph) delete HotGlyph;
	if(InUseGlyph) delete InUseGlyph;
}
//---------------------------------------------------------------------------

void __fastcall TRecycleBin::SmallResize()
{
	if(NormalGlyph) delete NormalGlyph;
	NormalGlyph=LoadPNG32Resource((System::UnicodeString)"PNGIMAGE_397");//"PNGIMAGE_145");//
	if(HotGlyph) delete HotGlyph;
	HotGlyph=LoadPNG32Resource((System::UnicodeString)"PNGIMAGE_395");//"PNGIMAGE_143");//
	if(InUseGlyph) delete InUseGlyph;
	InUseGlyph=LoadPNG32Resource((System::UnicodeString)"PNGIMAGE_396");//"PNGIMAGE_144");//
	if(smallsize)
	{
		DecreaseGlyph(NormalGlyph);
		DecreaseGlyph(HotGlyph);
		DecreaseGlyph(InUseGlyph);
	}
	Glyph=NormalGlyph;
	width=Glyph->Width;
	height=Glyph->Height;
	TVisualToolObject::SmallResize();
}
//---------------------------------------------------------------------------

void __fastcall TRecycleBin::writeInRect(bool value)
{
	if(inrect!=value)
	{
		inrect=value;
		if(dockable) indragndrop=value;
		if(indragndrop && InUseGlyph) Glyph=InUseGlyph;
		else if(inrect && HotGlyph) Glyph=HotGlyph;
		else Glyph=NormalGlyph;
		Update();
	}
}
//---------------------------------------------------------------------------

void __fastcall TRecycleBin::DockDrop(System::TObject* Sender, System::TObject* Fallen, int X, int Y)
{
	if(OnDockDrop!=NULL) (OnDockDrop)((TObject*)this, Fallen, X, Y);
	if(Fallen)
	{
		try
		{
			if(((TProfileLabel*)Fallen)->ObjectType==gotCustom && !((TProfileLabel*)Fallen)->Locked)
			{
				int i=Application->MessageBox(L"Do you really want to delete this Label?",
					((UnicodeString)_RadarMapName).w_str(), MB_YESNO);
				if(i==IDYES)
				{
					/*if(((TProfileLabel*)Fallen)->ConnectedObject)
					{
						delete ((TProfileLabel*)Fallen)->ConnectedObject;
						//((TProfileLabel*)Fallen)->ConnectedObject=NULL;
					}*/
					delete Fallen;
					Fallen=NULL;
					DraggingObject=NULL;
				}
				else
				{
					((TProfileLabel*)Fallen)->RestoreOldPos();
				}
			}
		}
		catch(Exception &e) {}
	}
}
//---------------------------------------------------------------------------

void __fastcall TRecycleBin::Redraw(TObject *Sender, TBitmap32 *Bmp)
{
	if(Visible && FitToImage && Bmp)
	{
		Bmp->BeginUpdate();
		try
		{
			if(Glyph) Bmp->Draw(ToolRect, Types::TRect(0, 0, ToolRect.Width(), ToolRect.Height()),
				Glyph);
		}
		__finally
		{
			Bmp->EndUpdate();
		}
		try
		{
			if(indragndrop && DraggingObject && ((TProfileLabel*)DraggingObject)->ObjectType==gotCustom)
			{
				((TProfileLabel*)DraggingObject)->Redraw(Sender, Bmp);
			}
		}
		catch(Exception &e) {}
	}
	TVisualToolObject::Redraw(Sender, Bmp);
}

//---------------------------------------------------------------------------
// TGyrocompassObject
//---------------------------------------------------------------------------
__fastcall TGyroCompassObject::TGyroCompassObject(TMyObject *AOwner, TImgView32 *ImgView, TObject *AManager)
	: TVisualToolObject(AOwner, ImgView, AManager)
{
	bool v;

	automaticupdate=false;
	Clear();
	BackgroundBmp=CaseBmp=GradsBmp=CompassArrowBmp=GlassBmp=TempBmp=NULL;
	if(Manager && ((TRadarMapManager*)Manager)->Settings)
	{
		CloseExternalSetting=&(((TRadarMapManager*)Manager)->Settings->ShowGyroCompass);
		v=((TRadarMapManager*)Manager)->Settings->ShowGyroCompass;
		ToolAlignExternalSetting=&(((TRadarMapManager*)Manager)->Settings->GyroCompassTAlign);
		toolalign=((TRadarMapManager*)Manager)->Settings->GyroCompassTAlign;
	}
	else
	{
		toolalign=vtoaRightBottom;
		v=visible;
	}
	AutomaticUpdate=false;
	showglass=true;
	SmallResize();
	AutomaticUpdate=true;
	RebuildInternalRects();
	Visible=v;
}
//---------------------------------------------------------------------------

__fastcall TGyroCompassObject::~TGyroCompassObject()
{
	if(BackgroundBmp) delete BackgroundBmp;
	if(CaseBmp) delete CaseBmp;
	if(GradsBmp) delete GradsBmp;
	if(CompassArrowBmp) delete CompassArrowBmp;
	if(GlassBmp) delete GlassBmp;
	if(TempBmp) delete TempBmp;
}
//---------------------------------------------------------------------------

void __fastcall TGyroCompassObject::SmallResize()
{
	if(!easyrendering)
	{
		if(BackgroundBmp) delete BackgroundBmp;
		BackgroundBmp=LoadPNG32Resource((System::UnicodeString)"PNGIMAGE_138");
		if(CaseBmp) delete CaseBmp;
		CaseBmp=LoadPNG32Resource((System::UnicodeString)"PNGIMAGE_139");
		if(GradsBmp) delete GradsBmp;
		GradsBmp=LoadPNG32Resource((System::UnicodeString)"PNGIMAGE_146");
		if(CompassArrowBmp) delete CompassArrowBmp;
		CompassArrowBmp=LoadPNG32Resource((System::UnicodeString)"PNGIMAGE_140");
		if(GlassBmp) delete GlassBmp;
		GlassBmp=LoadPNG32Resource((System::UnicodeString)"PNGIMAGE_141");
		if(smallsize)
		{
			DecreaseGlyph(BackgroundBmp);
			DecreaseGlyph(CaseBmp);
			DecreaseGlyph(GradsBmp);
			DecreaseGlyph(CompassArrowBmp);
			DecreaseGlyph(GlassBmp);
		}
		if(TempBmp) delete TempBmp;
		TempBmp=new TBitmap32();
		TempBmp->DrawMode=dmBlend;
		if(CompassArrowBmp) TempBmp->SetSizeFrom(CompassArrowBmp);
		if(CaseBmp)
		{
			width=CaseBmp->Width;
			height=CaseBmp->Height;
		}
		else
		{
			easyrendering=true;
			width=60;
			width+=SmallCloseBtn->Width;
			height=200;
		}
	}
	else
	{
		width=60;
		width+=SmallCloseBtn->Width;
		height=200;
	}
	TVisualToolObject::SmallResize();
}
//---------------------------------------------------------------------------

void __fastcall TGyroCompassObject::Redraw(TObject *Sender, TBitmap32 *Bmp)
{
	double xmid, ymid, dd;
	int x1, y1, x2, y2;
	double h=heading;

	if(Visible && FitToImage && Bmp)
	{
		try
		{
			Bmp->BeginUpdate();
			try
			{
				//+heading for compass view
				//-heading for gyrocompass view
				if(easyrendering)
				{
					xmid=ToolRect.Left+30;
					ymid=ToolRect.Top+30;
					x1 = (int)(xmid + 30 * sin(heading));
					x2 = (int)(xmid - 30 * sin(heading));
					y1 = (int)(ymid + 30 * cos(heading));
					y2 = (int)(ymid - 30 * cos(heading));
					Bmp->LineAS(x1, y1, x2, y2, clRed32);
					x1 = (int)(x2 + 14 * sin(heading-0.5));
					y1 = (int)(y2 + 14 * cos(heading-0.5));
					Bmp->LineAS(x1, y1, x2, y2, clRed32);
					x1 = (int)(x2 + 14 * sin(heading+0.5));
					y1 = (int)(y2 + 14 * cos(heading+0.5));
					Bmp->LineAS(x1, y1, x2, y2, clRed32);
					//Pitch
					ymid=ToolRect.Top+100;
					//x1 = xmid+30;//(int)(xmid + 30 * cos(pitch));
					//x2 = xmid-30;//(int)(xmid - 30 * cos(pitch));
					//y1 = (int)(ymid + 30 * sin(pitch));
					//y2 = (int)(ymid - 30 * sin(pitch));
					dd=pitch;
					if(dd>=0) while(dd>M_PI) dd-=M_PI2;
					else while(dd<-M_PI) dd+=M_PI2;
					y1 = ymid+(int)(30*dd/M_PI);
					Bmp->FrameRectS(xmid-30, ymid-30, xmid+31, ymid+31, clOlive32);
					Bmp->FillRect(xmid-30, y1, xmid+31, ymid+30, clOlive32);
					//Bmp->LineAS(x1, y1, x2, y1, clGreen32);
					//Roll
					ymid=ToolRect.Top+170;
					x1 = (int)(xmid + 30 * cos(roll));
					x2 = (int)(xmid - 30 * cos(roll));
					y1 = (int)(ymid + 30 * sin(roll));
					y2 = (int)(ymid - 30 * sin(roll));
					Bmp->FrameRectS(xmid-30, ymid-30, xmid+31, ymid+31, clGreen32);
					Bmp->LineAS(x1, y1, x2, y2, clLime32);
				}
				else
				{
					Types::TRect TempRect;

					TempBmp->BeginUpdate();
					try
					{   //Pitch
						ymid=-pitch;
						if(ymid>=0) while(ymid>M_PI) ymid-=M_PI2;
						else while(ymid<-M_PI) ymid+=M_PI2;
						ymid = (int)((BackgroundBmp->Height>>1) + 2.2*180.*ymid/M_PI); //2.2 px in 1 deg
						TempRect=Types::TRect(0, ymid-(TempBmp->Height>>1), TempBmp->Width, ymid+(TempBmp->Height>>1));
						TempBmp->Clear(Color32(0,0,0,0));
						TempBmp->Draw(Types::TRect(0, 0, TempBmp->Width, TempBmp->Height), TempRect, BackgroundBmp);
						//Roll, rotate TmpBmp correspondingly to roll
						RotateBitmap32(TempBmp, roll, true);
						//Heading
						TempBmp->Draw(Types::TRect(0, 0, TempBmp->Width, TempBmp->Height),
							Types::TRect(0, 0, TempBmp->Width, TempBmp->Height), GradsBmp);
						RotateBitmap32(TempBmp, -h, true);
						TempBmp->Draw(Types::TRect(0, 0, TempBmp->Width, TempBmp->Height),
							Types::TRect(0, 0, TempBmp->Width, TempBmp->Height), CompassArrowBmp);
						RotateBitmap32(TempBmp, h, true);
					}
					__finally
					{
						TempBmp->EndUpdate();
					}
					x1=ToolRect.Left+((CaseBmp->Width-TempBmp->Width)>>1);
					y1=ToolRect.Top+((CaseBmp->Height-TempBmp->Height)>>1);
					Bmp->Draw(Types::TRect(x1, y1, x1+TempBmp->Width, y1+TempBmp->Height),
						Types::TRect(0, 0, TempBmp->Width, TempBmp->Height),
						TempBmp);
					/*//Heading
					TBitmap32 *bmp=new TBitmap32();
					double h=-heading;

					bmp->DrawMode=dmTransparent;
					bmp->SetSize(TempBmp->Width, TempBmp->Height);
					bmp->Clear(Color32(0,0,0,0));
					bmp->Draw(Types::TRect(0, 0, TempBmp->Width, TempBmp->Height),
						Types::TRect(x1, y1, x1+TempBmp->Width, y1+TempBmp->Height), Bmp);
					RotateBitmap32(bmp, -h);
					bmp->Draw(Types::TRect(0, 0, TempBmp->Width, TempBmp->Height),
							Types::TRect(0, 0, TempBmp->Width, TempBmp->Height), CompassArrowBmp);
					RotateBitmap32(bmp, h);

					Bmp->Draw(Types::TRect(x1, y1, x1+TempBmp->Width, y1+TempBmp->Height),
						Types::TRect(0, 0, TempBmp->Width, TempBmp->Height),
						bmp);*/
					Bmp->Draw(ToolRect, Types::TRect(0, 0, CaseBmp->Width, CaseBmp->Height), CaseBmp);
					if(ShowGlass)
					{
						x1=ToolRect.Left+((CaseBmp->Width-GlassBmp->Width)>>1);
						y1=ToolRect.Top+((CaseBmp->Height-GlassBmp->Height)>>1);
						Bmp->Draw(Types::TRect(x1, y1, x1+GlassBmp->Width, y1+GlassBmp->Height),
							Types::TRect(0, 0, GlassBmp->Width, GlassBmp->Height),
							GlassBmp);
					}
				}
			}
			__finally
			{
				Bmp->EndUpdate();
			}
		}
		catch(Exception &e) {}
	}
	//TVisualToolObject::Redraw(Sender, Bmp);
}

//---------------------------------------------------------------------------
// TMapScale
//---------------------------------------------------------------------------
__fastcall TMapScale::TMapScale(TMyObject *AOwner, TImgView32 *ImgView,
	TGPSCoordinatesRect *GCR, TCoordinateSystem *aCS, TObject *AManager) : TVisualToolObject(AOwner, ImgView, AManager)
{
	fixedposition=true;
	mouseable=false;
	dockable=false;
	width=100;
	height=50;
	LatLonRect=GCR;
	CS=aCS;
	RebuildInternalRects();
	ToolAlign=vtoaLeftBottom;
	zoomtext="";
	ShowCloseBtn=false;
}
//---------------------------------------------------------------------------

void __fastcall TMapScale::Redraw(TObject *Sender, TBitmap32 *Bmp)
{
	if(Bmp && image)
	{
		Types::TRect vpr, bmr;
		double w, m, x, d, sad; //, h, coef;
		int x1, x2, y, tw, th;
		TCoordinateSystem cs;
		AnsiString str;
		TFont *font;

		if(LatLonRect)
		{
			if(CS) cs=*CS;
			else if(Manager && ((TRadarMapManager*)Manager)->Settings)
			{
				/*if(((TRadarMapManager*)Manager)->MapObjects && ((TRadarMapManager*)Manager)->MapObjects->Container)
					cs=((TRadarMapManager*)Manager)->MapObjects->Container->CoordinateSystem;
				else*/
				cs=((TRadarMapManager*)Manager)->Settings->DefaultCoordinateSystem;
			}
			else cs=csMetric;
			if(LatLonRect->LeftTop->GetCSDimension(cs)==csdDegrees) cs=csLocalMetric();
			w=fabs(LatLonRect->WidthInMeters(cs));
			//h=fabs(LatLonRect->HeightInMeters(cs));
			vpr=image->GetViewportRect();
			bmr=image->GetBitmapRect();
			//coef=(float)bmr.Width()/(float)vpr.Width();
			//if(coef>0) w/=coef;
			//else w=0;
			if(w>0)
			{
				x=0;
				d=1;
				do
				{
					x++;
					m=x*(float)bmr.Width()/w;//vpr.Width()/w;
				} while(m<width);
				while(m>width)
				{
					if(x<=d+d/10.) d/=10.;
					x-=d;
					m=x*(float)bmr.Width()/w;//vpr.Width()/w;
				}
				x1=ToolRect.left;
				y=ToolRect.bottom;
				x2=ToolRect.left+(int)m;
				font=new TFont();
				Bmp->BeginUpdate();
				try
				{
					font->Name=Bmp->Font->Name;
					font->Size=Bmp->Font->Size;
					font->Style=Bmp->Font->Style;
					Bmp->Font->Name="Tahoma";
					Bmp->Font->Size=9;
					Bmp->Font->Style=TFontStyles();
					Bmp->LineAS(x1, y-5, x1, y, clBlack32);
					Bmp->LineAS(x1, y, x2+1, y, clBlack32);
					Bmp->LineAS(x2, y-5, x2, y, clBlack32);
					if(x<1)
					{
						str=" cm";
						x*=100.;
					}
					else if(x>500)
					{
						str=" km";
						x/=1000.;
					}
					else str=" m";
					if(x<2) sad=2;
					else if(x<10) sad=1;
					else sad=0;
					str=FloatToStrF(x, ffFixed, 10, sad)+str;
					tw=Bmp->TextWidth(str);
					th=Bmp->TextHeight(str);
					Bmp->RenderText(x1+(((int)m-tw)>>1), y-th, str, 2, clBlack32);
					if(zoomtext!="" && zoomtext.Length()>0)
					{
						Bmp->Font->Size=6;
						str="Zoom "+zoomtext;
						tw=Bmp->TextWidth(str);
						Bmp->RenderText(x1+(((int)m-tw)>>1), y+3, str, 2, clBlack32);
					}
				}
				__finally
				{
					Bmp->Font->Name=font->Name;
					Bmp->Font->Size=font->Size;
					Bmp->Font->Style=font->Style;
					Bmp->EndUpdate();
					delete font;
				}
			}
		}
	}
}

//---------------------------------------------------------------------------
// TWindRose
//---------------------------------------------------------------------------
__fastcall TWindRose::TWindRose(TMyObject *AOwner, TImgView32 *ImgView, TObject *AManager) :
	TVisualToolObject(AOwner, ImgView, AManager)
{
	bool v;

	fixedposition=true;
	dockable=false;
	easyrendering=false;//true;
	ForceSmallSize=false;

	mouseable=false;

	RebuildInternalRects();
	WindRoseBmp=NULL;
	if(Manager && ((TRadarMapManager*)Manager)->Settings)
	{
		CloseExternalSetting=&(((TRadarMapManager*)Manager)->Settings->ShowWindRose);
		v=((TRadarMapManager*)Manager)->Settings->ShowWindRose;
		//ToolAlignExternalSetting=&(((TRadarMapManager*)Manager)->Settings->GyroCompassTAlign);
		//toolalign=((TRadarMapManager*)Manager)->Settings->GyroCompassTAlign;
	}
	else v=visible;
	toolalign=vtoaLeftTop;
	SmallResize();
	ShowCloseBtn=false;
	RebuildInternalRects();
	Visible=v;
}
//---------------------------------------------------------------------------

__fastcall TWindRose::~TWindRose()
{
	if(WindRoseBmp) delete WindRoseBmp;
}
//---------------------------------------------------------------------------

void __fastcall TWindRose::SmallResize()
{
	if(!easyrendering)
	{
		if(WindRoseBmp) delete WindRoseBmp;
		if(ForceSmallSize || smallsize)
			WindRoseBmp=LoadPNG32Resource((System::UnicodeString)"PNGIMAGE_394");//"PNGIMAGE_227");
		else WindRoseBmp=LoadPNG32Resource((System::UnicodeString)"PNGIMAGE_393");//"PNGIMAGE_166");
		//if(ForceSmallSize || smallsize) DecreaseGlyph(WindRoseBmp);
		if(WindRoseBmp)
		{
			width=WindRoseBmp->Width;
			height=WindRoseBmp->Height;
		}
		else
		{
			easyrendering=true;
			width=20;
			width+=SmallCloseBtn->Width;
			height=70;
		}
	}
	else
	{
		width=20;
		width+=SmallCloseBtn->Width;
		height=70;
	}
	TVisualToolObject::SmallResize();
}
//---------------------------------------------------------------------------

void __fastcall TWindRose::Redraw(TObject *Sender, TBitmap32 *Bmp)
{
	Types::TRect vpr;
	int x1, y1, w, h, tw, th;
	AnsiString str;

	if(Bmp && image)
	{
		vpr=image->GetViewportRect();
		x1=ToolRect.left;
		y1=ToolRect.top;
		w=ToolRect.Width();
		h=ToolRect.Height();
		if(!smallsize)
		{
			if(w+w*ForceSmallSize>vpr.Width()/3)// || h>vpr.Height()/3)
			{
				if(!ForceSmallSize)
				{
					ForceSmallSize=true;
					SmallResize();
					w=ToolRect.Width();
					h=ToolRect.Height();
                }
			}
			else if(ForceSmallSize)
			{
				ForceSmallSize=false;
				SmallResize();
				w=ToolRect.Width();
				h=ToolRect.Height();
            }
		}
		Bmp->BeginUpdate();
		try
		{
			if(easyrendering)
			{
				TFont *font;

				font=new TFont();
				try
				{
					font->Name=Bmp->Font->Name;
					font->Size=Bmp->Font->Size;
					font->Style=Bmp->Font->Style;
					Bmp->Font->Name="Tahoma";
					Bmp->Font->Size=12;
					Bmp->Font->Style=TFontStyles() << fsBold;
					str="N";
					tw=Bmp->TextWidth(str);
					th=Bmp->TextHeight(str);
					Bmp->RenderText(x1+((w-tw)>>1)+1, y1, str, 2, clRed32);
				}
				__finally
				{
                    Bmp->Font->Name=font->Name;
					Bmp->Font->Size=font->Size;
					Bmp->Font->Style=font->Style;
					delete font;
                }
				x1+=(w>>1);
				w=tw>>1;
				y1+=th+2;
				h-=th+2;
				Bmp->LineAS(x1-w, y1+th, x1, y1, clNavy32);
				Bmp->LineAS(x1+w, y1+th, x1, y1, clNavy32);
				Bmp->LineAS(x1, y1, x1, y1+h-th, clNavy32, true);
				y1+=h-th;
				for(int i=0; i<3; i++)
				{
					Bmp->LineAS(x1-w, y1+th, x1, y1, clNavy32);
					Bmp->LineAS(x1+w, y1+th, x1, y1, clNavy32);
					y1-=8;
				}
			}
			else
			{
				Bmp->Draw(Types::TRect(x1, y1, WindRoseBmp->Width+x1, WindRoseBmp->Height+y1),
					Types::TRect(0, 0, WindRoseBmp->Width, WindRoseBmp->Height), WindRoseBmp);
			}
		}
		__finally
		{
			Bmp->EndUpdate();
		}
	}
}

//---------------------------------------------------------------------------
// TToolClock
//---------------------------------------------------------------------------
__fastcall TToolClock::TToolClock(TMyObject *AOwner, TImgView32 *ImgView, TObject *AManager) :
	TVisualToolObject(AOwner, ImgView, AManager)
{
	bool v;

	fixedposition=true;
	dockable=false;
	easyrendering=false;//true;
	ForceSmallSize=false;

	mouseable=false;

	if(Manager && ((TRadarMapManager*)Manager)->Settings)
	{
		CloseExternalSetting=&(((TRadarMapManager*)Manager)->Settings->ShowClocks);
		v=((TRadarMapManager*)Manager)->Settings->ShowClocks;
	}
	else v=visible;
	toolalign=vtoaRightTop;
	SmallResize();

	time=Now();
	CS=csLatLon;
	color=Color32(100, 100, 100, 100);
	Timer=new TTimerThread(1000);
	Timer->MinStep=500;
	Timer->OnTimer=Refresh;
	Timer->Start();
	ShowCloseBtn=false;
	RebuildInternalRects();
	Visible=v;
}
//---------------------------------------------------------------------------

__fastcall TToolClock::~TToolClock()
{
	if(Timer!=NULL)
	{
		Timer->AskForTerminate();
		WaitOthers();
		if(Timer->ForceTerminate()) delete Timer;
		else Timer->FreeOnTerminate=true;
		Timer=NULL;
	}
}
//---------------------------------------------------------------------------

void __fastcall TToolClock::SmallResize()
{
	if(ForceSmallSize || smallsize)
	{
		width=60;
		height=50;
		border=3;
	}
	else
	{
		width=120;
		height=100;
		border=5;
	}
	TVisualToolObject::SmallResize();
}
//---------------------------------------------------------------------------

void __fastcall TToolClock::Refresh()
{
	System::Word h, m, s, h2, m2;
	DecodeTime(time, h, m, s, s);
	DecodeTime(Now(), h2, m2, s, s);
	if(h2-h+m2-m>0 || (Manager && ((TRadarMapManager*)Manager)->Settings &&
		CS!=((TRadarMapManager*)Manager)->Settings->DefaultCoordinateSystem))
	{
		Update();
	}
}
//---------------------------------------------------------------------------

void __fastcall TToolClock::Redraw(TObject *Sender, TBitmap32 *Bmp)
{
	Types::TRect vpr;
	int x1, y1, w, h, tw, th;
	AnsiString str, str2;
	TFont *font;
	TGPSCoordinate* c;
	System::Word hh, m, s;
	TColor32 cc;
	TColor32Entry ce, ce2;

	if(Bmp && image)
	{
		vpr=image->GetViewportRect();
		x1=ToolRect.left+border;
		y1=ToolRect.top+border;
		w=ToolRect.Width()-border*2;
		h=ToolRect.Height()-border*2;
		if(!smallsize)
		{
			if(w+w*ForceSmallSize>vpr.Width()/3)// || h>vpr.Height()/3)
			{
				if(!ForceSmallSize)
				{
					ForceSmallSize=true;
					SmallResize();
					w=ToolRect.Width()-border*2;
					h=ToolRect.Height()-border*2;
				}
			}
			else if(ForceSmallSize)
			{
				ForceSmallSize=false;
				SmallResize();
				w=ToolRect.Width()-border*2;
				h=ToolRect.Height()-border*2;
			}
		}
		font=new TFont();
		c=new TGPSCoordinate();
		Bmp->BeginUpdate();
		try
		{
			if(Manager && ((TRadarMapManager*)Manager)->Settings)
			{
				int i;
				ce.ARGB=color;
				ce2.ARGB=((TRadarMapManager*)Manager)->Settings->DXFBackgroundColor;
				i=abs(ce2.R-ce.R)+abs(ce2.G-ce.G)+abs(ce2.B-ce.B);
				if(i<100)
				{
					ce.R-=(100-i);
					ce.G-=(100-i);
					ce.B-=(100-i);
					color=ce.ARGB;
				}
			}
			CS=((TRadarMapManager*)Manager)->Settings->DefaultCoordinateSystem;
			time=Now();
			DecodeTime(time, hh, m, s, s);
			str="";
			if(hh<10) str+="0";
			str+=IntToStr(hh)+":";
			if(m<10) str+="0";
			str+=IntToStr(m);

			font->Name=Bmp->Font->Name;
			font->Size=Bmp->Font->Size;
			font->Style=Bmp->Font->Style;
			Bmp->Font->Name="Tahoma";
			Bmp->Font->Height=(h>>1)+(h>>3);
			tw=Bmp->TextWidth(str);
			th=Bmp->TextHeight(str);
			Bmp->RenderText(x1+((w-tw)>>1)+1, y1, str, 2, color);
			y1+=th;
			h-=Bmp->Font->Height;
			str=DateToStr(time);
			Bmp->Font->Height=h>>1;
			tw=Bmp->TextWidth(str);
			th=Bmp->TextHeight(str);
			Bmp->RenderText(x1+((w-tw)>>1)+1, y1, str, 2, color);
			y1+=th;
			str=c->GetCSName(CS);
			th=Bmp->TextHeight(str);
			if(str!=NULL && str.Length()>25)
			{
				int i=str.Length();
				
				if(i>25) i=25;
				while(i>0 && str[i]!=' ') i--;
				if(i>0 && str[i]==' ') 
				{
					str2=str;
					str2.SetLength(i);
					tw=Bmp->TextWidth(str2);
					Bmp->RenderText(x1+((w-tw)>>1)+1, y1, str2, 2, color);
					y1+=th;
					str=(char*)(str.c_str()+i);
				}
			}
			tw=Bmp->TextWidth(str);
			Bmp->RenderText(x1+((w-tw)>>1)+1, y1, str, 2, color);
		}
		__finally
		{
			Bmp->Font->Name=font->Name;
			Bmp->Font->Size=font->Size;
			Bmp->Font->Style=font->Style;
			Bmp->EndUpdate();
			delete c;
			delete font;
		}
	}
}

//---------------------------------------------------------------------------
// THyperbolaTool
//---------------------------------------------------------------------------
__fastcall THyperbolaTool::THyperbolaTool(TMyObject *AOwner, TImgView32 *ImgView, TObject *AManager,
	TSliderToolButton *AConnectedBtn) : TVisualToolObject(AOwner, ImgView, AManager, AConnectedBtn)
{
	Types::TRect* OperatingRect;

	if(Manager)
	{
		if(((TRadarMapManager*)Manager)->CurrentProfilesCount>0)
			OperatingRect=((TRadarMapManager*)Manager)->ProfRect[((TRadarMapManager*)Manager)->CurrentProfilesCount-1];
		else OperatingRect=NULL;
		if(((TRadarMapManager*)Manager)->Settings)
			permitivity=((TRadarMapManager*)Manager)->Settings->Permitivity;
		else permitivity=5.;
	}
	else
	{
		OperatingRect=NULL;
		permitivity=5.;
	}
	if(OperatingRect)
	{
		headpoint=Types::TPoint(OperatingRect->Width()>>1, OperatingRect->Height()>>1);
		branchpoint=Types::TPoint(OperatingRect->Width()>>2, 3*OperatingRect->Height()>>2);
	}
	else
	{
		Types::TRect vpr;

		if(ImgView) vpr=ImgView->GetViewportRect();
		else vpr=Types::TRect(0, 0, 100, 100);
		headpoint=Types::TPoint(vpr.Width()>>1, vpr.Height()>>1);
		branchpoint=Types::TPoint(vpr.Width()>>1, vpr.Height()>>1);
	}
	HyperbolaBrenchLength=0;
	Points=new TFixedPoint[HyperbolaBrenchLengthMax*2+1];
	for(int i=0; i<HyperbolaBrenchLengthMax*2+1; i++)
		Points[i]=FixedPoint(0,0);
	Message=NULL;
	//RecalculHyperbola();
	//toolalign=vtoaCenter;
	Visible=false;
	ShowCloseBtn=true;
	Moved=false;
	SmallResize();
	toolalign=vtoaNone;
	RebuildInternalRects();
	ShowApplyBtn=true;
	Visible=false;
}
//---------------------------------------------------------------------------

__fastcall THyperbolaTool::~THyperbolaTool()
{
	delete[] Points;
	//delete SmallOkBtn;
}
//---------------------------------------------------------------------------

void __fastcall THyperbolaTool::MouseClick(System::TObject* Sender, int X, int Y)
{
	if(Y>=HeadPoint.y && !Moved)
	{
		BranchPoint.x=X;
		BranchPoint.y=Y;
		RecalculHyperbola();
	}
}
//---------------------------------------------------------------------------

void __fastcall THyperbolaTool::RecalculHyperbola(bool recalculPermit)
{
	double ProfWidth, TimeRange, HypTop, Zero, BranchTop, BranchLeft;
	double _ProfHeight, _Zero;
	int _ZeroInPx, x, y, ProfIndex, i, j;
	double top;
	double km, c;
	Types::TRect *ProfRect;
	TProfile* Profile;
	Types::TPoint Head, Branch, p0, p2;

	if(Manager && ((TRadarMapManager*)Manager)->Settings)
	{
		ProfRect=((TRadarMapManager*)Manager)->GetProfRect(HeadPoint.x, HeadPoint.y);
		ProfIndex=((TRadarMapManager*)Manager)->GetProfRectIndex(ProfRect);
		if(ProfIndex>=0 && ((TRadarMapManager*)Manager)->CurrentProfiles[ProfIndex])
		{
			Profile=((TRadarMapManager*)Manager)->CurrentProfiles[ProfIndex];
			Head=Types::TPoint(HeadPoint.x-ProfRect->Left, HeadPoint.y-ProfRect->Top);
			Branch=Types::TPoint(BranchPoint.x-ProfRect->Left, BranchPoint.y-ProfRect->Top);
			if(Profile->OutputView && Profile->OutputView->WidthInMeters()>0)
			{
				ProfWidth=Profile->OutputView->WidthInMeters();
			}
			else
			{
				ProfWidth=((double)ProfRect->Width()/(double)((TRadarMapManager*)Manager)->Settings->TracesInPixel);
				ProfWidth*=Profile->HorRangeFull/(double)Profile->Traces;
			}
			//-- height of visible profile's rectangle in ns
			TimeRange=Profile->TimeRange*1e-9;
			//-- calculate ZeroPoint in ns
			Zero=(Profile->ZeroPoint/Profile->Samples)*TimeRange;
			//Zero=0;
			//-- calculate time offset from rect. top to hyp. top in ns
			HypTop=((double)Head.y/(double)ProfRect->Height())*TimeRange;
			HypTop-=Zero;
			if(recalculPermit)
			{
				//-- calculate time offset (delta t)
				BranchTop=(((double)dabs(Head.y-Branch.y)+1)/(double)ProfRect->Height())*TimeRange;
				//-- calculate horizontal offset in m (delta d)
				BranchLeft=(((double)dabs(Head.x-Branch.x)+1)/(double)ProfRect->Width())*ProfWidth;
				//if(permitivity<1) // Recalculate permitivity on the base of new branches
				   permitivity=(3e8*3e8*BranchTop/4./BranchLeft/BranchLeft)*(2*HypTop+BranchTop);
				if(permitivity<1) permitivity=1;
				if(permitivity>100) permitivity=100;
			}
			//-- recalculated heigh of visible profile's rectangle in meters on the base of changed Permitivity
			_ProfHeight=TimeRange*3e8/2./sqrt(permitivity);
			//-- offset from rectangle's top to zero-point in meters
			_Zero=Zero*3e8/2./sqrt(permitivity);
			//-- zero-point's position in pixels
			_ZeroInPx=(int)((Profile->ZeroPoint/Profile->Samples)*(float)ProfRect->Height());
			if(Head.y<_ZeroInPx)
			{
				Head.y=_ZeroInPx;
				HeadPoint.y=Head.y+ProfRect->Top;
			}
			top=-_Zero+((double)Head.y/(double)ProfRect->Height())*_ProfHeight;
			if (top>=0) top=top*top;
			else top=0;
			HyperbolaBrenchLength=HyperbolaBrenchLengthMax-
				(int)((log((double)permitivity)/log(100.))*(double)(HyperbolaBrenchLengthMax-HyperbolaBrenchLengthMin));
			Points[HyperbolaBrenchLength]=FixedPoint((float)(Head.x+ProfRect->Left), (float)(Head.y+ProfRect->Top));
			i=1;
			while(i<=HyperbolaBrenchLength)
			{
				km=((double)i/(double)ProfRect->Width())*ProfWidth;
				km*=km;
				c=sqrt(km+top);
				c+=_Zero;
				c=(c/_ProfHeight)*ProfRect->Height();
				y=(int)c+ProfRect->Top;
				x=Head.x+i+ProfRect->Left;
				if(x>=ProfRect->Left && x<ProfRect->Right && y>=ProfRect->Top && y<ProfRect->Bottom)
					Points[HyperbolaBrenchLength+i]=FixedPoint(x, y);
				else Points[HyperbolaBrenchLength+i]=FixedPoint(0,0);
				x=Head.x-i+ProfRect->Left;
				if (x>=ProfRect->Left && x<ProfRect->Right && y>ProfRect->Top && y<ProfRect->Bottom)
					Points[HyperbolaBrenchLength-i]=FixedPoint(x, y);
				else Points[HyperbolaBrenchLength-i]=FixedPoint(0,0);
				i++;
			}
			i+=HyperbolaBrenchLength;
			while(i<=HyperbolaBrenchLengthMax*2)
			{
				Points[i]=FixedPoint(0,0);
				i++;
			}
			i=0;
			while(i<HyperbolaBrenchLength*2+1)
			{
				j=i;
				while(j<HyperbolaBrenchLength*2+1 && Points[j].X==0 && Points[j].Y==0) j++;
				if (j>i)
				{
					if(j!=HyperbolaBrenchLength*2+1)
					{
						for (; i<j; i++)
						{
							Points[i].X=Points[j].X;
							Points[i].Y=Points[j].Y;
						}
					}
					else
					{
						j--;
						if(i>0)
							for (; j>=i; j--)
							{
								Points[j].X=Points[i-1].X;
								Points[j].Y=Points[i-1].Y;
							}
					}
					i=j;
				}
				else i++;
			}
			ToolRect.Left=Gr32::Point(Points[0]).x-20;
			ToolRect.Top=HeadPoint.y-20;
			width=Gr32::Point(Points[HyperbolaBrenchLength*2]).x-Gr32::Point(Points[0]).x+40;
			Height=Gr32::Point(Points[0]).y-HeadPoint.y+40;
		}
		else
		{
			HyperbolaBrenchLength=0;
			Message=(TObject*)((TRadarMapManager*)Manager)->ShowMessageObject(
				(TMessageObject*)Message, ((TRadarMapManager*)Manager)->RadarMessages,
				TMessageType::mtStop, mvtDisapering, 5000,
				"Cannot apply Hyperbola on empty profile!", (AnsiString)_RadarMapName);
			TVisualToolObject::ClosePressed(NULL, 0, 0);
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall THyperbolaTool::Redraw(TObject *Sender, TBitmap32 *Bmp)
{
	TFont *font;
	int th, tw;
	AnsiString str;

	if(Visible && Bmp && HyperbolaBrenchLength>0) // && FitToImage
	{
		Bmp->BeginUpdate();
		try
		{
			//Bmp->FillRectTS(ToolRect, Color32(240,240,240,160));
			//Bmp->FrameRectTS(ToolRect, Color32(80,80,80,255));
			DrawPolyLine(Bmp, clLime32, clGreen32, 3, false, Points, HyperbolaBrenchLength*2);
			font=new TFont();
			try
			{
				str="E="+FloatToStrF(Permitivity, ffFixed, 5, 1); //"e="
				font->Name=Bmp->Font->Name;
				font->Size=Bmp->Font->Size;
				font->Style=Bmp->Font->Style;
				Bmp->Font->Name="Tahoma";//"Symbol";
				Bmp->Font->Size=12;
				Bmp->Font->Style=TFontStyles();
				tw=Bmp->TextWidth(str);
				th=Bmp->TextHeight(str);
				Bmp->FillRectT(HeadPoint.x-(tw>>1)-2, HeadPoint.y+th-2, HeadPoint.x+(tw>>1)+2, HeadPoint.y+2*th+2,
					Color32(255, 255, 255, 128));
				Bmp->RenderText(HeadPoint.x-(tw>>1), HeadPoint.y+th, str, 2, clBlack32);
			}
			__finally
			{
				Bmp->Font->Name=font->Name;
				Bmp->Font->Size=font->Size;
				Bmp->Font->Style=font->Style;
				delete font;
			}
			Bmp->PenColor=clGreen32;// Color32(255, 0, 0, transparency);
		}
		__finally
		{
			Bmp->EndUpdate();
		}
		TVisualToolObject::Redraw(Sender, Bmp);
	}
}
//---------------------------------------------------------------------------

void __fastcall THyperbolaTool::MouseMove(System::TObject* Sender,
	Classes::TShiftState Shift, int X, int Y, TCustomLayer *Layer)
{
	if(pressed && image && !fixedposition)
	{
		int x, y, k, z;
		Types::TRect* ProfRect;

		x=HeadPoint.x-BranchPoint.x;
		y=HeadPoint.y-BranchPoint.y;
		k=X;//-dX;
		z=Y;//-dY;
		ProfRect=((TRadarMapManager*)Manager)->GetProfRect(k, z);
		if(ProfRect)
		{
			if(k<ProfRect->Left+20) k=ProfRect->Left+20;
			else if(k>ProfRect->Right-20) k=ProfRect->Right-20;
			if(z<ProfRect->Top+20) z=ProfRect->Top+20;
			else if(z>ProfRect->Bottom-20) z=ProfRect->Bottom-20;
			HeadPoint.x=k-dX; //k
			HeadPoint.y=z-dY; //z
			if(HeadPoint.x<ProfRect->Left+20) HeadPoint.x=ProfRect->Left+20;
			else if(HeadPoint.x>ProfRect->Right-20) HeadPoint.x=ProfRect->Right-20;
			if(HeadPoint.y<ProfRect->Top) HeadPoint.y=ProfRect->Top;
			else if(HeadPoint.y>ProfRect->Bottom-20) HeadPoint.y=ProfRect->Bottom-20;
			BranchPoint.x=HeadPoint.x-x;
			BranchPoint.y=HeadPoint.y-y;
			//ToolRect.Left=HeadPoint.x-(width>>1);
			//ToolRect.Top=HeadPoint.y;
			//if(abs(X1-X)>=2 || abs(Y1-Y)>=2)
			Moved=true;
			RecalculHyperbola(false);
			RebuildInternalRects();
			Update();
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall THyperbolaTool::OKPressed(System::TObject* Sender, int X, int Y)
{
	TProfile *Profile;
	int i;

	if(Manager)
	{
		((TRadarMapManager*)Manager)->Settings->Permitivity=permitivity;
		i=((TRadarMapManager*)Manager)->GetProfRectIndex(
			((TRadarMapManager*)Manager)->GetProfRect(HeadPoint.x, HeadPoint.y));
		if(i>=0 && ((TRadarMapManager*)Manager)->CurrentProfiles[i])
		{
			Profile=((TRadarMapManager*)Manager)->CurrentProfiles[i];
			if(((TRadarMapManager*)Manager)->CurrentSettings)
				((TRadarMapManager*)Manager)->CurrentSettings->Permitivity=permitivity;
			if(Profile)
			{
				Profile->Permit=permitivity;
				if(((TRadarMapManager*)Manager)->DepthScales[i])
				{
					((TRadarMapManager*)Manager)->DepthScales[i]->Max=0;
					((TRadarMapManager*)Manager)->DepthScales[i]->Min=Profile->Depth;
					((TRadarMapManager*)Manager)->DepthScales[i]->Max=-(((float)Profile->ZeroPoint/(float)Profile->Samples)*Profile->Depth);
					((TRadarMapManager*)Manager)->DepthScales[i]->Min+=((TRadarMapManager*)Manager)->DepthScales[i]->Max;
					((TRadarMapManager*)Manager)->DepthScales[i]->Draw();
				}
			}
		}
	}
	TVisualToolObject::OKPressed(Sender, X, Y);
}

//---------------------------------------------------------------------------
// TZeroPointTool
//---------------------------------------------------------------------------
__fastcall TZeroPointTool::TZeroPointTool(TMyObject *AOwner, TImgView32 *ImgView, TObject *AManager,
	TSliderToolButton *AConnectedBtn) : TVisualToolObject(AOwner, ImgView, AManager, AConnectedBtn)
{
	Types::TRect vpr;

	if(ImgView) vpr=ImgView->GetViewportRect();
	else vpr=Types::TRect(0, 0, 100, 100);
	if(Manager)
	{
		if(((TRadarMapManager*)Manager)->DepthScales[0])
		{
			zeropoint=-((TRadarMapManager*)Manager)->DepthScales[0]->Max/
				(((TRadarMapManager*)Manager)->DepthScales[0]->Min-((TRadarMapManager*)Manager)->DepthScales[0]->Max);
		}
		else zeropoint=0.;
		if(((TRadarMapManager*)Manager)->ProfRect[0]) vpr=Types::TRect(*((TRadarMapManager*)Manager)->ProfRect[0]);
	}
	else zeropoint=0.;
	Points=new TFixedPoint[2];
	Point=Types::TPoint(vpr.Left+(vpr.Width()>>1), vpr.top+(int)((float)vpr.Height()*zeropoint));
	//toolalign=vtoaCenter;
	Visible=false;
	ShowCloseBtn=true;
	SmallResize();
	toolalign=vtoaNone;
	RebuildInternalRects();
	RecalculZeroPoint();
	ShowApplyBtn=true;
	Visible=false;
}
//---------------------------------------------------------------------------

void __fastcall TZeroPointTool::MouseClick(System::TObject* Sender, int X, int Y)
{
	Point.x=X;
	Point.y=Y;
	RecalculZeroPoint();
}
//---------------------------------------------------------------------------

void __fastcall TZeroPointTool::RecalculZeroPoint(bool Recalcul)
{
	double ProfWidth, TimeRange, HypTop, Zero, BranchTop, BranchLeft;
	double _ProfHeight, _Zero;
	int _ZeroInPx, x, y, ProfIndex, i, j;
	double top;
	double km, c;
	Types::TRect *ProfRect;

	if(Manager && ((TRadarMapManager*)Manager)->Settings)
	{
		ProfRect=((TRadarMapManager*)Manager)->GetProfRect(Point.x, Point.y);
		ProfIndex=((TRadarMapManager*)Manager)->GetProfRectIndex(ProfRect);
		if(ProfIndex>=0 && ((TRadarMapManager*)Manager)->DepthScales[ProfIndex] &&
			((TRadarMapManager*)Manager)->CurrentProfiles[ProfIndex])
		{
			if(Recalcul) zeropoint=(float)(Point.y-ProfRect->Top)/(float)ProfRect->Height();
			else Point=Types::TPoint(ProfRect->Left+(ProfRect->Width()>>1), ProfRect->top+(int)((float)ProfRect->Height()*zeropoint));

			/*((TRadarMapManager*)Manager)->DepthScales[ProfIndex]->Max=0;
			((TRadarMapManager*)Manager)->DepthScales[ProfIndex]->Min=((TRadarMapManager*)Manager)->Profiles[ProfIndex]->Depth;
			((TRadarMapManager*)Manager)->DepthScales[ProfIndex]->Max=-(zeropoint*((TRadarMapManager*)Manager)->Profiles[ProfIndex]->Depth);
			((TRadarMapManager*)Manager)->DepthScales[ProfIndex]->Min+=((TRadarMapManager*)Manager)->DepthScales[ProfIndex]->Max;
			((TRadarMapManager*)Manager)->DepthScales[ProfIndex]->Draw();*/

			ToolRect.Left=ProfRect->Left;
			ToolRect.Top=ProfRect->Top+(int)((float)ProfRect->Height()*zeropoint)-30;
			width=ProfRect->Width();
			Height=60;
		}
		else
		{
			Message=(TObject*)((TRadarMapManager*)Manager)->ShowMessageObject(
				(TMessageObject*)Message, ((TRadarMapManager*)Manager)->RadarMessages,
				TMessageType::mtStop, mvtDisapering, 5000,
				"Cannot apply Zero Line adjustment on empty profile!", (AnsiString)_RadarMapName);
			TVisualToolObject::ClosePressed(NULL, 0, 0);
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TZeroPointTool::Redraw(TObject *Sender, TBitmap32 *Bmp)
{
	TFont *font;
	int th, tw;
	AnsiString str;

	if(Visible && Bmp && zeropoint>=0.) // && FitToImage
	{
		Bmp->BeginUpdate();
		try
		{
			font=new TFont();
			try
			{
				str=" >0< ";
				font->Name=Bmp->Font->Name;
				font->Size=Bmp->Font->Size;
				font->Style=Bmp->Font->Style;
				Bmp->Font->Name="Tahoma";//"Symbol";
				Bmp->Font->Size=16;
				Bmp->Font->Style=TFontStyles() << fsBold;
				tw=Bmp->TextWidth(str);
				th=Bmp->TextHeight(str);

				Points[0]=FixedPoint((int)ToolRect.Left, (int)Point.y);
				Points[1]=FixedPoint((int)(ToolRect.Left+((ToolRect.Width()-tw) >> 1)), (int)Point.y);
				DrawPolyLine(Bmp, clRed32, clMaroon32, 2, false, Points, 2);
				Points[1]=FixedPoint((int)ToolRect.Right, (int)Point.y);
				Points[0]=FixedPoint((int)(ToolRect.Left+((ToolRect.Width()+tw) >> 1)), (int)Point.y);
				DrawPolyLine(Bmp, clRed32, clMaroon32, 2, false, Points, 2);
				Bmp->RenderText(ToolRect.Left+((ToolRect.Width()-tw) >> 1), Point.y-(th >> 1)-1, str, 2, clRed32);
			}
			__finally
			{
				Bmp->Font->Name=font->Name;
				Bmp->Font->Size=font->Size;
				Bmp->Font->Style=font->Style;
				delete font;
			}
		}
		__finally
		{
			Bmp->EndUpdate();
		}
		TVisualToolObject::Redraw(Sender, Bmp);
	}
}
//---------------------------------------------------------------------------

void __fastcall TZeroPointTool::MouseMove(System::TObject* Sender,
	Classes::TShiftState Shift, int X, int Y, TCustomLayer *Layer)
{
	if(pressed && image && !fixedposition)
	{
		int k, z;
		Types::TRect* ProfRect;

		k=X-dX;
		z=Y-dY;
		ProfRect=((TRadarMapManager*)Manager)->GetProfRect(k, z);
		if(ProfRect)
		{
			Point.x=k;
			Point.y=z;
			RecalculZeroPoint();
			RebuildInternalRects();
			Update();
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TZeroPointTool::OKPressed(System::TObject* Sender, int X, int Y)
{
	TProfile *Profile;
	int i;

	if(Manager)
	{
		i=((TRadarMapManager*)Manager)->GetProfRectIndex(
			((TRadarMapManager*)Manager)->GetProfRect(Point.x, Point.y));
		if(i>=0 && ((TRadarMapManager*)Manager)->CurrentProfiles[i])
		{
			Profile=((TRadarMapManager*)Manager)->CurrentProfiles[i];
			if(Profile)
			{
				Profile->ZeroPoint=(int)(zeropoint*(float)Profile->Samples);
				if(((TRadarMapManager*)Manager)->DepthScales[i])
				{
					((TRadarMapManager*)Manager)->DepthScales[i]->Max=0;
					((TRadarMapManager*)Manager)->DepthScales[i]->Min=Profile->Depth;
					((TRadarMapManager*)Manager)->DepthScales[i]->Max=-(((float)Profile->ZeroPoint/(float)Profile->Samples)*Profile->Depth);
					((TRadarMapManager*)Manager)->DepthScales[i]->Min+=((TRadarMapManager*)Manager)->DepthScales[i]->Max;
					((TRadarMapManager*)Manager)->DepthScales[i]->Draw();
				}
			}
		}
	}
	TVisualToolObject::OKPressed(Sender, X, Y);
}

//---------------------------------------------------------------------------
// TToolPing
//---------------------------------------------------------------------------
__fastcall TToolPing::TToolPing(TMyObject *AOwner, TImgView32 *ImgView, TObject *AManager) :
	TVisualToolObject(AOwner, ImgView, AManager)
{
	bool v;

	fixedposition=false;//true;
	dockable=false;
	easyrendering=false;//true;
	ForceSmallSize=false;

	mouseable=true;//false;

	if(Manager && ((TRadarMapManager*)Manager)->Settings)
	{
		CloseExternalSetting=&(((TRadarMapManager*)Manager)->Settings->ShowPing);
		v=((TRadarMapManager*)Manager)->Settings->ShowPing;
	}
	else v=visible;
	toolalign=vtoaRightTop;
	SmallResize();

	text_color=Color32(100, 100, 100, 200);
	color=Color32(100, 100, 100, 150);
	PingStr="---";
	ping=new Icmp::TPing(false);
	ping->OnRequestUpdate=Refresh;
	ShowCloseBtn=false;
	RebuildInternalRects();
	Visible=v;
	ChangeIPAddress();
}
//---------------------------------------------------------------------------

__fastcall TToolPing::~TToolPing()
{
	if(ping) delete ping;
}
//---------------------------------------------------------------------------

void __fastcall TToolPing::ChangeIPAddress()
{
	if(ping && Manager && ((TRadarMapManager*)Manager)->Settings)
		ping->Ping(((TRadarMapManager*)Manager)->Settings->GeoradarTCPAddr, true);
}
//---------------------------------------------------------------------------

void __fastcall TToolPing::SmallResize()
{
	if(ForceSmallSize || smallsize)
	{
		width=60;
		height=50;
		border=2;
	}
	else
	{
		width=120;
		height=100;
		border=4;
	}
	TVisualToolObject::SmallResize();
}
//---------------------------------------------------------------------------

void __fastcall TToolPing::Refresh()
{
	if(ping)
	{
		if(ping->LastStatus==IP_SUCCESS && ping->LastRoundTripTime>=0)
		{
			if(ping->LastRoundTripTime<1) PingStr="<1";
			else PingStr="="+IntToStr((int)ping->LastRoundTripTime);
			PingStr+="ms";
		}
		else PingStr="---";
		LastRequestResult=ping->LastRequest;
		if(Message) ((TMessageObject*)Message)->Text=LastRequestResult;
		Update();
	}
}
//---------------------------------------------------------------------------

void __fastcall TToolPing::Redraw(TObject *Sender, TBitmap32 *Bmp)
{
	Types::TRect vpr;
	AnsiString str;
	int x1, y1, w, h, tw, th, dx;
	float f;
	TFont *font;
	TGPSCoordinate* c;
	System::Word hh, m, s;

	if(Bmp && image && ping)
	{
		vpr=image->GetViewportRect();
		x1=ToolRect.left+border;
		y1=ToolRect.top+border;
		w=ToolRect.Width()-border*2;
		h=ToolRect.Height()-border*2;
		if(!smallsize)
		{
			if(w+w*ForceSmallSize>vpr.Width()/3)// || h>vpr.Height()/3)
			{
				if(!ForceSmallSize)
				{
					ForceSmallSize=true;
					SmallResize();
					w=ToolRect.Width()-border*2;
					h=ToolRect.Height()-border*2;
				}
			}
			else if(ForceSmallSize)
			{
				ForceSmallSize=false;
				SmallResize();
				w=ToolRect.Width()-border*2;
				h=ToolRect.Height()-border*2;
			}
		}
		font=new TFont();
		c=new TGPSCoordinate();
		Bmp->BeginUpdate();
		try
		{
			font->Name=Bmp->Font->Name;
			font->Size=Bmp->Font->Size;
			font->Style=Bmp->Font->Style;
			Bmp->Font->Name="Tahoma";
			Bmp->Font->Height=(h>>2)-(h>>4);
			h-=Bmp->Font->Height;
			str="ping";
			tw=Bmp->TextWidth(str);
			th=Bmp->TextHeight(str);
			Bmp->RenderText(x1+((w-tw)>>1)+1, y1, str, 2, text_color);
			y1+=th;
			Bmp->Font->Name="Tahoma";
			Bmp->Font->Height=(h>>1)+(h>>3);
			str=PingStr;
			tw=Bmp->TextWidth(str);
			th=Bmp->TextHeight(str);
			if(PingStr=="---") dx=0;
			else dx=5;
			Bmp->RenderText(x1+((w-tw)>>1)+1-dx, y1, str, 2, color);
			y1+=th;
			h-=Bmp->Font->Height;
			if(Manager && ((TRadarMapManager*)Manager)->Settings)
			{
				str=((TRadarMapManager*)Manager)->Settings->GeoradarTCPAddr.Trim();
				//str=str
				Bmp->Font->Height=h>>1;
				tw=Bmp->TextWidth(str);
				th=Bmp->TextHeight(str);
				Bmp->RenderText(x1+((w-tw)>>1)+1, y1, str, 2, text_color);
				y1+=th;
				if(((TRadarMapManager*)Manager)->Connection)
				{
					f=((TRadarMapManager*)Manager)->Connection->Traffic;
					str=IntToStr(((TRadarMapManager*)Manager)->Connection->RecPDataPerSecond)+" Tps";
					if(f>0)
					{
						str+=", ";
						if(f<1024) str+=IntToStr((int)f)+" bps";
						else if(f<1024*1024) str+=FloatToStrF(f/1024., ffFixed, 10, 2)+" Kbps";
						else str+=FloatToStrF(f/(1024.*1024.), ffFixed, 10, 2)+" Mbps";
					}
				}
				else str="IDLE";
				Bmp->Font->Height=h/3;
				tw=Bmp->TextWidth(str);
				th=Bmp->TextHeight(str);
				Bmp->RenderText(x1+((w-tw)>>1)+1, y1, str, 2, text_color);
			}
		}
		__finally
		{
			Bmp->Font->Name=font->Name;
			Bmp->Font->Size=font->Size;
			Bmp->Font->Style=font->Style;
			Bmp->EndUpdate();
			delete c;
			delete font;
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TToolPing::MouseClick(System::TObject* Sender, int X, int Y)
{
	if(Manager)
	{
		if(!Message)
		{
			if(LastRequestResult!=NULL && LastRequestResult.Length()>0)
			{
				Message=((TRadarMapManager*)Manager)->ShowMessageObject((TMessageObject*)Message,
					((TRadarMapManager*)Manager)->RadarMessages, TMessageType::mtCustom,
					mvtConstant, 0, LastRequestResult, (AnsiString)_RadarMapName);
				if(Message) ((TMessageObject*)Message)->Color=mtRoutine;
			}
		}
		else
		{
			((TMessageObject*)Message)->SwitchOff();
			Message=NULL;
        }
	}
}

//---------------------------------------------------------------------------
// TToolMemGraph
//---------------------------------------------------------------------------
__fastcall TToolMemGraph::TToolMemGraph(TMyObject *AOwner, TImgView32 *ImgView, TObject *AManager) :
	TVisualToolObject(AOwner, ImgView, AManager)
{
	bool v;

	fixedposition=false;//true;
	dockable=false;
	easyrendering=false;//true;
	ForceSmallSize=false;

	mouseable=!fixedposition;//false;

	toolalign=vtoaRight;//vtoaRightBottom;//vtoaRightTop;
	if(Manager && ((TRadarMapManager*)Manager)->Settings)
	{
		CloseExternalSetting=&(((TRadarMapManager*)Manager)->Settings->ShowMemGraph);
		v=((TRadarMapManager*)Manager)->Settings->ShowMemGraph;
	}
	else v=visible;

	averagepointscoef=0.05;
	text_color=Color32(100, 100, 100, 150);
	Percentage=0.;
	CurrentMem=0;
	AverageMem=0;
	MaxMemPoint=0;
	MaxAverage=0;
	LastMaxAverage=0;
	LastMaxMemPoint=0;
	timepoints=0;
	MemPoints=NULL;
	AveragePoints=NULL;
	MemPointsIndex=0;
	SmallResize();
	Timer=new TTimerThread(1000);
	Timer->MinStep=500;
	Timer->OnTimer=Refresh;
	Timer->Start();
	ShowCloseBtn=false;
	RebuildInternalRects();
	Visible=v;
}
//---------------------------------------------------------------------------

__fastcall TToolMemGraph::~TToolMemGraph()
{
	if(Timer!=NULL)
	{
		Timer->AskForTerminate();
		WaitOthers();
		if(Timer->ForceTerminate()) delete Timer;
		else Timer->FreeOnTerminate=true;
		Timer=NULL;
	}
	if(MemPoints) delete[] MemPoints;
	if(AveragePoints) delete[] AveragePoints;
}
//---------------------------------------------------------------------------

void __fastcall TToolMemGraph::SmallResize()
{
	if(ForceSmallSize || smallsize)
	{
		width=60;
		height=50;
		border=3;
	}
	else
	{
		width=120;
		height=100;
		border=5;
	}
	TimePoints=width;
	TVisualToolObject::SmallResize();
}
//---------------------------------------------------------------------------

void __fastcall TToolMemGraph::writeTimePoints(int value)
{
	unsigned long ul;

	if(timepoints!=value)
	{
		MemPoints=(unsigned long*)ChangeArraySize((DWORD*)MemPoints, value);
		AveragePoints=(long*)ChangeArraySize((DWORD*)AveragePoints, value);
		if(value<MemPointsIndex)
		{
			MemPointsIndex=value;
			ul=MemPoints[0];
			for(int i=1; i<value-1; i++)
			{
				if(ul<MemPoints[i]) ul=MemPoints[i];
			}
			MaxMemPoint=ul;
		}
		timepoints=value;
		Update();
	}
}
//---------------------------------------------------------------------------

DWORD* __fastcall TToolMemGraph::ChangeArraySize(DWORD* Array, int newN)
{
	DWORD *temp;
	int i, j;

	temp=new DWORD[newN];
	if(Array)
	{
		if(newN>timepoints) i=timepoints;
		else i=newN;
		if(MemPointsIndex>newN) j=MemPointsIndex-newN;
		else j=0;
		memcpy(temp, (void*)(Array+j), i*sizeof(DWORD));
		delete[] Array;
	}
	Array=temp;
	if(newN>MemPointsIndex) memset(Array, (newN-MemPointsIndex)*sizeof(DWORD), 0x00);

	return Array;
}
//---------------------------------------------------------------------------

void __fastcall TToolMemGraph::Refresh()
{
	unsigned long ul;
	long l;
	double v;

	if(visible)
	{
		ul=MyFiles::GetCurrentProcessSize();
		if(ul>0)
		{
			CurrentMem=(float)ul; //in MB
			if(MemPointsIndex<TimePoints)
			{
				if(MemPointsIndex==0) AverageMem=(double)CurrentMem;
				else if(MemPointsIndex<AveragePointsN)
					AverageMem=((double)AverageMem*(double)MemPointsIndex+
						(double)CurrentMem)/(double)(MemPointsIndex+1);
				else
				{
					l=labs(AveragePoints[MemPointsIndex-AveragePointsN]);
					AverageMem+=((double)CurrentMem-(double)MemPoints[MemPointsIndex-AveragePointsN])/(double)AveragePointsN;
				}
				AveragePoints[MemPointsIndex]=(long)CurrentMem-(long)AverageMem;
				//if(MaxAverage<labs(AveragePoints[MemPointsIndex])) MaxAverage=labs(AveragePoints[MemPointsIndex]);
				if(MaxAverage<AveragePoints[MemPointsIndex]) MaxAverage=AveragePoints[MemPointsIndex];
				//MemPoints
				MemPoints[MemPointsIndex++]=CurrentMem;
				if(MaxMemPoint<CurrentMem) MaxMemPoint=CurrentMem;
			}
			else
			{
				//l=labs(AveragePoints[0]);
				l=AveragePoints[0];
				AverageMem+=((double)CurrentMem-(double)MemPoints[TimePoints-AveragePointsN])/(double)AveragePointsN;
				memmove(AveragePoints, (void*)(AveragePoints+1), (TimePoints-1)*sizeof(long));
				AveragePoints[TimePoints-1]=(long)CurrentMem-(long)AverageMem;
				//if(MaxAverage<labs(AveragePoints[TimePoints-1])) MaxAverage=labs(AveragePoints[TimePoints-1]);
				if(MaxAverage<AveragePoints[TimePoints-1]) MaxAverage=AveragePoints[TimePoints-1];
				else
				{
					if(l==MaxAverage)
					{
						//l=labs(AveragePoints[0]);
						l=AveragePoints[0];
						for(int i=1; i<TimePoints-1; i++)
						{
							//if(l<labs(AveragePoints[i]))
							if(l<AveragePoints[i])
							{
								//l=labs(AveragePoints[i]);
								l=AveragePoints[i];
								if(l==MaxAverage) break;
							}
						}
						MaxAverage=l;
					}
				}
				ul=MemPoints[0];
				memmove(MemPoints, (void*)(MemPoints+1), (TimePoints-1)*sizeof(unsigned long));
				MemPoints[TimePoints-1]=CurrentMem;
				if(MaxMemPoint<CurrentMem) MaxMemPoint=CurrentMem;
				else
				{
					if(ul==MaxMemPoint)
					{
						ul=MemPoints[0];
						for(int i=1; i<TimePoints-1; i++)
						{
							if(ul<MemPoints[i])
							{
								ul=MemPoints[i];
								if(ul==MaxMemPoint) break;
							}
						}
						MaxMemPoint=ul;
					}
				}
			}
			/*if(MemPointsIndex<TimePoints)
			{
				if(MemPointsIndex>0)
					AverageMem=((double)AverageMem*(double)MemPointsIndex+
						(double)CurrentMem)/(double)(MemPointsIndex+1);
				else AverageMem=(double)CurrentMem;
				AveragePoints[MemPointsIndex]=(long)CurrentMem-(long)AverageMem;
				if(MaxAverage<labs(AveragePoints[MemPointsIndex])) MaxAverage=labs(AveragePoints[MemPointsIndex]);
				MemPoints[MemPointsIndex++]=CurrentMem;
				if(MaxMemPoint<CurrentMem) MaxMemPoint=CurrentMem;
			}
			else
			{
				l=labs(AveragePoints[0]);
				AverageMem+=((double)CurrentMem-(double)MemPoints[0])/(double)TimePoints;
				memmove(AveragePoints, (void*)(AveragePoints+1), (TimePoints-1)*sizeof(long));
				AveragePoints[TimePoints-1]=(long)CurrentMem-(long)AverageMem;
				if(MaxAverage<labs(AveragePoints[TimePoints-1])) MaxAverage=labs(AveragePoints[TimePoints-1]);
				else
				{
					if(l==MaxAverage)
					{
                        l=labs(AveragePoints[0]);
						for(int i=1; i<TimePoints-1; i++)
						{
							if(l<labs(AveragePoints[i]))
							{
								l=labs(AveragePoints[i]);
								if(l==MaxAverage) break;
							}
						}
						MaxAverage=l;
					}
				}
				ul=MemPoints[0];
				memmove(MemPoints, (void*)(MemPoints+1), (TimePoints-1)*sizeof(unsigned long));
				MemPoints[TimePoints-1]=CurrentMem;
				if(MaxMemPoint<CurrentMem) MaxMemPoint=CurrentMem;
				else
				{
					if(ul==MaxMemPoint)
					{
						ul=MemPoints[0];
						for(int i=1; i<TimePoints-1; i++)
						{
							if(ul<MemPoints[i])
							{
								ul=MemPoints[i];
								if(ul==MaxMemPoint) break;
							}
						}
						MaxMemPoint=ul;
					}
				}
			}*/
			Percentage=100.*(1.-(float)CurrentMem/(float)CrashMemoryLevel);
			if(Percentage<0.) Percentage=0.;
		}
		else
		{
			CurrentMem=0;
			Percentage=0.;
		}
		Update();
	}
}
//---------------------------------------------------------------------------

void __fastcall TToolMemGraph::Redraw(TObject *Sender, TBitmap32 *Bmp)
{
	Types::TRect vpr;
	int x1, y1, w, h, tw, th, pw, ph, px1, py1, j;
	AnsiString str;
	TFont *font;
	TColor32 color, borderc;
	System::Byte A, R, G;
	double cm;
	float f;
	TPolygon32 *poly, *poly2;
	TColor32Entry ce;

	if(Bmp && image)
	{
		vpr=image->GetViewportRect();
		x1=ToolRect.left+border;
		y1=ToolRect.top+border;
		w=ToolRect.Width()-border*2;
		h=ToolRect.Height()-border*2;
		if(!smallsize)
		{
			if(w+w*ForceSmallSize>vpr.Width()/3)// || h>vpr.Height()/3)
			{
				if(!ForceSmallSize)
				{
					ForceSmallSize=true;
					SmallResize();
					w=ToolRect.Width()-border*2;
					h=ToolRect.Height()-border*2;
				}
			}
			else if(ForceSmallSize)
			{
				ForceSmallSize=false;
				SmallResize();
				w=ToolRect.Width()-border*2;
				h=ToolRect.Height()-border*2;
			}
		}
		font=new TFont();
		Bmp->BeginUpdate();
		try
		{
			if(CurrentMem>0)
			{
				if(CacheManager)
				{
					if(CurrentMem<CacheManager->MemoryLevelToStartCache)
					{
						A=150;
						G=200;
						R=(int)(200.*((float)CurrentMem/(float)CacheManager->MemoryLevelToStartCache));
					}
					else if(CurrentMem<CacheManager->CriticalMemoryLevel)
					{
						f=(float)(CurrentMem-CacheManager->MemoryLevelToStartCache)/
							(float)(CacheManager->CriticalMemoryLevel-CacheManager->MemoryLevelToStartCache);
						A=150+(int)(105.*f);
						R=200;
						G=200-(int)(100.*f);
					}
					else
					{
						A=255;
						R=200;
						G=100-(int)(100.*((float)(CurrentMem-CacheManager->CriticalMemoryLevel)/
							(float)(CrashMemoryLevel-CacheManager->CriticalMemoryLevel)));
					}
				}
				else
				{
					f=(float)CurrentMem/(float)CrashMemoryLevel;
					A=100+(int)(155.*f);
					R=(int)(200.*f);
					G=200-(int)(200.*f);
				}
			}
			else
			{
				str="---";
				R=200; G=0; A=100;
			}
			color=Color32(R, G, 0, A);
			borderc=Color32(R>>1, G>>1, 0, A>>1);
			if(!easyrendering)
			{
				poly=new TPolygon32();
				poly2=new TPolygon32();
				try
				{
					poly->Antialiased=true;
					poly->Closed=true;
					poly2->Antialiased=true;
					poly2->Closed=false;
					pw=ToolRect.Width();
					ph=ToolRect.Height();
					px1=ToolRect.left;
					py1=ToolRect.top;
					poly->Add(FixedPoint(px1+pw, py1+ph));
					if(LastMaxMemPoint>MaxMemPoint) LastMaxMemPoint-=(LastMaxMemPoint-MaxMemPoint)>>2;
					else LastMaxMemPoint=MaxMemPoint;
					if(LastMaxAverage>MaxAverage) LastMaxAverage-=(LastMaxAverage-MaxAverage)>>3;
					else LastMaxAverage=MaxAverage;
					for(int i=0; i<pw; i++)
					{
						j=MemPointsIndex-1-i;
						if(j>=0)
						{
							if(LastMaxMemPoint>0)
							{
								cm=(double)ph*(1.-(double)MemPoints[j]/(double)LastMaxMemPoint);
								poly->Add(FixedPoint(px1+pw-i, py1+(int)cm));
							}
							if(LastMaxAverage>0)
							{
								if(labs(AveragePoints[j])>MaxAverage) cm=MaxAverage*(labs(AveragePoints[j])/AveragePoints[j]);
								else cm=AveragePoints[j];
								cm=(double)ph*(0.5-0.5*cm/(double)LastMaxAverage);
								if(cm<0.) cm=0.;
								else if(cm>ph) cm=ph;
								poly2->Add(FixedPoint(px1+pw-i, py1+(int)cm));
							}
						}
						else
						{
							if(i>0)
							{
								poly->Add(FixedPoint(px1+pw-i-1, py1+ph));
								poly2->Add(FixedPoint(px1+pw-i-1, py1+(ph>>1)));
								poly2->Add(FixedPoint(px1, py1+(ph>>1)));
							}
							break;
						}
					}
					poly->Add(FixedPoint(px1, py1+ph));
					ce.ARGB=color; ce.A=ce.A>>2;
					poly->Draw(Bmp, borderc, ce.ARGB);
					A=ce.A;
					ce.ARGB=clBlue32; ce.A=A;
					poly2->DrawEdge(Bmp, ce.ARGB);
				}
				__finally
				{
					poly->Clear();
					delete poly;
					poly2->Clear();
					delete poly2;
				}
            }
			font->Name=Bmp->Font->Name;
			font->Size=Bmp->Font->Size;
			font->Style=Bmp->Font->Style;
			Bmp->Font->Name="Tahoma";
			Bmp->Font->Height=(h>>2)-(h>>4);
			h-=Bmp->Font->Height;
			str="memory";
			tw=Bmp->TextWidth(str);
			th=Bmp->TextHeight(str);
			Bmp->RenderText(x1+((w-tw)>>1)+1, y1, str, 2, text_color);
			y1+=th;
			Bmp->Font->Height=(h>>1)+(h>>3);
			str=FloatToStrF(Percentage, ffFixed, 4, 1)+"%";
			tw=Bmp->TextWidth(str);
			th=Bmp->TextHeight(str);
			Bmp->RenderText(x1+((w-tw)>>1)+1, y1, str, 2, color);
			y1+=th;
			h-=Bmp->Font->Height;
			if(CurrentMem>0)
			{
				cm=(double)CurrentMem/(1024.*1024.);
				if(cm<1000) str=FloatToStrF(cm, ffFixed, 10, 2)+" MB";
				else str=FloatToStrF(cm/1024., ffFixed, 10, 2)+" GB";
			}
			else str="UNKNOWN";
			Bmp->Font->Height=h>>1;
			tw=Bmp->TextWidth(str);
			th=Bmp->TextHeight(str);
			ce.ARGB=text_color; ce.A+=50;
			Bmp->RenderText(x1+((w-tw)>>1)+1, y1, str, 2, ce.ARGB);
		}
		__finally
		{
			Bmp->Font->Name=font->Name;
			Bmp->Font->Size=font->Size;
			Bmp->Font->Style=font->Style;
			Bmp->EndUpdate();
			delete font;
		}
	}
}

//---------------------------------------------------------------------------
// TToolProgressBar
//---------------------------------------------------------------------------
__fastcall __fastcall TToolProgressBar::TToolProgressBar(TMyObject *AOwner, TImgView32 *ImgView,
	TObject *AManager) : TVisualToolObject(AOwner, ImgView, AManager)
{
	visible=false;
	mouseable=false;
	toolalign=vtoaCenter;
	style=tpbsWindMill;
	RebuildInternalRects();
	users=max=min=0;
	pos=0;
	step=1;
	cancelcnt=lockcnt=0;
	//endless=false;
	color=Color32(190, 255, 0);
	textcolor=clRed32;
	min_size=64;
	width=min_size;
	height=min_size;
	RebuildInternalRects();
	windmillbmp=NULL;
	if(image)
	{
		Layer=new TBitmapLayer(image->Layers);
		Layer->OnPaint=&LayerPaint;
		Layer->Bitmap->DrawMode=dmBlend;
		Layer->BringToFront();
	}
	LastDrawedTick=::GetTickCount();
	fixedposition=true;
	dockable=false;
	ShowCloseBtn=false;
	GetSizedGlyph();
}
//---------------------------------------------------------------------------

_fastcall TToolProgressBar::~TToolProgressBar()
{
	if(Layer)
	{
		try
		{
			if(image && image->Layers) image->Layers->Delete(Layer->Index);
			else delete Layer;
		}
		catch(...) {}
		Layer=NULL;
	}
	delete windmillbmp;
}
//---------------------------------------------------------------------------

void __fastcall TToolProgressBar::writeColor(TColor32 value)
{
	if(color!=value)
	{
		color=value;
		ApplyColorMask(windmillbmp, color, textcolor);
	}
}
//---------------------------------------------------------------------------

void __fastcall TToolProgressBar::GetSizedGlyph()
{
	TBitmap32 *tmp;
	int tw, ts;

	if(!windmillbmp || windmillbmp->Width!=min_size)
	{
		if(windmillbmp) delete windmillbmp;
		windmillbmp=new TBitmap32();
		try
		{
			if(min_size<=16) tmp=LoadPNG32Resource((System::UnicodeString)"PNGIMAGE_282");
			else if(min_size<=32) tmp=LoadPNG32Resource((System::UnicodeString)"PNGIMAGE_281");
			else if(min_size<=64) tmp=LoadPNG32Resource((System::UnicodeString)"PNGIMAGE_280");
			else if(min_size<=128) tmp=LoadPNG32Resource((System::UnicodeString)"PNGIMAGE_279");
			else tmp=LoadPNG32Resource((System::UnicodeString)"PNGIMAGE_278");
			if(tmp->Width!=min_size)
				ScaleBitmap32(tmp, windmillbmp, (float)min_size/(float)tmp->Width,
					(float)min_size/(float)tmp->Height, false);
			else windmillbmp->Assign(tmp);
		}
		__finally
		{
			delete tmp;
		}
		ApplyColorMask(windmillbmp, color, textcolor);
		if(min_size<40) CancelStr="X";
		else CancelStr="Cancel";
		windmillbmp->Font->Name="Tahoma";
		windmillbmp->Font->Size=9;
		windmillbmp->Font->Style=TFontStyles();
		tw=windmillbmp->TextWidth(CancelStr);
		ts=(int)((float)min_size*0.625);
		if(tw<ts)
		{
			while(tw<ts && windmillbmp->Font->Size<14)
			{
				windmillbmp->Font->Size++;
				tw=windmillbmp->TextWidth(CancelStr);
			}
			windmillbmp->Font->Size--;
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TToolProgressBar::writeWidth(int value)
{
	width=value;
	if(width<height) min_size=width;
	else min_size=height;
	RebuildInternalRects();
	GetSizedGlyph();
	Update();
}
//---------------------------------------------------------------------------

void __fastcall TToolProgressBar::writeHeight(int value)
{
	height=value;
    if(width<height) min_size=width;
	else min_size=height;
	GetSizedGlyph();
	RebuildInternalRects();
	Update();
}
//---------------------------------------------------------------------------
void __fastcall TToolProgressBar::Show(int StepsCount, bool EnableCancelButton)
{
	if(EnableCancelButton)
	{
		cancelcnt++;
		if(Owner) lockcnt++;
	}
	mouseable=(cancelcnt>0);
	if(Owner) ((TGraphicObjectsContainer*)Owner)->LockControls=(lockcnt>0);
	if(users==0)
	{
		pos=0;
		max=StepsCount;
		if(cancelcnt>1) cancelcnt=1;
		if(lockcnt>1) lockcnt=1;
		if(Layer) Layer->BringToFront();
		RebuildInternalRects();
		if(Manager && ((TRadarMapManager*)Manager)->GlobalStopItEvent)
			ResetEvent(((TRadarMapManager*)Manager)->GlobalStopItEvent);
		Visible=true;
	}
	else
	{
		max+=StepsCount;
		Update();
	}
	users++;
}
//---------------------------------------------------------------------------
void __fastcall TToolProgressBar::Hide(bool DisableCancelButton)
{
	if(DisableCancelButton)
	{
		if(cancelcnt>0) cancelcnt--;
		if(Owner && lockcnt>0) lockcnt--;
	}
	mouseable=(cancelcnt>0);
	if(Owner) ((TGraphicObjectsContainer*)Owner)->LockControls=(lockcnt>0);
	users--;
	if(users==0)
	{
		Visible=false;
		if(Owner) ((TGraphicObjectsContainer*)Owner)->LockControls=false;
		mouseable=false;
		cancelcnt=0;
		lockcnt=0;
		if(Manager && ((TRadarMapManager*)Manager)->GlobalStopItEvent)
			ResetEvent(((TRadarMapManager*)Manager)->GlobalStopItEvent);
	}
	else Update();
}
//---------------------------------------------------------------------------

void __fastcall TToolProgressBar::Update()
{
	float r;
	unsigned long ul;

	if(Layer)
	{
		ul=::GetTickCount();
		if(ul-LastDrawedTick>40)
		{
			Layer->Update();
			if(ul-LastDrawedTick>66)
			{
				//LastDrawedTick=::GetTickCount();
				ProcessMessages();
			}
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TToolProgressBar::Redraw(TObject *Sender, TBitmap32 *Bmp)
{
	TBitmap32 *tmp;
	float r;
	int tw, th;

	if(Bmp && windmillbmp && width>0 && height>0)
	{
		Bmp->BeginUpdate();
		try
		{
			tmp=new TBitmap32();
			tmp->DrawMode=dmBlend;
			tmp->BeginUpdate();
			if((max-min)==0) r=0;
			else r=(float)(pos-min)/(float)(max-min)*M_PI2;
			RotateBitmap32(windmillbmp, tmp, r, false);
			if(mouseable)
			{
				tmp->Font->Name=windmillbmp->Font->Name;
				tmp->Font->Size=windmillbmp->Font->Size;
				tmp->Font->Style=windmillbmp->Font->Style;
				tmp->Font->Color=(TColor)RGB(128, 128, 128);
				tw=tmp->TextWidth(CancelStr);
				th=tmp->TextHeight(CancelStr);
				tmp->RenderText((tmp->Width-tw)>>1, (tmp->Height-th)>>1, CancelStr, 2, textcolor);
			}
			tmp->EndUpdate();
			Bmp->Draw(ToolRect.Left, ToolRect.top, tmp);
		}
		__finally
		{
			delete tmp;
			Bmp->EndUpdate();
		}
	}
	LastDrawedTick=::GetTickCount();
}
//---------------------------------------------------------------------------

void __fastcall TToolProgressBar::MouseClick(System::TObject* Sender, int X, int Y)
{
	if(Cancelable && Manager && ((TRadarMapManager*)Manager)->GlobalStopItEvent)
		SetEvent(((TRadarMapManager*)Manager)->GlobalStopItEvent);
}
//---------------------------------------------------------------------------

bool __fastcall TToolProgressBar::readCancelPressed()
{
	if(Cancelable && Manager && ((TRadarMapManager*)Manager)->GlobalStopItEvent &&
		WaitForSingleObject(((TRadarMapManager*)Manager)->GlobalStopItEvent, 0)==WAIT_OBJECT_0)
		return true;
	else return false;
}
//---------------------------------------------------------------------------
