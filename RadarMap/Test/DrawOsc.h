//---------------------------------------------------------------------------

#ifndef DrawOscH
#define DrawOscH
//---------------------------------------------------------------------------
#include <Classes.hpp>
//---------------------------------------------------------------------------
class TDrawOscThrd : public TThread
{
private:
protected:
        void __fastcall Execute();
public:
        __fastcall TDrawOscThrd(bool CreateSuspended);
        void __fastcall SetS1();
};
//---------------------------------------------------------------------------
#endif
