//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Layers.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "ImageButton"
#pragma link "G32_ProgressBar"
#pragma link "GR32_Image"
#pragma resource "*.dfm"
TLayersForm *LayersForm;

//---------------------------------------------------------------------------
__fastcall TLayersForm::TLayersForm(TComponent* Owner)
	: TForm(Owner)
{
	TitleDown=false;
	TVMousePressed=false;
	TVY=-1;
	TVp=0;
	List=new TList;
	treeview=new TCheckBoxTreeView(PagesPanel);
	TreeView->Left=TreeView1->Left;
	TreeView->Top=TreeView1->Top;
	TreeView->Width=TreeView1->Width;
	TreeView->Height=TreeView1->Height;
	TreeView->Color=TreeView1->Color;
	TreeView->Visible=true;
	TreeView->ShowButtons=true;
	TreeView->ShowLines=true;
	TreeView->ShowRoot=false;
	TreeView->ShowHint=true;
	TreeView->BorderStyle=bsSingle;
	TreeView->BorderWidth=1;
	TreeView->Ctl3D=false;
	TreeView->Images=ImageList48;
	TreeView->OnChange=TreeViewChange;
	TreeView->OnMouseDown=TreeViewMouseDown;
	TreeView->OnMouseUp=TreeViewMouseUp;
	TreeView->OnMouseMove=TreeViewMouseMove;
}
//---------------------------------------------------------------------------

void __fastcall TLayersForm::CloseIButtonClick(TObject *Sender)
{
	ModalResult=mrCancel;
	Close();
}
//---------------------------------------------------------------------------

void __fastcall TLayersForm::OkImageButtonClick(TObject *Sender)
{
	ModalResult=mrOk;
	//Close();
}
//---------------------------------------------------------------------------

void __fastcall TLayersForm::FormHide(TObject *Sender)
{
	TCloseAction b;

	FormClose(Sender, b);
}
//---------------------------------------------------------------------------

void __fastcall TLayersForm::TitlePanelMouseDown(TObject *Sender, TMouseButton Button,
		  TShiftState Shift, int X, int Y)
{
	TitleDown=true;
	TitleXY=ClientToScreen(Types::TPoint(X, Y));
}
//---------------------------------------------------------------------------

void __fastcall TLayersForm::TitlePanelMouseMove(TObject *Sender, TShiftState Shift,
		  int X, int Y)
{
	Types::TPoint tmp;

	if(TitleDown)
	{
		tmp=ClientToScreen(Types::TPoint(X, Y));
		Left+=tmp.x-TitleXY.x;
		Top+=tmp.y-TitleXY.y;
		TitleXY=tmp;
	}
}
//---------------------------------------------------------------------------

void __fastcall TLayersForm::TitlePanelMouseUp(TObject *Sender, TMouseButton Button,
		  TShiftState Shift, int X, int Y)
{
	TitleDown=false;
}
//---------------------------------------------------------------------------

void __fastcall TLayersForm::AnnButtonClick(TObject *Sender)
{
	TComponent* sender=(TComponent*)Sender;

	if(sender && TreeView->Selected && TreeView->Selected->ImageIndex>0 &&
		TreeView->Selected->ImageIndex!=sender->Tag)
	{
		TreeView->Selected->ImageIndex=sender->Tag;
		TreeView->Selected->SelectedIndex=sender->Tag;
		TreeViewChange(TreeView, TreeView->Selected);
	}
}
//---------------------------------------------------------------------------

void __fastcall TLayersForm::TreeViewChange(TObject *Sender, TTreeNode *Node)
{
	TopoButton->Enabled=(Node && Node->Data && Node->ImageIndex>0);
	UtilButton->Enabled=TopoButton->Enabled;
	AnnButton->Enabled=TopoButton->Enabled;
	if(TopoButton->Enabled)
	{
		TImageButton* btn;
		switch(Node->ImageIndex)
		{
			case 1: btn=TopoButton; break;
			case 2: btn=UtilButton; break;
			case 3: btn=AnnButton; break;
			default: btn=NULL;
		}
		if(btn!=TopoButton) TopoButton->Down=false;
		if(btn!=UtilButton) UtilButton->Down=false;
		if(btn!=AnnButton) AnnButton->Down=false;
		if(btn) btn->Down=true;
	}
	else
	{
		TopoButton->Down=false;
		UtilButton->Down=false;
		AnnButton->Down=false;
	}
}
//---------------------------------------------------------------------------

void __fastcall TLayersForm::FormClose(TObject *Sender, TCloseAction &Action)
{
//
}
//---------------------------------------------------------------------------

void __fastcall TLayersForm::TreeViewMouseDown(TObject *Sender, TMouseButton Button,
		  TShiftState Shift, int X, int Y)
{
	if(Sender)
	{
		TVMousePressed=true;
		TVY=Y;
		TVp=GetScrollPos(((TTreeView*)Sender)->Handle, SB_VERT);
	}
}
//---------------------------------------------------------------------------

void __fastcall TLayersForm::TreeViewMouseMove(TObject *Sender, TShiftState Shift,
		  int X, int Y)
{
	int p, h;
	SCROLLINFO si;

	if(Sender && TVMousePressed && TVY>=0)
	{
		if(Shift.Contains(ssLeft))
		{
			//b&=GetScrollRange(((TTreeView*)Sender)->Handle, SB_VERT, &pmin, &pmax);
			ZeroMemory(&si, sizeof(si));
			si.cbSize=sizeof(si);
			si.fMask=SIF_ALL;
			if(GetScrollInfo(((TTreeView*)Sender)->Handle, SB_VERT, &si) && (UINT)(si.nMax-si.nMin)>si.nPage)
			{
				p=TVp+(int)((float)(si.nPage)*(-(float)(Y-TVY)/(float)(((TTreeView*)Sender)->Height)));
				if(p<si.nMin) p=si.nMin;
				else if(p>si.nMax) p=si.nMax;
				((TTreeView*)Sender)->Perform(WM_VSCROLL, MAKEWPARAM(SB_THUMBPOSITION, (WORD)p), NULL);
			}
		}
		else
		{
            TVMousePressed=false;
			TVY=-1;
			TVp=0;
        }
	}
}
//---------------------------------------------------------------------------

void __fastcall TLayersForm::TreeViewMouseUp(TObject *Sender, TMouseButton Button,
		  TShiftState Shift, int X, int Y)
{
	if(Sender)
	{
		TVMousePressed=false;
		TVY=-1;
		TVp=0;
	}
}
//---------------------------------------------------------------------------

