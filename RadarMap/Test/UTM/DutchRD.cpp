//---------------------------------------------------------------------------


#pragma hdrstop

#include "DutchRD.h"
#include <vcl.h>

//---------------------------------------------------------------------------

#pragma package(smart_init)

/*
 * converts a RD coordinate into a WGS84 coordinate. Approximate
 * transformation has a 50 cm accuracy.
 *
 * @param X, Y
 *            RD coordinate.
 * @return Lat, Lon coordinate.
 */
void __fastcall DutchRD::RDtoLL(double X, double Y, double &Lat, double &Lon)
{
	double Lambda, Phi;

	try
	{
		RDtoBessel(X, Y, Lambda, Phi);
		BesselToLL(Lambda, Phi, Lat, Lon);
	}
	catch (Exception &e) {}
}

/*
 * converts a WGS84 coordinate into a RD coordinate.
 *
 * @param wgsCoordinate
 *            WGS84 coordinate.
 * @return RD coordinate.
 */
void __fastcall DutchRD::LLtoRD(double Lat, double Lon, double &X, double &Y)
{
	double Lambda, Phi;

	try
	{
		LLtoBessel(Lat, Lon, Lambda, Phi);
		BesselToRD(Lambda, Phi, X, Y);
	}
	catch (Exception &e) {}
}

// ------------------------------------------------------------------------
// input is x,y in RD output is phi,lambda on the Bessel ellipsoid
// ------------------------------------------------------------------------
void __fastcall DutchRD::RDtoBessel(double X, double Y, double &Lambda, double &Phi)
{
	double d__1, d__2;
	double cpsi, spsi, phiprime, b;
	int i;
	double q, r, w, ca, cb, dl, sa, sb;
	double dq, sdl, psi, tt;

	try
	{
		d__1 = X - x0;
		d__2 = Y - y0;
		if(d__1 * d__1 + d__2 * d__2>0)
			r = sqrt(d__1 * d__1 + d__2 * d__2);
		else r =0;
		if (r != 0.)
		{
			sa = (X - x0) / r;
			ca = (Y - y0) / r;
		}
		else
		{
			sa = 0.;
			ca = 0.;
		}
		psi = atan2(r, k * 2. * bigr) * 2.;
		cpsi = cos(psi);
		spsi = sin(psi);
		sb = ca * cos(b0) * spsi + sin(b0) * cpsi;
		d__1 = sb;
		if((d__1 * d__1)<1)
			cb = sqrt(1. - d__1 * d__1);
		else cb=1;
		b = acos(cb);
		sdl = sa * spsi / cb;
		dl = asin(sdl);
		Lambda = dl / n + lambda0;
		tt=tan(b / 2. + M_PI / 4.);
		if(tt>0)
		{
			w = log(tt);
			q = (w - m) / n;
			phiprime = atan(exp(q)) * 2. - M_PI / 2.;
			for (i = 1; i <= 4; ++i)
			{
				tt=(e * sin(phiprime) + 1.) / (1. - e * sin(phiprime));
				if(tt>0)
				{
					dq = e / 2.	* log(tt);
					Phi = atan(exp(q + dq)) * 2. - M_PI / 2.;
					phiprime = Phi;
				}
			}

			Lambda = Lambda / M_PI * 180.;
			Phi = Phi / M_PI * 180.;
		}
	}
	catch(Exception &e) {}
}

// ------------------------------------------------------------------------
// input is phi,lambda on the Bessel ellipsoid, output is x,y in RD
// ------------------------------------------------------------------------
void __fastcall DutchRD::BesselToRD(double Lambda, double Phi, double &X, double &Y)
{
	double d__1, d__2;
	double cpsi, cpsihalf, spsi, spsihalf, tpsihalf, b, s2psihalf, q, r, w, ca, lambda, dl, sa, dq, qprime, phi, tt;

	try
	{
		phi = Phi / 180. * M_PI;
		lambda = Lambda / 180. * M_PI;
		tt=tan(phi / 2. + M_PI / 4.);
		if(tt>0)
		{
			qprime = log(tt);
			tt=(e * sin(phi) + 1.)/(1. - e * sin(phi));
			if(tt>0)
			{
				dq = e / 2.	* log(tt);
				q = qprime - dq;
				w = n * q + m;
				b = atan(exp(w)) * 2. - M_PI / 2.;
				dl = n * (lambda - lambda0);
				d__1 = sin((b - b0) / 2.);
				d__2 = sin(dl / 2.);
				s2psihalf = d__1 * d__1 + d__2 * d__2 * cos(b) * cos(b0);
				if(s2psihalf<1)
					cpsihalf = sqrt(1. - s2psihalf);
				else cpsihalf=1;
				if(s2psihalf>0)
					spsihalf = sqrt(s2psihalf);
				else spsihalf=1;
				tpsihalf = spsihalf / cpsihalf;
				spsi = spsihalf * 2. * cpsihalf;
				cpsi = 1. - s2psihalf * 2.;
				sa = sin(dl) * cos(b) / spsi;
				ca = (sin(b) - sin(b0) * cpsi) / (cos(b0) * spsi);
				r = k * 2. * bigr * tpsihalf;
				X = r * sa + x0;
				Y = r * ca + y0;
			}
		}
	}
	catch(Exception &e) {}
}

void __fastcall DutchRD::BesselToLL(double Lambda, double Phi, double &Lat, double &Lon)
{
	double dlam, dphi, lamcor, phicor;

	try
	{
		dphi = Phi - 52.;
		dlam = Lambda - 5.;
		phicor = (-96.862 - dphi * 11.714 - dlam * 0.125) * 1e-5;
		lamcor = (dphi * 0.329 - 37.902 - dlam * 14.667) * 1e-5;
		Lat = Phi + phicor;
		Lon = Lambda + lamcor;
	}
	catch(Exception &e) {}
}

/**
 * converts a WGS84 coordinate into a bessel coordinate.
 *
 * @param wgsCoordinate
 *            A WGS84 coordinate.
 * @return A Bessel coordinate.
 */
void __fastcall DutchRD::LLtoBessel(double Lat, double Lon, double &Lambda, double &Phi)
{
	double dlam, dphi, lamcor, phicor;

	try
	{
		dphi = Lat - 52.;
		dlam = Lon - 5.;
		phicor = (-96.862 - dphi * 11.714 - dlam * 0.125) * 1e-5;
		lamcor = (dphi * 0.329 - 37.902 - dlam * 14.667) * 1e-5;
		Phi = Lat - phicor;
		Lambda = Lon - lamcor;
	}
	catch(Exception &e) {}
}

/*void __fastcall RDtoLL(double x, double y, double &Lat, double &Lon)
{
	double dX, dY, SomN, SomE;

	dX=(x-155000)*pow(10, -5);
	dY=(y-463000)*pow(10, -5);
	SomN = (3235.65389 * dY) + (-32.58297 * pow(dX, 2)) + (-0.2475 * pow(dY, 2)) +
		(-0.84978 * pow(dX, 2) * dY) + (-0.0655 * pow(dY, 3)) + (-0.01709 * pow(dX, 2) * pow(dY, 2)) +
		(-0.00738 *	dX) + (0.0053 * pow(dX, 4)) + (-0.00039 * pow(dX, 2) * pow(dY, 3)) +
		(0.00033 * pow(dX, 4) * dY) + (-0.00012 * dX * dY);
	SomE = (5260.52916 * dX) + (105.94684 * dX * dY) + (2.45656 * dX * pow(dY, 2)) +
		(-0.81885 * pow(dX, 3)) + (0.05594 * dX * pow(dY, 3)) + (-0.05607 * pow(dX, 3) * dY) +
		(0.01199 * dY) + (-0.00256 * pow(dX, 3) * pow(dY, 2)) + (0.00128 * dX * pow(dY, 4)) +
		(0.00022 * pow(dY, 2)) + (-0.00022 * pow(dX, 2)) + (0.00026 * pow(dX, 5));
	Lat = 52.15517 + (SomN / 3600);
	Lon = 5.387206 + (SomE / 3600);
}
//---------------------------------------------------------------------------*/
