//---------------------------------------------------------------------------

#ifndef DutchRDH
#define DutchRDH

#include <math.h>
//---------------------------------------------------------------------------

namespace DutchRD
{
/*
 * The Rd2WgsConverter is able to convert RD (Rijksdriehoekscordinaten)
 * coordinates into WGS84 (World Geodetic System 1984) coordinates and visa
 * versa.
 *
 * This implementation is based on an implementation written in C by E. Schrama.
 * The origional C code can be found at:
 * ftp://ocean.lr.tudelft.nl/ejo/rd2wgs/rd2wgs.tar
 *
 * @author Rob Vermeulen (rob.vermeulen@gmail.com)
 */
	const double x0 = 1.55e5;
	const double y0 = 4.63e5;
	const double k = 0.9999079;
	const double bigr = 6382644.571;
	const double m = 0.003773953832;
	const double n = 1.00047585668;
	const double lambda0 = M_PI * 0.029931327161111111;
	const double b0 = M_PI * 0.28956165138333334;
	const double e = 0.08169683122;

	/*
	 * converts a RD coordinate into a WGS84 coordinate. Approximate
	 * transformation has a 50 cm accuracy.
	 *
	 * @param X, Y
	 *            RD coordinate.
	 * @return Lat, Lon coordinate.
	 */
	void __fastcall RDtoLL(double X, double Y, double &Lat, double &Lon);
	/*
	 * converts a WGS84 coordinate into a RD coordinate.
	 *
	 * @param wgsCoordinate
	 *            WGS84 coordinate.
	 * @return RD coordinate.
	 */
	void __fastcall LLtoRD(double Lat, double Lon, double &X, double &Y);
	// ------------------------------------------------------------------------
	// input is x,y in RD output is phi,lambda on the Bessel ellipsoid
	// ------------------------------------------------------------------------
	void __fastcall RDtoBessel(double X, double Y, double &Lambda, double &Phi);
	// ------------------------------------------------------------------------
	// input is phi,lambda on the Bessel ellipsoid, output is x,y in RD
	// ------------------------------------------------------------------------
	void __fastcall BesselToRD(double Lambda, double Phi, double &X, double &Y);
	// ------------------------------------------------------------------------
	// input is phi,lambda on the Bessel ellipsoid, output is Lat, Lon in WGS84
	// ------------------------------------------------------------------------
	void __fastcall BesselToLL(double Lambda, double Phi, double &Lat, double &Lon);
	/**
	 * converts a WGS84 coordinate into a bessel coordinate.
	 *
	 * @param wgsCoordinate
	 *            A WGS84 coordinate.
	 * @return A Bessel coordinate.
	 */
	void __fastcall LLtoBessel(double Lat, double Lon, double &Lambda, double &Phi);
}
#endif
