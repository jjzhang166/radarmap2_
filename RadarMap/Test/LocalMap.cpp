//---------------------------------------------------------------------------

#include <vcl.h>
#include <math.h>
#include <algorithm>
using std::min;
using std::max;
#include <gdiplus.h>
#pragma hdrstop

#include "MapLabelGeometry.h" //#include "LocalMap.h"
#include <pngimage.hpp>
#include <jpeg.hpp>
#include "MyMath.h"
#include "Manager.h"
#include "LabelForm.h"
#include "InetMapBase.h"
#include "DbDXFMapsContainer.h"
#include "BitmapGDI.h"
#include "GainForm.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)

//---------------------------------------------------------------------------
// TMapLabelType
//---------------------------------------------------------------------------
TMapLabelType StrToTMapLabelType(AnsiString str)
{
	int n=sizeof(TMapLabelTypeStrings);
	TMapLabelType res=mltNone;

	for(int i=0; i<n; i++)
	{
		if(str==TMapLabelTypeStrings[i])
		{
			res=(TMapLabelType)i;
			break;
		}
	}
	return res;
}

//---------------------------------------------------------------------------
// THandObject
//---------------------------------------------------------------------------
void __fastcall THandObject::MouseMove(System::TObject* Sender, Classes::TShiftState Shift, int X, int Y, TCustomLayer *Layer)
{
	int dx, dy;

	if(Pressed && image!=NULL && !locked && Owner && ((TMapObjectsContainer*)Owner)->Container)
	{
		locked=true;
		try
		{
			dx=X-X1;
			dy=Y-Y1;
			X1=X;
			Y1=Y;
			if(Manager && ((TRadarMapManager*)Manager)->TrackingPath &&
				((TRadarMapManager*)Manager)->TrackingPath->FollowMe && ((TMapObjectsContainer*)Owner)->Messages)
			{
				((TMapObjectsContainer*)Owner)->Messages->AddMessage(TMessageType::mtWarning,
					"You can't move map while 'Follow Me' function is switched ON...", mvtDisapering, 5000);
			}
			else
			{
				if(dx!=0 || dy!=0)
				{
					Types::TRect vpr, bmr, mbr;
					int bmpw, bmph;
					TDoublePoint c;
					double fdx, fdy, w, h;
                    TMapsContainer *ptr;

					vpr=image->GetViewportRect();
					bmr=image->GetBitmapRect();
					mbr=((TMapObjectsContainer*)Owner)->Container->GetBitmapRect();
					bmpw=mbr.Width();
					bmph=mbr.Height();
					if(viewportarea && bmpw>0 && bmph>0)
					{
						c=viewportarea->Center();
						w=viewportarea->Width();
						h=viewportarea->Height();
						//dx=(int)((double)dx*w+0.5);
						//dy=(int)((double)dy*h+0.5);
						fdx=w*((double)dx/(double)bmpw);
						fdy=h*((double)dy/(double)bmph);
						//TDoublePoint cc=((TMapObjectsContainer*)Owner)->Container->ScreenToCoordinatesRect(Types::TPoint(X, Y));
						//Types::TPoint P=((TMapObjectsContainer*)Owner)->Container->CoordinatesRectToScreen(cc);
						if(c.X-fdx<0.) fdx=c.X;
						else if(c.X-fdx>1.) fdx=c.X-1;
						if(c.Y-fdy<0.) fdy=c.Y;
						else if(c.Y-fdy>1.) fdy=c.Y-1;
						if(fdx>0) dx=(int)((double)bmpw*fdx/w+0.5);
						else dx=(int)((double)bmpw*fdx/w-0.5);
						if(fdy>0) dy=(int)((double)bmph*fdy/h+0.5);
						else dy=(int)((double)bmph*fdy/h-0.5);
						c.X-=fdx;
						c.Y-=fdy;
						w/=2.;
						h/=2.;
						viewportarea->Left=c.X-w;
						viewportarea->Top=c.Y-h;
						viewportarea->Right=c.X+w;
						viewportarea->Bottom=c.Y+h;
					}
					ptr=((TMapObjectsContainer*)Owner)->Container->GetFirstObject(mtOnlineMap);
					if(!ptr || !((TInetMapBase*)ptr)->MapLoaderThreadId)
					{
						if(viewportarea && ((bmr.Left+dx)>=vpr.Left || (bmr.Right+dx)<=vpr.Right ||
							(bmr.Top+dy)>=vpr.Top || (bmr.Bottom+dy)<=vpr.Bottom)/* */) // || c.X<0 || c.X>1. || c.Y<0 || c.Y>1.))
						{
							((TMapObjectsContainer*)Owner)->Container->ZoomUpdate();
							if(!((TMapObjectsContainer*)Owner)->Container->IsContains(mtOnlineMap))
								image->ScrollToCenter((float)bmr.Width()/2., (float)bmr.Height()/2.);
						}
						else
						{
							image->Scroll(-dx, -dy);
							image->OnScroll(NULL);
						}
					}
				}
			}
		}
		__finally
		{
			locked=false;
		}
	}
}

//---------------------------------------------------------------------------
// TZoomObject
//---------------------------------------------------------------------------
__fastcall TZoomObject::TZoomObject(TMyObject *AOwner, TImgView32 *ImgView, Types::TRect Rect, TLockedDoubleRect *AViewportArea,
	TGraphicObjectsList *ATools, TObject *AManager, TMapScale *AMapScale)
	: TToolObject(AOwner, ImgView, Rect, AManager)
{
	//currentzoom=1;
	zoomstep=QuartZoomLevel;
	MaxZoom=DoubleZoomLevel;
	SelectedArea=Types::TRect(0,0,0,0);
	Tools=ATools;
	MapScale=AMapScale;
	viewportarea=AViewportArea;
	Clear();
}
//---------------------------------------------------------------------------

void __fastcall TZoomObject::MouseMove(System::TObject* Sender, Classes::TShiftState Shift, int X, int Y, TCustomLayer *Layer)
{
	if(Pressed)
	{
		/*Types::TRect tmp=image->GetBitmapRect();
		if(X<tmp.left) X=tmp.left;
		if(X<MapView->GetViewportRect().Left) X=MapView->GetViewportRect().Left;
		if(X>tmp.right) X=tmp.right;
		if(X>MapView->GetViewportRect().Right) X=MapView->GetViewportRect().Right;
		if(Y<tmp.top) Y=tmp.top;
		if(Y<MapView->GetViewportRect().Top) Y=MapView->GetViewportRect().Top;
		if(Y>tmp.bottom) Y=tmp.bottom;
		if(Y>MapView->GetViewportRect().Bottom) Y=MapView->GetViewportRect().Bottom;*/
		if(X<X1)
		{
			SelectedArea.left=X;
			SelectedArea.right=X1;
		}
		else
		{
			SelectedArea.left=X1;
			SelectedArea.right=X;
		}
		if(Y<Y1)
		{
			SelectedArea.top=Y;
			SelectedArea.bottom=Y1;
		}
		else
		{
			SelectedArea.top=Y1;
			SelectedArea.bottom=Y;
		}
		Update();
	}
}
//---------------------------------------------------------------------------

void __fastcall TZoomObject::MouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y, TCustomLayer *Layer)
{
	int x, y;
	float sX, sY, cz, f;
	Types::TRect tmp;

	if(SelectedArea.Width()>0 && SelectedArea.Height()>0 && image!=NULL)
	{
		tmp=image->GetViewportRect();
		x=SelectedArea.Left+(SelectedArea.Width()>>1);
		y=SelectedArea.Top+(SelectedArea.Height()>>1);
		sX=(float)tmp.Width()/(float)SelectedArea.Width();
		sY=(float)tmp.Height()/(float)SelectedArea.Height();
		cz=CurrentZoom;
		if(sX>sY) f=CurrentZoom*sY;
		else f=CurrentZoom*sX;
		CurrentZoom=ZoomStep*(float)((int)(0.5+f/ZoomStep));
		if(fabs(CurrentZoom-cz)>(ZoomStep/2)) ApplyZoomAtXY(x, y);
		SelectedArea=Types::TRect(0,0,0,0);
	}
	Visible=false;
}
//---------------------------------------------------------------------------

void __fastcall TZoomObject::Redraw(TObject *Sender, TBitmap32 *Bmp)
{
	Bmp->BeginUpdate();
	try
	{
		if(image!=NULL && Pressed)
		{
			TPolygon32 *p;
			p=new TPolygon32();
			p->Add(FixedPoint((int)SelectedArea.Left+1, (int)SelectedArea.Top+1));
			p->Add(FixedPoint((int)SelectedArea.Right-2, (int)SelectedArea.Top+1));
			p->Add(FixedPoint((int)SelectedArea.Right-2, (int)SelectedArea.Bottom-2));
			p->Add(FixedPoint((int)SelectedArea.Left+1, (int)SelectedArea.Bottom-2));
			Bmp->FrameRectS(SelectedArea, clWhite32);
			p->Draw(Bmp, clBlack32, SetAlpha(clBlack32, 64), NULL);
			delete p;
		}
	}
	__finally
	{
		Bmp->EndUpdate();
	}
}
//---------------------------------------------------------------------------

void __fastcall TZoomObject::ApplyZoom(bool WithUpdate)
{
	if(image!=NULL)
	{
		TDoublePoint c;
		//image->ScaleX=CurrentZoom;
		//image->ScaleY=CurrentZoom;
		if(ViewportArea)
		{
			float zc=0.5/CurrentZoom;
			c=ViewportArea->Center();

			ViewportArea->Left=c.X-zc;
			ViewportArea->Top=c.Y-zc;
			ViewportArea->Right=c.X+zc;
			ViewportArea->Bottom=c.Y+zc;
		}
		else
		{
			image->ScaleX=CurrentZoom;
			image->ScaleY=CurrentZoom;
		}
		//ZoomUpdateTools();
		if(WithUpdate && Owner && ((TMapObjectsContainer*)Owner)->Container)
		{
			((TMapObjectsContainer*)Owner)->Container->ZoomUpdate();
			//((TMapObjectsContainer*)Owner)->ZoomUpdate();
		}
		if(Owner)
		{
			try
			{
				Types::TRect tmp=image->GetBitmapRect();
				//Types::TPoint P;
				//P=((TMapObjectsContainer*)Owner)->Container->ViewportAreaToScreen(c);
				image->ScrollToCenter(tmp.Width() >> 1, tmp.Height() >> 1);
				((TGraphicObjectsContainer*)Owner)->OnScroll(NULL);
			}
			catch(Exception &e) {}
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TZoomObject::ApplyZoomAtXY(int X, int Y, bool WithUpdate)
{
	float x, y;
	Types::TRect tmp;

	try
	{
		if(image!=NULL)
		{
			tmp=image->GetBitmapRect();
			//image->
			if(tmp.Width()>0 && tmp.Height()>0)
			{
				/*if(X<tmp.left) X=tmp.left;
				else if(X>tmp.right) X=tmp.right;
				if(Y<tmp.top) Y=tmp.top;
				else if(Y>tmp.bottom) Y=tmp.bottom;
				x=((float)(X-tmp.Left))/(float)tmp.Width();
				y=((float)(Y-tmp.Top))/(float)tmp.Height();
				ApplyZoom(WithUpdate);
				tmp=image->GetBitmapRect();
				x*=(float)tmp.Width();//BitmapRect->Width();
				y*=(float)tmp.Height();//BitmapRect->Height();
				y/=image->ScaleX;
				x/=image->ScaleX;
				image->ScrollToCenter((int)x, (int)y);*/
				TDoublePoint c=((TMapObjectsContainer*)Owner)->Container->ScreenToViewportArea(Types::TPoint(X, Y));
				double w=ViewportArea->Width()/2., h=ViewportArea->Height()/2.;
				ViewportArea->Left=c.X-w;
				ViewportArea->Top=c.Y-h;
				ViewportArea->Right=c.X+w;
				ViewportArea->Bottom=c.Y+h;
				ApplyZoom(WithUpdate);
				if(Owner && ((TMapObjectsContainer*)Owner)->Messages)
				{
					((TMapObjectsContainer*)Owner)->Messages->AddMessage(TMessageType::mtInfo, "Zoom "+FloatToStrF(CurrentZoom*100, ffFixed, 10, 0)+"%", mvtDisapering, 2000);
				}
				if(MapScale) MapScale->ZoomText=IntToStr((int)(CurrentZoom*100.+0.5))+"%";
				if(WithUpdate && Tools) Tools->Update();
			}
		}
	}
	catch (Exception &e) {}
}
//---------------------------------------------------------------------------

void __fastcall TZoomObject::ActualZoom(bool WithUpdate)
{
	Types::TRect tmp;

	if(image!=NULL)
	{
		CurrentZoom=1;
		tmp=image->GetViewportRect();
		ApplyZoomAtXY(tmp.Width() >> 1, tmp.Height() >> 1, WithUpdate);
	}
}
//---------------------------------------------------------------------------

void __fastcall TZoomObject::ApplyCurrentZoom(bool WithUpdate)
{
	Types::TRect tmp;

	if(image!=NULL)
	{
		tmp=image->GetViewportRect();
		ApplyZoomAtXY(tmp.Width() >> 1, tmp.Height() >> 1, WithUpdate);
	}
}
//---------------------------------------------------------------------------

void __fastcall TZoomObject::ZoomIn()
{
	Types::TRect tmp;
	float cz;

	if(image!=NULL)
	{
		ChangeGradientZoomStep(CurrentZoom, CurrentZoom+ZoomStep);
		/*cz=CurrentZoom;
		cz+=ZoomStep;
		cz=ZoomStep*((float)(int)(0.5+cz/ZoomStep));
		CurrentZoom=cz;*/
		CurrentZoom+=ZoomStep;
		tmp=image->GetViewportRect();
		ApplyZoomAtXY(tmp.Width() >> 1, tmp.Height() >> 1);
	}
}
//---------------------------------------------------------------------------

void __fastcall TZoomObject::ZoomOut()
{
	Types::TRect tmp;

	if(image!=NULL)
	{
		ChangeGradientZoomStep(CurrentZoom+ZoomStep, CurrentZoom);
		CurrentZoom-=ZoomStep;
		tmp=image->GetViewportRect();
		ApplyZoomAtXY(tmp.Width() >> 1, tmp.Height() >> 1);
	}
}
//---------------------------------------------------------------------------

void __fastcall TZoomObject::ZoomFastOut()
{
	Types::TRect tmp;

	if(image!=NULL)
	{
		CurrentZoom=(int)(CurrentZoom-0.5);
		tmp=image->GetViewportRect();
		ApplyZoomAtXY(tmp.Width() >> 1, tmp.Height() >> 1);
	}
}
//---------------------------------------------------------------------------

void __fastcall TZoomObject::ZoomFastIn()
{
	Types::TRect tmp;

	if(image!=NULL)
	{
		CurrentZoom=(int)CurrentZoom+1;
		tmp=image->GetViewportRect();
		ApplyZoomAtXY(tmp.Width() >> 1, tmp.Height() >> 1);
	}
}
//---------------------------------------------------------------------------

void __fastcall TZoomObject::ChangeGradientZoomStep(float lastzoom, float value)
{
    if((int)(100.0*lastzoom+0.5)>(int)(100.0*value+0.5)) //ZoomOut
	{
		if((int)(100.0*currentzoom+0.5)==(int)(100.0*ZoomStep+0.5))
		{
			if((int)(100.0*ZoomStep+0.5)==(int)(100.0*DoubleZoomLevel+0.5))
			{
				zoomstep=QuartZoomLevel;
				MaxZoom=DoubleZoomLevel;
			}
			else if((int)(100.0*ZoomStep+0.5)==(int)(100.0*QuartZoomLevel+0.5))
			{
				zoomstep=NextZoomLevel;
				MaxZoom=QuartZoomLevel;
			}
			else if((int)(100.0*ZoomStep+0.5)==(int)(100.0*NextZoomLevel+0.5))
			{
				zoomstep=LastZoomLevel;
				MaxZoom=NextZoomLevel;
			}
		}
	}
	else if((int)(100.0*lastzoom+0.5)<(int)(100.0*value+0.5)) //ZoomIn
	{
		if((int)(100.0*currentzoom+0.5)==(int)(100.0*MaxZoom+0.5))
		{
			if((int)(100.0*ZoomStep+0.5)==(int)(100.0*LastZoomLevel+0.5))
			{
				zoomstep=NextZoomLevel;
				MaxZoom=QuartZoomLevel;
			}
			else if((int)(100.0*ZoomStep+0.5)==(int)(100.0*NextZoomLevel+0.5))
			{
				zoomstep=QuartZoomLevel;
				MaxZoom=DoubleZoomLevel;
			}
			else if((int)(100.0*ZoomStep+0.5)==(int)(100.0*QuartZoomLevel+0.5))
			{
				zoomstep=DoubleZoomLevel;
				MaxZoom=MaxZoomLevel;
            }
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TZoomObject::SetCurrentZoom(float value)
{
	//float lastzoom=currentzoom;
	if(value>=ZoomStep && value<MaxZoom) currentzoom=value;
	else if(value>=MaxZoom) currentzoom=MaxZoom;
	else if(value<ZoomStep) currentzoom=ZoomStep;

	// Apply gradient ZoomStep changing
	//ChangeGradientZoomStep(lastzoom, value);
}
//---------------------------------------------------------------------------

void __fastcall TZoomObject::SetCurrentZoomEx(float value)
{
	float lastzoom=currentzoom;

	if(value>=MaxZoom) currentzoom=MaxZoom;
	else if(value<ZoomStep) currentzoom=ZoomStep;
	else currentzoom=value;
	// Apply gradient ZoomStep changing
	ChangeGradientZoomStep(lastzoom, value);
}

//---------------------------------------------------------------------------
// TMapObjectsContainer
//---------------------------------------------------------------------------
__fastcall TMapObjectsContainer::TMapObjectsContainer(TImgView32 *ImgView, bool UseEvents,
	TMouseAction *ma, TVoidFunction aDBE, void* RMS, bool ReducedObjects)
	: TGraphicObjectsContainer(ImgView, UseEvents, ma, RMS)
{
	TMapsContainer *mc;

	container=new TMapsContainersCollection(this, ImgView, RMS);
	mc=new TEmptyMapsContainer(container, RMS, container->ViewportArea);
	dbe=aDBE;
	mc->OnDisableButton=aDBE;
	if(RMS)
	{
		mapscale=new TMapScale(this, ImgView, container->LatLonRect, NULL, ((TRadarMapSettings*)RMS)->Owner);
		if(!ReducedObjects)
		{
			windrose=new TWindRose(this, ImgView, ((TRadarMapSettings*)RMS)->Owner);
			clocks=new TToolClock(this, ImgView, ((TRadarMapSettings*)RMS)->Owner);
		}
		else
		{
			windrose=NULL;
			clocks=NULL;
		}
		hand=new THandObject(this, ImgView, ImgView->GetViewportRect(), container->ViewportArea, ((TRadarMapSettings*)RMS)->Owner);
		zoom=new TZoomObject(this, ImgView, ImgView->GetViewportRect(), container->ViewportArea, ToolsObjects, ((TRadarMapSettings*)RMS)->Owner, mapscale);
		if(((TRadarMapSettings*)RMS)->Messages)
			messages=new TMessagesScrollObject(this, ImgView, ImgView->GetViewportRect(), msaTop, ((TRadarMapSettings*)RMS)->Owner);
		else messages=NULL;
	}
	else
	{
		windrose=NULL;
		clocks=NULL;
		hand=NULL;
		zoom=NULL;

	}
}
//---------------------------------------------------------------------------

__fastcall TMapObjectsContainer::~TMapObjectsContainer(void)
{
	delete container;
}
//---------------------------------------------------------------------------

void __fastcall TMapObjectsContainer::MouseMove(System::TObject* Sender, Classes::TShiftState Shift, int X, int Y, TCustomLayer *Layer)
{
	TGraphicObjectsContainer::MouseMove(Sender, Shift, X, Y, Layer);
	if(!Entered && hand->Pressed)
	{
		hand->MouseMove(Sender, Shift, X, Y, Layer);
	}
}
//---------------------------------------------------------------------------

void __fastcall TMapObjectsContainer::MouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y, TCustomLayer *Layer)
{
	TStartStopWaypointLabel *wpl;
	TSliderToolButton *wpb;
	TStartStopWaypointKind kind;
	//TStartNGoLabel *sgl;

	TGraphicObjectsContainer::MouseDown(Sender, Button, Shift, X, Y, Layer);
	if(!Entered && mouseaction!=NULL)
	{
		switch(*mouseaction)
		{
			case tmaStartStopWaypoint:
				if(RadarMapSettings && ((TRadarMapSettings*)RadarMapSettings)->Owner)
				{
					wpb=((TRadarMapSettings*)RadarMapSettings)->Owner->StartStopWaypointBtn;
					if(wpb) kind=(TStartStopWaypointKind)wpb->Tag;
					else
					{
						if(((TRadarMapSettings*)RadarMapSettings)->Owner->StartWaypoint &&
							!((TRadarMapSettings*)RadarMapSettings)->Owner->StopWaypoint)
							kind=swkStop;
						else kind=swkStart;
					}
					wpl=new TStartStopWaypointLabel(this, Image->GetViewportRect(), Container->LatLonRect, Container->GetObject(0),
						Types::TPoint(X-Image->GetBitmapRect().Left, Y-Image->GetBitmapRect().Top), kind,
						(TObject*)((TRadarMapSettings*)RadarMapSettings)->Owner);
					if(kind==swkStop)
					{
						if(((TRadarMapSettings*)RadarMapSettings)->Owner->StartWaypoint)
							((TRadarMapSettings*)RadarMapSettings)->Owner->StartWaypoint->OpositeWayPoint=wpl;
						if(((TRadarMapSettings*)RadarMapSettings)->ShowLabelInformationOnPin)
							wpl->MouseUp(Sender, mbRight, TShiftState(ssRight), 0, 0, NULL);
						((TRadarMapSettings*)RadarMapSettings)->Owner->StartWaypoint=NULL;
						((TRadarMapSettings*)RadarMapSettings)->Owner->StopWaypoint=NULL;
					}
				}
				break;
			case tmaStartNGo:
				if(RadarMapSettings && ((TRadarMapSettings*)RadarMapSettings)->Owner)
				{
					//sgl=
					new TStartNGoLabel(this, Image->GetViewportRect(), Container->LatLonRect, Container,
						//Types::TPoint(X-Image->GetBitmapRect().Left, Y-Image->GetBitmapRect().Top),
						Types::TPoint(X, Y),
						(TObject*)((TRadarMapSettings*)RadarMapSettings)->Owner);
					if(OnDisableButton) (OnDisableButton)();/**/
					/*new TPosNotificationObject(this, Image, Image->GetViewportRect(), mvtConstant,
						5000, Color32(138, 237, 138), "StartNGoLabel", Container->LatLonRect, Container,
						Types::TPoint(X, Y),
						(TObject*)((TRadarMapSettings*)RadarMapSettings)->Owner);
					new TColorMaskLabel(this, Image->GetViewportRect(), Container->LatLonRect, Color32(138, 237, 138),
						Types::TPoint(X, Y),
						(TObject*)((TRadarMapSettings*)RadarMapSettings)->Owner);*/
				}
				break;
			case tmaPrediction:
				if(Shift.Contains(ssLeft) || Shift.Contains(ssTouch) || Shift.Contains(ssPen))
				{
					TMapsContainer* mc = Container->GetObject(0);
					if(mc) mc->GetFavotiteObjectsAtPoint(X, Y);
				}
				break;
			case tmaZoomIn:
				zoom->X1=X;
				zoom->Y1=Y;
				zoom->Pressed=true;
				zoom->InRect=true;
				zoom->Visible=true;
				break;
			case tmaMapLabelPolyLineGraphics:
			default:
				hand->Pressed=true;
				hand->X1=X;
				hand->Y1=Y;
				break;
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TMapObjectsContainer::MouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y, TCustomLayer *Layer)
{
	TGraphicObjectsContainer::MouseUp(Sender, Button, Shift, X, Y, Layer);
	hand->Pressed=false;
}
//---------------------------------------------------------------------------

int __fastcall TMapObjectsContainer::GetMapLabels(TList *list, TMapLabelType mlt,
	TProfileLabelType plt)
{
	int res=0;
	TProfileObject *po;
	TMapLabel *ml;

	if(list)
	{
		list->Clear();
		for(int k=0; k<CustomObjectsCount; k++)
		{
			po=dynamic_cast<TProfileObject*>(CustomObjectsList->Items[k]);
			if(po && po->ProfileObjectType==potMapLabel)
			{
				ml=dynamic_cast<TMapLabel*>(po);
				if(ml && (mlt==mltNone || ml->MapLabelType==mlt) && (plt==pltNone || ml->ProfileLabelType==plt))
					list->Add((void*)ml);
			}
		}
		res=list->Count;
	}
	return res;
}
//---------------------------------------------------------------------------

TMapLabel* __fastcall TMapObjectsContainer::GetMapLabel(unsigned int aID)
{
	TProfileObject *po;
	TMapLabel *ml, *res;

	res=NULL;
	for(int k=0; k<CustomObjectsCount; k++)
	{
		po=dynamic_cast<TProfileObject*>(CustomObjectsList->Items[k]);
		if(po && po->ProfileObjectType==potMapLabel )
		{
			ml=dynamic_cast<TMapLabel*>(po);
			if(ml && ml->ID==aID)
			{
				res=ml;
				break;
			}
		}
	}
	return res;
}

//---------------------------------------------------------------------------
// TMapCustomPathObject
//---------------------------------------------------------------------------
__fastcall TMapCustomPathObject::TMapCustomPathObject(TMyObject *AOwner, TProfileOutputView *OV, TGPSCoordinatesRect *GCR, TObject* RadarMapManager)
	: TProfileObject(AOwner, Types::TRect(0,0,0,0), OV, RadarMapManager)
{
	LatLonRect=GCR;
	//List=new TList;
	List=new TGPSCoordinatesList();
	LastAdded=NULL;
	color=clLime32;
	coordinatesrect=new TGPSCoordinatesRect(true);//false);
	profileobjecttype=potMapCustomPath;
}
//---------------------------------------------------------------------------

__fastcall TMapCustomPathObject::~TMapCustomPathObject()
{
	//Clear();
	delete List;
	delete coordinatesrect;
}
//---------------------------------------------------------------------------

void __fastcall TMapCustomPathObject::AddP(TGPSCoordinate* c)
{
	TGPSCoordinatesRect *cr, *cr2;
	//TGPSCoordinate *o, *o2;
	TDoublePoint o, o2, uv;
	TDoublePoint lt, rb;
	TMapsContainersCollection *container;
	TMapsContainer* mc;
	double w, h, rebuildviewport;
	bool eems;
	TDoublePoint dp;
	TCoordinateSystem cs=csLocalMetric();

	if(!c) return;
	try
	{
		try
		{
			if(Owner && ((TMapObjectsContainer*)Owner)->Container && ((TMapObjectsContainer*)Owner)->Container->Count>0)
			{
				container=((TMapObjectsContainer*)Owner)->Container;
				mc=container->GetObject(0);
#ifndef OnlyOneMap
				int i=1;

				while(mc && mc->MapType!=mtEmpty && i<container->Count)
				{
					mc=container->GetObject(i);
					i++;
				}
#endif
				if(mc && mc->MapType==mtEmpty && !mc->LatLonRect->Contains(c)) //!mc->CoordinatesRect.Contains(c->GetXY(mc->CoordinateSystem)))
				{
					if(Manager && ((TRadarMapManager*)Manager)->Settings)
					{
						w=(double)((TRadarMapManager*)Manager)->Settings->EmptyMapSize/2.0;//20.0;//
						eems=((TRadarMapManager*)Manager)->Settings->EnlargeEmptyMap;
					}
					else
					{
						w=50.;
						eems=false;
					}
					//if(CoordinatesRect->Valid)
					//	h=CoordinatesRect->Height(csMetric)*w/CoordinatesRect->Width(csMetric);
					if(mc->CoordinatesRect.IsValid())
						h=mc->CoordinatesRect.Height()*w/mc->CoordinatesRect.Width();
					else
					{
						if(((TEmptyMapsContainer*)mc)->RawPxRect)
							h=(double)((TEmptyMapsContainer*)mc)->RawPxRect->Height()*w/(double)((TEmptyMapsContainer*)mc)->RawPxRect->Width();
						else if(mc->Background) h=(double)mc->Background->Height*w/(double)mc->Background->Width;
						else h=w;
					}
					dp=c->GetXY(cs);//csMetric);
					uv=c->UnitVector(cs);
					o=DoublePoint(dp.X-w*uv.X, dp.Y-h*uv.Y, dp.Temp);
					o2=DoublePoint(dp.X+w*uv.X, dp.Y+h*uv.Y, dp.Temp);
					cr=new TGPSCoordinatesRect(false);
					try
					{
						if(mc->CoordinatesRect.IsValid())
						{
							lt=container->ViewportAreaToCoordinatesRect(container->ViewportArea->LeftTop);
							rb=container->ViewportAreaToCoordinatesRect(container->ViewportArea->RightBottom);
							rebuildviewport=true;
							if(eems)
							{
								cr2=new TGPSCoordinatesRect(false);
								cr2->SetCorners(o, o2, false, cs);//csMetric);
								try
								{
									cr->SetCorners(cr2->LeftTop->GetXY(mc->CoordinateSystem), mc->CoordinatesRect.LeftTop, false, mc->CoordinateSystem);
									o=cr->LeftTop->GetXY(mc->CoordinateSystem);
									cr->SetCorners(mc->CoordinatesRect.RightBottom, cr2->RightBottom->GetXY(mc->CoordinateSystem), false, mc->CoordinateSystem);
									o2=cr->RightBottom->GetXY(mc->CoordinateSystem);
									mc->CoordinatesRect.SetCorners(o, o2, false);
									mc->LatLonRectUpdate();
								}
								__finally
								{
									delete cr2;
									cr2=NULL;
								}
							}
						}
						else
						{
							rebuildviewport=false;
							cr->SetCorners(o, o2, false, cs);//csMetric);
							o=cr->LeftTop->GetXY(mc->CoordinateSystem);
							o2=cr->RightBottom->GetXY(mc->CoordinateSystem);
							mc->CoordinatesRect.SetCorners(o, o2, false);
							mc->LatLonRectUpdate();
						}
						if(!CoordinatesRect->Contains(mc->CoordinatesRect.LeftTop, mc->CoordinateSystem) ||
							!CoordinatesRect->Contains(mc->CoordinatesRect.RightBottom, mc->CoordinateSystem))
						{
							if(CoordinatesRect->Valid)
							{
								cr->SetCorners(mc->CoordinatesRect.LeftTop, CoordinatesRect->LeftTop->GetXY(mc->CoordinateSystem), false, mc->CoordinateSystem);
								o=cr->LeftTop->GetXY(mc->CoordinateSystem);
								cr->SetCorners(mc->CoordinatesRect.RightBottom, CoordinatesRect->RightBottom->GetXY(mc->CoordinateSystem), false, mc->CoordinateSystem);
								CoordinatesRect->SetCorners(o, cr->RightBottom->GetXY(mc->CoordinateSystem), false, mc->CoordinateSystem);
							}
							else CoordinatesRect->SetCorners(mc->CoordinatesRect.LeftTop, mc->CoordinatesRect.RightBottom, false, mc->CoordinateSystem);
						}
					}
					__finally
					{
						delete cr;
					}
					//Rebuilding container's rects...
					container->UpdateCoordinatesRect();
					if(rebuildviewport)
					{
						//container->UpdateCoordinatesRect();
						container->ViewportArea->LeftTop=container->CoordinatesRectToViewportArea(lt);
						container->ViewportArea->RightBottom=container->CoordinatesRectToViewportArea(rb);

						/*if(Owner && ((TMapObjectsContainer*)Owner)->Messages)
							((TMapObjectsContainer*)Owner)->Messages->AddMessage(TMessageType::mtText,
								"Map enlarged.", mvtDisapering, 3500);*/
//!!!!!!!!!!!!!!! --- Begin of Zoom Update ---
//!!!!!!!!!!!!!!! Current Zoom Update is not correct for 100%, "lt" and "rb" should not be changed
//!!!!!!!!!!!!!!! after map enlargement for map without zooming and dragging, but they are floating
//!!!!!!!!!!!!!!! from enlargement to enlargement, for code below! Without Zoom Update "lt" and "rb"
//!!!!!!!!!!!!!!! are stable for 100% (but ViewportArea is not square).
//!!!!!!!!!!!!!!!
//!!!!!!!!!!!!!!! It seems that linking of CoordinatesRect and ViewportArea has to be upgraded
//!!!!!!!!!!!!!!! with Zoom linking too, Zoom has to be strongly related to the ViewportArea sizes...
//!!!!!!!!!!!!!!!
//!!!!!!!!!!!!!!! There is a trick, change the devider to 20.0 instead of 2.0 to easy increase the
//!!!!!!!!!!!!!!! enlargemets count in line 738: w=(double)((TRadarMapManager*)Manager)->Settings->EmptyMapSize/2.0;
						if(((TMapObjectsContainer*)Owner)->Zoom)
						{
							Types::TRect vpt=((TMapObjectsContainer*)Owner)->Image->GetViewportRect();
							((TMapObjectsContainer*)Owner)->Zoom->SetCurrentZoomEx(1./((float)(container->ViewportArea->Width()+container->ViewportArea->Height())/2.));
							((TMapObjectsContainer*)Owner)->Zoom->Update();
							((TMapObjectsContainer*)Owner)->Zoom->ApplyZoomAtXY(vpt.Width() >> 1, vpt.Height() >> 1, false);
						}
//!!!!!!!!!!!!!!! --- End of Zoom Update ---
					}
				}
			}
		}
		catch(Exception &e) {}
		List->Add(c);
		LastAdded=c;
		if(coordinatesrect->Valid)
		{
			if(!coordinatesrect->Contains(c))
			{
				cr=new TGPSCoordinatesRect(false);
				try
				{
					cr->SetCorners(c, coordinatesrect->LeftTop);
					//o->Latitude=cr->LeftTop->Latitude;
					//o->Longitude=cr->LeftTop->Longitude;
					o=cr->LeftTop->GetXY(csLatLon);
					cr->SetCorners(c, coordinatesrect->RightBottom);
					coordinatesrect->SetCorners(o, cr->RightBottom->GetXY(csLatLon), false, csLatLon);
				}
				__finally
				{
					delete cr;
				}
			}
		}
		else coordinatesrect->SetCorners(c, c);
	}
	__finally
	{
		Rect=Types::TRect(0,0,0,0);
	}
}
//---------------------------------------------------------------------------

TGPSCoordinate* __fastcall TMapCustomPathObject::AddP(double ln, double lt, double Z, double Z_geoid, double conf)
{
	TGPSCoordinate *c=NULL;

	//ln=DegMinToDeg(ln);
	//lt=DegMinToDeg(lt);
	if(LastAdded==NULL || (LastAdded->Latitude!=lt || LastAdded->Longitude!=ln))
	{
		c=new TGPSCoordinate(this, lt, ln, Z, Z_geoid, NULL, conf);
		AddP(c);
		Update();
	}
	return c;
}
//---------------------------------------------------------------------------

void __fastcall TMapCustomPathObject::Insert(int Index, TGPSCoordinate* c)
{
	TGPSCoordinatesRect *cr;
	TGPSCoordinate *o;

	if(!c) return;
	cr=new TGPSCoordinatesRect(false);
	o=new TGPSCoordinate(false);
	try
	{
		List->Insert(Index, c);
		LastAdded=c;
		if(coordinatesrect->Valid)
		{
			cr->SetCorners(c, coordinatesrect->LeftTop);
			o->Latitude=cr->LeftTop->Latitude;
			o->Longitude=cr->LeftTop->Longitude;
			cr->SetCorners(c, coordinatesrect->RightBottom);
			coordinatesrect->SetCorners(o, cr->RightBottom);
		}
		else coordinatesrect->SetCorners(c, c);
	}
	__finally
	{
		delete o;
		delete cr;
	}
}
//---------------------------------------------------------------------------

TGPSCoordinate* __fastcall TMapCustomPathObject::Insert(int Index, double ln, double lt, double Z, double Z_geoid, double conf)
{
	TGPSCoordinate *c=NULL;

	//ln=DegMinToDeg(ln);
	//lt=DegMinToDeg(lt);
	if(LastAdded==NULL || (LastAdded->Latitude!=lt || LastAdded->Longitude!=ln))
	{
		c=new TGPSCoordinate(this, lt, ln, Z, Z_geoid, NULL, conf);
		Insert(Index, c);
		Update();
	}
	return c;
}
//---------------------------------------------------------------------------

bool __fastcall TMapCustomPathObject::IsInClientRect(Types::TPoint value)
{
	Types::TRect rct;

	if(Count>1)//>0)
	{
		if(Rect.Width()==0 && Rect.Height()==0) return false; //true;
		else
		{
			rct=ClientRect;
			rct.Left-=GPSPathSelectionWidth*3;
			rct.Top-=GPSPathSelectionWidth*3;
			rct.Right+=GPSPathSelectionWidth*3;
			rct.Bottom+=GPSPathSelectionWidth*3;
			if(rct.Contains(value))
			{
				return (FindPointInRangeAtXY(value.x, value.y, GPSPathSelectionWidth*3)!=NULL);
			}
			else return false;
		}
	}
	else return false;
}
//---------------------------------------------------------------------------

bool __fastcall TMapCustomPathObject::IsInClientRect(Types::TRect value)
{
	if(Rect.Width()==0 && Rect.Height()==0) return true;
	else return TProfileObject::IsInClientRect(value);
}

//---------------------------------------------------------------------------
// TMapPathObject
//---------------------------------------------------------------------------
__fastcall TMapPathObject::TMapPathObject(TMyObject *AOwner, TProfileOutputView *OV,
	TGPSCoordinatesRect *GCR, TObject* RadarMapManager, TObject* ASatellite)
	: TMapCustomPathObject(AOwner, OV, GCR, RadarMapManager)
{
	chainedcolors=false;
	color=clLime32;
	selectioncolor=clRed32;
	linewidth=GPSPathSelectionWidth;
	SelectedPoint=NULL;
	PressedIndex=-1;
	OV->Path=(TObject*)this;
	profileobjecttype=potMapPath;
	Satellite=ASatellite;
#ifdef _ShowGainButtonOnPath
	GainButton=new TSliderToolButton(AOwner, Types::TRect(2, 2, 34, 34)); //AOwner
	GainButton->DisabledGlyphName="PNGIMAGE_242";
	GainButton->HotGlyphName="PNGIMAGE_243";
	GainButton->NormalGlyphName="PNGIMAGE_244";
	GainButton->PressedGlyphName="PNGIMAGE_245";
	GainButton->Enabled=false;
	GainButton->Visible=false;
	GainButton->OnMouseClick=GainClick;
#endif
	name="";
	additionalname="";
}
//---------------------------------------------------------------------------

__fastcall TMapPathObject::~TMapPathObject()
{
#ifdef _ShowGainButtonOnPath
	delete GainButton;
#endif
}

AnsiString __fastcall TMapPathObject::readFullName()
{
	AnsiString res="";

	try
	{
		if(Satellite)
		{
			for(int i=0; i<((TProfileSatellites*)Satellite)->ProfilesCount; i++)
			{
				if(i>0) res+=", ";
				res+=((TProfileSatellites*)Satellite)->Profiles[i]->Name;
			}
		}
		if(res.Length()>0) res=name+" ("+res+")";
		if(AdditionalName!="") res+=" "+AdditionalName;
	}
	catch(Exception &e)
	{
		res=name;
	}

	return res;
}
//---------------------------------------------------------------------------

TGPSCoordinate* __fastcall TMapPathObject::AddP(double ln, double lt, double Z,
	double Z_geoid, TTrace *Ptr, bool &NotAddedButInsertedOrChanged, double conf)
{
	TGPSCoordinate *c;
	bool b;

	//if(!Ptr || Ptr->Index>MaxAddedTraceIndex)
	//{
		c=TMapCustomPathObject::AddP(ln, lt, Z, Z_geoid, conf);
		if(c!=NULL)
		{
			b=c->AutomaticUpdate;
			c->AutomaticUpdate=false;
			c->Trace=Ptr;
			c->AutomaticUpdate=b;
		}
		if(Ptr)	MaxAddedTraceIndex=Ptr->Index;
		NotAddedButInsertedOrChanged=false;
	//}
	/*else // replace is not so good, because we lost our first path and replace it with a bacward path...
	{
		c=(TGPSCoordinate*)List->Items[List->Count-1];
		for(int i=List->Count-1; i>=0; i--)
		{
			c=(TGPSCoordinate*)List->Items[i];
			if(c && c->Trace)
			{
				if(c->Trace->Index<=Ptr->Index)
				{
                    b=c->AutomaticUpdate;
					if(c->Trace->Index==Ptr->Index)
					{
						c->Latitude=DegMinToDeg(lt);
						c->Longitude=DegMinToDeg(ln);
					}
					else
					{
						c=Insert(i, ln, lt, Z);
						if(!c) return NULL;
					}
					c->AutomaticUpdate=false;
					c->Trace=Ptr;
					c->AutomaticUpdate=b;
					NotAddedButInsertedOrChanged=true;
					break;
				}
			}
		}
	}*/
	return c;
}
//---------------------------------------------------------------------------

void __fastcall TMapPathObject::DrawLine32(TBitmap32 *Bmp, TColor32 Color,
	TColor32 Border, int Width, bool Shadow, Gr32::TFixedPoint *LineFP,
	int &linefp_cnt, bool InOutputView)
{
	TColor32Entry ce;
	int a;

	if(InOutputView && this==((TRadarMapManager*)Manager)->CurrentPath)
	{
		//Width*=2;
		ce.ARGB=Color;
		a=ce.A;
		ce.ARGB=selectioncolor;
		ce.A=a;
		Color=ce.ARGB;
	}
	DrawPolyLine(Bmp, Color, Border, Width, Shadow, LineFP, linefp_cnt);
	linefp_cnt=0;
}
//---------------------------------------------------------------------------

void __fastcall TMapPathObject::DrawSegment32(TBitmap32 *Bmp, TColor32 Color,
	TColor32 Border, int Width, bool Shadow, Gr32::TFixedPoint *LineFP,
	int &linefp_cnt, bool InOutputView, Types::TPoint* EndPoints, int j)
{
	TColor32Entry ce;
	int a;

	DrawLine32(Bmp, Color, Border, Width, Shadow, LineFP, linefp_cnt, InOutputView);
	if(InOutputView && this==((TRadarMapManager*)Manager)->CurrentPath)
	{
		ce.ARGB=Color;
		a=ce.A;
		ce.ARGB=selectioncolor;
		ce.A=a;
		Color=ce.ARGB;
		DrawArrow32(Bmp, Color, Border, Width, Shadow, InOutputView, EndPoints, j);
	}
}
//---------------------------------------------------------------------------

void __fastcall TMapPathObject::DrawArrow32(TBitmap32 *Bmp, TColor32 Color,
	TColor32 Border, int Width, bool Shadow, bool InOutputView,
	Types::TPoint* EndPoints, int j, bool DrawName)
{
	int i, k, tw, th, tx, ty;
	double a, tr;
	TLine line, line2, line3, textvector;
    TFixedPoint *LineFP;
	int linefp_cnt=0, ArrowEdgeL=20;
	Types::TRect vpt;

	if(j>1 && EndPoints)
	{
		vpt=((TMapObjectsContainer *)Owner)->Image->GetViewportRect();
		LineFP=new TFixedPoint[j*2];
		try
		{
			line=TLine();
			for(i=0; i<(int)(j/2+0.5); i++)
			{
				line.X1+=EndPoints[i].x;
				line.Y1+=EndPoints[i].y;
			}
			line.X1=(float)line.X1/(float)i;
			line.Y1=(float)line.Y1/(float)i;
			for(i=(int)(j/2+0.5), k=0; i<j; i++, k++)
			{
				line.X2+=EndPoints[i].x;
				line.Y2+=EndPoints[i].y;
			}
			line.X2=(float)line.X2/(float)k;
			line.Y2=(float)line.Y2/(float)k;
			line.Move(EndPoints[j-1].x, EndPoints[j-1].y);
			line.SetLength(ArrowEdgeL);
			textvector=line;
			line2=line;
			line.Rotate(5*M_PI/6.);
			linefp_cnt=0;
			line3=line;
			if(line.IsInRect(vpt))
			{
				LineFP[linefp_cnt++]=FixedPoint(line.X1, line.Y1);
				LineFP[linefp_cnt++]=FixedPoint(line.X2, line.Y2);
			}
			if(line3!=line) DrawLine32(Bmp, Color, Border, Width, false, LineFP, linefp_cnt, InOutputView);
			line=line2;
			line.Rotate(-5*M_PI/6.);
			if(line.IsInRect(vpt))
			{
				LineFP[linefp_cnt++]=FixedPoint(line.X1, line.Y1);
				LineFP[linefp_cnt++]=FixedPoint(line.X2, line.Y2);
			}
			DrawLine32(Bmp, Color, Border, Width, false, LineFP, linefp_cnt, InOutputView);
			a=textvector.Angle();

			if(a>0) while(a>M_PI2) a-=M_PI2;
			else while(a<0) a+=M_PI2;

			if(DrawName && j>0 && vpt.Contains(Types::TPoint(textvector.X1, textvector.Y1)))
			{
				if(FullName!=NULL && FullName.Length()>0 && ((TRadarMapManager*)Manager)->Settings &&
					(((TRadarMapManager*)Manager)->Settings)->DrawPathName)
				{
					TFont *font=new TFont();
					double tr2;

					try
					{
						font->Name=Bmp->Font->Name;
						font->Size=Bmp->Font->Size;
						font->Style=Bmp->Font->Style;
						Bmp->Font->Name="Tahoma";
						Bmp->Font->Size=8;
						Bmp->Font->Style=TFontStyles();
						tw=Bmp->TextWidth(FullName);
						th=Bmp->TextHeight(FullName);
						/*tr=(double)tw+(double)(th>>1);
						if(fabs(a)<M_PI) tx=tr*fabs(a)/M_PI;
						else tx=tr*(2*M_PI-fabs(a))/M_PI;
						tx=textvector.X1-tx;
						ty=textvector.Y1;
						tx+=(th>>1);
						ty+=sin(a)*(double)th-(th>>1);//*/
						line=textvector;
						textvector.Reverse();
						textvector.SetLength(textvector.Length()-5);
						textvector.Reverse();
						tx=textvector.X1;
						ty=textvector.Y1;
						if(a>=-M_PI_4 && a<=M_PI_4) ty-=th*(1.-(a+M_PI_4)/M_PI_2);
						else if(a>=7.*M_PI_4) ty-=th*(1.-(a-7.*M_PI_4)/M_PI_2);
						else if(a>=3*M_PI_4 && a<=5.*M_PI_4)
						{
							tx-=tw;
							ty-=th*(a-3*M_PI_4)/M_PI_2;
						}
						else if(a>M_PI_4 && a<3.*M_PI_4)
						{
							tx-=tw*(a-M_PI_4)/M_PI_2;
						}
						else //if(a>5.*M_PI_4 && a<7.*M_PI_4)
						{
							tx-=tw*(1.-(a-5.*M_PI_4)/M_PI_2);
							ty-=th;
						}
						if(tx+tw>vpt.right) tx=vpt.right-tw-(th>>1);
						if(ty+th>vpt.Bottom) ty=vpt.Bottom-3*(th>>1);
						Bmp->RenderText(tx, ty, FullName, 2, Color);//clGray32);
						textvector=line;
					}
					__finally
					{
						Bmp->Font->Name=font->Name;
						Bmp->Font->Size=font->Size;
						Bmp->Font->Style=font->Style;
						delete font;
					}
				}
#ifdef _ShowGainButtonOnPath
				if(this==((TRadarMapManager*)Manager)->CurrentPath) //Draw GainButton
				{
					tw=GainButton->Width;
					th=GainButton->Height;
					line=textvector;
					textvector.Rotate(M_PI_2);
					textvector.SetLength(20);
					tx=textvector.X2;
					ty=textvector.Y2;
					if(a>=0 && a<=M_PI_2)
					{
						tx-=tw;
						ty-=th*a/M_PI_2;
					}
					else if(a>=M_PI && a<=3.*M_PI_2) ty-=th*(1.-(a-M_PI)/M_PI_2);
					else if(a>3.*M_PI_2 && a<=M_PI2) tx-=tw*(a-3.*M_PI_2)/M_PI_2;
					else //if(a>M_PI_2 && a<M_PI)
					{
						tx-=tw*(1.-(a-M_PI_2)/M_PI_2);
						ty-=th;
					}
					if(tx<vpt.Left) tx=vpt.left;
					if(ty<vpt.top) ty=vpt.top;

					GainButton->Left=tx;
					GainButton->Top=ty;
					GainButton->Visible=true;
					GainButton->Enabled=(((TRadarMapManager*)Manager)->CurrentProfile!=
						((TRadarMapManager*)Manager)->ReceivingProfile);
					textvector=line;
				}
				else GainButton->Visible=false;
#endif
			}
#ifdef _ShowGainButtonOnPath
			else GainButton->Visible=false;
#endif
		}
		__finally
		{
			delete[] LineFP;
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TMapPathObject::writeSelectionColor(TColor32 value)
{
	selectioncolor=value;
	if(chainedcolors)
	{
		unsigned char r, g, b;

		r=(value&0x00FF0000)>>16;
		g=(value&0x0000FF00)>>8;
		b=(value&0x000000FF);
		color=((value&0xFF000000) | (b<<16) | (r<<8) | g);
		//color=((value&0xFF000000) | ((~value)&0x00FFFFFF));
	}
	Update();
}
//---------------------------------------------------------------------------

void __fastcall TMapPathObject::writeColor(TColor32 value)
{
	color=value;
	if(chainedcolors)
	{
		unsigned char r, g, b;

		r=(value&0x00FF0000)>>16;
		g=(value&0x0000FF00)>>8;
		b=(value&0x000000FF);
		selectioncolor=((value&0xFF000000) | (g<<16) | (b<<8) | r);
	}
	Update();
}
//---------------------------------------------------------------------------

void __fastcall TMapPathObject::writeChainedColors(bool value)
{
	if(chainedcolors!=value)
	{
		chainedcolors=value;
		SelectionColor=SelectionColor;
	}
}
//---------------------------------------------------------------------------

void __fastcall TMapPathObject::DrawPath(TBitmap32 *Bmp, TColor32 Color, int Width, bool Shadow)
{
	TGPSCoordinate *llp;
	float x, y, x_old, y_old;
	Types::TRect bmr, vpt;
	TLine line, line2, line3;
	Types::TPoint *EndPoints;
	int i, j, k, N;
	TFixedPoint *LineFP;
	int linefp_cnt=0;
	TColor32 Border;
	bool InOutputView, WasLastPointInOutputView;

	if(Shadow) Border=clBlack32;
	else Border=0x0;
	Rect=Types::TRect(0,0,0,0);
    x_old=y_old=-100;
	if(Owner!=NULL && ((TMapObjectsContainer *)Owner)->Image!=NULL &&
		OutputView!=NULL && OutputView->Profile!=NULL && LatLonRect!=NULL &&
		LatLonRect->Width()>0. && LatLonRect->Height()>0.)
	{
		bmr=((TMapObjectsContainer *)Owner)->Image->GetBitmapRect();
		vpt=((TMapObjectsContainer *)Owner)->Image->GetViewportRect();
		if(List->Count<8) i=8;
		else i=List->Count;
		LineFP=new TFixedPoint[i];
		N=(int)(GPSPathSelectionArrowPointsQ/((float)bmr.Width()/(float)vpt.Width()));
		if(N>GPSPathSelectionArrowPointsQ) N=GPSPathSelectionArrowPointsQ;
		else if(N<GPSPathSelectionArrowPointsQ/2) N=GPSPathSelectionArrowPointsQ/2;
		EndPoints=new Types::TPoint[N];
		WasLastPointInOutputView=false;
		try
		{
			if(bmr.Width()>0 && bmr.Height()>0 && vpt.Width()>0 && vpt.Height()>0 && List->Count>0)
			{
				TCoordinateSystem cs;

				if(((TMapObjectsContainer*)Owner)->Container) cs=((TMapObjectsContainer*)Owner)->Container->CoordinateSystem;
				else
				{
					if(Manager && ((TRadarMapManager*)Manager)->Settings) cs=((TRadarMapManager*)Manager)->Settings->DefaultCoordinateSystem;
					else cs=csLatLon;
				}
				Bmp->BeginUpdate();
				j=0;
				for(i=0; i<List->Count; i++)
				{
					//llp=(TGPSCoordinate*)List->Items[i];
					llp=List->Items[i];
					if(llp!=NULL)
					{
						if(cs==csLatLon)
						{
							y=LatLonRect->LeftTop->Latitude-llp->Latitude;
							x=llp->Longitude-LatLonRect->LeftTop->Longitude;
						}
						else
						{
							y=LatLonRect->LeftTop->GetY(cs)-llp->GetY(cs);
							x=llp->GetX(cs)-LatLonRect->LeftTop->GetX(cs);
						}
						/*switch(cs)
						{
							case csDutch:
								y=LatLonRect->LeftTop->RdY-llp->RdY;
								x=llp->RdX-LatLonRect->LeftTop->RdX;
								break;
							case csBeLambert08:
							case csBeLambert72:
								y=LatLonRect->LeftTop->BeY-llp->BeY;
								x=llp->BeX-LatLonRect->LeftTop->BeX;
								break;
							case csUTM:
								y=LatLonRect->LeftTop->UtmY-llp->UtmY;
								x=llp->UtmX-LatLonRect->LeftTop->UtmX;
								break;
							case csLKS:
								y=LatLonRect->LeftTop->LksY-llp->LksY;
								x=llp->LksX-LatLonRect->LeftTop->LksX;
								break;
							case csLatLon:
							default:
								y=LatLonRect->LeftTop->Latitude-llp->Latitude;
								x=llp->Longitude-LatLonRect->LeftTop->Longitude;
								break;
						}*/
						////y=llp->Latitude-LatLonRect->LeftTop->Latitude;
						//y=LatLonRect->LeftTop->Latitude-llp->Latitude;
						y/=LatLonRect->Height(cs);
						//x=llp->Longitude-LatLonRect->LeftTop->Longitude;
						x/=LatLonRect->Width(cs);
						x*=bmr.Width();
						y*=bmr.Height();
						x+=bmr.Left;
						y+=bmr.Top;
						x=(int)(x+0.5);
						y=(int)(y+0.5);
						llp->x=x;
						llp->y=y;

						if(x<Rect.Left || j==0) Rect.Left=x;
						if(x>Rect.Right || j==0) Rect.Right=x;
						if(y<Rect.Top || j==0) Rect.Top=y;
						if(y>Rect.Bottom || j==0) Rect.Bottom=y;

						if(llp->Trace!=NULL && OutputView->InScrolling)
							InOutputView=(llp->Trace->Index>=OutputView->ScrollingLeftIndex &&
								llp->Trace->Index<=OutputView->ScrollingRightIndex);
						else InOutputView=(llp->Trace!=NULL && llp->Trace->Index>=OutputView->LeftIndex &&
							llp->Trace->Index<=OutputView->RightIndex);
						if((int)x!=(int)x_old || (int)y!=(int)y_old)
						{
							if(j>0)
							{
								line=TLine(x_old, y_old, x, y);
								if(line.IsInRect(vpt))
								{
									if(WasLastPointInOutputView!=InOutputView)
										DrawSegment32(Bmp, Color, Border, Width, Shadow, LineFP, linefp_cnt, !InOutputView, EndPoints, j);
									if(linefp_cnt==0) LineFP[linefp_cnt++]=FixedPoint(line.X1, line.Y1);
									LineFP[linefp_cnt++]=FixedPoint(line.X2, line.Y2);
								}
								else if(linefp_cnt>0) DrawSegment32(Bmp, Color, Border, Width, Shadow, LineFP, linefp_cnt, WasLastPointInOutputView, EndPoints, j);
								WasLastPointInOutputView=InOutputView;
							}
							if(j<N)
							{
								EndPoints[j]=Types::TPoint(x, y);
								j++;
							}
							else
							{
								for(k=0; k<N-1; k++) EndPoints[k]=EndPoints[k+1];
								EndPoints[N-1]=Types::TPoint(x, y);
							}
							x_old=x;
							y_old=y;
						}
					}
				}
				if(List->Count>0)
				{
					Rect.Left--;
					Rect.Right++;
					Rect.Top--;
					Rect.Bottom++;
				}
				DrawLine32(Bmp, Color, Border, Width, Shadow, LineFP, linefp_cnt, WasLastPointInOutputView);
				DrawArrow32(Bmp, Color, Border, Width, Shadow, InOutputView, EndPoints, j, true);
				Bmp->EndUpdate();
			}
		}
		__finally
		{
			delete[] EndPoints;
			delete[] LineFP;
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TMapPathObject::Redraw(TObject *Sender, TBitmap32 *Bmp)
{
	TColor32Entry ce;
	float coef;

	try
	{
		if(InRect || Pressed) DrawPath(Bmp, color, linewidth+2, true);
		else
		{
			ce.ARGB=color;
			if(this==((TRadarMapManager*)Manager)->CurrentPath) coef=0.9;
			else coef=0.7;
			ce.A=(Byte)((float)ce.A*coef);
			ce.R=(Byte)((float)ce.R*coef);
			ce.G=(Byte)((float)ce.G*coef);
			ce.B=(Byte)((float)ce.B*coef);
			DrawPath(Bmp, ce.ARGB, linewidth*(0.5+coef), this==((TRadarMapManager*)Manager)->CurrentPath);
#ifdef _ShowGainButtonOnPath
			if(OutputView && OutputView->Profile!=((TRadarMapManager*)Manager)->ReceivingProfile)
				GainButton->Redraw(Sender, Bmp);
#endif
		}
	}
	catch(...) {}
}
//---------------------------------------------------------------------------

void __fastcall TMapPathObject::GainClick(System::TObject* Sender, int X, int Y)
{
	if(this==((TRadarMapManager*)Manager)->CurrentPath &&
		(((TRadarMapManager*)Manager)->CurrentProfile!=
			((TRadarMapManager*)Manager)->ReceivingProfile))
	{
		TProfile* Prof=((TRadarMapManager*)Manager)->Current->Profiles[0];

		if(Prof && Prof->LastTracePtr) //Prof->CommonTrace)
		{
			TGainForm* GainForm;
			//bool ToBeDrawed=false;

			GainForm=new TGainForm(NULL, (TObject*)Manager);
			try
			{
				GainForm->GainItems->GainFunction->CopyFromProfile(Prof);
				GainForm->GainItems->ReAssignGainPoints();
				GainForm->GainItems->SelectedGainItem=GainForm->GainItems->GainFunction->GainPointsCount-1;
				GainForm->GainItems->SingleTrace=Prof->LastTracePtr;//(TTrace*)Prof->CommonTrace;
				if(GainForm->ShowModal())
				{
					for(int i=0; i<((TRadarMapManager*)Manager)->Current->ProfilesCount; i++)
					{
						Prof=((TRadarMapManager*)Manager)->Current->Profiles[i];
						if(Prof) GainForm->GainItems->GainFunction->CopyToProfile(Prof, true);
					}
					((TRadarMapManager*)Manager)->SwitchToPath(this);
				}
			}
			__finally
			{
				delete GainForm;
			}
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TMapPathObject::MouseClick(System::TObject* Sender, int X, int Y)
{
	TGPSCoordinate *llp;

	llp=FindPointInRangeAtXY(X, Y, GPSPathSelectionWidth*3);
	if(llp!=NULL && llp->Trace!=NULL && this==((TRadarMapManager*)Manager)->CurrentPath)
		OutputView->CenterAtPos(llp->Trace->Index);
}
//---------------------------------------------------------------------------

void __fastcall TMapPathObject::MouseMove(System::TObject* Sender,
	Classes::TShiftState Shift, int X, int Y, TCustomLayer *Layer)
{
	int x, y, d;
	Types::TRect tmp;
	TGPSCoordinate *llp, *llp2;
	bool UpdateStt=false;

	if((X!=old_X || Y!=old_Y) && OutputView!=NULL && OutputView->Profile!=NULL)
	{
		if(pressed && SelectedPoint!=NULL)
		{
			llp=FindNearestPointAtXY(X, Y);
			if(PressedIndex<0)
			{
				if(SelectedPoint->Trace!=NULL && SelectedPoint->Trace->Index>=OutputView->LeftIndex &&
				  SelectedPoint->Trace->Index<=OutputView->RightIndex)
				{
					PressedIndex=SelectedPoint->Trace->Index;
					OutputView->InScrolling=true;
				}
				else Pressed=false;
			}
			if(pressed)
			{
				if(llp!=NULL && llp->Trace!=NULL)
				{
					OutputView->ScrollingRightIndex=llp->Trace->Index+(OutputView->RightIndex-PressedIndex);
					OutputView->ScrollingLeftIndex=OutputView->ScrollingRightIndex-OutputView->Width();
					if(OutputView->ScrollingLeftIndex==0) OutputView->ScrollingRightIndex=OutputView->Width();
					UpdateStt=true;
					((TRadarMapManager*)Manager)->ProfileScroller->Update();
				}
			}
		}
		else
		{
			if(pressed)
			{
				llp=FindPointInRangeAtXY(X, Y, GPSPathSelectionWidth*3);
				UpdateStt=(OutputView->InScrolling || llp==NULL);
				OutputView->InScrolling=false;
				if(llp==NULL) pressed=false;
				SelectedPoint=llp;
			}
			PressedIndex=-1;
		}
	}
	if(UpdateStt) Update();
	old_X=X;
	old_Y=Y;
}
//---------------------------------------------------------------------------

void __fastcall TMapPathObject::MouseUp(TObject *Sender,
	TMouseButton Button, TShiftState Shift, int X, int Y,
	TCustomLayer *Layer)
{
	if(PressedIndex>0 && SelectedPoint!=NULL)
	{
		OutputView->ApplyScrolling();
		((TRadarMapManager*)Manager)->ProfileScroller->Update(); // Dolzhna byt' sdelana realizacija cherez Update Container'a
		Update();
	}
	SelectedPoint=NULL;
}
//---------------------------------------------------------------------------

void __fastcall TMapPathObject::MouseDblClick(System::TObject* Sender, int X, int Y)
{
	if(this!=((TRadarMapManager*)Manager)->CurrentPath)
	{
		((TRadarMapManager*)Manager)->SwitchToPath(this);


	}
}

//---------------------------------------------------------------------------
// TMapTrackPathObject
//---------------------------------------------------------------------------
__fastcall TMapTrackPathObject::TMapTrackPathObject(TMyObject *AOwner, TGPSCoordinatesRect *GCR, TObject* RadarMapManager)
	: TMapCustomPathObject(AOwner, NULL, GCR, RadarMapManager)
{
	mouseable=false;
	gpsunit=NULL;
	ZeroPoint=NULL;
	color=Color32(124, 0, 186, 255);
	direction=0.;
	PointsMax=10;
	timer=new TTimerThread(2500);
	timer->OnTimer=&OnIdle;
	timer->Start();
	//validazimuth=false;
	//azimuth=0;
	ListInEditingEvent=CreateEvent(NULL, true, false, NULL);
	PointGlyph=NULL;
	PressedPointGlyph=NULL;
	FollowMeGlyph=NULL;
	NoGpsGlyph=NULL;
	PointGlyphName="PNGIMAGE_457";//"PNGIMAGE_113";
	PressedPointGlyphName="PNGIMAGE_460";//"PNGIMAGE_249";
	FollowMeGlyphName="PNGIMAGE_458";
	NoGpsGlyphName="PNGIMAGE_459";
	mtpState=(TMapTrackPathState)(mtpsNoGPS | mtpsNormal);
	PointRect=Types::TRect(0,0,0,0);
	startwaypoint=NULL;
	stopwaypoint=NULL;
}
//---------------------------------------------------------------------------

__fastcall TMapTrackPathObject::~TMapTrackPathObject()
{
	try
	{
		timer->OnTimer=NULL;
		timer->AskForTerminate();
		if(timer->ForceTerminate()) delete timer;
		timer=NULL;
		PointGlyphName="";
		PressedPointGlyphName="";
		FollowMeGlyphName="";
		NoGpsGlyphName="";
	}
	catch(Exception &e) {}
	CloseHandle(ListInEditingEvent);
}
//---------------------------------------------------------------------------

void __fastcall TMapTrackPathObject::AddP(TGPSCoordinate *c)
{
	while(WaitForSingleObject(ListInEditingEvent, 0)==WAIT_OBJECT_0)
	{
		//ProcessMessages();
		MySleep(10);
	}
	SetEvent(ListInEditingEvent);
	try
	{
		TMapCustomPathObject::AddP(c);
		ListGarbageEater();
		mtpState=(TMapTrackPathState)(mtpState & ~mtpsNoGPS);
		timer->Start();
	}
	__finally
	{
		ResetEvent(ListInEditingEvent);
	}
}
//---------------------------------------------------------------------------

TGPSCoordinate* __fastcall TMapTrackPathObject::AddP(double ln, double lt, double Z, double Z_geoid, double coef)
{
	TGPSCoordinate* c;

	c=TMapCustomPathObject::AddP(ln, lt, Z, Z_geoid, coef);
	ApplyFollowMe();
	return c;
}
//---------------------------------------------------------------------------

void __fastcall TMapTrackPathObject::ListGarbageEater(void)
{
	//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	//Not protected by ListInEditingEvent from simultenious access to the List
	//Have to be called after ListInEditingEvent is cleared only
	//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	try
	{
		while(List->Count>PointsMax)
		{
			//delete (TGPSCoordinate*)List->Items[0];
			List->Delete(0);
		}
	}
	catch(Exception &e)	{}
}
//---------------------------------------------------------------------------

TGPSCoordinate* __fastcall TMapTrackPathObject::readPoints(int Index)
{
	TGPSCoordinate* c;

	while(WaitForSingleObject(ListInEditingEvent, 0)==WAIT_OBJECT_0)
	{
		//ProcessMessages();
		MySleep(10);
	}
	SetEvent(ListInEditingEvent);
	try
	{
		c=TMapCustomPathObject::readPoints(Index);
	}
	__finally
	{
		ResetEvent(ListInEditingEvent);
	}
	return c;
}
//---------------------------------------------------------------------------

TGPSCoordinate* __fastcall TMapTrackPathObject::readFirstPoint(void)
{
	TGPSCoordinate* c;

	while(WaitForSingleObject(ListInEditingEvent, 0)==WAIT_OBJECT_0)
	{
		//ProcessMessages();
		MySleep(10);
	}
	SetEvent(ListInEditingEvent);
	try
	{
		c=TMapCustomPathObject::readFirstPoint();
	}
	__finally
	{
		ResetEvent(ListInEditingEvent);
	}
	return c;
}
//---------------------------------------------------------------------------

TGPSCoordinate* __fastcall TMapTrackPathObject::readLastPoint(void)
{
    TGPSCoordinate* c;

	while(WaitForSingleObject(ListInEditingEvent, 0)==WAIT_OBJECT_0)
	{
//		ProcessMessages();
		MySleep(10);
	}
	SetEvent(ListInEditingEvent);
	try
	{
		c=TMapCustomPathObject::readLastPoint();
	}
	__finally
	{
		ResetEvent(ListInEditingEvent);
	}
	return c;
}
//---------------------------------------------------------------------------

void __fastcall TMapTrackPathObject::writePressed(bool value)
{
	if(pressed!=value)
	{
		pressed=value;
		if(pressed) mtpState=(TMapTrackPathState)(mtpState | mtpsPressed);
		else mtpState=(TMapTrackPathState)(mtpState & ~mtpsPressed);
	}
}
//---------------------------------------------------------------------------

void __fastcall TMapTrackPathObject::Redraw(TObject *Sender, TBitmap32 *Bmp)
{
	TGPSCoordinate *llp;
	float x, y, x_old, y_old;
	Types::TRect bmr, vpt;
	TColor32Entry ce;
	TLine line;
	int i, j;
	float da, A;
	bool ZeroPointDetected=false;

	Rect=Types::TRect(0,0,0,0);

	if(Owner!=NULL && ((TGraphicObjectsContainer *)Owner)->Image!=NULL &&
		LatLonRect!=NULL && LatLonRect->Width()>0. && LatLonRect->Height()>0.)
	{
		bmr=((TGraphicObjectsContainer *)Owner)->Image->GetBitmapRect();
		vpt=((TGraphicObjectsContainer *)Owner)->Image->GetViewportRect();
		while(WaitForSingleObject(ListInEditingEvent, 0)==WAIT_OBJECT_0)
		{
			//ProcessMessages();
			MySleep(10);
		}
		SetEvent(ListInEditingEvent);
		try
		{
			try
			{
				if(bmr.Width()>0 && bmr.Height()>0 && vpt.Width()>0 && vpt.Height()>0
					&& List->Count>0)
				{
					TCoordinateSystem cs;

					if(((TMapObjectsContainer*)Owner)->Container) cs=((TMapObjectsContainer*)Owner)->Container->CoordinateSystem;
					else
					{
						if(Manager && ((TRadarMapManager*)Manager)->Settings) cs=((TRadarMapManager*)Manager)->Settings->DefaultCoordinateSystem;
						else cs=csLatLon;
					}
					Bmp->BeginUpdate();
					if(mtpState & mtpsPressed) ce.ARGB=Color32(71, 158, 253, 255);
					else if(mtpState & mtpsNoGPS) ce.ARGB=Color32(152, 153, 157, 255);
					else if(mtpState & mtpsFollowMe) ce.ARGB=Color32(24, 137, 150, 255);
					else ce.ARGB=Color32(63, 103, 192, 255);//Color32(124, 0, 186, 255);
					da=256.0/(float)PointsMax;
					A=255;
					direction=0.;
					for(i=List->Count-1, j=0; i>=0; i--)
					{
						llp=(TGPSCoordinate*)List->Items[i];
						ce.A=(unsigned char)A;
						if(llp!=NULL)
						{
							if(cs==csLatLon)
							{
								y=LatLonRect->LeftTop->Latitude-llp->Latitude;
								x=llp->Longitude-LatLonRect->LeftTop->Longitude;
							}
							else
							{
								y=LatLonRect->LeftTop->GetY(cs)-llp->GetY(cs);
								x=llp->GetX(cs)-LatLonRect->LeftTop->GetX(cs);
							}
							/*switch(cs)
							{
								case csDutch:
									y=LatLonRect->LeftTop->RdY-llp->RdY;
									x=llp->RdX-LatLonRect->LeftTop->RdX;
									break;
								case csBeLambert08:
								case csBeLambert72:
									y=LatLonRect->LeftTop->BeY-llp->BeY;
									x=llp->BeX-LatLonRect->LeftTop->BeX;
									break;
								case csUTM:
									y=LatLonRect->LeftTop->UtmY-llp->UtmY;
									x=llp->UtmX-LatLonRect->LeftTop->UtmX;
									break;
								case csLKS:
									y=LatLonRect->LeftTop->LksY-llp->LksY;
									x=llp->LksX-LatLonRect->LeftTop->LksX;
									break;
								case csLatLon:
								default:
									y=LatLonRect->LeftTop->Latitude-llp->Latitude;
									x=llp->Longitude-LatLonRect->LeftTop->Longitude;
									break;
							}*/
							//y=LatLonRect->LeftTop->Latitude-llp->Latitude;
							if(LatLonRect->Height(cs)==0) y=0; //???????????
							else y/=LatLonRect->Height(cs);
							//x=llp->Longitude-LatLonRect->LeftTop->Longitude;
							if(LatLonRect->Width(cs)==0) x=0;
							else x/=LatLonRect->Width(cs);
							x*=bmr.Width();
							y*=bmr.Height();
							x+=bmr.Left;
							y+=bmr.Top;
							llp->x=x;
							llp->y=y;

							if(x<Rect.Left || j==0) Rect.Left=x;
							if(x>Rect.Right || j==0) Rect.Right=x;
							if(y<Rect.Top || j==0) Rect.Top=y;
							if(y>Rect.Bottom || j==0) Rect.Bottom=y;

							if(A<255)
							{
								line=TLine(x_old, y_old, x, y);
								if(i==List->Count-2)
								{
									direction=line.Angle()-M_PI_2;
									//direction=(direction-M_PI_2)+M_PI;
								}
								if(line.IsInRect(vpt))
								{ // draw line from x_old, y_old to x, y
									Bmp->LineAS(line.X1, line.Y1, line.X2, line.Y2, ce.ARGB);
								}
							}
							else direction=0.;
							x_old=x;
							y_old=y;
							j++;
							if(!ZeroPointDetected)
							{
								ZeroPoint=llp;
								ZeroPointDetected=true;
							}
						}
						A-=da;
					}
					if(PointGlyph && ZeroPoint)
					{
						if(vpt.Contains(Types::TPoint(ZeroPoint->x, ZeroPoint->y)))
						{
							TBitmap32 *tmp, *tmp2;

							tmp2=new TBitmap32();
							try
							{
								/*if(Pressed)
								{
									tmp=PressedPointGlyph;
									x=X1+ShiftX-dX;
									y=Y1+ShiftY-dY;
								}
								else
								{
									tmp=PointGlyph;
									x=ZeroPoint->x-(tmp->Width>>1);
									y=ZeroPoint->y-(tmp->Height>>1);
								}*/
								if(mtpState & mtpsPressed)
								{
									if(PressedPointGlyph) tmp=PressedPointGlyph;
									else tmp=PointGlyph;
									x=X1+ShiftX-dX;
									y=Y1+ShiftY-dY;
								}
								else
								{
									if((mtpState & mtpsNoGPS) && NoGpsGlyph) tmp=NoGpsGlyph;
									else if((mtpState & mtpsFollowMe) && FollowMeGlyph) tmp=FollowMeGlyph;
									else tmp=PointGlyph;

									tmp2->BeginUpdate();
									//tmp->SetSize(glyph->Height, glyph->Height);
									//tmp->DrawMode=dmBlend;
									//tmp->Clear(Color32(255,255,255,0));

									RotateBitmap32(tmp, tmp2, direction, false);
									tmp2->EndUpdate();
									tmp=tmp2;
									x=ZeroPoint->x-(tmp->Width>>1);
									y=ZeroPoint->y-(tmp->Height>>1);
								}
								Bmp->Draw(x, y, tmp);
								PointRect=Types::TRect(x, y, x+PointGlyph->Width, y+PointGlyph->Height);
								if(x<Rect.Left) Rect.Left=x;
								if(y<Rect.Top) Rect.Top=y;
								x+=tmp->Width;
								y+=tmp->Height;
								if(x>Rect.Right) Rect.Right=x;
								if(y>Rect.Bottom) Rect.Bottom=y;
							}
							__finally
							{
								delete tmp2;
                            }
						}
					}
					Bmp->EndUpdate();
				}
			}
			catch(...) {}
		}
		__finally
		{
			ResetEvent(ListInEditingEvent);
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TMapTrackPathObject::writeFollowMe(bool value)
{
	//followme=value;
	if(value)
	{
		mtpState=(TMapTrackPathState)(mtpState | mtpsFollowMe);
		ApplyFollowMe();
	}
	else
	{
		mtpState=(TMapTrackPathState)(mtpState & ~mtpsFollowMe);
		Update();
	}
}
//---------------------------------------------------------------------------

void __fastcall TMapTrackPathObject::ApplyFollowMe()
{
	if(mtpState & mtpsFollowMe)
	{
		ScrollToCoordinate(LastPoint);
	}
	else Update();
}
//---------------------------------------------------------------------------

void __fastcall TMapTrackPathObject::ScrollToCoordinate(TGPSCoordinate* llp)
{
	double x, y, w, h;
	double dx, dy;
	int bmrw, bmrh, bmpw, bmph;
	double vprw2, vprh2;
	Types::TRect bmr, vpr, gbr;
	Types::TPoint dSP;
	TDoublePoint vpac, dP;
	TLockedDoubleRect* viewportarea;
	TCoordinateSystem cs=csLocalMetric();;

	if(llp && LatLonRect && LatLonRect->Height()!=0 && LatLonRect->Width()!=0 &&
		Owner && ((TGraphicObjectsContainer *)Owner)->Image && Manager &&
		((TRadarMapManager*)Manager)->MapObjects &&
		((TRadarMapManager*)Manager)->MapObjects->Container)
	{
		bmr=((TGraphicObjectsContainer*)Owner)->Image->GetBitmapRect();
		bmrw=bmr.Width();
		bmrh=bmr.Height();

		//((TRadarMapManager*)Manager)->MapObjects->Container->CoordinatesRectToScreen(llp->)

		/*y=(double)bmrw*(LatLonRect->LeftTop->Latitude-llp->Latitude)/LatLonRect->Height();
		x=(double)bmrh*(llp->Longitude-LatLonRect->LeftTop->Longitude)/LatLonRect->Width();
		x/=((TGraphicObjectsContainer *)Owner)->Image->ScaleX;
		y/=((TGraphicObjectsContainer *)Owner)->Image->ScaleY;
		//x+=bmr.Left;
		//y+=bmr.Top;
		((TRadarMapManager*)Manager)->MapObjects->Image->ScrollToCenter(x, y);*/

		viewportarea=((TRadarMapManager*)Manager)->MapObjects->Container->ViewportArea;
		if(viewportarea)
		{
			vpr=((TGraphicObjectsContainer*)Owner)->Image->GetViewportRect();
			vprw2=(double)(vpr.Width()>>1);
			vprh2=(double)(vpr.Height()>>1);
			//vprw2=(double)vpr.Width()/2.;
			//vprh2=(double)vpr.Height()/2.;

			dSP=((TMapObjectsContainer*)Owner)->Container->CoordinatesRectToScreen(
				llp->GetXY(((TMapObjectsContainer*)Owner)->Container->CoordinateSystem)); //csMetric));

			dx=dSP.x-vprw2;
			dy=dSP.y-vprh2;

			if(dx!=0 || dy!=0)
			{
				Types::TRect mbr;
				TDoublePoint c;
				double fdx, fdy, w, h;

				mbr=((TMapObjectsContainer*)Owner)->Container->GetBitmapRect();
				bmpw=mbr.Width();
				bmph=mbr.Height();
				w=viewportarea->Width();
				h=viewportarea->Height();
				if(bmpw>0 && bmph>0 && w>0 && h>0)
				{
					c=viewportarea->Center();
					fdx=w*((double)dx/(double)bmpw);
					fdy=h*((double)dy/(double)bmph);
					//if(c.X-fdx<0.) fdx=c.X;          // Has to be commented, because the resulst is not the center of possible
					//else if(c.X-fdx>1.) fdx=c.X-1.;  // vieport, but vector how to scroll the image to the current "tracker" position
					//if(c.Y-fdy<0.) fdy=c.Y;
					//else if(c.Y-fdy>1.) fdy=c.Y-1.;
					if(fdx>0) dx=(int)((double)bmpw*fdx/w+0.5);
					else dx=(int)((double)bmpw*fdx/w-0.5);
					if(fdy>0) dy=(int)((double)bmph*fdy/h+0.5);
					else dy=(int)((double)bmph*fdy/h-0.5);
					c.X+=fdx;
					c.Y+=fdy;
					fdx=vprw2+(double)bmpw*fdx/w;
					fdy=vprh2+(double)bmph*fdy/h;
					w/=2.;
					h/=2.;
					viewportarea->Left=c.X-w;
					viewportarea->Top=c.Y-h;
					viewportarea->Right=c.X+w;
					viewportarea->Bottom=c.Y+h;
					if(fdx>0) dSP.x=(int)(fdx+0.5);
					else dSP.x=(int)(fdx-0.5);
					if(fdy>0) dSP.y=(int)(fdy+0.5);
					else dSP.y=(int)(fdy-0.5);
					if((dSP.x-bmr.Left)<vprw2 || (bmr.Right-dSP.x)<vprw2 ||	(dSP.y-bmr.Top)<vprh2 || (bmr.Bottom-dSP.y)<vprh2)
					{
						((TMapObjectsContainer*)Owner)->Container->ZoomUpdate();
						dSP.x=(double)bmrw/2.;
						dSP.y=(double)bmrh/2.;
						((TGraphicObjectsContainer*)Owner)->Image->ScrollToCenter(dSP.x, dSP.y);
					}
					else ((TGraphicObjectsContainer*)Owner)->Image->Scroll(dx, dy);
					((TGraphicObjectsContainer*)Owner)->Image->OnScroll(NULL);
				}
			}
		}
		else
		{
			/*dP=LatLonRect->Center(csMetric);
			dx=(llp->GetX(csMetric)-dP.X)/LatLonRect->Width(csMetric)*(double)bmrw;
			dy=-(llp->GetY(csMetric)-dP.Y)/LatLonRect->Height(csMetric)*(double)bmrh;*/
			dP=LatLonRect->Center(cs);//csMetric);
			dx=(llp->GetX(cs)-dP.X)/LatLonRect->Width(cs)*(double)bmrw;
			dy=-(llp->GetY(cs)-dP.Y)/LatLonRect->Height(cs)*(double)bmrh;
			if(dx!=0 || dy!=0)
			{
				((TGraphicObjectsContainer*)Owner)->Image->Scroll(dx, dy);
				((TGraphicObjectsContainer*)Owner)->Image->OnScroll(NULL);
			}
		}/**/
	}
}
//---------------------------------------------------------------------------

void __fastcall TMapTrackPathObject::MouseDown(TObject *Sender,
	TMouseButton Button, TShiftState Shift, int X, int Y, TCustomLayer *Layer)
{
	if(PointRect.Contains(Types::TPoint(X, Y)))
	{
		dX=X1-(PointRect.left);//+(PointRect.Width()>>1));
		dY=Y1-(PointRect.top);//+(PointRect.Height()>>1));
		ShiftX=0;
		ShiftY=0;
	}
	else
	{
		InRect=false;
		Pressed=false;
	}
}
//---------------------------------------------------------------------------

void __fastcall TMapTrackPathObject::MouseMove(System::TObject* Sender, Classes::TShiftState Shift,
	int X, int Y, TCustomLayer *Layer)
{
	if(Pressed)
	{
		if(X-X1!=ShiftX || Y-Y1!=ShiftY)
		{
			ShiftX=X-X1;
			ShiftY=Y-Y1;
			Update();
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TMapTrackPathObject::MouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift,
	int X, int Y, TCustomLayer *Layer)
{
	if(GpsUnit && Pressed)
	{
		double x, y;
		Types::TRect bmr;
		TCoordinateSystem cs=csLocalMetric();

		if(ZeroPoint)
		{
			x=X-ZeroPoint->x-dX;
			y=Y-ZeroPoint->y-dY;
		}
		else
		{
			x=X-X1-dX;
			y=Y-Y1-dY;
		}
		bmr=((TGraphicObjectsContainer *)Owner)->Image->GetBitmapRect();
		if(bmr.Width()>0 && bmr.Height()>0 && (x!=0 || y!=0))
		{
			x/=(double)bmr.Width();
			y/=(double)bmr.Height();
			x*=LatLonRect->Width(cs);//csMetric); //in deg
			y*=-LatLonRect->Height(cs);//csMetric); //in deg
			//y/=cos((LatLonRect->LeftTop->Latitude+LatLonRect->RightBottom->Latitude)/2.);
			GpsUnit->AddOffset(x, y);
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TMapTrackPathObject::writeGpsUnit(TGpsUnit* value)
{
	if(value!=gpsunit)
	{
		gpsunit=value;
		//mouseable=(value!=NULL);
	}
}
//---------------------------------------------------------------------------

bool __fastcall TMapTrackPathObject::readMouseable()
{
	if(owner && owner->ObjectType==gotContainer && ((TGraphicObjectsContainer*)owner)->MouseAction)
	{
		mouseable=(*((TGraphicObjectsContainer*)owner)->MouseAction==tmaPosCorrection);
	}/**/
	return mouseable;
}
//---------------------------------------------------------------------------

void __fastcall TMapTrackPathObject::writeStartWayPoint(TStartStopWaypointLabel *value)
{
	if(value!=startwaypoint && (!value || value->Kind==swkStart))
	{
		TStartStopWaypointLabel *tmp=startwaypoint;

		startwaypoint=value;
		if(tmp && tmp->Path==this) tmp->Path=NULL;
		if(value) value->Path=this;
	}
}
//---------------------------------------------------------------------------

void __fastcall TMapTrackPathObject::writeStopWayPoint(TStartStopWaypointLabel *value)
{
	if(value!=stopwaypoint && (!value || value->Kind==swkStop))
	{
		TStartStopWaypointLabel *tmp=stopwaypoint;

		stopwaypoint=value;
		if(tmp && tmp->Path==this) tmp->Path=NULL;
		if(value) value->Path=this;
	}
}

//---------------------------------------------------------------------------
// TMapLabel
//---------------------------------------------------------------------------
__fastcall TMapLabel::TMapLabel(TMyObject *AOwner, Types::TRect OutputRect,
	TProfileLabel *ConnectedPtr, TGPSCoordinatesRect *cRect, TObject* HostPath,
	TObject* RadarMapManager, TMapLabelType mlt)
	: TCustomProfileLabel(AOwner, OutputRect, ConnectedPtr->OutputView, ConnectedPtr->Xx, ConnectedPtr->Yy, RadarMapManager)
{
	int i;

	Closing=false;
	spiderweb=false;
	Geometries=new TMyObjectsList(this);
	Geometries->ObjectsReIndexing=false;
	Geometries->FreeObjects=false;
	activegeometry=NULL;
	SpiderYarn=new TMyObjectsList(this);
	SpiderYarn->ObjectsReIndexing=false;
	SpiderYarn->FreeObjects=false;
	ConnectedObject=ConnectedPtr;
	coordinate=NULL;
	maplabeltype=mlt;
	if(ConnectedPtr)
	{
		//if(ConnectedPtr->Xx==0 && ConnectedPtr->Yy==0)
		TraceIndex=ConnectedPtr->TraceIndex;
		ProfileIndex=ConnectedPtr->ProfileIndex;
		Tag=ConnectedPtr->Tag;
		Color=ConnectedPtr->Color;
		labeltype=ConnectedObject->ProfileLabelType;
		//NormalGlyph=((TProfileLabel*)ConnectedObject)->SmallGlyph;
		NormalGlyphName=((TProfileLabel*)ConnectedObject)->SmallGlyphName;
		if(NormalGlyph)
		{
			if(ConnectedPtr->NormalGlyph)
			{
				i=(int)((float)ConnectedPtr->Center.x*(float)NormalGlyph->Width/(float)ConnectedPtr->NormalGlyph->Width);
				center.x=i;
				i=(int)((float)ConnectedPtr->Center.y*(float)NormalGlyph->Height/(float)ConnectedPtr->NormalGlyph->Height);
				center.y=i;
			}
			else center=Types::TPoint(NormalGlyph->Width>>1, NormalGlyph->Height>>1);
		}
		else center=Types::TPoint(0, 0);
		if(ID==((TRadarMapManager*)Manager)->LastLabelID) ((TRadarMapManager*)Manager)->LastLabelID--;
		ID=ConnectedPtr->ID;
	}
	CoordinatesRect=cRect;
	Rect=Types::TRect(0, 0, 0, 0);
	profileobjecttype=potMapLabel;
	host=HostPath;
	icons=true;
}
//---------------------------------------------------------------------------

__fastcall TMapLabel::TMapLabel(TMyObject *AOwner, Types::TRect OutputRect,
	TGPSCoordinatesRect *cRect, TObject* aHost, TObject* RadarMapManager,
	TMapLabelType mlt) : TCustomProfileLabel(AOwner, OutputRect, NULL, 0, 0, RadarMapManager)
{
	Closing=false;
	spiderweb=false;
	Geometries=new TMyObjectsList(this);
	Geometries->ObjectsReIndexing=false;
	Geometries->FreeObjects=false;
	activegeometry=NULL;
	SpiderYarn=new TMyObjectsList(this);
	SpiderYarn->ObjectsReIndexing=false;
	SpiderYarn->FreeObjects=false;
	SpiderYarn->FreeObjects=false;
	maplabeltype=mlt;
	ConnectedObject=NULL;
	CoordinatesRect=cRect;
	Rect=Types::TRect(0, 0, 0, 0);
	profileobjecttype=potMapLabel;
	host=aHost;
	icons=true;
	coordinate=NULL;
	notification=NULL;
	showpopup=true;
}
//---------------------------------------------------------------------------

__fastcall TMapLabel::~TMapLabel()
{
	try
	{
		Closing=true;
		if(SuicideTimerItem) SuicideTimerItem->OnTimer=NULL;
		SuicideTimerItem=NULL;
		if(notification) delete notification;
		notification=NULL;
		SpiderWeb=false;
		if(Geometries)
		{
			TMapLabelGeometry *mlg;

			for(int i=0; i<Geometries->Count ; i++)
			{
				mlg=dynamic_cast<TMapLabelGeometry*>(Geometries->Items[i]);
				Geometries->Items[i]=NULL;
				if(mlg)
				{
					mlg->DeletePoint(this);
					if(mlg->VertexesCount<mlg->MinVertexesToDraw)
						delete mlg;
				}
			}
			delete Geometries;
		}
		Geometries=NULL;
		if(SpiderYarn) delete SpiderYarn;
		SpiderYarn=NULL;
	}
	catch(...) {}
}
//---------------------------------------------------------------------------

void __fastcall TMapLabel::writeTraceIndex(int value)
{
	if(traceindex!=value || !coordinate)
	{
		traceindex=value;
		if(coordinate)
		{
			delete coordinate;
			coordinate=NULL;
		}
		if(OutputView!=NULL)
		{
			trace=OutputView->GetTraceByIndex(traceindex, profileindex);
			if(trace)
			{
				coordinate=new TGPSCoordinate(NULL, trace);
				gpsconfidence=trace->GPSConfidence;
			}
		}
		if(connectedobject) connectedobject->TraceIndex=value;
		Update();
		if(Manager) ((TRadarMapManager*)Manager)->ResultsNotSaved=true;
		//if(connectedobject) connectedobject->Update();
	}
}
//---------------------------------------------------------------------------

void __fastcall TMapLabel::Suicide() // !!! Unsafe !!!
{
	try
	{
		if(!Closing)
		{
			if(LabelInfoForm->Label==this) LabelInfoForm->Label=NULL;
			SuicideTimerItem=NULL;
			delete this;
		}
	}
	catch(...) {}
}
//---------------------------------------------------------------------------

void __fastcall TMapLabel::SelfDestruction(unsigned long Delay)
{
	if(!Closing)
	{
		if(GlobalTimer)
		{
			if(SuicideTimerItem)
			{
				SuicideTimerItem->Start_tick=GlobalTimer->TickCounter;
				SuicideTimerItem->Interval=Delay;
			}
			else SuicideTimerItem=GlobalTimer->AddTimer(Delay, Suicide, true);
		}
		else
		{
			new TRunNForgetThread(Suicide, true, Delay);
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TMapLabel::PostponeSelfDestruction()
{
	if(!Closing && GlobalTimer && SuicideTimerItem)
		SuicideTimerItem->Start_tick=GlobalTimer->TickCounter;
}
//---------------------------------------------------------------------------

Types::TPoint __fastcall TMapLabel::GetScreenXY()
{
	Types::TPoint res;

	if(Coordinate && ((TMapObjectsContainer*)Owner)->Container)
		res=((TMapObjectsContainer*)Owner)->Container->CoordinatesRectToScreen(
			Coordinate->GetXY(((TMapObjectsContainer*)Owner)->Container->CoordinateSystem));
	else res=Types::TPoint(0,0);

	return res;
}
//---------------------------------------------------------------------------

void __fastcall TMapLabel::MouseUp(TObject *Sender, TMouseButton Button,
	TShiftState Shift, int X, int Y, TCustomLayer *Layer)
{
	if(Button==mbLeft && Pressed)
	{
		if(*((TGraphicObjectsContainer*)owner)->MouseAction==tmaMapLabelPolyLineGraphics)
		{
			SpiderWeb=!SpiderWeb;
		}
		else
		{
			if(PopedUp) PopDown();
			else PopUp();
		}
	}
	else TCustomProfileLabel::MouseUp(Sender, Button, Shift, X, Y, Layer);
}
//---------------------------------------------------------------------------

void __fastcall TMapLabel::writeSpiderWeb(bool value)
{
	TList *list;
	TMapLabel *ml, *ml2;
	TDoublePoint dp, dp2;
	Types::TRect vpt;
	TFloatLine fl;
	TCoordinateSystem cs;
	double d, d2, *mlds=NULL;
	TColor32Entry ce;
	bool *Angles;
	int n, k, AnglesN;
	float f;
	TMapLabelType mlt;
	TProfileLabelType plt;
	TMapLabelGeometry *geometry;

	if(value!=spiderweb)
	{
		if(value)
		{
			if(Owner && ((TMapObjectsContainer *)Owner)->Image && ((TMapObjectsContainer*)Owner)->Container &&
				Coordinate && Coordinate->Valid && Manager && ((TRadarMapManager*)Manager)->Settings)
			{
				vpt=((TMapObjectsContainer *)Owner)->Image->GetViewportRect();
				cs=((TMapObjectsContainer*)Owner)->Container->CoordinateSystem;
				AnglesN=(int)(360./((TRadarMapManager*)Manager)->Settings->MapLabelSpiderBeamsSector)+1;
				//if(Coordinate->GetCSDimension(cs)!=csdMeters) cs=csMetric;
				fl.XY1=DoublePoint(((TMapObjectsContainer*)Owner)->Container->CoordinatesRectToScreen(Coordinate->GetXY(cs)));
				if(vpt.Contains(fl.XY1.AsTPoint()))
				{
					try
					{
						list=new TList();
						if(((TRadarMapManager*)Manager)->Settings->SingleTypeMapLabelsGeometry)
						{
							mlt=MapLabelType;
							plt=ProfileLabelType;
						}
						else
						{
							mlt=mltNone;
							plt=pltNone;
						}
						if(((TMapObjectsContainer*)Owner)->GetMapLabels(list, mlt, plt)>0) //don't free objects, because GetMapLabels return active MapLabels list!!!
						{
							d=-1;
							n=0;
							ml2=NULL;
							mlds=new double[list->Count];
							Angles=new bool[AnglesN];
							for(int i=0; i<AnglesN; i++)
								Angles[i]=false;
							for(int i=0; i<list->Count; i++)
							{
								ml=dynamic_cast<TMapLabel*>((TObject*)list->Items[i]);
								if(ml && ml!=this && ml->Coordinate && ml->Coordinate->Valid)
								{
									if(ml->ActiveGeometry && ml->ActiveGeometry->IndexOf(this)>=0 && ml->SpiderWeb)
									{
										value=false;
										ml2=NULL;
										n=0;
										d=-1;
										break;
									}
									else
									{
										fl.XY2=DoublePoint(((TMapObjectsContainer*)Owner)->Container->CoordinatesRectToScreen(ml->Coordinate->GetXY(cs)));
										if(vpt.Contains(fl.XY2.AsTPoint()))
										{
											mlds[i]=fl.Length();
											if(d<0 || d>mlds[i])
											{
												d=mlds[i];
											}
											n++;
										}
										else mlds[i]=-1;
										if(ml->SpiderWeb)
										{
											ml2=ml;
											ml->SpiderWeb=false;
										}
									}
								}
								else mlds[i]=-1;
							}
							if(ml2)
							{
								//construct draw the line
								if(ml2->ActiveGeometry) geometry=ml2->ActiveGeometry;
								else
								{
									switch(*((TGraphicObjectsContainer*)owner)->MouseAction)
									{
										case tmaMapLabelPolyLineGraphics:
											geometry=new TMapLabelPolyLineGeometry(Owner, Output, ((TMapObjectsContainer*)Owner)->Container, Manager);
											((TMapObjectsContainer*)Owner)->CustomObjectsList->SendToBack(geometry->Index);
											geometry->SingleLabelType=((TRadarMapManager*)Manager)->Settings->SingleTypeMapLabelsGeometry;
											//geometry->LabelType=plt;
											//geometry->MapLabelType=mlt;
											ml2->AddGeometry(geometry);
											ce.ARGB=Color;
											ce.A=(System::Byte)((float)ce.A*0.85);
											geometry->Color=ce.ARGB;
											ce.ARGB=Color;
											ce.A=(System::Byte)((float)ce.A*0.85);
											ce.R=ce.R>>1;
											ce.G=ce.G>>1;
											ce.B=ce.B>>1;
											geometry->BorderColor=ce.ARGB;
											break;
										default:
											geometry=NULL;
									}
								}
								if(geometry) AddGeometry(geometry);
							}
							/*if(n>((TRadarMapManager*)Manager)->Settings->MaxMapLabelSpiderBeams)
								n=((TRadarMapManager*)Manager)->Settings->MaxMapLabelSpiderBeams;*/
							while(n>0 && d>=0)
							{
								d2=-1;
								for(int i=0; i<list->Count; i++)
								{
									if(mlds[i]>=0)
									{
										if(mlds[i]==d)
										{
											ml=dynamic_cast<TMapLabel*>((TObject*)list->Items[i]);
											if(ml && ml!=this &&  ml->Coordinate && ml->Coordinate->Valid)
											{
												fl.XY2=DoublePoint(((TMapObjectsContainer*)Owner)->Container->CoordinatesRectToScreen(ml->Coordinate->GetXY(cs)));
												f=fl.Angle();
												if(f<0) while(f<0) f+=M_PI2;
												else while(f>M_PI2) f-=M_PI2;
												k=(int)((f*180./M_PI)/((TRadarMapManager*)Manager)->Settings->MapLabelSpiderBeamsSector);
												if(!Angles[k] && (!ActiveGeometry || ml->ActiveGeometry!=ActiveGeometry))
												{
													geometry=new TMapLabelLineGeometry(Owner, Output, ((TMapObjectsContainer*)Owner)->Container, Manager);
													((TRadarMapManager*)Manager)->Settings->MapLabelGeometryIDs--;
													((TMapObjectsContainer*)Owner)->CustomObjectsList->SendToBack(geometry->Index);
													SpiderYarn->Add(geometry);
													ce.ARGB=Color;
													ce.A=ce.A>>2;
													geometry->Color=ce.ARGB;
													ce.ARGB=Color;
													ce.A=ce.A>>3;
													ce.R=ce.R>>1;
													ce.G=ce.G>>1;
													ce.B=ce.B>>1;
													geometry->BorderColor=ce.ARGB;
													geometry->AddPoint(this);
													geometry->AddPoint(ml);
													n--;
													Angles[k]=true;
													if(n==0) break;
												}
												mlds[i]=-1;
											}
										}
										else if(d2<0 || d2>mlds[i]) d2=mlds[i];
									}
								}
								d=d2;
							}
						}
						else value=false;
					}
					__finally
					{
						list->Clear();
						delete list;
						if(mlds) delete[] mlds;
					}
				}
			}
		}
		else ClearSpiderYarn();
		spiderweb=value;
		//Update();
	}
}
//---------------------------------------------------------------------------

void __fastcall TMapLabel::ClearSpiderYarn()
{
	TMapLabelGeometry *geometry;

	for(int i=0; i<SpiderYarn->Count; i++)
	{
		geometry=dynamic_cast<TMapLabelGeometry*>(SpiderYarn->Items[i]);
		if(geometry)
		{
			if(Geometries->IndexOf(geometry)>=0) Geometries->Delete(geometry);
			delete geometry;
			SpiderYarn->Items[i]=NULL;
		}
	}
	SpiderYarn->Clear();
}
//---------------------------------------------------------------------------

void __fastcall TMapLabel::AddGeometry(TMapLabelGeometry *geometry)
{
	if(geometry)
	{
		if(Geometries->IndexOf(geometry)<0)
		{
			Geometries->Add(geometry);
			geometry->AddPoint(this);
		}
		if(SpiderYarn->IndexOf(geometry)<0)
			activegeometry=geometry;
	}
}
//---------------------------------------------------------------------------

void __fastcall TMapLabel::DeleteGeometry(TMapLabelGeometry *geometry)
{
	if(geometry && Geometries->IndexOf(geometry)>=0)
	{
		Geometries->Delete(geometry);
		if(SpiderYarn->IndexOf(geometry)>=0)
			SpiderYarn->Delete(geometry);
		if(activegeometry==geometry) activegeometry=NULL;
	}
}
//---------------------------------------------------------------------------

void __fastcall TMapLabel::PopUp()
{
	if(Manager && ((TRadarMapManager*)Manager)->Settings &&
		((TRadarMapManager*)Manager)->Settings->ShowMapLabelNotification &&
		((TRadarMapManager*)Manager)->MapObjects &&
		((TRadarMapManager*)Manager)->MapObjects->Image &&
		((TRadarMapManager*)Manager)->MapObjects->Container)
	{
		TDoublePoint dp;

		if(!notification)
		{
			if(Coordinate)
			{
				notification=new TPosNotificationObject(((TRadarMapManager*)Manager)->MapObjects,
					((TRadarMapManager*)Manager)->MapObjects->Image,
					((TRadarMapManager*)Manager)->MapObjects->Image->GetViewportRect(),
					mvtConstant, 5000, color, "", Coordinate, Manager);
				notification->GlobalTimer=GlobalTimer;
			}
		}
		if(notification)
		{
			if(showpopup)
			{
				notification->Strings->Clear();
				if(Coordinate)
					notification->Strings->Add(Coordinate->AsString(
						((TRadarMapManager*)Manager)->MapObjects->Container->CoordinateSystem, false));
				if(Description!="" && DescriptionStrs && DescriptionStrs->Count>0)
				{
					for(int i=0; i<DescriptionStrs->Count; i++)
						notification->Strings->Add(DescriptionStrs->Strings[i]);
				}
				else
				{
					notification->Strings->Insert(0, "ID #"+IntToStr((int)ID));
					notification->Strings->Add("Created "+MyDateTimeToStr(CreatedTime));
					notification->Strings->Add("Modified "+MyDateTimeToStr(ModifiedTime));
				}
				//notification->Visible=!notification->Visible;
				if(!notification->Visible) notification->PopUp();
				//else notification->PopDown();
			}
			else notification->PopDown();//notification->Visible=false;
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TMapLabel::Redraw(TObject *Sender, TBitmap32 *Bmp)
{
	TGPSCoordinate *llp;
	TBitmap32 *tmp;
	Types::TRect bRect, vRect;
	Types::TPoint p;
	TCoordinateSystem cs;

	if(Bmp && Owner && ((TMapObjectsContainer*)Owner)->Image && CoordinatesRect && fabs(CoordinatesRect->Width())>0. &&
		fabs(CoordinatesRect->Height())>0. && Output.Width()!=0 && Output.Height()!=0)
	{
		llp=coordinate;
		/*llp->Latitude=DegMinToDeg(llp->Latitude);
		llp->Longitude=DegMinToDeg(llp->Longitude);*/
		if(llp)
		{
			tmp=new TBitmap32();
			try
			{
				try
				{
					tmp->DrawMode=dmBlend;
					vRect=((TMapObjectsContainer*)Owner)->Image->GetViewportRect();

					bRect=((TMapObjectsContainer*)Owner)->Image->GetBitmapRect();
                    cs=((TMapObjectsContainer*)Owner)->Container->CoordinateSystem;
					//y=bRect.Top+(int)((float)bRect.Height()*(CoordinatesRect->LeftTop->Latitude-llp->Latitude)/(float)CoordinatesRect->Height());
					//x=bRect.Left+(int)((float)bRect.Width()*(llp->Longitude-CoordinatesRect->LeftTop->Longitude)/(float)CoordinatesRect->Width());

					/*double cw, ch;
					int x, y;

					if(((TMapObjectsContainer*)Owner)->Container) cs=((TMapObjectsContainer*)Owner)->Container->CoordinateSystem;
					else
					{
						if(Manager && ((TRadarMapManager*)Manager)->Settings) cs=((TRadarMapManager*)Manager)->Settings->DefaultCoordinateSystem;
						else cs=csLatLon;
					}
					if(cs==csLatLon)
					{
						ch=CoordinatesRect->LeftTop->Latitude-llp->Latitude;
						cw=llp->Longitude-CoordinatesRect->LeftTop->Longitude;
					}
					else
					{
						ch=CoordinatesRect->LeftTop->GetY(cs)-llp->GetY(cs);
						cw=llp->GetX(cs)-CoordinatesRect->LeftTop->GetX(cs);
					}
					y=bRect.Top+(int)(0.5+(double)bRect.Height()*ch/CoordinatesRect->Height(cs));
					x=bRect.Left+(int)(0.5+(double)bRect.Width()*cw/CoordinatesRect->Width(cs));*/

					p=GetScreenXY();
					if(vRect.Contains(Types::TPoint(p.x, p.y)))
					{
						if(Glyph==NULL) Glyph=NormalGlyph;
						tmp->BeginUpdate();
						try
						{
							if(Glyph!=NULL)
							{
								tmp->SetSizeFrom(Glyph);
								tmp->Draw(0, 0, Glyph);
							}
							else
							{
								tmp->SetSize(16, 16);
								tmp->FillRectTS(0, 0, 16, 16, Color);
							}
						}
						__finally
						{
							tmp->EndUpdate();
						}
						if(Manager && ((TRadarMapManager*)Manager)->Settings &&
							((TRadarMapManager*)Manager)->Settings->ShowLabelConfidence &&
							GPSConfidence>0)
						{
							TColor32Entry ce1, ce2;
							float rx, ry;

							ce1.ARGB=ce2.ARGB=Color;
							if(ce1.A<128)
							{
								ce1.A=128;
								ce2.A=128;
							}
							ce1.A=(float)ce1.A*0.4;
							ce2.A=(float)ce2.A*0.15;
							//ry=rx=(double)bRect.Width()*(double)GPSConfidence/CoordinatesRect->Width((cs==csLatLon ? csLocalMetric() : cs));
							ry=rx=(double)bRect.Width()*(double)GPSConfidence/CoordinatesRect->WidthInMeters((cs==csLatLon ? csLocalMetric() : cs));
							DrawEllipse32(Bmp, p.x, p.y, rx, ry, vRect, ce1.ARGB, ce2.ARGB, edOutlineFill);
						}
						p.x-=center.x;//tmp->Width>>1;
						p.y-=center.y;//tmp->Height>>1;
						Rect=Types::TRect(p.x, p.y, p.x+tmp->Width, p.y+tmp->Height);
						Bmp->Draw(p.x, p.y, tmp);
						TCustomProfileLabel::IconsDraw(Bmp);
					}
					else
					{
						Rect=Types::TRect(0,0,0,0);
					}
				}
				catch(...) {}
			}
			__finally
			{
				delete tmp;
			}
		}
	}

}

//---------------------------------------------------------------------------
// TPinLabel
//---------------------------------------------------------------------------
__fastcall TPinLabel::TPinLabel(TMyObject *AOwner, Types::TRect OutputRect,
	TGPSCoordinatesRect *cRect, bool GetTrackCoordinate, TObject* RadarMapManager) :
	TMapLabel(AOwner, OutputRect, cRect, NULL, RadarMapManager, mltPinLabel)
{
	NormalGlyphName="PNGIMAGE_156";
	if(NormalGlyph) Center=Types::TPoint(11, 28);//(16, 28);//Types::TPoint(ml->NormalGlyph->Width>>1,  ml->NormalGlyph->Height);
	else Center=Types::TPoint(0, 0);
	AllwaysHot=true;
	Icons=true;
	if(GetTrackCoordinate && Manager && ((TRadarMapManager*)Manager)->TrackingPath)
	{
		Coordinate=new TGPSCoordinate(((TRadarMapManager*)Manager)->TrackingPath->LastPoint);
		GPSConfidence=Coordinate->Confidence;
	}
	Color=Color32(169, 2, 85, 255);//clRed32;
}

//---------------------------------------------------------------------------
// TColorMaskLabel
//---------------------------------------------------------------------------
__fastcall TColorMaskLabel::TColorMaskLabel(TMyObject *AOwner, Types::TRect OutputRect,
	TGPSCoordinatesRect *cRect, TColor32 aColor, Types::TPoint aSP,
	TObject* RadarMapManager, TMapLabelType mlt) : TMapLabel(AOwner, OutputRect, cRect, NULL,
	RadarMapManager, mlt)
{
	Initialize(aColor);
	sp=aSP;
	if(Manager && ((TRadarMapManager*)Manager)->MapObjects &&
		((TRadarMapManager*)Manager)->MapObjects->Container && (aSP.x!=0 || aSP.y!=0) &&
		CoordinatesRect && CoordinatesRect->Valid)
	{
		TMapsContainersCollection *Container=((TRadarMapManager*)Manager)->MapObjects->Container;

		StartingVPP=Container->ScreenToViewportArea(aSP);
		Coordinate=new TGPSCoordinate();
		Coordinate->SetXY(Container->CoordinateSystem,
			Container->ViewportAreaToCoordinatesRect(StartingVPP));
		GPSConfidence=Coordinate->Confidence;
	}
	else
	{
		StartingVPP=DoublePoint(0,0);
		Coordinate=NULL;
		Rect=Types::TRect(0,0,0,0);
	}
}
//---------------------------------------------------------------------------

__fastcall TColorMaskLabel::TColorMaskLabel(TMyObject *AOwner, Types::TRect OutputRect,
	TGPSCoordinatesRect *cRect, TColor32 aColor, TGPSCoordinate *aC,
	TObject* RadarMapManager, TMapLabelType mlt) : TMapLabel(AOwner, OutputRect, cRect, NULL,
	RadarMapManager, mlt)
{
	Initialize(aColor);
	sp=Types::TPoint(0,0);
	if(Manager && ((TRadarMapManager*)Manager)->MapObjects &&
		((TRadarMapManager*)Manager)->MapObjects->Container && aC && aC->Valid &&
		CoordinatesRect && CoordinatesRect->Valid)
	{
		TMapsContainersCollection *Container=((TRadarMapManager*)Manager)->MapObjects->Container;

		if(coordinate) delete coordinate;
		coordinate=new TGPSCoordinate(aC);
		StartingVPP=Container->CoordinatesRectToViewportArea(aC->GetXY(Container->CoordinateSystem));
		sp=Container->ViewportAreaToScreen(StartingVPP);
		gpsconfidence=Coordinate->Confidence;
	}
	else
	{
		StartingVPP=DoublePoint(0,0);
		Coordinate=NULL;
		Rect=Types::TRect(0,0,0,0);
	}
}
//---------------------------------------------------------------------------

void __fastcall TColorMaskLabel::Initialize(TColor32 aColor)
{
	NormalGlyphName="PNGIMAGE_461";
	color=aColor;
	ColoredBmp=new TBitmap32;
	ColoredBmp->DrawMode=dmBlend;
	if(NormalGlyph)
	{
		ColoredBmp->Assign(NormalGlyph);
		ApplyColorMask(ColoredBmp, color);
	}
	else
	{
		ColoredBmp->SetSize(12, 12);
		ColoredBmp->Clear(Color);
	}
	center=Types::TPoint((ColoredBmp->Width>>1)-1, (ColoredBmp->Height>>1)-1);
	allwayshot=true;
	icons=false;//true;
}
//---------------------------------------------------------------------------

void __fastcall TColorMaskLabel::MouseUp(TObject *Sender, TMouseButton Button,
	TShiftState Shift, int X, int Y, TCustomLayer *Layer)
{
	if(!notification && Coordinate) TMapLabel::MouseUp(Sender, Button, Shift, X, Y, Layer);
	else if(Button==mbLeft && Pressed) TMapLabel::MouseUp(Sender, Button, Shift, X, Y, Layer);
	/*{
		if(Manager && ((TRadarMapManager*)Manager)->Settings &&
			((TRadarMapManager*)Manager)->Settings->ShowMapLabelNotification &&
			((TRadarMapManager*)Manager)->MapObjects &&
			((TRadarMapManager*)Manager)->MapObjects->Image &&
			((TRadarMapManager*)Manager)->MapObjects->Container)
		{
			TDoublePoint dp;

			if(!notification && (StartingVPP.X!=0 || StartingVPP.Y!=0))
				notification=new TPosNotificationObject(((TRadarMapManager*)Manager)->MapObjects,
						((TRadarMapManager*)Manager)->MapObjects->Image,
						((TRadarMapManager*)Manager)->MapObjects->Image->GetViewportRect(),
						mvtConstant, 5000, color, "", StartingVPP, Manager);
			if(notification)
			{
				if(showpopup)
				{
					notification->Strings->Clear();
					notification->Strings->Add("ID #"+IntToStr((int)ID));
					if(Coordinate)
						notification->Strings->Add(Coordinate->AsString(
							((TRadarMapManager*)Manager)->MapObjects->Container->CoordinateSystem, false));
					notification->Strings->Add("Created "+MyDateTimeToStr(CreatedTime));
					notification->Strings->Add("Modified "+MyDateTimeToStr(ModifiedTime));
					if(!notification->Visible) notification->PopUp();
					else notification->PopDown();
					if(Description!="") notification->Strings->Add(Description);
				}
				else notification->PopDown();//notification->Visible=false;
			}
		}
	}*/
	else TCustomProfileLabel::MouseUp(Sender, Button, Shift, X, Y, Layer);
}
//---------------------------------------------------------------------------

void __fastcall TColorMaskLabel::Redraw(TObject *Sender, TBitmap32 *Bmp)
{
	TBitmap32 *tmp;
	Types::TRect vRect;
	Types::TPoint dp;

	if(Bmp && Owner && ((TMapObjectsContainer*)Owner)->Image && ColoredBmp)
	{
		try
		{
			vRect=((TMapObjectsContainer*)Owner)->Image->GetViewportRect();
			if(Coordinate && ((TMapObjectsContainer*)Owner)->Container)
				dp=((TMapObjectsContainer*)Owner)->Container->CoordinatesRectToScreen(
					Coordinate->GetXY(((TMapObjectsContainer*)Owner)->Container->CoordinateSystem));
			else dp=((TMapObjectsContainer*)Owner)->Container->ViewportAreaToScreen(StartingVPP);
			if(vRect.Contains(dp))
			{
				dp.x-=center.x;//tmp->Width>>1;
				dp.y-=center.y;//tmp->Height>>1;
				Rect=Types::TRect(dp.x, dp.y, dp.x+ColoredBmp->Width, dp.y+ColoredBmp->Height);
				Bmp->Draw(dp.x, dp.y, ColoredBmp);
				TCustomProfileLabel::IconsDraw(Bmp);
			}
			else
			{
				Rect=Types::TRect(0,0,0,0);
			}
		}
		catch(...) {}
	}
	else Rect=Types::TRect(0,0,0,0);
}

//---------------------------------------------------------------------------
// TColorPredictedLabel
//---------------------------------------------------------------------------
void __fastcall TColorPredictedLabel::Initialize(TMultiTimerThread *gTimer, unsigned long aLifeTime, TGPSCoordinate *aC)
{
	TMapsContainersCollection *Collection;
	TMapsContainer *mc;

	if(aC)
	{
		Collection=((TRadarMapManager*)Manager)->MapObjects->Container;
		if(Collection->Count>0)
		{
			mc=Collection->GetObject(0);
			if(mc && mc->PredictedCoordinatesList)
			{
				if(mc->PredictedCoordinatesList->AddUniq(aC, mc->CoordinateSystem)>=0)
					aC->ConnectedPtr=(void*)this;
			}
		}
	}
	GlobalTimer=gTimer;
	layerid=-1;
	SelfDestruction(aLifeTime);
}
//---------------------------------------------------------------------------

__fastcall TColorPredictedLabel::~TColorPredictedLabel()
{
	TMapsContainersCollection *Collection;
	TMapsContainer *mc;

	try
	{
		if(Coordinate)
		{
			Collection=((TRadarMapManager*)Manager)->MapObjects->Container;
			if(Collection->Count>0)
			{
				mc=Collection->GetObject(0);
				if(mc && mc->PredictedCoordinatesList)
					mc->PredictedCoordinatesList->DeleteUniq(Coordinate, mc->CoordinateSystem);
			}
		}
	}
    catch(...) {}
}

//---------------------------------------------------------------------------
// TReferencesCollector
//---------------------------------------------------------------------------
__fastcall TReferencesCollector::TReferencesCollector(TCustomProfileLabel *AOwner,
	int ReferencesQ, TStrings* APercentageImagePaths, AnsiString aIdleImage, unsigned long IdleTime, //in ms
	TObject* RadarMapManager)
{
	Manager=RadarMapManager;
	References=new TList();
	Owner=AOwner;
	coordinate=NULL;
	referencesq=ReferencesQ;
	iconindex=0;
	percentage=0;
	oncollected=NULL;
	onpercentage=NULL;
	iscollecting=false;
	PercentageImagePaths=APercentageImagePaths;
	if(Owner && PercentageImagePaths && PercentageImagePaths->Count>0)
	{
		Owner->NormalGlyphName=PercentageImagePaths->Strings[iconindex];
	}
	if(aIdleImage!=NULL && aIdleImage!="" && IdleTime!=IGNORE && IdleTime!=INFINITE)
	{
		IdleImage=aIdleImage;
		timer=new TTimerThread(IdleTime);
		timer->Stop();
	}
	else timer=NULL;
	StartReferencesCollection();
}
//---------------------------------------------------------------------------

__fastcall TReferencesCollector::~TReferencesCollector()
{
    if(timer)
	{
		timer->OnTimer=NULL;
		timer->AskForTerminate();
		if(timer->ForceTerminate()) delete timer;
		timer=NULL;
	}
	if(GpsListener)
	{
		if(Manager && ((TRadarMapManager*)Manager)->GpsListeners)
			((TRadarMapManager*)Manager)->GpsListeners->Delete(GpsListener);
		delete GpsListener;
		GpsListener=NULL;
	}
	if(References)
	{
		ClearReferences();
		delete References;
		References=NULL;
	}
	if(coordinate)
	{
		delete coordinate;
		coordinate=NULL;
	}
}
//---------------------------------------------------------------------------

void __fastcall TReferencesCollector::AddReference(TGPSCoordinate* c) //if valid c, then add it to the List and calcul the percentage state image and change it. if List is full then "SetCoordinate(...)".
{
	double lat, lon, z, zg, conf, x, y, d, dd=0.;
	int ii, n, rq;
	TGPSCoordinate* cc;
	TCoordinateSystem cs=csLocalMetric();
	TDoublePoint uv;

	if(c && References->Count<referencesq)
	{
		if(timer) timer->Start();
		References->Add(c);
		percentage=(float)References->Count/(float)referencesq;
		if(PercentageImagePaths)
		{
			ii=(int)(percentage*(double)(PercentageImagePaths->Count-1));
			percentage*=100.0;
			if(Owner && LabelInfoForm->Label==Owner)
			{
				LabelInfoForm->ReferencesPBar->Position=percentage;
				LabelInfoForm->RecolorReferencesPBar();
				//LabelInfoForm->ReferencesPBar->Visible=true;
			}
			if(ii<0) ii=0;
			else if(ii>PercentageImagePaths->Count-1) ii=PercentageImagePaths->Count-1;
			if(iconindex!=ii)
			{
				iconindex=ii;
				//NormalGlyph=LoadPNG32Resource(PercentageImagePaths->Strings[iconindex]);
				if(Owner)
				{
					Owner->NormalGlyphName=PercentageImagePaths->Strings[iconindex];
					Owner->Update();
				}
			}
		}
		lat=lon=z=zg=conf=0.0;
		n=References->Count;
		for(int i=0; i<References->Count; i++)
		{
			c=(TGPSCoordinate*)References->Items[i];
			if(c)
			{
				lat+=c->Latitude/(double)referencesq;
				lon+=c->Longitude/(double)referencesq;
				z+=c->Z/(double)referencesq;
				zg+=c->Z_Geoid/(double)referencesq;
				conf+=c->Confidence/(double)referencesq;
			}
			else
			{
				References->Delete(i);
				n--;
			}
		}
		if(onpercentage) (onpercentage)();
		if(n==referencesq)
		{
			c=new TGPSCoordinate(NULL, lat, lon, z, zg, conf);
			for(int i=0; i<References->Count; i++)
			{
				cc=(TGPSCoordinate*)References->Items[i];
				if(cc && cc->Valid)
				{
					uv=cc->UnitVector(cs);
					d=sqrt(sqr((cc->GetX(cs)-c->GetX(cs))*uv.X)+sqr((cc->GetY(cs)-c->GetY(cs))*uv.Y));
					dd+=d;
					rq++;
				}
			}
			if(rq>0) dd/=(double)rq;
			dd+=conf;
			c->Confidence=dd;
			if(coordinate)
			{
				delete coordinate;
				coordinate=NULL;
			}
			if(Owner)
			{
				Owner->GPSConfidence=dd;
				Owner->Coordinate=c;
			}
			else coordinate=c;
			/*//Delete itself from GpsListeners list
			if(Manager && ((TRadarMapManager*)Manager)->GpsListeners && GpsListener)
			{
				((TRadarMapManager*)Manager)->GpsListeners->Delete(GpsListener);
				delete GpsListener;
				GpsListener=NULL;
			}
			if(timer)
			{
				timer->OnTimer=NULL;
				timer->Stop();
			}
			Owner->Update();
			*/
			StopReferencesCollection();
			if(oncollected) (oncollected)();
			if(Owner && LabelInfoForm->Label==Owner)
			{
				LabelInfoForm->RefreshLabel();
				LabelInfoForm->ReferencesPBar->Visible=false;
				LabelInfoForm->XPanel->Visible=!LabelInfoForm->ReferencesPBar->Visible;
				LabelInfoForm->YPanel->Visible=!LabelInfoForm->ReferencesPBar->Visible;
				LabelInfoForm->CancelIButton->Enabled=false;
				//LabelInfoForm->OkImageButtonClick(NULL);
			}
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TReferencesCollector::StartReferencesCollection()
{
	if(GpsListener) StopReferencesCollection();
	if(Manager && ((TRadarMapManager*)Manager)->GpsListeners)
	{
		GpsListener=new TGpsListener();
		GpsListener->OnAddCoordinate=AddCoordinate;
		((TRadarMapManager*)Manager)->GpsListeners->Add(GpsListener);
	}
	else GpsListener=NULL;

	percentage=0;
	iconindex=0;
	ClearReferences();
	iscollecting=true;
	if(timer)
	{
		timer->OnTimer=&OnIdle;
		timer->Start();
	}
	if(Owner && PercentageImagePaths) Owner->NormalGlyphName=PercentageImagePaths->Strings[iconindex];
	if(onpercentage) (onpercentage)();
}
//---------------------------------------------------------------------------

void __fastcall TReferencesCollector::StopReferencesCollection()
{
	if(Manager && ((TRadarMapManager*)Manager)->GpsListeners && GpsListener)
	{
		((TRadarMapManager*)Manager)->GpsListeners->Delete(GpsListener);
		delete GpsListener;
		GpsListener=NULL;
	}
	if(PercentageImagePaths)
		iconindex=PercentageImagePaths->Count-1;
	else iconindex=0;
	percentage=100.0;
	iscollecting=false;
	if(timer)
	{
		timer->OnTimer=NULL;
		timer->Stop();
	}
	if(Owner && PercentageImagePaths) Owner->NormalGlyphName=PercentageImagePaths->Strings[iconindex];
	if(onpercentage) (onpercentage)();
}
//---------------------------------------------------------------------------

void __fastcall TReferencesCollector::AddCoordinate(TGPSCoordinate *c) // override from TGPSListener
{
	TGPSCoordinate *C=new TGPSCoordinate(c);
	AddReference(C);
}

//---------------------------------------------------------------------------
// TReferncePointLabel
//---------------------------------------------------------------------------
__fastcall TReferncePointLabel::TReferncePointLabel(TMyObject *AOwner,
	Types::TRect OutputRect, TGPSCoordinatesRect *cRect, TObject* Host,
	int ReferencesQ, Types::TPoint SP, TObject* RadarMapManager) :
	TMapLabel(AOwner, OutputRect, cRect, Host, RadarMapManager, mltReferncePointLabel)
{
	/*References=new TList();
	referencesq=ReferencesQ;*/
	PercentageImagePaths=new TStringList();
	PercentageImagePaths->Add((System::UnicodeString)"PNGIMAGE_205");
	PercentageImagePaths->Add((System::UnicodeString)"PNGIMAGE_206");
	PercentageImagePaths->Add((System::UnicodeString)"PNGIMAGE_207");
	PercentageImagePaths->Add((System::UnicodeString)"PNGIMAGE_208");
	PercentageImagePaths->Add((System::UnicodeString)"PNGIMAGE_209");
	//NormalGlyph=LoadPNG32Resource(PercentageImagePaths->Strings[iconindex]); //_210");
	RefCollector=new TReferencesCollector(this, ReferencesQ, PercentageImagePaths,
		"PNGIMAGE_456", 2500, Manager);
	//NormalGlyphName=PercentageImagePaths->Strings[RefCollector->IconIndex];
	AllwaysHot=true;
	Icons=true;
	Center=Types::TPoint(12, 39);//(14, 31); //(5, 30);
	sp=SP;
	try
	{
		if(host && ((TImageMapsContainer*)host)->MapType==mtAdjustable)
		{ // Add itself to the Host Image map, which has to control number of reference points
			((TImageMapsContainer*)host)->AddReferencePoint(sp, NULL, this);
		}
	}
	catch(Exception &e) {host = NULL;}
	profileobjecttype=potReferenceLabel;
	StartReferencesCollection();
}
//---------------------------------------------------------------------------

__fastcall TReferncePointLabel::~TReferncePointLabel()
{
	/*if(GpsListener)
	{
		if(Manager && ((TRadarMapManager*)Manager)->GpsListeners)
			((TRadarMapManager*)Manager)->GpsListeners->Delete(GpsListener);
		delete GpsListener;
		GpsListener=NULL;
	}*/
	try
	{
		if(host && ((TImageMapsContainer*)host)->MapType==mtAdjustable)
		{ // Delete itself from the Host Image map, which has to control number of reference points
			((TImageMapsContainer*)host)->DeleteReferencePointAtXY(sp);
		}
	}
	catch(Exception &e) {}
	//ClearReferences();
	//delete References;
	if(RefCollector) delete RefCollector;
	RefCollector=NULL;
	if(PercentageImagePaths) delete PercentageImagePaths;
	PercentageImagePaths=NULL;
}
//---------------------------------------------------------------------------

void __fastcall TReferncePointLabel::SetCoordinate(TGPSCoordinate* c) //if valid c, then set it as a final "coordinate" and anknoledge Host Image map, that reference is complete! valid=true;
{
	try
	{
		if(c && host && ((TImageMapsContainer*)host)->MapType==mtAdjustable)
		{
			((TImageMapsContainer*)host)->AddReferencePoint(TReferencePoint(sp, c, this));
			//percentage=100.;
			RefCollector->StopReferencesCollection();
		}
	}
	catch(Exception &e) {}
}
//---------------------------------------------------------------------------

void __fastcall TReferncePointLabel::writeCoordinate(TGPSCoordinate *value)
{
	TMapLabel::writeCoordinate(value);
	try
	{
		if(host && ((TImageMapsContainer*)host)->MapType==mtAdjustable)
		{
			//if(References->Count==referencesq)
			((TImageMapsContainer*)host)->ChangeReferencePointAtXY(sp, value);
			((TImageMapsContainer*)host)->RescaleReferences();
		}
	}
	catch(Exception &e) {}
}
//---------------------------------------------------------------------------

void __fastcall TReferncePointLabel::Redraw(TObject *Sender, TBitmap32 *Bmp)
{
	if(Coordinate && CoordinatesRect && CoordinatesRect->Valid) TMapLabel::Redraw(Sender, Bmp);
	else // draw in SourcePoint by XY from sp
	{
		TBitmap32 *tmp, *bck;
		Types::TRect bRect, vRect, *RawPxRect;
		Types::TPoint dp;
		//int x, y;

		if(Bmp && Owner && ((TMapObjectsContainer*)Owner)->Image && Output.Width()!=0 && Output.Height()!=0 && Host)
		{
			tmp=new TBitmap32();
			try
			{
				try
				{
					tmp->DrawMode=dmBlend;
					//bRect=((TMapObjectsContainer*)Owner)->Image->GetBitmapRect();
					bRect=((TMapsContainer*)Host)->GetBitmapRect();
					vRect=((TMapObjectsContainer*)Owner)->Image->GetViewportRect();
					if(host) // Host is an ImageMap where label is belongs to, SP is a XY ancher point in pixels on the Image
					{ // Add itself to the Host Image map, which has to control number of reference points
						try
						{
							if(((TImageMapsContainer*)host)->MapType==mtAdjustable)
							{
								bck=((TImageMapsContainer*)host)->InitialBmp;//Background;
								RawPxRect=((TImageMapsContainer*)host)->RawPxRect;
							}
							else
							{
								bck=NULL;
								RawPxRect=NULL;
							}
						}
						catch(Exception &e) {bck=NULL; RawPxRect=NULL;}
					}
					else bck=NULL;
					if(bck && bck->Width>0 && bck->Height>0 && RawPxRect && RawPxRect->Width()>0 && RawPxRect->Height()>0)
					{
						//y=bRect.Top+(int)((double)bRect.Height()*((double)sp.y/(double)bck->Height));
						//x=bRect.Left+(int)((double)bRect.Width()*((double)sp.x/(double)bck->Width));

						dp=((TMapObjectsContainer*)Owner)->Container->ViewportAreaToScreen(
							DoublePoint((double)(sp.x+RawPxRect->Left)/(double)bck->Width,
								(double)(sp.y+RawPxRect->Top)/(double)bck->Height));
						//if(vRect.Contains(Types::TPoint(x, y)))
						if(vRect.Contains(dp))
						{
							if(Glyph==NULL) Glyph=NormalGlyph;

							//??? MapMessages are used as a Last Glyph of second point ???

							tmp->BeginUpdate();
							try
							{
								if(Glyph!=NULL)
								{
									tmp->SetSizeFrom(Glyph);
									tmp->Draw(0, 0, Glyph);
								}
								else
								{
									tmp->SetSize(16, 16);
									tmp->FillRectTS(0, 0, 16, 16, Color);
								}
							}
							__finally
							{
								tmp->EndUpdate();
							}
							dp.x-=center.x;//tmp->Width>>1;
							dp.y-=center.y;//tmp->Height>>1;
							Rect=Types::TRect(dp.x, dp.y, dp.x+tmp->Width, dp.y+tmp->Height);
							Bmp->Draw(dp.x, dp.y, tmp);
							TCustomProfileLabel::IconsDraw(Bmp);
						}
						else
						{
							Rect=Types::TRect(0,0,0,0);
						}
					}
				}
				catch(...) {}
			}
			__finally
			{
				delete tmp;
			}
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TReferncePointLabel::MouseUp(TObject *Sender,
	TMouseButton Button, TShiftState Shift, int X, int Y, TCustomLayer *Layer)
{
	TCustomProfileLabel::MouseUp(Sender, Button, Shift, X, Y, Layer);
	if(LabelInfoForm->Label==this)
	{
		LabelInfoForm->ReferencesPBar->Position=RefCollector->Percentage;
		LabelInfoForm->RecolorReferencesPBar();
		if(RefCollector->Percentage<100.) LabelInfoForm->ReferencesPBar->Visible=true;
		else LabelInfoForm->ReferencesPBar->Visible=false;
		LabelInfoForm->XPanel->Visible=!LabelInfoForm->ReferencesPBar->Visible;
		LabelInfoForm->YPanel->Visible=!LabelInfoForm->ReferencesPBar->Visible;
		LabelInfoForm->CancelIButton->Enabled=LabelInfoForm->ReferencesPBar->Visible;
	}
}

//---------------------------------------------------------------------------
// TMapSparObject
//---------------------------------------------------------------------------
void __fastcall TMapSparObject::Redraw(TObject *Sender, TBitmap32 *Bmp)
{
	//TGPSCoordinate *llp, *ZeroPoint;
	float x, y, x_old, y_old;
	Types::TRect bmr, vpt;
	TColor32Entry ce;
	TLine line;
	double xmid, ymid;
	int i;
	float da, A;
	int x1, x2, y1, y2;

	Rect=Types::TRect(0,0,0,0);

	if(Bmp && Owner && ((TMapObjectsContainer *)Owner)->Image &&
		LatLonRect && LatLonRect->Width()>0. && LatLonRect->Height()>0.)
	{
		bmr=((TMapObjectsContainer *)Owner)->Image->GetBitmapRect();
		vpt=((TMapObjectsContainer *)Owner)->Image->GetViewportRect();
		while(WaitForSingleObject(ListInEditingEvent, 0)==WAIT_OBJECT_0)
		{
//			ProcessMessages();
			MySleep(10);
		}
		SetEvent(ListInEditingEvent);
		try
		{
			if(bmr.Width()>0 && bmr.Height()>0 && vpt.Width()>0 && vpt.Height()>0 && List->Count>0)
			{
				Bmp->BeginUpdate();
				try
				{
					try
					{
						//ZeroPoint=NULL;
						ce.ARGB=color;
						da=200.0/(float)PointsMax;
						A=200;
						xmid=0;
						ymid=0;

						//Spar tale from last detected points

						/*for(i=List->Count-1; i>0; i--)
						{
							llp=(TGPSCoordinate*)List->Items[i];
							ce.A=(unsigned char)A;
							if(llp!=NULL)
							{
								y=LatLonRect->LeftTop->Latitude-llp->Latitude;
								y/=LatLonRect->Height();
								x=llp->Longitude-LatLonRect->LeftTop->Longitude;
								x/=LatLonRect->Width();
								x*=bmr.Width();
								y*=bmr.Height();
								x+=bmr.Left;
								y+=bmr.Top;
								llp->x=x;
								llp->y=y;

								if(i==List->Count-1)
								{
									xmid=x;
									ymid=y;
								}

								if(x<Rect.Left || i==0) Rect.Left=x;
								if(x>Rect.Right || i==0) Rect.Right=x;
								if(y<Rect.Top || i==0) Rect.Top=y;
								if(y>Rect.Bottom || i==0) Rect.Bottom=y;

								if(A<200)
								{
									line=TLine(x_old, y_old, x, y);
									if(line.IsInRect(vpt))
									{ // draw line from x_old, y_old to x, y
										Bmp->LineAS(line.X1, line.Y1, line.X2, line.Y2, ce.ARGB);
									}
								}
								x_old=x;
								y_old=y;
								if(ZeroPoint==NULL) ZeroPoint=llp;
							}
							A-=da;
						}*/

						TPolygon32 *poly;
						double dOx, dOy, cxmid, cymid, yaw, cos_yaw, sin_yaw;
						TColor32 cc;
						TCoordinateSystem cs;

						if(((TMapObjectsContainer*)Owner)->Container) cs=((TMapObjectsContainer*)Owner)->Container->CoordinateSystem;
						else
						{
							if(Manager && ((TRadarMapManager*)Manager)->Settings) cs=((TRadarMapManager*)Manager)->Settings->DefaultCoordinateSystem;
							else cs=csLatLon;
						}
						TGPSCoordinate *llp=(TGPSCoordinate*)List->Items[List->Count-1];
						if(llp->Valid)
						{
							if(cs==csLatLon)
							{
								ymid=LatLonRect->LeftTop->Latitude-llp->Latitude;
								xmid=llp->Longitude-LatLonRect->LeftTop->Longitude;
							}
							else
							{
								ymid=LatLonRect->LeftTop->GetY(cs)-llp->GetY(cs);
								xmid=llp->GetX(cs)-LatLonRect->LeftTop->GetX(cs);
							}
							/*switch(cs)
							{
								case csDutch:
									ymid=LatLonRect->LeftTop->RdY-llp->RdY;
									xmid=llp->RdX-LatLonRect->LeftTop->RdX;
									break;
								case csBeLambert08:
								case csBeLambert72:
									ymid=LatLonRect->LeftTop->BeY-llp->BeY;
									xmid=llp->BeX-LatLonRect->LeftTop->BeX;
									break;
								case csUTM:
									ymid=LatLonRect->LeftTop->UtmY-llp->UtmY;
									xmid=llp->UtmX-LatLonRect->LeftTop->UtmX;
									break;
								case csLKS:
									ymid=LatLonRect->LeftTop->LksY-llp->LksY;
									xmid=llp->LksX-LatLonRect->LeftTop->LksX;
									break;
								case csLatLon:
								default:
									ymid=LatLonRect->LeftTop->Latitude-llp->Latitude;
									xmid=llp->Longitude-LatLonRect->LeftTop->Longitude;
									break;
							}*/
							//ymid=LatLonRect->LeftTop->Latitude-llp->Latitude;
							//xmid=llp->Longitude-LatLonRect->LeftTop->Longitude;
							ymid/=LatLonRect->Height(cs);
							xmid/=LatLonRect->Width(cs);
							xmid*=bmr.Width();
							ymid*=bmr.Height();
							xmid+=bmr.Left;
							ymid+=bmr.Top;

							yaw=SparData.yaw+SparData.sparHeading-M_PI;
							cos_yaw=cos(yaw);
							sin_yaw=sin(yaw);
							da=0.1*sqrt((double)(vpt.Width()*vpt.Width()+vpt.Height()*vpt.Height()))/(double)(2*PointsMax);
							if(Manager && ((TRadarMapManager*)Manager)->Settings &&	cs>csLatLon)
							{
								dOx=(float)bmr.Width()*SparData.dOffset/LatLonRect->Width(cs);
								dOy=(float)bmr.Height()*SparData.dOffset/LatLonRect->Height(cs);
								if(fabs(SparData.offset)>0 && 100.0*fabs(SparData.dOffset/SparData.offset)>((TRadarMapManager*)Manager)->Settings->SparConfidenceMax)
									cc=Color32(255, 0, 0, 5);
								else cc=Color32(0, 255, 0, 5);
								poly=new TPolygon32();
								try
								{
									for(i=1; i<=PointsMax; i++)
									{
										poly->Clear();

										cxmid=xmid+dOx*cos_yaw;
										cymid=ymid-dOy*sin_yaw;
										x=x1=(int)(cxmid+(float)i*da*sin_yaw);
										x2=(int)(cxmid-(float)i*da*sin_yaw);
										y=y1=(int)(cymid+(float)i*da*cos_yaw);
										y2=(int)(cymid-(float)i*da*cos_yaw);
										poly->Add(FixedPoint(x1, y1));
										poly->Add(FixedPoint(x2, y2));
										cxmid=xmid-dOx*cos_yaw;
										cymid=ymid+dOy*sin_yaw;
										x1=(int)(cxmid+(float)i*da*sin_yaw);
										x2=(int)(cxmid-(float)i*da*sin_yaw);
										y1=(int)(cymid+(float)i*da*cos_yaw);
										y2=(int)(cymid-(float)i*da*cos_yaw);
										poly->Add(FixedPoint(x2, y2));
										poly->Add(FixedPoint(x1, y1));
										poly->Add(FixedPoint(x, y));
										poly->DrawFill(Bmp, cc, NULL);
									}
								}
								__finally
								{
									delete poly;
								}
							}
							Bmp->FillRectS(xmid-4, ymid-4, xmid+4, ymid+4, color);
							ce.ARGB=color;
							A=255;
							x_old=xmid;
							y_old=ymid;
							for(i=1; i<=PointsMax; i++)
							{
								ce.A=(unsigned char)A;
								Bmp->PenColor=ce.ARGB;
								x = (int)(xmid+(float)i*da*sin_yaw);
								y = (int)(ymid+(float)i*da*cos_yaw);
								line=TLine(x_old, y_old, x, y);
								if(line.IsInRect(vpt))
									Bmp->LineAS(line.X1, line.Y1, line.X2, line.Y2, ce.ARGB);
								A-=(256.0/(float)PointsMax);
								x_old=x;
								y_old=y;
							}
							x_old=xmid;
							y_old=ymid;
							A=255;
							for(i=1; i<=PointsMax; i++)
							{
								ce.A=(unsigned char)A;
								Bmp->PenColor=ce.ARGB;
								x = (int)(xmid-(float)i*da*sin_yaw);
								y = (int)(ymid-(float)i*da*cos_yaw);
								line=TLine(x_old, y_old, x, y);
								if(line.IsInRect(vpt))
									Bmp->LineAS(line.X1, line.Y1, line.X2, line.Y2, ce.ARGB);
								A-=(256.0/(float)PointsMax);
								x_old=x;
								y_old=y;
							}
						}
					}
					catch(...) {}
				}
				__finally
				{
					Bmp->EndUpdate();
				}
			}
		}
		__finally
		{
			ResetEvent(ListInEditingEvent);
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TMapSparObject::AddSpar(double ln, double lt, Spar src)
{
	SparData=src;
	TMapSparObject::AddP(ln, lt, 0., src.utilAltitude, 0);
}

//---------------------------------------------------------------------------
// TStartStopWaypointLabel
//---------------------------------------------------------------------------
__fastcall TStartStopWaypointLabel::TStartStopWaypointLabel(TMyObject *AOwner,
	Types::TRect OutputRect, TGPSCoordinatesRect *cRect, TObject* Host,
	Types::TPoint SP, TStartStopWaypointKind AKind, TObject* RadarMapManager) :
	TMapLabel(AOwner, OutputRect, cRect, Host, RadarMapManager, mltStartStopWaypointLabel)
{
	kind=AKind;
	if(kind==swkStart)
	{
		//NormalGlyph=LoadPNG32Resource("PNGIMAGE_258"); // StartWaypoint
		NormalGlyphName="PNGIMAGE_258";
		((TRadarMapManager*)Manager)->StartWaypoint=this;
		Color=clGreen32;
	}
	else
	{
		//NormalGlyph=LoadPNG32Resource("PNGIMAGE_262"); // StopWaypoint
		NormalGlyphName="PNGIMAGE_262";
		((TRadarMapManager*)Manager)->StopWaypoint=this;
		Color=clRed32;
	}
	opositewaypoint=NULL;
	path=NULL;
	AllwaysHot=true;
	Icons=true;
	Center=Types::TPoint(10, 31);
	sp=SP;
	profileobjecttype=potWaypointLabel;
}
//---------------------------------------------------------------------------

__fastcall TStartStopWaypointLabel::~TStartStopWaypointLabel()
{
	if(Manager)
	{
		if(opositewaypoint && opositewaypoint->OpositeWayPoint==this)
		{
			TStartStopWaypointLabel* tmp=opositewaypoint;

			opositewaypoint=NULL;
			tmp->OpositeWayPoint=NULL;
			delete tmp;
		}
		if(path)
		{
			TMapTrackPathObject* tmp=path;

			path=NULL;
			if(tmp->StartWayPoint==this) tmp->StartWayPoint=NULL;
			else if(tmp->StopWayPoint==this) tmp->StopWayPoint=NULL;
		}
		if(((TRadarMapManager*)Manager)->StartWaypoint==this)
			((TRadarMapManager*)Manager)->StartWaypoint=NULL;
		else if(((TRadarMapManager*)Manager)->StopWaypoint==this)
			((TRadarMapManager*)Manager)->StopWaypoint=NULL;
	}
}
//---------------------------------------------------------------------------

void __fastcall TStartStopWaypointLabel::MouseUp(TObject *Sender,
	TMouseButton Button, TShiftState Shift, int X, int Y, TCustomLayer *Layer)
{
	TCustomProfileLabel::MouseUp(Sender, Button, Shift, X, Y, Layer);
	if(LabelInfoForm->Label==this)
	{
		//LabelInfoForm->ReferencesPBar->Position=percentage;
		//LabelInfoForm->RecolorReferencesPBar();
		//if(percentage<100.) LabelInfoForm->ReferencesPBar->Visible=true;
	}
}
//---------------------------------------------------------------------------

void __fastcall TStartStopWaypointLabel::writeOpositeWaypoint(TStartStopWaypointLabel *value)
{
	if(value!=opositewaypoint)
	{
		TStartStopWaypointLabel* tmp=opositewaypoint;

		opositewaypoint=value;
		if(tmp && tmp->OpositeWayPoint==this) tmp->OpositeWayPoint=NULL;
		if(value) value->OpositeWayPoint=this;
	}
}
//---------------------------------------------------------------------------

void __fastcall TStartStopWaypointLabel::writePath(TMapTrackPathObject *value)
{
	if(value!=path)
	{
		TMapTrackPathObject *tmp=path;

		path=value;
		if(tmp)
		{
			if(kind==swkStart && tmp->StartWayPoint==this) tmp->StartWayPoint=NULL;
			else if(kind==swkStop && tmp->StopWayPoint==this) tmp->StopWayPoint=NULL;
		}
		if(value)
		{
			if(kind==swkStart) value->StartWayPoint=this;
			else if(kind==swkStop) value->StopWayPoint=this;
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TStartStopWaypointLabel::Redraw(TObject *Sender, TBitmap32 *Bmp)
{
	if(Coordinate && CoordinatesRect && CoordinatesRect->Valid) TMapLabel::Redraw(Sender, Bmp);
	else // draw in SourcePoint by XY from sp
	{
		TBitmap32 *tmp, *bck;
		Types::TRect bRect, vRect;
		int x, y;

		if(Bmp && Owner && ((TMapObjectsContainer*)Owner)->Image && Output.Width()!=0 && Output.Height()!=0)
		{
			tmp=new TBitmap32();
			try
			{
				try
				{
					tmp->DrawMode=dmBlend;
					bRect=((TMapObjectsContainer*)Owner)->Image->GetBitmapRect();
					vRect=((TMapObjectsContainer*)Owner)->Image->GetViewportRect();
					if(host) // Host is an ImageMap where label is belongs to, SP is a XY ancher point in pixels on the Image
					{ // Add itself to the Host Image map, which has to control number of reference points
						try
						{
							bck=((TImageMapsContainer*)host)->Background;

						}
						catch(Exception &e) {bck=NULL;}
					}
					else bck=NULL;
					if(bck && bck->Width>0 && bck->Height>0)
					{
						y=bRect.Top+(int)((double)bRect.Height()*((double)sp.y/(double)bck->Height));
						x=bRect.Left+(int)((double)bRect.Width()*((double)sp.x/(double)bck->Width));
						if(vRect.Contains(Types::TPoint(x, y)))
						{
							if(Glyph==NULL) Glyph=NormalGlyph;
							tmp->BeginUpdate();
							try
							{
								if(Glyph!=NULL)
								{
									tmp->SetSizeFrom(Glyph);
									tmp->Draw(0, 0, Glyph);
								}
								else
								{
									tmp->SetSize(16, 16);
									tmp->FillRectTS(0, 0, 16, 16, Color);
								}
							}
							__finally
							{
								tmp->EndUpdate();
							}
							x-=center.x;//tmp->Width>>1;
							y-=center.y;//tmp->Height>>1;
							Rect=Types::TRect(x, y, x+tmp->Width, y+tmp->Height);
							Bmp->Draw(x, y, tmp);
							TCustomProfileLabel::IconsDraw(Bmp);
						}
						else
						{
							Rect=Types::TRect(0,0,0,0);
						}
					}
				}
				catch(...) {}
			}
			__finally
			{
				delete tmp;
			}
		}
	}
}

//---------------------------------------------------------------------------
// TStartNGoLabel
//---------------------------------------------------------------------------
__fastcall TStartNGoLabel::TStartNGoLabel(TMyObject *AOwner, Types::TRect OutputRect,
	TGPSCoordinatesRect *cRect, TMapsContainersCollection* aHost, Types::TPoint SP,
	TObject* RadarMapManager) :	TMapLabel(AOwner, OutputRect, cRect, (TObject*)aHost,
	RadarMapManager, mltStartNGoLabel)
{
	center=Types::TPoint(11, 29);
	center2=Center;
	allwayshot=true;
	icons=true;
	color=Color32(161, 2, 169);
	state=sgnNone;
	azimuth=0;
	dX=dY=0;
	drawglyph=true;
	sp=SP;
	NormalCoordinated="PNGIMAGE_440";
	NormalUnCoordinated="PNGIMAGE_441";
	NormalGlyphName=NormalUnCoordinated; //hot, normal - 439
	AzimuthGlyphName="PNGIMAGE_444";
	Fixed1GlyphName="PNGIMAGE_443";
	Fixed2GlyphName="PNGIMAGE_442";
	Fixed3GlyphName="PNGIMAGE_445";
	Fixed4GlyphName="PNGIMAGE_446";
	if(Owner && Owner->ObjectType==gotContainer)
	{
		bool err;

		try
		{
			if(aHost && aHost->ObjectType==gotContainersCollection)
			{
				TDoublePoint dp;

				StartingVPP=aHost->ScreenToViewportArea(SP);
				dp=aHost->ScreenToCoordinatesRect(SP);
				if(CoordinatesRect && CoordinatesRect->Valid)
				{
					if(coordinate) delete coordinate;
					coordinate=new TGPSCoordinate();
					coordinate->SetXY(aHost->CoordinateSystem, dp);
					NormalGlyphName="PNGIMAGE_440"; //hot, normal - 439
				}
				else Coordinate=NULL;
				//Rect=aHost->GetBitmapRect();
				state=sgnPlaced;
				err=false;
			}
			else err=true;
		}
		catch(Exception &e) {err=true;}
		if(err)
		{
			host=NULL;
			StartingVPP=DoublePoint(0,0);
			coordinate=NULL;
			state=sgnNone;
			Rect=Types::TRect(0,0,0,0);
		}
	}
	else
	{
		Coordinate=NULL;
	}
	GPSConfidence=0;
	profileobjecttype=potStartNGoLabel;
	pressed=true;
	AOwner->SetGlobalPressed(true);
}
//---------------------------------------------------------------------------

__fastcall TStartNGoLabel::~TStartNGoLabel()
{
	Closing=true;
	if(State<sgnStopping)
	{
		//State=sgnStopping;
        StopTWDRS();
	}
	if(Manager && ((TRadarMapManager*)Manager)->TwoWheelClient)
		((TRadarMapManager*)Manager)->TwoWheelClient->ForgetInitiator(this);
	NormalGlyphName="";
	AzimuthGlyphName="";
	Fixed1GlyphName="";
	Fixed2GlyphName="";
	Fixed3GlyphName="";
	Fixed4GlyphName="";
}
//---------------------------------------------------------------------------

void __fastcall TStartNGoLabel::Update(TMyObjectType Type)
{
	if(Closing) return;
	if(Coordinate && Coordinate->Valid && CoordinatesRect && CoordinatesRect->Valid)
		NormalGlyphName=NormalCoordinated;
	else
	{
		if(Host && CoordinatesRect && CoordinatesRect->Valid)
		{
			TDoublePoint dp;

			dp=((TMapsContainersCollection*)Host)->ViewportAreaToCoordinatesRect(StartingVPP);
			Coordinate=new TGPSCoordinate();
			Coordinate->SetXY(((TMapsContainersCollection*)Host)->CoordinateSystem, dp);
			NormalGlyphName=NormalCoordinated;
		}
		else NormalGlyphName=NormalUnCoordinated;
	}
	TGraphicObject::Update(Type);
}
//---------------------------------------------------------------------------

void __fastcall TStartNGoLabel::writeAzimuthGlyphName(AnsiString value)
{
	if(azimuthglyphname!=value)
	{
		if(azimuthglyphname!="")
		{
			RemovePngFromContainer(azimuthglyphname);
			if(Glyph==azimuthglyph) Glyph=NULL;
			azimuthglyph=NULL;
		}
		if(value!="") azimuthglyph=GetPngFromContainer(value);
		azimuthglyphname=value;
	}
}
//---------------------------------------------------------------------------

void __fastcall TStartNGoLabel::writeFixed1GlyphName(AnsiString value)
{
	if(fixed1glyphname!=value)
	{
		if(fixed1glyphname!="")
		{
			RemovePngFromContainer(fixed1glyphname);
			if(Glyph==fixed1glyph) Glyph=NULL;
			fixed1glyph=NULL;
		}
		if(value!="") fixed1glyph=GetPngFromContainer(value);
		fixed1glyphname=value;
	}
}
//---------------------------------------------------------------------------

void __fastcall TStartNGoLabel::writeFixed2GlyphName(AnsiString value)
{
	if(fixed2glyphname!=value)
	{
		if(fixed2glyphname!="")
		{
			RemovePngFromContainer(fixed2glyphname);
			if(Glyph==fixed2glyph) Glyph=NULL;
			fixed2glyph=NULL;
		}
		if(value!="") fixed2glyph=GetPngFromContainer(value);
		fixed2glyphname=value;
	}
}
//---------------------------------------------------------------------------

void __fastcall TStartNGoLabel::writeFixed3GlyphName(AnsiString value)
{
	if(fixed3glyphname!=value)
	{
		if(fixed3glyphname!="")
		{
			RemovePngFromContainer(fixed3glyphname);
			if(Glyph==fixed3glyph) Glyph=NULL;
			fixed3glyph=NULL;
		}
		if(value!="") fixed3glyph=GetPngFromContainer(value);
		fixed3glyphname=value;
	}
}
//---------------------------------------------------------------------------

void __fastcall TStartNGoLabel::writeFixed4GlyphName(AnsiString value)
{
	if(fixed4glyphname!=value)
	{
		if(fixed4glyphname!="")
		{
			RemovePngFromContainer(fixed4glyphname);
			if(Glyph==fixed4glyph) Glyph=NULL;
			fixed4glyph=NULL;
		}
		if(value!="") fixed4glyph=GetPngFromContainer(value);
		fixed4glyphname=value;
	}
}
//---------------------------------------------------------------------------

void __fastcall TStartNGoLabel::Redraw(TObject *Sender, TBitmap32 *Bmp)
{
	//TMapsContainer *map;
	TBitmap32 *TempBmp, *tmp, *bck, *glyph;
	Types::TPoint dp, dp2;
	Types::TRect vRect, tRect;
	int dpw, dph, ww, hh;

	if(Closing) return;
	if(Bmp && Owner && ((TMapObjectsContainer*)Owner)->Image)
	{
		vRect=((TMapObjectsContainer*)Owner)->Image->GetViewportRect();
		//if(Host) map=((TMapsContainersCollection*)Host)->GetObject(0);
		//else map=NULL;
		TempBmp=new TBitmap32();
		tmp=new TBitmap32();
		try
		{
			if(state==sgnNone)
			{
				TempBmp=NULL;
				tmp=NULL;
				glyph=NULL;
			}
			else
			{
				TempBmp->BeginUpdate();
				TempBmp->DrawMode=dmBlend;
				switch(state)
				{
					case sgnPlaced:	glyph=AzimuthGlyph; break;
					case sgnAzimuthed: glyph=Fixed1Glyph; break;
					case sgnStarted: glyph=Fixed2Glyph; break;
					case sgnStopping: glyph=Fixed3Glyph; break;
					case sgnStopped: glyph=Fixed4Glyph; break;
				}
				if(glyph->Height>glyph->Width) TempBmp->SetSize(glyph->Height, glyph->Height);
				else TempBmp->SetSize(glyph->Width, glyph->Width);
				TempBmp->Clear(Color32(255,255,255,0));
				dp.x=(TempBmp->Width-glyph->Width) >> 1;
				TempBmp->Draw(Types::TRect(dp.x, 0, dp.x+glyph->Width, glyph->Height),
					Types::TRect(0, 0, AzimuthGlyph->Width, glyph->Height), glyph);
				TempBmp->EndUpdate();
				tmp->BeginUpdate();
				//tmp->SetSize(glyph->Height, glyph->Height);
				//tmp->DrawMode=dmBlend;
				//tmp->Clear(Color32(255,255,255,0));
				RotateBitmap32(TempBmp, tmp, azimuth, false);
				tmp->EndUpdate();
			}
		}
		__finally
		{
			bck=new TBitmap32();
			Bmp->BeginUpdate();
			try
			{
				try
				{
					bck->DrawMode=dmBlend;
					if(Glyph==NULL) Glyph=NormalGlyph;
					bck->BeginUpdate();
					try
					{
						if(Glyph!=NULL)
						{
							bck->SetSizeFrom(Glyph);
							bck->Draw(0, 0, Glyph);
						}
						else
						{
							bck->SetSize(16, 16);
							bck->FillRectTS(0, 0, 16, 16, Color);
						}
					}
					__finally
					{
						bck->EndUpdate();
					}
					if(Host && Coordinate && CoordinatesRect && CoordinatesRect->Valid)
						dp=((TMapsContainersCollection*)Host)->CoordinatesRectToScreen(
							Coordinate->GetXY(((TMapsContainersCollection*)Host)->CoordinateSystem));
					else dp=((TMapsContainersCollection*)Host)->ViewportAreaToScreen(StartingVPP);
					if(vRect.Contains(dp))
					{
						dp.x-=center.x;
						dp.y-=center.y;
						dp2=dp;
						if(tmp)
						{
							dp.x-=(tmp->Width>>1)-center.x-1;
							dp.y-=(tmp->Height>>1)-center.y-1;
							dpw=tmp->Width;
							dph=tmp->Height;
							if(dp.x<0 && abs(dp.x)<dpw)
							{
								dpw+=dp.x;
								dp.x=0;
							}
							if(dp.y<0 && abs(dp.y)<dph)
							{
								dph+=dp.y;
								dp.y=0;
							}
							if(vRect.Contains(dp))
							{
								tRect=Types::TRect(tmp->Width-dpw, tmp->Height-dph, tmp->Width, tmp->Height);
								Bmp->Draw(dp.x, dp.y, tRect, tmp);
								if(glyph && state!=sgnPlaced)
								{
									ww=(tmp->Width-glyph->Width)>>1;
									hh=(tmp->Height-glyph->Height)>>1;
									dpw-=ww;
									dph-=hh;
									Rect=Types::TRect(dp.x+ww, dp.y, dp.x+dpw, dp.y+dph);
								}
								else Rect=Types::TRect(dp.x, dp.y, dp.x+dpw, dp.y+dph);
								center2=Types::TPoint(tmp->Width>>1, tmp->Height>>1);
							}
							else
							{
								Rect=Types::TRect(dp.x, dp.y, dp.x+bck->Width, dp.y+bck->Height);
								center2=center;
							}
						}
						//dp.x+=(tmp->Width>>1)-center.x-1;
						//dp.y+=(tmp->Height>>1)-center.y-1;
						if(drawglyph) Bmp->Draw(dp2.x, dp2.y, bck);
					}
					else Rect=Types::TRect(0,0,0,0);
				}
				catch(...) {}
			}
			__finally
			{
				Bmp->EndUpdate();
				delete bck;
			}
			delete TempBmp;
			delete tmp;
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TStartNGoLabel::MouseDown(TObject *Sender,
	TMouseButton Button, TShiftState Shift, int X, int Y, TCustomLayer *Layer)
{
	if(state==sgnPlaced)
	{
		dX=X1-(Rect.left+center2.x);
		dY=Y1-(Rect.top+center2.y);
	}
	else
	{
		pressed=false;
		if(Owner)
		{
			Owner->SetGlobalPressed(false);
			((TGraphicObjectsContainer*)Owner)->SetEntered(false);
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TStartNGoLabel::MouseUp(TObject *Sender, TMouseButton Button,
	TShiftState Shift, int X, int Y, TCustomLayer *Layer)
{
	TCustomProfileLabel::MouseUp(Sender, Button, Shift, X, Y, Layer);
}
//---------------------------------------------------------------------------

void __fastcall TStartNGoLabel::MouseDblClick(System::TObject* Sender, int X, int Y)
{
	if(state==sgnPlaced)
	{
		State=sgnAzimuthed;
	}
}
//---------------------------------------------------------------------------

void __fastcall TStartNGoLabel::MouseMove(System::TObject* Sender,
	Classes::TShiftState Shift, int X, int Y, TCustomLayer *Layer)
{
	if(Pressed && (Shift.Contains(ssLeft) || Shift.Contains(ssTouch) || Shift.Contains(ssPen)))
	{
		//X+=dX;
		//Y+=dY;
		//x=X;
		//y=Y;
		switch(state)
		{
			case sgnPlaced:
				AzimuthLine.X1=Rect.Left+center2.x;
				AzimuthLine.Y1=Rect.Top+center2.y;
				AzimuthLine.X2=X;
				AzimuthLine.Y2=Y;
				Azimuth=(AzimuthLine.Angle()-M_PI_2)+M_PI;
				break;
			case sgnAzimuthed:
			case sgnStarted:
			case sgnStopping:
			case sgnStopped:
			case sgnNone:
				break;
		}
		if(!Closing && LabelInfoForm->Label==this)
		{
			LabelInfoForm->Label=this;
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TStartNGoLabel::StartTWDRS()
{
	if(Manager && ((TRadarMapManager*)Manager)->TwoWheelClient)
	{
		TDouble3DPoint dp;
		TDoublePoint uv;
		TCoordinateSystem cs;

		if(Host && Coordinate && Coordinate->Valid && CoordinatesRect && CoordinatesRect->Valid)
		{
			cs=((TMapsContainersCollection*)Host)->CoordinateSystem;
            if(coordinate->GetCSDimension(cs)==csdDegrees) cs=csLocalMetric();
			if(cs==csUTM) cs=csMetric;
			dp=Coordinate->GetXYH(cs);
			uv=Coordinate->UnitVector(cs);
		}
		else
		{
			dp=TDouble3DPoint();
			uv=DoublePoint(1., 1.);
		}
		if(!((TRadarMapManager*)Manager)->TwoWheelClient->StartPositioning(&StartTWDRSParser,
			dp.X, dp.Y, dp.Z, RadToDeg(Azimuth), uv.X, uv.Y, this))
		{
            State=sgnPlaced;
        }
	}
}
//---------------------------------------------------------------------------

void __fastcall TStartNGoLabel::StartTWDRSParser(AnsiString Reply)
{
	int rsf;
	TStringList *strs;

	try
	{
		strs=new TStringList();
		if(Reply!=NULL && Reply!="" && Reply.Length()>0 && Manager && ((TRadarMapManager*)Manager)->TwoWheelClient &&
			((TRadarMapManager*)Manager)->TwoWheelClient->ParseStrings(Reply, strs, ',')>0)
		{
			//if(Reply.Pos("$lwheel,") >= 1)
			if(strs->Strings[0].LowerCase()=="$start" && strs->Count>=2 &&
				strs->Strings[1].UpperCase()=="OK")
			{
				State=sgnStarted;
			}
			else if(strs->Strings[0].LowerCase()=="$invalid")
			{
				if(Owner && ((TMapObjectsContainer*)Owner)->Messages)
				{
					((TRadarMapManager*)Manager)->ShowMessageObject(NULL, ((TMapObjectsContainer*)Owner)->Messages,
						TMessageType::mtStop, mvtDisapering, 5000, "WPS Communication error!", (AnsiString)_RadarMapName);
				}
				State=sgnPlaced;
			}
		}
		//if(Reply=="") ;//
		if(!Closing && LabelInfoForm->Label==this)
		{
			LabelInfoForm->Label=this;
		}
	}
	__finally
	{
		delete strs;
	}
}
//---------------------------------------------------------------------------

void __fastcall TStartNGoLabel::StopTWDRS()
{
	if(Manager && ((TRadarMapManager*)Manager)->TwoWheelClient)
	{
		if(Closing) ((TRadarMapManager*)Manager)->TwoWheelClient->StopPositioning(NULL,	this);
		else ((TRadarMapManager*)Manager)->TwoWheelClient->StopPositioning(&StopTWDRSParser, this);
	}
}
//---------------------------------------------------------------------------

void __fastcall TStartNGoLabel::StopTWDRSParser(AnsiString Reply)
{
	int rsf;
	TStringList *strs;

	try
	{
		strs=new TStringList();
		if(Reply!=NULL && Reply!="" && Reply.Length()>0 && Manager && ((TRadarMapManager*)Manager)->TwoWheelClient &&
			((TRadarMapManager*)Manager)->TwoWheelClient->ParseStrings(Reply, strs, ',')>0)
		{
			//if(Reply.Pos("$lwheel,") >= 1)
			if(strs->Strings[0].LowerCase()=="$stop" && strs->Count>=1)
			{
				State=sgnStopped;
			}
			else if(strs->Strings[0].LowerCase()=="$invalid")
			{
				if(Owner)
				{
					((TRadarMapManager*)Manager)->ShowMessageObject(NULL, ((TMapObjectsContainer*)Owner)->Messages,
						TMessageType::mtStop, mvtDisapering, 5000, "WPS Communication error!", (AnsiString)_RadarMapName);
				}
				state=sgnStarted;
			}
		}
		if(!Closing && LabelInfoForm->Label==this)
		{
			LabelInfoForm->Label=this;
		}
	}
	__finally
	{
		delete strs;
	}
}
//---------------------------------------------------------------------------

void __fastcall TStartNGoLabel::writeState(TStartNGoLabelState value)
{
    if(Closing) return;
	if(state!=value)
	{
		switch(value)
		{
			case sgnNone: break;
			case sgnPlaced: break;
			case sgnAzimuthed:
				if(Manager && ((TRadarMapManager*)Manager)->TwoWheelClient &&
					((TRadarMapManager*)Manager)->Settings->WheelPositioning==TPositioning::TwoWheel)
				{
					if(Host && Coordinate && Coordinate->Valid && CoordinatesRect && CoordinatesRect->Valid)
						StartingVPP=((TMapsContainersCollection*)Host)->CoordinatesRectToViewportArea(
							Coordinate->GetXY(((TMapsContainersCollection*)Host)->CoordinateSystem));
					new TRunNForgetThread(&StartTWDRS, false);
				}
				else
				{
					if(Owner && ((TMapObjectsContainer*)Owner)->Messages)
					{
						((TMapObjectsContainer*)Owner)->Messages->AddMessage(TMessageType::mtWarning,
							"WPS system is switched off!", mvtDisapering, 5000);
					}
					value=state;
				}
				break;
			case sgnStarted:
				if((!Coordinate || !Coordinate->Valid) && Manager &&
					((TRadarMapManager*)Manager)->LastGPSPoint && ((TRadarMapManager*)Manager)->LastGPSPoint->Valid)
				{
					if(Manager && ((TRadarMapManager*)Manager)->TwoWheelClient)
					{
						if(((TRadarMapManager*)Manager)->TwoWheelClient->Initialized)
							Coordinate=new TGPSCoordinate(((TRadarMapManager*)Manager)->LastGPSPoint);
					}
					else Coordinate=new TGPSCoordinate(((TRadarMapManager*)Manager)->LastGPSPoint);
				}
				break;
			case sgnStopping:
				new TRunNForgetThread(&StopTWDRS, false);
				break;
				//Stop TWDRS
			case sgnStopped:
				//TWDRS Stopping confirmation received
				break;
		}
		state=value;
		if(!Closing && LabelInfoForm->Label==this)
		{
			LabelInfoForm->Label=this;
		}
		Update();
	}
}
//---------------------------------------------------------------------------

void __fastcall TStartNGoLabel::writeAzimuth(double value)
{
	if(azimuth!=value)
	{
		azimuth=value;
		if(!Closing && LabelInfoForm->Label==this)
		{
			LabelInfoForm->Label=this;
		}
		Update();
	}
}

//---------------------------------------------------------------------------
// TStartNGoGPSLabel
//---------------------------------------------------------------------------
__fastcall TStartNGoGPSLabel::TStartNGoGPSLabel(TMyObject *AOwner, Types::TRect OutputRect,
	TGPSCoordinatesRect *cRect, TMapsContainersCollection* Host, Types::TPoint SP,
	int ReferencesQ, TObject* RadarMapManager) : TStartNGoLabel(AOwner, OutputRect, cRect, Host, SP, RadarMapManager)
{
	state=sgnNone;
	profileobjecttype=potStartNGoGPSLabel;
	PercentageImagePaths=new TStringList();
	PercentageImagePaths->Add((System::UnicodeString)"PNGIMAGE_452");
	PercentageImagePaths->Add((System::UnicodeString)"PNGIMAGE_453");
	PercentageImagePaths->Add((System::UnicodeString)"PNGIMAGE_454");
	PercentageImagePaths->Add((System::UnicodeString)"PNGIMAGE_455");
	PercentageImagePaths->Add((System::UnicodeString)"PNGIMAGE_450");
	//NormalGlyph=LoadPNG32Resource(PercentageImagePaths->Strings[iconindex]); //_210");
	NormalCoordinated="PNGIMAGE_450";
	NormalUnCoordinated="PNGIMAGE_451";
	RefCollector=new TReferencesCollector(this, ReferencesQ, PercentageImagePaths,
		NormalUnCoordinated, 2500, Manager);
	RefCollector->OnCollected=OnCoordinated;
}
//---------------------------------------------------------------------------

__fastcall TStartNGoGPSLabel::~TStartNGoGPSLabel()
{
	if(RefCollector) delete RefCollector;
	RefCollector=NULL;
	if(PercentageImagePaths) delete PercentageImagePaths;
	PercentageImagePaths=NULL;
}
//---------------------------------------------------------------------------

void __fastcall TStartNGoGPSLabel::OnCoordinated()
{
	State=sgnPlaced;
}
//---------------------------------------------------------------------------

void __fastcall TStartNGoGPSLabel::Redraw(TObject *Sender, TBitmap32 *Bmp)
{
	//if(RefCollector->Percentage<100) drawglyph=false;
	//else
	drawglyph=true;

	/*if(Host && Coordinate && CoordinatesRect && CoordinatesRect->Valid)
		dp=((TMapsContainersCollection*)Host)->CoordinatesRectToScreen(
			Coordinate->GetXY(((TMapsContainersCollection*)Host)->CoordinateSystem));
	else dp=((TMapsContainersCollection*)Host)->ViewportAreaToScreen(StartingVPP);*/

	TStartNGoLabel::Redraw(Sender, Bmp);
}
//---------------------------------------------------------------------------
