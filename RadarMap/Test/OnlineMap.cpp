//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "OnlineMap.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "ImageButton"
#pragma link "G32_ProgressBar"
#pragma link "GR32_Image"
#pragma resource "*.dfm"
TOnlineMapForm *OnlineMapForm;

//---------------------------------------------------------------------------
__fastcall TOnlineMapForm::TOnlineMapForm(TComponent* Owner, TRadarMapManager *aManager,
	TCoordinateSystem aCS) : TForm(Owner)
{
	ResMov=new TResourceMovie();
	ResMov->Image=Image321;
	ResMov->Endless=true;
	ResMov->FramePictureType=ptPNG;
	ResMov->FrameInterval=100;
	//URL="";
	manager=aManager;
	mapobjects=NULL;
	imc=NULL;

	mapcenter=new TGPSCoordinate();
	if(manager && manager->Settings)
	{
		TDoublePoint dp;

		mapcenter->SetXY(manager->Settings->LastMapCS, manager->Settings->LastMapCenter);
		dp=mapcenter->GetXY(aCS);
		if(mapcenter->GetCSDimension(aCS)==csdDegrees)
		{
			XEdit->Text=FloatToStrF(dp.X, ffFixed, 10, 6);
			YEdit->Text=FloatToStrF(dp.Y, ffFixed, 10, 6);
		}
		else
		{
			XEdit->Text=FloatToStrF(dp.X, ffFixed, 10, 2);
			YEdit->Text=FloatToStrF(dp.Y, ffFixed, 10, 2);
		}
		if(aCS==csUTM)
		{
			ZoneNumCBox->ItemIndex=ZoneNumCBox->Items->IndexOf(IntToStr(dp.Temp[0]));
			ZoneCharCBox->ItemIndex=ZoneCharCBox->Items->IndexOf((AnsiString)(dp.Temp[1]));
		}
		MapImageZoom=manager->Settings->LastMapZoom;
	}
	else MapImageZoom=16;
	ZoomLabel->Caption="x"+IntToStr(MapImageZoom);
	/*UtmZonePanel->Visible=(value==csUTM);
	ZoomPanel->Top=142+64*(int)(UtmZonePanel->Visible);
	csLabel->Caption=mapcenter->GetCSName(aCS);
	switch(aCS)
	{
		case csLatLon:
			Label13->Caption="X - Longitude, Y - Latitude [deg]";
			Label13->Visible=true;
			break;
		case csUTM:
			Label13->Caption="X - Easting, Y - Northing [m]";
			Label13->Visible=true;
			break;
		default:
			Label13->Visible=false;
	}*/
	coordinatesystem=aCS;
	CoordinateSystem=aCS;
	RefCollector=NULL;

	CreateMapObjects();

	MA=tmaNone;

	TitleDown=false;
	CRS_GUIDs = new TStringList();
	OkImageButton->Enabled=true;
}
//---------------------------------------------------------------------------

__fastcall TOnlineMapForm::~TOnlineMapForm()
{
	delete CRS_GUIDs;
	delete mapcenter;
	delete mapobjects;
	if(RefCollector) delete RefCollector;
}
//---------------------------------------------------------------------------

void __fastcall TOnlineMapForm::writeCoordinateSystem(TCoordinateSystem value)
{
	AnsiString str;
	double z;
	TGPSCoordinate *c;
	TDoublePoint dp;

	//if(coordinatesystem!=value)
	//{
		try
		{
			dp=DoublePoint(StrToFloat(XEdit->Text), StrToFloat(YEdit->Text));
			if(coordinatesystem==csUTM)
			{
				dp.Temp[0]=StrToInt(ZoneNumCBox->Text);
				dp.Temp[1]=ZoneCharCBox->Text[1];
			}
			mapcenter->SetXY(coordinatesystem, dp);
		}
		catch(...) {}
		coordinatesystem=value;
		csLabel->Caption=mapcenter->GetCSName(value);
		switch(value)
		{
			case csLatLon:
				Label13->Caption="X - Longitude, Y - Latitude [deg]";
				Label13->Visible=true;
				break;
/*			case csUTM:
				Label13->Caption="X - Easting, Y - Northing [m]";
				Label13->Visible=true;
				break;*/
			default:
				Label13->Visible=false;
		}
		UtmZonePanel->Visible=(value==csUTM);
		ZoomPanel->Top=142+64*(int)(UtmZonePanel->Visible);
		dp=mapcenter->GetXY(coordinatesystem);
		if(mapcenter->GetCSDimension(coordinatesystem)==csdDegrees)
		{
			XEdit->Text=FloatToStrF(dp.X, ffFixed, 10, 6);
			YEdit->Text=FloatToStrF(dp.Y, ffFixed, 10, 6);
		}
		else
		{
			XEdit->Text=FloatToStrF(dp.X, ffFixed, 10, 2);
			YEdit->Text=FloatToStrF(dp.Y, ffFixed, 10, 2);
		}
		if(value==csUTM)
		{
			ZoneNumCBox->ItemIndex=ZoneNumCBox->Items->IndexOf(IntToStr(dp.Temp[0]));
			ZoneCharCBox->ItemIndex=ZoneCharCBox->Items->IndexOf((AnsiString)(dp.Temp[1]));
		}
		//ReDownloadMap();
	//}
}
//---------------------------------------------------------------------------

void __fastcall TOnlineMapForm::RecolorReferencesPBar()
{
	unsigned char r, r2, g, g2, b, b2;
	float p, mm;

	r=GetRValue(0x00C0673F);
	g=GetGValue(0x00C0673F);
	b=GetBValue(0x00C0673F);
	r2=GetRValue(0x00FFA047);
	g2=GetGValue(0x00FFA047);
	b2=GetBValue(0x00FFA047);
	if(ReferencesPBar->Position<ReferencesPBar->Min) p=ReferencesPBar->Min;
	else if(ReferencesPBar->Position>ReferencesPBar->Max) p=ReferencesPBar->Max;
	else p=ReferencesPBar->Position;
	mm=ReferencesPBar->Max-ReferencesPBar->Min;
	r+=(unsigned char)(p*(float)(r2-r)/mm);
	g+=(unsigned char)(p*(float)(g2-g)/mm);
	b+=(unsigned char)(p*(float)(b2-b)/mm);
	ReferencesPBar->Color=(TColor)RGB(r, g, b);
}
//---------------------------------------------------------------------------

void __fastcall TOnlineMapForm::CloseIButtonClick(TObject *Sender)
{
	if(MoviePanel->Visible) CancelIButtonClick(CloseMovieButton);
	else
	{
		ModalResult=mrCancel;
	}
}

//---------------------------------------------------------------------------
void __fastcall TOnlineMapForm::ContentsIButtonClick(TObject *Sender)
{
	ContentsIButton->Down=true;
	PreviewIButton->Down=false;
	if(Notebook1->PageIndex!=Notebook1->Pages->IndexOf("CoordinatePage"))
	{
		XEdit->ReadOnly=false;
		YEdit->ReadOnly=false;
    }
	Notebook1->PageIndex=Notebook1->Pages->IndexOf("CoordinatePage");
}
//---------------------------------------------------------------------------

void __fastcall TOnlineMapForm::FormShow(TObject *Sender)
{
	Timer1Timer(Sender);
	if(XEdit->ReadOnly) HideCaret(XEdit->Handle);
	if(YEdit->ReadOnly) HideCaret(YEdit->Handle);
	ContentsIButtonClick(ContentsIButton);
}
//---------------------------------------------------------------------------

void __fastcall TOnlineMapForm::XEditChange(TObject *Sender)
{
	if(((TEdit*)Sender)->ReadOnly) HideCaret(((TEdit*)Sender)->Handle);
	//else ApplyIButton->Enabled=true;
}
//---------------------------------------------------------------------------

void __fastcall TOnlineMapForm::Timer1Timer(TObject *Sender)
{
	XEdit->Color=(TColor)0x00E9E2E1;
	YEdit->Color=(TColor)0x00E9E2E1;
	Timer1->Enabled=false;
}
//---------------------------------------------------------------------------

void __fastcall TOnlineMapForm::XEditKeyPress(TObject *Sender, wchar_t &Key)
{
	bool res=false;

	switch(Key)
	{
		case '0':
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
		case 8: res=true; break;
		case '.':
		case ',':
			if(((TEdit*)Sender)->Text.Pos(DecimalSeparator)>0) Key=0;
			else
			{
				Key=DecimalSeparator;
				res=true;
			}
			break;
		case '-':
			if(((TEdit*)Sender)->Text.Pos('-')>0) Key=0;
			break;
		default: ;
	}
	if(!res)
	{
		//((TEdit*)Sender)->Text="0.00";
		((TEdit*)Sender)->Color=(TColor)0xD0D0FE;
		if(!ReferencesPBar->Visible) ((TEdit*)Sender)->SetFocus();
		Timer1->Enabled=true;
		Key=0;
	}
}
//---------------------------------------------------------------------------

void __fastcall TOnlineMapForm::OkImageButtonClick(TObject *Sender)
{
	TDoublePoint dp;

	if(mapcenter)
	{
		try
		{
			dp=DoublePoint(StrToFloat(XEdit->Text), StrToFloat(YEdit->Text));
			if(coordinatesystem==csUTM)
			{
				dp.Temp[0]=StrToInt(ZoneNumCBox->Text);
				dp.Temp[1]=ZoneCharCBox->Text[1];
			}
			mapcenter->SetXY(coordinatesystem, dp);
			manager->Settings->LastMapCS=coordinatesystem;
			manager->Settings->LastMapCenter=dp;
			manager->Settings->LastMapZoom=mapzoom;
			ModalResult=mrOk;
		}
		catch(...) {ModalResult=mrCancel;}
	}
}
//---------------------------------------------------------------------------


void __fastcall TOnlineMapForm::FormHide(TObject *Sender)
{
	Close();
}
//---------------------------------------------------------------------------

void __fastcall TOnlineMapForm::XEditExit(TObject *Sender)
{
	try
	{
		StrToFloat(((TEdit*)Sender)->Text);
	}
	catch(Exception &e)
	{
		((TEdit*)Sender)->Text="0.00";
		((TEdit*)Sender)->Color=(TColor)0xD0D0FE;
		((TEdit*)Sender)->SetFocus();
		Timer1->Enabled=true;
	}
}
//---------------------------------------------------------------------------

void __fastcall TOnlineMapForm::AcquireIButtonClick(TObject *Sender)
{
	try
	{
		ReferencesPBar->Position=0;
		//RecolorReferencesPBar();
		ReferencesPBar->Visible=true;
		StartMovie(true, rmtGlobeOnline);//rmtSatellite);//
		if(!RefCollector)
		{
			RefCollector=new TReferencesCollector(NULL, Manager->Settings->ReferencePointCoordinatesQ, NULL,
				"", 0, Manager);
			RefCollector->OnPercentage=ChangePercentage;
			RefCollector->OnCollected=ReferencesCollected;
		}
		else RefCollector->StartReferencesCollection();
	}
	catch(...) {}
}
//---------------------------------------------------------------------------

void __fastcall TOnlineMapForm::ChangePercentage()
{
	try
	{
		if(RefCollector && Visible)
		{
			ReferencesPBar->Position=(int)(RefCollector->Percentage*100.);
			//RecolorReferencesPBar();
		}
	}
	catch(...) {}
}
//---------------------------------------------------------------------------

void __fastcall TOnlineMapForm::ReferencesCollected()
{
	TDouble3DPoint dp3;

	try
	{
		if(RefCollector)
		{
			ReferencesPBar->Position=RefCollector->Percentage;
			//RecolorReferencesPBar();
			if(RefCollector->Coordinate)
			{
				dp3=RefCollector->Coordinate->GetXYH(coordinatesystem);
				if(RefCollector->Coordinate->GetCSDimension(coordinatesystem)==csdDegrees)
				{
					XEdit->Text=FloatToStrF(dp3.X, ffFixed, 10, 6);
					YEdit->Text=FloatToStrF(dp3.Y, ffFixed, 10, 6);
				}
				else
				{
					XEdit->Text=FloatToStrF(dp3.X, ffFixed, 10, 2);
					YEdit->Text=FloatToStrF(dp3.Y, ffFixed, 10, 2);
				}
				if(coordinatesystem==csUTM)
				{
					ZoneNumCBox->ItemIndex=ZoneNumCBox->Items->IndexOf(IntToStr(dp3.Temp[0]));
					ZoneCharCBox->ItemIndex=ZoneCharCBox->Items->IndexOf((AnsiString)(dp3.Temp[1]));
				}
				mapcenter->SetXYH(coordinatesystem, dp3);
				//ReDownloadMap();
			}
			StopMovie();
		}
	}
	catch(...) {}
}
//---------------------------------------------------------------------------

void __fastcall TOnlineMapForm::CancelIButtonClick(TObject *Sender)
{
	try
	{
		if(Visible)
		{
			if(Manager && Manager->GlobalStopItEvent) SetEvent(Manager->GlobalStopItEvent);
			if(RefCollector)
			{
				RefCollector->StopReferencesCollection();
			}
			ContentsIButtonClick(ContentsIButton);
		}
		StopMovie();
	}
	catch(...) {}
}
//---------------------------------------------------------------------------

void __fastcall TOnlineMapForm::TitlePanelMouseDown(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y)
{
	TitleDown=true;
	TitleXY=ClientToScreen(Types::TPoint(X, Y));
}
//---------------------------------------------------------------------------

void __fastcall TOnlineMapForm::TitlePanelMouseMove(TObject *Sender, TShiftState Shift,
		  int X, int Y)
{
	Types::TPoint tmp;

	if(TitleDown)
	{
		tmp=ClientToScreen(Types::TPoint(X, Y));
		Left+=tmp.x-TitleXY.x;
		Top+=tmp.y-TitleXY.y;
		TitleXY=tmp;
	}
}
//---------------------------------------------------------------------------

void __fastcall TOnlineMapForm::TitlePanelMouseUp(TObject *Sender, TMouseButton Button,
	TShiftState Shift, int X, int Y)
{
	TitleDown=false;
}
//---------------------------------------------------------------------------

void __fastcall TOnlineMapForm::csLabelClick(TObject *Sender)
{
	Types::TPoint p;
	TGPSCoordinate *c;
	TMenuItem *NewItem;
	TPlugInObject *plugin;

	if(!MoviePanel->Visible)
	{
		CSPopupMenu->Items->Clear();
		try
		{
			c = new TGPSCoordinate();
			try
			{
				for(int i = 0; i < csTotalFixed; i++)
				{
					NewItem=new TMenuItem(this);
					NewItem->Caption=c->GetCSName((TCoordinateSystem)i)+" ("+c->GetCSEllipsoid((TCoordinateSystem)i)+")";
					NewItem->Tag=i;
					NewItem->OnClick=CSMenuItemClick;
					CSPopupMenu->Items->Add(NewItem);
				}
				CRS_GUIDs->Clear();
				if(PlugInManager && PlugInManager->PlugInsList->Count>0)
				{
					for(int i=0; i< PlugInManager->PlugInsList->Count; i++)
					{
						if(PlugInManager->PlugInsList->Items[i] && PlugInManager->PlugInsList->Items[i]->Type==pitCRS)
						{
							plugin = PlugInManager->PlugInsList->Items[i];
							NewItem=new TMenuItem(this);
							NewItem->Caption=plugin->Name;
							NewItem->Tag=i+csTotalFixed;
							NewItem->OnClick=CSMenuItemClick;
							CRS_GUIDs->Add(plugin->GUID);
							CSPopupMenu->Items->Add(NewItem);
						}
					}
				}
				p=Types::TPoint(csLabel->Left+PagesPanel->Left, csLabel->Top+PagesPanel->Top+csLabel->Height);
				p=ClientToScreen(p);
				CSPopupMenu->Popup(p.x, p.y);
			}
			catch(...) {}
		}
		__finally
		{
			delete c;
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TOnlineMapForm::CSMenuItemClick(TObject *Sender)
{
	int i;

	if(Sender)
	{
		i=((TMenuItem*)Sender)->Tag;
		if(i<csTotalFixed) CoordinateSystem=(TCoordinateSystem)i;
		else
		{
			i-=csTotalFixed;
			if(i<CRS_GUIDs->Count)
			{
				PlugInManager->SetActive(CRS_GUIDs->Strings[i], pitCRS);
				CoordinateSystem=csDLL;
			}
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TOnlineMapForm::PreviewIButtonClick(TObject *Sender)
{
	bool b;
	TDoublePoint dp;

	ContentsIButton->Down=false;
	PreviewIButton->Down=true;
	b=(Notebook1->PageIndex!=Notebook1->Pages->IndexOf("Preview"));
	Notebook1->PageIndex=Notebook1->Pages->IndexOf("Preview");
	if(b && mapobjects && mapobjects->Container)// && mapobjects->Zoom)
	{
		try
		{
			dp=DoublePoint(StrToFloat(XEdit->Text), StrToFloat(YEdit->Text));
			if(coordinatesystem==csUTM)
			{
				dp.Temp[0]=StrToInt(ZoneNumCBox->Text);
				dp.Temp[1]=ZoneCharCBox->Text[1];
			}
			mapcenter->SetXY(coordinatesystem, dp);
		}
		catch(...) {}

		try
		{
			mapobjects->Container->ZoomUpdate();

			if(!imc)
			{
				mapobjects->Container->Clear();
				mapobjects->Zoom->Clear();
				imc=new TOsmInetMap(mapobjects->Container, Manager->Settings,
					mapobjects->Container->ViewportArea);
			}
			bool b=imc->LockedCS;
			imc->LockedCS=false;
			imc->CoordinateSystem=CoordinateSystem;
			imc->LockedCS=b;
			imc->MapImageWidth=MapImage->Width << 1;
			imc->MapImageHeight=MapImage->Height << 1;
			imc->MapCenterLatitude=mapcenter->Latitude;// OnlineLoadDlg->MapCenterLatitude;
			imc->MapCenterLongitude=mapcenter->Longitude;// OnlineLoadDlg->MapCenterLongitude;
			imc->MapImageZoom=MapImageZoom; //OnlineLoadDlg->MapImageZoom;
			imc->MapViewType=imvtMap;//OnlineLoadDlg->MapViewType;
			ReDownloadMap();
		}
		catch(...) {}
	}
}
//---------------------------------------------------------------------------

void __fastcall TOnlineMapForm::CreateMapObjects()
{
	if(Manager)
	{
		if(mapobjects) delete mapobjects;
		mapobjects=new TMapObjectsContainer(MapImage, true, &MA, DisableMouseAction, Manager->Settings, true);
		mapobjects->SmallSize=Manager->Settings->SmallSizeControls;
		if(mapobjects->Container)
		{
			mapobjects->Container->CoordinateSystem=coordinatesystem;
			mapobjects->Container->Progress=new TToolProgressBar(mapobjects, MapImage, Manager);
		}
		if(mapobjects->MapScale)
		{
			mapobjects->MapScale->ToolAlign=vtoaLeftBottom;
			mapobjects->MapScale->CoordinateSystem=&coordinatesystem;
		}
		if(mapobjects->Messages) mapobjects->Messages->Align=msaBottom;

		MapToolbar=new TSliderToolbar(mapobjects, MapImage->GetViewportRect(), 48, staRight/*staLeft*/, true);
		MapToolbar->FitButtonsOnly=true;
		MapToolbar->Visible=true;
		MapToolbar->Transparent=true;

		ZoomInBtn=new TSliderToolButton(MapToolbar, Types::TRect(2, 102, 50, 150));
		ZoomInBtn->NormalGlyphName="PNGIMAGE_89";
		ZoomInBtn->DisabledGlyphName="PNGIMAGE_96";
		ZoomInBtn->HotGlyphName="PNGIMAGE_90";
		ZoomInBtn->Enabled=true;//false;
		ZoomInBtn->OnMouseUp=ZoomInMouseUp;

		ZoomOutBtn=new TSliderToolButton(MapToolbar, Types::TRect(2, 102, 50, 150));
		ZoomOutBtn->NormalGlyphName="PNGIMAGE_91";
		ZoomOutBtn->DisabledGlyphName="PNGIMAGE_97";
		ZoomOutBtn->HotGlyphName="PNGIMAGE_92";
		ZoomOutBtn->Enabled=true;//false;
		ZoomOutBtn->OnMouseUp=ZoomOutMouseUp;
	}
}
//---------------------------------------------------------------------------

void __fastcall TOnlineMapForm::ZoomInMouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y)
{
	/*if(mapobjects && mapobjects->Zoom && mapobjects->Image)
	{
		if(Manager->Settings->EasyZoomIn)
		{
			mapobjects->Zoom->ZoomIn();
			ZoomInBtn->Down = false;
		}
		else
		{
			ZoomInBtn->Down =!ZoomInBtn->Down;
			if(ZoomInBtn->Down) MA = tmaZoomIn;
			else DisableMouseAction();
		}
	}
	else ZoomInBtn->Down=false;*/
	ZoomBingMap(true);
	ZoomInBtn->Down=false;
}
//---------------------------------------------------------------------------

void __fastcall TOnlineMapForm::ZoomOutMouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y)
{
	ZoomInBtn->Down = false;
	ZoomBingMap(false);
	DisableMouseAction();
}
//---------------------------------------------------------------------------

void __fastcall TOnlineMapForm::StartMovie(bool WaitOrStop, TResourceMovieType rmt)
{
	WaitLabel->Visible=WaitOrStop;
	StopButton->Enabled=!WaitOrStop;
	StopButton->Visible=!WaitOrStop;
	MoviePanel->Visible=true;
	ProcessMessages();
	ContentsIButton->Enabled=false;
	OkImageButton->Enabled=false;
	PreviewIButton->Enabled=false;
	AcquireIButton->Enabled=false;
	XEdit->ReadOnly=true;
	YEdit->ReadOnly=true;
	ZoomTrackBar->Enabled=false;
	if(ResMov) ResMov->Start(rmt);
}
//---------------------------------------------------------------------------

void __fastcall TOnlineMapForm::StopMovie()
{
	try
	{
		if(Visible)
		{
			if(Manager && Manager->GlobalStopItEvent) SetEvent(Manager->GlobalStopItEvent);
			if(ResMov) ResMov->Stop();
			MoviePanel->Visible=false;
			ContentsIButton->Enabled=true;
			OkImageButton->Enabled=true;
			PreviewIButton->Enabled=true;
			AcquireIButton->Enabled=true;
			XEdit->ReadOnly=false;
			YEdit->ReadOnly=false;
			ZoomTrackBar->Enabled=true;
		}
	}
	catch(...) {}
}
//---------------------------------------------------------------------------

void __fastcall TOnlineMapForm::DisableMouseAction()
{
	switch(MA)
	{
		case tmaReferenceFlag: break;
		case tmaPosCorrection: break;
		case tmaStartStopWaypoint: break;
		case tmaStartNGo: break;
		case tmaZoomIn:
		case tmaNone:
		case tmaHand:
		default:
			break;
	}
	MA=tmaHand;
}
//---------------------------------------------------------------------------

void __fastcall TOnlineMapForm::ZoomTrackBarChange(TObject *Sender)
{
	mapzoom=ZoomTrackBar->Position;
	ZoomLabel->Caption="x"+IntToStr(ZoomTrackBar->Position);
}
//---------------------------------------------------------------------------

bool __fastcall TOnlineMapForm::ZoomBingMap(bool pIsZoomIn) {
	bool std_zoom = true;

	if (mapobjects && imc) {
		if (pIsZoomIn) imc->MapImageZoom++;
		else imc->MapImageZoom--;
		if (MapImageZoom != imc->MapImageZoom) {
			MapImageZoom = imc->MapImageZoom;
			mapobjects->Zoom->CurrentZoom = 1;
			ReDownloadMap();
		}
		std_zoom = false;
	}
	return std_zoom;
}
//---------------------------------------------------------------------------

void __fastcall TOnlineMapForm::ReDownloadMap()
{
	if(mapobjects && imc)
		new TInetMapLoader(Manager, imc, imc->Progress);
}
//---------------------------------------------------------------------------

