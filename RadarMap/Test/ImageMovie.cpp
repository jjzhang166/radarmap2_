//---------------------------------------------------------------------------

#pragma hdrstop

#include "ImageMovie.h"
#include <jpeg.hpp>
#include "GR32_PNG_Load.h"
#include <vcl.h>

//---------------------------------------------------------------------------

#pragma package(smart_init)
//#pragma link "ImageButton"
//#pragma link "GR32_Image"

__fastcall TResourceMovie::TResourceMovie() : TObject()
{
	image=NULL;
	endless=true;
	ptype=ptBitmap;
	frameindex=0;
	frames=new TStringList();
	timer=new TTimerThread(70);
	timer->OnTimer=&DrawFrame;
}
//---------------------------------------------------------------------------

__fastcall TResourceMovie::~TResourceMovie()
{
	Stop();
	if(frames) delete frames;
	frames=NULL;
	if(timer)
	{
		timer->OnTimer=NULL;
		timer->AskForTerminate();
		if(timer->ForceTerminate()) delete timer;
		timer=NULL;
	}
}
//---------------------------------------------------------------------------

void __fastcall TResourceMovie::DrawFrame()
{
	TBitmap32 *tmp;
	TJPEGImage *jp;
	TResourceStream *Stream;
	UnicodeString str;

	if(Image && Image->Visible && Image->Owner && ((TWinControl*)Image->Owner)->Visible && frames && Count>0)
	{
		str=frames->Strings[frameindex];
		if(str!="")
		{
			switch(ptype)
			{
				case ptBitmap:
					jp=NULL;
					Stream=new TResourceStream((int)HInstance, str, MAKEINTRESOURCEW(10));
					tmp=new TBitmap32();
					tmp->LoadFromStream(Stream);
					break;
				case ptPNG:
					jp=NULL;
					Stream=NULL;
					tmp=LoadPNG32Resource(str);
					break;
				case ptJPEG:
					Stream=new TResourceStream((int)HInstance, str, MAKEINTRESOURCEW(10));
					jp=new TJPEGImage();
					jp->LoadFromStream(Stream);
					tmp=new TBitmap32();
					tmp->Assign(jp);
					break;
				default:
					tmp=NULL;
					Stream=NULL;
					jp=NULL;
			}
			Image->Bitmap->BeginUpdate();
			try
			{
				Image->Bitmap->Clear(Color32(0,0,0,0));
				if(tmp) Image->Bitmap->Draw(0, 0, tmp);
			}
			__finally
			{
				if(jp) delete jp;
				if(Stream) delete Stream;
				if(tmp) delete tmp;
				Image->Bitmap->EndUpdate();
				Image->Bitmap->Changed();
			}
			frameindex++;
			if(frameindex>=Count)
			{
				frameindex=0;
				if(!endless) Stop();
			}
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TResourceMovie::Start(TResourceMovieType rmt)
{
	int i;

	Stop();
	Clear();

	switch(rmt)
	{
		case rmtNone:
		case rmtSettings:
			for(i=0; i<5; i++) Add("Movie_Settings"+IntToStr(i+1));
			break;
		case rmtCalibDist:
			for(i=0; i<18; i++) Add("Movie_Dist_Calib"+IntToStr(i+1));
			break;
		case rmtCalibAngle:
			for(i=0; i<18; i++) Add("Movie_Angle_Calib"+IntToStr(i+1));
			break;
		case rmtBookOpen:
			for(i=0; i<7; i++) Add("Book_movie_"+IntToStr(i+1));
			break;
		case rmtBookDelete:
			for(i=0; i<12; i++) Add("Book_delete_movie_"+IntToStr(i+1));
			break;
		case rmtBookAdd:
			for(i=11; i>=0; i--) Add("Book_delete_movie_"+IntToStr(i+1));
			break;
		case rmtBookUpdate:
			for(i=0; i<12; i++) Add("Book_delete_movie_"+IntToStr(i+1));
			Add("Book_delete_movie_1");
			for(i=11; i>=0; i--) Add("Book_delete_movie_"+IntToStr(i+1));
			break;
		case rmtSatellite:
			for(i=0; i<7; i++) Add("Satellite_movie_"+IntToStr(i+1));
			break;
		case rmtGlobe:
			for(i=0; i<25; i++) Add("Globe_movie_"+IntToStr(i+1));
			break;
		case rmtGlobeOnline:
			for(i=0; i<25; i++) Add("Globe_online_movie_"+IntToStr(i+1));
			break;
		default: return;
	}
	Start();
}
