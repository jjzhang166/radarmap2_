//---------------------------------------------------------------------------

#ifndef InterpolateGPSH
#define InterpolateGPSH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include "Profile.h"
//---------------------------------------------------------------------------
enum TInterpolationType {itNone=0, itPitch=1, itRoll=2, itPitchRoll=3, itHeading=4, itHeadingPitchRoll=5};

class TInterpolateGPS : public TThread
{
private:
protected:
		void __fastcall Execute();
		TTrace *FirstPtr;
		TProfile *Prof;
public:
		__fastcall TInterpolateGPS(TProfile* P, TTrace* Ptr);
		void __fastcall InterpolateTraces(int Pos);

};

long double DegMinToDeg(long double lt);
long double DegToDegMin(long double lt);
void IndInterpolateTraces(TProfile* Prof, TTrace *FirstPtr, int Pos, bool Backward);
void InterpolateTraces(TProfile* Prof, TTrace *FirstPtr, TInterpolationType What, bool Backward);
//---------------------------------------------------------------------------
#endif
