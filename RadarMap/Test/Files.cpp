//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Files.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "ImageButton"
#pragma resource "*.dfm"

#define TVIS_CHECKED 0x2000
#define TVIS_UNCHECKED 0x1000

TFilesForm *FilesForm;

//---------------------------------------------------------------------------

__fastcall TFilesForm::TFilesForm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------

__fastcall TFilesForm::~TFilesForm()
{
	Button1Click(NULL);
}
//---------------------------------------------------------------------------

void __fastcall TFilesForm::EmptyNodeData(TTreeNode* node)
{
	int i;

	if(node && node->Count>0)
	{
		i=0;
		do
		{
			if(node->Item[i] && node->Item[i]->Count>0) EmptyNodeData(node->Item[i]);
			else if(node->Data)
			{
				delete[] (char*)node->Data;
				node->Data=NULL;
			}
			i++;
		} while(i<node->Count);
	}
}

void __fastcall TFilesForm::Button1Click(TObject *Sender)
{
	TTreeNode *node, *item;

	if(TreeView->Items->Count>0)
	{
		for(int i=0; i<TreeView->Items->Count; i++)
		{
			EmptyNodeData(TreeView->Items->Item[i]);
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TFilesForm::TreeViewDblClick(TObject *Sender)
{
	TTreeNode *node;
	Types::TPoint p;
	char *d;
	AnsiString S;

	GetCursorPos(&p);
	p=TreeView->ScreenToClient(p);
	node=TreeView->GetNodeAt(p.x, p.y);
	if(node && node->Data)
	{
		try
		{
			d=(char*)node->Data;
			S=(char*)(d+2);
			S.SetLength(*(unsigned short int*)d);
			if(S!=NULL && S!="" && FileExists(S))
			{
				ShellExecute(0, "open", S.c_str(), NULL, NULL, SW_SHOW);
			}
		}
		catch(Exception &e) {}
		TreeView->Selected=node;
	}
}
//---------------------------------------------------------------------------

void __fastcall TFilesForm::Image2Click(TObject *Sender)
{
	ModalResult=mrClose;
}
//---------------------------------------------------------------------------

