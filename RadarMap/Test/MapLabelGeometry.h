//---------------------------------------------------------------------------

#ifndef MapLabelGeometryH
#define MapLabelGeometryH
//---------------------------------------------------------------------------
#include "LocalMap.h"

enum TMapLabelGeometryType {mlgtNone=0, mlgtLine=1, mlgtPolyLine=2};

const AnsiString TMapLabelGeometryTypeStrings[] = {
	"mlgtNone",
	"mlgtLine",
	"mlgtPolyLine"
};

TMapLabelGeometryType StrToTMapLabelGeometryType(AnsiString str);

typedef struct _TMapLabelGeometryEdge
{
	TGPSCoordinate *C1, *C2; //edge vertexes
	Types::TPoint P; //Point on the edge (x,y) in px
	double distance;
	int Index;
} TMapLabelGeometryEdge;

class TMapLabelGeometry : public TProfileObject
{
private:
	TMapLabelGeometryType geometrytype;
	TColor32 color, bordercolor;
	int thickness, editvertexindex, id;
	AnsiString name, description;
	TProfileLabelType labeltype;
	TMapLabelType maplabeltype;
	bool singlelabeltype;
	TGPSCoordinatesList* CoordinatesList;
	TMyObjectsList *LabelsList;
protected:
	int minvertexestodraw;
	TObject *Host;
	void __fastcall Clear();
	TGPSCoordinate* __fastcall readStartVertex() {if(CoordinatesList->Count>0) return CoordinatesList->Items[0]; else return NULL;}
	TGPSCoordinate* __fastcall readEndVertex() {if(CoordinatesList->Count>0) return CoordinatesList->Items[CoordinatesList->Count-1]; else return NULL;}
	int __fastcall readVertexesCount() {return  CoordinatesList->Count;}
	TGPSCoordinate* __fastcall readVertexes(int Index) {if(Index>=0 && Index<CoordinatesList->Count) return CoordinatesList->Items[Index]; else return NULL;}
	TMapLabel* __fastcall readMapLabels(int Index) {if(Index>=0 && Index<LabelsList->Count) return dynamic_cast<TMapLabel*>(LabelsList->Items[Index]); else return NULL;}
	void __fastcall writeColor(TColor32 value) {if(color!=value) {color=value; Update();}}
	void __fastcall writeBorderColor(TColor32 value) {if(bordercolor!=value) {bordercolor=value; Update();}}
	void __fastcall writeThickness(int value) {if(thickness!=value) {thickness=value; Update();}}
	void __fastcall writeEditVertexIndex(int value) {if(editvertexindex!=value) {editvertexindex=value; Update();}}
public:
	__fastcall TMapLabelGeometry(TMyObject *AOwner, Types::TRect OutputRect,
		TObject* aHost, TObject* RadarMapManager, TMapLabelGeometryType aType,
		TProfileLabelType aLabelType=pltNone);
	__fastcall ~TMapLabelGeometry() {Clear(); delete CoordinatesList; delete LabelsList;}

	virtual void __fastcall Redraw(TObject *Sender, TBitmap32 *Bmp) {}
	virtual void __fastcall MouseMove(System::TObject* Sender,
		  Classes::TShiftState Shift, int X, int Y, TCustomLayer *Layer) {}
	virtual void __fastcall MouseDown(TObject *Sender,
		  TMouseButton Button, TShiftState Shift, int X, int Y, TCustomLayer *Layer) {}
	virtual void __fastcall MouseUp(TObject *Sender,
		  TMouseButton Button, TShiftState Shift, int X, int Y, TCustomLayer *Layer);

	virtual int __fastcall AddPoint(TMapLabel* aLabel);
	void __fastcall DeletePoint(TMapLabel* aLabel);

	int __fastcall IndexOf(TMapLabel* aLabel) {return LabelsList->IndexOf(aLabel);}

	TMapLabelGeometryEdge __fastcall FindNearestEdgeAtXY(Types::TPoint p);

	virtual bool __fastcall IsInClientRect(Types::TPoint value) {return TGraphicObject::IsInClientRect(value);}
	virtual bool __fastcall IsInClientRect(Types::TRect value) {return TGraphicObject::IsInClientRect(value);}

	void __fastcall ChangeDesign(int aThickness, TColor32 aColor, TColor32 aBorderColor) {thickness=aThickness; color=aColor; bordercolor=aBorderColor; Update();}

	__property TMapLabelGeometryType GeometryType = {read=geometrytype};
	__property TGPSCoordinate* StartVertex = {read=readStartVertex};
	__property TGPSCoordinate* EndVertex = {read=readEndVertex};
	__property int VertexesCount = {read=readVertexesCount};
	__property TGPSCoordinate* Vertexes[int Index] = {read=readVertexes};
	__property TMapLabel* Labels[int Index] = {read=readMapLabels};
	__property TColor32 Color = {read=color, write=writeColor};
	__property TColor32 BorderColor = {read=bordercolor, write=writeBorderColor};
	__property int Thickness = {read=thickness, write=writeThickness};
	__property TProfileLabelType LabelType = {read=labeltype, write=labeltype};
	__property TMapLabelType MapLabelType = {read=maplabeltype, write=maplabeltype};
	__property bool SingleLabelType = {read=singlelabeltype, write=singlelabeltype};
	__property int MinVertexesToDraw = {read=minvertexestodraw};
	__property int EditVertexIndex = {read=editvertexindex, write=writeEditVertexIndex};
	__property AnsiString Description = {read=description, write=description};
	__property AnsiString Name = {read=name, write=name};
	__property int ID = {read=id, write=id};
};

class TMapLabelPolyLineGeometry : public TMapLabelGeometry
{
private:
protected:
	void __fastcall DrawLine(TBitmap32 *Bmp, TColor32 Color, TColor32 Border, int Thickness, bool Shadow);
public:
	__fastcall TMapLabelPolyLineGeometry(TMyObject *AOwner, Types::TRect OutputRect,
		TObject* aHost,	TObject* RadarMapManager, TMapLabelGeometryType aType=mlgtPolyLine,
		TProfileLabelType aLabelType=pltNone) :	TMapLabelGeometry(AOwner, OutputRect, aHost, RadarMapManager, aType, aLabelType) {minvertexestodraw=2;}

	virtual bool __fastcall IsInClientRect(Types::TPoint value);
	virtual bool __fastcall IsInClientRect(Types::TRect value);

	virtual void __fastcall Redraw(TObject *Sender, TBitmap32 *Bmp);
};

class TMapLabelLineGeometry : public TMapLabelPolyLineGeometry
{
private:
protected:
public:
	__fastcall TMapLabelLineGeometry(TMyObject *AOwner, Types::TRect OutputRect, TObject* aHost,
		TObject* RadarMapManager, TProfileLabelType aLabelType=pltNone) :
		TMapLabelPolyLineGeometry(AOwner, OutputRect, aHost, RadarMapManager, mlgtLine, aLabelType) {}

	void __fastcall MouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y, TCustomLayer *Layer) {}

	int __fastcall AddPoint(TMapLabel* aLabel) {if(VertexesCount<2) return TMapLabelGeometry::AddPoint(aLabel); else return -1;}
};

#endif
