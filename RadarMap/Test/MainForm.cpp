//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "MainForm.h"
#include "Receiver.h"
//#include "Drawing.h"
#include "Radar.h"
#include "Map.h"
//#include "Manager.h"
#include "Splash.h"
//#include "Lambert.h";
#include "Disclaimer.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)

#pragma link "G32_ProgressBar"
#pragma link "GR32_Image"
#pragma link "ImageButton"
#pragma resource "*.dfm"
TRadarMapForm *RadarMapForm;
TContainerPanel *RadarPanel, *MapPanel;
TRadarDialog *RadarDialog;
TMapForm *MapForm;
TRadarMapManager *RadarMapManager;

//---------------------------------------------------------------------------
TRadarPanelsOrientation __fastcall TRadarMapForm::DetectOrientation()
{
	TRadarPanelsOrientation res;

	if(Width<Height) res=rpoHorizontal;
	else res=rpoVertical;

	return res;
}

//---------------------------------------------------------------------------
__fastcall TRadarMapForm::TRadarMapForm(TComponent* Owner)
	: TForm(Owner)
{
	TRadarPanelsOrientation rpo;

	InResizing=false;
	_defaultCS=csMetric;
	_defaultCSD=csdMeters;
	RadarMapManager=new TRadarMapManager(this);
	if(RadarMapManager->Settings)
	{
		if(RadarMapManager->Settings->PanelsOrientation==rpoAuto)
			rpo=DetectOrientation();
		else rpo=RadarMapManager->Settings->PanelsOrientation;
		WindowState=RadarMapManager->Settings->WindowState;
		if(WindowState!=wsMaximized)
		{
			BorderStyle=bsSizeable;
			Top=RadarMapManager->Settings->WindowTop;
			Left=RadarMapManager->Settings->WindowLeft;
			Width=RadarMapManager->Settings->WindowWidth;
			Height=RadarMapManager->Settings->WindowHeight;
			WindowState=RadarMapManager->Settings->WindowState;

		}
	}
	else
	{
		rpo=rpoVertical;
		WindowState=wsMaximized;
		BorderStyle=bsNone;
	}

	RadarPanel=new TContainerPanel(this);//, "Radar Data");
	RadarPanel->LockChilds=true;
	switch(rpo)
	{
		case rpoHorizontal:
			Splitter->Align=alBottom;
			Splitter->Cursor=crVSplit;
			RadarPanel->Align=alBottom;
			RadarPanel->Height=0;
			break;
		default:
		case rpoVertical:
			Splitter->Align=alRight;
			Splitter->Cursor=crHSplit;
			RadarPanel->Align=alRight;
			RadarPanel->Width=0;
			break;
	}

	MapPanel=new TContainerPanel(this);//, "Map Data");
	MapPanel->Align=alClient;
	MapPanel->Width=0;
	MapPanel->LockChilds=true;

	RadarPanel->Show();
	RadarPanel->Refresh();/**/

	RadarDialog=new TRadarDialog(this, RadarMapManager);
	RadarDialog->ManualDock(RadarPanel->Container, NULL, alRight);
	RadarDialog->Left=0;

	MapForm=new TMapForm(this, RadarMapManager);
	MapForm->ManualDock(MapPanel->Container, NULL, alClient);
	MapForm->Left=0;

	RadarMapManager->SetClearProfileFunction(RadarDialog->RebuildAndRefresh);
	RadarMapManager->CreateObjectContainer(RadarDialog->ProfImage, RadarDialog->ProfRect, RadarDialog->ScaleL);
	RadarMapManager->ObjectsContainer->OnMouseClick=RadarDialog->MouseClick;
	RadarDialog->Initialize();

	RadarMapManager->CreateMapObjects(MapForm->MapImage, &(MapForm->MA), &(MapForm->DisableMouseAction));
	RadarMapManager->MapObjects->DisableMouseAction=&MapForm->DisableMouseAction;
	MapForm->Initialize();

	Caption=(AnsiString)_RadarMapName;

	//RadarDialog->LocalMapContainer=MapForm->LocalMapContainer;
	/*RadarDialog->MapObjects=MapForm->MapObjects;

	MapForm->LocalMapContainer->ProfileScroller=RadarDialog->ProfileScroller;
	MapForm->ObjectsContainer=RadarDialog->ObjectsContainer;*/
}
//---------------------------------------------------------------------------

void __fastcall TRadarMapForm::FormDestroy(TObject *Sender)
{
	if(RadarMapManager)
	{
		delete RadarMapManager;
		RadarMapManager=NULL;
	}
	if(RadarDialog)
	{
		delete RadarDialog;
		RadarDialog=NULL;
	}
	if(RadarPanel)
	{
		delete RadarPanel;
		RadarPanel=NULL;
	}
	if(MapForm)
	{
		delete MapForm;
		MapForm=NULL;
	}
	if(MapPanel)
	{
		delete MapPanel;
		MapPanel=NULL;
	}
	//delete RadarMapManager;
}
//---------------------------------------------------------------------------

void __fastcall TRadarMapForm::FormShow(TObject *Sender)
{
/*	Top=0;
	Left=0;
	Width=Screen->Width;
	Height=Screen->Height;*/
	if(MapForm && RadarDialog)
	{
		MapForm->Left=0;
		MapForm->Top=0;
		MapForm->Height=Height;
		MapForm->Width=Width;
		RadarDialog->Left=0;
		RadarDialog->Top=0;
		RadarDialog->Height=Height;
		RadarDialog->Width=0;
		RadarDialog->Show();
		//RadarDialog->BringToFront();
		MapForm->Show();
		//MapForm->BringToFront();
		Timer1->Enabled=true;
#ifdef RadarMap_TRIAL
		if(Now()>TDateTime(AbyrvalgY0+AbyrvalgY1, AbyrvalgM0+AbyrvalgM1, AbyrvalgD0+AbyrvalgD1) ||
			Now()<TDateTime(AbyrvalgY0+AbyrvalgY1, AbyrvalgM0+AbyrvalgM1-7, AbyrvalgD0+AbyrvalgD1))
		{
			try
			{
				struct _exception except;

				except.type=PERIODICAL;
				_matherr(&except);
			}
			catch(Exception &e)
			{
				Application->MessageBox(e.Message.w_str(), ((UnicodeString)_RadarMapName).w_str(), MB_OK);
				Sleep(2000);
				Close();
			}
		}
#endif
    }
}
//---------------------------------------------------------------------------

void __fastcall TRadarMapForm::ReOrient()
{
	TRadarPanelsOrientation rpo;
	float wh;

	if(RadarMapManager && RadarMapManager->Settings)
	{
		if(RadarMapManager && RadarMapManager->Settings &&
			RadarMapManager->Settings->PanelsOrientation==rpoAuto) rpo=DetectOrientation();
		else rpo=RadarMapManager->Settings->PanelsOrientation;
		switch(rpo)
		{
			case rpoHorizontal:
				if(RadarPanel->Align!=alBottom)
				{
					if(Width>0) wh=(float)RadarPanel->Width/(float)Width;
					else wh=1.;
					RadarPanel->Align=alBottom;
					if(wh<1) RadarPanel->Height=(int)((float)Height*wh);
					Splitter->Align=alBottom;
					Splitter->Cursor=crVSplit;
				}
				break;
			default:
			case rpoVertical:
				if(RadarPanel->Align!=alRight)
				{
					if(Height>0) wh=(float)RadarPanel->Height/(float)Height;
					else wh=1.;
					Splitter->Align=alRight;
					Splitter->Cursor=crHSplit;
					RadarPanel->Align=alRight;
					if(wh<1) RadarPanel->Width=(int)((float)Width*wh);
				}
				break;
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TRadarMapForm::ReBorder(bool FromSettings)
{
	if(RadarMapManager && RadarMapManager->Settings && RadarMapManager->Settings->WindowState!=WindowState)
	{
		InResizing=true;
		try
		{
			try
			{
				if(FromSettings)
				{
					WindowState=RadarMapManager->Settings->WindowState;
					if(WindowState!=wsMaximized)
					{
						Top=RadarMapManager->Settings->WindowTop;
						Left=RadarMapManager->Settings->WindowLeft;
						Width=RadarMapManager->Settings->WindowWidth;
						Height=RadarMapManager->Settings->WindowHeight;
						BorderStyle=bsSizeable;
					}
					else
					{
						BorderStyle=bsNone;
						Top=0;
						Left=0;
						Width=Screen->Width;
						Height=Screen->Height;
					}
				}
				else
				{
					RadarMapManager->Settings->WindowState=WindowState;
					if(WindowState!=wsMaximized)
					{
						BorderStyle=bsSizeable;
						RadarMapManager->Settings->WindowTop=Top;
						RadarMapManager->Settings->WindowLeft=Left;
						RadarMapManager->Settings->WindowWidth=Width;
						RadarMapManager->Settings->WindowHeight=Height;
					}
					else
					{
						BorderStyle=bsNone;
						Top=0;
						Left=0;
						Width=Screen->Width;
						Height=Screen->Height;
					}
				}
            }
			catch(...) {}
		}
		__finally
		{
			InResizing=false;
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TRadarMapForm::FormResize(TObject *Sender)
{
	TRadarPanelsOrientation rpo;
	float wh;
	int NewWidth, NewHeight;

	if(!InResizing && !SplashForm && RadarMapManager && RadarMapManager->Settings)
	{
		if(RadarMapManager->Settings->WindowState!=wsMaximized)
		{
			NewWidth=RadarMapManager->Settings->WindowWidth;
			NewHeight=RadarMapManager->Settings->WindowHeight;
		}
		else
		{
			NewWidth=Screen->Width;
			NewHeight=Screen->Height;
		}
		if(!InResizing && RadarMapManager->Settings->PanelsOrientation==rpoAuto) ReOrient();
		if(!InResizing && RadarMapManager->Settings->WindowState!=WindowState) ReBorder(false);
		else if(WindowState!=wsMaximized)
		{
			RadarMapManager->Settings->WindowTop=Top;
			RadarMapManager->Settings->WindowLeft=Left;
			RadarMapManager->Settings->WindowWidth=Width;
			RadarMapManager->Settings->WindowHeight=Height;
		}
		if(RadarMapManager && RadarMapManager->Settings)
		{
			if(RadarMapManager->Settings->PanelsOrientation==rpoAuto)
				rpo=DetectOrientation();
			else rpo=RadarMapManager->Settings->PanelsOrientation;
		}
		else rpo=rpoVertical;
		switch(rpo)
		{
			case rpoHorizontal:
				if(NewHeight>0) wh=(float)RadarPanel->Height/(float)NewHeight;
				else wh=1.;
				if(wh<1) RadarPanel->Height=(int)((float)Height*wh);
				MapPanel->Height=Height-RadarPanel->Height-15;
				break;
			default:
			case rpoVertical:
				if(NewWidth>0) wh=(float)RadarPanel->Width/(float)NewWidth;
				else wh=1.;
				if(wh<1) RadarPanel->Width=(int)((float)Width*wh);
				MapPanel->Width=Width-RadarPanel->Width-15;
				break;
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TRadarMapForm::FormKeyDown(TObject *Sender, WORD &Key, TShiftState Shift)
{
	RadarDialog->FormKeyDown(Sender, Key, Shift);
}
//---------------------------------------------------------------------------

void __fastcall TRadarMapForm::FormCanResize(TObject *Sender, int &NewWidth, int &NewHeight,
		  bool &Resize)
{
	if(NewWidth<700) NewWidth=700;
	if(NewHeight<480) NewHeight=480;
}
//---------------------------------------------------------------------------

void __fastcall TRadarMapForm::Timer1Timer(TObject *Sender)
{
	int w;
	TRadarPanelsOrientation rpo;

	if(WindowState!=wsMaximized)
	{
		Top=RadarMapManager->Settings->WindowTop;
		Left=RadarMapManager->Settings->WindowLeft;
	}
	else
	{
		Top=0;
		Left=0;
	}
	if(RadarMapManager->Settings)
	{
		if(RadarMapManager->Settings->PanelsOrientation==rpoAuto)
			rpo=DetectOrientation();
		else rpo=RadarMapManager->Settings->PanelsOrientation;
	}
	else rpo=rpoVertical;
	switch(rpo)
	{
		case rpoHorizontal:
			w=Height/2-RadarPanel->Height-15;
			if(w>200) w=200;
			w/=4;
			RadarPanel->Height+=w;
			MapPanel->Height=Height-RadarPanel->Height-15;
			break;
		default:
		case rpoVertical:
			w=Width/2-RadarPanel->Width-15;
			if(w>200) w=200;
			w/=4;
			RadarPanel->Width+=w;
			MapPanel->Width=Width-RadarPanel->Width-15;
			break;
	}
	//Hide();
	//SendToBack();
	if(w==0)
	{
		Timer1->Enabled=false;
		RadarDialog->RadarToolbar->Visible=true;
		RadarDialog->LabelsToolbar->Visible=true;
		MapForm->MapToolbar->Visible=true;
		MapForm->FileToolbar->Visible=true;

		SplashForm->FormStyle=fsNormal;
		//SplashForm->Hide();
		SplashForm->Close();
		delete SplashForm;
		SplashForm=NULL;

		if(RadarMapManager && RadarMapManager->Settings && RadarMapManager->Settings->ProfilePipesDetector)
		{
			RadarMapManager->Settings->ProfilePipesDetector=(DisclaimerForm->ShowModal()==mrOk);
		}

		//MapForm->ToolButton1Click(this);

		//RadarDialog->ObjectsContainer->Update();
		//Visible=true;
		//Splitter1->Align=alClient;
	}
}
//---------------------------------------------------------------------------

void __fastcall TRadarMapForm::FormActivate(TObject *Sender)
{
	//SendToBack();
}
//---------------------------------------------------------------------------

void __fastcall TRadarMapForm::FormClose(TObject *Sender, TCloseAction &Action)
{
	if(RadarDialog) RadarDialog->Close();
	if(MapForm) MapForm->Close();
}
//---------------------------------------------------------------------------

void __fastcall TRadarMapForm::FormCloseQuery(TObject *Sender, bool &CanClose)
{
	int mr, mr1;

	if(RadarMapManager->ResultsNotSaved)
	{
		mr=Application->MessageBox(L"Your current results are not saved. Are you sure you want to Exit?",
			(L"Exit from "+(UnicodeString)_RadarMapName).w_str(), MB_YESNO | MB_ICONQUESTION);
		if(mr==ID_NO) return;
	}
	else mr=Application->MessageBox(L"Are you sure you want to Exit?",
		(L"Exit from "+(UnicodeString)_RadarMapName).w_str(), MB_YESNO);
	if(mr==IDYES)
	{
		if(RadarMapManager->GlobalStopItEvent)
			SetEvent(RadarMapManager->GlobalStopItEvent);
		WaitOthers();
		CanClose=true;
		//Application->Terminate();
	}
	else CanClose=false;
}
//---------------------------------------------------------------------------

