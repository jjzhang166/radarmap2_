//---------------------------------------------------------------------------

#ifndef VisualToolObjectsH
#define VisualToolObjectsH

#include <GR32.hpp>
#include "SliderTools.h"
#include "Icmp.h"
#include "Defines.h"

//---------------------------------------------------------------------------

enum TVisualToolObjectAlign {vtoaNone=0, vtoaLeft=1, vtoaTop=2, vtoaRight=3,
	vtoaBottom=4, vtoaLeftTop=5, vtoaLeftBottom=6, vtoaRightTop=7,
	vtoaRightBottom=8, vtoaCenter=9};

class TVisualToolObject: public TToolObject
{
protected:
	int width, height, BorderWidth;
	int dX, dY;
	TVisualToolObjectAlign toolalign;
	bool fixedposition, showclosebtn, showapplybtn, FitToImage, rebuilded; //FitToImage - only for tools where ToolAlign!=vtoaNone
	Types::TRect toolrect;
	bool *CloseExternalSetting;
	TVisualToolObjectAlign *ToolAlignExternalSetting, dTA;
	bool easyrendering, alignwithneighbors;
	TSliderToolButton *SmallOkBtn, *SmallCloseBtn, *ConnectedBtn;
	TGraphicObjectsList *SurroundingTools;
	void __fastcall RebuildInternalRects(int Width, int Height, bool RebuildNeighbors=true);
	virtual void __fastcall writeWidth(int value) {width=value; RebuildInternalRects(); Update();}
	virtual void __fastcall writeHeight(int value) {height=value; RebuildInternalRects(); Update();}
	void __fastcall writeToolAlign(TVisualToolObjectAlign value) {toolalign=value; RebuildInternalRects(); Update();}
	void __fastcall writeShowCloseBtn(bool value) {showclosebtn=value; if(SmallCloseBtn) SmallCloseBtn->Visible=(visible==true ? showclosebtn : false); Update();}
	void __fastcall writeShowApplyBtn(bool value) {showapplybtn=value; if(SmallOkBtn) SmallOkBtn->Visible=(visible==true ? showapplybtn : false); Update();}
	void __fastcall writeEasyRendering(bool value);
	void __fastcall writeAlignWithNeighbors(bool value) {if(alignwithneighbors!=value) {alignwithneighbors=value; RebuildInternalRects(); Update();}}
	//void __fastcall writeVisible(bool value) {if(visible!=value) {if(SmallOkBtn) SmallOkBtn->Visible=(value==true ? showapplybtn : false); if(SmallCloseBtn) SmallCloseBtn->Visible=(value==true ? showclosebtn : false);} TToolObject::writeVisible(value);}
	void __fastcall writeVisible(bool value);
	virtual void __fastcall OKPressed(System::TObject* Sender, int X, int Y) {Visible=false; if(ConnectedBtn) ConnectedBtn->Down=false;}
	virtual void __fastcall ClosePressed(System::TObject* Sender, int X, int Y) {Visible=false; try {if(CloseExternalSetting) *CloseExternalSetting=Visible; } catch(Exception &e) {} if(ConnectedBtn) ConnectedBtn->Down=false;}

	//void __fastcall writeVisible(bool value) {if(visible!=value) {SmallCloseBtn->Visible=value; TToolObject::writeVisible(value);}}
	virtual void __fastcall SmallResize();
public:

	__fastcall TVisualToolObject(TMyObject *AOwner, TImgView32 *ImgView, TObject *AManager,
		TSliderToolButton *AConnectedBtn=NULL);
	__fastcall ~TVisualToolObject() {if(SmallOkBtn) delete SmallOkBtn; SmallOkBtn=NULL; if(SmallCloseBtn) delete SmallCloseBtn; SmallCloseBtn=NULL;}

	virtual void __fastcall MouseClick(System::TObject* Sender, int X, int Y) {}
	virtual void __fastcall Redraw(TObject *Sender, TBitmap32 *Bmp);
	virtual void __fastcall OnResize(TObject *Sender, int NewWidth, int NewHeight) {RebuildInternalRects(NewWidth, NewHeight);}
	virtual void __fastcall MouseMove(System::TObject* Sender,
		  Classes::TShiftState Shift, int X, int Y, TCustomLayer *Layer);
	virtual void __fastcall MouseUp(TObject *Sender,
		  TMouseButton Button, TShiftState Shift, int X, int Y,
		  TCustomLayer *Layer) {if(pressed && (toolalign==vtoaNone || toolalign!=dTA)) Update();}
	virtual void __fastcall MouseDown(TObject *Sender,
		  TMouseButton Button, TShiftState Shift, int X, int Y,
		  TCustomLayer *Layer) {dX=X1-ToolRect.left; dY=Y1-ToolRect.top; dTA=toolalign;}
	void __fastcall RebuildInternalRects(bool RebuildNeighbors=true);

	Types::TRect __fastcall readViewPort() {return ToolRect;}

	int __fastcall AddObject(TMyObject* o) {if(Owner) return Owner->AddObject(o); else return -1;} //((TGraphicObject*)o)->Owner=this;

	__property int Width = {read=width, write=writeWidth};
	__property int Height = {read=height, write=writeHeight};
	__property TVisualToolObjectAlign ToolAlign = {read=toolalign, write=writeToolAlign};
	__property bool FixedPoistion = {read=fixedposition, write=fixedposition};
	__property bool ShowCloseBtn = {read=showclosebtn, write=writeShowCloseBtn};
	__property bool ShowApplyBtn = {read=showapplybtn, write=writeShowApplyBtn};
	__property bool EasyRendering = {read=easyrendering, write=writeEasyRendering};
	__property bool AlignWithNeighbors = {read=alignwithneighbors, write=writeAlignWithNeighbors};
	__property Types::TRect ToolRect = {read=toolrect};
	__property bool Rebuilded = {read=rebuilded};
};

class TRecycleBin: public TVisualToolObject
{
private:
	TBitmap32 *Glyph, *NormalGlyph, *HotGlyph, *InUseGlyph;
protected:
	void __fastcall SmallResize();
	void __fastcall writeInRect(bool value);
public:
	__fastcall TRecycleBin(TMyObject *AOwner, TImgView32 *ImgView, TObject *AManager);
	__fastcall ~TRecycleBin();

	void __fastcall DockDrop(System::TObject* Sender, System::TObject* Fallen, int X, int Y);
	void __fastcall Redraw(TObject *Sender, TBitmap32 *Bmp);
};

class TGyroCompassObject: public TVisualToolObject
{
private:
	double pitch;
	double roll;
	double heading;
	bool automaticupdate, showglass;
	TBitmap32 *BackgroundBmp, *CaseBmp, *GradsBmp, *CompassArrowBmp, *GlassBmp, *TempBmp;
	double __fastcall AddValue(double value, double* array, int &currentindex);
protected:
	void __fastcall writeAutomaticUpdate(bool value) {automaticupdate=value; if(value) Update();}
	void __fastcall writePitch(double value) {double d=pitch; pitch=value; if(automaticupdate && d!=value) Update();}
	void __fastcall writeRoll(double value) {double d=roll; roll=value; if(automaticupdate && d!=value) Update();}
	void __fastcall writeHeading(double value) {double d=heading; heading=value; if(automaticupdate && d!=value) Update();}
	void __fastcall writeShowGlass(bool value) {showglass=value; Update();}
	void __fastcall SmallResize();
public:
	__fastcall TGyroCompassObject(TMyObject *AOwner, TImgView32 *ImgView, TObject *AManager);
	__fastcall ~TGyroCompassObject();

	void __fastcall Redraw(TObject *Sender, TBitmap32 *Bmp);
	void Clear() {pitch=roll=heading=0.0;}

	__property double Pitch = {read=pitch, write=writePitch}; //Angle of Attack, in rad
	__property double Roll = {read=roll, write=writeRoll}; //Tangage, in rad
	__property double Heading = {read=heading, write=writeHeading}; //Yaw, in rad to North
	__property bool AutomaticUpdate = {read=automaticupdate, write=writeAutomaticUpdate};
	__property bool ShowGlass = {read=showglass, write=writeShowGlass};
};

class TMapScale: public TVisualToolObject
{
private:
	TGPSCoordinatesRect *LatLonRect;
	AnsiString zoomtext;
	TCoordinateSystem *CS;
protected:
	void __fastcall writeZoomText(AnsiString value) {if(zoomtext!=value) {zoomtext=value; Update();}}
public:
	__fastcall TMapScale(TMyObject *AOwner, TImgView32 *ImgView, TGPSCoordinatesRect *GCR, TCoordinateSystem *aCS, TObject *AManager);
	__fastcall ~TMapScale() {}

	void __fastcall Redraw(TObject *Sender, TBitmap32 *Bmp);

	__property AnsiString ZoomText = {write=writeZoomText};
	__property TCoordinateSystem *CoordinateSystem = {read=CS, write=CS};
};

class TWindRose: public TVisualToolObject
{
private:
	TBitmap32 *WindRoseBmp;
	bool ForceSmallSize;
protected:
	void __fastcall SmallResize();
public:
	__fastcall TWindRose(TMyObject *AOwner, TImgView32 *ImgView, TObject *AManager);
	__fastcall ~TWindRose();

	void __fastcall Redraw(TObject *Sender, TBitmap32 *Bmp);
};

class TToolClock: public TVisualToolObject
{
private:
	bool ForceSmallSize;
	int border;
	TDateTime time;
	TCoordinateSystem CS;
	TTimerThread *Timer;
	TColor32 color;
	void __fastcall Refresh();
protected:
	void __fastcall SmallResize();
public:
	__fastcall TToolClock(TMyObject *AOwner, TImgView32 *ImgView, TObject *AManager);
	__fastcall ~TToolClock();

	void __fastcall Redraw(TObject *Sender, TBitmap32 *Bmp);
};

const int HyperbolaBrenchLengthMax = 300;
const int HyperbolaBrenchLengthMin = 75;
const int HyperbolaBrenchBorder = 30;

class THyperbolaTool: public TVisualToolObject
{
private:
	float permitivity;
	int HyperbolaBrenchLength;
	bool Moved;
	TObject *Message;
	Types::TPoint headpoint, branchpoint;
	TFixedPoint *Points;
	void __fastcall RecalculHyperbola(bool recalculPermit=true);
protected:
	void __fastcall OKPressed(System::TObject* Sender, int X, int Y);
	void __fastcall writePermitivity(float value) {if(value>=1. && value<=100) {permitivity=value; RecalculHyperbola(false); Update();}} //&& permitivity!=value
	void __fastcall writeHeadPoint(Types::TPoint value) {if(headpoint!=value) headpoint=value;}
	void __fastcall writeBranchPoint(Types::TPoint value) {if(branchpoint!=value) branchpoint=value;}
public:
	__fastcall THyperbolaTool(TMyObject *AOwner, TImgView32 *ImgView, TObject *AManager, TSliderToolButton *AConnectedBtn);
	__fastcall ~THyperbolaTool();

	void __fastcall MouseClick(System::TObject* Sender, int X, int Y);
	void __fastcall MouseMove(System::TObject* Sender,
		  Classes::TShiftState Shift, int X, int Y, TCustomLayer *Layer);
	void __fastcall MouseDown(TObject *Sender,
		  TMouseButton Button, TShiftState Shift, int X, int Y,
		  TCustomLayer *Layer) {dX=X1-HeadPoint.x; dY=Y1-HeadPoint.y; Moved=false;}
	void __fastcall MouseUp(TObject *Sender,
		  TMouseButton Button, TShiftState Shift, int X, int Y,
		  TCustomLayer *Layer) {Moved=false; TVisualToolObject::MouseUp(Sender, Button, Shift, X, Y, Layer); }

	void __fastcall Redraw(TObject *Sender, TBitmap32 *Bmp);

	__property float Permitivity = {read=permitivity, write=writePermitivity};
	__property Types::TPoint HeadPoint = {read=headpoint, write=writeHeadPoint};
	__property Types::TPoint BranchPoint = {read=branchpoint, write=writeBranchPoint};
};

class TZeroPointTool: public TVisualToolObject
{
private:
	float zeropoint;
	TObject *Message;
	Types::TPoint Point;
	TFixedPoint *Points;
	void __fastcall RecalculZeroPoint(bool Recalcul=true);
protected:
	virtual void __fastcall OKPressed(System::TObject* Sender, int X, int Y);
	void __fastcall writeZeroPoint(float value) {zeropoint=value; RecalculZeroPoint(false); Update();}
public:
	__fastcall TZeroPointTool(TMyObject *AOwner, TImgView32 *ImgView, TObject *AManager, TSliderToolButton *AConnectedBtn);
	__fastcall ~TZeroPointTool() {delete[] Points;}

	void __fastcall MouseClick(System::TObject* Sender, int X, int Y);
	void __fastcall MouseMove(System::TObject* Sender,
		  Classes::TShiftState Shift, int X, int Y, TCustomLayer *Layer);

	void __fastcall Redraw(TObject *Sender, TBitmap32 *Bmp);

	__property float ZeroPoint = {read=zeropoint, write=writeZeroPoint}; //from 0..1
};

class TToolPing: public TVisualToolObject
{
private:
	bool ForceSmallSize;
	TObject *Message;
	int border;
	AnsiString PingStr, LastRequestResult;
	Icmp::TPing *ping;
	TColor32 color, text_color;
	void __fastcall Refresh();
protected:
	void __fastcall SmallResize();
public:
	__fastcall TToolPing(TMyObject *AOwner, TImgView32 *ImgView, TObject *AManager);
	__fastcall ~TToolPing();

	void __fastcall ChangeIPAddress();
	void __fastcall MouseClick(System::TObject* Sender, int X, int Y);
	void __fastcall Redraw(TObject *Sender, TBitmap32 *Bmp);
};

class TToolMemGraph: public TVisualToolObject
{
private:
	bool ForceSmallSize;
	int border, timepoints;
	float averagepointscoef;
	unsigned long *MemPoints, MaxMemPoint, LastMaxMemPoint, CurrentMem;
	long *AveragePoints, MaxAverage, LastMaxAverage; //in MB
	int MemPointsIndex;
	float Percentage;
	TColor32 text_color;
	double AverageMem;
	TTimerThread *Timer;
	DWORD* __fastcall ChangeArraySize(DWORD* Array, int newN);
	void __fastcall Refresh();
protected:
	void __fastcall SmallResize();
	void __fastcall writeTimePoints(int value);
	int __fastcall readAveragePoints() {int a=(int)((float)timepoints*averagepointscoef); if(a==0 || a>timepoints) a=timepoints; return a;}

	__property int TimePoints = {read=timepoints, write=writeTimePoints};
	__property int AveragePointsN = {read=readAveragePoints};
public:
	__fastcall TToolMemGraph(TMyObject *AOwner, TImgView32 *ImgView, TObject *AManager);
	__fastcall ~TToolMemGraph();

	void __fastcall Redraw(TObject *Sender, TBitmap32 *Bmp);
};

enum TToolProgressBarStyle {tpbsWinStyle=0, tpbsWindMill=1};//, tpbsFlatProgress=2};

class TToolProgressBar: public TVisualToolObject
{
private:
	TBitmapLayer *Layer;
	TBitmap32* windmillbmp;
	float cpm;
	AnsiString CancelStr;
	unsigned long LastDrawedTick;
	unsigned int min, max, users, cancelcnt, lockcnt;
	int min_size, pos, step;
	TToolProgressBarStyle style;
	TColor32 color, textcolor;
	void __fastcall LayerPaint(TObject *Sender, TBitmap32 *Bmp) {if(visible) Redraw(Sender, Bmp);}
	void __fastcall GetSizedGlyph();
protected:
	bool __fastcall readCancelPressed();
	void __fastcall writeWidth(int value);
	void __fastcall writeHeight(int value);
	void __fastcall writeMin(unsigned int value) {if(value<=max) min=value; Update();}
	void __fastcall writeMax(unsigned int value) {if(value>=min) max=value; Update();}
	void __fastcall writeStep(int value) {if(value<(int)(max-min)) step=value;}
	void __fastcall writePos(int value) {if(value>=(int)min && value<=(int)max) {pos=value; Update();}}
	void __fastcall writeColor(TColor32 value);
public:
	__fastcall TToolProgressBar(TMyObject *AOwner, TImgView32 *ImgView, TObject *AManager);
	__fastcall ~TToolProgressBar();

	void __fastcall Show(int StepsCount, bool EnableCancelButton);
	void __fastcall Hide(bool DisableCancelButton);
	void __fastcall Show() {Show(0, false);}
	void __fastcall Hide() {Hide(false);}
	void __fastcall StepIt() {pos+=step; if(pos>(int)max) pos-=(int)(max-min+1); Update();}
	void __fastcall StepBy(int aStep) {pos+=aStep; while(pos>(int)max) pos-=(int)(max-min+1); Update();}
	//void __fastcall Start() {} //only for endless mode...
	//void __fastcall Stop() {} //only for endless mode...
	void __fastcall Redraw(TObject *Sender, TBitmap32 *Bmp);
	void __fastcall MouseClick(System::TObject* Sender, int X, int Y);
	void __fastcall Update(TMyObjectType Type) {TVisualToolObject::Update(Type);}
	void __fastcall Update();

	__property unsigned int Min = {read=min, write=writeMin};
	__property unsigned int Max = {read=max, write=writeMax};
	__property int Step = {read=step, write=writeStep};
	__property int Position = {read=pos, write=writePos};
	//__property bool Endless = {read=endless, write=endless};
	//__property float CyclesPerMinute = {read=cpm, write=cpm}; //only for endless mode...
	__property TColor32 Color = {read=color, write=writeColor};
    __property TColor32 TextColor = {read=textcolor, write=textcolor};
	__property bool Cancelable = {read=mouseable};
	__property bool CancelPressed = {read=readCancelPressed};
};

#endif
