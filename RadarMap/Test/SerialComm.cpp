//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "SerialComm.h"
#include "Manager.h"
#pragma package(smart_init)
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
// TCustomSerialClient
//---------------------------------------------------------------------------
void __fastcall TCustomSerialClient::ShowConnectionError(AnsiString msg)
{
	if(Manager)
	{
		try
		{
			if(!ErrMsg || (ErrMsg && (ErrMsg->SwitchedOff && ErrMsg->Text==NULL || ErrMsg->Text!=msg)))
				ErrMsg=((TRadarMapManager*)Manager)->ShowMessageObject(ErrMsg, ((TRadarMapManager*)Manager)->MapMessages,
					TMessageType::mtStop, mvtDisapering, 5000, msg, "Error"); //mvtFlashing
		}
		catch (Exception &e) {ErrMsg=NULL;}
	}
}

//---------------------------------------------------------------------------
// TCustomSerialThread
//---------------------------------------------------------------------------
bool __fastcall TCustomSerialThread::CheckAttemptsQuantity(int i)
{
	if(i<MaxSerialReadWriteAttempts) return true;
	else if(Owner)
	{
		Owner->ShowConnectionError("The "+Owner->Name+" is not responding! Please check its settings or reboot it.");
		//Stop();
		return false;
	}
	else return false;
}
//---------------------------------------------------------------------------
