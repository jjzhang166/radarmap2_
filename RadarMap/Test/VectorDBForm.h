//---------------------------------------------------------------------------

#ifndef VectorDBFormH
#define VectorDBFormH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <ImageButton.h>
#include <pngimage.hpp>
#include <GR32_Image.hpp>
#include <Graphics.hpp>
#include "Manager.h"
#include <Dialogs.hpp>
#include <Menus.hpp>
#include <ComCtrls.hpp>
#include <ImgList.hpp>
#include "ImageMovie.h"
#include "DXFtoDbLoader.h"
#ifdef _RAD101
	#include <System.ImageList.hpp>
#endif
//---------------------------------------------------------------------------

class TVectorDBForm : public TForm
{
__published:	// IDE-managed Components
	TPanel *Panel1;
	TPanel *Panel2;
	TPanel *Panel3;
	TPanel *Panel4;
	TPanel *TitlePanel;
	TPanel *Panel5;
	TPanel *Panel6;
	TPanel *Panel7;
	TPanel *TabsPanel;
	TPanel *PagesPanel;
	TPanel *OkCancelPanel;
	TLabel *Label1;
	TPanel *Panel8;
	TPanel *Panel9;
	TImageButton *CloseIButton;
	TImageButton *CancelImageButton;
	TImageButton *ContentsIButton;
	TImageButton *AddIButton;
	TImageButton *DeleteIButton;
	TImageButton *UpdateIButton;
	TImageButton *PreviewIButton;
	TEdit *TempEdit;
	TOpenDialog *OpenDialog;
	TPopupMenu *CSPopupMenu;
	TNotebook *Notebook1;
	TPanel *Panel11;
	TLabel *Label2;
	TLabel *csLabel;
	TLabel *Label13;
	TImageButton *BrowseIButton;
	TListView *FilesLView;
	TPanel *Panel17;
	TLabel *Label14;
	TLabel *FileLabel;
	TImageButton *NewIButton;
	TImageList *ImageList1;
	TSaveDialog *SaveDialog;
	TPanel *Panel10;
	TImgView32 *MapImage;
	TPanel *MoviePanel;
	TLabel *WaitLabel;
	TImageButton *CloseMovieButton;
	TImageButton *StopButton;
	TImage32 *Image321;
	TPanel *Panel14;
	TPanel *Panel15;
	TPanel *Panel16;
	TPanel *Panel18;
	TImageButton *OkImageButton;
	void __fastcall CloseIButtonClick(TObject *Sender);
	void __fastcall FormShow(TObject *Sender);
	void __fastcall OkImageButtonClick(TObject *Sender);
	void __fastcall FormHide(TObject *Sender);
	void __fastcall TitlePanelMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
	void __fastcall TitlePanelMouseMove(TObject *Sender, TShiftState Shift, int X, int Y);
	void __fastcall TitlePanelMouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift,
		  int X, int Y);
	void __fastcall csLabelClick(TObject *Sender);
	void __fastcall BrowseIButtonClick(TObject *Sender);
	void __fastcall ContentsIButtonClick(TObject *Sender);
	void __fastcall PreviewIButtonClick(TObject *Sender);
	void __fastcall NewIButtonClick(TObject *Sender);
	void __fastcall AddIButtonClick(TObject *Sender);
	void __fastcall FilesLViewClick(TObject *Sender);
	void __fastcall DeleteIButtonClick(TObject *Sender);
	void __fastcall UpdateIButtonClick(TObject *Sender);
	void __fastcall StopButtonClick(TObject *Sender);

private:	// User declarations
	AnsiString filename;
	TRadarMapManager *manager;
	bool TitleDown;
	Types::TPoint TitleXY;
	TStrings *CRS_GUIDs;
	TMapObjectsContainer *mapobjects;
	TDbDXFMapsContainer* dbmc;
	TMouseAction MA;
	TResourceMovie *ResMov;
	TSliderToolbar *MapToolbar;
	TSliderToolButton *LayersBtn, *ZoomInBtn, *ZoomOutBtn;
	TCoordinateSystem coordinatesystem;
	bool DblClick;
	void __fastcall ZoomInMouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y);
	void __fastcall ZoomOutMouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y);
	void __fastcall LayersClick(System::TObject* Sender, int X, int Y);
	void __fastcall ZoomInDblClick(System::TObject* Sender, int X, int Y);
	void __fastcall ZoomOutDblClick(System::TObject* Sender, int X, int Y);

	void __fastcall DisableMouseAction();
	void __fastcall DisableButtons();
	void __fastcall UpdateFileListView(AnsiString aFileName);
	bool __fastcall OpenVectorDB(AnsiString aFileName);
	void __fastcall writeFileName(AnsiString value);
	void __fastcall writeCoordinateSystem(TCoordinateSystem value);
	void __fastcall CSMenuItemClick(TObject *Sender);
	void __fastcall StopMovie();
	void __fastcall SuccessStopMovieAndReopen() {if(!OpenVectorDB(FileName)) FileName=""; StopMovie();}
	void __fastcall SuccessStopMovie() {UpdateFileListView(FileName); StopMovie();}
	void __fastcall CancelMovie();
	void __fastcall CreateMapObjects();
	void __fastcall StartMovie(bool WaitOrStop, TResourceMovieType rmt);
public:		// User declarations
	__fastcall TVectorDBForm(TComponent* Owner, TRadarMapManager *aManager,
		TCoordinateSystem aCS);
	__fastcall ~TVectorDBForm();

	__property AnsiString FileName = {read=filename, write=writeFileName};
	__property TCoordinateSystem CoordinateSystem = {read=coordinatesystem, write=writeCoordinateSystem};
	__property TRadarMapManager *Manager = {read=manager, write=manager};
};
//---------------------------------------------------------------------------
extern PACKAGE TVectorDBForm *VectorDBForm;
//---------------------------------------------------------------------------
#endif
