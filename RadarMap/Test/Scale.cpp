//---------------------------------------------------------------------------
#include <vcl.h>
#include <math.h>
#include <stdlib.h>
#pragma hdrstop

#include "Defines.h"
#include "Scale.h"

TScale::TScale(void)
{
    Rect.Left=0;
    Rect.Top=0;
    Rect.Right=0;
    Rect.Bottom=0;
    DC=NULL;
    Bmp=NULL;
    Color=clBlack;
    BG_Color=clBtnFace;
    Caption="";
    HotSide=hsTop;
    Min=0;
    Max=100;
    Step=1;
	Shift1=1;
    Shift2=1;
    dd=1.0;
    IntegerValues=false;
//    Adjusted=false;
    Font=new TFont;
}
//---------------------------------------------------------------------------

TScale::~TScale(void)
{
    try { delete Font; }
    catch (Exception &exception) { ; }
}
//---------------------------------------------------------------------------

void TScale::Adjust(void)
{
	float m1, m2;

	if(HotSide==hsLeft || HotSide==hsRight)
	{
		Width=Rect.Bottom-Rect.Top+1;
		Height=Rect.Right-Rect.Left+1;
	}
	else
	{
		Width=Rect.Right-Rect.Left+1;
		Height=Rect.Bottom-Rect.Top+1;
	}
	//if(Width<150) Step=2;
	//else Step=1;

	PrimLineWidth=1;
	SecLineWidth=1;
	PrimLineLenght=Height/11;
	if(PrimLineLenght<1) PrimLineLenght=1;
	SecLineLenght=PrimLineLenght;
	if(SecLineLenght>PrimLineLenght) SecLineLenght=PrimLineLenght;

	//Font->Name="Verdana"; // !!
	Font->Name="Tahoma"; // Best !!!
	//Font->Name="Arial";
	//Font->Name="Courier"; // XXX

//-- Space between strings
	if(Caption=="") Font->Height=(Height-PrimLineLenght);
	else Font->Height=(Height-PrimLineLenght)/2;

	if(Font->Height==0) Font->Height=1;
	if((float)Width/(float)Font->Height<10) Font->Height=Width/10;

//-- Shifts for strings
	Shift1=PrimLineLenght;
	Shift2=Shift1+Font->Height;
	if(HotSide==hsBottom)
	{
		Shift1+=Font->Height;
		Shift2+=Font->Height;
	}
	else if(HotSide==hsRight)
	{
		Shift1+=Font->Height;
		Shift2+=Font->Height;
	}

	Font->Color=Color;
//--- Rotate font ---
	GetObject(Font->Handle, sizeof(LOGFONT), &logFont);
	if(HotSide==hsTop || HotSide==hsBottom) logFont.lfEscapement=0; // at 0 degree
	else logFont.lfEscapement=900; // at 90 degree
	logFont.lfOrientation=logFont.lfEscapement;
//logFont.lfWeight=3000;
	if(DC!=NULL)
	{
		DC->Font->Handle=CreateFontIndirect(&logFont);
		m1=DC->TextWidth(ValueToStr(Max, false));
		m2=DC->TextWidth(ValueToStr(Min, false));
	}
	else if(Bmp!=NULL)
	{
		Bmp->Font->Handle=CreateFontIndirect(&logFont);
		m1=Bmp->TextWidth(ValueToStr(Max, false));
		m2=Bmp->TextWidth(ValueToStr(Min, false));
	}
	if(m1<m2) m1=m2;
	Step=1;
	if(Width>m1)
	{
		m1*=1.5;
		m1*=10;
		while(m1>Width)
		{
			Step++;
			m1/=(float)Step;
		}
	}/**/
}
//---------------------------------------------------------------------------

void TScale::Draw(void)
{
	try
	{
		if(DC!=NULL) DrawOnCanvas();
		else if(Bmp!=NULL) DrawOnBitmap32();
	}
	catch (Exception &exception) { ; }
}

void TScale::DrawOnCanvas(void)
{
    int w,k,j,num;
	TTRect rect;

    DC->Brush->Color=BG_Color;
    DC->Pen->Color=BG_Color;
    if(HotSide==hsTop || HotSide==hsBottom)
		DC->Rectangle(Rect.Left-1,Rect.Top,Rect.Right+1,Rect.Bottom);
	else DC->Rectangle(Rect.Left,Rect.Top-1,Rect.Right,Rect.Bottom+1);
	try
	{
		DC->Lock();
		DC->Pen->Color=Color;
		DC->Font->Color=Color;
		DC->Unlock();
	}
	catch (Exception &exception) {}
	Adjust();
	if(Caption=="" && Height<11) return;
	if(Caption!="" && Height<20) return;

	num=Calculate();
	DC->Brush->Style=bsClear;
//------------
    if(HotSide==hsTop)
    {
		DC->Pen->Width=PrimLineWidth;
		for(k=0; k<num; k++)
		{
			if(Max>Min) j=Rect.Left+Offset[k];
			else j=Rect.Right-Offset[k];
			Str=ValueToStr(Value[k]);
			DC->MoveTo(j,Rect.Top);
			DC->LineTo(j,Rect.Top+PrimLineLenght);
			w=DC->TextWidth(Str)/2;
			if(j-w<Rect.Left) j=Rect.Left+w;
			if(j+w>Rect.Right) j=Rect.Right-w;
			DC->TextOut(j-w,Rect.Top+Shift1,Str);
		}
		DC->TextOut(Rect.Left+Width/2-DC->TextWidth(Caption)/2, Rect.Top+Shift2,Caption);
	}
//------------
	else if(HotSide==hsBottom)
	{
		DC->Pen->Width=PrimLineWidth;
		for(k=0; k<num; k++)
		{
			if(Max>Min) j=Rect.Left+Offset[k];
			else j=Rect.Right-Offset[k];
			DC->MoveTo(j,Rect.Bottom-1);
			DC->LineTo(j,Rect.Bottom-1-PrimLineLenght);
			Str=ValueToStr(Value[k]);
			w=DC->TextWidth(Str)/2;
			if(j-w<Rect.Left) j=Rect.Left+w;
			if(j+w>Rect.Right) j=Rect.Right-w;
			DC->TextOut(j-w,Rect.Bottom-Shift1,Str);
		}
		DC->TextOut(Rect.Left+Width/2-DC->TextWidth(Caption)/2,Rect.Bottom-Shift2,Caption);
	}
//------------
	else if(HotSide==hsLeft)
    {
		DC->Pen->Width=PrimLineWidth;
		for(k=0; k<num; k++)
		{
			if(Max>Min) j=Rect.Bottom-Offset[k];
			else j=Rect.Top+Offset[k];
			DC->MoveTo(Rect.Left+1,j);
			DC->LineTo(Rect.Left+1+PrimLineLenght,j);
			Str=ValueToStr(Value[k]);
			w=DC->TextWidth(Str)/2;
			if(j+w>Rect.Bottom) j=Rect.Bottom-w;
			if(j-w<Rect.Top) j=Rect.Top+w;
			DC->TextOut(Rect.Left+Shift1,j+w,Str);
		}
		DC->TextOut(Rect.Left+Shift2,Rect.Top+Width/2+DC->TextWidth(Caption)/2,Caption);
    }
//------------
	else if(HotSide==hsRight)
    {
		DC->Pen->Width=PrimLineWidth;
		for(k=0; k<num; k++)
		{
			if(Max>Min) j=Rect.Bottom-Offset[k];
			else j=Rect.Top+Offset[k];
			DC->MoveTo(Rect.Right-1,j);
			DC->LineTo(Rect.Right-1-PrimLineLenght,j);
			Str=ValueToStr(Value[k]);
			w=DC->TextWidth(Str)/2;
			if(j+w>Rect.Bottom) j=Rect.Bottom-w;
			if(j-w<Rect.Top) j=Rect.Top+w;
			DC->TextOut(Rect.Right-Shift1,j+w,Str);
		}
		DC->TextOut(Rect.Right-Shift2,Rect.Top+Width/2+DC->TextWidth(Caption)/2,Caption);
    }
//------------
    DC->Brush->Style=bsSolid;
}
//---------------------------------------------------------------------------

void TScale::DrawOnBitmap32(void)
{
    int w,k,j,num;
    TTRect rect;

	if(HotSide==hsTop || HotSide==hsBottom)
		Bmp->FillRect(Rect.Left-1,Rect.Top,Rect.Right+1,Rect.Bottom, BG_Color);
	else Bmp->FillRect(Rect.Left,Rect.Top-1,Rect.Right,Rect.Bottom+1, BG_Color);
	Bmp->PenColor=((Color & 0xFF0000)>>16 | (Color & 0x00FF00) | (Color & 0x0000FF)<<16);
	Bmp->Font->Color=Color;
	Adjust();
    if(Caption=="" && Height<11) return;
    if(Caption!="" && Height<20) return;

	num=Calculate();

//------------
	if(HotSide==hsTop)
	{
		for(k=0; k<num; k++)
		{
			if(Max>Min) j=Rect.Left+Offset[k];
			else j=Rect.Right-Offset[k];
			Str=ValueToStr(Value[k]);
			Bmp->MoveTo(j,Rect.Top);
			Bmp->LineToS(j,Rect.Top+PrimLineLenght);
			w=Bmp->TextWidth(Str)/2;
			if(j-w<Rect.Left) j=Rect.Left+w;
			if(j+w>Rect.Right) j=Rect.Right-w;
			Bmp->Textout(j-w,Rect.Top+Shift1,Str);
		}
		Bmp->Textout(Rect.Left+Width/2-Bmp->TextWidth(Caption)/2, Rect.Top+Shift2,Caption);
	}
//------------
	if(HotSide==hsBottom)
	{
		for(k=0; k<num; k++)
		{
			if(Max>Min) j=Rect.Left+Offset[k];
			else j=Rect.Right-Offset[k];
			Bmp->MoveTo(j,Rect.Bottom-1);
			Bmp->LineToS(j,Rect.Bottom-1-PrimLineLenght);
			Str=ValueToStr(Value[k]);
			w=Bmp->TextWidth(Str)/2;
			if(j-w<Rect.Left) j=Rect.Left+w;
			if(j+w>Rect.Right) j=Rect.Right-w;
			Bmp->Textout(j-w,Rect.Bottom-Shift1,Str);
		}
		Bmp->Textout(Rect.Left+Width/2-Bmp->TextWidth(Caption)/2,Rect.Bottom-Shift2,Caption);
    }
//------------
    if(HotSide==hsLeft)
    {
		for(k=0; k<num; k++)
		{
			if(Max>Min) j=Rect.Bottom-Offset[k];
			else j=Rect.Top+Offset[k];
			Bmp->MoveTo(Rect.Left+1,j);
			Bmp->LineToS(Rect.Left+1+PrimLineLenght,j);
			Str=ValueToStr(Value[k]);
			w=Bmp->TextWidth(Str)/2;
			if(j+w>Rect.Bottom) j=Rect.Bottom-w;
			if(j-w<Rect.Top) j=Rect.Top+w;
			Bmp->Textout(Rect.Left+Shift1,j+w,Str);
		}
		Bmp->Textout(Rect.Left+Shift2,Rect.Top+Width/2+Bmp->TextWidth(Caption)/2,Caption);
    }
//------------
    if(HotSide==hsRight)
    {
		for(k=0; k<num; k++)
		{
			if(Max>Min) j=Rect.Bottom-Offset[k];
			else j=Rect.Top+Offset[k];
			Bmp->MoveTo(Rect.Right-1,j);
			Bmp->LineToS(Rect.Right-1-PrimLineLenght,j);
			Str=ValueToStr(Value[k]);
			w=Bmp->TextWidth(Str)/2;
			if(j+w>Rect.Bottom) j=Rect.Bottom-w;
			if(j-w<Rect.Top) j=Rect.Top+w;
			Bmp->Textout(Rect.Right-Shift1,j+w,Str);
		}
		Bmp->Textout(Rect.Right-Shift2,Rect.Top+Width/2+Bmp->TextWidth(Caption)/2,Caption);
	}
}
//---------------------------------------------------------------------------

int TScale::Calculate(void)
{
	double begin;
	float min, max;
	float d, d1, por, m1, m2, mm;
	float a;
	float val;
    float values[MaxValues];
	int n, nn, por_i, k, j, no_zer=1;
//    float y,yy;
    char str[8];
    bool StopStt;

	min=Min;
	max=Max;

	if(max==min) return 0;
	if(max<min)
	{
		a=max;
		max=min;
		min=a;
	}
	d=max-min;
	por=log(d)/log(10.);//Ln;
	por_i=(int)por;
	if(por_i<0) por_i--;
	d1=d/pow((float)10.,(float)por_i);

	//if(d<100 && !IntegerValues)
    if(!IntegerValues)
    {
        if(d1<0.5) dd=0.05;
        else if(d1<1.0) dd=0.1;
        else if(d1<2.0) dd=0.25;
        else if(d1<5.0) dd=0.5;
        else dd=1.0;
    }
    else dd=1.0;

	a=pow((float)10.,(float)por_i);
	modf(min/a/dd,&begin);
	begin*=dd;

	n=nn=0;
	m1=0;
	if(DC!=NULL) m1=DC->TextWidth(ValueToStr(max, false));
	else if(Bmp!=NULL) m1=Bmp->TextWidth(ValueToStr(max, false));
    if(DC!=NULL) m2=DC->TextWidth(ValueToStr(min, false));
	else if(Bmp!=NULL) m2=Bmp->TextWidth(ValueToStr(min, false));
    if(m2>m1) m1=m2;
	if(d<100) m1*=1.25;//4.;
    else
    {
        mm=4.;
        while((float)Width/(m1*mm)<3 && m1>1.5) mm/=1.25;
        m1*=mm;
    }
    StopStt=false;
    while(a>0 && nn*2<MaxValues && (float)Width/(float)(nn+1)>m1 && !StopStt)
    {
        nn=0;
        for(k=0; k<MaxValues; k++)
        {
            val=(float)(begin+k*dd)*a;
            values[nn]=val;
            if((float)val>=min && (float)val<=max) nn++;
            else if(val>max) break;
        }
        if(nn>0 && (float)Width/(float)nn>=m1 || n==0)
        {
            for(k=0; k<MaxValues; k++) Value[k]=values[k];
            n=nn;
        }
        a/=2.;
        if(a>=1) a=(int)a;
        else if(IntegerValues) StopStt=true;
        modf(min/a/dd,&begin);
        begin*=dd;
    }
//-------------
//    if(neg<0)
//	 yy=0.2*(float)(Width-1)*((Value[1]-Value[0])/(max-min));
//    else
//	 yy=0.5*(float)(Width-1)*((Value[1]-Value[0])/(max-min));

    while(n/3<Step) Step--;
    if(Step<1) Step=1;
	if(Step>1 && min<0 && max>0)
	{
		for(k=0; k<n; k+=Step)
		{
			gcvt(Value[k],6,str);
			if(strncmp(str, "0", 1)==0) no_zer=0;
		}
		if(no_zer==1)
		{
			for(k=0; k<11; k++) Value[k]=Value[k+1];
			n--;
		}
	}
	for(k=0,j=0; k<n; k+=Step,j++)
		Offset[j]=(int)((float)(Width-1)*((Value[k]-min)/(max-min)));
	if(Step>1)
	{
		for(k=0,j=0; k<n; k+=Step,j++)
			Value[j]=Value[k];
	}
	else j=n;

	return(j);
}
//---------------------------------------------------------------------------

AnsiString TScale::ValueToStr(float f, bool TrimZeros)
{
	float d=fabs(Max-Min);
	int dn;
	AnsiString str;

	if(d<=0) return "0";
	dn=0;
	while(d<10.)
	{
		d*=10.;
		if(d<2.0) dn+=4;
		else dn+=2;
	}
    if(dd<0.075 || (dd>0.15 && dd<0.4)) dn+=2;
    else if((dd>0.075 && dd<0.15) || (dd>0.4 && dd<0.75)) dn++;
	try
	{
		if(TrimZeros)
		{
			str=FloatToStrF(f, ffFixed, dn+6, dn);
			if(strchr(str.c_str(), (char)DecimalSeparator))
			{
				dn=str.Length();
				while(dn>0 && str[dn]=='0') dn--;
				if(str[dn]==DecimalSeparator) dn--;
				str.SetLength(dn);
            }
		}
		else str=FloatToStrF(f, ffFixed, dn+6, dn);
	}
	catch(Exception &e) {str="0";}

	return str;
}

#pragma package(smart_init)

