//---------------------------------------------------------------------------

#ifndef LayersH
#define LayersH
//---------------------------------------------------------------------------
#include "CheckBoxTreeView.h"
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <ImageButton.h>
#include <pngimage.hpp>
//#include "G32_ProgressBar.hpp"
#include <GR32_Image.hpp>
#include <Graphics.hpp>
//#include "MyGraph.h"
#include <Dialogs.hpp>
#include <Menus.hpp>
#include <CheckLst.hpp>
#include <ComCtrls.hpp>
#include <ImgList.hpp>
#ifdef _RAD101
	#include <System.ImageList.hpp>
#endif
//---------------------------------------------------------------------------
class TLayersForm : public TForm
{
__published:	// IDE-managed Components
	TPanel *Panel1;
	TPanel *Panel2;
	TPanel *Panel3;
	TPanel *Panel4;
	TPanel *TitlePanel;
	TPanel *Panel5;
	TPanel *Panel6;
	TPanel *Panel7;
	TPanel *TabsPanel;
	TPanel *PagesPanel;
	TPanel *OkCancelPanel;
	TLabel *Label1;
	TPanel *Panel8;
	TPanel *Panel9;
	TImageButton *CloseIButton;
	TImageButton *CancelImageButton;
	TImageButton *OkImageButton;
	TImageButton *TopoButton;
	TImageButton *AnnButton;
	TEdit *TempEdit;
	TImageButton *UtilButton;
	TTreeView *TreeView1;
	TImageList *ImageList48;
	void __fastcall CloseIButtonClick(TObject *Sender);
	void __fastcall OkImageButtonClick(TObject *Sender);
	void __fastcall FormHide(TObject *Sender);
	void __fastcall TitlePanelMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
		  int X, int Y);
	void __fastcall TitlePanelMouseMove(TObject *Sender, TShiftState Shift, int X, int Y);
	void __fastcall TitlePanelMouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift,
		  int X, int Y);
	void __fastcall AnnButtonClick(TObject *Sender);
	void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
private:	// User declarations
	bool TitleDown, TVMousePressed;
	int TVY, TVp;
	TList *List;
	TCheckBoxTreeView *treeview;
	Types::TPoint TitleXY;
	void __fastcall TreeViewChange(TObject *Sender, TTreeNode *Node);
	void __fastcall TreeViewMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
		  int X, int Y);
	void __fastcall TreeViewMouseMove(TObject *Sender, TShiftState Shift, int X, int Y);
	void __fastcall TreeViewMouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift,
		  int X, int Y);
public:		// User declarations
	__fastcall TLayersForm(TComponent* Owner);
	__fastcall ~TLayersForm() {List->Clear(); delete List;}/**/

	__property TCheckBoxTreeView *TreeView = {read=treeview};
};
//---------------------------------------------------------------------------
extern PACKAGE TLayersForm *LayersForm;
//---------------------------------------------------------------------------
#endif
