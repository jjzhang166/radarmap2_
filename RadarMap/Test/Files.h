//---------------------------------------------------------------------------

#ifndef FilesH
#define FilesH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <CheckLst.hpp>
#include <ComCtrls.hpp>
#include <ImgList.hpp>
#include <ExtCtrls.hpp>
#include <pngimage.hpp>
#include <ImageButton.h>
//---------------------------------------------------------------------------
class TFilesForm : public TForm
{
__published:	// IDE-managed Components
	TTreeView *TreeView;
	TImageList *ImageList1;
	TPanel *Panel2;
	TImageButton *OkIButton;
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall TreeViewDblClick(TObject *Sender);
	void __fastcall Image2Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
	static void __fastcall EmptyNodeData(TTreeNode* node);
	__fastcall TFilesForm(TComponent* Owner);
	__fastcall ~TFilesForm();
};
//---------------------------------------------------------------------------
extern PACKAGE TFilesForm *FilesForm;
//---------------------------------------------------------------------------
#endif
