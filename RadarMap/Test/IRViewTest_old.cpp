//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
#include <tchar.h>

#include "Splash.h"

//---------------------------------------------------------------------------
USEFORM("Progress.cpp", ProgressForm);
USEFORM("..\Preview.cpp", PreviewForm);
USEFORM("Layers.cpp", LayersForm);
USEFORM("Map.cpp", MapForm);
USEFORM("AddCoordinatesDlg.cpp", AddCoordinatesForm);
USEFORM("Unit2.cpp", Form2);
USEFORM("Col_l_s.cpp", UserPaletteDialog);
USEFORM("Radar.cpp", RadarDialog);
USEFORM("..\Splash.cpp", SplashForm);
//---------------------------------------------------------------------------
WINAPI _tWinMain(HINSTANCE, HINSTANCE, LPTSTR, int)
{
	try
	{
        SplashForm=new TSplashForm(Application);
		SplashForm->Show();
		SplashForm->Update();

		Application->Initialize();
		Application->MainFormOnTaskBar = true;
		Application->CreateForm(__classid(TForm2), &Form2);
		Application->CreateForm(__classid(TUserPaletteDialog), &UserPaletteDialog);
		Application->CreateForm(__classid(TLayersForm), &LayersForm);
		Application->CreateForm(__classid(TProgressForm), &ProgressForm);
		Application->CreateForm(__classid(TPreviewForm), &PreviewForm);
		Application->Run();
	}
	catch (Exception &exception)
	{
		Application->ShowException(&exception);
	}
	catch (...)
	{
		try
		{
			throw Exception("");
		}
		catch (Exception &exception)
		{
			Application->ShowException(&exception);
		}
	}
	return 0;
}
//---------------------------------------------------------------------------
