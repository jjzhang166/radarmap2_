//---------------------------------------------------------------------------

#ifndef ImageMovieH
#define ImageMovieH
//---------------------------------------------------------------------------
#include <GR32_Image.hpp>
#include "MyThreads.h"

enum TPictureType {ptBitmap, ptPNG, ptJPEG};
enum TResourceMovieType {rmtNone = 0, rmtBookOpen = 1, rmtBookAdd = 2,
	rmtBookDelete = 3, rmtBookUpdate = 4, rmtSettings = 5, rmtCalibDist = 6,
	rmtCalibAngle = 7, rmtSatellite = 8, rmtGlobe = 9, rmtGlobeOnline = 10};

class TResourceMovie: public TObject
{
private:
	TImage32 *image;
	TStrings *frames;
	TTimerThread *timer;
	bool endless;
	TPictureType ptype;
	int frameindex;

	void __fastcall DrawFrame();
protected:
	AnsiString __fastcall readFrame(int Index) {if(frames && Index>=0 && Index<frames->Count) return frames->Strings[Index]; else return "";}
	int __fastcall readCount() {if(frames) return frames->Count; else return -1;}
	int __fastcall readInterval() {if(timer) return timer->Interval; else return -1;}
	void __fastcall writeInterval(int value) {if(timer) timer->Interval=value;}
public:
	__fastcall TResourceMovie();
	__fastcall ~TResourceMovie();

	void __fastcall Start() {frameindex=0; if(timer) timer->Start();}
	void __fastcall Start(TResourceMovieType rmt);
	void __fastcall Stop() {if(timer) timer->Stop();}
	void __fastcall Add(UnicodeString str) {if(frames && str!="" && str!=NULL) frames->Add(str);}
	void __fastcall Delete(int index) {if(frames) frames->Delete(index);}
	void __fastcall Clear() {if(frames) frames->Clear();}

	__property TImage32 *Image = {read=image, write=image};
	__property int Count = {read=readCount};
	__property UnicodeString Frames[int Index] = {read=readFrame};
	__property bool Endless = {read=endless, write=endless};
	__property TPictureType FramePictureType = {read=ptype, write=ptype};
	__property int FrameInterval = {read=readInterval, write=writeInterval};
};

#endif
