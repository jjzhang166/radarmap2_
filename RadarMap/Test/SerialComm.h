//---------------------------------------------------------------------------

#ifndef SerialCommH
#define SerialCommH
//---------------------------------------------------------------------------
#include <Classes.hpp>
//#include "Defines.h"
#include "MessageObjects.h"

class TCustomSerialClient : public TObject
{
private:
	TMessageObject* errmsg;
	AnsiString host;
	AnsiString name;
protected:
	HANDLE CommHandle;
	TObject *Manager;
	TEventedQueue *writebuf;
	TEventedQueue *readbuf;
	bool connected;
	virtual bool __fastcall readConnected() {return connected;}
public:
	__fastcall TCustomSerialClient(TObject* AManager) {Manager=AManager; errmsg=NULL; writebuf=new TEventedQueue(); readbuf=new TEventedQueue(); CommHandle=NULL; name="Serial Device";}
	__fastcall ~TCustomSerialClient() {try {errmsg=NULL; if(writebuf) delete writebuf; writebuf=NULL;	if(readbuf) delete readbuf; readbuf=NULL;	} catch(...) {}}
	virtual void __fastcall Connect() {CommHandle=CreateFile(host.c_str(), GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL); connected=(CommHandle!=INVALID_HANDLE_VALUE);}
	virtual void __fastcall Disconnect() {CloseHandle(CommHandle); CommHandle=INVALID_HANDLE_VALUE; connected=false;}
	virtual void __fastcall Reconnect() {Disconnect(); Connect();}

	virtual void __fastcall WriteCommand(char *cmd) {if(writebuf) writebuf->Push(cmd);}
	virtual void* __fastcall GetReceivedData() {try {if(readbuf && WaitForSingleObject(readbuf->NotEmptyEvent, 0)==WAIT_OBJECT_0) return readbuf->Pop(); else return NULL;} catch(...){ return NULL;}}
	virtual void __fastcall ShowConnectionError(AnsiString str);

	__property HANDLE SerialHandle = {read=CommHandle};
	__property bool Connected = {read=readConnected};
	__property TEventedQueue* WriteBuf = {read=writebuf};
	__property TEventedQueue* ReadBuf = {read=readbuf};
	__property TMessageObject* ErrMsg = {read=errmsg, write=errmsg};
	__property AnsiString Host = {read=host, write=host};
	__property AnsiString Name = {read=name, write=name};
};

class TCustomSerialThread : public TStartStopThread
{
private:
protected:
	TCustomSerialClient *Owner;
	TObject *Manager;
	bool __fastcall CheckAttemptsQuantity(int i);
	bool __fastcall CheckTermination() {return (Terminated || WaitForSingleObject(SuspendedEvent, 0)==WAIT_OBJECT_0);}
	virtual void __fastcall ExecuteLoopBody() {}
public:
	__fastcall TCustomSerialThread(TCustomSerialClient *AOwner, TObject *AManager) : TStartStopThread() {Owner=AOwner; Manager=AManager;}
	__fastcall ~TCustomSerialThread() {}
};

#endif
