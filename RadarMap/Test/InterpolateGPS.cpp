//---------------------------------------------------------------------------

#include <vcl.h>
#include <math.h>
#pragma hdrstop

#include "InterpolateGPS.h"
#include "Defines.h"
#pragma package(smart_init)
//---------------------------------------------------------------------------

//   Important: Methods and properties of objects in VCL can only be
//   used in a method called using Synchronize, for example:
//
//      Synchronize(UpdateCaption);
//
//   where UpdateCaption could look like:
//
//      void __fastcall Unit1::UpdateCaption()
//      {
//        Form1->Caption = "Updated in a thread";
//      }
//---------------------------------------------------------------------------

__fastcall TInterpolateGPS::TInterpolateGPS(TProfile* P, TTrace *Ptr)
  : TThread(true)
{
	try
	{
		//if(!CreateSuspended) Suspend();
		FreeOnTerminate=true;
		FirstPtr=Ptr;
		Prof=P;
		//if(!CreateSuspended) Resume();
	}
	catch (Exception &exception) { ; }
}
//---------------------------------------------------------------------------

void __fastcall TInterpolateGPS::Execute()
{
	try
	{
		InterpolateTraces(TPositioning::GPS);
	}
	catch (Exception &exception) { ; }
}
//---------------------------------------------------------------------------

void __fastcall TInterpolateGPS::InterpolateTraces(int Pos)
{
	IndInterpolateTraces(Prof, FirstPtr, Pos, FirstPtr->Backward);
}

void IndInterpolateTraces(TProfile* Prof, TTrace *FirstPtr, int Pos, bool Backward)
{
	long double z, zg, c, dlt, dln, dz, dzg, td, a, b, xx, lat1, lon1, lat2, lon2,
		t, t2, conf, dconf, lcsX1, lcsY1, lcsH1, lcsX2, lcsY2, lcsH2, dlcsX, dlcsY, dlcsH;
	int n;
	double Tm, dTm;
	TTrace *tmp, *PtrNext;

	try
	{
		if(Prof && FirstPtr && (FirstPtr->Latitude!=0 || FirstPtr->Longitude!=0 || FirstPtr->Z!=0))
		{
			Tm=FirstPtr->GreenwichTm;
			lat2=FirstPtr->Latitude;//DegMinToDeg(FirstPtr->Latitude);
			lon2=FirstPtr->Longitude;//DegMinToDeg(FirstPtr->Longitude);
			lcsX2=FirstPtr->LocalCsX;//DegMinToDeg(FirstPtr->Latitude);
			lcsY2=FirstPtr->LocalCsY;
			lcsH2=FirstPtr->LocalCsH;
			z=FirstPtr->Z;
			zg=FirstPtr->Z_Geoid;
			conf=FirstPtr->GPSConfidence;
			if(!Backward) PtrNext=FirstPtr->PtrDn;
			else PtrNext=FirstPtr->PtrUp;
			if(PtrNext && PtrNext->Valid)
			{
				//FirstPtr=PtrNext;

				tmp=FirstPtr;
				c=1;
				if(!Backward) PtrNext=tmp->PtrDn;
				else PtrNext=tmp->PtrUp;

				/*while(PtrNext!=NULL && !tmp->GPSInterpolated)
				{
					tmp=PtrNext;
					if(!Backward) PtrNext=tmp->PtrDn;
					else PtrNext=tmp->PtrUp;
					if(tmp->GPSInterpolated && tmp->Latitude==0 && tmp->Longitude==0 && tmp->Z==0) tmp->GPSInterpolated=false;
					c++;
				}*/
				do
				{
					tmp=PtrNext;
					if(tmp!=NULL)
					{
						if(!Backward) PtrNext=tmp->PtrDn;
						else PtrNext=tmp->PtrUp;
						if(tmp->GPSInterpolated && tmp->Latitude==0 && tmp->Longitude==0 && tmp->Z==0) tmp->GPSInterpolated=false;
						c++;
					}
					else PtrNext=NULL;
				} while(PtrNext!=NULL && !tmp->GPSInterpolated);
				if(tmp && tmp->Valid && tmp->GPSInterpolated && (Pos==GPS || Pos==WheelGPS) && (tmp->Latitude!=0 || tmp->Longitude!=0 || tmp->Z!=0))
				{
					lat1=tmp->Latitude;//DegMinToDeg(tmp->Latitude);
					lon1=tmp->Longitude;//DegMinToDeg(tmp->Longitude);
					lcsX1=tmp->LocalCsX;
					lcsY1=tmp->LocalCsY;
					lcsH1=tmp->LocalCsH;
					dTm=(Tm-(double)tmp->GreenwichTm)/c;
					dz=(z-tmp->Z)/c;
					dzg=(zg-tmp->Z_Geoid)/c;
					dconf=(conf-tmp->GPSConfidence)/c;
					dlt=(lat2-lat1)/c;
					dln=(lon2-lon1)/c;
					dlcsX=(lcsX2-lcsX1)/c;
					dlcsY=(lcsY2-lcsY1)/c;
					dlcsH=(lcsH2-lcsH1)/c;
					if(Pos==GPS)
					{
						a=sinl(lat1/(long double)57.29577951)*sinl(lat2/(long double)57.29577951);
						b=cosl(lat1/(long double)57.29577951)*cosl(lat2/(long double)57.29577951)*cosl(lon2/(long double)57.29577951-lon1/(long double)57.29577951);
						xx=a+b;
						if((-xx*xx+(long double)1.)>0)
						  td=(long double)6367444.65*(atanl(-xx/sqrtl(-xx*xx+(long double)1.))+(long double)2.*atanl((long double)1.));
						else td=0;
						td/=c;
						if(!Backward) PtrNext=FirstPtr->PtrDn;
						else PtrNext=FirstPtr->PtrUp;
						if(PtrNext!=NULL)
							PtrNext->TraceDistance=td;
						Prof->HorRangeFull+=td;
						Prof->HorRangeTo=Prof->HorRangeFrom+Prof->HorRangeFull;
					}
				}
				else
				{
					dTm=(double)0.;
					dlt=0.;
					dln=0.;
					dlcsX=0.;
					dlcsY=0.;
					dlcsH=0.;
					dz=0.;
					dzg=0.;
					dconf=0.;
					td=0.;
				}
				tmp=FirstPtr;
				if(!Backward) PtrNext=tmp->PtrDn;
				else PtrNext=tmp->PtrUp;
				while(PtrNext && PtrNext->Valid && !PtrNext->GPSInterpolated)
				{
					tmp=PtrNext;
					Tm-=(double)dTm;
					lat2-=dlt;
					lon2-=dln;
					lcsX2-=dlcsX;
					lcsY2-=dlcsY;
					lcsH2-=dlcsH;
					z-=dz;
					zg-=dzg;
					conf-=dconf;
					tmp->GreenwichTm=Tm;
					tmp->Latitude=lat2;//DegToDegMin(lat2);
					tmp->Longitude=lon2;//DegToDegMin(lon2);
					tmp->LocalCsX=lcsX2;
					tmp->LocalCsY=lcsY2;
					tmp->LocalCsH=lcsH2;
					tmp->LocalCsCh1=FirstPtr->LocalCsCh1;
					tmp->LocalCsCh2=FirstPtr->LocalCsCh2;
					tmp->Z=z;
					tmp->Z_Geoid=zg;
					tmp->GPSConfidence=conf;
					if(Pos==GPS)
					{
						tmp->TraceDistance=td;
						Prof->HorRangeFull+=td;
						Prof->HorRangeTo=Prof->HorRangeFrom+Prof->HorRangeFull;
					}
					tmp->GPSInterpolated=true;
					if(!Backward) PtrNext=tmp->PtrDn;
					else PtrNext=tmp->PtrUp;
					c++;
				}
			}
		}
	}
	catch (...) {}
}
//---------------------------------------------------------------------------

void InterpolateTraces(TProfile* Prof, TTrace *FirstPtr, TInterpolationType What, bool Backward)
{
	long double dv1, dv2, v1, v2;
	int c;
	TTrace *PtrNext=NULL;
	TFlagedValue<float> *s1=NULL, *s2=NULL, *e1=NULL, *e2=NULL;

	try
	{
		if(Prof!=NULL && FirstPtr!=NULL)
		{
			switch(What)
			{
				case itPitchRoll:
				case itPitch:
					s1=(TFlagedValue<float>*)(&(FirstPtr->Pitch));
					if(What==itPitch) break;
				case itRoll:
					s2=(TFlagedValue<float>*)(&(FirstPtr->Roll));
					if(What!=itPitch) s1=s2;
					break;
				case itHeading:
					s1=(TFlagedValue<float>*)(&(FirstPtr->Heading));
					break;
			}
			if(s1 && s1->Flag)
			{
				PtrNext=FirstPtr;
				c=0;
				do
				{
					if(!Backward) PtrNext=PtrNext->PtrDn;
					else PtrNext=PtrNext->PtrUp;
					if(PtrNext!=NULL)
					{
						switch(What)
						{
							case itPitchRoll:
							case itPitch:
								e1=(TFlagedValue<float>*)(&(PtrNext->Pitch));
								if(What==itPitch) break;
							case itRoll:
								e2=(TFlagedValue<float>*)(&(PtrNext->Roll));
								if(What==itRoll) e1=e2;
								break;
							case itHeading:
								e1=(TFlagedValue<float>*)(&(PtrNext->Heading));
								break;
						}
						c++;
					}
				} while(PtrNext && e1 && !e1->Flag);
				if(e1 && !e1->Flag) c=1;
				if(c>0 && e1)
				{
					switch(What)
					{
						case itPitchRoll:
						case itPitch:
						case itRoll:
						case itHeading:
							v1=((TFlagedValue<float>*)s1)->Value;
							dv1=(v1-((TFlagedValue<float>*)e1)->Value)/(float)c;
							if(s2 && e2)
							{
								v2=((TFlagedValue<float>*)s2)->Value;
								dv2=(v2-((TFlagedValue<float>*)e2)->Value)/(float)c;
							}
							else
							{
								v2=0.;
								dv2=0.;
							}
							break;
					}
				}
				else
				{
					dv1=dv2=0.;
				}
				if(!Backward) FirstPtr=FirstPtr->PtrDn;
				else FirstPtr=FirstPtr->PtrUp;
				while(FirstPtr && FirstPtr!=PtrNext)
				{
					v1-=dv1;
					v2-=dv2;
					switch(What)
					{
						case itPitchRoll:
						case itPitch:
							FirstPtr->Pitch=v1;
							if(What==itPitch) break;
						case itRoll:
							FirstPtr->Roll=v2;
							if(What==itRoll || What==itPitchRoll) break;
						case itHeading:
							FirstPtr->Heading=v1;
							break;
					}
					if(!Backward) FirstPtr=FirstPtr->PtrDn;
					else FirstPtr=FirstPtr->PtrUp;
				}
			}
		}
	}
	catch(...) {}
}
//---------------------------------------------------------------------------

long double DegMinToDeg(long double lt)
{
	int n;
	long double t2;

	n=(int)(lt/100.);
	t2=(long double)n+(lt-100.0*(long double)n)/60.;
	return t2;
}
//---------------------------------------------------------------------------

long double DegToDegMin(long double lt)
{
	int n;
	long double t2;

	n=100*(int)lt;
	t2=(long double)n+60.*(lt-(int)lt);
	return t2;
}
//---------------------------------------------------------------------------


