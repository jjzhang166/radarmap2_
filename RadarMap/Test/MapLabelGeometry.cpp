//---------------------------------------------------------------------------

#pragma hdrstop

#include "MapLabelGeometry.h"
#include "GeometryForm.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

//---------------------------------------------------------------------------
// TMapLabelGeometryType
//---------------------------------------------------------------------------
TMapLabelGeometryType StrToTMapLabelGeometryType(AnsiString str)
{
	int n=sizeof(TMapLabelGeometryTypeStrings);
	TMapLabelGeometryType res=mlgtNone;

	for(int i=0; i<n; i++)
	{
		if(str==TMapLabelGeometryTypeStrings[i])
		{
			res=(TMapLabelGeometryType)i;
			break;
		}
	}
	return res;
}

//---------------------------------------------------------------------------
// TLocalMapCustomGeometry
//---------------------------------------------------------------------------
__fastcall TMapLabelGeometry::TMapLabelGeometry(TMyObject *AOwner, Types::TRect OutputRect,
	TObject* aHost, TObject* RadarMapManager, TMapLabelGeometryType aType, TProfileLabelType aLabelType) :
	TProfileObject(AOwner, OutputRect, NULL, RadarMapManager)
{
	profileobjecttype=potMapLabelGeometry;
	geometrytype=aType;
	Host=aHost;
	minvertexestodraw=2;
	editvertexindex=-1;
	CoordinatesList=new TGPSCoordinatesList();
	CoordinatesList->ItemsOwner=false;
	LabelsList=new TMyObjectsList(this);
	LabelsList->ObjectsReIndexing=false;
	LabelsList->FreeObjects=false;
	labeltype=aLabelType;
	maplabeltype=mltNone;
	color=clLime32;
	bordercolor=clBlack32;
	thickness=MapLabelGeometryLineThickness;
	if(Manager && (TRadarMapManager*)Manager)
	{
		id=++((TRadarMapManager*)Manager)->Settings->MapLabelGeometryIDs;
	}
	else id=1;
	name="Geometry "+IntToStr(id);
	description="";
}
//---------------------------------------------------------------------------

int __fastcall TMapLabelGeometry::AddPoint(TMapLabel* aLabel)
{
	int res=-1;

	if(aLabel)
	{
		if(!singlelabeltype || maplabeltype==mltNone || aLabel->MapLabelType==maplabeltype ||
			labeltype==pltNone || aLabel->ProfileLabelType==labeltype)
		{
			res=CoordinatesList->AddUniq(aLabel->Coordinate);
			if(res>=0)
			{
				LabelsList->Add(aLabel);
				if(labeltype==pltNone)
				{
					//color=aLabel->Color;
					//bordercolor
				}
				labeltype=aLabel->ProfileLabelType;
				maplabeltype=aLabel->MapLabelType;
				aLabel->AddGeometry(this);
			}
		}
	}
	return res;
}
//---------------------------------------------------------------------------

void __fastcall TMapLabelGeometry::DeletePoint(TMapLabel* aLabel)
{
	if(aLabel && aLabel->Coordinate)
	{
		CoordinatesList->DeleteUniq(aLabel->Coordinate);
		aLabel->DeleteGeometry(this);
		LabelsList->Delete(aLabel);
		if(VertexesCount==0) labeltype=pltNone;
	}
}
//---------------------------------------------------------------------------

void __fastcall TMapLabelGeometry::Clear()
{
	TMapLabel *ml;

	CoordinatesList->Clear();
	for(int i=0; i<LabelsList->Count; i++)
	{
		ml=dynamic_cast<TMapLabel*>(LabelsList->Items[i]);
		if(ml) ml->DeleteGeometry(this);
	}
	LabelsList->Clear();
	labeltype=pltNone;
}
//---------------------------------------------------------------------------

TMapLabelGeometryEdge __fastcall TMapLabelGeometry::FindNearestEdgeAtXY(Types::TPoint p)
{
	TLine line, line2, line3;
	TFloatLine fline;
	double k, b, d, dmin;
	int j, n, z;
	TMapLabelGeometryEdge res;
	TGPSCoordinate *llp;
	TDoublePoint dp, dp_old;

	j=n=-1;
	dmin=-1;
	z=0;
	for(int i=0; i<VertexesCount; i++)
	{
		//llp=(TGPSCoordinate*)List->Items[i];
		llp=Vertexes[i];
		if(llp && llp->Valid)
		{
			dp=DoublePoint(((TMapsContainersCollection*)Host)->CoordinatesRectToScreen(llp->GetXY(((TMapsContainersCollection*)Host)->CoordinateSystem)));
			if(z==0) line=TLine(dp.X, dp.Y, dp.X, dp.Y);
			else line=TLine(dp_old.X, dp_old.Y, dp.X, dp.Y);
			if(line.X1==line.X2) d=sqrt((long double)(line.X1-p.x)*(line.X1-p.x)+(line.Y1-p.y)*(line.Y1-p.y));
			else
			{
				line.LinearFunction(k, b);
				d=(k*(double)p.x-(double)p.y+b)/sqrt(k*k+1);
			}
			if(z==0)
			{
				dmin=d;
				j=i;
				res.P=Types::TPoint(dp.X, dp.Y);
				res.distance=d;
			}
			else if (dabs(d)<dabs(dmin))
			{
				fline=TFloatLine(p.x, p.y, (double)p.x+d, p.y);
				fline.Rotate(line.Angle()-M_PI_2);
				if(((fline.X2>=line.X1 && fline.X2<=line.X2) ||
					(fline.X2>=line.X2 && fline.X2<=line.X1)) &&
					((fline.Y2>=line.Y1 && fline.Y2<=line.Y2) ||
					(fline.Y2>=line.Y2 && fline.Y2<=line.Y1)))
				{
					res.P=Types::TPoint((int)fline.X2, (int)fline.Y2);
					res.distance=d;
					dmin=d;
					line2=TLine((int)fline.X2, (int)fline.Y2, dp.X, dp.Y);
					line3=TLine((int)fline.X2, (int)fline.Y2, dp_old.X, dp_old.Y);
					if(line2.Length()<=line3.Length())
					{
						j=i;
						n=i-1;
					}
					else
					{
						n=i;
						j=i-1;
					}
				}
			}
			dp_old=dp;
			z++;
		}
	}
	if(j>=0 && j<VertexesCount)
	{
		res.C1=Vertexes[j];
		if(n>0) res.C2=Vertexes[n];
		else res.C2=res.C1;
	}
	else
	{
		res.C1=NULL;
		res.C2=NULL;
		res.P=Types::TPoint(0, 0);
		res.distance=0;
	}
	res.Index=j;
	return res;
}
//---------------------------------------------------------------------------

void __fastcall TMapLabelGeometry::MouseUp(TObject *Sender, TMouseButton Button,
	TShiftState Shift, int X, int Y, TCustomLayer *Layer)
{
	TMapLabelGeometryEdge edge;

	if(Button==mbRight)
	{
		GeometryInfoForm->Manager=(TRadarMapManager*)Manager;
		GeometryInfoForm->Geometry=this;
		GeometryInfoForm->PointPx=Types::TPoint(X, Y);
		edge=FindNearestEdgeAtXY(GeometryInfoForm->PointPx);
		EditVertexIndex=edge.Index;
		if(GeometryInfoForm->Left<X+50) GeometryInfoForm->Left=X+50;
		GeometryInfoForm->Show();
	}
}

//---------------------------------------------------------------------------
// TMapLabelPolyLineGeometry
//---------------------------------------------------------------------------
void __fastcall TMapLabelPolyLineGeometry::DrawLine(TBitmap32 *Bmp, TColor32 Color, TColor32 Border, int Thickness, bool Shadow)
{
	TGPSCoordinate *llp;
	Types::TRect vpt;
	TLine line;
	int i, xmin, xmax, ymin, ymax, linefp_cnt=0;
	TFixedPoint *LineFP;
	TDoublePoint dp, dp_old;
	TColor32Entry ce;

	Rect=Types::TRect(0,0,0,0);
	if(Owner && ((TMapObjectsContainer *)Owner)->Image && Host)
	{
		vpt=((TMapObjectsContainer *)Owner)->Image->GetViewportRect();
		LineFP=new TFixedPoint[VertexesCount];
		try
		{
			if(vpt.Width()>0 && vpt.Height()>0 && VertexesCount>0)
			{
				Bmp->BeginUpdate();
				xmin=xmax=ymin=ymax=0;
				ce.ARGB=Color;
				if(EditVertexIndex==0)
				{
					ce.R=~ce.R;
					ce.G=~ce.G;
					ce.B=~ce.B;
				}
				for(i=0; i<VertexesCount; i++)
				{
					//llp=(TGPSCoordinate*)List->Items[i];
					llp=Vertexes[i];
					if(llp && llp->Valid)
					{
						dp=DoublePoint(((TMapsContainersCollection*)Host)->CoordinatesRectToScreen(llp->GetXY(((TMapsContainersCollection*)Host)->CoordinateSystem)));
						if(linefp_cnt==0)
						{
							line=TLine(dp.X , dp.Y, dp.X, dp.Y);
							xmin=xmax=dp.X;
							ymin=ymax=dp.Y;
						}
						else
						{
							line=TLine(dp_old.X , dp_old.Y, dp.X, dp.Y);
							if(xmin>dp.X) xmin=dp.X;
							if(xmax<dp.X) xmax=dp.X;
							if(ymin>dp.Y) ymin=dp.Y;
							if(ymax<dp.Y) ymax=dp.Y;
						}
						dp_old=dp;
						if(line.IsInRect(vpt)) LineFP[linefp_cnt++]=FixedPoint(line.X2, line.Y2);
						if(i==VertexesCount-1 || (EditVertexIndex>=0 && EditVertexIndex<VertexesCount && (i==(EditVertexIndex-1) || i==(EditVertexIndex+1))))
						{
							DrawPolyLine(Bmp, ce.ARGB, Border, Thickness, Shadow, LineFP, linefp_cnt);
							ce.R=~ce.R;
							ce.G=~ce.G;
							ce.B=~ce.B;
							LineFP[0]=LineFP[linefp_cnt-1];
							linefp_cnt=1;
						}
					}
				}
				Rect=Types::TRect(xmin, ymin, xmax, ymax);
				Bmp->EndUpdate();
			}
		}
		__finally
		{
			delete[] LineFP;
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TMapLabelPolyLineGeometry::Redraw(TObject *Sender, TBitmap32 *Bmp)
{
	TColor32Entry ce;
	float coef;

	try
	{
		if(InRect || Pressed) DrawLine(Bmp, Color, BorderColor, Thickness, false);//true);
		else
		{
			ce.ARGB=Color;
			coef=0.7;
			ce.A=(Byte)((float)ce.A*coef);
			ce.R=(Byte)((float)ce.R*coef);
			ce.G=(Byte)((float)ce.G*coef);
			ce.B=(Byte)((float)ce.B*coef);
			DrawLine(Bmp, ce.ARGB, BorderColor, Thickness*coef, false);
		}
	}
	catch(...) {}
}
//---------------------------------------------------------------------------

bool __fastcall TMapLabelPolyLineGeometry::IsInClientRect(Types::TPoint value)
{
	Types::TRect rct;
	bool res=false;
	TMapLabelGeometryEdge edge;

	if(VertexesCount>1)//>0)
	{
		if(Rect.Width()!=0 && Rect.Height()!=0)
		{
			rct=ClientRect;
			rct.Left-=Thickness*3;
			rct.Top-=Thickness*3;
			rct.Right+=Thickness*3;
			rct.Bottom+=Thickness*3;
			if(rct.Contains(value))
			{
				edge=FindNearestEdgeAtXY(value);
				res=(edge.C1 && edge.C2 && dabs(edge.distance)<Thickness*3);
			}
		}
	}
	return res;
}
//---------------------------------------------------------------------------

bool __fastcall TMapLabelPolyLineGeometry::IsInClientRect(Types::TRect value)
{
	if(Rect.Width()==0 && Rect.Height()==0) return true;
	else return TMapLabelGeometry::IsInClientRect(value);
}
