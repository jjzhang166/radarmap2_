//---------------------------------------------------------------------------

#ifndef MainFormH
#define MainFormH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
#include <GestureMgr.hpp>
#include "G32_ProgressBar.hpp"
#include "GR32_Image.hpp"
#include "Defines.h"
#include "ImageButton.h"
#include <pngimage.hpp>

//---------------------------------------------------------------------------
class TContainerPanel: public TPanel
{
private:
	AnsiString title;
	TLabel* Label1;
	TPanel* container;
	bool lockchilds;
	void __fastcall UnDock(System::TObject* Sender, TControl* Client, TWinControl* NewTarget, bool &Allow) {Allow=!lockchilds;}
#pragma warn -8022
	void __fastcall GetSiteInfo(System::TObject* Sender, TControl* DockClient, Types::TRect &InfluenceRect, const Types::TPoint &MousePos, bool &CanDock) {CanDock=!lockchilds;}
#pragma warn +8022
	//void __fastcall GetSiteInfo(TControl* DockClient, Types::TRect &InfluenceRect, const Types::TPoint &MousePos, bool &CanDock) {TWinControl::GetSiteInfo(DockClient, InfluenceRect, MousePos, CanDock);}
protected:
	//void __fastcall writeTitle(AnsiString value) {title=value; Label1->Caption=" "+value;}
public:
	__fastcall TContainerPanel(Classes::TComponent* AOwner)  : TPanel(AOwner) //, AnsiString t) : TPanel(AOwner)
	{
		Visible=false;
		Parent=(TWinControl*)AOwner;
		BorderStyle=bsNone;
		BevelInner=bvNone;
		BevelOuter=bvNone;
		/*title=t;
		Label1=new TLabel(this);
		Label1->Parent=this;
		Label1->Align=alTop;
		Label1->Color=clBlack;
		Label1->Font->Color=clWhite;
		Label1->Transparent=false;
		Label1->Caption=" "+t;
		Label1->Visible=true;*/
		container=new TPanel(this);
		container->Parent=this;
		container->Caption="";
		container->Align=alClient;
		container->DockSite=true;
		container->Visible=true;
		container->UseDockManager=true;
		//container->DragKind=dkDock;
		container->DragMode=dmAutomatic;
		container->OnUnDock=UnDock;
		container->OnGetSiteInfo=GetSiteInfo;
		Visible=true;
		//Label1->Show();
	}
	__fastcall ~TContainerPanel() {delete container;} //{delete Label1;}
	//__property AnsiString Title = {read=title, write=writeTitle};
	__property TPanel* Container = {read=container};
	__property bool LockChilds = {read=lockchilds, write=lockchilds};
};

class TRadarMapForm : public TForm
{
__published:	// IDE-managed Components
	TTimer *Timer1;
	TSplitter *Splitter;
	void __fastcall FormDestroy(TObject *Sender);
	void __fastcall FormShow(TObject *Sender);
	void __fastcall FormResize(TObject *Sender);
	void __fastcall FormKeyDown(TObject *Sender, WORD &Key, TShiftState Shift);
	void __fastcall FormCanResize(TObject *Sender, int &NewWidth, int &NewHeight, bool &Resize);
	void __fastcall Timer1Timer(TObject *Sender);
	void __fastcall FormActivate(TObject *Sender);
	void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
	void __fastcall FormCloseQuery(TObject *Sender, bool &CanClose);

private:	// User declarations
	bool InResizing;
	TRadarPanelsOrientation __fastcall DetectOrientation();
public:		// User declarations
	__fastcall TRadarMapForm(TComponent* Owner);
	void __fastcall ReOrient();
	void __fastcall ReBorder(bool FromSettings);
};
//---------------------------------------------------------------------------
extern PACKAGE TRadarMapForm *RadarMapForm;
//---------------------------------------------------------------------------
#endif
