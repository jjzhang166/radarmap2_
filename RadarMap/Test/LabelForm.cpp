//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "LabelForm.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "ImageButton"
#pragma link "G32_ProgressBar"
#pragma link "GR32_Image"
#pragma resource "*.dfm"
TLabelInfoForm *LabelInfoForm;

//---------------------------------------------------------------------------
__fastcall TLabelInfoForm::TLabelInfoForm(TComponent* Owner)
	: TForm(Owner)
{
	AttachedFile="";
	cs=csLatLon;
	manager=NULL;

	TitleDown=false;
	CRS_GUIDs = new TStringList();

	ImageButton3Click(ImageButton4);
}
//---------------------------------------------------------------------------

void __fastcall TLabelInfoForm::writeCoordinateSystem(TCoordinateSystem value)
{
	AnsiString str;
	double z;

	cs=value;
	if(label && label->Coordinate)
	{
		csLabel->Caption=label->Coordinate->GetCSName(value);
		if(label->Coordinate->GetCSDimension(cs)==csdDegrees)
		{
			XEdit->Text=FloatToStrF(label->Coordinate->GetX(cs), ffFixed, 10, 6);
			YEdit->Text=FloatToStrF(label->Coordinate->GetY(cs), ffFixed, 10, 6);
		}
		else
		{
			XEdit->Text=FloatToStrF(label->Coordinate->GetX(cs), ffFixed, 10, 2);
			YEdit->Text=FloatToStrF(label->Coordinate->GetY(cs), ffFixed, 10, 2);
		}
		UtmZonePanel->Visible=(cs==csUTM);
		if(cs==csUTM)
		{
			char *Temp=label->Coordinate->GetTemp(cs);

			if(Temp)
			{
				ZoneNumCBox->ItemIndex=ZoneNumCBox->Items->IndexOf(IntToStr(Temp[0]));
				ZoneCharCBox->ItemIndex=ZoneCharCBox->Items->IndexOf((AnsiString)Temp[1]);
			}
		}
		if(label->ProfileObjectType==potStartNGoLabel)
		{
			Label13->Visible=true;
			if(Label13->Caption!="") Label13->Caption+=", ";
			Label13->Caption+="A - Azimuth";
		}
		else
		{
			z=label->Coordinate->GetH(cs);
			//if(Manager && Manager->Settings) z-=Manager->Settings->GpsAntennaHeight;
			if(label->Z>=0) z-=label->Z;
			ZLabel->Caption=FloatToStrF(z, ffFixed, 10, 2)+" m";
			if(label->GPSConfidence>0) ConfidenceLabel->Caption="�"+FloatToStrF(label->GPSConfidence*100., ffFixed, 10, 1)+" cm";
			else ConfidenceLabel->Caption="---";
			if(label->Z>=0) DepthLabel->Caption=FloatToStrF(label->Z, ffFixed, 10, 2)+" m";
			Label13->Visible=(value==csLatLon);
			if(cs==csLatLon) Label13->Caption="X - Longitude, Y - Latitude [deg]";
			else Label13->Caption="";
		}
	}
	else
	{
		TGPSCoordinate *c=new TGPSCoordinate();

		try
		{
			csLabel->Caption=c->GetCSName(value);
		}
		__finally
		{
			delete c;
		}
		UtmZonePanel->Visible=false;
		XEdit->Text="";
		YEdit->Text="";
		ZLabel->Caption="";
		ConfidenceLabel->Caption="---";
		DepthLabel->Caption="";
		Label13->Visible=(value==csLatLon);
		if(value==csLatLon) Label13->Caption="X - Longitude, Y - Latitude [deg]";
		else Label13->Caption="";
	}
}
//---------------------------------------------------------------------------

void __fastcall TLabelInfoForm::writeLabel(TCustomProfileLabel* value)
{
	AnsiString str;
	double z;

	if(value)
	{
		try
		{
            Panel11->Visible=true;
			AttachedFile=value->Attachment;
			attachment_old=AttachedFile;
			TextMemo->Lines->Text=value->Description;
			description_old=TextMemo->Lines->Text;
			str="";

			if(value->ProfileObjectType==potStartNGoLabel || value->ProfileObjectType==potStartNGoGPSLabel)
			{
				if(value->ProfileObjectType==potStartNGoLabel) z=RadToDeg(((TStartNGoLabel*)value)->Azimuth);
				else z=RadToDeg(((TStartNGoGPSLabel*)value)->Azimuth);
				while(z>=180.) z-=360.;
				while(z<=-180.) z+=360.;
				ZLabel->Caption=FloatToStrF(z, ffFixed, 10, 2);
			}
			if(value->Coordinate)
			{
				/*str+=value->Coordinate->AsString(cs, false);
				z=value->Coordinate->GetH(cs);
				if(value->Z>=0) z-=value->Z;
				str+=" Z="+FloatToStrF(z, ffFixed, 10, 2)+" m";
				if(value->GPSConfidence>0) str+=" Confidence=�"+FloatToStrF(value->GPSConfidence*100., ffFixed, 10, 1)+" cm";
				if(value->Z>=0) str+=" Depth="+FloatToStrF(value->Z, ffFixed, 10, 2)+" m";
				str+="\n"; */
				if(value->Coordinate->GetCSDimension(cs)==csdDegrees)
				{
					XEdit->Text=FloatToStrF(value->Coordinate->GetX(cs), ffFixed, 10, 6);
					YEdit->Text=FloatToStrF(value->Coordinate->GetY(cs), ffFixed, 10, 6);
				}
				else
				{
					XEdit->Text=FloatToStrF(value->Coordinate->GetX(cs), ffFixed, 10, 2);
					YEdit->Text=FloatToStrF(value->Coordinate->GetY(cs), ffFixed, 10, 2);
				}
				UtmZonePanel->Visible=(cs==csUTM);
				if(cs==csUTM)
				{
					char *Temp=value->Coordinate->GetTemp(cs);

					if(Temp)
					{
						ZoneNumCBox->ItemIndex=ZoneNumCBox->Items->IndexOf(IntToStr(Temp[0]));
						ZoneCharCBox->ItemIndex=ZoneCharCBox->Items->IndexOf((AnsiString)Temp[1]);
					}
				}
				if(value->ProfileObjectType!=potStartNGoLabel && value->ProfileObjectType!=potStartNGoGPSLabel)
				{
					z=value->Coordinate->GetH(cs);
					if(value->Z>=0) z-=value->Z;
					ZLabel->Caption=FloatToStrF(z, ffFixed, 10, 2)+" m";
					if(value->GPSConfidence>0) ConfidenceLabel->Caption="�"+FloatToStrF(value->GPSConfidence*100., ffFixed, 10, 1)+" cm";
					else ConfidenceLabel->Caption="---";
					if(value->Z>=0) DepthLabel->Caption=FloatToStrF(value->Z, ffFixed, 10, 2)+" m";
				}
			}
			if((int)value->CreatedTime>0) CreatedLabel->Caption=MyDateTimeToStr(value->CreatedTime)+"\n";
			if((int)value->ModifiedTime>0) ModifiedLabel->Caption=MyDateTimeToStr(value->ModifiedTime);
			LabelIDEdit->Text=IntToStr((int)value->ID);
			if(cs==csLatLon) Label13->Caption="X - Longitude, Y - Latitude";
			else Label13->Caption="";
			switch(value->ProfileObjectType)
			{
				case potReferenceLabel:
				case potWaypointLabel:
					XEdit->ReadOnly=false;
					YEdit->ReadOnly=false;
					ZoneNumCBox->Enabled=true;
					ZoneCharCBox->Enabled=true;
					ReferencesPBar->Visible=((TReferncePointLabel*)value)->IsCollecting;
					ApplyIButton->Visible=true;
					AcquireIButton->Visible=true;
					ConfidencePanel->Visible=false;
					DepthPanel->Visible=false;
					StartTWDRSIButton->Visible=false;
					StopTWDRSIButton->Visible=false;
					Label13->Visible=(cs==csLatLon);
					Label5->Caption="H";
					break;
				case potStartNGoLabel:
				case potStartNGoGPSLabel:
					XEdit->ReadOnly=(((TStartNGoLabel*)value)->State!=sgnPlaced);
					YEdit->ReadOnly=(((TStartNGoLabel*)value)->State!=sgnPlaced);
					ZoneNumCBox->Enabled=!XEdit->ReadOnly;
					ZoneCharCBox->Enabled=!XEdit->ReadOnly;
					ReferencesPBar->Visible=(value->ProfileObjectType==potStartNGoGPSLabel && ((TStartNGoGPSLabel*)value)->IsCollecting);
					ApplyIButton->Visible=(((TStartNGoLabel*)value)->State<=sgnPlaced);
					AcquireIButton->Visible=(value->ProfileObjectType==potStartNGoGPSLabel && ((TStartNGoLabel*)value)->State<=sgnPlaced);
					ConfidencePanel->Visible=(!AcquireIButton->Visible && value->ProfileObjectType==potStartNGoLabel);
					DepthPanel->Visible=false;
					StartTWDRSIButton->Visible=true;
					StopTWDRSIButton->Visible=true;
					Label5->Caption="A";//+(char)176;
					if(Label13->Caption!="") Label13->Caption+=", ";
					Label13->Caption+="A - Azimuth";
					Label13->Visible=true;
					StartTWDRSIButton->Enabled=(((TStartNGoLabel*)value)->State==sgnPlaced);
					StopTWDRSIButton->Enabled=(((TStartNGoLabel*)value)->State==sgnStarted);
					break;
				default:
					XEdit->ReadOnly=true;
					YEdit->ReadOnly=true;
					ZoneNumCBox->Enabled=!XEdit->ReadOnly;
					ZoneCharCBox->Enabled=!XEdit->ReadOnly;
					ReferencesPBar->Visible=false;
					ApplyIButton->Visible=false;
					AcquireIButton->Visible=false;
					ConfidencePanel->Visible=true;
					DepthPanel->Visible=true;
					StartTWDRSIButton->Visible=false;
					StopTWDRSIButton->Visible=false;
					Label5->Caption="H";
					Label13->Visible=(cs==csLatLon);
			}
			if(Label13->Caption!="") Label13->Caption+=" [deg]";
			CancelIButton->Visible=AcquireIButton->Visible;
			XPanel->Visible=!ReferencesPBar->Visible;
			YPanel->Visible=!ReferencesPBar->Visible;
			ZPanel->Visible=!ApplyIButton->Visible;
			ApplyIButton->Enabled=false;
		}
		catch(Exception &e)
		{
			value=NULL;
		}
	}
	else
	{
		Panel11->Visible=false;
    }
	label=value;
}
//---------------------------------------------------------------------------

void __fastcall TLabelInfoForm::writeAttachedFile(AnsiString value)
{
	if(FileExists(value))
	{
		TBitmap32 *Src;
		bool Err=false;
		float w, h, ww, hh, c;

		attachedfile=value;
		FileLabel->Caption=ExtractFileName(value);

		//
		Src=new TBitmap32;
		try
		{
			try
			{
				//Src->LoadFromFile(value);
				Gdiplus::GdiplusStartupInput gdiplusStartupInput;
				ULONG_PTR  gdiplusToken;
				GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);
				try
				{
					try
					{
						Gdiplus::Graphics *graphics = NULL;
						Gdiplus::Bitmap *originalimage = new Gdiplus::Bitmap(((UnicodeString)value).c_str());// (&(Src->BitmapInfo), Src->Bits);
						Gdiplus::Image *thumbnail=NULL;
						try
						{
							if(originalimage->GetFlags()!=Gdiplus::ImageFlagsNone)
							{
								w=originalimage->GetWidth();
								h=originalimage->GetHeight();
								ww=PreviewImage->Width;
								hh=PreviewImage->Height;
								if(w/h<ww/hh) c=h/hh;
								else c=w/ww;
								h/=c;
								if(h>hh) h=hh;
								w/=c;
								if(w>ww) w=ww;
								Src->SetSize((int)w, (int)h);
								graphics=new Gdiplus::Graphics(Src->Handle);
								graphics->ResetTransform();
								thumbnail=originalimage->GetThumbnailImage(Src->Width, Src->Height, NULL, NULL);
								graphics->DrawImage(thumbnail, 0, 0);
							}
							else Err=true;
						}
						__finally
						{
							if(graphics) delete graphics;
							delete originalimage;
							if(thumbnail) delete thumbnail;
						}
					}
					catch(Exception &e)
					{
						Err=true;
					}
				}
				__finally
				{
					Gdiplus::GdiplusShutdown(gdiplusToken);
				}
                if(!Err)
				{
					PreviewImage->Bitmap->SetSizeFrom(Src);
					PreviewImage->Bitmap->Draw(0, 0, Src);
				}
			}
			catch(Exception &e)
			{
				Err=true;
			}
		}
		__finally
		{
			delete Src;
		}
		PreviewPanel->Visible=!Err;
		OpenIButton->Enabled=true;
		RemoveIButton->Enabled=true;
	}
	else
	{
		attachedfile="";
		FileLabel->Caption="";
		PreviewPanel->Visible=false;
		OpenIButton->Enabled=false;
		RemoveIButton->Enabled=false;
	}
}
//---------------------------------------------------------------------------

void __fastcall TLabelInfoForm::RecolorReferencesPBar()
{
	unsigned char r, r2, g, g2, b, b2;

	r=GetRValue(0x00C0673F);
	g=GetGValue(0x00C0673F);
	b=GetBValue(0x00C0673F);
	r2=GetRValue(0x00FFA047);
	g2=GetGValue(0x00FFA047);
	b2=GetBValue(0x00FFA047);
	r+=(unsigned char)((float)ReferencesPBar->Position*(float)(r2-r)/100.);
	g+=(unsigned char)((float)ReferencesPBar->Position*(float)(g2-g)/100.);
	b+=(unsigned char)((float)ReferencesPBar->Position*(float)(b2-b)/100.);
	ReferencesPBar->Color=(TColor)RGB(r, g, b);
}
//---------------------------------------------------------------------------

void __fastcall TLabelInfoForm::CloseIButtonClick(TObject *Sender)
{
	Close();
}

//---------------------------------------------------------------------------
void __fastcall TLabelInfoForm::ImageButton3Click(TObject *Sender)
{
	int z =((TComponent*)Sender)->Owner->ComponentCount;

	for(int i = 0; i < z; i++)
	{
		if(((TComponent *)Sender)->Owner->Components[i]->ClassName()=="TImageButton" && ((TComponent*)Sender)->Owner->Components[i]!=Sender)
			((TImageButton*)((TComponent *)Sender)->Owner->Components[i])->Down=false;
	}
	((TImageButton*)Sender)->Down=true;
	Notebook1->PageIndex=((TImageButton*)Sender)->Tag;
}
//---------------------------------------------------------------------------

void __fastcall TLabelInfoForm::FormShow(TObject *Sender)
{
	int z=ComponentCount;

	for(int i = 0; i < z; i++)
	{
		if(Components[i]->ClassName()=="TImageButton")
		{
			if(((TImageButton*)Components[i])->Tag==Notebook1->PageIndex)
				((TImageButton*)Components[i])->Down=true;
			else ((TImageButton*)Components[i])->Down=false;
		}
	}
	Timer1Timer(Sender);
	/*AcquireIButton->Visible=ApplyIButton->Visible;
	CancelIButton->Visible=ApplyIButton->Visible;
	ReferencesPBar->Visible=ApplyIButton->Visible;
	XLabel->Visible=ApplyIButton->Visible;
	YLabel->Visible=ApplyIButton->Visible;
	XPanel->Visible=!ApplyIButton->Visible;
	YPanel->Visible=!ApplyIButton->Visible;
	ZPanel->Visible=!ApplyIButton->Visible;*/
	if(XEdit->ReadOnly) HideCaret(XEdit->Handle);
	if(YEdit->ReadOnly) HideCaret(YEdit->Handle);
	//ImageButton3Click(ImageButton4);
}
//---------------------------------------------------------------------------

void __fastcall TLabelInfoForm::XEditChange(TObject *Sender)
{
	if(((TEdit*)Sender)->ReadOnly) HideCaret(((TEdit*)Sender)->Handle);
	else ApplyIButton->Enabled=true;
}
//---------------------------------------------------------------------------

void __fastcall TLabelInfoForm::PreviewImageClick(TObject *Sender)
{
	if(AttachedFile!="")
	{
		if(!ShellExecute(NULL, NULL, AttachedFile.c_str(), NULL, NULL, SW_SHOW))
		{
			ShowMessage("Cannot find associated program with "+FileLabel->Caption+"!");
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TLabelInfoForm::ImageButton7Click(TObject *Sender)
{
	int z =((TComponent*)Sender)->Owner->ComponentCount;

	for(int i = 0; i < z; i++)
	{
		if(((TComponent *)Sender)->Owner->Components[i]->ClassName()=="TImageButton" && ((TComponent*)Sender)->Owner->Components[i]!=Sender)
			((TImageButton*)((TComponent *)Sender)->Owner->Components[i])->Down=false;
	}
	((TImageButton*)Sender)->Down=true;

	int i=-1;
	try
	{
		if(label)
		{
			i=Application->MessageBox(L"Do you really want to delete this Label?",
				((UnicodeString)_RadarMapName).w_str(), MB_YESNO);
			if(i==IDYES)
			{
				delete label;
				label=NULL;
			}
		}
	}
	catch(Exception &e) {}
	if(i==IDYES) Close();
	else ImageButton3Click(ImageButton4);
}
//---------------------------------------------------------------------------

void __fastcall TLabelInfoForm::Notebook1PageChanged(TObject *Sender)
{
	//TempEdit->SetFocus();

}
//---------------------------------------------------------------------------

void __fastcall TLabelInfoForm::LabelIDEditKeyPress(TObject *Sender, wchar_t &Key)
{
	bool res=false;

	switch(Key)
	{
		case '0':
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
		case 8: res=true; break;
		default: ;
	}
	if(!res)
	{
		//if(label) ((TEdit*)Sender)->Text=IntToStr(label->ID);
		//else ((TEdit*)Sender)->Text="0";
		((TEdit*)Sender)->Color=(TColor)0xD0D0FE;
		//if(((TEdit*)Sender)->Visible) ((TEdit*)Sender)->SetFocus();
		Timer1->Enabled=true;
		Key=0;
	}
}
//---------------------------------------------------------------------------

void __fastcall TLabelInfoForm::Timer1Timer(TObject *Sender)
{
	LabelIDEdit->Color=(TColor)0x00E9E2E1;
	XEdit->Color=(TColor)0x00E9E2E1;
	YEdit->Color=(TColor)0x00E9E2E1;
	TextMemo->Color=(TColor)0x00E9E2E1;
	Timer1->Enabled=false;
}
//---------------------------------------------------------------------------

void __fastcall TLabelInfoForm::XEditKeyPress(TObject *Sender, wchar_t &Key)
{
	bool res=false;

	switch(Key)
	{
		case '0':
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
		case 8: res=true; break;
		case '.':
		case ',':
			if(((TEdit*)Sender)->Text.Pos(DecimalSeparator)>0) Key=0;
			else
			{
				Key=DecimalSeparator;
				res=true;
			}
			break;
		case '-':
			if(((TEdit*)Sender)->Text.Pos('-')>0) Key=0;
			break;
		default: ;
	}
	if(!res)
	{
		((TEdit*)Sender)->Color=(TColor)0xD0D0FE;
		//((TEdit*)Sender)->SetFocus();
		Timer1->Enabled=true;
		Key=0;
	}
}
//---------------------------------------------------------------------------

void __fastcall TLabelInfoForm::BrowseIButtonClick(TObject *Sender)
{
	if(OpenDialog->Execute())
	{
		AttachedFile=OpenDialog->FileName;
	}
}
//---------------------------------------------------------------------------

void __fastcall TLabelInfoForm::RemoveIButtonClick(TObject *Sender)
{
	AttachedFile="";
}
//---------------------------------------------------------------------------

void __fastcall TLabelInfoForm::OkImageButtonClick(TObject *Sender)
{
	if(label)
	{
		try
		{
			label->Attachment=AttachedFile;
			label->Description=TextMemo->Lines->Text;
			label->ID=StrToInt(LabelIDEdit->Text);
			if(attachment_old!=AttachedFile || description_old!=TextMemo->Lines->Text)
			{
				label->WriteModifiedTime(MyDateTimeToStr(Now()));
			}
		}
		catch(Exception &e) {}
	}
	Close();
}
//---------------------------------------------------------------------------

void __fastcall TLabelInfoForm::FormClose(TObject *Sender, TCloseAction &Action)
{
	label=NULL;
}
//---------------------------------------------------------------------------

void __fastcall TLabelInfoForm::FormHide(TObject *Sender)
{
	TCloseAction b;

	FormClose(Sender, b);
}
//---------------------------------------------------------------------------

void __fastcall TLabelInfoForm::ApplyIButtonClick(TObject *Sender)
{
	double x, y;
	TGPSCoordinate *c;
	bool err=false;

	if(label)
	{
		try
		{
			if(XEdit->Text.Length()>0 && YEdit->Text.Length()>0)
			{
				x=StrToFloat(XEdit->Text);
				y=StrToFloat(YEdit->Text);
				if(label->ProfileObjectType==potReferenceLabel)
					((TReferncePointLabel*)label)->StopReferencesCollection();
				else if(label->ProfileObjectType==potStartNGoGPSLabel)
					((TStartNGoGPSLabel*)label)->StopReferencesCollection();
				c=new TGPSCoordinate();
				/*if(cs==csLatLon)
				{
					c->Latitude=y;
					c->Longitude=x;
				}
				else
				{
					c->SetX(cs, x);
					c->SetY(cs, y);
				}*/
				if(cs==csLatLon) c->SetLatLon(y, x);
				else if(cs==csUTM)
				{
					char Temp[4];

					Temp[0]=StrToInt(ZoneNumCBox->Text);
					if(ZoneCharCBox->Text!="" && ZoneCharCBox->Text.Length()>0)
						Temp[1]=ZoneCharCBox->Text[1];
					c->SetXY(cs, x, y, Temp);
				}
				else c->SetXY(cs, x, y, NULL);
				c->Confidence=0.0;
				label->GPSConfidence=0.0;
				label->Coordinate=c;
				writeLabel(label);
				//OkImageButtonClick(NULL);
			}
			else err=true;
		}
		catch(Exception &e)
		{
			err=true;
		}
		if(err)
		{
            XEdit->Color=(TColor)0xD0D0FE;
			YEdit->Color=(TColor)0xD0D0FE;
//			if(!ReferencesPBar->Visible) XEdit->SetFocus();
			Timer1->Enabled=false;
        }
	}
}
//---------------------------------------------------------------------------

void __fastcall TLabelInfoForm::XEditExit(TObject *Sender)
{
	try
	{
		if(((TEdit*)Sender)->Text.Length()>0)
			StrToFloat(((TEdit*)Sender)->Text);
	}
	catch(Exception &e)
	{
		((TEdit*)Sender)->Text="0.00";
		((TEdit*)Sender)->Color=(TColor)0xD0D0FE;
//		if(!ReferencesPBar->Visible) ((TEdit*)Sender)->SetFocus();
		Timer1->Enabled=true;
	}
}
//---------------------------------------------------------------------------

void __fastcall TLabelInfoForm::LabelIDEditExit(TObject *Sender)
{
	int id;
	bool err=false;

	try
	{
		if(LabelIDEdit->Text.Length()>0)
		{
			id=StrToInt(LabelIDEdit->Text);
			if(manager && manager->MapObjects && manager->MapObjects->GetMapLabel(id))
				err=true;
		}
	}
	catch(Exception &e) {err=true;}
	if(err)
	{
		if(label) LabelIDEdit->Text=IntToStr((int)label->ID);
		else LabelIDEdit->Text=="0";
		LabelIDEdit->Color=(TColor)0xD0D0FE;
		//LabelIDEdit->SetFocus();
		Timer1->Enabled=true;
	}
}
//---------------------------------------------------------------------------

void __fastcall TLabelInfoForm::AcquireIButtonClick(TObject *Sender)
{
	if(label && (label->ProfileObjectType==potReferenceLabel || label->ProfileObjectType==potStartNGoGPSLabel))
	{
		label->Coordinate=NULL;
		if(label->ProfileObjectType==potReferenceLabel)
			((TReferncePointLabel*)label)->StartReferencesCollection();
		else if(label->ProfileObjectType==potStartNGoGPSLabel)
			((TStartNGoGPSLabel*)label)->StartReferencesCollection();
		ReferencesPBar->Position=0;
		RecolorReferencesPBar();
		ReferencesPBar->Visible=true;
		XPanel->Visible=!ReferencesPBar->Visible;
		YPanel->Visible=!ReferencesPBar->Visible;
		CancelIButton->Enabled=true;
		ApplyIButton->Enabled=false;
	}
}
//---------------------------------------------------------------------------

void __fastcall TLabelInfoForm::CancelIButtonClick(TObject *Sender)
{
	ReferencesPBar->Visible=false;
	if(label)
	{
		if(label->ProfileObjectType==potReferenceLabel)
			((TReferncePointLabel*)label)->StopReferencesCollection();
		else if(label->ProfileObjectType==potStartNGoGPSLabel)
		{
			((TStartNGoGPSLabel*)label)->StopReferencesCollection();
            ((TStartNGoGPSLabel*)label)->State=sgnPlaced;
		}
	}
	XPanel->Visible=!ReferencesPBar->Visible;
	YPanel->Visible=!ReferencesPBar->Visible;
	CancelIButton->Enabled=false;
	ApplyIButton->Enabled=false;
}
//---------------------------------------------------------------------------

void __fastcall TLabelInfoForm::TitlePanelMouseDown(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y)
{
	TitleDown=true;
	TitleXY=ClientToScreen(Types::TPoint(X, Y));
}
//---------------------------------------------------------------------------

void __fastcall TLabelInfoForm::TitlePanelMouseMove(TObject *Sender, TShiftState Shift,
		  int X, int Y)
{
	Types::TPoint tmp;

	if(TitleDown)
	{
		tmp=ClientToScreen(Types::TPoint(X, Y));
		Left+=tmp.x-TitleXY.x;
		Top+=tmp.y-TitleXY.y;
		TitleXY=tmp;
	}
}
//---------------------------------------------------------------------------

void __fastcall TLabelInfoForm::TitlePanelMouseUp(TObject *Sender, TMouseButton Button,
	TShiftState Shift, int X, int Y)
{
	TitleDown=false;
}
//---------------------------------------------------------------------------

void __fastcall TLabelInfoForm::csLabelClick(TObject *Sender)
{
	Types::TPoint p;
	TGPSCoordinate *c;
	TMenuItem *NewItem;
	TPlugInObject *plugin;

	CSPopupMenu->Items->Clear();
	try
	{
		c = new TGPSCoordinate();
		try
		{
			for(int i = 0; i < csTotalFixed; i++)
			{
				NewItem=new TMenuItem(this);
				NewItem->Caption=c->GetCSName((TCoordinateSystem)i)+" ("+c->GetCSEllipsoid((TCoordinateSystem)i)+")";
				NewItem->Tag=i;
				NewItem->OnClick=CSMenuItemClick;
				CSPopupMenu->Items->Add(NewItem);
			}
			CRS_GUIDs->Clear();
			if(PlugInManager && PlugInManager->PlugInsList->Count>0)
			{
				for(int i=0; i< PlugInManager->PlugInsList->Count; i++)
				{
					if(PlugInManager->PlugInsList->Items[i] && PlugInManager->PlugInsList->Items[i]->Type==pitCRS)
					{
						plugin = PlugInManager->PlugInsList->Items[i];
						NewItem=new TMenuItem(this);
						NewItem->Caption=plugin->Name;
						NewItem->Tag=i+csTotalFixed;
						NewItem->OnClick=CSMenuItemClick;
						CRS_GUIDs->Add(plugin->GUID);
						CSPopupMenu->Items->Add(NewItem);
					}
				}
			}
			p=Types::TPoint(csLabel->Left+PagesPanel->Left, csLabel->Top+PagesPanel->Top+csLabel->Height);
			p=ClientToScreen(p);
			CSPopupMenu->Popup(p.x, p.y);
		}
		catch(...) {}
	}
	__finally
	{
        delete c;
    }
}
//---------------------------------------------------------------------------

void __fastcall TLabelInfoForm::CSMenuItemClick(TObject *Sender)
{
	int i;

	if(Sender)
	{
		i=((TMenuItem*)Sender)->Tag;
		if(i<csTotalFixed) CoordinateSystem=(TCoordinateSystem)i;
		else
		{
			i-=csTotalFixed;
			if(i<CRS_GUIDs->Count)
			{
				PlugInManager->SetActive(CRS_GUIDs->Strings[i], pitCRS);
				CoordinateSystem=csDLL;
			}
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TLabelInfoForm::StartTWDRSIButtonClick(TObject *Sender)
{
	if(label && (label->ProfileObjectType==potStartNGoLabel || label->ProfileObjectType==potStartNGoGPSLabel) &&
		((TStartNGoLabel*)label)->State==sgnPlaced)
	{
		((TStartNGoLabel*)label)->State=sgnAzimuthed;
		StartTWDRSIButton->Enabled=(((TStartNGoLabel*)label)->State==sgnPlaced);
		StopTWDRSIButton->Enabled=(((TStartNGoLabel*)label)->State==sgnStarted);
	}
}
//---------------------------------------------------------------------------

void __fastcall TLabelInfoForm::StopTWDRSIButtonClick(TObject *Sender)
{
	if(label && (label->ProfileObjectType==potStartNGoLabel || label->ProfileObjectType==potStartNGoGPSLabel) &&
		((TStartNGoLabel*)label)->State>sgnPlaced)
	{
		((TStartNGoLabel*)label)->State=sgnStopping;
		StartTWDRSIButton->Enabled=(((TStartNGoLabel*)label)->State==sgnPlaced);
		StopTWDRSIButton->Enabled=(((TStartNGoLabel*)label)->State==sgnStarted);
	}
}
//---------------------------------------------------------------------------

void __fastcall TLabelInfoForm::TextMemoKeyPress(TObject *Sender, wchar_t &Key)
{
	bool res=false;

	if((Key>='a' && Key<='z') || (Key>='A' && Key<='Z') || (Key>='0' && Key<='9')) res=true;
	else switch(Key)
	{
		case '.':
		case ',':
		case '?':
		case '|':
		case ':':
		case ';':
		case '{':
		case '}':
		case '[':
		case ']':
		case '+':
		case '=':
		case '_':
		case '-':
		case ')':
		case '(':
		case '*':
		case '&':
		case '^':
		case '%':
		case '$':
		case '@':
		case '!':
		case '~':
		case ' ':
		case 8:
		case 13:
		case 127:
		res=true; break;
		default: ;
	}
	if(!res)
	{
		((TMemo*)Sender)->Color=(TColor)0xD0D0FE;
//		((TMemo*)Sender)->SetFocus();
		Timer1->Enabled=true;
		Key=0;
	}
}
//---------------------------------------------------------------------------

