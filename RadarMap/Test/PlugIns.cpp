//---------------------------------------------------------------------------
#pragma hdrstop

#include "PlugIns.h"
#include <stdio.h>

//---------------------------------------------------------------------------

#pragma package(smart_init)

TPlugInManager *PlugInManager;

bool __fastcall CheckLibrarySignature(HINSTANCE hInst)
{
	void (*GetSignature) (char*, int);
	char buf[sizeof(RadarMapSignatureGUID)];
	bool res=false;

	if(hInst)
	{
		GetSignature=(void (*)(char*, int))GetProcAddress(hInst, "_GetLibrarySignature");
		if(GetSignature)
		{
			GetSignature(buf, sizeof(buf));
			if(strncmp(buf, RadarMapSignatureGUID, GUID_Length)==0) res=true;
		}
	}
	return res;
}
//---------------------------------------------------------------------------

bool __fastcall IsValidGUID(AnsiString guid)
{
	bool res=false;

	if(guid.Length()==GUID_Length && guid[1]=='{' && guid[10]=='-' && guid[15]=='-' && guid[20]=='-' && guid[25]=='-' && guid[GUID_Length]=='}')
	{
		res=true;
		guid=guid.UpperCase();
		for(int i=2; i<GUID_Length-1; i++)
		{
			if(i!=10 && i!=15 && i!=20 && i!=25)
			{
				if(!((guid[i]>='0' && guid[i]<='9') || (guid[i]>='A' && guid[i]<='F')))
				{
					res=false;
					break;
				}
			}
		}
	}

	return res;
}

//---------------------------------------------------------------------------
// TPlugInObject
//---------------------------------------------------------------------------
__fastcall TPlugInObject::TPlugInObject(AnsiString aGUID, TPlugInType pit) : TMyObject()
{
	//strnset(guid, 0, sizeof(guid));
	guid=aGUID;
	plugintype=pit;
	hInstance=NULL;
	pInitPlugIn=pFreePlugIn=NULL;
	pGetPlugInInfo=NULL;
	pGetOption=pSetOption=NULL;
	options=new TList();
	dllname="";
	pluginid=-1;
}
//---------------------------------------------------------------------------

__fastcall TPlugInObject::TPlugInObject(AnsiString aGUID, TPlugInType pit,
	AnsiString aDllName, int aPlugInID, AnsiString aName, int aOptionsCount) : TMyObject()
{
	//strcpy(guid, aGUID, GUID_Length);
	guid=aGUID;
	plugintype=pit;
	hInstance=NULL;
	pInitPlugIn=pFreePlugIn=NULL;
	pGetPlugInInfo=NULL;
	pGetOption=pSetOption=NULL;
	options=new TList();
	dllname=aDllName;
	pluginid=aPlugInID;
	name=aName;
	optionscount=aOptionsCount;
}
//---------------------------------------------------------------------------

bool __fastcall TPlugInObject::Initialize()
{
	try
	{
		hInstance=LoadLibrary(DLLFileName.c_str());
		pInitPlugIn=(TInitFreePlugIn)GetProcAddress(hInstance, "_InitPlugIn");
		pFreePlugIn=(TInitFreePlugIn)GetProcAddress(hInstance, "_FreePlugIn");
		pGetPlugInInfo=(TGetPlugInInfo)GetProcAddress(hInstance, "_GetPlugInInfo");
		pGetOption=(TGetOption)GetProcAddress(hInstance, "_GetOption");
		pSetOption=(TGetOption)GetProcAddress(hInstance, "_SetOption");
		if(Active && CheckLibrarySignature(hInstance))
		{
			pInitPlugIn(PlugInID);
			if(optionscount!=options->Count || optionscount==0)
				GetOptions();
			else SetOptions();
		}
		else throw Exception("Cannot recognize Signature of PlugIns Library ("+DLLFileName+")");
	}
	catch(...)
	{
		hInstance=NULL;
		pInitPlugIn=pFreePlugIn=NULL;
		pGetPlugInInfo=NULL;
		pGetOption=pSetOption=NULL;
	}
	return Active;
}
//---------------------------------------------------------------------------

void __fastcall TPlugInObject::Free()
{
	if(Active)
	{
		try
		{
			pFreePlugIn(PlugInID);
			if(!FreeLibrary(hInstance)) UnmapViewOfFile(hInstance);
		}
		catch(...) {}
		hInstance=NULL;
		pInitPlugIn=pFreePlugIn=NULL;
		pGetPlugInInfo=NULL;
		pGetOption=pSetOption=NULL;
		for(int i=0; i<options->Count; i++)
			delete (TOption*)options->Items[i];
		options->Clear();
	}
}
//---------------------------------------------------------------------------

void __fastcall TPlugInObject::GetOptions(HINSTANCE hInst)
{
	//TGetOption GetOption;
	TOption *option, *option2;
	bool b;

	if(!hInst) hInst=hInstance;
	if(hInst)
	{
		//GetOption=(TGetOption)GetProcAddress(hInst, "_GetOption");
		if(!pGetOption) pGetOption=(TGetOption)GetProcAddress(hInst, "_GetOption");
		if(pGetOption && optionscount>0)
		{
			/*for(int i=0; i<options->Count; i++)
				delete options->Items[i];
			options->Clear();*/
			b=(options->Count==optionscount);
			for(int i=0; i<optionscount; i++)
			{
				option=new TOption;
				//if(!(GetOption)(PlugInID, i, (char*)option, sizeof(TOption)))
				if(!pGetOption(PlugInID, i, (char*)option, sizeof(TOption)))
				{
					delete option;
					option=NULL;
				}
				if(options->Count>i) option2=(TOption*)options->Items[i];
				else option2=NULL;
				if(!(b && option2 && option && option->Type==option2->Type))
				{
					if(options->Count>i)
					{
						int c=options->Count;
						for(int j=i; j<c; j++)
						{
							if(options->Items[i]) delete (TOption*)options->Items[i];
							options->Delete(i);
						}
					}
					b=false;
					options->Add(option);
				}
			}
		}
	}
}

void __fastcall TPlugInObject::SetOptions(HINSTANCE hInst)
{
	TOption* option;
	//void (*SetOption)(int, int, char*, unsigned int);

	if(!hInst) hInst=hInstance;
	if(hInst && options->Count>0)
	{
		//SetOption=(void (*) (int, int, char*, unsigned int))GetProcAddress(hInst, "_SetOption");
		if(pSetOption)
		{
			for(int i=0; i<options->Count; i++)
			{
				option=(TOption*)options->Items[i];
				if(option)
					//(SetOption)(PlugInID, i, (char*)option, sizeof(TOption));
					pSetOption(PlugInID, i, (char*)option, sizeof(TOption));
				/*if(GetPlugInInfo(Id, (char*)(&crs_info), sizeof(crs_info)))
				{
					piInfo=(char*)(&crs_info);
				}*/
			}
            UpdatePlugInInfo();
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TPlugInObject::SaveSettingsByGUID(AnsiString aGUID)
{
	TRegistry& regKey=*new TRegistry();
	AnsiString regkey=_RadarMapRegKey+(AnsiString)_PlugInsRegKey;
	TOption *option;

	if(aGUID=="") aGUID=guid;
	try
	{
		try
		{
			if(aGUID!="" && regKey.OpenKey(regkey+"\\"+aGUID, true))
			{
				regKey.WriteInteger("Type", (int)plugintype);
				regKey.WriteString("DLLFileName", dllname);
				regKey.WriteInteger("PlugInID", pluginid);
				regKey.WriteString("Name", name);
				regKey.WriteInteger("OptionsCount", optionscount);
				SaveLocalParameters(&regKey);
				regKey.CloseKey();
				if(optionscount>0 && regKey.OpenKey(regkey+"\\"+aGUID+"\\Options", true))
				{
					for(int i=0; i<options->Count; i++)
					{
						option=(TOption*)options->Items[i];
						if(option)
						{
							regKey.WriteInteger("OptionType"+IntToStr(i), (int)option->Type );
							regKey.WriteInteger("OptionSize"+IntToStr(i), option->Size);
							regKey.WriteBinaryData("OptionValue"+IntToStr(i), option->Value, option->Size);
						}
					}
				}
			}
		}
		catch(Exception &e)	{}
	}
	__finally
	{
		regKey.CloseKey();
		delete &regKey;
	}
}
//---------------------------------------------------------------------------

bool __fastcall TPlugInObject::LoadSettingsByGUID(AnsiString aGUID)
{
	TRegistry& regKey=*new TRegistry();
	AnsiString regkey=_RadarMapRegKey+(AnsiString)_PlugInsRegKey;
	TOption *option;

	if(aGUID=="") aGUID=guid;
	try
	{
		try
		{
			if(aGUID!="" && regKey.OpenKey(regkey+"\\"+aGUID, false))
			{
				if(regKey.ValueExists("Type")) plugintype=(TPlugInType)regKey.ReadInteger("Type");
				if(regKey.ValueExists("DLLFileName")) dllname=regKey.ReadString("DLLFileName");
				if(regKey.ValueExists("PlugInID")) pluginid=regKey.ReadInteger("PlugInID");
				if(regKey.ValueExists("Name")) name=regKey.ReadString("Name");
				if(regKey.ValueExists("OptionsCount")) optionscount=regKey.ReadInteger("OptionsCount");
				LoadLocalParameters(&regKey);
				regKey.CloseKey();
				if(optionscount>0 && regKey.OpenKey(regkey+"\\"+aGUID+"\\Options", false))
				{
					HINSTANCE hInst=LoadLibrary(dllname.c_str());
					if(hInst)
					{
						try
						{
							GetOptions(hInst);
						}
						__finally
						{
							if(!FreeLibrary(hInst)) UnmapViewOfFile(hInst);
                        }
					}
					for(int i=0; i<options->Count; i++)
					{
						option=(TOption*)options->Items[i];
						if(option)
						{
							if(regKey.ValueExists("OptionType"+IntToStr(i)) &&
								option->Type==(TOptionType)regKey.ReadInteger("OptionType"+IntToStr(i)))
							{
								option->Size=regKey.ReadInteger("OptionSize"+IntToStr(i));
								regKey.ReadBinaryData("OptionValue"+IntToStr(i), option->Value, option->Size);
							}
						}
					}
					SetOptions();
				}
			}
		}
		catch(Exception &e)	{}
	}
	__finally
	{
		regKey.CloseKey();
		delete &regKey;
	}
	return true;
}

//---------------------------------------------------------------------------
// TPlugInCoordinateSystem
//---------------------------------------------------------------------------
__fastcall TPlugInCoordinateSystem::TPlugInCoordinateSystem(AnsiString aGUID, AnsiString aDllName,
	int aPlugInID, AnsiString aName, int aOptionsCount, AnsiString aLLA2XYH_Name,
	AnsiString aXYH2LLA_Name, AnsiString aUnitVector_Name)
	: TPlugInObject(aGUID, pitCRS, aDllName, aPlugInID, aName, aOptionsCount)
{
	pLatLonAltToXYH=pXYHToLatLonAlt=NULL;
	pUnitVector=NULL;
	prefix=code=ellipsoidname=epsg=proj4="";
	dimension=csdMeters;
	sLatLonAltToXYH=aLLA2XYH_Name;
	sXYHToLatLonAlt=aXYH2LLA_Name;
	sUnitVector=aUnitVector_Name;
}
//---------------------------------------------------------------------------

bool __fastcall TPlugInCoordinateSystem::Initialize()
{
	bool res;

	if(TPlugInObject::Initialize())
	{
		try
		{
			pLatLonAltToXYH=(T3DPointConvert)GetProcAddress(hInstance, sLatLonAltToXYH.c_str());
			pXYHToLatLonAlt=(T3DPointConvert)GetProcAddress(hInstance, sXYHToLatLonAlt.c_str());
			pUnitVector=(TUnitVector)GetProcAddress(hInstance, sUnitVector.c_str());
		}
		catch(Exception &e)
		{
			pLatLonAltToXYH=pXYHToLatLonAlt=NULL;
			pUnitVector=NULL;
		}
		res=(pLatLonAltToXYH && pXYHToLatLonAlt);
	}
	else res=false;

	return res;
}
//---------------------------------------------------------------------------

void __fastcall TPlugInCoordinateSystem::Free()
{
	pLatLonAltToXYH=pXYHToLatLonAlt=NULL;
	pUnitVector=NULL;
	TPlugInObject::Free();
}
//---------------------------------------------------------------------------

void __fastcall TPlugInCoordinateSystem::SaveLocalParameters(TRegistry *regKey)
{
	if(regKey && regKey->CurrentPath.Pos(_PlugInsRegKey)>0)
	{
		regKey->WriteString("sLatLonAltToXYH", sLatLonAltToXYH);
		regKey->WriteString("sXYHToLatLonAlt", sXYHToLatLonAlt);
		regKey->WriteString("sUnitVector", sUnitVector);
		regKey->WriteBinaryData("LatLonBounds", &latlonbounds, sizeof(TDoubleRect));
		regKey->WriteBinaryData("XYBounds", &xybounds, sizeof(TDoubleRect));
		regKey->WriteString("Prefix", prefix);
		//regKey->WriteString("Name", name);
		regKey->WriteString("Code", code);
		regKey->WriteString("EllipsoidName", ellipsoidname);
		regKey->WriteString("EPSG", epsg);
		regKey->WriteString("PROJ4", proj4);
		regKey->WriteInteger("Dimension", dimension);
	}
}
//---------------------------------------------------------------------------

void __fastcall TPlugInCoordinateSystem::LoadLocalParameters(TRegistry *regKey)
{
	if(regKey && regKey->CurrentPath.Pos(_PlugInsRegKey)>0)
	{
		if(regKey->ValueExists("sLatLonAltToXYH")) sLatLonAltToXYH=regKey->ReadString("sLatLonAltToXYH");
		if(regKey->ValueExists("sXYHToLatLonAlt")) sXYHToLatLonAlt=regKey->ReadString("sXYHToLatLonAlt");
		if(regKey->ValueExists("sUnitVector")) sUnitVector=regKey->ReadString("sUnitVector");
		if(regKey->ValueExists("LatLonBounds")) regKey->ReadBinaryData("LatLonBounds", &latlonbounds, sizeof(TDoubleRect));
		if(regKey->ValueExists("XYBounds")) regKey->ReadBinaryData("XYBounds", &xybounds, sizeof(TDoubleRect));
		if(regKey->ValueExists("Prefix")) prefix=regKey->ReadString("Prefix");
		//if(regKey->ValueExists("Name")) name=regKey->ReadString("Name");
		if(regKey->ValueExists("Code")) code=regKey->ReadString("Code");
		if(regKey->ValueExists("EllipsoidName")) ellipsoidname=regKey->ReadString("EllipsoidName");
		if(regKey->ValueExists("EPSG")) epsg=regKey->ReadString("EPSG");
		if(regKey->ValueExists("PROJ4")) proj4=regKey->ReadString("PROJ4");
		if(regKey->ValueExists("Dimension")) dimension=(TCoordinateSystemDimension)regKey->ReadInteger("Dimension");
	}
}
//---------------------------------------------------------------------------

void __fastcall TPlugInCoordinateSystem::SetSettings(const char *PlugInInfo)
{
	TCRSItem crs_info=*((TCRSItem*)PlugInInfo);
	if(strncmp(crs_info.GUID, GUID.c_str(), GUID_Length)==0)
	{
		sLatLonAltToXYH=(AnsiString)crs_info.LatLonAltToXYHFunctionName;
		sXYHToLatLonAlt=(AnsiString)crs_info.XYHToLatLonAltFunctionName;
		sUnitVector=(AnsiString)crs_info.UnitVectorFunctionName;
		latlonbounds=TDoubleRect(crs_info.LatLonBounds.left, crs_info.LatLonBounds.top,
			crs_info.LatLonBounds.right, crs_info.LatLonBounds.bottom);
		xybounds=TDoubleRect(crs_info.XYBounds.left, crs_info.XYBounds.top,
			crs_info.XYBounds.right, crs_info.XYBounds.bottom);
		prefix=(AnsiString)crs_info.Prefix;
		name=(AnsiString)crs_info.Name;
		code=(AnsiString)crs_info.Code;
		ellipsoidname=(AnsiString)crs_info.EllipsoidName;
		epsg=(AnsiString)crs_info.EPSG;
		proj4=(AnsiString)crs_info.PROJ4str;
		dimension=crs_info.Dimension;
	}
}
//---------------------------------------------------------------------------

void __fastcall TPlugInCoordinateSystem::UpdatePlugInInfo()
{
	TCRSItem crs_info;

	if(pGetPlugInInfo )
	{
		pGetPlugInInfo(PlugInID, (char*)(&crs_info), sizeof(crs_info));
		if(strncmp(crs_info.GUID, GUID.c_str(), GUID_Length)==0)
		{
			sLatLonAltToXYH=(AnsiString)crs_info.LatLonAltToXYHFunctionName;
			sXYHToLatLonAlt=(AnsiString)crs_info.XYHToLatLonAltFunctionName;
			sUnitVector=(AnsiString)crs_info.UnitVectorFunctionName;
			latlonbounds=TDoubleRect(crs_info.LatLonBounds.left, crs_info.LatLonBounds.top,
				crs_info.LatLonBounds.right, crs_info.LatLonBounds.bottom);
			xybounds=TDoubleRect(crs_info.XYBounds.left, crs_info.XYBounds.top,
				crs_info.XYBounds.right, crs_info.XYBounds.bottom);
			prefix=(AnsiString)crs_info.Prefix;
			name=(AnsiString)crs_info.Name;
			code=(AnsiString)crs_info.Code;
			ellipsoidname=(AnsiString)crs_info.EllipsoidName;
			epsg=(AnsiString)crs_info.EPSG;
			proj4=(AnsiString)crs_info.PROJ4str;
			dimension=crs_info.Dimension;
		}
	}
}
//---------------------------------------------------------------------------

bool __fastcall TPlugInCoordinateSystem::writePlugInStrOption(int id, AnsiString value)
{
	TOption* option;
	bool res=false;

	try
	{
		if(!Active) Initialize();
		if(hInstance && OptionsCount>id && pSetOption)
		{
			option=(TOption*)Options[id];
			if(option)
			{
				sprintf(option->Value, value.c_str());
				pSetOption(PlugInID, id, (char*)option, sizeof(TOption));
				res=true;
			}
			UpdatePlugInInfo();
		}
	}
	catch(...) {res=false;}

	return res;
}
//---------------------------------------------------------------------------

void __fastcall TPlugInCoordinateSystem::writeDimension(TCoordinateSystemDimension value)
{
	TOption* option;

	try
	{
        if(!Active) Initialize();
		if(hInstance && OptionsCount>_proj4OptionDefinition && pSetOption)
		{
			option=(TOption*)Options[_proj4OptionDefinition];
			if(option)
			{
				*((int*)option->Value)=(int)value;
				pSetOption(PlugInID, _proj4OptionDefinition, (char*)option, sizeof(TOption));
				dimension=value;
			}
			UpdatePlugInInfo();
		}
	}
	catch(...) {}
}

//---------------------------------------------------------------------------
// TPlugInPositioning
//---------------------------------------------------------------------------
__fastcall TPlugInPositioning::TPlugInPositioning(AnsiString aGUID, AnsiString aDllName,
	int aPlugInID, AnsiString aName, int aOptionsCount,
	AnsiString aStart_Name, AnsiString aStop_Name,
	AnsiString aGetPassedDistance_Name, AnsiString aGetCorrectedXYH)
	: TPlugInObject(aGUID, pitPos, aDllName, aPlugInID, aName, aOptionsCount)
{
	pStartAcquisition=NULL;
	pStopAcquisition=NULL;
	pGetPassedDistance=NULL;
	pGetCorrectedXYH=NULL;
	sStartAcquisition=aStart_Name;
	sStopAcquisition=aStop_Name;
	sGetPassedDistance=aGetPassedDistance_Name;
	sGetCorrectedXYH=aGetCorrectedXYH;
}
//---------------------------------------------------------------------------

bool __fastcall TPlugInPositioning::Initialize()
{
	bool res;

	if(TPlugInObject::Initialize())
	{
		try
		{
			pStartAcquisition=(TStartAcquisition)GetProcAddress(hInstance, sStartAcquisition.c_str());
			pStopAcquisition=(TVoidProcedure)GetProcAddress(hInstance, sStopAcquisition.c_str());
			pGetPassedDistance=(TGetPassedDistance)GetProcAddress(hInstance, sGetPassedDistance.c_str());
			pGetCorrectedXYH=(TGetCorrectedXYH)GetProcAddress(hInstance, sGetCorrectedXYH.c_str());
		}
		catch(Exception &e)
		{
			pStartAcquisition=NULL;
			pStopAcquisition=NULL;
			pGetPassedDistance=NULL;
			pGetCorrectedXYH=NULL;
		}
		res=(pStartAcquisition && pStopAcquisition && pGetPassedDistance && pGetCorrectedXYH);
	}
	else res=false;

	return res;
}

void __fastcall TPlugInPositioning::SaveLocalParameters(TRegistry *regKey)
{
	if(regKey && regKey->CurrentPath.Pos(_PlugInsRegKey)>0)
	{
		regKey->WriteString("sStartAcquisition", sStartAcquisition);
		regKey->WriteString("sStopAcquisition", sStopAcquisition);
		regKey->WriteString("sGetPassedDistance", sGetPassedDistance);
		regKey->WriteString("sGetCorrectedXYH", sGetCorrectedXYH);
	}
}
//---------------------------------------------------------------------------

void __fastcall TPlugInPositioning::LoadLocalParameters(TRegistry *regKey)
{
	if(regKey && regKey->CurrentPath.Pos(_PlugInsRegKey)>0)
	{
		if(regKey->ValueExists("sStartAcquisition")) sStartAcquisition=regKey->ReadString("sStartAcquisition");
		if(regKey->ValueExists("sStopAcquisition")) sStopAcquisition=regKey->ReadString("sStopAcquisition");
		if(regKey->ValueExists("sGetPassedDistance")) sGetPassedDistance=regKey->ReadString("sGetPassedDistance");
		if(regKey->ValueExists("sGetCorrectedXYH")) sGetCorrectedXYH=regKey->ReadString("sGetCorrectedXYH");
	}
}
//---------------------------------------------------------------------------

void __fastcall TPlugInPositioning::SetSettings(const char *PlugInInfo)
{
	TPosItem pos_info=*((TPosItem*)PlugInInfo);
	if(strncmp(pos_info.GUID, GUID.c_str(), GUID_Length)==0)
	{
		sStartAcquisition=(AnsiString)pos_info.StartAcquisitionFunctionName;
		sStopAcquisition=(AnsiString)pos_info.StopAcquisitionFunctionName;
		sGetPassedDistance=(AnsiString)pos_info.GetPassedDistanceFunctionName;
		sGetCorrectedXYH=(AnsiString)pos_info.GetCorrectedXYHFunctionName;
	}
}

//---------------------------------------------------------------------------
// TPlugInObjectsList
//---------------------------------------------------------------------------
TPlugInObject* __fastcall TPlugInObjectsList::GetByGUID(AnsiString guid, TPlugInType type)
{
	TPlugInObject* res=NULL;

	for(int i=0; i<Count; i++)
	{
		if(Items[i] && Items[i]->GUID==guid && (type==pitUnknown || Items[i]->Type==type))
		{
			res=Items[i];
			break;
		}
	}
	return res;
}
//---------------------------------------------------------------------------

void __fastcall TPlugInObjectsList::DeleteByGUID(AnsiString guid, bool DeleteFromHard)
{
	AnsiString active="Active";
	for(int i=0; i<Count; i++)
	{
		if(Items[i] && Items[i]->GUID==guid)
		{
			if(Owner)
			{
				switch(Items[i]->Type)
				{
					case pitCRS:
						if(((TPlugInManager*)Owner)->ActiveCS==(TPlugInCoordinateSystem*)Items[i])
							((TPlugInManager*)Owner)->ClearActive(pitCRS);
						active="ActiveCS";
						break;
					case pitPos:
						if(((TPlugInManager*)Owner)->ActivePos==(TPlugInPositioning*)Items[i])
							((TPlugInManager*)Owner)->ClearActive(pitPos);
						active="ActivePos";
						break;
				}
			}
			TRegistry& regKey=*new TRegistry();
			AnsiString regkey=_RadarMapRegKey+(AnsiString)_PlugInsRegKey;
			try
			{
				if(regKey.KeyExists(regkey+"\\"+guid))
				{
					regKey.DeleteKey(regkey+"\\"+guid);
				}
				if(regKey.OpenKey(regkey, false))
				{
					if(regKey.ValueExists(active) &&
						regKey.ReadString(active)==guid)
							regKey.DeleteValue(active);
				}
			}
			__finally
			{
				delete &regKey;
			}
			if(DeleteFromHard) DeleteFile(Items[i]->DLLFileName);
			Delete(i); //deletes from list and frees the memory occupied by item
			break;
		}
	}
}

//---------------------------------------------------------------------------
// TPlugInManager
//---------------------------------------------------------------------------
__fastcall TPlugInManager::TPlugInManager() : TMyObject()
{
	PlugIns=new TPlugInObjectsList(this);
	//activeCS=(TPlugInCoordinateSystem*)PlugIns->Items[AddPlugIn("..\\..\\..\\CoordinateDLL\\Debug\\DHDN_Coordinate.dll", 4)];
	activeCS=NULL;
	activePos=NULL;
	LoadSettings();
}
//---------------------------------------------------------------------------

__fastcall TPlugInManager::~TPlugInManager()
{
	//SaveSettings();
	delete PlugIns;
}
//---------------------------------------------------------------------------

int __fastcall TPlugInManager::GetPlugInsCountInLibrary(AnsiString FileName)
{
	int (*GetCount) (void);
	HINSTANCE hInstance;
	int res=0;

	hInstance=LoadLibrary(FileName.c_str());
	if(hInstance)
	{
		try
		{
			GetCount=(int (*)(void))GetProcAddress(hInstance, "_GetPlugInsCount");
			if(GetCount && CheckLibrarySignature(hInstance))
			{
				res=GetCount();
			}
		}
		__finally
		{
			if(!FreeLibrary(hInstance)) UnmapViewOfFile(hInstance);
		}
	}
	return res;
}
//---------------------------------------------------------------------------

int __fastcall TPlugInManager::GetPlugInsListInLibrary(AnsiString FileName,
	TPlugInListItem *Items, int Count, TPlugInType NeededType)
{
	int (*GetCount) (void);
	int (*GetPlugInType) (int);
	void (*GetPlugInName) (int, char*, int);
	void (*GetPlugInGUID) (int, char*, int);
	bool (*GetPlugInInfo) (int, char*, int);
	void (*GetAbout) (char*, char*, char*);
	HINSTANCE hInstance;
	int c, i, j;
	//TCRSItem crs_info;
	TPlugInType pit;
	char guid[GUID_Length+1], name[PlugInNameLength+1];
	char About[256], Version[32], Description[256];

	i=j=0;
	hInstance=LoadLibrary(FileName.c_str());
	if(hInstance)
	{
		try
		{
			GetCount=(int (*)(void))GetProcAddress(hInstance, "_GetPlugInsCount");
			GetPlugInType=(int (*) (int))GetProcAddress(hInstance, "_GetPlugInType");
			GetPlugInName=(void (*) (int, char*, int))GetProcAddress(hInstance, "_GetPlugInName");
			GetPlugInGUID=(void (*) (int, char*, int))GetProcAddress(hInstance, "_GetPlugInGUID");
			GetPlugInInfo=(bool (*) (int, char*, int))GetProcAddress(hInstance, "_GetPlugInInfo");
			GetAbout=(void (*) (char*, char*, char*))GetProcAddress(hInstance, "_GetAbout");
			if(GetCount && GetPlugInName && GetPlugInGUID && GetPlugInInfo && GetPlugInType && GetAbout &&
				CheckLibrarySignature(hInstance))//
			{
				GetAbout(About, Version, Description);
				c=GetCount();
				if(c<Count) Count=c;
				while(c>0)
				{
					pit=(TPlugInType)GetPlugInType(j);
					if(NeededType==pitUnknown || NeededType==pit)
					{
						Items[i].DLLName=FileName;
						GetPlugInGUID(j, guid, GUID_Length);
						guid[GUID_Length]=0;
						Items[i].GUID=(AnsiString)guid;
						Items[i].ID=j;
						Items[i].Type=pit;
						GetPlugInName(j, name, PlugInNameLength);
                        name[PlugInNameLength]=0;
						Items[i].Name=name;
						/*switch(pit)
						{
							case pitCRS:
								if(GetPlugInInfo(j, (char*)(&crs_info), sizeof(crs_info)))
									Items[i].Name=(AnsiString)crs_info.Name;
								break;
						}*/
						Items[i].About=About;
						Items[i].Version=Version;
						Items[i].Description=Description;
						i++;
					}
					j++;
					c--;
				}

			}
		}
		__finally
		{
			if(!FreeLibrary(hInstance)) UnmapViewOfFile(hInstance);
		}
	}
	return i;
}
//---------------------------------------------------------------------------

void __fastcall TPlugInManager::SaveSettings()
{
	TRegistry& regKey=*new TRegistry();
	AnsiString regkey=_RadarMapRegKey+(AnsiString)_PlugInsRegKey;

	try
	{
		try
		{
            regKey.DeleteKey(regkey);
			for(int i=0; i<PlugIns->Count; i++)
			{
				if(PlugIns->Items[i]) PlugIns->Items[i]->SaveSettings();
			}
			if(regKey.OpenKey(regkey, true))
			{
				if(ActiveCS && ActiveCS->GUID!="") regKey.WriteString("ActiveCS", ActiveCS->GUID);
				if(ActivePos && ActivePos->GUID!="") regKey.WriteString("ActivePos", ActivePos->GUID);
			}
		}
		catch(Exception &e)	{}
	}
	__finally
	{
		regKey.CloseKey();
		delete &regKey;
	}
}
//---------------------------------------------------------------------------

void __fastcall TPlugInManager::LoadSettings()
{

	AnsiString guid, regkey=_RadarMapRegKey+(AnsiString)_PlugInsRegKey;
	TPlugInObject *plugin;
	TStrings *strs;
	TPlugInType plugintype;
	TRegistry& regKey=*new TRegistry();
	TRegistry& regKey2=*new TRegistry();

	strs=new TStringList();
	try
	{
		try
		{
			if(regKey.OpenKey(regkey, true))
			{
				regKey.GetKeyNames(strs);
				if(strs->Count>0)
				{
					for(int i=0; i<strs->Count; i++)
					{
						plugin=NULL;
						if(IsValidGUID(strs->Strings[i]) && regKey2.OpenKey(regkey+"\\"+strs->Strings[i], false))
						{
							try
							{
								try
								{
									if(regKey2.ValueExists("Type") && regKey2.ValueExists("DLLFileName") &&
										FileExists(regKey2.ReadString("DLLFileName")))
										plugintype=(TPlugInType)regKey2.ReadInteger("Type");
									else plugintype=pitUnknown;
									switch(plugintype)
									{
										case pitCRS:
											plugin=(TPlugInObject*)(new TPlugInCoordinateSystem(strs->Strings[i]));
											break;
										case pitPos:
											plugin=(TPlugInObject*)(new TPlugInPositioning(strs->Strings[i]));
											break;
										default: plugin=NULL;
									}
									if(plugin)
									{
										plugin->LoadSettings();
										PlugIns->Add(plugin);
									}
								}
								catch(Exception &e)
								{
									if(plugin) delete plugin;
								}
							}
							__finally
							{
								regKey2.CloseKey();
							}
						}
					}
					if(regKey.ValueExists("ActiveCS"))
					{
						guid=regKey.ReadString("ActiveCS");
						activeCS=(TPlugInCoordinateSystem*)PlugIns->GetByGUID(guid, pitCRS);
					}
					if(regKey.ValueExists("ActivePos"))
					{
						guid=regKey.ReadString("ActivePos");
						activePos=(TPlugInPositioning*)PlugIns->GetByGUID(guid, pitPos);
					}
				}
			}
		}
		catch(Exception &e)	{}
	}
	__finally
	{
		delete strs;
		regKey.CloseKey();
		delete &regKey;
	}
}
//---------------------------------------------------------------------------

int __fastcall TPlugInManager::AddPlugIn(AnsiString FileName, int Id)
{
	int (*GetCount) (void);
	int (*GetPlugInType) (int);
	void (*GetPlugInName) (int, char*, int);
	void (*GetPlugInGUID) (int, char*, int);
	bool (*GetPlugInInfo) (int, char*, int);
	HINSTANCE hInstance;
	char guid[GUID_Length+1], name[PlugInNameLength+1];
	TCRSItem crs_info;
	TPosItem pos_info;
	TPlugInObject *pi;
	char *piInfo;
	TPlugInType pit;
	int res=-1;

	if(Id>=0)
	{
		hInstance=LoadLibrary(FileName.c_str());
		if(hInstance)
		{
			try
			{
				GetCount=(int (*)(void))GetProcAddress(hInstance, "_GetPlugInsCount");
				GetPlugInType=(int (*) (int))GetProcAddress(hInstance, "_GetPlugInType");
				GetPlugInName=(void (*) (int, char*, int))GetProcAddress(hInstance, "_GetPlugInName");
				GetPlugInGUID=(void (*) (int, char*, int))GetProcAddress(hInstance, "_GetPlugInGUID");
				GetPlugInInfo=(bool (*) (int, char*, int))GetProcAddress(hInstance, "_GetPlugInInfo");
				if(GetCount && GetPlugInName && GetPlugInInfo && GetPlugInGUID && GetPlugInInfo && GetPlugInType &&
					CheckLibrarySignature(hInstance))//
				{
					if(Id<GetCount())
					{
						pit=(TPlugInType)GetPlugInType(Id);
						GetPlugInGUID(Id, guid, GUID_Length);
						guid[GUID_Length]=0;
						GetPlugInName(Id, name, PlugInNameLength);
						name[GUID_Length]=0;
						switch(pit)
						{
							case pitCRS:
								if(GetPlugInInfo(Id, (char*)(&crs_info), sizeof(crs_info)))
								{
								//strcpy(Items[i].Name, crs_info.Name);
									pi=(TPlugInObject*)(new TPlugInCoordinateSystem((AnsiString)guid, FileName, Id, name, crs_info.OptionsCount,
										(AnsiString)crs_info.LatLonAltToXYHFunctionName,
										(AnsiString)crs_info.XYHToLatLonAltFunctionName));
									piInfo=(char*)(&crs_info);
								}
								else
								{
									pi=NULL;
									piInfo=NULL;
								}
								break;
							case pitPos:
								if(GetPlugInInfo(Id, (char*)(&pos_info), sizeof(pos_info)))
								{
								//strcpy(Items[i].Name, crs_info.Name);
									pi=(TPlugInObject*)(new TPlugInPositioning((AnsiString)guid, FileName, Id, name, pos_info.OptionsCount,
										(AnsiString)pos_info.StartAcquisitionFunctionName,
										(AnsiString)pos_info.StopAcquisitionFunctionName,
										(AnsiString)pos_info.GetPassedDistanceFunctionName,
										(AnsiString)pos_info.GetCorrectedXYHFunctionName));
									piInfo=(char*)(&pos_info);
								}
								else
								{
									pi=NULL;
									piInfo=NULL;
								}
								break;
							default:
								pi=NULL;
								piInfo=NULL;
						}
						if(pi && piInfo)
						{
							pi->SetSettings(piInfo);
							res=PlugIns->Add(pi);
							pi->GetOptions(hInstance);
                        }
					}

				}
			}
			__finally
			{
				if(!FreeLibrary(hInstance)) UnmapViewOfFile(hInstance);
			}
		}
	}
	return res;
}
//---------------------------------------------------------------------------

void __fastcall TPlugInManager::SetActive(AnsiString guid, TPlugInType type)
{
	for(int i=0; i<PlugIns->Count; i++)
	{
		if(PlugIns->Items[i] && PlugIns->Items[i]->GUID==guid && PlugIns->Items[i]->Type==type)
		{
			switch((int)type)
			{
				case pitCRS: activeCS=(TPlugInCoordinateSystem*)PlugIns->Items[i]; break;
				case pitPos: activePos=(TPlugInPositioning*)PlugIns->Items[i]; break;
			}
			break;
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TPlugInManager::ClearActive(TPlugInType type)
{
	switch((int)type)
	{
		case pitCRS: activeCS=NULL; break;
		case pitPos: activePos=NULL; break;
	}
}
//---------------------------------------------------------------------------
