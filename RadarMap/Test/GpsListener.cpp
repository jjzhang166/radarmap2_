//---------------------------------------------------------------------------


#pragma hdrstop

#include "GpsListener.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

//---------------------------------------------------------------------------
// TGpsListenerCommand
//---------------------------------------------------------------------------
AnsiString __fastcall TGpsListenerCommand::readCommand()
{
	AnsiString res, name;
	TGpsListenerCommandParam *param;
	int i, j;

	res=command;
	if(Params && Params->Count>0)
	{
		for(j=0; j<Params->Count; j++)
		{
			param=(TGpsListenerCommandParam*)Params->Items[j];
			if(param && param->Name!="")
			{
				name="$"+param->Name+"$";
				i=res.Pos(name);
				if(i>=0) res=res.SubString(0, i-1)+param->Value+res.SubString(i+name.Length(), res.Length()-(i+name.Length())+1);
			}
		}
	}

	return res;
}
