//---------------------------------------------------------------------------

#ifndef PathsH
#define PathsH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <ImageButton.h>
#include <pngimage.hpp>
#include <ComCtrls.hpp>
#include <ImgList.hpp>
#include <GR32_Image.hpp>"
#include <Graphics.hpp>
#include "Manager.h"
#include <Dialogs.hpp>
#include <ComCtrls.hpp>
#include <ImgList.hpp>
#ifdef _RAD101
	#include <System.ImageList.hpp>
#endif
//---------------------------------------------------------------------------
class TPathsForm : public TForm
{
__published:	// IDE-managed Components
	TPanel *Panel1;
	TPanel *Panel2;
	TPanel *Panel3;
	TPanel *Panel4;
	TPanel *TitlePanel;
	TPanel *Panel5;
	TPanel *Panel6;
	TPanel *Panel7;
	TPanel *TabsPanel;
	TPanel *PagesPanel;
	TPanel *OkCancelPanel;
	TLabel *Label1;
	TPanel *Panel8;
	TPanel *Panel9;
	TImageButton *CloseIButton;
	TImageButton *OkImageButton;
	TImageButton *InfoBtn;
	TImageButton *AddBtn;
	TImageButton *DelBtn;
	TImageButton *GainBtn;
	TEdit *TempEdit;
	TTreeView *TreeView;
	TImageList *ImageList1;
	TImageButton *DetectBtn;
	void __fastcall CloseIButtonClick(TObject *Sender);
	void __fastcall InfoBtnClick(TObject *Sender);
	void __fastcall OkImageButtonClick(TObject *Sender);
	void __fastcall FormHide(TObject *Sender);
	void __fastcall TreeViewChange(TObject *Sender, TTreeNode *Node);
	void __fastcall DelBtnClick(TObject *Sender);
	void __fastcall AddBtnClick(TObject *Sender);
	void __fastcall GainBtnClick(TObject *Sender);
	void __fastcall TitlePanelMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
	void __fastcall TitlePanelMouseMove(TObject *Sender, TShiftState Shift, int X, int Y);
	void __fastcall TitlePanelMouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
	void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
	void __fastcall DetectBtnClick(TObject *Sender);

private:	// User declarations
	TProfileSatellitesList *Satellites;
	TRadarMapManager *Manager;
	TTreeNode *Root;
	bool TitleDown;
	Types::TPoint TitleXY;
public:		// User declarations
	__fastcall TPathsForm(TComponent* Owner, TRadarMapManager *AManager);
	__fastcall ~TPathsForm() {}
};
//---------------------------------------------------------------------------
extern PACKAGE TPathsForm *PathsForm;
//---------------------------------------------------------------------------
#endif
