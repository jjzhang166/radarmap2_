//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Path.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "ImageButton"
#pragma link "G32_ProgressBar"
#pragma link "GR32_Image"
#pragma resource "*.dfm"
TPathsForm *PathsForm;

//---------------------------------------------------------------------------
__fastcall TPathsForm::TPathsForm(TComponent* Owner, TRadarMapManager *AManager)
	: TForm(Owner)
{
	TTreeNode *node=NULL, *node2=NULL;

	Manager=AManager;
	Root=NULL;
	try
	{
		if(Manager && Manager->Satellites)
		{
			Satellites=AManager->Satellites;
			TreeView->Items->Clear();
			Root=TreeView->Items->Add(NULL, "Project");
			Root->ImageIndex=0;
			Root->Data=NULL;
			for(int i=0; i<Satellites->Count; i++)
			{
				if(Satellites->Items[i] && Satellites->Items[i]->Path && Satellites->Items[i]->OutputView &&
					Satellites->Items[i]->OutputView->Profile)
				{
					node=TreeView->Items->AddChild(Root, Satellites->Items[i]->Path->Name);
					node->Data=(void*)Satellites->Items[i];
					if(Satellites->Items[i]->ProfilesCount>1)
					{
						node->ImageIndex=3;
						// Add subnodes as profiles...
						for(int j=0; j<Satellites->Items[i]->ProfilesCount; j++)
						{
							node2=TreeView->Items->AddChild(node, Satellites->Items[i]->Profiles[j]->Name);
							node2->ImageIndex=1;
							//node2->Data=(void*)Satellites->Items[i];
							node2->Data=NULL;
							node2->SelectedIndex=node2->ImageIndex+1;
						}
					}
					else node->ImageIndex=1;
					node->SelectedIndex=node->ImageIndex+1;
				}
			}
			if(node) node->Selected=true;
			Root->Expand(true);
			//TreeView->
		}
	}
	catch(Exception &e)
	{
		TreeView->Items->Clear();
	}
	TitleDown=false;
}
//---------------------------------------------------------------------------

void __fastcall TPathsForm::CloseIButtonClick(TObject *Sender)
{
	Close();
}

//---------------------------------------------------------------------------
void __fastcall TPathsForm::InfoBtnClick(TObject *Sender)
{
/*	int z =((TComponent*)Sender)->Owner->ComponentCount;

	for(int i = 0; i < z; i++)
	{
		if(((TComponent *)Sender)->Owner->Components[i]->ClassName()=="TImageButton" && ((TComponent*)Sender)->Owner->Components[i]!=Sender)
			((TImageButton*)((TComponent *)Sender)->Owner->Components[i])->Down=false;
	}
	((TImageButton*)Sender)->Down=true;
	Notebook1->PageIndex=((TImageButton*)Sender)->Tag;*/
	try
	{
		if(Manager && Manager->Satellites)
		{
			if(Manager->Satellites->GeneratedInfo=="")
				Manager->Satellites->GenerateInfo();
			InfoForm->GeneratedInfo=Manager->Satellites->GeneratedInfo;
			InfoForm->BriefInfo=Manager->Satellites->BriefInfo;
			if(InfoForm->ShowModal()==mrOk)
			{
				Manager->Satellites->BriefInfo=InfoForm->BriefInfo;
				Manager->ResultsNotSaved=true;
			}
		}
	}
	catch(Exception &e) {}
}
//---------------------------------------------------------------------------

void __fastcall TPathsForm::FormShow(TObject *Sender)
{
	int z =ComponentCount;

	for(int i = 0; i < z; i++)
	{
		if(Components[i]->ClassName()=="TImageButton")
		{
			if(((TImageButton*)Components[i])->Tag==Notebook1->PageIndex)
				((TImageButton*)Components[i])->Down=true;
			else ((TImageButton*)Components[i])->Down=false;
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TPathsForm::ImageButton7Click(TObject *Sender)
{
	int z =((TComponent*)Sender)->Owner->ComponentCount;

	for(int i = 0; i < z; i++)
	{
		if(((TComponent *)Sender)->Owner->Components[i]->ClassName()=="TImageButton" && ((TComponent*)Sender)->Owner->Components[i]!=Sender)
			((TImageButton*)((TComponent *)Sender)->Owner->Components[i])->Down=false;
	}
	((TImageButton*)Sender)->Down=true;

	int i=-1;
	try
	{
		if(label)
		{
			i=Application->MessageBox(L"Do you really want to delete this Label?",
				((UnicodeString)_PrismName).w_str(), MB_YESNO);
			if(i==IDYES) delete label;
		}
	}
	catch(Exception &e) {}
	if(i==IDYES) Close();
	else ImageButton3Click(ImageButton4);
}
//---------------------------------------------------------------------------

void __fastcall TPathsForm::OkImageButtonClick(TObject *Sender)
{
	if(Manager && Satellites && TreeView->Selected && TreeView->Selected->Data)
	{
		Manager->SwitchToPath(((TProfileSatellites*)TreeView->Selected->Data)->Path);
	}
	ModalResult=mrOk;
}
//---------------------------------------------------------------------------


void __fastcall TPathsForm::FormHide(TObject *Sender)
{
	TCloseAction b;

	FormClose(Sender, b);
}
//---------------------------------------------------------------------------




void __fastcall TPathsForm::TreeViewChange(TObject *Sender, TTreeNode *Node)
{
	DelBtn->Enabled=(Node && Node->Data);
	DelLabel->Enabled=DelBtn->Enabled;
	GainBtn->Enabled=DelBtn->Enabled;
	GainLabel->Enabled=DelBtn->Enabled;
}
//---------------------------------------------------------------------------

void __fastcall TPathsForm::DelBtnClick(TObject *Sender)
{
	bool prohibited=false;

	if(Manager && Satellites && TreeView->Selected && TreeView->Selected->Data)
	{
		for(int i=0; i<Manager->ReceivingProfilesQuantity; i++)
		{
			for(int j=0; j<((TProfileSatellites*)TreeView->Selected->Data)->ProfilesCount; j++)
				if(((TProfileSatellites*)TreeView->Selected->Data)->Profiles[j]==Manager->ReceivingProfiles[i])
				{
					prohibited=true;
					break;
				}
			if(prohibited) break;
		}
		if(!prohibited)
		{
			if(Application->MessageBox(("Are you sure you want to delete "+TreeView->Selected->Text+"?").w_str(),
				((UnicodeString)"Delete Path").w_str(), MB_ICONQUESTION | MB_YESNO)==IDYES)
			{
				Satellites->Delete((TProfileSatellites*)TreeView->Selected->Data);
				Manager->SwitchToAvailablePath();
				TreeView->Items->Delete(TreeView->Selected);
			}
		}
		else
		{
			ShowMessage("Cannot delete active path!");
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TPathsForm::AddBtnClick(TObject *Sender)
{
	TTreeNode *node=NULL, *node2=NULL;

	try
	{
		TPreviewForm* PreviewForm;
		TCursor cur;
		bool res=false;

		if(Manager)
		{
			cur=Cursor;
			PreviewForm=new TPreviewForm(this, (TObject*)Manager);
			try
			{
				//PreviewForm->GMLOpenDialog->FilterIndex=RadarMapManager->Settings->OpenMapType;
				PreviewForm->GMLOpenDialog->Filter="SEG-Y Profile (*.sgy)|*.sgy|All files (*.*|*.*";
				PreviewForm->GMLOpenDialog->FilterIndex=0;
				PreviewForm->GMLOpenDialog->Options=PreviewForm->GMLOpenDialog->Options << ofAllowMultiSelect;
				if(PreviewForm->GMLOpenDialog->Execute())
				{
					Cursor=crHourGlass;
					Manager->MapObjects->Container->Progress=Manager->MapProgress;//MapForm->MyProgress;
					for(int i=0; i<PreviewForm->GMLOpenDialog->Files->Count; i++)
					{
						try
						{
							res=Manager->LoadPathFromSGY(PreviewForm->GMLOpenDialog->Files->Strings[i]);
							if(res && Root)
							{

								node=TreeView->Items->AddChild(Root, Manager->Current->Path->Name);
								node->Data=(void*)Manager->Current;
								if(Manager->CurrentProfilesCount>1)
								{
									node->ImageIndex=3;
									// Add subnodes as profiles...
									for(int j=0; j<Manager->CurrentProfilesCount; j++)
									{
										node2=TreeView->Items->AddChild(Root, Manager->Current->Profiles[j]->Name);
										node2->ImageIndex=1;
										//node2->Data=(void*)Satellites->Items[i];
										node2->Data=NULL;
										node2->SelectedIndex=node2->ImageIndex+1;
									}
								}
								else node->ImageIndex=1;
								node->SelectedIndex=node->ImageIndex+1;
                            }
						}
						catch(Exception &e)
						{
							res=false;
						}
						if(!res)
						{
							Manager->ShowMessageObject(NULL, Manager->MapMessages,  TMessageType::mtWarning, mvtDisapering, 5000,
								"Cannot open file "+PreviewForm->GMLOpenDialog->Files->Strings[i]+"!", "Error");
							break;
						}
					}
					if(node) node->Selected=true;
					Root->Expand(true);
				}
			}
			__finally
			{
				delete PreviewForm;
				Cursor=cur;
			}
		}
	}
	catch(Exception &e) {}
}
//---------------------------------------------------------------------------

void __fastcall TPathsForm::GainBtnClick(TObject *Sender)
{
	bool prohibited=false;

	if(Manager && Satellites && TreeView->Selected && TreeView->Selected->Data)
	{
		for(int i=0; i<Manager->ReceivingProfilesQuantity; i++)
		{
			for(int j=0; j<((TProfileSatellites*)TreeView->Selected->Data)->ProfilesCount; j++)
				if(((TProfileSatellites*)TreeView->Selected->Data)->Profiles[j]==Manager->ReceivingProfiles[i])
				{
					prohibited=true;
					break;
				}
			if(prohibited) break;
		}
		if(!prohibited)
		{
			TProfile* Prof=((TProfileSatellites*)TreeView->Selected->Data)->Profiles[0];

			if(Prof && Prof->LastTracePtr) //Prof->CommonTrace)
			{
				TGainForm* GainForm;
				bool ToBeDrawed=false;

				GainForm=new TGainForm(this, (TObject*)Manager);
				try
				{
					GainForm->GainItems->GainFunction->CopyFromProfile(Prof);
					GainForm->GainItems->ReAssignGainPoints();
					GainForm->GainItems->SelectedGainItem=GainForm->GainItems->GainFunction->GainPointsCount-1;
					GainForm->GainItems->SingleTrace=Prof->LastTracePtr;//(TTrace*)Prof->CommonTrace;
					if(GainForm->ShowModal())
					{
						for(int i=0; i<((TProfileSatellites*)TreeView->Selected->Data)->ProfilesCount; i++)
						{
							Prof=((TProfileSatellites*)TreeView->Selected->Data)->Profiles[i];
							if(Prof) GainForm->GainItems->GainFunction->CopyToProfile(Prof, true);
							if(Prof==Manager->CurrentProfiles[i]) ToBeDrawed=true;
						}
						if(ToBeDrawed) Manager->SwitchToPath(((TProfileSatellites*)TreeView->Selected->Data)->Path);
					}
				}
				__finally
				{
                	delete GainForm;
                }
			}
			/*

	  Trace=Prof->FirstTracePtr;
	  while(Trace!=NULL)
	  {
		for(int i=0; i<Prof->Samples; i++)
		{
		  smpl=(float)Trace->SourceData[i]*Gain->GainFunction[i];
		  if(smpl>32767) Trace->Data[i]=32767.;
		  else if(smpl<-32768) Trace->Data[i]=-32768.;
		  else Trace->Data[i]=(short int)smpl;
		}
		Trace=Trace->PtrUp;
	  }                    */
		}
		else
		{
			ShowMessage("Cannot change active path's gain!");
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TPathsForm::TitlePanelMouseDown(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y)
{
	TitleDown=true;
	TitleXY=ClientToScreen(Types::TPoint(X, Y));
}
//---------------------------------------------------------------------------

void __fastcall TPathsForm::TitlePanelMouseMove(TObject *Sender, TShiftState Shift,
          int X, int Y)
{
	Types::TPoint tmp;

	if(TitleDown)
	{
		tmp=ClientToScreen(Types::TPoint(X, Y));
		Left+=tmp.x-TitleXY.x;
		Top+=tmp.y-TitleXY.y;
		TitleXY=tmp;
	}
}
//---------------------------------------------------------------------------

void __fastcall TPathsForm::TitlePanelMouseUp(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y)
{
	TitleDown=false;
}
//---------------------------------------------------------------------------

