//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "DrawProf.h"
#include "Radar.h"
#include "Profile.h"
#pragma package(smart_init)

//---------------------------------------------------------------------------

__fastcall TDrawProfThrd::TDrawProfThrd(bool CreateSuspended, TRadarMapManager *RMM, TImgView32 *AImg)
        : TThread(CreateSuspended)
{
	RadarMapManager=RMM;
	Image=AImg;
	Profile=NULL;
	ProfileIndex=-1;
	LastDrawed=NULL;
	LastProf=NULL;
	//TraceAccumulator=NULL;
}
//---------------------------------------------------------------------------

void __fastcall TDrawProfThrd::Execute()
{
	MarkList=new TMarkList;
	bbb=new TBitmap32();
	mmm=new TBitmap32();
	try
	{
		try
		{
			//bbb->BeginUpdate();
			//bbb->DrawMode=dmBlend;
			//bbb->StretchFilter=sfLinear;
			bbb->SetSize(RadarDialog->ClientWidth, RadarDialog->ClientHeight);
			mmm->SetSize(10, 10);
			//bbb->FillRect(0, 0, RadarDialog->ClientWidth, RadarDialog->ClientHeight, clWhite32);
			for(int i=0; i<_MaxProfQuan; i++)
			{
				_X[i]=RadarDialog->ProfRect[i]->Left;
				_MarkY[i]=RadarDialog->ProfRect[i]->Top+54;
				AfterScrolling[i]=false;
			}
			do
			{
				RadarDialog->DPC=__LINE__;
				if(RadarMapManager && RadarMapManager->Current)
				{
					for(int i=0; i<RadarMapManager->CurrentProfilesCount; i++)
					{
						RadarDialog->DPC=__LINE__;
						if(RadarMapManager->CurrentProfiles[i] && WaitForSingleObject(RadarMapManager->CurrentProfiles[i]->TracesAddedEvent, 0)==WAIT_OBJECT_0)
						{
							RadarDialog->DPC=__LINE__;
							if(bbb->Width!=RadarDialog->ClientWidth || bbb->Height!=RadarDialog->ClientHeight)
								bbb->SetSize(RadarDialog->ClientWidth, RadarDialog->ClientHeight);
							//ResetEvent(RadarDialog->EventDrawProfHndl);
							ProfileIndex=i;
							Profile=RadarMapManager->CurrentProfiles[i];
							RadarDialog->DPC=__LINE__;
							Synchronize(DrawTrace);
						}
					}
				}
				else
				{
					LastProf=NULL;
					LastDrawed=NULL;
				}
				MySleep(5);
			} while(!Terminated);//RadarDialog->Process==Running);
		}
		catch(Exception &e)
		{
			LogEvent(e.Message, "file "+(AnsiString)__FILE__+" line "+IntToStr(RadarDialog->DPC));
		}
	}
	__finally
	{
		delete bbb;
		delete mmm;
		delete MarkList;
	}
}
//---------------------------------------------------------------------------

void __fastcall TDrawProfThrd::DrawTrace(void)
{
	float s, k, j, t;
	int y, sm, h, p, tmp, r, g, b, lineX;
	TTrace *Trace, *Trace2, *Trace3;//, *LstTrc;
	TBitmapLayer *L;
	TColor c;
	TMarkRect *mr;
	TBitmap32* Bmp;
	TFont *font;

	try
	{
		RadarDialog->DPC=__LINE__;
		if(RadarMapManager && Image && Profile && ProfileIndex>=0 && RadarDialog->ProfRect[ProfileIndex])
		{
			Bmp=Image->Bitmap;
			Trace=NULL;
			RadarDialog->DPC=__LINE__;
			if(AfterScrolling[ProfileIndex] && WaitForSingleObject(Profile->OutputView->FinishScrollingEvents[ProfileIndex], 0)==WAIT_OBJECT_0)
			{
				ww=RadarDialog->ProfRect[ProfileIndex]->Width();//Profile->OutputView->Width();
				Profile->OutputView->RightTraces[ProfileIndex]=Profile->LastTracePtr;
				Profile->OutputView->LeftIndex=Profile->LastTracePtr->Index-ww;
				if(Profile->OutputView->LeftIndex>Profile->CurrentTracePtr->Index)
					Profile->OutputView->LeftIndex=Profile->CurrentTracePtr->Index;
				Trace=Profile->OutputView->LeftTraces[ProfileIndex];
				if(Trace!=NULL)
				{
					if(RadarMapManager->Settings->WheelPositioning!=TPositioning::ManualP)
						RadarDialog->ScaleB[ProfileIndex]->Min=((float)Trace->Index)*RadarMapManager->Settings->WheelTraceDistance;
					else RadarDialog->ScaleB[ProfileIndex]->Min=Trace->Index;
					RadarDialog->BottomScaleRefresh();
					//AfterScrolling[ProfileIndex]=true;
				}
				Trace2=Trace;
				while(Trace2!=NULL)
				{
					Trace2->Drawed=false;
					Trace2=Trace2->PtrUp;
				}
				SetEvent(Profile->OutputView->ClearOutputViewEvents[ProfileIndex]);
				RadarMapManager->MapObjects->Update(gotCustom);
				AfterScrolling[ProfileIndex]=false;
			}
			RadarDialog->DPC=__LINE__;
			if(WaitForSingleObject(Profile->OutputView->AfterScrollingEvents[ProfileIndex], 0)==WAIT_OBJECT_0)
			{
				Trace=Profile->OutputView->LeftTraces[ProfileIndex];
				if(Trace!=NULL)
				{
					if(Trace==Profile->FirstTracePtr && Trace->Backward) Trace->Backward=false;
					if(RadarMapManager->Settings->WheelPositioning!=TPositioning::ManualP)
						RadarDialog->ScaleB[ProfileIndex]->Min=((float)Trace->Index)*RadarMapManager->Settings->WheelTraceDistance;
					else RadarDialog->ScaleB[ProfileIndex]->Min=Trace->Index;
					SetEvent(Profile->OutputView->ClearOutputViewEvents[ProfileIndex]);
					RadarMapManager->MapObjects->Update(gotCustom);
					AfterScrolling[ProfileIndex]=true;
				}
				//if(RadarMapManager->CurrentProfilesCount
			}
			RadarDialog->DPC=__LINE__;
			if(Trace==NULL)
			{
				//LstTrc=Profile->LastTracePtr;
				/*if(Profile==LastProf && LastDrawed)
				{
					if(LastDrawed->Backward) Trace=LastDrawed->PtrDn;
					else Trace=LastDrawed->PtrUp;
					if(!Trace) Trace=Profile->LastTracePtr;
				}
				else
				{ */
					if(Profile!=LastProf) LastDrawed=NULL;
					//Trace=Profile->LastTracePtr;
					Trace=LastDrawed;
					if(Trace!=NULL && !Trace->Backward)
					{
						while(Trace!=NULL && !Trace->Drawed)
							Trace=Trace->PtrDn;
						if(Trace==NULL) Trace=Profile->FirstTracePtr;
						else Trace=Trace->PtrUp;
					}
					else
					{
						while(Trace && Trace->Drawed && Trace->Backward)
							Trace=Trace->PtrDn;
						if(!Trace) Trace=Profile->LastTracePtr;
						if(Trace && !Trace->Drawed && !Trace->Backward)
						{
							while(Trace && !Trace->Drawed)
								Trace=Trace->PtrDn;
							if(!Trace) Trace=Profile->FirstTracePtr;
							else Trace=Trace->PtrUp;
						}
					}
				//}
			}
			RadarDialog->DPC=__LINE__;
			if(WaitForSingleObject(Profile->OutputView->ClearOutputViewEvents[ProfileIndex], 0)==WAIT_OBJECT_0)
			{
				//RadarDialog->RebuildRectsScales();
				RadarDialog->ProfChannelRefresh(ProfileIndex, false, true);
				RadarDialog->ScaleL[ProfileIndex]->Max=0;
				RadarDialog->ScaleL[ProfileIndex]->Min=Profile->Depth;
				RadarDialog->ScaleL[ProfileIndex]->Max=-(((float)Profile->ZeroPoint/(float)Profile->Samples)*Profile->Depth);
				RadarDialog->ScaleL[ProfileIndex]->Min+=RadarDialog->ScaleL[ProfileIndex]->Max;
				RadarDialog->ScaleL[ProfileIndex]->Draw();
				MarkList->Clear();
				_X[ProfileIndex]=RadarDialog->ProfRect[ProfileIndex]->Left;
			}
			RadarDialog->DPC=__LINE__;
			Bmp->BeginUpdate();
			RadarDialog->DPC=__LINE__;
			try
			{
				sm=Profile->Samples;
				RadarDialog->DPC=__LINE__;
				h=RadarDialog->ProfRect[ProfileIndex]->Height()-2;
				RadarDialog->DPC=__LINE__;
				s=(float)sm/(float)h;
				RadarDialog->DPC=__LINE__;
				k=0;
				RadarDialog->DPC=__LINE__;
				while(Trace!=NULL && !Trace->Drawed)
				{
					RadarDialog->DPC=__LINE__;
					if(Trace->Backward) _X[ProfileIndex]--;
					else _X[ProfileIndex]++;
					if(_X[ProfileIndex]>RadarDialog->ProfRect[ProfileIndex]->Right-2)
					{
						ww=1;//(int)((float)RadarDialog->ProfRect[ProfileIndex]->Width()*0.2);
						Dst.Left=RadarDialog->ProfRect[ProfileIndex]->Left+1;
						Dst.Top=RadarDialog->ProfRect[ProfileIndex]->Top+1;
						Dst.Right=RadarDialog->ProfRect[ProfileIndex]->Right-1-ww;
						Dst.Bottom=RadarDialog->ProfRect[ProfileIndex]->Bottom-1;
						Src.Left=RadarDialog->ProfRect[ProfileIndex]->Left+ww+1;
						Src.Top=RadarDialog->ProfRect[ProfileIndex]->Top+1;
						Src.Right=RadarDialog->ProfRect[ProfileIndex]->Right-1;
						Src.Bottom=RadarDialog->ProfRect[ProfileIndex]->Bottom-1;
						Bmp->Draw(Dst, Src, Bmp);
						//if(RadarMapManager->Settings->WheelPositioning!=TPositioning::ManualP)
						if(Profile->XUnits==cxuDistance)
						{
							RadarDialog->ScaleB[ProfileIndex]->Caption="Distance, m";
							RadarDialog->ScaleB[ProfileIndex]->Min+=(ww*RadarMapManager->Settings->WheelTraceDistance);
							RadarDialog->ScaleB[ProfileIndex]->Max+=(ww*RadarMapManager->Settings->WheelTraceDistance);
						}
						else if(Profile->XUnits==cxuTraces)
						{
							RadarDialog->ScaleB[ProfileIndex]->Caption="Traces";
							RadarDialog->ScaleB[ProfileIndex]->Min+=ww;
							RadarDialog->ScaleB[ProfileIndex]->Max+=ww;
						}
						RadarDialog->ScaleB[ProfileIndex]->Draw();
						MarkList->IncOffsetX();
						_X[ProfileIndex]=RadarDialog->ProfRect[ProfileIndex]->Right-1-ww;
					}
					if(Trace->Backward && _X[ProfileIndex]<=RadarDialog->ProfRect[ProfileIndex]->Left+1 &&
						RadarDialog->ProfRect[ProfileIndex]->Width()<Profile->Traces &&
						Profile->CurrentTracePtr!=Profile->FirstTracePtr)
					{
						ww=1;//(int)((float)RadarDialog->ProfRect[ProfileIndex]->Width()*0.2);
						Dst.Left=RadarDialog->ProfRect[ProfileIndex]->Left+1+ww;
						Dst.Top=RadarDialog->ProfRect[ProfileIndex]->Top+1;
						Dst.Right=RadarDialog->ProfRect[ProfileIndex]->Right-1;
						Dst.Bottom=RadarDialog->ProfRect[ProfileIndex]->Bottom-1;
						Src.Left=RadarDialog->ProfRect[ProfileIndex]->Left+1;
						Src.Top=RadarDialog->ProfRect[ProfileIndex]->Top+1;
						Src.Right=RadarDialog->ProfRect[ProfileIndex]->Right-1-ww;
						Src.Bottom=RadarDialog->ProfRect[ProfileIndex]->Bottom-1;
						bbb->Draw(Src, Src, Bmp);
						Src.Left=RadarDialog->ProfRect[ProfileIndex]->Left+1;
						Src.Top=RadarDialog->ProfRect[ProfileIndex]->Top+1;
						Src.Right=RadarDialog->ProfRect[ProfileIndex]->Right-1-ww;
						Src.Bottom=RadarDialog->ProfRect[ProfileIndex]->Bottom-1;
						//Bmp->Draw(Dst, Src, bbb);
						Bmp->Draw(Dst.left, Dst.Top, Types::TRect(Src.left, Src.top, Src.right, Src.bottom), bbb);
						//if(RadarMapManager->Settings->WheelPositioning!=TPositioning::ManualP)
						if(Profile->XUnits==cxuDistance)
						{
							RadarDialog->ScaleB[ProfileIndex]->Caption="Distance, m";
							RadarDialog->ScaleB[ProfileIndex]->Min-=(ww*RadarMapManager->Settings->WheelTraceDistance);
							RadarDialog->ScaleB[ProfileIndex]->Max-=(ww*RadarMapManager->Settings->WheelTraceDistance);
						}
						else if(Profile->XUnits==cxuTraces)
						{
							RadarDialog->ScaleB[ProfileIndex]->Caption="Traces";
							RadarDialog->ScaleB[ProfileIndex]->Min-=ww;
							RadarDialog->ScaleB[ProfileIndex]->Max-=ww;
						}
						RadarDialog->ScaleB[ProfileIndex]->Draw();
						MarkList->DecOffsetX();
						_X[ProfileIndex]=RadarDialog->ProfRect[ProfileIndex]->Left+1+ww;
					}
					RadarDialog->DPC=__LINE__;
					if(!Trace->Backward) _X[ProfileIndex]--;
					else _X[ProfileIndex]++;
					if(_X[ProfileIndex]<RadarDialog->ProfRect[ProfileIndex]->Left+1)
						_X[ProfileIndex]=RadarDialog->ProfRect[ProfileIndex]->Left+1;
					if(Profile->Traces>0 && (Profile->ZeroPoint==0 || Profile->NeedToScaleZero))
					{
						if(Profile->NeedToScaleZero) h=Profile->ZeroPoint;
						else
						{
							h=RadarDialog->SetZeroPoint(Trace, Profile->Samples);
							Profile->ZeroPoint=h;
						}
						//if(h==0) h=1;
						RadarDialog->ScaleL[ProfileIndex]->Max=0;
						RadarDialog->ScaleL[ProfileIndex]->Min=Profile->Depth;
						RadarDialog->ScaleL[ProfileIndex]->Max=-(((float)h/(float)Profile->Samples)*Profile->Depth);
						RadarDialog->ScaleL[ProfileIndex]->Min+=RadarDialog->ScaleL[ProfileIndex]->Max;
						RadarDialog->ScaleL[ProfileIndex]->Draw();
						Profile->NeedToScaleZero=false;
					}
					RadarDialog->DPC=__LINE__;
					for(y=RadarDialog->ProfRect[ProfileIndex]->Top+1; y<RadarDialog->ProfRect[ProfileIndex]->Bottom-1; y++)
					{
						j=k-(int)k;
						if((int)k<Profile->Samples-1)
							t=(1.-j)*(float)Trace->Data[(int)k]+j*(float)Trace->Data[(int)k+1];
						else t=(float)Trace->Data[(int)k];
						p=(int)(255.*(t/(float)(128<<(BitCount+1))))+128;
						if(p<0) p=0;
						if(p>255) p=255;
						c=(Profile->Colors[p]);
						if(Trace->Mark!=0)
						{
							r=c&0x0000FF;
							g=(c&0x00FF00)>>8;
							b=(c&0xFF0000)>>16;
							c=(TColor)RGB(r>>1, g>>1, b>>1);
						}
						if((y<MarkList->minY || y>MarkList->maxY) ||
							((y>=MarkList->minY && y<=MarkList->maxY) && !MarkList->TryPoint(Types::TPoint(_X[ProfileIndex], y))))
								Bmp->Pixel[_X[ProfileIndex]][y]=Color32(c);
						if(Trace->Backward) lineX=-1;
						else lineX=1;
						if((y<MarkList->minY || y>MarkList->maxY) ||
							((y>=MarkList->minY && y<=MarkList->maxY) && !MarkList->TryPoint(Types::TPoint(_X[ProfileIndex]+lineX, y))))
								Bmp->Pixel[_X[ProfileIndex]+lineX][y]=clWhite32;
						k+=s;
					}
					RadarDialog->DPC=__LINE__;
					k=0;
					if(Trace->Mark!=0)
					{
						sm=Trace->Mark;
						if(sm==255) sm=0;
						ww=Bmp->TextWidth(IntToStr(sm))+4;
						h=Bmp->TextHeight(IntToStr(sm))+4;
						ww/=2;
						if(_X[ProfileIndex]-RadarDialog->ProfRect[ProfileIndex]->Left-1<ww) p=RadarDialog->ProfRect[ProfileIndex]->Left+1;
						else if(RadarDialog->ProfRect[ProfileIndex]->Right-1-_X[ProfileIndex]<ww) p=RadarDialog->ProfRect[ProfileIndex]->Right-1-ww*2-1;
						else p=_X[ProfileIndex]-ww*2;//-ww;
						mr=new TMarkRect();
						mr->X1=p;
						mr->Y1=_MarkY[ProfileIndex];
						mr->X2=p+ww*2;
						mr->Y2=_MarkY[ProfileIndex]+h;
						MarkList->Add(mr);
						mmm->BeginUpdate();
						mmm->SetSize(ww*2, h);
						mmm->Clear(clBlack32);
						mmm->RenderText(2, 2, IntToStr(sm), 0, clWhite32);
						mmm->EndUpdate();
						Bmp->Draw(p, _MarkY[ProfileIndex], mmm);
					}
					RadarDialog->DPC=__LINE__;
					Trace->Drawed=true;
					LastDrawed=Trace;
					if(!Trace->Backward)
					{
						_X[ProfileIndex]++;
						Trace=Trace->PtrUp;
					}
					else
					{
						//if(Trace->PtrDn!=NULL && Trace->PtrDn->Backward)
						_X[ProfileIndex]--;
						Trace=Trace->PtrDn;
					}
					if(RadarDialog->RightTraceX[ProfileIndex]<_X[ProfileIndex]) RadarDialog->RightTraceX[ProfileIndex]=_X[ProfileIndex];
				}
				LastProf=Profile;
				//_X[ProfileIndex]--;
				RadarDialog->DPC=__LINE__;
				if(Profile && Profile->CurrentTracePtr && !AfterScrolling[ProfileIndex])
				{
					if(_X[ProfileIndex]<RadarDialog->RightTraceX[ProfileIndex])
					{
						Profile->OutputView->RightIndex=Profile->CurrentTracePtr->Index+(RadarDialog->RightTraceX[ProfileIndex]-_X[ProfileIndex]);
						Profile->OutputView->LeftIndex=Profile->OutputView->RightIndex-RadarDialog->CopyRect[ProfileIndex].Width();
						//Profile->OutputView->LeftIndex=Profile->CurrentTracePtr->Index-(_X[ProfileIndex]-RadarDialog->CopyRect.left);
					}
					else
					{
						Profile->OutputView->RightIndex=Profile->CurrentTracePtr->Index+(RadarDialog->CopyRect[ProfileIndex].right-RadarDialog->RightTraceX[ProfileIndex]);
						Profile->OutputView->LeftIndex=Profile->CurrentTracePtr->Index-(RadarDialog->RightTraceX[ProfileIndex]-RadarDialog->CopyRect[ProfileIndex].left);
					}
				}
			}
			__finally
			{
				RadarDialog->DPC=__LINE__;
				Bmp->EndUpdate();
				RadarDialog->DPC=__LINE__;
			}
			Bmp->Changed();
		}
		Profile=NULL;
		ProfileIndex=-1;
		RadarDialog->DPC=__LINE__;
	}
	catch(Exception &e)
	{
        LogEvent(e.Message, "file "+(AnsiString)__FILE__+" line "+IntToStr(RadarDialog->DPC));
	}
}
//---------------------------------------------------------------------------

TMarkList::TMarkList(void)
{
	List=new TList();
	minY=maxY=maxX=OffsetX=Count=0;
}

TMarkList::~TMarkList(void)
{
	Clear();
	delete List;
}

void TMarkList::Add(TMarkRect *mr)
{
	mr->X1+=OffsetX;
	mr->X2+=OffsetX;
	List->Add(mr);
	Count++;
	if(mr->Y1<minY) minY=mr->Y1;
	if(mr->Y2<minY) minY=mr->Y2;
	if(mr->Y1>maxY) maxY=mr->Y1;
	if(mr->Y2>maxY) maxY=mr->Y2;
	if(mr->X1>(maxX-OffsetX)) maxX=mr->X1+OffsetX;
	if(mr->X2>(maxX-OffsetX)) maxX=mr->X2+OffsetX;
}

void TMarkList::Delete(int num)
{
	delete (TMarkRect*)List->Items[num];
	List->Delete(num);
	Count--;
}

void TMarkList::Clear(void)
{
	int i;

	for(i=0; i<Count; i++)
		delete (TMarkRect*)List->Items[i];
	List->Clear();
	Count=0;
}

TMarkRect* TMarkList::Item(int num)
{
	return (TMarkRect*)List->Items[num];
}

bool TMarkList::TryPoint(Types::TPoint p)
{
	int i;
	TMarkRect *mr;

	if(p.x>(maxX-OffsetX)) return false;
	for(i=0; i<Count; i++)
	{
		mr=Item(i);
		if(p.x>=(mr->X1-OffsetX) && p.x<(mr->X2-OffsetX) && p.y>=mr->Y1 && p.y<mr->Y2)
			return true;
	}
	return false;
}


