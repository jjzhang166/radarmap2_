//---------------------------------------------------------------------------

#ifndef MapH
#define MapH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <ImgList.hpp>
#include <ToolWin.hpp>
#include <GR32_Image.hpp>
#include "LocalMap.h"
#include <Dialogs.hpp>
#include <ExtDlgs.hpp>
#include <ExtCtrls.hpp>
#include "Manager.h"
#include <GestureMgr.hpp>
#include "ImageMap.h"
#include "VectorMap.h"
#include <System.ImageList.hpp>
#ifdef _RAD101
	#include <System.ImageList.hpp>
#endif

//---------------------------------------------------------------------------
class TMapForm : public TForm
{
__published:	// IDE-managed Components
	TImageList *ImageList3;
	TImageList *ImageList2;
	TImageList *ImageList1;
	TOpenPictureDialog *OpenPictureDialog1;
	TImgView32 *MapImage;
	TOpenTextFileDialog *OpenTextFileDialog;
	TTimer *ButtonTimer;
	TGestureManager *MapGestureManager;
	void __fastcall FormResize(TObject *Sender);
	void __fastcall FormCanResize(TObject *Sender, int &NewWidth, int &NewHeight, bool &Resize);
private:	// User declarations
	bool OnTime, DblClick;
	//TToolProgressBar *myprogress;
	TRadarMapManager *RadarMapManager;
	bool __fastcall OpenMap(TMapType mt);

//	void __fastcall ZoomInMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y);
//	void __fastcall ZoomInOnTimer(TObject *Sender);
	void __fastcall ZoomInMouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y);
	void __fastcall ZoomInDblClick(System::TObject* Sender, int X, int Y);

//	void __fastcall ZoomOutMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y);
//	void __fastcall ZoomOutOnTimer(TObject *Sender);
	void __fastcall ZoomOutMouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y);
	void __fastcall ZoomOutDblClick(System::TObject* Sender, int X, int Y);

	bool __fastcall ZoomBingMap(bool pIsZoomIn);
	TDoublePoint __fastcall GetDbDxfMapViewCenterPosition(TDoubleRect pShowMapArea);
	void __fastcall PauseTerminal();
	void __fastcall ResumeTerminal();
	void __fastcall WriteCommand(char *cmd);
public:		// User declarations
	__fastcall TMapForm(TComponent* Owner, TRadarMapManager* RMM);
	__fastcall ~TMapForm();
	void __fastcall Initialize(void);

	//TLocalMapsContainer *LocalMapContainer;
	//TGraphicObjectsContainer *ObjectsContainer;
	TMouseAction MA;
	//TGPSMaps *GPSMaps;
	Types::TRect MapRect;

	//TMapObjectsContainer *MapObjects;
	TSliderToolbar *MapToolbar, *FileToolbar, *ToolsToolbar;
	TSliderToolbarChild *SatellitesBtn, *LabelsChildToolbar;
	TSliderToolButton *FollowMeBtn, *OpenBtn, *LayersBtn, *ZoomInBtn, *ZoomOutBtn,
		*PinBtn, *StartNGoBtn, *StartNGoGPSBtn, *PredictionBtn, *InfoBtn, *OnlineLoadBtn, //, *ExitBtn,
		*PosCorrectionBtn, *PosRecoveryBtn, *StartStopWaypointBtn, *PinLineBtn;
#ifdef _InfraRadarOnly
	TSliderToolButton *AcrobatBtn;
	void __fastcall AcrobatClick(System::TObject* Sender, int X, int Y);

#else
	TSliderToolButton *AddPathBtn;
	void __fastcall AddPathClick(System::TObject* Sender, int X, int Y);
#endif
	TSliderToolButton *NewBtn, *LoadBtn, *SaveBtn, *PathsBtn, *ExportDXFBtn,
		*ExportXYZCBtn, *ExportTXTBtn, *SeparatorBtn, *SettingsBtn;
	TSliderToolButton *ImageMapEditingBtn, *ReferenceBtn;

	TSliderToolbar *OpenMapToolbar;
	TSliderToolButton *KlicMapBtn, *DXFMapBtn, *ImageMapBtn, *OnlineMapBtn,
		*DBSLMapBtn, *DBSLMapSettingsBtn, *OnlineMapXYBtn;
	TSliderToolbarChild *OnlineMapToolbar, *DBSLMapToolbar;

	void __fastcall OpenClick(System::TObject* Sender, int X, int Y);
	void __fastcall LayersClick(System::TObject* Sender, int X, int Y);

	void __fastcall NewClick(System::TObject* Sender, int X, int Y);
	void __fastcall LoadClick(System::TObject* Sender, int X, int Y);
	void __fastcall SaveClick(System::TObject* Sender, int X, int Y);
	void __fastcall PathsClick(System::TObject* Sender, int X, int Y);
	void __fastcall SettingsClick(System::TObject* Sender, int X, int Y);
	void __fastcall ExitClick(System::TObject* Sender, int X, int Y);
	void __fastcall FollowMeClick(System::TObject* Sender, int X, int Y);
	void __fastcall SatellitesClick(System::TObject* Sender, int X, int Y);
	void __fastcall ExportDXFBtnClick(System::TObject* Sender, int X, int Y);
	void __fastcall ExportXYZCBtnClick(System::TObject* Sender, int X, int Y);
	void __fastcall ExportTXTBtnClick(System::TObject* Sender, int X, int Y);
	void __fastcall InfoClick(System::TObject* Sender, int X, int Y);
	void __fastcall ReferenceBtnClick(System::TObject* Sender, int X, int Y);
	void __fastcall ImageMapEditingBtnClick(System::TObject* Sender, int X, int Y);
	void __fastcall PosCorrectionClick(System::TObject* Sender, int X, int Y);
	void __fastcall PosRecoveryClick(System::TObject* Sender, int X, int Y);
	void __fastcall StartStopWaypointClick(System::TObject* Sender, int X, int Y);
	void __fastcall OnlineLoadClick(System::TObject* Sender, int X, int Y);
	void __fastcall KlicMapBtnClick(System::TObject* Sender, int X, int Y);
	void __fastcall DXFMapBtnClick(System::TObject* Sender, int X, int Y);
	void __fastcall ImageMapBtnClick(System::TObject* Sender, int X, int Y);
	void __fastcall DBSLMapBtnClick(System::TObject* Sender, int X, int Y);
	void __fastcall DBSLMapSettingsBtnClick(System::TObject* Sender, int X, int Y);
	void __fastcall OnlineMapBtnClick(System::TObject* Sender, int X, int Y);
	void __fastcall CancelMapBtnClick(System::TObject* Sender, int X, int Y);
	void __fastcall PinClick(System::TObject* Sender, int X, int Y);
	void __fastcall StartNGoClick(System::TObject* Sender, int X, int Y);
	void __fastcall StartNGoGPSClick(System::TObject* Sender, int X, int Y);
	void __fastcall PredictionBtnClick(System::TObject* Sender, int X, int Y);
	void __fastcall LabelsChildToolbarClick(System::TObject* Sender, int X, int Y);
	void __fastcall PinLineBtnClick(System::TObject* Sender, int X, int Y);

	void __fastcall DisableMouseAction();

	//__property TToolProgressBar *MyProgress={read=myprogress};
};
//---------------------------------------------------------------------------
extern PACKAGE TMapForm *MapForm;
//---------------------------------------------------------------------------
#endif
