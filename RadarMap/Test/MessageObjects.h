//---------------------------------------------------------------------------

#ifndef MessageObjectsH
#define MessageObjectsH
//---------------------------------------------------------------------------

#include <GR32.hpp>
#include "MyGraph.h"
#include "Defines.h"
#include "XMLMap.h"

enum TMessageVisibalityType {mvtConstant, mvtDisapering, mvtFlashing};

enum TMessageType {mtInfo = 0xff6464ff, mtWarning = 0xffff8000, mtStop = 0xffff0000,
	mtText = 0xff64ff64, mtRoutine = 0xffffffff, mtCustom = clBlack32};

const int AnimationFramesCount=15;
const int MessageBarHeight=32;
const int MessageBarBorderShift=80;
const int MessageBarBetweenShift=10;
const int MaxMessageBars=10;
const int MaxFlashingCount=10;

class TMessageObject: public TToolObject
{
private:
	TMessageType messagetype;
	int FlashingCount;
protected:
	TMessageVisibalityType visability;
	TBitmap32 *BufBmp, *TextBmp, *BgndBmp;
	int timeout;
	TColor32 color, textcolor;
	TFont* Font;
	bool switchedoff;
	AnsiString text;
	TMultiTimerItem *MainTimerItem, *StartAnimationTimerItem, *OnAnimationTimerItem;
    TVoidFunction SwitchOffCallback;
	virtual void __fastcall Rebuild(bool WithUpdate=true);
	virtual void __fastcall ReText();
	void __fastcall OnTimer();//TCustomTimerThread* Ptr);
	//void __fastcall StartAnimation(TAnimationType aType) {TGraphicObject::StartAnimation(aType);}
	void __fastcall StartMessageAnimation();//TCustomTimerThread* Ptr);
	virtual void __fastcall OnAnimation();//TCustomTimerThread* Ptr);
	TColor32 __fastcall readColor() {if(messagetype==TMessageType::mtCustom) return color; else return (TColor32)messagetype;}
	void __fastcall writeColor(TColor32 value) {color=value; if(messagetype==TMessageType::mtCustom) Rebuild();}
	void __fastcall writeTextColor(TColor32 value) {textcolor=value; Rebuild();}
	void __fastcall writeText(AnsiString value) {text=value; ReText(); Update();}
	virtual void __fastcall writeVisible(bool value);
public:
	__fastcall TMessageObject(TMyObject *AOwner, TImgView32 *ImgView, Types::TRect Rect, TMessageVisibalityType mvt,
		int ATimeOut, TMessageType mt, AnsiString str, TObject* AManager, TVoidFunction aSwitchOffCallback=(TVoidFunction)NULL);
	__fastcall ~TMessageObject();

	void __fastcall OnResize(TObject *Sender, int NewWidth, int NewHeight);
	virtual void __fastcall Redraw(TObject *Sender, TBitmap32 *Bmp);
	void __fastcall Show() {Visible=true;}
	void __fastcall Hide() {Visible=false;}
	void __fastcall SwitchOff();
	void __fastcall AssignTimers(TMultiTimerThread *TimerThrd);
	void __fastcall ChangeClientRectByOwner(TMyObject* AOwner, Types::TRect rct) {if(owner && owner==AOwner && Rect!=rct) {Rect=rct; Rebuild();}}

	__property AnsiString Text = {read=text, write=writeText};
	__property TColor32 Color = {read=readColor, write=writeColor};
	__property TColor32 TextColor = {read=textcolor, write=writeTextColor};
	__property TMessageVisibalityType Visability = {read=visability};
	__property TMessageType MessageType = {read=messagetype};
	__property int TimeOut = {read=timeout};
	__property bool SwitchedOff = {read=switchedoff};
};

class TPosNotificationObject: public TMessageObject
{
private:
	TBitmap32 *CloudBmp;
	AnsiString cloudbmpname, LeftCloud, TopCloud, RightCloud, BottomCloud, LastText;
	Types::TPoint LeftCloudLP, TopCloudLP, RightCloudLP, BottomCloudLP;
	TDoublePoint StartingVPP;
	TGPSCoordinate *coordinate;
	TColor32 color;
	TStrings *strings;
	bool Closing;
	Types::TPoint screenxy, BgndXY, lasttextxy;
	Types::TRect TextRect;
	void __fastcall Initialize();
protected:
	void __fastcall Rebuild(bool WithUpdate=true);
	void __fastcall ReText();
	TGPSCoordinate* __fastcall readCoordinate() {return coordinate;}
	bool __fastcall readPopedUp() {return visible;}
	void __fastcall writeCoordinate(TGPSCoordinate *value) {if(coordinate!=value) {if(coordinate) delete coordinate; coordinate=value;}}
	void __fastcall writeCloudBmpName(AnsiString value);
	bool __fastcall StartAnimation(TAnimationType aType);
	void __fastcall OnAnimation();
//	void __fastcall writeVisible(bool value) {TMessageObject::writeVisible(value); }
	__property AnsiString CloudBmpName = {read=cloudbmpname, write=writeCloudBmpName};
public:
	__fastcall TPosNotificationObject(TMyObject *AOwner, TImgView32 *ImgView,
		Types::TRect Rect, TMessageVisibalityType mvt, int ATimeOut, TColor32 AColor,
		AnsiString str, Types::TPoint SP, TObject* AManager);
	__fastcall TPosNotificationObject(TMyObject *AOwner, TImgView32 *ImgView,
		Types::TRect Rect, TMessageVisibalityType mvt, int ATimeOut, TColor32 AColor,
		AnsiString str, TGPSCoordinate *aCoordinate, TObject* AManager);
	__fastcall TPosNotificationObject(TMyObject *AOwner, TImgView32 *ImgView,
		Types::TRect Rect, TMessageVisibalityType mvt, int ATimeOut, TColor32 AColor,
		AnsiString str, TDoublePoint aSvpp, TObject* AManager);
	__fastcall ~TPosNotificationObject();

	//void __fastcall MouseClick(System::TObject* Sender, int X, int Y) {PopDown();}
	void __fastcall MouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y,
		TCustomLayer *Layer) {if(Pressed) PopDown();}

	void __fastcall Redraw(TObject *Sender, TBitmap32 *Bmp);
	void __fastcall PopUp() {if(!StartAnimation(atAppear)) Visible=true;}
	void __fastcall PopDown() {if(!StartAnimation(atDisappear)) Visible=false;}

	__property TGPSCoordinate *Coordinate = {read=readCoordinate, write=writeCoordinate};
	__property Types::TPoint ScreenXY = {read=screenxy}; //Screen XY point in px of map object
	__property TStrings *Strings = {read=strings};
	__property bool PopedUp = {read=readPopedUp};
};

enum TMessagesScrollAlign {msaTop, msaBottom, msaPosNotification};

class TMessagesScrollObject: public TToolObject
{
private:
	TGraphicObjectsList* Messages;
	TMessagesScrollAlign align;
	TMultiTimerThread *TimerThrd;
	TMultiTimerItem *GarbageEaterTimer;
	TMessageObject *LastAdded;
	int maxmessagesquantity;
	void __fastcall GarbageEater();
	void __fastcall Rebuild();
	void __fastcall CreateThreads();
	void __fastcall TerminateThreads();
protected:
	int __fastcall readCount() {return Messages->Count;}
	void __fastcall writeAlign(TMessagesScrollAlign value) {if(align!=value) {align=value; Update();}}
	void __fastcall writeMaxMessagesQuantity(int value);
public:
	__fastcall TMessagesScrollObject(TMyObject *AOwner, TImgView32 *ImgView, Types::TRect Rect,
		TMessagesScrollAlign msa, TObject* AManager);
	__fastcall ~TMessagesScrollObject();

	int __fastcall AddObject(TMyObject* o); // overload
	void __fastcall RemoveChildFromList(TMyObject* o);
	void __fastcall Update() {Update(objecttype);} //overload
	void __fastcall Update(TMyObjectType Type); //overload

	void __fastcall OnResize(TObject *Sender, int NewWidth, int NewHeight);
	void __fastcall Redraw(TObject *Sender, TBitmap32 *Bmp);
	TMessageObject* __fastcall AddMessage(TMessageType mt, AnsiString str,
		TMessageVisibalityType mvt, int ATimeOut,
		TVoidFunction SwitchOffCallback=(TVoidFunction)NULL);

	__property int Count = {read=readCount};
	__property TMessagesScrollAlign Align = {read=align, write=writeAlign};
	__property int MaxMessagesQuantity = {read=maxmessagesquantity, write=writeMaxMessagesQuantity};
};

#endif
