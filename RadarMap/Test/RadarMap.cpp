//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
#include <tchar.h>

#include "Defines.h"
#include "Splash.h"

//---------------------------------------------------------------------------
USEFORM("..\Settings.cpp", SettingsForm);
USEFORM("..\Preview.cpp", PreviewForm);
USEFORM("LabelForm.cpp", LabelInfoForm);
USEFORM("Radar.cpp", RadarDialog);
USEFORM("Map.cpp", MapForm);
USEFORM("..\OptionsPlugins.cpp", OptionsPluginsForm);
USEFORM("..\OptionsGpsUnit.cpp", OptionsGpsUnitForm);
USEFORM("VectorDBForm.cpp", VectorDBForm);
USEFORM("GeometryForm.cpp", GeometryInfoForm);
USEFORM("Paths.cpp", PathsForm);
USEFORM("OnlineMap.cpp", OnlineMapForm);
USEFORM("Layers.cpp", LayersForm);
USEFORM("..\PalettesRepository.cpp", PalettesForm);
USEFORM("..\TwoWheelsUnit.cpp", TwoWheelsForm);
USEFORM("..\Disclaimer.cpp", DisclaimerForm);
USEFORM("MainForm.cpp", RadarMapForm);
USEFORM("..\LoadPlugins.cpp", LoadPluginsForm);
USEFORM("..\Setup.cpp", SetupForm);
USEFORM("Files.cpp", FilesForm);
USEFORM("InfoForm.cpp", InfoForm);
USEFORM("GainForm.cpp", GainForm);
USEFORM("..\Splash.cpp", SplashForm);
//---------------------------------------------------------------------------
WINAPI _tWinMain(HINSTANCE, HINSTANCE, LPTSTR, int)
{
	try
	{
		LogEvent("RadarMap starting on OS "+GetHostOSName(true), "WinMain()");

		SplashForm=new TSplashForm(Application);
		SplashForm->PBar->Min=0;
		SplashForm->PBar->Max=13;
		//SplashForm->PBar->Step=1;
		SplashForm->PBar->Position=0;
#ifdef _WithSplash
		SplashForm->Show();
		SplashForm->Update();
#endif
		DecimalSeparator='.';
		SplashForm->PBar->Position++;
		Application->Initialize();
		SplashForm->PBar->Position++;
		Application->MainFormOnTaskBar = true;
		Application->Title = (AnsiString)_RadarMapName;
		Application->CreateForm(__classid(TRadarMapForm), &RadarMapForm);
		SplashForm->PBar->Position++;
		Application->CreateForm(__classid(TSettingsForm), &SettingsForm);
		SplashForm->PBar->Position++;
		Application->CreateForm(__classid(TLayersForm), &LayersForm);
		SplashForm->PBar->Position++;
		Application->CreateForm(__classid(TInfoForm), &InfoForm);
		SplashForm->PBar->Position++;
		Application->CreateForm(__classid(TDisclaimerForm), &DisclaimerForm);
		SplashForm->PBar->Position++;
		Application->CreateForm(__classid(TOptionsPluginsForm), &OptionsPluginsForm);
		SplashForm->PBar->Position++;
		Application->CreateForm(__classid(TLoadPluginsForm), &LoadPluginsForm);
		SplashForm->PBar->Position++;
		Application->CreateForm(__classid(TPalettesForm), &PalettesForm);
		SplashForm->PBar->Position++;
		Application->CreateForm(__classid(TLabelInfoForm), &LabelInfoForm);
		SplashForm->PBar->Position++;
		Application->CreateForm(__classid(TSettingsForm), &SettingsForm);
		SplashForm->PBar->Position++;
		Application->CreateForm(__classid(TOptionsGpsUnitForm), &OptionsGpsUnitForm);
		SplashForm->PBar->Position++;
		Application->CreateForm(__classid(TGeometryInfoForm), &GeometryInfoForm);
		SplashForm->PBar->Position++;
		Application->Run();

		LogEvent("RadarMap Closed", "WinMain()");
	}
	catch (Exception &exception)
	{
		Application->ShowException(&exception);
	}
	catch (...)
	{
		try
		{
			throw Exception("");
		}
		catch (Exception &exception)
		{
			Application->ShowException(&exception);
		}
	}
	return 0;
}
//---------------------------------------------------------------------------
