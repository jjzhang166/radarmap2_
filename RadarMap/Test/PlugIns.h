//---------------------------------------------------------------------------

#ifndef PlugInsH
#define PlugInsH
//---------------------------------------------------------------------------
#include "Defines.h"
#include "MyGraph.h"
#include "_PlugIns.h"
#include <registry.hpp>

#define _PlugInsRegKey "\\PlugIns"

bool __fastcall CheckLibrarySignature(HINSTANCE hInst);

bool __fastcall IsValidGUID(AnsiString guid);

class TPlugInObject : public TMyObject
{
private:
	TPlugInType plugintype;
	AnsiString dllname;
	AnsiString guid;
	TList *options;
	int optionscount;
	__fastcall bool readActive() {return (hInstance && pInitPlugIn && pFreePlugIn && pGetPlugInInfo);}
protected:
	AnsiString name;
	HINSTANCE hInstance;
	int pluginid;
	TInitFreePlugIn pInitPlugIn;
	TInitFreePlugIn pFreePlugIn;
	TGetPlugInInfo pGetPlugInInfo;
	TGetOption pGetOption, pSetOption;
	virtual void __fastcall UpdatePlugInInfo() {}
	virtual void __fastcall SaveLocalParameters(TRegistry *regKey) {}
	virtual void __fastcall LoadLocalParameters(TRegistry *regKey) {}
	virtual void __fastcall writeName(AnsiString value) {}
	void __fastcall SaveSettingsByGUID(AnsiString aGUID="");
	int __fastcall readOptionsCount() {return options->Count;}
	TOption* __fastcall readOption(int Index) {if(Index>=0 && Index<options->Count) return (TOption*)options->Items[Index]; else return NULL;}
public:
	__fastcall TPlugInObject(AnsiString aGUID, TPlugInType pit);
	__fastcall TPlugInObject(AnsiString aGUID, TPlugInType pit, AnsiString aDllName, int aPlugInID, AnsiString aName, int aOptionsCount);
	__fastcall ~TPlugInObject() {Free(); delete options;}

	virtual bool __fastcall Initialize();
	virtual void __fastcall Free();
	void __fastcall GetOptions(HINSTANCE hInst=NULL);
	void __fastcall SetOptions(HINSTANCE hInst=NULL);

	void __fastcall SaveSettings() {TPlugInObject::SaveSettingsByGUID();}
	bool __fastcall LoadSettings() {return TPlugInObject::LoadSettingsByGUID();}

	bool __fastcall LoadSettingsByGUID(AnsiString aGUID="");
	virtual void __fastcall SetSettings(const char *PlugInInfo) {}

	__property AnsiString GUID = {read=guid};
	__property TPlugInType Type = {read=plugintype};
	__property AnsiString DLLFileName = {read=dllname};
	__property bool Active = {read=readActive};
	__property int PlugInID = {read=pluginid};
	__property AnsiString Name = {read=name, write=writeName};
	__property int OptionsCount = {read=readOptionsCount};
	__property TOption* Options[int Index] = {read=readOption};
};

class TPlugInCoordinateSystem : public TPlugInObject
{
private:
	T3DPointConvert pLatLonAltToXYH;
	T3DPointConvert pXYHToLatLonAlt;
	TUnitVector pUnitVector;
	TDoubleRect latlonbounds;
	TDoubleRect xybounds;
	AnsiString sLatLonAltToXYH, sXYHToLatLonAlt, sUnitVector;
	AnsiString prefix;
	AnsiString code;
	AnsiString ellipsoidname;
	AnsiString epsg;
	AnsiString proj4;
	TCoordinateSystemDimension dimension;
protected:
	void __fastcall UpdatePlugInInfo();
	void __fastcall SaveLocalParameters(TRegistry *regKey);
	void __fastcall LoadLocalParameters(TRegistry *regKey);
	bool __fastcall writePlugInStrOption(int id, AnsiString value);
	void __fastcall writeName(AnsiString value) {if(writePlugInStrOption(_proj4OptionName, value)) name=value;}
	void __fastcall writePrefix(AnsiString value) {if(writePlugInStrOption(_proj4OptionPrefix, value)) prefix=value;}
	void __fastcall writeEllipsoidName(AnsiString value) {if(writePlugInStrOption(_proj4OptionEllipsoid, value)) ellipsoidname=value;}
	void __fastcall writeEPSG(AnsiString value) {if(writePlugInStrOption(_proj4OptionEPSG, value)) epsg=value;}
	void __fastcall writePROJ4(AnsiString value) {if(writePlugInStrOption(_proj4OptionDefinition, value)) proj4=value;}
	void __fastcall writeDimension(TCoordinateSystemDimension value);
public:
	__fastcall TPlugInCoordinateSystem(AnsiString aGUID) : TPlugInObject(aGUID, pitCRS) {}
	__fastcall TPlugInCoordinateSystem(AnsiString aGUID, AnsiString aDllName,
		int aPlugInID, AnsiString aName, int aOptionsCount, AnsiString aLLA2XYH_Name,
		AnsiString aXYH2LLA_Name, AnsiString aUnitVector_Name="");
	__fastcall ~TPlugInCoordinateSystem() {}

	bool __fastcall Initialize();
	void __fastcall Free();

	__fastcall bool LatLonAltToXYH(double lat, double lon, double alt,
		double &x, double &y, double &h) {if(!Active) Initialize(); if(pLatLonAltToXYH) return (pLatLonAltToXYH)(lat, lon, alt, x, y, h); else return false;}
	__fastcall bool XYHToLatLonAlt(double x, double y, double h,
		double &lat, double &lon, double &alt) {if(!Active) Initialize(); if(pXYHToLatLonAlt) return (pXYHToLatLonAlt)(x, y, h, lat, lon, alt); else return false;}
	__fastcall bool UnitVector(double lat, double lon, double alt,
		TDoublePoint &dp) {if(!Active) Initialize(); if(pUnitVector) return (pUnitVector)(lat, lon, alt, dp.X, dp.Y); else return false;}
	void __fastcall SetSettings(const char *PlugInInfo);

	__property TDoubleRect LatLonBounds = {read=latlonbounds};
	__property TDoubleRect XYBounds = {read=xybounds};
	__property AnsiString Prefix = {read=prefix, write=writePrefix};
	__property AnsiString Code = {read=code};
	__property AnsiString EllipsoidName = {read=ellipsoidname, write=writeEllipsoidName};
	__property AnsiString EPSG = {read=epsg, write=writeEPSG};
	__property AnsiString PROJ4 = {read=proj4, write=writePROJ4};
	__property TCoordinateSystemDimension Dimension = {read=dimension, write=writeDimension};
};

class TPlugInPositioning : public TPlugInObject
{
private:
	AnsiString sStartAcquisition, sStopAcquisition, sGetPassedDistance,
		sGetCorrectedXYH;
	TStartAcquisition pStartAcquisition;
	TVoidProcedure pStopAcquisition;
	TGetPassedDistance pGetPassedDistance;
	TGetCorrectedXYH pGetCorrectedXYH;
	double lastResult;
	bool lastFreshness;
protected:
	void __fastcall SaveLocalParameters(TRegistry *regKey);
	void __fastcall LoadLocalParameters(TRegistry *regKey);
	double __fastcall readLastResult() {lastFreshness = false; return lastResult;}
public:
	__fastcall TPlugInPositioning(AnsiString aGUID) : TPlugInObject(aGUID, pitPos) {lastResult=0; lastFreshness=false;}
	__fastcall TPlugInPositioning(AnsiString aGUID, AnsiString aDllName,
		int aPlugInID, AnsiString aName, int aOptionsCount,
		AnsiString aStart_Name, AnsiString aStop_Name,
		AnsiString aGetPassedDistance_Name, AnsiString aGetCorrectedXYH_Name);
	__fastcall ~TPlugInPositioning() {}

	bool __fastcall Initialize();
	void __fastcall SetSettings(const char *PlugInInfo);

	void StartAcquisition(T3DDoublePoint &XYH, TCoordinateSystemDimension &Dimension) {if(!Active) Initialize(); if(pStartAcquisition) (pStartAcquisition)(XYH, Dimension);}
	void StopAcquisition() {if(!Active) Initialize(); if(pStopAcquisition) (pStopAcquisition)();}
	// in case of csdDegrees, X = Longitude, Y = Latitude, H = Altitude
	double GetPassedDistance(double WheelDistance, T3DDoublePoint XYH, bool XYHFreshness, TCoordinateSystemDimension Dimension) {double res; if(!Active) Initialize(); if(pGetPassedDistance) res=(pGetPassedDistance)(WheelDistance, XYH, XYHFreshness, Dimension); else res=0; return res;}
	void GetCorrectedXYH(T3DDoublePoint XYH, bool XYHFreshness, TCoordinateSystemDimension Dimension) {if(!Active) Initialize(); if(pGetPassedDistance) (pGetCorrectedXYH)(XYH, XYHFreshness, Dimension);}
};

class TPlugInObjectsList: public TMyObjectsList
{
private:
protected:
	TPlugInObject* __fastcall readItem(int Index) {return (TPlugInObject*)readMyObjectItem(Index);}
	void __fastcall writeItem(int Index, TPlugInObject* value) {writeMyObjectItem(Index, (TMyObject*)value);}
public:
	__fastcall TPlugInObjectsList(TMyObject *AOwner) : TMyObjectsList(AOwner) {}
	__fastcall ~TPlugInObjectsList() {}

	int __fastcall Add(TPlugInObject* o) {return TMyObjectsList::Add((TMyObject*)o);}
	TPlugInObject* __fastcall GetByGUID(AnsiString guid, TPlugInType type=pitUnknown);
	void __fastcall DeleteByGUID(AnsiString guid, bool DeleteFromHard=false);

	__property TPlugInObject* Items[int Index] = {read=readItem, write=writeItem};
};

struct TPlugInListItem
{
	AnsiString DLLName;
	AnsiString GUID;
	AnsiString Name;
	AnsiString About;
	AnsiString Version;
	AnsiString Description;
	TPlugInType Type;
	int ID;
};

class TPlugInManager : public TMyObject
{
private:
	TPlugInObjectsList *PlugIns;
	TPlugInCoordinateSystem *activeCS;
	TPlugInPositioning *activePos;
public:
	__fastcall TPlugInManager();
	__fastcall ~TPlugInManager();

	int __fastcall GetPlugInsCountInLibrary(AnsiString FileName);
	int __fastcall GetPlugInsListInLibrary(AnsiString FileName,
		TPlugInListItem *Items, int Count, TPlugInType NeededType=pitUnknown);

	void __fastcall SaveSettings();
	void __fastcall LoadSettings();

	int __fastcall AddPlugIn(AnsiString FileName, int Id);

	void __fastcall SetActive(AnsiString guid, TPlugInType type);
	void __fastcall ClearActive(TPlugInType type);

	TPlugInObject* __fastcall GetByGUID(AnsiString guid, TPlugInType type=pitUnknown) {return PlugIns->GetByGUID(guid, type);}

	__property TPlugInCoordinateSystem *ActiveCS = {read=activeCS};
	__property TPlugInPositioning *ActivePos = {read=activePos};
	__property TPlugInObjectsList *PlugInsList = {read=PlugIns};
};

extern PACKAGE TPlugInManager *PlugInManager;

#endif
