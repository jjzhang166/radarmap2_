//---------------------------------------------------------------------------


#pragma hdrstop

#include "BitmapGDI.h"
#include <GR32_Filters.hpp>
#include <Math.hpp>

//---------------------------------------------------------------------------

#pragma package(smart_init)

void ScaleBitmap32(TBitmap32* Src, float ScaleX, float ScaleY, bool AsIs)
{
	/*try
	{
		if(Src)
		{
			Gdiplus::GdiplusStartupInput gdiplusStartupInput;
			ULONG_PTR  gdiplusToken;
			GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);
			try
			{
				Gdiplus::Graphics *graphics = new Gdiplus::Graphics(Src->Handle);
				Gdiplus::Bitmap *originalimage;
				if(AsIs) originalimage=new Gdiplus::Bitmap(&(Src->BitmapInfo), Src->Bits);
				else originalimage=new Gdiplus::Bitmap(Src->Width, Src->Height, Src->Width*4,
					PixelFormat32bppARGB, (unsigned char*)Src->Bits);
				try
				{
					try
					{
						graphics->ResetTransform();
						graphics->TranslateTransform(0, 0);
						graphics->ScaleTransform(ScaleX, ScaleY);
						//graphics->TranslateTransform(-(Src->Width>>1), -(Src->Height>>1));

						graphics->DrawImage(originalimage,0,0);
					}
					catch(Exception &e) {}
				}
				__finally
				{
					delete originalimage;
					delete graphics;
				}
			}
			__finally
			{
				Gdiplus::GdiplusShutdown(gdiplusToken);
			}
		}
	}
	catch(Exception &e) {}*/

	TBitmap32 *Dst;
	try
	{
		Dst=new TBitmap32();
		Dst->DrawMode=dmBlend;//
		Dst->SetSizeFrom(Src);
		Dst->Clear(Color32(0,0,0,0));
		ScaleBitmap32(Src, Dst, ScaleX, ScaleY, true);//AsIs);
	}
	__finally
	{
		Src->SetSize((int)((float)Src->Width*ScaleX)+1, (int)((float)Src->Height*ScaleY)+1);
		Src->DrawMode=dmBlend;//dmTransparent;//
		Src->Draw(0, 0, Dst);
		delete Dst;
	}
}
//---------------------------------------------------------------------------

void ScaleBitmap32(TBitmap32* Src, TBitmap32* Dst, float ScaleX, float ScaleY, bool AsIs)
{
	if(Src && Dst)
	{
		Dst->SetSizeFrom(Src);
		Dst->Clear(Color32(0,0,0,0));
		try
		{
			Gdiplus::GdiplusStartupInput gdiplusStartupInput;
			ULONG_PTR  gdiplusToken;
			GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);
			try
			{
				Gdiplus::Graphics *graphics = new Gdiplus::Graphics(Dst->Handle);
				Gdiplus::Bitmap *originalimage;
				if(AsIs) originalimage=new Gdiplus::Bitmap(&(Src->BitmapInfo), Src->Bits);
				else originalimage=new Gdiplus::Bitmap(Src->Width, Src->Height, Src->Width*4,
					PixelFormat32bppARGB, (unsigned char*)Src->Bits);
				try
				{
					try
					{
						graphics->ResetTransform();
						graphics->TranslateTransform(0, 0);
						graphics->ScaleTransform(ScaleX, ScaleY);
						//graphics->TranslateTransform(-(Src->Width>>1), -(Src->Height>>1));
						graphics->DrawImage(originalimage,0,0);
					}
					catch(Exception &e) {}
				}
				__finally
				{
					delete originalimage;
					delete graphics;
				}
			}
			__finally
			{
				Gdiplus::GdiplusShutdown(gdiplusToken);
			}
		}
		catch(Exception &e) {}
	}
}
//---------------------------------------------------------------------------

void ScaleBitmap32(TBitmap32* Src, TBitmap32* Dst, TDoubleRect *viewport, Types::TRect OutRect, bool AsIs)
{
	int x, y;
	TDoublePoint p;
	//unsigned long ul=::GetTickCount();

	if(Src && Dst)
	{
		p=viewport->Center();
		//x=(int)(-(p.X)*(double)Src->Width);
		//y=(int)(-(p.Y)*(double)Src->Height);
		Dst->SetSize(OutRect.Width(), OutRect.Height());
		x=(Dst->Width>>1)-(int)(p.X*(double)Src->Width/viewport->Width());
		y=(Dst->Height>>1)-(int)(p.Y*(double)Src->Height/viewport->Height());
		//x=((int)(-p.X*(double)Src->Width/viewport->Width()))>>1;
		//y=((int)(-p.Y*(double)Src->Height/viewport->Height()))>>1;
		Dst->Clear(Color32(0,0,0,0));
		Dst->DrawMode=dmBlend;
		try
		{
			Gdiplus::GdiplusStartupInput gdiplusStartupInput;
			ULONG_PTR  gdiplusToken;
			GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);
			try
			{
				Gdiplus::Graphics *graphics = new Gdiplus::Graphics(Dst->Handle);
				Gdiplus::Bitmap *originalimage;
				if(AsIs) originalimage=new Gdiplus::Bitmap(&(Src->BitmapInfo), Src->Bits);
				else originalimage=new Gdiplus::Bitmap(Src->Width, Src->Height, Src->Width*4,
					PixelFormat32bppARGB, (unsigned char*)Src->Bits);
				try
				{
					try
					{
						graphics->ResetTransform();
						//graphics->TranslateTransform(0, 0);
						graphics->TranslateTransform(x, y);
						//graphics->TranslateTransform(Dst->Width>>1, Dst->Height>>1);
						graphics->ScaleTransform(1./viewport->Width(), 1./viewport->Height());
						//graphics->TranslateTransform(-(Src->Width>>1), -(Src->Height>>1));
						graphics->DrawImage(originalimage,0,0);
					}
					catch(Exception &e) {}
				}
				__finally
				{
					delete originalimage;
					delete graphics;
				}
			}
			__finally
			{
				Gdiplus::GdiplusShutdown(gdiplusToken);
			}
		}
		catch(Exception &e) {}
		//ShowMessage(::GetTickCount()-ul);
	}
}
//---------------------------------------------------------------------------

void ScaleBitmap32(TBitmap32* Src, TBitmap32* Dst, TLockedDoubleRect *viewport, Types::TRect OutRect, bool AsIs)
{
	if(viewport) ScaleBitmap32(Src, Dst, &TDoubleRect(viewport->Left, viewport->Top,
		viewport->Right, viewport->Bottom),	OutRect, AsIs);
}
//---------------------------------------------------------------------------

void RotateBitmap32(TBitmap32* Src, double AngleRad, bool AsIs)
{
	double Degs=RadToDeg(AngleRad);

	RotateInDegBitmap32(Src, Degs, AsIs);
}
//---------------------------------------------------------------------------

void RotateInDegBitmap32(TBitmap32* Src, double Degs, bool AsIs)
{
	try
	{
		if(Src)
		{
			Gdiplus::GdiplusStartupInput gdiplusStartupInput;
			ULONG_PTR  gdiplusToken;
			GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);
			try
			{
				Gdiplus::Graphics *graphics = new Gdiplus::Graphics(Src->Handle);
				Gdiplus::Bitmap *originalimage;
				if(AsIs) originalimage=new Gdiplus::Bitmap(&(Src->BitmapInfo), Src->Bits);
				else originalimage=new Gdiplus::Bitmap(Src->Width, Src->Height, Src->Width*4,
					PixelFormat32bppARGB, (unsigned char*)Src->Bits);
				try
				{
					try
					{
						graphics->ResetTransform();
						graphics->TranslateTransform((Src->Width>>1), (Src->Height>>1));
						graphics->RotateTransform(Degs);
						graphics->TranslateTransform(-(Src->Width>>1), -(Src->Height>>1));

						graphics->DrawImage(originalimage,0,0);
					}
					catch(Exception &e) {}
				}
				__finally
				{
					delete originalimage;
					delete graphics;
				}
			}
			__finally
			{
				Gdiplus::GdiplusShutdown(gdiplusToken);
			}
		}
	}
	catch(Exception &e) {}
}
//---------------------------------------------------------------------------

void RotateBitmap32(TBitmap32* Src, TBitmap32* Dst, double AngleRad, bool AsIs)
{
	double Degs=RadToDeg(AngleRad);

	if(Src)
	{
		Types::TPoint cP;

		cP=Types::TPoint(Src->Width >> 1, Src->Height >> 1);
		RotateInDegBitmap32(Src, Dst, Degs, cP, AsIs);
	}
}
//---------------------------------------------------------------------------

void RotateInDegBitmap32(TBitmap32* Src, TBitmap32* Dst, double Degs, Types::TPoint cP, bool AsIs)
//void RotateInDegBitmap32(TBitmap32* Src, TBitmap32* Dst, double Degs, bool AsIs)
{
	if(Src && Dst)
	{
		Gdiplus::GdiplusStartupInput gdiplusStartupInput;
		ULONG_PTR  gdiplusToken;

		Dst->SetSizeFrom(Src);
		Dst->DrawMode=dmBlend;
		Dst->Clear(Color32(255,255,255,0));
		GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);
		try
		{
			Gdiplus::Graphics *graphics = new Gdiplus::Graphics(Dst->Handle);
			Gdiplus::Bitmap *originalimage;
			if(AsIs) originalimage=new Gdiplus::Bitmap(&(Src->BitmapInfo), Src->Bits);
			else originalimage=new Gdiplus::Bitmap(Src->Width, Src->Height, Src->Width*4,
				PixelFormat32bppARGB, (unsigned char*)Src->Bits);
			try
			{
				try
				{
					graphics->ResetTransform();
					//graphics->TranslateTransform((Src->Width>>1), (Src->Height>>1));
					graphics->TranslateTransform(cP.x, cP.y);
					graphics->RotateTransform(Degs);
					//graphics->TranslateTransform(-(Src->Width>>1), -(Src->Height>>1));
					graphics->TranslateTransform(-cP.x, -cP.y);

					graphics->DrawImage(originalimage,0,0);
				}
				catch(Exception &e) {}
			}
			__finally
			{
				delete originalimage;
				delete graphics;
			}
		}
		__finally
		{
			Gdiplus::GdiplusShutdown(gdiplusToken);
		}
	}
}
//---------------------------------------------------------------------------
void ApplyColorMask(TBitmap32* Bmp, TColor32 color, TDrawMode DrawMode)
{
	TBitmap32 *tmp;

	if(Bmp)
	{
		tmp=new TBitmap32();
		tmp->DrawMode=DrawMode;//dmTransparent;//dmBlend;
		tmp->SetSizeFrom(Bmp);
		tmp->Clear(color);
		IntensityToAlpha(tmp, Bmp);
		Bmp->Assign(tmp);
		delete tmp;
	}
}
//---------------------------------------------------------------------------

void ApplyColorMask(TBitmap32* Bmp, TColor32 color, TColor32 &text_color, TDrawMode DrawMode)
{
	TColor32Entry cec, cet;
	System::Byte r, g, b;

	ApplyColorMask(Bmp, color, DrawMode);
	cec.ARGB=color;
	cet.ARGB=text_color;
	r=cet.R-cec.R;
	g=cet.G-cec.G;
	b=cet.B-cec.B;
	if((int)(r+g+b)<96)
	{
		cec.R+=32;
		cec.G+=32;
		cec.B+=32;
		text_color=cec.ARGB;
	}
}
//---------------------------------------------------------------------------

