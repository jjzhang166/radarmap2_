//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "InfoForm.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "ImageButton"
#pragma link "G32_ProgressBar"
#pragma link "GR32_Image"
#pragma resource "*.dfm"
TInfoForm *InfoForm;

//---------------------------------------------------------------------------
__fastcall TInfoForm::TInfoForm(TComponent* Owner, TRadarMapManager *AManager)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------

void __fastcall TInfoForm::CloseIButtonClick(TObject *Sender)
{
	ModalResult=mrCancel;
}

//---------------------------------------------------------------------------
void __fastcall TInfoForm::InfoBtnClick(TObject *Sender)
{
	int z =((TComponent*)Sender)->Owner->ComponentCount;

	for(int i = 0; i < z; i++)
	{
		if(((TComponent *)Sender)->Owner->Components[i]->ClassName()=="TImageButton" && ((TComponent*)Sender)->Owner->Components[i]!=Sender)
			((TImageButton*)((TComponent *)Sender)->Owner->Components[i])->Down=false;
	}
	((TImageButton*)Sender)->Down=true;
	Notebook1->PageIndex=((TImageButton*)Sender)->Tag;
}
//---------------------------------------------------------------------------

void __fastcall TInfoForm::FormShow(TObject *Sender)
{
	/*int z =ComponentCount;

	/*for(int i = 0; i < z; i++)
	{
		if(Components[i]->ClassName()=="TImageButton")
		{
			if(((TImageButton*)Components[i])->Tag==Notebook1->PageIndex)
				((TImageButton*)Components[i])->Down=true;
			else ((TImageButton*)Components[i])->Down=false;
		}
	}*/
	HideCaret(BriefMemo->Handle);
	InfoBtnClick(InfoBtn);
}
//---------------------------------------------------------------------------

void __fastcall TInfoForm::OkImageButtonClick(TObject *Sender)
{
	ModalResult=mrOk;
}
//---------------------------------------------------------------------------

void __fastcall TInfoForm::TitlePanelMouseDown(TObject *Sender, TMouseButton Button,
		  TShiftState Shift, int X, int Y)
{
	TitleDown=true;
	TitleXY=ClientToScreen(Types::TPoint(X, Y));
}
//---------------------------------------------------------------------------

void __fastcall TInfoForm::TitlePanelMouseMove(TObject *Sender, TShiftState Shift,
		  int X, int Y)
{
	Types::TPoint tmp;

	if(TitleDown)
	{
		tmp=ClientToScreen(Types::TPoint(X, Y));
		Left+=tmp.x-TitleXY.x;
		Top+=tmp.y-TitleXY.y;
		TitleXY=tmp;
	}
}
//---------------------------------------------------------------------------

void __fastcall TInfoForm::TitlePanelMouseUp(TObject *Sender, TMouseButton Button,
		  TShiftState Shift, int X, int Y)
{
	TitleDown=false;
}
//---------------------------------------------------------------------------
void __fastcall TInfoForm::BriefMemoKeyPress(TObject *Sender, wchar_t &Key)
{
	if(Key=='[' || Key==']') Key=0; //Restriction due to "<![CDATA[" and "]]>" using of BriefInfo storing in GML format
}
//---------------------------------------------------------------------------

void __fastcall TInfoForm::GeneratedMemoChange(TObject *Sender)
{
	HideCaret(((TMemo*)Sender)->Handle);
}
//---------------------------------------------------------------------------

void __fastcall TInfoForm::GeneratedMemoMouseDown(TObject *Sender, TMouseButton Button,
		  TShiftState Shift, int X, int Y)
{
	HideCaret(GeneratedMemo->Handle);
}
//---------------------------------------------------------------------------

void __fastcall TInfoForm::GeneratedMemoClick(TObject *Sender)
{
	HideCaret(GeneratedMemo->Handle);
}
//---------------------------------------------------------------------------

void __fastcall TInfoForm::GeneratedMemoMouseMove(TObject *Sender, TShiftState Shift,
          int X, int Y)
{
	HideCaret(GeneratedMemo->Handle);
}
//---------------------------------------------------------------------------

void __fastcall TInfoForm::GeneratedMemoMouseUp(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y)
{
	HideCaret(GeneratedMemo->Handle);
}
//---------------------------------------------------------------------------

