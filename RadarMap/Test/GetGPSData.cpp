//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "GetGPSData.h"
#include "Radar.h"
#include "Manager.h"
#pragma package(smart_init)

//---------------------------------------------------------------------------
// TSerialTerminal
//---------------------------------------------------------------------------
__fastcall TSerialTerminal::TSerialTerminal(unsigned int AMaxLines, bool ALogToFile) : TObject() // 0 - Unlimited lines quantity
{
	MaxLines=AMaxLines;
	lines=new TStringList();
	busyevent=CreateEvent(NULL, true, false, NULL);
	memo=NULL;
	showlines=true;
	LogToFile=ALogToFile;
}
//---------------------------------------------------------------------------

__fastcall TSerialTerminal::~TSerialTerminal()
{
	CloseHandle(busyevent);
	delete lines;
	lines=NULL;
	if(LogFileStream) delete LogFileStream;
	LogFileStream=NULL;
	if(FileLines) delete FileLines;
	FileLines=NULL;
	memo=NULL;
}
//---------------------------------------------------------------------------

void __fastcall TSerialTerminal::writeLogToFile(bool value)
{
	if(LogFileStream) delete LogFileStream;
	LogFileStream=NULL;
	if(FileLines) delete FileLines;
	FileLines=NULL;
	if(value)
	{
		try
		{
			AnsiString FileName=(AnsiString)_RadarMapName+"_GNSS_Log.txt";
			LogFileStream=new TFileStream(FileName, fmCreate | fmShareExclusive);
			FileLines=new TStringList();
		}
		catch(Exception &e)
		{
			LogFileStream=NULL;
			FileLines=NULL;
		}
	}
}
//---------------------------------------------------------------------------

bool __fastcall TSerialTerminal::AddLine(AnsiString str, bool readwrite) //true - read, false - write
{
	if(WaitForSingleObject(BusyEvent, 10)!=WAIT_OBJECT_0 && lines)
	{
		SetEvent(BusyEvent);
		try
		{
			if(!readwrite) str="> "+str; // '>' is used to distinguish read or write operation
			else str="  "+str;
			if(MaxLines>0)
			{
				while((unsigned)lines->Count>(MaxLines-1)) lines->Delete(0);
			}
			lines->Add(str);
			if(LogFileStream && FileLines)
			{
				try
				{
					FileLines->Add(str);
					LogFileStream->Seek(0, soFromEnd);
					FileLines->SaveToStream(LogFileStream);
					FileLines->Clear();
				}
				catch(Exception &e) {}
			}
			if(memo && memo->Owner)
			{
				try
				{
					if(showlines && memo && ((TForm*)memo->Owner)->Visible)
					{
						if(memo) HideCaret(memo->Handle);
						if(memo) LockWindowUpdate(memo->Handle);
						if(MaxLines>0)
							while(memo && (unsigned)memo->Lines->Count>(MaxLines-1)) memo->Lines->Delete(0);
						if(memo) memo->Lines->Add(str);
						LockWindowUpdate(0);
					}
				}
				catch(Exception &e) {}
			}
		}
		__finally
		{
			ResetEvent(BusyEvent);
		}
		return true;
	}
	else return false;
}
//---------------------------------------------------------------------------

bool __fastcall TSerialTerminal::Clear()
{
	if(WaitForSingleObject(BusyEvent, 10)!=WAIT_OBJECT_0)
	{
		SetEvent(BusyEvent);
		try
		{
			lines->Clear();
		}
		__finally
		{
			ResetEvent(BusyEvent);
		}
		return true;
	}
	else return false;
}

UnicodeString __fastcall TSerialTerminal::readText()
{
	UnicodeString out;

	if(WaitForSingleObject(BusyEvent, 10)!=WAIT_OBJECT_0)
	{
		SetEvent(BusyEvent);
		try
		{
			out=lines->Text;
		}
		__finally
		{
			ResetEvent(BusyEvent);
		}
		return out;
	}
	else return "";
}

//---------------------------------------------------------------------------
// TSerialThread
//---------------------------------------------------------------------------
void __fastcall TSerialThread::AddReceivedData(char *Buf, int N)
{
	AnsiString *pstr=NULL;

	try
	{
		if(N>0 && Buf[0]!='\0')
		{
			Buf[N]='\0';
			pstr=new AnsiString(Buf);
			if(Owner && Owner->ReadBuf)
				Owner->ReadBuf->Push(pstr);
			else delete pstr;
		}
	}
	catch (Exception &e) {if(pstr) delete pstr;}
}
//---------------------------------------------------------------------------

void __fastcall TSerialThread::ExecuteLoopBody()
{
	AnsiString *pstr, str;
	unsigned long ul, s, z;
	int i;
	char Buf[MaxGpsBytesRead];

	try
	{
		if(Owner && Owner->WriteBuf && WaitForSingleObject(Owner->WriteBuf->NotEmptyEvent, 0)==WAIT_OBJECT_0) //Writing to device
		{
			try
			{
				pstr=(AnsiString*)Owner->WriteBuf->Pop();
				if(pstr!=NULL)
				{
					try
					{
						if(((TNmeaClient*)Owner)->Client && ((TNmeaClient*)Owner)->Client->Connected) ((TNmeaClient*)Owner)->Client->WriteLn(*pstr);
						else if(Owner->SerialHandle!=INVALID_HANDLE_VALUE)
						{
							s=0;
							i=0;
							while((int)s<pstr->Length() && CheckAttemptsQuantity(i))
							{
								if(!NmeaSettings->SimFileSource)
								{
									//EscapeCommFunction(Owner->SerialHandle,SETRTS);
									WriteFile(Owner->SerialHandle, (char*)(pstr->c_str()+s), 1, &ul, NULL);
									Sleep(50);//25);
									//EscapeCommFunction(Owner->SerialHandle,CLRRTS);
								}
								if(ul==0) i++;
								else s+=ul;
							}
							/*char *buf;
							int ll=pstr->Length();

							buf=new char[ll+2];
							for(int ii=0; ii<ll; ii++) buf[ii]=*(pstr->c_str()+ii);
							buf[ll]=13;//'\r';
							buf[ll+1]=10;
							while((int)s<pstr->Length() && CheckAttemptsQuantity(i))
							{
								if(!NmeaSettings->SimFileSource)
								{
									//EscapeCommFunction(Owner->SerialHandle,SETRTS);

									WriteFile(Owner->SerialHandle, (char*)(buf+s), 1, &ul, NULL);
									//EscapeCommFunction(Owner->SerialHandle,CLRRTS);
								}
								if(ul==0) i++;
								else s+=ul;
							}
							delete[] buf;*/
						}
					}
					__finally
					{
						if(Owner && ((TNmeaClient*)Owner)->Terminal && !Terminated)
							((TNmeaClient*)Owner)->Terminal->AddLine(*pstr, false);
						delete pstr;
					}
				}
			}
			catch(Exception &e) {}
		}
		pstr=NULL;
		if(WaitForSingleObject(SuspendedEvent, 0)!=WAIT_OBJECT_0) //Reading from device
		{
			try
			{
				i=0;
				if(((TNmeaClient*)Owner)->Client && ((TNmeaClient*)Owner)->Client->Connected)
				{
					s=0;
					z=0;
					do
					{
						try
						{
							Buf[i]=((TNmeaClient*)Owner)->Client->ReadByte();
							ul=1;
						}
						catch(...) {ul=0;}
						if(ul==0)
						{
							if(!z) WaitForSingleObject(IdleEvent, 50); //250);
							else s++;
							z++;
						}
						else if(Buf[i]=='\r' || Buf[i]=='\n') s++;
						else i++;
					} while(i<MaxGpsBytesRead && s==0 && !CheckTermination());// && s<MaxGpsReadWriteAttempts //
				}
				else if(Owner->SerialHandle!=INVALID_HANDLE_VALUE)
				{
					s=0;
					z=0;
					do
					{
						ReadFile(Owner->SerialHandle, (char*)(Buf+i), 1, &ul, NULL);
						if(ul==0)
						{
							if(!z) WaitForSingleObject(IdleEvent, 50); //250);
							else s++;
							z++;
						}
						else if(Buf[i]=='\r' || Buf[i]=='\n') s++;
						else i++;
					} while(i<MaxGpsBytesRead && s==0 && !CheckTermination());// && s<MaxGpsReadWriteAttempts //

					if(NmeaSettings->SimFileSource)
					{
						if(ul==0) SetFilePointer(Owner->SerialHandle, 0, NULL, FILE_BEGIN);
						MySleep(333); //33); //1000); //500); //
					}
				}
				//Buf[i+1]='\0';
				//AddReceivedData(Buf, i+1);
				if(i>=MaxGpsBytesRead) i=MaxGpsBytesRead-1;
				else if(i>0) i++; //to keep '\r' or '\n' at the end of string
				Buf[i]='\0';
				AddReceivedData(Buf, i);
				if(i>0 && Owner && ((TNmeaClient*)Owner)->Terminal && !Terminated)
					((TNmeaClient*)Owner)->Terminal->AddLine((AnsiString)Buf, true);
			}
			catch(Exception &e) {}
		}
	}
	catch(Exception &e) {}
	WaitForSingleObject(IdleEvent, 2);
	//MySleep(10);
}

//---------------------------------------------------------------------------
// TNmeaClient
//---------------------------------------------------------------------------
__fastcall TNmeaClient::TNmeaClient(TObject* AManager, TNmeaReceiverSettings *ANmeaSettings) : TCustomSerialClient (AManager)
{
	NmeaSettings=ANmeaSettings;
	nmeasummary=NULL;
	Name=NmeaSettings->DeviceName;
	terminal=new TSerialTerminal(NmeaSettings->MaxTerminalLines, NmeaSettings->LogToFile);
	SerialThrd=new TSerialThread(this, Manager, NmeaSettings);
	CommHandle=INVALID_HANDLE_VALUE;
	InitCommands=new TList();
	initializedevent=CreateEvent(NULL, true, false, NULL);
	InitThrd=NULL;
	clientstarted=false;

	Connect();
}
//---------------------------------------------------------------------------

__fastcall TNmeaClient::~TNmeaClient()
{
	ClearInitCommands();
	TerminateThreads();
	if(readConnected()) Disconnect();
	delete terminal;
	terminal=NULL;
	CloseHandle(initializedevent);
	delete InitCommands;
	if(nmeasummary) delete nmeasummary;
}
//---------------------------------------------------------------------------

void __fastcall TNmeaClient::TerminateThreads()
{
	if(InitThrd)
	{
		InitThrd->AskForSuspend();
		InitThrd->AskForTerminate();
		WaitOthers();
		if(InitThrd->ForceTerminate()) delete InitThrd;
		InitThrd=NULL;
	}
	if(SerialThrd)
	{
		SerialThrd->Stop();
		SerialThrd->AskForTerminate();
		WaitOthers();
		if(SerialThrd->ForceTerminate()) delete SerialThrd;
		SerialThrd=NULL;
	}
}
//---------------------------------------------------------------------------

void __fastcall TNmeaClient::Disconnect()
{
	Stop();
	if(Client) Client->Disconnect();
	else
	{
		CloseHandle(CommHandle);
		CommHandle=INVALID_HANDLE_VALUE;
	}
}
//---------------------------------------------------------------------------

void __fastcall TNmeaClient::Connect()
{
	connected=false;
	if(SerialThrd) SerialThrd->Stop();
	WaitOthers();
	if(NmeaSettings->SimTcpSource)
	{
		client=new TTCPClient(NULL);
		client->ReadTimeout=1;
		if(!Client->Connect(NmeaSettings->SimTcpAddr, 24, 250))
			ShowConnectionError("The GPS Simulator "+NmeaSettings->SimTcpAddr+" is unavailable now!");
	}
	else
	{
		AnsiString GpsSource;
		try
		{
			if(CommHandle!=INVALID_HANDLE_VALUE) CloseHandle(CommHandle);
		}
		catch(Exception &e) {}
		if(NmeaSettings->SimFileSource)
		{
			GpsSource=NmeaSettings->SimFileName;
			//ShowConnectionError(GpsSource);
		}
		else GpsSource="\\\\.\\"+NmeaSettings->ComPort;
		CommHandle=CreateFile(GpsSource.c_str(), GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
		if(CommHandle==INVALID_HANDLE_VALUE)
			ShowConnectionError("The NMEA Receiver port "+NmeaSettings->ComPort+" is unavailable now!"); //NmeaSettings->ComPort
		else
		{
			if(!NmeaSettings->SimFileSource)
			{
				COMMCONFIG GPSCom;
				COMMTIMEOUTS GPSComTO;
				unsigned long a=sizeof(GPSCom);

				GetCommConfig(CommHandle, &GPSCom, &a);
				GPSCom.dcb.DCBlength = sizeof(GPSCom.dcb);
				GPSCom.dcb.BaudRate=NmeaSettings->PortSpeed;
				//GPSCom.dcb.fBinary=true;
				GPSCom.dcb.fParity=true;
				//GPSCom.dcb.fAbortOnError=false;
				GPSCom.dcb.ByteSize=8;
				GPSCom.dcb.Parity=NOPARITY;
				GPSCom.dcb.StopBits=ONESTOPBIT;
				/*if(NmeaSettings->RTSCTSEnable)
				{
					GPSCom.dcb.fOutxCtsFlow=true;
					GPSCom.dcb.fRtsControl=RTS_CONTROL_HANDSHAKE;
				}
				else
				{
					GPSCom.dcb.fOutxCtsFlow=false;
					GPSCom.dcb.fRtsControl=RTS_CONTROL_DISABLE;
				}
				/*GPSCom.dcb.fBinary=true;
				GPSCom.dcb.fParity=true;//false;
				GPSCom.dcb.fOutxCtsFlow=false;
				GPSCom.dcb.fOutxDsrFlow=false;
				GPSCom.dcb.fDtrControl=DTR_CONTROL_DISABLE;
				GPSCom.dcb.fDsrSensitivity=false;
				GPSCom.dcb.fTXContinueOnXoff=true;
				GPSCom.dcb.fOutX=false;
				GPSCom.dcb.fInX=false;
				GPSCom.dcb.fErrorChar=false;
				GPSCom.dcb.fNull=false;
				//GPSCom.dcb.fRtsControl=RTS_CONTROL_HANDSHAKE;
				GPSCom.dcb.fAbortOnError=false;
				GPSCom.dcb.XonLim=0;
				GPSCom.dcb.XoffLim=0;
				GPSCom.dcb.ByteSize=8;
				GPSCom.dcb.Parity=NOPARITY;
				GPSCom.dcb.StopBits=ONESTOPBIT;
				GPSCom.dcb.XonChar=0;
				GPSCom.dcb.XoffChar=0;
				GPSCom.dcb.ErrorChar=0;
				GPSCom.dcb.EofChar=0;
				GPSCom.dcb.EvtChar=0;
				if(NmeaSettings->RTSCTSEnable)
				{
					GPSCom.dcb.fRtsControl=RTS_CONTROL_ENABLE;
				}
				else
				{
					GPSCom.dcb.fRtsControl=RTS_CONTROL_DISABLE;
				}*/
				SetCommConfig(CommHandle, &GPSCom, sizeof(GPSCom));
				//-- Serial port timeout settings ---------------
				GetCommTimeouts(CommHandle, &GPSComTO);
				GPSComTO.ReadIntervalTimeout=1;
				GPSComTO.ReadTotalTimeoutConstant=10;
				GPSComTO.ReadTotalTimeoutMultiplier=1;
				GPSComTO.WriteTotalTimeoutMultiplier=1;//0;
				GPSComTO.WriteTotalTimeoutConstant=10;
				SetCommTimeouts(CommHandle, &GPSComTO);
			}
			connected=true;
		}
	}
	if(SerialThrd)
	{
		if(Connected) SerialThrd->Start();
		else SerialThrd->Stop();
	}
}
//---------------------------------------------------------------------------

void __fastcall TNmeaClient::LoadCommandsFromFile(AnsiString FileName)
{
	TXMLExplorer *XMLExplorer;
	AnsiString str, str2, str3, str4;
	TGpsListenerCommand* cmd;
	TList *params;
	TGpsListenerCommandParam *param;
	AnsiString regkey=_RadarMapRegKey+(AnsiString)_GpsUnitsRegKey;

	params=new TList();
	try
	{
		ClearInitCommands();
		if(FileName!="" && FileExists(FileName))
		{
			XMLExplorer=new TXMLExplorer(NULL, FileName);
			TRegistry &regKey=*new TRegistry();
			try
			{
				if(XMLExplorer->FindTagStart("GpsUnit"))
				{
					if(XMLExplorer->FindTagAndGetContent("UnitName", str)) name=str;
					while(XMLExplorer->GetNextStartTagInside("Initializing", str))
					{
						if(str=="Command")
						{
							if(params) params->Clear();
							cmd=NULL;
							while(XMLExplorer->GetNextStartTagInside(str, str2))
							{
								if(str2=="Param")
								{
									str3=str2;
									param=NULL;
									while(XMLExplorer->GetNextStartTagInside(str3, str2))
									{
										if(str2=="Name")
										{
											if(XMLExplorer->GetContent(str4) && str4!="")
											{
												if(!param) param=new TGpsListenerCommandParam();
												param->Name=str4;
											}
										}
										else if(str2=="Description")
										{
											if(XMLExplorer->GetContent(str4) && str4!="")
											{
												if(!param) param=new TGpsListenerCommandParam();
												param->Description=str4;
											}
										}
										else if(str2=="Value")
										{
											if(XMLExplorer->GetContent(str4) && str4!="")
											{
												if(!param) param=new TGpsListenerCommandParam();
												param->Value=str4;
											}
										}
									}
                                    if(param && param->Name!="")
									{
										if(name!="")
										{
											if(regKey.OpenKey(regkey+"\\"+name, false))
											{
												if(regKey.ValueExists(param->Name))
													param->Value=regKey.ReadString(param->Name);
											}
											regKey.CloseKey();
											if(cmd) cmd->AddParam(param);
											else if(params) params->Add(param);
										}
										else delete param;
									}
								}
								else if(str2=="Query")
								{
									if(XMLExplorer->GetContent(str2) && str2!="")
										cmd=new TGpsListenerCommand(str2, params);
								}
								else if(str2=="Reply")
								{
									if(cmd && XMLExplorer->GetContent(str3) && str3!="") cmd->AddReply(str3.c_str());
									else break;
								}
							}
							if(cmd)
							{
								InitCommands->Add(cmd);
								if(params) params->Clear();
							}
						}
					}
				}
			}
			__finally
			{
				delete &regKey;
				delete XMLExplorer;
			}
		}
	}
	__finally
	{
		if(params)
		{
			for(int i=0; i<params->Count; i++)
				if(params->Items[i]) delete (TGpsListenerCommandParam*)params->Items[i];
			params->Clear();
			delete params;
		}

	}
}
//---------------------------------------------------------------------------

bool __fastcall TNmeaClient::AddSingleCommandWithReply(TGpsListenerCommand *cmd)
{
	bool res=false;
	if(cmd && cmd->Query!="" && (!NmeaSettings->SimFileSource && !NmeaSettings->SimTcpSource))
	{
		if(!Connected) Connect();
		if(Connected)
		{
			if(InitThrd && !Initialized && InitCommands->Count>0)
			{
				InitCommands->Add(cmd);
			}
			else
			{
				if(InitThrd)
				{
					InitThrd->AskForSuspend();
					InitThrd->AskForTerminate();
					WaitOthers();
					if(InitThrd->ForceTerminate()) delete InitThrd;
					InitThrd=NULL;
				}
				ResetEvent(initializedevent);
				ClearInitCommands();
				InitCommands->Add(cmd);
				InitThrd=new TNmeaInitializingThread(this, Manager, NmeaSettings);
				InitThrd->Start();
			}
			res=true;
		}
	}/**/
	return res;
}
//---------------------------------------------------------------------------

void __fastcall TNmeaClient::ClearCommandsSequence(bool WaitUntilPreviousEnds)
{
	if(WaitUntilPreviousEnds)
	{
		while(InitThrd && !Initialized && InitCommands->Count>0)
			WaitForSingleObject(initializedevent, 10);
	}
	if(InitThrd)
	{
		InitThrd->AskForSuspend();
		InitThrd->AskForTerminate();
		WaitOthers();
		if(InitThrd->ForceTerminate()) delete InitThrd;
		InitThrd=NULL;
	}
	ResetEvent(initializedevent);
	ClearInitCommands();
}
//---------------------------------------------------------------------------

void __fastcall TNmeaClient::AddCommandWithReplyToSequence(TGpsListenerCommand *cmd)
{
	InitCommands->Add(cmd);
}
//---------------------------------------------------------------------------

bool __fastcall TNmeaClient::StartCommandsSequence()
{
	bool res=false;

	try
	{
		if(!Connected) Connect();
		if(Connected)
		{
			InitThrd=new TNmeaInitializingThread(this, Manager, NmeaSettings);
			InitThrd->Start();
			res=true;
		}
	}
	catch(...) {}

	return res;

}
//---------------------------------------------------------------------------

void __fastcall TNmeaClient::Initialize()
{
	if(nmeasummary)
	{
		delete nmeasummary;
		nmeasummary=NULL;
	}
	if(NmeaSettings->InitOnStart && InitCommands->Count>0 && (!NmeaSettings->SimFileSource && !NmeaSettings->SimTcpSource))
	{
		if(InitThrd)
		{
			InitThrd->AskForSuspend();
			InitThrd->AskForTerminate();
			WaitOthers();
			if(InitThrd->ForceTerminate()) delete InitThrd;
			InitThrd=NULL;
		}
		ResetEvent(initializedevent);
		InitThrd=new TNmeaInitializingThread(this, Manager, NmeaSettings);
		InitThrd->Start();
	}
	else SetEvent(initializedevent);
	nmeasummary=new TNmeaSummaryThread(NmeaSettings, Manager);
	nmeasummary->Owner=this;
	nmeasummary->Set(nstGGA);
	nmeasummary->Set(nstGSA);
	nmeasummary->Set(nstGST);
	nmeasummary->Set(nstLLQ);
	nmeasummary->Set(nstCTMOTION);
	nmeasummary->Set(nstHDT);
	//nmeasummary->Set(nstTWPOS);
	//nmeasummary->Set(nstTWHPR);
}
//---------------------------------------------------------------------------

bool __fastcall TNmeaClient::CheckCRC(AnsiString str, unsigned int crc)
{
	return true;
}
//---------------------------------------------------------------------------

TNmeaSentenceType __fastcall TNmeaClient::NmeaParser(HANDLE ThreadIdleEvent) // (AnsiString Nmea)
{
	char Buf[128];
	AnsiString s1, *pstr, Line, crcStr;
	int i, ci;
	TNmeaSentenceType res=nstNone;

	try
	{
		if(WaitForSingleObject(InitializedEvent, 0)==WAIT_OBJECT_0 && ReadBuf && ReadBuf->Count>0)
		{
			pstr=(AnsiString*)GetReceivedData();
			if(pstr!=NULL && *pstr!="")
			{
				Line=*pstr;
				delete pstr;
				i=Line.Pos("$");
				ci=Line.Pos("*");
				if(i>0 && ci>i && Line.Length()-ci>1)
				{
					crcStr=Line.SubString(ci+1, Line.Length()-ci);
					Line=Line.SubString(i, ci-i); //NMEA sequence without '*'+CRC
					if(NmeaSettings->SimFileSource || CheckCRC(Line, HexToInt(crcStr)))
					{
						res=ParseNmeaSequence(Line);
					}
				}
			}
		}
	}
	catch(Exception &e) {}

	return res;
}
//---------------------------------------------------------------------------

TNmeaSentenceType __fastcall TNmeaClient::ParseNmeaSequence(AnsiString s1)
{
	TNmeaSentenceType res=nstNone;

	if(s1.Pos("GGA,")>0)
	{
		res=nstGGA;
		ParseGPSDataGGA((TNmeaGGA*)NmeaSummary->Get(res), s1);
	}
	else if(s1.Pos("GSA,")>0)
	{
		res=nstGSA;
		ParseGPSDataGSA((TNmeaGSA*)NmeaSummary->Get(res), s1);
	}
	else if(s1.Pos("GST,")>0)
	{
		res=nstGST;
		ParseGPSDataGST((TNmeaGST*)NmeaSummary->Get(res), s1);
	}
	else if(s1.Pos("LLQ,")>0)
	{
		res=nstLLQ;
		ParseGPSDataLLQ((TNmeaLLQ*)NmeaSummary->Get(res), s1);
	}
	else if(s1.Pos("CTMOTION,")>0)
	{
		res=nstCTMOTION;
		ParseGPSDataCTMOTION((TNmeaCTMOTION*)NmeaSummary->Get(res), s1);
	}
	else if(s1.Pos("HDT,")>0)
	{
		res=nstHDT;
		ParseGPSDataHDT((TNmeaHDT*)NmeaSummary->Get(res), s1);
	}
	else res=nstUnknown;
	if(res!=nstUnknown)
	{
		if(!NmeaSummary->Rebuild(res)) res=nstUnknown;
	}
	return res;
}
//---------------------------------------------------------------------------

AnsiString __fastcall TNmeaClient::GetNextNmeaParam(bool &EndFound, AnsiString *str)
{
	AnsiString res="";
	int i;

	if(str && str->Length()>0)
	{
		i=str->Pos(",");
		if(i==0)
		{
			i=str->Length()+1;
			EndFound=true;
		}
		res=str->SubString(1, i-1);
		*str=str->SubString(i+1, str->Length()-i);
	}
	return res;
}
//---------------------------------------------------------------------------

void __fastcall TNmeaClient::ParseGPSDataGGA(TNmeaGGA *out, AnsiString str)
{
	int i, gi, zz;
	AnsiString s1, s2, Line;

	Line=str;
	i=Line.Pos("GGA,");
	if(out && i>0 && Line.Length()-i-4>0)
	{
		try
		{
			Line=Line.SubString(i+4, Line.Length()-i-3);
			out->Corrupted=false;
			out->EndFound=false;
			//Greenwich time
			s2=GetNextNmeaParam(out->EndFound, &Line);
			if(s2!="" && s2.Length()>0)
			{
				gi=zz=1;
				s1="  .  .  .   ";
				while(gi<=s1.Length() && zz<=s2.Length() && ((s2[zz]>='0' && s2[zz]<='9') || s2[zz]=='.'))
				{
					if(s1[gi]=='.') gi++;
					s1[gi]=s2[zz];
					gi++;
					zz++;
					if(zz<=s2.Length() && s2[zz]=='.') zz++;
				}
				if(zz<7) out->Corrupted=true;
				else
				{
					while(s1.Length()>7 && (s1[s1.Length()]<'0' || s1[s1.Length()]>'9'))
						s1.SetLength(s1.Length()-1);
					if(s1.Length()>7)
					{
						try {out->Tm=MyStrToTime(s1, "hh.nn.ss.zz", '.');}
						catch(Exception &exception) {out->Corrupted=true;}
					}
					else out->Corrupted=true;
				}
			}
			else out->Tm=0;
			//Latitude
			if(!out->EndFound && !out->Corrupted)
			{
				s2=GetNextNmeaParam(out->EndFound, &Line);
				if(s2!="") try {out->Lat=StrToFloat(s2);} catch(Exception &exception) {out->Corrupted=true;}
				else out->Corrupted=true;
			}
			//North or South
			if(!out->EndFound && !out->Corrupted)
			{
				s2=GetNextNmeaParam(out->EndFound, &Line);
				if(s2!="" && s2=="S") out->Lat*=-1;
				else if(s2=="" || s2!="N") out->Corrupted=true;
			}
			//Longitude
			if(!out->EndFound && !out->Corrupted)
			{
				s2=GetNextNmeaParam(out->EndFound, &Line);
				if(s2!="") try {out->Lon=StrToFloat(s2);} catch(Exception &exception) {out->Corrupted=true;}
				else out->Corrupted=true;
			}
			//East or West
			if(!out->EndFound && !out->Corrupted)
			{
				s2=GetNextNmeaParam(out->EndFound, &Line);
				if(s2!="" && s2=="W") out->Lon*=-1;
				else if(s2=="" || s2!="E") out->Corrupted=true;
			}
			//Quality
			if(!out->EndFound && !out->Corrupted)
			{
				s2=GetNextNmeaParam(out->EndFound, &Line);
				if(s2!="" && s2[1]>='0' && s2[1]<='9') out->Quality=s2[1]-'0';
				else out->Corrupted=true;
			}
			else out->Quality=-1;
			if(Manager && ((TRadarMapManager*)Manager)->Settings &&
				(((TRadarMapManager*)Manager)->Settings->GPS.SimFileSource ||
				((TRadarMapManager*)Manager)->Settings->GPS.SimTcpSource))
				out->Quality=8;
			//Number of sattelites
			if(!out->EndFound && !out->Corrupted)
			{
				s2=GetNextNmeaParam(out->EndFound, &Line);
				if(s2!="") try {out->Satellites=StrToInt(s2);} catch(Exception &exception) {out->Satellites=-1;}
			}
			else out->Satellites=-1;
			//HDOP
			if(!out->EndFound && !out->Corrupted)
			{
				s2=GetNextNmeaParam(out->EndFound, &Line);
				if(s2!="") try {out->HDOP=StrToFloat(s2);} catch(Exception &exception) {out->HDOP=-1;}
				else out->HDOP=-1;
			}
			//Altitude
			if(!out->EndFound && !out->Corrupted)
			{
				s2=GetNextNmeaParam(out->EndFound, &Line);
				if(s2!="") try {out->Z=StrToFloat(s2);} catch(Exception &exception) {out->Corrupted=true;}
				else out->Corrupted=true;
			}
			//Altitude measurement units
			if(!out->EndFound && !out->Corrupted)
			{
				s2=GetNextNmeaParam(out->EndFound, &Line);
				if(s2!="" && s2=="K") out->Z*=1000.0;
				else if(s2=="" || s2!="M") out->Corrupted=true;
			}
			//Altitude Geoid
			if(!out->EndFound && !out->Corrupted)
			{
				s2=GetNextNmeaParam(out->EndFound, &Line);
				if(s2!="") try {out->Z_geoid=StrToFloat(s2);} catch(Exception &exception) {out->Z_geoid=0;}
				else out->Z_geoid=0.;
			}
			//Geoid measurement units
			if(!out->EndFound && !out->Corrupted)
			{
				s2=GetNextNmeaParam(out->EndFound, &Line);
				if(s2!="" && s2=="K") out->Z_geoid*=1000.0;
				else if(s2=="" || s2!="M") out->Z_geoid=0.;//out->Corrupted=true;
			}
			//time in seconds since last DGPS update
			if(!out->EndFound && !out->Corrupted)
			{
				s2=GetNextNmeaParam(out->EndFound, &Line);
				if(s2!="") try {out->DelaySinceLastUpdate=StrToFloat(s2);} catch(Exception &exception) {out->DelaySinceLastUpdate=-1;}
				else out->DelaySinceLastUpdate=-1;
			}
			out->Refreshed=true;
		}
		catch(Exception &e) {try {if(out) out->Corrupted=true;} catch(...) {}}
	 }
}
//---------------------------------------------------------------------------

//$GNGSA,A,3,20,11,13,23,17,04,31,,,,,,1.6,0.9,1.3*21
//$GNGSA,A,3,81,83,68,,,,,,,,,,1.6,0.9,1.3*2C
void __fastcall TNmeaClient::ParseGPSDataGSA(TNmeaGSA *out, AnsiString str)
{
	int i, gi;
	AnsiString s1, s2, Line;

	Line=str;
	i=Line.Pos("GSA,");
	if(out && i>0 && Line.Length()-i-4>0)
	{
		try
		{
			Line=Line.SubString(i+4, Line.Length()-i-3);
			out->Corrupted=false;
			out->EndFound=false;
			//Auto selection of 2D or 3D fix (M = manual)
			s2=GetNextNmeaParam(out->EndFound, &Line);
			if(s2!="" && (s2=="A" || s2=="M")) out->Selection=s2[1];
			else out->Corrupted=true;
			//3D fix
			if(!out->EndFound && !out->Corrupted)
			{
				s2=GetNextNmeaParam(out->EndFound, &Line);
				if(s2!="" && s2[1]>='0' && s2[1]<='9') out->FixType=s2[1]-'0';
				else out->Corrupted=true;
			}
			//PRNs of satellites used for fix (space for 12)
			for(gi=0; gi<12; gi++)
			{
				if(!out->EndFound && !out->Corrupted)
				{
					s2=GetNextNmeaParam(out->EndFound, &Line);
					if(s2!="") try {out->PRNs[gi]=StrToInt(s2);} catch(Exception &exception) {out->Corrupted=true;}
					else out->PRNs[gi]=0;
				}
				else out->PRNs[gi]=0;
			}
			//PDOP
			if(!out->EndFound && !out->Corrupted)
			{
				s2=GetNextNmeaParam(out->EndFound, &Line);
				if(s2!="") try {out->PDOP=StrToFloat(s2);} catch(Exception &exception) {out->Corrupted=true;}
				else out->Corrupted=true;
			}
			//HDOP
			if(!out->EndFound && !out->Corrupted)
			{
				s2=GetNextNmeaParam(out->EndFound, &Line);
				if(s2!="") try {out->HDOP=StrToFloat(s2);} catch(Exception &exception) {out->Corrupted=true;}
				else out->Corrupted=true;
			}
			//VDOP
			if(!out->EndFound && !out->Corrupted)
			{
				s2=GetNextNmeaParam(out->EndFound, &Line);
				if(s2!="") try {out->VDOP=StrToFloat(s2);} catch(Exception &exception) {out->Corrupted=true;}
				else out->Corrupted=true;
			}
			out->Refreshed=true;
		}
		catch(Exception &e) {try {if(out) out->Corrupted=true;} catch(...) {}}
	}
}
//---------------------------------------------------------------------------

void __fastcall TNmeaClient::ParseGPSDataGST(TNmeaGST *out, AnsiString str)
{
	int i, gi, zz;
	AnsiString s1, s2, Line;

	Line=str;
	i=Line.Pos("GST,");
	if(out && i>0 && Line.Length()-i-4>0)
	{
		try
		{
			Line=Line.SubString(i+4, Line.Length()-i-3);
			out->Corrupted=false;
			out->EndFound=false;
			//Greenwich time
			s2=GetNextNmeaParam(out->EndFound, &Line);
			if(s2!="" && s2.Length()>0)
			{
				gi=zz=1;
				s1="  .  .  .   ";
				while(gi<=s1.Length() && zz<=s2.Length() && ((s2[zz]>='0' && s2[zz]<='9') || s2[zz]=='.'))
				{
					if(s1[gi]=='.') gi++;
					s1[gi]=s2[zz];
					gi++;
					zz++;
					if(zz<=s2.Length() && s2[zz]=='.') zz++;
				}
				if(zz<7) out->Corrupted=true;
				else
				{
					try {out->Tm=MyStrToTime(s1, "hh.nn.ss.zzz", '.'); }
					catch(Exception &exception) {out->Corrupted=true;}
				}
			}
			else out->Tm=0;
			//RMS
			if(!out->EndFound && !out->Corrupted)
			{
				s2=GetNextNmeaParam(out->EndFound, &Line);
				if(s2!="") try {out->RMS=StrToFloat(s2);} catch(Exception &exception) {out->Corrupted=true;}
				else out->Corrupted=true;
			}
			//Standard deviation of semi-major axis of error ellipse
			if(!out->EndFound && !out->Corrupted)
			{
				s2=GetNextNmeaParam(out->EndFound, &Line);
				if(s2!="") try {out->MajorDiv=StrToFloat(s2);} catch(Exception &exception) {out->MajorDiv=-1;}
				else out->MajorDiv=-1;
			}
			//Standard deviation of semi-minor axis of error ellipse
			if(!out->EndFound && !out->Corrupted)
			{
				s2=GetNextNmeaParam(out->EndFound, &Line);
				if(s2!="") try {out->MinorDiv=StrToFloat(s2);} catch(Exception &exception) {out->MinorDiv=-1;}
				else out->MinorDiv=-1;
			}
			//Orientation of semi-major axis of error ellipse, in degrees from true North
			if(!out->EndFound && !out->Corrupted)
			{
				s2=GetNextNmeaParam(out->EndFound, &Line);
				if(s2!="") try {out->OrientationToNorth=StrToFloat(s2);} catch(Exception &exception) {out->OrientationToNorth=-1;}
				else out->OrientationToNorth=-1;
			}
			//Standard deviation of latitude error, in meters
			if(!out->EndFound && !out->Corrupted)
			{
				s2=GetNextNmeaParam(out->EndFound, &Line);
				if(s2!="") try {out->LatDiv=StrToFloat(s2);} catch(Exception &exception) {out->LatDiv=-1;}
				else out->LatDiv=-1;
			}
			//Standard deviation of longitude error, in meters
			if(!out->EndFound && !out->Corrupted)
			{
				s2=GetNextNmeaParam(out->EndFound, &Line);
				if(s2!="") try {out->LonDiv=StrToFloat(s2);} catch(Exception &exception) {out->LonDiv=-1;}
				else out->LonDiv=-1;
			}
			//Standard deviation of altitude error, in meters
			if(!out->EndFound && !out->Corrupted)
			{
				s2=GetNextNmeaParam(out->EndFound, &Line);
				if(s2!="") try {out->ZDiv=StrToFloat(s2);} catch(Exception &exception) {out->ZDiv=-1;}
				else out->ZDiv=-1;
			}
			out->Refreshed=true;
		}
		catch(Exception &e) {try {if(out) out->Corrupted=true;} catch(...) {}}
	}
}
//---------------------------------------------------------------------------

void __fastcall TNmeaClient::ParseGPSDataLLQ(TNmeaLLQ *out, AnsiString str)
{
	int i, gi, zz;
	AnsiString s1, s2, Line;

	Line=str;
	i=Line.Pos("LLQ,");
	if(out && i>0 && Line.Length()-i-4>0)
	{
		try
		{
			Line=Line.SubString(i+4, Line.Length()-i-3);
			out->Corrupted=false;
			out->EndFound=false;
			//UTC time
			s2=GetNextNmeaParam(out->EndFound, &Line);
			if(s2!="" && s2.Length()>0)
			{
				gi=zz=1;
				s1="  .  .  .   ";
				while(gi<=s1.Length() && zz<=s2.Length() && ((s2[zz]>='0' && s2[zz]<='9') || s2[zz]=='.'))
				{
					if(s1[gi]=='.') gi++;
					s1[gi]=s2[zz];
					gi++;
					zz++;
					if(zz<=s2.Length() && s2[zz]=='.') zz++;
				}
				if(zz<7) out->Corrupted=true;
				else
				{
					try {out->Tm=MyStrToTime(s1, "hh.nn.ss.zzz", '.');}
					catch(Exception &exception) {out->Corrupted=true;}
				}
			}
			else out->Tm=0;
			//UTC Date
			if(!out->EndFound && !out->Corrupted)
			{
				s2=GetNextNmeaParam(out->EndFound, &Line);
				if(s2!="" && s2.Length()>0)
				{
					s1="  .  .20  ";
					gi=zz=1;
					while(gi<=s1.Length() && zz<=s2.Length() && s2[zz]>='0' && s2[zz]<='9')
					{
						if(s1[gi]==' ')
						{
							s1[gi]=s2[zz];
							zz++;
						}
						gi++;
					}
					if(zz<7) out->Corrupted=true;
					else
					{
						try	{out->Dt=MyStrToDate(s1, "MM.dd.yyyy", '.');}
						catch(Exception &exception) {out->Corrupted=true;}
					}
				}
				else out->Dt=0;
			}
			//Grid Easting in meters
			if(!out->EndFound && !out->Corrupted)
			{
				s2=GetNextNmeaParam(out->EndFound, &Line);
				if(s2!="") try {out->Easting=StrToFloat(s2);} catch(Exception &exception) {out->Corrupted=true;}
				else out->Corrupted=true;
			}
			//Units of grid Easting, fixed "M"eters
			s2=GetNextNmeaParam(out->EndFound, &Line);
			//Northing
			if(!out->EndFound && !out->Corrupted)
			{
				s2=GetNextNmeaParam(out->EndFound, &Line);
				if(s2!="") try {out->Northing=StrToFloat(s2);} catch(Exception &exception) {out->Corrupted=true;}
				else out->Corrupted=true;
			}
			//Units of grid Northing, fixed "M"eters
			s2=GetNextNmeaParam(out->EndFound, &Line);
			//Quality
			if(!out->EndFound && !out->Corrupted)
			{
				s2=GetNextNmeaParam(out->EndFound, &Line);
				if(s2!="" && s2[1]>='0' && s2[1]<='9') try {out->Quality=s2[1]-'0';} catch(Exception &exception) {out->Corrupted=true;}
				else out->Quality=-1;
			}
			if(Manager && ((TRadarMapManager*)Manager)->Settings &&
				(((TRadarMapManager*)Manager)->Settings->GPS.SimFileSource ||
				((TRadarMapManager*)Manager)->Settings->GPS.SimTcpSource))
				out->Quality=8;
			//Number of sattelites
			if(!out->EndFound && !out->Corrupted)
			{
				s2=GetNextNmeaParam(out->EndFound, &Line);
				if(s2!="") try {out->Satellites=StrToInt(s2);} catch(Exception &exception) {out->Satellites=-1;}
				else out->Satellites=-1;
			}
			//Coordinate quality
			if(!out->EndFound && !out->Corrupted)
			{
				s2=GetNextNmeaParam(out->EndFound, &Line);
				if(s2!="") try {out->CoordinateQuality=StrToFloat(s2);} catch(Exception &exception) {out->CoordinateQuality=-1;}
				else out->CoordinateQuality=-1;
			}
			//Altitude
			if(!out->EndFound && !out->Corrupted)
			{
				s2=GetNextNmeaParam(out->EndFound, &Line);
				if(s2!="") try {out->Z=StrToFloat(s2);} catch(Exception &exception) {out->Corrupted=true;}
				else out->Corrupted=true;
			}
			out->Refreshed=true;
		}
		catch(Exception &e) {try {if(out) out->Corrupted=true;} catch(...) {}}
	}
}
//---------------------------------------------------------------------------

void __fastcall TNmeaClient::ParseGPSDataCTMOTION(TNmeaCTMOTION *out, AnsiString str)
{
	int i, gi, zz;
	AnsiString s1, s2, Line;

	Line=str;
	i=Line.Pos("CTMOTION,");
	if(out && i>0 && Line.Length()-i-9>0)
	{
		try
		{
			Line=Line.SubString(i+9, Line.Length()-i-3);
			out->Corrupted=false;
			out->EndFound=false;
			//Pitch
			s2=GetNextNmeaParam(out->EndFound, &Line);
			if(s2!="") try {out->Pitch=StrToFloat(s2);} catch(Exception &exception) {out->Corrupted=true;}
			else out->Corrupted=true;
			//Roll
			if(!out->EndFound && !out->Corrupted)
			{
				s2=GetNextNmeaParam(out->EndFound, &Line);
				if(s2!="") try {out->Roll=StrToFloat(s2);} catch(Exception &exception) {out->Corrupted=true;}
				else out->Corrupted=true;
			}
			out->Refreshed=true;
		}
		catch(Exception &e) {try {if(out) out->Corrupted=true;} catch(...) {}}
	}
}
//---------------------------------------------------------------------------

//$GPHDT,78.43*2C
void __fastcall TNmeaClient::ParseGPSDataHDT(TNmeaHDT *out, AnsiString str)
{
	int i, gi, zz;
	AnsiString s1, s2, Line;

	Line=str;
	i=Line.Pos("HDT,");
	if(out && i>0 && Line.Length()-i-4>0)
	{
		try
		{
			Line=Line.SubString(i+4, Line.Length()-i-3);
			out->Corrupted=false;
			out->EndFound=false;
			//Heading
			s2=GetNextNmeaParam(out->EndFound, &Line);
			if(s2!="") try {out->Heading=StrToFloat(s2);} catch (Exception &exception) {out->Corrupted=true;}
			else out->Corrupted=true;
			//True North
			if(!out->EndFound && !out->Corrupted)
			{
				s2=GetNextNmeaParam(out->EndFound, &Line);
				if(s2!="T") out->Corrupted=true;
            }
			out->Refreshed=true;
		}
		catch(Exception &e) {try {if(out) out->Corrupted=true;} catch(...) {}}
	}
}

//---------------------------------------------------------------------------
// TNmeaInitializingThread
//---------------------------------------------------------------------------
__fastcall TNmeaInitializingThread::TNmeaInitializingThread(TNmeaClient* AOwner,
	TObject *AManager, TNmeaReceiverSettings *ANmeaSettings) : TMyThread(true)
{
	Owner=AOwner;
	Manager=AManager;
	NmeaSettings=ANmeaSettings;
	CommandToParse=NULL;
	WhatToParse="";
}
//---------------------------------------------------------------------------

bool __fastcall TNmeaInitializingThread::CheckForInitTimeOuts(unsigned long InitStart, unsigned long StartTick)
{
	AnsiString msg;
	unsigned long t=Gr32_system::GetTickCount();

	if(NmeaSettings->InitTimeOut!=INFINITE)
	{
		if((t-InitStart)<NmeaSettings->InitTimeOut)
		{
			if(NmeaSettings->ReadReplyTimeOut!=INFINITE)
			{
				if((t-StartTick)<NmeaSettings->ReadReplyTimeOut) return true;
				else msg="The "+Owner->Name+" does not respond on initializing command (timeout is occured)!";
			}
			else return true;
		}
		else msg="Initializing for "+Owner->Name+" is stopped (timeout is occured)!";
	}
	else return true;

	Owner->ShowConnectionError(msg);
	AskForTerminate();
	return false;
}
//---------------------------------------------------------------------------

void __fastcall TNmeaInitializingThread::CommandParser()
{
	if(CommandToParse && WhatToParse!=NULL && WhatToParse.Length()>0)
	{
		(CommandToParse)(WhatToParse);
		CommandToParse=NULL;
		WhatToParse="";
	}
}
//---------------------------------------------------------------------------

void __fastcall TNmeaInitializingThread::Execute()
{
	TGpsListenerCommand *cmd;
	AnsiString *pstr, rec, InStr, str;
	TGpsListenerCommandReply *reply;
	int CurrentCmd, i, k;
	char Buf[2*MaxGpsBytesRead], *rbuf;
	unsigned long startTick, initStart;

	SetStarted();
	CurrentCmd=0;
	InStr="";
	initStart=Gr32_system::GetTickCount();
	do
	{
		try
		{
			if(Owner && Owner->Connected && Owner->InitCommandsCount>0)
			{
				cmd=Owner->Commands[CurrentCmd];
				if(cmd && cmd->Query!="")
				{
					Owner->WriteCommand(cmd->Query.c_str());
					if(cmd->RepliesSequence)
					{
						for(i=0; i<cmd->ReplyCount; i++)
						{
							startTick=Gr32_system::GetTickCount();
							reply=cmd->Replies[i];
							if(reply && reply->Reply!="")
							{
								rec="";
								while(rec!=reply->Reply && !Terminated && CheckForInitTimeOuts(initStart, startTick));
								{
									pstr=(AnsiString*)Owner->GetReceivedData();
									if(pstr!=NULL)
									{
										InStr=InStr+*pstr;
										delete pstr;
										str=reply->Reply;
										do
										{
											k=InStr.AnsiPos(str);
											str.SetLength(str.Length()-1);
										} while (k==0 && str.Length()>0);
										if(k==0) InStr="";
										else InStr=InStr.SubString(k, InStr.Length()-k+1);
										if(InStr.Length()>=reply->Reply.Length()) rec=InStr.SubString(1, reply->Reply.Length());
									}
									WaitForSingleObject(Owner->ReadBuf->NotEmptyEvent, 10); //MySleep(10);
									//WaitForSingleObject(IdleEvent, 10);
									CheckAskForSuspend();
								}
								if(!Terminated && CheckForInitTimeOuts(initStart, startTick) && !reply->Fixed && cmd->ParserProc &&//!cmd->Replies[i]->Fixed && cmd->ParserProc &&
									InStr!=NULL && InStr.Length()>reply->Reply.Length())
								{
									int j=reply->Reply.Length()+1;

									do
									{
										if(j<=InStr.Length() && InStr[j]!='\r' && InStr[j]!='\n') j++;
										if(j>=InStr.Length())
										{
											WaitForSingleObject(Owner->ReadBuf->NotEmptyEvent, 10); //MySleep(10);
											//WaitForSingleObject(IdleEvent, 10);
											pstr=(AnsiString*)Owner->GetReceivedData();
											if(pstr!=NULL) InStr=InStr+*pstr;
											else delete pstr;
										}
										CheckAskForSuspend();
									} while(!Terminated && CheckForInitTimeOuts(initStart, startTick) &&
										((j<=InStr.Length() && InStr[j]!='\r' && InStr[j]!='\n') || j>InStr.Length()));
									if(j<=InStr.Length() && (InStr[j]=='\r' || InStr[j]=='\n'))
									{
										str=InStr.SubString(1, j);
										CommandToParse=cmd->ParserProc;
										WhatToParse=str;
										//(cmd->ParserProc)(str);
										Synchronize(CommandParser);
										if(j==InStr.Length()) InStr="";
										else InStr=InStr.SubString(j+1, InStr.Length()-j);
									}
									else InStr=InStr.SubString(reply->Reply.Length()+1, InStr.Length()-reply->Reply.Length());
								}
								else if(!Terminated) InStr=InStr.SubString(reply->Reply.Length()+1, InStr.Length()-reply->Reply.Length());
								else break;
							}
						}
					}
					else if(cmd->ReplyCount>0)
					{
						startTick=Gr32_system::GetTickCount();
						k=0;
						i=0;
						while(k==0 && !Terminated && CheckForInitTimeOuts(initStart, startTick))
						{
							pstr=(AnsiString*)Owner->GetReceivedData();
							if(pstr!=NULL)
							{
								InStr=InStr+*pstr;
								delete pstr;
								i=0;
								reply=NULL;
								while(!Terminated && CheckForInitTimeOuts(initStart, startTick) && k==0 && i<cmd->ReplyCount)
								{
									reply=cmd->Replies[i];
									if(reply && reply->Reply!="")
										k=InStr.AnsiPos(reply->Reply);
									if(k==0) i++;
									CheckAskForSuspend();
								}
								if(i<cmd->ReplyCount && k>0 && reply && reply->Reply!="" && k<=(InStr.Length()-reply->Reply.Length()))
									InStr=InStr.SubString(k, InStr.Length()-k+1);
							}
							WaitForSingleObject(Owner->ReadBuf->NotEmptyEvent, 10); //MySleep(10);
							CheckAskForSuspend();
						}
						if(!Terminated && CheckForInitTimeOuts(initStart, startTick) && k>0 && reply && reply->Reply!="" &&
							!reply->Fixed && cmd->ParserProc && InStr!=NULL && InStr.Length()>reply->Reply.Length())
						{
							int j=reply->Reply.Length()+1;

							do
							{
								if(j<=InStr.Length() && InStr[j]!='\r' && InStr[j]!='\n') j++;
								if(j>=InStr.Length())
								{
									WaitForSingleObject(Owner->ReadBuf->NotEmptyEvent, 10); //MySleep(10);
									pstr=(AnsiString*)Owner->GetReceivedData();
									if(pstr!=NULL) InStr=InStr+*pstr;
									else delete pstr;
								}
								CheckAskForSuspend();
							} while(!Terminated && CheckForInitTimeOuts(initStart, startTick) &&
								((j<=InStr.Length() && InStr[j]!='\r' && InStr[j]!='\n') || j>InStr.Length()));
							if(j<=InStr.Length() && (InStr[j]=='\r' || InStr[j]=='\n'))
							{
								str=InStr.SubString(1, j);
								CommandToParse=cmd->ParserProc;
								WhatToParse=str;
								//(cmd->ParserProc)(str);
								Synchronize(CommandParser);
								if(j==InStr.Length()) InStr="";
								else InStr=InStr.SubString(j+1, InStr.Length()-j);
							}
							else InStr=InStr.SubString(reply->Reply.Length()+1, InStr.Length()-reply->Reply.Length());
						}
						else if(!Terminated) InStr=InStr.SubString(reply->Reply.Length()+1, InStr.Length()-reply->Reply.Length());
					}
				}
				CurrentCmd++;
				WaitForSingleObject(Owner->ReadBuf->NotEmptyEvent, 10); //MySleep(10);
				//WaitForSingleObject(IdleEvent, 10);
				if(CurrentCmd==Owner->InitCommandsCount)
				{
					SetEvent(Owner->InitializedEvent);
					if(cmd && cmd->ParserProc) (cmd->ParserProc)("");
				}
			}
			CheckAskForSuspend();
		}
		catch(Exception &e) {}
	} while(CurrentCmd<Owner->InitCommandsCount && !Terminated);
	SetIsTerminated();
}

//---------------------------------------------------------------------------
// TNmeaReceivingThread
//---------------------------------------------------------------------------
__fastcall TNmeaReceivingThread::TNmeaReceivingThread(TGpsUnit *AOwner, TNmeaClientsPool* AClients,
	TGpsListenersList* AListeners, TObject *AManager)
{
	Owner=AOwner;
	Manager=AManager;
	Clients=AClients;
	NmeaSummary=NULL;

	Listeners=AListeners;
	InitializingStarts=true;
	CenterPoint=NULL;
	OffsetX=0; OffsetY=0;
}
//---------------------------------------------------------------------------

void __fastcall TNmeaReceivingThread::ExecuteLoopBody()
{
	//TGPSCoordinate c;
	double OldLon=-1, OldLat=-1;
	TProfile *Profile;
	TTrace* TptrGPS=NULL;
	TNmeaSentenceType nst=nstNone;
	TGPSCoordinate *Coordinate=NULL;
	TCoordinateSystem cs;

	try
	{
		if(Owner && Owner->Clients)
		{
			for(int cc=0; cc<Owner->Clients->Count; cc++)
			{
				if(Owner->Clients->Pool[cc] && !Owner->Clients->Pool[cc]->Initialized)
				{
					if(InitializingStarts)
					{
						Owner->Refresh();
						InitializingStarts=false;
					}
					if(!Owner->Clients->Pool[cc]->ActiveInitializing)
						Owner->Clients->Pool[cc]->Initialize();
					WaitForSingleObject(IdleEvent, 100);
					return;
				}
				else
				{
					InitializingStarts=true;
					if(Owner->Clients->Pool[cc] && Owner->Clients->Pool[cc]->NmeaSummary)
					{
						if(!Owner->Clients->Pool[cc]->ClientStarted)
						{
							Owner->Clients->Pool[cc]->Start();
						}
						else
						{
							nst=Owner->Clients->Pool[cc]->NmeaParser(IdleEvent);
							NmeaSummary=Owner->Clients->Pool[cc]->NmeaSummary;
							Coordinate=NmeaSummary->Coordinate;
						}
					}
					else
					{
						nst=nstNone;
						Coordinate=NULL;
						NmeaSummary=NULL;
					}
					if(nst!=nstNone && nst!=nstUnknown) break;
				}
			}
		}
		else
		{
			nst=nstNone;
			Coordinate=NULL;
			NmeaSummary=NULL;
		}
		/*if(Owner && Owner->NmeaSummary)
		{
			nst=Owner->NmeaParser(IdleEvent);
			Coordinate=Owner->NmeaSummary->Coordinate;
		}
		else
		{
			nst=nstNone;
			Coordinate=NULL;
		}*/

		/*if(Coordinate && (nst==nstGGA || nst==nstLLQ || nst==nstTWPOS)) //|| nst==nstPlugIn
		{
			if(nst!=nstTWPOS && Manager && ((TRadarMapManager*)Manager)->Settings &&
				((TRadarMapManager*)Manager)->Settings->WheelPositioning==TPositioning::TwoWheel &&
				((TRadarMapManager*)Manager)->TwoWheelClient &&
				((TRadarMapManager*)Manager)->TwoWheelClient->NmeaSummary &&
				((TRadarMapManager*)Manager)->TwoWheelClient->NmeaSummary->Coordinate &&
				((TRadarMapManager*)Manager)->TwoWheelClient->NmeaSummary->Coordinate->Valid)
			{
				Coordinate=((TRadarMapManager*)Manager)->TwoWheelClient->NmeaSummary->Coordinate;
			}*/
		if(NmeaSummary && Coordinate && (nst==nstTWPOS || nst==nstGGA || nst==nstLLQ) &&
			(Manager && ((TRadarMapManager*)Manager)->Settings &&
			(((TRadarMapManager*)Manager)->Settings->WheelPositioning==TPositioning::TwoWheel && (nst==nstTWPOS ||
            ((TRadarMapManager*)Manager)->Settings->WPSasWheel ||
			!((TRadarMapManager*)Manager)->TwoWheelClient || !((TRadarMapManager*)Manager)->TwoWheelClient->Started)) ||
			(((TRadarMapManager*)Manager)->Settings->WheelPositioning!=TPositioning::TwoWheel && (nst==nstGGA || nst==nstLLQ))))
		{
			//Positioning Plugin correction...
			if(OffsetX!=0 || OffsetY!=0)
			{
				/*Coordinate->SetX(csUTM, Coordinate->GetX(csUTM)+OffsetX);
				Coordinate->SetY(csUTM, Coordinate->GetY(csUTM)+OffsetY);*/
				cs=csLocalMetric();
				TDoublePoint uv=Coordinate->UnitVector(cs);
				Coordinate->SetXY(cs, Coordinate->GetX(cs)+OffsetX*uv.X, Coordinate->GetY(cs)+OffsetY*uv.Y, Coordinate->GetTemp(cs));
			}
			if(Manager && ((TRadarMapManager*)Manager)->Settings &&
				(((TRadarMapManager*)Manager)->Settings->WheelPositioning!=TPositioning::TwoWheel ||
				(((TRadarMapManager*)Manager)->TwoWheelClient && !((TRadarMapManager*)Manager)->TwoWheelClient->Started)) &&
				((TRadarMapManager*)Manager)->Settings->GPS.SimTieCoordinatesToMapCenter &&
				(((TRadarMapManager*)Manager)->Settings->GPS.SimFileSource || ((TRadarMapManager*)Manager)->Settings->GPS.SimTcpSource))
			{
				double lat, lon;

				//lat=DegMinToDeg(Owner->GGA->Lat);
				//lon=DegMinToDeg(Owner->GGA->Lon);
				lat=Coordinate->Latitude;
				lon=Coordinate->Longitude;
				if(!CenterPoint && ((TRadarMapManager*)Manager)->MapObjects)
				{
					if(((TRadarMapManager*)Manager)->MapObjects->Container->LatLonRect->WidthInMeters()>0 &&
						((TRadarMapManager*)Manager)->MapObjects->Container->LatLonRect->HeightInMeters()>0)
					{
						FirstPoint.X=lon;
						FirstPoint.Y=lat;
						if(((TRadarMapManager*)Manager)->MapObjects && ((TRadarMapManager*)Manager)->MapObjects->Container &&
							((TRadarMapManager*)Manager)->MapObjects->Container->LatLonRect)
						{
							bool au;
							TGPSCoordinate *c1, *c2;

							au=((TRadarMapManager*)Manager)->MapObjects->Container->LatLonRect->AutomaticUpdate;
							if(!au)
							{
							   ((TRadarMapManager*)Manager)->MapObjects->Container->LatLonRect->AutomaticUpdate=true;
							   ((TRadarMapManager*)Manager)->MapObjects->Container->LatLonRect->AutomaticUpdate=false;
							}
							c1=((TRadarMapManager*)Manager)->MapObjects->Container->LatLonRect->LeftTop;
							c2=((TRadarMapManager*)Manager)->MapObjects->Container->LatLonRect->RightBottom;

							CenterPoint=new TGPSCoordinate(NULL, (c1->Latitude+c2->Latitude)/2, (c1->Longitude+c2->Longitude)/2, 0., 0., (c1->Confidence+c2->Confidence)/2);
						}
					}
					else Synchronize(AddTrackPoint);
				}
				if(CenterPoint)
				{
					Coordinate->Latitude=(lat-FirstPoint.Y+CenterPoint->Latitude);
					Coordinate->Longitude=(lon-FirstPoint.X+CenterPoint->Longitude);
					Synchronize(AddTrackPoint);
				}
				else Synchronize(ShowTrackPoint);
			}
			else Synchronize(AddTrackPoint);
			Synchronize(SetLastPoint);
			for(int i=0; i<((TRadarMapManager*)Manager)->ReceivingProfilesQuantity; i++)
			{
				try
				{
					Profile=((TRadarMapManager*)Manager)->ReceivingProfiles[i];
				}
				catch(Exception &e)
				{
					Profile=NULL;
				}
				if(Profile)
				{
					try
					{
						if(Profile->FirstTracePtr && !Profile->FirstTracePtr->GPSCoordinated)
							TptrGPS=Profile->FirstTracePtr;
						else if(Profile->CurrentTracePtr && !Profile->CurrentTracePtr->GPSCoordinated)
							TptrGPS=Profile->CurrentTracePtr;
						else if(Profile->LastTracePtr && !Profile->LastTracePtr->GPSCoordinated)
							TptrGPS=Profile->LastTracePtr;
						else TptrGPS=NULL;
					}
					catch (Exception &exception) { TptrGPS=NULL;}
				}
				else TptrGPS=NULL;
				if(TptrGPS!=NULL && !Terminated && Coordinate->Valid)
				{
					TptrGPS->GreenwichTm=NmeaSummary->Tm;//Now();
					TptrGPS->Latitude=Coordinate->Latitude;
					TptrGPS->Longitude=Coordinate->Longitude;

					if(Manager && ((TRadarMapManager*)Manager)->Settings)
					{
						TDouble3DPoint dp;
						cs=((TRadarMapManager*)Manager)->Settings->DefaultCoordinateSystem;

						dp=Coordinate->GetXYH(cs);
						TptrGPS->LocalCsX=dp.X;
						TptrGPS->LocalCsY=dp.Y;
						TptrGPS->LocalCsH=dp.Z;
						TptrGPS->LocalCsCh1=dp.Temp[0];
						TptrGPS->LocalCsCh2=dp.Temp[1];
					}
					TptrGPS->Z=Coordinate->Z;
					TptrGPS->Z_Geoid=Coordinate->Z_Geoid;
					TptrGPS->GPSConfidence=(NmeaSummary->RMS<0 ? (NmeaSummary->LLQQuality<0 ? 0: NmeaSummary->LLQQuality) : NmeaSummary->RMS);
					TptrGPS->GPSCoordinated=true;
					TptrGPS->GPSInterpolated=true;
					if(Profile && Profile->OutputView->Path!=NULL)
					{
						Profile->Coordinated=true;
						ToPtr=TptrGPS;
						if(Coordinate->Longitude!=OldLon || Coordinate->Latitude!=OldLat)
							Synchronize(DrawPoint);
						OldLon=Coordinate->Longitude;
						OldLat=Coordinate->Latitude;
					}
					try
					{
						IndInterpolateTraces(Profile, TptrGPS, TPositioning::WheelGPS, TptrGPS->Backward);
					}
					catch (Exception &exception) { ;}
				}
				Owner->Refresh(); /**/
			}
		}
		else if(nst==nstCTMOTION || nst==nstHDT || nst==nstTWHPR)
		{
			for(int i=0; i<((TRadarMapManager*)Manager)->ReceivingProfilesQuantity; i++)
			{
				try
				{
					Profile=((TRadarMapManager*)Manager)->ReceivingProfiles[i];
				}
				catch(Exception &e)
				{
					Profile=NULL;
				}
				if(Profile)
				{
					try
					{
						if(Profile->CurrentTracePtr) TptrGPS=Profile->CurrentTracePtr;
						else if(Profile->LastTracePtr) TptrGPS=Profile->LastTracePtr;
						else if(Profile->FirstTracePtr)	TptrGPS=Profile->FirstTracePtr;
						else TptrGPS=NULL;
					}
					catch (Exception &exception) { TptrGPS=NULL;}
				}
				if(TptrGPS)
				{
					TInterpolationType it;

					switch(nst)
					{
						case nstCTMOTION:
							TptrGPS->Pitch=DegToRad(NmeaSummary->Pitch);
							TptrGPS->Roll=DegToRad(NmeaSummary->Roll);
							it=itPitchRoll;
							break;
						case nstHDT:
							TptrGPS->Heading=DegToRad(NmeaSummary->Heading);
							it=itHeading;
							break;
						case nstTWHPR:
							TptrGPS->Pitch=DegToRad(NmeaSummary->Pitch);
							TptrGPS->Roll=DegToRad(NmeaSummary->Roll);
							InterpolateTraces(Profile, TptrGPS, itPitchRoll, TptrGPS->Backward);
							TptrGPS->Heading=DegToRad(NmeaSummary->Heading);
							it=itHeading;
							break;
						default: it=itNone;
					}
					InterpolateTraces(Profile, TptrGPS, it, TptrGPS->Backward);
				}
			}
		}
	}
	catch(Exception &e) {}
	WaitForSingleObject(IdleEvent, 10);
	//Sleep(10);
}
//---------------------------------------------------------------------------

void __fastcall TNmeaReceivingThread::DrawPoint()
{
	TGPSCoordinate *c;
	bool b;

	try
	{
		if(NmeaSummary && Manager && ((TRadarMapManager*)Manager)->ReceivingProfile &&
			((TRadarMapManager*)Manager)->ReceivingProfile->OutputView &&
			((TRadarMapManager*)Manager)->ReceivingProfile->OutputView->Path)
		{
			c=((TMapPathObject*)((TRadarMapManager*)Manager)->ReceivingProfile->OutputView->Path)->AddP(
				NmeaSummary->Lon, NmeaSummary->Lat, NmeaSummary->Z, NmeaSummary->Z_Geoid, ToPtr, b,
				(NmeaSummary->RMS<0 ? (NmeaSummary->LLQQuality<0 ? 0: NmeaSummary->LLQQuality) : NmeaSummary->RMS));
			if(((TRadarMapManager*)Manager)->LinesDetector!=NULL && c)
			{
				if(!b) ((TRadarMapManager*)Manager)->LinesDetector->AddedPoint=c; //add
				else ;// Begin from start point
			}
		}
	}
	catch(Exception &e) {}
}
//---------------------------------------------------------------------------

void __fastcall TNmeaReceivingThread::SetLastPoint()
{
	try
	{
		if(NmeaSummary && Manager && ((TRadarMapManager*)Manager)->LastGPSPoint &&
			(NmeaSummary->Lon!=0 || NmeaSummary->Lat!=0 || NmeaSummary->Z!=0))
		{
			/*((TRadarMapManager*)Manager)->LastGPSPoint->Latitude=NmeaSummary->Lat;
			((TRadarMapManager*)Manager)->LastGPSPoint->Longitude=NmeaSummary->Lon;
			((TRadarMapManager*)Manager)->LastGPSPoint->Z=NmeaSummary->Z;
			((TRadarMapManager*)Manager)->LastGPSPoint->Z_Geoid=NmeaSummary->Z_Geoid;*/
			((TRadarMapManager*)Manager)->LastGPSPoint->SetLatLonAlt(NmeaSummary->Lat,
				NmeaSummary->Lon, NmeaSummary->Z_Geoid);
		}
	}
	catch(Exception &e) {}
}
//---------------------------------------------------------------------------

void __fastcall TNmeaReceivingThread::ShowTrackPoint()
{
	try
	{
		if(NmeaSummary && (NmeaSummary->Lon!=0 || NmeaSummary->Lat!=0 || NmeaSummary->Z!=0))
		{
			AnsiString msg;
			TGPSCoordinate *c;
			double ln, lt;

			//ln=DegMinToDeg(Owner->GGA->Lon);
			//lt=DegMinToDeg(Owner->GGA->Lat);
			c=new TGPSCoordinate(NULL, NmeaSummary->Lat, NmeaSummary->Lon,
				NmeaSummary->Z, NmeaSummary->Z_Geoid,
				(NmeaSummary->RMS<0 ? (NmeaSummary->LLQQuality<0 ? 0: NmeaSummary->LLQQuality) : NmeaSummary->RMS));
			try
			{
				ShowTrackPoint2(c);
			}
			__finally
			{
				delete c;
			}
		}
	}
	catch(Exception &e)	{}
}
//---------------------------------------------------------------------------

void __fastcall TNmeaReceivingThread::ShowTrackPoint2(TGPSCoordinate *c)
{
	try
	{
		//Distribute to SPAR unit
		if(c && NmeaSummary)
		{
			if(Manager && ((TRadarMapManager*)Manager)->Settings && ((TRadarMapManager*)Manager)->Settings->SparEnable &&
				((TRadarMapManager*)Manager)->SparUnit && ((TRadarMapManager*)Manager)->SparUnit->SparData &&
				((TRadarMapManager*)Manager)->SparUnit->SparData->SparDataPtr)
			{
				TSparData *SparData;

				SparData=((TRadarMapManager*)Manager)->SparUnit->SparData;
				SparData->SparDataPtr->message=SPAR_DATA;
				SparData->SparDataPtr->pointType=USER_POINT;
				SparData->SparDataPtr->flags=0x01;
				SparData->SparDataPtr->vAntHeight=((TRadarMapManager*)Manager)->Settings->GPS.AntennaHeight;
				SparData->SparDataPtr->latitude=SparData->DegToRad(NmeaSummary->Lat);
				SparData->SparDataPtr->longitude=SparData->DegToRad(NmeaSummary->Lon);
				SparData->SparDataPtr->altitude=NmeaSummary->Z_Geoid;
				SparData->SparDataPtr->fixType=5; //RTK fix
				SparData->SparDataPtr->utc=(double)NmeaSummary->Tm*86400.;
				SparData->SparDataPtr->nEpochs=1;
				SparData->SparDataPtr->hdop=(NmeaSummary->HDOP<0 ? 0 : NmeaSummary->HDOP);		//
				SparData->SparDataPtr->vdop=(NmeaSummary->VDOP<0 ? 0 : NmeaSummary->VDOP);
				SparData->SparDataPtr->pdop=(NmeaSummary->PDOP<0 ? 0 : NmeaSummary->PDOP);
				SparData->SparDataPtr->hsdv=(NmeaSummary->MajorDiv<0 ? 0 : NmeaSummary->MajorDiv);	// Horizontal standard deviation in meters
				SparData->SparDataPtr->vsdv=(NmeaSummary->MinorDiv<0 ? 0 : NmeaSummary->MinorDiv);	// Vertical standard deviation in meters
				SparData->SparDataPtr->rms=(NmeaSummary->RMS<0 ? 0 : NmeaSummary->RMS);        // RMS value
				SparData->SparDataPtr->nSVs=(NmeaSummary->Satellites<0 ? 0 : NmeaSummary->Satellites);
				SparData->ReBuild();
			}
			//Distribute to GPS Message
			if(((TRadarMapManager*)Manager)->Settings->ShowGPSCoordinates)
			{
				AnsiString msg;
				TCoordinateSystem cs;

				if(Manager && ((TRadarMapManager*)Manager)->Settings)
				{
					/*if(((TRadarMapManager*)Manager)->MapObjects && ((TRadarMapManager*)Manager)->MapObjects->Container)
						cs=((TRadarMapManager*)Manager)->MapObjects->Container->CoordinateSystem;
					else*/
					cs=((TRadarMapManager*)Manager)->Settings->DefaultCoordinateSystem;
				}
				else cs=csLatLon;
				msg=c->AsString(cs);
				msg+=" ";
				if(NmeaSummary->Speed==true) msg+=FloatToStrF((double)NmeaSummary->Speed*3.6, ffFixed, 6, 2);
				else msg+=FloatToStrF(NmeaSummary->CoordinatedSpeed*3.6, ffFixed, 6, 2);
				msg+=" km/h";

				if(!((TRadarMapManager*)Manager)->CoordinatesMessage)
					((TRadarMapManager*)Manager)->CoordinatesMessage=((TRadarMapManager*)Manager)->ShowMessageObject(
						((TRadarMapManager*)Manager)->CoordinatesMessage, ((TRadarMapManager*)Manager)->MapMessages,
						TMessageType::mtRoutine, mvtConstant, 0, msg, (AnsiString)_RadarMapName);
				else ((TRadarMapManager*)Manager)->CoordinatesMessage->Text=msg;
			}
			else if(((TRadarMapManager*)Manager)->CoordinatesMessage)
			{
				((TRadarMapManager*)Manager)->CoordinatesMessage->SwitchOff();
				((TRadarMapManager*)Manager)->CoordinatesMessage=NULL;
			}
			//Distribute for Listeners
			if(Listeners && Listeners->Count>0) Listeners->AddCoordinateToAll(c);
		}
	}
	catch(Exception &e)	{}
}
//---------------------------------------------------------------------------

void __fastcall TNmeaReceivingThread::AddTrackPoint()
{
	TGPSCoordinate *c;

	if(NmeaSummary && Manager && ((TRadarMapManager*)Manager)->Settings &&
		(NmeaSummary->Lon!=0 || NmeaSummary->Lat!=0 || NmeaSummary->Z!=0))
	{
		if(((TRadarMapManager*)Manager)->Settings->GPSTracking && ((TRadarMapManager*)Manager)->TrackingPath)
		{
			c=((TRadarMapManager*)Manager)->TrackingPath->AddP(NmeaSummary->Lon, NmeaSummary->Lat,
				NmeaSummary->Z, NmeaSummary->Z_Geoid,
				(NmeaSummary->RMS<0 ? (NmeaSummary->LLQQuality<0 ? 0: NmeaSummary->LLQQuality) : NmeaSummary->RMS));
			ShowTrackPoint2(c);
		}
		else ShowTrackPoint();
	}
}

//---------------------------------------------------------------------------
// TNmeaGGA
//---------------------------------------------------------------------------
__fastcall TNmeaGGA::TNmeaGGA() : TNmeaSentence(nstGGA)
{
	Tm=0;
	Lat=Lon=Z=Z_geoid=0;
	Quality=-1;
	Satellites=-1;
	HDOP=-1;
	DelaySinceLastUpdate=-1;
}
//---------------------------------------------------------------------------

AnsiString __fastcall TNmeaGGA::AsString(bool Full)
{
	AnsiString str;

	str="";
	if(!Corrupted)
	{
		if(Full)
		{
			str+="Greenwich="+MyTimeToStr(Tm)+", ";
			str+="Lat="+FloatToStrF(Lat, ffFixed, 10, 4)+", ";
			str+="Lon="+FloatToStrF(Lon, ffFixed, 10, 4)+", ";
		}
		switch(Quality)
		{
			case 0: str+="Quality=Invalid"; break;
			case 1: str+="Quality=GPS"; break;
			case 2: str+="Quality=DGPS"; break;
			case 3: str+="Quality=PPS"; break;
			case 4: str+="Quality=RTK"; break;
			case 5: str+="Quality=Float RTK"; break;
			case 6: str+="Quality=Estimated"; break;
			case 7: str+="Quality=Manual input"; break;
			case 8: str+="Quality=Simulation"; break;
		}
		if(Satellites>=0)
		{
			if(str!=NULL && str!="") str+=", ";
			str+="Satellites="+IntToStr(Satellites);
		}
		if(HDOP>0)
		{
			if(str!=NULL && str!="") str+=", ";
			str+="HDOP="+FloatToStrF(HDOP, ffFixed, 5, 1);
		}
		if(DelaySinceLastUpdate>=0)
		{
			if(str!=NULL && str!="") str+=", ";
			str+="Delay="+FloatToStrF(DelaySinceLastUpdate, ffFixed, 5, 1);
		}
	}

	return str;
}

//---------------------------------------------------------------------------
// TNmeaGSA
//---------------------------------------------------------------------------
__fastcall TNmeaGSA::TNmeaGSA() : TNmeaSentence(nstGSA)
{
	Selection='\0'; // A - Auto, M - Manual
	FixType=-1; //1 = no fix, 2 = 2D fix, 3 = 3D fix
	for(int i=0; i<12; i++) PRNs[i]=0;
	PDOP=HDOP=VDOP=-1;
}
//---------------------------------------------------------------------------

AnsiString __fastcall TNmeaGSA::AsString(bool Full)
{
	AnsiString str;

	str="";
	if(!Corrupted)
	{
		if(Full)
		{
			if(Selection=='A') str+="Selection=Auto, ";
			else if(Selection=='M') str+="Selection=Manual, ";
			if(FixType==1) str+="Fix=no fix, ";
			else if(FixType==2) str+="Fix=2D fix, ";
			else if(FixType==3) str+="Fix=3D fix, ";
			str+="PRNs=";
			for(int i=0; i<12; i++) if(PRNs[i]>0) str+=IntToStr(PRNs[i])+"-";
			if(HDOP>0) str+=", HDOP="+FloatToStrF(HDOP, ffFixed, 5, 1);
			if(VDOP>0) str+=", VDOP="+FloatToStrF(VDOP, ffFixed, 5, 1);
		}
		if(PDOP>0)
		{
			if(str!=NULL && str!="") str+=", ";
			str+="PDOP="+FloatToStrF(PDOP, ffFixed, 5, 1);
		}
	}

	return str;
}

//---------------------------------------------------------------------------
// TNmeaGST
//---------------------------------------------------------------------------
__fastcall TNmeaGST::TNmeaGST() : TNmeaSentence(nstGST)
{
	Tm=0;
	RMS=MajorDiv=MinorDiv=OrientationToNorth=LatDiv=LonDiv=ZDiv=-1;
}
//---------------------------------------------------------------------------

//$GNGST,154013.80,0.642,1.746,1.303,27.197,1.663,1.407,2.456*79
AnsiString __fastcall TNmeaGST::AsString(bool Full)
{
	AnsiString str;

	str="";
	if(!Corrupted)
	{
		if(Full) str+="UTC="+MyTimeToStr(Tm)+", ";
		if(RMS>=0)
		{
			if(str!=NULL && str!="") str+=", ";
			str+="RMS="+FloatToStrF(RMS*100, ffFixed, 5, 1)+"cm";
		}
		if(Full)
		{
			if(MajorDiv>=0)
			{
				if(str!=NULL && str!="") str+=", ";
				str+="HSDV="+FloatToStrF(MajorDiv*100, ffFixed, 5, 1)+"cm";
			}
			if(MinorDiv>=0)
			{
				if(str!=NULL && str!="") str+=", ";
				str+="VSDV="+FloatToStrF(MinorDiv*100, ffFixed, 5, 1)+"cm";
			}
			if(OrientationToNorth>=0)
			{
				if(str!=NULL && str!="") str+=", ";
				str+="Orientation="+FloatToStrF(OrientationToNorth, ffFixed, 5, 3)+"�";
			}
			if(LatDiv>=0)
			{
				if(str!=NULL && str!="") str+=", ";
				str+="LatDiv="+FloatToStrF(LatDiv*100, ffFixed, 5, 1)+"cm";
			}
			if(LonDiv>=0)
			{
				if(str!=NULL && str!="") str+=", ";
				str+="LonDiv="+FloatToStrF(LonDiv*100, ffFixed, 5, 1)+"cm";
			}
			if(ZDiv>=0)
			{
				if(str!=NULL && str!="") str+=", ";
				str+="AltDiv="+FloatToStrF(ZDiv*100, ffFixed, 5, 1)+"cm";
			}
		}
	}
	return str;
}

//---------------------------------------------------------------------------
// TNmeaLLQ
//---------------------------------------------------------------------------
__fastcall TNmeaLLQ::TNmeaLLQ() : TNmeaSentence(nstLLQ)
{
	Tm=Dt=0;
	Easting=Northing=Z=0;
	Quality=CoordinateQuality=Satellites=-1;
}
//---------------------------------------------------------------------------

AnsiString __fastcall TNmeaLLQ::AsString(bool Full)
{
	AnsiString str;
	char ts, ds;
	AnsiString sdf, ltf;

	str="";
	if(!Corrupted)
	{
		if(Full)
		{
			str+="UTC="+MyDateToStr(Dt)+" "+MyTimeToStr(Tm)+", ";
			str+="Easting="+FloatToStrF(Easting, ffFixed, 10, 3)+", ";
			str+="Northing="+FloatToStrF(Northing, ffFixed, 10, 3)+", ";
			str+="Altitude="+FloatToStrF(Z, ffFixed, 5, 1)+", ";
		}
		switch(Quality)
		{
			case 0: str+="Quality=Invalid"; break;
			case 1: str+="Quality=Navigation"; break;
			case 2: str+="Quality=Ambiguities"; break;
			case 3: str+="Quality=No ambiguities"; break;
		}
		if(Satellites>=0)
		{
			if(str!=NULL && str!="") str+=", ";
			str+="Satellites="+IntToStr(Satellites);
		}
		if(CoordinateQuality>0)
		{
			if(str!=NULL && str!="") str+=", ";
			str+="CoordQlty="+FloatToStrF(CoordinateQuality, ffFixed, 5, 1);
		}
	}
	return str;
}

//---------------------------------------------------------------------------
// TNmeaCTMOTION
//---------------------------------------------------------------------------
AnsiString __fastcall TNmeaCTMOTION::AsString(bool Full)
{
	AnsiString str;

	str="";
	if(!Corrupted)
	{
		str+="Pitch="+FloatToStrF(Pitch, ffFixed, 5, 2)+"�";
		if(str!=NULL && str!="") str+=", ";
		str+="Roll="+FloatToStrF(Roll, ffFixed, 5, 2)+"�";
	}
	return str;
}

//---------------------------------------------------------------------------
// TNmeaHDT
//---------------------------------------------------------------------------
AnsiString __fastcall TNmeaHDT::AsString(bool Full)
{
	AnsiString str;

	str="";
	if(!Corrupted)
	{
		str+="Heading="+FloatToStrF(Heading, ffFixed, 5, 2)+"�";
	}
	return str;
}

//---------------------------------------------------------------------------
// TNmeaSummaryThread
//---------------------------------------------------------------------------
__fastcall TNmeaSummaryThread::TNmeaSummaryThread(TNmeaReceiverSettings *ANmeaSettings, TObject *AManager) :
	speed(5000)
{
	owner=NULL;//AOwner;
	Manager=AManager;
	NmeaSettings=ANmeaSettings;
	Sentences=new TNmeaSentence*[(int)nstUnknown];
	for(int i=0; i<(int)nstUnknown; i++) Sentences[i]=NULL;
	tm=0;
	ddistance=0;
	LastValidCoordinateTick=0;
	coordinate=new TGPSCoordinate();
	lastcoordinate=new TGPSCoordinate();
	quality=satellites=fixtype=0;
	hdop=vdop=pdop=minordiv=majordiv=0.0;
	pitch=roll=heading=0.0;
	distance=coordinatedspeed=0.0;
	//speed=TFreshValue<double>(5000);
	llqquality=rms=-1;
}
//---------------------------------------------------------------------------

__fastcall TNmeaSummaryThread::~TNmeaSummaryThread()
{
	if(coordinate) delete coordinate;
	coordinate=NULL;
	if(lastcoordinate) delete lastcoordinate;
	lastcoordinate=NULL;
	Clear();
	delete[] Sentences;
}
//---------------------------------------------------------------------------

void __fastcall TNmeaSummaryThread::Set(TNmeaSentence *NS)
{
	if(NS && NS->SentenceType<nstUnknown)
	{
		//NS->Refreshed=true;
		Sentences[NS->SentenceType]=NS;
	}
}
//---------------------------------------------------------------------------

TNmeaSentence* __fastcall TNmeaSummaryThread::Set(TNmeaSentenceType NST)
{
	TNmeaSentence* res=NULL;

	if(NST<nstUnknown)
	{
		if(Sentences[NST])
		{
			delete Sentences[NST];
			Sentences[NST]=NULL;
		}
		switch(NST)
		{
			case nstGGA: res=new TNmeaGGA(); break;
			case nstGSA: res=new TNmeaGSA(); break;
			case nstGST: res=new TNmeaGST(); break;
			case nstLLQ: res=new TNmeaLLQ(); break;
			case nstCTMOTION: res=new TNmeaCTMOTION(); break;
			case nstHDT: res=new TNmeaHDT(); break;
			case nstTWPOS: res=new TNmeaTWPOS(); break;
			case nstTWHPR: res=new TNmeaTWHPR(); break;
		}
		Set(res);
	}

	return res;
}
//---------------------------------------------------------------------------

TNmeaSentence* __fastcall TNmeaSummaryThread::Get(TNmeaSentenceType NST)
{
	TNmeaSentence* res=NULL;

	try
	{
		if(Sentences && NST<nstUnknown)
		{
			res=Sentences[(int)NST];
		}
	}
	catch(Exception &e) {}
	return res;
}
//---------------------------------------------------------------------------

bool __fastcall TNmeaSummaryThread::Rebuild(TNmeaSentenceType nst)
{
	TNmeaSentence *ns;
	int i, ib, ie;
	TCoordinateSystem cs;
	bool res=false;

	try
	{
		if(nst<nstUnknown && coordinate)
		{
			ns=Sentences[nst];
			if(ns && !ns->Corrupted)
			{
				cs=((TRadarMapManager*)Manager)->Settings->DefaultCoordinateSystem;
				if(coordinate->GetCSDimension(cs)==csdDegrees) cs=csLocalMetric();
				if(cs==csUTM) cs=csMetric;
				switch(nst)
				{
					case nstGGA:
						coordinate->AutomaticUpdate=false;
						/*coordinate->Latitude=DegMinToDeg(((TNmeaGGA*)ns)->Lat);
						coordinate->Longitude=DegMinToDeg(((TNmeaGGA*)ns)->Lon);*/
						coordinate->SetLatLon(DegMinToDeg(((TNmeaGGA*)ns)->Lat), DegMinToDeg(((TNmeaGGA*)ns)->Lon));
						coordinate->Z=((TNmeaGGA*)ns)->Z;
						coordinate->Z_Geoid=coordinate->Z-NmeaSettings->AntennaHeight;
						if(NmeaSettings->UseZGeoid) coordinate->Z_Geoid+=((TNmeaGGA*)ns)->Z_geoid;
						coordinate->AutomaticUpdate=true;
						tm=((TNmeaGGA*)ns)->Tm;
						quality=((TNmeaGGA*)ns)->Quality;
						satellites=((TNmeaGGA*)ns)->Satellites;
						hdop=((TNmeaGGA*)ns)->HDOP;
						break;
					case nstGSA:
						hdop=((TNmeaGSA*)ns)->HDOP;
						vdop=((TNmeaGSA*)ns)->VDOP;
						pdop=((TNmeaGSA*)ns)->PDOP;
						fixtype=((TNmeaGSA*)ns)->FixType;
						break;
					case nstGST:
						rms=((TNmeaGST*)ns)->RMS;
						tm=((TNmeaGST*)ns)->Tm;
						minordiv=((TNmeaGST*)ns)->MinorDiv;
						majordiv=((TNmeaGST*)ns)->MajorDiv;
						break;
					case nstLLQ:
						//coordinate->AutomaticUpdate=false;
						/*coordinate->SetX(cs, ((TNmeaLLQ*)ns)->Easting);
						coordinate->SetY(cs, ((TNmeaLLQ*)ns)->Northing);
						coordinate->SetH(cs, ((TNmeaLLQ*)ns)->Z-NmeaSettings->AntennaHeight);*/
						coordinate->SetXYH(cs, ((TNmeaLLQ*)ns)->Easting, ((TNmeaLLQ*)ns)->Northing, ((TNmeaLLQ*)ns)->Z-NmeaSettings->AntennaHeight);
						//coordinate->AutomaticUpdate=true;
						tm=((TNmeaLLQ*)ns)->Tm;
						quality=((TNmeaLLQ*)ns)->Quality;
						satellites=((TNmeaLLQ*)ns)->Satellites;
						llqquality=((TNmeaLLQ*)ns)->CoordinateQuality;
						break;
					case nstCTMOTION:
						pitch=((TNmeaCTMOTION*)ns)->Pitch;
						roll=((TNmeaCTMOTION*)ns)->Roll;
						break;
					case nstHDT:
						heading=((TNmeaHDT*)ns)->Heading;
						break;
					case nstTWPOS:
						if(!((TRadarMapManager*)Manager)->Settings->WPSasWheel)
							coordinate->SetXYH(cs, ((TNmeaTWPOS*)ns)->X,
								((TNmeaTWPOS*)ns)->Y, ((TNmeaTWPOS*)ns)->Z-NmeaSettings->AntennaHeight);
						tm=((TNmeaTWPOS*)ns)->Tm;
						distance=((TNmeaTWPOS*)ns)->D;
						speed=((TNmeaTWPOS*)ns)->S;
						break;
					case nstTWHPR:
						pitch=((TNmeaTWHPR*)ns)->Pitch;
						roll=((TNmeaTWHPR*)ns)->Roll;
						heading=((TNmeaTWHPR*)ns)->Heading;
						break;
				}
				if(coordinate->Valid && (nst==nstGGA || nst==nstLLQ || nst==nstTWPOS))
				{
					if(lastcoordinate->Valid && LastValidCoordinateTick!=0)
					{
						TDouble3DPoint cp, lp;
						ULONGLONG ull;
						double dx, dy, dz, dt;
						cs=csLocalMetric();
						cp=coordinate->GetXYH(cs);
						lp=lastcoordinate->GetXYH(cs);
						ull=::GetTickCount();
						dt=(double)ull-(double)LastValidCoordinateTick;
						if(dt>0)
						{
							dx=cp.X-lp.X;
							dy=cp.Y-lp.Y;
							dz=cp.Z-lp.Z;
							ddistance=sqrt(dx*dx+dy*dy+dz*dz);
							coordinatedspeed=1000.0*ddistance/dt; //meters per second
							if(Manager && ((TRadarMapManager*)Manager)->Settings &&
								dabs(coordinatedspeed)>((TRadarMapManager*)Manager)->Settings->MaxDeviceSpeed )
							{
								coordinate->SetLatLonAlt(lastcoordinate->Latitude, lastcoordinate->Longitude, lastcoordinate->Z_Geoid);
								coordinate->Z=lastcoordinate->Z;
								ddistance=0;
								coordinatedspeed=0;
							}
						}
						else
						{
							ddistance=0;
							coordinatedspeed=0;
						}
					}
					else
					{
						ddistance=0;
						coordinatedspeed=0;
					}
					lastcoordinate->SetLatLonAlt(coordinate->Latitude, coordinate->Longitude, coordinate->Z_Geoid);
					lastcoordinate->Z=coordinate->Z;
					LastValidCoordinateTick=::GetTickCount();
				}
#ifdef _Territory_Protection
				if(coordinate->Valid && nsTerritories::IsRestrictedPoint(coordinate->GetXY(csLatLon)))
				{
					coordinate->SetLatLonAlt(0, 0, 0);
					coordinate->Z=0;
					if(!((TRadarMapManager*)Manager)->CoordinatesMessage)
						((TRadarMapManager*)Manager)->CoordinatesMessage=((TRadarMapManager*)Manager)->ShowMessageObject(
							((TRadarMapManager*)Manager)->CoordinatesMessage, ((TRadarMapManager*)Manager)->MapMessages,
							TMessageType::mtStop, mvtConstant, 0, "You are not authorized under current LICENSE on this territory!", (AnsiString)_RadarMapName);
					else ((TRadarMapManager*)Manager)->CoordinatesMessage->Text="You are not authorized under current LICENSE on this territory!";
				}
#endif
				if(Manager && ((TRadarMapManager*)Manager)->TwoWheelClient && coordinate->Valid)
				{
					if(nst==nstGGA || nst==nstLLQ)
					{
						//if(((TRadarMapManager*)Manager)->TrackingPath &&
						((TRadarMapManager*)Manager)->TwoWheelClient->SetStartingPoint(coordinate, false);
					}
					else if(nst==nstTWPOS && ((TRadarMapManager*)Manager)->TwoWheelClient->Initiator &&
						((TRadarMapManager*)Manager)->TwoWheelClient->Initiator->Coordinate==NULL &&
						((TRadarMapManager*)Manager)->TwoWheelClient->Initialized)
					{
						((TRadarMapManager*)Manager)->TwoWheelClient->Initiator->Coordinate=new TGPSCoordinate(coordinate);
					}
				}
				if(Owner  && Owner->Unit && nst!=nstNone && nst!=nstGST) Owner->Unit->ApplyControlsRedraw(); //???????????????
				res=true;
			}
		}
	}
	catch(Exception &e) {}

	return res;
}

//---------------------------------------------------------------------------
// TGpsUnit
//---------------------------------------------------------------------------
__fastcall TGpsUnit::TGpsUnit(TNmeaClient* AGpsClient, TGpsListenersList* AListeners, TObject *AManager) : TIndicator()
{
	Manager=AManager;

	GpsClient=AGpsClient;
	clients=new TNmeaClientsPool();
	Clients->Add(GpsClient, this);

	RecThrd=NULL;
	Listeners=AListeners;
	MaxIcons=5;
	IconPaths=new System::UnicodeString[MaxIcons+1];
	NotConnected="PNGIMAGE_124";
	Simulation = "PNGIMAGE_125";
	IconPaths[0]="PNGIMAGE_118"; // Number of satellites less than 4
	IconPaths[1]="PNGIMAGE_119"; // 4 satellites
	IconPaths[2]="PNGIMAGE_120"; // 5 satellites
	IconPaths[3]="PNGIMAGE_121"; // 6 satellites
	IconPaths[4]="PNGIMAGE_122"; // 7 satellites
	IconPaths[5]="PNGIMAGE_123"; // 8 satellites
	index=0;
	NotIndexed=NotConnected;
	HDOP=1;
	Color=0xffffffff;
}
//---------------------------------------------------------------------------

__fastcall TGpsUnit::~TGpsUnit()
{
	TerminateThreads();
	delete Clients;
	//delete gga; //deletes by the TNmeaSummaryThread distructor;
	//delete gsa;
	//delete gst;
	//delete llq;
}
//---------------------------------------------------------------------------

void __fastcall TGpsUnit::ApplyControlsRedraw()
{
	TNmeaGGA *gga;
	TNmeaGSA *gsa;
	TNmeaGST *gst;
	TNmeaLLQ *llq;
	TNmeaCTMOTION *ctmotion;
	TNmeaHDT *hdt;
	TNmeaTWHPR *twhpr;

	TNmeaSummaryThread* nmeasummary;

	try
	{
		for(int cc=0; cc<Clients->Count; cc++)
		{
			if(Clients->Pool[cc] && Clients->Pool[cc]->NmeaSummary)
			{
				nmeasummary=Clients->Pool[cc]->NmeaSummary;

				gga=(TNmeaGGA*)nmeasummary->Get(nstGGA);
				gsa=(TNmeaGSA*)nmeasummary->Get(nstGSA);
				gst=(TNmeaGST*)nmeasummary->Get(nstGST);
				llq=(TNmeaLLQ*)nmeasummary->Get(nstLLQ);
				ctmotion=(TNmeaCTMOTION*)nmeasummary->Get(nstCTMOTION);
				hdt=(TNmeaHDT*)nmeasummary->Get(nstHDT);
				twhpr=(TNmeaTWHPR*)nmeasummary->Get(nstTWHPR);

				if(button)
				{
					try
					{
						if(Visible)
						{
							if(!GpsClient || !GpsClient->Connected) button->NormalGlyphName=NotConnected;
							else
							{
								if(gga && gga->Refreshed && !gga->Corrupted)
								{
									if(gga->Quality==0) NotIndexed=NotConnected;
									else if(gga->Quality==8) NotIndexed=Simulation;
									else
									{
										Index=gga->Satellites-4;
										if(gga->Quality==1 && Index>(int)(0.5*TIndicator::MaxIcons)) Index=(int)(0.5*TIndicator::MaxIcons); // GPS Quality
										else if(gga->Quality==2 && Index>(int)(0.5*TIndicator::MaxIcons)) Index=(int)(0.75*TIndicator::MaxIcons); //DGPS Quality
										HDOP=gga->HDOP;
									}
									switch (gga->Quality)
									{
										case 4:  Color=0xff64ff64; break;
										case 5:  Color=0xffffff64; break;
										default: Color=0xffff6464;
									}
									if(gga->DelaySinceLastUpdate>2)
									{
										if(IndexRefreshed) Index=0;
										Color=0xffff6464;
									}
								}
								if(llq && llq->Refreshed && !llq->Corrupted)
								{
									if(llq->Quality==0) NotIndexed=NotConnected;
									else if(llq->Quality==8) NotIndexed=Simulation;
									else
									{
										Index=llq->Satellites-4;
									}
									switch (llq->Quality)
									{
										case 3:  Color=0xff64ff64; break;
										case 2:  Color=0xffffff64; break;
										default: Color=0xffff6464;
									}
									if(llq->CoordinateQuality>0.05) //
									{
										if(IndexRefreshed) Index=0;
										Color=0xffff6464;
									}
								}
								if(gsa && gsa->Refreshed && !gsa->Corrupted)
								{
									if(gsa->FixType<2) NotIndexed=NotConnected;
									else if(gsa->FixType==2)
									{
										if(IndexRefreshed) Index=0;
										else NotIndexed=NotConnected;
									}
									HDOP=gsa->HDOP;
								}
								if(IndexRefreshed)
								{
									if(Index>0)
									{
										if(HDOP>MinimumValuableGPS_DOP) Index=0;
										else if(HDOP>1)
										{
											Index=(int)(((float)Index*(MinimumValuableGPS_DOP-HDOP+1)/MinimumValuableGPS_DOP)+0.5);
										}
									}
									else Index=0;
									button->NormalGlyphName=IconPaths[Index];
								}
								else
								{
									if(NotIndexedRefreshed) button->NormalGlyphName=NotIndexed;
								}
							}
						}
						else
						{
							button->NormalGlyphName=EmptyButton;
						}
					}
					catch (Exception &e) {}
				}
				if((ctmotion || hdt || twhpr) && ((TRadarMapManager*)Manager)->GyroCompass)
				{
					bool b;
					b=((TRadarMapManager*)Manager)->GyroCompass->AutomaticUpdate;
					((TRadarMapManager*)Manager)->GyroCompass->AutomaticUpdate=false;
					if(twhpr && !twhpr->Corrupted)
					{
						((TRadarMapManager*)Manager)->GyroCompass->Heading=DegToRad(twhpr->Heading);
						((TRadarMapManager*)Manager)->GyroCompass->Pitch=DegToRad(-twhpr->Pitch);
						((TRadarMapManager*)Manager)->GyroCompass->Roll=DegToRad(twhpr->Roll);
					}
					if(ctmotion && !ctmotion->Corrupted)
					{
						((TRadarMapManager*)Manager)->GyroCompass->Pitch=DegToRad(-ctmotion->Pitch);
						((TRadarMapManager*)Manager)->GyroCompass->Roll=DegToRad(ctmotion->Roll);
					}
					if(hdt && !hdt->Corrupted)	((TRadarMapManager*)Manager)->GyroCompass->Heading=DegToRad(hdt->Heading); //???????????????
					((TRadarMapManager*)Manager)->GyroCompass->AutomaticUpdate=b;
				}
				if(((TRadarMapManager*)Manager)->GpsMessage)
				{
					AnsiString gga_s="", llq_s="", gsa_s="", gst_s="", ctmotion_s="",
						hdt_s="", str;

					if(gga && !gga->Corrupted) gga_s=nmeasummary->Get(nstGGA)->AsString(false);
					if(llq && !llq->Corrupted) llq_s=nmeasummary->Get(nstLLQ)->AsString(false);
					if(gsa && !gsa->Corrupted) gsa_s=nmeasummary->Get(nstGSA)->AsString(false);
					if(gst && !gst->Corrupted) gst_s=nmeasummary->Get(nstGST)->AsString(false);
					//if(ctmotion && !ctmotion->Corrupted) ctmotion_s=nmeasummary->Get(nstCTMOTION)->AsString(false);
					//if(hdt && !hdt->Corrupted) gst_s=nmeasummary->Get(nstHDT)->AsString(false);
					if(gga_s!="") str+=gga_s;
					if(llq_s!="")
					{
						if(str!="") str+=", ";
						str+=llq_s;
					}
					if(gsa_s!="")
					{
						if(str!="") str+=", ";
						str+=gsa_s;
					}
					if(gst_s!="")
					{
						if(str!="") str+=", ";
						str+=gst_s;
					}
					if(ctmotion_s!="")
					{
						if(str!="") str+=", ";
						str+=ctmotion_s;
					}
					if(hdt_s!="")
					{
						if(str!="") str+=", ";
						str+=hdt_s;
					}
					if(str=="") str="Not connected or initializing...";
					((TRadarMapManager*)Manager)->GpsMessage->Color=Color;
					((TRadarMapManager*)Manager)->GpsMessage->Text=str;
				}
			}
		}
	}
	catch(Exception &e) {}
}
//---------------------------------------------------------------------------

void __fastcall TGpsUnit::TerminateThreads()
{
	if(RecThrd)
	{
		RecThrd->Stop();
		RecThrd->AskForTerminate();
		WaitOthers();
		if(RecThrd->ForceTerminate()) delete RecThrd;
		RecThrd=NULL;
	}
//	if(CoordinatesAcquirersList)
//	{
//		TGpsCoordinateAcquirer *thrd;
//
//		StopCoordinatesAcquirers();
//		for(int i=0; i<CoordinatesAcquirersList->Count; i++)
//		{
//			thrd=(TGpsCoordinateAcquirer*)CoordinatesAcquirersList->Items[i];
//			if(thrd)
//			{
//				thrd->AskForTerminate();
//				WaitOthers();
//				if(thrd->ForceTerminate()) delete thrd;
//			}
//			CoordinatesAcquirersList->Items[i]=NULL;
//		}
//		CoordinatesAcquirersList->Clear();
//		delete CoordinatesAcquirersList;
//	}
}
//---------------------------------------------------------------------------

void __fastcall TGpsUnit::Initialize()
{
	//for(int i=0; i<Clients->Count; i++) ResetEvent(Clients->Pool[i]->InitializedEvent);
	TerminateThreads();

//	CoordinatesAcquirersList=new TList();
//	AddCoordinatesAcquirer((TGpsCoordinateAcquirer*)(new TNmeaAcquirer(this, Manager)));
//	AddCoordinatesAcquirer((TGpsCoordinateAcquirer*)(new TPlugInAcquirer(this, Manager)));
//	StartCoordinatesAcquirers();

	RecThrd=new TNmeaReceivingThread(this, Clients, Listeners, Manager);
	RecThrd->Start();

	for(int i=0; i<Clients->Count; i++) if(!Clients->Pool[i]->Initialized) Clients->Pool[i]->Initialize();
}
//---------------------------------------------------------------------------

/*void __fastcall TGpsUnit::AddCoordinatesAcquirer(TGpsCoordinateAcquirer *thrd)
{
	if(CoordinatesAcquirersList && thrd)
	{
		CoordinatesAcquirersList->Add((void*)thrd);
	}
}
//---------------------------------------------------------------------------

void __fastcall TGpsUnit::StartCoordinatesAcquirers()
{
	if(CoordinatesAcquirersList)
	{
		TGpsCoordinateAcquirer *thrd;

		for(int i=0; i<CoordinatesAcquirersList->Count; i++)
		{
			thrd=(TGpsCoordinateAcquirer*)CoordinatesAcquirersList->Items[i];
			if(thrd) thrd->Start();
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TGpsUnit::StopCoordinatesAcquirers()
{
	if(CoordinatesAcquirersList)
	{
		TGpsCoordinateAcquirer *thrd;

		for(int i=0; i<CoordinatesAcquirersList->Count; i++)
		{
			thrd=(TGpsCoordinateAcquirer*)CoordinatesAcquirersList->Items[i];
			if(thrd) thrd->Stop();
		}
	}
}
//---------------------------------------------------------------------------

TNmeaSentenceType TGpsUnit::GetCoordinate()
{
	TNmeaSentenceType nst=nstNone;

	if(CoordinatesAcquirersList)
	{
		TGpsCoordinateAcquirer *thrd;

		for(int i=CoordinatesAcquirersList->Count-1; i>=0; i--)
		{
			thrd=(TGpsCoordinateAcquirer*)CoordinatesAcquirersList->Items[i];
			if(thrd && thrd->Freshness && thrd->Received!=nstNone)
			{
				nst=thrd->Received;
				break;
			}
		}
	}

	return nst;
}
//---------------------------------------------------------------------------*/

