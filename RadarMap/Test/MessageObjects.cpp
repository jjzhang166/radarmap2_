//---------------------------------------------------------------------------

#pragma hdrstop

#include "MessageObjects.h"
#include "Manager.h"
#include "BitmapGDI.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

//---------------------------------------------------------------------------
// TMessageObject
//---------------------------------------------------------------------------
__fastcall TMessageObject::TMessageObject(TMyObject *AOwner, TImgView32 *ImgView, Types::TRect Rect,
  TMessageVisibalityType mvt, int ATimeOut, TMessageType mt, AnsiString str, TObject* AManager,
  TVoidFunction aSwitchOffCallback) : TToolObject(AOwner, ImgView, Rect, AManager)
{
	AutomaticUpdate=false;
	try
	{
		mouseable=false;
		visible=false;
		switchedoff=false;
		if(ATimeOut<MinimalMessageTimeOut) timeout=MinimalMessageTimeOut;
		else timeout=ATimeOut;
		Font=new TFont();
		Font->Name="Tahoma";
		Font->Size=14;
		//Font->Style = Font->Style<<fsBold;
		BufBmp=new TBitmap32();
		BufBmp->DrawMode=dmBlend;
	   //	BufBmp->Font->Assign(Font);
	   //	BufBmp->MasterAlpha=255;
		TextBmp=new TBitmap32();
		TextBmp->DrawMode=dmBlend;
		TextBmp->Font->Assign(Font);
		//TextBmp->MasterAlpha=255;
		BgndBmp=new TBitmap32();
		BgndBmp->DrawMode=dmBlend;
		//BgndBmp->Font->Assign(Font);
		//BgndBmp->MasterAlpha=255;
		textcolor=clBlack32;
		MainTimerItem=NULL;
		StartAnimationTimerItem=NULL;
		OnAnimationTimerItem=NULL;
		messagetype=mt;
		text=str;
		FlashingCount=0;
		if(Rect.Width()==0 && Rect.Height()==0 && ImgView!=NULL)
			OnResize(Owner, image->GetViewportRect().Width(), image->GetViewportRect().Height());
		visability=mvt;
		SwitchOffCallback=aSwitchOffCallback;
	}
	__finally
	{
		AutomaticUpdate=true;
	}
}
//---------------------------------------------------------------------------

__fastcall TMessageObject::~TMessageObject()
{
	try
	{
		if(MainTimerItem) MainTimerItem->OnTimer=NULL;
		if(StartAnimationTimerItem) StartAnimationTimerItem->OnTimer=NULL;
		if(OnAnimationTimerItem) OnAnimationTimerItem->OnTimer=NULL;
		if(BufBmp) delete BufBmp;
		BufBmp=NULL;
		if(TextBmp) delete TextBmp;
		TextBmp=NULL;
		if(BgndBmp) delete BgndBmp;
		BgndBmp=NULL;
		if(Font) delete Font;
		Font=NULL;
	}
	catch (Exception &e) {}
}
//---------------------------------------------------------------------------

void __fastcall TMessageObject::writeVisible(bool value)
{
	if(visible!=value)
	{
		visible=value;
		if(visible)
		{
			BufBmp->MasterAlpha=0xff;
			AnimationInProgress=false;
		}
		else
		{
			BufBmp->MasterAlpha=0x00;
			if(visability==mvtDisapering) SwitchOff();
		}
		Rebuild();
	}
}
//---------------------------------------------------------------------------

void __fastcall TMessageObject::OnTimer()//TCustomTimerThread* Ptr)
{
	try
	{
		Visible=!Visible;
		if(OnAnimationTimerItem && StartAnimationTimerItem && MainTimerItem)
		{
			OnAnimationTimerItem->Start_tick=StartAnimationTimerItem->Start_tick=MainTimerItem->Start_tick;
			StartAnimationTimerItem->Enabled=Visible;
			OnAnimationTimerItem->Enabled=Visible;
		}
		FlashingCount+=Visible;
		if(!Visible && FlashingCount>=MaxFlashingCount) SwitchOff();
	}
	catch(Exception &e) {}
}
//---------------------------------------------------------------------------

void __fastcall TMessageObject::StartMessageAnimation()//TMessageAnimationType aType)//TCustomTimerThread* Ptr)
{
	try
	{
		AnimationType=atDisappear;
		BufBmp->MasterAlpha=0xff;
		AnimationInProgress=true;
		if(OnAnimationTimerItem) OnAnimationTimerItem->OnTimer=NULL;
		if(StartAnimationTimerItem && StartAnimationTimerItem->Owner)
		{
			OnAnimationTimerItem=StartAnimationTimerItem->Owner->AddTimer((int)((float)(timeout-StartAnimationTimerItem->Interval)/(float)AnimationFramesCount), OnAnimation);
		}
	}
	catch(Exception &e) {}
}
//---------------------------------------------------------------------------

void __fastcall TMessageObject::OnAnimation()//TCustomTimerThread* Ptr)
{
	int malpha;

	try
	{
		if(AnimationInProgress)
		{
			//BufBmp->MasterAlpha-=(int)((float)0xff/(float)AnimationFramesCount);
			malpha=BufBmp->MasterAlpha+(int)AnimationType*(int)((float)0xff/(float)AnimationFramesCount);
			if(malpha<0) malpha=0;
			else if(malpha>255) malpha=255;
			BufBmp->MasterAlpha=malpha;
			if(BufBmp->MasterAlpha<=0) AnimationInProgress=false;
			Update();
		}
		if(!AnimationInProgress)
		{
			if(OnAnimationTimerItem) OnAnimationTimerItem->OnTimer=NULL;
			OnAnimationTimerItem=NULL;
		}
	}
	catch(...) {}
}
//---------------------------------------------------------------------------

void __fastcall TMessageObject::OnResize(TObject *Sender, int NewWidth, int NewHeight)
{
	int w, h, x, y;
	Types::TRect ir;

	TImgView32 *image;
	if(Owner && ((TGraphicObjectsContainer*)Owner)->Image)
	{
		image=((TGraphicObjectsContainer*)Owner)->Image;
		NewWidth-=(image->Width-image->GetViewportRect().Width());
		NewHeight-=(image->Height-image->GetViewportRect().Height());
	}
	Output=Types::TRect(0, 0, NewWidth, NewHeight);
	h=MessageBarHeight;
	y=MessageBarBorderShift;
	w=NewWidth*0.8;
	if(w<100) w=100;
	x=(NewWidth-w)>>1;
	Rect=Types::TRect(x, y, x+w, y+h);
	Rebuild();
}
//---------------------------------------------------------------------------

void __fastcall TMessageObject::Rebuild(bool WithUpdate)
{
	int gw, x, y, h, w;
	float dc, dcc;
	TColor32 cl;
	TColor32Entry ce, ce2;

	if(BufBmp->MasterAlpha!=0x00)
	{
		w=Rect.Width();
		h=Rect.Height();
		BufBmp->SetSize(w, h);
		TextBmp->SetSize(w, h);
		gw=(int)((float)w*0.2);
		ce.ARGB=Color;
		ce.A=(Byte)((float)ce.A*0.8);//0.9);
		dc=(float)ce.A/(float)gw;///2.;
		ce2.ARGB=Color;
		dcc=dc;//(float)ce.A/2.;//dc;//0;
		y=h-1;
		BgndBmp->BeginUpdate();
		try
		{
			BgndBmp->SetSize(w, h);
			BgndBmp->Clear(Color32(0, 0, 0, 0));
			for(int i=0; i<gw; i++)
			{
				x=w-1-i;
				ce2.A=(Byte)dcc;
				BgndBmp->LineT(i, 0, i, y, ce2.ARGB);
				BgndBmp->LineT(x, 0, x, y, ce2.ARGB);
				dcc+=dc;
			}
			BgndBmp->FillRectT(gw, 0, w-gw, y, ce.ARGB);
		}
		__finally
		{
			BgndBmp->EndUpdate();
		}
		ReText();
	}
	if(WithUpdate) Update();
}
//---------------------------------------------------------------------------

void __fastcall TMessageObject::ReText()
{
	int tw, th, w, h, of=10;
	AnsiString str;

	try
	{
		if(text!=NULL)
		{
			w=TextBmp->Width;
			h=TextBmp->Height;
			str=text;
			tw=TextBmp->TextWidth(str);
			while(str.Length()>0 && tw>(w-of*2))
			{
				str.SetLength(str.Length()-4);
				str=str.Trim()+"...";
				tw=TextBmp->TextWidth(str);
			}
			TextBmp->BeginUpdate();
			try
			{
				TextBmp->Clear(Color32(0, 0, 0, 0));
				th=TextBmp->TextHeight(str);
				TextBmp->RenderText((w-tw)>>1, (h-th)>>1, str, 2, textcolor);
			}
			__finally
			{
				TextBmp->EndUpdate();
			}
		}
	}
	catch(...) {}
}
//---------------------------------------------------------------------------

void __fastcall TMessageObject::Redraw(TObject *Sender, TBitmap32 *Bmp)
{
	if(Bmp && visible)
	{
		Bmp->BeginUpdate();
		try
		{
			BufBmp->Clear(Color32(0, 0, 0, 0));
			BufBmp->Draw(0, 0, BgndBmp);
			BufBmp->Draw(0, 0, TextBmp);
			Bmp->Draw(Rect.Left, Rect.Top, BufBmp);
		}
		__finally
		{
			Bmp->EndUpdate();
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TMessageObject::SwitchOff()
{
	if(!switchedoff)
	{
		switchedoff=true;
		try
		{
			if(SwitchOffCallback) (SwitchOffCallback)();
		}
		catch(...) {}
		Visible=false;
    }
}
//---------------------------------------------------------------------------

void __fastcall TMessageObject::AssignTimers(TMultiTimerThread *TimerThrd)
{
	int t;

	AnimationInProgress=false;
	if(visability!=mvtConstant)
	{
		MainTimerItem=TimerThrd->AddTimer(TimeOut, OnTimer);
		if(((TRadarMapManager*)Manager)->Settings->MessagesAnimation)
		{
			t=(int)((float)timeout/4.);
			StartAnimationTimerItem=TimerThrd->AddTimer(timeout-t, StartMessageAnimation);
			//OnAnimationTimerItem=TimerThrd->AddTimer((int)((float)t/(float)AnimationFramesCount), OnAnimation);
		}
	}
}

//---------------------------------------------------------------------------
// TPosNotificationObject
//---------------------------------------------------------------------------
__fastcall TPosNotificationObject::TPosNotificationObject(TMyObject *AOwner,
	TImgView32 *ImgView, Types::TRect Rect, TMessageVisibalityType mvt,
	int ATimeOut, TColor32 AColor, AnsiString str, Types::TPoint aSP, TObject* AManager) :
	TMessageObject(AOwner, ImgView, Rect, mvt, ATimeOut, mtText, str, AManager)
{
	bool err;
	TDoublePoint dp;

	color=AColor;
	Initialize();
	try
	{
		if(Owner && Owner->ObjectType==gotContainer && Manager && ((TRadarMapManager*)Manager)->MapObjects &&
			((TRadarMapManager*)Manager)->MapObjects->Container && (aSP.x!=0 || aSP.y!=0))
		{
			StartingVPP=((TRadarMapManager*)Manager)->MapObjects->Container->ScreenToViewportArea(aSP);
			dp=((TRadarMapManager*)Manager)->MapObjects->Container->ScreenToCoordinatesRect(aSP);
			if(dp.X!=0 || dp.Y!=0)//(CoordinatesRect && CoordinatesRect->Valid)
			{
				coordinate=new TGPSCoordinate();
				coordinate->SetXY(((TRadarMapManager*)Manager)->MapObjects->Container->CoordinateSystem, dp);
			}
			else coordinate=NULL;
			err=false;
		}
		else err=true;
	}
	catch(Exception &e) {err=true;}
	if(err)
	{
		StartingVPP=DoublePoint(0,0);
		coordinate=NULL;
		Rect=Types::TRect(0,0,0,0);
	}
}
//---------------------------------------------------------------------------

__fastcall TPosNotificationObject::TPosNotificationObject(TMyObject *AOwner, TImgView32 *ImgView,
	Types::TRect Rect, TMessageVisibalityType mvt, int ATimeOut, TColor32 AColor,
	AnsiString str, TGPSCoordinate *aCoordinate, TObject* AManager) :
	TMessageObject(AOwner, ImgView, Rect, mvt, ATimeOut, mtText, str, AManager)
{
	bool err;

	color=AColor;
	Initialize();
	try
	{
		if(Owner && Owner->ObjectType==gotContainer &&
			Manager && ((TRadarMapManager*)Manager)->MapObjects &&
			((TRadarMapManager*)Manager)->MapObjects->Container && aCoordinate)
		{
			coordinate=new TGPSCoordinate(aCoordinate);
			StartingVPP=((TRadarMapManager*)Manager)->MapObjects->Container->CoordinatesRectToViewportArea(
				Coordinate->GetXY(((TRadarMapManager*)Manager)->MapObjects->Container->CoordinateSystem));
			err=false;
		}
		else err=true;
	}
	catch(Exception &e) {err=true;}
	if(err)
	{
		StartingVPP=DoublePoint(0,0);
		coordinate=NULL;
		Rect=Types::TRect(0,0,0,0);
	}
}
//---------------------------------------------------------------------------

__fastcall TPosNotificationObject::TPosNotificationObject(TMyObject *AOwner, TImgView32 *ImgView,
	Types::TRect Rect, TMessageVisibalityType mvt, int ATimeOut, TColor32 AColor,
	AnsiString str, TDoublePoint aSvpp, TObject* AManager) :
	TMessageObject(AOwner, ImgView, Rect, mvt, ATimeOut, mtText, str, AManager)
{
	bool err;
	TDoublePoint dp;

	color=AColor;
	Initialize();
	try
	{
		if(Owner && Owner->ObjectType==gotContainer &&
			Manager && ((TRadarMapManager*)Manager)->MapObjects &&
			((TRadarMapManager*)Manager)->MapObjects->Container && (aSvpp.X!=0 || aSvpp.Y!=0))
		{
			StartingVPP=aSvpp;
			dp=((TRadarMapManager*)Manager)->MapObjects->Container->ViewportAreaToCoordinatesRect(aSvpp);
			if(dp.X!=0 || dp.Y!=0)//(CoordinatesRect && CoordinatesRect->Valid)
			{
				coordinate=new TGPSCoordinate();
				coordinate->SetXY(((TRadarMapManager*)Manager)->MapObjects->Container->CoordinateSystem, dp);
				screenxy=((TRadarMapManager*)Manager)->MapObjects->Container->ViewportAreaToScreen(aSvpp);
			}
			else coordinate=NULL;
			err=false;
		}
		else err=true;
	}
	catch(Exception &e) {err=true;}
	if(err)
	{
		StartingVPP=DoublePoint(0,0);
		screenxy=Types::TPoint(-1, -1);
		BgndXY=Types::TPoint(-1, -1);
		coordinate=NULL;
		Rect=Types::TRect(0,0,0,0);
	}
}
//---------------------------------------------------------------------------

void __fastcall TPosNotificationObject::Initialize()
{
	Font->Size=10;
	TextBmp->Font->Assign(Font);
	visible=false;
	textcolor=clBlack32;
	Closing=false;
	CloudBmp=NULL;
	cloudbmpname="";
	LeftCloud="PNGIMAGE_462"; LeftCloudLP=Types::TPoint(218, 26);//(197, 26);
	TopCloud="PNGIMAGE_463"; TopCloudLP=Types::TPoint(26, 97);
	RightCloud="PNGIMAGE_464"; RightCloudLP=Types::TPoint(2, 26);
	BottomCloud="PNGIMAGE_465"; BottomCloudLP=Types::TPoint(26, 2);
	mouseable=true;
	strings=new TStringList;
	TextRect=Types::TRect(0,0,0,0);
	lasttextxy=Types::TPoint(-1, -1);
	LastText="";
	coordinate=NULL;
}
//---------------------------------------------------------------------------

__fastcall TPosNotificationObject::~TPosNotificationObject()
{
	Closing=true;
	if(coordinate) delete coordinate;
	coordinate=NULL;
	CloudBmpName="";
	if(strings) delete strings;
	strings=NULL;
}
//---------------------------------------------------------------------------

bool __fastcall TPosNotificationObject::StartAnimation(TAnimationType aType)
{
	bool res=true;

	try
	{
		if(aType==atDisappear) BufBmp->MasterAlpha=0xff;
		else
		{
			BufBmp->MasterAlpha=0x00;
			visible=true;
		}
		TGraphicObject::StartAnimation(aType);
	}
	catch(Exception &e) {res=false;}

	return res;
}
//---------------------------------------------------------------------------

void __fastcall TPosNotificationObject::OnAnimation()//TCustomTimerThread* Ptr)
{
	int malpha;

	try
	{
		if(BufBmp && AnimationInProgress)
		{
			malpha=BufBmp->MasterAlpha+(int)AnimationType*50;
			if(malpha<0) malpha=0;
			else if(malpha>255) malpha=255;
			BufBmp->MasterAlpha=malpha;
			if((AnimationType==atDisappear && BufBmp->MasterAlpha<=0))
			{
				AnimationInProgress=false;
				Visible=false;
			}
			else
			{
				if((AnimationType==atAppear && BufBmp->MasterAlpha>=255)) AnimationInProgress=false;
				Update();
			}
		}
	}
	catch(...) {}
}
//---------------------------------------------------------------------------

void __fastcall TPosNotificationObject::writeCloudBmpName(AnsiString value)
{
	if(cloudbmpname!=value)
	{
		if(cloudbmpname!="")
		{
			CloudBmp=NULL;
			RemovePngFromContainer(cloudbmpname);
		}
		if(value!="") CloudBmp=GetPngFromContainer(value);
		cloudbmpname=value;
	}
}
//---------------------------------------------------------------------------

void __fastcall TPosNotificationObject::Rebuild(bool WithUpdate)
{
	Types::TRect vRect;
	Types::TPoint dp, dp2, dp3;
	int w, h;

	if(Closing) return;

	if(BufBmp->MasterAlpha!=0x00 && visible && Owner && ((TMapObjectsContainer*)Owner)->Image &&
		Manager && ((TRadarMapManager*)Manager)->MapObjects &&
		((TRadarMapManager*)Manager)->MapObjects->Container)
	{
		vRect=((TMapObjectsContainer*)Owner)->Image->GetViewportRect();
		if(Coordinate)// && CoordinatesRect && CoordinatesRect->Valid)
			dp=((TRadarMapManager*)Manager)->MapObjects->Container->CoordinatesRectToScreen(
				Coordinate->GetXY(((TRadarMapManager*)Manager)->MapObjects->Container->CoordinateSystem));
		else dp=((TRadarMapManager*)Manager)->MapObjects->Container->ViewportAreaToScreen(StartingVPP);
		screenxy=dp;
		if(vRect.Contains(dp))
		{
			CloudBmpName=RightCloud;
			if(CloudBmp && CloudBmp->Width>0 && CloudBmp->Height>0 &&
				CloudBmp->Width<vRect.Width() && CloudBmp->Height<vRect.Height() &&
				(vRect.Width()-dp.x)>(CloudBmp->Width+CloudBmp->Height) &&
				dp.y>(CloudBmp->Height>>1) &&
				(vRect.Height()-dp.y)>(CloudBmp->Height>>1))
			{
				dp2=RightCloudLP;
				dp3.x=dp.x+(CloudBmp->Height>>1);
				dp3.y=dp.y-(CloudBmp->Height>>1);
				w=dp3.x+CloudBmp->Width-dp.x;
				h=CloudBmp->Height;
				BgndXY=Types::TPoint(dp.x, dp3.y);
				TextRect=Types::TRect(5, 0, CloudBmp->Width, CloudBmp->Height);
			}
			else
			{
				CloudBmpName=LeftCloud;
				if(CloudBmp && CloudBmp->Width>0 && CloudBmp->Height>0 &&
					CloudBmp->Width<vRect.Width() && CloudBmp->Height<vRect.Height() &&
					dp.x>(CloudBmp->Width+CloudBmp->Height) &&
					dp.y>(CloudBmp->Height>>1) &&
					(vRect.Height()-dp.y)>(CloudBmp->Height>>1))
				{
					dp2=LeftCloudLP;
					dp3.x=dp.x-CloudBmp->Width-(CloudBmp->Height>>1);
					dp3.y=dp.y-(CloudBmp->Height>>1);
					w=dp.x-dp3.x;
					h=CloudBmp->Height;
					BgndXY=dp3;
					TextRect=Types::TRect(0, 0, CloudBmp->Width-5, CloudBmp->Height);
				}
				else
				{
					CloudBmpName=TopCloud;
					if(CloudBmp && CloudBmp->Width>0 && CloudBmp->Height>0 &&
						CloudBmp->Width<vRect.Width() && CloudBmp->Height<vRect.Height() &&
						dp.y>(CloudBmp->Height<<1))
					{
						dp2=TopCloudLP;
						dp3.x=dp.x-(CloudBmp->Width>>1);
						dp3.y=dp.y-3*(CloudBmp->Height>>1);
						w=CloudBmp->Width;
						h=dp.y-dp3.y;
						BgndXY=dp3;
						TextRect=Types::TRect(0, 0, CloudBmp->Width, CloudBmp->Height-5);
					}
					else
					{
						CloudBmpName=BottomCloud;
						if(CloudBmp && CloudBmp->Width>0 && CloudBmp->Height>0 &&
							CloudBmp->Width<vRect.Width() && CloudBmp->Height<vRect.Height())
						{
							dp2=BottomCloudLP;
							dp3.x=dp.x-(CloudBmp->Width>>1);
							dp3.y=dp.y+(CloudBmp->Height>>1);
							w=CloudBmp->Width;
							h=dp3.y+CloudBmp->Height-dp.y;
							BgndXY=Types::TPoint(dp3.x, dp.y);
							TextRect=Types::TRect(0, 5, CloudBmp->Width, CloudBmp->Height);
						}
						else
						{
							CloudBmpName="";
							BgndXY=Types::TPoint(-1, -1);
						}
					}
				}
			}
			if(CloudBmp)
			{
				TColor32Entry ce;
				TBitmap32* tmp;
				int k;

				if(dp3.x<0) BgndXY.x=dp3.x=0;
				else if((vRect.Width()-dp3.x)<CloudBmp->Width) BgndXY.x=dp3.x=vRect.Width()-CloudBmp->Width;
				if(dp3.y<0) BgndXY.y=dp3.y=0;
				else if((vRect.Height()-dp3.y)<CloudBmp->Height) BgndXY.y=dp3.y=vRect.Height()-CloudBmp->Height;
				//TextXY.x+=dp3.x-BgndXY.x;
				//TextXY.y+=dp3.y-BgndXY.y;
				k=dp3.x-BgndXY.x;
				TextRect.Left+=k;
				TextRect.Right+=k;
				k=dp3.y-BgndXY.y;
				TextRect.Top+=k;
				TextRect.Bottom+=k;

				BgndBmp->BeginUpdate();
				try
				{
					BgndBmp->SetSize(w, h);
					ce.ARGB=color;
					//ce.A=200;
					BgndBmp->Clear(Color32(ce.R,ce.G,ce.B,0));
					BgndBmp->DrawMode=dmBlend;
					BgndBmp->LineAS(dp.x-BgndXY.x, dp.y-BgndXY.y, dp3.x+dp2.x-BgndXY.x, dp3.y+dp2.y-BgndXY.y, ce.ARGB);
					tmp=new TBitmap32;
					try
					{
						tmp->Assign(CloudBmp);
						ApplyColorMask(tmp, color, textcolor, dmTransparent);
						//BgndBmp->DrawMode=dmTransparent;
						BgndBmp->Draw(dp3.x-BgndXY.x, dp3.y-BgndXY.y, tmp);
						Rect=Types::TRect(dp3.x, dp3.y, dp3.x+tmp->Width, dp3.y+tmp->Height);
						//Rect=Types::TRect(BgndXY.x, BgndXY.y, BgndXY.x+tmp->Width, BgndXY.y+tmp->Height);
					}
					__finally
					{
						delete tmp;
					}
				}
				__finally
				{
					BgndBmp->EndUpdate();
				}
				ReText();
			}
		}
		else Rect=Types::TRect(0,0,0,0);
		if(WithUpdate) Update();
	}
	else Rect=Types::TRect(0,0,0,0);
}
//---------------------------------------------------------------------------

void __fastcall TPosNotificationObject::ReText()
{
	int tw, th, j, of=10, i;
	AnsiString str;

	if(Types::TPoint(TextRect.Left, TextRect.Top)!=lasttextxy || LastText!=Strings->Text)
	{
		try
		{
			TextBmp->BeginUpdate();
			try
			{
				TextBmp->Clear(Color32(0, 0, 0, 0));
				TextBmp->SetSizeFrom(BgndBmp);

				th=of+TextRect.Top;
				for(int i=-1; i<Strings->Count; i++)
				{
					if(i==-1) str=text;
					else str=Strings->Strings[i];
					if(str!=NULL)
					{
						j=TextBmp->TextHeight(str);//+of;
						tw=TextBmp->TextWidth(str);
						if(th+j>TextRect.Bottom-of) str+="...";
						while(str.Length()>0 && tw>(TextRect.Width()-of*2))//(w-of*2-TextRect.Left))
						{
							str.SetLength(str.Length()-4);
							str=str.Trim()+"...";
							tw=TextBmp->TextWidth(str);
						}
						TextBmp->RenderText(of+TextRect.Left, th, str, 0, textcolor);
						th+=j;
						if(th>TextRect.Bottom-of) break;
					}
				}
				lasttextxy=Types::TPoint(TextRect.Left, TextRect.Top);
				LastText=Strings->Text;
			}
			__finally
			{
				TextBmp->EndUpdate();
			}
		}
		catch(...) {}
	}
}
//---------------------------------------------------------------------------

/**/void __fastcall TPosNotificationObject::Redraw(TObject *Sender, TBitmap32 *Bmp)
{
	Types::TPoint p;
	Types::TRect vRect;

	if(Bmp && visible)
	{
		if(Owner && ((TMapObjectsContainer*)Owner)->Image &&
			Manager && ((TRadarMapManager*)Manager)->MapObjects &&
			((TRadarMapManager*)Manager)->MapObjects->Container)
		{
			if(Coordinate)
				p=((TRadarMapManager*)Manager)->MapObjects->Container->CoordinatesRectToScreen(
					Coordinate->GetXY(((TRadarMapManager*)Manager)->MapObjects->Container->CoordinateSystem));
			else p=((TRadarMapManager*)Manager)->MapObjects->Container->ViewportAreaToScreen(StartingVPP);
			if(p!=BgndXY) Rebuild(false);
		}
		vRect=((TMapObjectsContainer*)Owner)->Image->GetViewportRect();
		if(vRect.Contains(p) && BgndXY.x>=0 && BgndXY.y>=0 && BufBmp)
		{
			Bmp->BeginUpdate();
			try
			{
				try
				{
					BufBmp->BeginUpdate();
					BufBmp->SetSize(BgndBmp->Width, BgndBmp->Height);
					BufBmp->Clear(Color32(255,255,255,0));
					BufBmp->Draw(0, 0, BgndBmp);
					BufBmp->Draw(0, 0, TextBmp);
					BufBmp->EndUpdate();
					//Bmp->Draw(Rect.Left, Rect.Top, //BgndBmp);//BufBmp);
					Bmp->Draw(BgndXY.x, BgndXY.y, BufBmp);//BgndBmp);
				}
				catch(...) {}
			}
			__finally
			{
				Bmp->EndUpdate();
			}
		}
	}
}

/*void __fastcall TPosNotificationObject::Redraw(TObject *Sender, TBitmap32 *Bmp)
{
	Types::TRect vRect;
	Types::TPoint dp, dp2, dp3;

	if(Closing) return;
	if(visible && Bmp && Owner && ((TMapObjectsContainer*)Owner)->Image &&
		Manager && ((TRadarMapManager*)Manager)->MapObjects &&
		((TRadarMapManager*)Manager)->MapObjects->Container)
	{
		vRect=((TMapObjectsContainer*)Owner)->Image->GetViewportRect();
		if(Coordinate)// && CoordinatesRect && CoordinatesRect->Valid)
			dp=((TRadarMapManager*)Manager)->MapObjects->Container->CoordinatesRectToScreen(
				Coordinate->GetXY(((TRadarMapManager*)Manager)->MapObjects->Container->CoordinateSystem));
		else dp=((TRadarMapManager*)Manager)->MapObjects->Container->ViewportAreaToScreen(StartingVPP);
		if(vRect.Contains(dp))
		{
			CloudBmpName=RightCloud;
			if(CloudBmp && CloudBmp->Width>0 && CloudBmp->Height>0 &&
				CloudBmp->Width<vRect.Width() && CloudBmp->Height<vRect.Height() &&
				(vRect.Width()-dp.x)>(CloudBmp->Width+CloudBmp->Height) &&
				dp.y>(CloudBmp->Height>>1) &&
				(vRect.Height()-dp.y)>(CloudBmp->Height>>1))
			{
				dp2=RightCloudLP;
				dp3.x=dp.x+(CloudBmp->Height>>1);
				dp3.y=dp.y-(CloudBmp->Height>>1);
			}
			else
			{
				CloudBmpName=LeftCloud;
				if(CloudBmp && CloudBmp->Width>0 && CloudBmp->Height>0 &&
					CloudBmp->Width<vRect.Width() && CloudBmp->Height<vRect.Height() &&
					dp.x>(CloudBmp->Width+CloudBmp->Height) &&
					dp.y>(CloudBmp->Height>>1) &&
					(vRect.Height()-dp.y)>(CloudBmp->Height>>1))
				{
					dp2=LeftCloudLP;
					dp3.x=dp.x-CloudBmp->Width-(CloudBmp->Height>>1);
					dp3.y=dp.y-(CloudBmp->Height>>1);
				}
				else
				{
					CloudBmpName=TopCloud;
					if(CloudBmp && CloudBmp->Width>0 && CloudBmp->Height>0 &&
						CloudBmp->Width<vRect.Width() && CloudBmp->Height<vRect.Height() &&
						dp.y>(CloudBmp->Height<<1))
					{
						dp2=TopCloudLP;
						dp3.x=dp.x-(CloudBmp->Width>>1);
						dp3.y=dp.y-3*(CloudBmp->Height>>1);
					}
					else
					{
						CloudBmpName=BottomCloud;
						if(CloudBmp && CloudBmp->Width>0 && CloudBmp->Height>0 &&
							CloudBmp->Width<vRect.Width() && CloudBmp->Height<vRect.Height())
						{
							dp2=BottomCloudLP;
							dp3.x=dp.x-(CloudBmp->Width>>1);
							dp3.y=dp.y+(CloudBmp->Height>>1);
						}
						else CloudBmpName="";
					}
				}
			}
			if(CloudBmp)
			{
				TColor32Entry ce;
				TBitmap32* tmp;
				ce.ARGB = color;
				ce.A=128;

				if(dp3.x<0) dp3.x=0;
				else if((vRect.Width()-dp3.x)<CloudBmp->Width) dp3.x=vRect.Width()-CloudBmp->Width;
				if(dp3.y<0) dp3.y=0;
				else if((vRect.Height()-dp3.y)<CloudBmp->Height) dp3.y=vRect.Height()-CloudBmp->Height;
				Bmp->LineAS(dp.x, dp.y, dp3.x+dp2.x, dp3.y+dp2.y, ce.ARGB);
				tmp=new TBitmap32;
				try
				{
					tmp->Assign(CloudBmp);
					ApplyColorMask(tmp, color, textcolor);
					Bmp->Draw(dp3.x, dp3.y, tmp);
					Rect=Types::TRect(dp3.x, dp3.y, dp3.x+tmp->Width, dp3.y+tmp->Height);

					//Peredelat' cherez *BufBmp, *TextBmp, *BgndBmp;

				}
				__finally
				{
					delete tmp;
				}
			}
		}
	}
}*/

//---------------------------------------------------------------------------
// TMessagesScrollObject
//---------------------------------------------------------------------------
__fastcall TMessagesScrollObject::TMessagesScrollObject(TMyObject *AOwner, TImgView32 *ImgView, Types::TRect Rect,
	TMessagesScrollAlign msa, TObject* AManager) : TToolObject(AOwner, ImgView, Rect, AManager)
{
	align=msa;
	mouseable=false;
	visible=true;
	LastAdded=NULL;
	Messages=new TGraphicObjectsList(this);
	TimerThrd=NULL;
	GarbageEaterTimer=NULL;
	maxmessagesquantity=10;
	CreateThreads();
}
//---------------------------------------------------------------------------

__fastcall TMessagesScrollObject::~TMessagesScrollObject()
{
	TerminateThreads();
	delete Messages;
}
//---------------------------------------------------------------------------

void __fastcall TMessagesScrollObject::CreateThreads()
{
	TerminateThreads();
	TimerThrd=new TMultiTimerThread();
}
//---------------------------------------------------------------------------

void __fastcall TMessagesScrollObject::TerminateThreads()
{
	if(TimerThrd!=NULL)
	{
		TimerThrd->AskForTerminate();
		WaitOthers();
		if(TimerThrd->ForceTerminate()) delete TimerThrd;
		else TimerThrd->FreeOnTerminate=true;
		TimerThrd=NULL;
	}
}
//---------------------------------------------------------------------------

int __fastcall TMessagesScrollObject::AddObject(TMyObject* o)
{
	TMessageObject *mo, *mo2;
	int res=-1;

	try
	{
		mo=(TMessageObject*)o;
		if(mo && (!LastAdded || LastAdded->Text!=mo->Text || LastAdded->MessageType!=mo->MessageType || LastAdded->Visability!=mo->Visability))
		{
			if(mo->Visability==mvtDisapering)
			{
				try
				{
					for(int j=0; j<Messages->Count; j++)
					{
						mo2=(TMessageObject*)Messages->Items[j];
						if(mo2 && mo2->Visability==mvtDisapering && mo2->MessageType==mo->MessageType)
							mo2->SwitchOff();
					}
				}
				catch(Exception &e) {}
			}
			res=Messages->Add(mo);
			if(res>=0)
			{
				if(Messages->Count>MaxMessagesQuantity) writeMaxMessagesQuantity(MaxMessagesQuantity);
				LastAdded=mo;
				if(!GarbageEaterTimer) GarbageEaterTimer=TimerThrd->AddTimer(TimerThrd->MinStep*2, GarbageEater);
				mo->AssignTimers(TimerThrd);
			}
		}
	}
	catch(Exception &e) {}
	return res;
}
//---------------------------------------------------------------------------

void __fastcall TMessagesScrollObject::writeMaxMessagesQuantity(int value)
{
	int j, cnt;
	TMessageObject *mo;

	maxmessagesquantity=value;
	cnt=Messages->Count;
	if(cnt>maxmessagesquantity)
	{
		for(j=0; j<cnt-maxmessagesquantity; j++)
		{
			mo=(TMessageObject*)Messages->Items[j];
			if(mo) mo->SwitchOff();
		}
		GarbageEater();
	}
}
//---------------------------------------------------------------------------

void __fastcall TMessagesScrollObject::RemoveChildFromList(TMyObject* o)
{
	try
	{
		Messages->Remove(((TMessageObject*)o)->Index);
		Update();
	}
	catch(Exception &e) {}
}
//---------------------------------------------------------------------------

void __fastcall TMessagesScrollObject::GarbageEater()
{
	TMessageObject *mo;
	int i, cnt;

	cnt=Messages->Count;
	i=0;
	while(i<cnt)
	{
		try
		{
			mo=(TMessageObject*)Messages->Items[i];
			if(mo!=NULL && mo->SwitchedOff)
			{
				if(LastAdded && LastAdded->Text==mo->Text && LastAdded->MessageType==mo->MessageType && LastAdded->Visability==mo->Visability)
					LastAdded=NULL;
				Messages->Delete(i);
				cnt--;
			}
			else i++;
		}
		catch(Exception &e) {}
	}
	if(cnt==0)
	{
		LastAdded=NULL;
		GarbageEaterTimer->OnTimer=NULL;
		GarbageEaterTimer=NULL;
	}
}
//---------------------------------------------------------------------------

void __fastcall TMessagesScrollObject::Rebuild()
{
	TMessageObject *mo;
	int i, y, w, x, s;
	Types::TRect rct;

	rct=image->GetViewportRect();
	w=(int)((float)rct.Width()*0.95);
	x=(rct.Width()-w)>>1;
	if(w<100) w=rct.Width()-x;
	s=MessageBarHeight+MessageBarBetweenShift;
	y=MessageBarBorderShift;
	if(align==msaBottom)
	{
		y=rct.Height()-y-MessageBarHeight;
		s*=(-1);
	}
	for(i=0; i<Messages->Count; i++)
	{
		if(i>=MaxMessageBars) break;
		try
		{
			mo=(TMessageObject*)Messages->Items[i];
			if(mo!=NULL && !mo->SwitchedOff)
			{
				mo->ChangeClientRectByOwner(this, Types::TRect(x, y, x+w, y+MessageBarHeight));

				y+=s;
			}
		}
		catch(Exception &e) {}
	}
}
//---------------------------------------------------------------------------

void __fastcall TMessagesScrollObject::Update(TMyObjectType Type)
{
	Rebuild();
	TToolObject::Update(Type);
}
//---------------------------------------------------------------------------

void __fastcall TMessagesScrollObject::OnResize(TObject *Sender, int NewWidth, int NewHeight)
{
	TImgView32 *image;
	if(Owner && ((TGraphicObjectsContainer*)Owner)->Image)
	{
		image=((TGraphicObjectsContainer*)Owner)->Image;
		NewWidth-=(image->Width-image->GetViewportRect().Width());
		NewHeight-=(image->Height-image->GetViewportRect().Height());
	}
	Rect=Types::TRect(0, 0, NewWidth, NewHeight);
	Update();
}
//---------------------------------------------------------------------------

void __fastcall TMessagesScrollObject::Redraw(TObject *Sender, TBitmap32 *Bmp)
{
	TMessageObject *mo;
	Types::TRect rct;

	if(Visible && Bmp!=NULL && Rect.Width()!=0 && Rect.Height()!=0)
	{
		rct=Rect;
		if(rct.IsInRect(Owner->ViewPort))
		{
			for(int i=0; i<Messages->Count; i++)
			{
				if(i>=MaxMessageBars) break;
				try
				{
					mo=(TMessageObject*)Messages->Items[i];
					if(mo && !mo->SwitchedOff) mo->Redraw(Sender, Bmp);
				}
				catch (Exception &e) {}
			}
		}
	}
}
//---------------------------------------------------------------------------

TMessageObject* __fastcall TMessagesScrollObject::AddMessage(TMessageType mt, AnsiString str,
	TMessageVisibalityType mvt, int ATimeOut, TVoidFunction SwitchOffCallback)
{
	TMessageObject* mo;

	if(!LastAdded || LastAdded->Text!=str || LastAdded->MessageType!=mt || LastAdded->Visability!=mvt)
	{
		mo=new TMessageObject(NULL, image, Types::TRect(0,1,0,1), mvt, ATimeOut, mt, str, Manager, SwitchOffCallback);
		try
		{
			if(AddObject(mo)>=0) mo->Show();
		}
		catch(Exception &e)
		{
			delete mo;
			mo=NULL;
		}
	}
	return mo;
}