//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "GeometryForm.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "ImageButton"
#pragma link "G32_ProgressBar"
#pragma link "GR32_Image"
#pragma resource "*.dfm"
TGeometryInfoForm *GeometryInfoForm;

//---------------------------------------------------------------------------
__fastcall TGeometryInfoForm::TGeometryInfoForm(TComponent* Owner)
	: TForm(Owner)
{
	manager=NULL;
	geometry=NULL;
	pointpx=Types::TPoint(0, 0);
	TitleDown=false;
}
//---------------------------------------------------------------------------

void __fastcall TGeometryInfoForm::writeGeometry(TMapLabelGeometry* value)
{
	AnsiString str;
	double z;

	if(value)
	{
		try
		{
			TextMemo->Lines->Text=value->Description;
			IDEdit->Text=IntToStr((int)value->ID);
			NameEdit->Text=value->Name;
		}
		catch(Exception &e)
		{
			value=NULL;
		}
		ImageButton1->Enabled=true;
		ImageButton7->Enabled=true;
		ImageButton6->Enabled=true;
		ImageButton3->Enabled=true;
	}
	else
	{
		ImageButton1->Enabled=false;
		ImageButton7->Enabled=false;
		ImageButton6->Enabled=false;
		ImageButton3->Enabled=false;
	}
	if(geometry) geometry->EditVertexIndex=-1;
	geometry=value;
}
//---------------------------------------------------------------------------

void __fastcall TGeometryInfoForm::CloseIButtonClick(TObject *Sender)
{
	Close();
}

//---------------------------------------------------------------------------
void __fastcall TGeometryInfoForm::ImageButton3Click(TObject *Sender)
{
	int z =((TComponent*)Sender)->Owner->ComponentCount;

	for(int i = 0; i < z; i++)
	{
		if(((TComponent *)Sender)->Owner->Components[i]->ClassName()=="TImageButton" && ((TComponent*)Sender)->Owner->Components[i]!=Sender)
			((TImageButton*)((TComponent *)Sender)->Owner->Components[i])->Down=false;
	}
	((TImageButton*)Sender)->Down=true;
	Notebook1->PageIndex=((TImageButton*)Sender)->Tag;
}
//---------------------------------------------------------------------------

void __fastcall TGeometryInfoForm::FormShow(TObject *Sender)
{
	int z=ComponentCount;

	for(int i = 0; i < z; i++)
	{
		if(Components[i]->ClassName()=="TImageButton")
		{
			if(((TImageButton*)Components[i])->Tag==Notebook1->PageIndex)
				((TImageButton*)Components[i])->Down=true;
			else ((TImageButton*)Components[i])->Down=false;
		}
	}
	Timer1Timer(Sender);
	ImageButton3Click(ImageButton3);
}
//---------------------------------------------------------------------------

void __fastcall TGeometryInfoForm::ImageButton7Click(TObject *Sender)
{
	int z =((TComponent*)Sender)->Owner->ComponentCount;

	for(int i = 0; i < z; i++)
	{
		if(((TComponent *)Sender)->Owner->Components[i]->ClassName()=="TImageButton" && ((TComponent*)Sender)->Owner->Components[i]!=Sender)
			((TImageButton*)((TComponent *)Sender)->Owner->Components[i])->Down=false;
	}
	((TImageButton*)Sender)->Down=true;

	int i=-1;
	try
	{
		if(geometry)
		{
			i=Application->MessageBox(L"Do you really want to delete this Geometry?",
				((UnicodeString)_RadarMapName).w_str(), MB_YESNO);
			if(i==IDYES)
			{
				delete geometry;
				geometry=NULL;
			}
		}
	}
	catch(Exception &e) {}
	if(i==IDYES) Close();
	else ImageButton3Click(ImageButton3);
}
//---------------------------------------------------------------------------

void __fastcall TGeometryInfoForm::Notebook1PageChanged(TObject *Sender)
{
	//TempEdit->SetFocus();

}
//---------------------------------------------------------------------------

void __fastcall TGeometryInfoForm::IDEditKeyPress(TObject *Sender, wchar_t &Key)
{
	bool res=false;

	switch(Key)
	{
		case '0':
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
		case 8: res=true; break;
		default: ;
	}
	if(!res)
	{
		((TEdit*)Sender)->Color=(TColor)0xD0D0FE;
		((TEdit*)Sender)->SetFocus();
		Timer1->Enabled=true;
		Key=0;
	}
}
//---------------------------------------------------------------------------

void __fastcall TGeometryInfoForm::Timer1Timer(TObject *Sender)
{
	IDEdit->Color=(TColor)0x00E9E2E1;
	NameEdit->Color=(TColor)0x00E9E2E1;
	TextMemo->Color=(TColor)0x00E9E2E1;
	Timer1->Enabled=false;
}
//---------------------------------------------------------------------------

void __fastcall TGeometryInfoForm::XEditKeyPress(TObject *Sender, wchar_t &Key)
{
	bool res=false;

	switch(Key)
	{
		case '0':
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
		case 8: res=true; break;
		case '.':
		case ',':
			if(((TEdit*)Sender)->Text.Pos(DecimalSeparator)>0) Key=0;
			else
			{
				Key=DecimalSeparator;
				res=true;
			}
			break;
		case '-':
			if(((TEdit*)Sender)->Text.Pos('-')>0) Key=0;
			break;
		default: ;
	}
	if(!res)
	{
		//((TEdit*)Sender)->Text="0.00";
		((TEdit*)Sender)->Color=(TColor)0xD0D0FE;
		Timer1->Enabled=true;
		Key=0;
	}
}
//---------------------------------------------------------------------------

void __fastcall TGeometryInfoForm::OkImageButtonClick(TObject *Sender)
{
	if(geometry)
	{
		try
		{
			geometry->Description=TextMemo->Lines->Text;
			geometry->ID=StrToInt(IDEdit->Text);
			geometry->Name=NameEdit->Text;
		}
		catch(Exception &e) {}
	}
	Close();
}
//---------------------------------------------------------------------------

void __fastcall TGeometryInfoForm::FormClose(TObject *Sender, TCloseAction &Action)
{
	Geometry=NULL;
}
//---------------------------------------------------------------------------

void __fastcall TGeometryInfoForm::FormHide(TObject *Sender)
{
	TCloseAction b;

	FormClose(Sender, b);
}
//---------------------------------------------------------------------------

void __fastcall TGeometryInfoForm::IDEditExit(TObject *Sender)
{
	try
	{
		StrToInt(IDEdit->Text);
	}
	catch(Exception &e)
	{
		if(geometry) IDEdit->Text=IntToStr((int)geometry->ID);
		else IDEdit->Text=="0";
		IDEdit->Color=(TColor)0xD0D0FE;
		IDEdit->SetFocus();
		Timer1->Enabled=true;
	}
}
//---------------------------------------------------------------------------

void __fastcall TGeometryInfoForm::TitlePanelMouseDown(TObject *Sender, TMouseButton Button,
	TShiftState Shift, int X, int Y)
{
	TitleDown=true;
	TitleXY=ClientToScreen(Types::TPoint(X, Y));
}
//---------------------------------------------------------------------------

void __fastcall TGeometryInfoForm::TitlePanelMouseMove(TObject *Sender, TShiftState Shift,
		  int X, int Y)
{
	Types::TPoint tmp;

	if(TitleDown)
	{
		tmp=ClientToScreen(Types::TPoint(X, Y));
		Left+=tmp.x-TitleXY.x;
		Top+=tmp.y-TitleXY.y;
		TitleXY=tmp;
	}
}
//---------------------------------------------------------------------------

void __fastcall TGeometryInfoForm::TitlePanelMouseUp(TObject *Sender, TMouseButton Button,
	TShiftState Shift, int X, int Y)
{
	TitleDown=false;
}
//---------------------------------------------------------------------------

void __fastcall TGeometryInfoForm::ImageButton1Click(TObject *Sender)
{
	int z =((TComponent*)Sender)->Owner->ComponentCount;
	TMapLabelGeometryEdge edge;

	for(int i = 0; i < z; i++)
	{
		if(((TComponent *)Sender)->Owner->Components[i]->ClassName()=="TImageButton" && ((TComponent*)Sender)->Owner->Components[i]!=Sender)
			((TImageButton*)((TComponent *)Sender)->Owner->Components[i])->Down=false;
	}
	((TImageButton*)Sender)->Down=true;

	int i=-1;
	try
	{
		if(geometry)
		{
			edge=geometry->FindNearestEdgeAtXY(PointPx);
			if(edge.Index>=0 && edge.C1)
			{
				geometry->EditVertexIndex=edge.Index;
				i=Application->MessageBox(L"Do you really want to delete this Geometry Vertex?",
					((UnicodeString)_RadarMapName).w_str(), MB_YESNO);
				if(i==IDYES)
				{
					geometry->DeletePoint(geometry->Labels[edge.Index]);
				}
				//geometry->EditVertexIndex=-1;
			}
		}
	}
	catch(Exception &e) {}
	if(i==IDYES) Close();
	else ImageButton3Click(ImageButton3);
}
//---------------------------------------------------------------------------

void __fastcall TGeometryInfoForm::TextMemoKeyPress(TObject *Sender, wchar_t &Key)
{
	bool res=false;

	if((Key>='a' && Key<='z') || (Key>='A' && Key<='Z') || (Key>='0' && Key<='9')) res=true;
	else switch(Key)
	{
		case '.':
		case ',':
		case '?':
		case '|':
		case ':':
		case ';':
		case '{':
		case '}':
		case '[':
		case ']':
		case '+':
		case '=':
		case '_':
		case '-':
		case ')':
		case '(':
		case '*':
		case '&':
		case '^':
		case '%':
		case '$':
		case '@':
		case '!':
		case '~':
		case ' ':
		case 8:
		case 13:
		case 127:
		res=true; break;
		default: ;
	}
	if(!res)
	{
		((TMemo*)Sender)->Color=(TColor)0xD0D0FE;
		((TMemo*)Sender)->SetFocus();
		Timer1->Enabled=true;
		Key=0;
	}
}
//---------------------------------------------------------------------------

void __fastcall TGeometryInfoForm::NameEditKeyPress(TObject *Sender, wchar_t &Key)
{
    bool res=false;

	if((Key>='a' && Key<='z') || (Key>='A' && Key<='Z') || (Key>='0' && Key<='9')) res=true;
	else switch(Key)
	{
		case '.':
		case ',':
		case '?':
		case '|':
		case ':':
		case ';':
		case '{':
		case '}':
		case '[':
		case ']':
		case '+':
		case '=':
		case '_':
		case '-':
		case ')':
		case '(':
		case '*':
		case '&':
		case '^':
		case '%':
		case '$':
		case '@':
		case '!':
		case '~':
		case ' ':
		case 8:
		case 127:
		res=true; break;
		default: ;
	}
	if(!res)
	{
		((TEdit*)Sender)->Color=(TColor)0xD0D0FE;
		((TEdit*)Sender)->SetFocus();
		Timer1->Enabled=true;
		Key=0;
	}
}
//---------------------------------------------------------------------------

