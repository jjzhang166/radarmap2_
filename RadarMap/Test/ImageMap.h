//---------------------------------------------------------------------------

#ifndef ImageMapH
#define ImageMapH
//---------------------------------------------------------------------------
#include "BitmapMap.h"

#define ScalingPointRadius		6
#define ReferencesPointsMaxQ 	2

class TFrameVertex;

typedef void __fastcall (__closure *TFrameVertexMouseMoveEvent)(TFrameVertex* Sender, TShiftState Shift, int X, int Y);
typedef void __fastcall (__closure *TFrameVertexMouseUpEvent)(TFrameVertex* Sender, TMouseButton Button, TShiftState Shift, int X, int Y);

enum TFrameVertexType {fvtRect, fvtCircle};
enum TFrameVertexPosition {fvpTop=0, fvpRight, fvpBottom, fvpLeft, fvpRightTop,
	fvpRightBottom, fvpLeftBottom, fvpLeftTop, fvpCenter};
enum TBitmapFrameState {bfsDormancy, bfsScaling, bfsCroping, bfsReferencing};

class TFrameVertex: public TToolObject
{
private:
	Types::TPoint center;
	TFrameVertexPosition position;
	TFrameVertexType cornertype;
	Types::TRect *recttocontrol;
	TColor32 color;
	int dx, dy;
	TFrameVertexMouseMoveEvent onmousemove;
	TFrameVertexMouseUpEvent onmouseup;
	void __fastcall DefineRect();
protected:
	void __fastcall writeCenter(Types::TPoint value) {if(value!=center) {center=value; DefineRect();}}
	void __fastcall writeRectToControl(Types::TRect *value);
public:
	__fastcall TFrameVertex(TMyObject *AOwner, TImgView32 *ImgView, TFrameVertexPosition APos,
		TFrameVertexType AType, Types::TRect *ARectToControl, TObject *AManager);

	void __fastcall Redraw(TObject *Sender, TBitmap32 *Bmp);
	void __fastcall MouseMove(TObject* Sender, Classes::TShiftState Shift,
		int X, int Y, TCustomLayer *Layer) {if(onmousemove) (onmousemove)(this, Shift, X, Y); else if(recttocontrol) {}}
	void __fastcall MouseDown(TObject *Sender,
		TMouseButton Button, TShiftState Shift, int X, int Y,
		TCustomLayer *Layer);
	void __fastcall MouseUp(TObject *Sender,
		TMouseButton Button, TShiftState Shift, int X, int Y,
		TCustomLayer *Layer) {if(onmouseup) (onmouseup)(this, Button, Shift, X, Y);}
	void __fastcall Update(TMyObjectType Type) {DefineRect(); TGraphicObject::Update(Type);}
	void __fastcall Update() {TToolObject::Update();}

	__property TColor32 Color = {read=color, write=color};
	__property Types::TPoint Center = {read=center, write=writeCenter};
	__property TFrameVertexPosition Position = {read=position};
	__property TFrameVertexType CornerType = {read=cornertype};
	__property Types::TRect *RectToControl = {read=recttocontrol, write=writeRectToControl};
	__property TFrameVertexMouseMoveEvent OnMouseMove = {read=onmousemove, write=onmousemove};
	__property TFrameVertexMouseUpEvent OnMouseUp = {read=onmouseup, write=onmouseup};
	__property int dX = {read=dx};
	__property int dY = {read=dy};
};

class TImageMapTransformThread : public TMyThread
{
private:
	TObject *Manager;
	TBitmap32 *SrcBmp, *Thumb;
	TMyObject *Owner;
	Types::TRect *Rect, *ThumbRect;
	HANDLE refreshevent;
	void __fastcall ExecuteLoopBody();
	Gdiplus::GdiplusStartupInput gdiplusStartupInput;
	ULONG_PTR gdiplusToken;
	Gdiplus::Graphics *graphics;
	Gdiplus::Bitmap *originalimage;
	Gdiplus::Image *thumbnail;
	//void __fastcall Visualize() {if(Owner) Owner->Update(gotNone);}
public:
	__fastcall TImageMapTransformThread(TMyObject *AOwner, TBitmap32* ASrcBmp, TBitmap32* AThumb, Types::TRect *ARect, Types::TRect *AThumbRect, TObject* AManager);
	__fastcall ~TImageMapTransformThread();

	void __fastcall GenerateThumbnail();
	void __fastcall DefineGraphics();

	__property HANDLE RefreshEvent={read=refreshevent};
};

class TBitmapFrameTool: public TManagerObject
{
private:
	TList *Vertexes;
	Types::TRect boundaryrect;
	::TFloatRect croprect;
	TBitmapFrameState state;
	TBitmap32 *SrcBmp, *Thumb;
	TMyObject *Container;
	TImageMapTransformThread *TransformThrd;
	int dx, dy;
	float dw, dh; //scalar for conversation from bitmap to screen pixels
	TLockedDoubleRect *viewportarea;
	Types::TRect *RawPxRect;
	void __fastcall DefineRect();
	void __fastcall UpdateVertexes();
protected:
	void __fastcall writeState(TBitmapFrameState value);
	void __fastcall writeScaleRect(Types::TRect value) {if(Rect!=value) {Rect=value; TManagerObject::Update();}}
	void __fastcall writeCropRect(::TFloatRect value) {if(croprect!=value) {croprect=value; TManagerObject::Update();}}
	void __fastcall MoveFrame(TFrameVertex* Sender, Classes::TShiftState Shift, int X, int Y);
	void __fastcall ResizeFrame(TFrameVertex* Sender, Classes::TShiftState Shift, int X, int Y);
	void __fastcall RotateFrame(TFrameVertex* Sender, Classes::TShiftState Shift, int X, int Y);
	void __fastcall VertexMouseUp(TFrameVertex* Sender, TMouseButton Button, TShiftState Shift, int X, int Y);
	void __fastcall CropFrame(TFrameVertex* Sender, Classes::TShiftState Shift, int X, int Y);
	Types::TRect __fastcall readCropRectPx() {float w=Rect.Width(), h=Rect.Height(); return Types::TRect((int)(croprect.Left*w), (int)(croprect.Top*h), (int)(croprect.Right*w), (int)(croprect.Bottom*h));}
	Types::TRect __fastcall readOutputRect() {Types::TRect r=CropRectPx; return Types::TRect(Rect.Left+r.Left, Rect.Top+r.Top, Rect.left+r.Right, Rect.Top+r.Bottom);}
public:
	__fastcall TBitmapFrameTool(TGraphicObjectsContainer *AOwner, TMyObject *AContainer,
		TBitmap32* ABitmap, TLockedDoubleRect *AViewportArea, Types::TRect *ARawPxRect, TObject *AManager);
	__fastcall ~TBitmapFrameTool();

	void __fastcall Redraw(TObject *Sender, TBitmap32 *Bmp);

	void __fastcall Update(TMyObjectType Type) {DefineRect(); TGraphicObject::Update(Type);}
	void __fastcall Update() {TManagerObject::Update();}
	void __fastcall ZoomUpdate(TMyObjectType Type) {TManagerObject::ZoomUpdate(Type);}
	void __fastcall ZoomUpdate();

	void __fastcall MouseMove(TObject* Sender, Classes::TShiftState Shift,
		int X, int Y, TCustomLayer *Layer);
	void __fastcall MouseDown(TObject *Sender,
		TMouseButton Button, TShiftState Shift, int X, int Y,
		TCustomLayer *Layer) {dx=X1-Rect.Left; dy=Y1-Rect.Top;}
	void __fastcall MouseUp(TObject *Sender,
		  TMouseButton Button, TShiftState Shift, int X, int Y,
		  TCustomLayer *Layer);

	__property TBitmapFrameState State = {read=state, write=writeState};
	__property Types::TRect ScaleRect = {read=Rect, write=writeScaleRect};
	__property ::TFloatRect CropRect = {read=croprect, write=writeCropRect}; //0..1, where 1=ScaleRect->Width()
	__property Types::TRect CropRectPx = {read=readCropRectPx}; //internal crop rect in pixels
	__property Types::TRect OutputRect = {read=readOutputRect}; //Output Rect to bitmap
	__property Types::TRect BoundaryRect = {read=boundaryrect};
};

struct TReferencePoint
{
private:
	TGPSCoordinate *c;
	TObject *labelobject;
protected:
	void __fastcall writeC(TGPSCoordinate *Cc) {c=Cc;}
	bool __fastcall readValid() {return c!=NULL;}
public:
	Types::TPoint XY;

	TReferencePoint() {XY=Types::TPoint(); c=NULL; labelobject=NULL;}
	TReferencePoint(Types::TPoint xy, TGPSCoordinate* Cc, TObject* ALabel=NULL) {XY=xy; c=Cc; labelobject=ALabel;}
	//TReferencePoint(TReferencePoint rp) {XY=rp.XY; C=rp.C; if(C) valid=true; else valid=false;}

	__property TGPSCoordinate *C = {read=c, write=writeC};
	__property bool Valid = {read=readValid};
	__property TObject* LabelObject = {read=labelobject};
};

class TImageMapsContainer: public TBitmapMapsContainer
{
private:
//	TBitmap32 *bmpGB, *bmpUtil, *bmpInf;
	TGraphicObjectsContainer* ToolsOwner;
	TBitmapFrameTool* Frame;
	bool showbackground;
	AnsiString previewfilename;
	TList* ReferencePoints;
	TObject* WrongReferenceMsg;
protected:
	void __fastcall writeState(TBitmapFrameState value) {Frame->State=value;}
	TBitmapFrameState __fastcall readState() {return Frame->State;}
	Types::TRect __fastcall readScaleRect() {return Frame->ScaleRect;}
	::TFloatRect __fastcall readCropRect() {return Frame->CropRect;}
	TReferencePoint* __fastcall FindReferencePointAtXY(Types::TPoint XY);
	int __fastcall readReferencesCount() {return ReferencePoints->Count;}
public:
	__fastcall TImageMapsContainer(TMyObject *AOwner, void* ASettings, TLockedDoubleRect *AViewportArea, TGraphicObjectsContainer* AToolsOwner=NULL);
	__fastcall ~TImageMapsContainer();

	void __fastcall SaveData();

	void __fastcall LoadData(AnsiString Filename, HANDLE AStopItEvent, bool WithUpdate, TCoordinateSystem CS, bool Preview=false);
	void __fastcall LoadData(AnsiString Filename, HANDLE AStopItEvent, bool WithUpdate, bool Preview=false) {TMapsContainer::LoadData(Filename, AStopItEvent, WithUpdate, Preview);}
	void __fastcall LoadData(AnsiString Filename, HANDLE AStopItEvent) {TMapsContainer::LoadData(Filename, AStopItEvent);}//LoadData(Filename, AStopItEvent, true);}

	void __fastcall ZoomUpdate();
	void __fastcall ZoomUpdate(TMyObjectType Type) {Update(Type);};
	void __fastcall RenderMap();
	void __fastcall UtilityUpdate() {}
	void __fastcall InfoUpdate() {}

	void __fastcall NullBitmapFrame(TObject* AFrm) {if(Frame && Frame==AFrm) Frame=NULL;}

	bool __fastcall AddReferencePoint(TReferencePoint RP);
	bool __fastcall AddReferencePoint(Types::TPoint XY) {return AddReferencePoint(TReferencePoint(XY, (TGPSCoordinate*)NULL, NULL));}
	bool __fastcall AddReferencePoint(Types::TPoint XY, TGPSCoordinate* C, TObject *ALabel) {return AddReferencePoint(TReferencePoint(XY, C, ALabel));}
	void __fastcall DeleteReferencePointAtXY(Types::TPoint XY);
	void __fastcall ChangeReferencePointAtXY(Types::TPoint XY, TGPSCoordinate *c) {TReferencePoint* RP=FindReferencePointAtXY(XY); if(RP) RP->C=c;}
	Types::TPoint __fastcall FindNearestAtXY(Types::TPoint XY);
	void __fastcall RescaleReferences();

//	Types::TRect __fastcall GetBitmapRect() {return TBitmapMapsContainer::GetBitmapRect();}

	void __fastcall GetLayers(TCheckBoxTreeView *TreeView, TTreeNode *Root);
	void __fastcall SetLayers(TCheckBoxTreeView *TreeView, TTreeNode *Root);

	__property TBitmapFrameState State = {read=readState, write=writeState};
	__property Types::TRect ScaleRect = {read=readScaleRect};
	__property ::TFloatRect CropRect = {read=readCropRect};
	__property AnsiString PreviewFileName = {read=previewfilename, write=previewfilename};
	__property int ReferencesCount = {read=readReferencesCount};
};

#endif
