//---------------------------------------------------------------------------

#ifndef GeometryFormH
#define GeometryFormH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <ImageButton.h>
#include <pngimage.hpp>
#include <GR32_Image.hpp>
#include <Graphics.hpp>
#include "Manager.h"
#include <Dialogs.hpp>
#include <Menus.hpp>
#include "MapLabelGeometry.h"
//---------------------------------------------------------------------------
class TGeometryInfoForm : public TForm
{
__published:	// IDE-managed Components
	TPanel *Panel1;
	TPanel *Panel2;
	TPanel *Panel3;
	TPanel *Panel4;
	TPanel *TitlePanel;
	TPanel *Panel5;
	TPanel *Panel6;
	TPanel *Panel7;
	TPanel *TabsPanel;
	TPanel *PagesPanel;
	TPanel *OkCancelPanel;
	TLabel *Label1;
	TPanel *Panel8;
	TPanel *Panel9;
	TImageButton *CloseIButton;
	TImageButton *CancelImageButton;
	TImageButton *OkImageButton;
	TNotebook *Notebook1;
	TImageButton *ImageButton3;
	TImageButton *ImageButton6;
	TImageButton *ImageButton7;
	TPanel *Panel10;
	TPanel *Panel12;
	TLabel *Label10;
	TLabel *Label11;
	TEdit *IDEdit;
	TLabel *Label12;
	TPanel *Panel19;
	TPanel *Panel20;
	TLabel *Label18;
	TMemo *TextMemo;
	TEdit *TempEdit;
	TTimer *Timer1;
	TPanel *Panel11;
	TLabel *Label3;
	TEdit *NameEdit;
	TImageButton *ImageButton1;
	void __fastcall CloseIButtonClick(TObject *Sender);
	void __fastcall ImageButton3Click(TObject *Sender);
	void __fastcall FormShow(TObject *Sender);
	void __fastcall ImageButton7Click(TObject *Sender);
	void __fastcall Notebook1PageChanged(TObject *Sender);
	void __fastcall IDEditKeyPress(TObject *Sender, wchar_t &Key);
	void __fastcall Timer1Timer(TObject *Sender);
	void __fastcall XEditKeyPress(TObject *Sender, wchar_t &Key);
	void __fastcall OkImageButtonClick(TObject *Sender);
	void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
	void __fastcall FormHide(TObject *Sender);
	void __fastcall IDEditExit(TObject *Sender);
	void __fastcall TitlePanelMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
		  int X, int Y);
	void __fastcall TitlePanelMouseMove(TObject *Sender, TShiftState Shift, int X, int Y);
	void __fastcall TitlePanelMouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift,
		  int X, int Y);
	void __fastcall ImageButton1Click(TObject *Sender);
	void __fastcall TextMemoKeyPress(TObject *Sender, wchar_t &Key);
	void __fastcall NameEditKeyPress(TObject *Sender, wchar_t &Key);
private:	// User declarations
	TMapLabelGeometry *geometry;
	TRadarMapManager *manager;
	bool TitleDown;
	Types::TPoint TitleXY, pointpx;
	void __fastcall writeGeometry(TMapLabelGeometry* value);
public:		// User declarations
	__fastcall TGeometryInfoForm(TComponent* Owner);
	__fastcall ~TGeometryInfoForm() {}
	void __fastcall RefreshGeometry() {writeGeometry(geometry);}

	__property TMapLabelGeometry* Geometry={read=geometry, write=writeGeometry};
	__property TRadarMapManager *Manager = {read=manager, write=manager};
	__property Types::TPoint PointPx = {read=pointpx, write=pointpx};
};
//---------------------------------------------------------------------------
extern PACKAGE TGeometryInfoForm *GeometryInfoForm;
//---------------------------------------------------------------------------
#endif
