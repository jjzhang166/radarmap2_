//---------------------------------------------------------------------------

#ifndef GpsListenerH
#define GpsListenerH
//---------------------------------------------------------------------------

#include "GPSCoordinate.h"
#include "NmeaRecSettings.h"
//---------------------------------------------------------------------------

typedef void __fastcall (__closure *TGpsListenerAddCoordinate)(TGPSCoordinate *c);

class TGpsListener : public TObject
{
private:
	TGpsListenerAddCoordinate onaddcoordinate;
protected:
public:
	__fastcall TGpsListener() {onaddcoordinate=NULL;}

	virtual void __fastcall AddCoordinate(TGPSCoordinate *c) {if(onaddcoordinate) (onaddcoordinate)(c);}
	__property TGpsListenerAddCoordinate OnAddCoordinate = {write=onaddcoordinate};
};

class TGpsListenersList : public TObject
{
private:
	TList *List;
protected:
	int __fastcall readCount() {return List->Count;}
	TGpsListener* __fastcall readItem(int Index) {if(Index>=0 && Index<List->Count) return (TGpsListener*)List->Items[Index]; else return NULL;}
public:
	__fastcall TGpsListenersList() {List=new TList();}
	void __fastcall Clear() {for(int i=0; i<List->Count; i++) delete (TGpsListener*)List->Items[i]; List->Clear();}
	__fastcall ~TGpsListenersList() {Clear(); delete List;}

	void __fastcall Add(TGpsListener *Item) {if(Item && List->IndexOf((void*)Item)<0) List->Add((void*)Item);}
	void __fastcall Delete(TGpsListener *Item) {if(Item && List->IndexOf((void*)Item)>=0) List->Delete(List->IndexOf((void*)Item));}

	void __fastcall AddCoordinateToAll(TGPSCoordinate* c) {for(int i=0; i<List->Count; i++) ((TGpsListener*)List->Items[i])->AddCoordinate(c);}

	__property int Count = {read=readCount};
	__property TGpsListener* Items[int Index] = {read=readItem};
};

struct TGpsListenerCommandParam
{
	TGpsListenerCommandParam() {Name=""; Description=""; Value="";}
	AnsiString Name;
	AnsiString Description;
	AnsiString Value;
};

enum TReplyResult {rrNAK = 0, rrACK = 1};

struct TGpsListenerCommandReply
{
	TGpsListenerCommandReply(AnsiString str, TReplyResult rr, bool fx=true) {Reply=str; Result=rr; Fixed=fx;}
	AnsiString Reply;
	bool Fixed; // If Fixed==true => Reply coinsides with preseted "AnsiString Reply", otherwise reply has additional characters until the "\r\n"
	TReplyResult Result;
};

typedef void __fastcall (__closure *TGpsListenerReplyParser)(AnsiString Reply);

class TGpsListenerCommand : public TObject
{
private:
	TList *List, *Params;
	AnsiString command;
	TGpsListenerReplyParser parserproc;
	bool repliessequence; // If all replies needed thane TRUE, otherwise, in case of one of replies - FALSE
protected:
	void __fastcall Clear() {for(int i=0; i<List->Count; i++) delete (TGpsListenerCommandReply*)List->Items[i]; List->Clear();}
	void __fastcall ClearParams() {for(int i=0; i<Params->Count; i++) delete (TGpsListenerCommandParam*)Params->Items[i]; Params->Clear();}
	int __fastcall readCount() {return List->Count;}
	TGpsListenerCommandReply* __fastcall readReply(int Index) {if(Index>=0 && Index<List->Count) return ((TGpsListenerCommandReply*)List->Items[Index]); else return NULL;}
	AnsiString __fastcall readCommand();
public:
	__fastcall TGpsListenerCommand(AnsiString cmd, TList* aParams=NULL) {command=cmd; List=new TList(); Params=new TList(); if(aParams) for(int i=0; i<aParams->Count; i++) Params->Add(aParams->Items[i]); parserproc=NULL; repliessequence=true;}
	__fastcall ~TGpsListenerCommand() {Clear(); delete List; ClearParams(); delete Params;}

	void __fastcall AddReply(AnsiString str, TReplyResult result=rrACK, bool aFixed=true) {if(str!=NULL && str!="") AddReply(str.c_str(), result, aFixed);}
	void __fastcall AddReply(char* buf, TReplyResult result=rrACK, bool aFixed=true) {if(buf) AddReply(new TGpsListenerCommandReply(AnsiString(buf), result, aFixed));}
	void __fastcall AddReply(TGpsListenerCommandReply *res) {if(res) List->Add(res);}
	void __fastcall AddParam(TGpsListenerCommandParam *param) {Params->Add(param);}

	__property AnsiString SrcQuery = {read=command};
	__property AnsiString Query = {read=readCommand};
	__property TGpsListenerCommandReply* Replies[int Index] = {read=readReply};
	__property int ReplyCount = {read=readCount};
	__property TGpsListenerReplyParser ParserProc = {read=parserproc, write=parserproc};
	__property bool RepliesSequence = {read=repliessequence, write=repliessequence};
};

#endif
