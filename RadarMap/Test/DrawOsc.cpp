//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "DrawOsc.h"
#include "Radar.h"
#pragma package(smart_init)
//---------------------------------------------------------------------------

__fastcall TDrawOscThrd::TDrawOscThrd(bool CreateSuspended)
        : TThread(CreateSuspended)
{
}
//---------------------------------------------------------------------------
void __fastcall TDrawOscThrd::Execute()
{
  do
  {
    if(WaitForSingleObject(RadarDialog->EventDrawOscHndl, 10)==WAIT_OBJECT_0)
    {
      ResetEvent(RadarDialog->EventDrawOscHndl);
      //Synchronize(SetS1);
	  Synchronize(RadarDialog->TraceImageRefresh);
	  Sleep(10);
    }
  } while(!Terminated);//RadarDialog->Process==Running);
}
//---------------------------------------------------------------------------

void __fastcall TDrawOscThrd::SetS1()
{
  //RadarDialog->TracesST->Caption=IntToStr(RadarDialog->TracesQ);
  //RadarDialog->DistanceST->Caption=FloatToStrF(RadarDialog->Profile->HorRangeFull,
  //  ffFixed, 10, 2)+" m";
  // RadarDialog->BatteryST->Caption=FloatToStrF(RadarDialog->BatteryVoltage,
  //   ffFixed, 10, 2)+" V";

  //RadarDialog->DistanceST->Caption=IntToStr(RadarDialog->Valg);
  //RadarDialog->Memo1->Lines->Add(RadarDialog->WheelStr);

  /*if(RadarDialog->tpsCnt>=1.0) RadarDialog->DistanceST->Caption=FloatToStrF(RadarDialog->tps,
    ffFixed, 10, 1)+" "+FloatToStrF(RadarDialog->tpsTotal/RadarDialog->tpsCnt,
    ffFixed, 10, 1);
  else RadarDialog->DistanceST->Caption=FloatToStrF(RadarDialog->tps,
    ffFixed, 10, 1);*/
}
