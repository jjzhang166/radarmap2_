//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
#include <tchar.h>

#include "Defines.h"
#include "Splash.h"
//---------------------------------------------------------------------------
TFormatSettings _FormatSettings;
char _WChar2CharBuf[_WChar2CharBufLength];
//---------------------------------------------------------------------------
USEFORM("..\PalettesRepository.cpp", PalettesForm);
USEFORM("..\Preview.cpp", PreviewForm);
USEFORM("..\LoadPlugins.cpp", LoadPluginsForm);
USEFORM("..\OptionsGpsUnit.cpp", OptionsGpsUnitForm);
USEFORM("..\OptionsPlugins.cpp", OptionsPluginsForm);
USEFORM("Paths.cpp", PathsForm);
USEFORM("Radar.cpp", RadarDialog);
USEFORM("OnlineMap.cpp", OnlineMapForm);
USEFORM("MainForm.cpp", RadarMapForm);
USEFORM("Map.cpp", MapForm);
USEFORM("..\TwoWheelsUnit.cpp", TwoWheelsForm);
USEFORM("VectorDBForm.cpp", VectorDBForm);
USEFORM("..\Splash.cpp", SplashForm);
USEFORM("..\Settings.cpp", SettingsForm);
USEFORM("..\Setup.cpp", SetupForm);
USEFORM("InfoForm.cpp", InfoForm);
USEFORM("Layers.cpp", LayersForm);
USEFORM("LabelForm.cpp", LabelInfoForm);
USEFORM("GainForm.cpp", GainForm);
USEFORM("Files.cpp", FilesForm);
USEFORM("GeometryForm.cpp", GeometryInfoForm);
USEFORM("..\Disclaimer.cpp", DisclaimerForm);
//---------------------------------------------------------------------------
WINAPI _tWinMain(HINSTANCE, HINSTANCE, LPTSTR, int)
{
	try
	{
        _FormatSettings.DecimalSeparator=DecimalSeparator;
		LogEvent("RadarMap starting on OS "+GetHostOSName(true), "WinMain()");

		SplashForm=new TSplashForm(Application);
		SplashForm->PBar->Min=0;
		SplashForm->PBar->Max=13;
		//SplashForm->PBar->Step=1;
		SplashForm->PBar->Position=0;
#ifdef _WithSplash
		SplashForm->Show();
		SplashForm->Update();
#endif
		SplashForm->PBar->Position++;
		Application->Initialize();
		SplashForm->PBar->Position++;
		Application->MainFormOnTaskBar = true;
		Application->Title = (AnsiString)_RadarMapName;
		Application->Title = "RadarMap";
		Application->CreateForm(__classid(TRadarMapForm), &RadarMapForm);
		SplashForm->PBar->Position++;
		Application->CreateForm(__classid(TSettingsForm), &SettingsForm);
		SplashForm->PBar->Position++;
		Application->CreateForm(__classid(TLayersForm), &LayersForm);
		SplashForm->PBar->Position++;
		Application->CreateForm(__classid(TInfoForm), &InfoForm);
		SplashForm->PBar->Position++;
		Application->CreateForm(__classid(TDisclaimerForm), &DisclaimerForm);
		SplashForm->PBar->Position++;
		Application->CreateForm(__classid(TOptionsPluginsForm), &OptionsPluginsForm);
		SplashForm->PBar->Position++;
		Application->CreateForm(__classid(TLoadPluginsForm), &LoadPluginsForm);
		SplashForm->PBar->Position++;
		Application->CreateForm(__classid(TPalettesForm), &PalettesForm);
		SplashForm->PBar->Position++;
		Application->CreateForm(__classid(TLabelInfoForm), &LabelInfoForm);
		SplashForm->PBar->Position++;
		Application->CreateForm(__classid(TSettingsForm), &SettingsForm);
		SplashForm->PBar->Position++;
		Application->CreateForm(__classid(TOptionsGpsUnitForm), &OptionsGpsUnitForm);
		SplashForm->PBar->Position++;
		Application->CreateForm(__classid(TGeometryInfoForm), &GeometryInfoForm);
		SplashForm->PBar->Position++;
		Application->Run();

		LogEvent("RadarMap Closed", "WinMain()");
	}
	catch (Exception &exception)
	{
		Application->ShowException(&exception);
	}
	catch (...)
	{
		try
		{
			throw Exception("");
		}
		catch (Exception &exception)
		{
			Application->ShowException(&exception);
		}
	}
	return 0;
}
//---------------------------------------------------------------------------
