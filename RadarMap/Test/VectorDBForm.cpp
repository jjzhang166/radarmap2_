//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "VectorDBForm.h"
#include "DbDXFMapsContainer.h"
#include "MyFiles.h"
#include <sys\stat.h>
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "ImageButton"
#pragma link "G32_ProgressBar"
#pragma link "GR32_Image"
#pragma resource "*.dfm"
TVectorDBForm *VectorDBForm;

//---------------------------------------------------------------------------
__fastcall TVectorDBForm::TVectorDBForm(TComponent* Owner, TRadarMapManager *aManager,
	TCoordinateSystem aCS) : TForm(Owner)
{
	ResMov=new TResourceMovie();
	ResMov->Image=Image321;
	ResMov->Endless=true;
	ResMov->FramePictureType=ptPNG;
	ResMov->FrameInterval=100;
	FileName="";
	manager=aManager;
	mapobjects=NULL;
	dbmc=NULL;
	coordinatesystem=csTotalFixed;
	//if(aCS==csLatLon) aCS=csLocalMetric();
	CoordinateSystem=aCS;

	CreateMapObjects();

	MA=tmaNone;
	DblClick=false;

	TitleDown=false;
	CRS_GUIDs = new TStringList();
	OkImageButton->Enabled=false;
}
//---------------------------------------------------------------------------

__fastcall TVectorDBForm::~TVectorDBForm()
{
	if(mapobjects) delete mapobjects;
	mapobjects=NULL;
	if(ResMov) delete ResMov;
	ResMov=NULL;
	if(CRS_GUIDs) delete CRS_GUIDs;
	CRS_GUIDs=NULL;
}
//---------------------------------------------------------------------------

void __fastcall TVectorDBForm::writeCoordinateSystem(TCoordinateSystem value)
{
	AnsiString str;
	double z;
	TGPSCoordinate *c;

	if(coordinatesystem!=value)
	{
		coordinatesystem=value;
		c=new TGPSCoordinate();
		try
		{
			csLabel->Caption=c->GetCSName(value);
		}
		__finally
		{
			delete c;
		}
		Label13->Visible=(value==csLatLon);
		csLabel->Font->Color=((value==csLatLon) ? clRed : Label13->Font->Color);
		if(dbmc && !OpenVectorDB(FileName)) FileName="";
	}
}
//---------------------------------------------------------------------------

void __fastcall TVectorDBForm::CloseIButtonClick(TObject *Sender)
{
	if(MoviePanel->Visible) StopButtonClick(StopButton);
	else
	{
		ModalResult=mrCancel;
    }
}

//---------------------------------------------------------------------------

void __fastcall TVectorDBForm::FormShow(TObject *Sender)
{
	ContentsIButtonClick(ContentsIButton);
}
//---------------------------------------------------------------------------

void __fastcall TVectorDBForm::OkImageButtonClick(TObject *Sender)
{
	ModalResult=mrOk;
}
//---------------------------------------------------------------------------

void __fastcall TVectorDBForm::FormHide(TObject *Sender)
{
	Close();
}
//---------------------------------------------------------------------------

void __fastcall TVectorDBForm::TitlePanelMouseDown(TObject *Sender, TMouseButton Button,
		  TShiftState Shift, int X, int Y)
{
	TitleDown=true;
	TitleXY=ClientToScreen(Types::TPoint(X, Y));
}
//---------------------------------------------------------------------------

void __fastcall TVectorDBForm::TitlePanelMouseMove(TObject *Sender, TShiftState Shift,
		  int X, int Y)
{
	Types::TPoint tmp;

	if(TitleDown)
	{
		tmp=ClientToScreen(Types::TPoint(X, Y));
		Left+=tmp.x-TitleXY.x;
		Top+=tmp.y-TitleXY.y;
		TitleXY=tmp;
	}
}
//---------------------------------------------------------------------------

void __fastcall TVectorDBForm::TitlePanelMouseUp(TObject *Sender, TMouseButton Button,
		  TShiftState Shift, int X, int Y)
{
	TitleDown=false;
}
//---------------------------------------------------------------------------

void __fastcall TVectorDBForm::csLabelClick(TObject *Sender)
{
	Types::TPoint p;
	TGPSCoordinate *c;
	TMenuItem *NewItem;
	TPlugInObject *plugin;

	if(!MoviePanel->Visible)
	{
		CSPopupMenu->Items->Clear();
		try
		{
			c = new TGPSCoordinate();
			try
			{
				for(int i = 0; i < csTotalFixed; i++)
				{
					if((TCoordinateSystem)i!=csLatLon)
					{
						NewItem=new TMenuItem(this);
						NewItem->Caption=c->GetCSName((TCoordinateSystem)i)+" ("+c->GetCSEllipsoid((TCoordinateSystem)i)+")";
						NewItem->Tag=i;
						NewItem->OnClick=CSMenuItemClick;
						CSPopupMenu->Items->Add(NewItem);
					}
				}
				CRS_GUIDs->Clear();
				if(PlugInManager && PlugInManager->PlugInsList->Count>0)
				{
					for(int i=0; i< PlugInManager->PlugInsList->Count; i++)
					{
						if(PlugInManager->PlugInsList->Items[i] && PlugInManager->PlugInsList->Items[i]->Type==pitCRS)
						{
							plugin = PlugInManager->PlugInsList->Items[i];
							NewItem=new TMenuItem(this);
							NewItem->Caption=plugin->Name;
							NewItem->Tag=i+csTotalFixed;
							NewItem->OnClick=CSMenuItemClick;
							CRS_GUIDs->Add(plugin->GUID);
							CSPopupMenu->Items->Add(NewItem);
						}
					}
				}
				p=Types::TPoint(csLabel->Left+PagesPanel->Left, csLabel->Top+PagesPanel->Top+csLabel->Height);
				p=ClientToScreen(p);
				CSPopupMenu->Popup(p.x, p.y);
			}
			catch(...) {}
		}
		__finally
		{
			delete c;
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TVectorDBForm::CSMenuItemClick(TObject *Sender)
{
	int i;

	if(Sender)
	{
		i=((TMenuItem*)Sender)->Tag;
		if(i<csTotalFixed) CoordinateSystem=(TCoordinateSystem)i;
		else
		{
			i-=csTotalFixed;
			if(i<CRS_GUIDs->Count)
			{
				PlugInManager->SetActive(CRS_GUIDs->Strings[i], pitCRS);
				CoordinateSystem=csDLL;
			}
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TVectorDBForm::DisableMouseAction()
{
	switch(MA)
	{
		case tmaReferenceFlag: break;
		case tmaPosCorrection: break;
		case tmaStartStopWaypoint: break;
		case tmaStartNGo: break;
		case tmaZoomIn:
		case tmaNone:
		case tmaHand:
		default:
			break;
	}
	MA=tmaHand;
}
//---------------------------------------------------------------------------

void __fastcall TVectorDBForm::CreateMapObjects()
{
	if(Manager)
	{
		if(mapobjects) delete mapobjects;
		mapobjects=new TMapObjectsContainer(MapImage, true, &MA, DisableMouseAction, Manager->Settings, true);
		mapobjects->SmallSize=Manager->Settings->SmallSizeControls;
		if(mapobjects->Container)
		{
			mapobjects->Container->CoordinateSystem=coordinatesystem;
			mapobjects->Container->Progress=new TToolProgressBar(mapobjects, MapImage, Manager);
		}
		if(mapobjects->MapScale) mapobjects->MapScale->ToolAlign=vtoaLeftBottom;
		if(mapobjects->Messages) mapobjects->Messages->Align=msaBottom;

		MapToolbar=new TSliderToolbar(mapobjects, MapImage->GetViewportRect(), 48, staRight/*staLeft*/, true);
		MapToolbar->FitButtonsOnly=true;
		MapToolbar->Visible=true;
		MapToolbar->Transparent=true;

		LayersBtn=new TSliderToolButton(MapToolbar, Types::TRect(2, 52, 50, 100));
		LayersBtn->NormalGlyphName="PNGIMAGE_87";
		LayersBtn->DisabledGlyphName="PNGIMAGE_106";
		LayersBtn->HotGlyphName="PNGIMAGE_88";
		LayersBtn->Enabled=true;
		LayersBtn->OnMouseClick=LayersClick;

		ZoomInBtn=new TSliderToolButton(MapToolbar, Types::TRect(2, 102, 50, 150));
		ZoomInBtn->NormalGlyphName="PNGIMAGE_89";
		ZoomInBtn->DisabledGlyphName="PNGIMAGE_96";
		ZoomInBtn->HotGlyphName="PNGIMAGE_90";
		ZoomInBtn->Enabled=true;//false;
		ZoomInBtn->OnMouseUp=ZoomInMouseUp;
		//ZoomInBtn->OnMouseDblClick=ZoomInDblClick;

		ZoomOutBtn=new TSliderToolButton(MapToolbar, Types::TRect(2, 102, 50, 150));
		ZoomOutBtn->NormalGlyphName="PNGIMAGE_91";
		ZoomOutBtn->DisabledGlyphName="PNGIMAGE_97";
		ZoomOutBtn->HotGlyphName="PNGIMAGE_92";
		ZoomOutBtn->Enabled=true;//false;
		ZoomOutBtn->OnMouseUp=ZoomOutMouseUp;
		//ZoomOutBtn->OnMouseDblClick=ZoomOutDblClick;
	}
}
//---------------------------------------------------------------------------

void __fastcall TVectorDBForm::LayersClick(System::TObject* Sender, int X, int Y)
{
	try
	{
		if(Sender && ((TGraphicObject*)Sender)->Pressed)
		{
			ZoomInBtn->Down=false;
			DisableMouseAction();
			if(mapobjects && mapobjects->Container)
			{
				mapobjects->Container->GetLayers(LayersForm->TreeView);
				if(LayersForm->ShowModal()==mrOk)
				{
					mapobjects->Container->SetLayers(LayersForm->TreeView);
				}
			}
		}
	}
	catch(Exception &) {}
}
//---------------------------------------------------------------------------

void __fastcall TVectorDBForm::ZoomInMouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y)
{
	if(!DblClick)
	{
		if(mapobjects && mapobjects->Zoom && mapobjects->Image)
		{
			if(Manager->Settings->EasyZoomIn)
			{
				mapobjects->Zoom->ZoomIn();
				ZoomInBtn->Down = false;
			}
			else
			{
				ZoomInBtn->Down =!ZoomInBtn->Down;
				if(ZoomInBtn->Down) MA = tmaZoomIn;
				else DisableMouseAction();
			}
		}
		else ZoomInBtn->Down=false;
	}
	else DblClick=false;
}
//---------------------------------------------------------------------------

void __fastcall TVectorDBForm::ZoomInDblClick(System::TObject* Sender, int X, int Y)
{
	ZoomInBtn->Down=false;
	if(mapobjects && mapobjects->Zoom) mapobjects->Zoom->ZoomFastIn();
	DisableMouseAction();
	DblClick=true;
}
//---------------------------------------------------------------------------

void __fastcall TVectorDBForm::ZoomOutMouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y)
{
	if(!DblClick)
	{
		ZoomInBtn->Down = false;
		if(mapobjects && mapobjects->Zoom) mapobjects->Zoom->ZoomOut();
		DisableMouseAction();
	}
	else DblClick = false;
}
//---------------------------------------------------------------------------

void __fastcall TVectorDBForm::ZoomOutDblClick(System::TObject* Sender, int X, int Y)
{
	ZoomInBtn->Down=false;
	if(mapobjects && mapobjects->Zoom) mapobjects->Zoom->ZoomFastOut();
	DisableMouseAction();
	DblClick=false;
}
//---------------------------------------------------------------------------

void __fastcall TVectorDBForm::UpdateFileListView(AnsiString aFileName)
{
    std::vector<FileRecord> files;
	std::vector<FileRecord>::iterator it;
	TListItem *item;
    struct stat fileinfo;
	AnsiString full_name, fszName;
	long double fsz;

	try {
		FilesLView->Items->Clear();
		if(FileExists(aFileName))
		{
			TDbDXFMapsContainer::GetDXFFilesList(aFileName, &files);

			for (it = files.begin(); it != files.end(); it++) {
				full_name = it->mFileDir + it->mFileName;

				item = FilesLView->Items->Add();
				item->ImageIndex = 0;
				item->Caption = it->mFileName;


				fsz = it->mFileSize;
				if(it->mFileSize >= _GIGABYTE)
				{
					fszName = " GB";
					fsz /= (long double)_GIGABYTE;
				}
				else if(it->mFileSize >= _MEGABYTE)
				{
					fszName = " MB";
					fsz /= (long double)_MEGABYTE;
				}
				else if(it->mFileSize >= _KILOBYTE)
				{
					fszName = " KB";
					fsz /= (long double)_KILOBYTE;
				}
				else fszName = "";
				item->SubItems->Add(FloatToStrF(fsz, ffFixed, 8, 2) + fszName);
				item->SubItems->Add(FileRecord::TimestampToStrDateTime(it->mFileCreateDate));

				if (0 == stat(full_name.c_str(), &fileinfo)) {
					item->SubItems->Add(FileRecord::TimestampToStrDateTime(fileinfo.st_atime));
				}
				else {
					item->SubItems->Add("Absent");
				}
				item->SubItems->Add(full_name);
			}
		}
	}
	catch(...) {
	}
}
//---------------------------------------------------------------------------

bool __fastcall TVectorDBForm::OpenVectorDB(AnsiString aFileName)
{
	bool res=false;

	if(mapobjects && mapobjects->Container && FileExists(aFileName) && CoordinateSystem!=csLatLon)
	{
		StartMovie(true, rmtBookOpen);
		mapobjects->Container->Clear();
		mapobjects->Zoom->Clear();
		dbmc=new TDbDXFMapsContainer(mapobjects->Container,
			Manager->Settings, mapobjects->Container->ViewportArea);
		dbmc->DisconnectGpsListener();
		dbmc->LockedCS=false;
		if(Manager && Manager->GlobalStopItEvent) ResetEvent(Manager->GlobalStopItEvent);
		dbmc->LoadDataAsThread(aFileName, Manager->GlobalStopItEvent, false,//true,//
			CoordinateSystem, SuccessStopMovie, CancelMovie, false);
		/*try
		{
			FilesLView->Items->Clear();
			files=new TStringList();
			try
			{
				dbmc->GetDXFFilesList(files);
				for(int i=0; i<files->Count; i++)
				{
					if(files->Strings[i]!=NULL && files->Strings[i].Length()>0)
					{
						item=FilesLView->Items->Add();
						item->ImageIndex=0;
						item->Caption=files->Strings[i];
						if(dbmc->GetDXFFileInfo(files->Strings[i], &fileinfo))
						{
							item->SubItems->Add(IntToStr((__int64)fileinfo.st_size));
							item->SubItems->Add(TDXFtoDbLoader::TimestampToStrDateTime(fileinfo.st_atime));
						}
						if(FileExists(files->Strings[i]))
						{
							// ToDO with stat date
						}
					}
				}
				res=true;
			}
			catch(...) {}
		}
		__finally
		{
			delete files;
		}*/
		UpdateFileListView(aFileName);
		res=true;
	}
	else
	{
		FilesLView->Items->Clear();
	}
	DisableButtons();

	return res;
}
//---------------------------------------------------------------------------

void __fastcall TVectorDBForm::DisableButtons()
{
	DeleteIButton->Enabled=false;
	UpdateIButton->Enabled=false;
	ContentsIButton->Enabled=!MoviePanel->Visible;
	PreviewIButton->Enabled=!MoviePanel->Visible && FileExists(FileName);
	AddIButton->Enabled=!MoviePanel->Visible && PreviewIButton->Enabled && ContentsIButton->Down;
	NewIButton->Enabled=!MoviePanel->Visible && ContentsIButton->Down;
	OkImageButton->Enabled=PreviewIButton->Enabled;
}
//---------------------------------------------------------------------------

void __fastcall TVectorDBForm::writeFileName(AnsiString value)
{
	bool Err=true;

	if(value!=NULL && value.Length()>0)
	{
		try
		{
			FileLabel->Caption=ExtractFileName(value);
			filename=value;
			Err=false;
		}
		catch(...) {}
	}
	if(Err)
	{
		filename="";
		FileLabel->Caption="...";
		DisableButtons();
	}
	FilesLView->Items->Clear();
}
//---------------------------------------------------------------------------

void __fastcall TVectorDBForm::BrowseIButtonClick(TObject *Sender)
{
	OpenDialog->Title="Browse for the MultiFile Map";
	OpenDialog->Filter="MultiFile Map|*.dbsl|Any file|*.*";
	if(CoordinateSystem!=csLatLon && OpenDialog->Execute())
	{
		FileName=OpenDialog->FileName;
		if(!OpenVectorDB(FileName)) FileName="";
	}
	else if(!FileExists(FileName)) FileName="";
	DisableButtons();
}
//---------------------------------------------------------------------------

void __fastcall TVectorDBForm::ContentsIButtonClick(TObject *Sender)
{
	PreviewIButton->Down=false;
	ContentsIButton->Down=true;
	Notebook1->PageIndex=Notebook1->Pages->IndexOf("Contents");
	DisableButtons();
}
//---------------------------------------------------------------------------

void __fastcall TVectorDBForm::PreviewIButtonClick(TObject *Sender)
{
	bool b;

	ContentsIButton->Down=false;
	PreviewIButton->Down=true;
	b=(Notebook1->PageIndex!=Notebook1->Pages->IndexOf("Preview"));
	Notebook1->PageIndex=Notebook1->Pages->IndexOf("Preview");
	if(b && mapobjects && mapobjects->Container)// && mapobjects->Zoom)
	{
		//mapobjects->Update();
		//mapobjects->Container->Update();
		//mapobjects->Zoom->Update();
		mapobjects->Container->ZoomUpdate();
	}
	DisableButtons();
}
//---------------------------------------------------------------------------

void __fastcall TVectorDBForm::NewIButtonClick(TObject *Sender)
{
	AnsiString fn;

	SaveDialog->Title="Create a new MultiFile Map";
	SaveDialog->Filter="MultiFile Map|*.dbsl|Any file|*.*";
	if(SaveDialog->Execute())
	{
		// Create a new dbsl file from resourced empty one
		fn=SaveDialog->FileName;
		if(ExtractFileExt(fn)=="") fn+=".dbsl";
		else if(ExtractFileExt(fn)==".") fn+="dbsl";
		if(FileExists(fn)) DeleteFile(fn);
		if (TDXFtoDbLoader::CreateNewDatabase(fn))
		{
			FileName=fn;
			if(!OpenVectorDB(fn)) FileName="";
		}
	}
	DisableButtons();
}
//---------------------------------------------------------------------------

void __fastcall TVectorDBForm::AddIButtonClick(TObject *Sender)
{
	TListItem *item;
	struct stat fileinfo;
	//bool ok_load = false;

	OpenDialog->Title="Add Vector file to the MultiFile Map";
	OpenDialog->Filter="AutoCAD Drawing eXchange Format Files (*.dxf)|*.dxf|Any file|*.*";
	if(mapobjects && dbmc && OpenDialog->Execute())
	{
		StartMovie(true, rmtBookAdd);
		dbmc->Backup();
		/*if (0 == stat(OpenDialog->FileName.t_str(), &fileinfo)) {
			item->SubItems->Strings[0] = IntToStr((__int64)(fileinfo.st_size));
			item->SubItems->Strings[2] = FileRecord::TimestampToStrDateTime(fileinfo.st_atime);
			if(Manager && Manager->GlobalStopItEvent) ResetEvent(Manager->GlobalStopItEvent);
			if(dbmc->AddDXFFile(OpenDialog->FileName, SuccessStopMovie, CancelMovie))// , TProgressBar* ProgressBar);
			{
				if(dbmc->GetDXFFileInfo(OpenDialog->FileName, &fileinfo)) {
//					item->SubItems->Add(IntToStr((__int64)fileinfo.st_size));
//					item->SubItems->Add(FileRecord::TimestampToStrDateTime(fileinfo.st_atime));
				}
				ok_load = true;
			}
		}

		if (!ok_load) {
//			item->SubItems->Add("- Error -");
			item->SubItems->Strings[2] = "-- Error --";
		}*/
		if(Manager && Manager->GlobalStopItEvent) ResetEvent(Manager->GlobalStopItEvent);
		if(!dbmc->AddDXFFile(OpenDialog->FileName, SuccessStopMovieAndReopen, CancelMovie))
			CancelMovie();
	}
	DisableButtons();
}
//---------------------------------------------------------------------------

void __fastcall TVectorDBForm::FilesLViewClick(TObject *Sender)
{
	if(FilesLView->Selected)
	{
		DeleteIButton->Enabled=true;
		UpdateIButton->Enabled=true;
	}
	else
	{
		DisableButtons();
	}
}
//---------------------------------------------------------------------------

void __fastcall TVectorDBForm::DeleteIButtonClick(TObject *Sender)
{
	AnsiString str;

	if(FilesLView->Selected && mapobjects && dbmc)
	{
		int i=Application->MessageBox(L"Are you sure to delete Vector file from the MultiFile Map?", L"Delete Vector File", MB_YESNO | MB_ICONQUESTION);
		if(i==ID_YES)
		{
			StartMovie(true, rmtBookDelete);
			dbmc->Backup();
			if(Manager && Manager->GlobalStopItEvent) ResetEvent(Manager->GlobalStopItEvent);
			if(FilesLView->Selected->SubItems->Count>3)
			{
				if(!dbmc->DeleteDXFFile(FilesLView->Selected->SubItems->Strings[3],
					SuccessStopMovieAndReopen, CancelMovie))
					CancelMovie();
			}
			else FilesLView->Items->Delete(FilesLView->Items->IndexOf(FilesLView->Selected));
		}
	}
	DisableButtons();
}
//---------------------------------------------------------------------------

void __fastcall TVectorDBForm::UpdateIButtonClick(TObject *Sender)
{
	if(FilesLView->Selected && mapobjects && dbmc)
	{
		OpenDialog->Title="Update Vector file in the MultiFile Map";
		OpenDialog->Filter="AutoCAD Drawing eXchange Format Files (*.dxf)|*.dxf|Any file|*.*";
		if(mapobjects && dbmc && OpenDialog->Execute() )
		{
			StartMovie(true, rmtBookUpdate);
			dbmc->Backup();
			if(Manager && Manager->GlobalStopItEvent) ResetEvent(Manager->GlobalStopItEvent);
			if(FilesLView->Selected->SubItems->Count>3)
			{
				if(!dbmc->RefreshDXFFile(FilesLView->Selected->SubItems->Strings[3],
					OpenDialog->FileName, SuccessStopMovieAndReopen, CancelMovie))
					CancelMovie();
			}
		}
	}
	DisableButtons();
}
//---------------------------------------------------------------------------

void __fastcall TVectorDBForm::StartMovie(bool WaitOrStop, TResourceMovieType rmt)
{
	WaitLabel->Visible=WaitOrStop;
	StopButton->Enabled=!WaitOrStop;
	StopButton->Visible=!WaitOrStop;
	MoviePanel->Visible=true;
	ProcessMessages();
	BrowseIButton->Enabled=false;
	FilesLView->Enabled=false;
	ContentsIButton->Enabled=false;
	NewIButton->Enabled=false;
	OkImageButton->Enabled=false;
	DeleteIButton->Enabled=false;
	UpdateIButton->Enabled=false;
	PreviewIButton->Enabled=false;
	AddIButton->Enabled=false;
	if(ResMov) ResMov->Start(rmt);
}
//---------------------------------------------------------------------------

void __fastcall TVectorDBForm::StopMovie()
{
	try
	{
		if(Visible)
		{
			if(ResMov) ResMov->Stop();
			MoviePanel->Visible=false;
			DisableButtons();
			BrowseIButton->Enabled=true;
			FilesLView->Enabled=true;
			ContentsIButton->Enabled=true;
			NewIButton->Enabled=true;
		}
	}
	catch(...) {}
}
//---------------------------------------------------------------------------

void __fastcall TVectorDBForm::CancelMovie()
{
	try
	{
		if(Visible)
		{
			if(Manager && Manager->GlobalStopItEvent) SetEvent(Manager->GlobalStopItEvent);
			if(dbmc)
			{
				dbmc->RollbackBackup();
				if(mapobjects)
				{
					mapobjects->Container->Clear();
					mapobjects->Zoom->Clear();
				}
				else delete dbmc;
				dbmc=NULL;
			}
			ContentsIButtonClick(ContentsIButton);
		}
		FileName="";
		StopMovie();
	}
	catch(...) {}
}
//---------------------------------------------------------------------------

void __fastcall TVectorDBForm::StopButtonClick(TObject *Sender)
{
	CancelMovie();
}
//---------------------------------------------------------------------------

