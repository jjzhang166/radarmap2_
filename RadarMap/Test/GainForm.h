//---------------------------------------------------------------------------

#ifndef GainFormH
#define GainFormH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <ImageButton.h>
#include <pngimage.hpp>
#include <GR32_Image.hpp>
#include "GainItems.h"
#include <Graphics.hpp>
#include "Manager.h"
#include <Dialogs.hpp>
#include <ComCtrls.hpp>
#include <ImgList.hpp>
//---------------------------------------------------------------------------
class TGainForm : public TForm
{
__published:	// IDE-managed Components
	TPanel *Panel1;
	TPanel *Panel2;
	TPanel *Panel3;
	TPanel *Panel4;
	TPanel *TitlePanel;
	TPanel *Panel5;
	TPanel *Panel6;
	TPanel *Panel7;
	TPanel *PagesPanel;
	TPanel *OkCancelPanel;
	TLabel *Label1;
	TPanel *Panel8;
	TPanel *Panel9;
	TImageButton *CloseIButton;
	TImageButton *CancelImageButton;
	TImageButton *OkImageButton;
	TEdit *TempEdit;
	TPanel *Panel10;
	TPanel *Panel11;
	TPanel *Panel12;
	TPanel *Panel14;
	TPanel *Panel15;
	TPanel *Panel13;
	TPanel *GainPointsPanel;
	TImgView32 *Image;
	void __fastcall CloseIButtonClick(TObject *Sender);
	void __fastcall OkImageButtonClick(TObject *Sender);
	void __fastcall TitlePanelMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
	void __fastcall TitlePanelMouseMove(TObject *Sender, TShiftState Shift, int X, int Y);
	void __fastcall TitlePanelMouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
private:	// User declarations
	bool TitleDown;
	Types::TPoint TitleXY;
	TGainItems *gainitems;
	Types::TRect BordersRect;
	TTrace *trace;
	int ChannelsPxOffset;
	TOscilloscopeGrid *GainImageGrid;
	void __fastcall writeTrace(TTrace *value) {if(gainitems) gainitems->SingleTrace=value;}
public:		// User declarations
	__fastcall TGainForm(TComponent* Owner, TObject* AManager);
	__fastcall ~TGainForm() {delete GainImageGrid; delete gainitems;}

	__property TGainItems *GainItems = {read=gainitems};
	__property TTrace *Trace = {read=trace, write=writeTrace};
};
//---------------------------------------------------------------------------
extern PACKAGE TGainForm *GainForm;
//---------------------------------------------------------------------------
#endif
