//---------------------------------------------------------------------------

#pragma hdrstop

#include "ImageMap.h"
#include "Manager.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

//---------------------------------------------------------------------------
// TFrameVertex
//---------------------------------------------------------------------------
__fastcall TFrameVertex::TFrameVertex(TMyObject *AOwner, TImgView32 *ImgView,
	TFrameVertexPosition APos, TFrameVertexType AType, Types::TRect *ARectToControl, TObject *AManager)
	: TToolObject((TMyObject*)AOwner, ImgView, Types::TRect(0,0,0,0), AManager)
{
	visible=false;
	position=APos;
	cornertype=AType;
	recttocontrol=ARectToControl;
	color=clBlack32;
	onmousemove=NULL;
	onmouseup=NULL;
	if(AOwner) ((TGraphicObjectsContainer *)AOwner)->SendToBack(objecttype, index);
	AutomaticUpdate=true;
	DefineRect();
}
//---------------------------------------------------------------------------

void __fastcall TFrameVertex::DefineRect()
{
	Types::TRect rtc;
	if(recttocontrol)
	{
		rtc=Types::TRect(recttocontrol->left, recttocontrol->top, recttocontrol->right, recttocontrol->bottom);
		if(rtc.Width()<ScalingPointRadius*2+1)
		{
			 rtc=Types::TRect(((rtc.left+rtc.right)>>1)-ScalingPointRadius, rtc.top,
				((rtc.left+rtc.right)>>1)+ScalingPointRadius, rtc.bottom);
		}
		if(rtc.Height()<ScalingPointRadius*2+1)
		{
			rtc=Types::TRect(rtc.left, ((rtc.top+rtc.bottom)>>1)-ScalingPointRadius,
				rtc.right, ((rtc.top+rtc.bottom)>>1)+ScalingPointRadius);
		}
		switch(position)
		{
			case fvpCenter: center=Types::TPoint((rtc.Left+rtc.Right)>>1, (rtc.Top+rtc.Bottom)>>1);	break;
			case fvpLeftTop: center=Types::TPoint(rtc.left, rtc.top); break;
			case fvpLeftBottom:	center=Types::TPoint(rtc.left, rtc.bottom-1); break;
			case fvpRightBottom: center=Types::TPoint(rtc.right-1, rtc.bottom-1); break;
			case fvpRightTop: center=Types::TPoint(rtc.right-1, rtc.top); break;
			case fvpTop: center=Types::TPoint((rtc.left+rtc.right)>>1, rtc.top); break;
			case fvpLeft: center=Types::TPoint(rtc.left, (rtc.top+rtc.bottom)>>1); break;
			case fvpBottom:	center=Types::TPoint((rtc.left+rtc.right)>>1, rtc.bottom-1); break;
			case fvpRight: center=Types::TPoint(rtc.right-1, (rtc.top+rtc.bottom)>>1); break;
		}
		Rect=Types::TRect(center.x-ScalingPointRadius, center.y-ScalingPointRadius,
			center.x+ScalingPointRadius+1, center.y+ScalingPointRadius+1);
	}
}
//---------------------------------------------------------------------------

void __fastcall TFrameVertex::writeRectToControl(Types::TRect *value)
{
	if(recttocontrol!=value)
	{
		recttocontrol=value;
		DefineRect();
		TGraphicObject::Update();
	}
}
//---------------------------------------------------------------------------

void __fastcall TFrameVertex::Redraw(TObject *Sender, TBitmap32 *Bmp)
{
	TPolygon32 *poly;
	TColor32Entry ce;

	if(Visible && Bmp)
	{
		//DefineRect();
		ce.ARGB=Color32((TColor)((TRadarMapManager*)Manager)->Settings->DXFBackgroundColor);
		ce.A=200;
		Bmp->BeginUpdate();
		try
		{
			switch(cornertype)
			{
				case fvtCircle:
					DrawEllipse32(Bmp, center.x, center.y, ScalingPointRadius, ScalingPointRadius,
						image->GetViewportRect(), color, ce.ARGB, edOutlineFill);
					break;
				case fvtRect:
				default:
					Bmp->FillRectTS(Rect, ce.ARGB);
					Bmp->FrameRectS(Rect, color);
			}
		}
		__finally
		{
			Bmp->EndUpdate();
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TFrameVertex::MouseDown(TObject *Sender,
	TMouseButton Button, TShiftState Shift, int X, int Y,
	TCustomLayer *Layer)
{
	dx=X1-center.x;
	dy=Y1-center.y;
}

//---------------------------------------------------------------------------
// TBitmapFrameTool
//---------------------------------------------------------------------------
__fastcall TBitmapFrameTool::TBitmapFrameTool(TGraphicObjectsContainer *AOwner, TMyObject* AContainer,
	TBitmap32* ABitmap, TLockedDoubleRect *AViewportArea, Types::TRect *ARawPxRect, TObject *AManager)
	: TManagerObject((TMyObject*)AOwner, gotCustom, Types::TRect(0,0,0,0), AManager)
{
	TFrameVertex *cv;

	//mouseable=false;
	allowneighbormousemove=true;
	SrcBmp=ABitmap;
	if(SrcBmp) boundaryrect=SrcBmp->BoundsRect();
	else boundaryrect=Types::TRect();
	croprect=::TFloatRect(0,0,1.,1.);
	Thumb=new TBitmap32();
	Thumb->DrawMode=dmBlend;
	Thumb->SetSizeFrom(SrcBmp);
	TransformThrd=NULL;
	viewportarea=AViewportArea;
	RawPxRect=ARawPxRect;
	DefineRect();
	Container=AContainer;
	//if(Container) ((TMapsContainer*)Container)->ShowBackground=false;
	TransformThrd=new TImageMapTransformThread(this /*Container*/, SrcBmp, Thumb, &boundaryrect, &Rect, Manager);
	//TransformThrd=NULL;
	Vertexes=new TList;
	//cv=new TFrameVertex(Owner, ((TMapObjectsContainer*)Owner)->Image, fvpCenter, fvtRect, &SrcBmp, Manager);
	//cv->OnMouseMove=MoveFrame;
	//Controls->Add(cv);
	cv=new TFrameVertex(Owner, ((TMapObjectsContainer*)Owner)->Image, fvpLeftTop, fvtRect, &Rect, Manager);
	cv->OnMouseMove=ResizeFrame;
	cv->OnMouseUp=VertexMouseUp;
	Vertexes->Add(cv);
	cv=new TFrameVertex(Owner, ((TMapObjectsContainer*)Owner)->Image, fvpLeftBottom, fvtRect, &Rect, Manager);
	cv->OnMouseMove=ResizeFrame;
	cv->OnMouseUp=VertexMouseUp;
	Vertexes->Add(cv);
	cv=new TFrameVertex(Owner, ((TMapObjectsContainer*)Owner)->Image, fvpRightBottom, fvtRect, &Rect, Manager);
	cv->OnMouseMove=ResizeFrame;
	cv->OnMouseUp=VertexMouseUp;
	Vertexes->Add(cv);
	//cv=new TFrameVertex(Owner, ((TMapObjectsContainer*)Owner)->Image, fvpRightTop, fvtCircle, &Rect, Manager);
	//cv->OnMouseMove=RotateFrame;
	cv=new TFrameVertex(Owner, ((TMapObjectsContainer*)Owner)->Image, fvpRightTop, fvtRect, &Rect, Manager);
	cv->OnMouseMove=ResizeFrame;
	cv->OnMouseUp=VertexMouseUp;
	Vertexes->Add(cv);
	/*cv=new TFrameVertex(Owner, image, fvpTop, fvtRect, &OutputRect, Manager);
	cv->OnMouseMove=CropFrame;
	Vertexes->Add(cv);
	cv=new TFrameVertex(Owner, image, fvpLeft, fvtRect, &OutputRect, Manager);
	cv->OnMouseMove=CropFrame;
	Vertexes->Add(cv);
	cv=new TFrameVertex(Owner, image, fvpBottom, fvtRect, &OutputRect, Manager);
	cv->OnMouseMove=CropFrame;
	Vertexes->Add(cv);
	cv=new TFrameVertex(Owner, image, fvpRight, fvtRect, &OutputRect, Manager);
	cv->OnMouseMove=CropFrame;
	Vertexes->Add(cv);*/
	State=bfsDormancy;//bfsScaling;//
	if(AOwner) ((TGraphicObjectsContainer *)AOwner)->SendToBack(objecttype, index);

	if(TransformThrd) SetEvent(TransformThrd->RefreshEvent);
}
//---------------------------------------------------------------------------

__fastcall TBitmapFrameTool::~TBitmapFrameTool()
{
	//if(Container) ((TMapsContainer*)Container)->ShowBackground=true;
	if(TransformThrd)
	{
		TransformThrd->AskForTerminate();
		WaitOthers();
		if(TransformThrd->ForceTerminate()) delete TransformThrd;
		TransformThrd=NULL;
    }
	delete Thumb;
	Thumb=NULL;
	for(int i=0; i<Vertexes->Count; i++)
		delete (TFrameVertex*)Vertexes->Items[i];
	Vertexes->Clear();
	delete Vertexes;
	try
	{
		if(Container) ((TImageMapsContainer*)Container)->NullBitmapFrame(this);
	}
    catch(Exception &e) {}
}
//---------------------------------------------------------------------------

void __fastcall TBitmapFrameTool::writeState(TBitmapFrameState value)
{
	TFrameVertex *cv;

	if(state!=value)
	{
		state=value;
		for(int i=0; i<Vertexes->Count; i++)
		{
			cv=(TFrameVertex*)Vertexes->Items[i];
			if(cv)
			{
				switch(state)
				{
					case bfsScaling:
						if(cv->Position>=fvpRightTop && cv->Position<=fvpLeftTop) cv->Visible=true;
						else cv->Visible=false;
						break;
					case bfsCroping:
						if(cv->Position>=fvpTop && cv->Position<=fvpLeft) cv->Visible=true;
						else cv->Visible=false;
						break;
					case bfsReferencing:
						//write whot ToDo and hide all controls (without 'break;'}
					case bfsDormancy: //Calm down state :)
					default:
						cv->Visible=false;
				}
			}
		}
		if(state==bfsDormancy) Visible=false;
		else Visible=true;
	}
}
//---------------------------------------------------------------------------

void __fastcall TBitmapFrameTool::DefineRect()
{
/*	Types::TRect bmr, temp;
	//int w, h;
	//bool b=false;
	TImgView32 *image;

	if(Owner)
	{
		image=((TMapObjectsContainer*)Owner)->Image;
		if(image && image->Bitmap)
		{
			bmr=image->GetBitmapRect();
			dw=(float)bmr.Width()/(float)image->Bitmap->Width;
			dh=(float)bmr.Height()/(float)image->Bitmap->Height;
			temp=boundaryrect.Scale(dw, dh);
			Rect=temp.Move(bmr.Left, bmr.Top);
//			w=boundaryrect.Width();
//			h=boundaryrect.Height();
//			if(Thumb->Width>w) w=Thumb->Width;
//			else b=true;
//			if(Thumb->Height>h) h=Thumb->Height;
//			else b=true;
//			if(b)
//			{
//				Thumb->SetSize(w, h);
//				if(TransformThrd) TransformThrd->DefineGraphics();
//			}
		}
	}*/
	double w, h, vpw, vph;
	Types::TRect rct, bmr;
	Types::TPoint P;
	TImgView32 *image;

	w=RawPxRect->Width();
	h=RawPxRect->Height();
	if(w==0) w=SrcBmp->Width;
	if(h==0) h=SrcBmp->Height;
	if(viewportarea)
	{
		vpw=viewportarea->Width();
		vph=viewportarea->Height();
		if(Owner)
		{
			image=((TMapObjectsContainer*)Owner)->Image;
			if(image && image->Bitmap)
			{
				if(Container) bmr=((TImageMapsContainer*)Container)->GetBitmapRect();
				else bmr=image->GetBitmapRect();
				P=((TMapObjectsContainer*)Owner)->Container->ViewportAreaToScreen(
					DoublePoint((double)RawPxRect->Left/(double)bmr.Width(), (double)RawPxRect->Top/(double)bmr.Height()));
				rct.Left=P.x;
				rct.Top=P.y;
				rct.Right=rct.Left+(int)(w/vpw+0.5);
				rct.Bottom=rct.Top+(int)(h/vph+0.5);
				/*P=((TMapObjectsContainer*)Owner)->Container->ViewportAreaToScreen(
					DoublePoint((double)RawPxRect->Right/(double)bmr.Width(), (double)RawPxRect->Bottom/(double)bmr.Height()));
				rct.Right=P.x;
				rct.Bottom=P.y;*/
			}
		}
	}
	else rct=Types::TRect(0, 0, w, h);
	boundaryrect=rct;
	Rect=rct;
}
//---------------------------------------------------------------------------

void __fastcall TBitmapFrameTool::ZoomUpdate()
{
	DefineRect();
	if(TransformThrd && Visible)// && State!=bfsDormancy && State!=bfsReferencing)
	{
		TransformThrd->GenerateThumbnail();
		SetEvent(TransformThrd->RefreshEvent);
	}
	else TGraphicObject::ZoomUpdate();
}
//---------------------------------------------------------------------------

void __fastcall TBitmapFrameTool::Redraw(TObject *Sender, TBitmap32 *Bmp)
{
	TColor32Entry ce;

	if(Visible && Bmp)
	{
		DefineRect();
		Bmp->BeginUpdate();
		try
		{
			switch(state)
			{
				case bfsReferencing:
					//break;
				case bfsCroping:
					Bmp->FrameRectTS(OutputRect, clDimGray32);
				case bfsScaling:
/*					if(Rect.Width()>Thumb->Width || Rect.Height()>Thumb->Height)
					{
						//Types::TRect vpt;
						int l, t, r, b;

						//vpt=image->GetViewPortRect();
						if(Rect.Left<0) l=0;
						else l=Rect.Left;
						if(Rect.Top<0) t=0;
						else t=Rect.Top;
						if(Rect.Right>Thumb->Width) r=Thumb->Width;
						else r=Rect.Right;
						if(Rect.Bottom>Thumb->Height) b=Thumb->Height;
						else b=Rect.Bottom;

						if(Rect.Contains(Types::TRect(l, t, r, b)))
							Bmp->Draw(Rect, Types::TRect(0, 0, Thumb->Width, Thumb->Height), Thumb);
					}
					else Bmp->Draw(Rect, Types::TRect(0,0,Rect.Width(), Rect.Height()), Thumb);  */
					ce.ARGB=Color32((TColor)((TRadarMapManager*)Manager)->Settings->DXFBackgroundColor);
					ce.A=80;//120;
					Bmp->FillRectTS(Rect, ce.ARGB);
					Bmp->FrameRectTS(Rect, clBlack32);
					break;
				case bfsDormancy:
				default:
					;
			}
		}
		__finally
		{
			Bmp->EndUpdate();
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TBitmapFrameTool::UpdateVertexes()
{
	DefineRect();
	Container->Tag=0;
	if(TransformThrd && Visible && State!=bfsDormancy && State!=bfsReferencing)
		SetEvent(TransformThrd->RefreshEvent);
	for(int i=0; i<Vertexes->Count; i++)
	{
		((TFrameVertex*)Vertexes->Items[i])->Update();
	}
}
//---------------------------------------------------------------------------

void __fastcall TBitmapFrameTool::MouseMove(TObject* Sender, Classes::TShiftState Shift,
	int X, int Y, TCustomLayer *Layer)
{
	/*int x, y;
	if(Pressed)
	{
		x=X;
		y=Y;
		X-=X1;
		Y-=Y1;
		X=(int)((float)X/dw);
		Y=(int)((float)Y/dh);
		boundaryrect=boundaryrect.Move(X, Y);
		UpdateVertexes();
		X1=x;
		Y1=y;
	}*/
}
//---------------------------------------------------------------------------

void __fastcall TBitmapFrameTool::MouseUp(TObject *Sender, TMouseButton Button,
	TShiftState Shift, int X, int Y, TCustomLayer *Layer)
{
	try
	{
		if(State==bfsReferencing && Owner && ((TGraphicObjectsContainer*)Owner)->MouseAction && viewportarea && SrcBmp &&
			*((TGraphicObjectsContainer*)Owner)->MouseAction==tmaReferenceFlag && Manager && ((TRadarMapManager*)Manager)->Settings &&
			Owner && ((TGraphicObjectsContainer*)Owner)->Image && Container && ((TMapsContainer*)Container)->Background &&
			((TMapsContainer*)Container)->Background->Width>0 && ((TMapsContainer*)Container)->Background->Height>0)
		{
			TReferncePointLabel *rpl;
			Types::TPoint sp, pp;
			TDoublePoint dp;
			Types::TRect vpt;//, brt;
			TImgView32 *Image;
			//int bw, bh;//, w, h;
			//float vpw, vph;

			//vpw=viewportarea->Width();
			//vph=viewportarea->Height();
			Image=((TGraphicObjectsContainer*)Owner)->Image;
			vpt=Image->GetViewportRect();
			//brt=Image->GetBitmapRect();
			//bw=brt.Width();
			//bh=brt.Height();
			dp=((TMapObjectsContainer*)Owner)->Container->ScreenToViewportArea(Types::TPoint(X, Y));
			//if(bw>0 && X>=brt.Left && X<(bw+brt.Left) && bh>0 && Y>=brt.Top && Y<(bh+brt.Top))
			if(dp.X>=0 && dp.X<=1. && dp.Y>=0 && dp.Y<=1.)
			{
				if(((TImageMapsContainer*)Container)->ReferencesCount<ReferencesPointsMaxQ)
				{
					//w=((TMapsContainer*)Container)->Background->Width;
					//h=((TMapsContainer*)Container)->Background->Height;

					//pw=(SrcBmp->Width/vpw);
					//x0=-viewportarea->Left*pw;
					//rct.Left=x0+RawPxRect->Left/vpw+bmr.Left;
					sp.x=(int)(dp.X*(double)SrcBmp->Width+0.5)-RawPxRect->Left;
					sp.y=(int)(dp.Y*(double)SrcBmp->Height+0.5)-RawPxRect->Top;
					// 2 - 1492 x 188
					/*sp.x=X-brt.Left;
					sp.x=(int)((double)w*((double)sp.x/(double)bw));
					sp.y=Y-brt.Top;
					sp.y=(int)((double)h*((double)sp.y/(double)bh));*/
					pp=((TImageMapsContainer*)Container)->FindNearestAtXY(sp);
					if(pp.x>0 && pp.y>0 && abs((double)pp.x-sp.x)>=((int)0.1*(double)0.1*RawPxRect->Width()) && abs((double)pp.y-sp.y)>=((int)0.1*(double)RawPxRect->Height()))
					{
						rpl=new TReferncePointLabel(Owner, vpt, ((TMapsContainer*)Container)->LatLonRect, Container,
							((TRadarMapManager*)Manager)->Settings->ReferencePointCoordinatesQ, sp, Manager);
						rpl->MouseUp(Sender, mbRight, TShiftState(ssRight), 0, 0, NULL);
						rpl->Color=clPurple32;
						if(((TMapsContainer*)Container)->OnDisableButton)
							(((TMapsContainer*)Container)->OnDisableButton)();//tmaReferenceFlag);
					}
					else
					{
						((TRadarMapManager*)Manager)->ShowMessageObject(NULL, ((TRadarMapManager*)Manager)->MapMessages,
							TMessageType::mtWarning, mvtDisapering, 3000, "Too close to the existing reference point!", "Warning");
					}
				}
				else
				{
					((TRadarMapManager*)Manager)->ShowMessageObject(NULL, ((TRadarMapManager*)Manager)->MapMessages,
						TMessageType::mtWarning, mvtDisapering, 3000, "Not more than "+IntToStr(ReferencesPointsMaxQ)+" reference points per Image is allowed!", "Warning");
				}
			}
			else
			{
				((TRadarMapManager*)Manager)->ShowMessageObject(NULL, ((TRadarMapManager*)Manager)->MapMessages,
						TMessageType::mtWarning, mvtDisapering, 3000, "Point is out of the image!", "Warning");
			}
		}
	}
	catch(Exception &e) {}
}
//---------------------------------------------------------------------------

void __fastcall TBitmapFrameTool::MoveFrame(TFrameVertex* Sender, Classes::TShiftState Shift, int X, int Y)
{

}
//---------------------------------------------------------------------------

void __fastcall TBitmapFrameTool::VertexMouseUp(TFrameVertex* Sender, TMouseButton Button, TShiftState Shift, int X, int Y)
{
	if(TransformThrd && Visible && State!=bfsDormancy && State!=bfsReferencing)
	{
		TransformThrd->GenerateThumbnail();
		SetEvent(TransformThrd->RefreshEvent);
	}
}
//---------------------------------------------------------------------------

void __fastcall TBitmapFrameTool::ResizeFrame(TFrameVertex* Sender, Classes::TShiftState Shift, int X, int Y)
{
	Types::TRect vpt, bmr;
	TImgView32 *image;

	if(Owner)
	{
		image=((TMapObjectsContainer*)Owner)->Image;
		if(Sender->Pressed && image && image->Bitmap)
		{
			vpt=((TMapObjectsContainer*)Owner)->Image->GetViewportRect();
			bmr=image->GetBitmapRect();
			X-=Sender->dX+bmr.Left;
			X=(int)((float)X/dw);
			Y-=Sender->dY+bmr.Top;
			Y=(int)((float)Y/dh);
			switch(Sender->Position)
			{
				//Rotate
				case fvpRightTop:
					if(X<boundaryrect.Left) X=boundaryrect.Left;
					if(Y>boundaryrect.Bottom) Y=boundaryrect.Bottom;
					boundaryrect=Types::TRect(boundaryrect.Left, Y, X, boundaryrect.Bottom);
					break;
				case fvpRightBottom:
					if(X<boundaryrect.Left) X=boundaryrect.Left;
					if(Y<boundaryrect.Top) Y=boundaryrect.Top;
					boundaryrect=Types::TRect(boundaryrect.Left, boundaryrect.Top, X, Y);
					break;
				case fvpLeftBottom:
					if(X>boundaryrect.Right) X=boundaryrect.Right;
					if(Y<boundaryrect.Top) Y=boundaryrect.Top;
					boundaryrect=Types::TRect(X, boundaryrect.Top, boundaryrect.Right, Y);
					break;
				case fvpLeftTop:
					if(X>boundaryrect.Right) X=boundaryrect.Right;
					if(Y>boundaryrect.Bottom) Y=boundaryrect.Bottom;
					boundaryrect=Types::TRect(X, Y, boundaryrect.Right, boundaryrect.Bottom);
					break;
				case fvpTop:
					if(Y>boundaryrect.Bottom) Y=boundaryrect.Bottom;
					boundaryrect=Types::TRect(boundaryrect.Left, Y, boundaryrect.Right, boundaryrect.Bottom);
					break;
				case fvpRight:
					if(X<boundaryrect.Left) X=boundaryrect.Left;
					boundaryrect=Types::TRect(boundaryrect.Left, boundaryrect.Top, X, boundaryrect.Bottom);
					break;
				case fvpBottom:
					if(Y<boundaryrect.Top) Y=boundaryrect.Top;
					boundaryrect=Types::TRect(boundaryrect.Left, boundaryrect.Top, boundaryrect.Right, Y);
					break;
				case fvpLeft:
					if(X>boundaryrect.Right) X=boundaryrect.Right;
					boundaryrect=Types::TRect(X, boundaryrect.Top, boundaryrect.Right, boundaryrect.Bottom);
					break;
				//Move
				case fvpCenter: break;
			}
			UpdateVertexes();
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TBitmapFrameTool::RotateFrame(TFrameVertex* Sender, Classes::TShiftState Shift, int X, int Y)
{

}
//---------------------------------------------------------------------------

void __fastcall TBitmapFrameTool::CropFrame(TFrameVertex* Sender, Classes::TShiftState Shift, int X, int Y)
{

}

//---------------------------------------------------------------------------
// TImageMapTransformThread
//---------------------------------------------------------------------------
__fastcall TImageMapTransformThread::TImageMapTransformThread(TMyObject *AOwner,
	TBitmap32* ASrcBmp, TBitmap32* AThumb, Types::TRect *ARect, Types::TRect *AThumbRect,
	TObject* AManager) : TMyThread(false)
{
	Owner=AOwner;
	SrcBmp=ASrcBmp;
	Thumb=AThumb;
	Rect=ARect;
	ThumbRect=AThumbRect;
	Manager=AManager;
	refreshevent=CreateEvent(NULL, false, false, NULL);
	Priority=tpLowest;
	GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);
	graphics=NULL;
	if(SrcBmp && AThumb)
	{
		DefineGraphics();
		originalimage=new Gdiplus::Bitmap(&(SrcBmp->BitmapInfo), SrcBmp->Bits);
		/*SrcBmp->BeginUpdate();
		try
		{
			TColor32 c;
			try
			{
				if(Manager) c=((TRadarMapManager*)Manager)->Settings->DXFBackgroundColor;
				else c=clWhite32;
				SrcBmp->Clear(c);
			}
			catch(Exception &e) {}
		}
		__finally
		{
			SrcBmp->EndUpdate();
		}*/
	}
	else
	{
		originalimage=NULL;
	}
	thumbnail=NULL;
}
//---------------------------------------------------------------------------

__fastcall TImageMapTransformThread::~TImageMapTransformThread()
{
	if(graphics) delete graphics;
	SrcBmp->BeginUpdate();
	try
	{
		try
		{
			graphics=new Gdiplus::Graphics(SrcBmp->Handle);
			graphics->DrawImage(originalimage, 0, 0, Rect->Width(), Rect->Height());
		}
		catch(Exception &e) {}
	}
	__finally
	{
		SrcBmp->EndUpdate();
	}
	if(originalimage) delete originalimage;
	if(graphics) delete graphics;
	if(thumbnail) delete thumbnail;
	Gdiplus::GdiplusShutdown(gdiplusToken);
}
//---------------------------------------------------------------------------

void __fastcall TImageMapTransformThread::GenerateThumbnail()
{
	if(thumbnail) delete thumbnail;
	if(originalimage && ThumbRect)
		thumbnail=originalimage->GetThumbnailImage(ThumbRect->Width(), ThumbRect->Height(), NULL, NULL);
	else thumbnail=NULL;
}
//---------------------------------------------------------------------------

void __fastcall TImageMapTransformThread::DefineGraphics()
{
	if(graphics) delete graphics;
	if(Thumb) graphics=new Gdiplus::Graphics(Thumb->Handle);
	else graphics=NULL;
}

void __fastcall TImageMapTransformThread::ExecuteLoopBody()
{
	//unsigned long tc;

	if(WaitForSingleObject(RefreshEvent, 100)==WAIT_OBJECT_0)
	{
		//tc=Gr32_system::GetTickCount();
		//while(WaitForSingleObject(RefreshEvent, 250)==WAIT_OBJECT_0) ;// && (tc-Gr32_system::GetTickCount())<500) tc=Gr32_system::GetTickCount();
		try
		{
			/*if(Owner && graphics && originalimage)
			{
				Thumb->BeginUpdate();
				try
				{
					try
					{
						graphics->ResetTransform();
						if(!thumbnail) GenerateThumbnail();
						if(thumbnail && Thumb)
						{
							if(ThumbRect->Width()>Thumb->Width || ThumbRect->Height()>Thumb->Height)
							{
								graphics->DrawImage(originalimage, 0, 0);//l, t);
							}
							else graphics->DrawImage(thumbnail, 0, 0, ThumbRect->Width(), ThumbRect->Height());
						}
					}
					catch(Exception &e)
					{
						if(originalimage) delete originalimage;
						originalimage=NULL;
						if(graphics) delete graphics;
						graphics=NULL;
						try
						{
							if(thumbnail) delete thumbnail;
						}
						catch(Exception &e) {}
						thumbnail=NULL;
						Gdiplus::GdiplusShutdown(gdiplusToken);
						GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);
						DefineGraphics();
						originalimage=new Gdiplus::Bitmap(&(SrcBmp->BitmapInfo), SrcBmp->Bits);
					}
				}
				__finally
				{
					Thumb->EndUpdate();
				}
			}*/
		}
		__finally
		{
			if(Owner && WaitForSingleObject(RefreshEvent, 0)!=WAIT_OBJECT_0)
			{
				//Synchronize(Visualize);
				Owner->Update(gotNone);
			}
		}
	}
	MySleep(40);
}

//---------------------------------------------------------------------------
// TImageMapsContainer
//---------------------------------------------------------------------------
__fastcall TImageMapsContainer::TImageMapsContainer(TMyObject *AOwner, void* ASettings, TLockedDoubleRect *AViewportArea,
	TGraphicObjectsContainer* AToolsOwner) : TBitmapMapsContainer(AOwner, ASettings, AViewportArea)
{
	maptype=mtAdjustable;
	filename="adjustable";
	klicnummer="Adjustable";
	ToolsOwner=AToolsOwner;
	previewfilename="";
	/*if(ToolsOwner) Frame=new TBitmapFrameTool(ToolsOwner, this, ((TMapObjectsContainer*)ToolsOwner)->Image,
		Background, Manager);
	else*/
	Frame=NULL;
	ReferencePoints=new TList;
	/*if(Background)
	{
		Background->BeginUpdate();
		try
		{
			Background->Clear(((TRadarMapSettings*)radarmapsettings)->DXFBackgroundColor);
		}
		__finally
		{
			Background->EndUpdate();
		}
	}*/
	WrongReferenceMsg=NULL;
	RenderMap();
	Update();
}
//---------------------------------------------------------------------------

__fastcall TImageMapsContainer::~TImageMapsContainer()
{
	if(WrongReferenceMsg) ((TMessageObject*)WrongReferenceMsg)->SwitchOff();
	for(int i=0; i<ReferencePoints->Count; i++) delete (TReferencePoint*)ReferencePoints->Items[i];
	delete ReferencePoints;
	if(Frame) delete Frame;
}
//---------------------------------------------------------------------------

bool __fastcall TImageMapsContainer::AddReferencePoint(TReferencePoint RP)
{
	bool res=false;
	TReferencePoint *rp;

	rp=FindReferencePointAtXY(RP.XY);
	if(rp)
	{
		rp->C=RP.C;
		res=true;
	}
	else if(RP.XY.x>=0 && RP.XY.x<=InitialBmp->Width && RP.XY.y>=0 && RP.XY.y<=InitialBmp->Height &&
		ReferencePoints->Count<ReferencesPointsMaxQ)
	{
		try
		{
			ReferencePoints->Add(new TReferencePoint(RP));
			RescaleReferences();
			res=true;
		}
		catch(Exception &e) {res=false;}
	}
	return res;
}
//---------------------------------------------------------------------------

TReferencePoint* __fastcall TImageMapsContainer::FindReferencePointAtXY(Types::TPoint XY)
{
	TReferencePoint *rp, *res=NULL;

	for(int i=0; i<ReferencePoints->Count; i++)
	{
		try
		{
			rp=(TReferencePoint*)ReferencePoints->Items[i];
			if(rp && rp->XY==XY)
			{
				res=rp;
				break;
			}
		}
		catch(Exception &e) {res=NULL;}
	}
	return res;
}
//---------------------------------------------------------------------------

Types::TPoint __fastcall TImageMapsContainer::FindNearestAtXY(Types::TPoint XY)
{
	TReferencePoint *rp;
	Types::TPoint res;
	float d, d2;

	if(bmpGB)
	{
		res=Types::TPoint(bmpGB->Width << 2, bmpGB->Height << 2);
		d=sqrt(sqr(XY.x-res.x)+sqr(XY.y-res.y));
		for(int i=0; i<ReferencePoints->Count; i++)
		{
			try
			{
				rp=(TReferencePoint*)ReferencePoints->Items[i];
				if(rp)
				{
					d2=sqrt(sqr(XY.x-rp->XY.x)+sqr(XY.y-rp->XY.y));
					if(d2<d)
					{
						d=d2;
						res=rp->XY;
					}
				}
			}
			catch(Exception &e) {res=Types::TPoint(-1, -1);}
		}
	}
	else res=Types::TPoint(-1, -1);
	return res;
}
//---------------------------------------------------------------------------

void __fastcall TImageMapsContainer::DeleteReferencePointAtXY(Types::TPoint XY)
{
	TReferencePoint *RP;

	RP=FindReferencePointAtXY(XY);
	if(RP)
	{
		ReferencePoints->Delete(ReferencePoints->IndexOf(RP));
		delete RP;
		RescaleReferences();
	}
}
//---------------------------------------------------------------------------

void __fastcall TImageMapsContainer::RescaleReferences()
{
	TReferencePoint *RP;
	int i;

	for(i=0; i<ReferencePoints->Count; i++)
	{
		try
		{
			RP=(TReferencePoint*)ReferencePoints->Items[i];
			if(!RP || !RP->Valid) break;
		}
		catch(Exception &e) {}
	}
	if(i>=ReferencesPointsMaxQ && bmpGB && bmpGB->Width>0 && bmpGB->Height>0 &&
		RawPxRect && RawPxRect->Width()>0 && RawPxRect->Height()>0)
	{
		// All reference points are valid, lets rescale it !!!
		int x1, y1, x2, y2, x, y;
		double X1, Y1, X2, Y2, pw, ph, CW, CH, xc, yc;
		//TGPSCoordinate *tmp=new TGPSCoordinate();

		x2=y2=0;
		x1=bmpGB->Width;
		y1=bmpGB->Height;
//		X2=X1=Y2=Y1=0;
		try
		{
			for(i=0; i<ReferencesPointsMaxQ; i++)
			{
				RP=(TReferencePoint*)ReferencePoints->Items[i];
				if(RP && RP->Valid)
				{
					/*if(X1==0 && X2==0 && Y1==0 && Y2==0)
					{
						X1=X2=RP->C->GetX(coordinatesystem);
						Y1=Y2=RP->C->GetY(coordinatesystem);
						x1=x2=RP->XY.x;
						y1=y2=RP->XY.y;
					}
					if(X1>=RP->C->GetX(coordinatesystem))
					{
						X1=RP->C->GetX(coordinatesystem);
						x1=RP->XY.x;
					}
					if(Y1>=RP->C->GetY(coordinatesystem))
					{
						Y1=RP->C->GetY(coordinatesystem);
						y1=RP->XY.y;
					}
					if(X2<=RP->C->GetX(coordinatesystem))
					{
						X2=RP->C->GetX(coordinatesystem);
						x2=RP->XY.x;
					}
					if(Y2<=RP->C->GetY(coordinatesystem))
					{
						Y2=RP->C->GetY(coordinatesystem);
						y2=RP->XY.y;
					}*/
					if(x1>=RP->XY.x)
					{
						x1=RP->XY.x;
						X1=RP->C->GetX(coordinatesystem);
					}
					if(y1>=RP->XY.y)
					{
						y1=RP->XY.y;
						//Y1=RP->C->GetY(coordinatesystem);
						Y2=RP->C->GetY(coordinatesystem);
					}
					if(x2<=RP->XY.x)
					{
						x2=RP->XY.x;
						X2=RP->C->GetX(coordinatesystem);
					}
					if(y2<=RP->XY.y)
					{
						y2=RP->XY.y;
						//Y2=RP->C->GetY(coordinatesystem);
						Y1=RP->C->GetY(coordinatesystem);
					}
				}
			}
			//if(x2<x1) {x=x2; x2=x1; x1=x;}
			//if(y2<y1) {y=y2; y2=y1; y1=y;}
			if(X1<X2 && Y1<Y2)
			{
				pw=(double)(x2-x1);
				ph=(double)(y2-y1);
				CW=X2-X1;
				CH=Y2-Y1;
				xc=CW/(double)pw;
				yc=CH/(double)ph;
				X1-=(double)(x1+RawPxRect->Left)*xc;
				//Y1-=(double)y1*yc;
				Y1-=(double)(bmpGB->Height-(y2+RawPxRect->Top))*yc;
				X2+=(double)(bmpGB->Width-(x2+RawPxRect->Left))*xc;
				//Y2+=(double)(Background->Height-y2)*yc;
				Y2+=(double)(y1+RawPxRect->Top)*yc;
				Lower=DoublePoint(X1, Y1);//Y2);
				Upper=DoublePoint(X2, Y2);//Y1);
				Update();
				if(((TRadarMapManager*)Manager)->Settings->SaveImageMapChanges) SaveData();
				if(WrongReferenceMsg)
				{
					((TMessageObject*)WrongReferenceMsg)->SwitchOff();
					WrongReferenceMsg=NULL;
				}
			}
			else
			{
				WrongReferenceMsg=(TObject*)((TRadarMapManager*)Manager)->ShowMessageObject((TMessageObject*)WrongReferenceMsg, ((TRadarMapManager*)Manager)->MapMessages,
					TMessageType::mtWarning, mvtConstant, 0, "Reference points are wrongly placed or Map is not North-oriented", "Warning");
				Lower=DoublePoint(0,0);
				Upper=DoublePoint(0,0);
				//LatLonRect->Clear();
				Update();
			}
		}
		catch(Exception &e) {}
	}
	else
	{
		// CoordRect=NULL;
	}
}
//---------------------------------------------------------------------------

void __fastcall TImageMapsContainer::ZoomUpdate()
{
	LatLonRectUpdate();
	if(ViewportArea)
	{
		if(bmpGB)
		{
			if(radarmapsettings)
				ApplyScaling(bmpGB, Background, ((TRadarMapSettings*)radarmapsettings)->DXFBackgroundColor);
			else ApplyScaling(bmpGB, Background, clWhite32);
		}
		if(bmpUtil)	ApplyScaling(bmpUtil, UtilityBmp);
		if(bmpInf) ApplyScaling(bmpInf, InfoBmp);
	}
	if(Frame) Frame->ZoomUpdate();
}
//---------------------------------------------------------------------------

void __fastcall TImageMapsContainer::RenderMap()
{
	if(bmpGB)
	{
		if(!background) background=new TBitmap32();
		Background->BeginUpdate();
		try
		{
			//Background->SetSize(bmpGB->Width, bmpGB->Height);
			//Background->Clear(clWhite32);
			if(Visible)
			{
				//Background->Draw(0, 0, bmpGB);
				if(radarmapsettings)
					ApplyScaling(bmpGB, Background, ((TRadarMapSettings*)radarmapsettings)->DXFBackgroundColor);
				else ApplyScaling(bmpGB, Background, clWhite32);
			}
		}
		__finally
		{
			Background->EndUpdate();
		}
		UtilityUpdate();
		InfoUpdate();
	}
}
//---------------------------------------------------------------------------

void __fastcall TImageMapsContainer::LoadData(AnsiString Filename, HANDLE AStopItEvent, bool WithUpdate,
	TCoordinateSystem CS, bool Preview)
{
	AnsiString ext;
	int w, h;

	if(FileExists(Filename))
	{
		if(Preview && previewfilename!="")
		{
			ext=ExtractFileExt(previewfilename).UpperCase();
			previewfilename="";
		}
		else ext=ExtractFileExt(Filename).UpperCase();
		if(bmpGB) delete bmpGB;
		if(rawpxrect) delete rawpxrect;
		rawpxrect=NULL;
		if(ext==".PNG")
		{
			bmpGB=LoadPNG32(Filename);
		}
		else if(ext==".JPEG" || ext==".JPG" || ext==".BMP" || ext==".GIF" || ext==".TIF" || ext==".TIFF" || ext==".EMF" || ext==".WMF")
		{
			bmpGB=new TBitmap32();
			bmpGB->LoadFromFile(Filename);
		}
		else
		{
			bmpGB=NULL;
			if(Manager)
			{
				((TRadarMapManager*)Manager)->ShowMessageObject(NULL, ((TRadarMapManager*)Manager)->MapMessages,
					TMessageType::mtWarning, mvtDisapering, 5000, "Cannot open image!", "Warning");
			}
			return;
		}
		if(!Preview)
		{
			TBitmap32 *tmp;

			tmp=bmpGB;
			if(tmp->Width<MaxBitmapWidth || tmp->Height<MaxBitmapHeight)
			{
				if(tmp->Width<MaxBitmapWidth) w=MaxBitmapWidth;
				else w=tmp->Width;
				if(tmp->Height<MaxBitmapHeight) h=MaxBitmapHeight;
				else h=tmp->Height;
				bmpGB=new TBitmap32();
				bmpGB->SetSize(w, h);
				if(Manager && ((TRadarMapManager*)Manager)->Settings)
					bmpGB->Clear(((TRadarMapManager*)Manager)->Settings->DXFBackgroundColor);
				else bmpGB->Clear(clWhite32);
				bmpGB->BeginUpdate();

				rawpxrect=new Types::TRect((w-tmp->Width)>>1, (h-tmp->Height)>>1, (w+tmp->Width)>>1, (h+tmp->Height)>>1);
				bmpGB->Draw(RawPxRect->Left, RawPxRect->Top, tmp);
				bmpGB->EndUpdate();
				delete tmp;
			}
			else rawpxrect=new Types::TRect(0, 0, tmp->Width, tmp->Height);
			if(bmpUtil) bmpUtil->SetSizeFrom(bmpGB);
			if(bmpInf) bmpInf->SetSizeFrom(bmpGB);
			if(WithUpdate && !Preview)
			{
				Update();
				if(bmpGB)
				{
					if(Frame) delete Frame;
					if(ToolsOwner)
					{
						Frame=new TBitmapFrameTool(ToolsOwner, this, bmpGB, ViewportArea, RawPxRect, Manager);
					}
					else Frame=NULL;
				}
			}
			filename=Filename;
			coordinatesystem=CS;
			LatLonRect->Clear();
			LockedCS=true;
			//Load xml data
			if(FileExists(filename+".xml"))
			{
				TXMLExplorer *XMLExplorer;
				AnsiString lc, uc, str, str2, str3, str4;

				XMLExplorer=new TXMLExplorer(NULL, filename+".xml");
				try
				{
					while(XMLExplorer->GetNextStartTagInside("xrm:RadarMapImageMap", str))
					{
						if(str=="gml:Envelope")
						{
							while(XMLExplorer->GetNextStartTagInside(str, str2))
							{
								if(str2=="gml:lowerCorner")
								{
									if(XMLExplorer->GetContent(str3) && str3!="") lc=str3;
								}
								else if(str2=="gml:upperCorner")
								{
									if(XMLExplorer->GetContent(str3) && str3!="") uc=str3;
								}
							}
						}
						else if(str=="xrm:map")
						{
							AnsiString kn="", cs="", guid="", filename=""; //, fn=""
							AnsiString proj4prefix="", proj4name="", proj4ellipsoid="",
								proj4epsg="", proj4="";
							TCoordinateSystemDimension proj4unit=csdNone;
							//bool err=false;

							while(XMLExplorer->GetNextStartTagInside(str, str2))
							{
								/*if(str2=="filename")
								{
									if(XMLExplorer->GetContent(str3) && str3!="") fn=str3;
								}
								else*/ if(str2=="Klicnummer" || str2=="xrm:Klicnummer")
								{
									if(XMLExplorer->GetContent(str3) && str3!="") klicnummer=str3;
								}
								else if(str2=="CoordinateSystem" || str2=="xrm:CoordinateSystem") XMLExplorer->GetContent(cs);
								else if(str2=="csGUID" || str2=="xrm:csGUID") XMLExplorer->GetContent(guid);
								else if(str2=="csPROJ4Prefix" || str2=="xrm:csPROJ4Prefix") XMLExplorer->GetContent(proj4prefix);
								else if(str2=="csPROJ4Name" || str2=="xrm:csPROJ4Name") XMLExplorer->GetContent(proj4name);
								else if(str2=="csPROJ4Ellipsoid" || str2=="xrm:csPROJ4Ellipsoid") XMLExplorer->GetContent(proj4ellipsoid);
								else if(str2=="csPROJ4EPSG" || str2=="xrm:csPROJ4EPSG") XMLExplorer->GetContent(proj4epsg);
								else if(str2=="csPROJ4Unit" || str2=="xrm:csPROJ4Unit")
								{
									if(XMLExplorer->GetContent(str3) && str3!="")
									{
										try
										{
											proj4unit=(TCoordinateSystemDimension)StrToInt(str3);
										}
										catch(...) {proj4unit=csdNone;}
									}
								}
								else if(str2=="csPROJ4Definition" || str2=="xrm:csPROJ4Definition") XMLExplorer->GetContent(proj4);
								else if(str2=="csPlugIn" || str2=="xrm:csPlugIn") XMLExplorer->GetContent(filename);
							}
							if(cs!="")
							{
								if(cs=="csDutch") coordinatesystem=csDutch;
								else if(cs=="csBeLambert72") coordinatesystem=csBeLambert72;
								else if(cs=="csBeLambert08") coordinatesystem=csBeLambert08;
								else if(cs=="csUTM") coordinatesystem=csUTM;
								else if(cs=="csLatLon") coordinatesystem=csLatLon;
								//else if(cs=="csLKS") coordinatesystem=csLKS;
								//else if((cs.SubString(1, 4)=="dll_" || cs.SubString(1, 5)=="csDLL") && guid!="")
								else if(cs=="csPROJ4" && proj4!=NULL && proj4.Length()>0 &&
									PlugInManager && PlugInManager->GetByGUID(_csPROJ4GUID, pitCRS))
								{
									PlugInManager->SetActive(_csPROJ4GUID, pitCRS);
									if(proj4prefix!=NULL && proj4prefix.Length()>0)
										PlugInManager->ActiveCS->Prefix=proj4prefix;
									if(proj4name!=NULL && proj4name.Length()>0)
										PlugInManager->ActiveCS->Name=proj4name;
									if(proj4ellipsoid!=NULL && proj4ellipsoid.Length()>0)
										PlugInManager->ActiveCS->EllipsoidName=proj4ellipsoid;
									if(proj4epsg!=NULL && proj4epsg.Length()>0)
										PlugInManager->ActiveCS->EPSG=proj4epsg;
									if(proj4unit!=csdNone) PlugInManager->ActiveCS->Dimension=proj4unit;
									PlugInManager->ActiveCS->PROJ4=proj4;
									coordinatesystem=csDLL;
								}
								else if(guid!="")
								{
									if(PlugInManager && IsValidGUID(guid) && PlugInManager->GetByGUID(guid, pitCRS))
									{
										PlugInManager->SetActive(guid, pitCRS);
										coordinatesystem=csDLL;
									}
									else
									{
										Application->MessageBox(((UnicodeString)"The unknown Coordinate System PlugIn ("+cs+" - File: "+filename+") is used. Please install it before opening the data!").w_str(), L"RadarMap", MB_OK | MB_ICONERROR);
										((TRadarMapManager*)Manager)->NewProject();
										return;
									}
								}
								else
								{
									Application->MessageBox(((UnicodeString)"The unknown Coordinate System PlugIn ("+cs+") is used!").w_str(), L"RadarMap", MB_OK | MB_ICONERROR);
									((TRadarMapManager*)Manager)->NewProject();
									return;
								}
							}
							else coordinatesystem=((TRadarMapManager*)Manager)->Settings->DefaultCoordinateSystem;
							LowerCorner=lc;
							UpperCorner=uc;
						}
						else if(str=="xrm:mapLabel")
						{
							AnsiString tp="";
							TColor32 color=clRed32;
							AnsiString description="", attachment="", created="", modified="";
							TDouble3DPoint pos=TDouble3DPoint(-9999, -9999, -9999);
							TDoublePoint pos_px=DoublePoint(-9999, -9999);
							double confidence=0;
							unsigned int id=0;

							while(XMLExplorer->GetNextStartTagInside(str, str2))
							{
								if(str2=="type" || str2=="xrm:type")
								{
									if(XMLExplorer->GetContent(str3) && str3!="") tp=str3;
									else tp="";
								}
								else if(str2=="color" || str2=="xrm:color")
								{
									if(XMLExplorer->GetContent(str3) && str3!="")
									{
										try
										{
											color=TColor32(StrToInt(str3));
										}
										catch (Exception &e) {}
									}
								}
								else if(str2=="description" || str2=="xrm:description") XMLExplorer->GetContent(description, true);
								else if(str2=="linkedfile" || str2=="xrm:linkedfile")	XMLExplorer->GetContent(attachment, true);
								else if(str2=="created" || str2=="xrm:created")	XMLExplorer->GetContent(created);
								else if(str2=="modified" || str2=="xrm:modified")	XMLExplorer->GetContent(modified);
								else if(str2=="point" || str2=="xrm:point")
								{
									while(XMLExplorer->GetNextStartTagInside(str2, str3))
										if(str3=="gml:pos" && XMLExplorer->GetContent(str4) && str4!="") pos=TDouble3DPoint(str4);//StringToDouble3DPoint(str4);
								}
								else if(str2=="point_px" || str2=="xrm:point_px")
								{
									while(XMLExplorer->GetNextStartTagInside(str2, str3))
										if(str3=="gml:pos" && XMLExplorer->GetContent(str4) && str4!="") pos_px=DoublePoint(str4);//StringToDoublePoint(str4);
								}
								else if(str2=="confidence" || str2=="xrm:confidence")
								{
									if(XMLExplorer->GetContent(str3) && str3!="")
									{
										try
										{
											confidence=StrToFloat(str3);
										}
										catch (Exception &e) {}
									}
								}
								else if(str2=="ID" || str2=="xrm:ID")
								{
									if(XMLExplorer->GetContent(str3) && str3!="")
									{
										try
										{
											id=StrToInt(str3);
										}
										catch (Exception &e) {}
									}
								}
							}
							if(tp=="potReferenceLabel" && pos.X!=-9999 && pos.Y!=-9999 && pos.Z!=-9999 && pos_px.X!=-9999 && pos_px.Y!=-9999)
							{
								if(ToolsOwner && ToolsOwner->Image)
								{
									TImgView32* Image=ToolsOwner->Image;
									Types::TRect vpt=Image->GetViewportRect();
									TReferncePointLabel* rpl;
									TGPSCoordinate *c;

									rpl=new TReferncePointLabel(ToolsOwner, vpt, LatLonRect, this,
										((TRadarMapManager*)Manager)->Settings->ReferencePointCoordinatesQ, Types::TPoint(pos_px.X, pos_px.Y), Manager);
									rpl->StopReferencesCollection();
									c=new TGPSCoordinate();
									/*if(coordinatesystem==csLatLon)
									{
										c->Latitude=pos.X;
										c->Longitude=pos.Y;
										c->Z_Geoid=pos.Z;
									}
									else
									{
										c->SetX(coordinatesystem, pos.X);
										c->SetY(coordinatesystem, pos.Y);
										c->SetH(coordinatesystem, pos.Z);
									}*/
									if(coordinatesystem==csLatLon) c->SetLatLonAlt(pos.X, pos.Y, pos.Z);
									else c->SetXYH(coordinatesystem, pos.X, pos.Y, pos.Z);
									c->Confidence=confidence;
									rpl->GPSConfidence=confidence;
									rpl->Coordinate=c;
									rpl->ID=id;
									rpl->Color=color;
								}
							}
						}
					}
				}
				__finally
				{
					delete XMLExplorer;
				}
			}
		}
		else if(bmpGB)
		{
			if(!background) background=new TBitmap32();
			background->SetSize(bmpGB->Width, bmpGB->Height);
			Background->BeginUpdate();
			Background->Draw(0, 0, bmpGB);
			Background->EndUpdate();
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TImageMapsContainer::SaveData()
{
	TStrings* Strs;
	TReferncePointLabel *rpl;
	TReferencePoint* RP;
	AnsiString nfn, fn, Dir, str;

	Strs=new TStringList();
	Strs->Add("<?xml version='1.0' encoding='UTF-8'?>");
	Strs->Add("<xrm:RadarMapImageMap xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:gml=\"http://www.opengis.net/gml\" xmlns:fme=\"http://www.safe.com/gml/fme\" xsi:schemaLocation=\"xmlRadarMap http://www.radarmap.eu/gml/2_5_0/radarmap.xsd\" xmlns:xrm=\"xmlRadarMap\">");
	try
	{
		TGPSCoordinate *c1, *c2, *c;
		TMapsContainer *mc;

		c1=LatLonRect->LeftTop;
		c2=LatLonRect->RightBottom;
		if(!c1->AutomaticUpdate)
		{
			c1->AutomaticUpdate=true;
			c2->AutomaticUpdate=true;
			c1->AutomaticUpdate=false;
			c2->AutomaticUpdate=false;
		}
		Strs->Add("  <gml:boundedBy>");
		Strs->Add("    <gml:Envelope>");
		if(CoordinateSystem==csLatLon || CoordinateSystem==csUTM)
		{
			Strs->Add("      <gml:lowerCorner>"+FloatToStrF(c2->Latitude, ffFixed, 11, 6)+" "+
				FloatToStrF(c1->Longitude, ffFixed, 11, 6)+"</gml:lowerCorner>");
			Strs->Add("      <gml:upperCorner>"+FloatToStrF(c1->Latitude, ffFixed, 11, 6)+" "+
				FloatToStrF(c2->Longitude, ffFixed, 11, 6)+"</gml:upperCorner>");
		}
		else
		{
			Strs->Add("      <gml:lowerCorner>"+FloatToStrF(c1->GetX(CoordinateSystem), ffFixed, 10, 3)+" "+
				FloatToStrF(c2->GetY(CoordinateSystem), ffFixed, 10, 3)+"</gml:lowerCorner>");
			Strs->Add("      <gml:upperCorner>"+FloatToStrF(c2->GetX(CoordinateSystem), ffFixed, 10, 3)+" "+
				FloatToStrF(c1->GetY(CoordinateSystem), ffFixed, 10, 3)+"</gml:upperCorner>");
		}
		Strs->Add("    </gml:Envelope>");
		Strs->Add("  </gml:boundedBy>");
		Strs->Add("  <gml:featureMember>");
		Strs->Add("    <xrm:map>");
		Strs->Add("      <xrm:filename>"+filename+"</xrm:filename>");
		Strs->Add("      <xrm:Klicnummer>"+Klicnummer+"</xrm:Klicnummer>");
		Strs->Add("      <xrm:CoordinateSystem>"+c1->GetCSCodeName(coordinatesystem)+"</xrm:CoordinateSystem>");
		str=c1->GetCSGUID(coordinatesystem);
		if(IsValidGUID(str))
		{
			Strs->Add("      <xrm:csGUID>"+str+"</xrm:csGUID>");
			if(coordinatesystem==csDLL && PlugInManager)
			{
				TPlugInObject *plugin;

				plugin=PlugInManager->GetByGUID(str, pitCRS);
				if(plugin) Strs->Add("      <xrm:csPlugIn>"+ExtractFileName(plugin->DLLFileName)+", "+plugin->PlugInID+"</xrm:csPlugIn>");
				if(plugin) Strs->Add("      <xrm:csPlugInName>"+c1->GetCSName(coordinatesystem)+"</xrm:csPlugInName>");
			}
		}
		str=c1->GetPROJ4(coordinatesystem);
		if(str!=NULL && str.Length()>0)
		{
			Strs->Add("      <xrm:csPROJ4>");
			Strs->Add("        <xrm:csPROJ4Prefix>"+c1->GetCSPrefix(coordinatesystem)+"</xrm:csPROJ4Prefix>");
			Strs->Add("        <xrm:csPROJ4Name>"+c1->GetCSName(coordinatesystem)+"</xrm:csPROJ4Name>");
			Strs->Add("        <xrm:csPROJ4Ellipsoid>"+c1->GetCSEllipsoid(coordinatesystem)+"</xrm:csPROJ4Ellipsoid>");
			Strs->Add("        <xrm:csPROJ4EPSG>"+c1->GetCSURI(coordinatesystem)+"</xrm:csPROJ4EPSG>");
			Strs->Add("        <xrm:csPROJ4Unit>"+IntToStr((int)c1->GetCSDimension(coordinatesystem))+"</xrm:csPROJ4Unit>");
			Strs->Add("        <xrm:csPROJ4Definition>"+str+"</xrm:csPROJ4Definition>");
			Strs->Add("      </xrm:csPROJ4>");
		}
		Strs->Add("    </xrm:map>");
		Strs->Add("  </gml:featureMember>");
		try
		{
			for(int i=0; i<ReferencesPointsMaxQ; i++)
			{
				RP=(TReferencePoint*)ReferencePoints->Items[i];
				if(RP && RP->Valid)
				{
					rpl=(TReferncePointLabel*)RP->LabelObject;
					if(rpl)
					{
						Strs->Add("  <gml:featureMember>");
						Strs->Add("    <xrm:mapLabel>");
						try
						{
							Strs->Add("      <xrm:type>potReferenceLabel</xrm:type>");
							Strs->Add("      <xrm:color>"+IntToStr((int)rpl->Color)+"</xrm:color>");
							c=RP->C;
							if(coordinatesystem==csLatLon || coordinatesystem==csUTM)
								Strs->Add("      <xrm:point><gml:pos>"+FloatToStrF(c->Latitude, ffFixed, 11, 6)+" "+
									FloatToStrF(c->Longitude, ffFixed, 11, 6)+" "+
									FloatToStrF(c->Z_Geoid, ffFixed, 10, 2)+"</gml:pos></xrm:point>");
							else Strs->Add("      <xrm:point><gml:pos>"+FloatToStrF(c->GetX(coordinatesystem), ffFixed, 10, 3)+" "+
									FloatToStrF(c->GetY(coordinatesystem), ffFixed, 10, 3)+" "+
									FloatToStrF(c->GetH(coordinatesystem), ffFixed, 10, 2)+"</gml:pos></xrm:point>");
							Strs->Add("      <xrm:point_px><gml:pos>"+IntToStr((__int64)RP->XY.x)+" "+IntToStr((__int64)RP->XY.y)+"</gml:pos></xrm:point_px>");
							if(rpl->Description!="") Strs->Add("      <xrm:description>"+rpl->Description+"</xrm:description>");
							if(rpl->Attachment!="")
							{
								fn=rpl->Attachment;
								if(((TRadarMapManager*)Manager)->Settings->CopyAttachments)
								{
									Dir=ExtractFilePath(filename);
									if(Dir!=ExtractFilePath(fn))
									{
										try
										{
											nfn=Dir+ExtractFileName(fn);
											CopyFile(fn.c_str(), nfn.c_str(), false);
											fn=nfn;
										}
										catch(Exception &e) {}
									}
								}
								Strs->Add("      <xrm:linkedfile>"+fn+"</xrm:linkedfile>");
							}
							Strs->Add("      <xrm:created>"+MyDateTimeToStr(rpl->CreatedTime)+"</xrm:created>");
							Strs->Add("      <xrm:modified>"+MyDateTimeToStr(rpl->ModifiedTime)+"</xrm:modified>");
							Strs->Add("      <xrm:confidence>"+FloatToStrF(rpl->GPSConfidence, ffFixed, 10, 3)+"</xrm:confidence>");
							Strs->Add("      <xrm:ID>"+IntToStr((int)rpl->ID)+"</xrm:ID>");
						}
						__finally
						{
							Strs->Add("    </xrm:mapLabel>");
							Strs->Add("  </gml:featureMember>");
						}
					}
				}
			}
		}
		catch(Exception &e) {}
	}
	__finally
	{
		Strs->Add("</xrm:RadarMapImageMap>");
		Dir=ExtractFilePath(filename);
		nfn=Dir+ExtractFileName(filename)+".xml";
		Strs->SaveToFile(nfn);
		delete Strs;
	}
}
//---------------------------------------------------------------------------

void __fastcall TImageMapsContainer::GetLayers(TCheckBoxTreeView *TreeView, TTreeNode *Root)
{
	TTreeNode *node;
	TDXFLayer *layer;
	int i, j, k;

	if(TreeView==NULL) return;

	if(Root==NULL)
	{
		TreeView->Items->Clear();
		Root=TreeView->Items->Add(NULL, Klicnummer);
		TreeView->SetChecking(Root, Visible);
	}
	Root->Data=(void*)((TMapsContainer*)this);
}
//---------------------------------------------------------------------------

void __fastcall TImageMapsContainer::SetLayers(TCheckBoxTreeView *TreeView, TTreeNode *Root)
{
	int i;
	TTreeNode *node;
	TMapsContainer *mc;

	if(TreeView==NULL) return;

	AutomaticUpdate=false;
	try
	{
		try
		{
			if(!Root) Root=TreeView->Items->GetFirstNode();
			if(Root)
			{
				mc=(TMapsContainer*)Root->Data;
				if(mc->MapType==mtAdjustable && Root->Text==Klicnummer)
				{
					Visible=TreeView->Checked(Root);
				}
			}
		}
		catch(Exception &e) {}
	}
	__finally
	{
		AutomaticUpdate=true;
		Update();
	}
}
