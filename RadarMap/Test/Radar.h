//---------------------------------------------------------------------------

#ifndef RadarH
#define RadarH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <GR32_Image.hpp>
#include <ExtCtrls.hpp>
#include "Profile.h"
#include <IdBaseComponent.hpp>
#include <IdComponent.hpp>
#include <IdTCPClient.hpp>
#include <IdTCPConnection.hpp>
#include "Receiver.h"
#include "DrawProf.h"
#include "Scale.h"
#include <stdio.h>
#include "LocalMap.h"
#include "Manager.h"
#include <G32_ProgressBar.hpp>

#define MySamples 512 //Number of Samples which have to be stored in TTrace
#define RxSamples 544 //Trace Samples + TraceMark Samples
#define BitCount 8

enum ProcessStatus {Initialized=0, Stopped=1, Running=2, Paused=3};
//---------------------------------------------------------------------------
class TAutoStopProfile;

class TRadarDialog : public TForm
{
__published:	// IDE-managed Components
		TPanel *ToolbarPanel;
		TPanel *TracePanel;
		TImage32 *TraceImage;
		TPanel *Panel2;
		TPanel *Panel3;
	TStaticText *BatteryST;
	TPanel *PalettePanel;
	TImage32 *PaletteImage;
	TStaticText *StaticText1;
	TImgView32 *ProfImage;
		void __fastcall FormKeyDown(TObject *Sender, WORD &Key, TShiftState Shift);
		void __fastcall FormActivate(TObject *Sender);
		void __fastcall FormResize(TObject *Sender);
		void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
		void __fastcall FormCanResize(TObject *Sender, int &NewWidth,
		  int &NewHeight, bool &Resize);
		void __fastcall FormShow(TObject *Sender);
	void __fastcall FormCloseQuery(TObject *Sender, bool &CanClose);
private:	// User declarations
	bool EN[8];
	void SetButtonsState(void);
	THyperbolaTool *hyperbola;
	TZeroPointTool *zeroline;
	TAutoStopProfile *AutoStop;
	void __fastcall StartWPSasWheelParser(AnsiString Reply);
public:		// User declarations
		TRadarMapManager* RadarMapManager;
		ProcessStatus Process;
		Types::TRect DataRect;
		//HANDLE EventDrawProfHndl;
		TReceive *RecThrd;
		TDrawProfThrd *DrawProfThrd;
		Types::TRect *ProfRect[_MaxProfQuan], CopyRect[_MaxProfQuan];
		TScale *ScaleL[_MaxProfQuan], *ScaleB[_MaxProfQuan];
		TColor32 *bgColor, bgColorTemp;
		int DPC;
		bool SoundsEnable;
		void StartPressed(void);
		void StopPressed(void);
		void MarkPressed(void);
		void ExitPressed(void);
		void BottomScaleRefresh(int Index=-1);
		void PlusPressed(void);
		void MinusPressed(void);

		void __fastcall ProfChannelRefresh(int Index, bool ZeroBottomScale, bool WithUpdate);
		void __fastcall RebuildAndRefresh(bool ZeroBottomScale);
		void __fastcall ProfImageRefresh(bool ZeroBottomScale);
		void __fastcall RebuildRectsScales();

		__fastcall TRadarDialog(TComponent* Owner, TRadarMapManager* RMM);
		__fastcall ~TRadarDialog();

		void __fastcall Initialize(void);

		int SetZeroPoint(TTrace* Ptr, int Smpls);
		bool DrawWhileWheelIsStopped;
		float Permit;
		AnsiString IPAddress;
		int MyLeft, MyTop, MyWidth, MyHeight;
		AnsiString WheelStr;
		float BatteryVoltage;
		float OldDelay;
		int Delay, DelayWin, DelayLevel;
		bool AutoDelay;
		int RightTraceX[_MaxProfQuan];
		int OldProfilesCount;

		TSliderToolbar *RadarToolbar, *LabelsToolbar;
		TSliderToolButton *StartBtn, *StopBtn, *PlusBtn, *MinusBtn, *ExitBtn, *BatteryBtn, *BatteryBtn2;
		TSliderToolButton *STB[8];
		TSliderToolbarChild *RgbFiguresBtn, *WaterBtn, *ElectroBtn, *GasLowBtn, *GasHighBtn, *SewageBtn, *TelecomBtn, *FiberBtn;
		TSliderToolButton *WaterCircBtn, *WaterRectBtn, *WaterTriaBtn,
						  *ElectroCircBtn, *ElectroRectBtn, *ElectroTriaBtn,
						  *GasLowCircBtn, *GasLowRectBtn, *GasLowTriaBtn,
						  *GasHighCircBtn, *GasHighRectBtn, *GasHighTriaBtn,
						  *SewageCircBtn,*SewageRectBtn, *SewageTriaBtn,
						  *TelecomCircBtn, *TelecomRectBtn, *TelecomTriaBtn,
						  *FiberCircBtn, *FiberRectBtn, *FiberTriaBtn;
		TSliderToolButton *CircRedBtn, *CircGreenBtn, *CircBlueBtn,
						  *RectRedBtn, *RectGreenBtn, *RectBlueBtn,
						  *TriaRedBtn, *TriaGreenBtn, *TriaBlueBtn;
		TSliderToolButton *LabelBtn, *HyperbolaBtn, *ZeroBtn;
		TSliderToolbarChild *ToolsToolbar;
		void __fastcall StartBtnPressed(System::TObject* Sender, int X, int Y);
		void __fastcall StopBtnPressed(System::TObject* Sender, int X, int Y);
		void __fastcall PlusBtnPressed(System::TObject* Sender, int X, int Y);
		void __fastcall MinusBtnPressed(System::TObject* Sender, int X, int Y);
		void __fastcall ExitBtnPressed(System::TObject* Sender, int X, int Y);
		void __fastcall LabelBtnPressed(System::TObject* Sender, int X, int Y);
		void __fastcall LaptopBatteryPressed(System::TObject* Sender, int X, int Y);
        void __fastcall SwitchOffLaptopBatteryMsg();
		void __fastcall BatteryPressed(System::TObject* Sender, int X, int Y);
#ifndef _InfraRadarOnly
		TSliderToolButton *SetupBtn, *MarkBtn, *PauseBtn;
		void __fastcall SetupBtnClick(System::TObject* Sender, int X, int Y);
		void __fastcall MarkBtnPressed(System::TObject* Sender, int X, int Y);
		void __fastcall PauseBtnPressed(System::TObject* Sender, int X, int Y);
		void __fastcall PausePressed(void);
#endif
		void __fastcall HyperbolaBtnPressed(System::TObject* Sender, int X, int Y);
		void __fastcall ZeroBtnPressed(System::TObject* Sender, int X, int Y);
		void __fastcall MouseClick(System::TObject* Sender, int X, int Y);
};
//---------------------------------------------------------------------------

class TAutoStopProfile : public TMyThread
{
private:
	TRadarMapManager *Manager;
	TRadarDialog *RadarDlg;
protected:
	void __fastcall ExecuteLoopBody();
public:
	__fastcall TAutoStopProfile(bool CreateSuspended, TRadarMapManager *AManager, TRadarDialog *ARadarDlg) : TMyThread(CreateSuspended) {Manager=AManager; RadarDlg=ARadarDlg;}
	__fastcall ~TAutoStopProfile() {}
};
//---------------------------------------------------------------------------
extern PACKAGE TRadarDialog *RadarDialog;
//---------------------------------------------------------------------------
#endif
