//---------------------------------------------------------------------------

#ifndef BitmapGDIH
#define BitmapGDIH
//---------------------------------------------------------------------------
#include <GR32.hpp>
#include <gdiplus.h>
#include "Defines.h"

void ScaleBitmap32(TBitmap32* Src, float ScaleX, float ScaleY, bool AsIs);
void ScaleBitmap32(TBitmap32* Src, TBitmap32* Dst, float ScaleX, float ScaleY, bool AsIs);
void ScaleBitmap32(TBitmap32* Src, TBitmap32* Dst, TDoubleRect *viewport, Types::TRect OutRect, bool AsIs);
void ScaleBitmap32(TBitmap32* Src, TBitmap32* Dst, TLockedDoubleRect *viewport, Types::TRect OutRect, bool AsIs);
void RotateBitmap32(TBitmap32* Src, double AngleRad, bool AsIs);
void RotateInDegBitmap32(TBitmap32* Src, double Degs, bool AsIs);
void RotateBitmap32(TBitmap32* Src, TBitmap32* Dst, double AngleRad, bool AsIs);
void RotateInDegBitmap32(TBitmap32* Src, TBitmap32* Dst, double Degs, Types::TPoint cP, bool AsIs);
void ApplyColorMask(TBitmap32* Bmp, TColor32 color, TDrawMode DrawMode=dmBlend);
void ApplyColorMask(TBitmap32* Bmp, TColor32 color, TColor32 &text_color, TDrawMode DrawMode=dmBlend);


#endif
