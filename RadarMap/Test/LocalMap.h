//---------------------------------------------------------------------------

#ifndef LocalMapH
#define LocalMapH
//---------------------------------------------------------------------------

//#include "Drawing.h"
#include <GR32.hpp>
#include "SliderTools.h"
#include "ImageMap.h"
#include "VectorMap.h"
#include "Spar.h"
#include "Defines.h"
#include "VisualToolObjects.h"
#include "GetGPSData.h"
#include "MessageObjects.h"
//#include "GainForm.h"

//---------------------------------------------------------------------------
#define CoordinateBoxHalfSize   5
#define CoordinateBoxColor      clLime32
#define MinimalDegreeAccuracy   0.000005
#define MinimalPixelAccuracy    50

//---------------------------------------------------------------------------

typedef bool __fastcall (__closure *TMyMouseMoveEvent)(System::TObject* Sender, int X, int Y, TCustomLayer *Layer);

typedef bool __fastcall (__closure *TMyMouseButtonEvent)(System::TObject* Sender, TMouseButton Button, int X, int Y, TCustomLayer *Layer);

enum TMapLabelType {mltNone=0, mltMapLabel=1, mltPinLabel=2, mltColorMaskLabel=3,
	mltColorPredictedLabel=4, mltReferncePointLabel=5, mltStartNGoLabel=6,
	mltStartStopWaypointLabel=7};
const AnsiString TMapLabelTypeStrings[]=
{
	"mltNone",
	"mltMapLabel",
	"mltPinLabel",
	"mltColorMaskLabel",
	"mltColorPredictedLabel",
	"mltReferncePointLabel",
	"mltStartNGoLabel",
	"mltStartStopWaypointLabel"
};

TMapLabelType StrToTMapLabelType(AnsiString str);


class THandObject: public TToolObject
{
private:
	bool locked;
	TLockedDoubleRect *viewportarea;
public:
	__fastcall THandObject(TMyObject *AOwner, TImgView32 *ImgView, Types::TRect Rect, TLockedDoubleRect *AViewportArea, TObject *AManager)
		: TToolObject(AOwner, ImgView, Rect, AManager) {viewportarea=AViewportArea; locked=false; forcemouseable=true;}
	__fastcall ~THandObject() {}

	void __fastcall MouseMove(System::TObject* Sender, Classes::TShiftState Shift, int X, int Y, TCustomLayer *Layer);
};

const float MaxZoomLevel=20.0;
const float DoubleZoomLevel=2.0;
const float QuartZoomLevel=0.25;
const float NextZoomLevel=0.05;
const float LastZoomLevel=0.01;

class TZoomObject: public TToolObject
{
private:
	Types::TRect SelectedArea;
	//In times:
	float zoomstep, MaxZoom;
	float currentzoom;
	TGraphicObjectsList *Tools;
	TMapScale *MapScale;
	TLockedDoubleRect *viewportarea;
protected:
	void __fastcall ChangeGradientZoomStep(float lastzoom, float value);
	void __fastcall SetCurrentZoom(float value);
	Types::TRect __fastcall readClientRect() {if(image!=NULL) return image->GetViewportRect(); else return Types::TRect(0,0,0,0);}
public:
	__fastcall TZoomObject(TMyObject *AOwner, TImgView32 *ImgView, Types::TRect Rect, TLockedDoubleRect *AViewportArea,
		TGraphicObjectsList *ATools, TObject *AManager, TMapScale *AMapScale=NULL);
	__fastcall ~TZoomObject() {}

	void __fastcall ApplyZoom(bool WithUpdate=true);
	void __fastcall ApplyZoomAtXY(int X, int Y, bool WithUpdate=true);
	void __fastcall ActualZoom(bool WithUpdate=true);
	void __fastcall ApplyCurrentZoom(bool WithUpdate=true);
	void __fastcall ZoomIn();
	void __fastcall ZoomOut();
	void __fastcall ZoomFastOut();
	void __fastcall ZoomFastIn();

	void __fastcall Clear() {currentzoom=1.0; if(viewportarea) viewportarea->Set(0, 0, 1, 1); if(MapScale) MapScale->ZoomText=IntToStr((int)(currentzoom*100.))+"%";}

	void __fastcall SetCurrentZoomEx(float value);

	void __fastcall Redraw(TObject *Sender, TBitmap32 *Bmp);
	void __fastcall MouseMove(System::TObject* Sender,
		  Classes::TShiftState Shift, int X, int Y, TCustomLayer *Layer);
	void __fastcall MouseUp(TObject *Sender,
		  TMouseButton Button, TShiftState Shift, int X, int Y,
		  TCustomLayer *Layer);
	void __fastcall ZoomUpdateTools() {if(Tools) Tools->ZoomUpdate();}
	//void __fastcall MouseClick(System::TObject* Sender, int X, int Y);

	__property float CurrentZoom = {read=currentzoom, write=SetCurrentZoom}; //in times
	__property float ZoomStep = {read=zoomstep};
	__property TLockedDoubleRect *ViewportArea = {read=viewportarea};
};

class TMapLabel;

class TMapObjectsContainer: public TGraphicObjectsContainer
{
private:
	TMapsContainersCollection *container;
	TMapScale* mapscale;
	TWindRose* windrose;
	TToolClock* clocks;
	TZoomObject *zoom;
	THandObject *hand;
	TMessagesScrollObject *messages;
	TVoidFunction dbe;
protected:
public:
	__fastcall TMapObjectsContainer(TImgView32 *ImgView, bool UseEvents,
		TMouseAction *ma, TVoidFunction DBE, void* RMS, bool ReducedObjects=false);
	__fastcall ~TMapObjectsContainer(void);

	void __fastcall MouseMove(System::TObject* Sender, Classes::TShiftState Shift, int X, int Y, TCustomLayer *Layer);    //Hand ????
	void __fastcall MouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y, TCustomLayer *Layer);
	void __fastcall MouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y, TCustomLayer *Layer);
	void __fastcall RenderMaps() {container->RenderMaps(); container->Update();}
	void __fastcall OnScroll(TObject *Sender) {if(zoom) zoom->ZoomUpdateTools(); TGraphicObjectsContainer::OnScroll(Sender);}

	int __fastcall TMapObjectsContainer::GetMapLabels(TList *list, TMapLabelType mlt=mltNone,
		TProfileLabelType plt=pltNone);
	TMapLabel* __fastcall GetMapLabel(unsigned int aID);

	__property TMapsContainersCollection *Container = {read=container};
	__property TZoomObject *Zoom = {read=zoom};
	__property TMapScale *MapScale = {read=mapscale};
	__property TVoidFunction OnDisableButton = {read=dbe};
	__property TMessagesScrollObject *Messages = {read=messages};
	__property THandObject *Hand = {read=hand};
};

class TMapCustomPathObject: public TProfileObject
{
private:
protected:
	//TList *List;
	TGPSCoordinatesList *List;
	TGPSCoordinatesRect *LatLonRect, *coordinatesrect;
	TGPSCoordinate* LastAdded;
	TColor32 color;
	int __fastcall readCount() {return List->Count;}
	virtual void __fastcall writeColor(TColor32 value) {color=value; Update();}
	virtual TGPSCoordinate* __fastcall readPoints(int Index) {if(Index>=0 && Index<List->Count) return (TGPSCoordinate*)List->Items[Index]; else return NULL;}
	virtual TGPSCoordinate* __fastcall readFirstPoint(void) {if(List->Count>0) return (TGPSCoordinate*)List->Items[0]; else return NULL;}
	virtual TGPSCoordinate* __fastcall readLastPoint(void) {if(List->Count>0) return (TGPSCoordinate*)List->Items[List->Count-1]; else return NULL;}
public:
	__fastcall TMapCustomPathObject(TMyObject *AOwner, TProfileOutputView *OV, TGPSCoordinatesRect *GCR, TObject* RadarMapManager);
	__fastcall ~TMapCustomPathObject();

	virtual void __fastcall MouseClick(System::TObject* Sender, int X, int Y) {}
	virtual void __fastcall Redraw(TObject *Sender, TBitmap32 *Bmp) {}
	virtual void __fastcall MouseMove(System::TObject* Sender,
		  Classes::TShiftState Shift, int X, int Y, TCustomLayer *Layer) {}
	virtual void __fastcall MouseUp(TObject *Sender,
		  TMouseButton Button, TShiftState Shift, int X, int Y,
		  TCustomLayer *Layer) {}
	virtual void __fastcall MouseDblClick(System::TObject* Sender, int X, int Y) {}

	virtual void __fastcall AddP(TGPSCoordinate *c);
	virtual TGPSCoordinate* __fastcall AddP(double ln, double lt, double Z, double Z_geoid, double conf);

	void __fastcall Insert(int Index, TGPSCoordinate *c);
	TGPSCoordinate* __fastcall Insert(int Index, double ln, double lt, double Z, double Z_geoid, double conf=0);
	TGPSCoordinate* __fastcall FindNearestPointAtXY(int x, int y) {return List->FindNearestPointAtXY(x, y);}
	TGPSCoordinate* __fastcall FindNearestPointAtXY(int x, int y, int &distance) {return List->FindNearestPointAtXY(x, y, distance);}
	TGPSCoordinate* __fastcall FindPointInRangeAtXY(int x, int y, int range) {return List->FindPointInRangeAtXY(x, y, range);}
	TGPSCoordinate* __fastcall FindNearestPointAtLatLon(double lat, double lon) {return List->FindNearestPointAtLatLon(lat, lon);}
	//void __fastcall Clear() {for(int i=0; i<List->Count; i++) delete (TGPSCoordinate*)List->Items[i]; List->Clear();}
	bool __fastcall IsInClientRect(Types::TPoint value);
	bool __fastcall IsInClientRect(Types::TRect value);

	__property int Count = {read=readCount};
	__property TGPSCoordinate* Points[int Index] = {read=readPoints};
	__property TColor32 Color = {read=color, write=writeColor};
	__property TGPSCoordinatesRect* CoordinatesRect = {read=coordinatesrect};
	__property TGPSCoordinate* FirstPoint = {read=readFirstPoint};
	__property TGPSCoordinate* LastPoint = {read=readLastPoint};
};

#define _ShowGainButtonOnPath
#undef _ShowGainButtonOnPath

class TMapPathObject: public TMapCustomPathObject
{
private:
	AnsiString name, additionalname;
	TGPSCoordinate* SelectedPoint;
	TColor32 selectioncolor;
	bool chainedcolors;
	int MaxAddedTraceIndex;
	int linewidth;
	int PressedIndex, old_X, old_Y;
	TObject *Satellite;
#ifdef _ShowGainButtonOnPath
	TSliderToolButton* GainButton;
#endif
	void __fastcall DrawPath(TBitmap32 *Bmp, TColor32 Color, int Width, bool Shadow);
	void __fastcall DrawLine32(TBitmap32 *Bmp, TColor32 Color, TColor32 Border,
		int Width, bool Shadow, Gr32::TFixedPoint *LineFP, int &linefp_cnt,
		bool InOutputView);
	void __fastcall DrawSegment32(TBitmap32 *Bmp, TColor32 Color, TColor32 Border,
		int Width, bool Shadow, Gr32::TFixedPoint *LineFP, int &linefp_cnt,
		bool InOutputView, Types::TPoint* EndPoints, int j);
	void __fastcall DrawArrow32(TBitmap32 *Bmp, TColor32 Color,
		TColor32 Border, int Width, bool Shadow, bool InOutputView,
		Types::TPoint* EndPoints, int j, bool DrawName=false);
	//void __fastcall DrawPolyLine(TBitmap32 *Bmp, TColor32 Color, TColor32 Border,
	//	int Width, bool Shadow, Gr32::TFixedPoint *LineFP, int linefp_cnt);
protected:
	void __fastcall writeSelectionColor(TColor32 value);
    void __fastcall writeColor(TColor32 value);
	void __fastcall writeChainedColors(bool value);
	void __fastcall writeLineWidth(int value) {if(linewidth!=value) {linewidth=value; Update();}}
	void __fastcall writeAdditionalName(AnsiString value) {if(additionalname!=value) {additionalname=value; Update();}}
	AnsiString __fastcall readFullName();// {if(outputview && outputview->Profile) return outputview->Profile->Name; else return "";}
	void __fastcall GainClick(System::TObject* Sender, int X, int Y);
public:
	__fastcall TMapPathObject(TMyObject *AOwner, TProfileOutputView *OV, TGPSCoordinatesRect *GCR, TObject* RadarMapManager, TObject *ASatellite);
	__fastcall ~TMapPathObject();

	void __fastcall AddP(TGPSCoordinate *c) {TMapCustomPathObject::AddP(c);}
	TGPSCoordinate* __fastcall AddP(double ln, double lt, double Z, double Z_geoid, double conf) {return TMapCustomPathObject::AddP(ln, lt, Z, Z_geoid, conf);}
	TGPSCoordinate* __fastcall AddP(double ln, double lt, double Z, double Z_geoid, TTrace *Ptr, bool &NotAddedButInsertedOrChanged, double conf);
	void __fastcall MouseClick(System::TObject* Sender, int X, int Y);
	void __fastcall Redraw(TObject *Sender, TBitmap32 *Bmp);
	void __fastcall MouseMove(System::TObject* Sender,
		  Classes::TShiftState Shift, int X, int Y, TCustomLayer *Layer);
	void __fastcall MouseUp(TObject *Sender,
		  TMouseButton Button, TShiftState Shift, int X, int Y,
		  TCustomLayer *Layer);
	void __fastcall MouseDblClick(System::TObject* Sender, int X, int Y);

	__property TColor32 SelectionColor = {read=selectioncolor, write=writeSelectionColor};
	__property bool ChainedColors = {read=chainedcolors, write=writeChainedColors};
	__property int LineWidth = {read=linewidth, write=writeLineWidth};
	__property AnsiString Name = {read=name, write=name};
	__property AnsiString FullName = {read=readFullName};
	__property AnsiString AdditionalName = {read=additionalname, write=writeAdditionalName};
};

class TStartStopWaypointLabel;

enum TMapTrackPathState {mtpsNormal = 1, mtpsFollowMe = 2, mtpsPressed = 4, mtpsNoGPS = 8};

class TMapTrackPathObject: public TMapCustomPathObject
{
private:
	void __fastcall ListGarbageEater(void);
	TBitmap32 *PointGlyph, *PressedPointGlyph, *FollowMeGlyph, *NoGpsGlyph;
	AnsiString pointglyphname, pressedpointglyphname, followmeglyphname, nogpsglyphname;
	TGPSCoordinate *ZeroPoint;
	Types::TRect PointRect;
	int dX, dY, ShiftX, ShiftY;
	double direction;
	TTimerThread *timer;
	//bool validazimuth;
	//double azimuth;
	TMapTrackPathState mtpState;
	TGpsUnit* gpsunit;
	TStartStopWaypointLabel *startwaypoint, *stopwaypoint;
	void __fastcall ApplyFollowMe();
protected:
	int PointsMax;
	HANDLE ListInEditingEvent;
	void __fastcall OnIdle() {mtpState=(TMapTrackPathState)(mtpState | mtpsNoGPS); timer->Stop(); Update();}
	TGPSCoordinate* __fastcall readPoints(int Index);
	TGPSCoordinate* __fastcall readFirstPoint(void);
	TGPSCoordinate* __fastcall readLastPoint(void);
	bool __fastcall readFollowMe() {return (mtpState & mtpsFollowMe);}
	void __fastcall writeFollowMe(bool value);
	void __fastcall writeGpsUnit(TGpsUnit* value);
	bool __fastcall readMouseable();
	void __fastcall writePressed(bool value);
	void __fastcall writeStartWayPoint(TStartStopWaypointLabel *value);
	void __fastcall writeStopWayPoint(TStartStopWaypointLabel *value);
	void __fastcall writePointGlyphName(AnsiString value) {if(pointglyphname!=value) {if(pointglyphname!="") {RemovePngFromContainer(pointglyphname); PointGlyph=NULL;} if(value!="") PointGlyph=GetPngFromContainer(value); pointglyphname=value;}}
	void __fastcall writePressedPointGlyphName(AnsiString value) {if(pressedpointglyphname!=value) {if(pressedpointglyphname!="") {RemovePngFromContainer(pressedpointglyphname); PressedPointGlyph=NULL;} if(value!="") PressedPointGlyph=GetPngFromContainer(value); pressedpointglyphname=value;}}
	void __fastcall writeFollowMeGlyphName(AnsiString value) {if(followmeglyphname!=value) {if(followmeglyphname!="") {RemovePngFromContainer(followmeglyphname); FollowMeGlyph=NULL;} if(value!="") FollowMeGlyph=GetPngFromContainer(value); followmeglyphname=value;}}
	void __fastcall writeNoGpsGlyphName(AnsiString value) {if(nogpsglyphname!=value) {if(nogpsglyphname!="") {RemovePngFromContainer(nogpsglyphname); NoGpsGlyph=NULL;} if(value!="") NoGpsGlyph=GetPngFromContainer(value); nogpsglyphname=value;}}
	__property AnsiString PointGlyphName = {read=pointglyphname, write=writePointGlyphName};
	__property AnsiString PressedPointGlyphName = {read=pressedpointglyphname, write=writePressedPointGlyphName};
	__property AnsiString FollowMeGlyphName = {read=followmeglyphname, write=writeFollowMeGlyphName};
	__property AnsiString NoGpsGlyphName = {read=nogpsglyphname, write=writeNoGpsGlyphName};
public:
	__fastcall TMapTrackPathObject(TMyObject *AOwner, TGPSCoordinatesRect *GCR, TObject* RadarMapManager);
	__fastcall ~TMapTrackPathObject();

	virtual void __fastcall Redraw(TObject *Sender, TBitmap32 *Bmp);

    void __fastcall MouseDown(TObject *Sender,
		  TMouseButton Button, TShiftState Shift, int X, int Y, TCustomLayer *Layer);
	void __fastcall MouseMove(System::TObject* Sender,
		  Classes::TShiftState Shift, int X, int Y, TCustomLayer *Layer);
	void __fastcall MouseUp(TObject *Sender,
		  TMouseButton Button, TShiftState Shift, int X, int Y,
		  TCustomLayer *Layer);

	void __fastcall AddP(TGPSCoordinate *c);
	TGPSCoordinate* __fastcall AddP(double ln, double lt, double Z, double Z_geoid, double conf);
	void __fastcall ScrollToCoordinate(TGPSCoordinate* llp);

	__property bool FollowMe = {read=readFollowMe, write=writeFollowMe};
	__property TGpsUnit* GpsUnit = {read=gpsunit, write=writeGpsUnit};
	__property TStartStopWaypointLabel* StartWayPoint = {read=startwaypoint, write=writeStartWayPoint};
	__property TStartStopWaypointLabel* StopWayPoint = {read=stopwaypoint, write=writeStopWayPoint};
	__property double Direction = {read=direction};
	//__property bool ValidAzimuth = {read=validazimuth};
	//__property double Azimuth = {read=azimuth};
};

class TMapLabelGeometry;

class TMapLabel: public TCustomProfileLabel
{
private:
	TMapLabelType maplabeltype;
	TMultiTimerItem* SuicideTimerItem;
	bool spiderweb;
	TMyObjectsList* SpiderYarn;
	void __fastcall Suicide(); // !!! Unsafe !!!
protected:
	bool Closing;
	TObject *host;
	TPosNotificationObject* notification;
	bool showpopup;
	TGPSCoordinatesRect* CoordinatesRect;
	TMyObjectsList* Geometries;
	TMapLabelGeometry* activegeometry;
	TGPSCoordinate* __fastcall readCoordinate() {return coordinate;}
	bool __fastcall readPopedUp() {return (notification && showpopup && notification->PopedUp);}
	void __fastcall writeTraceIndex(int value);
	void __fastcall writeShowPupUp(bool value) {if(value!=showpopup) {showpopup=value; if(notification && notification->Visible) notification->Visible=value;}}
	void __fastcall writeSpiderWeb(bool value);
	Types::TPoint __fastcall GetScreenXY();
	void __fastcall SelfDestruction(unsigned long Delay);
	void __fastcall ClearSpiderYarn();
public:
	__fastcall TMapLabel(TMyObject *AOwner, Types::TRect OutputRect,
		TProfileLabel *ConnectedPtr, TGPSCoordinatesRect *cRect, TObject* HostPath,
		TObject* RadarMapManager, TMapLabelType mlt=mltMapLabel);
	__fastcall TMapLabel(TMyObject *AOwner, Types::TRect OutputRect,
		TGPSCoordinatesRect *cRect, TObject* aHost, TObject* RadarMapManager,
		TMapLabelType mlt=mltMapLabel); // Host is a path where label is belongs to.
	__fastcall ~TMapLabel();

	void __fastcall MouseUp(TObject *Sender, TMouseButton Button,
		TShiftState Shift, int X, int Y, TCustomLayer *Layer);
	virtual void __fastcall Redraw(TObject *Sender, TBitmap32 *Bmp);

	void __fastcall PopUp();
	void __fastcall PopDown() {if(notification && notification->PopedUp) notification->PopDown();}
	void __fastcall PostponeSelfDestruction();
	void __fastcall AddGeometry(TMapLabelGeometry *geometry);
	void __fastcall DeleteGeometry(TMapLabelGeometry *geometry);
	void __fastcall DeactiveGeometry() {activegeometry=NULL;}

	__property TObject* Host = {read=host};
	__property TMapLabelType MapLabelType = {read=maplabeltype};
	//__property TPosNotificationObject* PopUpNotification = {read=notification};
	__property bool ShowPopUp = {read=showpopup, write=writeShowPupUp};
	__property bool PopedUp = {read=readPopedUp};
	__property bool SpiderWeb = {read=spiderweb, write=writeSpiderWeb};
	__property TMapLabelGeometry* ActiveGeometry = {read=activegeometry};
};

class TPinLabel: public TMapLabel
{
public:
	__fastcall TPinLabel(TMyObject *AOwner, Types::TRect OutputRect,
		TGPSCoordinatesRect *cRect, bool GetTrackCoordinate,
		TObject* RadarMapManager);
};

class TColorMaskLabel: public TMapLabel
{
private:
	TBitmap32 *ColoredBmp;
	void __fastcall Initialize(TColor32 aColor);
protected:
	TDoublePoint StartingVPP;
	Types::TPoint sp;
public:
	__fastcall TColorMaskLabel(TMyObject *AOwner, Types::TRect OutputRect,
		TGPSCoordinatesRect *cRect, TColor32 aColor, Types::TPoint aSP,
		TObject* RadarMapManager, TMapLabelType mlt=mltColorMaskLabel);
	__fastcall TColorMaskLabel(TMyObject *AOwner, Types::TRect OutputRect,
		TGPSCoordinatesRect *cRect, TColor32 aColor, TGPSCoordinate *aC,
		TObject* RadarMapManager, TMapLabelType mlt=mltColorMaskLabel);
	__fastcall ~TColorMaskLabel() {if(ColoredBmp) delete ColoredBmp; ColoredBmp=NULL;}

	void __fastcall MouseUp(TObject *Sender, TMouseButton Button,
		TShiftState Shift, int X, int Y, TCustomLayer *Layer);

	void __fastcall Redraw(TObject *Sender, TBitmap32 *Bmp);
};

class TColorPredictedLabel: public TColorMaskLabel
{
private:
    int layerid;
protected:
	void __fastcall writeDescription(AnsiString value) {TurnOffUpdates(); TCustomProfileLabel::writeDescription(value); TurnOnUpdates();}
	void __fastcall Initialize(TMultiTimerThread *gTimer, unsigned long aLifeTime, TGPSCoordinate *aC);
public:
	__fastcall TColorPredictedLabel(TMyObject *AOwner, Types::TRect OutputRect,
		TGPSCoordinatesRect *cRect, TColor32 aColor, Types::TPoint aSP,
		TMultiTimerThread *gTimer, unsigned long aLifeTime,
		TObject* RadarMapManager) : TColorMaskLabel(AOwner, OutputRect, cRect,
		aColor, aSP, RadarMapManager, mltColorPredictedLabel) {Initialize(gTimer, aLifeTime, NULL);}
	__fastcall TColorPredictedLabel(TMyObject *AOwner, Types::TRect OutputRect,
		TGPSCoordinatesRect *cRect, TColor32 aColor, TGPSCoordinate *aC,
		TMultiTimerThread *gTimer, unsigned long aLifeTime,
		TObject* RadarMapManager) : TColorMaskLabel(AOwner, OutputRect, cRect,
		aColor, aC, RadarMapManager, mltColorPredictedLabel) {Initialize(gTimer, aLifeTime, aC);}
	__fastcall ~TColorPredictedLabel();

	__property int LayerID = {read=layerid, write=layerid};
};

class TReferencesCollector: TObject
{
private:
	TList *References;
	TStrings *PercentageImagePaths;
	TCustomProfileLabel *Owner;
	int referencesq, iconindex;
	float percentage;
	TGpsListener *GpsListener;
	TObject *Manager;
	TVoidFunction oncollected, onpercentage;
	TTimerThread *timer;
	AnsiString IdleImage;
	bool iscollecting;
	TGPSCoordinate* coordinate;
protected:
	void __fastcall OnIdle() {if(IdleImage!=NULL && IdleImage!="" && Owner) {Owner->NormalGlyphName=IdleImage; Owner->Update();}}
	TGPSCoordinate* __fastcall readCoordinate() {if(Owner) return Owner->Coordinate; else return coordinate;}
public:
	__fastcall TReferencesCollector(TCustomProfileLabel *AOwner,
		int ReferencesQ, TStrings* APercentageImagePaths, AnsiString aIdleImage,
		unsigned long IdleTime, TObject* RadarMapManager);
	__fastcall ~TReferencesCollector();

	void __fastcall ClearReferences() {for(int i=0; i<References->Count; i++) delete /*(TGPSCoordinate*)*/References->Items[i]; References->Clear();}
	void __fastcall AddReference(TGPSCoordinate* c);
	void __fastcall AddCoordinate(TGPSCoordinate *c);
	void __fastcall StartReferencesCollection();
	void __fastcall StopReferencesCollection();

	__property int ReferencesQuantity = {read=referencesq};
	__property int IconIndex = {read=iconindex};
	__property float Percentage = {read=percentage};

	__property bool IsCollecting = {read=iscollecting};
	__property TVoidFunction OnCollected = {read=oncollected, write=oncollected};
	__property TVoidFunction OnPercentage = {read=onpercentage, write=onpercentage};
	__property TGPSCoordinate *Coordinate = {read=readCoordinate};
};

class TReferncePointLabel : public TMapLabel
{
private:
	/*TList *References;
	int referencesq, iconindex;
	float percentage;
	TGpsListener *GpsListener;*/
	TStrings *PercentageImagePaths;
	TReferencesCollector *RefCollector;
	Types::TPoint sp;
protected:
	virtual void __fastcall writeCoordinate(TGPSCoordinate *value);
	int __fastcall readReferencesQuantity() {return RefCollector->ReferencesQuantity;}
	bool __fastcall readIsCollecting() {return RefCollector->IsCollecting;}
public:
	__fastcall TReferncePointLabel(TMyObject *AOwner, Types::TRect OutputRect,
		TGPSCoordinatesRect *cRect, TObject* Host, int ReferencesQ, Types::TPoint SP,
		TObject* RadarMapManager);
	__fastcall ~TReferncePointLabel();

	//void __fastcall ClearReferences() {for(int i=0; i<References->Count; i++) delete /*(TGPSCoordinate*)*/References->Items[i]; References->Clear();}
	void __fastcall SetCoordinate(TGPSCoordinate* c);
	/*void __fastcall AddReference(TGPSCoordinate* c);
	void __fastcall StartReferencesCollection();
	void __fastcall StopReferencesCollection();
	void __fastcall AddCoordinate(TGPSCoordinate *c);*/
	void __fastcall StartReferencesCollection() {RefCollector->StartReferencesCollection();}
	void __fastcall StopReferencesCollection() {RefCollector->StopReferencesCollection();}

	void __fastcall Redraw(TObject *Sender, TBitmap32 *Bmp);
	void __fastcall MouseUp(TObject *Sender, TMouseButton Button,
		TShiftState Shift, int X, int Y, TCustomLayer *Layer);

	__property int ReferencesQuantity = {read=readReferencesQuantity};//referencesq};
	__property Types::TPoint SourcePoint = {read=sp};
	__property bool IsCollecting = {read = readIsCollecting};
};

enum TStartNGoLabelState {sgnNone=0, sgnPlaced=1, sgnAzimuthed=2, sgnStarted=3,
	sgnStopping=4, sgnStopped=5};

class TStartNGoLabel : public TMapLabel
{
private:
	Types::TPoint sp;
	TDoublePoint StartingVPP;
	double azimuth;
	int dX, dY;
	TLine AzimuthLine;
	Types::TPoint center2;
	TBitmap32 *azimuthglyph, *fixed1glyph, *fixed2glyph, *fixed3glyph, *fixed4glyph;
	AnsiString azimuthglyphname, fixed1glyphname, fixed2glyphname, fixed3glyphname,
		fixed4glyphname;
	void __fastcall writeAzimuthGlyphName(AnsiString value);
	void __fastcall writeFixed1GlyphName(AnsiString value);
	void __fastcall writeFixed2GlyphName(AnsiString value);
	void __fastcall writeFixed3GlyphName(AnsiString value);
	void __fastcall writeFixed4GlyphName(AnsiString value);
	void __fastcall StartTWDRS();
	void __fastcall StartTWDRSParser(AnsiString Reply);
	void __fastcall StopTWDRS();
	void __fastcall StopTWDRSParser(AnsiString Reply);
protected:
	AnsiString NormalCoordinated, NormalUnCoordinated;
	TStartNGoLabelState state;
	bool drawglyph;
	void __fastcall writeState(TStartNGoLabelState value);
	void __fastcall writeAzimuth(double value);
	__property TBitmap32* AzimuthGlyph = {read=azimuthglyph};//, write=writeNormalGlyph};
	__property AnsiString AzimuthGlyphName = {read=azimuthglyphname, write=writeAzimuthGlyphName};
	__property TBitmap32* Fixed1Glyph = {read=fixed1glyph};//, write=writeNormalGlyph};
	__property AnsiString Fixed1GlyphName = {read=fixed1glyphname, write=writeFixed1GlyphName};
	__property TBitmap32* Fixed2Glyph = {read=fixed2glyph};//, write=writeNormalGlyph};
	__property AnsiString Fixed2GlyphName = {read=fixed2glyphname, write=writeFixed2GlyphName};
	__property TBitmap32* Fixed3Glyph = {read=fixed3glyph};//, write=writeNormalGlyph};
	__property AnsiString Fixed3GlyphName = {read=fixed3glyphname, write=writeFixed3GlyphName};
	__property TBitmap32* Fixed4Glyph = {read=fixed4glyph};//, write=writeNormalGlyph};
	__property AnsiString Fixed4GlyphName = {read=fixed4glyphname, write=writeFixed4GlyphName};
public:
	__fastcall TStartNGoLabel(TMyObject *AOwner, Types::TRect OutputRect,
		TGPSCoordinatesRect *cRect, TMapsContainersCollection* Host, Types::TPoint SP,
		TObject* RadarMapManager);
	__fastcall ~TStartNGoLabel();

	virtual void __fastcall Update(TMyObjectType Type);
	void __fastcall Update() {Update(objecttype);}

	void __fastcall MouseMove(System::TObject* Sender,
		Classes::TShiftState Shift, int X, int Y, TCustomLayer *Layer);
	void __fastcall MouseDown(TObject *Sender,
		TMouseButton Button, TShiftState Shift, int X, int Y, TCustomLayer *Layer);
	void __fastcall MouseUp(TObject *Sender, TMouseButton Button,
		TShiftState Shift, int X, int Y, TCustomLayer *Layer);
	void __fastcall MouseHold(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y,
		TCustomLayer *Layer) {if(OnMouseHold!=NULL) (OnMouseHold)((TObject*)this, Button, Shift, X, Y);}// Pressed=false;}
	void __fastcall MouseDblClick(System::TObject* Sender, int X, int Y);

	virtual void __fastcall Redraw(TObject *Sender, TBitmap32 *Bmp);

	__property TStartNGoLabelState State = {read=state, write=writeState};
	__property double Azimuth = {read=azimuth, write=writeAzimuth}; // in radians, in ration to N
}; /**/

class TStartNGoGPSLabel : public TStartNGoLabel
{
private:
	TStrings *PercentageImagePaths;
	TReferencesCollector *RefCollector;
protected:
	int __fastcall readReferencesQuantity() {return RefCollector->ReferencesQuantity;}
	void __fastcall OnCoordinated();
    bool __fastcall readIsCollecting() {return RefCollector->IsCollecting;}
public:
	__fastcall TStartNGoGPSLabel(TMyObject *AOwner, Types::TRect OutputRect,
		TGPSCoordinatesRect *cRect, TMapsContainersCollection* Host, Types::TPoint SP,
		int ReferencesQ, TObject* RadarMapManager);
	__fastcall ~TStartNGoGPSLabel();

	void __fastcall Update(TMyObjectType Type) {TGraphicObject::Update(Type);}
	void __fastcall Update() {Update(objecttype);}

	void __fastcall StartReferencesCollection() {State=sgnNone; RefCollector->StartReferencesCollection();}
	void __fastcall StopReferencesCollection() {RefCollector->StopReferencesCollection();}

	void __fastcall Redraw(TObject *Sender, TBitmap32 *Bmp);

	__property int ReferencesQuantity = {read=readReferencesQuantity};
	__property bool IsCollecting = {read = readIsCollecting};
};

class TMapSparObject: public TMapTrackPathObject
{
private:
	//TSparUnit *SparDataPtr;
	Spar SparData;
protected:
public:
	__fastcall TMapSparObject(TMyObject *AOwner, TGPSCoordinatesRect *GCR, TObject* RadarMapManager) :
		TMapTrackPathObject(AOwner, GCR, RadarMapManager) {color=clNavy32;}
	__fastcall ~TMapSparObject() {}

	void __fastcall Redraw(TObject *Sender, TBitmap32 *Bmp);

	void __fastcall AddSpar(double ln, double lt, Spar src);
};

enum TStartStopWaypointKind {swkStart, swkStop};

class TStartStopWaypointLabel : public TMapLabel
{
private:
	Types::TPoint sp;
	TStartStopWaypointKind kind;
	TStartStopWaypointLabel *opositewaypoint;
	TMapTrackPathObject *path;
protected:
	void __fastcall writeOpositeWaypoint(TStartStopWaypointLabel *value);
	void __fastcall writePath(TMapTrackPathObject *value);
public:
	__fastcall TStartStopWaypointLabel(TMyObject *AOwner, Types::TRect OutputRect,
		TGPSCoordinatesRect *cRect, TObject* Host, Types::TPoint SP,
		TStartStopWaypointKind aKind, TObject* RadarMapManager);
	__fastcall ~TStartStopWaypointLabel();

	void __fastcall Redraw(TObject *Sender, TBitmap32 *Bmp);

	void __fastcall MouseUp(TObject *Sender, TMouseButton Button,
		TShiftState Shift, int X, int Y, TCustomLayer *Layer);

	__property TStartStopWaypointKind Kind = {read=kind};
	__property Types::TPoint SourcePoint = {read=sp};
	__property TStartStopWaypointLabel *OpositeWayPoint = {read=opositewaypoint, write=writeOpositeWaypoint};
	__property TMapTrackPathObject *Path = {read=path, write=writePath};
};

#endif

