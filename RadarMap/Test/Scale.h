//---------------------------------------------------------------------------
#ifndef ScaleH
#define ScaleH
#include <GR32_Image.hpp>

enum DefHotSide {hsLeft=0,hsRight=1,hsTop=2,hsBottom=3};

#define Ln 2.30259
#define MaxValues 51
#define min_scale_width 24
#define TTRect Types::TRect //TRect //

class TScale
{

private:
    void Adjust(void);
    void DrawOnCanvas(void);
    void DrawOnBitmap32(void);
    TFont *Font;
    LOGFONT logFont;
    int PrimLineWidth;
    int SecLineWidth;
    int PrimLineLenght;
    int SecLineLenght;
	float Value[MaxValues];
	float Offset[MaxValues];
    float dd;
    int Step;
	int Width;   // as for horizontal scale !
    int Height;  // as for horizontal scale !
    int Shift1;
    int Shift2;
	int Calculate(void);
	AnsiString ValueToStr(float f, bool TrimZeros=true);
	AnsiString Str;
//    bool Adjusted;
public:
    TScale(void);
    ~TScale();
//--------
    TCanvas *DC;
    TBitmap32 *Bmp;
	TTRect Rect;
	TColor Color;
	TColor BG_Color;
	String Caption;
	DefHotSide HotSide;
	float Min;
	float Max;
    AnsiString Units;
    bool IntegerValues;
//--------
	void Draw(void);
};



//---------------------------------------------------------------------------
#endif
