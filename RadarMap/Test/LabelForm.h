//---------------------------------------------------------------------------

#ifndef LabelFormH
#define LabelFormH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <ImageButton.h>
#include <pngimage.hpp>
#include <G32_ProgressBar.hpp>
#include <GR32_Image.hpp>
#include <Graphics.hpp>
#include "Manager.h"
#include <Dialogs.hpp>
#include <Menus.hpp>
//---------------------------------------------------------------------------
class TLabelInfoForm : public TForm
{
__published:	// IDE-managed Components
	TPanel *Panel1;
	TPanel *Panel2;
	TPanel *Panel3;
	TPanel *Panel4;
	TPanel *TitlePanel;
	TPanel *Panel5;
	TPanel *Panel6;
	TPanel *Panel7;
	TPanel *TabsPanel;
	TPanel *PagesPanel;
	TPanel *OkCancelPanel;
	TLabel *Label1;
	TPanel *Panel8;
	TPanel *Panel9;
	TImageButton *CloseIButton;
	TImageButton *CancelImageButton;
	TImageButton *OkImageButton;
	TNotebook *Notebook1;
	TImageButton *ImageButton3;
	TImageButton *ImageButton4;
	TImageButton *ImageButton5;
	TImageButton *ImageButton6;
	TImageButton *ImageButton7;
	TPanel *Panel11;
	TLabel *Label2;
	TPanel *XPanel;
	TLabel *Label3;
	TPanel *YPanel;
	TLabel *Label4;
	TPanel *ZPanel;
	TLabel *Label5;
	TPanel *DepthPanel;
	TLabel *Label6;
	TPanel *Panel15;
	TLabel *Label7;
	TPanel *Panel16;
	TLabel *Label8;
	TLabel *csLabel;
	TLabel *CreatedLabel;
	TLabel *ModifiedLabel;
	TLabel *DepthLabel;
	TG32_ProgressBar *ReferencesPBar;
	TLabel *XLabel;
	TEdit *XEdit;
	TEdit *YEdit;
	TLabel *YLabel;
	TImageButton *ApplyIButton;
	TImageButton *AcquireIButton;
	TImageButton *CancelIButton;
	TPanel *Panel10;
	TPanel *Panel12;
	TLabel *Label10;
	TLabel *Label11;
	TEdit *LabelIDEdit;
	TLabel *Label12;
	TPanel *Panel14;
	TPanel *Panel17;
	TLabel *Label14;
	TLabel *Label16;
	TImageButton *BrowseIButton;
	TPanel *PreviewPanel;
	TLabel *Label15;
	TImageButton *OpenIButton;
	TImage32 *PreviewImage;
	TImageButton *RemoveIButton;
	TPanel *Panel19;
	TPanel *Panel20;
	TLabel *Label18;
	TMemo *TextMemo;
	TEdit *TempEdit;
	TTimer *Timer1;
	TPanel *ConfidencePanel;
	TLabel *Label17;
	TLabel *ConfidenceLabel;
	TLabel *FileLabel;
	TOpenDialog *OpenDialog;
	TLabel *Label13;
	TPopupMenu *CSPopupMenu;
	TImageButton *StartTWDRSIButton;
	TImageButton *StopTWDRSIButton;
	TLabel *ZLabel;
	TPanel *UtmZonePanel;
	TLabel *Label9;
	TComboBox *ZoneNumCBox;
	TComboBox *ZoneCharCBox;
	void __fastcall CloseIButtonClick(TObject *Sender);
	void __fastcall ImageButton3Click(TObject *Sender);
	void __fastcall FormShow(TObject *Sender);
	void __fastcall XEditChange(TObject *Sender);
	void __fastcall PreviewImageClick(TObject *Sender);
	void __fastcall ImageButton7Click(TObject *Sender);
	void __fastcall Notebook1PageChanged(TObject *Sender);
	void __fastcall LabelIDEditKeyPress(TObject *Sender, wchar_t &Key);
	void __fastcall Timer1Timer(TObject *Sender);
	void __fastcall XEditKeyPress(TObject *Sender, wchar_t &Key);
	void __fastcall BrowseIButtonClick(TObject *Sender);
	void __fastcall RemoveIButtonClick(TObject *Sender);
	void __fastcall OkImageButtonClick(TObject *Sender);
	void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
	void __fastcall FormHide(TObject *Sender);
	void __fastcall ApplyIButtonClick(TObject *Sender);
	void __fastcall XEditExit(TObject *Sender);
	void __fastcall LabelIDEditExit(TObject *Sender);
	void __fastcall AcquireIButtonClick(TObject *Sender);
	void __fastcall CancelIButtonClick(TObject *Sender);
	void __fastcall TitlePanelMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
	void __fastcall TitlePanelMouseMove(TObject *Sender, TShiftState Shift, int X, int Y);
	void __fastcall TitlePanelMouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
	void __fastcall csLabelClick(TObject *Sender);
	void __fastcall StartTWDRSIButtonClick(TObject *Sender);
	void __fastcall StopTWDRSIButtonClick(TObject *Sender);
	void __fastcall TextMemoKeyPress(TObject *Sender, wchar_t &Key);
private:	// User declarations
	TCoordinateSystem cs;
	AnsiString attachedfile, attachment_old, description_old;
	TCustomProfileLabel *label;
	TRadarMapManager *manager;
	bool TitleDown;
	Types::TPoint TitleXY;
	TStrings *CRS_GUIDs;
	void __fastcall writeAttachedFile(AnsiString value);
	void __fastcall writeLabel(TCustomProfileLabel* value);
	void __fastcall writeCoordinateSystem(TCoordinateSystem value);
	void __fastcall CSMenuItemClick(TObject *Sender);
public:		// User declarations
	__fastcall TLabelInfoForm(TComponent* Owner);
	__fastcall ~TLabelInfoForm() {delete CRS_GUIDs;}
	void __fastcall RecolorReferencesPBar();
	void __fastcall RefreshLabel() {writeLabel(label);}

	__property AnsiString AttachedFile={read=attachedfile, write=writeAttachedFile};
	__property TCustomProfileLabel* Label={read=label, write=writeLabel};
	__property TCoordinateSystem CoordinateSystem={read=cs, write=writeCoordinateSystem};
	__property TRadarMapManager *Manager = {read=manager, write=manager};
};
//---------------------------------------------------------------------------
extern PACKAGE TLabelInfoForm *LabelInfoForm;
//---------------------------------------------------------------------------
#endif
