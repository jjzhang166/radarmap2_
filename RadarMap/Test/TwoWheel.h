//---------------------------------------------------------------------------

#ifndef TwoWheelH
#define TwoWheelH
//---------------------------------------------------------------------------
#include "GetGPSData.h"
#include "MyGraph.h"

enum TTwoWheelCalibrationType {twctNone, twctDistance, twctAngle, twctMotion};

class TNmeaTWPOS: public TNmeaSentence
{
public:
	double X, Y, Z, D, S; //X, Y, Z, D in meters; S - in meters per second
	TDateTime Tm;

	__fastcall TNmeaTWPOS() : TNmeaSentence(nstTWPOS) {Tm=0; X=Y=Z=D=S=0;}
	AnsiString __fastcall AsString(bool Full);
};

class TNmeaTWHPR: public TNmeaSentence
{
public:
	double Heading, Pitch, Roll; //in degrees

	__fastcall TNmeaTWHPR() : TNmeaSentence(nstTWHPR) {Heading=Pitch=Roll=0;}
	AnsiString __fastcall AsString(bool Full);
};

struct TTwoWheelSettings
{
	double CalibDistValue;
	int CalibAngleValue;
	int LeftWheelPPR, RightWheelPPR;
	double CenterX, CenterY;
	double LeftWheelD, RightWheelD, WBase;
	bool RightWheelPP, LeftWheelPP; // Push/Pull
};

class TTwoWheelNmeaClient: public TNmeaClient
{
private:
	TGPSCoordinate* startingpoint;
	bool useazimuth, useyawsensor;
	double azimuth; //in degrees;
	ULONGLONG LastTickCount, LastSuccessfulParsingTick;
	TTwoWheelCalibrationType currentcalib;
	TCustomProfileLabel* initiator;
	bool started;
protected:
	TTwoWheelSettings params;
	void __fastcall StopItselfByInitiator();
public:
	__fastcall TTwoWheelNmeaClient(TObject* AManager, TNmeaReceiverSettings *ANmeaSettings);
	__fastcall ~TTwoWheelNmeaClient();

	void __fastcall Disconnect();
	void __fastcall Stop();

	void __fastcall Initialize();
	void __fastcall SetStartingPoint(TGPSCoordinate *sp, bool ua, double a=0.0);
	bool __fastcall StartAcquisition();
	bool __fastcall StopPositioning(TGpsListenerReplyParser parserproc, TCustomProfileLabel *aInitiator = NULL);
	bool __fastcall GetParams(TGpsListenerReplyParser parserproc);
	bool __fastcall StartPositioning(TGpsListenerReplyParser parserproc,
		double startX, double startY, double startH, double startAzimuth,
		double startUVX, double startUVY, TCustomProfileLabel *aInitiator = NULL);
	bool __fastcall CalibDistStart(TGpsListenerReplyParser parserproc);
	bool __fastcall CalibDistStop(TGpsListenerReplyParser parserproc);
	bool __fastcall CalibAngleStart(TGpsListenerReplyParser parserproc);
	bool __fastcall CalibAngleStop(TGpsListenerReplyParser parserproc);
	bool __fastcall CancelCalibration(TGpsListenerReplyParser parserproc);
	bool __fastcall SetFullParams(TGpsListenerReplyParser parserproc);
	bool __fastcall SetParams(TGpsListenerReplyParser parserproc);
	bool __fastcall CalibrationStart(TTwoWheelCalibrationType calib);
	bool __fastcall CalibrationStop(TTwoWheelCalibrationType calib, float value=0);
	void __fastcall CalibrationSet(char *Buf, int N);
	void __fastcall CalibrationGet(char *Buf, int &N);
	TNmeaSentenceType __fastcall NmeaParser(HANDLE ThreadIdleEvent=INVALID_HANDLE_VALUE);
	TNmeaSentenceType __fastcall ParseNmeaSequence(AnsiString s1);
	void __fastcall ParseGPSDataTWPOS(TNmeaTWPOS *out, AnsiString str);
	void __fastcall ParseGPSDataTWHPR(TNmeaTWHPR *out, AnsiString str);
	int ParseStrings(AnsiString InStr, TStrings *Out, char Separator);

	double __fastcall GetPassedDistance(bool freshness);
	void __fastcall StartProfiling() {LastTickCount=0;}
	bool __fastcall InCalibration() {return currentcalib!=twctNone;}
	void __fastcall ForgetInitiator(TCustomProfileLabel *aInitiator) {if(aInitiator && initiator==aInitiator) {initiator=NULL; StopPositioning(NULL, NULL);}}

	__property TGPSCoordinate* StartingPoint = {read=startingpoint};
	__property double StartingAzimuth = {read=azimuth};
	__property bool UseAzimuth = {read=useazimuth};
	__property bool UseYawSensor = {read=useyawsensor};
	__property TTwoWheelCalibrationType CurrentCalibration = {read=currentcalib};
	__property TTwoWheelSettings Parameters = {read=params};
	__property bool Started = {read=started};
	__property TCustomProfileLabel* Initiator = {read=initiator};
};

#endif
