//---------------------------------------------------------------------------

#ifndef DrawProfH
#define DrawProfH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Windows.h>
#include <GR32_Image.hpp>
#include <GR32_polygons.hpp>
#include "Manager.h"
//---------------------------------------------------------------------------

class TMarkRect
{
public:
  int X1, Y1, X2, Y2;
};

class TMarkList
{
private:
protected:
	TList *List;
public:
	int minY, maxY, maxX, OffsetX, Count;
	TMarkList(void);
	~TMarkList(void);
	void Add(TMarkRect *mr);
	void Delete(int num);
	void Clear(void);
	TMarkRect* Item(int num);
	bool TryPoint(Types::TPoint p);
	void IncOffsetX() { OffsetX++; }
	void DecOffsetX() { OffsetX--; }
};

class TDrawProfThrd : public TThread
{
private:
	TBitmap32 *bbb, *mmm;
	TRadarMapManager *RadarMapManager;
	TImgView32 *Image;
	TProfile *Profile;
	int ProfileIndex;
	int _X[_MaxProfQuan], ww, _MarkY[_MaxProfQuan];
	Types::TRect Dst, Src;
	TPolygon32 *Poly32;
	TMarkList *MarkList;
	TTrace *LastDrawed;
	//TTraceAccumulator *TraceAccumulator;
	TProfile *LastProf;
	bool AfterScrolling[_MaxProfQuan];
protected:
	void __fastcall Execute();
public:
	__fastcall TDrawProfThrd(bool CreateSuspended, TRadarMapManager *RMM, TImgView32 *AImg);
	void __fastcall DrawTrace(void);
};
//---------------------------------------------------------------------------
#endif
