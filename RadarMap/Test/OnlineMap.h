//---------------------------------------------------------------------------

#ifndef OnlineMapH
#define OnlineMapH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <ImageButton.h>
#include <pngimage.hpp>
#include <G32_ProgressBar.hpp>
#include <GR32_Image.hpp>
#include <Graphics.hpp>
#include "Manager.h"
#include <Dialogs.hpp>
#include <Menus.hpp>
#include <ComCtrls.hpp>
#include "OsmInetMap.h"
#include "ImageMovie.h"
//---------------------------------------------------------------------------

class TOnlineMapForm : public TForm
{
__published:	// IDE-managed Components
	TPanel *Panel1;
	TPanel *Panel2;
	TPanel *Panel3;
	TPanel *Panel4;
	TPanel *TitlePanel;
	TPanel *Panel5;
	TPanel *Panel6;
	TPanel *Panel7;
	TPanel *TabsPanel;
	TPanel *PagesPanel;
	TPanel *OkCancelPanel;
	TLabel *Label1;
	TPanel *Panel8;
	TPanel *Panel9;
	TImageButton *CloseIButton;
	TImageButton *CancelImageButton;
	TNotebook *Notebook1;
	TPanel *Panel11;
	TLabel *Label2;
	TPanel *XPanel;
	TLabel *Label3;
	TPanel *YPanel;
	TLabel *Label4;
	TLabel *csLabel;
	TLabel *XLabel;
	TEdit *XEdit;
	TEdit *YEdit;
	TLabel *YLabel;
	TImageButton *AcquireIButton;
	TEdit *TempEdit;
	TTimer *Timer1;
	TLabel *Label13;
	TPopupMenu *CSPopupMenu;
	TImageButton *ContentsIButton;
	TImageButton *PreviewIButton;
	TPanel *ZoomPanel;
	TLabel *Label6;
	TTrackBar *ZoomTrackBar;
	TImageButton *OkImageButton;
	TPanel *MoviePanel;
	TLabel *WaitLabel;
	TImageButton *CloseMovieButton;
	TImageButton *StopButton;
	TImage32 *Image321;
	TPanel *Panel14;
	TPanel *Panel15;
	TPanel *Panel16;
	TPanel *Panel18;
	TPanel *Panel10;
	TImgView32 *MapImage;
	TG32_ProgressBar *ReferencesPBar;
	TPanel *UtmZonePanel;
	TLabel *Label5;
	TLabel *Label7;
	TComboBox *ZoneNumCBox;
	TComboBox *ZoneCharCBox;
	TLabel *ZoomLabel;
	void __fastcall CloseIButtonClick(TObject *Sender);
	void __fastcall ContentsIButtonClick(TObject *Sender);
	void __fastcall FormShow(TObject *Sender);
	void __fastcall XEditChange(TObject *Sender);
	void __fastcall Timer1Timer(TObject *Sender);
	void __fastcall XEditKeyPress(TObject *Sender, wchar_t &Key);
	void __fastcall OkImageButtonClick(TObject *Sender);
	void __fastcall FormHide(TObject *Sender);
	void __fastcall XEditExit(TObject *Sender);
	void __fastcall AcquireIButtonClick(TObject *Sender);
	void __fastcall CancelIButtonClick(TObject *Sender);
	void __fastcall TitlePanelMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
	void __fastcall TitlePanelMouseMove(TObject *Sender, TShiftState Shift, int X, int Y);
	void __fastcall TitlePanelMouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift,
		  int X, int Y);
	void __fastcall csLabelClick(TObject *Sender);
	void __fastcall PreviewIButtonClick(TObject *Sender);
	void __fastcall ZoomTrackBarChange(TObject *Sender);
private:	// User declarations
	TCoordinateSystem coordinatesystem;
	TRadarMapManager *manager;
	bool TitleDown;
	Types::TPoint TitleXY;
	TStrings *CRS_GUIDs;
	TResourceMovie* ResMov;
	TInetMapBase* imc;
	TMouseAction MA;
	TMapObjectsContainer *mapobjects;
	TSliderToolbar *MapToolbar;
	TSliderToolButton *LayersBtn, *ZoomInBtn, *ZoomOutBtn;
	int mapzoom;
	TGPSCoordinate *mapcenter;
	TReferencesCollector *RefCollector;
	void __fastcall ZoomInMouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y);
	void __fastcall ZoomOutMouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y);
	void __fastcall LayersClick(System::TObject* Sender, int X, int Y);

	bool __fastcall ZoomBingMap(bool pIsZoomIn);
	void __fastcall ReDownloadMap();
	void __fastcall DisableMouseAction();
	void __fastcall ChangePercentage();
	void __fastcall ReferencesCollected();

	void __fastcall StopMovie();
	void __fastcall CreateMapObjects();
	void __fastcall StartMovie(bool WaitOrStop, TResourceMovieType rmt);
	void __fastcall writeCoordinateSystem(TCoordinateSystem value);
	void __fastcall writeMapZoom(int value) {ZoomTrackBar->Position=value; mapzoom=value;}
	void __fastcall CSMenuItemClick(TObject *Sender);
public:		// User declarations
	__fastcall TOnlineMapForm(TComponent* Owner, TRadarMapManager *aManager, TCoordinateSystem aCS);
	__fastcall ~TOnlineMapForm();
	void __fastcall RecolorReferencesPBar();

	__property TCoordinateSystem CoordinateSystem={read=coordinatesystem, write=writeCoordinateSystem};
	__property TRadarMapManager *Manager = {read=manager, write=manager};
	__property int MapImageZoom = {read = mapzoom, write = writeMapZoom};
	__property TGPSCoordinate *MapCenter = {read=mapcenter};
};
//---------------------------------------------------------------------------
extern PACKAGE TOnlineMapForm *OnlineMapForm;
//---------------------------------------------------------------------------
#endif
