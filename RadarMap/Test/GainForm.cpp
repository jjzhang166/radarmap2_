//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "GainForm.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "ImageButton"
#pragma link "G32_ProgressBar"
#pragma link "GR32_Image"
#pragma resource "*.dfm"
TGainForm *GainForm;

//---------------------------------------------------------------------------
__fastcall TGainForm::TGainForm(TComponent* Owner, TObject* AManager)
	: TForm(Owner)
{
	Image->SetupBitmap(true, 0x00E9E2E1);
	BordersRect=Types::TRect(13, 13, Image->Width-12, Image->Height-12);
	ChannelsPxOffset=4;
	GainImageGrid=new TOscilloscopeGrid(Image, ((TRadarMapManager*)AManager)->ReceivingSettings,
		ChannelsPxOffset, &BordersRect);
	gainitems=new TGainItems(AManager, NULL, Image, &BordersRect, false, true, NULL, NULL, GainPointsPanel, (TColor32)0x00E9E2E1);
	GainItems->Visible=true;
}
//---------------------------------------------------------------------------

void __fastcall TGainForm::CloseIButtonClick(TObject *Sender)
{
	ModalResult=mrCancel;
}
//---------------------------------------------------------------------------

void __fastcall TGainForm::OkImageButtonClick(TObject *Sender)
{
	ModalResult=mrOk;
}
//---------------------------------------------------------------------------

void __fastcall TGainForm::TitlePanelMouseDown(TObject *Sender, TMouseButton Button,
		  TShiftState Shift, int X, int Y)
{
	TitleDown=true;
	TitleXY=ClientToScreen(Types::TPoint(X, Y));
}
//---------------------------------------------------------------------------

void __fastcall TGainForm::TitlePanelMouseMove(TObject *Sender, TShiftState Shift,
		  int X, int Y)
{
	Types::TPoint tmp;

	if(TitleDown)
	{
		tmp=ClientToScreen(Types::TPoint(X, Y));
		Left+=tmp.x-TitleXY.x;
		Top+=tmp.y-TitleXY.y;
		TitleXY=tmp;
	}
}
//---------------------------------------------------------------------------

void __fastcall TGainForm::TitlePanelMouseUp(TObject *Sender, TMouseButton Button,
		  TShiftState Shift, int X, int Y)
{
	TitleDown=false;
}
//---------------------------------------------------------------------------
