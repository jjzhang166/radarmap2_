//---------------------------------------------------------------------------

#ifndef InfoFormH
#define InfoFormH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <ImageButton.h>
#include <pngimage.hpp>
#include <GR32_Image.hpp>
#include <Graphics.hpp>
#include "Manager.h"
#include <Dialogs.hpp>
#include <ComCtrls.hpp>
#include <ImgList.hpp>
#ifdef _RAD101
	#include <System.ImageList.hpp>
#endif
//---------------------------------------------------------------------------
class TInfoForm : public TForm
{
__published:	// IDE-managed Components
	TPanel *Panel1;
	TPanel *Panel2;
	TPanel *Panel3;
	TPanel *Panel4;
	TPanel *TitlePanel;
	TPanel *Panel5;
	TPanel *Panel6;
	TPanel *Panel7;
	TPanel *TabsPanel;
	TPanel *PagesPanel;
	TPanel *OkCancelPanel;
	TLabel *Label1;
	TPanel *Panel8;
	TPanel *Panel9;
	TImageButton *CloseIButton;
	TImageButton *CancelImageButton;
	TImageButton *OkImageButton;
	TImageButton *InfoBtn;
	TImageButton *AddBtn;
	TEdit *TempEdit;
	TImageList *ImageList1;
	TNotebook *Notebook1;
	TPanel *Panel10;
	TLabel *Label2;
	TPanel *Panel11;
	TLabel *Label3;
	TPanel *Panel20;
	TMemo *GeneratedMemo;
	TPanel *Panel12;
	TMemo *BriefMemo;
	void __fastcall CloseIButtonClick(TObject *Sender);
	void __fastcall InfoBtnClick(TObject *Sender);
	void __fastcall FormShow(TObject *Sender);
	void __fastcall OkImageButtonClick(TObject *Sender);
	void __fastcall TitlePanelMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
	void __fastcall TitlePanelMouseMove(TObject *Sender, TShiftState Shift, int X, int Y);
	void __fastcall TitlePanelMouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
	void __fastcall BriefMemoKeyPress(TObject *Sender, wchar_t &Key);
	void __fastcall GeneratedMemoChange(TObject *Sender);
	void __fastcall GeneratedMemoMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
	void __fastcall GeneratedMemoClick(TObject *Sender);
	void __fastcall GeneratedMemoMouseMove(TObject *Sender, TShiftState Shift, int X,
          int Y);
	void __fastcall GeneratedMemoMouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
private:	// User declarations
	bool TitleDown;
	Types::TPoint TitleXY;
	UnicodeString __fastcall readBriefInfo() {return BriefMemo->Lines->Text;}
	void __fastcall writeGeneratedInfo(UnicodeString value) {GeneratedMemo->Lines->Text=value;}
	void __fastcall writeBriefInfo(UnicodeString value) {BriefMemo->Lines->Text=value;}
public:		// User declarations
	__fastcall TInfoForm(TComponent* Owner, TRadarMapManager *AManager);

	__property UnicodeString GeneratedInfo = {write=writeGeneratedInfo};
	__property UnicodeString BriefInfo = {read=readBriefInfo, write=writeBriefInfo};
};
//---------------------------------------------------------------------------
extern PACKAGE TInfoForm *InfoForm;
//---------------------------------------------------------------------------
#endif
