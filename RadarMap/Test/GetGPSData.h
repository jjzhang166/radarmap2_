//---------------------------------------------------------------------------

#ifndef GetGPSDataH
#define GetGPSDataH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include "SerialComm.h"
#include "Profile.h"
#include "InterpolateGPS.h"
#include "GpsListener.h"
#include "TCPClient.h"
//---------------------------------------------------------------------------

enum TNmeaSentenceType {nstNone = 0, nstGGA = 1, nstGSA = 2, nstGST = 3, nstLLQ = 4,
	nstHDT = 5, nstCTMOTION = 6, nstPlugIn = 7,
	nstTWPOS = 8, nstTWHPR = 9,//, nstTWWHE = 10
	nstUnknown = 10};


class TSerialTerminal : public TObject
{
private:
	TStrings *lines, *FileLines;
	HANDLE busyevent;
	TFileStream* LogFileStream;
	unsigned int MaxLines;
	TMemo* memo;
	bool showlines;
protected:
	int __fastcall readCount() {return lines->Count;}
	UnicodeString __fastcall readText();
	void __fastcall writeMemo(TMemo *value) {if(memo!=value) {memo=value; if(memo) memo->Lines->Text=lines->Text;}}
	bool __fastcall readLogToFile() {return LogFileStream!=NULL;}
	void __fastcall writeLogToFile(bool value);
public:
	__fastcall TSerialTerminal(unsigned int AMaxLines, bool ALogToFile);// 0 - Unlimited lines quantity
	__fastcall ~TSerialTerminal();

	bool __fastcall AddLine(AnsiString str, bool readwrite); //true - read, false - write
	bool __fastcall Clear();

	__property HANDLE BusyEvent = {read=busyevent};
	__property UnicodeString Text = {read=readText};
	__property int Count = {read=readCount};
	__property TMemo* Memo = {write=writeMemo};
	__property bool LogToFile = {read=readLogToFile, write=writeLogToFile};
	__property bool ShowLines = {read=showlines, write = showlines};
};

class TSerialThread: public TCustomSerialThread
{
private:
	TNmeaReceiverSettings *NmeaSettings;
protected:
	void __fastcall AddReceivedData(char *Buf, int N);
	void __fastcall ExecuteLoopBody();
public:
	__fastcall TSerialThread(TCustomSerialClient *AOwner, TObject *AManager, TNmeaReceiverSettings *ANmeaSettings) : TCustomSerialThread(AOwner, AManager) {NmeaSettings=ANmeaSettings;}
	__fastcall ~TSerialThread() {}
};

class TNmeaClient;

class TNmeaInitializingThread: public TMyThread
{
private:
	TObject *Manager;
	TNmeaClient* Owner;
	TNmeaReceiverSettings *NmeaSettings;
	TGpsListenerReplyParser CommandToParse;
	AnsiString WhatToParse;
	void __fastcall CommandParser();
	bool __fastcall CheckForInitTimeOuts(unsigned long InitStart, unsigned long StartTick);
protected:
	void __fastcall Execute();
public:
	__fastcall TNmeaInitializingThread(TNmeaClient* AOwner, TObject *AManager, TNmeaReceiverSettings *ANmeaSettings);
	__fastcall ~TNmeaInitializingThread() {Owner=NULL;}
};

class TNmeaSentence: public TObject
{
private:
	bool refreshed;
	bool corrupted;
	bool endfound;
	TNmeaSentenceType sentencetype;
protected:
	bool __fastcall readRefreshed() {bool b=refreshed; refreshed=false; return b;}
public:
	__fastcall TNmeaSentence(TNmeaSentenceType AType) {refreshed=false; corrupted=true; endfound=false; sentencetype=AType;}
	virtual AnsiString __fastcall AsString(bool Full) {return "";};

	__property bool Refreshed = {read=readRefreshed, write=refreshed};
	__property TNmeaSentenceType SentenceType = {read=sentencetype};
	__property bool Corrupted = {read=corrupted, write = corrupted};
	__property bool EndFound = {read=endfound, write = endfound};
};

class TNmeaGGA: public TNmeaSentence
{
public:
	TDateTime Tm;
	double Lat, Lon, Z, Z_geoid;
	int Quality;
	int Satellites;
	double HDOP;
	double DelaySinceLastUpdate;

	__fastcall TNmeaGGA();
	AnsiString __fastcall AsString(bool Full);
};

class TNmeaGSA: public TNmeaSentence
{
public:
	char Selection; // A - Auto, M - Manual
	int FixType; //1 = no fix, 2 = 2D fix, 3 = 3D fix
	int PRNs[12];
	double PDOP, HDOP, VDOP;

	__fastcall TNmeaGSA();
	AnsiString __fastcall AsString(bool Full);
};

class TNmeaGST: public TNmeaSentence
{
public:
	TDateTime Tm;
	double RMS, MajorDiv, MinorDiv, OrientationToNorth, LatDiv, LonDiv, ZDiv;

	__fastcall TNmeaGST();
	AnsiString __fastcall AsString(bool Full);
};

class TNmeaLLQ: public TNmeaSentence
{
public:
	TDateTime Tm, Dt;
	double Easting, Northing, Z;
	int Quality;
	int Satellites;
	double CoordinateQuality;

	__fastcall TNmeaLLQ();
	AnsiString __fastcall AsString(bool Full);
};

class TNmeaCTMOTION: public TNmeaSentence
{
public:
	double Pitch, Roll;

	__fastcall TNmeaCTMOTION() : TNmeaSentence(nstCTMOTION) {Pitch=Roll=-1;}
	AnsiString __fastcall AsString(bool Full);
};

class TNmeaHDT: public TNmeaSentence
{
public:
	double Heading;

	__fastcall TNmeaHDT() : TNmeaSentence(nstHDT) {Heading=-1;}
	AnsiString __fastcall AsString(bool Full);
};

class TGpsUnit;
class TNmeaSummaryThread;

class TNmeaClient : public TCustomSerialClient
{
private:
	TGpsUnit* unit;
	AnsiString name;
	TList* InitCommands;
	TTCPClient *client;
	TSerialTerminal *terminal;
	HANDLE initializedevent;
	TNmeaInitializingThread *InitThrd;
	void __fastcall TerminateThreads();
	bool __fastcall readInitialized() {return (WaitForSingleObject(initializedevent, 0)==WAIT_OBJECT_0);}
	bool __fastcall readActiveInitializing() {return (InitThrd && !InitThrd->IsTerminated());}
protected:
	bool clientstarted;
	TNmeaReceiverSettings *NmeaSettings;
	TSerialThread *SerialThrd;
	TNmeaSummaryThread *nmeasummary;
	virtual void __fastcall SetStartingPoint(TGPSCoordinate *sp, bool ua, double a=0.0) {}
	bool __fastcall readConnected() {if(Client) return Client->Connected; else return connected;}
	void __fastcall ClearInitCommands() {ResetEvent(initializedevent); for(int i=0; i<InitCommands->Count; i++) delete (TGpsListenerCommand*)InitCommands->Items[i]; InitCommands->Clear();}
	int __fastcall readCount() {return InitCommands->Count;}
	bool __fastcall AddSingleCommandWithReply(TGpsListenerCommand *cmd);
	void __fastcall ClearCommandsSequence(bool WaitUntilPreviousEnds);
	void __fastcall AddCommandWithReplyToSequence(TGpsListenerCommand *cmd);
	bool __fastcall StartCommandsSequence();
	TGpsListenerCommand* __fastcall readCommands(int Index) {if(Index>=0 && Index<InitCommands->Count) return (TGpsListenerCommand*)InitCommands->Items[Index]; else return NULL;}
public:
	__fastcall TNmeaClient(TObject* AManager, TNmeaReceiverSettings *ANmeaSettings);
	__fastcall ~TNmeaClient();

	void __fastcall Connect();
	virtual void __fastcall Disconnect();

	void __fastcall WriteCommand(char *cmd) {writebuf->Push(new AnsiString((AnsiString)cmd+"\r\n"));}//));}//
	void __fastcall PauseTerminal() {if(terminal) terminal->ShowLines=false;}
	void __fastcall ResumeTerminal() {if(terminal) terminal->ShowLines=true;}

	bool __fastcall CheckCRC(AnsiString str, unsigned int crc);
	virtual TNmeaSentenceType __fastcall NmeaParser(HANDLE ThreadIdleEvent=INVALID_HANDLE_VALUE);
	void __fastcall LoadCommandsFromFile(AnsiString FileName);
	virtual void __fastcall Initialize();
	virtual void __fastcall Start() {clientstarted=true;}
	virtual void __fastcall Stop() {clientstarted=false;}
	void __fastcall Reconnect() {bool b=Initialized; TCustomSerialClient::Reconnect(); if(b) Initialize();}

	AnsiString __fastcall GetNextNmeaParam(bool &EndFound, AnsiString *str);
	virtual TNmeaSentenceType __fastcall ParseNmeaSequence(AnsiString s1);
	void __fastcall ParseGPSDataGGA(TNmeaGGA *out, AnsiString Line);
	void __fastcall ParseGPSDataGSA(TNmeaGSA *out, AnsiString Line);
	void __fastcall ParseGPSDataGST(TNmeaGST *out, AnsiString Line);
	void __fastcall ParseGPSDataLLQ(TNmeaLLQ *out, AnsiString Line);
	void __fastcall ParseGPSDataCTMOTION(TNmeaCTMOTION *out, AnsiString Line);
	void __fastcall ParseGPSDataHDT(TNmeaHDT *out, AnsiString Line);

	__property TTCPClient *Client = {read=client};
	__property TSerialTerminal* Terminal = {read=terminal};
	__property Name = {read=name};
	__property int InitCommandsCount = {read=readCount};
	__property HANDLE InitializedEvent = {read=initializedevent};
	__property bool Initialized = {read=readInitialized};
	__property bool ActiveInitializing = {read=readActiveInitializing};
	__property TGpsListenerCommand* Commands[int Index] = {read=readCommands};
	__property TNmeaSummaryThread *NmeaSummary = {read=nmeasummary};
	__property TGpsUnit *Unit = {read=unit, write=unit};
	__property bool ClientStarted = {read=clientstarted};
};

class TNmeaClientsPool: TObject
{
private:
	TList* List;
	int __fastcall readCount() {return List->Count;}
	TNmeaClient* __fastcall readPool(int Index) {if(Index>=0 && Index<List->Count) return (TNmeaClient*)List->Items[Index]; else return NULL;}
protected:
public:
	__fastcall TNmeaClientsPool() {List=new TList();}
	__fastcall ~TNmeaClientsPool() {Clear(); delete List;}

	int __fastcall IndexOf(TNmeaClient* value) {int i=-1; if(value) i=List->IndexOf((void*)value); return i;}
	void __fastcall Add(TNmeaClient* value, TGpsUnit* AUnit) {if(value && IndexOf(value)<0) {List->Add((void*)value); value->Unit=AUnit;}}
	void __fastcall Delete(int Index, bool FreeItem=false) {if(Pool[Index]) {if(FreeItem) delete Pool[Index]; List->Delete(Index);}}
	void __fastcall Clear(bool FreeItem=false) {for(int i=0; i<List->Count; i++) if(FreeItem && Pool[i]) delete Pool[i]; List->Clear();}

	__property int Count = {read=readCount};
	__property TNmeaClient* Pool[int Index] = {read=readPool};
};

class TNmeaReceivingThread: public TStartStopThread
{
private:
	TGpsUnit* Owner;
	TObject *Manager;
	TGpsListenersList* Listeners;
	TNmeaClientsPool* Clients;
	bool FirstPointStt, InitializingStarts;
	TDoublePoint FirstPoint;
	TGPSCoordinate *CenterPoint;
	double OffsetX, OffsetY;
	TTrace* ToPtr;
	TNmeaSummaryThread *NmeaSummary;
	void __fastcall DrawPoint();
	void __fastcall SetLastPoint();
	void __fastcall AddTrackPoint();
	void __fastcall ShowTrackPoint();
	void __fastcall ShowTrackPoint2(TGPSCoordinate *c);
protected:
	void __fastcall ExecuteLoopBody();
public:
	__fastcall TNmeaReceivingThread(TGpsUnit *AOwner, TNmeaClientsPool* AClients, TGpsListenersList* AListeners, TObject *AManager);
	__fastcall ~TNmeaReceivingThread() {if(CenterPoint) delete CenterPoint;}

	void __fastcall ReCenter() {if(CenterPoint) delete CenterPoint; CenterPoint=NULL;}
	void __fastcall SetOffset(double X, double Y) {OffsetX=X; OffsetY=Y;}
	void __fastcall AddOffset(double X, double Y) {OffsetX+=X; OffsetY+=Y;}
};

class TNmeaSummaryThread: public TObject
{
private:
	TNmeaReceiverSettings *NmeaSettings;
	TNmeaClient* owner;
	TNmeaSentence **Sentences;
	TDateTime tm;
	TGPSCoordinate *coordinate, *lastcoordinate;
	TObject *Manager;
	int quality;
	int satellites;
	ULONGLONG LastValidCoordinateTick;
	double coordinatedspeed, ddistance;
	double hdop, vdop, pdop;
	int fixtype;
	double rms, majordiv, minordiv, llqquality;
	double pitch, roll, heading;
	double distance;
	TFreshValue<double> speed;
	void __fastcall Clear() {for(int i=0; i<nstUnknown; i++) {if(Sentences[i]) delete Sentences[i]; Sentences[i]=NULL;}}
protected:
	double __fastcall readLon() {return coordinate->Longitude;}
	double __fastcall readLat() {return coordinate->Latitude;}
	double __fastcall readZ() {return coordinate->Z;}
	double __fastcall readZGeoid() {return coordinate->Z_Geoid;}
public:
	__fastcall TNmeaSummaryThread(TNmeaReceiverSettings *ANmeaSettings, TObject *AManager);
	__fastcall ~TNmeaSummaryThread();

	void __fastcall Set(TNmeaSentence *NS);
	TNmeaSentence* __fastcall Set(TNmeaSentenceType NST);
	TNmeaSentence* __fastcall Get(TNmeaSentenceType NST);
	bool __fastcall Rebuild(TNmeaSentenceType nst);

	__property TDateTime Tm = {read=tm};
	__property TGPSCoordinate *Coordinate = {read=coordinate};
	__property double Lon = {read=readLon};
	__property double Lat = {read=readLat};
	__property double Z = {read=readZ};
	__property double Z_Geoid = {read=readZGeoid};
	__property int Quality = {read=quality};
	__property int Satellites = {read=satellites};
	__property double HDOP = {read=hdop};
	__property double VDOP = {read=vdop};
	__property double PDOP = {read=pdop};
	__property int FixType = {read=fixtype};
	__property double RMS = {read=rms};
	__property double MajorDiv = {read=majordiv};
	__property double MinorDiv = {read=minordiv};
	__property double LLQQuality = {read=llqquality};
	__property double Pitch = {read=pitch}; // in Degrees
	__property double Roll = {read=roll}; // in Degrees
	__property double Heading = {read=heading}; // in Degrees
	__property double Distance = {read=distance};
	__property TFreshValue<double> Speed = {read=speed}; // in m per second
	__property double CoordinatedSpeed = {read=coordinatedspeed}; // speed in m per second between current and last received coordinates
	__property double dDistance = {read=ddistance}; // distance in m between current and last received coordinates
	__property TNmeaClient* Owner = {read=owner, write=owner};
};

class TGpsUnit : public TIndicator
{
private:
	TNmeaClientsPool* clients;
	TObject *Manager;
//	TList* CoordinatesAcquirersList;
	TNmeaReceivingThread *RecThrd;
	TGpsListenersList* Listeners;
	/*TNmeaGGA *gga;
	TNmeaGSA *gsa;
	TNmeaGST *gst;
	TNmeaLLQ *llq;*/
	TColor32 Color;
	int index;
	bool indexrefreshed, notindexedrefreshed;
	float HDOP;
	UnicodeString NotConnected, Simulation, notindexed;
	TNmeaClient* GpsClient;

	void __fastcall writeIndex(int value) {if(value<0) index=0; else if(value>TIndicator::MaxIcons) index=TIndicator::MaxIcons; else index=value; indexrefreshed=true;}
	bool __fastcall readIndexRefreshed() {bool b=indexrefreshed; indexrefreshed=false; return b;}
	bool __fastcall readNotIndexedRefreshed() {bool b=notindexedrefreshed; notindexedrefreshed=false; return b;}
	void __fastcall writeNotIndexed(UnicodeString value) {notindexed=value; indexrefreshed=false; notindexedrefreshed=true;}
//	void __fastcall AddCoordinatesAcquirer(TGpsCoordinateAcquirer *thrd);
//	void __fastcall StartCoordinatesAcquirers();
//	void __fastcall StopCoordinatesAcquirers();
	__property int Index={read=index, write=writeIndex};
	__property bool IndexRefreshed={read=readIndexRefreshed};
	__property bool NotIndexedRefreshed={read=readNotIndexedRefreshed};
	__property UnicodeString NotIndexed = {read=notindexed, write=writeNotIndexed};
protected:
	void __fastcall TerminateThreads();
public:
	__fastcall TGpsUnit(TNmeaClient *AGpsClient, TGpsListenersList* AListeners, TObject *AManager);
	__fastcall ~TGpsUnit();

	void __fastcall AddNmeaClient(TNmeaClient *value) {Clients->Add(value, this);}
	void __fastcall DeleteNmeaClient(TNmeaClient *value) {Clients->Delete(Clients->IndexOf(value)); if(value==GpsClient) GpsClient=NULL;}

	void __fastcall Initialize();
//	void __fastcall AssignGPSDataGGA(TNmeaGGA *out, HANDLE ThreadIdleEvent=INVALID_HANDLE_VALUE);
//	void __fastcall AssignGPSDataGSA(TNmeaGSA *out, HANDLE ThreadIdleEvent=INVALID_HANDLE_VALUE);
//	void __fastcall AssignGPSDataGST(TNmeaGST *out, HANDLE ThreadIdleEvent=INVALID_HANDLE_VALUE);
//	void __fastcall AssignGPSDataLLQ(TNmeaLLQ *out, HANDLE ThreadIdleEvent=INVALID_HANDLE_VALUE);
//	void __fastcall AssignGPSDataCTMOTION(TNmeaCTMOTION *out, HANDLE ThreadIdleEvent=INVALID_HANDLE_VALUE);
//	void __fastcall AssignGPSDataHDT(TNmeaHDT *out, HANDLE ThreadIdleEvent=INVALID_HANDLE_VALUE);
//	void __fastcall AssignGPSDataTWPOS(TNmeaTWPOS *out, HANDLE ThreadIdleEvent=INVALID_HANDLE_VALUE);
//	void __fastcall AssignGPSDataTWHPR(TNmeaTWHPR *out, HANDLE ThreadIdleEvent=INVALID_HANDLE_VALUE);
	void __fastcall StopReceiving() {if(RecThrd) RecThrd->Stop();}
	void __fastcall StartReceiving() {if(RecThrd) RecThrd->Start();}
	void __fastcall ReCenter() {if(RecThrd) RecThrd->ReCenter();}
	void __fastcall SetOffset(double X, double Y) {if(RecThrd) RecThrd->SetOffset(X, Y);}
	void __fastcall AddOffset(double X, double Y) {if(RecThrd) RecThrd->AddOffset(X, Y);}
	void __fastcall ApplyControlsRedraw();
	//TNmeaSentenceType GetCoordinate();

	__property TNmeaClientsPool* Clients = {read=clients};
};

#endif
