// ---------------------------------------------------------------------------

#include <vcl.h>
#include <comdef.h>
#pragma hdrstop

#include "Settings.h"
#include "PalettesRepository.h"
#include "Radar.h"
#include "LoadPlugins.h"
#include "PlugIns.h"
#include "OptionsPlugins.h"
#include "OptionsGpsUnit.h"
#include "Disclaimer.h"
#include "Defines.h"
#include <registry.hpp>
#include "TwoWheelsUnit.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "GR32_Image"
#pragma link "ImageButton"
#pragma resource "*.dfm"
TSettingsForm *SettingsForm;
// ---------------------------------------------------------------------------

/*void __fastcall CheckImage(TObject *Sender)
{
	if(Sender)
	{
		((TImage*)Sender)->Stretch = true;
		if(SettingsForm) ((TImage*)Sender)->Picture->Assign(SettingsForm->CheckedImage->Picture);
	}
}
// ---------------------------------------------------------------------------

void __fastcall UncheckImage(TObject *Sender)
{
	if(Sender)
	{
		((TImage*)Sender)->Stretch = false;
		if(SettingsForm) ((TImage*)Sender)->Picture->Assign(SettingsForm->UncheckedImage->Picture);
	}
}
// ---------------------------------------------------------------------------

void __fastcall CheckUncheckImage(TObject *Sender)
{
	if(Sender)
	{
		if(((TImage*)Sender)->Stretch) UncheckImage(Sender);
		else CheckImage(Sender);
	}
}
// ---------------------------------------------------------------------------

void __fastcall CheckRadioImagesOnPanel(TObject *Sender)
{
	int z =((TComponent *)Sender)->Owner->ComponentCount;

	for(int i = 0; i < z; i++)
	{
		if(((TComponent *)Sender)->Owner->Components[i]->ClassName()== "TImage" && ((TComponent *)Sender)->Owner->Components[i]->Tag ==
			((TComponent *)Sender)->Tag && ((TComponent *)Sender)->Owner->Components[i]!= Sender)
		{
			((TImage*)((TComponent *)Sender)->Owner->Components[i])->Stretch = false;
			((TImage*)((TComponent *)Sender)->Owner->Components[i])->Picture->Assign(UncheckedImage->Picture);
		}
	}
	((TImage*)Sender)->Stretch = true;
	((TImage*)Sender)->Picture->Assign(CheckedImage->Picture);
}*/

// ---------------------------------------------------------------------------
// TSettingsForm
// ---------------------------------------------------------------------------
__fastcall TSettingsForm::TSettingsForm(TComponent* Owner) : TForm(Owner)
{
	Manager = NULL;
	SelectedPaletteItem = NULL;
	PaletteItems = new TPaletteItem[PaletteMaxColors];
	PaletteItems[0] = TPaletteItem(1, 0, clYellow, ColorPanel1, PaletteCheckImage1, PaletteCanImage1, NULL, Label89, Shape1);
	PaletteItems[1] = TPaletteItem(2, 64, clRed, ColorPanel2, PaletteCheckImage2, PaletteCanImage2, PaletteCanCoverImage2, Label100, Shape2);
	PaletteItems[2] = TPaletteItem(3, 64, clWhite, ColorPanel3, PaletteCheckImage3, PaletteCanImage3, PaletteCanCoverImage3, Label90, Shape3);
	PaletteItems[3] = TPaletteItem(4, 64, clWhite, ColorPanel4, PaletteCheckImage4, PaletteCanImage4, PaletteCanCoverImage4, Label99, Shape4);
	PaletteItems[4] = TPaletteItem(5, 128, clLime, ColorPanel5, PaletteCheckImage5, PaletteCanImage5, PaletteCanCoverImage5, Label91, Shape5);
	PaletteItems[5] = TPaletteItem(6, 128, clWhite, ColorPanel6, PaletteCheckImage6, PaletteCanImage6, PaletteCanCoverImage6, Label98, Shape6);
	PaletteItems[6] = TPaletteItem(7, 128, clWhite, ColorPanel7, PaletteCheckImage7, PaletteCanImage7, PaletteCanCoverImage7, Label92, Shape7);
	PaletteItems[7] = TPaletteItem(8, 128, clWhite, ColorPanel8, PaletteCheckImage8, PaletteCanImage8, PaletteCanCoverImage8, Label93, Shape8);
	PaletteItems[8] = TPaletteItem(9, 192, clBlue, ColorPanel9, PaletteCheckImage9, PaletteCanImage9, PaletteCanCoverImage9, Label94, Shape9);
	PaletteItems[9] = TPaletteItem(10, 255, clBlack, ColorPanel10, PaletteCheckImage10, PaletteCanImage10, NULL, Label95, Shape10);
	Palette = NULL;

	Caption = (AnsiString)_RadarMapName + " Settings";
	Label82->Caption = _RadarMapWeb;

	CRS_GUIDs = new TStringList();
	Pos_GUIDs = new TStringList();

	WriteCommand = NULL;
	PauseTerminal = NULL;
	ResumeTerminal = NULL;

	Panel9MousePressed = false;
	Panel9Y = -1;

	//int bt=OnlineButton->Top+70;
	int bt=MultiFileMapButton->Top+70;
#ifdef _AutoSaveXYZCRestriction
	Label114->Visible = false;
	AutoStopProfileImage->Visible = false;
	Label127->Visible = false;
	Panel44->Visible = false;
	AutoSaveImage->Visible = false;
	Panel45->Visible = false;
	AutosaveBrowseImage->Visible = false;
	XYZCButton->Visible=false;
#else
	XYZCButton->Top=bt;
	XYZCButton->Visible=true;
	bt+=70;
#endif

#ifndef _PipeRecognition
	ProfilePipesDetectorImage->Visible=false;
	Label132->Visible=false;
#else
	ProfilePipesDetectorImage->Visible=true;
	Label132->Visible=true;
#endif

#ifdef _InfraRadarOnly
	GainButton->Top=bt;
	GainButton->Visible=true;
	bt+=70;
#else
	GainButton->Visible=false;
#endif

#ifdef _SPAR
	SPARButton->Top=bt;
	SPARButton->Visible=true;
	bt+=70;
#else
	SPARButton->Visible=false;
#endif

#ifdef RadarMap_TRIAL
	TrialLabel->Visible=true;
#else
	TrialLabel->Visible=false;
#endif

	AboutButton->Top=bt;
	AboutButton->Visible=true;
}
// ---------------------------------------------------------------------------

__fastcall TSettingsForm::~TSettingsForm()
{
	CRS_GUIDs->Clear();
	delete CRS_GUIDs;
	Pos_GUIDs->Clear();
	delete Pos_GUIDs;
	delete PaletteItems;
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::WheelImageForceClick(TObject *Sender)
{
	((TImageButton*)Sender)->GroupToggle();
	SetPositioningControls();
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::WheelImageClick(TObject *Sender)
{
	((TImageButton*)Sender)->GroupToggle();
	if(!((TImageButton*)Sender)->Down)
	{
		if(Sender != WheelImage) WheelImage->GroupToggle();
		else ManualImage->GroupToggle();
	}
	SetPositioningControls();
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::SetPositioningControls()
{
	Label5->Enabled = WheelImage->Down;
	//Label6->Enabled = WheelImage->Down;
	Label7->Enabled = WheelImage->Down;
	// Label8->Enabled=WheelImage->Down;
	// Label9->Enabled=WheelImage->Down;
	StepEdit->Enabled = WheelImage->Down;
	QEdit->Enabled = WheelImage->Down;
	// DxEdit->Enabled=WheelImage->Down;

	TwoWheelSettingsButton->Enabled=TwoWheelImage->Down;
	WPSasWheelImage->Enabled=TwoWheelImage->Down;
	Label167->Enabled=TwoWheelImage->Down;

	Label137->Enabled = TwoWheelImage->Down;
	Label138->Enabled = TwoWheelImage->Down;
	Label139->Enabled = TwoWheelImage->Down;
	Label141->Enabled = TwoWheelImage->Down;
	TwoWheelPortCBox->Enabled = TwoWheelImage->Down;
	TwoWheelSpeedCBox->Enabled = TwoWheelImage->Down;
	TwoWheelRtsImage->Enabled = TwoWheelImage->Down;
	UseGpsAsStartImage->Enabled = TwoWheelImage->Down;

	PosPlugInButton->Enabled = PosPlugInImage->Down;
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::Label1Click(TObject *Sender)
{
	WheelImageClick(WheelImage);
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::Label2Click(TObject *Sender)
{
	WheelImageClick(ManualImage);
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::CollectSettings(bool TryToApply)
{
	settings.GeoradarTCPAddr = GPRAddrEdit->Text;
	if(WheelImage->Down) settings.WheelPositioning = TPositioning::Wheel;
	else if(ManualImage->Down) settings.WheelPositioning = TPositioning::ManualP;
	else if(TwoWheelImage->Down) settings.WheelPositioning = TPositioning::TwoWheel;
	else if(PosPlugInImage->Down) settings.WheelPositioning = TPositioning::PosDLL;
	settings.WPSasWheel = WPSasWheelImage->Down;
	if(PosPlugInCBox->ItemIndex >= 0) PlugInManager->SetActive(Pos_GUIDs->Strings[PosPlugInCBox->ItemIndex], pitPos);
	else PlugInManager->ClearActive(pitPos);
	try {settings.WheelDiameter = StrToFloat(StepEdit->Text) / 1000.;} catch(Exception & e) {}
	try {settings.WheelPulses = StrToInt(QEdit->Text);} catch(Exception & e) {}
	try {settings.WheelTraceDistance = StrToFloat(DxEdit->Text);} catch(Exception & e) {}
	try {settings.Permitivity = StrToFloat(PermitEdit->Text);} catch(Exception & e) {}
	settings.BatteryType = (TBatteryType)BatteryTypeCBox->ItemIndex;
	settings.WeightedSubtractionFilter = WeightedSubtractionFilterImage->Down;
	try {settings.WSWindowWidth = StrToInt(WSWindowWidthEdit->Text);} catch(Exception & e) {}
	settings.MoveoutCorrection = MoveoutCorrectionImage->Down;
	settings.AutoStopProfile = AutoStopProfileImage->Down;
	try {settings.AutoStopProfileSize = StrToInt(AutoStopProfileEdit->Text);} catch(Exception & e) {}
	settings.AutoSaveProfile = AutoSaveImage->Down;
	settings.AutoCloseProfile = AutoCloseImage->Down;
	settings.AutoSaveDir = AutoSaveDirEdit->Text;
	settings.MapObjectsPrediction = PredictionIButton->Down;
	try {settings.MapObjectsPredictingRadius = StrToFloat(PredictionRadiusEdit->Text);} catch(Exception & e) {}
	try {settings.LabelDestructionTimeByClick = (StrToFloat(PredictionTimeoutEdit->Text)+0.0001)*1000.;} catch(Exception & e) {}
	try {settings.LabelDestructionTimeByTrack = (StrToFloat(PredictionTimeoutEdit->Text)+0.0001)*1000.;} catch(Exception & e) {}
	if(PortCBox->Text != "") settings.GPS.ComPort = PortCBox->Text;
	try {settings.GPS.PortSpeed = StrToInt(SpeedCBox->Text);} catch(Exception & e) {}
	if(GainButton->Visible) settings.ApplyGain = ApplyGainImage->Down;
	else settings.ApplyGain = true;
	settings.GPS.RTSCTSEnable = RTSCTSImage->Down;
	settings.GPS.SimTieCoordinatesToMapCenter = GpsTieCenterImage->Down;
	settings.GPS.SimTcpSource = GpsSimTcpImage->Down;
	settings.GPS.SimFileSource = GpsSimFileImage->Down;
	settings.GPS.UseZGeoid = GpsUseZGeoidImage->Down;
	settings.GPS.LogToFile = GpsLogToFileImage->Down;
	settings.GPS.UnitName = GpsUtitCBox->Text;
	settings.GPS.UnitFileName = settings.GetGpsUnitFileName(settings.GPS.UnitName);

	if(TwoWheelPortCBox->Text != "") settings.TwoWheelsDRS.ComPort = TwoWheelPortCBox->Text;
	try {settings.TwoWheelsDRS.PortSpeed = StrToInt(TwoWheelSpeedCBox->Text);} catch(Exception & e) {}
	settings.TwoWheelsDRS.RTSCTSEnable = TwoWheelRtsImage->Down;
	settings.TwoWheelsDRS.SimTieCoordinatesToMapCenter = GpsTieCenterImage->Down;
	settings.TwoWheelsDRS.SimTcpSource = false;
	//settings.TwoWheelsDRS.SimFileSource = GpsSimFileImage->Down;
	settings.TwoWheelsDRS.UseZGeoid = true;
	settings.TwoWheelsDRS.LogToFile = false;
	//settings.TwoWheelsDRS.UnitFileName = settings.GetGpsUnitFileName(settings.TwoWheelsDRS.UnitName);
	settings.TwoWheelsDRS.UseGpsForStart = UseGpsAsStartImage->Down;

	settings.MapPreview = MapPreviewImage->Down;
	settings.EasyRendering = EasyRenderingImage->Down;
	settings.GPSTracking = GpsTrackingImage->Down;
	settings.FollowMe = FollowMeImage->Down;
	settings.CommunicationLinesDetector = LinesDetectionImage->Down;
	settings.ProfilePipesDetector = ProfilePipesDetectorImage->Down;
	settings.EasyZoomIn = EasyZoomInImage->Down;
	settings.ShowBattery = BatteryImage->Down;
	// settings.GPS.UnitType=GpsUtitCBox->ItemIndex;

	if(DefaultCoordinateSystemCBox->ItemIndex < csTotalFixed)
		settings.DefaultCoordinateSystem =(TCoordinateSystem)DefaultCoordinateSystemCBox->ItemIndex;
	else if(DefaultCoordinateSystemCBox->ItemIndex != DefaultCoordinateSystemCBox->Items->Count - 1)
	{
		if(DefaultCoordinateSystemCBox->ItemIndex - csTotalFixed < CRS_GUIDs->Count)
		{
			PlugInManager->SetActive(CRS_GUIDs->Strings[DefaultCoordinateSystemCBox->ItemIndex - csTotalFixed], pitCRS);
			settings.DefaultCoordinateSystem = csDLL;
		}
	}
	try
	{
		settings.GPS.AntennaHeight = StrToFloat(AntennaHeightEdit->Text);
	}
	catch(Exception & e) {}

	settings.DXFBackgroundColor = Color32(DXFBackgroundColorPanel->Color);
	settings.DXFColorInvertThreshold = StrToInt(DXFColorInvertThresholdEdit->Text);
	settings.DXFIncludeMap = DXFMapExportImage->Down;
	settings.DXFUsePitch = DXFPitchImage->Down;
	settings.DXFUseRoll = DXFRollImage->Down;
	settings.DXFConfidence = DXFConfidenceImage->Down;
	settings.DXFDescriptionAsText = DXFDescriptionAsTextImage->Down;
	settings.DXFDepthToDescription = DXFDepthToDescriptionImage->Down;
	settings.DXFIdToDescription = DXFIdToDescriptionImage->Down;
	settings.DXFPolyline3D = DXFPolyline3DImage->Down;
	try
	{
		settings.DXFDescriptionTextHeight = StrToFloat(TextHeightSEdit->Text);
	}
	catch(Exception & e) {}

	// ---- Update BingOnlineMap information ---------------------------------
	settings.BingInetMapAppKey = txtBingInetMapAppKey->Text;

	if (this->rbAerial->Down) settings.BingInetMapViewType = "Satellite";
	else if (this->rbAerialLabels->Down) settings.BingInetMapViewType = "SatelliteLabels";
	else settings.BingInetMapViewType = "Map";

	try
	{
		settings.BingInetMapDefaultZoom = ZoomTrackBar->Position;
	}
	catch(Exception & e) {}

	settings.SparEnable = SparEnableImage->Down;
	settings.SparSimulation = SparSimulationImage->Down;
	settings.SparUseBaseline = SparUseBaselineImage->Down;
	settings.SparShell = SparShellEdit->Text;
	try
	{
		settings.SparShellWait = StrToInt(SparShellWaitEdit->Text)* 1000;
	}
	catch(Exception & e) {}
	settings.SparCOMPortNumber = SparCOMPortNumberCBox->ItemIndex + 1;
	try
	{
		settings.SparFrequency = StrToInt(SparFrequencyCBox->Text);
	}
	catch(Exception & e) {}
	settings.SparAverageTime = SparAverageTimeCBox->ItemIndex;
	settings.SparAverageType = SparAverageTypeCBox->ItemIndex;
	settings.SparUpdateRate = SparUpdateRateCBox->ItemIndex;
	try
	{
		settings.SparElevation = StrToFloat(SparElevationEdit->Text);
	}
	catch(Exception & e) {}
	try
	{
		settings.SparToAntennaFwd = StrToFloat(SparToAntennaFwdEdit->Text);
	}
	catch(Exception & e) {}
	try
	{
		settings.SparToAntennaRight = StrToFloat(SparToAntennaRightEdit->Text);
	}
	catch(Exception & e) {}
	try
	{
		settings.SparToAntennaUp = StrToFloat(SparToAntennaUpEdit->Text);
	}
	catch(Exception & e) {}
	try
	{
		settings.SparConfidenceMax = StrToInt(SparConfidenceMaxEdit->Text);
	}
	catch(Exception & e) {}
	try
	{
		settings.SparMinDbField = StrToInt(SparMinDbFieldEdit->Text);
	}
	catch(Exception & e) {}
	settings.SparAnglesCompensation = CompensationImage->Down;
	try
	{
		settings.SparPitchCompensation = StrToFloat(PitchCompensationEdit->Text);
	}
	catch(Exception & e) {}
	try
	{
		settings.SparRollCompensation = StrToFloat(RollCompensationEdit->Text);
	}
	catch(Exception & e) {}
	try
	{
		settings.SparHeadingCompensation = StrToFloat(HeadingCompensationEdit->Text);
	}
	catch(Exception & e) {}
	try
	{
		settings.XYZCCellWidth = StrToFloat(XYZCCellWidthEdit->Text);
	}
	catch(Exception & e) {}
	try
	{
		settings.XYZCCellHeight = StrToFloat(XYZCCellHeightEdit->Text);
	}
	catch(Exception & e) {}
	settings.XYZCUseZ = XYZCUseZImage->Down;
	settings.XYZCUsePitch = XYZCUsePitchImage->Down;
	settings.XYZCUseRoll = XYZCUseRollImage->Down;
	settings.XYZCGained = XYZCGainImage->Down;
	settings.XYZCAbsolute = XYZCAbsoluteImage->Down;
	settings.XYZCEnvelope = XYZCEnvelopeImage->Down;
	settings.ShowRecycleBin = RecycleBinImage->Down;
	settings.ShowGyroCompass = GyrocompassImage->Down;
	settings.SmallSizeControls =!LargeControlsImage->Down;
	settings.ShowLabelIcons = ShowLabelIconsImage->Down;
	settings.CopyAttachments = CopyAttachmentsImage->Down;
	settings.ShowLabelInformationOnPin = ShowLabelOnPinImage->Down;
	settings.ShowLabelConfidence = ShowLabelConfidenceImage->Down;
	settings.TraceTransparency = TrackBar2->Position;
	settings.ShowAnnotationLayerOnOpen =!HideAnnotationLayerOnOpenImage->Down;
	settings.EnlargeEmptyMap = EnlargeEmptyMapImage->Down;
	try
	{
		settings.EmptyMapSize = StrToInt(EmptyMapSizeEdit->Text);
	}
	catch(Exception & e) {}

	if(TryToApply && Manager && !Manager->Connection)
	{
		Manager->ApplyNewSettings(settings);
		Palette->SavePaletteToRegistry((AnsiString)((AnsiString)_RadarMapRegKey + "\\Palettes").c_str(), "DefaultPalette");
	}
	if(GainItems) GainItems->ApplyGainFunction();
	settings.DbStoreLayersVisability = DbStoreLayersVisabilityIButton->Down;
	settings.DbShowLayerFileName = DbShowLayerFileNameIButton->Down;
	settings.DbMergeLayersWithSameName = DbMergeLayersWithSameNameIButton->Down;
	settings.SingleTypeMapLabelsGeometry = SingleTypeIButton->Down;

	if(rpoAutoIButton->Down) settings.PanelsOrientation = TRadarPanelsOrientation::rpoAuto;
	else if(rpoHorizontalIButton->Down) settings.PanelsOrientation = TRadarPanelsOrientation::rpoHorizontal;
	else settings.PanelsOrientation = TRadarPanelsOrientation::rpoVertical;
	if(RMMaximizedIButton->Down) settings.WindowState = TWindowState::wsMaximized;
	else settings.WindowState = TWindowState::wsNormal;
}

void __fastcall TSettingsForm::Image1Click(TObject *Sender)
{
	CollectSettings(false);

	if(RadarMapManager)
	{
		if(RadarMapManager->ReceivingSettings)
			RadarMapManager->ReceivingSettings->SaveSettings();
		// RadarMapManager->CloseConnection();
	}
	ModalResult = mrOk;
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::StepEditKeyPress(TObject *Sender, wchar_t &Key)
{
	switch(Key)
	{
		case '0':
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
		case 8:
			break;
		default:
			Key = 0;
	}
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::DxEditKeyPress(TObject *Sender, wchar_t &Key)
{
	switch(Key)
	{
		case '0':
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
		case 8:
			break;
		case '.':
		case ',':
			if(((TEdit*)Sender)->Text.Pos(DecimalSeparator)> 0)
				Key = 0;
			else
				Key = DecimalSeparator;
			break;
		default:
			Key = 0;
	}
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::GPRAddrEditExit(TObject *Sender)
{
	try
	{
		String IP = ((TMaskEdit*)Sender)->Text;
		int net1 = IP.SubString(0, 3).TrimRight().ToInt();
		int net2 = IP.SubString(5, 3).TrimRight().ToInt();
		int host1 = IP.SubString(9, 3).TrimRight().ToInt();
		int host2 = IP.SubString(13, 3).TrimRight().ToInt();
		// a range test that you cannot validate through edit masks
		if (net1 < 0 || net1 > 255 || net2 < 0 || net2 > 255 || host1 < 0 || host1 > 255 || host2 < 0 || host2 > 255)
			throw(Exception("Not a valid IP address."));
	}
	catch(Exception & e)
	{
		ShowMessage("Not a valid IP address.");
		((TMaskEdit*)Sender)->SetFocus();
	}
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::GpsSimTcpImageClick(TObject *Sender)
{
	((TImageButton*)Sender)->Toggle();
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::Label11Click(TObject *Sender)
{
	GpsSimTcpImage->Toggle();
	if(GpsSimTcpImage->Down)
	{
		GpsSimFileImage->Down = false;
		GpsUtitCBox->Enabled = false;
		PortCBox->Enabled = false;
		SpeedCBox->Enabled = false;
	}
	else
	{
		GpsUtitCBox->Enabled = true;
		PortCBox->Enabled = true;
		SpeedCBox->Enabled = true;
	}
	CollectSettings(true);
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::Label12Click(TObject *Sender)
{
	GpsSimFileImage->Toggle();
	if(GpsSimFileImage->Down)
	{
		GpsSimTcpImage->Down = false;
		GpsUnitIButton->Enabled = false;
		GpsUtitCBox->ItemIndex = 0;
		GpsUtitCBox->Enabled = false;
		PortCBox->Enabled = false;
		SpeedCBox->Enabled = false;
	}
	else {
		if(GpsUtitCBox->ItemIndex > 0 && GpsUtitCBox->ItemIndex != GpsUtitCBox->Items->Count - 1)
			GpsUnitIButton->Enabled = true;
		else
			GpsUnitIButton->Enabled = false;
		GpsUtitCBox->Enabled = true;
		PortCBox->Enabled = true;
		SpeedCBox->Enabled = true;
	}
	CollectSettings(true);
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::Label13Click(TObject *Sender)
{
	GpsTieCenterImage->Toggle();
	CollectSettings(true);
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::Label19Click(TObject *Sender)
{
	LinesDetectionImage->Toggle();
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::Label18Click(TObject *Sender)
{
	EasyRenderingImage->Toggle();
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::Label17Click(TObject *Sender)
{
	EasyZoomInImage->Toggle();
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::Label16Click(TObject *Sender)
{
	FollowMeImage->Toggle();
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::Label15Click(TObject *Sender)
{
	GpsTrackingImage->Toggle();
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::Label14Click(TObject *Sender)
{
	MapPreviewImage->Toggle();
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::FormShow(TObject *Sender)
{
	TGPSCoordinate *c;
	TPlugInObject *plugin;
	TListItem *Item;
	int i;

	SelectedPaletteItem = NULL;
	PaletteCanImage1Click(PaletteCanImage1);
	RebuildPaletteImage();
	Memo1Change(Sender);
	Notebook1->PageIndex = 0;
	GPRAddrEdit->Text = settings.GeoradarTCPAddr;
	StepEdit->Text = IntToStr((int)(settings.WheelDiameter * 1000. + 0.5));
	QEdit->Text = IntToStr(settings.WheelPulses);
	DxEdit->Text = FloatToStrF(settings.WheelTraceDistance, ffFixed, 10, 2);
	PermitEdit->Text = FloatToStrF(settings.Permitivity, ffFixed, 10, 2);
	BatteryTypeCBox->ItemIndex =(int)settings.BatteryType;
	WeightedSubtractionFilterImage->Down = settings.WeightedSubtractionFilter;
	WSWindowWidthEdit->Text = settings.WSWindowWidth;
	MoveoutCorrectionImage->Down = settings.MoveoutCorrection;
	AutoStopProfileImage->Down=settings.AutoStopProfile;
	AutoStopProfileEdit->Text = settings.AutoStopProfileSize;
	AutoSaveImage->Down=settings.AutoSaveProfile;
	AutoCloseImage->Down=settings.AutoCloseProfile;
	AutoSaveDirEdit->Text = settings.AutoSaveDir;
	ApplyGainImage->Down=settings.ApplyGain;

	PredictionIButton->Down = settings.MapObjectsPrediction;
	PredictionRadiusEdit->Text = FloatToStrF(settings.MapObjectsPredictingRadius, ffFixed, 10, 2);
	PredictionTimeoutEdit->Text = FloatToStrF((float)settings.LabelDestructionTimeByTrack/1000.+0.0001, ffFixed, 10, 1);
	PredictionTimeoutEdit->Text = FloatToStrF((float)settings.LabelDestructionTimeByClick/1000.+0.0001, ffFixed, 10, 1);

	PortCBox->ItemIndex = PortCBox->Items->IndexOf(settings.GPS.ComPort);
	SpeedCBox->ItemIndex = SpeedCBox->Items->IndexOf(IntToStr(settings.GPS.PortSpeed));
	GpsTieCenterImage->Down=settings.GPS.SimTieCoordinatesToMapCenter;
	GpsSimTcpImage->Down=settings.GPS.SimTcpSource;
	GpsSimFileImage->Down=settings.GPS.SimFileSource;
	RTSCTSImage->Down=settings.GPS.RTSCTSEnable;
	GpsUseZGeoidImage->Down=settings.GPS.UseZGeoid;
	GpsLogToFileImage->Down=settings.GPS.LogToFile;

	TwoWheelPortCBox->ItemIndex = TwoWheelPortCBox->Items->IndexOf(settings.TwoWheelsDRS.ComPort);
	TwoWheelSpeedCBox->ItemIndex = TwoWheelSpeedCBox->Items->IndexOf(IntToStr(settings.TwoWheelsDRS.PortSpeed));
	TwoWheelRtsImage->Down=settings.TwoWheelsDRS.RTSCTSEnable;
	UseGpsAsStartImage->Down=settings.TwoWheelsDRS.UseGpsForStart;

	if(GpsSimTcpImage->Down || GpsSimFileImage->Down)
	{
		GpsUtitCBox->Enabled = false;
		PortCBox->Enabled = false;
		SpeedCBox->Enabled = false;
	}
	else
	{
		GpsUtitCBox->Enabled = true;
		PortCBox->Enabled = true;
		SpeedCBox->Enabled = true;
	}
	MapPreviewImage->Down=settings.MapPreview;
	EasyRenderingImage->Down=settings.EasyRendering;
	GpsTrackingImage->Down=settings.GPSTracking;
	FollowMeImage->Down=settings.FollowMe;
	LinesDetectionImage->Down=settings.CommunicationLinesDetector;
	ProfilePipesDetectorImage->Down=settings.ProfilePipesDetector;
	EasyZoomInImage->Down=settings.EasyZoomIn;
	BatteryImage->Down=settings.ShowBattery;

	GpsUtitCBox->Items->Clear();
	GpsUtitCBox->Items->Add("NMEA GPS Receiver");

	TRegistry& regKey =*new TRegistry();
	TRegistry& regKey2 =*new TRegistry();
	AnsiString regkey = _RadarMapRegKey +(AnsiString)_GpsUnitsRegKey;
	AnsiString str = "";
	TStrings *strs = new TStringList();

	try {
		try {
			if(regKey.OpenKey(regkey, false)) {
				regKey.GetKeyNames(strs);
				if(strs->Count > 0)
					for(i = 0; i < strs->Count; i++)
						if(strs->Strings[i]!= "" && strs->Strings[i]!= "Add..." && regKey2.OpenKey(regkey + "\\" + strs->Strings[i],
								false) && regKey2.ValueExists("FileName") && FileExists(regKey2.ReadString("FileName"))) {
							GpsUtitCBox->Items->Add(strs->Strings[i]);
							regKey2.CloseKey();
						}
			}
		}
		catch(Exception & e) {
		}
	}
	__finally {
		delete &regKey2;
		regKey.CloseKey();
		delete &regKey;
		delete strs;
	}
	GpsUtitCBox->Items->Add("Add...");
	i = GpsUtitCBox->Items->IndexOf(settings.GPS.UnitName);
	if(i < 0 || i == GpsUtitCBox->Items->Count - 1)
		i = 0;
	GpsUtitCBox->ItemIndex = i;
	if(GpsUtitCBox->ItemIndex > 0 && GpsUtitCBox->ItemIndex != GpsUtitCBox->Items->Count - 1)
		GpsUnitIButton->Enabled = true;
	else
		GpsUnitIButton->Enabled = false;

	DelPlugInBtn->Enabled = false;
	SettingsPlugInBtn->Enabled = false;
	CRS_GUIDs->Clear();
	Pos_GUIDs->Clear();
	PlugInLView->Items->Clear();
	DefaultCoordinateSystemCBox->Items->Clear();
	PosPlugInCBox->Items->Clear();
	c = new TGPSCoordinate();
	try {
		for(int i = 0; i < csTotalFixed; i++) {
			DefaultCoordinateSystemCBox->Items->Add(c->GetCSName((TCoordinateSystem)i)+ " (" + c->GetCSEllipsoid((TCoordinateSystem)i)
				+ ")");
		}
		if(PlugInManager && PlugInManager->PlugInsList->Count > 0)
		{
			for(int i = 0; i < PlugInManager->PlugInsList->Count; i++)
			{
				if(PlugInManager->PlugInsList->Items[i])// && PlugInManager->PlugInsList->Items[i]->Type==pitCRS)
				{
					plugin = PlugInManager->PlugInsList->Items[i];
					Item = PlugInLView->Items->Add();
					Item->Caption = plugin->Name;
					switch(plugin->Type) {
					case pitCRS:
						Item->ImageIndex =(int)plugin->Type;
						Item->SubItems->Add("CRS");
						DefaultCoordinateSystemCBox->Items->Add(plugin->Name);
						CRS_GUIDs->Add(plugin->GUID);
						break;
					case pitPos:
						Item->ImageIndex =(int)plugin->Type;
						Item->SubItems->Add("POS");
						PosPlugInCBox->Items->Add(plugin->Name);
						Pos_GUIDs->Add(plugin->GUID);
						break;
					default:
						Item->ImageIndex = 0;
						Item->SubItems->Add("UNK");
					}
					Item->SubItems->Add(plugin->GUID);
					Item->SubItems->Add(ExtractFileName(plugin->DLLFileName)+ ", " + IntToStr(plugin->PlugInID));
				}
			}
		}
		DefaultCoordinateSystemCBox->Items->Add("Add...");
		PosPlugInCBox->Items->Add("Add...");
	}
	__finally {
		delete c;
	}
	if(settings.DefaultCoordinateSystem < csTotalFixed)
		DefaultCoordinateSystemCBox->ItemIndex = settings.DefaultCoordinateSystem;
	else if(PlugInManager && PlugInManager->ActiveCS)
		DefaultCoordinateSystemCBox->ItemIndex = CRS_GUIDs->IndexOf(PlugInManager->ActiveCS->GUID) + csTotalFixed;
	else DefaultCoordinateSystemCBox->ItemIndex = 0;
	if(PlugInManager && PlugInManager->ActivePos) PosPlugInCBox->ItemIndex = Pos_GUIDs->IndexOf(PlugInManager->ActivePos->GUID);

	if(settings.WheelPositioning == TPositioning::Wheel) WheelImageForceClick(WheelImage);
	else if(settings.WheelPositioning == PosDLL && PlugInManager && PlugInManager->ActivePos) WheelImageForceClick(PosPlugInImage);
	else if(settings.WheelPositioning == TPositioning::TwoWheel) WheelImageForceClick(TwoWheelImage);
	else WheelImageForceClick(ManualImage);
	WPSasWheelImage->Down = settings.WPSasWheel;

	AntennaHeightEdit->Text = FloatToStrF(settings.GPS.AntennaHeight, ffFixed, 10, 2);
	DXFBackgroundColorPanel->Color = WinColor(settings.DXFBackgroundColor);
	DXFColorInvertThresholdEdit->Text = IntToStr(settings.DXFColorInvertThreshold);
	DXFMapExportImage->Down = settings.DXFIncludeMap;
	DXFPitchImage->Down = settings.DXFUsePitch;
	DXFRollImage->Down = settings.DXFUseRoll;
	DXFConfidenceImage->Down = settings.DXFConfidence;
	DXFDescriptionAsTextImage->Down = settings.DXFDescriptionAsText;
	DXFDepthToDescriptionImage->Down = settings.DXFDepthToDescription;
	DXFIdToDescriptionImage->Down = settings.DXFIdToDescription;
	DXFPolyline3DImage->Down = settings.DXFPolyline3D;
	TrackBar1->Position =(int)(settings.DXFDescriptionTextHeight * 10.0);

	// ---- Init fields using BingOnlineMap information ------------------------
	txtBingInetMapAppKey->Text = settings.BingInetMapAppKey;

	if (settings.BingInetMapViewType.LowerCase() == "satellite") rbAerial->Down = true;
	else if (settings.BingInetMapViewType.LowerCase() == "satellitelabels") rbAerialLabels->Down = true;
	else rbMap->Down = true;

	// Max definition must be before Min
	ZoomTrackBar->Position = settings.BingInetMapDefaultZoom;
	ZoomLabel->Caption="x"+IntToStr(ZoomTrackBar->Position);

	SparEnableImage->Down=settings.SparEnable;
	SparSimulationImage->Down=settings.SparSimulation;
	SparUseBaselineImage->Down=settings.SparUseBaseline;
	SparShellEdit->Text = settings.SparShell;
	SparShellWaitEdit->Text = IntToStr((int)(settings.SparShellWait / 1000));
	SparCOMPortNumberCBox->ItemIndex = settings.SparCOMPortNumber - 1;
	SparFrequencyCBox->ItemIndex = SparFrequencyCBox->Items->IndexOf(IntToStr((int)(settings.SparFrequency + 0.5)));
	SparAverageTimeCBox->ItemIndex = settings.SparAverageTime;
	SparAverageTypeCBox->ItemIndex = settings.SparAverageType;
	SparUpdateRateCBox->ItemIndex = settings.SparUpdateRate;
	SparElevationEdit->Text = FloatToStrF(settings.SparElevation, ffFixed, 10, 2);
	SparToAntennaFwdEdit->Text = FloatToStrF(settings.SparToAntennaFwd, ffFixed, 10, 2);
	SparToAntennaRightEdit->Text = FloatToStrF(settings.SparToAntennaRight, ffFixed, 10, 2);
	SparToAntennaUpEdit->Text = FloatToStrF(settings.SparToAntennaUp, ffFixed, 10, 2);
	SparConfidenceMaxEdit->Text = IntToStr(settings.SparConfidenceMax);
	SparMinDbFieldEdit->Text = IntToStr(settings.SparMinDbField);
	CompensationImage->Down=settings.SparAnglesCompensation;
	PitchCompensationEdit->Text = FloatToStrF(settings.SparPitchCompensation, ffFixed, 10, 2);
	RollCompensationEdit->Text = FloatToStrF(settings.SparRollCompensation, ffFixed, 10, 2);
	HeadingCompensationEdit->Text = FloatToStrF(settings.SparHeadingCompensation, ffFixed, 10, 2);

	XYZCCellWidthEdit->Text = FloatToStrF(settings.XYZCCellWidth, ffFixed, 10, 2);
	XYZCCellHeightEdit->Text = FloatToStrF(settings.XYZCCellHeight, ffFixed, 10, 2);
	XYZCUseZImage->Down=settings.XYZCUseZ;
	XYZCUsePitchImage->Down=settings.XYZCUsePitch;
	XYZCUseRollImage->Down=settings.XYZCUseRoll;
	XYZCGainImage->Down=settings.XYZCGained;
	XYZCAbsoluteImage->Down=settings.XYZCAbsolute;
	XYZCEnvelopeImage->Down=settings.XYZCEnvelope;

	RecycleBinImage->Down=settings.ShowRecycleBin;
	GyrocompassImage->Down=settings.ShowGyroCompass;
	LargeControlsImage->Down=!settings.SmallSizeControls;
	ShowLabelIconsImage->Down=settings.ShowLabelIcons;
	CopyAttachmentsImage->Down=settings.CopyAttachments;
	ShowLabelOnPinImage->Down=settings.ShowLabelInformationOnPin;
	ShowLabelConfidenceImage->Down=settings.ShowLabelConfidence;
	HideAnnotationLayerOnOpenImage->Down=!settings.ShowAnnotationLayerOnOpen;
	EnlargeEmptyMapImage->Down=settings.EnlargeEmptyMap;
	EmptyMapSizeEdit->Text = IntToStr(settings.EmptyMapSize);

	if(RadarMapManager) {
#ifdef _InfraRadarOnly
		GainImage->Layers->Clear();
		GainImageGrid = new TOscilloscopeGrid(GainImage, NULL, 0, NULL);
		GainImageGrid->Frame = false;
		GainItems = new TGainItems(RadarMapManager, NULL, GainImage, NULL, true, false, TrackBar2, &(ApplyGainImage->Down));
		GainItems->AssignControls(1, GPImage1, GPImage1d);
		GainItems->AssignControls(2, GPImage2, GPImage2d);
		GainItems->AssignControls(3, GPImage3, GPImage3d);
		GainItems->AssignControls(4, GPImage4, GPImage4d);
		GainItems->AssignControls(5, GPImage5, GPImage5d);
		GainItems->AssignControls(6, GPImage6, GPImage6d);
		GainItems->AssignControls(7, GPImage7, GPImage7d);
		GainItems->AssignControls(8, GPImage8, GPImage8d);
		GainItems->AssignControls(9, GPImage9, GPImage9d);
		GainItems->AssignControls(10, GPImage10, GPImage10d);
		GainItems->AssignControls(gcLock, Image10, Image16);
		GainItems->AssignControls(gcNextPoint, Image13, NULL);
		GainItems->AssignControls(gcPrevPoint, Image7, NULL);
		GainItems->AssignControls(gcMoveLeft, Image8, NULL);
		GainItems->AssignControls(gcMoveRight, Image9, NULL);
		GainItems->AssignControls(gcMoveUp, Image12, NULL);
		GainItems->AssignControls(gcMoveDown, Image11, NULL);
#else
		GainImageGrid = NULL;
		GainItems = NULL;
#endif
	}
	else {
		GainImageGrid = NULL;
		GainItems = NULL;
	}

	TerminalPausedPanel->Visible = PauseTerminalIButton->Down;

	TitleDown=false;
	ToolbarNotebook->PageIndex=0;
	InfoBtnClick(GPRButton);

	HideCaret(GPRAddrEdit->Handle);

	ScrollBox1->VertScrollBar->Position=0;
	ScrollBox2->VertScrollBar->Position=0;
	ScrollBox3->VertScrollBar->Position=0;
	ScrollBox4->VertScrollBar->Position=0;
	ScrollBox5->VertScrollBar->Position=0;
	ScrollBox6->VertScrollBar->Position=0;
	OkCancelPanel->SetFocus();

	Label81->Caption =(AnsiString)_RadarMapCopyRight + "\nv" +(AnsiString)_RadarMapVersion;
	BuildDateLabel->Visible=false;
	BuildDateLabel->Caption="Build date is "+(AnsiString)_RadarMapBuildDate;
#ifdef _InfraRadarOnly
	Label103->Visible = true;
#else
	Label103->Visible = false;
#endif

	DbStoreLayersVisabilityIButton->Down = settings.DbStoreLayersVisability;
	DbShowLayerFileNameIButton->Down = settings.DbShowLayerFileName;
	DbMergeLayersWithSameNameIButton->Down = settings.DbMergeLayersWithSameName;

	SingleTypeIButton->Down = settings.SingleTypeMapLabelsGeometry;

	switch(settings.PanelsOrientation)
	{
		case rpoAuto: rpoAutoIButton->GroupToggle(); break;
		case rpoHorizontal: rpoHorizontalIButton->GroupToggle(); break;
		default:
		case rpoVertical: rpoVerticalIButton->GroupToggle(); break;
	}
	RMMaximizedIButton->Down = (settings.WindowState == wsMaximized);
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::DxEditExit(TObject *Sender)
{
	try
	{
		if(StrToFloat(DxEdit->Text)< 0.01)
			DxEdit->Text = "0.01";
		else if(StrToFloat(DxEdit->Text)> 0.5)
			DxEdit->Text = "0.5";
	}
	catch(Exception & e)
	{
		DxEdit->Text = "0.01";
	}
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::StepEditExit(TObject *Sender)
{
	try
	{
		if(StrToInt(StepEdit->Text)< 30)
			StepEdit->Text = "30";
		else if(StrToInt(StepEdit->Text)> 500)
			StepEdit->Text = "500";
	}
	catch(Exception & e)
	{
		StepEdit->Text = "128";
	}
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::PermitEditExit(TObject *Sender)
{
	try
	{
		if(StrToFloat(PermitEdit->Text)< 1)
			PermitEdit->Text = "1.00";
		else if(StrToFloat(PermitEdit->Text)> 100)
			PermitEdit->Text = "100.00";
	}
	catch(Exception & e)
	{
		PermitEdit->Text = "9.00";
	}
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::Label21Click(TObject *Sender)
{
	BatteryImage->Toggle();
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::SendImageClick(TObject *Sender)
{
	if(WriteCommand)
	{
		WriteCommand(SendEdit->Text.t_str());//_bstr_t(SendEdit->Text.t_str()));
		if(SendEdit->Text.Trim()!= "" && SendEdit->Items->IndexOf(SendEdit->Text)< 0)
			SendEdit->Items->Add(SendEdit->Text);
		SendEdit->Text = "";
	}
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::Memo1Change(TObject *Sender)
{
	HideCaret(Memo1->Handle);
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::SendEditKeyPress(TObject *Sender, wchar_t &Key)
{
	if(Key == VK_RETURN)
	{
		SendImageClick(Sender);
	}
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::Label24Click(TObject *Sender)
{
	RTSCTSImage->Toggle();
	CollectSettings(true);
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::Notebook1PageChanged(TObject *Sender)
{
	if(Notebook1->ActivePage == "Serial Terminal")
	{
		Memo1->Perform(EM_LINESCROLL, 0, Memo1->Lines->Count);
	}
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::GpsUtitCBoxChange(TObject *Sender)
{
	int j;
	AnsiString str;
	TXMLExplorer *XMLExplorer;

	if(GpsUtitCBox->ItemIndex == GpsUtitCBox->Items->Count - 1)
	{
		AnsiString FilterStr, TitleStr;

		OpenDialog->InitialDir = ExtractFilePath(Application->ExeName);
		FilterStr = OpenDialog->Filter;
		OpenDialog->Filter = "GPS Receiver setup commands list (*.xml)|*.xml";
		if(OpenDialog->Execute()) {
			if(FileExists(OpenDialog->FileName))
			{
				XMLExplorer = new TXMLExplorer(NULL, OpenDialog->FileName);
				try
				{
					if(XMLExplorer->FindTagStart("GpsUnit"))
					{
						if(XMLExplorer->FindTagAndGetContent("UnitName", str))
						{
							if(str == "" || GpsUtitCBox->Items->IndexOf(str)>= 0)
							{
								Application->MessageBox(L"This GPS Receiver is allready exists!", L"Add GPS Receiver...",
									MB_OK | MB_ICONWARNING);
							}
							else
							{
								TRegistry& regKey =*new TRegistry();
								AnsiString regkey = _RadarMapRegKey +(AnsiString)_GpsUnitsRegKey;

								try
								{
									try
									{
										if(regKey.OpenKey(regkey + "\\" + str, true))
										regKey.WriteString("FileName", OpenDialog->FileName);
										GpsUtitCBox->Items->Delete(GpsUtitCBox->Items->Count - 1); // delete last Add...
										j = GpsUtitCBox->Items->Add(str);
										GpsUtitCBox->Items->Add("Add...");
										GpsUtitCBox->ItemIndex = j;
									}
									catch(Exception & e) {
									}
								}
								__finally {
									regKey.CloseKey();
									delete &regKey;
								}
							}
						}
					}
				}
				__finally
				{
					delete XMLExplorer;
				}
			}
		}
		if(GpsUtitCBox->ItemIndex == GpsUtitCBox->Items->Count - 1)
			GpsUtitCBox->ItemIndex--;
	}
	if(GpsUtitCBox->ItemIndex > 0 && GpsUtitCBox->ItemIndex != GpsUtitCBox->Items->Count - 1)
		GpsUnitIButton->Enabled = true;
	else
		GpsUnitIButton->Enabled = false;
	CollectSettings(true);
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::DXFBackgroundColorPanelClick(TObject *Sender)
{
	ColorDialog1->Color = ((TPanel*)Sender)->Color;
	if(ColorDialog1->Execute()) ((TPanel*)Sender)->Color = ColorDialog1->Color;
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::Label26Click(TObject *Sender)
{
	DXFMapExportImage->Toggle();
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::DXFColorInvertThresholdEditExit(TObject *Sender)
{
	try
	{
		if(StrToInt(DXFColorInvertThresholdEdit->Text)< 0)
			DXFColorInvertThresholdEdit->Text = "0";
		else if(StrToInt(DXFColorInvertThresholdEdit->Text)> 255)
			DXFColorInvertThresholdEdit->Text = "255";
	}
	catch(Exception & e)
	{
		DXFColorInvertThresholdEdit->Text = "64";
	}
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::AntennaHeightEditExit(TObject *Sender)
{
	try
	{
		if(StrToFloat(AntennaHeightEdit->Text)< 0.00)
			AntennaHeightEdit->Text = "0.00";
		else if(StrToFloat(AntennaHeightEdit->Text)> 5.0)
			AntennaHeightEdit->Text = "5.0";
	}
	catch(Exception & e)
	{
		AntennaHeightEdit->Text = "1.5";
	}
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::SparShellWaitEditExit(TObject *Sender)
{
	try
	{
		if(StrToInt(SparShellWaitEdit->Text)< 1)
			SparShellWaitEdit->Text = "1";
		else if(StrToInt(SparShellWaitEdit->Text)> 30)
			SparShellWaitEdit->Text = "30";
	}
	catch(Exception & e)
	{
		SparShellWaitEdit->Text = "5";
	}
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::SparElevationEditExit(TObject *Sender)
{
	try
	{
		if(StrToFloat(SparElevationEdit->Text)< 0.3)
			SparElevationEdit->Text = "0.3";
		else if(StrToFloat(SparElevationEdit->Text)> 2)
			SparElevationEdit->Text = "2.0";
	}
	catch(Exception & e)
	{
		SparElevationEdit->Text = "0.5";
	}
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::SparToAntennaFwdEditExit(TObject *Sender)
{
	try
	{
		if(StrToFloat(SparToAntennaFwdEdit->Text)<-2)
			SparToAntennaFwdEdit->Text = "-2.0";
		else if(StrToFloat(SparToAntennaFwdEdit->Text)> 2)
			SparToAntennaFwdEdit->Text = "2.0";
	}
	catch(Exception & e)
	{
		SparToAntennaFwdEdit->Text = "0.0";
	}
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::SparToAntennaFwdEditKeyPress(TObject *Sender, wchar_t &Key)
{
	switch(Key)
	{
		case '0':
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
		case 8:
			return;
		case '.':
		case ',':
			if(((TEdit*)Sender)->Text.Pos(DecimalSeparator)> 0)
				Key = 0;
			else
				Key = DecimalSeparator;
			break;
		case '-':
			if(((TEdit*)Sender)->Text.Pos('-')> 0)
				Key = 0;
			break;
		default:
			Key = 0;
	}
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::SparToAntennaRightEditExit(TObject *Sender)
{
	try
	{
		if(StrToFloat(SparToAntennaRightEdit->Text)<-2)
			SparToAntennaRightEdit->Text = "-2.0";
		else if(StrToFloat(SparToAntennaRightEdit->Text)> 2)
			SparToAntennaRightEdit->Text = "2.0";
	}
	catch(Exception & e)
	{
		SparToAntennaRightEdit->Text = "0.0";
	}
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::SparToAntennaUpEditExit(TObject *Sender)
{
	try
	{
		if(StrToFloat(SparToAntennaUpEdit->Text)< 0)
			SparToAntennaUpEdit->Text = "0.0";
		else if(StrToFloat(SparToAntennaUpEdit->Text)> 2)
			SparToAntennaUpEdit->Text = "2.0";
	}
	catch(Exception & e)
	{
		SparToAntennaUpEdit->Text = "0.0";
	}
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::SparConfidenceMaxEditExit(TObject *Sender)
{
	try
	{
		if(StrToInt(SparConfidenceMaxEdit->Text)< 1)
			SparConfidenceMaxEdit->Text = "1";
		else if(StrToInt(SparConfidenceMaxEdit->Text)> 1000)
			SparConfidenceMaxEdit->Text = "1000";
	}
	catch(Exception & e)
	{
		SparConfidenceMaxEdit->Text = "50";
	}
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::SparMinDbFieldEditExit(TObject *Sender)
{
	try
	{
		if(StrToInt(SparMinDbFieldEdit->Text)< 0)
			SparMinDbFieldEdit->Text = "0";
		else if(StrToInt(SparMinDbFieldEdit->Text)> 100)
			SparMinDbFieldEdit->Text = "100";
	}
	catch(Exception & e)
	{
		SparMinDbFieldEdit->Text = "20";
	}
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::SparShellEditExit(TObject *Sender)
{
	if(!FileExists(SparShellEdit->Text))
	{
		SparShellEdit->Text = "";
		SparEnableImage->Down=false;
	}
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::Button1Click(TObject *Sender)
{
	if(Trim(SparShellEdit->Text)!= "")
		OpenDialog->InitialDir = ExtractFilePath(SparShellEdit->Text);
	else
		OpenDialog->InitialDir = "C:\\";
	if(OpenDialog->Execute())
	{
		if(FileExists(OpenDialog->FileName))
			SparShellEdit->Text = OpenDialog->FileName;
	}
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::XYZCCellWidthEditExit(TObject *Sender)
{
	try
	{
		if(StrToFloat(XYZCCellWidthEdit->Text)< 0)
			XYZCCellWidthEdit->Text = "0.00";
		else if(StrToFloat(XYZCCellWidthEdit->Text)> 50)
			XYZCCellWidthEdit->Text = "50.00";
	}
	catch(Exception & e)
	{
		XYZCCellWidthEdit->Text = "0.05";
	}
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::XYZCCellHeightEditExit(TObject *Sender) {
	try
	{
		if(StrToFloat(XYZCCellHeightEdit->Text)< 0)
			XYZCCellHeightEdit->Text = "0.00";
		else if(StrToFloat(XYZCCellHeightEdit->Text)> 5)
			XYZCCellHeightEdit->Text = "5.00";
	}
	catch(Exception & e)
	{
		XYZCCellWidthEdit->Text = "0.05";
	}
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::Label65Click(TObject *Sender)
{
	RecycleBinImage->Toggle();
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::Label66Click(TObject *Sender)
{
	GyrocompassImage->Toggle();
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::Label67Click(TObject *Sender)
{
	LargeControlsImage->Toggle();
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::HeadingCompensationEditExit(TObject *Sender)
{
	try
	{
		if(StrToInt(((TEdit*)Sender)->Text)<-180)
			((TEdit*)Sender)->Text = "-180";
		else if(StrToInt(((TEdit*)Sender)->Text)> 180)
			((TEdit*)Sender)->Text = "180";
	}
	catch(Exception & e)
	{
		((TEdit*)Sender)->Text = "0";
	}
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::StaticText4Click(TObject *Sender)
{
	CompensationImage->Toggle();
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::Image3Click(TObject *Sender)
{
	bool err = false;

	if(RadarMapManager && RadarMapManager->SparUnit && RadarMapManager->SparUnit->ReadBuf && WaitForSingleObject
		(RadarMapManager->SparUnit->ReadBuf->NotEmptyEvent, 1000)== WAIT_OBJECT_0)
	{
		Spar *Data;

		try
		{
			Data =(Spar*)RadarMapManager->SparUnit->ReadBuf->Pop();
		}
		catch(Exception & e)
		{
			Data = NULL;
		}
		if(Data)
		{
			PitchCompensationEdit->Text = FloatToStrF(RadToDeg(Data->sparPitch), ffFixed, 10, 2);
			RollCompensationEdit->Text = FloatToStrF(RadToDeg(Data->sparRoll), ffFixed, 10, 2);
		}
		else err = true;
	}
	else err = true;
	if(err)
	{
		ShowMessage("Cannot get a Spar information!");
	}
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::Label78Click(TObject *Sender)
{
	ShowLabelOnPinImage->Toggle();
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::Image4Click(TObject *Sender)
{
	bool err = false;

	if(RadarMapManager && RadarMapManager->SparUnit && RadarMapManager->SparUnit->ReadBuf && WaitForSingleObject
		(RadarMapManager->SparUnit->ReadBuf->NotEmptyEvent, 1000)== WAIT_OBJECT_0)
	{
		Spar *Data;

		try
		{
			Data =(Spar*)RadarMapManager->SparUnit->ReadBuf->Pop();
		}
		catch(Exception & e)
		{
			Data = NULL;
		}
		if(Data)
		{
			HeadingCompensationEdit->Text = FloatToStrF(RadToDeg(Data->sparHeading), ffFixed, 10, 2);
		}
		else err = true;
	}
	else err = true;
	if(err)
	{
		ShowMessage("Cannot get a Spar information!");
	}
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::Label82Click(TObject *Sender)
{
	ShellExecute(NULL, NULL, ((TLabel*)Sender)->Caption.t_str(), NULL, NULL, SW_SHOW);
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::GpsUseZGeoidImageClick(TObject *Sender)
{
	GpsUseZGeoidImage->Toggle();
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::Label86Click(TObject *Sender)
{
	XYZCGainImage->Toggle();
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::XYZCAbsoluteImageClick(TObject *Sender)
{
	XYZCAbsoluteImage->Toggle();
	if(XYZCAbsoluteImage->Down) XYZCEnvelopeImage->Down=false;
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::XYZCEnvelopeImageClick(TObject *Sender)
{
	XYZCEnvelopeImage->Toggle();
	if(XYZCEnvelopeImage->Down) XYZCAbsoluteImage->Down=false;
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::Label87Click(TObject *Sender)
{
	DXFPitchImage->Toggle();
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::Label88Click(TObject *Sender)
{
	DXFRollImage->Toggle();
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::Label61Click(TObject *Sender)
{
	XYZCUseZImage->Toggle();
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::Label62Click(TObject *Sender)
{
	XYZCUsePitchImage->Toggle();
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::Label63Click(TObject *Sender)
{
	XYZCUseRollImage->Toggle();
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::PaletteCheckImageClick(TObject *Sender)
{
	if(Sender && Sender->ClassName()== "TImageButton")
	{
		try
		{
			int i =((TImageButton*)Sender)->Tag;

			((TImageButton*)Sender)->Toggle();
			if(i > 1 && i < PaletteMaxColors)
			{
				PaletteItems[i - 1].Checked =((TImageButton*)Sender)->Down;
				/* if(SelectedPaletteItem==&PaletteItems[i-1] ||
				PaletteItems[i-1].Checked)
				SelectedPaletteItem=&PaletteItems[i-1]; */
				if(PaletteItems[i - 1].Checked)
					PaletteCanImage1Click((TObject*)PaletteItems[i - 1].Caption);
				else
				{
					for(int j = 0; j < PaletteMaxColors; j++)
					{
						if(PaletteItems[j].Selected)
						{
							PaletteCanImage1Click((TObject*)PaletteItems[j].Caption);
							break;
						}
					}
				}
			}
			RebuildPaletteImage();
		}
		catch(Exception & e) {}
	}
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::ApplyPalette(TPalette* APalette)
{
	TPaletteColor *pc;
	int maxi, i;

	if(APalette && APalette->Colors)
	{
		if(APalette->Colors->Count >= PaletteMaxColors)
			maxi = PaletteMaxColors;
		else
			maxi = APalette->Colors->Count;
		if(maxi > 0)
		{
			Palette = APalette;
			for(i = 0; i < maxi - 1; i++)
			{
				pc = Palette->Colors->GetForce(i);
				if(pc)
				{
					PaletteItems[i].Color = pc->Color;
					PaletteItems[i].Position = pc->Position;
					PaletteItems[i].Checked = pc->Checked;
					PaletteItems[i].Selected = false;
				}
			}
			for(; i < PaletteMaxColors - 1; i++)
			{
				PaletteItems[i].Color = clWhite;
				if(pc)
					PaletteItems[i].Position = pc->Position;
				else
					PaletteItems[i].Position = PaletteMaxPosition - 1;
				PaletteItems[i].Checked = false;
				PaletteItems[i].Selected = false;
			}
			pc = Palette->Colors->GetForce(maxi - 1);
			if(pc)
			{
				PaletteItems[PaletteMaxColors - 1].Color = pc->Color;
				PaletteItems[PaletteMaxColors - 1].Position = pc->Position;
				PaletteItems[PaletteMaxColors - 1].Checked = pc->Checked;
				PaletteItems[PaletteMaxColors - 1].Selected = false;
			}
			SelectedPaletteItem = &PaletteItems[0];
		}
	}
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::ColorLabelClick(TObject *Sender)
{
	if(SelectedPaletteItem)
		PaletteCanImage1DblClick((TObject*)SelectedPaletteItem->Caption);
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::PaletteCanImage1Click(TObject *Sender)
{
	if(!DblClickTimer->Enabled)
	{
		int i, j, k;
		float h;

		DblClickTimer->Interval = GetDoubleClickTime();
		OnClickSender = Sender;
		DblClickTimer->Enabled = true;

		i =((TComponent*)Sender)->Tag - 1;
		if(i >= 0 && i < PaletteMaxColors)
		{
			j = i - 1;
			while(j > 0 && !PaletteItems[j].Checked)
				j--;
			if(j < 0)
				j = 0;
			PaletteItems[i].Min = PaletteItems[j].Position;
			k = i + 1;
			while(k < PaletteMaxColors - 1 && !PaletteItems[k].Checked)
				k++;
			if(k > PaletteMaxColors - 1)
				k = PaletteMaxColors - 1;
			PaletteItems[i].Max = PaletteItems[k].Position;
			SelectedPaletteItem =&PaletteItems[i];
		}
	}
	else if(OnClickSender == Sender)
	{
		DblClickTimer->Enabled = false;
		PaletteCanImage1DblClick(Sender);
	}
	else
	{
		DblClickTimer->Enabled = false;
		OnClickSender = NULL;
	}
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::PaletteCanImage1DblClick(TObject *Sender)
{
	int i;

	if(Sender)
	{
		i =((TComponent*)Sender)->Tag - 1;
		if(i >= 0 && i < PaletteMaxColors)
		{
			ColorDialog1->Color = PaletteItems[i].Color;
			if(ColorDialog1->Execute())
			{
				PaletteItems[i].Color = ColorDialog1->Color;
				RebuildPaletteImage();
			}
			if(SelectedPaletteItem)
			{
				ColorShape->Brush->Color = SelectedPaletteItem->Color;
				ColorLabel->Font->Color = SelectedPaletteItem->Caption->Font->Color;
				ColorLabel->Color = SelectedPaletteItem->Color;
			}
		}
	}
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::SelectPaletteItem(TPaletteItem *PaletteItem)
{
	int t, h;

	t = PaletteImage->Top;
	h = PaletteImage->Height;
	selectedpaletteitem = NULL;
	if(PaletteItem)
	{
		ColorShape->Brush->Color = PaletteItem->Color;
		ColorLabel->Color = PaletteItem->Color;
		if(PaletteItem->Caption)
		{
			ColorLabel->Font->Color = PaletteItem->Caption->Font->Color;
			ColorLabel->Caption = PaletteItem->Caption->Caption;
		}
		for(int i = 0; i < PaletteMaxColors; i++)
			PaletteItems[i].Selected = false;
		PaletteItem->Selected = true;
		ColorTBar->Min = 0;
		ColorTBar->Max = PaletteMaxPosition;
		ColorTBar->Position = PaletteMaxPosition >> 1;
		ColorTBar->Max = PaletteItem->Max;
		ColorTBar->Min = PaletteItem->Min;
		ColorTBar->Position = PaletteItem->Position;
		if(PaletteItem->ID > 1 && PaletteItem->ID < PaletteMaxColors)
		{
			ColorTBar->Visible = true;
			if(PaletteItem->Checked)
				ColorTBar->Enabled = true;
			else
				ColorTBar->Enabled = false;
			t +=(float)h *(float)ColorTBar->Min /(float)PaletteMaxPosition;
			h *=(float)(ColorTBar->Max - ColorTBar->Min)/(float)PaletteMaxPosition;
		}
		else
		{
			ColorTBar->Enabled = false;
			ColorTBar->Visible = false;
		}
		t -= ColorTrackBarShift;
		h += 2 * ColorTrackBarShift - 1;
		ColorTBar->Top = t;
		ColorTBar->Height = h;
	}
	else
	{
		ColorShape->Brush->Color = (TColor)0x00F8F9FF;
		ColorLabel->Color = (TColor)0x00F8F9FF;
		ColorLabel->Caption = "";
		ColorTBar->Enabled = false;
		ColorTBar->Position = 128;
	}
	selectedpaletteitem = PaletteItem;
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::RebuildPaletteImage()
{
	if(Palette)
	{
		for(int i = 0; i < PaletteMaxColors; i++)
			Palette->Assign(new TPaletteColor(PaletteItems[i].Color, PaletteItems[i].Position, PaletteItems[i].Checked), i, false);
		Palette->RebuildPalette();
		Palette->DrawPalette(PaletteImage->Bitmap, PaletteImage->Width, PaletteImage->Height);
		PaletteImage->Repaint();
	}
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::DblClickTimerTimer(TObject *Sender)
{
	DblClickTimer->Enabled = false;
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::ColorTBarChange(TObject *Sender)
{
	if(SelectedPaletteItem)
	{
		SelectedPaletteItem->Position = ColorTBar->Position;
		RebuildPaletteImage();
	}
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::ColorTBarEnter(TObject *Sender)
{
	PaletteImage->SetFocus();
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::PaletteLoadImageClick(TObject *Sender)
{
	int res;

	PalettesForm->CaptionST->Caption = "LOAD PALETTE...";
	res = PalettesForm->ShowModal();
	if(res > 100)
	{
		res -= 100;
		if(res > 0 && res <= PaletteMaxUnits)
		{
			Palette->LoadPaletteFromRegistry((AnsiString)((AnsiString)_RadarMapRegKey + "\\Palettes").c_str(), "Palette" + IntToStr(res));
			ApplyPalette(Palette);
			RebuildPaletteImage();
		}
	}
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::PaletteSaveImageClick(TObject *Sender)
{
	int res;

	PalettesForm->CaptionST->Caption = "SAVE PALETTE...";
	res = PalettesForm->ShowModal();
	if(res > 100)
	{
		res -= 100;
		if(res > 0 && res <= PaletteMaxUnits)
		{
			Palette->SavePaletteToRegistry((AnsiString)((AnsiString)_RadarMapRegKey + "\\Palettes").c_str(), "Palette" + IntToStr(res));
		}
	}
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::Label101Click(TObject *Sender)
{
	ShowLabelConfidenceImage->Toggle();
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::ApplyGainImageClick(TObject *Sender)
{
	ApplyGainImage->Toggle();
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::Label105Click(TObject *Sender)
{
	DXFConfidenceImage->Toggle();
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::Label106Click(TObject *Sender)
{
	DXFDescriptionAsTextImage->Toggle();
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::Label107Click(TObject *Sender)
{
	DXFPolyline3DImage->Toggle();
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::GpsLogToFileImageClick(TObject *Sender)
{
	GpsLogToFileImage->Toggle();
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::Label109Click(TObject *Sender)
{
	DXFDepthToDescriptionImage->Toggle();
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::Label110Click(TObject *Sender)
{
	DXFIdToDescriptionImage->Toggle();
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::TextHeightSEditKeyPress(TObject *Sender, wchar_t &Key)
{
	if(Key == '.' || Key == ',')
	{
		if(TextHeightSEdit->Text.Pos(DecimalSeparator)== 0)
			Key = DecimalSeparator;
	}
	else if((Key < '0' || Key > '9') && Key != 8)
		Key = 0;
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::TrackBar1Change(TObject *Sender)
{
	if(TrackBar1->Position != UpDown1->Position)
		UpDown1->Position = TrackBar1->Position;
	TextHeightSEdit->Text = FloatToStrF((float)TrackBar1->Position / 10., ffFixed, 10, 1);
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::TextHeightSEditChange(TObject *Sender)
{
	try
	{
		if(StrToFloat(TextHeightSEdit->Text)> 10)
			TextHeightSEdit->Text = "10.0";
		TrackBar1->Position =(int)(StrToFloat(TextHeightSEdit->Text)* 10.0);
	}
	catch(Exception & e)
	{
		TextHeightSEdit->Text = "1.0";
	}
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::UpDown1Click(TObject *Sender, TUDBtnType Button)
{
	if(TrackBar1->Position != UpDown1->Position)
		TrackBar1->Position = UpDown1->Position;
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::FormClose(TObject *Sender, TCloseAction &Action)
{
	if(GainItems)
	{
		delete GainItems;
		GainItems = NULL;
	}
	if(GainImageGrid)
	{
		delete GainImageGrid;
		GainImageGrid = NULL;
	}
	if(PauseTerminalIButton->Down) PauseTerminalIButtonClick(PauseTerminalIButton);
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::Label112Click(TObject *Sender)
{
	HideAnnotationLayerOnOpenImage->Toggle();
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::EmptyMapSizeEditExit(TObject *Sender)
{
	try
	{
		if(StrToInt(EmptyMapSizeEdit->Text)< 10)
			EmptyMapSizeEdit->Text = "10";
		else if(StrToInt(EmptyMapSizeEdit->Text)> 1000)
			EmptyMapSizeEdit->Text = "1000";
	}
	catch(Exception & e)
	{
		EmptyMapSizeEdit->Text = "100";
	}
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::Label113Click(TObject *Sender)
{
	EnlargeEmptyMapImage->Toggle();
}

// ---------------------------------------------------------------------------
void __fastcall TSettingsForm::WSWindowWidthEditExit(TObject *Sender)
{
	try
	{
		if(StrToInt(((TEdit*)Sender)->Text)< 1)
			((TEdit*)Sender)->Text = "1";
		else if(StrToInt(((TEdit*)Sender)->Text)> 1000)
			((TEdit*)Sender)->Text = "1000";
	}
	catch(Exception & e)
	{
		((TEdit*)Sender)->Text = "100";
	}
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::Label116Click(TObject *Sender)
{
	WeightedSubtractionFilterImage->Toggle();
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::Label119Click(TObject *Sender)
{
	MoveoutCorrectionImage->Toggle();
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::QEditExit(TObject *Sender)
{
	try
	{
		if(StrToInt(QEdit->Text)< 32)
			QEdit->Text = "32";
		else if(StrToInt(QEdit->Text)> 1024)
			QEdit->Text = "1024";
	}
	catch(Exception & e)
	{
		QEdit->Text = "128";
	}
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::AddPlugInBtnClick(TObject *Sender)
{
	TPlugInListItem *plugin;
	TPlugInObject *pio;
	TTreeNode *Root;
	TListItem *Item;
	int i, j;
	bool direxists, dllcopied = false;
	AnsiString FN;

	if(PlugInManager && PlugInDialog->Execute())
	{
		LoadPluginsForm->OutputPlugIns(PlugInDialog->FileName, pitUnknown);
		if(LoadPluginsForm->ShowModal()== mrOk)
		{
			Root = LoadPluginsForm->TreeView->Items->GetFirstNode();
			for(int k = 0; k < LoadPluginsForm->TreeView->Items->Count; k++)
			{
				if((LoadPluginsForm->TreeView->Checked(k) || LoadPluginsForm->TreeView->Checked(Root)) &&
					LoadPluginsForm->TreeView->Items->Item[k]->Data)
				{
					plugin =(TPlugInListItem*)LoadPluginsForm->TreeView->Items->Item[k]->Data;
					if(!PlugInManager->PlugInsList->GetByGUID(plugin->GUID))
					{
						if(LoadPluginsForm->CopyCheckBox->Down)
						{
							if(!dllcopied)
							{
								if(FileExists(plugin->DLLName) && RadarMapManager)
								{
									AnsiString Dir = RadarMapManager->StartDirectory + _PlugInsRegKey;

									direxists = DirectoryExists(Dir);
									if(!direxists) direxists = CreateDirectory(Dir.c_str(), NULL);
									if(direxists)
									{
										FN = Dir + "\\" + ExtractFileName(plugin->DLLName);
										dllcopied = CopyFile(plugin->DLLName.c_str(), FN.c_str(), true);
										if(dllcopied) plugin->DLLName = FN;
										else Application->MessageBox(((UnicodeString)"Cannot copy Plug-In file to the RadarMap folder!").c_str(), ((UnicodeString)"RadarMap").c_str(), MB_ICONWARNING | MB_OK);
									}
								}
							}
							else if(FileExists(FN))	plugin->DLLName = FN;
						}
						i = PlugInManager->AddPlugIn(plugin->DLLName, plugin->ID);
						if(i >= 0)
						{
							pio = PlugInManager->PlugInsList->Items[i];
							if(pio)
							{
								Item = PlugInLView->Items->Add();
								Item->Caption = plugin->Name;
								switch(plugin->Type)
								{
									case pitCRS:
										Item->ImageIndex =(int)plugin->Type;
										Item->SubItems->Add("CRS");
										DefaultCoordinateSystemCBox->Items->Delete(DefaultCoordinateSystemCBox->Items->Count - 1);
										// delete last Add...
										j = DefaultCoordinateSystemCBox->Items->Add
											(plugin->Name + " (" +((TPlugInCoordinateSystem*)pio)->EllipsoidName + ")");
										CRS_GUIDs->Add(plugin->GUID);
										DefaultCoordinateSystemCBox->Items->Add("Add...");
										DefaultCoordinateSystemCBox->ItemIndex = j;
										break;
									case pitPos:
										Item->ImageIndex =(int)plugin->Type;
										Item->SubItems->Add("POS");
										PosPlugInCBox->Items->Delete(PosPlugInCBox->Items->Count - 1); // delete last Add...
										j = PosPlugInCBox->Items->Add(plugin->Name);
										Pos_GUIDs->Add(plugin->GUID);
										PosPlugInCBox->Items->Add("Add...");
										PosPlugInCBox->ItemIndex = j;
										break;
									default:
										Item->ImageIndex = 0;
										Item->SubItems->Add("UNK");
								}
								Item->SubItems->Add(plugin->GUID);
								Item->SubItems->Add(ExtractFileName(plugin->DLLName)+ ", " + IntToStr(plugin->ID));
							}
						}
					}
					else
					{
						ShowMessage("Plug-In \"" + plugin->Name + "\" with GUID " + plugin->GUID + " already exists!");
					}
				}
			}
		}
	}
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::DelPlugInBtnClick(TObject *Sender)
{
	int i;
	//bool answ = false;

	if(PlugInLView->Selected && PlugInManager)
	{
		TPlugInObject *plugin;
		plugin = PlugInManager->PlugInsList->GetByGUID(PlugInLView->Selected->SubItems->Strings[1]);
		if(plugin)
		{
			i = Application->MessageBox(((UnicodeString)("Do you want to delete \"" + PlugInLView->Selected->Caption + "\"?")).c_str(),
				L"Delete RadarMap Plug-In", MB_ICONQUESTION | MB_YESNO);
			if(i == IDYES)
			{
				switch(plugin->Type)
				{
					case pitCRS:
						i = CRS_GUIDs->IndexOf(plugin->GUID);
						if(i >= 0)
						{
							CRS_GUIDs->Delete(i);
							i += csTotalFixed;
							if(DefaultCoordinateSystemCBox->ItemIndex == i)
								DefaultCoordinateSystemCBox->ItemIndex =-1;
							DefaultCoordinateSystemCBox->Items->Delete(i);
						}
						break;
					case pitPos:
						i = Pos_GUIDs->IndexOf(plugin->GUID);
						if(i >= 0)
						{
							Pos_GUIDs->Delete(i);
							if(PosPlugInCBox->ItemIndex == i)
							{
								PosPlugInCBox->ItemIndex =-1;
								if(PosPlugInImage->Down)
									WheelImageForceClick(ManualImage);
							}
							PosPlugInCBox->Items->Delete(i);
						}
						break;
				}
				plugin->Free();
				if(plugin->Active)
					Application->MessageBox(((UnicodeString)"Please close the RadarMap and rerun it again to finalize PlugIn deleting.").c_str(),
						((UnicodeString)"RadarMap").c_str(), MB_ICONINFORMATION | MB_OK);
				//else answ = (Application->MessageBox(((UnicodeString)"Do you want to delete a PlugIn file from the Disk?.").c_str(),
				//	((UnicodeString)"RadarMap").c_str(), MB_ICONINFORMATION | MB_YESNO)== IDYES);
				PlugInManager->PlugInsList->DeleteByGUID(plugin->GUID);//, answ);
				i = PlugInLView->Selected->Index;
				PlugInLView->Selected = NULL;
				PlugInLView->Items->Delete(i);
				DelPlugInBtn->Enabled = false;
				SettingsPlugInBtn->Enabled = false;
			}
		}
	}
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::PlugInLViewChange(TObject *Sender, TListItem *Item, TItemChange Change)
{
	TPlugInObject *plugin;

	DelPlugInBtn->Enabled =(Item != NULL);
	if(Item && Item->SubItems && Item->SubItems->Count > 1)
	{
		plugin = PlugInManager->PlugInsList->GetByGUID(Item->SubItems->Strings[1]);
		SettingsPlugInBtn->Enabled =(plugin && plugin->OptionsCount > 0);
	}
	else SettingsPlugInBtn->Enabled = false;
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::DefaultCoordinateSystemCBoxChange(TObject *Sender)
{
	if(DefaultCoordinateSystemCBox->ItemIndex == DefaultCoordinateSystemCBox->Items->Count - 1)
	{
		AddPlugInBtnClick(Sender);
		if(DefaultCoordinateSystemCBox->ItemIndex == DefaultCoordinateSystemCBox->Items->Count - 1)
			DefaultCoordinateSystemCBox->ItemIndex--;
	}
	CollectSettings(true);
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::PosPlugInImageClick(TObject *Sender)
{
	if(PosPlugInCBox->Items->Count > 1)
	{
		if(PosPlugInCBox->ItemIndex == PosPlugInCBox->Items->Count - 1)
		{
			ShowMessage("Please choose the Positioning Plug-In first!");
		}
		else WheelImageClick(Sender);
	}
	else ShowMessage("Cannot find any Positioning Plug-In!");
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::PlugInCBoxChange(TObject *Sender)
{
	if(((TComboBox*)Sender)->ItemIndex ==((TComboBox*)Sender)->Items->Count - 1)
	{
		AddPlugInBtnClick(Sender);
		if(((TComboBox*)Sender)->ItemIndex ==((TComboBox*)Sender)->Items->Count - 1)
			((TComboBox*)Sender)->ItemIndex--;
	}
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::PosPlugInCBoxChange(TObject *Sender)
{
	PlugInCBoxChange(PosPlugInCBox);
	PosPlugInButton->Enabled =!(((TComboBox*)Sender)->ItemIndex ==((TComboBox*)Sender)->Items->Count - 1);
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::PosPlugInButtonClick(TObject *Sender)
{
	if(PosPlugInCBox->ItemIndex >= 0 && PosPlugInCBox->ItemIndex != PosPlugInCBox->Items->Count - 1)
		SettingsPlugInBtnClick(Sender);
	else PosPlugInButton->Enabled = false;
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::SettingsPlugInBtnClick(TObject *Sender)
{
	if(PlugInManager && (Sender != SettingsPlugInBtn || PlugInLView->Selected))
	{
		TPlugInObject *plugin;

		if(Sender == SettingsPlugInBtn)	plugin = PlugInManager->PlugInsList->GetByGUID(PlugInLView->Selected->SubItems->Strings[1]);
		else if(PosPlugInCBox->ItemIndex != PosPlugInCBox->Items->Count - 1 && PosPlugInCBox->ItemIndex >= 0)
			plugin = PlugInManager->PlugInsList->GetByGUID(Pos_GUIDs->Strings[PosPlugInCBox->ItemIndex]);
		else plugin = NULL;
		if(plugin && plugin->OptionsCount > 0)
		{
			OptionsPluginsForm->GetPlugInOptions(plugin);
			if(OptionsPluginsForm->ShowModal()==mrOk)
				PlugInLView->Selected->Caption = plugin->Name;
		}
	}
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::Label124Click(TObject *Sender)
{
	CacheTracesImage->Toggle();
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::Label126Click(TObject *Sender)
{
	CacheProfilesImage->Toggle();
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::PredictionRadiusEditExit(TObject *Sender)
{
	try
	{
		if(StrToInt(PredictionRadiusEdit->Text)<=0)
			PredictionRadiusEdit->Text="1";
		else if(StrToInt(PredictionRadiusEdit->Text)>1000)
			PredictionRadiusEdit->Text="1000";
	}
	catch(Exception & e)
	{
		PredictionRadiusEdit->Text="5";
	}
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::PauseTerminalIButtonClick(TObject *Sender)
{
	PauseTerminalIButton->Toggle();
	if(PauseTerminalIButton->Down)
	{
		if(PauseTerminal) PauseTerminal();
		Memo1->ScrollBars = ssVertical;
		Label157->Font->Color=(TColor)0x00FFA047;
		Label157->Caption="Paused";
	}
	else
	{
		if(ResumeTerminal) ResumeTerminal();
		Memo1->ScrollBars=ssNone;
		Label157->Font->Color=(TColor)0x004F2822;
		Label157->Caption="Pause";
	}
	TerminalPausedPanel->Visible=PauseTerminalIButton->Down;
	Memo1->Perform(EM_LINESCROLL, 0, Memo1->Lines->Count);
	ImageButton1->Enabled=!PauseTerminalIButton->Down;
	SendEdit->Enabled=!PauseTerminalIButton->Down;
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::AutoStopProfileEditExit(TObject *Sender)
{
	try
	{
		if(StrToInt(((TEdit*)Sender)->Text)< 1)
			((TEdit*)Sender)->Text = "1";
		else if(StrToInt(((TEdit*)Sender)->Text)> 1024)
			((TEdit*)Sender)->Text = "1024";
	}
	catch(Exception & e)
	{
		((TEdit*)Sender)->Text = "50";
	}
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::AutosaveBrowseImageClick(TObject *Sender)
{
	UnicodeString S;
	if(Trim(AutoSaveDirEdit->Text)!= "") S = ExtractFilePath(AutoSaveDirEdit->Text);
	else S = "C:\\";
	if(SelectDirectory(S, TSelectDirOpts()<< sdAllowCreate << sdPerformCreate << sdPrompt, 0))
	{
		if(DirectoryExists(S)) AutoSaveDirEdit->Text = S;
	}
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::Label132Click(TObject *Sender)
{
	ProfilePipesDetectorImageClick(ProfilePipesDetectorImage);
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::ProfilePipesDetectorImageClick(TObject *Sender)
{
	if(!((TImageButton*)Sender)->Down)
	{
		/* TRegistry& regKey=*new TRegistry();
		try
		{
		try
		{
		bool b=false;

		if(regKey.OpenKey(_RadarMapRegKey,true))
		{
		if(regKey.ValueExists("ObjectsDisclaimerNeedAgreement")) b=regKey.ReadBool("ObjectsDisclaimerNeedAgreement");
		else b=true;
		if(b) b=!(DisclaimerForm->ShowModal()==mrOk);
		regKey.WriteBool("ObjectsDisclaimerNeedAgreement", b);
		if(!b) ((TImageButton*)Sender)->Down=true;
		}
		}
		catch(Exception &e)
		{
		((TImageButton*)Sender)->Down=false;
		}
		}
		__finally
		{
		delete &regKey;
		} */
		if(DisclaimerForm->ShowModal() == mrOk) ((TImageButton*)Sender)->Down=true;
		else ((TImageButton*)Sender)->Down=false;
	}
	else ((TImageButton*)Sender)->Down=false;
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::GpsUnitIButtonClick(TObject *Sender)
{
	if(GpsUtitCBox->ItemIndex > 0 && GpsUtitCBox->ItemIndex != GpsUtitCBox->Items->Count - 1)
	{
		if(OptionsGpsUnitForm->ShowOptions(settings.GetGpsUnitFileName(GpsUtitCBox->Text))== mrOk)
			GpsUtitCBoxChange(Sender);
	}
	else GpsUnitIButton->Enabled = false;
}
// ---------------------------------------------------------------------------

void __fastcall TSettingsForm::TwoWheelSettingsButtonClick(TObject *Sender)
{
	//OptionsGpsUnitForm->ShowOptions(settings.TwoWheelsDRS.UnitFileName);
	TTwoWheelsForm *twf = NULL;

	try
	{
		twf = new TTwoWheelsForm(this, Manager);
		twf->ShowModal();
	}
	__finally
	{
		try
		{
			if(twf) delete twf;
		}
		catch(...) {}
		twf = NULL;
	}
}
//---------------------------------------------------------------------------

void __fastcall TSettingsForm::Label141Click(TObject *Sender)
{
	UseGpsAsStartImage->Toggle();
	//CollectSettings(true);
}
//---------------------------------------------------------------------------

void __fastcall TSettingsForm::Label137Click(TObject *Sender)
{
	TwoWheelRtsImage->Toggle();
	CollectSettings(true);
}
//---------------------------------------------------------------------------

void __fastcall TSettingsForm::InfoBtnClick(TObject *Sender)
{
	((TImageButton*)Sender)->GroupToggle();
	Notebook1->PageIndex=((TImageButton*)Sender)->Tag;
}
//---------------------------------------------------------------------------

void __fastcall TSettingsForm::NextButtonClick(TObject *Sender)
{
	ToolbarNotebook->PageIndex++;
	InfoBtnClick(DXFButton);
}
//---------------------------------------------------------------------------

void __fastcall TSettingsForm::PreviousButtonClick(TObject *Sender)
{
	ToolbarNotebook->PageIndex--;
	InfoBtnClick(GPRButton);
}
//---------------------------------------------------------------------------

void __fastcall TSettingsForm::TitlePanelMouseDown(TObject *Sender, TMouseButton Button,
		  TShiftState Shift, int X, int Y)
{
	TitleDown = true;
	TitleXY = ClientToScreen(Types::TPoint(X, Y));
}
//---------------------------------------------------------------------------

void __fastcall TSettingsForm::TitlePanelMouseMove(TObject *Sender, TShiftState Shift,
		  int X, int Y)
{
	Types::TPoint tmp;

	if(TitleDown)
	{
		tmp = ClientToScreen(Types::TPoint(X, Y));
		Left += tmp.x - TitleXY.x;
		Top += tmp.y - TitleXY.y;
		TitleXY = tmp;
	}
}
//---------------------------------------------------------------------------

void __fastcall TSettingsForm::TitlePanelMouseUp(TObject *Sender, TMouseButton Button,
		  TShiftState Shift, int X, int Y)
{
	TitleDown = false;
}
//---------------------------------------------------------------------------

void __fastcall TSettingsForm::CancelImageButtonClick(TObject *Sender)
{
	ModalResult = mrCancel;
}
//---------------------------------------------------------------------------

void __fastcall TSettingsForm::Label140Click(TObject *Sender)
{
	WheelImageClick(TwoWheelImage);
}
//---------------------------------------------------------------------------

void __fastcall TSettingsForm::Label123Click(TObject *Sender)
{
	WheelImageClick(PosPlugInImage);
}
//---------------------------------------------------------------------------
void __fastcall TSettingsForm::ImageButton2Click(TObject *Sender)
{
	ShellExecute(NULL, NULL, "https://msdn.microsoft.com/en-us/library/ff428642.aspx", NULL, NULL, SW_SHOW);
}
//---------------------------------------------------------------------------


void __fastcall TSettingsForm::Panel9MouseDown(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y)
{
	Panel9MousePressed=true;
	Panel9Y=Y;
}
//---------------------------------------------------------------------------

void __fastcall TSettingsForm::Panel9MouseUp(TObject *Sender, TMouseButton Button,
		  TShiftState Shift, int X, int Y)
{
	Panel9MousePressed=false;
	Panel9Y=-1;
}
//---------------------------------------------------------------------------


void __fastcall TSettingsForm::Panel9MouseMove(TObject *Sender, TShiftState Shift,
		  int X, int Y)
{
	if(Panel9MousePressed && Panel9Y>=0) ScrollBox3->VertScrollBar->Position+=Panel9Y-Y;
}
//---------------------------------------------------------------------------

void __fastcall TSettingsForm::Panel6MouseDown(TObject *Sender, TMouseButton Button,
		  TShiftState Shift, int X, int Y)
{
	Panel6MousePressed=true;
	Panel6Y=Y;
}
//---------------------------------------------------------------------------

void __fastcall TSettingsForm::Panel6MouseMove(TObject *Sender, TShiftState Shift,
		  int X, int Y)
{
	Panel6MousePressed=false;
	Panel6Y=-1;
}
//---------------------------------------------------------------------------

void __fastcall TSettingsForm::Panel6MouseUp(TObject *Sender, TMouseButton Button,
		  TShiftState Shift, int X, int Y)
{
	if(Panel6MousePressed && Panel6Y>=0) ScrollBox1->VertScrollBar->Position+=Panel6Y-Y;
}
//---------------------------------------------------------------------------

void __fastcall TSettingsForm::Panel2MouseDown(TObject *Sender, TMouseButton Button,
		  TShiftState Shift, int X, int Y)
{
	Panel2MousePressed=true;
	Panel2Y=Y;
}
//---------------------------------------------------------------------------

void __fastcall TSettingsForm::Panel2MouseUp(TObject *Sender, TMouseButton Button,
		  TShiftState Shift, int X, int Y)
{
	Panel2MousePressed=false;
	Panel2Y=-1;
}
//---------------------------------------------------------------------------

void __fastcall TSettingsForm::Panel2MouseMove(TObject *Sender, TShiftState Shift,
		  int X, int Y)
{
	if(Panel2MousePressed && Panel2Y>=0) ScrollBox2->VertScrollBar->Position+=Panel2Y-Y;
}
//---------------------------------------------------------------------------

void __fastcall TSettingsForm::Panel5MouseDown(TObject *Sender, TMouseButton Button,
		  TShiftState Shift, int X, int Y)
{
	Panel5MousePressed=true;
	Panel5Y=Y;
}
//---------------------------------------------------------------------------

void __fastcall TSettingsForm::Panel5MouseUp(TObject *Sender, TMouseButton Button,
		  TShiftState Shift, int X, int Y)
{
	Panel5MousePressed=false;
	Panel5Y=-1;
}
//---------------------------------------------------------------------------

void __fastcall TSettingsForm::Panel5MouseMove(TObject *Sender, TShiftState Shift,
		  int X, int Y)
{
	if(Panel5MousePressed && Panel5Y>=0) ScrollBox4->VertScrollBar->Position+=Panel5Y-Y;
}
//---------------------------------------------------------------------------

void __fastcall TSettingsForm::Panel33MouseDown(TObject *Sender, TMouseButton Button,
		  TShiftState Shift, int X, int Y)
{
	Panel33MousePressed=true;
	Panel33Y=Y;
}
//---------------------------------------------------------------------------

void __fastcall TSettingsForm::Panel33MouseUp(TObject *Sender, TMouseButton Button,
		  TShiftState Shift, int X, int Y)
{
	Panel33MousePressed=false;
	Panel33Y=-1;
}
//---------------------------------------------------------------------------

void __fastcall TSettingsForm::Panel33MouseMove(TObject *Sender, TShiftState Shift,
		  int X, int Y)
{
	if(Panel33MousePressed && Panel33Y>=0) ScrollBox5->VertScrollBar->Position+=Panel33Y-Y;
}
//---------------------------------------------------------------------------

void __fastcall TSettingsForm::Panel7MouseDown(TObject *Sender, TMouseButton Button,
		  TShiftState Shift, int X, int Y)
{
	Panel7MousePressed=true;
	Panel7Y=Y;
}
//---------------------------------------------------------------------------

void __fastcall TSettingsForm::Panel7MouseUp(TObject *Sender, TMouseButton Button,
		  TShiftState Shift, int X, int Y)
{
	Panel7MousePressed=false;
	Panel7Y=-1;
}
//---------------------------------------------------------------------------

void __fastcall TSettingsForm::Panel7MouseMove(TObject *Sender, TShiftState Shift,
		  int X, int Y)
{
	if(Panel7MousePressed && Panel7Y>=0) ScrollBox6->VertScrollBar->Position+=Panel7Y-Y;
}
//---------------------------------------------------------------------------

void __fastcall TSettingsForm::rbMapClick(TObject *Sender)
{
	((TImageButton*)Sender)->GroupToggle();
}
//---------------------------------------------------------------------------

void __fastcall TSettingsForm::Label77Click(TObject *Sender)
{
	CopyAttachmentsImage->Toggle();
}
//---------------------------------------------------------------------------

void __fastcall TSettingsForm::Label76Click(TObject *Sender)
{
	ShowLabelIconsImage->Toggle();
}
//---------------------------------------------------------------------------

void __fastcall TSettingsForm::Label127Click(TObject *Sender)
{
	AutoStopProfileImage->Toggle();
}
//---------------------------------------------------------------------------

void __fastcall TSettingsForm::Label128Click(TObject *Sender)
{
	AutoSaveImage->Toggle();
}
//---------------------------------------------------------------------------

void __fastcall TSettingsForm::Label158Click(TObject *Sender)
{
	rbMap->GroupToggle();
}
//---------------------------------------------------------------------------

void __fastcall TSettingsForm::Label159Click(TObject *Sender)
{
	rbAerial->GroupToggle();
}
//---------------------------------------------------------------------------

void __fastcall TSettingsForm::Label160Click(TObject *Sender)
{
    rbAerialLabels->GroupToggle();
}
//---------------------------------------------------------------------------

void __fastcall TSettingsForm::PredictionTimeoutEditExit(TObject *Sender)
{
	try
	{
		if(StrToFloat(PredictionTimeoutEdit->Text)<1)
			PredictionTimeoutEdit->Text="1";
		else if(StrToFloat(PredictionTimeoutEdit->Text)>120)
			PredictionTimeoutEdit->Text="120";
	}
	catch(Exception & e)
	{
		PredictionTimeoutEdit->Text="5";
	}
}
//---------------------------------------------------------------------------

void __fastcall TSettingsForm::CacheTracesEditExit(TObject *Sender)
{
	try
	{
		if(StrToInt(CacheTracesEdit->Text)<1)
			CacheTracesEdit->Text="1";
		else if(StrToInt(CacheTracesEdit->Text)>120)
			CacheTracesEdit->Text="120";
	}
	catch(Exception & e)
	{
		CacheTracesEdit->Text="10";
	}
}
//---------------------------------------------------------------------------

void __fastcall TSettingsForm::Label97Click(TObject *Sender)
{
	DbStoreLayersVisabilityIButton->Toggle();
}
//---------------------------------------------------------------------------

void __fastcall TSettingsForm::Label96Click(TObject *Sender)
{
	DbShowLayerFileNameIButton->Toggle();
}
//---------------------------------------------------------------------------

void __fastcall TSettingsForm::Label166Click(TObject *Sender)
{
	DbMergeLayersWithSameNameIButton->Toggle();
}
//---------------------------------------------------------------------------

void __fastcall TSettingsForm::Label57Click(TObject *Sender)
{
	PredictionIButton->Toggle();
}
//---------------------------------------------------------------------------

void __fastcall TSettingsForm::ZoomTrackBarChange(TObject *Sender)
{
	ZoomLabel->Caption="x"+IntToStr(ZoomTrackBar->Position);
}
//---------------------------------------------------------------------------

void __fastcall TSettingsForm::Label133Click(TObject *Sender)
{
	SingleTypeIButton->Toggle();
}
//---------------------------------------------------------------------------

void __fastcall TSettingsForm::Label167Click(TObject *Sender)
{
	GpsSimTcpImageClick(WPSasWheelImage);
}
//---------------------------------------------------------------------------

void __fastcall TSettingsForm::Label168Click(TObject *Sender)
{
	AutoCloseImage->Toggle();
}
//---------------------------------------------------------------------------

void __fastcall TSettingsForm::OkCancelPanelMouseDown(TObject *Sender, TMouseButton Button,
		  TShiftState Shift, int X, int Y)
{
	if(Shift.Contains(ssAlt) && Shift.Contains(ssCtrl)) BuildDateLabel->Visible=!BuildDateLabel->Visible;
}
//---------------------------------------------------------------------------

void __fastcall TSettingsForm::Label169Click(TObject *Sender)
{
	rpoHorizontalIButton->GroupToggle();
}
//---------------------------------------------------------------------------

void __fastcall TSettingsForm::Label134Click(TObject *Sender)
{
	rpoVerticalIButton->GroupToggle();
}
//---------------------------------------------------------------------------

void __fastcall TSettingsForm::Label170Click(TObject *Sender)
{
	rpoAutoIButton->GroupToggle();
}
//---------------------------------------------------------------------------

void __fastcall TSettingsForm::Label172Click(TObject *Sender)
{
	RMMaximizedIButton->Toggle();
}
//---------------------------------------------------------------------------

