//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "TwoWheelsUnit.h"
#include "Manager.h"
#include <mmsystem.h>

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "ImageButton"
#pragma link "GR32_Image"
#pragma resource "*.dfm"
TTwoWheelsForm *TwoWheelsForm;

//---------------------------------------------------------------------------
// TOptionsGpsUnitForm
//---------------------------------------------------------------------------
__fastcall TTwoWheelsForm::TTwoWheelsForm(TComponent* Owner, TObject *aManager)
	: TForm(Owner)
{
	Manager=aManager;
	ResMov=new TResourceMovie();
	ResMov->Image=Image321;
	ResMov->Endless=true;
	ResMov->FramePictureType=ptPNG;
	ReceivedStrFlags=rsfNone;
	SetParamsWay=spwNone;
	Panel10MousePressed = false;
	Panel10Y = -1;
}
//---------------------------------------------------------------------------

__fastcall TTwoWheelsForm::~TTwoWheelsForm()
{
	if(ResMov) delete ResMov;
	ResMov=NULL;
}
//---------------------------------------------------------------------------

void __fastcall TTwoWheelsForm::OkImageClick(TObject *Sender)
{
	if(ApplyButton->Enabled)
	{
		CalibrationType=twctNone;
		ReceivedStrFlags &= ~(rsfWheels | rsfLWheel | rsfRWheel | rsfCenterXY);
		if(Manager && ((TRadarMapManager*)Manager)->TwoWheelClient && ((TRadarMapManager*)Manager)->TwoWheelClient->Connected)
		{
			SetParamsWay=spwOK;
			StartMovie(true, rmtSettings);
			new TRunNForgetThread(&SetParams, false);
		}
		else
		{
			if(Manager)
			{
				((TRadarMapManager*)Manager)->ShowMessageObject(NULL, ((TRadarMapManager*)Manager)->MapMessages,
					TMessageType::mtStop, mvtDisapering, 5000, "WPS is not connected!", (AnsiString)_RadarMapName);
			}
			StopMovie();
		}
	}
	else ModalResult=mrOk;
}
//---------------------------------------------------------------------------

void __fastcall TTwoWheelsForm::CancelImageClick(TObject *Sender)
{
	if(MoviePanel->Visible) CloseMovieButtonClick(Sender);
	else
	{
		ReceivedStrFlags = rsfNone;
		ModalResult = mrCancel;
	}
}
//---------------------------------------------------------------------------

void __fastcall TTwoWheelsForm::FormClose(TObject *Sender, TCloseAction &Action)
{
	//FormHide(Sender);
}
//---------------------------------------------------------------------------

void __fastcall TTwoWheelsForm::TitlePanelMouseDown(TObject *Sender, TMouseButton Button,
		  TShiftState Shift, int X, int Y)
{
	TitleDown=true;
	TitleXY=ClientToScreen(Types::TPoint(X, Y));
}
//---------------------------------------------------------------------------

void __fastcall TTwoWheelsForm::TitlePanelMouseMove(TObject *Sender, TShiftState Shift,
		  int X, int Y)
{
	Types::TPoint tmp;

	if(TitleDown)
	{
		tmp=ClientToScreen(Types::TPoint(X, Y));
		Left+=tmp.x-TitleXY.x;
		Top+=tmp.y-TitleXY.y;
		TitleXY=tmp;
	}
}
//---------------------------------------------------------------------------

void __fastcall TTwoWheelsForm::TitlePanelMouseUp(TObject *Sender, TMouseButton Button,
		  TShiftState Shift, int X, int Y)
{
	TitleDown=false;
}
//---------------------------------------------------------------------------

void __fastcall TTwoWheelsForm::FormShow(TObject *Sender)
{
	TitleDown=false;
	MoviePanel->Visible=false;
	ApplyButton->Enabled=false;
	Image321->SetupBitmap(true, Color32(0,0,0,0));
	Image321->Bitmap->SetSize(Image321->Width, Image321->Height);
	Image321->Bitmap->Clear();
	ScrollBox1->VertScrollBar->Position=0;
	LeftPprEdit->Text="1000";
	RightPprEdit->Text="1000";
	LeftWheelDEdit->Text="0.124";
	RightWheelDEdit->Text="0.124";
	WheelsBaseEdit->Text="0.285";
	XEdit->Text="0.0";
	YEdit->Text="0.0";
	ManualPullButton->Down=true;
	ManualPushButton->Down=false;
	ReceivedStrFlags=rsfNone;
	SetParamsWay=spwNone;
	Notebook1->PageIndex=0;
	ReloadButtonClick(NULL);
}
//---------------------------------------------------------------------------

void __fastcall TTwoWheelsForm::ManualComponentsControl()
{
	LeftWheelLabel->Enabled=ManualImage->Down;
	LeftWheelDEdit->Enabled=ManualImage->Down;
	RightWheelLabel->Enabled=ManualImage->Down;
	RightWheelDEdit->Enabled=ManualImage->Down;
	WheelsBaseLabel->Enabled=ManualImage->Down;
	WheelsBaseEdit->Enabled=ManualImage->Down;
	ManualPushButton->Enabled=ManualImage->Down;
	ManualPushLabel->Enabled=ManualImage->Down;
	ManualPullButton->Enabled=ManualImage->Down;
	ManualPullLabel->Enabled=ManualImage->Down;
	CalibrateButton->Enabled=CalibrationImage->Down;
}
//---------------------------------------------------------------------------

void __fastcall TTwoWheelsForm::ManualImageClick(TObject *Sender)
{
	((TImageButton*)Sender)->GroupToggle();
	if(!((TImageButton*)Sender)->Down)
	{
		if(Sender != CalibrationImage) CalibrationImage->GroupToggle();
		else ManualImage->GroupToggle();
	}
	ManualComponentsControl();
	ApplyButton->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TTwoWheelsForm::ManualPushButtonClick(TObject *Sender)
{
	((TImageButton*)Sender)->GroupToggle();
	if(!((TImageButton*)Sender)->Down)
	{
		if(Sender != ManualPushButton) ManualPushButton->GroupToggle();
		else ManualPullButton->GroupToggle();
	}
	ApplyButton->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TTwoWheelsForm::ManualLabelClick(TObject *Sender)
{
	ManualImageClick(ManualImage);
}
//---------------------------------------------------------------------------

void __fastcall TTwoWheelsForm::CalibLabelClick(TObject *Sender)
{
	ManualImageClick(CalibrationImage);
}
//---------------------------------------------------------------------------

void __fastcall TTwoWheelsForm::ManualPushLabelClick(TObject *Sender)
{
	ManualPushButtonClick(ManualPushButton);
}
//---------------------------------------------------------------------------

void __fastcall TTwoWheelsForm::ManualPullLabelClick(TObject *Sender)
{
	ManualPushButtonClick(ManualPullButton);
}
//---------------------------------------------------------------------------

void __fastcall TTwoWheelsForm::RightWheelDEditKeyPress(TObject *Sender, wchar_t &Key)

{
	switch(Key)
	{
		case '0':
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
		case 8: break;
		case '.':
		case ',':
			if(((TEdit*)Sender)->Text.Pos(DecimalSeparator)> 0)	Key = 0;
			else Key = DecimalSeparator;
			break;
		default:
			Key = 0;
	}
}
//---------------------------------------------------------------------------

void __fastcall TTwoWheelsForm::PprEditKeyPress(TObject *Sender, wchar_t &Key)
{
	switch(Key)
	{
		case '0':
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
		case 8: break;
		default:
			Key = 0;
	}
}
//---------------------------------------------------------------------------

void __fastcall TTwoWheelsForm::StopMovie()
{
	try
	{
		if(Visible)
		{
			LeftPprLabel->Enabled=true;
			LeftPprEdit->Enabled=true;
			RightPprLabel->Enabled=true;
			RightPprEdit->Enabled=true;
			ManualLabel->Enabled=true;
			ManualImage->Enabled=true;
			ManualComponentsControl();
			CalibrationImage->Enabled=true;
			CalibLabel->Enabled=true;
			XEdit->Enabled=true;
			YEdit->Enabled=true;
			ReloadButton->Enabled=true;
			OkImageButton->Enabled=true;
			CalibDistStartButton->Enabled=true;
			CalibAngleStartButton->Enabled=true;
			if(ResMov) ResMov->Stop();
			MoviePanel->Visible=false;
		}
	}
	catch(...) {}
}
//---------------------------------------------------------------------------

void __fastcall TTwoWheelsForm::StartMovie(bool WaitOrStop, TResourceMovieType tp)
{
// Settings
	LeftPprLabel->Enabled=false;
	LeftPprEdit->Enabled=false;
	RightPprLabel->Enabled=false;
	RightPprEdit->Enabled=false;
	ManualLabel->Enabled=false;
	ManualImage->Enabled=false;
	LeftWheelLabel->Enabled=false;
	LeftWheelDEdit->Enabled=false;
	RightWheelLabel->Enabled=false;
	RightWheelDEdit->Enabled=false;
	WheelsBaseLabel->Enabled=false;
	WheelsBaseEdit->Enabled=false;
	ManualPushButton->Enabled=false;
	ManualPushLabel->Enabled=false;
	ManualPullButton->Enabled=false;
	ManualPullLabel->Enabled=false;
	CalibrationImage->Enabled=false;
	CalibLabel->Enabled=false;
	CalibrateButton->Enabled=false;
	XEdit->Enabled=false;
	YEdit->Enabled=false;
	ReloadButton->Enabled=false;
// Calib Dist
	CalibDistStartButton->Enabled=false;
// Calib Angle
	CalibAngleStartButton->Enabled=false;
	OkImageButton->Enabled=false;
	if(ResMov)
	{
		/*ResMov->Stop();
		ResMov->Clear();
		switch(tp)
		{
			case rmtNone:
			case rmtSettings:
				for(int i=0; i<5; i++) ResMov->Add("Movie_Settings"+IntToStr(i+1));
				break;
			case rmtCalibDist:
				for(int i=0; i<18; i++) ResMov->Add("Movie_Dist_Calib"+IntToStr(i+1));
				break;
			case rmtCalibAngle:
				for(int i=0; i<18; i++) ResMov->Add("Movie_Angle_Calib"+IntToStr(i+1));
				break;
		}
		ResMov->Start();*/
		ResMov->Start(tp);
	}
	WaitLabel->Visible=WaitOrStop;
	StopButton->Enabled=!WaitOrStop;
	StopButton->Visible=!WaitOrStop;
	MoviePanel->Visible=true;
}
//---------------------------------------------------------------------------

void __fastcall TTwoWheelsForm::CloseMovieButtonClick(TObject *Sender)
{
	if(CalibrationType==twctDistance || CalibrationType==twctAngle)
	{
		if(Manager && ((TRadarMapManager*)Manager)->TwoWheelClient && ((TRadarMapManager*)Manager)->TwoWheelClient->Connected)
			((TRadarMapManager*)Manager)->TwoWheelClient->CalibrationStop(twctNone);
	}
	StopMovie();
}
//---------------------------------------------------------------------------

void __fastcall TTwoWheelsForm::GetParams()
{
	if(Manager && ((TRadarMapManager*)Manager)->TwoWheelClient)
		((TRadarMapManager*)Manager)->TwoWheelClient->GetParams(&ParametersParser);
}
//---------------------------------------------------------------------------

void __fastcall TTwoWheelsForm::ReloadButtonClick(TObject *Sender)
{
	CalibrationType=twctNone;
	ReceivedStrFlags &= ~(rsfWheels | rsfLWheel | rsfRWheel | rsfWBase | rsfCenterXY);
	ApplyButton->Enabled = false;
	CalibOdoLabel->Visible = false;
	CalibNeedLabel->Visible = false;
	SerialNumberLabel->Visible = false;
	FWLabel->Visible = false;
	FWLabel->Font->Color = CalibOdoLabel->Font->Color;
	FWUpdateButton->Enabled = false;
	OdometerLabel->Caption = "";
	if(Manager && ((TRadarMapManager*)Manager)->TwoWheelClient && ((TRadarMapManager*)Manager)->TwoWheelClient->Connected)
	{
		StartMovie(true, rmtSettings);
		new TRunNForgetThread(&GetParams, false);
	}
	else
	{
		if(Manager)
		{
			((TRadarMapManager*)Manager)->ShowMessageObject(NULL, ((TRadarMapManager*)Manager)->MapMessages,
				TMessageType::mtStop, mvtDisapering, 5000, "WPS is not connected!", (AnsiString)_RadarMapName);
		}
		StopMovie();
	}
}
//---------------------------------------------------------------------------

bool IsBufDigitsOnly(char* buf, int N, bool Hex) // Hex number has to start from '0x'
{
	int j = 0;
	bool res = true;

	while(j<N && buf[j] != 0 && res)
	{
		if(buf[j] == '-')
		{
			if(j != 0 || buf[j + 1] == 0) res = false;
		}
		else if(buf[j] < 0x30) res = false;
		else if(buf[j] > 0x39) // check if for ASCII digits (from 1 to 64K or HEX in format 0xXXXX, uint16
		{
			if(Hex && N >= 3 && buf[0] == '0' && buf[1] == 'x')
			{
				if(j > 1 && (buf[j] < 'a' || buf[j] > 'f')) res = false;
			}
			else res = false;
		}
		j++;
	}
	if(j == 0) res = false;

	return res;
}
//---------------------------------------------------------------------------

bool IsBufFloatOnly(char* buf, int N)
{
	int j = 0;
	bool res = true, ds = false;

	while(j < N && buf[j] != 0 && res)
	{
		if(buf[j] == DecimalSeparator && !ds) ds = true;
		else if(buf[j] == '-')
		{
			if(j != 0 || buf[j + 1] == 0) res = false;
		}
		else if(buf[j] < 0x30 || buf[j] > 0x39) res = false;
		j++;
	}
	if(j == 0) res = false;

	return res;
}
//---------------------------------------------------------------------------

void __fastcall TTwoWheelsForm::ParametersParser(AnsiString Reply)
{
	int rsf;
	TStringList *strs;

	try
	{
		try
		{
			strs=new TStringList();
			if(Reply!=NULL && Reply!="" && Reply.Length()>0 && Manager && ((TRadarMapManager*)Manager)->TwoWheelClient &&
				((TRadarMapManager*)Manager)->TwoWheelClient->ParseStrings(Reply, strs, ',')>0)
			{
				if(strs->Strings[0].LowerCase()=="$lwheel" && strs->Count>=4 &&
					IsBufFloatOnly(strs->Strings[1].t_str(), strs->Strings[1].Length()) &&
					IsBufDigitsOnly(strs->Strings[2].t_str(), strs->Strings[2].Length(), false))
				{
					LeftPprEdit->Text = strs->Strings[2];
					LeftWheelDEdit->Text = strs->Strings[1];
					ReceivedStrFlags |= rsfLWheel;
					if(strs->Strings[3].LowerCase() == "pull")
						ManualPullButton->GroupToggle();
					else if(strs->Strings[3].LowerCase() == "push")
						ManualPushButton->GroupToggle();
					else ReceivedStrFlags &= ~rsfLWheel;
				}
				else if(strs->Strings[0].LowerCase()=="$rwheel" && strs->Count>=3 &&
					IsBufFloatOnly(strs->Strings[1].t_str(), strs->Strings[1].Length()) &&
					IsBufDigitsOnly(strs->Strings[2].t_str(), strs->Strings[2].Length(), false))
				{
					RightPprEdit->Text = strs->Strings[2];
					RightWheelDEdit->Text = strs->Strings[1];
					ReceivedStrFlags |= rsfRWheel;
					if(strs->Strings[3].LowerCase() == "pull")
						ManualPullButton->GroupToggle();
					else if(strs->Strings[3].LowerCase() == "push")
						ManualPushButton->GroupToggle();
					else ReceivedStrFlags &= ~rsfRWheel;
				}
				else if(strs->Strings[0].LowerCase()=="$wbase" && strs->Count>=2 &&
					IsBufFloatOnly(strs->Strings[1].t_str(), strs->Strings[1].Length()))
				{
					WheelsBaseEdit->Text = strs->Strings[1];
					ReceivedStrFlags |= rsfWBase;
				}
				else if(strs->Strings[0].LowerCase()=="$centerxy" && strs->Count>=5 &&
					IsBufFloatOnly(strs->Strings[1].t_str(), strs->Strings[1].Length()) &&
					IsBufFloatOnly(strs->Strings[3].t_str(), strs->Strings[3].Length()))
				{
					XEdit->Text = strs->Strings[1];
					YEdit->Text = strs->Strings[3];
					ReceivedStrFlags |= rsfCenterXY;
				}
				else if(strs->Strings[0].LowerCase()=="$odometer" && strs->Count>=3 &&
					IsBufFloatOnly(strs->Strings[1].t_str(), strs->Strings[1].Length()))
				{
					float to, co;

					to = StrToFloat(strs->Strings[1]);
					OdometerLabel->Caption = FloatToStrF(to, ffFixed, 10, 2) + strs->Strings[2].LowerCase();
					if(CalibrationImage->Down && strs->Count > 3 &&
						IsBufFloatOnly(strs->Strings[3].t_str(), strs->Strings[3].Length()))
					{
						co = StrToFloat(strs->Strings[3]);
						CalibOdoLabel->Caption = FloatToStrF(to-co, ffFixed, 10, 2) +
							strs->Strings[4].LowerCase() + " pased since last calibration...";
						CalibOdoLabel->Visible = true;
					}
					else CalibOdoLabel->Visible = false;
				}
				else if(strs->Strings[0].LowerCase()=="$calibwhe" && strs->Count>=2 &&
					strs->Strings[1].LowerCase()=="need")
				{
					CalibNeedLabel->Visible = (strs->Strings[2].LowerCase() == "true" ||
						strs->Strings[2].LowerCase() == "on");
				}
				else if(strs->Strings[0].LowerCase()=="$about" && strs->Count>=2)
				{
					if(IsBufFloatOnly(strs->Strings[1].t_str(), strs->Strings[1].Length()))
					{
						FWLabel->Caption = strs->Strings[1].t_str();
						FWLabel->Visible = true;
						/*
						float f1, f2;
						AnsiString wpsver;

						f1 = StrToFloat(strs->Strings[1].t_str());
						wpsver = GetOnlineWPSVersion();
						if(IsBufFloatOnly(wpsver, wpsver.Length()))
						{
							f2 = StrToFloat(wpsver);
							if(f2 > f1)
							{
								FWLabel->Font->Color = clRed;
								FWUpdateButton->Enabled = true;
							}
							else FWUpdateButton->Enabled = false;
						}
						else FWUpdateButton->Enabled = false;
						*/
					}
					else
					{
						FWLabel->Visible = false;
						FWUpdateButton->Enabled = false;
					}
					if(strs->Strings[2].Length() > 2 && strs->Strings[2].Pos("sn") == 1 &&
						IsBufDigitsOnly((char*)(strs->Strings[2].t_str() + 2), strs->Strings[2].Length() - 2, false))
					{
						AnsiString s;

						s = (char*)(strs->Strings[2].t_str() + 2);
						while(s.Length() < 5) s = "0" + s;
						SerialNumberLabel->Caption = "s/n " + s;
						SerialNumberLabel->Visible = true;
					}
					else SerialNumberLabel->Enabled = false;
				}
				else if(strs->Strings[0].LowerCase()=="$invalid")
				{
					ReceivedStrFlags &= ~(rsfWheels | rsfLWheel | rsfRWheel | rsfWBase | rsfCenterXY);
					if(Manager)
					{
						((TRadarMapManager*)Manager)->ShowMessageObject(NULL, ((TRadarMapManager*)Manager)->MapMessages,
							TMessageType::mtStop, mvtDisapering, 5000, "WPS Communication error!", (AnsiString)_RadarMapName);
					}
					StopMovie();
				}
				rsf = rsfLWheel | rsfRWheel | rsfWBase | rsfCenterXY;
				if((ReceivedStrFlags & rsf) == rsf)
					StopMovie();
			}
			else if(Reply=="") StopMovie();
		}
		catch (...) {}
	}
	__finally
	{
		delete strs;
	}
}
//---------------------------------------------------------------------------

void __fastcall TTwoWheelsForm::CalibrateButtonClick(TObject *Sender)
{
	if(ApplyButton->Enabled)
	{
		Application->MessageBox(L"You didn't apply changes. Please, apply it before calibration!",
			L"WPS System", MB_ICONWARNING | MB_OK);
	}
	else
	{
		//CalibrateButton->Visible=false;
		ApplyButton->Enabled = false;
		CalibDistStartButton->Enabled=true;
		CalibDistApplyButton->Enabled=false;
		CalibDistEdit->Enabled=false;
		CalibAngleStartButton->Enabled=true;
		CalibAngleApplyButton->Enabled=false;
		CalibAngleEdit->Enabled=false;
		CalibDistEdit->Text=25.0;
		CalibAngleEdit->Text=3;
		Notebook1->PageIndex=1;
		//CalibrateButton->Enabled=true;
	}
}
//---------------------------------------------------------------------------

void __fastcall TTwoWheelsForm::StopButtonClick(TObject *Sender)
{
	if(CalibrationType==twctDistance)
	{
		CalibDistApplyButton->Enabled=true;
		CalibDistEdit->Enabled=true;
		CalibDistEdit->SetFocus();
	}
	else if(CalibrationType==twctAngle)
	{
		CalibAngleApplyButton->Enabled=true;
		CalibAngleEdit->Enabled=true;
		CalibAngleEdit->SetFocus();
	}
	StopMovie();
}
//---------------------------------------------------------------------------

void __fastcall TTwoWheelsForm::StartCalibDist()
{
	CalibStartOK=0;
	if(Manager && ((TRadarMapManager*)Manager)->TwoWheelClient)
		((TRadarMapManager*)Manager)->TwoWheelClient->CalibDistStart(&CalibParser);
}
//---------------------------------------------------------------------------

void __fastcall TTwoWheelsForm::CalibDistStartButtonClick(TObject *Sender)
{
	CalibrationType=twctDistance;
	ReceivedStrFlags &= (rsfWheels | rsfLWheel | rsfRWheel | rsfWBase | rsfCenterXY);
	if(Manager && ((TRadarMapManager*)Manager)->TwoWheelClient && ((TRadarMapManager*)Manager)->TwoWheelClient->Connected)
	{
		CalibDistApplyButton->Enabled=false;
		CalibDistEdit->Enabled=false;
		StartMovie(true, rmtSettings);
		new TRunNForgetThread(&StartCalibDist, false);
	}
	else
	{
		if(Manager)
		{
			((TRadarMapManager*)Manager)->ShowMessageObject(NULL, ((TRadarMapManager*)Manager)->MapMessages,
				TMessageType::mtStop, mvtDisapering, 5000, "WPS is not connected!", (AnsiString)_RadarMapName);
		}
		StopMovie();
	}
}
//---------------------------------------------------------------------------

void __fastcall TTwoWheelsForm::CalibParser(AnsiString Reply)
{
	TStringList *strs;

	try
	{
		try
		{
			strs=new TStringList();
			if(Reply!=NULL && Reply!="" && Reply.Length()>0 && Manager && ((TRadarMapManager*)Manager)->TwoWheelClient &&
				((TRadarMapManager*)Manager)->TwoWheelClient->ParseStrings(Reply, strs, ',')>0)
			{
				if(strs->Strings[0].LowerCase()=="$calibwhe" && strs->Count>=2 && strs->Strings[1].UpperCase()=="OK")
				{
					if(ReceivedStrFlags & rsfWrongCalibration)
					{
						StopMovie();
						Notebook1->PageIndex=0;
					}
					else if(!(ReceivedStrFlags & rsfCalibDistStart))
					{
						CalibStartOK++;
						if(CalibStartOK > 1)
						{
							sndPlaySound("LETSGO.WAV", SND_SYNC);
							StartMovie(false, rmtCalibDist);
							ReceivedStrFlags |= rsfCalibDistStart;
						}
					}
					else if(!(ReceivedStrFlags & rsfCalibDistStop)) // CalibDistStop compleate
					{
						WaitLabel->Visible=true;
						StopButton->Visible=false;
						StopButton->Enabled=false;
						sndPlaySound("stopped.wav", SND_ASYNC);
						ReceivedStrFlags |= rsfCalibDistStop;
						StopMovie();
						Notebook1->PageIndex = 2;
					}
					else if(!(ReceivedStrFlags & rsfCalibAngleStart))
					{
						CalibStartOK++;
						if(CalibStartOK>1)
						{
							sndPlaySound("LETSGO.WAV", SND_SYNC);
							StartMovie(false, rmtCalibAngle);
							ReceivedStrFlags |= rsfCalibAngleStart;
						}
					}
					else if(!(ReceivedStrFlags & rsfCalibAngleStop)) // CalibAngleStop compleate
					{
						WaitLabel->Visible=true;
						StopButton->Visible=false;
						StopButton->Enabled=false;
						sndPlaySound("stopped.wav", SND_ASYNC);
						ReceivedStrFlags |= rsfCalibAngleStop;
						ReceivedStrFlags &= ~rsfWrongCalibration;
						StopMovie();
						Notebook1->PageIndex = 0;
					}
					else StopMovie();
				}
				else if(strs->Strings[0].LowerCase()=="$invalid")
				{
					if(Manager)
					{
						((TRadarMapManager*)Manager)->ShowMessageObject(NULL, ((TRadarMapManager*)Manager)->MapMessages,
							TMessageType::mtStop, mvtDisapering, 5000, "WPS Communication error!", (AnsiString)_RadarMapName);
					}
					StopMovie();
				}
			}
			//else if(Reply=="") StopMovie();
		}
		catch(...) {}
	}
	__finally
	{
		delete strs;
	}
}
//---------------------------------------------------------------------------

void __fastcall TTwoWheelsForm::ApplyCalibDist()
{
	if(Manager && ((TRadarMapManager*)Manager)->TwoWheelClient)
	{
		((TRadarMapManager*)Manager)->TwoWheelClient->Parameters.CalibDistValue = StrToFloat(CalibDistEdit->Text);
		((TRadarMapManager*)Manager)->TwoWheelClient->CalibDistStop(&CalibParser);
	}
}
//---------------------------------------------------------------------------

void __fastcall TTwoWheelsForm::CalibDistApplyButtonClick(TObject *Sender)
{
	int id;

	if(StrToFloat(CalibDistEdit->Text)<1)
	{
		Application->MessageBox(L"Wheels diameter calibration needs 1 m at least!",
			L"WPS System", MB_ICONWARNING | MB_OK);
	}
	else if(StrToFloat(CalibDistEdit->Text)+0.0001<10)
	{
		id=Application->MessageBox(L"Calibration distance lower than 10 m is not enough for good calibration. Do you want to apply this value (Yes) or stop the calibration (No)?",
			L"WPS System", MB_ICONWARNING | MB_YESNOCANCEL);
	}
	else id=IDYES;
	if(id==IDYES)
	{
		CalibrationType=twctDistance;
		ReceivedStrFlags &= (rsfWheels | rsfLWheel | rsfRWheel | rsfWBase | rsfCenterXY |
			rsfCalibDistStart);
		if(Manager && ((TRadarMapManager*)Manager)->TwoWheelClient && ((TRadarMapManager*)Manager)->TwoWheelClient->Connected)
		{
			if(ReceivedStrFlags & rsfCalibDistStart)
			{
				CalibDistStartButton->Enabled=false;
				CalibDistApplyButton->Enabled=false;
				CalibDistEdit->Enabled=false;
				StartMovie(true, rmtSettings);
				new TRunNForgetThread(&ApplyCalibDist, false);
			}
			else if(Manager)
			{
				((TRadarMapManager*)Manager)->ShowMessageObject(NULL, ((TRadarMapManager*)Manager)->MapMessages,
					TMessageType::mtStop, mvtDisapering, 5000, "Callibration doesn't started!", (AnsiString)_RadarMapName);
			}
		}
		else
		{
			if(Manager)
			{
				((TRadarMapManager*)Manager)->ShowMessageObject(NULL, ((TRadarMapManager*)Manager)->MapMessages,
					TMessageType::mtStop, mvtDisapering, 5000, "WPS is not connected!", (AnsiString)_RadarMapName);
			}
			StopMovie();
		}
	}
	else ReturnButtonClick(CalibDistApplyButton);
}
//---------------------------------------------------------------------------

void __fastcall TTwoWheelsForm::CancelCalibration()
{
    if(Manager && ((TRadarMapManager*)Manager)->TwoWheelClient)
		((TRadarMapManager*)Manager)->TwoWheelClient->CancelCalibration(&CalibParser);
}
//---------------------------------------------------------------------------

void __fastcall TTwoWheelsForm::ReturnButtonClick(TObject *Sender)
{
	ReceivedStrFlags &= (rsfWheels | rsfLWheel | rsfRWheel | rsfWBase | rsfCenterXY);
	ReceivedStrFlags |= rsfWrongCalibration;
	if(Manager && ((TRadarMapManager*)Manager)->TwoWheelClient && ((TRadarMapManager*)Manager)->TwoWheelClient->Connected)
	{
		CalibDistStartButton->Enabled=false;
		CalibDistApplyButton->Enabled=false;
		CalibDistEdit->Enabled=false;
		CalibAngleStartButton->Enabled=false;
		CalibAngleApplyButton->Enabled=false;
		CalibAngleEdit->Enabled=false;
		StartMovie(true, rmtSettings);
		new TRunNForgetThread(&CancelCalibration, false);
	}
}
//---------------------------------------------------------------------------

void __fastcall TTwoWheelsForm::StartCalibAngle()
{
    CalibStartOK=0;
	if(Manager && ((TRadarMapManager*)Manager)->TwoWheelClient)
	{
		((TRadarMapManager*)Manager)->TwoWheelClient->CalibAngleStart(&CalibParser);
	}
}
//---------------------------------------------------------------------------

void __fastcall TTwoWheelsForm::CalibAngleStartButtonClick(TObject *Sender)
{
	CalibrationType=twctAngle;
	ReceivedStrFlags &= (rsfWheels | rsfLWheel | rsfRWheel | rsfWBase | rsfCenterXY |
		rsfCalibDistStart | rsfCalibDistStop);
	if(Manager && ((TRadarMapManager*)Manager)->TwoWheelClient && ((TRadarMapManager*)Manager)->TwoWheelClient->Connected)
	{
		CalibAngleStartButton->Enabled=false;
		CalibAngleApplyButton->Enabled=false;
		CalibAngleEdit->Enabled=false;
		StartMovie(true, rmtSettings);
		new TRunNForgetThread(&StartCalibAngle, false);
	}
	else
	{
		if(Manager)
		{
			((TRadarMapManager*)Manager)->ShowMessageObject(NULL, ((TRadarMapManager*)Manager)->MapMessages,
				TMessageType::mtStop, mvtDisapering, 5000, "WPS is not connected!", (AnsiString)_RadarMapName);
		}
		StopMovie();
	}
}
//---------------------------------------------------------------------------
void __fastcall TTwoWheelsForm::ApplyCalibAngle()
{
	if(Manager && ((TRadarMapManager*)Manager)->TwoWheelClient)
	{
		((TRadarMapManager*)Manager)->TwoWheelClient->Parameters.CalibAngleValue = StrToInt(CalibAngleEdit->Text);
		((TRadarMapManager*)Manager)->TwoWheelClient->CalibAngleStop(&CalibParser);
	}
}
//---------------------------------------------------------------------------

void __fastcall TTwoWheelsForm::CalibAngleApplyButtonClick(TObject *Sender)
{
	if(StrToInt(CalibAngleEdit->Text)==0)
	{
		Application->MessageBox(L"Wheels base calibration needs 1 pased circle at least!",
			L"WPS System", MB_ICONWARNING | MB_OK);
	}
	else
	{
		CalibrationType=twctAngle;
		ReceivedStrFlags &= (rsfWheels | rsfLWheel | rsfRWheel | rsfWBase | rsfCenterXY |
			rsfCalibDistStart | rsfCalibDistStop | rsfCalibAngleStart);
		if(Manager && ((TRadarMapManager*)Manager)->TwoWheelClient && ((TRadarMapManager*)Manager)->TwoWheelClient->Connected)
		{
			if(ReceivedStrFlags & rsfCalibAngleStart)
			{
				CalibAngleStartButton->Enabled=false;
				CalibAngleApplyButton->Enabled=false;
				CalibAngleEdit->Enabled=false;
				StartMovie(true, rmtSettings);
				new TRunNForgetThread(&ApplyCalibAngle, false);
			}
			else if(Manager)
			{
				((TRadarMapManager*)Manager)->ShowMessageObject(NULL, ((TRadarMapManager*)Manager)->MapMessages,
					TMessageType::mtStop, mvtDisapering, 5000, "Callibration doesn't started!", (AnsiString)_RadarMapName);
			}
		}
		else
		{
			if(Manager)
			{
				((TRadarMapManager*)Manager)->ShowMessageObject(NULL, ((TRadarMapManager*)Manager)->MapMessages,
					TMessageType::mtStop, mvtDisapering, 5000, "WPS is not connected!", (AnsiString)_RadarMapName);
			}
			StopMovie();
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TTwoWheelsForm::Notebook1PageChanged(TObject *Sender)
{
	if(Notebook1->PageIndex==0)
	{
		ReturnButton->Visible=false;
		if(ReceivedStrFlags & rsfWrongCalibration)
		{
			if(Manager)
			{
				((TRadarMapManager*)Manager)->ShowMessageObject(NULL, ((TRadarMapManager*)Manager)->MapMessages,
					TMessageType::mtStop, mvtDisapering, 5000, "WPS is not calibrated properly!", (AnsiString)_RadarMapName);
			}
		}
		ReloadButtonClick(NULL);
	}
	/*else if(Notebook1->PageIndex==0)
	{
		ReceivedStrFlags &= (rsfWheels | rsfLWheel | rsfRWheel | rsfWBase | rsfCenterXY);
	}*/
	else
	{
		ReturnButton->Visible=true;
    }
}
//---------------------------------------------------------------------------

void __fastcall TTwoWheelsForm::WheelsBaseEditChange(TObject *Sender)
{
	ApplyButton->Enabled = !MoviePanel->Visible;
}
//---------------------------------------------------------------------------

void __fastcall TTwoWheelsForm::SetParams()
{
	if(Manager && ((TRadarMapManager*)Manager)->TwoWheelClient)
	{

		((TRadarMapManager*)Manager)->TwoWheelClient->Parameters.LeftWheelPPR=StrToInt(LeftPprEdit->Text);
		((TRadarMapManager*)Manager)->TwoWheelClient->Parameters.RightWheelPPR=StrToInt(RightPprEdit->Text);
		((TRadarMapManager*)Manager)->TwoWheelClient->Parameters.CenterX=StrToFloat(XEdit->Text);
		((TRadarMapManager*)Manager)->TwoWheelClient->Parameters.CenterY=StrToFloat(YEdit->Text);
		if(ManualImage->Down)
		{
			((TRadarMapManager*)Manager)->TwoWheelClient->Parameters.LeftWheelD=StrToFloat(LeftWheelDEdit->Text);
			((TRadarMapManager*)Manager)->TwoWheelClient->Parameters.RightWheelD=StrToFloat(RightWheelDEdit->Text);
			((TRadarMapManager*)Manager)->TwoWheelClient->Parameters.WBase=StrToFloat(WheelsBaseEdit->Text);
			((TRadarMapManager*)Manager)->TwoWheelClient->Parameters.RightWheelPP=ManualPushButton->Down;
			((TRadarMapManager*)Manager)->TwoWheelClient->Parameters.LeftWheelPP=ManualPushButton->Down;
			((TRadarMapManager*)Manager)->TwoWheelClient->SetFullParams(&SetParamsParser);
		}
		else ((TRadarMapManager*)Manager)->TwoWheelClient->SetParams(&SetParamsParser);
	}
}
//---------------------------------------------------------------------------

void __fastcall TTwoWheelsForm::ApplyButtonClick(TObject *Sender)
{
	CalibrationType=twctNone;
	ReceivedStrFlags &= ~(rsfWheels | rsfLWheel | rsfRWheel | rsfCenterXY);
	if(Manager && ((TRadarMapManager*)Manager)->TwoWheelClient && ((TRadarMapManager*)Manager)->TwoWheelClient->Connected)
	{
		SetParamsWay=spwApply;
		StartMovie(true, rmtSettings);
		new TRunNForgetThread(&SetParams, false);
	}
	else
	{
		if(Manager)
		{
			((TRadarMapManager*)Manager)->ShowMessageObject(NULL, ((TRadarMapManager*)Manager)->MapMessages,
				TMessageType::mtStop, mvtDisapering, 5000, "WPS is not connected!", (AnsiString)_RadarMapName);
		}
		StopMovie();
	}
}
//---------------------------------------------------------------------------

void __fastcall TTwoWheelsForm::SetParamsParser(AnsiString Reply)
{
	int rsf;
	TStringList *strs;

	try
	{
		try
		{
			strs=new TStringList();
			if(Reply!=NULL && Reply!="" && Reply.Length()>0 && Manager && ((TRadarMapManager*)Manager)->TwoWheelClient &&
				((TRadarMapManager*)Manager)->TwoWheelClient->ParseStrings(Reply, strs, ',')>0)
			{
				//if(Reply.Pos("$lwheel,") >= 1)
				if(strs->Strings[0].LowerCase()=="$lwheel" && strs->Count>=2 &&
					strs->Strings[1].UpperCase()=="OK")
				{
					ReceivedStrFlags |= rsfLWheel;
				}
				else if(strs->Strings[0].LowerCase()=="$rwheel"  && strs->Count>=2 &&
					strs->Strings[1].UpperCase()=="OK")
				{
					ReceivedStrFlags |= rsfRWheel;
				}
				else if(strs->Strings[0].LowerCase()=="$wbase" && strs->Count>=2 &&
					strs->Strings[1].UpperCase()=="OK")
				{
					ReceivedStrFlags |= rsfWBase;
				}
				else if(strs->Strings[0].LowerCase()=="$centerxy" && strs->Count>=2 &&
					strs->Strings[1].UpperCase()=="OK")
				{
					ReceivedStrFlags |= rsfCenterXY;
				}
				else if(strs->Strings[0].LowerCase()=="$invalid")
				{
					ReceivedStrFlags &= ~(rsfWheels | rsfLWheel | rsfRWheel | rsfWBase | rsfCenterXY);
					if(Manager)
					{
						((TRadarMapManager*)Manager)->ShowMessageObject(NULL, ((TRadarMapManager*)Manager)->MapMessages,
							TMessageType::mtStop, mvtDisapering, 5000, "WPS Communication error!", (AnsiString)_RadarMapName);
					}
					StopMovie();
				}
				rsf = rsfLWheel | rsfRWheel | rsfCenterXY;
				if((ReceivedStrFlags & rsf) == rsf)
				{
					StopMovie();
					if(SetParamsWay==spwApply) ApplyButton->Enabled = false;
					else if(SetParamsWay==spwOK) ModalResult=mrOk;
				}
			}
			if(Reply=="") StopMovie();
		}
		catch(...) {}
	}
	__finally
	{
		delete strs;
	}
}
//---------------------------------------------------------------------------

void __fastcall TTwoWheelsForm::Panel10MouseDown(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y)
{
	Panel10MousePressed=true;
	Panel10Y=Y;
}
//---------------------------------------------------------------------------

void __fastcall TTwoWheelsForm::Panel10MouseMove(TObject *Sender, TShiftState Shift,
		  int X, int Y)
{
	if(Panel10MousePressed && Panel10Y>=0) ScrollBox1->VertScrollBar->Position+=Panel10Y-Y;
}
//---------------------------------------------------------------------------

void __fastcall TTwoWheelsForm::Panel10MouseUp(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y)
{
	Panel10MousePressed=false;
	Panel10Y=-1;
}
//---------------------------------------------------------------------------

