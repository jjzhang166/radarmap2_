//---------------------------------------------------------------------------
#ifndef Out_segyH
#define Out_segyH

#include "Profile.h"
#include "MyFiles.h"

int Output_SEGY(char *Name, TProfile* Prof, TObject* Manager=NULL);

int Output_SEGY(char *Name, TProfile* Prof, TProgressBar *PB, TObject* Manager=NULL);

int Output_SEGY_Unshared(TUnsharedFile *cFile, TProfile* Prof, TProgressBar *PB, TObject* Manager=NULL);
//---------------------------------------------------------------------------
#endif
