// -----------------------------------------------------------------------------
// AutoGain.h
// -----------------------------------------------------------------------------

#ifndef AutoGainH
#define AutoGainH
//------------------------------------------------------------------------------
#include "AutomaticDetector.h"
#include "ProfilePeakList.h"

// -----------------------------------------------------------------------------
enum TAutoGainMethod {agmToMaxMethod, agmCamelMethod, agmLinearMethod};

// -----------------------------------------------------------------------------
// TadAutoGain
// -----------------------------------------------------------------------------
class TadAutoGain: public TCustomizeAutomaticDetection
{
private:

    TAutoGainMethod  mGainMethod;
    float horRangeFull, timeRange; /* don' used*/
    float zeroPoint /*all*/;

protected:

    TProfilePeakList *mPeakList;

    float mNoiseProc;
    int mNoiseValue;
    int mMaxValue;

    int __fastcall GetNoiseValue();
	int __fastcall GetFrameMaxValue(TSample *pTraceData, int pSamplesCnt, int pFrameSize, int pSampleIdx);
	int __fastcall GetFrameMaxValue(TSample *pTraceData, int pSamplesCnt, int pFrameSize, int pSampleIdx, TProfilePeakList *pList);
    int __fastcall SearchHill(TSample *pTraceData, int pSamplesCnt, int pStartSampleIdx);

    inline int GetDeltaSamplesSign(int pDeltaValue) {   /* Camel*/
        int trend = (0 == pDeltaValue) ?  0 : (pDeltaValue < 0) ?  -1 :  1;

        return trend;
    }

public:

    __fastcall TadAutoGain(int AWindowWidth, float APermittivity, bool ANeedSaveTrace, float AHorRangeFull
                         , float ATimeRange, float AZeroPoint, int pPositiveMax, TAutoGainMethod pMethod) :
        TCustomizeAutomaticDetection(ptAutoGain) {

        windowwidth = AWindowWidth;
        permittivity = APermittivity;
        needsavetrace = ANeedSaveTrace;
        horRangeFull = AHorRangeFull;
        timeRange = ATimeRange;
        zeroPoint = AZeroPoint;
        mGainMethod = pMethod;

        mNoiseProc = 0.0;
        mNoiseValue = 0;

		mMaxValue = ((int)pow((double)2., (int)(sizeof(int)*4)) >> 1) - 1;//((int)pow(2, sizeof(pPositiveMax) * 4) >> 2) - 1;//32000;
        mPeakList = NULL;
   }
    __fastcall ~TadAutoGain() {}

	virtual void __fastcall Make(HANDLE aStopEvent) = NULL;

    static TadAutoGain * __fastcall GetInstance(int AWindowWidth, float APermittivity, bool ANeedSaveTrace, float AHorRangeFull
                         , float ATimeRange, float AZeroPoint, int pPositiveMax, TAutoGainMethod pMethod);

//    __property float TimeRange = { read = timeRange, write = timeRange };
//    __property float HorRangeFull = { read=horRangeFull, write = horRangeFull };
    __property float ZeroPoint = { read = zeroPoint, write = zeroPoint };
    __property TProfilePeakList *ProfilePeakList = { write = mPeakList};
};

#endif

