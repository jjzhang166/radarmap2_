//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "AutomaticDetector.h"
#include "AutoGain.h"
//#include "HoughHyp.h"
//#include "HoughLines.h"
#include <math.h>

//---------------------------------------------------------------------------

#pragma package(smart_init)

FourierOut2DArrayStruct::~FourierOut2DArrayStruct()
{
	if(FArray)
	{
		for(int k=0; k<M; k++)
		{
			if(FArray[k]) delete[] FArray[k];
		}
		delete[] FArray;
	}
}
//---------------------------------------------------------------------------

FourierOut2DArrayStruct::FourierOut2DArrayStruct()
{
	N=0;
	M=0;
	FArray=NULL;
	needclone=false;
	CycleNumb=0;
}
//---------------------------------------------------------------------------

float __fastcall TCustomizeAutomaticDetection::GetHorRange(TTrace *TraceFrom, TTrace *TraceTo)
{
	float hor_range = 0;
	TTrace *tmptrace;

	if(TraceFrom && TraceTo)
	{
		/*tmptrace = TraceFrom;
		while(tmptrace && tmptrace->PtrUp && tmptrace != TraceTo->PtrUp)
		{
			hor_range += tmptrace->TraceDistance;
			tmptrace = tmptrace->PtrUp;
		}*/
		hor_range=TraceTo->HorPos-TraceFrom->HorPos;
	}
	return hor_range;
}
//---------------------------------------------------------------------------

void __fastcall TadHighPassFilter::Make(HANDLE aStopEvent)
{
	long int i;
	Win=new long int[FirstTrace->Samples];
	try
	{
		try
		{
			for(int k=0;k<FirstTrace->Samples;k++) Win[k]=0;
			Trace=FirstTrace;
			o=0; h=FirstTrace->Samples;
			if(hPww==WindowWidth) //Background removal
			{
				Trace=FirstTrace;
				i=0;
				while(Trace && i<WindowWidth)
				{
					for(int k=o;k<h;k++)
						Win[k]+=Trace->SourceData[k];
					Trace=Trace->PtrUp;
					i++;
					if(aStopEvent && WaitForSingleObject(aStopEvent, 0)==WAIT_OBJECT_0) break;
				}
				for(int k=o;k<h;k++) Win[k]=(double)Win[k]/(double)WindowWidth;
				i=0;
				Trace=FirstTrace;
				while(Trace && i<WindowWidth)
				{
					for(int k=o;k<h;k++)
					{
						tv=(int)(Trace->SourceData[k]-Win[k]);
						if(tv>Trace->PositiveMax) tv=Trace->PositiveMax;
						else if(tv<Trace->NegativeMax) tv=Trace->NegativeMax;
						Trace->SourceData[k]=(int)tv;
					}
					Trace=Trace->PtrUp;
					i++;
					if(aStopEvent && WaitForSingleObject(aStopEvent, 0)==WAIT_OBJECT_0) break;
				}
			}
			else
			{
				TTrace *ptr;
				TTrace* CloneFirst;
				int w2=hPww>>1;//LastTrace->Index-FirstTrace->Index;

				Trace=FirstTrace;
				temptrace=NULL;
				while(Trace && Trace->Index<=LastTrace->Index)
				{
					ptr=new TTrace(Trace->Samples);
					ptr->CopyTraceDataOnly(Trace);
					if(temptrace)
					{
						ptr->PtrDn=temptrace;
						temptrace->PtrUp=ptr;
					}
					else CloneFirst=ptr;
					temptrace=ptr;
					Trace=Trace->PtrUp;
					if(aStopEvent && WaitForSingleObject(aStopEvent, 0)==WAIT_OBJECT_0) break;
				}
				ptr=CloneFirst;
				temptrace=ptr;
				if(hPww>WindowWidth) hPww=WindowWidth;
				for(int j=0;j<hPww;j++)
				{
					for(int k=o;k<h;k++)
						Win[k]+=temptrace->SourceData[k];
					temptrace=temptrace->PtrUp;
				}
				Trace=FirstTrace;
				i=0;
				while(ptr && temptrace && Trace && i<WindowWidth)
				{
					if(i>w2 && i<WindowWidth-w2)
					{
						for(int k=o;k<h;k++)
							Win[k]=Win[k]-ptr->SourceData[k]+temptrace->SourceData[k];
						ptr=ptr->PtrUp;
						temptrace=temptrace->PtrUp;
					}
					for(int k=o;k<h;k++)
					{
						tv=(int)Trace->SourceData[k]-(int)((double)Win[k]/(double)hPww);
						if(tv>Trace->PositiveMax) tv=Trace->PositiveMax;
						else if(tv<Trace->NegativeMax) tv=Trace->NegativeMax;
						Trace->SourceData[k]=(int)tv;
					}
					Trace=Trace->PtrUp;
					i++;
					if(aStopEvent && WaitForSingleObject(aStopEvent, 0)==WAIT_OBJECT_0) break;
				}
				Trace=CloneFirst;
				while(Trace)
				{
					temptrace=Trace->PtrUp;
					delete Trace;
					Trace=temptrace;
				}
			}
		}
		catch(...) {}
	}
	__finally
	{
		delete[] Win;
    }
    ResultFirstTrace=FirstTrace; //creating result traces
	ResultLastTrace=LastTrace;   //creating result traces
}

