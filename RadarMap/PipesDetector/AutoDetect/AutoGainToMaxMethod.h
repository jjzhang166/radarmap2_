// -----------------------------------------------------------------------------
// AutoGainToMaxMethod.h
// -----------------------------------------------------------------------------

#ifndef AutoGainToMaxMethodH
#define AutoGainToMaxMethodH

// -----------------------------------------------------------------------------
#include "AutoGain.h"

// -----------------------------------------------------------------------------
// TadAutoGainToMaxMethod
// -----------------------------------------------------------------------------
class TadAutoGainToMaxMethod : public TadAutoGain {

private:

    int mFrameSize, mFrameShift;

protected:

public:

    __fastcall TadAutoGainToMaxMethod(int AWindowWidth, float APermittivity, bool ANeedSaveTrace, float AHorRangeFull,
                                      float ATimeRange, float AZeroPoint, int pPositiveMax, TAutoGainMethod pMethod)
         : TadAutoGain(AWindowWidth, APermittivity, ANeedSaveTrace, AHorRangeFull, ATimeRange, AZeroPoint, pPositiveMax,
                       agmToMaxMethod) {

        mNoiseProc = 0.0;
        mFrameSize = 32;     // MakeAutoGainToMaxMethod
        mFrameShift = 2;     // MakeAutoGainToMaxMethod
   }
    __fastcall ~TadAutoGainToMaxMethod() {}

    virtual void __fastcall Make(HANDLE aStopEvent);
};
// -----------------------------------------------------------------------------

#endif
