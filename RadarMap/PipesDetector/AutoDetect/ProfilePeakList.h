// -----------------------------------------------------------------------------
// ProfilePeakList.h
// -----------------------------------------------------------------------------

#ifndef ProfilePeakListH
#define ProfilePeakListH

// -----------------------------------------------------------------------------
// TProfilePeak
// -----------------------------------------------------------------------------
struct TProfilePeak {

public:
    float traceDistance;
    long traceIndex;
    int sampleIndex;
    int peakValue;
    int autoGainValue;
    int isPositive;

    TProfilePeak(float pDistance = 0, long pTraceIndex = 0, int pSampleIndex = 0, int pPeakValue = 0, int pAutoGainValue = 0, int pIsPositive = 1) {
        this->traceIndex = pTraceIndex;
        this->sampleIndex = pSampleIndex;
        this->peakValue = pPeakValue;
        this->autoGainValue = pAutoGainValue;
        this->isPositive = pIsPositive;
        this->traceDistance = pDistance;
    }
};

// -----------------------------------------------------------------------------
// TProfilePeakList
// -----------------------------------------------------------------------------
class TProfilePeakList {

private:
    TList *peakList;

    int __fastcall readPeaksCount() {
        return this->peakList->Count;
    }
    TProfilePeak* __fastcall readPeakListItem(int pIndex) {
        if (pIndex >= 0 && pIndex < this->peakList->Count) { return static_cast<TProfilePeak*> (this->peakList->Items[pIndex]); }
        else { return NULL; }
    }
    void __fastcall writePeakListItem(int pIndex, TProfilePeak* value) {
        if (pIndex >= 0 && pIndex < this->peakList->Count) { this->peakList->Items[pIndex] = value; }
    }

protected:

public:
    __fastcall TProfilePeakList() { this->peakList = new TList(); }
    __fastcall ~TProfilePeakList();

    int __fastcall Add(TProfilePeak* pPeak) {
        int i = -1;
        if (pPeak) i = this->peakList->Add(pPeak);
        return i;
    }
    void __fastcall AddAndClear(TProfilePeakList* pList);
    void __fastcall Delete(int pIndex);
    void __fastcall Clear();
    TProfilePeak* __fastcall Remove(int pIndex);

    __property TProfilePeak* Items[int Index] = { read = readPeakListItem, write = writePeakListItem };
    __property int Count = { read = readPeaksCount };
};
//------------------------------------------------------------------------------

#endif
