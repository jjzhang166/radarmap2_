// -----------------------------------------------------------------------------
// AutoGainToMaxMethod.cpp
// -----------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "AutoGainToMaxMethod.h"

#pragma package(smart_init)

// -------- Constructor / Destructor -------------------------------------------

// ---------- Make -------------------------------------------------------------
void __fastcall TadAutoGainToMaxMethod::Make(HANDLE aStopEvent) {
    TTrace *trace;
    TTraceIterator *trace_itr;
	TSample *trace_data = NULL;

    int samples_cnt = FirstTrace->Samples;
    int frame_size = mFrameSize;
    int frame_shift = mFrameShift;
	int frame_max_value, sample_value;
    float koeff = 0.;

    mNoiseValue = this->GetNoiseValue();

    trace_itr = new TTraceIterator();
    trace_itr->SetFirstTracePtr(FirstTrace);

	for (trace = trace_itr->GetCurrentPtr(); ;  trace = trace_itr->GetNext()) {

		trace_data = trace->GetArrayAddress(tatSource);

        for (int i = ZeroPoint; i < samples_cnt; i += frame_shift) {

            if (i + frame_size < samples_cnt) { frame_max_value = this->GetFrameMaxValue(trace_data, samples_cnt, frame_size, i); }

            if (frame_max_value > mNoiseValue) {

                koeff = (float)mMaxValue / (float)frame_max_value;

                for (int j = 0; j < frame_shift; j++) {

                    if (i + j < samples_cnt) {
                        sample_value = trace_data[i + j];
                        trace_data[i + j] = (TSample)(koeff * sample_value);
                    }
                }
            }
        }
         
		if (trace == LastTrace || (aStopEvent && WaitForSingleObject(aStopEvent, 0)==WAIT_OBJECT_0)) { break; }
   }

    ResultFirstTrace=FirstTrace; //creating result traces
    ResultLastTrace=LastTrace;   //creating result traces
    delete trace_itr;
}
// -----------------------------------------------------------------------------

