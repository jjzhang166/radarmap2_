// -----------------------------------------------------------------------------
// AutoGainToMaxMethod.cpp
// -----------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "AutoGainCamelMethod.h"

#pragma package(smart_init)

// -------- Constructor / Destructor -------------------------------------------

// ---------- Make -------------------------------------------------------------
void __fastcall TadAutoGainCamelMethod::Make(HANDLE aStopEvent) {
	TTraceIterator *trace_itr;
    TProfilePeakList peak_list;
    int samples_cnt = FirstTrace->Samples;
	TSample *avg_samples = NULL;
    int *delta_samples = NULL;
    TSample *trace_data = NULL;
    int frame_size = mCamelFrameSize;
    int start_sample, end_frame;
    int trc_nr = 0;

    mNoiseValue = this->GetNoiseValue();

	avg_samples = new TSample[samples_cnt];
    delta_samples = new int[samples_cnt];

	for (int i = 0; i < samples_cnt; i++) { delta_samples[i] = 0; avg_samples[i]=0;}

	trace_itr = new TTraceIterator();
    trace_itr->SetFirstTracePtr(FirstTrace);

	for (TTrace *trace = trace_itr->GetCurrentPtr(); ;  trace = trace_itr->GetNext(), trc_nr++) {
		trace_data = trace->GetArrayAddress(tatSource);

//		if (trc_nr == 392) {
//		   int aa = 0;
//		}

		this->CamelMethodPeakSmooth(avg_samples, samples_cnt, frame_size, 0);
		this->CamelMethodCalcAvgSamples(trace_data, samples_cnt, avg_samples, frame_size);
		start_sample = this->CamelMethodGetStartSample(avg_samples, samples_cnt, ZeroPoint);

//        this->CamelMethodPeakSmooth(avg_samples, samples_cnt, frame_size, start_sample);

        for (int i = start_sample; i < samples_cnt - 1; ) {
            end_frame = this->SearchHill(avg_samples, samples_cnt, i);
            this->CamelMethodGainFrame(avg_samples, samples_cnt, i, end_frame, &peak_list);

            if (this->mPeakList) {
                TProfilePeak *peak = NULL;

                while (peak_list.Count > 0) {
                    peak = peak_list.Remove(0);

                    peak->traceDistance = trace->TraceDistance;
                    peak->traceIndex = trace->Index;
                    peak->autoGainValue = *(avg_samples + peak->sampleIndex);

                    this->mPeakList->Add(peak);
                }
            }
            peak_list.Clear();

            // set start sample of next frame ----------------------------------
            i = end_frame + 1;
        }

        for (int i = 0; i < samples_cnt; i++) { *(trace_data++) = *(avg_samples + i); }

		if (trace == LastTrace || (aStopEvent && WaitForSingleObject(aStopEvent, 0)==WAIT_OBJECT_0)) {
            break;
        }
    }

    ResultFirstTrace=FirstTrace; //creating result traces
    ResultLastTrace=LastTrace;   //creating result traces
    delete trace_itr;
    delete[] avg_samples;
    delete[] delta_samples;
}
// -----------------------------------------------------------------------------

// ---------- CamelMethodCalcAvgSamples ----------------------------------------
void __fastcall TadAutoGainCamelMethod::CamelMethodCalcAvgSamples(TSample *pTraceData, int pSamplesCnt, TSample *pAvgTraceData, int pFrameSize) {
	TSample *ptr_avg_data = pAvgTraceData;
	int start_frame = 0;
    int half_frame = pFrameSize / 2;

	for (int i = 0; i < pSamplesCnt - 1; i++) {
        start_frame = i - half_frame;
        if (start_frame < 0) { start_frame = 0; }

        *(ptr_avg_data++) = this->GetFrameAvgValue(pTraceData, pSamplesCnt, pFrameSize, start_frame);
    }
}
// -----------------------------------------------------------------------------

// ---------- CamelMethodGetStartSample ----------------------------------------
int __fastcall TadAutoGainCamelMethod::CamelMethodGetStartSample(TSample *pTraceData, int pSamplesCnt, int pZeroSampleIdx) {
    int start_sample = pZeroSampleIdx;
    int prev_value = pTraceData[pZeroSampleIdx];
    int sign_prev_value = (prev_value < 0) ? 0 : 1;
    int value = 0, sign_value = 1;

    for (int i = pZeroSampleIdx - 1; i >= 0; i--) {
        value = pTraceData[i];
        sign_value = (value < 0) ? 0 : 1;

        if (sign_value != sign_prev_value) {
            break;
        }
        prev_value = value;
        sign_prev_value = sign_value;
        start_sample--;
    }

    return start_sample;
}
// -----------------------------------------------------------------------------

// ---------- CamelMethodPeakSmooth --------------------------------------------
void __fastcall TadAutoGainCamelMethod::CamelMethodPeakSmooth(TSample *pTraceData, int pSamplesCnt, int pFrameSize, int pStartSampleIdx) {
    int start_sample, end_frame;
	int max_peak_samples = (int) ((float)pFrameSize * mMaxPeakSamplesProc / 100.0 + 0.5);
    int with_frame_idx;

	try
	{
		for (int i = pStartSampleIdx; i < pSamplesCnt - 1; ) {
			end_frame = this->SearchHill(pTraceData, pSamplesCnt, i);

			if (end_frame - i + 1 > max_peak_samples) {
				// set start sample of next frame ----------------------------------
				with_frame_idx = i;
				i = end_frame + 1;
			}
			else {
				// peak smoothing --------------------------------------------------
				int frame_size = end_frame - i + 1;
				int frame_max_value = this->GetFrameMaxValue(pTraceData, pSamplesCnt, frame_size, i);
				double prev_sample_value = (i <= 0) ? 1 : abs(pTraceData[i - 1]) - 1;
				double next_sample_value = (i < pSamplesCnt - 1) ? abs(pTraceData[i + 1]) - 1 : 1;
				double delta;
				int sign, value, new_value;

				for (int j = i;  j <= end_frame; j++) {
					sign = this->GetDeltaSamplesSign(pTraceData[j]);
					if (0 != sign) { break; }
				}

				// peak is positive, than prev and next hill is negative -----------
				if (sign > 0) {
					prev_sample_value = 0 - prev_sample_value;
					next_sample_value = 0 - next_sample_value;
				}
				delta = prev_sample_value;

				for (int j = i;  j <= end_frame; j++) {
					value = abs(pTraceData[j]);

					if (value == frame_max_value) {
						delta = next_sample_value;
						new_value = frame_max_value - 1;
					}
					else {
						new_value = (int)((delta * value) / frame_max_value);
						new_value = delta - new_value;
					}
					pTraceData[j] = new_value;
				}

				// reset start sample to prev width frame and repeat hill search ---
				i = with_frame_idx;
			}
		}
	}
	catch(...) {}
}
// -----------------------------------------------------------------------------

// ---------- CamelMethodGaimFrame ---------------------------------------------
void __fastcall TadAutoGainCamelMethod::CamelMethodGainFrame(TSample *pTraceData, int pSamplesCnt, int pStartSample, int pEndSample, TProfilePeakList *pList) {
    int frame_size = pEndSample - pStartSample + 1;
    int frame_max_value = this->GetFrameMaxValue(pTraceData, pSamplesCnt, frame_size, pStartSample, pList);

	if ((frame_max_value != 0) && (frame_max_value > mNoiseValue)) {
		TSample *ptr_sample = pTraceData + pStartSample;
        int sample_value;
        float koeff = (float)mMaxValue / (float)frame_max_value;

        for (int i = pStartSample; i < pEndSample + 1; i++) {
             sample_value = *ptr_sample;
             *(ptr_sample++) = (int)(koeff * sample_value);
        }
    }
}
// -----------------------------------------------------------------------------

// ---------- GetFrameAvgValue -------------------------------------------------
int __fastcall TadAutoGainCamelMethod::GetFrameAvgValue(TSample *pTraceData, int pSamplesCnt, int pFrameSize, int pSampleIdx) {
	TSample *ptr_sample = pTraceData + pSampleIdx;
    int result = *ptr_sample;
    int end_idx = min(pSampleIdx + pFrameSize, pSamplesCnt);
    long sum = 0;

    for (int i = pSampleIdx;  i < end_idx; i++) {
        sum += *(ptr_sample++);
    }
    result = sum / (end_idx - pSampleIdx);

    return result;
}
// -----------------------------------------------------------------------------

