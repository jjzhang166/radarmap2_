// -----------------------------------------------------------------------------
// AutoGainLinearMethod.cpp
// -----------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "AutoGainLinearMethod.h"

#pragma package(smart_init)

// -------- Constructor / Destructor -------------------------------------------

// ---------- Make -------------------------------------------------------------
void __fastcall TadAutoGainLinearMethod::Make(HANDLE aStopEvent) {
    TTraceIterator trace_itr;
    TProfilePeakList peak_list;
    TProfilePeak max_peak;
    int samples_cnt = this->FirstTrace->Samples;
    int *copy_samples = NULL;
    TSample *trace_data = NULL;
    int start_sample = this->ZeroPoint;

    int *max_value_buf = NULL;
    int *max_sample_buf = NULL;
    int max_buf_size = mTraceFrameSize + 1;
    int max_buf_count = 0;
    int total_max_buf_value = 0;
    int total_max_buf_index = 0;
    double max_sample_koeff = 1, max_sample_index;

    try {

        try {
            max_value_buf = new int[max_buf_size];
            max_sample_buf = new int[max_buf_size];
            copy_samples = new int[samples_cnt];

            this->mNoiseValue = this->GetNoiseValue();

            trace_itr.SetFirstTracePtr(this->FirstTrace);

            for (TTrace *trace = trace_itr.GetCurrentPtr(); ;  trace = trace_itr.GetNext()) {

                // make copy of source trace samples ---------------------------
                trace_data = trace->GetArrayAddress(tatSource);
                memcpy(copy_samples, trace_data, sizeof(int) * samples_cnt);

                // find maximum of trace samples value -------------------------
                max_peak.peakValue = 0;
                max_peak.sampleIndex = -1;

                this->FindTraceMaxValue(trace_data, samples_cnt, start_sample, &max_peak);

                // push max trace samples value to buffer ----------------------
                if (max_buf_count >= max_buf_size) {
                    total_max_buf_value -= max_value_buf[0];
                    total_max_buf_index -= max_sample_buf[0];

                    max_buf_count = max_buf_size - 1;

                    for (int i = 0; i < max_buf_count; i++) {
                        max_value_buf[i] = max_value_buf[i + 1];
                        max_sample_buf[i] = max_sample_buf[i + 1];
                    }
                }

                max_value_buf[max_buf_count] = max_peak.peakValue;
                max_sample_buf[max_buf_count] = max_peak.sampleIndex;
                total_max_buf_value += max_peak.peakValue;
                total_max_buf_index += max_peak.sampleIndex;

                max_buf_count++;

                // calculate averange frame maximum ----------------------------
                max_sample_index = (double)total_max_buf_index / (double)(max_buf_count);

                max_sample_koeff = (double)total_max_buf_value / (double)(max_buf_count);
                max_sample_koeff = (double)mMaxValue / max_sample_koeff;
                max_sample_koeff = 1.0 + (max_sample_koeff - 1.0) * mGainPercentage;

                // calculate gain of source trace samples ----------------------
                this->CalcGainValues(trace_data, samples_cnt, max_sample_index, max_sample_koeff);

                // fill peak information and add peak to common list -----------
                if (this->mPeakList) {
                    TProfilePeak *peak = NULL;

                    this->FillTracePeakList(trace_data, samples_cnt, &peak_list);

                    while (peak_list.Count > 0) {
                        peak = peak_list.Remove(0);

                        peak->traceDistance = trace->TraceDistance;
                        peak->traceIndex = trace->Index;
                        peak->peakValue = *(copy_samples + peak->sampleIndex);
                        peak->autoGainValue = *(trace_data + peak->sampleIndex);

                        this->mPeakList->Add(peak);
                    }
                    peak_list.Clear();
                }

                if (trace == this->LastTrace || (aStopEvent && WaitForSingleObject(aStopEvent, 0)==WAIT_OBJECT_0)) { break; }
            }

            this->ResultFirstTrace = this->FirstTrace; //creating result traces
            this->ResultLastTrace = this->LastTrace;   //creating result traces
        }
        catch(...) {}
    }
    __finally
    {
        if (copy_samples) { delete[] copy_samples; }
        if (max_value_buf) { delete[] max_value_buf; }
        if (max_sample_buf) { delete[] max_sample_buf; }
    }
}
// -----------------------------------------------------------------------------

// ---------- FindTraceMaxValue ------------------------------------------------
bool __fastcall TadAutoGainLinearMethod::FindTraceMaxValue(TSample *pTraceData, int pSamplesCnt, int pStartSample, TProfilePeak *pMaxInfo) {
	TSample *ptr_sample = pTraceData + pStartSample;
    int sample_value;
    bool is_changed = false;

    for (int i = pStartSample; i < pSamplesCnt; i++) {
        sample_value = abs(*(ptr_sample++));

        if (sample_value > pMaxInfo->peakValue) {
            pMaxInfo->peakValue = sample_value;
            pMaxInfo->sampleIndex = i;
            pMaxInfo->isPositive = (*ptr_sample >= 0) ? 1 : 0;

            is_changed = true;
        }
    }
    return is_changed;
}
// -----------------------------------------------------------------------------

// ---------- CalcGainValues ---------------------------------------------------
void __fastcall TadAutoGainLinearMethod::CalcGainValues(TSample *pTraceData, int pSamplesCnt, double pMaxAmpSampleIdx, double pMaxAmpSampleKoeff) {
    TSample *ptr_sample = pTraceData + (int)this->ZeroPoint;
    int samples_cnt = FirstTrace->Samples;
    int sample_value, gain_value;
    double koeff = pMaxAmpSampleKoeff / pMaxAmpSampleIdx;

    for (int i = this->ZeroPoint; i < samples_cnt; i++) {
         sample_value = *(ptr_sample);

         if (abs(sample_value) > mNoiseValue) {
             gain_value = (int)(koeff * (double)i * (double)sample_value);

             if (abs(gain_value) > mMaxValue) {
                 if (gain_value > 0) { gain_value = mMaxValue - 1; }
                 else { gain_value = (mMaxValue - 2) * -1; }
             }
             *ptr_sample = gain_value;
         }
         ptr_sample++;
    }
}
// -----------------------------------------------------------------------------

// ---------- FillTracePeakList ------------------------------------------------
void __fastcall TadAutoGainLinearMethod::FillTracePeakList(TSample *pTraceData, int pSamplesCnt, TProfilePeakList *pList) {
    TProfilePeakList peak_list;
    int start_sample = this->ZeroPoint;
    int end_frame, frame_size, frame_max_value;

    for (int i = start_sample; i < pSamplesCnt - 1; ) {
        end_frame = this->SearchHill(pTraceData, pSamplesCnt, i);
        frame_size = end_frame - i + 1;
        frame_max_value = this->GetFrameMaxValue(pTraceData, pSamplesCnt, frame_size, i, &peak_list);

        if ((frame_max_value != 0) && (frame_max_value > mNoiseValue)) {
            pList->AddAndClear(&peak_list);
        }
        peak_list.Clear();

        // set start sample of next frame --------------------------------------
        i = end_frame + 1;
    }
}
// -----------------------------------------------------------------------------

