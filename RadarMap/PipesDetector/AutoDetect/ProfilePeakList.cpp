// -----------------------------------------------------------------------------
// ProfilePeakList.cpp
// -----------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "ProfilePeakList.h"

#pragma package(smart_init)

// -------- Constructor / Destructor -------------------------------------------
__fastcall TProfilePeakList::~TProfilePeakList() {

     this->Clear();
     delete this->peakList;
}
// -----------------------------------------------------------------------------

// -------- AddAndClear --------------------------------------------------------
void __fastcall TProfilePeakList::AddAndClear(TProfilePeakList* pList) {

    while (pList->Count > 0) {
    	this->Add(pList->Remove(0));
    }
}
//------------------------------------------------------------------------------

// -------- Delete -------------------------------------------------------------
void __fastcall TProfilePeakList::Delete(int pIndex) {

    if (pIndex >= 0 && pIndex < this->peakList->Count) {
    	delete static_cast<TProfilePeak*> (this->peakList->Items[pIndex]);
    	this->peakList->Delete(pIndex);                                                                 
    }
}
//------------------------------------------------------------------------------

// -------- Clear --------------------------------------------------------------
void __fastcall TProfilePeakList::Clear() {

    if (this->peakList != NULL) {

    	try {
	    for (int i = 0; i < this->peakList->Count; i++) {
	    	delete static_cast<TProfilePeak*> (this->peakList->Items[i]);
	    }
	}
	__finally  {
	    this->peakList->Clear();
	}
    }
}
//------------------------------------------------------------------------------

// -------- Remove -------------------------------------------------------------
TProfilePeak* __fastcall TProfilePeakList::Remove(int pIndex) {
    TProfilePeak *peak = NULL;

    if (pIndex >= 0 && pIndex < this->peakList->Count) {
    	peak = this->Items[pIndex];
    	this->peakList->Delete(pIndex);
    }
    return peak;
}
//------------------------------------------------------------------------------

