//---------------------------------------------------------------------------

#ifndef AutomaticDetectorH
#define AutomaticDetectorH
//---------------------------------------------------------------------------
#include "Profile.h"
#include "MyThreads.h"
#include <math.h>
#include "ProfilePeakList.h"

//---------------------------------------------------------------------------
enum TMyProcessType {ptBackgroundRem, ptDetection, ptMigration, ptEnvelope, ptHighPassFilter,
								ptDrawProcess, ptAutoGain, ptCoordExport, ptXinterp, ptLines, ptHyp};
enum TMyWorkMode {wmHough = 0, wmMigration = 1};
enum TAutoDetectionProcessMethod {adpMaxDetect, adpRecursive};
//---------------------------------------------------------------------------

int min (int __t1, int __t2) { if(__t1<__t2) return __t1; else return __t2;}

//////-----------------------------Table of detected objects
struct DOTableStruct
{
	int DOSampleIndexArray;
	int DOTraceIndexArray;
	float DObjectDataArray;
	float DOZ;
	double DOLatitude;
	double DOLongitude;
	float Permit;
	double Accum;
	bool Detection_Predicted;
};

//////-----------------------------------First and Last traces of the Window
struct WindowFirstAndLastTrace
{
	TTrace *FirstTrace, *LastTrace;
};

//////-----------------------------------
struct FourierOut2DArrayStruct
{
    Complex **FArray;
    int M;
    int N;
    int CycleNumb;
    bool needclone;
    FourierOut2DArrayStruct();
    ~FourierOut2DArrayStruct();
};
//////-----------------------------------

class TCustomizeAutomaticDetection: public TObject
{
private:
    TMyProcessType type;
protected:
    TTrace *firsttrace, *lasttrace, *resultfirsttrace, *resultlasttrace, *temptrace;
    float max, mean, permittivity, e_pixels;
	int windowwidth, shift, val;
    bool needsavetrace, fullwindow;

    float __fastcall GetHorRange(TTrace *TraceFrom, TTrace *TraceTo);
public:
    __fastcall TCustomizeAutomaticDetection(TMyProcessType AType)
        : TObject() {type=AType; firsttrace=NULL; resultlasttrace=NULL; resultfirsttrace=NULL; lasttrace=NULL;}
    __fastcall ~TCustomizeAutomaticDetection() {}

	virtual void __fastcall Make(HANDLE aStopEvent) {}
	virtual void __fastcall AddBlock(TMyProcessType AType) {}
    virtual void __fastcall AddTrace(TMyProcessType AType) {}
	__property TTrace *FirstTrace ={read=firsttrace,write=firsttrace};
	__property TTrace *LastTrace ={read=lasttrace,write=lasttrace};
	__property TTrace *ResultFirstTrace ={read=resultfirsttrace, write=resultfirsttrace};
	__property TTrace *ResultLastTrace ={read=resultlasttrace, write=resultlasttrace};
	__property float Permittivity ={read=permittivity,write=permittivity};
	__property float E_Pixels ={read=e_pixels,write=e_pixels};
	__property int WindowWidth ={read=windowwidth,write=windowwidth};
	__property bool NeedSaveTrace ={read=needsavetrace,write=needsavetrace};
	__property bool FW ={read=fullwindow,write=fullwindow};
	__property int Shift ={read=shift,write=shift};
	__property TMyProcessType Type ={read=type};

};

//////-----------------------------------TadHighPassFilter
class TadHighPassFilter: public TCustomizeAutomaticDetection
{
private:
	TProfile *profile;
	TTrace *Trace, *iterTrace;
	int tv,o,h;
	long int *Win;
	int hPww;
protected:
	void __fastcall writeHPww(int value) {if(value>0) hPww=value;}
public:
	__fastcall TadHighPassFilter(TProfile *AProfile, int AWindowWidth, int AHPww)
		: TCustomizeAutomaticDetection(ptHighPassFilter) {profile=AProfile; windowwidth=AWindowWidth; HPww=AHPww;}
	__fastcall ~TadHighPassFilter() {}

	virtual void __fastcall Make(HANDLE aStopEvent);
	__property int HPww ={read=hPww, write=writeHPww};
};

#endif


