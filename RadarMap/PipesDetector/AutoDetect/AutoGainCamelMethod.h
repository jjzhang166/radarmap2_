// -----------------------------------------------------------------------------
// AutoGainCamelMethod.h
// -----------------------------------------------------------------------------

#ifndef AutoGainCamelMethodH
#define AutoGainCamelMethodH

// -----------------------------------------------------------------------------
#include "AutoGain.h"

// -----------------------------------------------------------------------------
// TadAutoGainCamelMethod
// -----------------------------------------------------------------------------
class TadAutoGainCamelMethod : public TadAutoGain {

private:

	float mMaxPeakSamplesProc;
    int mCamelFrameSize;

	void __fastcall CamelMethodCalcAvgSamples(TSample *pTraceData, int pSamplesCnt, TSample *pAvgTraceData, int pFrameSize);
	int __fastcall CamelMethodGetStartSample(TSample *pTraceData, int pSamplesCnt, int pZeroSampleIdx);
    void __fastcall CamelMethodPeakSmooth(TSample *pTraceData, int pSamplesCnt, int pFrameSize, int pStartSampleIdx);
    void __fastcall CamelMethodGainFrame(TSample *pTraceData, int pSamplesCnt, int pStartSample, int pEndSample, TProfilePeakList *pList);
	int __fastcall GetFrameAvgValue(TSample *pTraceData, int pSamplesCnt, int pFrameSize, int pSampleIdx);

protected:

public:

    __fastcall TadAutoGainCamelMethod(int AWindowWidth, float APermittivity, bool ANeedSaveTrace, float AHorRangeFull,
                       float ATimeRange, float AZeroPoint, int pPositiveMax, TAutoGainMethod pMethod)
         : TadAutoGain(AWindowWidth, APermittivity, ANeedSaveTrace, AHorRangeFull, ATimeRange, AZeroPoint, pPositiveMax,
                       agmCamelMethod) {

        mNoiseProc = 0.0;
        mCamelFrameSize = 10;
        mMaxPeakSamplesProc = 60.0;   // procent from  mCamelFrameSize
   }
    __fastcall ~TadAutoGainCamelMethod() {}

	virtual void __fastcall Make(HANDLE aStopEvent);
};
// -----------------------------------------------------------------------------

#endif

