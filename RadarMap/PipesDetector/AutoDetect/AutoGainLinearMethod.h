// -----------------------------------------------------------------------------
// AutoGainLinearMethod.h
// -----------------------------------------------------------------------------

#ifndef AutoGainLinearMethodH
#define AutoGainLinearMethodH

#include "AutoGain.h"

// -----------------------------------------------------------------------------
// TadAutoGainLinearMethod
// -----------------------------------------------------------------------------
class TadAutoGainLinearMethod : public TadAutoGain {

private:

    int mTraceFrameSize;
    double mGainPercentage;

	bool __fastcall FindTraceMaxValue(TSample *pTraceData, int pSamplesCnt, int pStartSample, TProfilePeak *pMaxInfo);
	void __fastcall CalcGainValues(TSample *pTraceData, int pSamplesCnt, double pMaxAmpSampleIdx, double pMaxAmpSampleKoeff);
    void __fastcall FillTracePeakList(TSample *pTraceData, int pSamplesCnt, TProfilePeakList *pList);

protected:

public:

    __fastcall TadAutoGainLinearMethod(int AWindowWidth, float APermittivity, bool ANeedSaveTrace, float AHorRangeFull,
                       float ATimeRange, float AZeroPoint, int pPositiveMax, TAutoGainMethod pMethod)
         : TadAutoGain(AWindowWidth, APermittivity, ANeedSaveTrace, AHorRangeFull, ATimeRange, AZeroPoint, pPositiveMax,
                       agmLinearMethod) {

        mNoiseProc = 0.0;
        mTraceFrameSize = 100;
        mGainPercentage = 1.;//1.;
   }
    __fastcall ~TadAutoGainLinearMethod() {}

	virtual void __fastcall Make(HANDLE aStopEvent);

    __property int TraceFrameSize = {read = mTraceFrameSize, write = mTraceFrameSize};
    __property double GainPercentage = {read = mGainPercentage, write = mGainPercentage};
};
// -----------------------------------------------------------------------------
#endif
