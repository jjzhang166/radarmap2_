//---------------------------------------------------------------------------

#ifndef HoughHypH
#define HoughHypH
//---------------------------------------------------------------------------
#include "AutomaticDetector.h"
#include <math.h>
#include <Classes.hpp>
#include "Defines.h"

typedef float THoughValue;
//---------------------------------------------------------------------------
template <class A_Type> class TFlaxibleArray
{
private:
	int size;
	A_Type *data;
	A_Type __fastcall readData(int Index) {if(data && Index >= 0 && Index < size) return data[Index]; else return 0;}
	void __fastcall writeData(int Index, A_Type value);
public:
	__fastcall TFlaxibleArray() {data=NULL; size=0;}
	__fastcall TFlaxibleArray(int N) {size=N; data=new A_Type[N]; memset(data, 0, sizeof(A_Type)*N);}
	__fastcall ~TFlaxibleArray() {if(data) delete[] data; data=NULL;}

	void __fastcall Clear() {if(data) memset(data, 0, sizeof(A_Type)*size);}

	__property int Size = {read=size};
	__property A_Type Data[int Index] = {read=readData, write=writeData};
};
//---------------------------------------------------------------------------

enum TObjectType {otLine = 0, otHyperbola = 1};
//---------------------------------------------------------------------------

class THyperbola
{
private:
    TDoublePoint* points;
    TDoublePoint center;
    int branchlength, size, head;
    double permit;
    bool automaticupdate;
    TDoublePoint __fastcall readPoint(int Index) {if(Index>=0 && Index<size) return points[Index]; else return DoublePoint(0, 0);}
    void __fastcall writePermit(double value) {permit=value; if(automaticupdate) Update();}
    void __fastcall writeCenter(TDoublePoint value) {center=value; if(automaticupdate) Update();}
    void __fastcall writeBranchLength(int value) {branchlength=value; if(automaticupdate) Update();}
public:
    __fastcall THyperbola(double APermit, TDoublePoint ACenter, int ABranchLength);
	__fastcall ~THyperbola() {if(points) delete[] points; points=NULL;}

    void __fastcall Update();

    __property bool AutomaticUpdate = {read=automaticupdate, write=automaticupdate};
    __property double Epsilon = {read = permit, write=writePermit};
    __property TDoublePoint Points[int index] = {read=readPoint};
    __property int Size = {read=size};
    __property int HeadIndex = {read=head};
    __property TDoublePoint Center = {read=center, write=writeCenter};
    __property int BranchLength = {read=branchlength, write=writeBranchLength};
};
//---------------------------------------------------------------------------

class TFoundObject
{
private:
    THyperbola *hyperbola;
protected:
	int x, y, branchlength;
	bool positive;
	float e, acc, accthd, accmax, ecoeff, weight, side, hypvalue;
	virtual int __fastcall readX() {return x;}
	virtual int __fastcall readY() {return y;}
	virtual float __fastcall readE() {return e;}
	float __fastcall readAcc() {return acc;}
	void __fastcall RecalculWeight();
	void __fastcall writeAccThd(float value) {accthd=value; RecalculWeight();}
	void __fastcall writeAccMax(float value) {accmax=value; RecalculWeight();}
	void __fastcall writeSide(float value) {side=value; RecalculWeight();}
	void __fastcall writeHypValue(float value) {hypvalue=value; RecalculWeight();}
public:
	__fastcall TFoundObject(int aX, int aY, float aE, float aECoeff, int aAcc);
	__fastcall ~TFoundObject();

	void __fastcall FinalizeIt();

	__property int X = {read=readX};
	__property int Y = {read=readY};
	__property float E = {read=readE}; // Epsilon (Permittivity) value
	__property float ECoeff = {read=ecoeff};
	__property float Acc = {read=readAcc}; //Accumulator value
	__property float AccThreshold = {read=accthd, write=writeAccThd};
	__property float AccMax = {read=accmax, write=writeAccMax};
	__property float Side = {read=side, write=writeSide};
	__property float Weight = {read=weight};
	__property float HypValue = {read=hypvalue, write=writeHypValue};
	__property bool Positive = {read=positive, write=positive};
    __property THyperbola *Hyperbola = {read=hyperbola};
    __property int BranchLength = {read=branchlength, write=branchlength};
};
//---------------------------------------------------------------------------

class TFoundObjectList : public TFoundObject
{
private:
	TList* List;
    float mine, maxe;
    int __fastcall readCount() {return List->Count;}
    TFoundObject* __fastcall readItem(int Index) {if(Index>=0 && Index<List->Count) return (TFoundObject*)List->Items[Index]; else return NULL;}
    void __fastcall writeItem(int Index, TFoundObject* value) {if(Index>=0 && Index<List->Count) List->Items[Index]=value;}
protected:
    int __fastcall readX() {return (int)((float)x/(float)List->Count);}
    int __fastcall readY() {return (int)((float)y/(float)List->Count);};
    float __fastcall readE() {return e/(float)List->Count;}
public:
	__fastcall TFoundObjectList() : TFoundObject(0, 0, 0, 0, 0) {List=new TList(); x=y=0; e=acc=0; mine=maxe=-1;}
    __fastcall ~TFoundObjectList() {Clear(); delete List;}

    int __fastcall Add(TFoundObject *Item);
    void __fastcall Delete(int Index, bool recalcul);
    void __fastcall Delete(int Index) {Delete(Index, true);}
    void __fastcall Clear() {int c=List->Count; for(int i=0; i<c; i++) {TFoundObject* fo=(TFoundObject*)List->Items[i]; if(fo) try {delete fo;} catch(...) {}} List->Clear(); x=y=0; e=acc=0; mine=maxe=-1;}

    __property int Count = {read=readCount};
	__property TFoundObject* Items[int Index] = {read=readItem, write=writeItem};
    __property float MinE = {read=mine};
    __property float MaxE = {read=maxe};
};
//---------------------------------------------------------------------------

struct TAccumValue
{
	THoughValue Total;
	THoughValue LeftBranch;
	THoughValue RightBranch;
	THoughValue Side; //-1..0..1 - negative for left and positive for right
};
//---------------------------------------------------------------------------

TAccumValue AccumValue(double aTotal, double aLeft, double aRight, double aSide) {TAccumValue out; out.Total=aTotal; out.LeftBranch=aLeft; out.RightBranch=aRight; out.Side=aSide; return out;}
//---------------------------------------------------------------------------

class THoughHyp: public TCustomizeAutomaticDetection
{
private:
	char **processBuf;
	int inputdatacnt;
	TDouble3DPoint *inputdata;
	int width, height, period, branchlength;
	float permfrom, permto, permstep;
	int maxmax, average_thickness_half;
	float GaussDistrib[10][10], GR_sigma, GD_NormFact;
	int GN, GN2;
	bool positive;
	TProfilePeakList *peak_List;
	TFoundObjectList *objectslist;
	float perm_conv_coef;
	TProfile *profile;
	float __fastcall EpsilonToPixels();
	//void __fastcall Precise_the_points();
	void __fastcall PermitSigmaFilter();
	void __fastcall SaveBitmap(AnsiString AFileName, int AWidth, int AHeight, char **AInBuf);
public:
	__fastcall THoughHyp(TProfilePeakList *aPeak_List, int aWidth, int aHeight,
		float aPermFrom, float aPermTo, float aPermStep, TFoundObjectList *aObjectsList,
		TProfile *aProfile);
	__fastcall ~THoughHyp();

	void __fastcall Make(HANDLE aStopEvent);
	bool __fastcall Process(HANDLE aStopEvent);
	void __fastcall GetInitialData(HANDLE aStopEvent);//TBitmap32 *ABm);
	void __fastcall Precise_the_points();

	__property int Width = {read=width};
	__property int Height = {read=height};
	__property float PermFrom = {read=permfrom};
	__property float PermTo = {read=permto};
	__property float PermStep = {read=permstep};
	__property int BranchLength = {read=branchlength};
	__property int Period = {read=period};
	__property int MaxMax = {read=maxmax, write=maxmax};
	__property int Average_thickness = {read=average_thickness_half};
	__property TDouble3DPoint *InputData = {read=inputdata};
	__property int InputDataCount = {read=inputdatacnt};
	__property float PixelsEpsilonCoef = {read=perm_conv_coef};
	__property TFoundObjectList *ObjectsList = {read=objectslist};
	__property TProfilePeakList *Peak_List = {read=peak_List};
	__property bool Positive = {read=positive, write=positive};
};
//---------------------------------------------------------------------------

#endif
