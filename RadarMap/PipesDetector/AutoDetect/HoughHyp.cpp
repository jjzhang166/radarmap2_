//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "HoughHyp.h"
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
// TFlaxibleArray<A_Type>
//---------------------------------------------------------------------------
template <class A_Type> void __fastcall TFlaxibleArray<A_Type>::writeData(int Index, A_Type value)
{
    if(data && Index>=0 && Index<size)
    {
        data[Index]=value;
    }
}

//---------------------------------------------------------------------------
// THyperbola
//---------------------------------------------------------------------------
__fastcall THyperbola::THyperbola(double APermit, TDoublePoint ACenter, int ABranchLength)
{
    size=0;
	automaticupdate=true;
	permit=APermit;
	center=ACenter;
    branchlength=ABranchLength;
    points=new TDoublePoint[branchlength*2+1];
    Update();
}
//---------------------------------------------------------------------------

void __fastcall THyperbola::Update()
{
    int x, y, hold=-1, dh, z;

    size=0;
    try
    {
        for(int i=branchlength; i>=-branchlength; i--)
		{
            if(i==0)
			{
                head=size;
                points[size++]=DoublePoint(center.X, center.Y);
            }
            else
            {
                x=center.X-i;
                y=permit*i*i+center.Y*center.Y;
                if(y>0)
                {
					y=(int)sqrt((float)y);
                    if(sqrt((center.X-x)*(center.X-x)+(center.Y-y)*(center.Y-y))<branchlength)
                    {
                        if(hold>=0)
                        {
                            dh=y-hold;
                            for(z=1; z<abs(dh); z++)
                                points[size++]=DoublePoint(x, hold+z*dh/abs(dh));
                        }
                        hold=y;/**/
                        points[size++]=DoublePoint(x, y);
                    }
                }
            }
        }
    }
    catch(...)
    {
        size=0;
    }
}

//---------------------------------------------------------------------------
// TFoundObject
//---------------------------------------------------------------------------
__fastcall TFoundObject::TFoundObject(int aX, int aY, float aE, float aECoeff, int aAcc)
{
    x=aX;
    y=aY;
	e=aE;
	ecoeff=aECoeff;
	acc=aAcc;
	weight=-1.;
	accthd=accmax=side=hypvalue=0.;
	branchlength=100;
	hyperbola=NULL;
	positive=true;
}
//---------------------------------------------------------------------------

__fastcall TFoundObject::~TFoundObject()
{
	try
	{
		if(hyperbola) delete hyperbola;
	}
	catch (Exception &exception) {return;}
}
//---------------------------------------------------------------------------

void __fastcall TFoundObject::RecalculWeight()
{
	/*if(accmax>0 && accthd>0 && acc>0)
	{
		if(accmax==accthd) weight=0.5;
		else weight=0.5*(acc-accthd)/(accmax-accthd);
		weight*=fabs(side);
		weight+=0.5;
		if(weight>1.)
			weight=1.;
		weight*=0.99; //100% - it's impossible
	}*/
	if(accmax>0 && branchlength>0 && hypvalue>0)
	{
		if(accmax==(int)hypvalue) weight=0.5;
		else weight=0.5*(hypvalue-branchlength)/(accmax-branchlength);
		weight*=fabs(side);
		weight+=0.5;
		if(weight>1.)
			weight=1.;
		weight*=0.99; //100% - it's impossible
	}
}
//---------------------------------------------------------------------------

void __fastcall TFoundObject::FinalizeIt()
{
    bool au;

    if(!hyperbola) hyperbola=new THyperbola(e, DoublePoint(x, y), branchlength);
    else
    {
        au=hyperbola->AutomaticUpdate;
        hyperbola->AutomaticUpdate=false;
        hyperbola->Center=DoublePoint(x, y);
        hyperbola->Epsilon=E;
        hyperbola->BranchLength=branchlength;
        hyperbola->AutomaticUpdate=au;
		hyperbola->Update();
    }
}

//---------------------------------------------------------------------------
// TFoundObjectList
//---------------------------------------------------------------------------
int __fastcall TFoundObjectList::Add(TFoundObject *Item)
{
    int res;

    if(Item)
    {
        x+=Item->X;
        y+=Item->Y;
		e+=Item->E;
		acc+=Item->Acc;
        if(mine>Item->E || mine==-1) mine=Item->E;
        if(maxe<Item->E || maxe==-1) maxe=Item->E;
        res=List->Add((void*)Item);
    }
    else res=-1;

    return res;
}
//---------------------------------------------------------------------------

void __fastcall TFoundObjectList::Delete(int Index, bool recalcul)
{
    TFoundObject *fo, *fo2;
    try
    {
        if(Index>=0 && Index<List->Count)
        {
            fo=(TFoundObject*)List->Items[Index];
            if(fo)
            {
                if(recalcul)
                {
                    x-=fo->X;
                    y-=fo->Y;
					e-=fo->E;
                    acc-=fo->Acc;
					if(fo->E==maxe || fo->E==mine)
                    {
                        mine=maxe;
                        maxe=0;
                        for(int i=0; i<List->Count; i++)
                        {
                            fo2=(TFoundObject*)List->Items[i];
                            if(i!=Index && fo2)
                            {
                                if(maxe<fo2->E) maxe=fo2->E;
                                if(mine>fo2->E) mine=fo2->E;
                            }
                        }
                    }
                }
                delete fo;
			}
            List->Delete(Index);
        }
    }
    catch(...) {}
}

//---------------------------------------------------------------------------
// THoughHyp
//---------------------------------------------------------------------------
__fastcall THoughHyp::THoughHyp(TProfilePeakList *aPeak_List, int aWidth,
	int aHeight, float aPermFrom, float aPermTo, float aPermStep,
	TFoundObjectList *aObjectsList, TProfile *aProfile)
	: TCustomizeAutomaticDetection(ptHyp)
{
	inputdata=NULL;
	width=aWidth;
	height=aHeight;
	peak_List=aPeak_List;
	objectslist=aObjectsList;
	perm_conv_coef=EpsilonToPixels();
	permfrom=aPermFrom;
	permto=aPermTo;
	permstep=aPermStep;
	profile=aProfile;
	maxmax=0;
	positive=true;
	branchlength=period=0;
	//Gauss Distribution for Accum bluring
	GR_sigma=0.67;//0.5;//0.34;//1.;//0.;//
	GN2=(int)(3.*GR_sigma);
	GN=GN2 << 1;
	if(GN>=10)
	{
		GN=10;
		GN2=GN >> 1;
		GR_sigma=(double)GN2/3.;
	}
	GaussDistrib[GN2][GN2]=1.;
	for(int i=1; i<=GN2; i++)
	{
		GaussDistrib[GN2+i][GN2]=exp(((double)-i*i)/(2.*GR_sigma*GR_sigma));
		GaussDistrib[GN2-i][GN2]=GaussDistrib[GN2+i][GN2];
	}
	for(int i=0; i<=GN2; i++)
		for(int j=0; j<=GN2; j++)
		{
			GaussDistrib[i][j]=GaussDistrib[i][GN2]*GaussDistrib[j][GN2];
			GaussDistrib[GN-i][j]=GaussDistrib[i][j];
			GaussDistrib[i][GN-j]=GaussDistrib[i][j];
			GaussDistrib[GN-i][GN-j]=GaussDistrib[i][j];
		}
	GD_NormFact=0;
	for(int i=0; i<=GN; i++)
		for(int j=0; j<=GN; j++)
			GD_NormFact+=GaussDistrib[i][j];
	/*GD_NormFact=sqrt(GD_NormFact);
	for(int i=0; i<=GN; i++)
		for(int j=0; j<=GN; j++)
			GaussDistrib[i][j]/=GD_NormFact;
	//GD_NormFact=1;*/

	processBuf=new char*[Width];
	for(int w=0; w<Width; w++)
		processBuf[w]=new char[Height];

}
//---------------------------------------------------------------------------

__fastcall THoughHyp::~THoughHyp()
{
	if(inputdata) delete[] inputdata;

	for(int w=0; w<Width; w++)
		delete[] processBuf[w];
	delete[] processBuf;
}
//---------------------------------------------------------------------------

void __fastcall THoughHyp::GetInitialData(HANDLE aStopEvent)//TBitmap32 *ABm)
{
	int win, i, k, z, w, h, avr, spy, y, y_old, y_new, count, aver_y, aver_y_count;
	int points_cnt, source_data, maxp;
	TTrace *temptrace;
	char **buf;
	AnsiString S;
	float freq, T;

	try
	{
		buf=new char*[Width];
		//char **processBuf=new char*[Width];
		for(w=0; w<Width; w++)
		{
			buf[w]=new char[Height];
			//processBuf[w]=new char[Height];
			for(h=0; h<Height; h++)
			{
				processBuf[w][h]=0;
				buf[w][h]=0;
			}
		}
		if(peak_List && profile && FirstTrace)
		{
			//clear the processbuff
			maxp=10000000;
			for(i=0; i<peak_List->Count; i++)
				if(abs(peak_List->Items[i]->peakValue)<maxp)
					maxp=abs(peak_List->Items[i]->autoGainValue);
			for(i=0; i<peak_List->Count; i++)
			{
				w=peak_List->Items[i]->traceIndex;
				h=peak_List->Items[i]->sampleIndex;
				if(peak_List->Items[i]->isPositive==Positive)
					//if(abs(peak_List->Items[i]->autoGainValue)>(float)max*0.125)//0.0625)
						buf[w-FirstTrace->Index][h]=1;
				if(aStopEvent && WaitForSingleObject(aStopEvent, 0)==WAIT_OBJECT_0) break;
			}
			//SaveBitmap("input0.bmp", Width, Height, buf);
			/*
			k=profile->TextInfo.AnsiPos("Antenna: ");
			if(k>0)
			{
				k+=9;
				while(profile->TextInfo[k]!=' ' && k<profile->TextInfo.Length()-1)
				{
					S+=profile->TextInfo[k];
					k++;
				}
				try
				{
					freq=StrToFloat(S)*1000000;
				}
				catch(...)
				{
					freq=500000000;
				}
			}
			else freq = 500*1000000;

			//calculation of brach length
			T = (1./freq)*2.5;
			branchlength = 2*T*(float)FirstTrace->Samples/(profile->TimeRange*1e-9);*/

			count=average_thickness_half=aver_y_count=aver_y=y=y_old=y_new=0;
			for(w=0; w<Width; w++)
			{
				y_old=-1;
				h=0;
				while(h<Height)
				{
					if(buf[w][h]==1)
					{
						spy=h;
						while(h<Height-1 && buf[w][h]==1) h++;
						y_new=(h+1-spy)>>1;
						average_thickness_half+=y_new;
						y_new+=spy;
						if(y_old>=0)
						{
							y=y_new-y_old;
							if(y<Height/4)
							{
								aver_y+=y;
								aver_y_count++;
							}
						}
						y_old=y_new;
						count++;
					}
					h++;
				}
				if(aStopEvent && WaitForSingleObject(aStopEvent, 0)==WAIT_OBJECT_0) break;
			}
			if(aver_y_count>0)
			{
				aver_y=(THoughValue)aver_y/(THoughValue)aver_y_count+0.5;
			}
			else aver_y=100;
			period=aver_y;
			branchlength=period*2;//3;//
			average_thickness_half=average_thickness_half/count;
		}
		else    //if there is no peaklist do from profile
		{
			//finding average_thickness_half
			count=average_thickness_half=aver_y_count=aver_y=y=y_old=y_new=0;
			maxp=0;
			temptrace=FirstTrace;
			for(w=0; w<Width; w++)
			{
				for(h=0; h<Height; h++)
					if(abs(temptrace->SourceData[h])>maxp)
						maxp=abs(temptrace->SourceData[h]);
				temptrace=temptrace->PtrUp;
			}
			temptrace=FirstTrace;
			for(w=0; w<Width; w++)
			{
				y_old=-1;
				for(h=0; h<Height; h++)
				{
					if(!Positive) source_data=-temptrace->SourceData[h];
					else source_data=temptrace->SourceData[h];
					if(source_data>maxp*0.0625)
					{
						spy = h;
						while(h<Height && abs(temptrace->SourceData[h])>max*0.0625)
							h++;
						y_new=(h+1-spy)>>1;
						average_thickness_half+=y_new;
						y_new+=spy;
						if(y_old>=0)
						{
							y=y_new - y_old;
							if(y<Height/4)
							{
								aver_y+=y;
								aver_y_count++;
							}
						}
						y_old=y_new;
						count++;
					}
					buf[w][h]=0;
					if(aStopEvent && WaitForSingleObject(aStopEvent, 0)==WAIT_OBJECT_0) break;
				}
				temptrace=temptrace->PtrUp;
			}
			if(aver_y_count>0) aver_y=(THoughValue)aver_y/(THoughValue)aver_y_count+0.5;
			else aver_y=100;
			period=aver_y;
			branchlength=period*2;//3;//
			//average_thickness_half=average_thickness_half / count;
			average_thickness_half=1;
	   ///////////////////////////
			temptrace=FirstTrace;
			for(w=0; w<Width; w++)
			{
				if(temptrace)
				{
					for(h=0; h<Height; h++)
					{
						if(!Positive) source_data=-temptrace->SourceData[h];
						else source_data=temptrace->SourceData[h];
						if(source_data>max*0.0625)
						{
							spy=h;
							while(h<Height && abs(temptrace->SourceData[h])>max*0.0625)
								h++;
							if(temptrace && spy<Height) buf[w][spy]=1;
						}
					}
					temptrace=temptrace->PtrUp;
				}
				else break;
				if(aStopEvent && WaitForSingleObject(aStopEvent, 0)==WAIT_OBJECT_0) break;
			}
		}
		int x, xi, yi, wx, wy, ww;
		float c, t, a, b, dx, dy, yy, xx;

		/*int interp_l=period*0.25;

		count = average_thickness_half = aver_y_count = aver_y = y = y_old = y_new = 0;
		for(w = 0; w < Width; w++)
		{
			y_old = -1;
			for(h = 0; h < Height; h++)
			{
				if(ABm->Pixel[w][h] == clBlack32)
				{
					spy = h;
					while(h < Height - 1 && ABm->Pixel[w][h] == clBlack32)
					{
						h++;
					}
					y_new = (h+1-spy)>>1;
					average_thickness_half += y_new;
					y_new += spy;
					if(y_old >=0)
					{
						y = y_new - y_old;
						if(y < Height/4)
						{
							aver_y += y;
							aver_y_count ++;
						}
					}
					y_old = y_new;
					count++;
				}
			}
		}
		if(aver_y_count > 0) aver_y = (double)aver_y/(double)aver_y_count + 0.5;
		else aver_y = 100;
		branchlength = aver_y * 2;
		average_thickness_half = average_thickness_half / count;
		for(w=0; w<Width; w++)
		{
			for(h=0; h<Height; h++)
			{
				try
				{
					if(ABm->Pixel[w][h]==clBlack32)
					{
						spy=h;
						while(h<Height-1 && ABm->Pixel[w][h]==clBlack32)
						{
							buf[w][h]=0;
							h++;
						}
						if(spy+average_thickness_half<Height) buf[w][spy+average_thickness_half]=1;
					}
				}
				catch(...) {}
			}
		}*/
		/*int scale_factor = 1, scale_w=(scale_factor<<1), i, j, s;
		//branchlength /= scale_w;
		//average_thickness_half /= scale_w;
		//interp_l /= scale_w;

		points_cnt=0;
		for(w=scale_factor, x=0; w<Width-scale_factor; w+=scale_w, x++)
		{
			for(h=scale_factor, y=0; h<Height-scale_factor; h+=scale_w, y++)
			{
				s=0;
				for(i=-scale_factor; i<scale_factor; i++)
					for(j=-scale_factor; j<scale_factor; j++)
                        s+=buf[i+w][j+h];
                //processBuf[x][y]=(int)(s>0);
                //points_cnt+=processBuf[x][y];
                processBuf[w][h]=(int)(s>0);
                points_cnt+=processBuf[w][h];
            }
        }
        for(w=0; w<Width; w++)
        {
            for(h=0; h<Height; h++)
			{
				buf[w][h]=processBuf[w][h];
				processBuf[w][h]=0;
			}
		}*/
		/*points_cnt=0;
		for(w=0; w<Width; w++)
		{
			for(h=0; h<Height; h++)
			{
				if(buf[w][h]>0)
				{
					xi=yi=-1;
					c=interp_l;
					x=w+1;//w;//
					y=h-interp_l;
					if(y<0) y=0;
					while(x<w+interp_l && x<Width)
					{
						if(buf[x][y]>0)// && !(x==w && y==h))
						{
							a=x-w;
							b=y-h;
							t=sqrt(a*a+b*b);
							if(c>t)
							{
								xi=x;
								yi=y;
								c=t;
							}
						}
						y++;
						if(y>=Height || y>=h+interp_l)// || (x==w && y==h))
						{
							x++;
							y=h-interp_l;
							if(y<0) y=0;
						}
					}
					wx=xi-w;
					wy=yi-h;
					if(xi>=0 && yi>=0)// && wy!=0)// && c>2.)//
					{
						//ABm->Line(w, h, xi, yi, clBlack32, true);
						if(wx>abs(wy)) ww=wx+1;
						else ww=abs(wy)+1;
						dx=(float)wx/(float)ww;
                        dy=(float)wy/(float)ww;
						xx=0;//dx;
						yy=0;//dy;
						for(x=0; x<ww; x++)
						{
							processBuf[w+(int)xx][h+(int)yy]=1;
							points_cnt++;
							xx+=dx;
							yy+=dy;
						}
					}
				}
			}
			if(aStopEvent && WaitForSingleObject(aStopEvent, 0)==WAIT_OBJECT_0) break;
		}*/
		points_cnt=0;
		for(w=0; w<Width; w++)
		{
			for(h=0; h<Height; h++)
			{
				if(buf[w][h]>0)
				{
					processBuf[w][h]=1;
					points_cnt++;
				}
			}
		}
		// !!!!!!!!!!!!!!!!! HPFpixels !!!!!!!!!!!!!!!!!!!!!!
//		double max, *temp=new double[Width+Height], sum;
//		int hpf_w=1;

		/*points_cnt=0;
		for(h=0; h<Height; h++)
		{
			for(w=hpf_w; w<Width-hpf_w; w++)
			{
				temp[w]=0;
				for(int i=w-hpf_w; i<=w+hpf_w; i++)
					temp[w]+=processBuf[i][h];
				temp[w]/=((double)hpf_w*2.+1);
				temp[w]=processBuf[w][h]-temp[w];
			}
			for(w=0; w<Width; w++)
				if(w>=hpf_w && w<Width-hpf_w && temp[w]>0)
				{
					processBuf[w][h]=1;
					points_cnt++;
				}
				else processBuf[w][h]=0;
		}*/

		//Vertical blur
		/*max=0;
		points_cnt=0;
		for(w=GN2; w<Width-GN2; w++)
		{
			for(h=GN2; h<Height-GN2; h++)
			{
				sum=0;
				for(int i=0; i<=GN; i++)
					sum+=GaussDistrib[i][GN2]*processBuf[w][h+(i-GN2)];
				temp[h]=sum;
				if(max<sum)
				{
					max=sum;
				}
			}
			for(h=GN2; h<Height-GN2; h++) processBuf[w][h]=temp[h];
		}
		//Horizontal blur
		max=0;
		for(h=GN2; h<Height-GN2; h++)
		{
			for(w=GN2; w<Width-GN2; w++)
			{
				sum=0;
				for(int i=0; i<=GN; i++)
					sum+=GaussDistrib[i][GN2]*processBuf[w+(i-GN2)][h];
				temp[w]=sum;
				if(max<sum)
				{
					max=sum;
				}
			}
			for(w=GN2; w<Width-GN2; w++)
			{
				processBuf[w][h]=temp[w];
				points_cnt+=(int)(processBuf[w][h]>0);
			}
		}

		for(h=GN2; h<Height-GN2; h++)
			for(w=GN2; w<Width-GN2; w++)
				processBuf[w][h]/=max;//*max;
		*/
//		delete[] temp;

//------------------------------------------
		inputdatacnt=points_cnt;
		if(inputdata) delete[] inputdata;
		if(inputdatacnt>0)
		{
			inputdata=new TDouble3DPoint[inputdatacnt];
			i=0;
			for(h=0; h<Height; h++)
				for(w=0; w<Width; w++)
					if(processBuf[w][h]>0) inputdata[i++]=TDouble3DPoint(w, h, processBuf[w][h]);
			inputdatacnt=i;
			//SaveBitmap("input5.bmp", Width, Height, processBuf);
		}
		else inputdata=NULL;
		for(w=0; w<Width; w++)
		{
			delete[] buf[w];
			//delete[] processBuf[w];
		}
		delete[] buf;
		//delete[] processBuf;
	}
	catch(...){}
}
//---------------------------------------------------------------------------

void __fastcall THoughHyp::SaveBitmap(AnsiString AFileName, int AWidth, int AHeight, char **AInBuf)
{
	TBitmap32 *Bmp;
	int w, h;

	if(AInBuf)
	{
		Bmp = new TBitmap32();
		Bmp->SetSize(AWidth, AHeight);
		Bmp->Clear(clWhite32);
		try
		{
			for(w = 0; w < AWidth; w++)
				for(h = 0; h < AHeight; h++)
				{
					try
					{
						if(AInBuf[w][h]>0)
							Bmp->Pixel[w][h] = clBlack32;
					}
					catch(...){}
				}
			Bmp->SaveToFile(AFileName);
		}
		catch(...){}
		delete Bmp;
		Bmp = NULL;
	}
}

bool __fastcall THoughHyp::Process(HANDLE aStopEvent)
{
	bool res=false;
	int i, j, ai, x, X, y, h, max, hold, dh, z, k, yold, thd, xa, ya, *distrib; //, average_cnt;
	float p, var, rms, eFrom, eTo, eStep, dy;
	TAccumValue **accum=NULL;
	TDoublePoint *points;
	TFoundObject *fo;
	int size;
	unsigned long ul=::GetTickCount();

	//Form1->Memo1->Lines->Clear();
	if(InputData && InputDataCount>0 && branchlength>0)
	{
		try
		{
			try
			{
				res=true;
				accum=new TAccumValue*[Width+1];
				for(ai=0; ai<Width; ai++)
					accum[ai]=new TAccumValue[Height+1];
				points=new TDoublePoint[branchlength*4+1];
				perm_conv_coef=EpsilonToPixels();
				eFrom=PermFrom*perm_conv_coef;
				eTo=PermTo*perm_conv_coef;
				eStep=PermStep*perm_conv_coef;
				for(p=eFrom; p<eTo; p+=eStep)
				{
					yold=-1;
//					average_cnt=0;
					max=0;
					distrib=NULL;
					for(i=0; i<Width; i++) //memset(accum[i], sizeof(TAccumValue)*Height, 0);
						for(j=0; j<Height; j++) accum[i][j]=AccumValue(0,0,0,0);
					for(i=0; i<inputdatacnt; i++)
					{
						//if(InputData[i].X>=0 && InputData[i].X<Width && InputData[i].Y>=0 && InputData[i].Y<Height && InputData[i].Z>0)
						if(InputData[i].Z>0)
						{
							if(InputData[i].Y!=yold)
							{
								yold=InputData[i].Y;
								hold=-1;
								size=0;
								/*for(j=branchlength; j>=-branchlength; j--)
								{
									if(j==0) points[size++]=DoublePoint(0, yold);
									else
									{
										x=-j;
										y=-p*j*j+yold*yold;
										if(y>0)
										{
											y=sqrt(y);
											if(sqrt(x*x+(yold-y)*(yold-y))<branchlength)
											{
												if(hold>=0)
												{
													dh=y-hold;
													for(z=1; z<abs(dh); z++)
														points[size++]=DoublePoint(x, hold+z*dh/abs(dh));
												}
												hold=y;
												points[size++]=DoublePoint(x, y);
											}
										}
									}
								}*/
								for(j=branchlength; j>=-branchlength; j--)
								{
									if(j==0) points[size++]=DoublePoint(0, yold);
									else
									{
										x=-j;
										dy=-p*j*j+yold*yold;
										if(dy>0)
										{
											y=sqrt(dy);//(int)(sqrt(dy)+0.5);
											if(sqrt((float)(x*x+(yold-y)*(yold-y)))<branchlength)
											{
												if(hold>=0)
												{
													dh=y-hold;
													for(z=1; z<abs(dh); z++)
														points[size++]=DoublePoint(x, hold+z*dh/abs(dh));
												}
												hold=y;
												points[size++]=DoublePoint(x, y);
											}
										}
									}
								}
								if(aStopEvent && WaitForSingleObject(aStopEvent, 0)==WAIT_OBJECT_0) break;
                            }
							hold = -1;
							for(j=0; j<size; j++)
                            {
                                X=points[j].X;
                                x=InputData[i].X+X;
                                y=points[j].Y;
                                /*if(x>=0 && x<Width && y>=0 && y<Height)
                                {
                                    var=InputData[i].Z;
                                    accum[x][y].Total+=var;
                                    if(max<accum[x][y].Total) max=accum[x][y].Total;
                                    if(X<0) accum[x][y].LeftBranch+=var;
                                    else if(X>0) accum[x][y].RightBranch+=var;
                                    else
                                    {
										var/=2.;
                                        accum[x][y].LeftBranch+=var;
                                        accum[x][y].RightBranch+=var;
                                    }
                                }*/
                                if(x>GN2 && x+GN2<Width && y>GN2 && y+GN2<Height)
                                {
									for(int xx=0; xx<=GN; xx++)
										for(int yy=0; yy<=GN; yy++)
										{
											var=GaussDistrib[xx][yy];//*InputData[i].Z;
											xa=x-(GN2-xx);
											ya=y-(GN2-yy);
											accum[xa][ya].Total+=var;
											if(max<accum[xa][ya].Total) max=accum[xa][ya].Total;
                                            if(X<0) accum[xa][ya].LeftBranch+=var;
                                            else if(X>0) accum[xa][ya].RightBranch+=var;
                                            else
											{
                                                var/=2.;
                                                accum[xa][ya].LeftBranch+=var;
												accum[xa][ya].RightBranch+=var;
                                            }
                                        }
                                }
							}
						}
					}
					if(max>0)
                    {
						//if(max>maxmax) maxmax=max;
						distrib=new int[max+2];
						for(i=0; i<=max; i++) distrib[i]=0;
						//rms=0;
                        try
                        {
                            for(i=0; i<Width; i++)
                            {
                                for(j=0; j<Height; j++)
                                {
                                    if(accum[i][j].Total>0)
                                    {
                                        accum[i][j].Side=(accum[i][j].RightBranch-accum[i][j].LeftBranch)/accum[i][j].Total;
                                        if(accum[i][j].Side<0) accum[i][j].Side=-1-accum[i][j].Side;
                                        else accum[i][j].Side=1-accum[i][j].Side;
										//rms+=(accum[i][j].Total-1.)*(accum[i][j].Total-1.); //without average due to max of distribution is on 1.
										//average_cnt++;

										distrib[(int)(accum[i][j].Total+0.5)]++; //filling the distribution bufer
										accum[i][j].Total*=fabs(accum[i][j].Side);
										if(accum[i][j].Total>maxmax) maxmax=accum[i][j].Total;
									}
								}
								if(aStopEvent && WaitForSingleObject(aStopEvent, 0)==WAIT_OBJECT_0) break;
							}
							//rms=sqrt(rms/(float)average_cnt);
							//z=(int)(Average+3.*rms); // the rule of 3 sigmas
							//z=(int)(1.+3.*rms); //1. instead of average due to max of distribution is on 1.
							z=(int)(max*0.3);
                            thd=0;
                            if(z>0)
                            {
                                for(i=z+1, k=0; i<max; i++)
                                {
                                    if(distrib[i]==0)
									{
										k++;
										if(k>0)//1)//
										{
											thd=i+1;
											break;
										}
									}
								}
							}
							if(thd==0) thd=max+1;
							else if(thd<(branchlength/8.)*GD_NormFact) thd=(branchlength/8.)*GD_NormFact;

							// Draw Accumulator
							/*
							float acc;
							Form1->HoughImage->Bitmap->BeginUpdate();
                            try
                            {
                                Form1->HoughImage->Bitmap->Clear(clNavy32);
                                for(x = 0; x < Width; x++)
                                    for(y = 0; y < Height; y++)
                                        if(accum[x][y].Total>0)// Max*Threshold)
                                        {
                                            if(accum[x][y].Total>=thd)
                                                Form1->HoughImage->Bitmap->Pixel[x][y] = clRed32;
                                            else
                                            {
                                                acc=255.*(float)accum[x][y].Total/(float)Max;
                                                Form1->HoughImage->Bitmap->Pixel[x][y]=Color32((unsigned char)acc,
                                                    (unsigned char)acc, (unsigned char)acc, 255);
                                            }
                                        }
                            }
                            __finally
                            {
                                Form1->HoughImage->Bitmap->EndUpdate();
                                Form1->HoughImage->Refresh();
                            }*/

							//------- Level to Objects -------
							if(ObjectsList)
							{
								for(i=0; i<Width; i++)
								{
									for(j=0; j<Height-Period*2; j++)
									{
										if(accum[i][j].Total>=thd)// && fabs(accum[i][j].Side)>=0.707)//0.5)
										{
											fo=new TFoundObject(i, j, p, 1./perm_conv_coef, accum[i][j].Total);
											fo->BranchLength=BranchLength;
											fo->AccThreshold=thd;
											//fo->AccMax=max;
											//fo->AccMax=maxmax;
											fo->Side=accum[i][j].Side;
											fo->Positive=Positive;
											fo->FinalizeIt();
											fo->HypValue=var=rms=0;

											if(fo->Hyperbola)
											{
												for(z=0; z<fo->Hyperbola->Size; z++)
												{
													x=fo->Hyperbola->Points[z].X;
													y=fo->Hyperbola->Points[z].Y;
													int yy=y;
													//for(int yy=y-GN2; yy<=y+GN2; yy++)
														for(int xx=x-GN2; xx<=x+GN2; xx++)
														{
															if(xx>=0 && xx<Width && yy>=0 && yy<Height)// && processBuf[xx][yy]>0)//
															{
																THyperbola *hyp;
																hyp=fo->Hyperbola;//new THyperbola(-p, DoublePoint(xx, yy), branchlength);
																for(int ii=0; ii<hyp->Size; ii++)
																{
																	//xa=hyp->Points[ii].X-i+GN2;
																	//ya=hyp->Points[ii].Y-j+GN2;
																	xa=hyp->Points[ii].X-i;//+GN2;
																	ya=(-1)*(hyp->Points[ii].Y-j);
																	xa+=xx;
																	ya+=yy;
																	xa=xa-i+GN2;
																	ya=ya-j+GN2;
																	if(xa>=0 && xa<=GN && ya>=0 && ya<=GN)
																	{
																		fo->HypValue+=GaussDistrib[xa][ya]*processBuf[xx][yy];
																		if(ii>hyp->HeadIndex) rms+=GaussDistrib[xa][ya]*processBuf[xx][yy];
																		else if(ii<hyp->HeadIndex) rms-=GaussDistrib[xa][ya]*processBuf[xx][yy];
																		var+=GaussDistrib[xa][ya];
																	}
																}
																//delete hyp;
															}
														}
												}
											}
											if(fo->HypValue>0)
												fo->Side=1.-fabs(rms/fo->HypValue);
											fo->HypValue*=fo->Side;
											//fo->HypValue/=var;
											//fo->HypValue*=100.;
											//Form1->Memo1->Lines->Add("#"+IntToStr(ObjectsList->Count)+"\tE="+IntToStr((int)p)+
											//    "\t"+IntToStr((int)accum[i][j].Total)+"\t"+FloatToStrF(accum[i][j].Side, ffFixed, 4, 2));
											if(fo->HypValue>maxmax) maxmax=fo->HypValue;
											if(fo->HypValue>fo->BranchLength)//*GD_NormFact)
												ObjectsList->Add(fo);
											//else delete fo;
										}
									}
								}
							}
						}
						__finally
						{
							delete[] distrib;
						}
					}
					//Sleep(10ul);
					if(aStopEvent && WaitForSingleObject(aStopEvent, 0)==WAIT_OBJECT_0)
					{
						res=false;
						break;
					}
				}
			}
			catch(...)
			{
				res=false;
				yold=-1;
            }
        }
        __finally
        {
            try
            {
                if(accum)
                {
                    for(j=0; j<ai; j++)
                        delete[] (TAccumValue*)accum[j];
					delete[] (TAccumValue**)accum;
                    accum=NULL;
                }
                delete[] points;
            }
			catch(...) {}
		}
	}
	ul=::GetTickCount()-ul;
	return res;
}
//---------------------------------------------------------------------------

void __fastcall THoughHyp::PermitSigmaFilter()
{
	TFoundObject *fo;
	int c, j;
	float ae, rms;

	if(ObjectsList && ObjectsList->E>0 && ObjectsList->Acc>0 && ObjectsList->MaxE>0 && ObjectsList->MaxE>ObjectsList->MinE)
	{
		c=ObjectsList->Count;
		ae=ObjectsList->E;
		rms=(ObjectsList->MaxE-ObjectsList->MinE)/2./3.; //~~~~
		rms=0;
		for(int i=0; i<c; i++)
		{
			fo=ObjectsList->Items[i];
			if(fo)
			{
				rms+=(fo->E-ae)*(fo->E-ae);
			}
		}
		rms/=(float)c;
		rms=sqrt(rms);
		for(int i=0, j=0; i<c; i++)
		{
			fo=ObjectsList->Items[j];
			if(fo->E<ae-rms || fo->E>ae+rms) ObjectsList->Delete(j, false);
			else j++;
		}
	}
}

float __fastcall THoughHyp::EpsilonToPixels() //Calculation of Epselon_pixels !Temp without ZeroPoint
{
	float res, dx, dy, hor_range;

	if(profile && Width>0 && profile->Samples>0)
	{
		dx=GetHorRange(FirstTrace, LastTrace)/Width;
		dy=(profile->TimeRange*1e-9*3e8)/(2.*(float)profile->Samples);
		if(dy>0) res=(dx/dy)*(dx/dy);
        else res=1.;
	}
	else res=1.;

	return res;
}
//---------------------------------------------------------------------------

void __fastcall THoughHyp::Precise_the_points()
{
	int c, i, j, k, a;
	double radius;
	TFoundObject *newFObject, *tmpFObject;

	a=BranchLength/2;//4; //25;//radius of search
	try
	{
		for(j=0; j<ObjectsList->Count; j++)
		{
			tmpFObject=ObjectsList->Items[j];
			if(tmpFObject)
			{
				k=j;
				for(i=j+1; i<ObjectsList->Count; i++)
				{
					newFObject=ObjectsList->Items[i];
					if(newFObject && newFObject!=tmpFObject)
					{
						radius=(newFObject->Y-tmpFObject->Y)*(newFObject->Y-tmpFObject->Y)+
							(newFObject->X-tmpFObject->X)*(newFObject->X-tmpFObject->X);
						if(radius>=0 && (int)(sqrt(radius)+0.5)<=a)
						{
							//if(newFObject->Acc>tmpFObject->Acc)
							if(newFObject->HypValue>tmpFObject->HypValue)
							{
								delete tmpFObject;
								ObjectsList->Items[k]=NULL;
								tmpFObject=newFObject;
								k=i;
							}
							else
							{
								delete newFObject;
								ObjectsList->Items[i]=NULL;
								newFObject=NULL;
							}
						}
					}
				}
			}
		}
		c=ObjectsList->Count;
		i=0;
		for(j=0; j<c; j++)
		{
			if(!ObjectsList->Items[i]) ObjectsList->Delete(i);
			else
			{
				ObjectsList->Items[i]->AccMax=maxmax;
				i++;
			}
		}
	}
	catch(...)
	{
		i=0;
	}
}
//---------------------------------------------------------------------------

void __fastcall THoughHyp::Make(HANDLE aStopEvent)
{
	int p, permit, x, w, h;
	THoughValue var;

	try
	{
		GetInitialData(aStopEvent);
		if(Process(aStopEvent))
		{
			//Precise_the_points();
		}
	}
	catch(...) {}
	ResultFirstTrace=FirstTrace; //creating result traces
	ResultLastTrace=LastTrace;   //creating result traces
}

#pragma package(smart_init)

