// -----------------------------------------------------------------------------
// AutoGain.cpp
// -----------------------------------------------------------------------------

#include <vcl.h>
#include <math.h>
#include <stdlib.h>
#pragma hdrstop

#include "AutoGain.h"
#include "AutoGainCamelMethod.h"
#include "AutoGainLinearMethod.h"
#include "AutoGainToMaxMethod.h"

// ---------- GetNoiseValue ----------------------------------------------------
int __fastcall TadAutoGain::GetNoiseValue() {
    TTrace *trace;
    TTraceIterator *trace_itr;
    int result = 0;
    int trace_max_value = 0;
    int max_value = 0;

    trace_itr = new TTraceIterator();
    trace_itr->SetFirstTracePtr(FirstTrace);

    for (trace = trace_itr->GetCurrentPtr(); ;  trace = trace_itr->GetNext()) {
        trace_max_value = trace->GetMaxInSourceTrace();

        if (max_value < trace_max_value) { max_value = trace_max_value; } // min() - Ok  max() - error ???

        if (trace == LastTrace) { break; }

    }

    result = (int) ((float)max_value * mNoiseProc / 100.0 + 0.5);

    delete trace_itr;
    return result;
}
// -----------------------------------------------------------------------------

// ---------- GetFrameMaxValue -------------------------------------------------
int __fastcall TadAutoGain::GetFrameMaxValue(TSample *pTraceData, int pSamplesCnt, int pFrameSize, int pSampleIdx) {
	TSample *ptr_sample = pTraceData + pSampleIdx;
    int result = abs(*ptr_sample);
	int end_idx = min(pSampleIdx + pFrameSize, pSamplesCnt);
    int value = 0;

    for (int i = pSampleIdx;  i < end_idx; i++) {
        value = abs(*(ptr_sample++));
        if (result < value) { result = value; } // result = max( result, value );  // min() - Ok  max() - error ???
    }

    return result;
}
// -----------------------------------------------------------------------------

// ---------- GetFrameMaxValue -------------------------------------------------
int __fastcall TadAutoGain::GetFrameMaxValue(TSample *pTraceData, int pSamplesCnt, int pFrameSize, int pSampleIdx, TProfilePeakList *pList) {
	TSample *ptr_sample = pTraceData + pSampleIdx;
    int result = abs(*ptr_sample);
    int end_idx = min(pSampleIdx + pFrameSize, pSamplesCnt);
    int value = -1;
    int max_sample_idx_start = -1;
    int max_sample_idx_end = -1;
    bool find_max = FALSE;

    for (int i = pSampleIdx;  i < end_idx; i++) {
        value = abs(*(ptr_sample++));

	if (result < value) {
	    result = value;
	    max_sample_idx_start = max_sample_idx_end = i;
            find_max = TRUE;
	}
	else if (pList) {
	    if (result == value) {
	        max_sample_idx_end = i;
	    }
	    else if (find_max) {
	        int peak_idx = (max_sample_idx_start + max_sample_idx_end) >> 1;
		int is_positive = (*(pTraceData + peak_idx) >= 0) ? 1 : 0;
		TProfilePeak *peak = new TProfilePeak(-1, -1, peak_idx, value, -1, is_positive);

                pList->Add(peak);
                find_max = FALSE;
	    }
	}
    }

    return result;
}
// -----------------------------------------------------------------------------

// ---------- SearchHill -------------------------------------------------------
int __fastcall TadAutoGain::SearchHill(TSample *pTraceData, int pSamplesCnt, int pStartSampleIdx) {
	int prev_value, sign_prev_value, value, sign_value = 1, end_sample;

	try
	{
		prev_value = pTraceData[pStartSampleIdx];
		sign_prev_value = (prev_value < 0) ? 0 : 1;
		value = 0;
		sign_value = 1;
		end_sample = pStartSampleIdx;

		for (int i = pStartSampleIdx + 1; i < pSamplesCnt; i++) {

			value = pTraceData[i];

			if (0 != value) {
				sign_value = (value < 0) ? 0 : 1;

				if (sign_value != sign_prev_value) {
					break;
				}
			}
			prev_value = value;
			sign_prev_value = sign_value;
			end_sample++;
		}
	}
	catch(...) {}

    return end_sample;
}
// -----------------------------------------------------------------------------

// ---------- GetInstance ------------------------------------------------------
TadAutoGain * __fastcall TadAutoGain::GetInstance(int AWindowWidth, float APermittivity, bool ANeedSaveTrace, float AHorRangeFull,
                         float ATimeRange, float AZeroPoint, int pPositiveMax, TAutoGainMethod pMethod) {
    TadAutoGain *instance = NULL;
    switch (pMethod) {
        case agmToMaxMethod:
            instance = new TadAutoGainToMaxMethod(AWindowWidth, APermittivity, ANeedSaveTrace, AHorRangeFull, ATimeRange,
                                                  AZeroPoint, pPositiveMax, pMethod);
            break;

        case agmCamelMethod:
            instance = new TadAutoGainCamelMethod(AWindowWidth, APermittivity, ANeedSaveTrace, AHorRangeFull, ATimeRange,
                                                  AZeroPoint, pPositiveMax, pMethod);
            break;

        case agmLinearMethod:
            instance = new TadAutoGainLinearMethod(AWindowWidth, APermittivity, ANeedSaveTrace, AHorRangeFull, ATimeRange,
                                                   AZeroPoint, pPositiveMax, pMethod);
            break;
    }
    return instance;
}

