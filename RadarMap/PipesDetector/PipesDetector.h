//---------------------------------------------------------------------------

#ifndef PipesDetectorH
#define PipesDetectorH
//---------------------------------------------------------------------------
#include "Defines.h"
#include "Profile.h"
#include "HoughHyp.h"

class TPipesDetectorThread;

class TPipesDetector: public TObject
{
private:
	TProfile *profile;
	TProfileWin *ProfileWin, *GlobalWin;
	TObject *Manager;
	TPipesDetectorThread *detector_thread;
	HANDLE StopEvent;
	int ChildCount;
public:
	__fastcall TPipesDetector(TProfile *AProfile, TObject* RadarMapManager);
	__fastcall ~TPipesDetector();

	__property TProfile* Profile = {read=profile};
};

class TPipesDetectorThread: public TMyThread
{
private:
	TObject *Manager;
	TProfileWin *ProfileWin, *GlobalWin;
	TPipesDetector *Owner;
	TList *WinsInProcess;
	HANDLE StopEvent;
	int* ChildCount;
protected:
	void __fastcall ExecuteLoopBody();
public:
	__fastcall TPipesDetectorThread(TPipesDetector* aOwner, TProfileWin *aProfileWin,
		TProfileWin *aGlobalWin, HANDLE aStopEvent, int *aChildCount, TObject* RadarMapManager);
	__fastcall ~TPipesDetectorThread();
};

class TProfWinProcess: public TMyThread
{
private:
	TTrace *SourceLeftTrace, *SourceRightTrace;
	TObject *Manager;
	TProfile *Profile;
	TProfileWin *PrivateWin, *GlobalWin;
	TPipesDetectorThread *Owner;
	HANDLE StopEvent;
	int* ChildCount;
protected:
	void __fastcall Execute();
	int __fastcall DetectObjects(TProfileWin *AProfileWin, TFoundObjectList *AFoundObjects);
	void __fastcall Objects2Labels (TProfileWin *AProfileWin, TFoundObjectList *AFoundObjects);
public:
	__fastcall TProfWinProcess(TProfileWin *AProfileWin, TProfileWin *AGlobalWin,
		TPipesDetectorThread *AOwner, HANDLE aStopEvent, int *aChildCount,
		TObject* RadarMapManager);
	__fastcall ~TProfWinProcess();
};

#endif
