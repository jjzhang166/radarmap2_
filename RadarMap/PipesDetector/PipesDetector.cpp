//---------------------------------------------------------------------------

#pragma hdrstop

#include "PipesDetector.h"
#include "Manager.h"

#include "AutomaticDetector.h"
#include "ProfilePeakList.h"
#include "AutoGain.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

//---------------------------------------------------------------------------
// TPipesDetector
//---------------------------------------------------------------------------
__fastcall TPipesDetector::TPipesDetector(TProfile *AProfile, TObject* RadarMapManager)
{
	profile=AProfile;
	Manager=RadarMapManager;
	ProfileWin=new TProfileWin(profile);
	GlobalWin=new TProfileWin(profile);
	StopEvent=CreateEvent(NULL, true, false, NULL);
	ChildCount=0;
	detector_thread=new TPipesDetectorThread(this, ProfileWin, GlobalWin, StopEvent, &ChildCount, Manager);
}
//---------------------------------------------------------------------------

__fastcall TPipesDetector::~TPipesDetector()
{
	int i=0;
	SetEvent(StopEvent);
	detector_thread->AskForTerminate();
	WaitOthers();
	if(detector_thread->ForceTerminate()) delete detector_thread;
	detector_thread=NULL;
	delete ProfileWin;
	ProfileWin=NULL;
	delete GlobalWin;
	GlobalWin=NULL;
	while(ChildCount>0 && i<20) {WaitOthers(); i++;}
	CloseHandle(StopEvent);
}

//---------------------------------------------------------------------------
// TPipesDetectorThread
//---------------------------------------------------------------------------
__fastcall TPipesDetectorThread::TPipesDetectorThread(TPipesDetector* AOwner,
	TProfileWin *AProfileWin, TProfileWin *AGlobalWin, HANDLE aStopEvent,
	int *aChildCount, TObject* RadarMapManager) : TMyThread(false)
{
	Owner=AOwner;
	ProfileWin=AProfileWin;
	GlobalWin=AGlobalWin;
	Manager=RadarMapManager;
	WinsInProcess=new TList();
	StopEvent=aStopEvent;
	ChildCount=aChildCount;
	(*ChildCount)++;
}
//---------------------------------------------------------------------------

__fastcall TPipesDetectorThread::~TPipesDetectorThread()
{
	TProfWinProcess *pwp;

	for(int i=0; i<WinsInProcess->Count; i++)
	{
		pwp=(TProfWinProcess*)WinsInProcess->Items[i];
		if(pwp)
		{
			if(pwp->SuccessfullyTerminated) delete pwp;
			else
			{
				pwp->AskForTerminate();
				WaitOthers();
				if(pwp->ForceTerminate()) delete pwp;
			}
			WinsInProcess->Items[i]=NULL;
		}
	}
	WinsInProcess->Clear();
	delete WinsInProcess;
	WinsInProcess=NULL;
	(*ChildCount)--;
}
//---------------------------------------------------------------------------

void __fastcall TPipesDetectorThread::ExecuteLoopBody()
{
	TRadarMapSettings *Settings;
	TProfWinProcess *pwp;
	float dist;
	int j, i, c;

	try
	{
		if(Manager) Settings=((TRadarMapManager*)Manager)->Settings;
		else Settings=NULL;
	}
	catch(...)
	{
		Settings=NULL;
	}
	try
	{
		if(Settings && ProfileWin && ProfileWin->Profile && ProfileWin->Profile->FirstTracePtr &&
			ProfileWin->Profile->LastTracePtr && GlobalWin)
		{
			dist=ProfileWin->Profile->HorRangeFull-ProfileWin->RightPos;
			if(ProfileWin->RightPos>Settings->PipesDetectionWinOverlap)
				dist+=Settings->PipesDetectionWinOverlap;
			if(dist>Settings->PipesDetectionWinWidth) //is there enough distance for detection
			{
				// Indicate the borders of next detection window
				ProfileWin->LeftPos=ProfileWin->RightPos-Settings->PipesDetectionWinOverlap;
				ProfileWin->RightPos=ProfileWin->LeftPos+Settings->PipesDetectionWinWidth;
				if(ProfileWin->LeftIndex>GlobalWin->LeftIndex)
					GlobalWin->LeftIndex=ProfileWin->LeftIndex;
				if(ProfileWin->RightIndex>GlobalWin->RightIndex)
					GlobalWin->RightIndex=ProfileWin->RightIndex;
				pwp=new TProfWinProcess(ProfileWin, GlobalWin, this, StopEvent, ChildCount, Manager);
				WinsInProcess->Add(pwp);
			}
			else
			{
				bool stop=true;

				for(i=0; ((TRadarMapManager*)Manager)->ReceivingProfilesQuantity; i++)
					if(ProfileWin->Profile==((TRadarMapManager*)Manager)->ReceivingProfiles[i])
					{
						stop=true;
						break;
					}
				if(stop)
				{
					Terminate();
					int si=((TRadarMapManager*)Manager)->Satellites->IndexOf(ProfileWin->Profile);

					if(si>=0 && ((TRadarMapManager*)Manager)->Satellites->Items[si])
					{
						((TRadarMapManager*)Manager)->Satellites->Items[si]->StopPipesDetector();
					}
				}
			}
			if(WinsInProcess->Count>0)
			{
				for(i=0; i<WinsInProcess->Count; i++)
				{
					pwp=(TProfWinProcess*)WinsInProcess->Items[i];
					if(pwp && pwp->SuccessfullyTerminated)
					{
						delete pwp;
						WinsInProcess->Items[i]=NULL;
					}
				}
				c=WinsInProcess->Count;
				for(j=0, i=0; j<c; j++)
				{
					if(WinsInProcess->Items[i]) i++;
					else WinsInProcess->Delete(i);
				}
			}
		}
	}
	catch(...) {}
	MySleep(100);
	if(StopEvent && WaitForSingleObject(StopEvent, 0)==WAIT_OBJECT_0)
		AskForTerminate();
}

//---------------------------------------------------------------------------
// TProfWinProcess
//---------------------------------------------------------------------------
__fastcall TProfWinProcess::TProfWinProcess(TProfileWin *AProfileWin,
	TProfileWin *AGlobalWin, TPipesDetectorThread *AOwner, HANDLE aStopEvent,
	int *aChildCount, TObject* RadarMapManager) : TMyThread(true)
{
	TTrace *ptr, *ptrNew;

	Owner=AOwner;
	if(AProfileWin && AGlobalWin && AProfileWin->Profile)
	{
		Profile=new TProfile();
		Profile->Samples=AProfileWin->Profile->Samples;
		Profile->TimeRange=AProfileWin->Profile->TimeRange;
		Profile->ZeroPoint=AProfileWin->Profile->ZeroPoint;
		Profile->SampleFormat=AProfileWin->Profile->SampleFormat;
		Profile->Permit=AProfileWin->Profile->Permit;
		Profile->PositiveMax=AProfileWin->Profile->PositiveMax;
		Profile->NegativeMax=AProfileWin->Profile->NegativeMax;
		ptr=AProfileWin->LeftTrace;
		for(int i=AProfileWin->LeftIndex; i<=AProfileWin->RightIndex; i++)
		{
			if(ptr)
			{
				ptrNew=new TTrace(ptr->Samples);
				ptrNew->CopyTrace(ptr);
				Profile->Add(ptrNew);
				ptr=ptr->PtrUp;
			}
			else break;
		}
		PrivateWin=new TProfileWin(Profile);
		PrivateWin->LeftIndex=0;
		PrivateWin->RightIndex=Profile->Traces;/**/
		SourceLeftTrace=AProfileWin->LeftTrace;
		SourceRightTrace=AProfileWin->RightTrace;
		GlobalWin=AGlobalWin;
	}
	else
	{
		Profile=NULL;
		PrivateWin=NULL;
	}
	Manager=RadarMapManager;
	StopEvent=aStopEvent;
	ChildCount=aChildCount;
	(*ChildCount)++;
	AskForResume();
}
//---------------------------------------------------------------------------

__fastcall TProfWinProcess::~TProfWinProcess()
{
	if(PrivateWin)
	{
		delete PrivateWin;
		PrivateWin=NULL;
	}
	if(Profile)
	{
		delete Profile;
		Profile=NULL;
	}
	(*ChildCount)--;
}
//---------------------------------------------------------------------------

void __fastcall TProfWinProcess::Execute()
{
	float dist;
	TFoundObjectList *FoundObjects;

	TMyThread::SetStarted();
	FoundObjects=new TFoundObjectList();
	try
	{
		try
		{
			if(PrivateWin && PrivateWin->Profile && PrivateWin->LeftTrace &&
				PrivateWin->RightTrace && PrivateWin->Width()>0)
			{
				try
				{
					if(DetectObjects(PrivateWin, FoundObjects)>0)
						Objects2Labels(PrivateWin, FoundObjects);
				}
				__finally
				{
					/*for(int i=0; i<FoundObjects->Count; i++)
					{
						if(FoundObjects->Items[i])
							delete (TFoundObject*)FoundObjects->Items[i];
					}*/
					FoundObjects->Clear();
				}
			}
		}
		catch(...) {}
		MySleep(100);
		if(StopEvent && WaitForSingleObject(StopEvent, 0)==WAIT_OBJECT_0)
			AskForTerminate();
	}
	__finally
	{
		delete FoundObjects;
		FoundObjects=NULL;
	}
	TMyThread::SetIsTerminated();
	ExitThread(99);
}
//---------------------------------------------------------------------------

int __fastcall TProfWinProcess::DetectObjects(TProfileWin *AProfileWin,
	TFoundObjectList *aFoundObjects)
{
	int res;
	TRadarMapSettings *Settings;
	TadAutoGain *gain_method;
	THoughHyp *hough_hyp, *hough_hyp_neg;
	TProfilePeakList *peakList;
	TList* list;
	TCustomizeAutomaticDetection* tmp;

	try
	{
		if(Manager) Settings=((TRadarMapManager*)Manager)->Settings;
		else Settings=NULL;
	}
	catch(...)
	{
		Settings=NULL;
	}
	if(Settings && AProfileWin && AProfileWin->Profile && AProfileWin->LeftTrace && aFoundObjects)
	{
		list=new TList();
		peakList=new TProfilePeakList();

		gain_method=TadAutoGain::GetInstance(AProfileWin->Width(), Settings->Permitivity, false, AProfileWin->WidthInMeters(),
			AProfileWin->Profile->TimeRange, AProfileWin->Profile->ZeroPoint, AProfileWin->Profile->PositiveMax, agmCamelMethod);
		gain_method->ProfilePeakList=peakList;
		hough_hyp=new THoughHyp(peakList, AProfileWin->Width()+1, AProfileWin->Profile->Samples+1,
			Settings->PipesDetectionPermitFrom, Settings->PipesDetectionPermitFrom+Settings->PipesDetectionPermitRange, 1.,
			aFoundObjects, AProfileWin->Profile);
		hough_hyp->MaxMax=Settings->PipesDetectionHoughAccumMax;
		hough_hyp_neg=new THoughHyp(peakList, AProfileWin->Width()+1, AProfileWin->Profile->Samples+1,
			Settings->PipesDetectionPermitFrom, Settings->PipesDetectionPermitFrom+Settings->PipesDetectionPermitRange, 1.,
			aFoundObjects, AProfileWin->Profile);
		hough_hyp_neg->MaxMax=Settings->PipesDetectionHoughAccumMax;
		hough_hyp_neg->Positive=false;/**/

		list->Add((void*)new TadHighPassFilter(AProfileWin->Profile, AProfileWin->Width(), 2./AProfileWin->LeftTrace->TraceDistance));//AProfileWin->Width()));//
		list->Add((void*)gain_method);  // agmCamelMethod | agmToMaxMethod | agmLinearMethod);
		list->Add((void*)hough_hyp);
		list->Add((void*)hough_hyp_neg);

		for(int i=0; i<list->Count; i++)
		{
			tmp=(TCustomizeAutomaticDetection*)list->Items[i];
			if(tmp)
			{
				tmp->FirstTrace=AProfileWin->LeftTrace;
				tmp->LastTrace=AProfileWin->RightTrace;
				tmp->Make(StopEvent);
			}
			if(StopEvent && WaitForSingleObject(StopEvent, 0)==WAIT_OBJECT_0) break;
		}

		if(hough_hyp->MaxMax>Settings->PipesDetectionHoughAccumMax) Settings->PipesDetectionHoughAccumMax=hough_hyp->MaxMax;
		if(hough_hyp_neg->MaxMax>Settings->PipesDetectionHoughAccumMax) Settings->PipesDetectionHoughAccumMax=hough_hyp_neg->MaxMax;
		else hough_hyp_neg->MaxMax=Settings->PipesDetectionHoughAccumMax;
		hough_hyp_neg->Precise_the_points();

		for(int i=0; i<list->Count; i++)
			if(list->Items[i]) delete (TCustomizeAutomaticDetection*)list->Items[i];
		delete list;
		delete peakList;
		res=aFoundObjects->Count;
	}
	return res;
}
//---------------------------------------------------------------------------

void __fastcall TProfWinProcess::Objects2Labels(TProfileWin *AProfileWin, TFoundObjectList *AFoundObjects)
{
	TFoundObject *fo;
	TTrace *ptr;
	TProfileLabel *pl;
	TMapLabel *ml;
	TRadarMapManager *RadarMapManager;
	TRadarMapSettings *Settings;
	float lb;
	int X;

	try
	{
		RadarMapManager=(TRadarMapManager*)Manager;
		if(Manager) Settings=RadarMapManager->Settings;
		else Settings=NULL;
	}
	catch(...)
	{
		RadarMapManager=NULL;
		Settings=NULL;
	}
	if(RadarMapManager && RadarMapManager->ObjectsContainer && Settings &&
		SourceLeftTrace && SourceRightTrace && GlobalWin && GlobalWin->Profile &&
			AFoundObjects && AFoundObjects->Count>0)
	{
		for(int i=0; i<AFoundObjects->Count; i++)
		{
			fo=AFoundObjects->Items[i];
			if(fo && AProfileWin->IsInWin(fo->X) &&
				fo->Y>GlobalWin->Profile->ZeroPoint &&
				fo->Y<AProfileWin->Profile->Samples)
			{
				X=fo->X+SourceLeftTrace->Index;
				ptr=GlobalWin->GetTraceByIndex(X, 0, GlobalWin->LeftTrace);
				if(SourceLeftTrace->HorPos==0) lb=0;
				else lb=SourceLeftTrace->HorPos+Settings->PipesDetectionWinResultsBorder;
				if(ptr && ptr->HorPos>=lb && ptr->HorPos<=SourceRightTrace->HorPos-Settings->PipesDetectionWinResultsBorder)
				{
					pl=new TProfileLabel(RadarMapManager->ObjectsContainer, RadarMapManager->ObjectsContainer->ViewPort,
						RadarMapManager->CurrentOutputView, (fo->Positive ? pltAutoDetectPos : pltAutoDetectNeg),
						X, fo->Y, clBlue, RadarMapManager);
					if(pl)
					{
						if(fo->ECoeff>0) lb=fo->ECoeff;
						else lb=1.;
						pl->Description="Permittivity = "+FloatToStrF(fo->E*lb, ffFixed, 10, 2)+"\n";
						pl->Description+="Accumulator = "+FloatToStrF(fo->Acc, ffFixed, 10, 2)+"\n";
						if(fo->Weight>0) pl->Description+="Probability = "+FloatToStrF(fo->Weight*100., ffFixed, 10, 2)+" %"+"\n";
						pl->Description+="HypValue = "+FloatToStrF(fo->HypValue, ffFixed, 10, 2)+"\n";

						pl->Data=fo;
						AFoundObjects->Items[i]=NULL;
						if(pl->Coordinate && pl->Coordinate->Valid &&
							RadarMapManager->MapObjects && RadarMapManager->MapObjects->Image && RadarMapManager->MapObjects->Container)
						{
							//pl->GPSConfidence
							ml=new TMapLabel(RadarMapManager->MapObjects, RadarMapManager->MapObjects->Image->GetViewportRect(),
								pl, RadarMapManager->MapObjects->Container->LatLonRect, (TObject*)RadarMapManager->CurrentPath, RadarMapManager);
							ml->Description=pl->Description;
						}
					}
				}
			}
			if(StopEvent && WaitForSingleObject(StopEvent, 0)==WAIT_OBJECT_0) break;
		}
	}
}

