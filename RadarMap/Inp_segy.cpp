//---------------------------------------------------------------------------
#include <vcl.h>
#include <math.h>
#include <float.h>
#pragma hdrstop

#include "Inp_segy.h"
#include "Control_unit.h"
#include "GPSCoordinate.h"

#define MaxFileSizeMB 100

bool Reverse2;

TTrace* AddOrReplace(TProfile* Prof, TTrace *NewPtr, TTrace* ExistingPtr, bool AddOrReplace);
void FilterOff(TProfile *Prof);
void IntReverse2(int &ival);
void FloatReverse2(float &fval);
void ShortIntReverse2(short int &sval);
void DoubleReverse2(double &dval);
void CalculGainFunction(TList *Vs, float *GF, int Smpls);

// DevidingType - 0, file opening without restrictions
// DevidingType - 1, open part of file from indicated StratTrace. The size of part (in MB) couldn't be more then Size
// DevidingType - 2, open part of file from indicated StratTrace. The size of part (in traces) couldn't be more then Size
// DevidingType - 3, open part of file from indicated StratTrace and up to the next Marker.
int Input_SEGY(char *Name, TProfile* Prof, TToolProgressBar *PB, bool SizeRestriction, bool BuiltInData, bool ApplyGain)
{
	return Input_SEGY(Name, Prof, PB, SizeRestriction, 0, 0, 0, BuiltInData, ApplyGain);
}

int Input_SEGY(char *Name, TProfile* Prof, TToolProgressBar *PB, bool SizeRestriction, int DevidingType,
	int StartTrace, int Size, bool BuiltInData, bool ApplyGain)
{
	TUnsharedFile *cFile;
	int res=1;

	if(FileExists(Name) && Prof)
	{
		cFile=new TUnsharedFile(NULL, Name, true);
		try
		{
			try
			{
				res=Input_SEGY_Unshared(cFile, Prof, PB, SizeRestriction, DevidingType, StartTrace, Size, BuiltInData, ApplyGain);
			}
			catch(...) {}
		}
		__finally
		{
			delete cFile;
			cFile=NULL;
		}
	}
	return res;
}
//---------------------------------------------------------------------------

int Input_SEGY_Unshared(TUnsharedFile *cFile, TProfile* Prof, TToolProgressBar *PB, bool SizeRestriction,
	int DevidingType, int StartTrace, int Size, bool BuiltInData, bool ApplyGain)
{
	char *header=NULL, *buf=NULL;
	short int sval;
	unsigned short tH, tM, tS, tMS, dY, dM, dD;
	TDateTime dt;
	unsigned long rd, bytes_to_read;
	int traces, samples, m, i, k, j, val, cc, sample_format;
	int PositiveMax, NegativeMax, k_start, k_end, m_c, tf, tt, sf, st, res=1;
	float fval, OldVal=0., asd, X0=0., X9=0., smpl, dist=0., DepthConstant, zp;
	TTrace *Trace, *BackTrace, *Ptr;
	bool DoubleStt, ProfContainsBackwardMovement=false;
	double dval;
	AnsiString TMP;
	TMyPoint *mp;
	TSampleFormat sformat;
	union CharPoint
	{
		TMyPointStruct mp;
		char c[sizeof(TMyPointStruct)];
	} ChP;
	union FPoint
	{
		float i;
		char c[4];
	} FP;

	if(cFile && cFile->Open() && Prof)
	{
		BackTrace=NULL;
		header=new char[3201];
		if(PB) PB->Show(0, true);
		try // - finally
		{
			try // - catch
			{
				//----------- Read SEG-Y EDCDIC Reel Header ------------------
				if(cFile->Read(header, 3200)!=3200) throw Exception("Cannot open file!");
				if(!BuiltInData)
				{
					Prof->ClearZeroPoint();
					Prof->Permit=1;
					try
					{
						try
						{
							header[10]=0;
							if(strcmp(header,"This SEG-Y")==0)
							{
								for(k=11,j=0; k<3200-81; k++)
								{
									if(*(header+k)=='\n') j++;
									if(j==5 && Prof->Permit==1) // Finding permittivity
									{
										for(i=k; i<3200-81; i++)
											if(*(header+i)=='E' && *(header+i+1)=='=') break;
										i+=2;
										cc=0;
										for(m=i; m<3200-81; m++)
										{
											if(*(header+m)==',' || *(header+m)=='.') {if(cc==0) *(header+m)=DecimalSeparator; else break; cc++;}
											else if(*(header+m)=='\r') break;
											else if(*(header+m)<48 || *(header+m)>57) break;
										}
										*(header+m)=0;
										try
										{
											Prof->Permit=StrToFloat((AnsiString)(header+i));
										}
										catch (Exception &exception) {}
										break;
									}
									if(j==12) break;
								}
								k++;
								for(i=3199-104; i>=k; i--)
									if(*(header+i)!=' ') break;
								*(header+i+1)=0;
								Prof->TextInfo=(header+k);
							}
						}
						catch(...) {}
					}
					__finally
					{
						if(Prof->Permit<=1 || Prof->Permit>100) Prof->Permit=9;
					}
					if(header[3199-81]<11 && header[3199-81]>0)
					{
						cc=header[3199-81]; // Number of gain function points (min=2, max=10)
						for(int k=0; k<Prof->Vertexes->Count; k++)
							delete (TMyPoint*)(Prof->Vertexes->Items[k]); // Clear Profile Gain Points
						Prof->Vertexes->Clear();
						for(int k=0; k<cc; k++)
						{
							for(int z=0; z<8; z++) ChP.c[z]=header[3199-81+k*8+z+1];
							mp=new TMyPoint();//TMyPoint;
							mp->x=ChP.mp.x; // Normalized point position (0..1) in samples, float
							mp->y=ChP.mp.y; // Normalized point gain (0..1) in dB, float
							Prof->Vertexes->Add(mp);
						}
					}
					// Filter points;
					if(header[3199-103]<=4 && header[3199-103]>=0)
					{
						Prof->FILTER_TYPE=(TFilterType)header[3199-103];
						if(Prof->FILTER_TYPE==FILTER_CUSTOM)
						{
							for(int k=0; k<4; k++)
							{
								for(int z=0; z<4; z++) FP.c[z]=header[3199-103+k*4+z+1];
								if(FP.i<=1 && FP.i>=0) Prof->FilterPoints[k]=FP.i;
								else
								{
									FilterOff(Prof);
									break;
								}
							}
						}
						else FilterOff(Prof);
					}
					else FilterOff(Prof);
					TMP=ExtractFileExt(cFile->FileName);
					if(TMP!="" && TMP.UpperCase()==".TMP")
					{
						tf=*(int*)(header+(3199-119));
						tt=*(int*)(header+(3199-119+4));
						sf=*(int*)(header+(3199-119+8));
						st=*(int*)(header+(3199-119+12));
					}
				}
				if(header[3199-83]=='D') DoubleStt=true;
				else DoubleStt=false;
				if(cFile->Read(header, 400)!=400) throw Exception("Cannot open file!");
				// Data sample format code
				sval=*(short int*)(header+300);
				sval=*(short int*)(header+24);
				//sval=3; For Nogin
				if((sval&0xFF00)) Reverse2=true;
				else Reverse2=false;
				ShortIntReverse2(sval);
				sample_format=sval;
				// Number of samples per trace for this reel's data
				sval=*(short int*)(header+20);
				ShortIntReverse2(sval);
				samples=sval;
				if(!BuiltInData)
				{
					// Line number
					val=*(int*)(header+4);
					IntReverse2(val);
					// Reel number
					val=*(int*)(header+8);
					IntReverse2(val);
					// Number of data traces per second
					sval=*(short int*)(header+12);
					ShortIntReverse2(sval);
					// Number of auxiliary traces per second
					sval=*(short int*)(header+14);
					ShortIntReverse2(sval);
					//samples=1018; For Nogin
					// Sample interval of this reel's data in MICROseconds
					sval=*(short int*)(header+16);
					ShortIntReverse2(sval);
					Prof->TimeRange=(float)sval/1000*(float)(samples-1);
					if(Prof->TimeRange==0) Prof->TimeRange=100;
					// CDP fold (expected number of data traces per ensemble)
					sval=*(short int*)(header+26);
					ShortIntReverse2(sval);
					// Measuring system (1-meters, 2-feets)
					sval=*(short int*)(header+54);
					ShortIntReverse2(sval);
					// Scales units
					Prof->XUnits=CheckXUnits(*(char*)(header+60));
					Prof->YUnits=CheckYUnits(*(char*)(header+61));
					Prof->Samples=samples;
				}
				bytes_to_read=0;
				switch(sample_format)
				{
					case 1: // 32-bit IBM floating point
					case 2: // 32-bit fixed point (integer)
					case 4: // 32-bit fixed point with gain code (integer)
						bytes_to_read=samples*4;
						PositiveMax=(int)((unsigned int)(1<<23) - 1);
						NegativeMax=-(int)((unsigned int)(1<<23) - 1);
						sformat=sfInt32bit;
						break;
					case 3: // 16-bit fixed point (integer)
						bytes_to_read=samples*2;
						PositiveMax=(1<<15) - 1;
						NegativeMax=-((1<<15) - 1);
						sformat=sfShort16bit;
						break;
					default: throw Exception("Cannot open file!");
				}
				buf=new char[bytes_to_read];
				rd=cFile->FileSize;
				if(rd==INVALID_FILE_SIZE) throw Exception("Cannot open file!");
				traces=(rd-3200-400)/(240+bytes_to_read);
				if(PB) PB->Max+=traces;
				if(traces==0 || (BuiltInData && (Prof->Samples!=samples || Prof->SampleFormat!=sformat)))
					throw Exception("Cannot built-in file!");
				Prof->SampleFormat=sformat;
				// Very big file
				/*int fs=(int)(GetFileSize(hndl, NULL)/(1024l*1024l));
				if(SizeRestriction && fs>MaxFileSizeMB)
				{
					Prof->Traces=traces;
					CloseHandle(hndl);
					delete[] header;
					delete[] buf;
					if(PB) PB->Hide();
					return (-1)*fs;
				}*/
				switch(DevidingType)
				{
					case 0: // file opening without restrictions
						k_start=0;
						k_end=traces;
						break;
					case 1: // open part of file from indicated StratTrace. The size of part (in MB) couldn't be more then Size
						if(StartTrace<0) StartTrace=0;
						if(StartTrace>traces) StartTrace=traces;
						k_start=StartTrace;
						k_end=k_start+(Size*1024*1024-3200-400)/(240+bytes_to_read);
						if(k_end>traces) k_end=traces;
						break;
					case 2: // open part of file from indicated StratTrace. The size of part (in traces) couldn't be more then Size
						if(StartTrace<0) StartTrace=0;
						if(StartTrace>traces) StartTrace=traces;
						k_start=StartTrace;
						k_end=k_start+Size;
						if(k_end>traces) k_end=traces;
						break;
					case 3: // open part of file from indicated StratTrace and up to the next Marker.
						if(StartTrace<0) StartTrace=0;
						if(StartTrace>traces) StartTrace=traces;
						k_start=StartTrace;
						k_end=traces;
						break;
				}
				rd=(240+bytes_to_read)*k_start;
				if(k_start>0 && cFile->Read(buf, 0, rd, fmdCurrent)!=rd) //Move file pointer
					throw Exception("Cannot open file!");
				m_c=0;
				DepthConstant=Prof->TimeRange*1e-9*3e8/2.0/sqrt(Prof->Permit);
				Ptr=Prof->FirstTracePtr;
				for(k=k_start; k<k_end; k++)
				{
					if(cFile->Read(header, 240)!=240) throw Exception("Cannot open file!");
					if(cFile->Read(buf, bytes_to_read)!=bytes_to_read) throw Exception("Cannot open file!");
					sval=*(short int*)(header+32);
					ShortIntReverse2(sval);
					Prof->Stacking=sval;
					// Check 'dead' trace
					val=*(short int*)(header+28);
					IntReverse2(val);
					if(val==2) continue;
					// Lag time between shot and recording start in PICOseconds
					if(Prof->ZeroPoint==0)
					{
						sval=*(short int*)(header+214);
						ShortIntReverse2(sval);
						if(sval==0 || (sval!=1 && (sval % 10)!=0)) dval =-1000;
						else dval=sval;
						sval=*(short int*)(header+108);
						ShortIntReverse2(sval);
						if(Prof->TimeRange>0)
						{
							zp=(float)sval/Prof->TimeRange*(float)(samples-1);
							if(dval<0) zp/=fabs(dval);
							else zp*=dval;
							Prof->ZeroPoint=(int)zp;
						}
						else Prof->ZeroPoint=0;
					}
					Trace=new TTrace(samples, Prof);
					try
					{
						// Marks indicator
						sval=*(short int*)(header+236);
						ShortIntReverse2(sval);
						if(sval==0x5555) // Mark
						{
							sval=*(short int*)(header+238);
							ShortIntReverse2(sval);
							Trace->Mark=sval;
							if(sval>0) m_c++;
						}
						for(j=0; j<samples; j++)
						{
							switch(sample_format)
							{
								case 1: // 32-bit IBM floating point
									fval=*((float*)buf+j);
									FloatReverse2(fval);
									val=(int)fval;
									break;
								case 2: // 32-bit fixed point (integer)
									val=*((int*)buf+j);
									IntReverse2(val);
									break;
								case 3: // 16-bit fixed point (integer)
									sval=*((short int*)buf+j);
									ShortIntReverse2(sval);
									val=sval;
									break;
								case 4: // 32-bit fixed point with gain code (integer)
									val=*((int*)buf+j);
									IntReverse2(val);
									break;
							}
							Trace->SourceData[j]=(short int)val;
						}
						sval=*(short int*)(header+156);
						ShortIntReverse2(sval);
						dY=sval;
						sval=*(short int*)(header+158);
						ShortIntReverse2(sval);
						dD=sval;
						try
						{
							if(dY>1900 && dY<2100 && dD>0 && dD<366)
								dt=(TDateTime)((double)EncodeDate(dY, 1, 1)+(double)dD+0.1);
							else dt=Now();
						}
						catch(...)
						{
							dt=Now();
						}
						dt.DecodeDate(&dY, &dM, &dD);
						sval=*(short int*)(header+160);
						ShortIntReverse2(sval);
						tH=sval;
						sval=*(short int*)(header+162);
						ShortIntReverse2(sval);
						tM=sval;
						sval=*(short int*)(header+164);
						ShortIntReverse2(sval);
						tS=sval;
						sval=*(short int*)(header+168);
						ShortIntReverse2(sval);
						tMS=sval;

						sval=*((short int*)(header+166));
						ShortIntReverse2(sval);
						try
						{
							Trace->TimeStamp=TDateTime(dY, dM, dD, tH, tM, tS, tMS);/**/
						}
						catch(...)
						{
							Trace->TimeStamp=Now();
                        }
						Trace->GreenwichTm=Trace->TimeStamp;

						val=*((int*)(header+48));
						if(val<0) Trace->Backward=true;
						else Trace->Backward=false;
						sval=*((short int*)(header+88));
						ShortIntReverse2(sval);
						if(sval==1) // if not arc, seconds
						{
							val=*(int*)(header+72);
							IntReverse2(val);
							asd=(float)val/1000.;
							Trace->TraceDistance=asd-OldVal;
							dist+=Trace->TraceDistance;
							OldVal=asd;
							val=*(int*)(header+80);
							IntReverse2(val);
							if(k==k_start) X0=val;
							if(k==k_end-1) X9=val;
							Trace->X=val;
							val=*(int*)(header+76);
							IntReverse2(val);
							Trace->Y=val;
						}
						else
						{
							val=*(int*)(header+80);
							IntReverse2(val);
							if(k==k_start) X0=val;
							if(k==k_end-1) X9=val;
							Trace->X=val;
							if(DoubleStt)
							{
								dval=*(double*)(header+182);
								DoubleReverse2(dval);
								Trace->Longitude=DegMinToDeg(dval);
								dval=*(double*)(header+190);
								DoubleReverse2(dval);
								Trace->Latitude=DegMinToDeg(dval);
							}
							else
							{
								fval=*(float*)(header+72);
								FloatReverse2(fval);
								Trace->Longitude=DegMinToDeg(fval);
								fval=*(float*)(header+76);
								FloatReverse2(fval);
								Trace->Latitude=DegMinToDeg(fval);
							}
							fval=*(float*)(header+40);
							FloatReverse2(fval);
							Trace->Z=fval;
							fval=*(float*)(header+44);
							FloatReverse2(fval);
							Trace->Z_Geoid=fval;
							Prof->Coordinated=true;
						}
						fval=*(float*)(header+84);
						FloatReverse2(fval);
						Prof->YPosition=fval;
						fval=*(float*)(header+52);
						FloatReverse2(fval);
						fval/=DepthConstant;
						fval*=(float)Prof->Samples;
						Trace->SamplesShifting=(int)fval;
						fval=*(float*)(header+56);
						FloatReverse2(fval);
						Trace->Heading=fval;
						fval=*(float*)(header+60);
						FloatReverse2(fval);
						Trace->Pitch=fval;
						fval=*(float*)(header+64);
						FloatReverse2(fval);
						Trace->Roll=fval;

						fval=*(float*)(header+170);
						FloatReverse2(fval);
						Trace->LocalCsY=fval;
						fval=*(float*)(header+174);
						FloatReverse2(fval);
						Trace->LocalCsX=fval;
						fval=*(float*)(header+178);
						FloatReverse2(fval);
						Trace->LocalCsH=fval;
						Trace->LocalCsCh1=*(header+234);
						Trace->LocalCsCh2=*(header+235);

						sval=*((short int*)(header+124));
						ShortIntReverse2(sval);
						if(sval==2)
						{
							sval=*((short int*)(header+132));
							ShortIntReverse2(sval);
							if(sval==4)
							{
								sval=*((short int*)(header+138));
								ShortIntReverse2(sval);
								if(sval==3)
								{
									Trace->SparDataPtr=new Spar;
									Trace->SparDataPtr->sparHeading=Trace->Heading;
									Trace->SparDataPtr->sparPitch=Trace->Pitch;
									Trace->SparDataPtr->sparRoll=Trace->Roll;
									dval=*(double*)(header+198);
									DoubleReverse2(dval);
									Trace->SparDataPtr->utilLongitude=dval;
									dval=*(double*)(header+206);
									DoubleReverse2(dval);
									Trace->SparDataPtr->utilLatitude=dval;
									dval=*(double*)(header+216);
									DoubleReverse2(dval);
									Trace->SparDataPtr->utilAltitude=dval;
									fval=*(float*)(header+126);
									FloatReverse2(fval);
									Trace->SparDataPtr->offset=fval;
									fval=*(float*)(header+134);
									FloatReverse2(fval);
									Trace->SparDataPtr->dOffset=fval;
									fval=*(float*)(header+140);
									FloatReverse2(fval);
									Trace->SparDataPtr->yaw=fval;
									fval=*(float*)(header+144);
									FloatReverse2(fval);
									Trace->SparDataPtr->dYaw=fval;
									fval=*(float*)(header+148);
									FloatReverse2(fval);
									Trace->SparDataPtr->depth=fval;
									fval=*(float*)(header+152);
									FloatReverse2(fval);
									Trace->SparDataPtr->dDepth=fval;
									fval=*(float*)(header+224);
									FloatReverse2(fval);
									Trace->SparDataPtr->dBField=fval;
								}
							}
						}
						if(Trace->Backward)
						{
							if(BackTrace==NULL) BackTrace=Prof->LastTracePtr;
							else if(BackTrace!=NULL) BackTrace=BackTrace->PtrDn;
							if(BackTrace==NULL)
							{
								if(Prof->FirstTracePtr==NULL) Trace=AddOrReplace(Prof, Trace, Ptr, !BuiltInData);
								BackTrace=Prof->FirstTracePtr;
							}
							// else can't be used !!!
							if(BackTrace!=NULL) //Replace existing trace by bacward
							{
								BackTrace->CopyTrace(Trace);
								dist-=Trace->TraceDistance;
								delete Trace;
								Trace=NULL;
								ProfContainsBackwardMovement=true;
							}
						}
						else
						{
							if(BackTrace!=NULL) BackTrace=BackTrace->PtrUp;
							if(BackTrace!=NULL)
							{
								dist-=BackTrace->TraceDistance;
								BackTrace->CopyTrace(Trace);
								delete Trace;
							}
							else Trace=AddOrReplace(Prof, Trace, Ptr, !BuiltInData);
						}
						Trace=NULL;
						if(BuiltInData) Ptr=Ptr->PtrUp;
					}
					catch(...)
					{
						if(Trace) delete Trace;
						Trace=NULL;
					}
					if(DevidingType==3 && m_c>0)
					{
						if(Prof->Traces>1) k_end=k;
						else m_c=0;
					}
					if(PB) PB->StepIt();
				}
				if(!BuiltInData)
				{
					Prof->HorRangeFrom=X0;
					Prof->HorRangeTo=X9;
					sval=*(short int*)(header+70); // ????????????? (Q) which header is used after traces reading? - (A) The last one...
					ShortIntReverse2(sval);
					if(sval>0)
					{
						Prof->HorRangeFrom*=(float)sval;
						Prof->HorRangeTo*=(float)sval;
					}
					else if(sval<0)
					{
						Prof->HorRangeFrom/=(float)(-sval);
						Prof->HorRangeTo/=(float)(-sval);
					}
					/*Prof->HorRangeFrom-=Prof->FirstTracePtr->TraceDistance;
					if(Prof->HorRangeFrom<0) Prof->HorRangeFrom=0;*/
					if(ProfContainsBackwardMovement)
					{
						Prof->HorRangeTo=Prof->HorRangeFrom+dist;
						Prof->FileModified=true;
					}
					else
					{
						double horpos=Prof->HorRangeFrom;
						if(dist<=0)
						{
							dist=(float)(Prof->HorRangeTo-Prof->HorRangeFrom)/(float)Prof->Traces;
							Trace=Prof->FirstTracePtr;
							while(Trace!=NULL)
							{
								Trace->TraceDistance=dist;
								Trace->HorPos=horpos;
								horpos+=dist;
								Trace=Trace->PtrUp;
							}
						}
					}
					Prof->HorRangeFull=fabs(Prof->HorRangeTo-Prof->HorRangeFrom);
					/*if(Prof->Coordinated)
					{
						long double lat1, lon1, lat2, lon2, a, b, xx, dd;

						dist=0.0;
						Trace=Prof->FirstTracePtr;
						while(Trace!=NULL)
						{
							if(Trace->PtrDn!=NULL)
							{
								//lat1=Trace->Latitude/100.;
								//lon1=Trace->Longitude/100.;
								//lat2=Trace->PtrDn->Latitude/100.;
								//lon2=Trace->PtrDn->Longitude/100.;
								//a=lat1-(int)lat1;
								//lat1=(long double)100.*a/(long double)60.+(int)lat1;
								//a=lat2-(int)lat2;
								//lat2=(long double)100.*a/(long double)60.+(int)lat2;
								//a=lon1-(int)lon1;
								//lon1=(long double)100.*a/(long double)60.+(int)lon1;
								//a=lon2-(int)lon2;
								//lon2=(long double)100.*a/(long double)60.+(int)lon2;
								a=sinl(lat1/(long double)57.29577951)*sinl(lat2/(long double)57.29577951);
								b=cosl(lat1/(long double)57.29577951)*cosl(lat2/(long double)57.29577951)*cosl(lon2/(long double)57.29577951-lon1/(long double)57.29577951);
								xx=a+b;
								if((-xx*xx+(long double)1.)>0) dd=(long double)6367444.65*(atanl(-xx/sqrtl(-xx*xx+(long double)1.))+(long double)2.*atanl((long double)1.));
								else dd=0;
								Trace->TraceDistance=dd;
								dist+=Trace->TraceDistance;
							}
							k++;
							Trace=Trace->PtrUp;
						}
						Prof->FirstTracePtr->TraceDistance=Prof->FirstTracePtr->PtrUp->TraceDistance/2.;
						Prof->FirstTracePtr->PtrUp->TraceDistance/=2.;
					}*/
					if(Prof->HorRangeFull<0.05)
					{
						Prof->HorRangeFull=Prof->HorRangeTo=dist;
						Prof->HorRangeFrom=0;
					}
				}
				//CalculGainFunction(Prof->Vertexes, Prof->GainFunction, Prof->Samples);
				TGainFunction *Gain;
				Gain=new TGainFunction(Prof->Samples);
				Gain->CopyFromProfile(Prof);
				Gain->ApplyGain=ApplyGain;

				Trace=Prof->FirstTracePtr;
				while(Trace!=NULL)
				{
					for(int i=0; i<Prof->Samples; i++)
					{
						smpl=(float)Trace->SourceData[i]*Gain->GainFunction[i];
						if(smpl>PositiveMax) Trace->Data[i]=PositiveMax;
						else if(smpl<NegativeMax) Trace->Data[i]=NegativeMax;
						else Trace->Data[i]=(short int)smpl;
					}
					Trace=Trace->PtrUp;
				}
				Prof->FullName=cFile->FileName;
				if(tf>=0 && tf<Prof->Traces && tt>0 && tt<Prof->Traces && (tt-tf)>0 &&
					sf>=0 && sf<Prof->Samples && st>0 && st<Prof->Samples && (st-sf)>0)
				{
					Prof->TraceTo=tt;
					Prof->TraceFrom=tf;
					Prof->SampleTo=st;
					Prof->SampleFrom=sf;
				}
			}
			catch(...) {}
		}
		__finally
		{
			if(PB) PB->Hide(true);
			delete[] header;
			if(buf) delete[] buf;
			cFile->Close();
			res=0;
		}
	}

	return res;
}
//---------------------------------------------------------------------------

TTrace* AddOrReplace(TProfile* Prof, TTrace *NewPtr, TTrace* ExistingPtr, bool AddOrReplace) // AddOrReplace: true to add trace, false to replace
{
	TTrace* res=NULL;

	if(!AddOrReplace && ExistingPtr)
	{
		//ExistingPtr->CopyTrace(NewPtr);
		ExistingPtr->CopyTraceDataOnly(NewPtr);
		delete NewPtr;
		res=ExistingPtr;
	}
	else
	{
		Prof->Add(NewPtr);
		res=NewPtr;
	}
	return res;
}

void FilterOff(TProfile *Prof)
{
  Prof->FILTER_TYPE=FILTER_OFF;
  Prof->FilterPoints[0]=0.01;
  Prof->FilterPoints[1]=0.05;
  Prof->FilterPoints[2]=0.2;
  Prof->FilterPoints[3]=0.5;
}

void IntReverse2(int &ival)
{
    union Dat
     {
     int i;
     char c[4];
     }Dat;
    char ch;

    if(!Reverse2) return;

    Dat.i=ival;
    ch=Dat.c[0];
    Dat.c[0]=Dat.c[3];
    Dat.c[3]=ch;
    ch=Dat.c[1];
    Dat.c[1]=Dat.c[2];
    Dat.c[2]=ch;
    ival=Dat.i;
}

void ShortIntReverse2(short int &sval)
{
    union Dat
	{
		short int i;
		char c[2];
	} Dat;
	char ch;

	if(!Reverse2) return;

	Dat.i=sval;
	ch=Dat.c[0];
	Dat.c[0]=Dat.c[1];
	Dat.c[1]=ch;
	sval=Dat.i;
}

void FloatReverse2(float &fval)
{
	union Dat
	{
		float f;
		char c[4];
	} Dat;
	char ch;
	int res;

	if(Reverse2)
	{
		Dat.f=fval;
		ch=Dat.c[0];
		Dat.c[0]=Dat.c[3];
		Dat.c[3]=ch;
		ch=Dat.c[1];
		Dat.c[1]=Dat.c[2];
		Dat.c[2]=ch;
		fval=Dat.f;
	}
	try
	{
		//res=_fpclass(fval) & _FPCLASS_UNSUP	& _FPCLASS_SNAN	& _FPCLASS_QNAN	& _FPCLASS_NINF	& _FPCLASS_PINF;
		if(_isnan(fval)) res=1;
		else res=0;
	}
	catch(Exception &e)
	{
		res=1;
	}
	if(res) fval=0;
}

void CalculGainFunction(TList *Vs, float *GF, int Smpls)
{
	float delta, bp, ep, tmp;
	int bs, es;
	int k=0;
	TMyPoint *gp, *gpn;

	while(k<Vs->Count-1)
	{
		gp=(TMyPoint*)Vs->Items[k];
		gpn=(TMyPoint*)Vs->Items[k+1];
		if(gp)
		{
			bp=pow(10., gp->y*(float)MaxGain/20.);
			ep=pow(10., gpn->y*(float)MaxGain/20.);
			bs=(int)(gp->x*(float)Smpls);
			es=(int)(gpn->x*(float)Smpls);
			delta=(ep-bp)/(float)(es-bs);
			for(int i=bs; i<es; i++) GF[i]=(float)(i-bs)*delta+(float)bp;
			k++;
		}
	}
}

void DoubleReverse2(double &dval)
{
    union Dat
	{
		double d;
		char c[8];
	} Dat;
    char ch;
    int res;

    if(Reverse2)
    {
        Dat.d=dval;
		for(int i=0; i<4; i++)
        {
			ch=Dat.c[i];
			Dat.c[i]=Dat.c[7-i];
          	Dat.c[i]=ch;
        }
        dval=Dat.d;
    }
    try
	{
		//res=_fpclass(dval) & _FPCLASS_UNSUP	& _FPCLASS_SNAN	& _FPCLASS_QNAN	& _FPCLASS_NINF	& _FPCLASS_PINF;
		if(_isnan(dval)) res=1;
		else res=0;
	}
	catch(Exception &e)
	{
		res=1;
	}
    if(res) dval=0;
}

#pragma package(smart_init)
