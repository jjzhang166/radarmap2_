//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Disclaimer.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "ImageButton"
#pragma link "G32_ProgressBar"
#pragma link "GR32_Image"
#pragma resource "*.dfm"
TDisclaimerForm *DisclaimerForm;

//---------------------------------------------------------------------------
__fastcall TDisclaimerForm::TDisclaimerForm(TComponent* Owner, TRadarMapManager *AManager)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------

void __fastcall TDisclaimerForm::CloseIButtonClick(TObject *Sender)
{
	ModalResult=mrCancel;
}

//---------------------------------------------------------------------------

void __fastcall TDisclaimerForm::FormShow(TObject *Sender)
{
	HideCaret(Memo1->Handle);
}
//---------------------------------------------------------------------------

void __fastcall TDisclaimerForm::OkImageButtonClick(TObject *Sender)
{
	ModalResult=mrOk;
}
//---------------------------------------------------------------------------

void __fastcall TDisclaimerForm::FormHide(TObject *Sender)
{
	Close();
}
//---------------------------------------------------------------------------

void __fastcall TDisclaimerForm::TitlePanelMouseDown(TObject *Sender, TMouseButton Button,
		  TShiftState Shift, int X, int Y)
{
	TitleDown=true;
	TitleXY=ClientToScreen(Types::TPoint(X, Y));
}
//---------------------------------------------------------------------------

void __fastcall TDisclaimerForm::TitlePanelMouseMove(TObject *Sender, TShiftState Shift,
		  int X, int Y)
{
	Types::TPoint tmp;

	if(TitleDown)
	{
		tmp=ClientToScreen(Types::TPoint(X, Y));
		Left+=tmp.x-TitleXY.x;
		Top+=tmp.y-TitleXY.y;
		TitleXY=tmp;
	}
}
//---------------------------------------------------------------------------

void __fastcall TDisclaimerForm::TitlePanelMouseUp(TObject *Sender, TMouseButton Button,
		  TShiftState Shift, int X, int Y)
{
	TitleDown=false;
}
//---------------------------------------------------------------------------

void __fastcall TDisclaimerForm::BriefMemoKeyPress(TObject *Sender, wchar_t &Key)
{
	Key=0; //Restriction due to "<![CDATA[" and "]]>" using of BriefInfo storing in GML format
}
//---------------------------------------------------------------------------

void __fastcall TDisclaimerForm::BriefMemoChange(TObject *Sender)
{
	HideCaret(((TMemo*)Sender)->Handle);
}
//---------------------------------------------------------------------------
