//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Setup.h"
#include "Manager.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "GR32_Image"
#pragma link "ImageButton"
#pragma resource "*.dfm"

//int iii;

//---------------------------------------------------------------------------
// TSetupRecThread
//---------------------------------------------------------------------------
__fastcall TSetupRecThread::TSetupRecThread(bool CreateSuspended, TObject* AManager,
	TImgView32 *AImage, Types::TRect *BR, int AChannelsPxOffset) : TMyThread(CreateSuspended)
{
	Trace=NULL;
	Manager=AManager;
	Image=AImage;
	BordersRect=BR;
	transparency=255;
	ChannelsPxOffset=AChannelsPxOffset;
	Bmp=new TBitmap32();
	//Bmp=ABmp;
	Refresh();
	if(Manager && ((TRadarMapManager*)Manager)->ReceivingSettings)
	{
		((TRadarMapManager*)Manager)->ReceivingSettings->Positioning=ManualP;
		if(Image)
		{
			Layers=new TBitmapLayer*[((TRadarMapManager*)Manager)->ReceivingProfilesQuantity];
			for(int i=0; i<((TRadarMapManager*)Manager)->ReceivingProfilesQuantity; i++)
			{
				Layers[i]=new TBitmapLayer(Image->Layers);
				Layers[i]->Bitmap->DrawMode=dmBlend;
				Layers[i]->BringToFront();
				Layers[i]->OnPaint=&LayerPaint;
			}
		}
		else Layers=NULL;
	}
	else Layers=NULL;
}
//---------------------------------------------------------------------------

__fastcall TSetupRecThread::~TSetupRecThread()
{
	try
	{
		if(Manager && Image && Layers)
		{
			for(int i=0; i<((TRadarMapManager*)Manager)->ReceivingProfilesQuantity; i++)
				if(Layers[i]) Image->Layers->Delete(Layers[i]->Index);
			delete[] Layers;
		}
		delete Bmp;
	}
	catch(Exception &e) {}
}
//---------------------------------------------------------------------------

void __fastcall TSetupRecThread::ExecuteLoopBody()
{

	if(Manager && ((TRadarMapManager*)Manager)->ReceivingSettings)
	{
		if(((TRadarMapManager*)Manager)->ReceivingSettings->SelectedChannelMode==TChannelMode::Double)
		{
			for(int i=0; i<((TRadarMapManager*)Manager)->ReceivingProfilesQuantity; i++)
			{
				if(((TRadarMapManager*)Manager)->ReceivingProfiles[i] &&
					((TRadarMapManager*)Manager)->ReceivingProfiles[i]->LastTracePtr &&
						!((TRadarMapManager*)Manager)->ReceivingProfiles[i]->LastTracePtr->Drawed)
				{
					Trace=((TRadarMapManager*)Manager)->ReceivingProfiles[i]->LastTracePtr;
					Trace->Channel=i;
					Synchronize(ProcessTrace);
				}
			}
		}
		else
		{
			if(((TRadarMapManager*)Manager)->ReceivingProfile)
				Trace=((TRadarMapManager*)Manager)->ReceivingProfile->LastTracePtr;
			if(Trace && !Trace->Drawed)
			{
				Trace->Channel=0;
				Synchronize(ProcessTrace);
			}
		}
		//((TRadarMapManager*)Manager)->ReceivingProfile->Clear();
	}
	WaitForSingleObject(IdleEvent, 2);
}
//---------------------------------------------------------------------------

void __fastcall TSetupRecThread::ProcessTrace()
{
	int w, ww, h, hh, y, x, max, i, x0, y0, ChN, ChNPx;
	float ds, f, d;

	try
	{
		if(Bmp && Trace && !Trace->Drawed && Image && Layers && Layers[Trace->Channel])
		{
			if(Manager && ((TRadarMapManager*)Manager)->ReceivingSettings)
			{
				switch(((TRadarMapManager*)Manager)->ReceivingSettings->SelectedChannelMode)
				{
					case TChannelMode::Double: ChN=2;break;
					case TChannelMode::Channel1:
					case TChannelMode::Channel2:
					case TChannelMode::Tx1Rx2:
					case TChannelMode::Tx2Rx1: ChN=1;break;
					case TChannelMode::Circle:
					default: ChN=1;
				}
			}
			else ChN=1;
			ChNPx=(float)ChannelsPxOffset*(float)(ChN-1)/(float)ChN;
			if(BordersRect)
			{
				w=BordersRect->Width();
				//h=BordersRect->Height()-1;
				h=(int)((float)(BordersRect->Height()-1)/(float)ChN)-ChNPx;
				x0=BordersRect->Left;
				y0=BordersRect->Top;
				if(ChN>1) y0+=Trace->Channel*(h+ChannelsPxOffset);
			}
			else
			{
				w=Image->Width;
				//h=Image->Height;
				h=(int)((float)(Image->Height-1)/(float)ChN)-ChNPx;
				x0=0;
				y0=0;
				if(ChN>1) y0+=Trace->Channel*(h+ChannelsPxOffset);
			}
			Bmp->BeginUpdate();
			try
			{
				try
				{
					if(w>0 && h>0)
					{
						Bmp->FillRect(x0-1, y0-1, x0+w+1, y0+h+1, 0x00000000);
						ww=w;
						hh=h;
						max=(hh>>1);
						y=max+y0;
						if(ChN==2)
						{
							if(Trace->Channel==0) Bmp->PenColor=Color32(0, 0, 255, transparency);//clBlue32;
							else if(Trace->Channel==1) Bmp->PenColor=Color32(255, 0, 0, transparency);//clRed32;
							else Bmp->PenColor=Color32(0, 255, 0, transparency);//clGreen32;
						}
						else Bmp->PenColor=Color32(255, 0, 0, transparency);//clRed32;

						ds=(float)Trace->Samples/(float)ww;
						f=0;
						for(i=0; i<ww; i++)
						{
							d=Trace->Data[(int)f];
							if(d>=0) d/=(float)PositiveMax;
							else d/=(float)-NegativeMax;
							d*=(float)max;
							if(i==0) Bmp->MoveTo(x0+i, y-d);
							else Bmp->LineToAS(x0+i, y-d);
							f+=ds;
						}
					}
				}
				catch(Exception &e) {}
			}
			__finally
			{
				Bmp->EndUpdate();
			}
			//if(Trace->Channel==0)
			Layers[Trace->Channel]->Update();
			Trace->Drawed=true;
			if(((TRadarMapManager*)Manager)->ReceivingProfiles[Trace->Channel] &&
				Trace!=((TRadarMapManager*)Manager)->ReceivingProfiles[Trace->Channel]->FirstTracePtr)
					 ((TRadarMapManager*)Manager)->ReceivingProfiles[Trace->Channel]->DeleteFirstTrace();
		}
	}
	catch(Exception &e) {}
}
//---------------------------------------------------------------------------

void __fastcall TSetupRecThread::LayerPaint(TObject *Sender, TBitmap32 *LayerBmp)
{
	if(Image && Bmp && LayerBmp)
	{
		LayerBmp->BeginUpdate();
		try
		{
			//LayerBmp->SetSizeFrom(Image);
			LayerBmp->Draw(0, 0, Bmp);
		}
		__finally
		{
			LayerBmp->EndUpdate();
		}
	}
}

//---------------------------------------------------------------------------
// TAutoDelayThread
//---------------------------------------------------------------------------
__fastcall TAutoDelayThread::TAutoDelayThread(TObject* AManager, TChannel *AChannel,
	TSetupRecThread *ASetupThrd, TAfterAutoDelay aad, TShowDelay sd) : TMyThread(true)
{
	SetupThrd=ASetupThrd;
	Trace=NULL;
	Manager=AManager;
	channel=AChannel;
	AfterAutoDelay=aad;
	ShowDelay=sd;
	if(SetupThrd) SetupThrd->AskForSuspend();
	Cnt=0;
	Delay=80;
	threshold=(int)((float)PositiveMax/3.25);
	ic=0;
	FindIt=false;
	if(Manager && ((TRadarMapManager*)Manager)->ReceivingSettings && channel)
	{
		initialchannelindex=((TRadarMapManager*)Manager)->ReceivingSettings->SelectedChannelIndex;
		((TRadarMapManager*)Manager)->ReceivingSettings->SelectedChannelIndex=channel->ChannelNumber;
		initialdelay=channel->PulseDelay;
		channel->PulseDelay=Delay;
		initialstacking=((TRadarMapManager*)Manager)->ReceivingSettings->Stacking;
		initialtimerange=channel->TimeRange;
		if(channel->SelectedAntenna && channel->SelectedAntenna->SelectedTimeRangeIndex==0)
		{
			channel->TimeRange=channel->SelectedAntenna->LocalMaxTimeRange;
		}
		((TRadarMapManager*)Manager)->ReceivingSettings->Stacking=1;
		((TRadarMapManager*)Manager)->Connection->WriteCommand(
			((TRadarMapManager*)Manager)->ReceivingSettings->GetTuneCommand(), NULL);
	}
	AskForResume();
}
//---------------------------------------------------------------------------

__fastcall TAutoDelayThread::~TAutoDelayThread()
{
	//if(SetupThrd) SetupThrd->AskForResume();
}
//---------------------------------------------------------------------------

void __fastcall TAutoDelayThread::DrawDelay()
{
	if(ShowDelay)
	{
		(ShowDelay)(Delay);
	}
}
//---------------------------------------------------------------------------

void __fastcall TAutoDelayThread::ExecuteLoopBody()
{
	int d, devider;

	if(Trace==NULL) MySleep(100);
	if(Manager && ((TRadarMapManager*)Manager)->ReceivingSettings &&
		channel && channel->SelectedAntenna && channel->SelectedAntenna->TimeRange<=50)
		devider=1;
	else devider=0;
	if(((TRadarMapManager*)Manager)->ReceivingProfile)
	{
		Trace=((TRadarMapManager*)Manager)->ReceivingProfile->LastTracePtr;
		if(Trace && !Trace->Drawed)
		{
			try
			{
				d=-1;
				for(int i=0; i<Trace->Samples; i++)
				{
					if(abs(Trace->Data[i])>threshold)
					{
						d=i;
						break;
					}
				}
				if(d<0) Delay+=20 >> devider;
				else
				{
					if(d>=80) Delay+=14;
					else if(d<80 && d>=50) Delay+=8 >> devider;
					else if(d<50 && d>=30) Delay+=4 >> devider;
					else if(d<30 && d>=20) Delay+=2 >> devider;
					else if(d<20 && d>11) Delay++;
					else if(d<=11 && d>=9) //We've find it!!!
					{
						FindIt=true;
					}
					else if(d<9 && d>=5) Delay--;
					else if(d<5 && d>0) {Delay-=20 >> devider; ic++;} //>0) {Delay-=20; ic++;}
					else if(d==0) Delay+=10 >> devider;
				}
				if(Manager && ((TRadarMapManager*)Manager)->ReceivingSettings && channel)
				{
					channel->PulseDelay=Delay;
					((TRadarMapManager*)Manager)->Connection->WriteCommand(
						((TRadarMapManager*)Manager)->ReceivingSettings->GetTuneCommand(), NULL);
				}
				else Cnt=5;
				if(SetupThrd)
				{
					SetupThrd->Trace=Trace;
					SetupThrd->ProcessTrace();
				}
				Synchronize(DrawDelay);
			}
			catch(Exception &e) {}
		}
		//((TRadarMapManager*)Manager)->ReceivingProfile->Clear();
	}
	if(Delay>=1024 || ic>2)
	{
		Delay=80;
		Cnt++;
	}
	if(Cnt>=4 || FindIt)
	{
		if(AfterAutoDelay)
		{
			if(FindIt) (AfterAutoDelay)(Delay);
			else (AfterAutoDelay)(InitialDelay);
		}
		AskForTerminate();
		if(SetupThrd) SetupThrd->AskForResume();
	}
	WaitForSingleObject(IdleEvent, 5);
}

//---------------------------------------------------------------------------
// TSetupForm
//---------------------------------------------------------------------------
void __fastcall TSetupForm::CheckRadioImagesOnPanel(TObject *Sender)
{
  int z=((TComponent *)Sender)->Owner->ComponentCount;

  for(int i=0; i<z; i++)
  {
	if(((TComponent *)Sender)->Owner->Components[i]->ClassName()=="TImage" &&
		((TComponent *)Sender)->Owner->Components[i]->Tag==((TComponent *)Sender)->Tag &&
		((TComponent *)Sender)->Owner->Components[i]!=Sender)
	{
		((TImage*)((TComponent *)Sender)->Owner->Components[i])->Stretch=false;
		((TImage*)((TComponent *)Sender)->Owner->Components[i])->Picture->Assign(UncheckedImage->Picture);
	}
  }
  ((TImage*)Sender)->Stretch=true;
  ((TImage*)Sender)->Picture->Assign(CheckedImage->Picture);
}
//---------------------------------------------------------------------------

__fastcall TSetupForm::TSetupForm(TComponent* Owner, TObject* AManager)
	: TForm(Owner)
{
	Manager=AManager;
	if(Manager && ((TRadarMapManager*)Manager)->ReceivingSettings)
		Gain=new TGainFunction(((TRadarMapManager*)Manager)->ReceivingSettings->RealSamples);
	else Gain=NULL;
	BordersRect=Types::TRect(13, 13, Image->Width-12, Image->Height-12);
	ChannelsPxOffset=4;
	SetupThrd=new TSetupRecThread(false, Manager, Image, &BordersRect, ChannelsPxOffset);
	GainImageGrid=new TOscilloscopeGrid(Image, ((TRadarMapManager*)Manager)->ReceivingSettings,
		ChannelsPxOffset, &BordersRect);
	if(Manager && ((TRadarMapManager*)Manager)->ReceivingSettings)
	{
		GainItems=new TGainItems(Manager, NULL, Image, &BordersRect, false, false, NULL, NULL, GainPointsPanel);
		GainItems->Visible=!GainPanel->Collapsed;
		GainItems->AssignControls(gcLock, ImageUnlock, ImageLock);
		GainItems->AssignControls(gcNextPoint, ImageFF, NULL);
		GainItems->AssignControls(gcPrevPoint, ImageRR, NULL);
		GainItems->AssignControls(gcMoveLeft, ImageLeft, NULL);
		GainItems->AssignControls(gcMoveRight, ImageRight, NULL);
		GainItems->AssignControls(gcMoveUp, ImageUp, NULL);
		GainItems->AssignControls(gcMoveDown, ImageDown, NULL);

		TempGains=new TGainFunction*[((TRadarMapManager*)Manager)->ReceivingSettings->ChannelQuantity];
		Gains=new TGainFunction*[((TRadarMapManager*)Manager)->ReceivingSettings->ChannelQuantity];

		//TempGain=new TGainFunction(((TRadarMapManager*)Manager)->ReceivingSettings->RealSamples);
		for(int i=0; i<((TRadarMapManager*)Manager)->ReceivingSettings->ChannelQuantity; i++)
		{
			TempGains[i]=((TRadarMapManager*)Manager)->ReceivingSettings->Channels[i]->Gain;
			Gains[i]=new TGainFunction(TempGains[i]->RealSamples);
			Gains[i]->Copy(TempGains[i]);
			((TRadarMapManager*)Manager)->ReceivingSettings->Channels[i]->Gain=Gains[i];
		}
		//((TRadarMapManager*)Manager)->ReceivingSettings->SelectedChannel->Gain=GainItems->GainFunction;
	}
	else
	{
		TempGains=NULL;
		GainItems=NULL;
	}
	AutoDelayThrd=NULL;
	Channel=NULL;
}
//---------------------------------------------------------------------------

__fastcall TSetupForm::~TSetupForm()
{
	SetupThrd->AskForTerminate();
	WaitOthers();
	if(SetupThrd->ForceTerminate()) delete SetupThrd;
	if(AutoDelayThrd)
	{
		AutoDelayThrd->AskForTerminate();
		WaitOthers();
		if(AutoDelayThrd->ForceTerminate()) delete AutoDelayThrd;
	}
	delete[] TempGains;
	for(int i=0; i<((TRadarMapManager*)Manager)->ReceivingSettings->ChannelQuantity; i++)
	{
		delete Gains[i];
	}
	delete[] Gains;
	delete GainItems;
	delete Gain;
	delete GainImageGrid;
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Ch1ImageClick(TObject *Sender)
{
	if(!((TImage*)Sender)->Stretch) CheckRadioImagesOnPanel(Sender);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Label2Click(TObject *Sender)
{
	if(Manager && ((TRadarMapManager*)Manager)->ReceivingSettings)
	{
		Ch1Panel->Collapse();
		Ch2Panel->Collapse();
		//if(TempGain) ((TRadarMapManager*)Manager)->ReceivingSettings->SelectedChannel->Gain=TempGain;
		if(((TRadarMapManager*)Manager)->ReceivingSettings->SelectedChannelMode!=TChannelMode::Channel1)
		{
			((TRadarMapManager*)Manager)->ReceivingSettings->SelectedChannelMode = TChannelMode::Channel1;
			RebuildPanels();
			Adjust();
		}
		//TempGain=((TRadarMapManager*)Manager)->ReceivingSettings->SelectedChannel->Gain;
		ChannelPanel->Collapse();
		CategoryPanelGroup->ScrollBy(0, -100);
	}
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Label1Click(TObject *Sender)
{
	if(Manager && ((TRadarMapManager*)Manager)->ReceivingSettings)
	{
		Ch1Panel->Collapse();
		Ch2Panel->Collapse();
		//if(TempGain) ((TRadarMapManager*)Manager)->ReceivingSettings->SelectedChannel->Gain=TempGain;
		if(((TRadarMapManager*)Manager)->ReceivingSettings->SelectedChannelMode!=TChannelMode::Channel2)
		{
			((TRadarMapManager*)Manager)->ReceivingSettings->SelectedChannelMode = TChannelMode::Channel2;
			RebuildPanels();
			Adjust();
		}
		//TempGain=((TRadarMapManager*)Manager)->ReceivingSettings->SelectedChannel->Gain;
		ChannelPanel->Collapse();
		CategoryPanelGroup->ScrollBy(0, -100);
	}
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Label7Click(TObject *Sender)
{
	if(Manager && ((TRadarMapManager*)Manager)->ReceivingSettings &&
		((TRadarMapManager*)Manager)->ReceivingSettings->SelectedChannel)
	{
		((TRadarMapManager*)Manager)->ReceivingSettings->SelectedChannel->SelectedHPF=THPFState::HPFStateOFF;
		Adjust();
		FilterPanel2->Collapse();
		FilterPanel->Collapse();
		CategoryPanelGroup->ScrollBy(0, -100);
	}
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Label8Click(TObject *Sender)
{
	if(Manager && ((TRadarMapManager*)Manager)->ReceivingSettings &&
		((TRadarMapManager*)Manager)->ReceivingSettings->SelectedChannel)
	{
		((TRadarMapManager*)Manager)->ReceivingSettings->SelectedChannel->SelectedHPF=THPFState::Weak;
		Adjust();
		FilterPanel->Collapse();
		CategoryPanelGroup->ScrollBy(0, -100);
	}
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Label9Click(TObject *Sender)
{
	if(Manager && ((TRadarMapManager*)Manager)->ReceivingSettings &&
		((TRadarMapManager*)Manager)->ReceivingSettings->SelectedChannel)
	{
		((TRadarMapManager*)Manager)->ReceivingSettings->SelectedChannel->SelectedHPF=THPFState::Strong;
		Adjust();
		FilterPanel->Collapse();
		CategoryPanelGroup->ScrollBy(0, -100);
	}
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Label10Click(TObject *Sender)
{
	if(Manager && ((TRadarMapManager*)Manager)->ReceivingSettings &&
		((TRadarMapManager*)Manager)->ReceivingSettings->SelectedChannel)
	{
		((TRadarMapManager*)Manager)->ReceivingSettings->SelectedChannel->SelectedHPF=THPFState::SuperStrong;
		Adjust();
		FilterPanel->Collapse();
		CategoryPanelGroup->ScrollBy(0, -100);
	}
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Image1Click(TObject *Sender)
{
	if(TempGains)
	{
		for(int i=0; i<((TRadarMapManager*)Manager)->ReceivingSettings->ChannelQuantity; i++)
		{
			if(TempGains[i])
			{
				TempGains[i]->Copy(((TRadarMapManager*)Manager)->ReceivingSettings->Channels[i]->Gain);
				((TRadarMapManager*)Manager)->ReceivingSettings->Channels[i]->Gain=TempGains[i];
			}
		}
	}
	//((TRadarMapManager*)Manager)->ReceivingSettings->SelectedChannel->Gain=TempGain;
	//if(TempGain && GainItems && GainItems->GainFunction)
	//	  TempGain->Copy(GainItems->GainFunction);
	ModalResult=mrOk;
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Image2Click(TObject *Sender)
{
	if(TempGains)
	{
		for(int i=0; i<((TRadarMapManager*)Manager)->ReceivingSettings->ChannelQuantity; i++)
			((TRadarMapManager*)Manager)->ReceivingSettings->Channels[i]->Gain=TempGains[i];
	}

	//((TRadarMapManager*)Manager)->ReceivingSettings->SelectedChannel->Gain=TempGain;
	ModalResult=mrCancel;
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Adjust()
{
	AnsiString str, str2;

	if(Manager && ((TRadarMapManager*)Manager)->ReceivingSettings)
	{
		if(((TRadarMapManager*)Manager)->ReceivingSettings->SelectedChannelMode==TChannelMode::Double && Channel)
			((TRadarMapManager*)Manager)->ReceivingSettings->SelectedChannelIndex=Channel->ChannelNumber;
		((TRadarMapManager*)Manager)->Connection->WriteCommand(
			((TRadarMapManager*)Manager)->ReceivingSettings->GetTuneCommand(), NULL);
		switch(((TRadarMapManager*)Manager)->ReceivingSettings->Medium)
		{
			case 0: break; //Ch1ImageClick(Medium1Image); break;
			case 1: Ch1ImageClick(Medium2Image); break;
			case 2: Ch1ImageClick(Medium3Image); break;
			case 3: Ch1ImageClick(Medium4Image); break;
			case 4: break; //Ch1ImageClick(Medium5Image); break;
			case 5: Ch1ImageClick(Medium6Image); break;
			case 6: Ch1ImageClick(Medium7Image); break;
			case 7: break; //Ch1ImageClick(Medium8Image); break;
			case 8: Ch1ImageClick(Medium9Image); break;
			case 9: break; //Ch1ImageClick(Medium10Image); break;
			case 10: break; //Ch1ImageClick(Medium11Image); break;
			case 11: Ch1ImageClick(Medium12Image); break;
			case 12: break; //Ch1ImageClick(Medium13Image); break;
			case 13: break; //Ch1ImageClick(Medium14Image); break;
			case 14: Ch1ImageClick(Medium15Image); break;
			case 15: Ch1ImageClick(Medium16Image); break;
		}
		MediumPanel->Caption="Medium : "+SetupMediumStrings[((TRadarMapManager*)Manager)->ReceivingSettings->Medium];
		switch(((TRadarMapManager*)Manager)->ReceivingSettings->Stacking)
		{
			case 2: Ch1ImageClick(Stacking2Image); break;
			case 4: Ch1ImageClick(Stacking3Image); break;
			case 8: Ch1ImageClick(Stacking4Image); break;
			case 16: Ch1ImageClick(Stacking5Image); break;
			case 32: Ch1ImageClick(Stacking6Image); break;
			case 64: Ch1ImageClick(Stacking7Image); break;
			case 128: Ch1ImageClick(Stacking8Image); break;
			case 256: Ch1ImageClick(Stacking9Image); break;
			case 512: Ch1ImageClick(Stacking10Image); break;
			case 1024: Ch1ImageClick(Stacking11Image); break;
			case 1:
			default: Ch1ImageClick(Stacking1Image); break;
		}
		StackingPanel->Caption="Stacking : "+IntToStr(((TRadarMapManager*)Manager)->ReceivingSettings->Stacking);
		if(SamplesPanel->Visible)
		{
			switch(((TRadarMapManager*)Manager)->ReceivingSettings->Samples)
			{
				case 128: Ch1ImageClick(Samples1Image); str=Label37->Caption; break;
				case 256: Ch1ImageClick(Samples2Image); str=Label40->Caption; break;
				case 1024: Ch1ImageClick(Samples4Image); str=Label39->Caption; break;
				case 512:
				default: Ch1ImageClick(Samples3Image); str=Label38->Caption; break;
			}
			SamplesPanel->Caption="Samples : "+str;
		}

		switch(((TRadarMapManager*)Manager)->ReceivingSettings->SelectedChannelMode)
		{
			case TChannelMode::Channel1:
			case TChannelMode::Channel2:
				if(((TRadarMapManager*)Manager)->ReceivingSettings->SelectedChannelMode==TChannelMode::Channel1)
				{
                    Ch1ImageClick(Ch1Image);
					str2=Label2->Caption;
				}
				else if(((TRadarMapManager*)Manager)->ReceivingSettings->SelectedChannelMode==TChannelMode::Channel2)
				{
					Ch1ImageClick(Ch2Image);
					str2=Label1->Caption;
				}
                if(((TRadarMapManager*)Manager)->ReceivingSettings->SelectedChannel)
				{
					if(AntennaPanel->Visible)
					{
						switch(((TRadarMapManager*)Manager)->ReceivingSettings->SelectedChannel->SelectedAntennaIndex)
						{
							case 0: Ch1ImageClick(Ant1Image); str=Label23->Caption; break;
							case 1: Ch1ImageClick(Ant2Image); str=Label24->Caption; break;
							case 2: Ch1ImageClick(Ant3Image); str=Label26->Caption; break;
							case 3: Ch1ImageClick(Ant4Image); str=Label27->Caption; break;
							case 4: Ch1ImageClick(Ant5Image); str=Label28->Caption; break;
							case 5: Ch1ImageClick(Ant6Image); str=Label29->Caption; break;
							case 6: Ch1ImageClick(Ant7Image); str=Label30->Caption; break;
							//case 7: Ch1ImageClick(Ant8Image); str=Label31->Caption; break;
						}
						AntennaPanel->Caption="Antenna : "+str;
					}
					else if(AntennaPanel2->Visible)
					{
						switch(((TRadarMapManager*)Manager)->ReceivingSettings->SelectedChannel->SelectedAntennaIndex)
						{
							case 0: Ch1ImageClick(Ant1Image2); str=Label32->Caption; break;
							case 1: Ch1ImageClick(Ant2Image2); str=Label35->Caption; break;
							case 2: Ch1ImageClick(Ant3Image2); str=Label33->Caption; break;
							case 3: Ch1ImageClick(Ant4Image2); str=Label34->Caption; break;
						}
						AntennaPanel2->Caption="Antenna : "+str;
					}
					if(ZondTimePanel->Visible)
					{
						switch(((TRadarMapManager*)Manager)->ReceivingSettings->SelectedChannel->SelectedAntenna->SelectedTimeRangeIndex)
						{
							case 0:
								Ch1ImageClick(TimeCustomImage);
								str="Custom "+IntToStr(((TRadarMapManager*)Manager)->ReceivingSettings->SelectedChannel->SelectedAntenna->TimeRange)+" ns";
								Label3->Caption=str;
								break; //custom;
							case 1: Ch1ImageClick(Time50Image); str=Label4->Caption; break;
							case 2: Ch1ImageClick(Time100Image); str=Label5->Caption; break;
							case 3: Ch1ImageClick(Time200Image); str=Label6->Caption; break;
							case 4: Ch1ImageClick(Time300Image); str=Label11->Caption; break;
							case 5: Ch1ImageClick(Time500Image); str=Label12->Caption; break;
							case 6: Ch1ImageClick(Time800Image); str=Label14->Caption; break;
							case 7: Ch1ImageClick(Time1200Image); str=Label13->Caption; break;
							case 8: Ch1ImageClick(Time2000Image); str=Label15->Caption; break;
						}
						ZondTimePanel->Caption="Time Range : "+str;
					}
					else if(PythonTimePanel->Visible)
					{
						switch(((TRadarMapManager*)Manager)->ReceivingSettings->SelectedChannel->SelectedAntenna->SelectedTimeRangeIndex)
						{
							case 0:
								switch(((TRadarMapManager*)Manager)->ReceivingSettings->SelectedChannel->SelectedAntenna->TimeRange)
								{
									case 500: Ch1ImageClick(Time500Image2); str=Label18->Caption; break;
									case 750: Ch1ImageClick(Time750Image2); str=Label19->Caption; break;
									case 1000: Ch1ImageClick(Time1000Image2); str=Label20->Caption; break;
									default:
										Ch1ImageClick(TimeCustom2Image);
										str="Custom "+IntToStr(((TRadarMapManager*)Manager)->ReceivingSettings->SelectedChannel->SelectedAntenna->TimeRange)+" ns";
										Label17->Caption=str;
										break; //custom;
								}
								break;
							case 1: Ch1ImageClick(Time1500Image2); str=Label21->Caption; break;
						}
						PythonTimePanel->Caption="Time Range : "+str;
					}
					else if(InfraRadarTimePanel->Visible)
					{
						switch(((TRadarMapManager*)Manager)->ReceivingSettings->SelectedChannel->SelectedAntenna->SelectedTimeRangeIndex)
						{
							case 0:
								Ch1ImageClick(TimeCustom3Image);
								str="Custom "+IntToStr(((TRadarMapManager*)Manager)->ReceivingSettings->SelectedChannel->SelectedAntenna->TimeRange)+" ns";
								Label22->Caption=str;
								break; //custom;
							case 1: Ch1ImageClick(Time50Image3); str=Label25->Caption; break;
						}
						InfraRadarTimePanel->Caption="Time Range : "+str;
					}
					if(FilterPanel->Visible)
					{
						switch(((TRadarMapManager*)Manager)->ReceivingSettings->SelectedChannel->SelectedHPF)
						{
							case THPFState::HPFStateOFF: Ch1ImageClick(FilterOffImage); str="OFF"; break;
							case THPFState::Weak: Ch1ImageClick(FilterWeakImage); str="Weak"; break;
							case THPFState::Strong: Ch1ImageClick(FilterStrongImage); str="Strong"; break;
							case THPFState::SuperStrong: Ch1ImageClick(FilterSuperImage); str="Super Strong"; break;
							case THPFState::HPFStateCustom: break;
						}
						FilterPanel->Caption="Filter : "+str;
					}
					else if(FilterPanel2->Visible)
					{
						switch(((TRadarMapManager*)Manager)->ReceivingSettings->SelectedChannel->SelectedHPF)
						{
							case THPFState::HPFStateOFF: Ch1ImageClick(FilterOffImage2); str="OFF"; break;
							case THPFState::HPFPreset: Ch1ImageClick(FilterPresetImage); str="Preset"; break;
							case THPFState::HPFStateCustom: break;
						}
						FilterPanel2->Caption="Filter : "+str;
					}
					StaticText1->Caption=IntToStr(((TRadarMapManager*)Manager)->ReceivingSettings->SelectedChannel->PulseDelay);
					PulseDelayPanel->Caption="Pulse Delay : "+StaticText1->Caption;
				}
				break;
			case TChannelMode::Double:
				Ch1ImageClick(DoubleChImage);
				GainImageGrid->Refresh();
				SetupThrd->Refresh();
				str2=Label70->Caption;
				if(((TRadarMapManager*)Manager)->ReceivingSettings->Channels)
				{
					if(((TRadarMapManager*)Manager)->ReceivingSettings->Channels[0])
					{
						switch(((TRadarMapManager*)Manager)->ReceivingSettings->Channels[0]->SelectedAntennaIndex)
						{
							case 0: Ch1ImageClick(Image55); str=Label23->Caption; break;
							case 1: Ch1ImageClick(Image56); str=Label24->Caption; break;
							case 2: Ch1ImageClick(Image57); str=Label26->Caption; break;
							case 3: Ch1ImageClick(Image58); str=Label27->Caption; break;
							case 4: Ch1ImageClick(Image59); str=Label28->Caption; break;
							case 5: Ch1ImageClick(Image60); str=Label29->Caption; break;
							case 6: Ch1ImageClick(Image61); str=Label30->Caption; break;
							//case 7: Ch1ImageClick(Image62); str=Label31->Caption; break;
						}
						Ch1AntennaPanel->Caption="Antenna : "+str;
						switch(((TRadarMapManager*)Manager)->ReceivingSettings->Channels[0]->SelectedAntenna->SelectedTimeRangeIndex)
						{
							case 0:
								Ch1ImageClick(Ch1TimeCustomImage);
								str="Custom "+IntToStr(((TRadarMapManager*)Manager)->ReceivingSettings->Channels[0]->SelectedAntenna->TimeRange)+" ns";
								Label3->Caption=str;
								break; //custom;
							case 1: Ch1ImageClick(Image46); str=Label4->Caption; break;
							case 2: Ch1ImageClick(Image47); str=Label5->Caption; break;
							case 3: Ch1ImageClick(Image48); str=Label6->Caption; break;
							case 4: Ch1ImageClick(Image49); str=Label11->Caption; break;
							case 5: Ch1ImageClick(Image50); str=Label12->Caption; break;
							case 6: Ch1ImageClick(Image43); str=Label14->Caption; break;
							case 7: Ch1ImageClick(Image42); str=Label13->Caption; break;
							case 8: Ch1ImageClick(Image44); str=Label15->Caption; break;
						}
						Ch1TimePanel->Caption="Time Range : "+str;
						switch(((TRadarMapManager*)Manager)->ReceivingSettings->Channels[0]->SelectedHPF)
						{
							case THPFState::HPFStateOFF: Ch1ImageClick(Image31); str="OFF"; break;
							case THPFState::Weak: Ch1ImageClick(Image32); str="Weak"; break;
							case THPFState::Strong: Ch1ImageClick(Image33); str="Strong"; break;
							case THPFState::SuperStrong: Ch1ImageClick(Image34); str="Super Strong"; break;
							case THPFState::HPFStateCustom: break;
						}
						Ch1FilterPanel->Caption="Filter : "+str;
						StaticText3->Caption=IntToStr(((TRadarMapManager*)Manager)->ReceivingSettings->Channels[0]->PulseDelay);
						Ch1PulseDelayPanel->Caption="Pulse Delay : "+StaticText3->Caption;
					}
					if(((TRadarMapManager*)Manager)->ReceivingSettings->Channels[1])
					{
						switch(((TRadarMapManager*)Manager)->ReceivingSettings->Channels[1]->SelectedAntennaIndex)
						{
							case 0: Ch1ImageClick(Image76); str=Label23->Caption; break;
							case 1: Ch1ImageClick(Image77); str=Label24->Caption; break;
							case 2: Ch1ImageClick(Image78); str=Label26->Caption; break;
							case 3: Ch1ImageClick(Image79); str=Label27->Caption; break;
							case 4: Ch1ImageClick(Image80); str=Label28->Caption; break;
							case 5: Ch1ImageClick(Image81); str=Label29->Caption; break;
							case 6: Ch1ImageClick(Image82); str=Label30->Caption; break;
							//case 7: Ch1ImageClick(Image83); str=Label31->Caption; break;
						}
						Ch2AntennaPanel->Caption="Antenna : "+str;
						switch(((TRadarMapManager*)Manager)->ReceivingSettings->Channels[1]->SelectedAntenna->SelectedTimeRangeIndex)
						{
							case 0:
								Ch1ImageClick(Ch2TimeCustomImage);
								str="Custom "+IntToStr(((TRadarMapManager*)Manager)->ReceivingSettings->Channels[1]->SelectedAntenna->TimeRange)+" ns";
								Label3->Caption=str;
								break; //custom;
							case 1: Ch1ImageClick(Image71); str=Label4->Caption; break;
							case 2: Ch1ImageClick(Image72); str=Label5->Caption; break;
							case 3: Ch1ImageClick(Image73); str=Label6->Caption; break;
							case 4: Ch1ImageClick(Image74); str=Label11->Caption; break;
							case 5: Ch1ImageClick(Image75); str=Label12->Caption; break;
							case 6: Ch1ImageClick(Image68); str=Label14->Caption; break;
							case 7: Ch1ImageClick(Image67); str=Label13->Caption; break;
							case 8: Ch1ImageClick(Image69); str=Label15->Caption; break;
						}
						Ch2TimePanel->Caption="Time Range : "+str;
						switch(((TRadarMapManager*)Manager)->ReceivingSettings->Channels[1]->SelectedHPF)
						{
							case THPFState::HPFStateOFF: Ch1ImageClick(Image63); str="OFF"; break;
							case THPFState::Weak: Ch1ImageClick(Image64); str="Weak"; break;
							case THPFState::Strong: Ch1ImageClick(Image65); str="Strong"; break;
							case THPFState::SuperStrong: Ch1ImageClick(Image66); str="Super Strong"; break;
							case THPFState::HPFStateCustom: break;
						}
						Ch2FilterPanel->Caption="Filter : "+str;
						StaticText4->Caption=IntToStr(((TRadarMapManager*)Manager)->ReceivingSettings->Channels[1]->PulseDelay);
						Ch2PulseDelayPanel->Caption="Pulse Delay : "+StaticText4->Caption;
					}
				}
				break;
			case TChannelMode::Tx1Rx2:
			case TChannelMode::Tx2Rx1:
			case TChannelMode::Circle: str2=""; break;
		}
		ChannelPanel->Caption="Channels : "+str2;
		//ChannelPanel->Repaint();
	}
	GainPanel->Collapse();
	Ch1GainPanel->Collapse();
	Ch2GainPanel->Collapse();
}
//--------------------------------------------------------------------------

void __fastcall TSetupForm::RebuildPanels()
{
	int t=0;

	if(Manager && ((TRadarMapManager*)Manager)->ReceivingSettings)
	{
		SendMessage(CategoryPanelGroup->Handle, WM_SETREDRAW, false, 0);
		for(int i=0; i<CategoryPanelGroup->Panels->Count; i++)
		{
			((TCategoryPanel*)CategoryPanelGroup->Panels->Items[i])->Visible=false;
			((TCategoryPanel*)CategoryPanelGroup->Panels->Items[i])->Top=0;
		}
		MediumPanel->Top=t;
		MediumPanel->Visible=true;
		t+=MediumPanel->Height;

		switch(((TRadarMapManager*)Manager)->ReceivingSettings->UnitType)
		{
			case DOUBLE9:
			case DUAL10:
			case COBRA:
			case COBRA_CBD:
				SamplesPanel->Top=t;
				SamplesPanel->Visible=true;
				t+=SamplesPanel->Height;
				break;
		}
		StackingPanel->Top=t;
		StackingPanel->Visible=true;
		t+=StackingPanel->Height;
		switch(((TRadarMapManager*)Manager)->ReceivingSettings->UnitType)
		{
			case ZYNQ_0:
			case PYTHON3:
			case PYTHON3bis:
            	if(((TRadarMapManager*)Manager)->ReceivingSettings->UnitType==PYTHON3bis)
				{
					Image8->Visible=true;
					Label106->Visible=true;
				}
				else
				{
					Image8->Visible=false;
					Label106->Visible=false;
                }
				AntennaPanel2->Top=t;
				AntennaPanel2->Visible=true;
				t+=AntennaPanel2->Height;
				PythonTimePanel->Top=t;
				PythonTimePanel->Visible=true;
				t+=PythonTimePanel->Height;
				FilterPanel2->Top=t;
				FilterPanel2->Visible=true;
				t+=FilterPanel2->Height;
				GainPanel->Top=t;
				GainPanel->Visible=true;
				break;
			case INFRARADAR:
			case INFRARADAR100:
				if(((TRadarMapManager*)Manager)->ReceivingSettings->UnitType==INFRARADAR)
				{
					Label25->Caption="50 ns";
					Label25->Tag=50;
				}
				else
				{
					Label25->Caption="100 ns";
					Label25->Tag=100;
				}
				InfraRadarTimePanel->Top=t;
				InfraRadarTimePanel->Visible=true;
				t+=InfraRadarTimePanel->Height;
				GainPanel->Top=t;
				GainPanel->Visible=true;
				break;
			case DOUBLE9:
			case DUAL10:
			case COBRA:
			case COBRA_CBD:
			case DOUBLE1:
			case DOUBLE2:
				ChannelPanel->Top=t;
				ChannelPanel->Visible=true;
				t+=ChannelPanel->Height;
				if(((TRadarMapManager*)Manager)->ReceivingSettings->SelectedChannelMode==TChannelMode::Double)
				{
					Ch1Panel->Top=t;
					Ch1Panel->Visible=true;
					t+=Ch1Panel->Height;
					Ch2Panel->Top=t;
					Ch2Panel->Visible=true;
					t+=Ch2Panel->Height;
					if(((TRadarMapManager*)Manager)->ReceivingSettings->UnitType==COBRA ||
						((TRadarMapManager*)Manager)->ReceivingSettings->UnitType==COBRA_CBD)
					{
						Ch1AntennaPanel->Visible=false;
						Ch2AntennaPanel->Visible=false;
						/*Image56->Visible=false;
						Image57->Visible=false;
						Image58->Visible=false;
						Image59->Visible=false;
						Image60->Visible=false;
						Image61->Visible=false;
						Image77->Visible=false;
						Image78->Visible=false;
						Image79->Visible=false;
						Image80->Visible=false;
						Image81->Visible=false;
						Image82->Visible=false;
						Label91->Visible=false;
						Label92->Visible=false;
						Label93->Visible=false;
						Label94->Visible=false;
						Label95->Visible=false;
						Label96->Visible=false;
						Label99->Visible=false;
						Label100->Visible=false;
						Label101->Visible=false;
						Label102->Visible=false;
						Label103->Visible=false;
						Label104->Visible=false;
						Label90->Caption="Cobra Shielded";
						Label98->Caption="Cobra Shielded";*/
					}
					else
					{
						Ch1AntennaPanel->Visible=true;
						Ch2AntennaPanel->Visible=true;
						/*Image56->Visible=true;
						Image57->Visible=true;
						Image58->Visible=true;
						Image59->Visible=true;
						Image60->Visible=true;
						Image61->Visible=true;
						Image77->Visible=true;
						Image78->Visible=true;
						Image79->Visible=true;
						Image80->Visible=true;
						Image81->Visible=true;
						Image82->Visible=true;
						Label91->Visible=true;
						Label92->Visible=true;
						Label93->Visible=true;
						Label94->Visible=true;
						Label95->Visible=true;
						Label96->Visible=true;
						Label99->Visible=true;
						Label100->Visible=true;
						Label101->Visible=true;
						Label102->Visible=true;
						Label103->Visible=true;
						Label104->Visible=true;
						Label90->Caption="100 MHz Shielded";
						Label98->Caption="100 MHz Shielded";*/
					}
					break;
				}
			case SINGLE1:
			case SINGLE2:
			default:
				/*if(((TRadarMapManager*)Manager)->ReceivingSettings->UnitType!=COBRA &&
					((TRadarMapManager*)Manager)->ReceivingSettings->UnitType!=COBRA_CBD)
				{
					Ant2Image->Visible=true;
					Ant3Image->Visible=true;
					Ant4Image->Visible=true;
					Ant5Image->Visible=true;
					Ant6Image->Visible=true;
					Ant7Image->Visible=true;
					Label24->Visible=true;
					Label26->Visible=true;
					Label27->Visible=true;
					Label28->Visible=true;
					Label29->Visible=true;
					Label30->Visible=true;
					Label23->Caption="100 MHz Shielded";
				}
				else
				{
					Ant2Image->Visible=false;
					Ant3Image->Visible=false;
					Ant4Image->Visible=false;
					Ant5Image->Visible=false;
					Ant6Image->Visible=false;
					Ant7Image->Visible=false;
					Label24->Visible=false;
					Label26->Visible=false;
					Label27->Visible=false;
					Label28->Visible=false;
					Label29->Visible=false;
					Label30->Visible=false;
					Label23->Caption="Cobra Shielded";
				}*/
                if(((TRadarMapManager*)Manager)->ReceivingSettings->UnitType!=COBRA &&
					((TRadarMapManager*)Manager)->ReceivingSettings->UnitType!=COBRA_CBD)
				{
					AntennaPanel->Top=t;
					AntennaPanel->Visible=true;
					t+=AntennaPanel->Height;
				}
				ZondTimePanel->Top=t;
				ZondTimePanel->Visible=true;
				t+=ZondTimePanel->Height;
				FilterPanel->Top=t;
				FilterPanel->Visible=true;
				t+=FilterPanel->Height;
				GainPanel->Top=t;
				GainPanel->Visible=true;
				PulseDelayPanel->Top=t;
				PulseDelayPanel->Visible=true;
				t+=PulseDelayPanel->Height;
				break;
		}
		CaptionST->Caption=((TRadarMapManager*)Manager)->ReceivingSettings->UnitName+" SETUP";
		SendMessage(CategoryPanelGroup->Handle, WM_SETREDRAW, true, 0);
		CategoryPanelGroup->Invalidate();
	}
}

void __fastcall TSetupForm::FormShow(TObject *Sender)
{
	if(Manager && ((TRadarMapManager*)Manager)->ReceivingSettings)
	{
		RebuildPanels();
		Caption=((TRadarMapManager*)Manager)->ReceivingSettings->UnitName+" Setup";
		/*
		if(((TRadarMapManager*)Manager)->ReceivingSettings->UnitType>=SINGLE1 &&
			((TRadarMapManager*)Manager)->ReceivingSettings->UnitType<=INFRARADAR)
				Label68->Caption=RadarTypeStrings[((TRadarMapManager*)Manager)->ReceivingSettings->UnitType];
		else Label68->Caption=RadarTypeStrings[OtherCHM];*/
		Label68->Caption=((TRadarMapManager*)Manager)->ReceivingSettings->Name;
	}
	CategoryPanelGroup->CollapseAll();
	CategoryPanelGroup->ScrollBy(0, -100);
	Ch1CategoryPanelGroup->CollapseAll();
	Ch1CategoryPanelGroup->ScrollBy(0, -100);
	Ch2CategoryPanelGroup->CollapseAll();
	Ch2CategoryPanelGroup->ScrollBy(0, -100);
	Adjust();
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Image6Click(TObject *Sender)
{
	if(Manager && ((TRadarMapManager*)Manager)->ReceivingSettings &&
		((TRadarMapManager*)Manager)->ReceivingSettings->SelectedChannel)
	{
		((TRadarMapManager*)Manager)->ReceivingSettings->SelectedChannel->PulseDelay++;
		Adjust();
	}
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Image4Click(TObject *Sender)
{
	if(Manager && ((TRadarMapManager*)Manager)->ReceivingSettings &&
		((TRadarMapManager*)Manager)->ReceivingSettings->SelectedChannel)
	{
		((TRadarMapManager*)Manager)->ReceivingSettings->SelectedChannel->PulseDelay--;
		Adjust();
	}
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Image5Click(TObject *Sender)
{
	if(Manager && ((TRadarMapManager*)Manager)->ReceivingSettings &&
		((TRadarMapManager*)Manager)->ReceivingSettings->SelectedChannel)
	{
		((TRadarMapManager*)Manager)->ReceivingSettings->SelectedChannel->PulseDelay+=20;
		Adjust();
	}
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Image3Click(TObject *Sender)
{
	if(Manager && ((TRadarMapManager*)Manager)->ReceivingSettings &&
		((TRadarMapManager*)Manager)->ReceivingSettings->SelectedChannel)
	{
		((TRadarMapManager*)Manager)->ReceivingSettings->SelectedChannel->PulseDelay-=20;
		Adjust();
	}
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Image7Click(TObject *Sender)
{
	int z=((TComponent *)Sender)->Owner->ComponentCount;

	if(AutoDelayThrd)
	{
		AutoDelayThrd->AskForTerminate();
		WaitOthers();
		if(AutoDelayThrd->ForceTerminate()) delete AutoDelayThrd;
		AutoDelayThrd=NULL;
	}
	for(int i=0; i<z; i++)
	{
		if(((TComponent *)Sender)->Owner->Components[i]->ClassName()=="TImage")
		{
			((TImage*)((TComponent *)Sender)->Owner->Components[i])->Enabled=false;
		}
	}/**/
	Image10->Enabled=true;

	GainPanel->Collapse();
	CategoryPanelGroup->Enabled=false;
	Ch1CategoryPanelGroup->Enabled=false;
	Ch2CategoryPanelGroup->Enabled=false;

	Gain->Copy(((TRadarMapManager*)Manager)->ReceivingSettings->SelectedChannel->Gain);
	((TRadarMapManager*)Manager)->ReceivingSettings->SelectedChannel->Gain->GainPointsCount=3;
	((TRadarMapManager*)Manager)->ReceivingSettings->SelectedChannel->Gain->GainPoints[0]=TGainPoint(0, 18);
	((TRadarMapManager*)Manager)->ReceivingSettings->SelectedChannel->Gain->GainPoints[1]=TGainPoint(0.02, 18);
	((TRadarMapManager*)Manager)->ReceivingSettings->SelectedChannel->Gain->GainPoints[2]=TGainPoint(1, 0);/**/
	AutoDelayThrd=new TAutoDelayThread(Manager, ((TRadarMapManager*)Manager)->ReceivingSettings->SelectedChannel,
		SetupThrd, AfterAutoDelay, ShowDelay);
	WaitPanel->Visible=true;
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::ShowDelay(int Delay)
{
	StaticText1->Caption=IntToStr(Delay);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Ch1ShowDelay(int Delay)
{
	StaticText3->Caption=IntToStr(Delay);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Ch2ShowDelay(int Delay)
{
	StaticText4->Caption=IntToStr(Delay);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::AfterAutoDelay(int Delay)
{
	int z=((TComponent *)this)->ComponentCount;

	if(Manager && ((TRadarMapManager*)Manager)->ReceivingSettings && AutoDelayThrd)
	{
		((TRadarMapManager*)Manager)->ReceivingSettings->Stacking=AutoDelayThrd->InitialStacking;
		if(AutoDelayThrd->Channel)
		{
			AutoDelayThrd->Channel->TimeRange=AutoDelayThrd->InitialTimeRange;
			AutoDelayThrd->Channel->PulseDelay=Delay;
			AutoDelayThrd->Channel->Gain->Copy(Gain);
		}
	}
	for(int i=0; i<z; i++)
	{
		if(((TComponent *)this)->Components[i]->ClassName()=="TImage")
		{
			((TImage*)((TComponent *)this)->Components[i])->Enabled=true;
		}
	}
	Adjust();/**/
	//GainPanel->Enabled=true;
	CategoryPanelGroup->Enabled=true;
	Ch1CategoryPanelGroup->Enabled=true;
	Ch2CategoryPanelGroup->Enabled=true;
	WaitPanel->Visible=false;
	ProcessMessages();
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Image10Click(TObject *Sender)
{
	if(AutoDelayThrd)
	{
		AutoDelayThrd->AskForSuspend();
		WaitOthers();
		AfterAutoDelay(AutoDelayThrd->InitialDelay);
		AutoDelayThrd->AskForTerminate();
		WaitOthers();
		if(AutoDelayThrd->ForceTerminate()) delete AutoDelayThrd;
		AutoDelayThrd=NULL;
		if(SetupThrd) SetupThrd->AskForResume();
	}
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Label18Click(TObject *Sender)
{
	if(Manager && ((TRadarMapManager*)Manager)->ReceivingSettings &&
		((TRadarMapManager*)Manager)->ReceivingSettings->SelectedChannel)
	{
		((TRadarMapManager*)Manager)->ReceivingSettings->SelectedChannel->SelectedAntenna->TimeRange=((TLabel*)Sender)->Tag;
		Adjust();
		InfraRadarTimePanel->Collapse();
		PythonTimePanel->Collapse();
		ZondTimePanel->Collapse();
	}
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Time500Image2Click(TObject *Sender)
{
	Label18Click(Label18);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::TimeCustomImageClick(TObject *Sender)
{
	int z=((TComponent *)Sender)->Owner->ComponentCount;

	if(Ch1Panel->Visible || Ch2Panel->Visible)
	{
		if(((TControl*)Sender)->Tag==188) //Channel 1
		{
			Ch1ImageClick(Ch1TimeCustomImage);
			Ch1TimePanel->Caption="Time Range "+Label17->Caption;
			if(Manager && ((TRadarMapManager*)Manager)->ReceivingSettings &&
				((TRadarMapManager*)Manager)->ReceivingSettings->Channels)
				Channel=((TRadarMapManager*)Manager)->ReceivingSettings->Channels[0];
			else Channel=NULL;
		}
		else if(((TControl*)Sender)->Tag==288) //Channel 2
		{
			Ch1ImageClick(Ch1TimeCustomImage);
			Ch1TimePanel->Caption="Time Range "+Label17->Caption;
			if(Manager && ((TRadarMapManager*)Manager)->ReceivingSettings &&
				((TRadarMapManager*)Manager)->ReceivingSettings->Channels)
				Channel=((TRadarMapManager*)Manager)->ReceivingSettings->Channels[1];
			else Channel=NULL;
		}
		else Channel=NULL;
		if(Channel && Channel->SelectedAntenna)
			CustomTimeRange=Channel->SelectedAntenna->TimeRange;
		else CustomTimeRange=300;
	}
	else
	{
		if(ZondTimePanel->Visible)
		{
			Ch1ImageClick(TimeCustomImage);
			ZondTimePanel->Caption="Time Range "+Label17->Caption;
		}
		else if(PythonTimePanel->Visible)
		{
			Ch1ImageClick(TimeCustom2Image);
			PythonTimePanel->Caption="Time Range "+Label17->Caption;
		}
		else if(InfraRadarTimePanel->Visible)
		{
			Ch1ImageClick(TimeCustom3Image);
			InfraRadarTimePanel->Caption="Time Range "+Label17->Caption;
		}
		if(Manager && ((TRadarMapManager*)Manager)->ReceivingSettings)
			Channel=((TRadarMapManager*)Manager)->ReceivingSettings->SelectedChannel;
		else Channel=NULL;
		if(Channel) CustomTimeRange=Channel->SelectedAntenna->TimeRange;
		else CustomTimeRange=300;
	}
	if(!CustomPanel->Visible)
	{
		for(int i=0; i<z; i++)
		{
			if(((TComponent *)Sender)->Owner->Components[i]->ClassName()=="TImage")
			{
				((TImage*)((TComponent *)Sender)->Owner->Components[i])->Enabled=false;
			}
		}
		Image17->Enabled=true;
		Image18->Enabled=true;
		Image19->Enabled=true;
		Image20->Enabled=true;
		Image21->Enabled=true;
		Image22->Enabled=true;
		StaticText2->Caption=IntToStr(CustomTimeRange);
		CustomPanel->Visible=true;
	}
	else CustomPanel->Visible=true;
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Image18Click(TObject *Sender)
{
	if(CustomPanel->Visible)
	{
		int z=((TComponent *)Sender)->Owner->ComponentCount;

		for(int i=0; i<z; i++)
		{
			if(((TComponent *)Sender)->Owner->Components[i]->ClassName()=="TImage")
			{
				((TImage*)((TComponent *)Sender)->Owner->Components[i])->Enabled=true;
			}
		}
		try
		{
			CustomTimeRange=StrToInt(StaticText2->Caption);
		}
		catch(Exception &e) {}
		if(Channel && Channel->SelectedAntenna)
			Channel->SelectedAntenna->TimeRange=CustomTimeRange;
		Adjust();
		CustomPanel->Visible=false;
		InfraRadarTimePanel->Collapse();
		PythonTimePanel->Collapse();
		ZondTimePanel->Collapse();
	}
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Image19Click(TObject *Sender)
{
	if(Channel && Channel->SelectedAntenna)
	{
		Channel->SelectedAntenna->TimeRange-=20;
		StaticText2->Caption=IntToStr(Channel->SelectedAntenna->TimeRange);
	}
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Image20Click(TObject *Sender)
{
	if(Channel && Channel->SelectedAntenna)
	{
		Channel->SelectedAntenna->TimeRange--;
		StaticText2->Caption=IntToStr(Channel->SelectedAntenna->TimeRange);
	}
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Image21Click(TObject *Sender)
{
	if(Channel && Channel->SelectedAntenna)
	{
		Channel->SelectedAntenna->TimeRange++;
		StaticText2->Caption=IntToStr(Channel->SelectedAntenna->TimeRange);
	}
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Image22Click(TObject *Sender)
{
	if(Channel && Channel->SelectedAntenna)
	{
		Channel->SelectedAntenna->TimeRange+=20;
		StaticText2->Caption=IntToStr(Channel->SelectedAntenna->TimeRange);
	}
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Image17Click(TObject *Sender)
{
	if(CustomPanel->Visible)
	{
		int z=((TComponent *)Sender)->Owner->ComponentCount;

		for(int i=0; i<z; i++)
		{
			if(((TComponent *)Sender)->Owner->Components[i]->ClassName()=="TImage")
			{
				((TImage*)((TComponent *)Sender)->Owner->Components[i])->Enabled=true;
			}
		}
		if(Channel && Channel->SelectedAntenna)
			Channel->SelectedAntenna->TimeRange=CustomTimeRange;
		Adjust();
		CustomPanel->Visible=false;;
	}
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Time750Image2Click(TObject *Sender)
{
	Label18Click(Label19);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Time1000Image2Click(TObject *Sender)
{
	Label18Click(Label20);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Time1500Image2Click(TObject *Sender)
{
	Label18Click(Label21);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Time50ImageClick(TObject *Sender)
{
	Label18Click(Label4);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Time100ImageClick(TObject *Sender)
{
	Label18Click(Label5);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Time200ImageClick(TObject *Sender)
{
	Label18Click(Label6);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Time300ImageClick(TObject *Sender)
{
	Label18Click(Label11);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Time500ImageClick(TObject *Sender)
{
	Label18Click(Label12);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Time800ImageClick(TObject *Sender)
{
	Label18Click(Label14);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Time1200ImageClick(TObject *Sender)
{
	Label18Click(Label13);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Time2000ImageClick(TObject *Sender)
{
	Label18Click(Label15);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Time50Image3Click(TObject *Sender)
{
	Label18Click(Label25);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Label23Click(TObject *Sender)
{
	if(Manager && ((TRadarMapManager*)Manager)->ReceivingSettings &&
		((TRadarMapManager*)Manager)->ReceivingSettings->SelectedChannel)
	{
		((TRadarMapManager*)Manager)->ReceivingSettings->SelectedChannel->SelectedAntennaIndex=((TLabel*)Sender)->Tag;
		Adjust();
		AntennaPanel2->Collapse();
		AntennaPanel->Collapse();
	}
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Ant1ImageClick(TObject *Sender)
{
	Label23Click(Label23);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Ant2ImageClick(TObject *Sender)
{
	Label23Click(Label24);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Ant3ImageClick(TObject *Sender)
{
	Label23Click(Label26);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Ant4ImageClick(TObject *Sender)
{
	Label23Click(Label27);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Ant5ImageClick(TObject *Sender)
{
	Label23Click(Label28);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Ant6ImageClick(TObject *Sender)
{
	Label23Click(Label29);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Ant7ImageClick(TObject *Sender)
{
	Label23Click(Label30);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Ant8ImageClick(TObject *Sender)
{
	//Label23Click(Label31);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Ant1Image2Click(TObject *Sender)
{
	Label23Click(Label32);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Ant2Image2Click(TObject *Sender)
{
    Label23Click(Label35);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Ant3Image2Click(TObject *Sender)
{
	Label23Click(Label33);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Ant4Image2Click(TObject *Sender)
{
	Label23Click(Label34);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Label37Click(TObject *Sender)
{
	if(Manager && ((TRadarMapManager*)Manager)->ReceivingSettings)
	{
		((TRadarMapManager*)Manager)->ReceivingSettings->Samples=((TLabel*)Sender)->Tag;
		Adjust();
        SamplesPanel->Collapse();
	}
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Samples1ImageClick(TObject *Sender)
{
	Label37Click(Label37);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Samples2ImageClick(TObject *Sender)
{
	Label37Click(Label40);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Samples3ImageClick(TObject *Sender)
{
	Label37Click(Label38);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Samples4ImageClick(TObject *Sender)
{
	Label37Click(Label39);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Label41Click(TObject *Sender)
{
	if(Manager && ((TRadarMapManager*)Manager)->ReceivingSettings)
	{
		((TRadarMapManager*)Manager)->ReceivingSettings->Medium =((TLabel*)Sender)->Tag;
		((TRadarMapManager*)Manager)->ReceivingSettings->Permitivity = Permittivities[((TLabel*)Sender)->Tag];
		Adjust();
		MediumPanel->Collapse();
		CategoryPanelGroup->ScrollBy(0, -100);
	}
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::MediumPanelExpand(TObject *Sender)
{
	/*for(int i=0; i<CategoryPanelGroup1->Panels->Count; i++)
	{
		if((TCategoryPanel*)CategoryPanelGroup1->Panels->Items[i]!=(TCategoryPanel*)Sender)
			((TCategoryPanel*)CategoryPanelGroup1->Panels->Items[i])->Collapse();
	}*/
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Medium9ImageClick(TObject *Sender)
{
	Label41Click(Label42);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Medium1ImageClick(TObject *Sender)
{
	Label41Click(Label41);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Medium2ImageClick(TObject *Sender)
{
	Label41Click(Label44);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Medium3ImageClick(TObject *Sender)
{
	Label41Click(Label45);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Medium4ImageClick(TObject *Sender)
{
	Label41Click(Label46);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Medium5ImageClick(TObject *Sender)
{
	Label41Click(Label49);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Medium6ImageClick(TObject *Sender)
{
	Label41Click(Label52);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Medium7ImageClick(TObject *Sender)
{
	Label41Click(Label53);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Medium8ImageClick(TObject *Sender)
{
	Label41Click(Label54);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Medium10ImageClick(TObject *Sender)
{
	Label41Click(Label43);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Medium11ImageClick(TObject *Sender)
{
	Label41Click(Label47);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Medium12ImageClick(TObject *Sender)
{
	Label41Click(Label48);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Medium13ImageClick(TObject *Sender)
{
	Label41Click(Label50);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Medium14ImageClick(TObject *Sender)
{
	Label41Click(Label51);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Medium15ImageClick(TObject *Sender)
{
	Label41Click(Label55);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Medium16ImageClick(TObject *Sender)
{
	Label41Click(Label56);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Label63Click(TObject *Sender)
{
	if(Manager && ((TRadarMapManager*)Manager)->ReceivingSettings)
	{
		((TRadarMapManager*)Manager)->ReceivingSettings->Stacking =((TLabel*)Sender)->Tag;
		Adjust();
		StackingPanel->Collapse();
		CategoryPanelGroup->ScrollBy(0, -100);
	}
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Stacking1ImageClick(TObject *Sender)
{
	Label63Click(Label63);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Stacking2ImageClick(TObject *Sender)
{
	Label63Click(Label64);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Stacking3ImageClick(TObject *Sender)
{
	Label63Click(Label57);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Stacking4ImageClick(TObject *Sender)
{
	Label63Click(Label60);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Stacking5ImageClick(TObject *Sender)
{
	Label63Click(Label58);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Stacking6ImageClick(TObject *Sender)
{
	Label63Click(Label59);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Stacking7ImageClick(TObject *Sender)
{
	Label63Click(Label62);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Stacking8ImageClick(TObject *Sender)
{
	Label63Click(Label61);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Stacking9ImageClick(TObject *Sender)
{
	Label63Click(Label65);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Stacking10ImageClick(TObject *Sender)
{
	Label63Click(Label66);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Stacking11ImageClick(TObject *Sender)
{
	Label63Click(Label67);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::GainPanelCollapse(TObject *Sender)
{
	Timer1->Enabled=false;
	GainPointsPanel->Visible=true;
	GainItems->Channel=NULL;
	if(GainItems) GainItems->Visible=false;
	Timer1->Enabled=true;
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::GainPanelExpand(TObject *Sender)
{
	if(GainItems && Manager && ((TRadarMapManager*)Manager)->ReceivingSettings)
	{
		Timer1->Enabled=false;
		GainPointsPanel->Visible=false;
		GainItems->Channel=((TRadarMapManager*)Manager)->ReceivingSettings->SelectedChannel;
		GainItems->Visible=true;
		Timer1->Enabled=true;
	}
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Timer1Timer(TObject *Sender)
{
	int i, step=3;

	if(GainPointsPanel->Visible)
	{
		if(TopFakePanel->Height>17)
		{
			i=TopFakePanel->Height-17;
			if(i>step) i=step;
			BottomFakePanel->Height+=i;
			TopFakePanel->Height-=i;
		}
		else
		{
			GainPointsPanel->Visible=false;
			Timer1->Enabled=false;
		}
	}
	else
	{
		if(TopFakePanel->Height<34)
		{
			i=34-TopFakePanel->Height;
			if(i>step) i=step;
			BottomFakePanel->Height-=i;
			TopFakePanel->Height+=i;
		}
		else
		{
			GainPointsPanel->Visible=true;
			Timer1->Enabled=false;
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Label69Click(TObject *Sender)
{
	if(Manager && ((TRadarMapManager*)Manager)->ReceivingSettings &&
		((TRadarMapManager*)Manager)->ReceivingSettings->SelectedChannel)
	{
		((TRadarMapManager*)Manager)->ReceivingSettings->SelectedChannel->SelectedHPF=THPFState::HPFPreset;
		Adjust();
		FilterPanel2->Collapse();
		CategoryPanelGroup->ScrollBy(0, -100);
	}
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Label70Click(TObject *Sender)
{
	if(Manager && ((TRadarMapManager*)Manager)->ReceivingSettings)
	{
		//if(TempGain) ((TRadarMapManager*)Manager)->ReceivingSettings->SelectedChannel->Gain=TempGain;
		GainPanel->Collapse();
		Ch1GainPanel->Collapse();
		Ch2GainPanel->Collapse();
		CategoryPanelGroup->CollapseAll();
		CategoryPanelGroup->ScrollBy(0, -100);
		Ch1CategoryPanelGroup->CollapseAll();
		Ch1CategoryPanelGroup->ScrollBy(0, -100);
		Ch2CategoryPanelGroup->CollapseAll();
		Ch2CategoryPanelGroup->ScrollBy(0, -100);
		if(((TRadarMapManager*)Manager)->ReceivingSettings->SelectedChannelMode!=TChannelMode::Double)
		{
			((TRadarMapManager*)Manager)->ReceivingSettings->SelectedChannelMode = TChannelMode::Double;
			RebuildPanels();
			Adjust();
		}
		//TempGain=((TRadarMapManager*)Manager)->ReceivingSettings->SelectedChannel->Gain;
		//if(GainItems) ((TRadarMapManager*)Manager)->ReceivingSettings->SelectedChannel->Gain=GainItems->GainFunction;
		ChannelPanel->Collapse();
		CategoryPanelGroup->ScrollBy(0, -100);
	}
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Label90Click(TObject *Sender)
{
	//if(Manager && ((TRadarMapManager*)Manager)->ReceivingSettings &&
	//	((TRadarMapManager*)Manager)->ReceivingSettings->Channels &&
	//		((TRadarMapManager*)Manager)->ReceivingSettings->Channels[0])
	//{
	//	((TRadarMapManager*)Manager)->ReceivingSettings->Channels[0]->SelectedAntennaIndex=((TLabel*)Sender)->Tag;
	if(Channel)
	{
		Channel->SelectedAntennaIndex=((TLabel*)Sender)->Tag;
		Adjust();
		Ch1AntennaPanel->Collapse();
	}
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Image55Click(TObject *Sender)
{
	Label90Click(Label90);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Image57Click(TObject *Sender)
{
	Label90Click(Label92);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Image59Click(TObject *Sender)
{
	Label90Click(Label94);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Image61Click(TObject *Sender)
{
	Label90Click(Label96);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Image56Click(TObject *Sender)
{
	Label90Click(Label91);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Image58Click(TObject *Sender)
{
	Label90Click(Label93);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Image60Click(TObject *Sender)
{
	Label90Click(Label95);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Image62Click(TObject *Sender)
{
	//Label90Click(Label97);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Label82Click(TObject *Sender)
{
	//if(Manager && ((TRadarMapManager*)Manager)->ReceivingSettings &&
	//	((TRadarMapManager*)Manager)->ReceivingSettings->Channels &&
	//		((TRadarMapManager*)Manager)->ReceivingSettings->Channels[0])
	//{
	//	((TRadarMapManager*)Manager)->ReceivingSettings->Channels[0]->SelectedAntenna->TimeRange=((TLabel*)Sender)->Tag;
	if(Channel && Channel->SelectedAntenna)
	{
		Channel->SelectedAntenna->TimeRange=((TLabel*)Sender)->Tag;
		Adjust();
		Ch1TimePanel->Collapse();
	}
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Image47Click(TObject *Sender)
{
	Label82Click(Label82);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Image49Click(TObject *Sender)
{
	Label82Click(Label84);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Image43Click(TObject *Sender)
{
	Label82Click(Label78);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Image44Click(TObject *Sender)
{
	Label82Click(Label79);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Image46Click(TObject *Sender)
{
	Label82Click(Label81);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Image48Click(TObject *Sender)
{
	Label82Click(Label83);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Image50Click(TObject *Sender)
{
	Label82Click(Label85);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Image42Click(TObject *Sender)
{
	Label82Click(Label77);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Ch1PanelExpand(TObject *Sender)
{
	Ch2GainPanel->Collapse();
	Ch2Panel->Collapse();
	if(Manager && ((TRadarMapManager*)Manager)->ReceivingSettings &&
		((TRadarMapManager*)Manager)->ReceivingSettings->Channels)
		Channel=((TRadarMapManager*)Manager)->ReceivingSettings->Channels[0];
	else Channel=NULL;
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Ch2PanelExpand(TObject *Sender)
{
	Ch1GainPanel->Collapse();
	Ch1Panel->Collapse();
	CategoryPanelGroup->ScrollBy(0, -100);
	if(Manager && ((TRadarMapManager*)Manager)->ReceivingSettings &&
		((TRadarMapManager*)Manager)->ReceivingSettings->Channels)
		Channel=((TRadarMapManager*)Manager)->ReceivingSettings->Channels[1];
	else Channel=NULL;
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Ch2PanelCollapse(TObject *Sender)
{
	Ch1GainPanel->Collapse();
	Ch2GainPanel->Collapse();
	Channel=NULL;
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Image31Click(TObject *Sender)
{
	if(Channel)
	{
		Channel->SelectedHPF=THPFState::HPFStateOFF;
		Adjust();
		Ch1FilterPanel->Collapse();
		Ch2FilterPanel->Collapse();
		Ch1CategoryPanelGroup->ScrollBy(0, -100);
		Ch2CategoryPanelGroup->ScrollBy(0, -100);
	}
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Image32Click(TObject *Sender)
{
	if(Channel)
	{
		Channel->SelectedHPF=THPFState::Weak;
		Adjust();
		Ch1FilterPanel->Collapse();
		Ch2FilterPanel->Collapse();
		Ch1CategoryPanelGroup->ScrollBy(0, -100);
		Ch2CategoryPanelGroup->ScrollBy(0, -100);
	}
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Image33Click(TObject *Sender)
{
	if(Channel)
	{
		Channel->SelectedHPF=THPFState::Strong;
		Adjust();
		Ch1FilterPanel->Collapse();
		Ch2FilterPanel->Collapse();
		Ch1CategoryPanelGroup->ScrollBy(0, -100);
		Ch2CategoryPanelGroup->ScrollBy(0, -100);
	}
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Image34Click(TObject *Sender)
{
	if(Channel)
	{
		Channel->SelectedHPF=THPFState::SuperStrong;
		Adjust();
		Ch1FilterPanel->Collapse();
		Ch2FilterPanel->Collapse();
		Ch1CategoryPanelGroup->ScrollBy(0, -100);
		Ch2CategoryPanelGroup->ScrollBy(0, -100);
	}
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Image76Click(TObject *Sender)
{
	Label90Click(Label98);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Image78Click(TObject *Sender)
{
	Label90Click(Label100);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Image80Click(TObject *Sender)
{
	Label90Click(Label102);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Image82Click(TObject *Sender)
{
	Label90Click(Label104);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Image77Click(TObject *Sender)
{
	Label90Click(Label99);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Image79Click(TObject *Sender)
{
	Label90Click(Label101);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Image81Click(TObject *Sender)
{
	Label90Click(Label103);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Image83Click(TObject *Sender)
{
	//Label90Click(Label105);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Image72Click(TObject *Sender)
{
	Label82Click(Label86);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Image71Click(TObject *Sender)
{
	Label82Click(Label76);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Image73Click(TObject *Sender)
{
	Label82Click(Label87);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Image74Click(TObject *Sender)
{
	Label82Click(Label88);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Image75Click(TObject *Sender)
{
	Label82Click(Label89);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Image68Click(TObject *Sender)
{
	Label82Click(Label73);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Image67Click(TObject *Sender)
{
	Label82Click(Label72);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Image69Click(TObject *Sender)
{
	Label82Click(Label74);
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Image24Click(TObject *Sender)
{
	if(Channel)
	{
		Channel->PulseDelay-=20;
		Adjust();
	}
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Image25Click(TObject *Sender)
{
	if(Channel)
	{
		Channel->PulseDelay--;
		Adjust();
	}
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Image27Click(TObject *Sender)
{
	if(Channel)
	{
		Channel->PulseDelay++;
		Adjust();
	}
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Image26Click(TObject *Sender)
{
	if(Channel)
	{
		Channel->PulseDelay+=20;
		Adjust();
	}
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Image28Click(TObject *Sender)
{
	int z=((TComponent *)Sender)->Owner->ComponentCount;

	if(Channel && ((TComponent*)Sender)->Tag>=17000)
	{
		if(AutoDelayThrd)
		{
			AutoDelayThrd->AskForTerminate();
			WaitOthers();
			if(AutoDelayThrd->ForceTerminate()) delete AutoDelayThrd;
			AutoDelayThrd=NULL;
		}
		for(int i=0; i<z; i++)
		{
			if(((TComponent *)Sender)->Owner->Components[i]->ClassName()=="TImage")
			{
				((TImage*)((TComponent *)Sender)->Owner->Components[i])->Enabled=false;
			}
		}/**/
		Image10->Enabled=true;

		GainPanel->Collapse();
		Ch1GainPanel->Collapse();
		Ch2GainPanel->Collapse();

		CategoryPanelGroup->Enabled=false;
		Ch1CategoryPanelGroup->Enabled=false;
		Ch2CategoryPanelGroup->Enabled=false;

		Gain->Copy(Channel->Gain);
		Channel->Gain->GainPointsCount=3;
		Channel->Gain->GainPoints[0]=TGainPoint(0, 18);
		Channel->Gain->GainPoints[1]=TGainPoint(0.02, 18);
		Channel->Gain->GainPoints[2]=TGainPoint(1, 0);/**/
		if(((TComponent*)Sender)->Tag==17000)
			AutoDelayThrd=new TAutoDelayThread(Manager, Channel, SetupThrd, AfterAutoDelay, Ch1ShowDelay);
		else if(((TComponent*)Sender)->Tag==17001)
			AutoDelayThrd=new TAutoDelayThread(Manager, Channel, SetupThrd, AfterAutoDelay, Ch2ShowDelay);
		WaitPanel->Visible=true;
	}
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Ch1GainPanelExpand(TObject *Sender)
{
	if(GainItems && Channel)
	{
		////GainItems->GainFunction->Copy(Channel->Gain);
		////Channel->Gain=GainItems->GainFunction;
		Timer1->Enabled=false;
		GainPointsPanel->Visible=false;
		GainItems->Channel=Channel;
		GainItems->Visible=true;
		Timer1->Enabled=true;
	}
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Ch1GainPanelCollapse(TObject *Sender)
{
	if(GainItems && Channel)
	{
		Timer1->Enabled=false;
		GainPointsPanel->Visible=true;
		if(GainItems) GainItems->Visible=false;
		////Gains[0]->Copy(GainItems->GainFunction);
		GainItems->Channel=NULL;
		////Channel->Gain=Gains[0];
		Timer1->Enabled=true;
	}
}
//---------------------------------------------------------------------------

void __fastcall TSetupForm::Ch2GainPanelCollapse(TObject *Sender)
{
	if(GainItems && Channel)
	{
		Timer1->Enabled=false;
		GainPointsPanel->Visible=true;
		if(GainItems) GainItems->Visible=false;
		////Gains[1]->Copy(GainItems->GainFunction);
		GainItems->Channel=NULL;
		////Channel->Gain=Gains[1];
		Timer1->Enabled=true;
	}
}
//---------------------------------------------------------------------------


void __fastcall TSetupForm::Image8Click(TObject *Sender)
{
	Label18Click(Label106);
}
//---------------------------------------------------------------------------


