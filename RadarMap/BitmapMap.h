//---------------------------------------------------------------------------

#ifndef BitmapMapH
#define BitmapMapH
//---------------------------------------------------------------------------

#include "XMLMap.h"
#include "MyQueues.h"

class TBitmapMapsContainer: public TMapsContainer
{
private:
	TBitmap32* bmpgb;
	TBusyLocker *bmpGBLocker;
	TBitmap32* __fastcall readBmpGB();
	void __fastcall writeBmpGB(TBitmap32 *value);
protected:
	TBitmap32 *bmpUtil, *bmpInf;
	Types::TRect* rawpxrect;
	void __fastcall DrawXMLBitmap(TBitmap32 *Dst, TXMLMapBitmap *map);
	void __fastcall ApplyScaling(TBitmap32 *Src, TBitmap32 *Dst, TColor32 ClearColor=Color32(0,0,0,0));
	void __fastcall LatLonRectScaling(TDoubleRect *drct);
	__property TBitmap32 *bmpGB = {read=readBmpGB, write=writeBmpGB};
public:
	__fastcall TBitmapMapsContainer(TMyObject *AOwner, void* ASettings, TLockedDoubleRect *AViewportArea);
	__fastcall ~TBitmapMapsContainer();
	virtual void __fastcall RenderMap();
	virtual void __fastcall UtilityUpdate();
	virtual void __fastcall InfoUpdate();

	//real loaded image's bitmap size
	virtual Types::TRect __fastcall GetBitmapRect();

	__property Types::TRect* RawPxRect = {read=rawpxrect};
	__property TBitmap32* InitialBmp = {read=readBmpGB};
};

class TXMLMapsContainer: public TBitmapMapsContainer
{
private:
	void __fastcall FindMapsForTheme(TXMLMapTheme *Theme, AnsiString path, AnsiString Mask, TXMLMapBitmapType T);
protected:
public:
	__fastcall TXMLMapsContainer(TMyObject *AOwner, void* ASettings, TLockedDoubleRect *AViewportArea) : TBitmapMapsContainer(AOwner, ASettings, AViewportArea) {maptype=mtXML;}
	__fastcall ~TXMLMapsContainer() {}

	void __fastcall ZoomUpdate();
	void __fastcall ZoomUpdate(TMyObjectType Type) {Update(Type);};
	void __fastcall LoadData(AnsiString Filename, HANDLE AStopItEvent, bool WithUpdate, TCoordinateSystem CS, bool Preview=false);
	void __fastcall LoadData(AnsiString Filename, HANDLE AStopItEvent, bool WithUpdate, bool Preview=false) {TMapsContainer::LoadData(Filename, AStopItEvent, WithUpdate, Preview);}
	void __fastcall LoadData(AnsiString Filename, HANDLE AStopItEvent) {TMapsContainer::LoadData(Filename, AStopItEvent);}//LoadData(Filename, AStopItEvent, true);}
	void __fastcall GetLayers(TCheckBoxTreeView *TreeView, TTreeNode *Root);
	void __fastcall SetLayers(TCheckBoxTreeView *TreeView, TTreeNode *Root);
};

class TEmptyMapsContainer: public TBitmapMapsContainer
{
private:
#ifdef _RadarMap_Logo_BGND
	void __fastcall WatermarkLayerPaint(TObject *Sender, TBitmap32 *Bmp);
#endif
public:
	__fastcall TEmptyMapsContainer(TMyObject *AOwner, void* ASettings, TLockedDoubleRect *AViewportArea);
	__fastcall ~TEmptyMapsContainer() {}

	virtual void __fastcall RenderMap();
	void __fastcall UtilityUpdate() {}
	void __fastcall InfoUpdate() {}

	virtual Types::TRect __fastcall GetBitmapRect() {if(Background) return Types::TRect(0, 0, Background->Width, Background->Height); else return Types::TRect(0, 0, 0, 0);}

	void __fastcall GetLayers(TCheckBoxTreeView *TreeView, TTreeNode *Root);
	void __fastcall SetLayers(TCheckBoxTreeView *TreeView, TTreeNode *Root);
};

#endif
