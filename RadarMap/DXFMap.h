//---------------------------------------------------------------------------

#ifndef DXFMapH
#define DXFMapH
//---------------------------------------------------------------------------
#include "GPSCoordinate.h"
#include "DXF_Constants.h"
#include "VisualToolObjects.h"
#include "MyFiles.h"

enum TDXFObjectType {dotNone = 0, dotContainer = 1, dotEntity = 2, dotBlock = 3, dotLayer = 7};

class TDXFObject: public TObject
{
private:
protected:
	int color;
	__int64 handle;
	double textheightcoef;
	AnsiString name, description, comment, flags;
	TDXFObjectType objecttype;
	virtual TColor32 __fastcall readColorPX();
	void __fastcall writeDescription(AnsiString value) {};
	void __fastcall writeColorPX(TColor32 value);
public:
	__fastcall TDXFObject() {objecttype=dotNone; color=0; name=""; description=""; comment=""; flags="0"; textheightcoef=25; handle=0;}

	virtual bool __fastcall Import(TDXFObject *src);

	__property int Color = {read=color, write=color};
	__property TColor32 ColorPX = {read=readColorPX, write=writeColorPX};
	__property TDXFObjectType ObjectType = {read=objecttype};
	__property AnsiString Name = {read=name, write=name};
	__property AnsiString Description = {read=description, write=writeDescription};
	__property AnsiString Comment = {read=comment, write=comment};
	__property AnsiString Flags = {read=flags, write=flags};
	__property double TextHeightCoef = {read=textheightcoef, write=textheightcoef};
	__property __int64 Handle = {read=handle, write=handle};
};

struct TDXFGroup {
	int ID;
	AnsiString Value;

	TDXFGroup() {
		ID =-1;
		Value = "";
	}

	TDXFGroup(const TDXFGroup *pRight) {
		ID = pRight->ID;
		Value = pRight->Value;
	}
};
class TDXFObjectsList;

class TDXFGroupList : public TList
{
private:
protected:
public:
	__fastcall TDXFGroupList() : TList() {};
	__fastcall ~TDXFGroupList() { Clear(); };

	int __fastcall Add(TDXFGroup *o) { return TList::Add(o);}
	bool __fastcall Import(TDXFGroupList *src);
	void __fastcall Clear();
};

enum TDXFEntityType {detNone, detPolyLine, detLine, detCircle, detArc, detText,
	detInsert, detVertex, detEllipse, detPoint, detLwPolyLine, detSpline, detHatch};

class TDXFEntity : public TDXFObject
{
private:
protected:
	TDXFObject *owner;
	TDXFEntityType type;
	TDouble3DPoint xyz;
	//double z;
	TColor32 __fastcall readColorPX();
	AnsiString layername;
	TDXFGroupList *stringGroups;

	virtual void __fastcall writeOwner(TDXFObject *value) {owner=value;}
public:
	__fastcall TDXFEntity(TDXFObject *AOwner) {owner=AOwner; objecttype=dotEntity; xyz=TDouble3DPoint(); type=detNone; layername=""; stringGroups = NULL;} //z=0.0;}
	__fastcall ~TDXFEntity() { if (stringGroups) { delete stringGroups; } }

	virtual bool __fastcall Import(TDXFObject *src);

	__property TDXFEntityType Type = {read=type};
	__property TDouble3DPoint XYZ = {read=xyz, write=xyz};
	//__property double Z = {read=z, write=z};
	__property TDXFObject *Owner = {read=owner, write=writeOwner};
	__property AnsiString LayerName = {read=layername, write=layername};
	__property TDXFGroupList *StringGroups = {read = stringGroups, write = stringGroups};
};

class TDXFVertex : public TDXFEntity
{
private:
protected:
public:
	__fastcall TDXFVertex(TDXFObject *AOwner) : TDXFEntity(AOwner) {type=detVertex; description="VERTEX";}
	__fastcall ~TDXFVertex() {}

	bool __fastcall Import(TDXFObject *src);
};

class TDXFLine : public TDXFEntity
{
private:
	TDouble3DPoint point2;
protected:
public:
	__fastcall TDXFLine(TDXFObject *AOwner) : TDXFEntity(AOwner) {type=detLine; description="LINE";}
	__fastcall ~TDXFLine() {}

	bool __fastcall Import(TDXFObject *src);

	__property TDouble3DPoint Point2 = {read=point2};
};

const int CirclePoints = 10;
const int ArcPoints = 10;
const int EllipsePoints = 10;

class TDXFCircle : public TDXFEntity
{
private:
	float r;
protected:
public:
	__fastcall TDXFCircle(TDXFObject *AOwner) : TDXFEntity(AOwner) {type=detCircle; description="CIRCLE";}
	__fastcall ~TDXFCircle() {}

	bool __fastcall Import(TDXFObject *src);

	__property float R = {read=r, write=r};
};

class TDXFArc : public TDXFEntity
{
private:
	float r;
	float start, stop; //in Degrees;
protected:
public:
	__fastcall TDXFArc(TDXFObject *AOwner) : TDXFEntity(AOwner) {type=detArc; r=start=stop=0; description="ARC";}
	__fastcall ~TDXFArc() {}

	bool __fastcall Import(TDXFObject *src);

	__property float R = {read=r, write=r};
	__property float Start = {read=start, write=start};
	__property float Stop = {read=stop, write=stop};
};

class TDXFEllipse : public TDXFEntity
{
private:
	TDouble3DPoint mjaep;
	float mn2mj, start, stop; //in Radians;
protected:
public:
	__fastcall TDXFEllipse(TDXFObject *AOwner) : TDXFEntity(AOwner) {type=detEllipse; mjaep=TDouble3DPoint(); start=stop=0.0; mn2mj=1.0; description="ELLIPSE";}
	__fastcall ~TDXFEllipse() {}

	bool __fastcall Import(TDXFObject *src);

	__property TDouble3DPoint MjEndPoint = {read=mjaep, write=mjaep}; //Major Axis End Point
	__property float Mn2MjRatio = {read=mn2mj, write=mn2mj}; // Minor axis to Major axis Ratio
	__property float Start = {read=start, write=start};
	__property float Stop = {read=stop, write=stop};
};

class TDXFPoint : public TDXFEntity
{
private:
protected:
public:
	__fastcall TDXFPoint(TDXFObject *AOwner) : TDXFEntity(AOwner) {type=detPoint; description="POINT";}
	__fastcall ~TDXFPoint() {}

	bool __fastcall Import(TDXFObject *src);
};

enum TDXFHorizontalAlignment {dhaLeft=0, dhaCenter=1, dhaRight=2, dhaAligned=3, dhaMiddle=4, dhaFit=5};
enum TDXFVerticalAlignment {dvaBaseline=0, dvaBottom=1, dvaMiddle=2, dvaTop=3};

class TDXFText : public TDXFEntity
{
private:
	AnsiString text, style;
	float height; //*5
	bool enlargedrawingheight;
	float angle; //degrees
	TDouble3DPoint alignmentpoint;
	TDXFHorizontalAlignment hora;
	TDXFVerticalAlignment vera;
protected:
	float __fastcall readHeight() {return height+0.5*enlargedrawingheight*height;}
	int __fastcall readHeightPX() {if(Owner) return (int)(Owner->TextHeightCoef*Height+0.5); else return (int)(25.*Height+0.5);}
public:
	__fastcall TDXFText(TDXFObject *AOwner) : TDXFEntity(AOwner) {type=detText; style="STANDARD"; text=""; height=2; enlargedrawingheight=false; angle=0; alignmentpoint=TDouble3DPoint(); hora=dhaLeft; vera=dvaBaseline; description="TEXT";}
	__fastcall ~TDXFText() {}

	bool __fastcall Import(TDXFObject *src);

	__property AnsiString Text = {read=text, write=text};
	__property AnsiString Style = {read=style, write=style};
	__property float Height = {read=readHeight, write=height};
	__property bool EnlargeDrawingHeight = {read=enlargedrawingheight, write=enlargedrawingheight};
	__property int HeightPX = {read=readHeightPX};
	__property float Angle = {read=angle, write=angle};
	__property TDouble3DPoint AlignmentPoint = {read=alignmentpoint};
	__property TDXFHorizontalAlignment HorA = {read=hora, write=hora};
	__property TDXFVerticalAlignment VerA = {read=vera, write=vera};
};

class TDXFBlock;

class TDXFInsert : public TDXFEntity
{
private:
	float x_scale, y_scale, z_scale;
	float angle; //degrees
	TDXFBlock *block;
	AnsiString blockname;
public:
	__fastcall TDXFInsert(TDXFObject *AOwner) : TDXFEntity(AOwner) {type=detInsert; block=NULL; blockname=""; x_scale=y_scale=z_scale=1; description="INSERT";}
	__fastcall ~TDXFInsert() {}

	bool __fastcall Import(TDXFObject *src);

	__property TDXFBlock *Block = {read=block, write=block};
	__property AnsiString BlockName = {read=blockname, write=blockname};
	__property float X_scale = {read = x_scale, write = x_scale};
	__property float Y_scale = {read = y_scale, write = y_scale};
	__property float Z_scale = {read = z_scale, write = z_scale};
	__property float Angle = {read = angle, write = angle};
};

class TDXFObjectsList : public TObject
{
private:
	TList *Objects;
	TDXFObject* LastFound;
protected:
	int __fastcall readObjectsCount() {return Objects->Count;}
	virtual TDXFObject* __fastcall readMyObjectItem(int Index) {if(Index>=0 && Index<Objects->Count) return (TDXFObject*)Objects->Items[Index]; else return NULL;}
	void __fastcall writeMyObjectItem(int Index, TDXFObject* value) {if(Index>=0 && Index<Objects->Count) Objects->Items[Index]=(TObject*)value;}
public:
	__fastcall TDXFObjectsList() {Objects=new TList(); LastFound=NULL;}
	__fastcall ~TDXFObjectsList();

	int __fastcall Add(TDXFObject* o) {int i=-1; if(o) i=Objects->Add(o); return i;}
	void __fastcall Delete(int Index);
	void __fastcall Clear();
	TDXFObject* __fastcall GetItemByName(AnsiString name);
	void __fastcall DeleteEmty();
	TDXFObject* __fastcall GetLastFound() {return LastFound;}
	void __fastcall SetLastFound(TDXFObject* item) {LastFound=item;}

	__property TDXFObject* Items[int Index] = {read=readMyObjectItem, write=writeMyObjectItem};
	__property int Count = {read=readObjectsCount};
};

class TDXFObjectsContainer : public TDXFObject
{
private:
protected:
	TDXFObjectsList *list;
	__int64 *handle_index;
	TDouble3DPoint *maxcorner, *mincorner;
	void __fastcall writeDescription(AnsiString value) {description=value;}
	void __fastcall writeEnlargeDrawingTextHeight(bool value);
public:
	__fastcall TDXFObjectsContainer(__int64 *HIndex, AnsiString AName) : TDXFObject() {objecttype=dotContainer; list=new TDXFObjectsList(); handle_index=HIndex; name=AName; maxcorner=NULL; mincorner=NULL;}
	__fastcall ~TDXFObjectsContainer() {delete list; if(maxcorner) delete maxcorner; if(mincorner) delete mincorner;}

	virtual void __fastcall Add(TDXFEntity *o);
	bool __fastcall Render(TBitmap32 *Dst, TColor32 bgColor, int InvertColor, TDoubleRect rct, TDouble3DPoint SourcePoint, double Scale_x, double Scale_y, double angle); //angle in degrees
	bool __fastcall Render(TBitmap32 *Dst, TColor32 bgColor, int InvertColor, TDoubleRect rct) {TDouble3DPoint p; p=TDouble3DPoint(); return Render(Dst, bgColor, InvertColor, rct, p, 1., 1., 0.);}
	bool __fastcall Import(TDXFObject *src);

	void __fastcall ApplyMaxCorner(TDouble3DPoint ddp) {if(!maxcorner) maxcorner=new TDouble3DPoint(); (*maxcorner)=ddp;}
	void __fastcall ApplyMinCorner(TDouble3DPoint ddp) {if(!mincorner) mincorner=new TDouble3DPoint(); (*mincorner)=ddp;}

	__property TDXFObjectsList *List = {read=list};
	__property __int64 *Handle_Index = {read=handle_index, write=handle_index};
	__property bool EnlargeDrawingTextHeight = {write=writeEnlargeDrawingTextHeight};
	__property TDouble3DPoint *MaxCorner = {read = maxcorner};
	__property TDouble3DPoint *MinCorner = {read = mincorner};
};

class TDXFBlock : public TDXFObjectsContainer
{
private:
protected:
public:
	__fastcall TDXFBlock(__int64 *HIndex, AnsiString AName) : TDXFObjectsContainer(HIndex, AName) {objecttype=dotBlock;}
	__fastcall ~TDXFBlock() {}

	bool __fastcall Import(TDXFObject *src);

	void __fastcall Add(TDXFEntity *o);
};

enum TDXFLayerType {dltNone=0, dltTopography=1, dltUtility=2, dltAnnotation=3, dltAutoCAD=4};

class TDXFLayer : public TDXFObjectsContainer
{
private:
	TBitmap32 *bitmap;
	bool visible;//, utility;
	TDXFLayerType layertype;
	int idDbRecord;
protected:
	bool __fastcall readUtility() {return (layertype==dltUtility);}
public:
	__fastcall TDXFLayer(__int64 *HIndex, AnsiString AName) : TDXFObjectsContainer(HIndex, AName) {objecttype=dotLayer; bitmap=NULL; visible=true; layertype=dltAutoCAD;}//utility=false;}
	__fastcall ~TDXFLayer() {}

	bool __fastcall Import(TDXFObject *src);
	void __fastcall ChangeLayerType(TDXFLayerType tt) {if(layertype!=tt) layertype=tt;}

	__property TBitmap32 *Bitmap = {read=bitmap};
	__property bool Visible = {read=visible, write=visible};
	__property bool Utility = {read=readUtility};//{read=utility, write=utility};
	__property TDXFLayerType LayerType = {read=layertype};
	__property int IdDbRecord = {read = idDbRecord, write = idDbRecord};
};

class TDXFPolyLine : public TDXFEntity
{
private:
	TDXFObjectsContainer *vertexes;
	bool closed, polyline3D;
	float thickness;
protected:
	int __fastcall readObjectsCount() {return vertexes->List->Count;}
	TDXFVertex* __fastcall readMyObjectItem(int Index) {if(Index>=0 && Index<vertexes->List->Count) return (TDXFVertex*)vertexes->List->Items[Index]; else return NULL;}
	void __fastcall writeMyObjectItem(int Index, TDXFVertex* value) {if(Index>=0 && Index<vertexes->List->Count) vertexes->List->Items[Index]=(TDXFObject*)value;}
	void __fastcall writeOwner(TDXFObject *value);
public:
	__fastcall TDXFPolyLine(TDXFObject *AOwner);
	__fastcall ~TDXFPolyLine() {delete vertexes;}

	void __fastcall AddVertex(TDXFVertex *v) {vertexes->Add(v);}
	bool __fastcall Import(TDXFObject *src);

	__property TDXFObjectsContainer *Container = {read=vertexes};
	__property bool Closed = {read=closed, write=closed};
	__property bool Polyline3D = {read=polyline3D, write=polyline3D};
	__property float Thickness3D = {read=thickness, write=thickness};
	__property TDXFVertex* Vertexes[int Index] = {read=readMyObjectItem, write=writeMyObjectItem};
	__property TDXFObjectsContainer *VertexesContainer = {read=vertexes};
	__property int Count = {read=readObjectsCount};
};

class TDXFLwPolyLine : public TDXFEntity
{
private:
	TList *vertexes;
	bool closed, plinegen;
	float thickness, width;
	int addedX_cnt, addedY_cnt;
protected:
	int __fastcall readObjectsCount() {return vertexes->Count;}
	TDoublePoint* __fastcall readMyObjectItem(int Index) {if(Index>=0 && Index<vertexes->Count) return (TDoublePoint*)vertexes->Items[Index]; else return NULL;}
	void __fastcall writeMyObjectItem(int Index, TDoublePoint* value) {if(Index>=0 && Index<vertexes->Count) vertexes->Items[Index]=(TDoublePoint*)value;}
public:
	__fastcall TDXFLwPolyLine(TDXFObject *AOwner);
	__fastcall ~TDXFLwPolyLine() {for(int i=0; i<vertexes->Count; i++) delete vertexes->Items[i]; vertexes->Clear(); delete vertexes;}

	void __fastcall AddedVertexX();
	void __fastcall AddedVertexY();
	void __fastcall AddVertex(TDoublePoint *v) {vertexes->Add((void*)v);}
	bool __fastcall Import(TDXFObject *src);

	__property bool Closed = {read=closed, write=closed};
	__property bool Plinegen = {read=plinegen, write=plinegen};
	__property float Thickness3D = {read=thickness, write=thickness};
	__property TDoublePoint* Vertexes[int Index] = {read=readMyObjectItem, write=writeMyObjectItem};
	__property int Count = {read=readObjectsCount};
	__property float Width = {read=width, write=width};
};

class TDXFSpline;
/*class TDXFSpline : public TDXFEntity
{
private:
	TList *vertexes;
	bool closed, periodic, rational, planar, linear; // Flags
	float thickness, width;
	int addedX_cnt, addedY_cnt;
	int degree;
protected:
	int __fastcall readObjectsCount() {return vertexes->Count;}
	TDoublePoint* __fastcall readMyObjectItem(int Index) {if(Index>=0 && Index<vertexes->Count) return (TDoublePoint*)vertexes->Items[Index]; else return NULL;}
	void __fastcall writeMyObjectItem(int Index, TDoublePoint* value) {if(Index>=0 && Index<vertexes->Count) vertexes->Items[Index]=(TDoublePoint*)value;}
public:
	__fastcall TDXFLwPolyLine(TDXFObject *AOwner);
	__fastcall ~TDXFLwPolyLine() {delete vertexes;}

	void __fastcall AddedVertexX();
	void __fastcall AddedVertexY();
	void __fastcall AddVertex(TDoublePoint *v) {vertexes->Add((void*)v);}
	bool __fastcall Import(TDXFObject *src);

	__property bool Closed = {read=closed, write=closed};
	__property bool Plinegen = {read=plinegen, write=plinegen};
	__property float Thickness3D = {read=thickness, write=thickness};
	__property TDoublePoint* Vertexes[int Index] = {read=readMyObjectItem, write=writeMyObjectItem};
	__property int Count = {read=readObjectsCount};
	__property float Width = {read=width, write=width};
};*/

class TDXFHatch;// : public TDXFEntity

const AnsiString EmptyLayerName = "RadarMapEmptyLayer";

class TDXFCollection : public TObject
{
private:
	AnsiString FileName;
	TDXFObjectsList *layers, *blocks;
	AnsiString autocadversion;
	TDoublePoint lowercorner, uppercorner;
	TDouble3DPoint *maxcorner, *mincorner;
	TStrings* comments;
	__int64 handle_index;
	int idDbRecord;
	TToolProgressBar *progress;
	bool enlargedrawingtextheight;
	bool sqlsource;
	bool __fastcall ReadNextGroup(TBufferedROFile *cFile, TDXFGroup &Out, HANDLE AStopItEvent=NULL);
	void __fastcall ReadHeader(TBufferedROFile *cFile, HANDLE AStopItEvent=NULL);
	void __fastcall ReadTables(TBufferedROFile *cFile, HANDLE AStopItEvent=NULL);
	bool __fastcall ReadEntity(TBufferedROFile *cFile, TDXFGroup &gr, TDXFObjectsContainer *Container, TDXFEntityType et, bool ReadFirst, HANDLE AStopItEvent=NULL);
	bool __fastcall LookForEntities(TBufferedROFile *cFile, TDXFGroup &gr, TDXFObjectsContainer *Container, HANDLE AStopItEvent=NULL);
	void __fastcall ReadBlocks(TBufferedROFile *cFile, HANDLE AStopItEvent=NULL);
	void __fastcall ReadEntities(TBufferedROFile *cFile, HANDLE AStopItEvent=NULL);
	float __fastcall readAutoCadVersionNum();
	void __fastcall WriteHeader(TStrings *strs);
	void __fastcall WriteTables(TStrings *strs);
	void __fastcall WriteEntities(TStrings *strs, TDXFObjectsContainer *Container);
	void __fastcall WriteBlocks(TStrings *strs);
protected:
	void __fastcall writeEnlargeDrawingTextHeight(bool value);
public:
	__fastcall TDXFCollection();
	__fastcall ~TDXFCollection();

	void __fastcall Clear() {blocks->Clear(); layers->Clear();}

	TDXFLayer* __fastcall GetLayerByName(AnsiString name, bool CreateIfNotExists=false);
	TDXFLayer* __fastcall GetLayerByDbId(int LayerID);
	TDXFBlock* __fastcall GetBlockByName(AnsiString name) {return (TDXFBlock*)blocks->GetItemByName(name);}
	bool __fastcall AddLayer(TDXFLayer *l, bool ignoreWithEqualsName = true);//bool __fastcall AddLayer(TDXFLayer *l);
	bool __fastcall AddBlock(TDXFBlock *b);

	bool __fastcall LoadData(AnsiString filename, HANDLE AStopItEvent, bool aPreview);
	bool __fastcall SaveData(AnsiString filename);

	bool __fastcall Import(TDXFCollection *In);

	bool __fastcall Render(TBitmap32 *Dst, TDoubleRect rct) {return true;} //for(int i=0; i<layers->Count; i++) {((TDXFLayer*)layers->Items[i])->Render(); bitmap.Draw(layers->Bitmap);}

	void __fastcall ApplyMaxCorner(TDouble3DPoint ddp) {if(!maxcorner) maxcorner=new TDouble3DPoint(ddp); else (*maxcorner)=ddp;}
	void __fastcall ApplyMinCorner(TDouble3DPoint ddp) {if(!mincorner) mincorner=new TDouble3DPoint(ddp); else (*mincorner)=ddp;}

	__property AnsiString DXFFileName = {read=FileName, write = FileName};
	__property TDXFObjectsList *Layers = {read=layers};
	__property TDXFObjectsList *Blocks = {read=blocks};
	__property AnsiString AutoCadVersion = {read = autocadversion};
	__property float AutoCadVersionNum = {read = readAutoCadVersionNum};
	__property TDoublePoint LowerCorner = {read = lowercorner};
	__property TDoublePoint UpperCorner = {read = uppercorner};
	__property TDouble3DPoint *MaxCorner = {read = maxcorner};
	__property TDouble3DPoint *MinCorner = {read = mincorner};
	__property TStrings* Comments = {read = comments};
	__property TToolProgressBar *Progress = {write=progress};
	__property __int64 Handle_index = {read = handle_index};
	__property int IdDbRecord = {read = idDbRecord, write = idDbRecord};
	__property bool EnlargeDrawingTextHeight = {read=enlargedrawingtextheight, write=writeEnlargeDrawingTextHeight};
};

#endif
