// CodeGear C++Builder
// Copyright (c) 1995, 2009 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'Gr32_png.pas' rev: 21.00

#ifndef Gr32_pngHPP
#define Gr32_pngHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member functions
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <Sysinit.hpp>	// Pascal unit
#include <Sysutils.hpp>	// Pascal unit
#include <Windows.hpp>	// Pascal unit
#include <Classes.hpp>	// Pascal unit
#include <Gr32.hpp>	// Pascal unit
#include <Graphics.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Gr32_png
{
//-- type declarations -------------------------------------------------------
//-- var, const, procedure ---------------------------------------------------
extern PACKAGE void __fastcall LoadPNGintoBitmap32(Gr32::TBitmap32* DstBitmap, Classes::TStream* SrcStream, /* out */ bool &AlphaChannelUsed)/* overload */;
extern PACKAGE void __fastcall LoadPNGintoBitmap32(Gr32::TBitmap32* DstBitmap, System::UnicodeString Filename, /* out */ bool &AlphaChannelUsed)/* overload */;

}	/* namespace Gr32_png */
using namespace Gr32_png;
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// Gr32_pngHPP
