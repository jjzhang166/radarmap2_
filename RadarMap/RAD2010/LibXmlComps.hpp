// CodeGear C++Builder
// Copyright (c) 1995, 2009 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'Libxmlcomps.pas' rev: 21.00

#ifndef LibxmlcompsHPP
#define LibxmlcompsHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member functions
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <Sysinit.hpp>	// Pascal unit
#include <Classes.hpp>	// Pascal unit
#include <Libxmlparser.hpp>	// Pascal unit
#include <Sysutils.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Libxmlcomps
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TXmlScanner;
class PASCALIMPLEMENTATION TXmlScanner : public Libxmlparser::TCustomXmlScanner
{
	typedef Libxmlparser::TCustomXmlScanner inherited;
	
public:
	__property XmlParser;
	__property StopParser;
	
__published:
	__property Filename;
	__property Normalize;
	__property OnXmlProlog;
	__property OnComment;
	__property OnPI;
	__property OnDtdRead;
	__property OnStartTag;
	__property OnEmptyTag;
	__property OnEndTag;
	__property OnContent;
	__property OnCData;
	__property OnElement;
	__property OnAttList;
	__property OnEntity;
	__property OnNotation;
	__property OnDtdError;
	__property OnLoadExternal;
	__property OnTranslateEncoding;
	__property OnTranslateCharacter;
public:
	/* TCustomXmlScanner.Create */ inline __fastcall virtual TXmlScanner(Classes::TComponent* AOwner) : Libxmlparser::TCustomXmlScanner(AOwner) { }
	/* TCustomXmlScanner.Destroy */ inline __fastcall virtual ~TXmlScanner(void) { }
	
};


class DELPHICLASS TEasyXmlScanner;
class PASCALIMPLEMENTATION TEasyXmlScanner : public Libxmlparser::TCustomXmlScanner
{
	typedef Libxmlparser::TCustomXmlScanner inherited;
	
protected:
	virtual void __fastcall WhenCData(System::UnicodeString Content);
	
public:
	__property XmlParser;
	__property StopParser;
	
__published:
	__property Filename;
	__property Normalize;
	__property OnComment;
	__property OnPI;
	__property OnStartTag;
	__property OnEmptyTag;
	__property OnEndTag;
	__property OnContent;
	__property OnLoadExternal;
	__property OnTranslateEncoding;
public:
	/* TCustomXmlScanner.Create */ inline __fastcall virtual TEasyXmlScanner(Classes::TComponent* AOwner) : Libxmlparser::TCustomXmlScanner(AOwner) { }
	/* TCustomXmlScanner.Destroy */ inline __fastcall virtual ~TEasyXmlScanner(void) { }
	
};


//-- var, const, procedure ---------------------------------------------------
extern PACKAGE void __fastcall Register(void);

}	/* namespace Libxmlcomps */
using namespace Libxmlcomps;
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// LibxmlcompsHPP
