// CodeGear C++Builder
// Copyright (c) 1995, 2009 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'Gr32_misc.pas' rev: 21.00

#ifndef Gr32_miscHPP
#define Gr32_miscHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member functions
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <Sysinit.hpp>	// Pascal unit
#include <Windows.hpp>	// Pascal unit
#include <Types.hpp>	// Pascal unit
#include <Classes.hpp>	// Pascal unit
#include <Sysutils.hpp>	// Pascal unit
#include <Math.hpp>	// Pascal unit
#include <Gr32.hpp>	// Pascal unit
#include <Gr32_lowlevel.hpp>	// Pascal unit
#include <Gr32_blend.hpp>	// Pascal unit
#include <Gr32_transforms.hpp>	// Pascal unit
#include <Gr32_math.hpp>	// Pascal unit
#include <Gr32_polygons.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Gr32_misc
{
//-- type declarations -------------------------------------------------------
#pragma option push -b-
enum TBalloonPos { bpNone, bpTopLeft, bpTopRight, bpBottomLeft, bpBottomRight };
#pragma option pop

#pragma option push -b-
enum TCorner { cTopLeft, cTopRight, cBottomLeft, cBottomRight };
#pragma option pop

typedef DynamicArray<DynamicArray<DynamicArray<Gr32::TFixedPoint> > > TArrayOfArrayOfArrayOfFixedPoint;

//-- var, const, procedure ---------------------------------------------------
static const ShortInt MAXIMUM_SHADOW_FADE = 0x0;
static const ShortInt MEDIUM_SHADOW_FADE = 0x5;
static const ShortInt MINIMUM_SHADOW_FADE = 0xa;
static const ShortInt NO_SHADOW_FADE = 0xb;
static const Extended rad01 = 1.745329E-02;
static const Extended rad30 = 5.235988E-01;
static const Extended rad45 = 7.853982E-01;
static const Extended rad60 = 1.047198E+00;
static const Extended rad90 = 1.570796E+00;
static const Extended rad180 = 3.141593E+00;
static const Extended rad270 = 4.712389E+00;
static const Extended rad360 = 6.283185E+00;
static const Extended DegToRad = 1.745329E-02;
static const Extended RadToDeg = 5.729578E+01;
#define SingleLineLimit  (1.500000E+00)
static const Extended SqrtTwo = 1.414214E+00;
#define half  (5.000000E-01)
#define cbezier_tolerance  (5.000000E-01)
#define qbezier_tolerance  (5.000000E-01)
extern PACKAGE void __fastcall OffsetPoint(Gr32::TFloatPoint &pt, float dx, float dy)/* overload */;
extern PACKAGE void __fastcall OffsetPoint(Gr32::TFixedPoint &pt, float dx, float dy)/* overload */;
extern PACKAGE void __fastcall OffsetPoints(Gr32::TArrayOfFixedPoint &pts, float dx, float dy)/* overload */;
extern PACKAGE void __fastcall OffsetPoints(Gr32::TArrayOfFloatPoint &pts, float dx, float dy)/* overload */;
extern PACKAGE void __fastcall OffsetPolyPoints(Gr32::TArrayOfArrayOfFixedPoint &polyPts, float dx, float dy);
extern PACKAGE void __fastcall OffsetPolyPolyPoints(TArrayOfArrayOfArrayOfFixedPoint &polyPolyPts, float dx, float dy);
extern PACKAGE Gr32::TArrayOfArrayOfFixedPoint __fastcall CopyPolyPoints(const Gr32::TArrayOfArrayOfFixedPoint polyPts);
extern PACKAGE Gr32::TArrayOfFixedPoint __fastcall ReversePoints(const Gr32::TArrayOfFixedPoint pts)/* overload */;
extern PACKAGE Gr32::TArrayOfFloatPoint __fastcall ReversePoints(const Gr32::TArrayOfFloatPoint pts)/* overload */;
extern PACKAGE void __fastcall ConcatenatePoints(Gr32::TArrayOfFixedPoint &pts, const Gr32::TArrayOfFixedPoint extras)/* overload */;
extern PACKAGE void __fastcall ConcatenatePoints(Gr32::TArrayOfFixedPoint &pts, Gr32::TFixedPoint const *extras, const int extras_Size)/* overload */;
extern PACKAGE void __fastcall ConcatenatePolyPoints(Gr32::TArrayOfArrayOfFixedPoint &polyPts, const Gr32::TArrayOfArrayOfFixedPoint extras);
extern PACKAGE Gr32::TArrayOfFloatPoint __fastcall AFixedToAFloat(Gr32::TArrayOfFixedPoint pts);
extern PACKAGE Gr32::TArrayOfArrayOfFloatPoint __fastcall AAFixedToAAFloat(Gr32::TArrayOfArrayOfFixedPoint ppts);
extern PACKAGE Gr32::TArrayOfFixedPoint __fastcall AFloatToAFixed(Gr32::TArrayOfFloatPoint pts);
extern PACKAGE Gr32::TArrayOfArrayOfFixedPoint __fastcall AAFloatToAAFixed(Gr32::TArrayOfArrayOfFloatPoint ppts);
extern PACKAGE Gr32::TArrayOfFixedPoint __fastcall MakeArrayOfFixedPoints(float const *a, const int a_Size)/* overload */;
extern PACKAGE Gr32::TArrayOfFixedPoint __fastcall MakeArrayOfFixedPoints(const Types::TRect &rec)/* overload */;
extern PACKAGE Gr32::TArrayOfFixedPoint __fastcall MakeArrayOfFixedPoints(const Gr32::TFloatRect &rec)/* overload */;
extern PACKAGE Gr32::TArrayOfFixedPoint __fastcall MakeArrayOfFixedPoints(const Gr32::TFixedRect &rec)/* overload */;
extern PACKAGE Gr32::TArrayOfFixedPoint __fastcall MakeArrayOfFixedPoints(const Gr32::TArrayOfFloatPoint a)/* overload */;
extern PACKAGE Gr32::TArrayOfFloatPoint __fastcall MakeArrayOfFloatPoints(float const *a, const int a_Size)/* overload */;
extern PACKAGE Gr32::TArrayOfFloatPoint __fastcall MakeArrayOfFloatPoints(const Gr32::TArrayOfFixedPoint a)/* overload */;
extern PACKAGE Gr32::TArrayOfArrayOfFloatPoint __fastcall MakeArrayOfArrayOfFloatPoints(const Gr32::TArrayOfArrayOfFixedPoint a);
extern PACKAGE Gr32::TArrayOfFloat __fastcall MakeArrayOfFloat(float const *a, const int a_Size);
extern PACKAGE void __fastcall OffsetFixedRect(Gr32::TFixedRect &rec, float dx, float dy);
extern PACKAGE void __fastcall OffsetFloatRect(Gr32::TFloatRect &rec, float dx, float dy);
extern PACKAGE bool __fastcall IsDuplicatePoint(const Gr32::TFixedPoint &p1, const Gr32::TFixedPoint &p2)/* overload */;
extern PACKAGE bool __fastcall IsDuplicatePoint(const Gr32::TFloatPoint &p1, const Gr32::TFloatPoint &p2)/* overload */;
extern PACKAGE bool __fastcall IsDuplicateRect(const Gr32::TFloatRect &rec1, const Gr32::TFloatRect &rec2);
extern PACKAGE Gr32::TArrayOfFixedPoint __fastcall StripDuplicatePoints(Gr32::TFixedPoint const *pts, const int pts_Size)/* overload */;
extern PACKAGE Gr32::TArrayOfFixedPoint __fastcall StripDuplicatePoints(const Gr32::TArrayOfFixedPoint pts)/* overload */;
extern PACKAGE Gr32::TFixedPoint __fastcall GetPointAtAngleFromPoint(const Gr32::TFixedPoint &pt, const float dist, const float radians);
extern PACKAGE float __fastcall SquaredDistBetweenPoints(const Gr32::TFixedPoint &pt1, const Gr32::TFixedPoint &pt2)/* overload */;
extern PACKAGE float __fastcall SquaredDistBetweenPoints(const Gr32::TFloatPoint &pt1, const Gr32::TFloatPoint &pt2)/* overload */;
extern PACKAGE float __fastcall DistBetweenPoints(const Gr32::TFixedPoint &pt1, const Gr32::TFixedPoint &pt2)/* overload */;
extern PACKAGE float __fastcall DistBetweenPoints(const Gr32::TFloatPoint &pt1, const Gr32::TFloatPoint &pt2)/* overload */;
extern PACKAGE Gr32::TFloatPoint __fastcall ClosestPointOnLine(const Gr32::TFloatPoint &pt, const Gr32::TFloatPoint &lnA, const Gr32::TFloatPoint &lnB, bool ForceResultBetweenLinePts);
extern PACKAGE float __fastcall DistOfPointFromLine(const Gr32::TFloatPoint &pt, const Gr32::TFloatPoint &lnA, const Gr32::TFloatPoint &lnB, bool ForceResultBetweenLinePts)/* overload */;
extern PACKAGE float __fastcall DistOfPointFromLine(const Gr32::TFixedPoint &pt, const Gr32::TFixedPoint &lnA, const Gr32::TFixedPoint &lnB, bool ForceResultBetweenLinePts)/* overload */;
extern PACKAGE bool __fastcall IntersectionPoint(const Gr32::TFixedPoint &ln1A, const Gr32::TFixedPoint &ln1B, const Gr32::TFixedPoint &ln2A, const Gr32::TFixedPoint &ln2B, /* out */ Gr32::TFixedPoint &IntersectPoint);
extern PACKAGE Gr32::TFixedPoint __fastcall RotatePoint(const Gr32::TFixedPoint &pt, const Gr32::TFixedPoint &origin, const float radians);
extern PACKAGE Gr32::TArrayOfFixedPoint __fastcall RotatePoints(Gr32::TFixedPoint const *pts, const int pts_Size, const Gr32::TFixedPoint &origin, float radians)/* overload */;
extern PACKAGE Gr32::TArrayOfFixedPoint __fastcall RotatePoints(Gr32::TFixedPoint const *pts, const int pts_Size, float radians)/* overload */;
extern PACKAGE void __fastcall RotatePolyPoints(Gr32::TArrayOfArrayOfFixedPoint &pts, const Gr32::TFixedPoint &origin, float radians);
extern PACKAGE Gr32::TFixedPoint __fastcall MidPoint(const Gr32::TFixedPoint &pt1, const Gr32::TFixedPoint &pt2);
extern PACKAGE float __fastcall GetAngleOfPt2FromPt1(const Gr32::TFixedPoint &pt1, const Gr32::TFixedPoint &pt2)/* overload */;
extern PACKAGE float __fastcall GetAngleOfPt2FromPt1(const Gr32::TFloatPoint &pt1, const Gr32::TFloatPoint &pt2)/* overload */;
extern PACKAGE bool __fastcall PtInPolygon(const Gr32::TFixedPoint &Pt, Gr32::TFixedPoint const *Pts, const int Pts_Size)/* overload */;
extern PACKAGE bool __fastcall PtInPolygon(const Gr32::TFloatPoint &pt, const Gr32::TArrayOfFloatPoint Pts)/* overload */;
extern PACKAGE bool __fastcall FixedPtInRect(const Gr32::TFixedRect &R, const Gr32::TFixedPoint &P);
extern PACKAGE Types::TPoint __fastcall MakePoint(const Gr32::TFixedPoint &pt)/* overload */;
extern PACKAGE Types::TPoint __fastcall MakePoint(const Gr32::TFloatPoint &pt)/* overload */;
extern PACKAGE Gr32::TArrayOfFloatPoint __fastcall FloatPoints(const Gr32::TArrayOfFixedPoint pts)/* overload */;
extern PACKAGE Gr32::TArrayOfFixedPoint __fastcall FixedPoints(const Gr32::TArrayOfFloatPoint pts)/* overload */;
extern PACKAGE Gr32::TArrayOfArrayOfFloatPoint __fastcall FloatPoints(const Gr32::TArrayOfArrayOfFixedPoint ppts)/* overload */;
extern PACKAGE Gr32::TArrayOfArrayOfFixedPoint __fastcall FixedPoints(const Gr32::TArrayOfArrayOfFloatPoint ppts)/* overload */;
extern PACKAGE Gr32::TArrayOfFixedPoint __fastcall CopyPoints(const Gr32::TArrayOfFixedPoint pts);
extern PACKAGE Types::TRect __fastcall GetBoundsRect(Gr32::TFixedPoint *pts, const int pts_Size);
extern PACKAGE Gr32::TFixedRect __fastcall GetBoundsFixedRect(Gr32::TFixedPoint *pts, const int pts_Size)/* overload */;
extern PACKAGE Gr32::TFixedRect __fastcall GetBoundsFixedRect(Gr32::TArrayOfArrayOfFixedPoint polyPts)/* overload */;
extern PACKAGE Gr32::TFloatRect __fastcall GetBoundsFloatRect(Gr32::TFixedPoint *pts, const int pts_Size)/* overload */;
extern PACKAGE Gr32::TFloatRect __fastcall GetBoundsFloatRect(Gr32::TFloatPoint *pts, const int pts_Size)/* overload */;
extern PACKAGE Gr32::TFixedRect __fastcall GetRectUnion(const Gr32::TFixedRect &rec1, const Gr32::TFixedRect &rec2);
extern PACKAGE Gr32::TArrayOfFixedPoint __fastcall GetConvexHull(const Gr32::TArrayOfFixedPoint polygon);
extern PACKAGE Gr32::TBitmap32* __fastcall CreateMaskFromPolygon(Gr32::TBitmap32* bitmap, const Gr32::TArrayOfFixedPoint polygon)/* overload */;
extern PACKAGE Gr32::TBitmap32* __fastcall CreateMaskFromPolygon(Gr32::TBitmap32* bitmap, const Gr32::TArrayOfArrayOfFixedPoint polygons)/* overload */;
extern PACKAGE void __fastcall ApplyMaskToAlpha(Gr32::TBitmap32* bitmap, Gr32::TBitmap32* mask, bool invertMask = false);
extern PACKAGE void __fastcall ApplyMask(Gr32::TBitmap32* modifiedBmp, Gr32::TBitmap32* originalBmp, Gr32::TBitmap32* maskBmp, bool invertMask = false);
extern PACKAGE void __fastcall InvertMask(Gr32::TBitmap32* maskBmp);
extern PACKAGE Gr32::TArrayOfColor32 __fastcall MakeArrayOfColor32(Gr32::TColor32 *colors, const int colors_Size);
extern PACKAGE Gr32::TArrayOfFixedPoint __fastcall SmoothChart(Gr32::TFixedPoint const *chartPts, const int chartPts_Size, int xStep);
extern PACKAGE Gr32::TArrayOfFixedPoint __fastcall GetCBezierPoints(Gr32::TFixedPoint const *control_points, const int control_points_Size);
extern PACKAGE Gr32::TArrayOfFixedPoint __fastcall GetCSplinePoints(Gr32::TFixedPoint const *control_points, const int control_points_Size);
extern PACKAGE Gr32::TArrayOfFixedPoint __fastcall GetQBezierPoints(Gr32::TFixedPoint const *control_points, const int control_points_Size);
extern PACKAGE Gr32::TArrayOfFixedPoint __fastcall GetQSplinePoints(Gr32::TFixedPoint const *control_points, const int control_points_Size);
extern PACKAGE Gr32::TArrayOfFixedPoint __fastcall GetEllipsePoints(const Gr32::TFloatRect &ellipseRect);
extern PACKAGE Gr32::TFloatPoint __fastcall GetPtOnEllipseFromAngleEccentric(const Gr32::TFloatRect &ellipseRect, float eccentric_angle_radians);
extern PACKAGE Gr32::TArrayOfFixedPoint __fastcall GetPointsAroundEllipse(const Gr32::TFloatRect &ellipseRect, int PointCount);
extern PACKAGE Gr32::TArrayOfFixedPoint __fastcall GetArcPoints(const Gr32::TFloatRect &ellipseRect, const Gr32::TFloatPoint &startPt, const Gr32::TFloatPoint &endPt)/* overload */;
extern PACKAGE Gr32::TArrayOfFixedPoint __fastcall GetArcPoints(const Gr32::TFloatRect &ellipseRect, float start_degrees, float end_degrees)/* overload */;
extern PACKAGE Gr32::TArrayOfFixedPoint __fastcall GetArcPointsEccentric(const Gr32::TFloatRect &ellipseRect, float start_eccentric, float end_eccentric);
extern PACKAGE Gr32::TArrayOfFixedPoint __fastcall GetPiePoints(const Gr32::TFloatRect &ellipseRect, const Gr32::TFloatPoint &startPt, const Gr32::TFloatPoint &endPt)/* overload */;
extern PACKAGE Gr32::TArrayOfFixedPoint __fastcall GetPiePoints(const Gr32::TFloatRect &ellipseRect, float start_degrees, float end_degrees)/* overload */;
extern PACKAGE Gr32::TArrayOfFixedPoint __fastcall GetPiePointsEccentric(const Gr32::TFloatRect &ellipseRect, float start_eccentric, float end_eccentric);
extern PACKAGE Gr32::TArrayOfFixedPoint __fastcall GetRoundedRectanglePoints(const Gr32::TFloatRect &rect, unsigned roundingPercent);
extern PACKAGE Gr32::TArrayOfFixedPoint __fastcall GetBalloonedEllipsePoints(const Gr32::TFloatRect &ellipseRect, TBalloonPos balloonPos);
extern PACKAGE Gr32::TArrayOfFixedPoint __fastcall GetPipeCornerPoints(const Gr32::TFloatRect &rec, float pipeWidth, TCorner corner);
extern PACKAGE Gr32::TArrayOfFixedPoint __fastcall GetStarPoints(const Gr32::TFixedPoint &Center, int PointCount, float radius1, float radius2);
extern PACKAGE Gr32::TColor32 __fastcall GradientColor(Gr32::TColor32 color1, Gr32::TColor32 color2, float frac);
extern PACKAGE Gr32::TColor32 __fastcall GetColor(Gr32::TColor32 const *colors, const int colors_Size, float fraction);
extern PACKAGE bool __fastcall IsClockwise(const Gr32::TArrayOfFixedPoint pts)/* overload */;
extern PACKAGE bool __fastcall IsClockwise(const Gr32::TArrayOfFloatPoint pts)/* overload */;
extern PACKAGE Gr32::TArrayOfArrayOfFixedPoint __fastcall BuildDashedLine(const Gr32::TArrayOfFixedPoint Points, float const *DashArray, const int DashArray_Size, float DashOffset = 0.000000E+00);
extern PACKAGE Gr32::TFloatPoint __fastcall GetUnitVector(const Gr32::TFixedPoint &pt1, const Gr32::TFixedPoint &pt2);
extern PACKAGE Gr32::TFloatPoint __fastcall GetUnitNormal(const Gr32::TFixedPoint &pt1, const Gr32::TFixedPoint &pt2)/* overload */;
extern PACKAGE Gr32::TFloatPoint __fastcall GetUnitNormal(const Gr32::TFloatPoint &pt1, const Gr32::TFloatPoint &pt2)/* overload */;
extern PACKAGE void __fastcall SimpleLine(Gr32::TBitmap32* bitmap, Gr32::TFixedPoint const *pts, const int pts_Size, Gr32::TColor32 color, bool closed = false)/* overload */;
extern PACKAGE void __fastcall SimpleLine(Gr32::TBitmap32* bitmap, const Gr32::TArrayOfArrayOfFixedPoint ppts, Gr32::TColor32 color, bool closed)/* overload */;
extern PACKAGE void __fastcall SimpleFill(Gr32::TBitmap32* bitmap, Gr32::TFixedPoint *pts, const int pts_Size, Gr32::TColor32 edgeColor, Gr32::TColor32 fillColor, Gr32_polygons::TPolyFillMode aFillMode = (Gr32_polygons::TPolyFillMode)(0x1))/* overload */;
extern PACKAGE void __fastcall SimpleFill(Gr32::TBitmap32* bitmap, const Gr32::TArrayOfArrayOfFixedPoint pts, Gr32::TColor32 edgeColor, Gr32::TColor32 fillColor, Gr32_polygons::TPolyFillMode aFillMode = (Gr32_polygons::TPolyFillMode)(0x1))/* overload */;
extern PACKAGE void __fastcall SimpleFill(Gr32::TBitmap32* bitmap, Gr32::TFloatPoint const *pts, const int pts_Size, Gr32::TColor32 edgeColor, Gr32::TColor32 fillColor, Gr32_polygons::TPolyFillMode aFillMode = (Gr32_polygons::TPolyFillMode)(0x1))/* overload */;
extern PACKAGE void __fastcall SimpleFill(Gr32::TBitmap32* bitmap, const Gr32::TArrayOfArrayOfFloatPoint pts, Gr32::TColor32 edgeColor, Gr32::TColor32 fillColor, Gr32_polygons::TPolyFillMode aFillMode = (Gr32_polygons::TPolyFillMode)(0x1))/* overload */;
extern PACKAGE void __fastcall SimpleFill(Gr32::TBitmap32* bitmap, Gr32::TFixedPoint const *pts, const int pts_Size, Gr32::TColor32 edgeColor, Gr32::TBitmap32* pattern, Gr32_polygons::TPolyFillMode aFillMode = (Gr32_polygons::TPolyFillMode)(0x1))/* overload */;
extern PACKAGE void __fastcall SimpleFill(Gr32::TBitmap32* bitmap, const Gr32::TArrayOfArrayOfFixedPoint pts, Gr32::TColor32 edgeColor, Gr32::TBitmap32* pattern, Gr32_polygons::TPolyFillMode aFillMode = (Gr32_polygons::TPolyFillMode)(0x1))/* overload */;
extern PACKAGE void __fastcall SimpleGradientFill(Gr32::TBitmap32* bitmap, Gr32::TFixedPoint *pts, const int pts_Size, Gr32::TColor32 edgeColor, Gr32::TColor32 const *colors, const int colors_Size, int angle_degrees)/* overload */;
extern PACKAGE bool __fastcall NearlyMatch(const float s1, const float s2, const float tolerance);
extern PACKAGE void __fastcall SimpleGradientFill(Gr32::TBitmap32* bitmap, Gr32::TArrayOfArrayOfFixedPoint pts, Gr32::TColor32 edgeColor, Gr32::TColor32 const *colors, const int colors_Size, int angle_degrees)/* overload */;
extern PACKAGE void __fastcall SimpleStippleFill(Gr32::TBitmap32* bitmap, Gr32::TFixedPoint *pts, const int pts_Size, Gr32::TColor32 const *colors, const int colors_Size, float step, int angle_degrees);
extern PACKAGE void __fastcall SimpleRadialFill(Gr32::TBitmap32* bitmap, const Gr32::TFixedPoint &pt, float radius, Gr32::TColor32 const *colors, const int colors_Size)/* overload */;
extern PACKAGE void __fastcall SimpleRadialFill(Gr32::TBitmap32* bitmap, const Gr32::TFloatRect &rec, Gr32::TColor32 const *colors, const int colors_Size)/* overload */;
extern PACKAGE void __fastcall SimpleRadialFill(Gr32::TBitmap32* bitmap, Gr32::TFixedPoint const *pts, const int pts_Size, Gr32::TColor32 const *colors, const int colors_Size, int angle_degrees = 0x0)/* overload */;
extern PACKAGE void __fastcall SimpleRadialFill(Gr32::TBitmap32* bitmap, const Gr32::TArrayOfArrayOfFixedPoint ppts, Gr32::TColor32 const *colors, const int colors_Size, int angle_degrees = 0x0)/* overload */;
extern PACKAGE void __fastcall SimpleShadow(Gr32::TBitmap32* bitmap, const Gr32::TArrayOfFixedPoint pts, int dx, int dy, int fadeRate, Gr32::TColor32 shadowColor, bool closed = false, bool NoInteriorBleed = false)/* overload */;
extern PACKAGE void __fastcall SimpleShadow(Gr32::TBitmap32* bitmap, const Gr32::TArrayOfArrayOfFixedPoint pts, int dx, int dy, int fadeRate, Gr32::TColor32 shadowColor, bool closed = false, bool NoInteriorBleed = false)/* overload */;
extern PACKAGE void __fastcall Simple3D(Gr32::TBitmap32* bitmap, const Gr32::TArrayOfArrayOfFloatPoint ppts, int dx, int dy, int fadeRate, Gr32::TColor32 topLeftColor, Gr32::TColor32 bottomRightColor)/* overload */;
extern PACKAGE void __fastcall Simple3D(Gr32::TBitmap32* bitmap, const Gr32::TArrayOfFixedPoint pts, int dx, int dy, int fadeRate, Gr32::TColor32 topLeftColor, Gr32::TColor32 bottomRightColor)/* overload */;
extern PACKAGE void __fastcall Simple3D(Gr32::TBitmap32* bitmap, const Gr32::TArrayOfArrayOfFixedPoint pts, int dx, int dy, int fadeRate, Gr32::TColor32 topLeftColor, Gr32::TColor32 bottomRightColor)/* overload */;
extern PACKAGE void __fastcall SimpleBevel(Gr32::TBitmap32* bitmap, Gr32::TArrayOfArrayOfFixedPoint ppts, int strokeWidth, Gr32::TColor32 lightClr, Gr32::TColor32 darkClr, int angle_degrees);

}	/* namespace Gr32_misc */
using namespace Gr32_misc;
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// Gr32_miscHPP
