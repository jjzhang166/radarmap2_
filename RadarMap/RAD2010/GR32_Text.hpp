// CodeGear C++Builder
// Copyright (c) 1995, 2009 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'Gr32_text.pas' rev: 21.00

#ifndef Gr32_textHPP
#define Gr32_textHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member functions
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <Sysinit.hpp>	// Pascal unit
#include <Windows.hpp>	// Pascal unit
#include <Classes.hpp>	// Pascal unit
#include <Sysutils.hpp>	// Pascal unit
#include <Math.hpp>	// Pascal unit
#include <Graphics.hpp>	// Pascal unit
#include <Types.hpp>	// Pascal unit
#include <Gr32.hpp>	// Pascal unit
#include <Gr32_lowlevel.hpp>	// Pascal unit
#include <Gr32_blend.hpp>	// Pascal unit
#include <Gr32_math.hpp>	// Pascal unit
#include <Gr32_polygons.hpp>	// Pascal unit
#include <Gr32_transforms.hpp>	// Pascal unit
#include <Gr32_backends.hpp>	// Pascal unit
#include <Gr32_misc.hpp>	// Pascal unit
#include <Gr32_lines.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Gr32_text
{
//-- type declarations -------------------------------------------------------
#pragma option push -b-
enum TAlignH { aLeft, aRight, aCenter, aJustify };
#pragma option pop

#pragma option push -b-
enum TAlignV { aBottom, aMiddle, aTop };
#pragma option pop

struct TFloatSize
{
	
public:
	float sx;
	float sy;
};


struct TGlyphInfo
{
	
public:
	bool cached;
	#pragma pack(push,1)
	_GLYPHMETRICS metrics;
	#pragma pack(pop)
	Gr32::TArrayOfArrayOfFixedPoint pts;
};


typedef TMetaClass* TTrueTypeFontClass;

class DELPHICLASS TTrueTypeFont;
class PASCALIMPLEMENTATION TTrueTypeFont : public Classes::TPersistent
{
	typedef Classes::TPersistent inherited;
	
private:
	Graphics::TFont* fFont;
	HDC fMemDC;
	HFONT fOldFntHdl;
	bool fHinted;
	bool fFlushed;
	Graphics::TFontName __fastcall GetFontName(void);
	void __fastcall SetFontName(const Graphics::TFontName aFontName);
	int __fastcall GetHeight(void);
	void __fastcall SetHeight(int aHeight);
	int __fastcall GetSize(void);
	void __fastcall SetSize(int aSize);
	Graphics::TFontStyles __fastcall GetStyle(void);
	void __fastcall SetStyle(Graphics::TFontStyles aStyle);
	Graphics::TFontCharset __fastcall GetCharSet(void);
	void __fastcall SetCharSet(Graphics::TFontCharset aCharSet);
	void __fastcall SetHinted(bool value);
	
protected:
	__property HDC MemDC = {read=fMemDC, nodefault};
	
public:
	__fastcall virtual TTrueTypeFont(void)/* overload */;
	__fastcall virtual TTrueTypeFont(Graphics::TFont* aFont)/* overload */;
	__fastcall virtual TTrueTypeFont(const Graphics::TFontName aFontName, int aHeight, Graphics::TFontStyles aStyle, Graphics::TFontCharset aCharSet)/* overload */;
	__fastcall virtual ~TTrueTypeFont(void);
	__classmethod bool __fastcall IsValidTTFont(Graphics::TFont* font);
	bool __fastcall Lock(void);
	bool __fastcall IsLocked(void);
	void __fastcall Unlock(void);
	virtual void __fastcall Assign(Classes::TPersistent* Source);
	virtual bool __fastcall GetGlyphInfo(System::WideChar wc, /* out */ _GLYPHMETRICS &gm, /* out */ Gr32::TArrayOfArrayOfFixedPoint &polyPts);
	bool __fastcall GetTextMetrics(/* out */ tagTEXTMETRICW &tm);
	bool __fastcall GetOutlineTextMetrics(/* out */ _OUTLINETEXTMETRICW &otm);
	void __fastcall Flush(void);
	__property bool Flushed = {read=fFlushed, write=fFlushed, nodefault};
	__property Graphics::TFontName FontName = {read=GetFontName, write=SetFontName};
	__property int Height = {read=GetHeight, write=SetHeight, nodefault};
	__property int Size = {read=GetSize, write=SetSize, nodefault};
	__property Graphics::TFontCharset CharSet = {read=GetCharSet, write=SetCharSet, nodefault};
	__property Graphics::TFontStyles Style = {read=GetStyle, write=SetStyle, nodefault};
	__property bool Hinted = {read=fHinted, write=SetHinted, nodefault};
};


class DELPHICLASS TTrueTypeFontAnsiCache;
class PASCALIMPLEMENTATION TTrueTypeFontAnsiCache : public TTrueTypeFont
{
	typedef TTrueTypeFont inherited;
	
private:
	typedef StaticArray<TGlyphInfo, 96> _TTrueTypeFontAnsiCache__1;
	
	
private:
	_TTrueTypeFontAnsiCache__1 fGlyphInfo;
	
public:
	virtual bool __fastcall GetGlyphInfo(System::WideChar wc, /* out */ _GLYPHMETRICS &gm, /* out */ Gr32::TArrayOfArrayOfFixedPoint &polyPts);
public:
	/* TTrueTypeFont.Create */ inline __fastcall virtual TTrueTypeFontAnsiCache(void)/* overload */ : TTrueTypeFont() { }
	/* TTrueTypeFont.Destroy */ inline __fastcall virtual ~TTrueTypeFontAnsiCache(void) { }
	
};


class DELPHICLASS TText32;
class PASCALIMPLEMENTATION TText32 : public System::TObject
{
	typedef System::TObject inherited;
	
private:
	Gr32::TBitmap32* fTmpBmp;
	bool fLCDDraw;
	int fAngle;
	TFloatSize fSkew;
	TFloatSize fScale;
	TFloatSize fTranslate;
	float fPadding;
	bool fInverted;
	bool fMirrored;
	Gr32::TFixedPoint fCurrentPos;
	#pragma pack(push,1)
	_MAT2 fGlyphMatrix;
	#pragma pack(pop)
	#pragma pack(push,1)
	_MAT2 fCurrentPosMatrix;
	#pragma pack(pop)
	#pragma pack(push,1)
	_MAT2 fUnrotatedMatrix;
	#pragma pack(pop)
	void __fastcall SetInverted(bool value);
	void __fastcall SetMirrored(bool value);
	void __fastcall SetAngle(int value);
	void __fastcall SetScaleX(float value);
	void __fastcall SetScaleY(float value);
	void __fastcall SetSkewX(float value);
	void __fastcall SetSkewY(float value);
	void __fastcall SetPadding(float value);
	void __fastcall PrepareMatrices(void);
	void __fastcall DrawInternal(Gr32::TBitmap32* bitmap, const System::UnicodeString text, TTrueTypeFont* ttFont, Gr32::TColor32 color);
	void __fastcall DrawInternalLCD(Gr32::TBitmap32* bitmap, const System::UnicodeString text, TTrueTypeFont* ttFont, Gr32::TColor32 color);
	Gr32::TFixedPoint __fastcall GetCurrentPos(void);
	void __fastcall SetCurrentPos(const Gr32::TFixedPoint &newPos);
	
protected:
	void __fastcall GetTextMetrics(const System::UnicodeString text, TTrueTypeFont* ttFont, const Gr32::TFixedPoint &InsertionPt, /* out */ Gr32::TFixedPoint &NextInsertionPt, /* out */ Gr32::TFixedRect &BoundsRect);
	void __fastcall GetDrawInfo(const System::UnicodeString text, TTrueTypeFont* ttFont, const Gr32::TFloatPoint &InsertionPt, /* out */ Gr32::TFloatPoint &NextInsertionPt, /* out */ Gr32_misc::TArrayOfArrayOfArrayOfFixedPoint &polyPolyPts);
	
public:
	__fastcall TText32(void);
	__fastcall virtual ~TText32(void);
	Gr32::TFixedRect __fastcall GetTextFixedRect(float X, float Y, const System::UnicodeString text, TTrueTypeFont* ttFont);
	float __fastcall GetTextHeight(const System::UnicodeString text, TTrueTypeFont* ttFont);
	float __fastcall GetTextWidth(const System::UnicodeString text, TTrueTypeFont* ttFont);
	int __fastcall CountCharsThatFit(float X, float Y, const System::UnicodeString text, TTrueTypeFont* ttFont, const Gr32::TFloatRect &boundsRect, bool forceWordBreak = false);
	void __fastcall Draw(Gr32::TBitmap32* bitmap, float X, float Y, const System::UnicodeString text, TTrueTypeFont* ttFont, Gr32::TColor32 color)/* overload */;
	void __fastcall Draw(Gr32::TBitmap32* bitmap, const System::UnicodeString text, TTrueTypeFont* ttFont, Gr32::TColor32 color)/* overload */;
	void __fastcall Draw(Gr32::TBitmap32* bitmap, Gr32::TFixedPoint const *path, const int path_Size, const System::UnicodeString text, TTrueTypeFont* ttFont, Gr32::TColor32 color, TAlignH alignH = (TAlignH)(0x2), TAlignV alignV = (TAlignV)(0x1), float offsetFromLine = 0.000000E+00)/* overload */;
	void __fastcall Draw(Gr32::TBitmap32* bitmap, const Gr32::TFloatRect &boundsRect, const System::UnicodeString text, TTrueTypeFont* ttFont, Gr32::TColor32 color, TAlignH alignH, TAlignV alignV, bool forceWordBreak = false)/* overload */;
	Gr32_misc::TArrayOfArrayOfArrayOfFixedPoint __fastcall GetEx(const Gr32::TFloatRect &boundsRect, const System::UnicodeString text, TTrueTypeFont* ttFont, TAlignH alignH, TAlignV alignV, bool forceWordBreak = false)/* overload */;
	void __fastcall DrawAndOutline(Gr32::TBitmap32* bitmap, float X, float Y, const System::UnicodeString text, TTrueTypeFont* ttFont, float outlinePenWidth, Gr32::TColor32 outlineColor, Gr32::TColor32 fillColor);
	Gr32::TArrayOfArrayOfFixedPoint __fastcall Get(float X, float Y, const System::UnicodeString text, TTrueTypeFont* ttFont, /* out */ Gr32::TFloatPoint &NextInsertionPt)/* overload */;
	Gr32_misc::TArrayOfArrayOfArrayOfFixedPoint __fastcall GetEx(float X, float Y, const System::UnicodeString text, TTrueTypeFont* ttFont, /* out */ Gr32::TFloatPoint &NextInsertionPt)/* overload */;
	Gr32::TArrayOfArrayOfFixedPoint __fastcall Get(Gr32::TFixedPoint const *path, const int path_Size, const System::UnicodeString text, TTrueTypeFont* ttFont, TAlignH alignH, TAlignV alignV, bool rotateTextToPath, float offsetFromLine = 0.000000E+00)/* overload */;
	Gr32_misc::TArrayOfArrayOfArrayOfFixedPoint __fastcall GetEx(Gr32::TFixedPoint const *path, const int path_Size, const System::UnicodeString text, TTrueTypeFont* ttFont, TAlignH alignH, TAlignV alignV, bool rotateTextToPath, float offsetFromLine = 0.000000E+00)/* overload */;
	Gr32::TArrayOfArrayOfFixedPoint __fastcall GetInflated(float X, float Y, float delta, const System::UnicodeString text, TTrueTypeFont* ttFont, /* out */ Gr32::TFloatPoint &NextInsertionPt);
	Gr32_misc::TArrayOfArrayOfArrayOfFixedPoint __fastcall GetInflatedEx(float X, float Y, float delta, const System::UnicodeString text, TTrueTypeFont* ttFont, /* out */ Gr32::TFloatPoint &NextInsertionPt);
	Gr32::TArrayOfArrayOfFixedPoint __fastcall GetBetweenPaths(const Gr32::TArrayOfFixedPoint bottomCurve, const Gr32::TArrayOfFixedPoint topCurve, const System::UnicodeString text, TTrueTypeFont* ttFont);
	Gr32_misc::TArrayOfArrayOfArrayOfFixedPoint __fastcall GetBetweenPathsEx(const Gr32::TArrayOfFixedPoint bottomCurve, const Gr32::TArrayOfFixedPoint topCurve, const System::UnicodeString text, TTrueTypeFont* ttFont);
	void __fastcall Scale(float sx, float sy);
	void __fastcall Skew(float sx, float sy);
	void __fastcall Translate(float dx, float dy);
	void __fastcall ClearTransformations(void);
	__property int Angle = {read=fAngle, write=SetAngle, nodefault};
	__property float ScaleX = {read=fScale.sx, write=SetScaleX};
	__property float ScaleY = {read=fScale.sy, write=SetScaleY};
	__property float SkewX = {read=fSkew.sx, write=SetSkewX};
	__property float SkewY = {read=fSkew.sy, write=SetSkewY};
	__property float TranslateX = {read=fTranslate.sx, write=fTranslate.sx};
	__property float TranslateY = {read=fTranslate.sy, write=fTranslate.sy};
	__property Gr32::TFixedPoint CurrentPos = {read=GetCurrentPos, write=SetCurrentPos};
	__property bool LCDDraw = {read=fLCDDraw, write=fLCDDraw, nodefault};
	__property bool Inverted = {read=fInverted, write=SetInverted, nodefault};
	__property bool Mirrored = {read=fMirrored, write=SetMirrored, nodefault};
	__property float Spacing = {read=fPadding, write=SetPadding};
};


//-- var, const, procedure ---------------------------------------------------
extern PACKAGE TTrueTypeFontClass TrueTypeFontClass;
extern PACKAGE bool Text32LCDDrawDefault;
extern PACKAGE _MAT2 identity_mat2;
extern PACKAGE _MAT2 vert_flip_mat2;
extern PACKAGE _MAT2 horz_flip_mat2;
extern PACKAGE TFloatSize __fastcall FloatSize(float sx, float sy);
extern PACKAGE void __fastcall ScaleMat2(_MAT2 &mat, const float sx, const float sy);
extern PACKAGE void __fastcall SkewMat2(_MAT2 &mat, const float fx, const float fy);
extern PACKAGE void __fastcall RotateMat2(_MAT2 &mat, const float angle_radians);
extern PACKAGE bool __fastcall GetTTFontCharInfo(HDC MemDC, System::WideChar wc, float dx, float dy, const _MAT2 &mat2, bool Hinted, /* out */ _GLYPHMETRICS &gm, /* out */ Gr32::TArrayOfArrayOfFixedPoint &polyPts);
extern PACKAGE bool __fastcall GetTTFontCharMetrics(HDC MemDC, System::WideChar wc, float dx, float dy, const _MAT2 &mat2, /* out */ _GLYPHMETRICS &gm);
extern PACKAGE void __fastcall LCDPolygonSmoothing(Gr32::TBitmap32* bitmap, Gr32::TBitmap32* tmpBitmap, Gr32_misc::TArrayOfArrayOfArrayOfFixedPoint ppts, Gr32::TColor32 TextColor, Gr32::TColor32 BkColor = (Gr32::TColor32)(0x0));
extern PACKAGE void __fastcall SimpleText(Gr32::TBitmap32* bmp, Graphics::TFont* font, int X, int Y, const System::WideString widetext, Gr32::TColor32 color);
extern PACKAGE void __fastcall SimpleTextLCD(Gr32::TBitmap32* bmp, Graphics::TFont* font, int X, int Y, const System::UnicodeString wideText, Gr32::TColor32 color);

}	/* namespace Gr32_text */
using namespace Gr32_text;
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// Gr32_textHPP
