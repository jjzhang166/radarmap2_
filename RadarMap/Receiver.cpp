//---------------------------------------------------------------------------

#pragma hdrstop

#include "Receiver.h"
#include "Manager.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

//---------------------------------------------------------------------------
// TTraceProcess Class
//---------------------------------------------------------------------------
__fastcall TTraceProcess::TTraceProcess(bool CreateSuspended, TParsedDataQueue* q, TObject* RadarMapManager)
	: TMyThread(CreateSuspended)
{
	ParsedDataQueue=q;
	rdpc=0;
	rpdps=0;
	trcs=0;
	nrec=0;
	coef=-1;
	coef_sum=0;
	t1=DateTimeToTimeStamp(Now());
	wpps=0;
	wheelspeed=0;
	RecoveredFromPauseEvent=CreateEvent(NULL, false, false, NULL);
	wt1=Gr32_system::GetTickCount();
	Manager=RadarMapManager;
}

void __fastcall TTraceProcess::CountPDataPerSec()
{
	int ms;

	t2=DateTimeToTimeStamp(Now());
	ms=t2.Time-t1.Time;
	trcs++;
	if(ms>1000)
	{
		rpdps=1000.*(trcs/(float)ms);
		trcs=0;
		t1=DateTimeToTimeStamp(Now());
	}
}

void __fastcall TTraceProcess::CountWheelSpeed(TGPRUnit* Settings)
{
	unsigned long ms;
	float wp;

	wt2=Gr32_system::GetTickCount();
	ms=wt2-wt1;
	if(ms>1000)
	{
		wp=1000.*(wpps/(float)ms);
		if(Manager && Settings && Settings->PulsesPerTrace!=0)
			wheelspeed=((TRadarMapManager*)Manager)->Settings->WheelTraceDistance*wp/Settings->PulsesPerTrace;
		wpps=0;
		wt1=Gr32_system::GetTickCount();
	}
}

void __fastcall TTraceProcess::writeAllreadyStopped(bool value)
{
	bool b;

	allreadystopped=value;
	if(value)
	{
		b=Suspended;
		if(!b) ForceSuspend();
		ParsedDataQueue->Clear();
		if(!b) ForceResume();
	}
}

void __fastcall TTraceProcess::MoveoutCorrection(TProfile* Profile, TTrace* Trace, float Value)
{
	float TimeFull, dt, t, d, v, dv, tt, f;
	int Samples, j, k, i;
	short int *Data, *SourceData;

	if(Profile && Trace && Value>0 && Value<5 && Profile->ZeroPoint>0)
	{
		Samples=Profile->Samples;
		TimeFull=Profile->TimeRange*1E-9;
		dt=TimeFull/(float)Samples;
		d=Value;
		v=3E8/sqrt(Profile->Permit);
		dv=d/v;
		Data=new short int[Samples];
		SourceData=new short int[Samples];
		try
		{
			for(j=Profile->ZeroPoint; j<Samples; j++)
			{
				tt=dt*(float)(j-Profile->ZeroPoint);
				t=sqrt(dv*dv+tt*tt)-dv;
				f=(t/TimeFull)*(float)Samples+(float)Profile->ZeroPoint;
				if(f>=0 && f<(Samples-1))
				{
					v=1.0-(f-(int)f);
					SourceData[j]=(short int)((float)Trace->SourceData[(int)f]*v+
						(float)Trace->SourceData[(int)f+1]*(1.-v));
					Data[j]=(short int)((float)Trace->Data[(int)f]*v+
						(float)Trace->Data[(int)f+1]*(1.-v));
				}
			}
			for(j=Profile->ZeroPoint; j<Samples; j++)
			{
				Trace->SourceData[j]=SourceData[j];
				Trace->Data[j]=Data[j];
			}
		}
		__finally
		{
			delete[] Data;
			delete[] SourceData;
		}
	}
}

void __fastcall TTraceProcess::Execute()
{
	TParsedTraceData *Data;
	TGPRUnit* Settings;
	TChannel* Channel;
	TTrace *ptr, *NewTrace;
	TDateTime t1, t2;
	float dt;
	int ia;

	SetStarted();
	do
	{
		try
		{
			if(ParsedDataQueue && WaitForSingleObject(ParsedDataQueue->NotEmptyEvent, INFINITE)==WAIT_OBJECT_0 && !allreadystopped)
			{
				Data=(TParsedTraceData*)ParsedDataQueue->Pop();
				try
				{
					if(Data && Data->RxData && Data->Settings && !allreadystopped)
					{
						rdpc++; // received parsed data count
						CountPDataPerSec();
						Settings=(TGPRUnit*)Data->Settings;
						if(Settings->SelectedChannelMode==TChannelMode::Double || Settings->SelectedChannelMode==TChannelMode::Circle)
							Channel=Settings->Channels[Data->ChannelNumber];
						else Channel=Settings->SelectedChannel;
						Channel->RebuildAndStackData(Data, Settings->Stacking);
						if(WaitForSingleObject(RecoveredFromPauseEvent, 0)==WAIT_OBJECT_0)
							Settings->ResetChannelCollecting();
						if(Manager && Settings->sdfBattery && Settings->Battery) ((TRadarMapManager*)Manager)->Battery->AddValue(Data->BatteryValue);
						if(Manager && ((TRadarMapManager*)Manager)->Settings && ((TRadarMapManager*)Manager)->Settings->OneWheelForAllChannels &&
							(Settings->SelectedChannelMode==TChannelMode::Double || Settings->SelectedChannelMode==TChannelMode::Circle))
						{
							for(int i=0; i<Settings->ChannelQuantity; i++)
								if(i!=Data->ChannelNumber)
									Settings->Channels[i]->WheelPulsesStack+=Data->WheelPulses;
						}/**/
						if(Channel->IsStacked(Settings->Stacking) && Channel->Profile && !allreadystopped)
						{
							ptr=Channel->CurrentTrace;
							if((Settings->Positioning & TPositioning::Wheel || Settings->Positioning==TPositioning::PosDLL) && !allreadystopped) // Wheel, Wheel+GPS
							{
								Channel->WheelPulsesSum+=Channel->WheelPulsesStack;
								wpps+=Channel->WheelPulsesStack;
								CountWheelSpeed(Settings);
								if(Channel->WheelPulsesStack!=0 && !allreadystopped)
								{
									if(fabs(Channel->WheelPulsesSum)<Settings->PulsesPerTrace)
									{
										dt=Channel->WheelPulsesStack/Settings->PulsesPerTrace;
										Channel->TraceAccumulator->Accumulate(ptr, dt);
									}
									else
									{
										ia=0;
										if (Channel->WheelPulsesSum < 0) ptr->Backward=true;
										else ptr->Backward=false;
										while(fabs(Channel->WheelPulsesSum)>=Settings->PulsesPerTrace)
										{
											NewTrace=new TTrace(Channel->RealSamples);

											/*dt=(Settings->PulsesPerTrace-fabs(Channel->WheelPulsesSum)+abs(Channel->WheelPulsesStack))/
												Settings->PulsesPerTrace;
											if (fabs(dt)>1) dt=1;
											Channel->TraceAccumulator->Accumulate(ptr, dt);
											NewTrace->CopyTrace((TTrace*)Channel->TraceAccumulator, ptr->Backward);
											Channel->TraceAccumulator->Discharge();*/

											if(ia==0)
											{
												if (Channel->WheelPulsesSum>0)
													dt=(Settings->PulsesPerTrace-(Channel->WheelPulsesSum-Channel->WheelPulsesStack))/Settings->PulsesPerTrace;
												else
													dt=-(Settings->PulsesPerTrace+(Channel->WheelPulsesSum-Channel->WheelPulsesStack))/Settings->PulsesPerTrace;
												Channel->TraceAccumulator->Accumulate(ptr, dt);
												NewTrace->CopyTrace((TTrace*)Channel->TraceAccumulator, ptr->Backward);
												Channel->TraceAccumulator->Discharge();
											}
											else NewTrace->CopyTrace(ptr);

											NewTrace->TraceDistance=Settings->TraceDistance;

											if(Manager && ((TRadarMapManager*)Manager)->Settings->MoveoutCorrection)
												MoveoutCorrection(Channel->Profile, NewTrace, Channel->SelectedAntenna->Offset);

											if(WaitForSingleObject(Channel->MarkPressedEvent, 0)==WAIT_OBJECT_0)
												NewTrace->Mark=Settings->MarkCounter;
											Channel->Profile->AddNewTrace(NewTrace, true);
											if(NewTrace->Backward && Manager && ((TRadarMapManager*)Manager)->RadarMessages)
											{
												((TRadarMapManager*)Manager)->ShowMessageObject(NULL, ((TRadarMapManager*)Manager)->RadarMessages,
													TMessageType::mtText, mvtDisapering, 5000, "You are moving backward, your data will be overwritten!", "Information");
											}
											if (Channel->WheelPulsesSum>0) Channel->WheelPulsesSum -= Settings->PulsesPerTrace;
											else Channel->WheelPulsesSum += Settings->PulsesPerTrace;
											ia++;
										}
										dt = Channel->WheelPulsesSum / Settings->PulsesPerTrace;
										if(fabs(dt)<1)
										{
											ptr->Backward = false;
											Channel->TraceAccumulator->Accumulate(ptr, dt);
										}
									}
									Channel->WheelPulsesStack=0;
								}
								delete ptr;
							}
							else
							{
								ptr->TraceDistance=0;
								//!!!!!!!!!!!!!
								ptr->TraceDistance=0.01;
								//!!!!!!!!!!!!!
								if(Manager && ((TRadarMapManager*)Manager)->Settings->MoveoutCorrection)
									MoveoutCorrection(Channel->Profile, ptr, Channel->SelectedAntenna->Offset);
								if(WaitForSingleObject(Channel->MarkPressedEvent, 0)==WAIT_OBJECT_0)
									ptr->Mark=Settings->MarkCounter;
								Channel->Profile->AddNewTrace(ptr, true);
								/*CalculSpeed();
								if(MainForm->SaveToHard && (RadarDialog->TracesQ-RadarDialog->TraceIndex[q])>=MainForm->TraceQuantity)
								{
									SetEvent(RadarDialog->EventSaveProfHndl);
								}*/
							}
						}
					}
				}
				__finally
				{
					if(Data) delete Data;
				}
			}
			CheckAskForSuspend();
		}
		catch(...) {}
	} while(!Terminated);
	SetIsTerminated();
}

//---------------------------------------------------------------------------
// TReceive Class
//---------------------------------------------------------------------------

__fastcall TReceive::TReceive(bool CreateSuspended, TTCPClient* tc) : TMyThread(CreateSuspended)
{
	settings=NULL;
	tcp_client=tc;
	ParsedDataQueue=new TParsedDataQueue();
	PausedEvent=CreateEvent(NULL, true, false, NULL);
	TracesOverspeed=0;
	//if(CreateSuspended) Resume();
}

__fastcall TReceive::~TReceive()
{
	try
	{
		delete ParsedDataQueue;
		ParsedDataQueue=NULL;
		if(PausedEvent) CloseHandle(PausedEvent);
		PausedEvent=NULL;
	}
	catch(...) {}
}

void __fastcall TReceive::AskForPause()
{
	SetEvent(PausedEvent);
}

void __fastcall TReceive::AskForContinue()
{
	ResetEvent(PausedEvent);
}

void __fastcall TReceive::ExecuteLoopBody()
{
	AnsiString B;
	TParsedData *Data;

	if(tcp_client->Connected && settings)
	{
		for(int i=0; i<settings->RxPacketSize; i++)
			Buf[i]=tcp_client->ReadByte();
		Data=settings->ReceivedDataParser(Buf, settings->RxPacketSize);//B.c_str(), B.Length());
		if(Data)
		{
			switch(Data->Type)
			{
				case pdtTrace:
					if(WaitForSingleObject(PausedEvent, 0)==WAIT_OBJECT_0) //Paused
					{
						delete Data;
						Data=NULL;
					}
					else
					{
						if(settings->Manager &&
							settings->Positioning==TPositioning::PosDLL &&
							PlugInManager && PlugInManager->ActivePos)
						{
							TCoordinateSystem cs;
							TCoordinateSystemDimension csd;
							T3DDoublePoint dp;
							double d, t;
							bool freshness;

							cs=((TRadarMapManager*)settings->Manager)->Settings->DefaultCoordinateSystem;
							freshness=((TRadarMapManager*)settings->Manager)->LastGPSPoint->Freshness;
							((TRadarMapManager*)settings->Manager)->LastGPSPoint->GetXYH(cs, dp.X, dp.Y, dp.Z);
							d=((TParsedTraceData*)Data)->WheelPulses*settings->WheelStep;
							csd=((TRadarMapManager*)settings->Manager)->LastGPSPoint->GetCSDimension(cs);

							d=PlugInManager->ActivePos->GetPassedDistance(d, dp, freshness, csd);

							((TParsedTraceData*)Data)->WheelPulses=d/settings->WheelStep;
							t=((TParsedTraceData*)Data)->WheelPulses/settings->PulsesPerTrace;
							if(t>1.0)
							{
								if(t!=TracesOverspeed)
								{
									LogEvent("Traces overspeed "+FloatToStrF(t, ffFixed, 10, 2)+". PlugIn="+
										ExtractFileName(PlugInManager->ActivePos->DLLFileName), "TReceive::ExecuteLoopBody()");
									TracesOverspeed=t;
								}
								if(t>10.0) ((TParsedTraceData*)Data)->WheelPulses=settings->PulsesPerTrace*10.0;
							}
						}
						else if(settings->Positioning==TPositioning::TwoWheel &&
							settings->Manager && ((TRadarMapManager*)settings->Manager)->TwoWheelClient)
						{
							bool freshness;
							double d, t;

							freshness=((TRadarMapManager*)settings->Manager)->LastGPSPoint->Freshness;
							d=((TRadarMapManager*)settings->Manager)->TwoWheelClient->GetPassedDistance(freshness);

							((TParsedTraceData*)Data)->WheelPulses=d/settings->WheelStep;
							t=((TParsedTraceData*)Data)->WheelPulses/settings->PulsesPerTrace;
							if(t>1.0)
							{
								if(t!=TracesOverspeed)
								{
									LogEvent("Traces overspeed "+FloatToStrF(t, ffFixed, 10, 2)+". Device=WPS",
										"TReceive::ExecuteLoopBody()");
									TracesOverspeed=t;
								}
								if(t>10.0) ((TParsedTraceData*)Data)->WheelPulses=settings->PulsesPerTrace*10.0;
							}
                        }
						ParsedDataQueue->Push(Data);
					}
					break;
				default: delete Data; Data=NULL;
			}
		}
	}
}

//---------------------------------------------------------------------------
// TConnection Class
//---------------------------------------------------------------------------

__fastcall TConnection::TConnection(Classes::TComponent* AOwner, TObject* RadarMapManager)
{
	status=csNone;
	Owner=AOwner;
	Manager=RadarMapManager;
	ipaddress="192.168.0.10"; //"127.0.0.1";//
	port=23;
	tcp_client=new TTCPClient(AOwner);
	rec_thrd=NULL;
	settings=NULL;
	proc_thrd=NULL;
	SettingsLinked=false;
	status=csStarted;
	//StartThreads();
}

__fastcall TConnection::~TConnection()
{
	if(tcp_client!=NULL)
	{
		delete tcp_client;
		tcp_client=NULL;
	}
	if(!SettingsLinked && settings!=NULL)
	{
		delete settings;
		settings=NULL;
	}
}

TGPRUnit* __fastcall TConnection::LinkSettings(void)
{
	SettingsLinked=true;
	return settings;
}

void __fastcall TConnection::StartThreads()
{
	rec_thrd=new TReceive(true, tcp_client);
	settings=NULL;
	proc_thrd=new TTraceProcess(false, rec_thrd->Queue, Manager);
}

void __fastcall TConnection::TerminateThreads()
{
	proc_thrd->AskForTerminate();
	rec_thrd->AskForTerminate();
	WaitOthers();
	if(proc_thrd->ForceTerminate()) delete proc_thrd;
	if(rec_thrd->ForceTerminate()) delete rec_thrd;
	proc_thrd=NULL;
	rec_thrd=NULL;
}

bool __fastcall TConnection::readConnected()
{
	try
	{
		return tcp_client->Connected;
	}
	catch(Exception &e)
	{
		return false;
	}
}

bool __fastcall TConnection::TryToConnect() // There is the call of WhoIs and applying it to the RadarSettings inside
{
	TCommand cmd;
	AnsiString Reply, ipadr=ipadr=IPaddress+":"+IntToStr(Port);
	int tc=0, pos;
	bool res=false;

	if(TryToDisconnect() && tcp_client)
	{
		//LogEvent("PC=1 "+BooleanToStr(tcp_client->Connected, "Connected", "Disconnected"), "TConnection::TryToConnect()");
		while(tc<2 && tcp_client && !tcp_client->Connected)
		{
			try
			{
				if(tcp_client) tcp_client->Connect(IPaddress, Port, 500);
			}
			catch(Exception &e) {LogEvent(e.Message, "TConnection::TryToConnect() 1 "+ipadr);}
			if(tcp_client && !tcp_client->Connected) MySleep(500ul);
			tc++;
		}
		//LogEvent("PC=2 "+BooleanToStr(tcp_client->Connected, "Connected", "Disconnected")+" "+ipadr, "TConnection::TryToConnect()");
		if(tcp_client && tcp_client->Connected)
		{
			if(rec_thrd==NULL) StartThreads();
			//LogEvent("PC=3 "+BooleanToStr(tcp_client->Connected, "Connected", "Disconnected"), "TConnection::TryToConnect() "+ipadr);
			cmd.Cmd="W";
			cmd.NeedSynchroReply=true;
			cmd.CommandReplyLength=7;
			Reply="";
			if(WriteCommand(cmd, &Reply) && Reply!=NULL && Reply.Length()>0)
			{
				//LogEvent("PC=4 "+BooleanToStr(tcp_client->Connected, "Connected", "Disconnected"), "TConnection::TryToConnect() "+ipadr);
				if(Reply=="SINGLE1") 		settings=new TGPR_Single1("", TRegime::Signal, Manager);
				else if(Reply=="SINGLE2") 	settings=new TGPR_Single2("", TRegime::Signal, Manager);
				else if(Reply=="DOUBLE1") 	settings=new TGPR_Double1("", TRegime::Signal, Manager);
				else if(Reply=="DOUBLE2") 	settings=new TGPR_FullDouble("", TRegime::Signal, Manager);
				else if(Reply=="DOUBLE9") 	settings=new TGPR_Advanced("", TRegime::Signal, Manager);
				else if(Reply=="DUAL_10") 	settings=new TGPR_Dual10("", TRegime::Signal, Manager);
				else if(Reply=="COBRA_1")   settings=new TGPR_Cobra("", TRegime::Signal, Manager);
				else if(Reply=="COBRA_2")   settings=new TGPR_CobraCBD("", TRegime::Signal, Manager);
				else if(Reply=="PYTHON3") 	settings=new TGPR_Python3("", TRegime::Signal, Manager);
				else if(Reply=="PYTHON4") 	settings=new TGPR_Python3bis("", TRegime::Signal, Manager);
				else if(Reply.SubString(0, 5)=="$iam ") // ZYNQ family unit
				{
					// Need to send normal command "$whois" to finally understand which unit is connected from ZYNQ family
					cmd.Cmd="$whois";
					cmd.NeedSynchroReply=false;
					cmd.CommandReplyLength=-1;
					Reply="";
					if(WriteCommand(cmd, &Reply) && Reply!=NULL && Reply.Length()>0)
					{
						pos=Reply.Pos("$iam ");
						if(pos>0)
						{
							pos+=((AnsiString)"$iam ").Length();
							if(Reply.SubString(pos, Reply.Length()-pos+1)=="ZYNQ_0")
								settings=new TGPR_Zynq("", TRegime::Signal, Manager);
							else settings=new TGPR_Zynq("", TRegime::Signal, Manager);
						}
						else settings=NULL;
					}
					else settings=NULL;
				}
				//"DigAssist50" - but command Whois send back only 7 symbols
				else //if(Reply=="DigAssi" || Reply=="IR100ns") settings=new TGPR_InfraRadar("", TRegime::Signal, Manager);
				{
InfraRadar:         //LogEvent("PC=5 "+BooleanToStr(tcp_client->Connected, "Connected", "Disconnected")+" "+ipadr, "TConnection::TryToConnect()");
					MySleep(100ul);
					TryToDisconnect();
					//LogEvent("PC=6 "+BooleanToStr(tcp_client->Connected, "Connected", "Disconnected")+" "+ipadr, "TConnection::TryToConnect()");
					if(rec_thrd==NULL) StartThreads();
					//LogEvent("PC=7 "+BooleanToStr(tcp_client->Connected, "Connected", "Disconnected")+" "+ipadr, "TConnection::TryToConnect()");
					cmd.Cmd="P1";
					cmd.NeedSynchroReply=false;
					cmd.CommandReplyLength=0;
					tc=0;
					//while(tc<2 && !tcp_client->Connected)
					//{
					MySleep(100);//ShowMessage("3.2 "+BooleanToStr(tcp_client->Connected, "Connected", "Disconnected"));
						try
						{
							if(tcp_client) tcp_client->Connect(IPaddress, Port, 500);
						}
						catch(Exception &e) {LogEvent(e.Message, "TConnection::TryToConnect() 2 "+ipadr);}
						//if(tcp_client && !tcp_client->Connected) MySleep(500ul);
					//	tc++;
					//}
					//LogEvent("PC=8 "+BooleanToStr(tcp_client->Connected, "Connected", "Disconnected")+" "+ipadr, "TConnection::TryToConnect()");

					if(Reply=="IR100ns") settings=new TGPR_InfraRadar100("", TRegime::Signal, Manager);
					else settings=new TGPR_InfraRadar("", TRegime::Signal, Manager);
					WriteCommand(cmd, &Reply);
					//LogEvent("PC=9 "+BooleanToStr(tcp_client->Connected, "Connected", "Disconnected")+" "+ipadr, "TConnection::TryToConnect()");
				}
				//else settings=new TGPR_Single1("", TRegime::Signal, Manager);
			}
			else
			{
				goto InfraRadar;
/*#ifdef _InfraRadarOnly
				goto InfraRadar;
#else
				settings=new TGPR_Single1("", TRegime::Signal, Manager);
#endif*/
			}
			//LogEvent("PC=10 "+BooleanToStr(tcp_client->Connected, "Connected", "Disconnected")+" "+ipadr, "TConnection::TryToConnect()");
			if(settings) settings->LoadSettings();
			rec_thrd->Settings=settings;
			status=csConnected;
		}
		if(tcp_client) res=tcp_client->Connected;
		else res=false;
	}
	else res=false;
	LogEvent("PC=11 "+BooleanToStr(tcp_client->Connected, "Connected", "Disconnected")+" "+ipadr, "TConnection::TryToConnect()");
	return res;
}

bool __fastcall TConnection::TryToDisconnect()
{
	bool res=false;

	try
	{
		if(tcp_client && tcp_client->Connected)
		{
			if(rec_thrd)
			{
				StopReceiving();
				TerminateThreads();
			}
			tcp_client->Disconnect();
			status=csDisconnected;
		}
		if(tcp_client) res=!tcp_client->Connected;
		else res=false;
	}
	catch(Exception &e)
	{
		LogEvent(e.Message, "TConnection::TryToDisconnect()");
		TerminateThreads();
		res=false;
	}
	return res;
}

bool __fastcall TConnection::WriteCommand(TCommand cmd, AnsiString *Reply)
{
	int MaxLength=40960;
	int rt, i, j, z, dS, smpls;
	unsigned char *Tmp, ch, TmpLength;
	bool TmpCreated;
	AnsiString str;
	bool suspended, TimeOutOcured, Success;

	if(tcp_client && rec_thrd)
	{
		suspended=rec_thrd->Suspended;
		if(!suspended) rec_thrd->AskForSuspend();
		if(!rec_thrd->Started || WaitForSingleObject(rec_thrd->SuspendedEvent, 250)==WAIT_OBJECT_0)
		{
			try
			{
				tcp_client->WriteLn(cmd.Cmd);
			}
			catch (Exception &e)
			{
				return false;
			}
			if(cmd.Cmd.UpperCase()=="W" || settings!=NULL)
			{
				if(cmd.Cmd.UpperCase()=="W" || (settings->NeedSynchroReply && cmd.NeedSynchroReply))
				{
					if(settings && settings->SynchroLength>0 && settings->SynchroWord)
					{
						Tmp=settings->SynchroWord;
						TmpLength=settings->SynchroLength;
						TmpCreated=false;
					}
					else
					{
						Tmp=new unsigned char[4];
						Tmp[0]=0; Tmp[1]=127; Tmp[2]=0; Tmp[3]=127;
						TmpLength=4;
						TmpCreated=true;
					}
					rt=tcp_client->ReadTimeout;
					tcp_client->ReadTimeout=1000;//20;
					try
					{
						z=0;
						do
						{
							Success=false;
							TimeOutOcured=false;
							if(z>0)
							{
								try
								{
									tcp_client->WriteLn(cmd.Cmd);
								}
								catch (Exception &e)
								{
									break;
								}
							}
							try
							{
								j=0;
								for(i=0; i<MaxLength; i++)
								{
									/*if(i>0) for(j=1; j<TmpLength; j++) Tmp[j-1] = Tmp[j];
									Tmp[3]=tcp_client->ReadByte();
									if(Tmp[0]==0 && Tmp[1]==127 && Tmp[2]==0 && Tmp[3]==127)*/
									if(Tmp[j]==tcp_client->ReadByte()) j++;
									else j=0;
									if(j==TmpLength)
									{
										if(rec_thrd && rec_thrd->Settings && rec_thrd->Settings->UnitType<ZYNQ_0 && z==0)
										{
											if(rec_thrd->Settings->OldSamples==0) smpls=rec_thrd->Settings->Samples;
											else smpls=rec_thrd->Settings->OldSamples;
											if(smpls>0)	dS=((i+1) % (smpls*2))-4;
											else dS=0;
											if(dS<=1 && cmd.Cmd.UpperCase()[1]=='T')
											{
												rec_thrd->Settings->ApplySamples(true);
												break;
											}
										}
										else break;
									}
								}
							}
							catch (Exception &e)
							{
								//delete[] Tmp;
								//tcp_client->ReadTimeout=rt;
								//if(!suspended) rec_thrd->AskForResume();
								//LogEvent(e.Message, "TConnection::WriteCommand("+cmd.Cmd+")");
								//return false;
								LogEvent(e.Message, "TConnection::WriteCommand("+cmd.Cmd+")");
								TimeOutOcured=true;
							}
							if(!TimeOutOcured && i<MaxLength) Success=true;
							else z++;
						} while(!Success && z<3);
					}
					__finally
					{
						if(TmpCreated) delete[] Tmp;
						tcp_client->ReadTimeout=rt;
					}
					if(!Success)
					{
						if(!suspended) rec_thrd->AskForResume();
						LogEvent("TConnection::WriteCommand()", cmd.Cmd);
						return false;
					}
				}
			}
			if(abs(cmd.CommandReplyLength)>0 && Reply!=NULL)
			{
				try
				{
					if(cmd.CommandReplyLength==1)
					{
						str="";
						ch=tcp_client->ReadByte();
						str+=ch;
					}
					else if(cmd.CommandReplyLength<0) str=tcp_client->ReadLn();
					else str=tcp_client->ReadString(cmd.CommandReplyLength);
					if(Reply!=NULL) *Reply=str;
				}
				catch (Exception &e)
				{
					if(!suspended) rec_thrd->AskForResume();
					LogEvent(e.Message, "TConnection::WriteCommand("+cmd.Cmd+") 2");
					return false;
				}
			}
		}
		else
		{
			if(!suspended) rec_thrd->AskForResume();
			return false;
		}
		if(!suspended) rec_thrd->AskForResume();
		MySleep(100);
		return true;
	}
	return false;
}

bool __fastcall TConnection::StartReceiving()
{
	if(tcp_client && tcp_client->Connected)
	{
		if(settings!=NULL)
		{
			if(WriteCommand(settings->GetStartCommand(), NULL))
			{
				rec_thrd->AskForResume();
				status=csReceiving;
				return true;
			}
		}
	}
	return false;
}

bool __fastcall TConnection::StopReceiving()
{
	try
	{
		if(settings && proc_thrd)
		{
			proc_thrd->AllreadyStopped=true;
			if(tcp_client && tcp_client->Connected)
				WriteCommand(settings->GetStopCommand(), NULL);
			if(rec_thrd) rec_thrd->AskForSuspend();
			status=csStopped;
			return true;
		}
		return false;
	}
	catch(Exception &e)
	{
		return false;
	}
}

void __fastcall TConnection::PauseReceiving()
{
	try
	{
		if(settings && rec_thrd) rec_thrd->AskForPause();
		status=csPaused;
	}
	catch(Exception &e) {}
}

void __fastcall TConnection::ContinueReceiving()
{
	try
	{
		if(settings && rec_thrd) rec_thrd->AskForContinue();
		status=csReceiving;
	}
	catch(Exception &e) {}
}

int __fastcall TConnection::readReceivedTracesCount()
{
	if(proc_thrd) return proc_thrd->RecPDataCount;
	else return 0;
}
