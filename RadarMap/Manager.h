//---------------------------------------------------------------------------

#ifndef ManagerH
#define ManagerH
//---------------------------------------------------------------------------

#include "Profile.h"
#include "Receiver.h"
#include "MyGraph.h"
#include "VisualToolObjects.h"
#include "MapLabelGeometry.h"
#include "LinesDetector.h"
#include "PipesDetector.h"
#include "GetGPSData.h"
#include "TwoWheel.h"
#include "SparUnit.h"
#include "Palette.h"
#include "Scale.h"
#include "PlugIns.h"
#include "CacheManager.h"
//#include "CacheWalker.h"
#include "DXF_Constants.h"

#define _GpsUnitsRegKey "\\GpsUnits"

class TRadarMapManager;

struct TRadarMapSettings;

class TProfileSatellites : public TObject
{
private:
	TRadarMapManager *Owner;
	TProfileOutputView *outputview;
	TMapPathObject *path;
	TPipesDetector *pipesdetector;
	TGraphicObjectsList *customobjects;
	TGPRUnit* connectionsettings;
	TList *profiles;
protected:
	int __fastcall readProfilesCount() {return profiles->Count;}
	TProfile* __fastcall readProfile(int Index) {if(Index>=0 && Index<profiles->Count) return (TProfile*)profiles->Items[Index]; else return NULL;}
	TRadarMapSettings* __fastcall readSettings();
	void __fastcall ClearProfiles() {for(int i=0; i<profiles->Count; i++) delete (TProfile*)profiles->Items[i]; profiles->Clear();}
public:
	__fastcall TProfileSatellites(TRadarMapManager *AOwner, TProfile *Profile);
	__fastcall ~TProfileSatellites();

	void __fastcall SetPath(TMapPathObject *mpo) {if(mpo!=NULL) {if(path) delete path; path=mpo;}}

	void __fastcall AddProfile(TProfile *Profile);
	void __fastcall DeleteProfile(int Index);
	void __fastcall StartPipesDetector() {if(pipesdetector) StopPipesDetector(); if(outputview) pipesdetector=new TPipesDetector(outputview->Profile, (TObject*)Owner);}
	void __fastcall StopPipesDetector() {delete pipesdetector; pipesdetector=NULL;}

	__property TRadarMapSettings *Settings = {read=readSettings};
	__property TProfileOutputView *OutputView = {read=outputview};
	__property int ProfilesCount = {read=readProfilesCount};
	__property TProfile* Profiles[int Index] = {read=readProfile};
	__property TMapPathObject *Path = {read = path};
	__property TGraphicObjectsList *CustomObjects = {read = customobjects};
	__property TPipesDetector* PipesDetector = {read = pipesdetector};
	__property TGPRUnit* ConnectionSettings = {read = connectionsettings};
};

class TProfileSatellitesList : public TObject
{
private:
	TList *List;
	TRadarMapManager *Owner;
	UnicodeString generatedinfo, briefinfo;
	bool *CustomObjectsChanged;
	int pathindex;
protected:
	int __fastcall readListCount() {if(List) return List->Count; else return 0;}
	TProfileSatellites* __fastcall readListItem(int Index) {if(Index>=0 && Index<List->Count) return (TProfileSatellites*)List->Items[Index]; else return NULL;}
	void __fastcall writeListItem(int Index, TProfileSatellites* value) {if(Index>=0 && Index<List->Count) List->Items[Index]=(TObject*)value;}
public:
	__fastcall TProfileSatellitesList(TRadarMapManager *AOwner) {Owner=AOwner; List=new TList; generatedinfo=""; briefinfo=""; pathindex=0;}
	__fastcall ~TProfileSatellitesList() {Clear(); delete List; List=NULL;}
	int __fastcall Add(TProfileSatellites* o);
	void __fastcall Delete(int Index) {if(Items[Index]) {delete Items[Index]; List->Delete(Index);}}
	void __fastcall Delete(TProfileSatellites* Satellite) {if(List->IndexOf(Satellite)>=0) Delete(List->IndexOf(Satellite));}
	void __fastcall ClearSatellites(TToolProgressBar *PB=NULL) {for(int i=0; i<List->Count; i++) {delete (TProfileSatellites*)Items[i]; ProcessMessages(); if(PB) PB->StepIt();} List->Clear(); pathindex=0;}
	void __fastcall Clear(TToolProgressBar *PB=NULL) {generatedinfo=""; briefinfo=""; ClearSatellites(PB);}
	void __fastcall GenerateInfo();
	int __fastcall IndexOf(TProfileSatellites* Satellite) {return List->IndexOf(Satellite);}
	int __fastcall IndexOf(TProfileOutputView* OV) {int res=-1; for(int i=0; i<List->Count; i++) if(Items[i] && ((TProfileSatellites*)Items[i])->OutputView==OV) {res=i; break;} return res;}
	int __fastcall IndexOf(TProfile* prof) {int res=-1; for(int i=0; i<List->Count; i++) if(Items[i] && ((TProfileSatellites*)Items[i])->ProfilesCount>0) {for(int j=0; j<((TProfileSatellites*)Items[i])->ProfilesCount; j++) if(((TProfileSatellites*)Items[i])->Profiles[j]==prof) {res=i; break;} if(res>=0) break;} return res;}

	__property TProfileSatellites* Items[int Index] = {read=readListItem, write=writeListItem};
	__property int Count = {read=readListCount};
	__property UnicodeString GeneratedInfo = {read=generatedinfo, write=generatedinfo};
	__property UnicodeString BriefInfo = {read=briefinfo, write=briefinfo};
	__property bool* ObjectsChanged = {read=CustomObjectsChanged, write=CustomObjectsChanged};
	__property int PathIndex = {read=pathindex};
};

typedef void __fastcall (__closure *TProfileClearFunction)(bool ZeroBottomScale);

class TRadarMapManager : public TObject
{
private:
	AnsiString PrismDirectory;

	Classes::TComponent* Owner;
	TRadarMapSettings* settings;
	TConnection* connection;
	TGraphicObjectsContainer *objectscontainer;
	TProfileScroller* profilescroller;
	TMapObjectsContainer *mapobjects;
	TProfileSatellitesList* SatellitesList;
	TProfileSatellites *current;
	int currentindex;
	TLinesDetector* linesdetector;
	TMapTrackPathObject *trackingpath;
	TBattery *battery;
	TLaptopBattery *laptopbattery;
	HANDLE newsettingsavailableevent;
	HANDLE globalstopitevent;
	TNmeaClient *gpsclient;
	TTwoWheelNmeaClient *twowheelclient;
	TGpsListenersList *gpslisteners;
	TGpsUnit* gpsunit;
	TSparUnit* sparunit;
	TMapSparObject* sparonmap;
	TGyroCompassObject* gyrocompass;
	TMessagesScrollObject *mapmessages, *gpsmessages, *radarmessages;
	TMessageObject *coordinatesmessage, *gpsmessage, *laptopmessage;
	Types::TRect **profrects;
	TScale **scales;
	TRecycleBin *recyclebin;
	TToolPing *pingtool;
	TToolMemGraph *memgraph;
	bool resultsnotsaved;
	TPalette *palette;
	TProfileClearFunction ProfileClearFunction;
	TGPSCoordinate *lastGPSpoint;
	TSliderToolButton *startstopwaypointbtn;
	TStartStopWaypointLabel *startwaypoint, *stopwaypoint;
	TToolProgressBar *mapprogress;
	//THyperbolaTool *hyperbola;
protected:
	unsigned int lastlabelid;
	TGPRUnit* __fastcall readReceivingSettings(void) {if(Connection && Connection->Settings) return Connection->Settings; else return NULL;}
	TProfile* __fastcall readReceivingProfile(void);
	TProfile* __fastcall readReceivingProfiles(int Index);
	int __fastcall readReceivingProfilesQuantity(void);
	TProfile* __fastcall readCurrentProfile(void);
	TProfile* __fastcall readCurrentProfiles(int Index);
	int __fastcall readCurrentProfilesCount();
	TProfileOutputView* __fastcall readCurrentOutputView(void) {if(Current) return Current->OutputView; else return NULL;}
	TMapPathObject* __fastcall readCurrentPath(void) {if(Current) return Current->Path; else return NULL;}
	TGPRUnit* __fastcall readCurrentSettings(void) {if(Current) return Current->ConnectionSettings; else return NULL;}
	TDXFBlock* __fastcall CreateLabelBlock(__int64 *HIndex, AnsiString AName, float Coef);
	AnsiString __fastcall LabelToText(TCoordinateSystem cs, TCustomProfileLabel *pl, AnsiString LabelType="");
	Types::TRect* __fastcall readProfrect(int Index) {if(profrects && Index>=0 && Index<_MaxProfQuan) return profrects[Index]; else return NULL;}
	TScale* __fastcall readDepthScales(int Index) {if(scales && Index>=0 && Index<_MaxProfQuan) return scales[Index]; else return NULL;}
	void __fastcall writeStartWaypoint(TStartStopWaypointLabel *value);
	void __fastcall writeStopWaypoint(TStartStopWaypointLabel *value);
public:
	__fastcall TRadarMapManager(Classes::TComponent* AOwner);
	__fastcall ~TRadarMapManager();

	void __fastcall DeleteFilesInDirectory(AnsiString PathName, AnsiString FileMask);
	void __fastcall DefaultSettings();
	void __fastcall LoadSettings();
	void __fastcall SaveSettings();

	void __fastcall CreateObjectContainer(TImgView32* ProfImage, Types::TRect **profRect, TScale** AScales);
	void __fastcall CreateMapObjects(TImgView32* MapImage, TMouseAction *ma, TVoidFunction DBE);
	bool __fastcall OpenConnection(void);
	void __fastcall CloseConnection(void);
	void __fastcall CreateProfileSatellites(void);

	void __fastcall SwitchToPath(TMapPathObject* APath);
	void __fastcall SwitchToAvailablePath();

	void __fastcall LoadResultsFromGML(AnsiString FileName);
	void __fastcall SaveResultsToGML(AnsiString FileName);

	void __fastcall ExportResultsToDXF(AnsiString FileName);
	void __fastcall ExportResultsToXYZC(AnsiString FileName);
	void __fastcall ExportResultsToTXT(AnsiString FileName);

	void __fastcall ApplyNewSettings(TRadarMapSettings newSettings);

	void __fastcall NewProject();

	bool __fastcall LoadPathFromSGY(AnsiString FileName);

	void __fastcall SetClearProfileFunction(TProfileClearFunction value) {ProfileClearFunction=value;}

	TMessageObject* ShowMessageObject(TMessageObject* InMsgObj, TMessagesScrollObject* mso,
		TMessageType mt, TMessageVisibalityType mvt, int ATimeOut, AnsiString Message,
		AnsiString Title, TVoidFunction SwitchOffCallback=(TVoidFunction)NULL);

	Types::TRect* __fastcall GetProfRect(int X, int Y);
	int __fastcall GetProfRectIndex(Types::TRect* pr);

	void __fastcall RenewMapTrackingPath();

	__property AnsiString StartDirectory = {read=PrismDirectory};

	__property TRadarMapSettings* Settings = {read=settings};
	__property TConnection* Connection = {read=connection};
	__property TGraphicObjectsContainer *ObjectsContainer = {read=objectscontainer};
	__property TProfileScroller* ProfileScroller = {read=profilescroller};
	__property TMapObjectsContainer *MapObjects = {read=mapobjects};
	__property TLinesDetector *LinesDetector = {read=linesdetector};
	__property TBattery *Battery = {read=battery};
	__property TLaptopBattery *LaptopBattery = {read=laptopbattery};
	__property TMapTrackPathObject *TrackingPath = {read=trackingpath};
	__property TNmeaClient* GpsClient = {read=gpsclient};
	__property TTwoWheelNmeaClient* TwoWheelClient = {read=twowheelclient};
	__property TGpsListenersList *GpsListeners = {read=gpslisteners};
	__property TGpsUnit* GpsUnit = {read=gpsunit};
	__property TSparUnit *SparUnit = {read=sparunit};
	__property TGyroCompassObject *GyroCompass = {read=gyrocompass};
	__property TMapSparObject *SparOnMap = {read=sparonmap};
	__property HANDLE NewSettingsAvailableEvent = {read=newsettingsavailableevent};
	__property HANDLE GlobalStopItEvent = {read=globalstopitevent};
	__property TMessagesScrollObject* MapMessages = {read=mapmessages};
	__property TMessagesScrollObject* GpsMessages = {read=gpsmessages};
	__property TMessagesScrollObject* RadarMessages = {read=radarmessages};
	__property Types::TRect *ProfRect[int Index] = {read=readProfrect};
	__property TMessageObject *CoordinatesMessage = {read=coordinatesmessage, write=coordinatesmessage};
	__property TMessageObject *GpsMessage = {read=gpsmessage, write=gpsmessage};
	__property TMessageObject *LaptopMessage = {read=laptopmessage, write=laptopmessage};
	__property TRecycleBin *RecycleBin = {read=recyclebin};
	__property TToolPing *PingTool = {read=pingtool};
	__property TToolMemGraph *MemGraph = {read=memgraph};
	//__property THyperbolaTool *Hyperbola = {read=hyperbola};

	__property TProfileSatellites *Current = {read = current};
	__property int CurrentIndex = {read=currentindex};
	__property TGPRUnit* ReceivingSettings = {read=readReceivingSettings};
	__property TProfile* ReceivingProfile = {read=readReceivingProfile};
	__property TProfile* ReceivingProfiles[int Index] = {read=readReceivingProfiles};
	__property int ReceivingProfilesQuantity = {read=readReceivingProfilesQuantity};
	__property TProfile* CurrentProfile = {read=readCurrentProfile};
	__property TProfile* CurrentProfiles[int Index] = {read=readCurrentProfiles};
	__property TScale* DepthScales[int Index] = {read=readDepthScales};
	__property int CurrentProfilesCount = {read=readCurrentProfilesCount};
	__property TProfileOutputView* CurrentOutputView = {read=readCurrentOutputView};
	__property TMapPathObject* CurrentPath = {read=readCurrentPath};
	__property TGPRUnit* CurrentSettings = {read=readCurrentSettings};
	__property TProfileSatellitesList* Satellites = {read=SatellitesList};
	__property bool ResultsNotSaved = {read=resultsnotsaved, write=resultsnotsaved};
	__property TPalette* Palette = {read=palette, write=palette};
	__property unsigned int LastLabelID = {read=lastlabelid, write=lastlabelid};
	__property TGPSCoordinate *LastGPSPoint = {read=lastGPSpoint};
	__property TSliderToolButton *StartStopWaypointBtn = {read=startstopwaypointbtn, write=startstopwaypointbtn};
	__property TStartStopWaypointLabel *StartWaypoint = {read=startwaypoint, write=writeStartWaypoint};
	__property TStartStopWaypointLabel *StopWaypoint = {read=stopwaypoint, write=writeStopWaypoint};
	__property TToolProgressBar *MapProgress = {read=mapprogress};
};

extern TCoordinateSystem _defaultCS;
extern TCoordinateSystemDimension _defaultCSD;

TCoordinateSystem csLocalMetric()
{
	TCoordinateSystem res;

	if(_defaultCSD==csdMeters) res=_defaultCS;
	else res=csMetric;

	return res;
}

#define _AssignRadarMapSettingsAsCopy
#undef _AssignRadarMapSettingsAsCopy

struct TRadarMapSettings
{
private:
	//TCoordinateSystem defaultCS;
	//TCoordinateSystemDimension defaultCSD;
	TCoordinateSystem __fastcall readDefaultCS() {return _defaultCS;}
	TCoordinateSystemDimension __fastcall readDefaultCSD() {return _defaultCSD;}
	void __fastcall writeDefaultCS(TCoordinateSystem value)
	{
		TGPSCoordinate *c=new TGPSCoordinate();
		try
		{
			try
			{
				_defaultCSD=c->GetCSDimension(value);
				_defaultCS=value;
			}
			catch(...) {}
		}
		__finally
		{
			delete c;
		}
	}
public:
	TRadarMapSettings() {Owner=NULL;}
	TRadarMapSettings(TRadarMapManager* AOwner) {Owner=AOwner;}

	TRadarMapManager* Owner;

	AnsiString RadarMapDirectory;
	int RadarMapOutputsCounter;
	int MapLabelGeometryIDs;

	//Not for distribution
	bool MapScaleApplying;
	bool LinesDetectorDebug;

	//Not distrubuted still
	bool Messages;
	bool MessagesAnimation;

	//GPR
	AnsiString GeoradarTCPAddr;
	TPositioning WheelPositioning;
	float WheelTraceDistance;
	int TracesInPixel;
	int WheelPulses;
	float WheelDiameter;
	float Permitivity;
	bool ApplyConstantDeduct;
	TBatteryType BatteryType;
	bool ApplyGain;
	bool MoveoutCorrection;
	bool OneWheelForAllChannels;

	//Automatic profiling
	bool AutoStopProfile;
	int AutoStopProfileSize; //in MB
	bool AutoSaveProfile;
	bool AutoCloseProfile;
	AnsiString AutoSaveDir;

	//Filters
	bool WeightedSubtractionFilter;
	int WSWindowWidth; //WeightedSubtraction filter window width in traces;

	__property TCoordinateSystem DefaultCoordinateSystem = {read=readDefaultCS, write=writeDefaultCS};
	__property TCoordinateSystemDimension DefaultCSDimension = {read=readDefaultCSD};
	bool UseGreenwichTime;

	//GPS
	/*AnsiString GpsSimTcpAddr;
	AnsiString GpsSimFileName;
	AnsiString GpsComPort;
	int GpsPortSpeed;
	int GpsMaxTerminalLines; //Not distrubuted still
	unsigned int GpsReadReplyTimeOut; //in ms also could be INFINITE //Absolutely Not distrubuted still
	unsigned int GpsInitTimeOut; //in ms also could be INFINITE //Absolutely Not distrubuted still
	AnsiString GpsUnitName;
	AnsiString GpsUnitFileName;
	//int GpsUnitType; //Type of the connected GPS receiver (0 - NMEA GPS, 1- Ashtech ProMark 800)
	bool GpsRTSCTSEnable;
	float GpsAntennaHeight;
	bool GpsUseZGeoid;
	bool GpsLogToFile;
	bool GpsSimTcpSource;
	bool GpsSimTieCoordinatesToMapCenter;
	bool GpsSimFileSource;

	//TwoWhelsDRS
	AnsiString TwoWheelComPort;
	int TwoWheelPortSpeed;*/

	TNmeaReceiverSettings GPS;
	TNmeaReceiverSettings TwoWheelsDRS;
	bool WPSasWheel;
	float MaxDeviceSpeed;

	//DXF
	bool DXFIncludeMap;
	TColor32 DXFBackgroundColor;
	int DXFColorInvertThreshold;
	bool DXFPathEndsWithArrow;
	bool DXFUsePitch;
	bool DXFUseRoll;
	bool DXFConfidence;
	bool DXFDescriptionAsText;
	float DXFDescriptionTextHeight; //in drawing units (m)
	bool DXFDepthToDescription;
	bool DXFIdToDescription;
	bool DXFPolyline3D;
	bool DXFEnlargeText;

	//SQLiteDXF
//	bool DbDXFAllowed;
	bool DbStoreLayersVisability;
	bool DbMergeLayersWithSameName;
	bool DbShowLayerFileName;

	//BingOnlineMap
	AnsiString BingInetMapAppKey;
	AnsiString BingInetMapViewType;
	int BingInetMapDefaultZoom;
	int BingInetMapMinZoom;
	int BingInetMapMaxZoom;

	//ImageMap
	bool SaveImageMapChanges;

	//XYZC
	float XYZCCellWidth; //in m
	float XYZCCellHeight; //in m
	bool XYZCUseZ;
	bool XYZCUsePitch;
	bool XYZCUseRoll;
	bool XYZCGained;
	bool XYZCAbsolute;
	bool XYZCEnvelope;

	//SPAR
	bool SparEnable;
	AnsiString SparPipeName;
	AnsiString SparShell;
	long SparShellWait;
	bool SparSimulation;
	int SparCOMPortNumber;
	float SparFrequency;
	char SparAverageType;
	char SparAverageTime;
	char SparUpdateRate;
	float SparElevation;
	float SparToAntennaFwd;
	float SparToAntennaRight;
	float SparToAntennaUp;
	bool SparUseBaseline;
	int SparConfidenceMax; //deviation in percents
	int SparMinDbField;
	bool SparGyroCompass;
	bool SparAnglesCompensation;
	float SparPitchCompensation;
	float SparRollCompensation;
	float SparHeadingCompensation;
	bool SparEliminateFSVCorrection;

	//PipesDetector
	bool ProfilePipesDetector;
	float PipesDetectionWinWidth; //in meters
	float PipesDetectionWinOverlap; //in meters
	float PipesDetectionWinResultsBorder; //in meters
	float PipesDetectionPermitFrom;
	float PipesDetectionPermitRange;
	int PipesDetectionHoughAccumMax;

	//Performance
	bool MapPreview;
	bool EasyRendering; //~~, ProfileScroller - done, MapPathObject - not, GyroCompass - On the way
	bool GPSTracking;
	bool ShowGPSCoordinates;
	bool FollowMe;
	bool CommunicationLinesDetector;
	bool EasyZoomIn;
	bool ShowBattery;
	bool ShowRecycleBin;
	TVisualToolObjectAlign RecycleBinTAlign;
	bool ShowGyroCompass;
	bool ShowWindRose;
	bool ShowClocks;
	bool ShowPing;
	bool ShowMemGraph;
	TVisualToolObjectAlign GyroCompassTAlign;
	bool SmallSizeControls;
	bool ShowLabelIcons;
	bool CopyAttachments;
	bool ShowLabelInformationOnPin;
	bool ShowLabelConfidence;
	int TraceTransparency;
	bool ShowAnnotationLayerOnOpen;
	int EmptyMapSize;
	bool EnlargeEmptyMap;
	bool ShowMapLabelNotification;
	bool DrawPathName;
	TColor32 ProfBackgroundColor;
	bool Gml3Dpoints;
	TRadarPanelsOrientation PanelsOrientation;
	TWindowState WindowState;
	int WindowTop;
	int WindowLeft;
	int WindowWidth;
	int WindowHeight;

	bool MapObjectsPrediction;
	float MapObjectsPredictingRadius;
	unsigned long LabelDestructionTimeByClick;
	unsigned long LabelDestructionTimeByTrack;
	bool MapObjectsPredictionSinglePerLayer;

	//MapLabelsGeometry
	float MapLabelSpiderBeamsSector; //in degries;
	bool SingleTypeMapLabelsGeometry;

	//Chris Howard
	AnsiString ChrisMasterIni;
	AnsiString ChrisSlaveIni;
	AnsiString ChrisExe, ChrisExe_AVX1, ChrisExe_AVX2, ChrisExe_SSE2, ChrisExe_Pent;
	bool ChrisShowExeOutput, ChrisExeKeepAliving;
	int ChrisExeTimeout;

	
	//Constants
	int MaximumLayerSizePX; //Not distrubuted still
	int OpenMapType; //Not distrubuted still
	bool DoubleMaximumLayerSizePX;
	int ReferencePointCoordinatesQ;//Not distrubuted still

	//Internal variables
	TDoublePoint LastMapCenter;
	TCoordinateSystem LastMapCS;
	int LastMapZoom;
	unsigned char PathColorIncIndex;

#ifdef _AssignRadarMapSettingsAsCopy
    void Copy(TRadarMapSettings *ASettings)
	{
		if(ASettings)
		{
			Owner=ASettings->Owner;
			RadarMapDirectory=ASettings->RadarMapDirectory;
			RadarMapOutputsCounter=ASettings->RadarMapOutputsCounter;
			MapLabelGeometryIDs=ASettings->MapLabelGeometryIDs;
			MapScaleApplying=ASettings->MapScaleApplying;
			LinesDetectorDebug=ASettings->LinesDetectorDebug;
			Messages=ASettings->Messages;
			MessagesAnimation=ASettings->MessagesAnimation;
			GeoradarTCPAddr=ASettings->GeoradarTCPAddr;
			WheelPositioning=ASettings->WheelPositioning;
			WheelTraceDistance=ASettings->WheelTraceDistance;
			TracesInPixel=ASettings->TracesInPixel;
			WheelPulses=ASettings->WheelPulses;
			WheelDiameter=ASettings->WheelDiameter;
			Permitivity=ASettings->Permitivity;
			ApplyConstantDeduct=ASettings->ApplyConstantDeduct;
			BatteryType=ASettings->BatteryType;
			ApplyGain=ASettings->ApplyGain;
			MoveoutCorrection=ASettings->MoveoutCorrection;
			OneWheelForAllChannels=ASettings->OneWheelForAllChannels;
			AutoStopProfile=ASettings->AutoStopProfile;
			AutoStopProfileSize=ASettings->AutoStopProfileSize;
			AutoSaveProfile=ASettings->AutoSaveProfile;
			AutoCloseProfile=ASettings->AutoCloseProfile;
			AutoSaveDir=ASettings->AutoSaveDir;
			WeightedSubtractionFilter=ASettings->WeightedSubtractionFilter;
			WSWindowWidth=ASettings->WSWindowWidth;
			UseGreenwichTime=ASettings->UseGreenwichTime;
			GPS=ASettings->GPS;
			TwoWheelsDRS=ASettings->TwoWheelsDRS;
			WPSasWheel=ASettings->WPSasWheel;
			MaxDeviceSpeed=ASettings->MaxDeviceSpeed;
			DXFIncludeMap=ASettings->DXFIncludeMap;
			DXFBackgroundColor=ASettings->DXFBackgroundColor;
			DXFColorInvertThreshold=ASettings->DXFColorInvertThreshold;
			DXFPathEndsWithArrow=ASettings->DXFPathEndsWithArrow;
			DXFUsePitch=ASettings->DXFUsePitch;
			DXFUseRoll=ASettings->DXFUseRoll;
			DXFConfidence=ASettings->DXFConfidence;
			DXFDescriptionAsText=ASettings->DXFDescriptionAsText;
			DXFDescriptionTextHeight=ASettings->DXFDescriptionTextHeight;
			DXFDepthToDescription=ASettings->DXFDepthToDescription;
			DXFIdToDescription=ASettings->DXFIdToDescription;
			DXFPolyline3D=ASettings->DXFPolyline3D;
			DXFEnlargeText=ASettings->DXFEnlargeText;
			DbStoreLayersVisability=ASettings->DbStoreLayersVisability;
			DbMergeLayersWithSameName=ASettings->DbMergeLayersWithSameName;
			DbShowLayerFileName=ASettings->DbShowLayerFileName;
			BingInetMapAppKey=ASettings->BingInetMapAppKey;
			BingInetMapViewType=ASettings->BingInetMapViewType;
			BingInetMapDefaultZoom=ASettings->BingInetMapDefaultZoom;
			BingInetMapMinZoom=ASettings->BingInetMapMinZoom;
			BingInetMapMaxZoom=ASettings->BingInetMapMaxZoom;
			SaveImageMapChanges=ASettings->SaveImageMapChanges;
			XYZCCellWidth=ASettings->XYZCCellWidth;
			XYZCCellHeight=ASettings->XYZCCellHeight;
			XYZCUseZ=ASettings->XYZCUseZ;
			XYZCUsePitch=ASettings->XYZCUsePitch;
			XYZCUseRoll=ASettings->XYZCUseRoll;
			XYZCGained=ASettings->XYZCGained;
			XYZCAbsolute=ASettings->XYZCAbsolute;
			XYZCEnvelope=ASettings->XYZCEnvelope;
			SparEnable=ASettings->SparEnable;
			SparPipeName=ASettings->SparPipeName;
			SparShell=ASettings->SparShell;
			SparShellWait=ASettings->SparShellWait;
			SparSimulation=ASettings->SparSimulation;
			SparCOMPortNumber=ASettings->SparCOMPortNumber;
			SparFrequency=ASettings->SparFrequency;
			SparAverageType=ASettings->SparAverageType;
			SparAverageTime=ASettings->SparAverageTime;
			SparUpdateRate=ASettings->SparUpdateRate;
			SparElevation=ASettings->SparElevation;
			SparToAntennaFwd=ASettings->SparToAntennaFwd;
			SparToAntennaRight=ASettings->SparToAntennaRight;
			SparToAntennaUp=ASettings->SparToAntennaUp;
			SparUseBaseline=ASettings->SparUseBaseline;
			SparConfidenceMax=ASettings->SparConfidenceMax;
			SparMinDbField=ASettings->SparMinDbField;
			SparGyroCompass=ASettings->SparGyroCompass;
			SparAnglesCompensation=ASettings->SparAnglesCompensation;
			SparPitchCompensation=ASettings->SparPitchCompensation;
			SparRollCompensation=ASettings->SparRollCompensation;
			SparHeadingCompensation=ASettings->SparHeadingCompensation;
			SparEliminateFSVCorrection=ASettings->SparEliminateFSVCorrection;
			ProfilePipesDetector=ASettings->ProfilePipesDetector;
			PipesDetectionWinWidth=ASettings->PipesDetectionWinWidth;
			PipesDetectionWinOverlap=ASettings->PipesDetectionWinOverlap;
			PipesDetectionWinResultsBorder=ASettings->PipesDetectionWinResultsBorder;
			PipesDetectionPermitFrom=ASettings->PipesDetectionPermitFrom;
			PipesDetectionPermitRange=ASettings->PipesDetectionPermitRange;
			PipesDetectionHoughAccumMax=ASettings->PipesDetectionHoughAccumMax;
			MapPreview=ASettings->MapPreview;
			EasyRendering=ASettings->EasyRendering;
			GPSTracking=ASettings->GPSTracking;
			ShowGPSCoordinates=ASettings->ShowGPSCoordinates;
			FollowMe=ASettings->FollowMe;
			CommunicationLinesDetector=ASettings->CommunicationLinesDetector;
			EasyZoomIn=ASettings->EasyZoomIn;
			ShowBattery=ASettings->ShowBattery;
			ShowRecycleBin=ASettings->ShowRecycleBin;
			RecycleBinTAlign=ASettings->RecycleBinTAlign;
			ShowGyroCompass=ASettings->ShowGyroCompass;
			ShowWindRose=ASettings->ShowWindRose;
			ShowClocks=ASettings->ShowClocks;
			ShowPing=ASettings->ShowPing;
			ShowMemGraph=ASettings->ShowMemGraph;
			GyroCompassTAlign=ASettings->GyroCompassTAlign;
			SmallSizeControls=ASettings->SmallSizeControls;
			ShowLabelIcons=ASettings->ShowLabelIcons;
			CopyAttachments=ASettings->CopyAttachments;
			ShowLabelInformationOnPin=ASettings->ShowLabelInformationOnPin;
			ShowLabelConfidence=ASettings->ShowLabelConfidence;
			TraceTransparency=ASettings->TraceTransparency;
			ShowAnnotationLayerOnOpen=ASettings->ShowAnnotationLayerOnOpen;
			EmptyMapSize=ASettings->EmptyMapSize;
			EnlargeEmptyMap=ASettings->EnlargeEmptyMap;
			ShowMapLabelNotification=ASettings->ShowMapLabelNotification;
			DrawPathName=ASettings->DrawPathName;
			ProfBackgroundColor=ASettings->ProfBackgroundColor;
			Gml3Dpoints=ASettings->Gml3Dpoints;
			TRadarPanelsOrientation PanelsOrientation;
			WindowState=ASettings->WindowState;
			WindowTop=ASettings->WindowTop;
			WindowLeft=ASettings->WindowLeft;
			WindowWidth=ASettings->WindowWidth;
			WindowHeight=ASettings->WindowHeight;
			MapObjectsPrediction=ASettings->MapObjectsPrediction;
			MapObjectsPredictingRadius=ASettings->MapObjectsPredictingRadius;
			LabelDestructionTimeByClick=ASettings->LabelDestructionTimeByClick;
			LabelDestructionTimeByTrack=ASettings->LabelDestructionTimeByTrack;
			MapObjectsPredictionSinglePerLayer=ASettings->MapObjectsPredictionSinglePerLayer;
			MapLabelSpiderBeamsSector=ASettings->MapLabelSpiderBeamsSector;
			SingleTypeMapLabelsGeometry=ASettings->SingleTypeMapLabelsGeometry;
			ChrisMasterIni=ASettings->ChrisMasterIni;
			ChrisSlaveIni=ASettings->ChrisSlaveIni;
			ChrisExe=ASettings->ChrisExe;
			ChrisExe_AVX1=ASettings->ChrisExe_AVX1;
			ChrisExe_AVX2=ASettings->ChrisExe_AVX2;
			ChrisExe_SSE2=ASettings->ChrisExe_SSE2;
			ChrisExe_Pent=ASettings->ChrisExe_Pent;
			ChrisShowExeOutput=ASettings->ChrisShowExeOutput;
			ChrisExeKeepAliving=ASettings->ChrisExeKeepAliving;
			ChrisExeTimeout=ASettings->ChrisExeTimeout;
			MaximumLayerSizePX=ASettings->MaximumLayerSizePX;
			OpenMapType=ASettings->OpenMapType;
			DoubleMaximumLayerSizePX=ASettings->DoubleMaximumLayerSizePX;
			ReferencePointCoordinatesQ=ASettings->ReferencePointCoordinatesQ;
			LastMapCenter=ASettings->LastMapCenter;
			LastMapCS=ASettings->LastMapCS;
			LastMapZoom=ASettings->LastMapZoom;
			PathColorIncIndex=ASettings->PathColorIncIndex;
		}
	}
#endif
	AnsiString GetChrisExe()
	{
		int sci=_SupportedCpuInstructions();
		AnsiString str;

		if(sci & sciAVX2) str=ChrisExe_AVX2;
		else if(sci & sciAVX) str=ChrisExe_AVX1;
		else if(sci & sciSSE2) str=ChrisExe_SSE2;
		else str=ChrisExe_Pent;

		return str;
	}
	TColor32 GetPathColor(unsigned char i) {return DXFColors32[i];}
	TColor32 GetNextPathColor()
	{
		unsigned char r, r2, g, g2, b, b2;
		int dd, dd2;

		do
		{
			r2=(DXFColors32[PathColorIncIndex]&0x00FF0000)>>16;
			g2=(DXFColors32[PathColorIncIndex]&0x0000FF00)>>8;
			b2=(DXFColors32[PathColorIncIndex]&0x000000FF);
			/*if(PathColorIncIndex==0) PathColorIncIndex++;
			else PathColorIncIndex+=6;//++;*/
			PathColorIncIndex++;
			//if(PathColorIncIndex<16)
			r=(DXFColors32[PathColorIncIndex]&0x00FF0000)>>16;
			g=(DXFColors32[PathColorIncIndex]&0x0000FF00)>>8;
			b=(DXFColors32[PathColorIncIndex]&0x000000FF);
			dd=(int)(((float)r+(float)g+(float)b)/3.);
			dd=dabs(dd-r)+dabs(dd-g)+dabs(dd-b);
			dd2=dabs(r2-r)+dabs(g2-g)+dabs(b2-b);

		} while((r==g && g==b) || dd<=12 || dd2<=32 || ((r>g+32) && (r>b+32)));//(r==0xFF && g<12 && b<12));
		return (0xFF000000 | (r<<16) | (g<<8) | b);
	}
	AnsiString GetGpsUnitFileName(AnsiString aGpsUnitName)
	{
		TRegistry& regKey=*new TRegistry();
		AnsiString regkey=_RadarMapRegKey+(AnsiString)_GpsUnitsRegKey;
		AnsiString str="";

		try
		{
			try
			{
				if(regKey.OpenKey(regkey+"\\"+aGpsUnitName, false))
					if(regKey.ValueExists("FileName")) str=regKey.ReadString("FileName");
			}
			catch(Exception &e)	{}
		}
		__finally
		{
			regKey.CloseKey();
			delete &regKey;
		}

		return str;
	}
	AnsiString AsString()
	{
		AnsiString str;

		str="---=== GPR ===---\r\n\r\n";
		str+="IP address: "+GeoradarTCPAddr+"\r\n";
		str+="Positioning: ";
		switch(WheelPositioning)
		{
			case ManualP: str+="Manual\r\n"; break;
			case Wheel:
			case WheelGPS:
				str+="Wheel";
				if(WheelPositioning & TPositioning::GPS) str+="+GPS";
				str+="\r\n";
				str+="  Wheel diameter: "+FloatToStrF(Owner->Settings->WheelDiameter, ffFixed, 5, 2)+" m\r\n";
				str+="  Wheel pulses: "+IntToStr(Owner->Settings->WheelPulses)+"\r\n";
				str+="  Trace distance: "+FloatToStrF(Owner->Settings->WheelTraceDistance, ffFixed, 5, 2)+" m\r\n";
				break;
			default: str+="\r\n";
		}
		str+="Traces in pixel: "+IntToStr(TracesInPixel)+"\r\n";
		str+="Permitivity: "+FloatToStrF(Permitivity, ffFixed, 5, 2)+"\r\n";
		if(Owner)
		{
			TGPRUnit* settings=Owner->CurrentSettings;
			if(settings)
			{
				str+=settings->AsString();
			}
		}
		str+="Moveout Correction: "+BooleanToStr(MoveoutCorrection, "Enabled", "Disabled")+"\r\n";

		str+="\r\n---=== GPS ===---\r\n\r\n";
		str+="GPS Type: "+GPS.UnitName+" ("+GPS.UnitFileName+")\r\n";
		str+="GPS COM Port: "+GPS.ComPort+"\r\n";
		str+="GPS COM Port speed: "+IntToStr(GPS.PortSpeed)+"\r\n";
		str+="GPS COM Port RTS/CTS: "+BooleanToStr(GPS.RTSCTSEnable, "Enabled", "Disabled")+"\r\n";
		str+="GPS Antenna height above ground: "+FloatToStrF(GPS.AntennaHeight, ffFixed, 10, 2)+" m\r\n";
		str+="Default coordinate system: ";
		switch(DefaultCoordinateSystem)
		{
			case csDutch: str+="Dutch RD (Bessel 1841)\r\n"; break;
			case csBeLambert72: str+="Belgian Lambert72 (Hayford 1909)\r\n"; break;
			case csBeLambert08: str+="Belgian Lambert2008 (GRS 80)\r\n"; break;
			case csUTM: str+="UTM Universal Transverse Mercator (GRS 80)\r\n"; break;
			//case csLKS: str+="Latvian Geodetic Coordinate System 1992 (GRS 80)\r\n"; break;
			case csLatLon: str+="Latitude - Longitude (WGS84)\r\n"; break;
			case csDLL: str+="PlugIn CRS\r\n"; break;
			default: str+="\r\n";
		}
		str+="GNSS logging to file: "+BooleanToStr(GPS.LogToFile, "Enabled", "Disabled")+"\r\n";
		str+="Greenwich time instead of Timestamp: "+BooleanToStr(GPS.LogToFile, "Enabled", "Disabled")+"\r\n";

		str+="\r\n---=== GPS Simulator ===---\r\n\r\n";
		str+="GPS Receiver simulation from TCP/IP: "+BooleanToStr(GPS.SimTcpSource, "Enabled", "Disabled")+"\r\n";
		str+="GPS Receiver simulation from text file: "+BooleanToStr(GPS.SimFileSource, "Enabled", "Disabled")+"\r\n";
		str+="Tie GPS coordinaes to the map center: "+BooleanToStr(GPS.SimTieCoordinatesToMapCenter, "Enabled", "Disabled")+"\r\n";

		str+="\r\n---=== AutoCAD DXF ===---\r\n\r\n";
		str+="Include DXF map into DXF export results: "+BooleanToStr(DXFIncludeMap, "Enabled", "Disabled")+"\r\n";
		str+="Consider Pitch angle for Labels in DXF export results: "+BooleanToStr(DXFUsePitch, "Enabled", "Disabled")+"\r\n";
		str+="Consider Roll angle for Labels in DXF export results: "+BooleanToStr(DXFUseRoll, "Enabled", "Disabled")+"\r\n";
		str+="Export coordinate\'s confidence as a circle: "+BooleanToStr(DXFConfidence, "Enabled", "Disabled")+"\r\n";
		str+="Export Label\'s description as a text: "+BooleanToStr(DXFDescriptionAsText, "Enabled", "Disabled")+"\r\n";
		str+="Export paths as 3D Polylines: "+BooleanToStr(DXFPolyline3D, "Enabled", "Disabled")+"\r\n";
		str+="Enlarged Text size: "+BooleanToStr(DXFEnlargeText, "Enabled", "Disabled")+"\r\n";

		str+="\r\n---=== ImageMap ===---\r\n\r\n";
		str+="Store any changes on the ImageMap: "+BooleanToStr(SaveImageMapChanges, "Enabled", "Disabled")+"\r\n";

		str+="\r\n---=== XYZC ===---\r\n\r\n";
		str+="Cell width: "+FloatToStrF(XYZCCellWidth, ffFixed, 10, 2)+" m\r\n";
		str+="Cell height: "+FloatToStrF(XYZCCellHeight, ffFixed, 10, 2)+" m\r\n";
		str+="Consider acquired Z coordinate: "+BooleanToStr(XYZCUseZ, "Enabled", "Disabled")+"\r\n";
		str+="Consider Pitch angle: "+BooleanToStr(XYZCUsePitch, "Enabled", "Disabled")+"\r\n";
		str+="Consider Roll angle: "+BooleanToStr(XYZCUseRoll, "Enabled", "Disabled")+"\r\n";

		str+="\r\n---=== SPAR ===---\r\n\r\n";
		str+="Use Spar data: "+BooleanToStr(SparEnable, "Enabled", "Disabled")+"\r\n";
		str+="Spar 'pipe' name: "+SparPipeName+"\r\n";
		str+="FieldSens View path: "+SparShell+"\r\n";
		str+="FieldSens View running timeout: "+IntToStr((int)((float)SparShellWait/1000.))+" sec\r\n";
		str+="Use Spar simulation: "+BooleanToStr(SparSimulation, "Enabled", "Disabled")+"\r\n";
		str+="Spar COM Port: COM"+IntToStr(SparCOMPortNumber)+"\r\n";
		str+="Spar frequency: "+FloatToStrF(SparFrequency, ffFixed, 10, 2)+" Hz\r\n";
		str+="Spar averaging type: ";
		switch(SparAverageType)
		{
			case 0: str+="None\r\n"; break;
			case 1: str+="Linear\r\n"; break;
			case 2: str+="Exponential\r\n"; break;
			default: str+="\r\n";
		}
		str+="Spar averaging time: ";
		switch(SparAverageTime)
		{
			case 0: str+="0.2 sec\r\n"; break;
			case 1: str+="0.4 sec\r\n"; break;
			case 2: str+="0.6 sec\r\n"; break;
			case 3: str+="0.8 sec\r\n"; break;
			case 4: str+="1 sec\r\n"; break;
			case 5: str+="2 sec\r\n"; break;
			case 6: str+="5 sec\r\n"; break;
			case 7: str+="10 sec\r\n"; break;
			default: str+="\r\n";
		}
		str+="Spar updating time: ";
		switch(SparUpdateRate)
		{
			case 0: str+="0.2 sec\r\n"; break;
			case 1: str+="0.4 sec\r\n"; break;
			case 2: str+="0.6 sec\r\n"; break;
			case 3: str+="0.8 sec\r\n"; break;
			case 4: str+="1 sec\r\n"; break;
			case 5: str+="2 sec\r\n"; break;
			case 6: str+="5 sec\r\n"; break;
			case 7: str+="10 sec\r\n"; break;
			default: str+="\r\n";
		}
		str+="Spar height over ground: "+FloatToStrF(SparElevation, ffFixed, 10, 2)+" m\r\n";
		str+="Spar to GPS antenna forward: "+FloatToStrF(SparToAntennaFwd, ffFixed, 10, 2)+" m\r\n";
		str+="Spar to GPS antenna right: "+FloatToStrF(SparToAntennaRight, ffFixed, 10, 2)+" m\r\n";
		str+="Spar to GPS antenna up: "+FloatToStrF(SparToAntennaUp, ffFixed, 10, 2)+" m\r\n";
		str+="Use moving baseline: "+BooleanToStr(SparUseBaseline, "Enabled", "Disabled")+"\r\n";
		str+="Maximum allowed confidence: "+IntToStr(SparConfidenceMax)+" %\r\n";
		str+="Minimum allowed field strength: "+IntToStr(SparMinDbField)+" dB\r\n";
		str+="Angles compensation: "+BooleanToStr(SparAnglesCompensation, "Enabled", "Disabled")+"\r\n";
		str+="  Pitch: "+FloatToStrF(SparPitchCompensation, ffFixed, 10, 2)+" deg\r\n";
		str+="  Roll: "+FloatToStrF(SparRollCompensation, ffFixed, 10, 2)+" deg\r\n";
		str+="  Heading: "+FloatToStrF(SparHeadingCompensation, ffFixed, 10, 2)+" deg\r\n";
		str+="Consider Pitch angle: "+BooleanToStr(SparEliminateFSVCorrection, "Enabled", "Disabled")+"\r\n";

		str+="\r\n---=== Performance ===---\r\n\r\n";
		str+="Preview on openeing/saving: "+BooleanToStr(MapPreview, "Enabled", "Disabled")+"\r\n";
		str+="Simple rendering: "+BooleanToStr(EasyRendering, "Enabled", "Disabled")+"\r\n";
		str+="Permanent GPS tracking: "+BooleanToStr(GPSTracking, "Enabled", "Disabled")+"\r\n";
		str+="'Follow Me' - tracked position allways in the center: "+BooleanToStr(FollowMe, "Enabled", "Disabled")+"\r\n";
		str+="Utility lines detection: "+BooleanToStr(CommunicationLinesDetector, "Enabled", "Disabled")+"\r\n";
		str+="Profile pipes detection: "+BooleanToStr(ProfilePipesDetector, "Enabled", "Disabled")+"\r\n";
		str+="Single click ZoomIn: "+BooleanToStr(EasyZoomIn, "Enabled", "Disabled")+"\r\n";
		str+="Show GPR battery level: "+BooleanToStr(ShowBattery, "Enabled", "Disabled")+"\r\n";
		str+="Show Recycle Bin: "+BooleanToStr(ShowRecycleBin, "Enabled", "Disabled")+"\r\n";
		str+="Show GyroCompass: "+BooleanToStr(ShowGyroCompass, "Enabled", "Disabled")+"\r\n";
		str+="Show WindRose: "+BooleanToStr(ShowWindRose, "Enabled", "Disabled")+"\r\n";
		str+="Show Clocks: "+BooleanToStr(ShowClocks, "Enabled", "Disabled")+"\r\n";
		str+="Show Ping: "+BooleanToStr(ShowPing, "Enabled", "Disabled")+"\r\n";
		str+="Show Memory Graph: "+BooleanToStr(ShowMemGraph, "Enabled", "Disabled")+"\r\n";
		str+="Large controls: "+BooleanToStr(!SmallSizeControls, "Enabled", "Disabled")+"\r\n";
		str+="Show label icons: "+BooleanToStr(ShowLabelIcons, "Enabled", "Disabled")+"\r\n";
		str+="Copy label linked files while results saving to save directory: "+BooleanToStr(CopyAttachments, "Enabled", "Disabled")+"\r\n";
		str+="Show 'Label information' dialog on Pin label creation: "+BooleanToStr(ShowLabelInformationOnPin, "Enabled", "Disabled")+"\r\n";
		str+="Show label confidence area on map: "+BooleanToStr(ShowLabelConfidence, "Enabled", "Disabled")+"\r\n";
		str+="Hide annotation layers as default: "+BooleanToStr(!ShowAnnotationLayerOnOpen, "Enabled", "Disabled")+"\r\n";
		str+="Initial empty map size: "+IntToStr(EmptyMapSize)+" m\r\n";
		str+="Enlarge empty map size outside of borders: "+BooleanToStr(!EnlargeEmptyMap, "Enabled", "Disabled")+"\r\n";
		str+="Show map label brief info: "+BooleanToStr(!ShowMapLabelNotification, "Enabled", "Disabled")+"\r\n";

		return str;
	}
};

#endif
