#ifndef NMEARECSETTINGS_H
#define NMEARECSETTINGS_H

struct TNmeaReceiverSettings
{
	AnsiString DeviceName;
	AnsiString ComPort;
	int PortSpeed;
	int MaxTerminalLines; //Not distrubuted still
	unsigned int ReadReplyTimeOut; //in ms also could be INFINITE //Absolutely Not distrubuted still
	unsigned int InitTimeOut; //in ms also could be INFINITE //Absolutely Not distrubuted still
	AnsiString UnitName;
	AnsiString UnitFileName;
	//int UnitType; //Type of the connected GPS receiver (0 - NMEA GPS, 1- Ashtech ProMark 800)
	bool RTSCTSEnable;
	float AntennaHeight;
	bool UseZGeoid;
	bool LogToFile;
	bool SimTcpSource;
	bool SimTieCoordinatesToMapCenter;
	bool SimFileSource;
	bool UseGpsForStart;
	bool InitOnStart;
	AnsiString SimTcpAddr;
	AnsiString SimFileName;
};

#endif