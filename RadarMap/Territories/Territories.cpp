#pragma hdrstop

#include "Territories.h"

#pragma package(smart_init)

const TDoubleRect nsTerritories::TerritoryRects[] = { //Left, Right - Longitude, Top, Bottom - Latitude
	TDoubleRect(3.2, 50.75, 7.22, 53.7), // The Netherlands
	TDoubleRect(2.5, 49.5, 6.4, 51.51), // Belgium
	TDoubleRect(5.87, 47.27, 15.05, 55.09), // Germany
	TDoubleRect(-8.74, 49.81, 1.84, 60.9), // UK
	TDoubleRect(-125, 25, -66.8, 49), // USA
	TDoubleRect(-141, 41.6, -50, 83.5),  // Canada
	TDoubleRect(9.52, 49.02, 17.16, 46.372), // Austria
	TDoubleRect(5.955, 47.81, 10.492, 45.817) // Swidzerland
};

const nsTerritories::TSmallDoublePoint NetherlandsBounds[] = {
	nsTerritories::TSmallDoublePoint(3.2, 50.75),
	nsTerritories::TSmallDoublePoint(3.2, 53.7),
	nsTerritories::TSmallDoublePoint(7.22, 53.7),
	nsTerritories::TSmallDoublePoint(7.22, 50.75)
};

const nsTerritories::TSmallDoublePoint BelgiumBounds[] = {
	nsTerritories::TSmallDoublePoint(2.5, 49.5),
	nsTerritories::TSmallDoublePoint(2.5, 51.51),
	nsTerritories::TSmallDoublePoint(6.4, 51.51),
	nsTerritories::TSmallDoublePoint(6.4, 49.5)
};

const nsTerritories::TSmallDoublePoint GermanyBounds[] = {
	nsTerritories::TSmallDoublePoint(5.87, 47.27),
	nsTerritories::TSmallDoublePoint(5.87, 55.09),
	nsTerritories::TSmallDoublePoint(15.05, 55.09),
	nsTerritories::TSmallDoublePoint(15.05, 47.27)
};

const nsTerritories::TSmallDoublePoint UKBounds[] = {
	nsTerritories::TSmallDoublePoint(-8.74, 49.81),
	nsTerritories::TSmallDoublePoint(-8.74, 60.9),
	nsTerritories::TSmallDoublePoint(1.84, 60.9),
	nsTerritories::TSmallDoublePoint(1.84, 49.81)
};

const nsTerritories::TSmallDoublePoint USABounds[] = {
	nsTerritories::TSmallDoublePoint(-125, 25),
	nsTerritories::TSmallDoublePoint(-125, 49),
	nsTerritories::TSmallDoublePoint(-66.8, 49),
	nsTerritories::TSmallDoublePoint(-66.8, 25)
};

const nsTerritories::TSmallDoublePoint CanadaBounds[] = {
	nsTerritories::TSmallDoublePoint(-141, 41.6),
	nsTerritories::TSmallDoublePoint(-141, 83.5),
	nsTerritories::TSmallDoublePoint(-50, 83.5),
	nsTerritories::TSmallDoublePoint(-50, 41.6)
};

const nsTerritories::TSmallDoublePoint AustriaBounds[] = {
	nsTerritories::TSmallDoublePoint(9.52, 49.02),
	nsTerritories::TSmallDoublePoint(9.52, 46.372),
	nsTerritories::TSmallDoublePoint(17.16, 46.372),
	nsTerritories::TSmallDoublePoint(17.16, 49.02)
};

const nsTerritories::TSmallDoublePoint SwidzerlandBounds[] = {
	nsTerritories::TSmallDoublePoint(5.955, 47.81),
	nsTerritories::TSmallDoublePoint(5.955, 45.817),
	nsTerritories::TSmallDoublePoint(10.492, 45.817),
	nsTerritories::TSmallDoublePoint(10.492, 47.81)
};

const nsTerritories::TSmallDoublePoint* nsTerritories::RestrictedBounds[] = {
	NetherlandsBounds,
	BelgiumBounds,
	GermanyBounds,
	UKBounds,
	USABounds,
	CanadaBounds,
	AustriaBounds,
	SwidzerlandBounds
};

const int nsTerritories::RestrictedBoundsPointsCount[] = {
	4, // The Netherlands
	4, // Belgium
	4, // Germany
	4, // UK
	4, // USA
	4, // Canada
	4, // Austria
	4  // Swidzerland
};

TRestrictedTerritory nsTerritories::IsInTerritoriesRect(TDoublePoint p, TRestrictedTerritory StartFrom) //p.X - Longitude, p.Y - Latitude
{
	TRestrictedTerritory res=rtNone;
	int sf=(int)StartFrom+1;
	TDoubleRect *dr;

	try
	{
		for(int i=sf; i<rtTotal; i++)
		{
			dr=(TDoubleRect*)&nsTerritories::TerritoryRects[i];
			if(dr->Contains(p)) //Bounds rect contains point
			{
				res=(TRestrictedTerritory)i;
				break;
			}
		}
	}
	catch(...) {}

	return res;
}

bool nsTerritories::IsInBounds(TDoublePoint p, const TSmallDoublePoint *inArray, int inN)
{
	bool res=false;//true;
	TSmallDoublePoint sdp;

	for (int i=0, j=inN-1; i<inN; j=i++)
	{
		if((((inArray[i].Y<=p.Y) && (p.Y<inArray[j].Y)) || ((inArray[j].Y<=p.Y) && (p.Y<inArray[i].Y))) &&
			(p.X>(inArray[j].X-inArray[i].X)*(p.Y-inArray[i].Y)/(inArray[j].Y-inArray[i].Y)+inArray[i].X))
				res=!res;
	}
	return res;
}

TRestrictedTerritory nsTerritories::IsInTerritoriesBounds(TDoublePoint p, TRestrictedTerritory StartFrom) //p.X - Longitude, p.Y - Latitude
{
	bool b;
	TRestrictedTerritory res=StartFrom, rt;
	int sf;

	try
	{
		sf=(int)StartFrom;
		if(sf<0) sf=0;
		for(int i=sf; i<rtTotal; i++)
		{
			rt=(TRestrictedTerritory)i;
			b=IsInBounds(p, RestrictedBounds[rt], RestrictedBoundsPointsCount[rt]); //check the territory bounds
			if(b) res=rt;
			else res=rtNone;
			if((StartFrom>rtNone && StartFrom<rtTotal) || b) break;
		}
	}
	catch(...) {}

	return res;
}

bool nsTerritories::IsRestrictedPoint(TDoublePoint p, TRestrictedTerritory *out) //p.X - Longitude, p.Y - Latitude
{
	bool res=false;
	TRestrictedTerritory rt=rtNone, rb;

	try
	{
		do
		{
			rt=IsInTerritoriesRect(p, rt);
			res=(rt!=rtNone && rt<rtTotal);
			if(res)
			{
				rb=IsInTerritoriesBounds(p, rt);
				if(rb!=rtNone && rb<rtTotal)
				{
					if(out) *out=rb;
					res=true;
					break;
				}
			}
		} while(res);
	}
	catch(...) {res=false;}

	return res;
}
