#ifndef Territories_H
#define Territories_H

#include "DoubleTypes.h"

enum TRestrictedTerritory {rtNone = -1, rtNetherlands = 0, rtBelgium = 1, rtGermany = 2,
	rtUK = 3, rtUSA = 4, rtCanada = 5, rtAustria = 6, rtSwidzerland = 7, rtTotal = 6};

namespace nsTerritories
{
	struct TSmallDoublePoint
	{
		double X;
		double Y;

		TSmallDoublePoint() {X=Y=0;}
		TSmallDoublePoint(double x, double y) {X=x; Y=y;}
	};

	extern const TDoubleRect TerritoryRects[];
	extern const TSmallDoublePoint *RestrictedBounds[];
	extern const int RestrictedBoundsPointsCount[];

	TRestrictedTerritory IsInTerritoriesRect(TDoublePoint p, TRestrictedTerritory StartFrom=rtNone); //p.X - Longitude, p.Y - Latitude
	bool IsInBounds(TDoublePoint p, const TSmallDoublePoint *inArray, int inN);
	TRestrictedTerritory IsInTerritoriesBounds(TDoublePoint p, TRestrictedTerritory StartFrom=rtNone); //p.X - Longitude, p.Y - Latitude
	bool IsRestrictedPoint(TDoublePoint p, TRestrictedTerritory *out=NULL); //p.X - Longitude, p.Y - Latitude
};

#endif
