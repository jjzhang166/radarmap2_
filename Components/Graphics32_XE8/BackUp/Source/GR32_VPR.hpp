﻿// CodeGear C++Builder
// Copyright (c) 1995, 2016 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'GR32_VPR.pas' rev: 31.00 (Windows)

#ifndef Gr32_vprHPP
#define Gr32_vprHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>
#include <SysInit.hpp>
#include <GR32.hpp>

//-- user supplied -----------------------------------------------------------

namespace Gr32_vpr
{
//-- forward type declarations -----------------------------------------------
struct TValueSpan;
//-- type declarations -------------------------------------------------------
typedef int *PInteger;

using Gr32::PSingleArray;

using Gr32::TSingleArray;

typedef TValueSpan *PValueSpan;

struct DECLSPEC_DRECORD TValueSpan
{
public:
	int X1;
	int X2;
	Gr32::TSingleArray *Values;
};


typedef void __fastcall (__closure *TRenderSpanEvent)(const TValueSpan &Span, int DstY);

typedef void __fastcall (*TRenderSpanProc)(void * Data, const TValueSpan &Span, int DstY);

//-- var, const, procedure ---------------------------------------------------
extern DELPHI_PACKAGE void __fastcall RenderPolyPolygon(const Gr32::TArrayOfArrayOfFloatPoint Points, const Gr32::TFloatRect &ClipRect, const TRenderSpanProc RenderProc, void * Data = (void *)(0x0))/* overload */;
extern DELPHI_PACKAGE void __fastcall RenderPolygon(const Gr32::TArrayOfFloatPoint Points, const Gr32::TFloatRect &ClipRect, const TRenderSpanProc RenderProc, void * Data = (void *)(0x0))/* overload */;
extern DELPHI_PACKAGE void __fastcall RenderPolyPolygon(const Gr32::TArrayOfArrayOfFloatPoint Points, const Gr32::TFloatRect &ClipRect, const TRenderSpanEvent RenderProc)/* overload */;
extern DELPHI_PACKAGE void __fastcall RenderPolygon(const Gr32::TArrayOfFloatPoint Points, const Gr32::TFloatRect &ClipRect, const TRenderSpanEvent RenderProc)/* overload */;
}	/* namespace Gr32_vpr */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_GR32_VPR)
using namespace Gr32_vpr;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// Gr32_vprHPP
