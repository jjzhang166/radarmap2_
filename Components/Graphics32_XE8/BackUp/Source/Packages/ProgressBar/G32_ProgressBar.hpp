// CodeGear C++Builder
// Copyright (c) 1995, 2009 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'G32_progressbar.pas' rev: 21.00

#ifndef G32_progressbarHPP
#define G32_progressbarHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member functions
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <Sysinit.hpp>	// Pascal unit
#include <Windows.hpp>	// Pascal unit
#include <Classes.hpp>	// Pascal unit
#include <Graphics.hpp>	// Pascal unit
#include <Forms.hpp>	// Pascal unit
#include <Gr32.hpp>	// Pascal unit
#include <Gr32_image.hpp>	// Pascal unit
#include <Gr32_blend.hpp>	// Pascal unit
#include <Controls.hpp>	// Pascal unit
#include <Menus.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace G32_progressbar
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TG32_ProgressBar;
class PASCALIMPLEMENTATION TG32_ProgressBar : public Gr32_image::TCustomPaintBox32
{
	typedef Gr32_image::TCustomPaintBox32 inherited;
	
private:
	Graphics::TColor FBackColor;
	Forms::TFormBorderStyle FBorderStyle;
	int FContrast;
	bool FFramed;
	int FMax;
	int FMin;
	int FPosition;
	void __fastcall SetBackColor(Graphics::TColor Value);
	void __fastcall SetBorderStyle(Forms::TBorderStyle Value);
	void __fastcall SetContrast(int Value);
	void __fastcall SetFramed(bool Value);
	void __fastcall SetMax(int Value);
	void __fastcall SetMin(int Value);
	void __fastcall SetPosition(int Value);
	
protected:
	virtual void __fastcall DoPaintBuffer(void);
	Types::TRect __fastcall GetBarRect(void);
	void __fastcall PaintBar(Gr32::TBitmap32* Buffer, const Types::TRect &ARect);
	void __fastcall PaintFrame(Gr32::TBitmap32* Buffer, const Types::TRect &ARect);
	
public:
	__fastcall virtual TG32_ProgressBar(Classes::TComponent* AOwner);
	
__published:
	__property Align = {default=0};
	__property Anchors = {default=3};
	__property Graphics::TColor BackColor = {read=FBackColor, write=SetBackColor, default=-16777200};
	__property Forms::TBorderStyle BorderStyle = {read=FBorderStyle, write=SetBorderStyle, nodefault};
	__property Caption;
	__property Color = {default=-16777211};
	__property Constraints;
	__property int Contrast = {read=FContrast, write=SetContrast, default=64};
	__property Cursor = {default=0};
	__property DragCursor = {default=-12};
	__property DragKind = {default=0};
	__property DragMode = {default=0};
	__property Font;
	__property bool Framed = {read=FFramed, write=SetFramed, default=1};
	__property Height = {default=16};
	__property HelpContext = {default=0};
	__property int Max = {read=FMax, write=SetMax, default=100};
	__property int Min = {read=FMin, write=SetMin, default=0};
	__property ParentColor = {default=1};
	__property ParentFont = {default=1};
	__property ParentShowHint = {default=1};
	__property PopupMenu;
	__property int Position = {read=FPosition, write=SetPosition, nodefault};
	__property ShowHint;
	__property Visible = {default=1};
	__property Width = {default=150};
	__property OnDragDrop;
	__property OnDragOver;
	__property OnEndDrag;
	__property OnMouseDown;
	__property OnMouseMove;
	__property OnMouseUp;
	__property OnStartDrag;
public:
	/* TCustomPaintBox32.Destroy */ inline __fastcall virtual ~TG32_ProgressBar(void) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TG32_ProgressBar(HWND ParentWindow) : Gr32_image::TCustomPaintBox32(ParentWindow) { }
	
};


//-- var, const, procedure ---------------------------------------------------
extern PACKAGE void __fastcall Register(void);

}	/* namespace G32_progressbar */
using namespace G32_progressbar;
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// G32_progressbarHPP
