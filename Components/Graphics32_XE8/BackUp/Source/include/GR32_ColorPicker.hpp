﻿// CodeGear C++Builder
// Copyright (c) 1995, 2016 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'GR32_ColorPicker.pas' rev: 31.00 (Windows)

#ifndef Gr32_colorpickerHPP
#define Gr32_colorpickerHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>
#include <SysInit.hpp>
#include <Winapi.Windows.hpp>
#include <Winapi.Messages.hpp>
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.Forms.hpp>
#include <GR32.hpp>
#include <GR32_Polygons.hpp>
#include <GR32_Containers.hpp>
#include <GR32_ColorGradients.hpp>
#include <System.UITypes.hpp>
#include <Vcl.Menus.hpp>

//-- user supplied -----------------------------------------------------------

namespace Gr32_colorpicker
{
//-- forward type declarations -----------------------------------------------
class DELPHICLASS TScreenColorPickerForm;
class DELPHICLASS THueCirclePolygonFiller;
class DELPHICLASS THueSaturationCirclePolygonFiller;
class DELPHICLASS TBarycentricGradientPolygonFillerEx;
class DELPHICLASS TCustomColorPicker;
class DELPHICLASS TCustomColorPickerHS;
class DELPHICLASS TCustomColorPickerHSV;
class DELPHICLASS TCustomColorPickerGTK;
class DELPHICLASS TColorPickerHS;
class DELPHICLASS TColorPickerHSV;
class DELPHICLASS TColorPickerGTK;
//-- type declarations -------------------------------------------------------
class PASCALIMPLEMENTATION TScreenColorPickerForm : public Vcl::Forms::TCustomForm
{
	typedef Vcl::Forms::TCustomForm inherited;
	
private:
	Gr32::TColor32 FSelectedColor;
	System::Classes::TNotifyEvent FOnColorSelected;
	
protected:
	virtual void __fastcall CreateParams(Vcl::Controls::TCreateParams &Params);
	DYNAMIC void __fastcall KeyDown(System::Word &Key, System::Classes::TShiftState Shift);
	DYNAMIC void __fastcall MouseDown(System::Uitypes::TMouseButton Button, System::Classes::TShiftState Shift, int X, int Y);
	DYNAMIC void __fastcall MouseMove(System::Classes::TShiftState Shift, int X, int Y);
	
public:
	__fastcall virtual TScreenColorPickerForm(System::Classes::TComponent* AOwner);
	__property Gr32::TColor32 SelectedColor = {read=FSelectedColor, write=FSelectedColor, nodefault};
	__property System::Classes::TNotifyEvent OnColorSelected = {read=FOnColorSelected, write=FOnColorSelected};
	
__published:
	__property OnKeyUp;
	__property OnKeyPress;
	__property OnKeyDown;
	__property OnMouseMove;
	__property OnMouseUp;
	__property OnMouseDown;
public:
	/* TCustomForm.CreateNew */ inline __fastcall virtual TScreenColorPickerForm(System::Classes::TComponent* AOwner, int Dummy) : Vcl::Forms::TCustomForm(AOwner, Dummy) { }
	/* TCustomForm.Destroy */ inline __fastcall virtual ~TScreenColorPickerForm(void) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TScreenColorPickerForm(HWND ParentWindow) : Vcl::Forms::TCustomForm(ParentWindow) { }
	
};


#pragma pack(push,4)
class PASCALIMPLEMENTATION THueCirclePolygonFiller : public Gr32_polygons::TCustomPolygonFiller
{
	typedef Gr32_polygons::TCustomPolygonFiller inherited;
	
private:
	Gr32::TFloatPoint FCenter;
	bool FWebSafe;
	
protected:
	virtual Gr32_polygons::TFillLineEvent __fastcall GetFillLine(void);
	HIDESBASE virtual void __fastcall FillLine(Gr32::PColor32 Dst, int DstX, int DstY, int Length, Gr32::PColor32 AlphaValues);
	virtual void __fastcall FillLineWebSafe(Gr32::PColor32 Dst, int DstX, int DstY, int Length, Gr32::PColor32 AlphaValues);
	
public:
	__fastcall THueCirclePolygonFiller(const Gr32::TFloatPoint &Center, bool WebSafe);
	__property Gr32::TFloatPoint Center = {read=FCenter, write=FCenter};
	__property bool WebSafe = {read=FWebSafe, write=FWebSafe, nodefault};
public:
	/* TObject.Destroy */ inline __fastcall virtual ~THueCirclePolygonFiller(void) { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION THueSaturationCirclePolygonFiller : public THueCirclePolygonFiller
{
	typedef THueCirclePolygonFiller inherited;
	
private:
	float FRadius;
	float FInvRadius;
	float FValue;
	void __fastcall SetRadius(const float Value);
	
protected:
	virtual void __fastcall FillLine(Gr32::PColor32 Dst, int DstX, int DstY, int Length, Gr32::PColor32 AlphaValues);
	virtual void __fastcall FillLineWebSafe(Gr32::PColor32 Dst, int DstX, int DstY, int Length, Gr32::PColor32 AlphaValues);
	
public:
	__fastcall THueSaturationCirclePolygonFiller(const Gr32::TFloatPoint &Center, float Radius, float Value, bool WebSafe);
	__property float Radius = {read=FRadius, write=SetRadius};
	__property float Value = {read=FValue, write=FValue};
public:
	/* TObject.Destroy */ inline __fastcall virtual ~THueSaturationCirclePolygonFiller(void) { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TBarycentricGradientPolygonFillerEx : public Gr32_colorgradients::TBarycentricGradientPolygonFiller
{
	typedef Gr32_colorgradients::TBarycentricGradientPolygonFiller inherited;
	
private:
	bool FWebSafe;
	
protected:
	virtual Gr32_polygons::TFillLineEvent __fastcall GetFillLine(void);
	void __fastcall FillLineWebSafe(Gr32::PColor32 Dst, int DstX, int DstY, int Length, Gr32::PColor32 AlphaValues);
	
public:
	__property bool WebSafe = {read=FWebSafe, write=FWebSafe, nodefault};
public:
	/* TObject.Create */ inline __fastcall TBarycentricGradientPolygonFillerEx(void) : Gr32_colorgradients::TBarycentricGradientPolygonFiller() { }
	/* TObject.Destroy */ inline __fastcall virtual ~TBarycentricGradientPolygonFillerEx(void) { }
	
};

#pragma pack(pop)

enum DECLSPEC_DENUM TVisualAidType : unsigned char { vatSolid, vatInvert };

class PASCALIMPLEMENTATION TCustomColorPicker : public Vcl::Controls::TCustomControl
{
	typedef Vcl::Controls::TCustomControl inherited;
	
	
__published:
	typedef void __fastcall (__closure *TAdjustCalc)(float X, float Y);
	
	enum DECLSPEC_DENUM _TCustomColorPicker__1 : unsigned char { pcHue, pcSaturation, pcLuminance, pcValue };
	
	typedef System::Set<_TCustomColorPicker__1, _TCustomColorPicker__1::pcHue, _TCustomColorPicker__1::pcValue> TPreserveComponent;
	
	
private:
	Gr32::TBitmap32* FBuffer;
	TAdjustCalc FAdjustCalc;
	Gr32::TColor32 FSelectedColor;
	bool FBufferValid;
	TPreserveComponent FPreserveComponent;
	bool FWebSafe;
	TVisualAidType FVisualAidType;
	Gr32::TColor32 FVisualAidColor;
	float FVisualAidLineThickness;
	System::Classes::TNotifyEvent FOnChanged;
	void __fastcall SetWebSafe(const bool Value);
	void __fastcall SetSelectedColor(const Gr32::TColor32 Value);
	void __fastcall SetVisualAidType(const TVisualAidType Value);
	void __fastcall SetVisualAidColor(const Gr32::TColor32 Value);
	void __fastcall SetVisualAidLineThickness(const float Value);
	HIDESBASE MESSAGE void __fastcall WMEraseBkgnd(Winapi::Messages::TWMEraseBkgnd &Message);
	MESSAGE void __fastcall WMGetDlgCode(Winapi::Messages::TWMNoParams &Msg);
	
protected:
	virtual void __fastcall Paint(void);
	virtual void __fastcall PaintColorPicker(void) = 0 ;
	virtual void __fastcall SelectedColorChanged(void);
	
public:
	__fastcall virtual TCustomColorPicker(System::Classes::TComponent* AOwner);
	__fastcall virtual ~TCustomColorPicker(void);
	virtual void __fastcall Invalidate(void);
	DYNAMIC void __fastcall Resize(void);
	__property Gr32::TColor32 SelectedColor = {read=FSelectedColor, write=SetSelectedColor, nodefault};
	__property TVisualAidType VisualAidType = {read=FVisualAidType, write=SetVisualAidType, nodefault};
	__property Gr32::TColor32 VisualAidColor = {read=FVisualAidColor, write=SetVisualAidColor, nodefault};
	__property float VisualAidLineThickness = {read=FVisualAidLineThickness, write=SetVisualAidLineThickness};
	__property bool WebSafe = {read=FWebSafe, write=SetWebSafe, nodefault};
	__property System::Classes::TNotifyEvent OnChanged = {read=FOnChanged, write=FOnChanged};
public:
	/* TWinControl.CreateParented */ inline __fastcall TCustomColorPicker(HWND ParentWindow) : Vcl::Controls::TCustomControl(ParentWindow) { }
	
};


class PASCALIMPLEMENTATION TCustomColorPickerHS : public TCustomColorPicker
{
	typedef TCustomColorPicker inherited;
	
	
private:
	enum DECLSPEC_DENUM TMarkerType : unsigned char { mtCross, mtCircle };
	
	
private:
	float FHue;
	float FSaturation;
	TMarkerType FMarkerType;
	void __fastcall PickHue(float X, float Y);
	void __fastcall SetHue(const float Value);
	void __fastcall SetSaturation(const float Value);
	void __fastcall SetMarkerType(const TMarkerType Value);
	
protected:
	virtual void __fastcall PaintColorPicker(void);
	DYNAMIC void __fastcall MouseDown(System::Uitypes::TMouseButton Button, System::Classes::TShiftState Shift, int X, int Y);
	DYNAMIC void __fastcall MouseMove(System::Classes::TShiftState Shift, int X, int Y);
	virtual void __fastcall SelectedColorChanged(void);
	
public:
	__fastcall virtual TCustomColorPickerHS(System::Classes::TComponent* AOwner);
	__property TMarkerType MarkerType = {read=FMarkerType, write=SetMarkerType, nodefault};
	__property float Hue = {read=FHue, write=SetHue};
	__property float Saturation = {read=FSaturation, write=SetSaturation};
public:
	/* TCustomColorPicker.Destroy */ inline __fastcall virtual ~TCustomColorPickerHS(void) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TCustomColorPickerHS(HWND ParentWindow) : TCustomColorPicker(ParentWindow) { }
	
};


class PASCALIMPLEMENTATION TCustomColorPickerHSV : public TCustomColorPicker
{
	typedef TCustomColorPicker inherited;
	
	
__published:
	enum DECLSPEC_DENUM _TCustomColorPickerHSV__1 : unsigned char { vaHueLine, vaSaturationCircle, vaSelection };
	
	typedef System::Set<_TCustomColorPickerHSV__1, _TCustomColorPickerHSV__1::vaHueLine, _TCustomColorPickerHSV__1::vaSelection> TVisualAid;
	
	
private:
	Gr32::TFloatPoint FCenter;
	float FHue;
	float FRadius;
	int FCircleSteps;
	float FSaturation;
	float FValue;
	TVisualAid FVisualAid;
	void __fastcall PickHue(float X, float Y);
	void __fastcall PickValue(float X, float Y);
	void __fastcall SetHue(const float Value);
	void __fastcall SetSaturation(const float Value);
	void __fastcall SetValue(const float Value);
	void __fastcall SetVisualAid(const TVisualAid Value);
	
protected:
	virtual void __fastcall PaintColorPicker(void);
	DYNAMIC void __fastcall MouseDown(System::Uitypes::TMouseButton Button, System::Classes::TShiftState Shift, int X, int Y);
	DYNAMIC void __fastcall MouseUp(System::Uitypes::TMouseButton Button, System::Classes::TShiftState Shift, int X, int Y);
	DYNAMIC void __fastcall MouseMove(System::Classes::TShiftState Shift, int X, int Y);
	virtual void __fastcall SelectedColorChanged(void);
	
public:
	__fastcall virtual TCustomColorPickerHSV(System::Classes::TComponent* AOwner);
	DYNAMIC void __fastcall Resize(void);
	__property float Hue = {read=FHue, write=SetHue};
	__property float Saturation = {read=FSaturation, write=SetSaturation};
	__property float Value = {read=FValue, write=SetValue};
	__property TVisualAid VisualAid = {read=FVisualAid, write=SetVisualAid, nodefault};
public:
	/* TCustomColorPicker.Destroy */ inline __fastcall virtual ~TCustomColorPickerHSV(void) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TCustomColorPickerHSV(HWND ParentWindow) : TCustomColorPicker(ParentWindow) { }
	
};


class PASCALIMPLEMENTATION TCustomColorPickerGTK : public TCustomColorPicker
{
	typedef TCustomColorPicker inherited;
	
	
__published:
	enum DECLSPEC_DENUM _TCustomColorPickerGTK__1 : unsigned char { vagHueLine, vagSelection };
	
	typedef System::Set<_TCustomColorPickerGTK__1, _TCustomColorPickerGTK__1::vagHueLine, _TCustomColorPickerGTK__1::vagSelection> TVisualAidGTK;
	
	
private:
	Gr32::TFloatPoint FCenter;
	float FHue;
	float FRadius;
	float FInnerRadius;
	int FCircleSteps;
	float FSaturation;
	float FValue;
	TVisualAidGTK FVisualAid;
	void __fastcall PickHue(float X, float Y);
	void __fastcall PickSaturationValue(float X, float Y);
	void __fastcall SetHue(const float Value);
	void __fastcall SetSaturation(const float Value);
	void __fastcall SetValue(const float Value);
	void __fastcall SetVisualAid(const TVisualAidGTK Value);
	void __fastcall SetRadius(const float Value);
	
protected:
	virtual void __fastcall PaintColorPicker(void);
	DYNAMIC void __fastcall MouseDown(System::Uitypes::TMouseButton Button, System::Classes::TShiftState Shift, int X, int Y);
	DYNAMIC void __fastcall MouseUp(System::Uitypes::TMouseButton Button, System::Classes::TShiftState Shift, int X, int Y);
	DYNAMIC void __fastcall MouseMove(System::Classes::TShiftState Shift, int X, int Y);
	virtual void __fastcall SelectedColorChanged(void);
	__property float Radius = {read=FRadius, write=SetRadius};
	__property Gr32::TFloatPoint Center = {read=FCenter, write=FCenter};
	
public:
	__fastcall virtual TCustomColorPickerGTK(System::Classes::TComponent* AOwner);
	DYNAMIC void __fastcall Resize(void);
	__property float Hue = {read=FHue, write=SetHue};
	__property float Saturation = {read=FSaturation, write=SetSaturation};
	__property float Value = {read=FValue, write=SetValue};
	__property TVisualAidGTK VisualAid = {read=FVisualAid, write=SetVisualAid, nodefault};
public:
	/* TCustomColorPicker.Destroy */ inline __fastcall virtual ~TCustomColorPickerGTK(void) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TCustomColorPickerGTK(HWND ParentWindow) : TCustomColorPicker(ParentWindow) { }
	
};


class PASCALIMPLEMENTATION TColorPickerHS : public TCustomColorPickerHS
{
	typedef TCustomColorPickerHS inherited;
	
__published:
	__property Align = {default=0};
	__property Anchors = {default=3};
	__property DragCursor = {default=-12};
	__property DragKind = {default=0};
	__property Enabled = {default=1};
	__property Hue = {default=0};
	__property MarkerType;
	__property ParentBackground;
	__property ParentColor = {default=1};
	__property ParentShowHint = {default=1};
	__property PopupMenu;
	__property Saturation = {default=0};
	__property SelectedColor;
	__property TabOrder = {default=-1};
	__property TabStop = {default=0};
	__property WebSafe = {default=0};
	__property OnCanResize;
	__property OnChanged;
	__property OnClick;
	__property OnDblClick;
	__property OnDragDrop;
	__property OnDragOver;
	__property OnEndDrag;
	__property OnMouseDown;
	__property OnMouseMove;
	__property OnMouseUp;
	__property OnMouseWheel;
	__property OnMouseWheelDown;
	__property OnMouseWheelUp;
	__property OnMouseEnter;
	__property OnMouseLeave;
	__property OnResize;
	__property OnStartDrag;
public:
	/* TCustomColorPickerHS.Create */ inline __fastcall virtual TColorPickerHS(System::Classes::TComponent* AOwner) : TCustomColorPickerHS(AOwner) { }
	
public:
	/* TCustomColorPicker.Destroy */ inline __fastcall virtual ~TColorPickerHS(void) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TColorPickerHS(HWND ParentWindow) : TCustomColorPickerHS(ParentWindow) { }
	
};


class PASCALIMPLEMENTATION TColorPickerHSV : public TCustomColorPickerHSV
{
	typedef TCustomColorPickerHSV inherited;
	
__published:
	__property Align = {default=0};
	__property Anchors = {default=3};
	__property DragCursor = {default=-12};
	__property DragKind = {default=0};
	__property Enabled = {default=1};
	__property Hue = {default=0};
	__property ParentBackground;
	__property ParentColor = {default=1};
	__property ParentShowHint = {default=1};
	__property PopupMenu;
	__property Saturation = {default=0};
	__property SelectedColor;
	__property TabOrder = {default=-1};
	__property TabStop = {default=0};
	__property Value = {default=0};
	__property VisualAid = {default=7};
	__property WebSafe = {default=0};
	__property OnCanResize;
	__property OnChanged;
	__property OnClick;
	__property OnDblClick;
	__property OnDragDrop;
	__property OnDragOver;
	__property OnEndDrag;
	__property OnMouseDown;
	__property OnMouseMove;
	__property OnMouseUp;
	__property OnMouseWheel;
	__property OnMouseWheelDown;
	__property OnMouseWheelUp;
	__property OnMouseEnter;
	__property OnMouseLeave;
	__property OnResize;
	__property OnStartDrag;
public:
	/* TCustomColorPickerHSV.Create */ inline __fastcall virtual TColorPickerHSV(System::Classes::TComponent* AOwner) : TCustomColorPickerHSV(AOwner) { }
	
public:
	/* TCustomColorPicker.Destroy */ inline __fastcall virtual ~TColorPickerHSV(void) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TColorPickerHSV(HWND ParentWindow) : TCustomColorPickerHSV(ParentWindow) { }
	
};


class PASCALIMPLEMENTATION TColorPickerGTK : public TCustomColorPickerGTK
{
	typedef TCustomColorPickerGTK inherited;
	
__published:
	__property Align = {default=0};
	__property Anchors = {default=3};
	__property DragCursor = {default=-12};
	__property DragKind = {default=0};
	__property Enabled = {default=1};
	__property Hue = {default=0};
	__property ParentBackground;
	__property ParentColor = {default=1};
	__property ParentShowHint = {default=1};
	__property PopupMenu;
	__property Saturation = {default=0};
	__property SelectedColor;
	__property TabOrder = {default=-1};
	__property TabStop = {default=0};
	__property Value = {default=0};
	__property VisualAid = {default=3};
	__property WebSafe = {default=0};
	__property OnCanResize;
	__property OnChanged;
	__property OnClick;
	__property OnDblClick;
	__property OnDragDrop;
	__property OnDragOver;
	__property OnEndDrag;
	__property OnMouseDown;
	__property OnMouseMove;
	__property OnMouseUp;
	__property OnMouseWheel;
	__property OnMouseWheelDown;
	__property OnMouseWheelUp;
	__property OnMouseEnter;
	__property OnMouseLeave;
	__property OnResize;
	__property OnStartDrag;
public:
	/* TCustomColorPickerGTK.Create */ inline __fastcall virtual TColorPickerGTK(System::Classes::TComponent* AOwner) : TCustomColorPickerGTK(AOwner) { }
	
public:
	/* TCustomColorPicker.Destroy */ inline __fastcall virtual ~TColorPickerGTK(void) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TColorPickerGTK(HWND ParentWindow) : TCustomColorPickerGTK(ParentWindow) { }
	
};


//-- var, const, procedure ---------------------------------------------------
}	/* namespace Gr32_colorpicker */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_GR32_COLORPICKER)
using namespace Gr32_colorpicker;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// Gr32_colorpickerHPP
