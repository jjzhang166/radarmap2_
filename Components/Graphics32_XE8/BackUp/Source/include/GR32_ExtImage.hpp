﻿// CodeGear C++Builder
// Copyright (c) 1995, 2016 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'GR32_ExtImage.pas' rev: 31.00 (Windows)

#ifndef Gr32_extimageHPP
#define Gr32_extimageHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>
#include <SysInit.hpp>
#include <Winapi.Windows.hpp>
#include <Winapi.Messages.hpp>
#include <GR32.hpp>
#include <GR32_Image.hpp>
#include <GR32_Rasterizers.hpp>
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <System.Types.hpp>
#include <System.UITypes.hpp>

//-- user supplied -----------------------------------------------------------

namespace Gr32_extimage
{
//-- forward type declarations -----------------------------------------------
class DELPHICLASS TSyntheticImage32;
class DELPHICLASS TRenderThread;
//-- type declarations -------------------------------------------------------
enum DECLSPEC_DENUM TRenderMode : unsigned char { rnmFull, rnmConstrained };

class PASCALIMPLEMENTATION TSyntheticImage32 : public Gr32_image::TPaintBox32
{
	typedef Gr32_image::TPaintBox32 inherited;
	
private:
	Gr32_rasterizers::TRasterizer* FRasterizer;
	bool FAutoRasterize;
	System::Classes::TWndMethod FDefaultProc;
	bool FResized;
	TRenderThread* FRenderThread;
	Gr32::TAreaChangedEvent FOldAreaChanged;
	System::Types::TRect FDstRect;
	TRenderMode FRenderMode;
	bool FClearBuffer;
	void __fastcall SetRasterizer(Gr32_rasterizers::TRasterizer* const Value);
	void __fastcall StopRenderThread(void);
	void __fastcall SetDstRect(const System::Types::TRect &Value);
	void __fastcall SetRenderMode(const TRenderMode Value);
	
protected:
	void __fastcall RasterizerChanged(System::TObject* Sender);
	virtual void __fastcall SetParent(Vcl::Controls::TWinControl* AParent);
	void __fastcall FormWindowProc(Winapi::Messages::TMessage &Message);
	void __fastcall DoRasterize(void);
	__property RepaintMode = {default=0};
	
public:
	__fastcall virtual TSyntheticImage32(System::Classes::TComponent* AOwner);
	__fastcall virtual ~TSyntheticImage32(void);
	DYNAMIC void __fastcall Resize(void);
	void __fastcall Rasterize(void);
	__property System::Types::TRect DstRect = {read=FDstRect, write=SetDstRect};
	
__published:
	__property bool AutoRasterize = {read=FAutoRasterize, write=FAutoRasterize, nodefault};
	__property Gr32_rasterizers::TRasterizer* Rasterizer = {read=FRasterizer, write=SetRasterizer};
	__property Buffer;
	__property Color = {default=-16777211};
	__property bool ClearBuffer = {read=FClearBuffer, write=FClearBuffer, nodefault};
	__property TRenderMode RenderMode = {read=FRenderMode, write=SetRenderMode, nodefault};
public:
	/* TWinControl.CreateParented */ inline __fastcall TSyntheticImage32(HWND ParentWindow) : Gr32_image::TPaintBox32(ParentWindow) { }
	
};


class PASCALIMPLEMENTATION TRenderThread : public System::Classes::TThread
{
	typedef System::Classes::TThread inherited;
	
private:
	Gr32::TBitmap32* FDest;
	Gr32_rasterizers::TRasterizer* FRasterizer;
	Gr32::TAreaChangedEvent FOldAreaChanged;
	System::Types::TRect FArea;
	System::Types::TRect FDstRect;
	void __fastcall SynchronizedAreaChanged(void);
	void __fastcall AreaChanged(System::TObject* Sender, const System::Types::TRect &Area, const unsigned Hint);
	
protected:
	virtual void __fastcall Execute(void);
	void __fastcall Rasterize(void);
	
public:
	__fastcall TRenderThread(Gr32_rasterizers::TRasterizer* Rasterizer, Gr32::TBitmap32* Dst, const System::Types::TRect &DstRect, bool Suspended);
public:
	/* TThread.Destroy */ inline __fastcall virtual ~TRenderThread(void) { }
	
};


//-- var, const, procedure ---------------------------------------------------
extern DELPHI_PACKAGE void __fastcall Rasterize(Gr32_rasterizers::TRasterizer* Rasterizer, Gr32::TBitmap32* Dst, const System::Types::TRect &DstRect);
}	/* namespace Gr32_extimage */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_GR32_EXTIMAGE)
using namespace Gr32_extimage;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// Gr32_extimageHPP
