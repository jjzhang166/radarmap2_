﻿// CodeGear C++Builder
// Copyright (c) 1995, 2016 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'GR32_Paths.pas' rev: 31.00 (Windows)

#ifndef Gr32_pathsHPP
#define Gr32_pathsHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>
#include <SysInit.hpp>
#include <System.Classes.hpp>
#include <System.SysUtils.hpp>
#include <GR32.hpp>
#include <GR32_Polygons.hpp>
#include <GR32_Transforms.hpp>
#include <GR32_Brushes.hpp>
#include <GR32_Geometry.hpp>

//-- user supplied -----------------------------------------------------------

namespace Gr32_paths
{
//-- forward type declarations -----------------------------------------------
class DELPHICLASS TCustomPath;
class DELPHICLASS TFlattenedPath;
class DELPHICLASS TCustomCanvas;
class DELPHICLASS TCanvas32;
//-- type declarations -------------------------------------------------------
enum DECLSPEC_DENUM TControlPointOrigin : unsigned char { cpNone, cpCubic, cpConic };

class PASCALIMPLEMENTATION TCustomPath : public Gr32::TThreadPersistent
{
	typedef Gr32::TThreadPersistent inherited;
	
private:
	Gr32::TFloatPoint FCurrentPoint;
	Gr32::TFloatPoint FLastControlPoint;
	TControlPointOrigin FControlPointOrigin;
	
protected:
	virtual void __fastcall AddPoint(const Gr32::TFloatPoint &Point);
	virtual void __fastcall AssignTo(System::Classes::TPersistent* Dest);
	
public:
	__fastcall virtual TCustomPath(void);
	virtual void __fastcall Clear(void);
	void __fastcall MoveTo(const float X, const float Y)/* overload */;
	virtual void __fastcall MoveTo(const Gr32::TFloatPoint &P)/* overload */;
	void __fastcall MoveToRelative(const float X, const float Y)/* overload */;
	void __fastcall MoveToRelative(const Gr32::TFloatPoint &P)/* overload */;
	void __fastcall LineTo(const float X, const float Y)/* overload */;
	virtual void __fastcall LineTo(const Gr32::TFloatPoint &P)/* overload */;
	void __fastcall LineToRelative(const float X, const float Y)/* overload */;
	void __fastcall LineToRelative(const Gr32::TFloatPoint &P)/* overload */;
	void __fastcall HorizontalLineTo(const float X)/* overload */;
	void __fastcall HorizontalLineToRelative(const float X)/* overload */;
	void __fastcall VerticalLineTo(const float Y)/* overload */;
	void __fastcall VerticalLineToRelative(const float Y)/* overload */;
	void __fastcall CurveTo(const float X1, const float Y1, const float X2, const float Y2, const float X, const float Y)/* overload */;
	void __fastcall CurveTo(const float X2, const float Y2, const float X, const float Y)/* overload */;
	virtual void __fastcall CurveTo(const Gr32::TFloatPoint &C1, const Gr32::TFloatPoint &C2, const Gr32::TFloatPoint &P)/* overload */;
	virtual void __fastcall CurveTo(const Gr32::TFloatPoint &C2, const Gr32::TFloatPoint &P)/* overload */;
	void __fastcall CurveToRelative(const float X1, const float Y1, const float X2, const float Y2, const float X, const float Y)/* overload */;
	void __fastcall CurveToRelative(const float X2, const float Y2, const float X, const float Y)/* overload */;
	void __fastcall CurveToRelative(const Gr32::TFloatPoint &C1, const Gr32::TFloatPoint &C2, const Gr32::TFloatPoint &P)/* overload */;
	void __fastcall CurveToRelative(const Gr32::TFloatPoint &C2, const Gr32::TFloatPoint &P)/* overload */;
	void __fastcall ConicTo(const float X1, const float Y1, const float X, const float Y)/* overload */;
	virtual void __fastcall ConicTo(const Gr32::TFloatPoint &P1, const Gr32::TFloatPoint &P)/* overload */;
	void __fastcall ConicTo(const float X, const float Y)/* overload */;
	virtual void __fastcall ConicTo(const Gr32::TFloatPoint &P)/* overload */;
	void __fastcall ConicToRelative(const float X1, const float Y1, const float X, const float Y)/* overload */;
	void __fastcall ConicToRelative(const Gr32::TFloatPoint &P1, const Gr32::TFloatPoint &P)/* overload */;
	void __fastcall ConicToRelative(const float X, const float Y)/* overload */;
	void __fastcall ConicToRelative(const Gr32::TFloatPoint &P)/* overload */;
	virtual void __fastcall BeginPath(void);
	virtual void __fastcall EndPath(void);
	virtual void __fastcall ClosePath(void);
	virtual void __fastcall Rectangle(const Gr32::TFloatRect &Rect);
	virtual void __fastcall RoundRect(const Gr32::TFloatRect &Rect, const float Radius);
	virtual void __fastcall Arc(const Gr32::TFloatPoint &P, float StartAngle, float EndAngle, float Radius);
	virtual void __fastcall Ellipse(float Rx, float Ry, int Steps = 0x64)/* overload */;
	virtual void __fastcall Ellipse(const float Cx, const float Cy, const float Rx, const float Ry, int Steps = 0x64)/* overload */;
	virtual void __fastcall Circle(const float Cx, const float Cy, const float Radius, int Steps = 0x64)/* overload */;
	virtual void __fastcall Circle(const Gr32::TFloatPoint &Center, float Radius, int Steps = 0x64)/* overload */;
	virtual void __fastcall Polygon(const Gr32::TArrayOfFloatPoint APoints);
	__property Gr32::TFloatPoint CurrentPoint = {read=FCurrentPoint, write=FCurrentPoint};
public:
	/* TThreadPersistent.Destroy */ inline __fastcall virtual ~TCustomPath(void) { }
	
};


class PASCALIMPLEMENTATION TFlattenedPath : public TCustomPath
{
	typedef TCustomPath inherited;
	
private:
	Gr32::TArrayOfArrayOfFloatPoint FPath;
	Gr32::TArrayOfFloatPoint FPoints;
	int FPointIndex;
	System::Classes::TNotifyEvent FOnBeginPath;
	System::Classes::TNotifyEvent FOnEndPath;
	System::Classes::TNotifyEvent FOnClosePath;
	Gr32::TArrayOfFloatPoint __fastcall GetPoints(void);
	
protected:
	virtual void __fastcall AddPoint(const Gr32::TFloatPoint &Point);
	virtual void __fastcall AssignTo(System::Classes::TPersistent* Dest);
	
public:
	virtual void __fastcall Clear(void);
	virtual void __fastcall DrawPath(void);
	virtual void __fastcall MoveTo(const Gr32::TFloatPoint &P)/* overload */;
	virtual void __fastcall ClosePath(void);
	virtual void __fastcall BeginPath(void);
	virtual void __fastcall EndPath(void);
	virtual void __fastcall Polygon(const Gr32::TArrayOfFloatPoint APoints);
	__property Gr32::TArrayOfFloatPoint Points = {read=GetPoints};
	__property Gr32::TArrayOfArrayOfFloatPoint Path = {read=FPath};
	__property System::Classes::TNotifyEvent OnBeginPath = {read=FOnBeginPath, write=FOnBeginPath};
	__property System::Classes::TNotifyEvent OnEndPath = {read=FOnEndPath, write=FOnEndPath};
	__property System::Classes::TNotifyEvent OnClosePath = {read=FOnClosePath, write=FOnClosePath};
public:
	/* TCustomPath.Create */ inline __fastcall virtual TFlattenedPath(void) : TCustomPath() { }
	
public:
	/* TThreadPersistent.Destroy */ inline __fastcall virtual ~TFlattenedPath(void) { }
	
	/* Hoisted overloads: */
	
public:
	inline void __fastcall  MoveTo(const float X, const float Y){ TCustomPath::MoveTo(X, Y); }
	
};


class PASCALIMPLEMENTATION TCustomCanvas : public Gr32::TThreadPersistent
{
	typedef Gr32::TThreadPersistent inherited;
	
private:
	TFlattenedPath* FPath;
	Gr32_transforms::TTransformation* FTransformation;
	void __fastcall SetTransformation(Gr32_transforms::TTransformation* const Value);
	
protected:
	virtual void __fastcall DrawPath(void) = 0 ;
	virtual void __fastcall DoBeginPath(System::TObject* Sender);
	virtual void __fastcall DoEndPath(System::TObject* Sender);
	virtual void __fastcall DoClosePath(System::TObject* Sender);
	virtual void __fastcall AssignTo(System::Classes::TPersistent* Dest);
	
public:
	__fastcall virtual TCustomCanvas(void);
	__fastcall virtual ~TCustomCanvas(void);
	__property Gr32_transforms::TTransformation* Transformation = {read=FTransformation, write=SetTransformation};
	__property TFlattenedPath* Path = {read=FPath};
};


class PASCALIMPLEMENTATION TCanvas32 : public TCustomCanvas
{
	typedef TCustomCanvas inherited;
	
private:
	Gr32::TBitmap32* FBitmap;
	Gr32_polygons::TPolygonRenderer32* FRenderer;
	Gr32_brushes::TBrushCollection* FBrushes;
	System::UnicodeString __fastcall GetRendererClassName(void);
	void __fastcall SetRendererClassName(const System::UnicodeString Value);
	void __fastcall SetRenderer(Gr32_polygons::TPolygonRenderer32* ARenderer);
	
protected:
	virtual void __fastcall AssignTo(System::Classes::TPersistent* Dest);
	virtual void __fastcall DrawPath(void);
	__classmethod virtual Gr32_polygons::TPolygonRenderer32Class __fastcall GetPolygonRendererClass();
	virtual void __fastcall BrushCollectionChangeHandler(System::TObject* Sender);
	
public:
	__fastcall virtual TCanvas32(Gr32::TBitmap32* ABitmap);
	__fastcall virtual ~TCanvas32(void);
	void __fastcall RenderText(float X, float Y, const System::WideString Text)/* overload */;
	void __fastcall RenderText(const Gr32::TFloatRect &DstRect, const System::WideString Text, unsigned Flags)/* overload */;
	Gr32::TFloatRect __fastcall MeasureText(const Gr32::TFloatRect &DstRect, const System::WideString Text, unsigned Flags);
	__property Gr32::TBitmap32* Bitmap = {read=FBitmap};
	__property Gr32_polygons::TPolygonRenderer32* Renderer = {read=FRenderer, write=SetRenderer};
	__property System::UnicodeString RendererClassName = {read=GetRendererClassName, write=SetRendererClassName};
	__property Gr32_brushes::TBrushCollection* Brushes = {read=FBrushes};
};


typedef void __fastcall (__closure *TAddPointEvent)(const Gr32::TFloatPoint &Point);

//-- var, const, procedure ---------------------------------------------------
static const System::Int8 DefaultCircleSteps = System::Int8(0x64);
#define DefaultBezierTolerance  (2.500000E-01)
extern DELPHI_PACKAGE float CBezierTolerance;
extern DELPHI_PACKAGE float QBezierTolerance;
}	/* namespace Gr32_paths */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_GR32_PATHS)
using namespace Gr32_paths;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// Gr32_pathsHPP
