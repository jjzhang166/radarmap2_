﻿// CodeGear C++Builder
// Copyright (c) 1995, 2016 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'GR32_Polygons.pas' rev: 31.00 (Windows)

#ifndef Gr32_polygonsHPP
#define Gr32_polygonsHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>
#include <SysInit.hpp>
#include <System.Types.hpp>
#include <GR32.hpp>
#include <GR32_Containers.hpp>
#include <GR32_VPR.hpp>
#include <GR32_Transforms.hpp>
#include <GR32_Resamplers.hpp>
#include <System.Classes.hpp>

//-- user supplied -----------------------------------------------------------

namespace Gr32_polygons
{
//-- forward type declarations -----------------------------------------------
class DELPHICLASS TCustomPolygonRenderer;
class DELPHICLASS TPolygonRenderer32;
class DELPHICLASS TPolygonRenderer32VPR;
class DELPHICLASS TPolygonRenderer32LCD;
class DELPHICLASS TPolygonRenderer32LCD2;
class DELPHICLASS TCustomPolygonFiller;
class DELPHICLASS TCallbackPolygonFiller;
class DELPHICLASS TInvertPolygonFiller;
class DELPHICLASS TBitmapPolygonFiller;
class DELPHICLASS TSamplerFiller;
//-- type declarations -------------------------------------------------------
enum DECLSPEC_DENUM TJoinStyle : unsigned char { jsMiter, jsBevel, jsRound };

enum DECLSPEC_DENUM TEndStyle : unsigned char { esButt, esSquare, esRound };

enum DECLSPEC_DENUM TPolyFillMode : unsigned char { pfAlternate, pfWinding, pfEvenOdd = 0x0, pfNonZero };

class PASCALIMPLEMENTATION TCustomPolygonRenderer : public Gr32::TThreadPersistent
{
	typedef Gr32::TThreadPersistent inherited;
	
public:
	__fastcall virtual TCustomPolygonRenderer(void);
	virtual void __fastcall PolyPolygonFS(const Gr32::TArrayOfArrayOfFloatPoint Points, const Gr32::TFloatRect &ClipRect, Gr32_transforms::TTransformation* Transformation)/* overload */;
	virtual void __fastcall PolyPolygonFS(const Gr32::TArrayOfArrayOfFloatPoint Points, const Gr32::TFloatRect &ClipRect)/* overload */;
	virtual void __fastcall PolygonFS(const Gr32::TArrayOfFloatPoint Points, const Gr32::TFloatRect &ClipRect, Gr32_transforms::TTransformation* Transformation)/* overload */;
	virtual void __fastcall PolygonFS(const Gr32::TArrayOfFloatPoint Points, const Gr32::TFloatRect &ClipRect)/* overload */;
public:
	/* TThreadPersistent.Destroy */ inline __fastcall virtual ~TCustomPolygonRenderer(void) { }
	
};


typedef System::TMetaClass* TCustomPolygonRendererClass;

class PASCALIMPLEMENTATION TPolygonRenderer32 : public TCustomPolygonRenderer
{
	typedef TCustomPolygonRenderer inherited;
	
private:
	Gr32::TBitmap32* FBitmap;
	TPolyFillMode FFillMode;
	Gr32::TColor32 FColor;
	TCustomPolygonFiller* FFiller;
	void __fastcall SetColor(const Gr32::TColor32 Value);
	void __fastcall SetFillMode(const TPolyFillMode Value);
	void __fastcall SetFiller(TCustomPolygonFiller* const Value);
	
protected:
	virtual void __fastcall SetBitmap(Gr32::TBitmap32* const Value);
	
public:
	__fastcall TPolygonRenderer32(Gr32::TBitmap32* Bitmap, TPolyFillMode Fillmode)/* overload */;
	HIDESBASE virtual void __fastcall PolygonFS(const Gr32::TArrayOfFloatPoint Points)/* overload */;
	HIDESBASE virtual void __fastcall PolyPolygonFS(const Gr32::TArrayOfArrayOfFloatPoint Points)/* overload */;
	__property Gr32::TBitmap32* Bitmap = {read=FBitmap, write=SetBitmap};
	__property TPolyFillMode FillMode = {read=FFillMode, write=SetFillMode, nodefault};
	__property Gr32::TColor32 Color = {read=FColor, write=SetColor, nodefault};
	__property TCustomPolygonFiller* Filler = {read=FFiller, write=SetFiller};
public:
	/* TThreadPersistent.Destroy */ inline __fastcall virtual ~TPolygonRenderer32(void) { }
	
	/* Hoisted overloads: */
	
public:
	inline void __fastcall  PolygonFS(const Gr32::TArrayOfFloatPoint Points, const Gr32::TFloatRect &ClipRect, Gr32_transforms::TTransformation* Transformation){ TCustomPolygonRenderer::PolygonFS(Points, ClipRect, Transformation); }
	inline void __fastcall  PolygonFS(const Gr32::TArrayOfFloatPoint Points, const Gr32::TFloatRect &ClipRect){ TCustomPolygonRenderer::PolygonFS(Points, ClipRect); }
	inline void __fastcall  PolyPolygonFS(const Gr32::TArrayOfArrayOfFloatPoint Points, const Gr32::TFloatRect &ClipRect, Gr32_transforms::TTransformation* Transformation){ TCustomPolygonRenderer::PolyPolygonFS(Points, ClipRect, Transformation); }
	inline void __fastcall  PolyPolygonFS(const Gr32::TArrayOfArrayOfFloatPoint Points, const Gr32::TFloatRect &ClipRect){ TCustomPolygonRenderer::PolyPolygonFS(Points, ClipRect); }
	
};


typedef System::TMetaClass* TPolygonRenderer32Class;

typedef void __fastcall (*TFillProc)(Gr32::PSingleArray Coverage, Gr32::PColor32Array AlphaValues, int Count, Gr32::TColor32 Color);

class PASCALIMPLEMENTATION TPolygonRenderer32VPR : public TPolygonRenderer32
{
	typedef TPolygonRenderer32 inherited;
	
private:
	TFillProc FFillProc;
	void __fastcall UpdateFillProcs(void);
	
protected:
	virtual void __fastcall RenderSpan(const Gr32_vpr::TValueSpan &Span, int DstY);
	virtual void __fastcall FillSpan(const Gr32_vpr::TValueSpan &Span, int DstY);
	virtual Gr32_vpr::TRenderSpanEvent __fastcall GetRenderSpan(void);
	
public:
	virtual void __fastcall PolyPolygonFS(const Gr32::TArrayOfArrayOfFloatPoint Points, const Gr32::TFloatRect &ClipRect)/* overload */;
public:
	/* TPolygonRenderer32.Create */ inline __fastcall TPolygonRenderer32VPR(Gr32::TBitmap32* Bitmap, TPolyFillMode Fillmode)/* overload */ : TPolygonRenderer32(Bitmap, Fillmode) { }
	
public:
	/* TThreadPersistent.Destroy */ inline __fastcall virtual ~TPolygonRenderer32VPR(void) { }
	
	/* Hoisted overloads: */
	
public:
	inline void __fastcall  PolyPolygonFS(const Gr32::TArrayOfArrayOfFloatPoint Points){ TPolygonRenderer32::PolyPolygonFS(Points); }
	inline void __fastcall  PolyPolygonFS(const Gr32::TArrayOfArrayOfFloatPoint Points, const Gr32::TFloatRect &ClipRect, Gr32_transforms::TTransformation* Transformation){ TCustomPolygonRenderer::PolyPolygonFS(Points, ClipRect, Transformation); }
	
};


class PASCALIMPLEMENTATION TPolygonRenderer32LCD : public TPolygonRenderer32VPR
{
	typedef TPolygonRenderer32VPR inherited;
	
protected:
	virtual void __fastcall RenderSpan(const Gr32_vpr::TValueSpan &Span, int DstY);
	
public:
	virtual void __fastcall PolyPolygonFS(const Gr32::TArrayOfArrayOfFloatPoint Points, const Gr32::TFloatRect &ClipRect)/* overload */;
public:
	/* TPolygonRenderer32.Create */ inline __fastcall TPolygonRenderer32LCD(Gr32::TBitmap32* Bitmap, TPolyFillMode Fillmode)/* overload */ : TPolygonRenderer32VPR(Bitmap, Fillmode) { }
	
public:
	/* TThreadPersistent.Destroy */ inline __fastcall virtual ~TPolygonRenderer32LCD(void) { }
	
	/* Hoisted overloads: */
	
public:
	inline void __fastcall  PolyPolygonFS(const Gr32::TArrayOfArrayOfFloatPoint Points){ TPolygonRenderer32::PolyPolygonFS(Points); }
	inline void __fastcall  PolyPolygonFS(const Gr32::TArrayOfArrayOfFloatPoint Points, const Gr32::TFloatRect &ClipRect, Gr32_transforms::TTransformation* Transformation){ TCustomPolygonRenderer::PolyPolygonFS(Points, ClipRect, Transformation); }
	
};


class PASCALIMPLEMENTATION TPolygonRenderer32LCD2 : public TPolygonRenderer32LCD
{
	typedef TPolygonRenderer32LCD inherited;
	
public:
	virtual void __fastcall RenderSpan(const Gr32_vpr::TValueSpan &Span, int DstY);
public:
	/* TPolygonRenderer32.Create */ inline __fastcall TPolygonRenderer32LCD2(Gr32::TBitmap32* Bitmap, TPolyFillMode Fillmode)/* overload */ : TPolygonRenderer32LCD(Bitmap, Fillmode) { }
	
public:
	/* TThreadPersistent.Destroy */ inline __fastcall virtual ~TPolygonRenderer32LCD2(void) { }
	
};


typedef void __fastcall (__closure *TFillLineEvent)(Gr32::PColor32 Dst, int DstX, int DstY, int Length, Gr32::PColor32 AlphaValues);

#pragma pack(push,4)
class PASCALIMPLEMENTATION TCustomPolygonFiller : public System::TObject
{
	typedef System::TObject inherited;
	
protected:
	virtual TFillLineEvent __fastcall GetFillLine(void) = 0 ;
	
public:
	virtual void __fastcall BeginRendering(void);
	virtual void __fastcall EndRendering(void);
	__property TFillLineEvent FillLine = {read=GetFillLine};
public:
	/* TObject.Create */ inline __fastcall TCustomPolygonFiller(void) : System::TObject() { }
	/* TObject.Destroy */ inline __fastcall virtual ~TCustomPolygonFiller(void) { }
	
};

#pragma pack(pop)

class PASCALIMPLEMENTATION TCallbackPolygonFiller : public TCustomPolygonFiller
{
	typedef TCustomPolygonFiller inherited;
	
private:
	TFillLineEvent FFillLineEvent;
	
protected:
	virtual TFillLineEvent __fastcall GetFillLine(void);
	
public:
	__property TFillLineEvent FillLineEvent = {read=FFillLineEvent, write=FFillLineEvent};
public:
	/* TObject.Create */ inline __fastcall TCallbackPolygonFiller(void) : TCustomPolygonFiller() { }
	/* TObject.Destroy */ inline __fastcall virtual ~TCallbackPolygonFiller(void) { }
	
};


#pragma pack(push,4)
class PASCALIMPLEMENTATION TInvertPolygonFiller : public TCustomPolygonFiller
{
	typedef TCustomPolygonFiller inherited;
	
protected:
	virtual TFillLineEvent __fastcall GetFillLine(void);
	void __fastcall FillLineBlend(Gr32::PColor32 Dst, int DstX, int DstY, int Length, Gr32::PColor32 AlphaValues);
public:
	/* TObject.Create */ inline __fastcall TInvertPolygonFiller(void) : TCustomPolygonFiller() { }
	/* TObject.Destroy */ inline __fastcall virtual ~TInvertPolygonFiller(void) { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TBitmapPolygonFiller : public TCustomPolygonFiller
{
	typedef TCustomPolygonFiller inherited;
	
private:
	Gr32::TCustomBitmap32* FPattern;
	int FOffsetY;
	int FOffsetX;
	
protected:
	virtual TFillLineEvent __fastcall GetFillLine(void);
	void __fastcall FillLineOpaque(Gr32::PColor32 Dst, int DstX, int DstY, int Length, Gr32::PColor32 AlphaValues);
	void __fastcall FillLineBlend(Gr32::PColor32 Dst, int DstX, int DstY, int Length, Gr32::PColor32 AlphaValues);
	void __fastcall FillLineBlendMasterAlpha(Gr32::PColor32 Dst, int DstX, int DstY, int Length, Gr32::PColor32 AlphaValues);
	void __fastcall FillLineCustomCombine(Gr32::PColor32 Dst, int DstX, int DstY, int Length, Gr32::PColor32 AlphaValues);
	
public:
	__property Gr32::TCustomBitmap32* Pattern = {read=FPattern, write=FPattern};
	__property int OffsetX = {read=FOffsetX, write=FOffsetX, nodefault};
	__property int OffsetY = {read=FOffsetY, write=FOffsetY, nodefault};
public:
	/* TObject.Create */ inline __fastcall TBitmapPolygonFiller(void) : TCustomPolygonFiller() { }
	/* TObject.Destroy */ inline __fastcall virtual ~TBitmapPolygonFiller(void) { }
	
};

#pragma pack(pop)

class PASCALIMPLEMENTATION TSamplerFiller : public TCustomPolygonFiller
{
	typedef TCustomPolygonFiller inherited;
	
private:
	Gr32::TCustomSampler* FSampler;
	Gr32_resamplers::TGetSampleInt FGetSample;
	void __fastcall SetSampler(Gr32::TCustomSampler* const Value);
	
protected:
	virtual void __fastcall SamplerChanged(void);
	virtual TFillLineEvent __fastcall GetFillLine(void);
	void __fastcall SampleLineOpaque(Gr32::PColor32 Dst, int DstX, int DstY, int Length, Gr32::PColor32 AlphaValues);
	
public:
	__fastcall virtual TSamplerFiller(Gr32::TCustomSampler* Sampler);
	virtual void __fastcall BeginRendering(void);
	virtual void __fastcall EndRendering(void);
	__property Gr32::TCustomSampler* Sampler = {read=FSampler, write=SetSampler};
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TSamplerFiller(void) { }
	
};


//-- var, const, procedure ---------------------------------------------------
extern DELPHI_PACKAGE Gr32_containers::TClassList* PolygonRendererList;
extern DELPHI_PACKAGE TPolygonRenderer32Class DefaultPolygonRendererClass;
extern DELPHI_PACKAGE void __fastcall RegisterPolygonRenderer(TCustomPolygonRendererClass PolygonRendererClass);
extern DELPHI_PACKAGE void __fastcall PolyPolygonFS(Gr32::TBitmap32* Bitmap, const Gr32::TArrayOfArrayOfFloatPoint Points, Gr32::TColor32 Color, TPolyFillMode FillMode = (TPolyFillMode)(0x0), Gr32_transforms::TTransformation* Transformation = (Gr32_transforms::TTransformation*)(0x0))/* overload */;
extern DELPHI_PACKAGE void __fastcall PolygonFS(Gr32::TBitmap32* Bitmap, const Gr32::TArrayOfFloatPoint Points, Gr32::TColor32 Color, TPolyFillMode FillMode = (TPolyFillMode)(0x0), Gr32_transforms::TTransformation* Transformation = (Gr32_transforms::TTransformation*)(0x0))/* overload */;
extern DELPHI_PACKAGE void __fastcall PolyPolygonFS(Gr32::TBitmap32* Bitmap, const Gr32::TArrayOfArrayOfFloatPoint Points, TCustomPolygonFiller* Filler, TPolyFillMode FillMode = (TPolyFillMode)(0x0), Gr32_transforms::TTransformation* Transformation = (Gr32_transforms::TTransformation*)(0x0))/* overload */;
extern DELPHI_PACKAGE void __fastcall PolygonFS(Gr32::TBitmap32* Bitmap, const Gr32::TArrayOfFloatPoint Points, TCustomPolygonFiller* Filler, TPolyFillMode FillMode = (TPolyFillMode)(0x0), Gr32_transforms::TTransformation* Transformation = (Gr32_transforms::TTransformation*)(0x0))/* overload */;
extern DELPHI_PACKAGE void __fastcall PolygonFS_LCD(Gr32::TBitmap32* Bitmap, const Gr32::TArrayOfFloatPoint Points, Gr32::TColor32 Color, TPolyFillMode FillMode = (TPolyFillMode)(0x0), Gr32_transforms::TTransformation* Transformation = (Gr32_transforms::TTransformation*)(0x0))/* overload */;
extern DELPHI_PACKAGE void __fastcall PolyPolygonFS_LCD(Gr32::TBitmap32* Bitmap, const Gr32::TArrayOfArrayOfFloatPoint Points, Gr32::TColor32 Color, TPolyFillMode FillMode = (TPolyFillMode)(0x0), Gr32_transforms::TTransformation* Transformation = (Gr32_transforms::TTransformation*)(0x0))/* overload */;
extern DELPHI_PACKAGE void __fastcall PolygonFS_LCD2(Gr32::TBitmap32* Bitmap, const Gr32::TArrayOfFloatPoint Points, Gr32::TColor32 Color, TPolyFillMode FillMode = (TPolyFillMode)(0x0), Gr32_transforms::TTransformation* Transformation = (Gr32_transforms::TTransformation*)(0x0))/* overload */;
extern DELPHI_PACKAGE void __fastcall PolyPolygonFS_LCD2(Gr32::TBitmap32* Bitmap, const Gr32::TArrayOfArrayOfFloatPoint Points, Gr32::TColor32 Color, TPolyFillMode FillMode = (TPolyFillMode)(0x0), Gr32_transforms::TTransformation* Transformation = (Gr32_transforms::TTransformation*)(0x0))/* overload */;
extern DELPHI_PACKAGE void __fastcall PolyPolygonFS(Gr32::TBitmap32* Bitmap, const Gr32::TArrayOfArrayOfFloatPoint Points, const System::Types::TRect &ClipRect, Gr32::TColor32 Color, TPolyFillMode FillMode = (TPolyFillMode)(0x0), Gr32_transforms::TTransformation* Transformation = (Gr32_transforms::TTransformation*)(0x0))/* overload */;
extern DELPHI_PACKAGE void __fastcall PolygonFS(Gr32::TBitmap32* Bitmap, const Gr32::TArrayOfFloatPoint Points, const System::Types::TRect &ClipRect, Gr32::TColor32 Color, TPolyFillMode FillMode = (TPolyFillMode)(0x0), Gr32_transforms::TTransformation* Transformation = (Gr32_transforms::TTransformation*)(0x0))/* overload */;
extern DELPHI_PACKAGE void __fastcall PolyPolygonFS(Gr32::TBitmap32* Bitmap, const Gr32::TArrayOfArrayOfFloatPoint Points, const System::Types::TRect &ClipRect, TCustomPolygonFiller* Filler, TPolyFillMode FillMode = (TPolyFillMode)(0x0), Gr32_transforms::TTransformation* Transformation = (Gr32_transforms::TTransformation*)(0x0))/* overload */;
extern DELPHI_PACKAGE void __fastcall PolygonFS(Gr32::TBitmap32* Bitmap, const Gr32::TArrayOfFloatPoint Points, const System::Types::TRect &ClipRect, TCustomPolygonFiller* Filler, TPolyFillMode FillMode = (TPolyFillMode)(0x0), Gr32_transforms::TTransformation* Transformation = (Gr32_transforms::TTransformation*)(0x0))/* overload */;
extern DELPHI_PACKAGE void __fastcall PolygonFS_LCD(Gr32::TBitmap32* Bitmap, const Gr32::TArrayOfFloatPoint Points, const System::Types::TRect &ClipRect, Gr32::TColor32 Color, TPolyFillMode FillMode = (TPolyFillMode)(0x0), Gr32_transforms::TTransformation* Transformation = (Gr32_transforms::TTransformation*)(0x0))/* overload */;
extern DELPHI_PACKAGE void __fastcall PolyPolygonFS_LCD(Gr32::TBitmap32* Bitmap, const Gr32::TArrayOfArrayOfFloatPoint Points, const System::Types::TRect &ClipRect, Gr32::TColor32 Color, TPolyFillMode FillMode = (TPolyFillMode)(0x0), Gr32_transforms::TTransformation* Transformation = (Gr32_transforms::TTransformation*)(0x0))/* overload */;
extern DELPHI_PACKAGE void __fastcall PolygonFS_LCD2(Gr32::TBitmap32* Bitmap, const Gr32::TArrayOfFloatPoint Points, const System::Types::TRect &ClipRect, Gr32::TColor32 Color, TPolyFillMode FillMode = (TPolyFillMode)(0x0), Gr32_transforms::TTransformation* Transformation = (Gr32_transforms::TTransformation*)(0x0))/* overload */;
extern DELPHI_PACKAGE void __fastcall PolyPolygonFS_LCD2(Gr32::TBitmap32* Bitmap, const Gr32::TArrayOfArrayOfFloatPoint Points, const System::Types::TRect &ClipRect, Gr32::TColor32 Color, TPolyFillMode FillMode = (TPolyFillMode)(0x0), Gr32_transforms::TTransformation* Transformation = (Gr32_transforms::TTransformation*)(0x0))/* overload */;
extern DELPHI_PACKAGE void __fastcall PolyPolylineFS(Gr32::TBitmap32* Bitmap, const Gr32::TArrayOfArrayOfFloatPoint Points, Gr32::TColor32 Color, bool Closed = false, float StrokeWidth = 1.000000E+00f, TJoinStyle JoinStyle = (TJoinStyle)(0x0), TEndStyle EndStyle = (TEndStyle)(0x0), float MiterLimit = 4.000000E+00f, Gr32_transforms::TTransformation* Transformation = (Gr32_transforms::TTransformation*)(0x0))/* overload */;
extern DELPHI_PACKAGE void __fastcall PolyPolylineFS(Gr32::TBitmap32* Bitmap, const Gr32::TArrayOfArrayOfFloatPoint Points, TCustomPolygonFiller* Filler, bool Closed = false, float StrokeWidth = 1.000000E+00f, TJoinStyle JoinStyle = (TJoinStyle)(0x0), TEndStyle EndStyle = (TEndStyle)(0x0), float MiterLimit = 4.000000E+00f, Gr32_transforms::TTransformation* Transformation = (Gr32_transforms::TTransformation*)(0x0))/* overload */;
extern DELPHI_PACKAGE void __fastcall PolylineFS(Gr32::TBitmap32* Bitmap, const Gr32::TArrayOfFloatPoint Points, Gr32::TColor32 Color, bool Closed = false, float StrokeWidth = 1.000000E+00f, TJoinStyle JoinStyle = (TJoinStyle)(0x0), TEndStyle EndStyle = (TEndStyle)(0x0), float MiterLimit = 4.000000E+00f, Gr32_transforms::TTransformation* Transformation = (Gr32_transforms::TTransformation*)(0x0))/* overload */;
extern DELPHI_PACKAGE void __fastcall PolylineFS(Gr32::TBitmap32* Bitmap, const Gr32::TArrayOfFloatPoint Points, TCustomPolygonFiller* Filler, bool Closed = false, float StrokeWidth = 1.000000E+00f, TJoinStyle JoinStyle = (TJoinStyle)(0x0), TEndStyle EndStyle = (TEndStyle)(0x0), float MiterLimit = 4.000000E+00f, Gr32_transforms::TTransformation* Transformation = (Gr32_transforms::TTransformation*)(0x0))/* overload */;
extern DELPHI_PACKAGE void __fastcall DashLineFS(Gr32::TBitmap32* Bitmap, const Gr32::TArrayOfFloatPoint Points, const Gr32::TArrayOfFloat Dashes, Gr32::TColor32 Color, bool Closed = false, float Width = 1.000000E+00f)/* overload */;
extern DELPHI_PACKAGE void __fastcall DashLineFS(Gr32::TBitmap32* Bitmap, const Gr32::TArrayOfFloatPoint Points, const Gr32::TArrayOfFloat Dashes, Gr32::TColor32 FillColor, Gr32::TColor32 StrokeColor, bool Closed, float Width, float StrokeWidth = 2.000000E+00f)/* overload */;
extern DELPHI_PACKAGE void __fastcall DashLineFS(Gr32::TBitmap32* Bitmap, const Gr32::TArrayOfFloatPoint Points, const Gr32::TArrayOfFloat Dashes, TCustomPolygonFiller* Filler, bool Closed = false, float Width = 1.000000E+00f)/* overload */;
extern DELPHI_PACKAGE void __fastcall DashLineFS(Gr32::TBitmap32* Bitmap, const Gr32::TArrayOfFloatPoint Points, const Gr32::TArrayOfFloat Dashes, TCustomPolygonFiller* Filler, Gr32::TColor32 StrokeColor, bool Closed, float Width, float StrokeWidth = 2.000000E+00f)/* overload */;
extern DELPHI_PACKAGE void __fastcall PolyPolygonXS(Gr32::TBitmap32* Bitmap, const Gr32::TArrayOfArrayOfFixedPoint Points, Gr32::TColor32 Color, TPolyFillMode FillMode = (TPolyFillMode)(0x0), Gr32_transforms::TTransformation* Transformation = (Gr32_transforms::TTransformation*)(0x0))/* overload */;
extern DELPHI_PACKAGE void __fastcall PolygonXS(Gr32::TBitmap32* Bitmap, const Gr32::TArrayOfFixedPoint Points, Gr32::TColor32 Color, TPolyFillMode FillMode = (TPolyFillMode)(0x0), Gr32_transforms::TTransformation* Transformation = (Gr32_transforms::TTransformation*)(0x0))/* overload */;
extern DELPHI_PACKAGE void __fastcall PolyPolygonXS(Gr32::TBitmap32* Bitmap, const Gr32::TArrayOfArrayOfFixedPoint Points, TCustomPolygonFiller* Filler, TPolyFillMode FillMode = (TPolyFillMode)(0x0), Gr32_transforms::TTransformation* Transformation = (Gr32_transforms::TTransformation*)(0x0))/* overload */;
extern DELPHI_PACKAGE void __fastcall PolygonXS(Gr32::TBitmap32* Bitmap, const Gr32::TArrayOfFixedPoint Points, TCustomPolygonFiller* Filler, TPolyFillMode FillMode = (TPolyFillMode)(0x0), Gr32_transforms::TTransformation* Transformation = (Gr32_transforms::TTransformation*)(0x0))/* overload */;
extern DELPHI_PACKAGE void __fastcall PolygonXS_LCD(Gr32::TBitmap32* Bitmap, const Gr32::TArrayOfFixedPoint Points, Gr32::TColor32 Color, TPolyFillMode FillMode = (TPolyFillMode)(0x0), Gr32_transforms::TTransformation* Transformation = (Gr32_transforms::TTransformation*)(0x0));
extern DELPHI_PACKAGE void __fastcall PolyPolygonXS_LCD(Gr32::TBitmap32* Bitmap, const Gr32::TArrayOfArrayOfFixedPoint Points, Gr32::TColor32 Color, TPolyFillMode FillMode = (TPolyFillMode)(0x0), Gr32_transforms::TTransformation* Transformation = (Gr32_transforms::TTransformation*)(0x0))/* overload */;
extern DELPHI_PACKAGE void __fastcall PolygonXS_LCD2(Gr32::TBitmap32* Bitmap, const Gr32::TArrayOfFixedPoint Points, Gr32::TColor32 Color, TPolyFillMode FillMode = (TPolyFillMode)(0x0), Gr32_transforms::TTransformation* Transformation = (Gr32_transforms::TTransformation*)(0x0));
extern DELPHI_PACKAGE void __fastcall PolyPolygonXS_LCD2(Gr32::TBitmap32* Bitmap, const Gr32::TArrayOfArrayOfFixedPoint Points, Gr32::TColor32 Color, TPolyFillMode FillMode = (TPolyFillMode)(0x0), Gr32_transforms::TTransformation* Transformation = (Gr32_transforms::TTransformation*)(0x0))/* overload */;
extern DELPHI_PACKAGE void __fastcall PolyPolylineXS(Gr32::TBitmap32* Bitmap, const Gr32::TArrayOfArrayOfFixedPoint Points, Gr32::TColor32 Color, bool Closed = false, Gr32::TFixed StrokeWidth = (Gr32::TFixed)(0x10000), TJoinStyle JoinStyle = (TJoinStyle)(0x0), TEndStyle EndStyle = (TEndStyle)(0x0), Gr32::TFixed MiterLimit = (Gr32::TFixed)(0x40000), Gr32_transforms::TTransformation* Transformation = (Gr32_transforms::TTransformation*)(0x0))/* overload */;
extern DELPHI_PACKAGE void __fastcall PolyPolylineXS(Gr32::TBitmap32* Bitmap, const Gr32::TArrayOfArrayOfFixedPoint Points, TCustomPolygonFiller* Filler, bool Closed = false, Gr32::TFixed StrokeWidth = (Gr32::TFixed)(0x10000), TJoinStyle JoinStyle = (TJoinStyle)(0x0), TEndStyle EndStyle = (TEndStyle)(0x0), Gr32::TFixed MiterLimit = (Gr32::TFixed)(0x40000), Gr32_transforms::TTransformation* Transformation = (Gr32_transforms::TTransformation*)(0x0))/* overload */;
extern DELPHI_PACKAGE void __fastcall PolylineXS(Gr32::TBitmap32* Bitmap, const Gr32::TArrayOfFixedPoint Points, Gr32::TColor32 Color, bool Closed = false, Gr32::TFixed StrokeWidth = (Gr32::TFixed)(0x10000), TJoinStyle JoinStyle = (TJoinStyle)(0x0), TEndStyle EndStyle = (TEndStyle)(0x0), Gr32::TFixed MiterLimit = (Gr32::TFixed)(0x40000), Gr32_transforms::TTransformation* Transformation = (Gr32_transforms::TTransformation*)(0x0))/* overload */;
extern DELPHI_PACKAGE void __fastcall PolylineXS(Gr32::TBitmap32* Bitmap, const Gr32::TArrayOfFixedPoint Points, TCustomPolygonFiller* Filler, bool Closed = false, Gr32::TFixed StrokeWidth = (Gr32::TFixed)(0x10000), TJoinStyle JoinStyle = (TJoinStyle)(0x0), TEndStyle EndStyle = (TEndStyle)(0x0), Gr32::TFixed MiterLimit = (Gr32::TFixed)(0x40000), Gr32_transforms::TTransformation* Transformation = (Gr32_transforms::TTransformation*)(0x0))/* overload */;
extern DELPHI_PACKAGE void __fastcall DashLineXS(Gr32::TBitmap32* Bitmap, const Gr32::TArrayOfFixedPoint Points, const Gr32::TArrayOfFixed Dashes, Gr32::TColor32 Color, bool Closed = false, Gr32::TFixed Width = (Gr32::TFixed)(0x10000))/* overload */;
extern DELPHI_PACKAGE void __fastcall DashLineXS(Gr32::TBitmap32* Bitmap, const Gr32::TArrayOfFixedPoint Points, const Gr32::TArrayOfFixed Dashes, Gr32::TColor32 FillColor, Gr32::TColor32 StrokeColor, bool Closed, Gr32::TFixed Width, Gr32::TFixed StrokeWidth = (Gr32::TFixed)(0x20000))/* overload */;
extern DELPHI_PACKAGE void __fastcall DashLineXS(Gr32::TBitmap32* Bitmap, const Gr32::TArrayOfFixedPoint Points, const Gr32::TArrayOfFixed Dashes, TCustomPolygonFiller* Filler, bool Closed = false, Gr32::TFixed Width = (Gr32::TFixed)(0x10000))/* overload */;
extern DELPHI_PACKAGE void __fastcall DashLineXS(Gr32::TBitmap32* Bitmap, const Gr32::TArrayOfFixedPoint Points, const Gr32::TArrayOfFixed Dashes, TCustomPolygonFiller* Filler, Gr32::TColor32 StrokeColor, bool Closed, Gr32::TFixed Width, Gr32::TFixed StrokeWidth = (Gr32::TFixed)(0x20000))/* overload */;
extern DELPHI_PACKAGE void __fastcall FillBitmap(Gr32::TBitmap32* Bitmap, TCustomPolygonFiller* Filler);
}	/* namespace Gr32_polygons */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_GR32_POLYGONS)
using namespace Gr32_polygons;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// Gr32_polygonsHPP
