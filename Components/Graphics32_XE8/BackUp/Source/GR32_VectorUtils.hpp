﻿// CodeGear C++Builder
// Copyright (c) 1995, 2016 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'GR32_VectorUtils.pas' rev: 31.00 (Windows)

#ifndef Gr32_vectorutilsHPP
#define Gr32_vectorutilsHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>
#include <SysInit.hpp>
#include <GR32.hpp>
#include <GR32_Transforms.hpp>
#include <GR32_Polygons.hpp>

//-- user supplied -----------------------------------------------------------

namespace Gr32_vectorutils
{
//-- forward type declarations -----------------------------------------------
//-- type declarations -------------------------------------------------------
typedef System::StaticArray<int, 3> TTriangleVertexIndices;

typedef System::DynamicArray<TTriangleVertexIndices> TArrayOfTriangleVertexIndices;

//-- var, const, procedure ---------------------------------------------------
#define DEFAULT_MITER_LIMIT  (4.000000E+00)
static const int DEFAULT_MITER_LIMIT_FIXED = int(0x40000);
static const System::Extended TWOPI = 6.283185E+00;
extern DELPHI_PACKAGE bool __fastcall InSignedRange(const float X, const float X1, const float X2)/* overload */;
extern DELPHI_PACKAGE bool __fastcall InSignedRange(const Gr32::TFixed X, const Gr32::TFixed X1, const Gr32::TFixed X2)/* overload */;
extern DELPHI_PACKAGE bool __fastcall OverlapExclusive(const float X1, const float X2, const float Y1, const float Y2)/* overload */;
extern DELPHI_PACKAGE bool __fastcall OverlapExclusive(const Gr32::TFloatPoint &Pt1, const Gr32::TFloatPoint &Pt2)/* overload */;
extern DELPHI_PACKAGE bool __fastcall OverlapExclusive(const Gr32::TFixed X1, const Gr32::TFixed X2, const Gr32::TFixed Y1, const Gr32::TFixed Y2)/* overload */;
extern DELPHI_PACKAGE bool __fastcall OverlapExclusive(const Gr32::TFixedPoint &Pt1, const Gr32::TFixedPoint &Pt2)/* overload */;
extern DELPHI_PACKAGE bool __fastcall OverlapInclusive(const float X1, const float X2, const float Y1, const float Y2)/* overload */;
extern DELPHI_PACKAGE bool __fastcall OverlapInclusive(const Gr32::TFloatPoint &Pt1, const Gr32::TFloatPoint &Pt2)/* overload */;
extern DELPHI_PACKAGE bool __fastcall OverlapInclusive(const Gr32::TFixed X1, const Gr32::TFixed X2, const Gr32::TFixed Y1, const Gr32::TFixed Y2)/* overload */;
extern DELPHI_PACKAGE bool __fastcall OverlapInclusive(const Gr32::TFixedPoint &Pt1, const Gr32::TFixedPoint &Pt2)/* overload */;
extern DELPHI_PACKAGE bool __fastcall Intersect(const Gr32::TFloatPoint &A1, const Gr32::TFloatPoint &A2, const Gr32::TFloatPoint &B1, const Gr32::TFloatPoint &B2, /* out */ Gr32::TFloatPoint &P)/* overload */;
extern DELPHI_PACKAGE bool __fastcall Intersect(const Gr32::TFixedPoint &A1, const Gr32::TFixedPoint &A2, const Gr32::TFixedPoint &B1, const Gr32::TFixedPoint &B2, /* out */ Gr32::TFixedPoint &P)/* overload */;
extern DELPHI_PACKAGE int __fastcall FindNearestPointIndex(const Gr32::TFloatPoint &Point, Gr32::TArrayOfFloatPoint Points)/* overload */;
extern DELPHI_PACKAGE int __fastcall FindNearestPointIndex(const Gr32::TFixedPoint &Point, Gr32::TArrayOfFixedPoint Points)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfFloatPoint __fastcall VertexReduction(Gr32::TArrayOfFloatPoint Points, float Epsilon = 1.000000E+00f)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfFixedPoint __fastcall VertexReduction(Gr32::TArrayOfFixedPoint Points, Gr32::TFixed Epsilon = (Gr32::TFixed)(0x10000))/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfFloatPoint __fastcall ClosePolygon(const Gr32::TArrayOfFloatPoint Points)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfFixedPoint __fastcall ClosePolygon(const Gr32::TArrayOfFixedPoint Points)/* overload */;
extern DELPHI_PACKAGE bool __fastcall ClipLine(int &X1, int &Y1, int &X2, int &Y2, int MinX, int MinY, int MaxX, int MaxY)/* overload */;
extern DELPHI_PACKAGE bool __fastcall ClipLine(float &X1, float &Y1, float &X2, float &Y2, float MinX, float MinY, float MaxX, float MaxY)/* overload */;
extern DELPHI_PACKAGE bool __fastcall ClipLine(Gr32::TFixed &X1, Gr32::TFixed &Y1, Gr32::TFixed &X2, Gr32::TFixed &Y2, Gr32::TFixed MinX, Gr32::TFixed MinY, Gr32::TFixed MaxX, Gr32::TFixed MaxY)/* overload */;
extern DELPHI_PACKAGE bool __fastcall ClipLine(Gr32::TFloatPoint &P1, Gr32::TFloatPoint &P2, const Gr32::TFloatRect &ClipRect)/* overload */;
extern DELPHI_PACKAGE bool __fastcall ClipLine(Gr32::TFixedPoint &P1, Gr32::TFixedPoint &P2, const Gr32::TFixedRect &ClipRect)/* overload */;
extern DELPHI_PACKAGE void __fastcall Extract(Gr32::TArrayOfFloat Src, Gr32::TArrayOfInteger Indexes, /* out */ Gr32::TArrayOfFloat &Dst)/* overload */;
extern DELPHI_PACKAGE void __fastcall Extract(Gr32::TArrayOfFixed Src, Gr32::TArrayOfInteger Indexes, /* out */ Gr32::TArrayOfFixed &Dst)/* overload */;
extern DELPHI_PACKAGE void __fastcall FastMergeSort(const Gr32::TArrayOfFloat Values, /* out */ Gr32::TArrayOfInteger &Indexes)/* overload */;
extern DELPHI_PACKAGE void __fastcall FastMergeSort(const Gr32::TArrayOfFixed Values, /* out */ Gr32::TArrayOfInteger &Indexes)/* overload */;
extern DELPHI_PACKAGE TArrayOfTriangleVertexIndices __fastcall DelaunayTriangulation(Gr32::TArrayOfFloatPoint Points);
extern DELPHI_PACKAGE Gr32::TArrayOfFloatPoint __fastcall BuildArc(const Gr32::TFloatPoint &P, float StartAngle, float EndAngle, float Radius, int Steps)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfFloatPoint __fastcall BuildArc(const Gr32::TFloatPoint &P, float StartAngle, float EndAngle, float Radius)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfFixedPoint __fastcall BuildArc(const Gr32::TFixedPoint &P, float StartAngle, float EndAngle, float Radius, int Steps)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfFixedPoint __fastcall BuildArc(const Gr32::TFixedPoint &P, float StartAngle, float EndAngle, float Radius)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfFloatPoint __fastcall Line(const Gr32::TFloatPoint &P1, const Gr32::TFloatPoint &P2)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfFloatPoint __fastcall Line(const float X1, const float Y1, const float X2, const float Y2)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfFloatPoint __fastcall VertLine(const float X, const float Y1, const float Y2);
extern DELPHI_PACKAGE Gr32::TArrayOfFloatPoint __fastcall HorzLine(const float X1, const float Y, const float X2);
extern DELPHI_PACKAGE unsigned __fastcall CalculateCircleSteps(float Radius);
extern DELPHI_PACKAGE Gr32::TArrayOfFloatPoint __fastcall Circle(const Gr32::TFloatPoint &P, const float Radius, int Steps)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfFloatPoint __fastcall Circle(const Gr32::TFloatPoint &P, const float Radius)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfFloatPoint __fastcall Circle(const float X, const float Y, const float Radius, int Steps)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfFloatPoint __fastcall Circle(const float X, const float Y, const float Radius)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfFloatPoint __fastcall Pie(const Gr32::TFloatPoint &P, const float Radius, const float Angle, const float Offset, int Steps)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfFloatPoint __fastcall Pie(const Gr32::TFloatPoint &P, const float Radius, const float Angle, const float Offset = 0.000000E+00f)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfFloatPoint __fastcall Pie(const Gr32::TFloatPoint &P, const float Radius, const float Angle, int Steps)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfFloatPoint __fastcall Pie(const float X, const float Y, const float Radius, const float Angle, const float Offset = 0.000000E+00f)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfFloatPoint __fastcall Pie(const float X, const float Y, const float Radius, const float Angle, const float Offset, int Steps)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfFloatPoint __fastcall Pie(const float X, const float Y, const float Radius, const float Angle, int Steps)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfFloatPoint __fastcall Ellipse(const Gr32::TFloatPoint &P, const Gr32::TFloatPoint &R, int Steps)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfFloatPoint __fastcall Ellipse(const Gr32::TFloatPoint &P, const Gr32::TFloatPoint &R)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfFloatPoint __fastcall Ellipse(const float X, const float Y, const float Rx, const float Ry, int Steps)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfFloatPoint __fastcall Ellipse(const float X, const float Y, const float Rx, const float Ry)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfFloatPoint __fastcall Star(const float X, const float Y, const float Radius, int Vertices = 0x5, float Rotation = 0.000000E+00f)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfFloatPoint __fastcall Star(const Gr32::TFloatPoint &P, const float Radius, int Vertices = 0x5, float Rotation = 0.000000E+00f)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfFloatPoint __fastcall Star(const float X, const float Y, const float InnerRadius, const float OuterRadius, int Vertices = 0x5, float Rotation = 0.000000E+00f)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfFloatPoint __fastcall Star(const Gr32::TFloatPoint &P, const float InnerRadius, const float OuterRadius, int Vertices = 0x5, float Rotation = 0.000000E+00f)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfFloatPoint __fastcall Rectangle(const Gr32::TFloatRect &R);
extern DELPHI_PACKAGE Gr32::TArrayOfFloatPoint __fastcall RoundRect(const Gr32::TFloatRect &R, const float Radius);
extern DELPHI_PACKAGE Gr32::TArrayOfFloatPoint __fastcall BuildNormals(const Gr32::TArrayOfFloatPoint Points)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfFixedPoint __fastcall BuildNormals(const Gr32::TArrayOfFixedPoint Points)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfFloatPoint __fastcall Grow(const Gr32::TArrayOfFloatPoint Points, const Gr32::TArrayOfFloatPoint Normals, const float Delta, Gr32_polygons::TJoinStyle JoinStyle = (Gr32_polygons::TJoinStyle)(0x0), bool Closed = true, float MiterLimit = 4.000000E+00f)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfFloatPoint __fastcall Grow(const Gr32::TArrayOfFloatPoint Points, const float Delta, Gr32_polygons::TJoinStyle JoinStyle = (Gr32_polygons::TJoinStyle)(0x0), bool Closed = true, float MiterLimit = 4.000000E+00f)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfFixedPoint __fastcall Grow(const Gr32::TArrayOfFixedPoint Points, const Gr32::TArrayOfFixedPoint Normals, const Gr32::TFixed Delta, Gr32_polygons::TJoinStyle JoinStyle = (Gr32_polygons::TJoinStyle)(0x0), bool Closed = true, Gr32::TFixed MiterLimit = (Gr32::TFixed)(0x40000))/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfFixedPoint __fastcall Grow(const Gr32::TArrayOfFixedPoint Points, const Gr32::TFixed Delta, Gr32_polygons::TJoinStyle JoinStyle = (Gr32_polygons::TJoinStyle)(0x0), bool Closed = true, Gr32::TFixed MiterLimit = (Gr32::TFixed)(0x40000))/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfFloatPoint __fastcall ReversePolygon(const Gr32::TArrayOfFloatPoint Points)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfFixedPoint __fastcall ReversePolygon(const Gr32::TArrayOfFixedPoint Points)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfFloatPoint __fastcall BuildPolyline(const Gr32::TArrayOfFloatPoint Points, float StrokeWidth, Gr32_polygons::TJoinStyle JoinStyle = (Gr32_polygons::TJoinStyle)(0x0), Gr32_polygons::TEndStyle EndStyle = (Gr32_polygons::TEndStyle)(0x0), float MiterLimit = 4.000000E+00f)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfArrayOfFloatPoint __fastcall BuildPolyPolyLine(const Gr32::TArrayOfArrayOfFloatPoint Points, bool Closed, float StrokeWidth, Gr32_polygons::TJoinStyle JoinStyle = (Gr32_polygons::TJoinStyle)(0x0), Gr32_polygons::TEndStyle EndStyle = (Gr32_polygons::TEndStyle)(0x0), float MiterLimit = 4.000000E+00f)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfFixedPoint __fastcall BuildPolyline(const Gr32::TArrayOfFixedPoint Points, Gr32::TFixed StrokeWidth, Gr32_polygons::TJoinStyle JoinStyle = (Gr32_polygons::TJoinStyle)(0x0), Gr32_polygons::TEndStyle EndStyle = (Gr32_polygons::TEndStyle)(0x0), Gr32::TFixed MiterLimit = (Gr32::TFixed)(0x40000))/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfArrayOfFixedPoint __fastcall BuildPolyPolyLine(const Gr32::TArrayOfArrayOfFixedPoint Points, bool Closed, Gr32::TFixed StrokeWidth, Gr32_polygons::TJoinStyle JoinStyle = (Gr32_polygons::TJoinStyle)(0x0), Gr32_polygons::TEndStyle EndStyle = (Gr32_polygons::TEndStyle)(0x0), Gr32::TFixed MiterLimit = (Gr32::TFixed)(0x40000))/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfArrayOfFloatPoint __fastcall BuildDashedLine(const Gr32::TArrayOfFloatPoint Points, const Gr32::TArrayOfFloat DashArray, float DashOffset = 0.000000E+00f, bool Closed = false)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfArrayOfFixedPoint __fastcall BuildDashedLine(const Gr32::TArrayOfFixedPoint Points, const Gr32::TArrayOfFixed DashArray, Gr32::TFixed DashOffset = (Gr32::TFixed)(0x0), bool Closed = false)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfFloatPoint __fastcall ClipPolygon(const Gr32::TArrayOfFloatPoint Points, const Gr32::TFloatRect &ClipRect)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfFixedPoint __fastcall ClipPolygon(const Gr32::TArrayOfFixedPoint Points, const Gr32::TFixedRect &ClipRect)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfArrayOfFloatPoint __fastcall CatPolyPolygon(const Gr32::TArrayOfArrayOfFloatPoint P1, const Gr32::TArrayOfArrayOfFloatPoint P2)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfArrayOfFixedPoint __fastcall CatPolyPolygon(const Gr32::TArrayOfArrayOfFixedPoint P1, const Gr32::TArrayOfArrayOfFixedPoint P2)/* overload */;
extern DELPHI_PACKAGE Gr32::TFloatRect __fastcall PolygonBounds(const Gr32::TArrayOfFloatPoint Points)/* overload */;
extern DELPHI_PACKAGE Gr32::TFixedRect __fastcall PolygonBounds(const Gr32::TArrayOfFixedPoint Points)/* overload */;
extern DELPHI_PACKAGE Gr32::TFloatRect __fastcall PolypolygonBounds(const Gr32::TArrayOfArrayOfFloatPoint Points)/* overload */;
extern DELPHI_PACKAGE Gr32::TFixedRect __fastcall PolypolygonBounds(const Gr32::TArrayOfArrayOfFixedPoint Points)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfFloatPoint __fastcall ScalePolygon(const Gr32::TArrayOfFloatPoint Points, float ScaleX, float ScaleY)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfFixedPoint __fastcall ScalePolygon(const Gr32::TArrayOfFixedPoint Points, Gr32::TFixed ScaleX, Gr32::TFixed ScaleY)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfArrayOfFloatPoint __fastcall ScalePolyPolygon(const Gr32::TArrayOfArrayOfFloatPoint Points, float ScaleX, float ScaleY)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfArrayOfFixedPoint __fastcall ScalePolyPolygon(const Gr32::TArrayOfArrayOfFixedPoint Points, Gr32::TFixed ScaleX, Gr32::TFixed ScaleY)/* overload */;
extern DELPHI_PACKAGE void __fastcall ScalePolygonInplace(const Gr32::TArrayOfFloatPoint Points, float ScaleX, float ScaleY)/* overload */;
extern DELPHI_PACKAGE void __fastcall ScalePolygonInplace(const Gr32::TArrayOfFixedPoint Points, Gr32::TFixed ScaleX, Gr32::TFixed ScaleY)/* overload */;
extern DELPHI_PACKAGE void __fastcall ScalePolyPolygonInplace(const Gr32::TArrayOfArrayOfFloatPoint Points, float ScaleX, float ScaleY)/* overload */;
extern DELPHI_PACKAGE void __fastcall ScalePolyPolygonInplace(const Gr32::TArrayOfArrayOfFixedPoint Points, Gr32::TFixed ScaleX, Gr32::TFixed ScaleY)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfFloatPoint __fastcall TranslatePolygon(const Gr32::TArrayOfFloatPoint Points, float OffsetX, float OffsetY)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfFixedPoint __fastcall TranslatePolygon(const Gr32::TArrayOfFixedPoint Points, Gr32::TFixed Offsetx, Gr32::TFixed OffsetY)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfArrayOfFloatPoint __fastcall TranslatePolyPolygon(const Gr32::TArrayOfArrayOfFloatPoint Points, float OffsetX, float OffsetY)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfArrayOfFixedPoint __fastcall TranslatePolyPolygon(const Gr32::TArrayOfArrayOfFixedPoint Points, Gr32::TFixed OffsetX, Gr32::TFixed OffsetY)/* overload */;
extern DELPHI_PACKAGE void __fastcall TranslatePolygonInplace(const Gr32::TArrayOfFloatPoint Points, float OffsetX, float OffsetY)/* overload */;
extern DELPHI_PACKAGE void __fastcall TranslatePolygonInplace(const Gr32::TArrayOfFixedPoint Points, Gr32::TFixed Offsetx, Gr32::TFixed OffsetY)/* overload */;
extern DELPHI_PACKAGE void __fastcall TranslatePolyPolygonInplace(const Gr32::TArrayOfArrayOfFloatPoint Points, float OffsetX, float OffsetY)/* overload */;
extern DELPHI_PACKAGE void __fastcall TranslatePolyPolygonInplace(const Gr32::TArrayOfArrayOfFixedPoint Points, Gr32::TFixed OffsetX, Gr32::TFixed OffsetY)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfFloatPoint __fastcall TransformPolygon(const Gr32::TArrayOfFloatPoint Points, Gr32_transforms::TTransformation* Transformation)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfFixedPoint __fastcall TransformPolygon(const Gr32::TArrayOfFixedPoint Points, Gr32_transforms::TTransformation* Transformation)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfArrayOfFloatPoint __fastcall TransformPolyPolygon(const Gr32::TArrayOfArrayOfFloatPoint Points, Gr32_transforms::TTransformation* Transformation)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfArrayOfFixedPoint __fastcall TransformPolyPolygon(const Gr32::TArrayOfArrayOfFixedPoint Points, Gr32_transforms::TTransformation* Transformation)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfFloatPoint __fastcall BuildPolygonF(const float *Data, const int Data_High)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfFixedPoint __fastcall BuildPolygonX(const Gr32::TFixed *Data, const int Data_High)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfArrayOfFloatPoint __fastcall PolyPolygon(const Gr32::TArrayOfFloatPoint Points)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfArrayOfFixedPoint __fastcall PolyPolygon(const Gr32::TArrayOfFixedPoint Points)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfFloatPoint __fastcall FixedPointToFloatPoint(const Gr32::TArrayOfFixedPoint Points)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfArrayOfFloatPoint __fastcall FixedPointToFloatPoint(const Gr32::TArrayOfArrayOfFixedPoint Points)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfFixedPoint __fastcall FloatPointToFixedPoint(const Gr32::TArrayOfFloatPoint Points)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfArrayOfFixedPoint __fastcall FloatPointToFixedPoint(const Gr32::TArrayOfArrayOfFloatPoint Points)/* overload */;
}	/* namespace Gr32_vectorutils */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_GR32_VECTORUTILS)
using namespace Gr32_vectorutils;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// Gr32_vectorutilsHPP
