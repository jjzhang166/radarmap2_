﻿// CodeGear C++Builder
// Copyright (c) 1995, 2016 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'GR32_Brushes.pas' rev: 31.00 (Windows)

#ifndef Gr32_brushesHPP
#define Gr32_brushesHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>
#include <SysInit.hpp>
#include <System.Classes.hpp>
#include <GR32.hpp>
#include <GR32_Polygons.hpp>
#include <GR32_Transforms.hpp>

//-- user supplied -----------------------------------------------------------

namespace Gr32_brushes
{
//-- forward type declarations -----------------------------------------------
class DELPHICLASS TBrushCollection;
class DELPHICLASS TCustomBrush;
class DELPHICLASS TSolidBrush;
class DELPHICLASS TNestedBrush;
class DELPHICLASS TStrokeBrush;
class DELPHICLASS TGrowBrush;
class DELPHICLASS TDashedBrush;
//-- type declarations -------------------------------------------------------
typedef System::TMetaClass* TBrushClass;

class PASCALIMPLEMENTATION TBrushCollection : public Gr32::TNotifiablePersistent
{
	typedef Gr32::TNotifiablePersistent inherited;
	
public:
	TCustomBrush* operator[](int Index) { return this->Items[Index]; }
	
private:
	System::Classes::TList* FItems;
	System::Classes::TPersistent* FOwner;
	void __fastcall InsertItem(TCustomBrush* Item);
	void __fastcall RemoveItem(TCustomBrush* Item);
	int __fastcall GetCount(void);
	TCustomBrush* __fastcall GetItem(int Index);
	void __fastcall SetItem(int Index, TCustomBrush* const Value);
	
public:
	__fastcall TBrushCollection(System::Classes::TPersistent* AOwner);
	__fastcall virtual ~TBrushCollection(void);
	TCustomBrush* __fastcall Add(TBrushClass ItemClass);
	void __fastcall Clear(void);
	void __fastcall Delete(int Index);
	TCustomBrush* __fastcall Insert(int Index, TBrushClass ItemClass);
	__property System::Classes::TPersistent* Owner = {read=FOwner};
	__property int Count = {read=GetCount, nodefault};
	__property TCustomBrush* Items[int Index] = {read=GetItem, write=SetItem/*, default*/};
};


class PASCALIMPLEMENTATION TCustomBrush : public Gr32::TNotifiablePersistent
{
	typedef Gr32::TNotifiablePersistent inherited;
	
private:
	TBrushCollection* FBrushCollection;
	bool FVisible;
	int __fastcall GetIndex(void);
	void __fastcall SetBrushCollection(TBrushCollection* const Value);
	void __fastcall SetVisible(const bool Value);
	
protected:
	virtual void __fastcall SetIndex(int Value);
	virtual void __fastcall UpdateRenderer(Gr32_polygons::TCustomPolygonRenderer* Renderer);
	
public:
	__fastcall virtual TCustomBrush(TBrushCollection* ABrushCollection);
	__fastcall virtual ~TCustomBrush(void);
	virtual void __fastcall Changed(void);
	virtual void __fastcall PolyPolygonFS(Gr32_polygons::TCustomPolygonRenderer* Renderer, const Gr32::TArrayOfArrayOfFloatPoint Points, const Gr32::TFloatRect &ClipRect, Gr32_transforms::TTransformation* Transformation, bool Closed);
	virtual void __fastcall PolygonFS(Gr32_polygons::TCustomPolygonRenderer* Renderer, const Gr32::TArrayOfFloatPoint Points, const Gr32::TFloatRect &ClipRect, Gr32_transforms::TTransformation* Transformation, bool Closed);
	__property int Index = {read=GetIndex, write=SetIndex, nodefault};
	__property TBrushCollection* BrushCollection = {read=FBrushCollection, write=SetBrushCollection};
	__property bool Visible = {read=FVisible, write=SetVisible, nodefault};
};


class PASCALIMPLEMENTATION TSolidBrush : public TCustomBrush
{
	typedef TCustomBrush inherited;
	
private:
	Gr32::TColor32 FFillColor;
	Gr32_polygons::TPolyFillMode FFillMode;
	Gr32_polygons::TCustomPolygonFiller* FFiller;
	void __fastcall SetFillColor(const Gr32::TColor32 Value);
	void __fastcall SetFillMode(const Gr32_polygons::TPolyFillMode Value);
	void __fastcall SetFiller(Gr32_polygons::TCustomPolygonFiller* const Value);
	
protected:
	virtual void __fastcall UpdateRenderer(Gr32_polygons::TCustomPolygonRenderer* Renderer);
	
public:
	__fastcall virtual TSolidBrush(TBrushCollection* ABrushCollection);
	__property Gr32::TColor32 FillColor = {read=FFillColor, write=SetFillColor, nodefault};
	__property Gr32_polygons::TPolyFillMode FillMode = {read=FFillMode, write=SetFillMode, nodefault};
	__property Gr32_polygons::TCustomPolygonFiller* Filler = {read=FFiller, write=SetFiller};
public:
	/* TCustomBrush.Destroy */ inline __fastcall virtual ~TSolidBrush(void) { }
	
};


class PASCALIMPLEMENTATION TNestedBrush : public TSolidBrush
{
	typedef TSolidBrush inherited;
	
private:
	TBrushCollection* FBrushes;
	
public:
	__fastcall virtual TNestedBrush(TBrushCollection* ABrushCollection);
	__fastcall virtual ~TNestedBrush(void);
	virtual void __fastcall PolyPolygonFS(Gr32_polygons::TCustomPolygonRenderer* Renderer, const Gr32::TArrayOfArrayOfFloatPoint Points, const Gr32::TFloatRect &ClipRect, Gr32_transforms::TTransformation* Transformation, bool Closed);
	virtual void __fastcall PolygonFS(Gr32_polygons::TCustomPolygonRenderer* Renderer, const Gr32::TArrayOfFloatPoint Points, const Gr32::TFloatRect &ClipRect, Gr32_transforms::TTransformation* Transformation, bool Closed);
	__property TBrushCollection* Brushes = {read=FBrushes};
};


class PASCALIMPLEMENTATION TStrokeBrush : public TSolidBrush
{
	typedef TSolidBrush inherited;
	
private:
	float FStrokeWidth;
	Gr32_polygons::TJoinStyle FJoinStyle;
	float FMiterLimit;
	Gr32_polygons::TEndStyle FEndStyle;
	void __fastcall SetStrokeWidth(const float Value);
	void __fastcall SetEndStyle(const Gr32_polygons::TEndStyle Value);
	void __fastcall SetJoinStyle(const Gr32_polygons::TJoinStyle Value);
	void __fastcall SetMiterLimit(const float Value);
	
public:
	__fastcall virtual TStrokeBrush(TBrushCollection* BrushCollection);
	virtual void __fastcall PolyPolygonFS(Gr32_polygons::TCustomPolygonRenderer* Renderer, const Gr32::TArrayOfArrayOfFloatPoint Points, const Gr32::TFloatRect &ClipRect, Gr32_transforms::TTransformation* Transformation, bool Closed);
	__property float StrokeWidth = {read=FStrokeWidth, write=SetStrokeWidth};
	__property Gr32_polygons::TJoinStyle JoinStyle = {read=FJoinStyle, write=SetJoinStyle, nodefault};
	__property Gr32_polygons::TEndStyle EndStyle = {read=FEndStyle, write=SetEndStyle, nodefault};
	__property float MiterLimit = {read=FMiterLimit, write=SetMiterLimit};
public:
	/* TCustomBrush.Destroy */ inline __fastcall virtual ~TStrokeBrush(void) { }
	
};


class PASCALIMPLEMENTATION TGrowBrush : public TNestedBrush
{
	typedef TNestedBrush inherited;
	
private:
	float FGrowAmount;
	Gr32_polygons::TJoinStyle FJoinStyle;
	float FMiterLimit;
	void __fastcall SetGrowAmount(const float Value);
	void __fastcall SetJoinStyle(const Gr32_polygons::TJoinStyle Value);
	void __fastcall SetMiterLimit(const float Value);
	
public:
	__fastcall virtual TGrowBrush(TBrushCollection* BrushCollection);
	virtual void __fastcall PolyPolygonFS(Gr32_polygons::TCustomPolygonRenderer* Renderer, const Gr32::TArrayOfArrayOfFloatPoint Points, const Gr32::TFloatRect &ClipRect, Gr32_transforms::TTransformation* Transformation, bool Closed);
	__property float GrowAmount = {read=FGrowAmount, write=SetGrowAmount};
	__property Gr32_polygons::TJoinStyle JoinStyle = {read=FJoinStyle, write=SetJoinStyle, nodefault};
	__property float MiterLimit = {read=FMiterLimit, write=SetMiterLimit};
public:
	/* TNestedBrush.Destroy */ inline __fastcall virtual ~TGrowBrush(void) { }
	
};


class PASCALIMPLEMENTATION TDashedBrush : public TStrokeBrush
{
	typedef TStrokeBrush inherited;
	
private:
	float FDashOffset;
	Gr32::TArrayOfFloat FDashArray;
	void __fastcall SetDashOffset(const float Value);
	
public:
	virtual void __fastcall PolyPolygonFS(Gr32_polygons::TCustomPolygonRenderer* Renderer, const Gr32::TArrayOfArrayOfFloatPoint Points, const Gr32::TFloatRect &ClipRect, Gr32_transforms::TTransformation* Transformation, bool Closed);
	void __fastcall SetDashArray(const float *ADashArray, const int ADashArray_High);
	__property float DashOffset = {read=FDashOffset, write=SetDashOffset};
public:
	/* TStrokeBrush.Create */ inline __fastcall virtual TDashedBrush(TBrushCollection* BrushCollection) : TStrokeBrush(BrushCollection) { }
	
public:
	/* TCustomBrush.Destroy */ inline __fastcall virtual ~TDashedBrush(void) { }
	
};


//-- var, const, procedure ---------------------------------------------------
}	/* namespace Gr32_brushes */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_GR32_BRUSHES)
using namespace Gr32_brushes;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// Gr32_brushesHPP
