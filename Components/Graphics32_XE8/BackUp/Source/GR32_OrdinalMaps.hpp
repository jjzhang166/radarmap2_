﻿// CodeGear C++Builder
// Copyright (c) 1995, 2016 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'GR32_OrdinalMaps.pas' rev: 31.00 (Windows)

#ifndef Gr32_ordinalmapsHPP
#define Gr32_ordinalmapsHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>
#include <SysInit.hpp>
#include <Winapi.Windows.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.Graphics.hpp>
#include <System.Classes.hpp>
#include <System.SysUtils.hpp>
#include <GR32.hpp>

//-- user supplied -----------------------------------------------------------

namespace Gr32_ordinalmaps
{
//-- forward type declarations -----------------------------------------------
class DELPHICLASS TBooleanMap;
class DELPHICLASS TByteMap;
class DELPHICLASS TWordMap;
class DELPHICLASS TIntegerMap;
class DELPHICLASS TCardinalMap;
class DELPHICLASS TFloatMap;
template<typename T> class DELPHICLASS TGenericMap__1;
//-- type declarations -------------------------------------------------------
enum DECLSPEC_DENUM TConversionType : unsigned char { ctRed, ctGreen, ctBlue, ctAlpha, ctUniformRGB, ctWeightedRGB };

class PASCALIMPLEMENTATION TBooleanMap : public Gr32::TCustomMap
{
	typedef Gr32::TCustomMap inherited;
	
private:
	bool __fastcall GetValue(int X, int Y);
	void __fastcall SetValue(int X, int Y, const bool Value);
	
protected:
	Gr32::TByteArray *FBits;
	virtual void __fastcall ChangeSize(int &Width, int &Height, int NewWidth, int NewHeight);
	
public:
	__fastcall virtual TBooleanMap(void)/* overload */;
	__fastcall virtual ~TBooleanMap(void);
	virtual bool __fastcall Empty(void);
	void __fastcall Clear(System::Byte FillValue);
	void __fastcall ToggleBit(int X, int Y);
	__property bool Value[int X][int Y] = {read=GetValue, write=SetValue/*, default*/};
	__property Gr32::PByteArray Bits = {read=FBits};
public:
	/* TCustomMap.Create */ inline __fastcall TBooleanMap(int Width, int Height)/* overload */ : Gr32::TCustomMap(Width, Height) { }
	
};


class PASCALIMPLEMENTATION TByteMap : public Gr32::TCustomMap
{
	typedef Gr32::TCustomMap inherited;
	
private:
	System::Byte __fastcall GetValue(int X, int Y);
	System::PByte __fastcall GetValPtr(int X, int Y);
	void __fastcall SetValue(int X, int Y, System::Byte Value);
	Gr32::PByteArray __fastcall GetScanline(int Y);
	
protected:
	Gr32::TByteArray *FBits;
	virtual void __fastcall AssignTo(System::Classes::TPersistent* Dst);
	virtual void __fastcall ChangeSize(int &Width, int &Height, int NewWidth, int NewHeight);
	
public:
	__fastcall virtual TByteMap(void)/* overload */;
	__fastcall virtual ~TByteMap(void);
	virtual void __fastcall Assign(System::Classes::TPersistent* Source);
	virtual bool __fastcall Empty(void);
	void __fastcall Clear(System::Byte FillValue);
	void __fastcall ReadFrom(Gr32::TCustomBitmap32* Source, TConversionType Conversion);
	void __fastcall WriteTo(Gr32::TCustomBitmap32* Dest, TConversionType Conversion)/* overload */;
	void __fastcall WriteTo(Gr32::TCustomBitmap32* Dest, const Gr32::TPalette32 &Palette)/* overload */;
	__property Gr32::PByteArray Bits = {read=FBits};
	__property Gr32::PByteArray Scanline[int Y] = {read=GetScanline};
	__property System::PByte ValPtr[int X][int Y] = {read=GetValPtr};
	__property System::Byte Value[int X][int Y] = {read=GetValue, write=SetValue/*, default*/};
public:
	/* TCustomMap.Create */ inline __fastcall TByteMap(int Width, int Height)/* overload */ : Gr32::TCustomMap(Width, Height) { }
	
};


class PASCALIMPLEMENTATION TWordMap : public Gr32::TCustomMap
{
	typedef Gr32::TCustomMap inherited;
	
private:
	PWORD __fastcall GetValPtr(int X, int Y);
	System::Word __fastcall GetValue(int X, int Y);
	void __fastcall SetValue(int X, int Y, const System::Word Value);
	Gr32::PWordArray __fastcall GetScanline(int Y);
	
protected:
	Gr32::TWordArray *FBits;
	virtual void __fastcall ChangeSize(int &Width, int &Height, int NewWidth, int NewHeight);
	
public:
	__fastcall virtual TWordMap(void)/* overload */;
	__fastcall virtual ~TWordMap(void);
	virtual void __fastcall Assign(System::Classes::TPersistent* Source);
	virtual bool __fastcall Empty(void);
	void __fastcall Clear(System::Word FillValue);
	__property PWORD ValPtr[int X][int Y] = {read=GetValPtr};
	__property System::Word Value[int X][int Y] = {read=GetValue, write=SetValue/*, default*/};
	__property Gr32::PWordArray Bits = {read=FBits};
	__property Gr32::PWordArray Scanline[int Y] = {read=GetScanline};
public:
	/* TCustomMap.Create */ inline __fastcall TWordMap(int Width, int Height)/* overload */ : Gr32::TCustomMap(Width, Height) { }
	
};


class PASCALIMPLEMENTATION TIntegerMap : public Gr32::TCustomMap
{
	typedef Gr32::TCustomMap inherited;
	
private:
	System::PInteger __fastcall GetValPtr(int X, int Y);
	int __fastcall GetValue(int X, int Y);
	void __fastcall SetValue(int X, int Y, const int Value);
	Gr32::PIntegerArray __fastcall GetScanline(int Y);
	
protected:
	Gr32::TIntegerArray *FBits;
	virtual void __fastcall ChangeSize(int &Width, int &Height, int NewWidth, int NewHeight);
	
public:
	__fastcall virtual TIntegerMap(void)/* overload */;
	__fastcall virtual ~TIntegerMap(void);
	virtual void __fastcall Assign(System::Classes::TPersistent* Source);
	virtual bool __fastcall Empty(void);
	void __fastcall Clear(int FillValue = 0x0);
	__property System::PInteger ValPtr[int X][int Y] = {read=GetValPtr};
	__property int Value[int X][int Y] = {read=GetValue, write=SetValue/*, default*/};
	__property Gr32::PIntegerArray Bits = {read=FBits};
	__property Gr32::PIntegerArray Scanline[int Y] = {read=GetScanline};
public:
	/* TCustomMap.Create */ inline __fastcall TIntegerMap(int Width, int Height)/* overload */ : Gr32::TCustomMap(Width, Height) { }
	
};


class PASCALIMPLEMENTATION TCardinalMap : public Gr32::TCustomMap
{
	typedef Gr32::TCustomMap inherited;
	
private:
	System::PCardinal __fastcall GetValPtr(unsigned X, unsigned Y);
	unsigned __fastcall GetValue(unsigned X, unsigned Y);
	void __fastcall SetValue(unsigned X, unsigned Y, const unsigned Value);
	Gr32::PCardinalArray __fastcall GetScanline(int Y);
	
protected:
	Gr32::TCardinalArray *FBits;
	virtual void __fastcall ChangeSize(int &Width, int &Height, int NewWidth, int NewHeight);
	
public:
	__fastcall virtual TCardinalMap(void)/* overload */;
	__fastcall virtual ~TCardinalMap(void);
	virtual void __fastcall Assign(System::Classes::TPersistent* Source);
	virtual bool __fastcall Empty(void);
	void __fastcall Clear(unsigned FillValue = (unsigned)(0x0));
	__property System::PCardinal ValPtr[unsigned X][unsigned Y] = {read=GetValPtr};
	__property unsigned Value[unsigned X][unsigned Y] = {read=GetValue, write=SetValue/*, default*/};
	__property Gr32::PCardinalArray Bits = {read=FBits};
	__property Gr32::PCardinalArray Scanline[int Y] = {read=GetScanline};
public:
	/* TCustomMap.Create */ inline __fastcall TCardinalMap(int Width, int Height)/* overload */ : Gr32::TCustomMap(Width, Height) { }
	
};


class PASCALIMPLEMENTATION TFloatMap : public Gr32::TCustomMap
{
	typedef Gr32::TCustomMap inherited;
	
private:
	Gr32::PFloat __fastcall GetValPtr(int X, int Y);
	float __fastcall GetValue(int X, int Y);
	void __fastcall SetValue(int X, int Y, const float Value);
	Gr32::PFloatArray __fastcall GetScanline(int Y);
	
protected:
	Gr32::TFloatArray *FBits;
	virtual void __fastcall ChangeSize(int &Width, int &Height, int NewWidth, int NewHeight);
	
public:
	__fastcall virtual TFloatMap(void)/* overload */;
	__fastcall virtual ~TFloatMap(void);
	virtual void __fastcall Assign(System::Classes::TPersistent* Source);
	virtual bool __fastcall Empty(void);
	void __fastcall Clear(void)/* overload */;
	void __fastcall Clear(float FillValue)/* overload */;
	__property Gr32::PFloat ValPtr[int X][int Y] = {read=GetValPtr};
	__property float Value[int X][int Y] = {read=GetValue, write=SetValue/*, default*/};
	__property Gr32::PFloatArray Bits = {read=FBits};
	__property Gr32::PFloatArray Scanline[int Y] = {read=GetScanline};
public:
	/* TCustomMap.Create */ inline __fastcall TFloatMap(int Width, int Height)/* overload */ : Gr32::TCustomMap(Width, Height) { }
	
};


// Template declaration generated by Delphi parameterized types is
// used only for accessing Delphi variables and fields.
// Don't instantiate with new type parameters in user code.
template<typename T> class PASCALIMPLEMENTATION TGenericMap__1 : public Gr32::TCustomMap
{
	typedef Gr32::TCustomMap inherited;
	
private:
	T __fastcall GetValue(int X, int Y);
	void __fastcall SetValue(int X, int Y, const T Value);
	
protected:
	void *FBits;
	virtual void __fastcall ChangeSize(int &Width, int &Height, int NewWidth, int NewHeight);
	
public:
	__fastcall virtual TGenericMap__1(void)/* overload */;
	__fastcall virtual ~TGenericMap__1(void);
	virtual void __fastcall Assign(System::Classes::TPersistent* Source);
	virtual bool __fastcall Empty(void);
	void __fastcall Clear(void)/* overload */;
	void __fastcall Clear(T FillValue)/* overload */;
	__property T Value[int X][int Y] = {read=GetValue, write=SetValue/*, default*/};
	__property void * Bits = {read=FBits};
public:
	/* TCustomMap.Create */ inline __fastcall TGenericMap__1(int Width, int Height)/* overload */ : Gr32::TCustomMap(Width, Height) { }
	
};


//-- var, const, procedure ---------------------------------------------------
}	/* namespace Gr32_ordinalmaps */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_GR32_ORDINALMAPS)
using namespace Gr32_ordinalmaps;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// Gr32_ordinalmapsHPP
