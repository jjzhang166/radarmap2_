﻿// CodeGear C++Builder
// Copyright (c) 1995, 2016 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'GR32_Backends.pas' rev: 31.00 (Windows)

#ifndef Gr32_backendsHPP
#define Gr32_backendsHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>
#include <SysInit.hpp>
#include <Winapi.Windows.hpp>
#include <Winapi.Messages.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.Graphics.hpp>
#include <System.Classes.hpp>
#include <System.SysUtils.hpp>
#include <GR32.hpp>
#include <GR32_Containers.hpp>
#include <GR32_Image.hpp>
#include <GR32_Paths.hpp>

//-- user supplied -----------------------------------------------------------

namespace Gr32_backends
{
//-- forward type declarations -----------------------------------------------
__interface ITextSupport;
typedef System::DelphiInterface<ITextSupport> _di_ITextSupport;
__interface IFontSupport;
typedef System::DelphiInterface<IFontSupport> _di_IFontSupport;
__interface ITextToPathSupport;
typedef System::DelphiInterface<ITextToPathSupport> _di_ITextToPathSupport;
__interface ICanvasSupport;
typedef System::DelphiInterface<ICanvasSupport> _di_ICanvasSupport;
__interface IDeviceContextSupport;
typedef System::DelphiInterface<IDeviceContextSupport> _di_IDeviceContextSupport;
__interface IBitmapContextSupport;
typedef System::DelphiInterface<IBitmapContextSupport> _di_IBitmapContextSupport;
__interface IPaintSupport;
typedef System::DelphiInterface<IPaintSupport> _di_IPaintSupport;
//-- type declarations -------------------------------------------------------
__interface  INTERFACE_UUID("{225997CC-958A-423E-8B60-9EDE0D3B53B5}") ITextSupport  : public System::IInterface 
{
	virtual void __fastcall Textout(int X, int Y, const System::UnicodeString Text) = 0 /* overload */;
	virtual void __fastcall Textout(int X, int Y, const System::Types::TRect &ClipRect, const System::UnicodeString Text) = 0 /* overload */;
	virtual void __fastcall Textout(System::Types::TRect &DstRect, const unsigned Flags, const System::UnicodeString Text) = 0 /* overload */;
	virtual System::Types::TSize __fastcall TextExtent(const System::UnicodeString Text) = 0 ;
	virtual void __fastcall TextoutW(int X, int Y, const System::WideString Text) = 0 /* overload */;
	virtual void __fastcall TextoutW(int X, int Y, const System::Types::TRect &ClipRect, const System::WideString Text) = 0 /* overload */;
	virtual void __fastcall TextoutW(System::Types::TRect &DstRect, const unsigned Flags, const System::WideString Text) = 0 /* overload */;
	virtual System::Types::TSize __fastcall TextExtentW(const System::WideString Text) = 0 ;
};

__interface  INTERFACE_UUID("{67C73044-1EFF-4FDE-AEA2-56BFADA50A48}") IFontSupport  : public System::IInterface 
{
	virtual System::Classes::TNotifyEvent __fastcall GetOnFontChange(void) = 0 ;
	virtual void __fastcall SetOnFontChange(System::Classes::TNotifyEvent Handler) = 0 ;
	virtual Vcl::Graphics::TFont* __fastcall GetFont(void) = 0 ;
	virtual void __fastcall SetFont(Vcl::Graphics::TFont* const Font) = 0 ;
	virtual void __fastcall UpdateFont(void) = 0 ;
	__property Vcl::Graphics::TFont* Font = {read=GetFont, write=SetFont};
	__property System::Classes::TNotifyEvent OnFontChange = {read=GetOnFontChange, write=SetOnFontChange};
};

__interface  INTERFACE_UUID("{6C4037E4-FF4D-4EE2-9C20-B9DB9C64B42D}") ITextToPathSupport  : public System::IInterface 
{
	virtual void __fastcall TextToPath(Gr32_paths::TCustomPath* Path, const float X, const float Y, const System::WideString Text) = 0 /* overload */;
	virtual void __fastcall TextToPath(Gr32_paths::TCustomPath* Path, const Gr32::TFloatRect &DstRect, const System::WideString Text, unsigned Flags) = 0 /* overload */;
	virtual Gr32::TFloatRect __fastcall MeasureText(const Gr32::TFloatRect &DstRect, const System::WideString Text, unsigned Flags) = 0 ;
};

__interface  INTERFACE_UUID("{5ACFEEC7-0123-4AD8-8AE6-145718438E01}") ICanvasSupport  : public System::IInterface 
{
	virtual System::Classes::TNotifyEvent __fastcall GetCanvasChange(void) = 0 ;
	virtual void __fastcall SetCanvasChange(System::Classes::TNotifyEvent Handler) = 0 ;
	virtual Vcl::Graphics::TCanvas* __fastcall GetCanvas(void) = 0 ;
	virtual void __fastcall DeleteCanvas(void) = 0 ;
	virtual bool __fastcall CanvasAllocated(void) = 0 ;
	__property Vcl::Graphics::TCanvas* Canvas = {read=GetCanvas};
	__property System::Classes::TNotifyEvent OnCanvasChange = {read=GetCanvasChange, write=SetCanvasChange};
};

__interface  INTERFACE_UUID("{DD1109DA-4019-4A5C-A450-3631A73CF288}") IDeviceContextSupport  : public System::IInterface 
{
	virtual HDC __fastcall GetHandle(void) = 0 ;
	virtual void __fastcall Draw(const System::Types::TRect &DstRect, const System::Types::TRect &SrcRect, HDC hSrc) = 0 ;
	virtual void __fastcall DrawTo(HDC hDst, int DstX, int DstY) = 0 /* overload */;
	virtual void __fastcall DrawTo(HDC hDst, const System::Types::TRect &DstRect, const System::Types::TRect &SrcRect) = 0 /* overload */;
	__property HDC Handle = {read=GetHandle};
};

__interface  INTERFACE_UUID("{DF0F9475-BA13-4C6B-81C3-D138624C4D08}") IBitmapContextSupport  : public System::IInterface 
{
	virtual tagBITMAPINFO __fastcall GetBitmapInfo(void) = 0 ;
	virtual NativeUInt __fastcall GetBitmapHandle(void) = 0 ;
	__property tagBITMAPINFO BitmapInfo = {read=GetBitmapInfo};
	__property NativeUInt BitmapHandle = {read=GetBitmapHandle};
};

__interface  INTERFACE_UUID("{CE64DBEE-C4A9-4E8E-ABCA-1B1FD6F45924}") IPaintSupport  : public System::IInterface 
{
	virtual void __fastcall ImageNeeded(void) = 0 ;
	virtual void __fastcall CheckPixmap(void) = 0 ;
	virtual void __fastcall DoPaint(Gr32::TBitmap32* ABuffer, Gr32_containers::TRectList* AInvalidRects, Vcl::Graphics::TCanvas* ACanvas, Gr32_image::TCustomPaintBox32* APaintBox) = 0 ;
};

enum DECLSPEC_DENUM TRequireOperatorMode : unsigned char { romAnd, romOr };

//-- var, const, procedure ---------------------------------------------------
extern DELPHI_PACKAGE System::ResourceString _RCStrCannotAllocateDIBHandle;
#define Gr32_backends_RCStrCannotAllocateDIBHandle System::LoadResourceString(&Gr32_backends::_RCStrCannotAllocateDIBHandle)
extern DELPHI_PACKAGE System::ResourceString _RCStrCannotCreateCompatibleDC;
#define Gr32_backends_RCStrCannotCreateCompatibleDC System::LoadResourceString(&Gr32_backends::_RCStrCannotCreateCompatibleDC)
extern DELPHI_PACKAGE System::ResourceString _RCStrCannotSelectAnObjectIntoDC;
#define Gr32_backends_RCStrCannotSelectAnObjectIntoDC System::LoadResourceString(&Gr32_backends::_RCStrCannotSelectAnObjectIntoDC)
extern DELPHI_PACKAGE void __fastcall RequireBackendSupport(Gr32::TCustomBitmap32* TargetBitmap, GUID *RequiredInterfaces, const int RequiredInterfaces_High, TRequireOperatorMode Mode, bool UseOptimizedDestructiveSwitchMethod, /* out */ Gr32::TCustomBackend* &ReleasedBackend);
extern DELPHI_PACKAGE void __fastcall RestoreBackend(Gr32::TCustomBitmap32* TargetBitmap, Gr32::TCustomBackend* const SavedBackend);
}	/* namespace Gr32_backends */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_GR32_BACKENDS)
using namespace Gr32_backends;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// Gr32_backendsHPP
