﻿// CodeGear C++Builder
// Copyright (c) 1995, 2016 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'GR32_Polygons_old.pas' rev: 31.00 (Windows)

#ifndef Gr32_polygons_oldHPP
#define Gr32_polygons_oldHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>
#include <SysInit.hpp>
#include <System.Classes.hpp>
#include <System.Types.hpp>
#include <GR32.hpp>
#include <GR32_Containers.hpp>
#include <GR32_VPR.hpp>
#include <GR32_Transforms.hpp>
#include <GR32_Resamplers.hpp>
#include <GR32_LowLevel.hpp>
#include <GR32_Math.hpp>
#include <GR32_Blend.hpp>

//-- user supplied -----------------------------------------------------------

namespace Gr32_polygons_old
{
//-- forward type declarations -----------------------------------------------
class DELPHICLASS TCustomPolygonFillerOld;
class DELPHICLASS TPolygon32;
class DELPHICLASS TBitmapPolygonFillerOld;
class DELPHICLASS TSamplerFillerOld;
//-- type declarations -------------------------------------------------------
enum DECLSPEC_DENUM TPolyFillModeOld : unsigned char { pfAlternateOld, pfWindingOld };

enum DECLSPEC_DENUM TAntialiasMode : unsigned char { am32times, am16times, am8times, am4times, am2times, amNone };

typedef void __fastcall (__closure *TFillLineEvent)(Gr32::PColor32 Dst, int DstX, int DstY, int Length, Gr32::PColor32 AlphaValues);

#pragma pack(push,4)
class PASCALIMPLEMENTATION TCustomPolygonFillerOld : public System::TObject
{
	typedef System::TObject inherited;
	
protected:
	virtual TFillLineEvent __fastcall GetFillLine(void) = 0 ;
	
public:
	__property TFillLineEvent FillLine = {read=GetFillLine};
public:
	/* TObject.Create */ inline __fastcall TCustomPolygonFillerOld(void) : System::TObject() { }
	/* TObject.Destroy */ inline __fastcall virtual ~TCustomPolygonFillerOld(void) { }
	
};

#pragma pack(pop)

class PASCALIMPLEMENTATION TPolygon32 : public Gr32::TThreadPersistent
{
	typedef Gr32::TThreadPersistent inherited;
	
private:
	bool FAntialiased;
	bool FClosed;
	TPolyFillModeOld FFillMode;
	Gr32::TArrayOfArrayOfFixedPoint FNormals;
	Gr32::TArrayOfArrayOfFixedPoint FPoints;
	TAntialiasMode FAntialiasMode;
	
protected:
	void __fastcall BuildNormals(void);
	virtual void __fastcall CopyPropertiesTo(TPolygon32* Dst);
	virtual void __fastcall AssignTo(System::Classes::TPersistent* Dst);
	
public:
	__fastcall virtual TPolygon32(void);
	__fastcall virtual ~TPolygon32(void);
	void __fastcall Add(const Gr32::TFixedPoint &P);
	void __fastcall AddPoints(Gr32::TFixedPoint &First, int Count);
	bool __fastcall ContainsPoint(const Gr32::TFixedPoint &P);
	void __fastcall Clear(void);
	TPolygon32* __fastcall Grow(const Gr32::TFixed Delta, float EdgeSharpness = 0.000000E+00f);
	void __fastcall Draw(Gr32::TCustomBitmap32* Bitmap, Gr32::TColor32 OutlineColor, Gr32::TColor32 FillColor, Gr32_transforms::TTransformation* Transformation = (Gr32_transforms::TTransformation*)(0x0))/* overload */;
	void __fastcall Draw(Gr32::TCustomBitmap32* Bitmap, Gr32::TColor32 OutlineColor, TFillLineEvent FillCallback, Gr32_transforms::TTransformation* Transformation = (Gr32_transforms::TTransformation*)(0x0))/* overload */;
	void __fastcall Draw(Gr32::TCustomBitmap32* Bitmap, Gr32::TColor32 OutlineColor, TCustomPolygonFillerOld* Filler, Gr32_transforms::TTransformation* Transformation = (Gr32_transforms::TTransformation*)(0x0))/* overload */;
	void __fastcall DrawEdge(Gr32::TCustomBitmap32* Bitmap, Gr32::TColor32 Color, Gr32_transforms::TTransformation* Transformation = (Gr32_transforms::TTransformation*)(0x0));
	void __fastcall DrawFill(Gr32::TCustomBitmap32* Bitmap, Gr32::TColor32 Color, Gr32_transforms::TTransformation* Transformation = (Gr32_transforms::TTransformation*)(0x0))/* overload */;
	void __fastcall DrawFill(Gr32::TCustomBitmap32* Bitmap, TFillLineEvent FillCallback, Gr32_transforms::TTransformation* Transformation = (Gr32_transforms::TTransformation*)(0x0))/* overload */;
	void __fastcall DrawFill(Gr32::TCustomBitmap32* Bitmap, TCustomPolygonFillerOld* Filler, Gr32_transforms::TTransformation* Transformation = (Gr32_transforms::TTransformation*)(0x0))/* overload */;
	void __fastcall NewLine(void);
	void __fastcall Offset(const Gr32::TFixed Dx, const Gr32::TFixed Dy);
	TPolygon32* __fastcall Outline(void);
	void __fastcall Transform(Gr32_transforms::TTransformation* Transformation);
	Gr32::TFixedRect __fastcall GetBoundingRect(void);
	__property bool Antialiased = {read=FAntialiased, write=FAntialiased, nodefault};
	__property TAntialiasMode AntialiasMode = {read=FAntialiasMode, write=FAntialiasMode, nodefault};
	__property bool Closed = {read=FClosed, write=FClosed, nodefault};
	__property TPolyFillModeOld FillMode = {read=FFillMode, write=FFillMode, nodefault};
	__property Gr32::TArrayOfArrayOfFixedPoint Normals = {read=FNormals, write=FNormals};
	__property Gr32::TArrayOfArrayOfFixedPoint Points = {read=FPoints, write=FPoints};
};


#pragma pack(push,4)
class PASCALIMPLEMENTATION TBitmapPolygonFillerOld : public TCustomPolygonFillerOld
{
	typedef TCustomPolygonFillerOld inherited;
	
private:
	Gr32::TCustomBitmap32* FPattern;
	int FOffsetY;
	int FOffsetX;
	
protected:
	virtual TFillLineEvent __fastcall GetFillLine(void);
	void __fastcall FillLineOpaque(Gr32::PColor32 Dst, int DstX, int DstY, int Length, Gr32::PColor32 AlphaValues);
	void __fastcall FillLineBlend(Gr32::PColor32 Dst, int DstX, int DstY, int Length, Gr32::PColor32 AlphaValues);
	void __fastcall FillLineBlendMasterAlpha(Gr32::PColor32 Dst, int DstX, int DstY, int Length, Gr32::PColor32 AlphaValues);
	void __fastcall FillLineCustomCombine(Gr32::PColor32 Dst, int DstX, int DstY, int Length, Gr32::PColor32 AlphaValues);
	
public:
	__property Gr32::TCustomBitmap32* Pattern = {read=FPattern, write=FPattern};
	__property int OffsetX = {read=FOffsetX, write=FOffsetX, nodefault};
	__property int OffsetY = {read=FOffsetY, write=FOffsetY, nodefault};
public:
	/* TObject.Create */ inline __fastcall TBitmapPolygonFillerOld(void) : TCustomPolygonFillerOld() { }
	/* TObject.Destroy */ inline __fastcall virtual ~TBitmapPolygonFillerOld(void) { }
	
};

#pragma pack(pop)

class PASCALIMPLEMENTATION TSamplerFillerOld : public TCustomPolygonFillerOld
{
	typedef TCustomPolygonFillerOld inherited;
	
private:
	Gr32::TCustomSampler* FSampler;
	Gr32_resamplers::TGetSampleInt FGetSample;
	void __fastcall SetSampler(Gr32::TCustomSampler* const Value);
	
protected:
	virtual TFillLineEvent __fastcall GetFillLine(void);
	void __fastcall SampleLineOpaque(Gr32::PColor32 Dst, int DstX, int DstY, int Length, Gr32::PColor32 AlphaValues);
	
public:
	__property Gr32::TCustomSampler* Sampler = {read=FSampler, write=SetSampler};
public:
	/* TObject.Create */ inline __fastcall TSamplerFillerOld(void) : TCustomPolygonFillerOld() { }
	/* TObject.Destroy */ inline __fastcall virtual ~TSamplerFillerOld(void) { }
	
};


//-- var, const, procedure ---------------------------------------------------
static const TAntialiasMode DefaultAAMode = (TAntialiasMode)(2);
static const int GR32_MaxListSize = int(0x7ffffff);
extern DELPHI_PACKAGE void __fastcall PolylineTS(Gr32::TCustomBitmap32* Bitmap, const Gr32::TArrayOfFixedPoint Points, Gr32::TColor32 Color, bool Closed = false, Gr32_transforms::TTransformation* Transformation = (Gr32_transforms::TTransformation*)(0x0));
extern DELPHI_PACKAGE void __fastcall PolylineAS(Gr32::TCustomBitmap32* Bitmap, const Gr32::TArrayOfFixedPoint Points, Gr32::TColor32 Color, bool Closed = false, Gr32_transforms::TTransformation* Transformation = (Gr32_transforms::TTransformation*)(0x0));
extern DELPHI_PACKAGE void __fastcall PolylineXS(Gr32::TCustomBitmap32* Bitmap, const Gr32::TArrayOfFixedPoint Points, Gr32::TColor32 Color, bool Closed = false, Gr32_transforms::TTransformation* Transformation = (Gr32_transforms::TTransformation*)(0x0));
extern DELPHI_PACKAGE void __fastcall PolylineXSP(Gr32::TCustomBitmap32* Bitmap, const Gr32::TArrayOfFixedPoint Points, bool Closed = false, Gr32_transforms::TTransformation* Transformation = (Gr32_transforms::TTransformation*)(0x0));
extern DELPHI_PACKAGE void __fastcall PolyPolylineTS(Gr32::TCustomBitmap32* Bitmap, const Gr32::TArrayOfArrayOfFixedPoint Points, Gr32::TColor32 Color, bool Closed = false, Gr32_transforms::TTransformation* Transformation = (Gr32_transforms::TTransformation*)(0x0));
extern DELPHI_PACKAGE void __fastcall PolyPolylineAS(Gr32::TCustomBitmap32* Bitmap, const Gr32::TArrayOfArrayOfFixedPoint Points, Gr32::TColor32 Color, bool Closed = false, Gr32_transforms::TTransformation* Transformation = (Gr32_transforms::TTransformation*)(0x0));
extern DELPHI_PACKAGE void __fastcall PolyPolylineXS(Gr32::TCustomBitmap32* Bitmap, const Gr32::TArrayOfArrayOfFixedPoint Points, Gr32::TColor32 Color, bool Closed = false, Gr32_transforms::TTransformation* Transformation = (Gr32_transforms::TTransformation*)(0x0));
extern DELPHI_PACKAGE void __fastcall PolyPolylineXSP(Gr32::TCustomBitmap32* Bitmap, const Gr32::TArrayOfArrayOfFixedPoint Points, bool Closed = false, Gr32_transforms::TTransformation* Transformation = (Gr32_transforms::TTransformation*)(0x0));
extern DELPHI_PACKAGE void __fastcall PolygonTS(Gr32::TCustomBitmap32* Bitmap, const Gr32::TArrayOfFixedPoint Points, Gr32::TColor32 Color, TPolyFillModeOld Mode = (TPolyFillModeOld)(0x0), Gr32_transforms::TTransformation* Transformation = (Gr32_transforms::TTransformation*)(0x0))/* overload */;
extern DELPHI_PACKAGE void __fastcall PolygonTS(Gr32::TCustomBitmap32* Bitmap, const Gr32::TArrayOfFixedPoint Points, TFillLineEvent FillLineCallback, TPolyFillModeOld Mode = (TPolyFillModeOld)(0x0), Gr32_transforms::TTransformation* Transformation = (Gr32_transforms::TTransformation*)(0x0))/* overload */;
extern DELPHI_PACKAGE void __fastcall PolygonTS(Gr32::TCustomBitmap32* Bitmap, const Gr32::TArrayOfFixedPoint Points, TCustomPolygonFillerOld* Filler, TPolyFillModeOld Mode = (TPolyFillModeOld)(0x0), Gr32_transforms::TTransformation* Transformation = (Gr32_transforms::TTransformation*)(0x0))/* overload */;
extern DELPHI_PACKAGE void __fastcall PolygonXS(Gr32::TCustomBitmap32* Bitmap, const Gr32::TArrayOfFixedPoint Points, Gr32::TColor32 Color, TPolyFillModeOld Mode = (TPolyFillModeOld)(0x0), const TAntialiasMode AAMode = (TAntialiasMode)(0x2), Gr32_transforms::TTransformation* Transformation = (Gr32_transforms::TTransformation*)(0x0))/* overload */;
extern DELPHI_PACKAGE void __fastcall PolygonXS(Gr32::TCustomBitmap32* Bitmap, const Gr32::TArrayOfFixedPoint Points, TFillLineEvent FillLineCallback, TPolyFillModeOld Mode = (TPolyFillModeOld)(0x0), const TAntialiasMode AAMode = (TAntialiasMode)(0x2), Gr32_transforms::TTransformation* Transformation = (Gr32_transforms::TTransformation*)(0x0))/* overload */;
extern DELPHI_PACKAGE void __fastcall PolygonXS(Gr32::TCustomBitmap32* Bitmap, const Gr32::TArrayOfFixedPoint Points, TCustomPolygonFillerOld* Filler, TPolyFillModeOld Mode = (TPolyFillModeOld)(0x0), const TAntialiasMode AAMode = (TAntialiasMode)(0x2), Gr32_transforms::TTransformation* Transformation = (Gr32_transforms::TTransformation*)(0x0))/* overload */;
extern DELPHI_PACKAGE void __fastcall PolyPolygonTS(Gr32::TCustomBitmap32* Bitmap, const Gr32::TArrayOfArrayOfFixedPoint Points, Gr32::TColor32 Color, TPolyFillModeOld Mode = (TPolyFillModeOld)(0x0), Gr32_transforms::TTransformation* Transformation = (Gr32_transforms::TTransformation*)(0x0))/* overload */;
extern DELPHI_PACKAGE void __fastcall PolyPolygonTS(Gr32::TCustomBitmap32* Bitmap, const Gr32::TArrayOfArrayOfFixedPoint Points, TFillLineEvent FillLineCallback, TPolyFillModeOld Mode = (TPolyFillModeOld)(0x0), Gr32_transforms::TTransformation* Transformation = (Gr32_transforms::TTransformation*)(0x0))/* overload */;
extern DELPHI_PACKAGE void __fastcall PolyPolygonTS(Gr32::TCustomBitmap32* Bitmap, const Gr32::TArrayOfArrayOfFixedPoint Points, TCustomPolygonFillerOld* Filler, TPolyFillModeOld Mode = (TPolyFillModeOld)(0x0), Gr32_transforms::TTransformation* Transformation = (Gr32_transforms::TTransformation*)(0x0))/* overload */;
extern DELPHI_PACKAGE void __fastcall PolyPolygonXS(Gr32::TCustomBitmap32* Bitmap, const Gr32::TArrayOfArrayOfFixedPoint Points, Gr32::TColor32 Color, TPolyFillModeOld Mode = (TPolyFillModeOld)(0x0), const TAntialiasMode AAMode = (TAntialiasMode)(0x2), Gr32_transforms::TTransformation* Transformation = (Gr32_transforms::TTransformation*)(0x0))/* overload */;
extern DELPHI_PACKAGE void __fastcall PolyPolygonXS(Gr32::TCustomBitmap32* Bitmap, const Gr32::TArrayOfArrayOfFixedPoint Points, TFillLineEvent FillLineCallback, TPolyFillModeOld Mode = (TPolyFillModeOld)(0x0), const TAntialiasMode AAMode = (TAntialiasMode)(0x2), Gr32_transforms::TTransformation* Transformation = (Gr32_transforms::TTransformation*)(0x0))/* overload */;
extern DELPHI_PACKAGE void __fastcall PolyPolygonXS(Gr32::TCustomBitmap32* Bitmap, const Gr32::TArrayOfArrayOfFixedPoint Points, TCustomPolygonFillerOld* Filler, TPolyFillModeOld Mode = (TPolyFillModeOld)(0x0), const TAntialiasMode AAMode = (TAntialiasMode)(0x2), Gr32_transforms::TTransformation* Transformation = (Gr32_transforms::TTransformation*)(0x0))/* overload */;
extern DELPHI_PACKAGE Gr32::TFixedRect __fastcall PolygonBoundsOld(const Gr32::TArrayOfFixedPoint Points, Gr32_transforms::TTransformation* Transformation = (Gr32_transforms::TTransformation*)(0x0));
extern DELPHI_PACKAGE Gr32::TFixedRect __fastcall PolyPolygonBoundsOld(const Gr32::TArrayOfArrayOfFixedPoint Points, Gr32_transforms::TTransformation* Transformation = (Gr32_transforms::TTransformation*)(0x0));
extern DELPHI_PACKAGE bool __fastcall PtInPolygonOld(const Gr32::TFixedPoint &Pt, const Gr32::TArrayOfFixedPoint Points);
}	/* namespace Gr32_polygons_old */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_GR32_POLYGONS_OLD)
using namespace Gr32_polygons_old;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// Gr32_polygons_oldHPP
