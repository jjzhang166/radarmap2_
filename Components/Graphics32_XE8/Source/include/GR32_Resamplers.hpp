﻿// CodeGear C++Builder
// Copyright (c) 1995, 2016 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'GR32_Resamplers.pas' rev: 31.00 (Windows)

#ifndef Gr32_resamplersHPP
#define Gr32_resamplersHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>
#include <SysInit.hpp>
#include <Winapi.Windows.hpp>
#include <System.Types.hpp>
#include <System.Classes.hpp>
#include <System.SysUtils.hpp>
#include <GR32.hpp>
#include <GR32_Transforms.hpp>
#include <GR32_Containers.hpp>
#include <GR32_OrdinalMaps.hpp>
#include <GR32_Blend.hpp>

//-- user supplied -----------------------------------------------------------

namespace Gr32_resamplers
{
//-- forward type declarations -----------------------------------------------
class DELPHICLASS EBitmapException;
class DELPHICLASS ESrcInvalidException;
class DELPHICLASS ENestedException;
class DELPHICLASS TCustomKernel;
class DELPHICLASS TBoxKernel;
class DELPHICLASS TLinearKernel;
class DELPHICLASS TCosineKernel;
class DELPHICLASS TSplineKernel;
class DELPHICLASS TMitchellKernel;
class DELPHICLASS TCubicKernel;
class DELPHICLASS THermiteKernel;
class DELPHICLASS TWindowedSincKernel;
class DELPHICLASS TAlbrechtKernel;
class DELPHICLASS TLanczosKernel;
class DELPHICLASS TGaussianKernel;
class DELPHICLASS TBlackmanKernel;
class DELPHICLASS THannKernel;
class DELPHICLASS THammingKernel;
class DELPHICLASS TSinshKernel;
class DELPHICLASS TNearestResampler;
class DELPHICLASS TLinearResampler;
class DELPHICLASS TDraftResampler;
class DELPHICLASS TKernelResampler;
class DELPHICLASS TNestedSampler;
class DELPHICLASS TTransformer;
class DELPHICLASS TSuperSampler;
class DELPHICLASS TAdaptiveSuperSampler;
class DELPHICLASS TPatternSampler;
struct TBufferEntry;
class DELPHICLASS TKernelSampler;
class DELPHICLASS TConvolver;
class DELPHICLASS TSelectiveConvolver;
class DELPHICLASS TMorphologicalSampler;
class DELPHICLASS TDilater;
class DELPHICLASS TEroder;
class DELPHICLASS TExpander;
class DELPHICLASS TContracter;
//-- type declarations -------------------------------------------------------
typedef System::StaticArray<int, 33> TKernelEntry;

typedef TKernelEntry *PKernelEntry;

typedef System::DynamicArray<Gr32::TArrayOfInteger> TArrayOfKernelEntry;

typedef System::StaticArray<Gr32::TArrayOfInteger, 1> TKernelEntryArray;

typedef TKernelEntryArray *PKernelEntryArray;

typedef float __fastcall (__closure *TFilterMethod)(float Value);

#pragma pack(push,4)
class PASCALIMPLEMENTATION EBitmapException : public System::Sysutils::Exception
{
	typedef System::Sysutils::Exception inherited;
	
public:
	/* Exception.Create */ inline __fastcall EBitmapException(const System::UnicodeString Msg) : System::Sysutils::Exception(Msg) { }
	/* Exception.CreateFmt */ inline __fastcall EBitmapException(const System::UnicodeString Msg, const System::TVarRec *Args, const int Args_High) : System::Sysutils::Exception(Msg, Args, Args_High) { }
	/* Exception.CreateRes */ inline __fastcall EBitmapException(NativeUInt Ident)/* overload */ : System::Sysutils::Exception(Ident) { }
	/* Exception.CreateRes */ inline __fastcall EBitmapException(System::PResStringRec ResStringRec)/* overload */ : System::Sysutils::Exception(ResStringRec) { }
	/* Exception.CreateResFmt */ inline __fastcall EBitmapException(NativeUInt Ident, const System::TVarRec *Args, const int Args_High)/* overload */ : System::Sysutils::Exception(Ident, Args, Args_High) { }
	/* Exception.CreateResFmt */ inline __fastcall EBitmapException(System::PResStringRec ResStringRec, const System::TVarRec *Args, const int Args_High)/* overload */ : System::Sysutils::Exception(ResStringRec, Args, Args_High) { }
	/* Exception.CreateHelp */ inline __fastcall EBitmapException(const System::UnicodeString Msg, int AHelpContext) : System::Sysutils::Exception(Msg, AHelpContext) { }
	/* Exception.CreateFmtHelp */ inline __fastcall EBitmapException(const System::UnicodeString Msg, const System::TVarRec *Args, const int Args_High, int AHelpContext) : System::Sysutils::Exception(Msg, Args, Args_High, AHelpContext) { }
	/* Exception.CreateResHelp */ inline __fastcall EBitmapException(NativeUInt Ident, int AHelpContext)/* overload */ : System::Sysutils::Exception(Ident, AHelpContext) { }
	/* Exception.CreateResHelp */ inline __fastcall EBitmapException(System::PResStringRec ResStringRec, int AHelpContext)/* overload */ : System::Sysutils::Exception(ResStringRec, AHelpContext) { }
	/* Exception.CreateResFmtHelp */ inline __fastcall EBitmapException(System::PResStringRec ResStringRec, const System::TVarRec *Args, const int Args_High, int AHelpContext)/* overload */ : System::Sysutils::Exception(ResStringRec, Args, Args_High, AHelpContext) { }
	/* Exception.CreateResFmtHelp */ inline __fastcall EBitmapException(NativeUInt Ident, const System::TVarRec *Args, const int Args_High, int AHelpContext)/* overload */ : System::Sysutils::Exception(Ident, Args, Args_High, AHelpContext) { }
	/* Exception.Destroy */ inline __fastcall virtual ~EBitmapException(void) { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION ESrcInvalidException : public System::Sysutils::Exception
{
	typedef System::Sysutils::Exception inherited;
	
public:
	/* Exception.Create */ inline __fastcall ESrcInvalidException(const System::UnicodeString Msg) : System::Sysutils::Exception(Msg) { }
	/* Exception.CreateFmt */ inline __fastcall ESrcInvalidException(const System::UnicodeString Msg, const System::TVarRec *Args, const int Args_High) : System::Sysutils::Exception(Msg, Args, Args_High) { }
	/* Exception.CreateRes */ inline __fastcall ESrcInvalidException(NativeUInt Ident)/* overload */ : System::Sysutils::Exception(Ident) { }
	/* Exception.CreateRes */ inline __fastcall ESrcInvalidException(System::PResStringRec ResStringRec)/* overload */ : System::Sysutils::Exception(ResStringRec) { }
	/* Exception.CreateResFmt */ inline __fastcall ESrcInvalidException(NativeUInt Ident, const System::TVarRec *Args, const int Args_High)/* overload */ : System::Sysutils::Exception(Ident, Args, Args_High) { }
	/* Exception.CreateResFmt */ inline __fastcall ESrcInvalidException(System::PResStringRec ResStringRec, const System::TVarRec *Args, const int Args_High)/* overload */ : System::Sysutils::Exception(ResStringRec, Args, Args_High) { }
	/* Exception.CreateHelp */ inline __fastcall ESrcInvalidException(const System::UnicodeString Msg, int AHelpContext) : System::Sysutils::Exception(Msg, AHelpContext) { }
	/* Exception.CreateFmtHelp */ inline __fastcall ESrcInvalidException(const System::UnicodeString Msg, const System::TVarRec *Args, const int Args_High, int AHelpContext) : System::Sysutils::Exception(Msg, Args, Args_High, AHelpContext) { }
	/* Exception.CreateResHelp */ inline __fastcall ESrcInvalidException(NativeUInt Ident, int AHelpContext)/* overload */ : System::Sysutils::Exception(Ident, AHelpContext) { }
	/* Exception.CreateResHelp */ inline __fastcall ESrcInvalidException(System::PResStringRec ResStringRec, int AHelpContext)/* overload */ : System::Sysutils::Exception(ResStringRec, AHelpContext) { }
	/* Exception.CreateResFmtHelp */ inline __fastcall ESrcInvalidException(System::PResStringRec ResStringRec, const System::TVarRec *Args, const int Args_High, int AHelpContext)/* overload */ : System::Sysutils::Exception(ResStringRec, Args, Args_High, AHelpContext) { }
	/* Exception.CreateResFmtHelp */ inline __fastcall ESrcInvalidException(NativeUInt Ident, const System::TVarRec *Args, const int Args_High, int AHelpContext)/* overload */ : System::Sysutils::Exception(Ident, Args, Args_High, AHelpContext) { }
	/* Exception.Destroy */ inline __fastcall virtual ~ESrcInvalidException(void) { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION ENestedException : public System::Sysutils::Exception
{
	typedef System::Sysutils::Exception inherited;
	
public:
	/* Exception.Create */ inline __fastcall ENestedException(const System::UnicodeString Msg) : System::Sysutils::Exception(Msg) { }
	/* Exception.CreateFmt */ inline __fastcall ENestedException(const System::UnicodeString Msg, const System::TVarRec *Args, const int Args_High) : System::Sysutils::Exception(Msg, Args, Args_High) { }
	/* Exception.CreateRes */ inline __fastcall ENestedException(NativeUInt Ident)/* overload */ : System::Sysutils::Exception(Ident) { }
	/* Exception.CreateRes */ inline __fastcall ENestedException(System::PResStringRec ResStringRec)/* overload */ : System::Sysutils::Exception(ResStringRec) { }
	/* Exception.CreateResFmt */ inline __fastcall ENestedException(NativeUInt Ident, const System::TVarRec *Args, const int Args_High)/* overload */ : System::Sysutils::Exception(Ident, Args, Args_High) { }
	/* Exception.CreateResFmt */ inline __fastcall ENestedException(System::PResStringRec ResStringRec, const System::TVarRec *Args, const int Args_High)/* overload */ : System::Sysutils::Exception(ResStringRec, Args, Args_High) { }
	/* Exception.CreateHelp */ inline __fastcall ENestedException(const System::UnicodeString Msg, int AHelpContext) : System::Sysutils::Exception(Msg, AHelpContext) { }
	/* Exception.CreateFmtHelp */ inline __fastcall ENestedException(const System::UnicodeString Msg, const System::TVarRec *Args, const int Args_High, int AHelpContext) : System::Sysutils::Exception(Msg, Args, Args_High, AHelpContext) { }
	/* Exception.CreateResHelp */ inline __fastcall ENestedException(NativeUInt Ident, int AHelpContext)/* overload */ : System::Sysutils::Exception(Ident, AHelpContext) { }
	/* Exception.CreateResHelp */ inline __fastcall ENestedException(System::PResStringRec ResStringRec, int AHelpContext)/* overload */ : System::Sysutils::Exception(ResStringRec, AHelpContext) { }
	/* Exception.CreateResFmtHelp */ inline __fastcall ENestedException(System::PResStringRec ResStringRec, const System::TVarRec *Args, const int Args_High, int AHelpContext)/* overload */ : System::Sysutils::Exception(ResStringRec, Args, Args_High, AHelpContext) { }
	/* Exception.CreateResFmtHelp */ inline __fastcall ENestedException(NativeUInt Ident, const System::TVarRec *Args, const int Args_High, int AHelpContext)/* overload */ : System::Sysutils::Exception(Ident, Args, Args_High, AHelpContext) { }
	/* Exception.Destroy */ inline __fastcall virtual ~ENestedException(void) { }
	
};

#pragma pack(pop)

typedef Gr32::TColor32 __fastcall (__closure *TGetSampleInt)(int X, int Y);

typedef Gr32::TColor32 __fastcall (__closure *TGetSampleFloat)(float X, float Y);

typedef Gr32::TColor32 __fastcall (__closure *TGetSampleFixed)(Gr32::TFixed X, Gr32::TFixed Y);

#pragma pack(push,4)
class PASCALIMPLEMENTATION TCustomKernel : public System::Classes::TPersistent
{
	typedef System::Classes::TPersistent inherited;
	
protected:
	Gr32::TNotifiablePersistent* FObserver;
	virtual void __fastcall AssignTo(System::Classes::TPersistent* Dst);
	virtual bool __fastcall RangeCheck(void);
	
public:
	__fastcall virtual TCustomKernel(void);
	void __fastcall Changed(void);
	virtual float __fastcall Filter(float Value) = 0 ;
	virtual float __fastcall GetWidth(void) = 0 ;
	__property Gr32::TNotifiablePersistent* Observer = {read=FObserver};
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~TCustomKernel(void) { }
	
};

#pragma pack(pop)

typedef System::TMetaClass* TCustomKernelClass;

#pragma pack(push,4)
class PASCALIMPLEMENTATION TBoxKernel : public TCustomKernel
{
	typedef TCustomKernel inherited;
	
public:
	virtual float __fastcall Filter(float Value);
	virtual float __fastcall GetWidth(void);
public:
	/* TCustomKernel.Create */ inline __fastcall virtual TBoxKernel(void) : TCustomKernel() { }
	
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~TBoxKernel(void) { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TLinearKernel : public TCustomKernel
{
	typedef TCustomKernel inherited;
	
public:
	virtual float __fastcall Filter(float Value);
	virtual float __fastcall GetWidth(void);
public:
	/* TCustomKernel.Create */ inline __fastcall virtual TLinearKernel(void) : TCustomKernel() { }
	
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~TLinearKernel(void) { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TCosineKernel : public TCustomKernel
{
	typedef TCustomKernel inherited;
	
public:
	virtual float __fastcall Filter(float Value);
	virtual float __fastcall GetWidth(void);
public:
	/* TCustomKernel.Create */ inline __fastcall virtual TCosineKernel(void) : TCustomKernel() { }
	
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~TCosineKernel(void) { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TSplineKernel : public TCustomKernel
{
	typedef TCustomKernel inherited;
	
protected:
	virtual bool __fastcall RangeCheck(void);
	
public:
	virtual float __fastcall Filter(float Value);
	virtual float __fastcall GetWidth(void);
public:
	/* TCustomKernel.Create */ inline __fastcall virtual TSplineKernel(void) : TCustomKernel() { }
	
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~TSplineKernel(void) { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TMitchellKernel : public TCustomKernel
{
	typedef TCustomKernel inherited;
	
protected:
	virtual bool __fastcall RangeCheck(void);
	
public:
	virtual float __fastcall Filter(float Value);
	virtual float __fastcall GetWidth(void);
public:
	/* TCustomKernel.Create */ inline __fastcall virtual TMitchellKernel(void) : TCustomKernel() { }
	
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~TMitchellKernel(void) { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TCubicKernel : public TCustomKernel
{
	typedef TCustomKernel inherited;
	
private:
	float FCoeff;
	void __fastcall SetCoeff(const float Value);
	
protected:
	virtual bool __fastcall RangeCheck(void);
	
public:
	__fastcall virtual TCubicKernel(void);
	virtual float __fastcall Filter(float Value);
	virtual float __fastcall GetWidth(void);
	
__published:
	__property float Coeff = {read=FCoeff, write=SetCoeff};
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~TCubicKernel(void) { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION THermiteKernel : public TCustomKernel
{
	typedef TCustomKernel inherited;
	
private:
	float FBias;
	float FTension;
	void __fastcall SetBias(const float Value);
	void __fastcall SetTension(const float Value);
	
protected:
	virtual bool __fastcall RangeCheck(void);
	
public:
	__fastcall virtual THermiteKernel(void);
	virtual float __fastcall Filter(float Value);
	virtual float __fastcall GetWidth(void);
	
__published:
	__property float Bias = {read=FBias, write=SetBias};
	__property float Tension = {read=FTension, write=SetTension};
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~THermiteKernel(void) { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TWindowedSincKernel : public TCustomKernel
{
	typedef TCustomKernel inherited;
	
private:
	float FWidth;
	float FWidthReciprocal;
	
protected:
	virtual bool __fastcall RangeCheck(void);
	virtual float __fastcall Window(float Value) = 0 ;
	
public:
	__fastcall virtual TWindowedSincKernel(void);
	virtual float __fastcall Filter(float Value);
	void __fastcall SetWidth(float Value);
	virtual float __fastcall GetWidth(void);
	__property float WidthReciprocal = {read=FWidthReciprocal};
	
__published:
	__property float Width = {read=FWidth, write=SetWidth};
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~TWindowedSincKernel(void) { }
	
};

#pragma pack(pop)

class PASCALIMPLEMENTATION TAlbrechtKernel : public TWindowedSincKernel
{
	typedef TWindowedSincKernel inherited;
	
private:
	int FTerms;
	System::StaticArray<double, 12> FCoefPointer;
	void __fastcall SetTerms(int Value);
	
protected:
	virtual float __fastcall Window(float Value);
	
public:
	__fastcall virtual TAlbrechtKernel(void);
	
__published:
	__property int Terms = {read=FTerms, write=SetTerms, nodefault};
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~TAlbrechtKernel(void) { }
	
};


#pragma pack(push,4)
class PASCALIMPLEMENTATION TLanczosKernel : public TWindowedSincKernel
{
	typedef TWindowedSincKernel inherited;
	
protected:
	virtual float __fastcall Window(float Value);
public:
	/* TWindowedSincKernel.Create */ inline __fastcall virtual TLanczosKernel(void) : TWindowedSincKernel() { }
	
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~TLanczosKernel(void) { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TGaussianKernel : public TWindowedSincKernel
{
	typedef TWindowedSincKernel inherited;
	
private:
	float FSigma;
	float FSigmaReciprocalLn2;
	void __fastcall SetSigma(const float Value);
	
protected:
	virtual float __fastcall Window(float Value);
	
public:
	__fastcall virtual TGaussianKernel(void);
	
__published:
	__property float Sigma = {read=FSigma, write=SetSigma};
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~TGaussianKernel(void) { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TBlackmanKernel : public TWindowedSincKernel
{
	typedef TWindowedSincKernel inherited;
	
protected:
	virtual float __fastcall Window(float Value);
public:
	/* TWindowedSincKernel.Create */ inline __fastcall virtual TBlackmanKernel(void) : TWindowedSincKernel() { }
	
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~TBlackmanKernel(void) { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION THannKernel : public TWindowedSincKernel
{
	typedef TWindowedSincKernel inherited;
	
protected:
	virtual float __fastcall Window(float Value);
public:
	/* TWindowedSincKernel.Create */ inline __fastcall virtual THannKernel(void) : TWindowedSincKernel() { }
	
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~THannKernel(void) { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION THammingKernel : public TWindowedSincKernel
{
	typedef TWindowedSincKernel inherited;
	
protected:
	virtual float __fastcall Window(float Value);
public:
	/* TWindowedSincKernel.Create */ inline __fastcall virtual THammingKernel(void) : TWindowedSincKernel() { }
	
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~THammingKernel(void) { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TSinshKernel : public TCustomKernel
{
	typedef TCustomKernel inherited;
	
private:
	float FWidth;
	float FCoeff;
	void __fastcall SetCoeff(const float Value);
	
protected:
	virtual bool __fastcall RangeCheck(void);
	
public:
	__fastcall virtual TSinshKernel(void);
	void __fastcall SetWidth(float Value);
	virtual float __fastcall GetWidth(void);
	virtual float __fastcall Filter(float Value);
	
__published:
	__property float Coeff = {read=FCoeff, write=SetCoeff};
	__property float Width = {read=GetWidth, write=SetWidth};
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~TSinshKernel(void) { }
	
};

#pragma pack(pop)

class PASCALIMPLEMENTATION TNearestResampler : public Gr32::TCustomResampler
{
	typedef Gr32::TCustomResampler inherited;
	
private:
	TGetSampleInt FGetSampleInt;
	
protected:
	Gr32::TColor32 __fastcall GetPixelTransparentEdge(int X, int Y);
	virtual float __fastcall GetWidth(void);
	virtual void __fastcall Resample(Gr32::TCustomBitmap32* Dst, const System::Types::TRect &DstRect, const System::Types::TRect &DstClip, Gr32::TCustomBitmap32* Src, const System::Types::TRect &SrcRect, Gr32::TDrawMode CombineOp, Gr32::TPixelCombineEvent CombineCallBack);
	
public:
	virtual Gr32::TColor32 __fastcall GetSampleInt(int X, int Y);
	virtual Gr32::TColor32 __fastcall GetSampleFixed(Gr32::TFixed X, Gr32::TFixed Y);
	virtual Gr32::TColor32 __fastcall GetSampleFloat(float X, float Y);
	virtual void __fastcall PrepareSampling(void);
public:
	/* TCustomResampler.Create */ inline __fastcall virtual TNearestResampler(void)/* overload */ : Gr32::TCustomResampler() { }
	/* TCustomResampler.Create */ inline __fastcall virtual TNearestResampler(Gr32::TCustomBitmap32* ABitmap)/* overload */ : Gr32::TCustomResampler(ABitmap) { }
	
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~TNearestResampler(void) { }
	
};


class PASCALIMPLEMENTATION TLinearResampler : public Gr32::TCustomResampler
{
	typedef Gr32::TCustomResampler inherited;
	
private:
	TLinearKernel* FLinearKernel;
	TGetSampleFixed FGetSampleFixed;
	
protected:
	virtual float __fastcall GetWidth(void);
	Gr32::TColor32 __fastcall GetPixelTransparentEdge(Gr32::TFixed X, Gr32::TFixed Y);
	virtual void __fastcall Resample(Gr32::TCustomBitmap32* Dst, const System::Types::TRect &DstRect, const System::Types::TRect &DstClip, Gr32::TCustomBitmap32* Src, const System::Types::TRect &SrcRect, Gr32::TDrawMode CombineOp, Gr32::TPixelCombineEvent CombineCallBack);
	
public:
	__fastcall virtual TLinearResampler(void)/* overload */;
	__fastcall virtual ~TLinearResampler(void);
	virtual Gr32::TColor32 __fastcall GetSampleFixed(Gr32::TFixed X, Gr32::TFixed Y);
	virtual Gr32::TColor32 __fastcall GetSampleFloat(float X, float Y);
	virtual void __fastcall PrepareSampling(void);
public:
	/* TCustomResampler.Create */ inline __fastcall virtual TLinearResampler(Gr32::TCustomBitmap32* ABitmap)/* overload */ : Gr32::TCustomResampler(ABitmap) { }
	
};


class PASCALIMPLEMENTATION TDraftResampler : public TLinearResampler
{
	typedef TLinearResampler inherited;
	
protected:
	virtual void __fastcall Resample(Gr32::TCustomBitmap32* Dst, const System::Types::TRect &DstRect, const System::Types::TRect &DstClip, Gr32::TCustomBitmap32* Src, const System::Types::TRect &SrcRect, Gr32::TDrawMode CombineOp, Gr32::TPixelCombineEvent CombineCallBack);
public:
	/* TLinearResampler.Create */ inline __fastcall virtual TDraftResampler(void)/* overload */ : TLinearResampler() { }
	/* TLinearResampler.Destroy */ inline __fastcall virtual ~TDraftResampler(void) { }
	
public:
	/* TCustomResampler.Create */ inline __fastcall virtual TDraftResampler(Gr32::TCustomBitmap32* ABitmap)/* overload */ : TLinearResampler(ABitmap) { }
	
};


enum DECLSPEC_DENUM TKernelMode : unsigned char { kmDynamic, kmTableNearest, kmTableLinear };

class PASCALIMPLEMENTATION TKernelResampler : public Gr32::TCustomResampler
{
	typedef Gr32::TCustomResampler inherited;
	
private:
	TCustomKernel* FKernel;
	TKernelMode FKernelMode;
	Gr32_ordinalmaps::TIntegerMap* FWeightTable;
	int FTableSize;
	Gr32::TColor32 FOuterColor;
	void __fastcall SetKernel(TCustomKernel* const Value);
	System::UnicodeString __fastcall GetKernelClassName(void);
	void __fastcall SetKernelClassName(const System::UnicodeString Value);
	void __fastcall SetKernelMode(const TKernelMode Value);
	void __fastcall SetTableSize(int Value);
	
protected:
	virtual float __fastcall GetWidth(void);
	
public:
	__fastcall virtual TKernelResampler(void)/* overload */;
	__fastcall virtual ~TKernelResampler(void);
	virtual Gr32::TColor32 __fastcall GetSampleFloat(float X, float Y);
	virtual void __fastcall Resample(Gr32::TCustomBitmap32* Dst, const System::Types::TRect &DstRect, const System::Types::TRect &DstClip, Gr32::TCustomBitmap32* Src, const System::Types::TRect &SrcRect, Gr32::TDrawMode CombineOp, Gr32::TPixelCombineEvent CombineCallBack);
	virtual void __fastcall PrepareSampling(void);
	virtual void __fastcall FinalizeSampling(void);
	
__published:
	__property System::UnicodeString KernelClassName = {read=GetKernelClassName, write=SetKernelClassName};
	__property TCustomKernel* Kernel = {read=FKernel, write=SetKernel};
	__property TKernelMode KernelMode = {read=FKernelMode, write=SetKernelMode, nodefault};
	__property int TableSize = {read=FTableSize, write=SetTableSize, nodefault};
public:
	/* TCustomResampler.Create */ inline __fastcall virtual TKernelResampler(Gr32::TCustomBitmap32* ABitmap)/* overload */ : Gr32::TCustomResampler(ABitmap) { }
	
};


class PASCALIMPLEMENTATION TNestedSampler : public Gr32::TCustomSampler
{
	typedef Gr32::TCustomSampler inherited;
	
private:
	Gr32::TCustomSampler* FSampler;
	TGetSampleInt FGetSampleInt;
	TGetSampleFixed FGetSampleFixed;
	TGetSampleFloat FGetSampleFloat;
	void __fastcall SetSampler(Gr32::TCustomSampler* const Value);
	
protected:
	virtual void __fastcall AssignTo(System::Classes::TPersistent* Dst);
	
public:
	__fastcall virtual TNestedSampler(Gr32::TCustomSampler* ASampler);
	virtual void __fastcall PrepareSampling(void);
	virtual void __fastcall FinalizeSampling(void);
	virtual bool __fastcall HasBounds(void);
	virtual Gr32::TFloatRect __fastcall GetSampleBounds(void);
	
__published:
	__property Gr32::TCustomSampler* Sampler = {read=FSampler, write=SetSampler};
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~TNestedSampler(void) { }
	
};


typedef void __fastcall (__closure *TReverseTransformInt)(int DstX, int DstY, /* out */ int &SrcX, /* out */ int &SrcY);

typedef void __fastcall (__closure *TReverseTransformFixed)(Gr32::TFixed DstX, Gr32::TFixed DstY, /* out */ Gr32::TFixed &SrcX, /* out */ Gr32::TFixed &SrcY);

typedef void __fastcall (__closure *TReverseTransformFloat)(float DstX, float DstY, /* out */ float &SrcX, /* out */ float &SrcY);

class PASCALIMPLEMENTATION TTransformer : public TNestedSampler
{
	typedef TNestedSampler inherited;
	
private:
	Gr32_transforms::TTransformation* FTransformation;
	TReverseTransformInt FTransformationReverseTransformInt;
	TReverseTransformFixed FTransformationReverseTransformFixed;
	TReverseTransformFloat FTransformationReverseTransformFloat;
	void __fastcall SetTransformation(Gr32_transforms::TTransformation* const Value);
	
public:
	__fastcall TTransformer(Gr32::TCustomSampler* ASampler, Gr32_transforms::TTransformation* ATransformation);
	virtual void __fastcall PrepareSampling(void);
	virtual Gr32::TColor32 __fastcall GetSampleInt(int X, int Y);
	virtual Gr32::TColor32 __fastcall GetSampleFixed(Gr32::TFixed X, Gr32::TFixed Y);
	virtual Gr32::TColor32 __fastcall GetSampleFloat(float X, float Y);
	virtual bool __fastcall HasBounds(void);
	virtual Gr32::TFloatRect __fastcall GetSampleBounds(void);
	
__published:
	__property Gr32_transforms::TTransformation* Transformation = {read=FTransformation, write=SetTransformation};
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~TTransformer(void) { }
	
};


typedef int TSamplingRange;

class PASCALIMPLEMENTATION TSuperSampler : public TNestedSampler
{
	typedef TNestedSampler inherited;
	
private:
	TSamplingRange FSamplingY;
	TSamplingRange FSamplingX;
	Gr32::TFixed FDistanceX;
	Gr32::TFixed FDistanceY;
	Gr32::TFixed FOffsetX;
	Gr32::TFixed FOffsetY;
	Gr32::TFixed FScale;
	void __fastcall SetSamplingX(const TSamplingRange Value);
	void __fastcall SetSamplingY(const TSamplingRange Value);
	
public:
	__fastcall virtual TSuperSampler(Gr32::TCustomSampler* Sampler);
	virtual Gr32::TColor32 __fastcall GetSampleFixed(Gr32::TFixed X, Gr32::TFixed Y);
	
__published:
	__property TSamplingRange SamplingX = {read=FSamplingX, write=SetSamplingX, nodefault};
	__property TSamplingRange SamplingY = {read=FSamplingY, write=SetSamplingY, nodefault};
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~TSuperSampler(void) { }
	
};


typedef Gr32::TColor32 __fastcall (__closure *TRecurseProc)(Gr32::TFixed X, Gr32::TFixed Y, Gr32::TFixed W, const Gr32::TColor32 C1, const Gr32::TColor32 C2);

class PASCALIMPLEMENTATION TAdaptiveSuperSampler : public TNestedSampler
{
	typedef TNestedSampler inherited;
	
private:
	Gr32::TFixed FMinOffset;
	int FLevel;
	int FTolerance;
	void __fastcall SetLevel(const int Value);
	Gr32::TColor32 __fastcall DoRecurse(Gr32::TFixed X, Gr32::TFixed Y, Gr32::TFixed Offset, const Gr32::TColor32 A, const Gr32::TColor32 B, const Gr32::TColor32 C, const Gr32::TColor32 D, const Gr32::TColor32 E);
	Gr32::TColor32 __fastcall QuadrantColor(const Gr32::TColor32 C1, const Gr32::TColor32 C2, Gr32::TFixed X, Gr32::TFixed Y, Gr32::TFixed Offset, TRecurseProc Proc);
	Gr32::TColor32 __fastcall RecurseAC(Gr32::TFixed X, Gr32::TFixed Y, Gr32::TFixed Offset, const Gr32::TColor32 A, const Gr32::TColor32 C);
	Gr32::TColor32 __fastcall RecurseBD(Gr32::TFixed X, Gr32::TFixed Y, Gr32::TFixed Offset, const Gr32::TColor32 B, const Gr32::TColor32 D);
	
protected:
	virtual bool __fastcall CompareColors(Gr32::TColor32 C1, Gr32::TColor32 C2);
	
public:
	__fastcall virtual TAdaptiveSuperSampler(Gr32::TCustomSampler* Sampler);
	virtual Gr32::TColor32 __fastcall GetSampleFixed(Gr32::TFixed X, Gr32::TFixed Y);
	
__published:
	__property int Level = {read=FLevel, write=SetLevel, nodefault};
	__property int Tolerance = {read=FTolerance, write=FTolerance, nodefault};
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~TAdaptiveSuperSampler(void) { }
	
};


typedef System::DynamicArray<Gr32::TArrayOfFloatPoint> Gr32_resamplers__82;

typedef System::DynamicArray<System::DynamicArray<Gr32::TArrayOfFloatPoint> > TFloatSamplePattern;

typedef System::DynamicArray<Gr32::TArrayOfFixedPoint> Gr32_resamplers__92;

typedef System::DynamicArray<System::DynamicArray<Gr32::TArrayOfFixedPoint> > TFixedSamplePattern;

class PASCALIMPLEMENTATION TPatternSampler : public TNestedSampler
{
	typedef TNestedSampler inherited;
	
private:
	TFixedSamplePattern FPattern;
	void __fastcall SetPattern(const TFixedSamplePattern Value);
	
protected:
	Gr32::TWrapProc WrapProcVert;
	
public:
	__fastcall virtual ~TPatternSampler(void);
	virtual Gr32::TColor32 __fastcall GetSampleFixed(Gr32::TFixed X, Gr32::TFixed Y);
	__property TFixedSamplePattern Pattern = {read=FPattern, write=SetPattern};
public:
	/* TNestedSampler.Create */ inline __fastcall virtual TPatternSampler(Gr32::TCustomSampler* ASampler) : TNestedSampler(ASampler) { }
	
};


typedef TBufferEntry *PBufferEntry;

struct DECLSPEC_DRECORD TBufferEntry
{
public:
	int B;
	int G;
	int R;
	int A;
};


class PASCALIMPLEMENTATION TKernelSampler : public TNestedSampler
{
	typedef TNestedSampler inherited;
	
private:
	Gr32_ordinalmaps::TIntegerMap* FKernel;
	TBufferEntry FStartEntry;
	int FCenterX;
	int FCenterY;
	
protected:
	virtual void __fastcall UpdateBuffer(TBufferEntry &Buffer, Gr32::TColor32 Color, int Weight) = 0 ;
	virtual Gr32::TColor32 __fastcall ConvertBuffer(TBufferEntry &Buffer);
	
public:
	__fastcall virtual TKernelSampler(Gr32::TCustomSampler* ASampler);
	__fastcall virtual ~TKernelSampler(void);
	virtual Gr32::TColor32 __fastcall GetSampleInt(int X, int Y);
	virtual Gr32::TColor32 __fastcall GetSampleFixed(Gr32::TFixed X, Gr32::TFixed Y);
	
__published:
	__property Gr32_ordinalmaps::TIntegerMap* Kernel = {read=FKernel, write=FKernel};
	__property int CenterX = {read=FCenterX, write=FCenterX, nodefault};
	__property int CenterY = {read=FCenterY, write=FCenterY, nodefault};
};


class PASCALIMPLEMENTATION TConvolver : public TKernelSampler
{
	typedef TKernelSampler inherited;
	
protected:
	virtual void __fastcall UpdateBuffer(TBufferEntry &Buffer, Gr32::TColor32 Color, int Weight);
public:
	/* TKernelSampler.Create */ inline __fastcall virtual TConvolver(Gr32::TCustomSampler* ASampler) : TKernelSampler(ASampler) { }
	/* TKernelSampler.Destroy */ inline __fastcall virtual ~TConvolver(void) { }
	
};


class PASCALIMPLEMENTATION TSelectiveConvolver : public TConvolver
{
	typedef TConvolver inherited;
	
private:
	Gr32::TColor32 FRefColor;
	int FDelta;
	TBufferEntry FWeightSum;
	
protected:
	virtual void __fastcall UpdateBuffer(TBufferEntry &Buffer, Gr32::TColor32 Color, int Weight);
	virtual Gr32::TColor32 __fastcall ConvertBuffer(TBufferEntry &Buffer);
	
public:
	__fastcall virtual TSelectiveConvolver(Gr32::TCustomSampler* ASampler);
	virtual Gr32::TColor32 __fastcall GetSampleInt(int X, int Y);
	virtual Gr32::TColor32 __fastcall GetSampleFixed(Gr32::TFixed X, Gr32::TFixed Y);
	
__published:
	__property int Delta = {read=FDelta, write=FDelta, nodefault};
public:
	/* TKernelSampler.Destroy */ inline __fastcall virtual ~TSelectiveConvolver(void) { }
	
};


class PASCALIMPLEMENTATION TMorphologicalSampler : public TKernelSampler
{
	typedef TKernelSampler inherited;
	
protected:
	virtual Gr32::TColor32 __fastcall ConvertBuffer(TBufferEntry &Buffer);
public:
	/* TKernelSampler.Create */ inline __fastcall virtual TMorphologicalSampler(Gr32::TCustomSampler* ASampler) : TKernelSampler(ASampler) { }
	/* TKernelSampler.Destroy */ inline __fastcall virtual ~TMorphologicalSampler(void) { }
	
};


class PASCALIMPLEMENTATION TDilater : public TMorphologicalSampler
{
	typedef TMorphologicalSampler inherited;
	
protected:
	virtual void __fastcall UpdateBuffer(TBufferEntry &Buffer, Gr32::TColor32 Color, int Weight);
public:
	/* TKernelSampler.Create */ inline __fastcall virtual TDilater(Gr32::TCustomSampler* ASampler) : TMorphologicalSampler(ASampler) { }
	/* TKernelSampler.Destroy */ inline __fastcall virtual ~TDilater(void) { }
	
};


class PASCALIMPLEMENTATION TEroder : public TMorphologicalSampler
{
	typedef TMorphologicalSampler inherited;
	
protected:
	virtual void __fastcall UpdateBuffer(TBufferEntry &Buffer, Gr32::TColor32 Color, int Weight);
	
public:
	__fastcall virtual TEroder(Gr32::TCustomSampler* ASampler);
public:
	/* TKernelSampler.Destroy */ inline __fastcall virtual ~TEroder(void) { }
	
};


class PASCALIMPLEMENTATION TExpander : public TKernelSampler
{
	typedef TKernelSampler inherited;
	
protected:
	virtual void __fastcall UpdateBuffer(TBufferEntry &Buffer, Gr32::TColor32 Color, int Weight);
public:
	/* TKernelSampler.Create */ inline __fastcall virtual TExpander(Gr32::TCustomSampler* ASampler) : TKernelSampler(ASampler) { }
	/* TKernelSampler.Destroy */ inline __fastcall virtual ~TExpander(void) { }
	
};


class PASCALIMPLEMENTATION TContracter : public TExpander
{
	typedef TExpander inherited;
	
private:
	Gr32::TColor32 FMaxWeight;
	
protected:
	virtual void __fastcall UpdateBuffer(TBufferEntry &Buffer, Gr32::TColor32 Color, int Weight);
	
public:
	virtual void __fastcall PrepareSampling(void);
	virtual Gr32::TColor32 __fastcall GetSampleInt(int X, int Y);
	virtual Gr32::TColor32 __fastcall GetSampleFixed(Gr32::TFixed X, Gr32::TFixed Y);
public:
	/* TKernelSampler.Create */ inline __fastcall virtual TContracter(Gr32::TCustomSampler* ASampler) : TExpander(ASampler) { }
	/* TKernelSampler.Destroy */ inline __fastcall virtual ~TContracter(void) { }
	
};


//-- var, const, procedure ---------------------------------------------------
static const System::Int8 MAX_KERNEL_WIDTH = System::Int8(0x10);
extern DELPHI_PACKAGE Gr32_containers::TClassList* KernelList;
extern DELPHI_PACKAGE Gr32_containers::TClassList* ResamplerList;
extern DELPHI_PACKAGE TBufferEntry EMPTY_ENTRY;
extern DELPHI_PACKAGE Gr32::TColor32 __fastcall (*BlockAverage)(unsigned Dlx, unsigned Dly, Gr32::PColor32 RowSrc, unsigned OffSrc);
extern DELPHI_PACKAGE Gr32::TColor32 __fastcall (*Interpolator)(unsigned WX_256, unsigned WY_256, Gr32::PColor32 C11, Gr32::PColor32 C21);
extern DELPHI_PACKAGE System::ResourceString _SDstNil;
#define Gr32_resamplers_SDstNil System::LoadResourceString(&Gr32_resamplers::_SDstNil)
extern DELPHI_PACKAGE System::ResourceString _SSrcNil;
#define Gr32_resamplers_SSrcNil System::LoadResourceString(&Gr32_resamplers::_SSrcNil)
extern DELPHI_PACKAGE System::ResourceString _SSrcInvalid;
#define Gr32_resamplers_SSrcInvalid System::LoadResourceString(&Gr32_resamplers::_SSrcInvalid)
extern DELPHI_PACKAGE System::ResourceString _SSamplerNil;
#define Gr32_resamplers_SSamplerNil System::LoadResourceString(&Gr32_resamplers::_SSamplerNil)
extern DELPHI_PACKAGE void __fastcall Convolve(Gr32::TCustomBitmap32* Src, Gr32::TCustomBitmap32* Dst, Gr32_ordinalmaps::TIntegerMap* Kernel, int CenterX, int CenterY);
extern DELPHI_PACKAGE void __fastcall Dilate(Gr32::TCustomBitmap32* Src, Gr32::TCustomBitmap32* Dst, Gr32_ordinalmaps::TIntegerMap* Kernel, int CenterX, int CenterY);
extern DELPHI_PACKAGE void __fastcall Erode(Gr32::TCustomBitmap32* Src, Gr32::TCustomBitmap32* Dst, Gr32_ordinalmaps::TIntegerMap* Kernel, int CenterX, int CenterY);
extern DELPHI_PACKAGE void __fastcall Expand(Gr32::TCustomBitmap32* Src, Gr32::TCustomBitmap32* Dst, Gr32_ordinalmaps::TIntegerMap* Kernel, int CenterX, int CenterY);
extern DELPHI_PACKAGE void __fastcall Contract(Gr32::TCustomBitmap32* Src, Gr32::TCustomBitmap32* Dst, Gr32_ordinalmaps::TIntegerMap* Kernel, int CenterX, int CenterY);
extern DELPHI_PACKAGE void __fastcall IncBuffer(TBufferEntry &Buffer, Gr32::TColor32 Color);
extern DELPHI_PACKAGE void __fastcall MultiplyBuffer(TBufferEntry &Buffer, int W);
extern DELPHI_PACKAGE void __fastcall ShrBuffer(TBufferEntry &Buffer, int Shift);
extern DELPHI_PACKAGE Gr32::TColor32 __fastcall BufferToColor32(const TBufferEntry &Buffer, int Shift);
extern DELPHI_PACKAGE void __fastcall BlockTransfer(Gr32::TCustomBitmap32* Dst, int DstX, int DstY, const System::Types::TRect &DstClip, Gr32::TCustomBitmap32* Src, const System::Types::TRect &SrcRect, Gr32::TDrawMode CombineOp, Gr32::TPixelCombineEvent CombineCallBack = 0x0);
extern DELPHI_PACKAGE void __fastcall BlockTransferX(Gr32::TCustomBitmap32* Dst, Gr32::TFixed DstX, Gr32::TFixed DstY, Gr32::TCustomBitmap32* Src, const System::Types::TRect &SrcRect, Gr32::TDrawMode CombineOp, Gr32::TPixelCombineEvent CombineCallBack = 0x0);
extern DELPHI_PACKAGE void __fastcall BlendTransfer(Gr32::TCustomBitmap32* Dst, int DstX, int DstY, const System::Types::TRect &DstClip, Gr32::TCustomBitmap32* SrcF, const System::Types::TRect &SrcRectF, Gr32::TCustomBitmap32* SrcB, const System::Types::TRect &SrcRectB, Gr32_blend::TBlendReg BlendCallback)/* overload */;
extern DELPHI_PACKAGE void __fastcall BlendTransfer(Gr32::TCustomBitmap32* Dst, int DstX, int DstY, const System::Types::TRect &DstClip, Gr32::TCustomBitmap32* SrcF, const System::Types::TRect &SrcRectF, Gr32::TCustomBitmap32* SrcB, const System::Types::TRect &SrcRectB, Gr32_blend::TBlendRegEx BlendCallback, int MasterAlpha)/* overload */;
extern DELPHI_PACKAGE void __fastcall StretchTransfer(Gr32::TCustomBitmap32* Dst, const System::Types::TRect &DstRect, const System::Types::TRect &DstClip, Gr32::TCustomBitmap32* Src, const System::Types::TRect &SrcRect, Gr32::TCustomResampler* Resampler, Gr32::TDrawMode CombineOp, Gr32::TPixelCombineEvent CombineCallBack = 0x0);
extern DELPHI_PACKAGE TFixedSamplePattern __fastcall CreateJitteredPattern(int TileWidth, int TileHeight, int SamplesX, int SamplesY);
extern DELPHI_PACKAGE void __fastcall RegisterResampler(Gr32::TCustomResamplerClass ResamplerClass);
extern DELPHI_PACKAGE void __fastcall RegisterKernel(TCustomKernelClass KernelClass);
}	/* namespace Gr32_resamplers */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_GR32_RESAMPLERS)
using namespace Gr32_resamplers;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// Gr32_resamplersHPP
