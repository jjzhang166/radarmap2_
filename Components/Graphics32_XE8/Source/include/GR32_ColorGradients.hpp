﻿// CodeGear C++Builder
// Copyright (c) 1995, 2016 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'GR32_ColorGradients.pas' rev: 31.00 (Windows)

#ifndef Gr32_colorgradientsHPP
#define Gr32_colorgradientsHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>
#include <SysInit.hpp>
#include <System.Types.hpp>
#include <System.Classes.hpp>
#include <System.SysUtils.hpp>
#include <System.Math.hpp>
#include <GR32.hpp>
#include <GR32_Polygons.hpp>
#include <GR32_VectorUtils.hpp>

//-- user supplied -----------------------------------------------------------

namespace Gr32_colorgradients
{
//-- forward type declarations -----------------------------------------------
struct TColor32GradientStop;
struct TColor32FloatPoint;
class DELPHICLASS TColor32LookupTable;
class DELPHICLASS TColor32Gradient;
class DELPHICLASS TCustomSparsePointGradientSampler;
class DELPHICLASS TBarycentricGradientSampler;
class DELPHICLASS TBilinearGradientSampler;
class DELPHICLASS TCustomArbitrarySparsePointGradientSampler;
class DELPHICLASS TInvertedDistanceWeightingSampler;
class DELPHICLASS TVoronoiSampler;
class DELPHICLASS TGourandShadedDelaunayTrianglesSampler;
class DELPHICLASS TCustomGradientSampler;
class DELPHICLASS TCustomGradientLookUpTableSampler;
class DELPHICLASS TCustomCenterLutGradientSampler;
class DELPHICLASS TConicGradientSampler;
class DELPHICLASS TCustomCenterRadiusLutGradientSampler;
class DELPHICLASS TRadialGradientSampler;
class DELPHICLASS TCustomCenterRadiusAngleLutGradientSampler;
class DELPHICLASS TDiamondGradientSampler;
class DELPHICLASS TXGradientSampler;
class DELPHICLASS TLinearGradientSampler;
class DELPHICLASS TXYGradientSampler;
class DELPHICLASS TXYSqrtGradientSampler;
class DELPHICLASS TCustomSparsePointGradientPolygonFiller;
class DELPHICLASS TBarycentricGradientPolygonFiller;
class DELPHICLASS TCustomArbitrarySparsePointGradientPolygonFiller;
class DELPHICLASS TGourandShadedDelaunayTrianglesPolygonFiller;
class DELPHICLASS TCustomGradientPolygonFiller;
class DELPHICLASS TCustomGradientLookupTablePolygonFiller;
class DELPHICLASS TCustomLinearGradientPolygonFiller;
class DELPHICLASS TLinearGradientPolygonFiller;
class DELPHICLASS TCustomRadialGradientPolygonFiller;
class DELPHICLASS TRadialGradientPolygonFiller;
class DELPHICLASS TSVGRadialGradientPolygonFiller;
//-- type declarations -------------------------------------------------------
struct DECLSPEC_DRECORD TColor32GradientStop
{
public:
	float Offset;
	Gr32::TColor32 Color32;
};


typedef System::DynamicArray<TColor32GradientStop> TArrayOfColor32GradientStop;

struct DECLSPEC_DRECORD TColor32FloatPoint
{
public:
	Gr32::TFloatPoint Point;
	Gr32::TColor32 Color32;
};


typedef System::DynamicArray<TColor32FloatPoint> TArrayOfColor32FloatPoint;

class PASCALIMPLEMENTATION TColor32LookupTable : public System::Classes::TPersistent
{
	typedef System::Classes::TPersistent inherited;
	
private:
	Gr32::TColor32Array *FGradientLUT;
	System::Byte FOrder;
	unsigned FMask;
	unsigned FSize;
	System::Classes::TNotifyEvent FOnOrderChanged;
	void __fastcall SetOrder(const System::Byte Value);
	Gr32::TColor32 __fastcall GetColor32(int Index);
	void __fastcall SetColor32(int Index, const Gr32::TColor32 Value);
	
protected:
	void __fastcall OrderChanged(void);
	virtual void __fastcall AssignTo(System::Classes::TPersistent* Dest);
	
public:
	__fastcall virtual TColor32LookupTable(System::Byte Order);
	__fastcall virtual ~TColor32LookupTable(void);
	__property System::Byte Order = {read=FOrder, write=SetOrder, nodefault};
	__property unsigned Size = {read=FSize, nodefault};
	__property unsigned Mask = {read=FMask, nodefault};
	__property Gr32::TColor32 Color32[int Index] = {read=GetColor32, write=SetColor32};
	__property Gr32::PColor32Array Color32Ptr = {read=FGradientLUT};
	__property System::Classes::TNotifyEvent OnOrderChanged = {read=FOnOrderChanged, write=FOnOrderChanged};
};


class PASCALIMPLEMENTATION TColor32Gradient : public System::Classes::TInterfacedPersistent
{
	typedef System::Classes::TInterfacedPersistent inherited;
	
private:
	TArrayOfColor32GradientStop FGradientColors;
	System::Classes::TNotifyEvent FOnGradientColorsChanged;
	TColor32GradientStop __fastcall GetGradientEntry(int Index);
	int __fastcall GetGradientCount(void);
	Gr32::TColor32 __fastcall GetStartColor(void);
	Gr32::TColor32 __fastcall GetEndColor(void);
	void __fastcall SetEndColor(const Gr32::TColor32 Value);
	void __fastcall SetStartColor(const Gr32::TColor32 Value);
	
protected:
	virtual void __fastcall GradientColorsChanged(void);
	virtual void __fastcall AssignTo(System::Classes::TPersistent* Dest);
	
public:
	__fastcall TColor32Gradient(Gr32::TColor32 Color)/* overload */;
	__fastcall TColor32Gradient(Gr32::TColor32 StartColor, Gr32::TColor32 EndColor)/* overload */;
	__fastcall TColor32Gradient(const TArrayOfColor32GradientStop GradientColors)/* overload */;
	void __fastcall LoadFromStream(System::Classes::TStream* Stream);
	void __fastcall SaveToStream(System::Classes::TStream* Stream);
	void __fastcall ClearColorStops(void)/* overload */;
	void __fastcall ClearColorStops(Gr32::TColor32 Color)/* overload */;
	virtual void __fastcall AddColorStop(float Offset, Gr32::TColor32 Color)/* overload */;
	virtual void __fastcall AddColorStop(const TColor32GradientStop &ColorStop)/* overload */;
	void __fastcall SetColors(const System::TVarRec *GradientColors, const int GradientColors_High)/* overload */;
	void __fastcall SetColors(const TArrayOfColor32GradientStop GradientColors)/* overload */;
	void __fastcall SetColors(const Gr32::TArrayOfColor32 GradientColors)/* overload */;
	void __fastcall SetColors(const Gr32::TPalette32 &Palette)/* overload */;
	Gr32::TColor32 __fastcall GetColorAt(float Offset);
	void __fastcall FillColorLookUpTable(Gr32::TColor32 *ColorLUT, const int ColorLUT_High)/* overload */;
	void __fastcall FillColorLookUpTable(Gr32::PColor32Array ColorLUT, int Count)/* overload */;
	void __fastcall FillColorLookUpTable(TColor32LookupTable* ColorLUT)/* overload */;
	__property TColor32GradientStop GradientEntry[int Index] = {read=GetGradientEntry};
	__property int GradientCount = {read=GetGradientCount, nodefault};
	__property Gr32::TColor32 StartColor = {read=GetStartColor, write=SetStartColor, nodefault};
	__property Gr32::TColor32 EndColor = {read=GetEndColor, write=SetEndColor, nodefault};
	__property System::Classes::TNotifyEvent OnGradientColorsChanged = {read=FOnGradientColorsChanged, write=FOnGradientColorsChanged};
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~TColor32Gradient(void) { }
	
private:
	void *__IStreamPersist;	// System::Classes::IStreamPersist 
	
public:
	#if defined(MANAGED_INTERFACE_OPERATORS)
	// {B8CD12A3-267A-11D4-83DA-00C04F60B2DD}
	operator System::Classes::_di_IStreamPersist()
	{
		System::Classes::_di_IStreamPersist intf;
		this->GetInterface(intf);
		return intf;
	}
	#else
	operator System::Classes::IStreamPersist*(void) { return (System::Classes::IStreamPersist*)&__IStreamPersist; }
	#endif
	
};


class PASCALIMPLEMENTATION TCustomSparsePointGradientSampler : public Gr32::TCustomSampler
{
	typedef Gr32::TCustomSampler inherited;
	
protected:
	virtual int __fastcall GetCount(void) = 0 ;
	virtual Gr32::TColor32 __fastcall GetColor(int Index) = 0 ;
	virtual Gr32::TFloatPoint __fastcall GetPoint(int Index) = 0 ;
	virtual TColor32FloatPoint __fastcall GetColorPoint(int Index) = 0 ;
	virtual void __fastcall SetColor(int Index, const Gr32::TColor32 Value) = 0 ;
	virtual void __fastcall SetColorPoint(int Index, const TColor32FloatPoint &Value) = 0 ;
	virtual void __fastcall SetPoint(int Index, const Gr32::TFloatPoint &Value) = 0 ;
	
public:
	virtual Gr32::TColor32 __fastcall GetSampleFixed(Gr32::TFixed X, Gr32::TFixed Y);
	virtual Gr32::TColor32 __fastcall GetSampleInt(int X, int Y);
	virtual void __fastcall SetPoints(Gr32::TArrayOfFloatPoint Points) = 0 ;
	virtual void __fastcall SetColorPoints(TArrayOfColor32FloatPoint ColorPoints) = 0 /* overload */;
	virtual void __fastcall SetColorPoints(Gr32::TArrayOfFloatPoint Points, Gr32::TArrayOfColor32 Colors) = 0 /* overload */;
	__property Gr32::TColor32 Color[int Index] = {read=GetColor, write=SetColor};
	__property Gr32::TFloatPoint Point[int Index] = {read=GetPoint, write=SetPoint};
	__property TColor32FloatPoint ColorPoint[int Index] = {read=GetColorPoint, write=SetColorPoint};
	__property int Count = {read=GetCount, nodefault};
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~TCustomSparsePointGradientSampler(void) { }
	
public:
	/* TObject.Create */ inline __fastcall TCustomSparsePointGradientSampler(void) : Gr32::TCustomSampler() { }
	
};


class PASCALIMPLEMENTATION TBarycentricGradientSampler : public TCustomSparsePointGradientSampler
{
	typedef TCustomSparsePointGradientSampler inherited;
	
protected:
	System::StaticArray<TColor32FloatPoint, 3> FColorPoints;
	System::StaticArray<Gr32::TFloatPoint, 2> FDists;
	virtual int __fastcall GetCount(void);
	virtual Gr32::TColor32 __fastcall GetColor(int Index);
	virtual TColor32FloatPoint __fastcall GetColorPoint(int Index);
	virtual Gr32::TFloatPoint __fastcall GetPoint(int Index);
	virtual void __fastcall SetColor(int Index, const Gr32::TColor32 Value);
	virtual void __fastcall SetColorPoint(int Index, const TColor32FloatPoint &Value);
	virtual void __fastcall SetPoint(int Index, const Gr32::TFloatPoint &Value);
	virtual void __fastcall AssignTo(System::Classes::TPersistent* Dest);
	void __fastcall CalculateBarycentricCoordinates(float X, float Y, /* out */ float &U, /* out */ float &V, /* out */ float &W);
	
public:
	__fastcall virtual TBarycentricGradientSampler(const TColor32FloatPoint &P1, const TColor32FloatPoint &P2, const TColor32FloatPoint &P3)/* overload */;
	bool __fastcall IsPointInTriangle(float X, float Y)/* overload */;
	bool __fastcall IsPointInTriangle(const Gr32::TFloatPoint &Point)/* overload */;
	virtual void __fastcall SetPoints(Gr32::TArrayOfFloatPoint Points);
	virtual void __fastcall SetColorPoints(TArrayOfColor32FloatPoint ColorPoints)/* overload */;
	virtual void __fastcall SetColorPoints(Gr32::TArrayOfFloatPoint Points, Gr32::TArrayOfColor32 Colors)/* overload */;
	virtual void __fastcall PrepareSampling(void);
	virtual Gr32::TColor32 __fastcall GetSampleFloat(float X, float Y);
	Gr32::TColor32 __fastcall GetSampleFloatInTriangle(float X, float Y);
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~TBarycentricGradientSampler(void) { }
	
};


class PASCALIMPLEMENTATION TBilinearGradientSampler : public TCustomSparsePointGradientSampler
{
	typedef TCustomSparsePointGradientSampler inherited;
	
protected:
	System::StaticArray<TColor32FloatPoint, 4> FColorPoints;
	System::StaticArray<Gr32::TFloatPoint, 3> FDists;
	float FDot;
	float FBiasK0;
	float FBiasU;
	int FK2Sign;
	float FK2Value;
	virtual int __fastcall GetCount(void);
	virtual Gr32::TColor32 __fastcall GetColor(int Index);
	virtual TColor32FloatPoint __fastcall GetColorPoint(int Index);
	virtual Gr32::TFloatPoint __fastcall GetPoint(int Index);
	virtual void __fastcall SetColor(int Index, const Gr32::TColor32 Value);
	virtual void __fastcall SetColorPoint(int Index, const TColor32FloatPoint &Value);
	virtual void __fastcall SetPoint(int Index, const Gr32::TFloatPoint &Value);
	virtual void __fastcall AssignTo(System::Classes::TPersistent* Dest);
	
public:
	virtual void __fastcall SetPoints(Gr32::TArrayOfFloatPoint Points);
	virtual void __fastcall SetColorPoints(TArrayOfColor32FloatPoint ColorPoints)/* overload */;
	virtual void __fastcall SetColorPoints(Gr32::TArrayOfFloatPoint Points, Gr32::TArrayOfColor32 Colors)/* overload */;
	virtual void __fastcall PrepareSampling(void);
	virtual Gr32::TColor32 __fastcall GetSampleFloat(float X, float Y);
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~TBilinearGradientSampler(void) { }
	
public:
	/* TObject.Create */ inline __fastcall TBilinearGradientSampler(void) : TCustomSparsePointGradientSampler() { }
	
};


class PASCALIMPLEMENTATION TCustomArbitrarySparsePointGradientSampler : public TCustomSparsePointGradientSampler
{
	typedef TCustomSparsePointGradientSampler inherited;
	
private:
	TArrayOfColor32FloatPoint FColorPoints;
	
protected:
	virtual void __fastcall AssignTo(System::Classes::TPersistent* Dest);
	virtual int __fastcall GetCount(void);
	virtual Gr32::TColor32 __fastcall GetColor(int Index);
	virtual TColor32FloatPoint __fastcall GetColorPoint(int Index);
	virtual Gr32::TFloatPoint __fastcall GetPoint(int Index);
	virtual void __fastcall SetColor(int Index, const Gr32::TColor32 Value);
	virtual void __fastcall SetColorPoint(int Index, const TColor32FloatPoint &Value);
	virtual void __fastcall SetPoint(int Index, const Gr32::TFloatPoint &Value);
	
public:
	virtual void __fastcall Add(const Gr32::TFloatPoint &Point, Gr32::TColor32 Color)/* overload */;
	virtual void __fastcall Add(const TColor32FloatPoint &ColorPoint)/* overload */;
	virtual void __fastcall SetColorPoints(TArrayOfColor32FloatPoint ColorPoints)/* overload */;
	virtual void __fastcall SetColorPoints(Gr32::TArrayOfFloatPoint Points, Gr32::TArrayOfColor32 Colors)/* overload */;
	virtual void __fastcall SetPoints(Gr32::TArrayOfFloatPoint Points);
	virtual void __fastcall Clear(void);
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~TCustomArbitrarySparsePointGradientSampler(void) { }
	
public:
	/* TObject.Create */ inline __fastcall TCustomArbitrarySparsePointGradientSampler(void) : TCustomSparsePointGradientSampler() { }
	
};


class PASCALIMPLEMENTATION TInvertedDistanceWeightingSampler : public TCustomArbitrarySparsePointGradientSampler
{
	typedef TCustomArbitrarySparsePointGradientSampler inherited;
	
private:
	Gr32::TArrayOfFloat FDists;
	bool FUsePower;
	float FPower;
	float FScaledPower;
	
public:
	__fastcall virtual TInvertedDistanceWeightingSampler(void);
	virtual void __fastcall PrepareSampling(void);
	virtual void __fastcall FinalizeSampling(void);
	virtual Gr32::TColor32 __fastcall GetSampleFloat(float X, float Y);
	__property float Power = {read=FPower, write=FPower};
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~TInvertedDistanceWeightingSampler(void) { }
	
};


class PASCALIMPLEMENTATION TVoronoiSampler : public TCustomArbitrarySparsePointGradientSampler
{
	typedef TCustomArbitrarySparsePointGradientSampler inherited;
	
public:
	virtual Gr32::TColor32 __fastcall GetSampleFloat(float X, float Y);
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~TVoronoiSampler(void) { }
	
public:
	/* TObject.Create */ inline __fastcall TVoronoiSampler(void) : TCustomArbitrarySparsePointGradientSampler() { }
	
};


class PASCALIMPLEMENTATION TGourandShadedDelaunayTrianglesSampler : public TCustomArbitrarySparsePointGradientSampler
{
	typedef TCustomArbitrarySparsePointGradientSampler inherited;
	
	
private:
	typedef System::DynamicArray<TBarycentricGradientSampler*> _TGourandShadedDelaunayTrianglesSampler__1;
	
	
private:
	Gr32_vectorutils::TArrayOfTriangleVertexIndices FTriangles;
	_TGourandShadedDelaunayTrianglesSampler__1 FBarycentric;
	
public:
	virtual void __fastcall PrepareSampling(void);
	virtual void __fastcall FinalizeSampling(void);
	virtual Gr32::TColor32 __fastcall GetSampleFloat(float X, float Y);
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~TGourandShadedDelaunayTrianglesSampler(void) { }
	
public:
	/* TObject.Create */ inline __fastcall TGourandShadedDelaunayTrianglesSampler(void) : TCustomArbitrarySparsePointGradientSampler() { }
	
};


class PASCALIMPLEMENTATION TCustomGradientSampler : public Gr32::TCustomSampler
{
	typedef Gr32::TCustomSampler inherited;
	
private:
	TColor32Gradient* FGradient;
	Gr32::TWrapMode FWrapMode;
	void __fastcall SetGradient(TColor32Gradient* const Value);
	void __fastcall SetWrapMode(const Gr32::TWrapMode Value);
	
protected:
	bool FInitialized;
	virtual void __fastcall AssignTo(System::Classes::TPersistent* Dest);
	void __fastcall GradientChangedHandler(System::TObject* Sender);
	void __fastcall GradientSamplerChanged(void);
	virtual void __fastcall WrapModeChanged(void);
	virtual void __fastcall UpdateInternals(void) = 0 ;
	__property bool Initialized = {read=FInitialized, nodefault};
	
public:
	__fastcall virtual TCustomGradientSampler(Gr32::TWrapMode WrapMode)/* overload */;
	__fastcall virtual TCustomGradientSampler(TColor32Gradient* ColorGradient)/* overload */;
	__fastcall virtual ~TCustomGradientSampler(void);
	virtual void __fastcall PrepareSampling(void);
	virtual Gr32::TColor32 __fastcall GetSampleInt(int X, int Y);
	virtual Gr32::TColor32 __fastcall GetSampleFixed(Gr32::TFixed X, Gr32::TFixed Y);
	__property TColor32Gradient* Gradient = {read=FGradient, write=SetGradient};
	__property Gr32::TWrapMode WrapMode = {read=FWrapMode, write=SetWrapMode, nodefault};
};


class PASCALIMPLEMENTATION TCustomGradientLookUpTableSampler : public TCustomGradientSampler
{
	typedef TCustomGradientSampler inherited;
	
private:
	TColor32LookupTable* FGradientLUT;
	Gr32::TColor32Array *FLutPtr;
	int FLutMask;
	Gr32::TWrapProc FWrapProc;
	
protected:
	virtual void __fastcall AssignTo(System::Classes::TPersistent* Dest);
	virtual void __fastcall WrapModeChanged(void);
	virtual void __fastcall UpdateInternals(void);
	__property Gr32::PColor32Array LutPtr = {read=FLutPtr};
	__property int LutMask = {read=FLutMask, nodefault};
	__property Gr32::TWrapProc WrapProc = {read=FWrapProc};
	
public:
	__fastcall virtual TCustomGradientLookUpTableSampler(Gr32::TWrapMode WrapMode)/* overload */;
	__fastcall virtual ~TCustomGradientLookUpTableSampler(void);
public:
	/* TCustomGradientSampler.Create */ inline __fastcall virtual TCustomGradientLookUpTableSampler(TColor32Gradient* ColorGradient)/* overload */ : TCustomGradientSampler(ColorGradient) { }
	
};


class PASCALIMPLEMENTATION TCustomCenterLutGradientSampler : public TCustomGradientLookUpTableSampler
{
	typedef TCustomGradientLookUpTableSampler inherited;
	
private:
	Gr32::TFloatPoint FCenter;
	
protected:
	virtual void __fastcall AssignTo(System::Classes::TPersistent* Dest);
	virtual void __fastcall Transform(float &X, float &Y);
	
public:
	__fastcall virtual TCustomCenterLutGradientSampler(Gr32::TWrapMode WrapMode)/* overload */;
	__property Gr32::TFloatPoint Center = {read=FCenter, write=FCenter};
public:
	/* TCustomGradientLookUpTableSampler.Destroy */ inline __fastcall virtual ~TCustomCenterLutGradientSampler(void) { }
	
public:
	/* TCustomGradientSampler.Create */ inline __fastcall virtual TCustomCenterLutGradientSampler(TColor32Gradient* ColorGradient)/* overload */ : TCustomGradientLookUpTableSampler(ColorGradient) { }
	
};


class PASCALIMPLEMENTATION TConicGradientSampler : public TCustomCenterLutGradientSampler
{
	typedef TCustomCenterLutGradientSampler inherited;
	
private:
	float FScale;
	float FAngle;
	
protected:
	virtual void __fastcall AssignTo(System::Classes::TPersistent* Dest);
	virtual void __fastcall UpdateInternals(void);
	
public:
	virtual Gr32::TColor32 __fastcall GetSampleFloat(float X, float Y);
	__property float Angle = {read=FAngle, write=FAngle};
public:
	/* TCustomCenterLutGradientSampler.Create */ inline __fastcall virtual TConicGradientSampler(Gr32::TWrapMode WrapMode)/* overload */ : TCustomCenterLutGradientSampler(WrapMode) { }
	
public:
	/* TCustomGradientLookUpTableSampler.Destroy */ inline __fastcall virtual ~TConicGradientSampler(void) { }
	
public:
	/* TCustomGradientSampler.Create */ inline __fastcall virtual TConicGradientSampler(TColor32Gradient* ColorGradient)/* overload */ : TCustomCenterLutGradientSampler(ColorGradient) { }
	
};


class PASCALIMPLEMENTATION TCustomCenterRadiusLutGradientSampler : public TCustomCenterLutGradientSampler
{
	typedef TCustomCenterLutGradientSampler inherited;
	
private:
	float FRadius;
	void __fastcall SetRadius(const float Value);
	
protected:
	virtual void __fastcall AssignTo(System::Classes::TPersistent* Dest);
	virtual void __fastcall RadiusChanged(void);
	
public:
	__fastcall virtual TCustomCenterRadiusLutGradientSampler(Gr32::TWrapMode WrapMode)/* overload */;
	__property float Radius = {read=FRadius, write=SetRadius};
public:
	/* TCustomGradientLookUpTableSampler.Destroy */ inline __fastcall virtual ~TCustomCenterRadiusLutGradientSampler(void) { }
	
public:
	/* TCustomGradientSampler.Create */ inline __fastcall virtual TCustomCenterRadiusLutGradientSampler(TColor32Gradient* ColorGradient)/* overload */ : TCustomCenterLutGradientSampler(ColorGradient) { }
	
};


class PASCALIMPLEMENTATION TRadialGradientSampler : public TCustomCenterRadiusLutGradientSampler
{
	typedef TCustomCenterRadiusLutGradientSampler inherited;
	
private:
	float FScale;
	
protected:
	virtual void __fastcall UpdateInternals(void);
	
public:
	virtual Gr32::TColor32 __fastcall GetSampleFloat(float X, float Y);
public:
	/* TCustomCenterRadiusLutGradientSampler.Create */ inline __fastcall virtual TRadialGradientSampler(Gr32::TWrapMode WrapMode)/* overload */ : TCustomCenterRadiusLutGradientSampler(WrapMode) { }
	
public:
	/* TCustomGradientLookUpTableSampler.Destroy */ inline __fastcall virtual ~TRadialGradientSampler(void) { }
	
public:
	/* TCustomGradientSampler.Create */ inline __fastcall virtual TRadialGradientSampler(TColor32Gradient* ColorGradient)/* overload */ : TCustomCenterRadiusLutGradientSampler(ColorGradient) { }
	
};


class PASCALIMPLEMENTATION TCustomCenterRadiusAngleLutGradientSampler : public TCustomCenterRadiusLutGradientSampler
{
	typedef TCustomCenterRadiusLutGradientSampler inherited;
	
private:
	float FAngle;
	Gr32::TFloatPoint FSinCos;
	void __fastcall SetAngle(const float Value);
	
protected:
	virtual void __fastcall AssignTo(System::Classes::TPersistent* Dest);
	virtual void __fastcall AngleChanged(void);
	virtual void __fastcall RadiusChanged(void);
	virtual void __fastcall Transform(float &X, float &Y);
	
public:
	__fastcall virtual TCustomCenterRadiusAngleLutGradientSampler(Gr32::TWrapMode WrapMode)/* overload */;
	__property float Angle = {read=FAngle, write=SetAngle};
public:
	/* TCustomGradientLookUpTableSampler.Destroy */ inline __fastcall virtual ~TCustomCenterRadiusAngleLutGradientSampler(void) { }
	
public:
	/* TCustomGradientSampler.Create */ inline __fastcall virtual TCustomCenterRadiusAngleLutGradientSampler(TColor32Gradient* ColorGradient)/* overload */ : TCustomCenterRadiusLutGradientSampler(ColorGradient) { }
	
};


class PASCALIMPLEMENTATION TDiamondGradientSampler : public TCustomCenterRadiusAngleLutGradientSampler
{
	typedef TCustomCenterRadiusAngleLutGradientSampler inherited;
	
private:
	float FScale;
	
protected:
	virtual void __fastcall UpdateInternals(void);
	
public:
	virtual Gr32::TColor32 __fastcall GetSampleFloat(float X, float Y);
public:
	/* TCustomCenterRadiusAngleLutGradientSampler.Create */ inline __fastcall virtual TDiamondGradientSampler(Gr32::TWrapMode WrapMode)/* overload */ : TCustomCenterRadiusAngleLutGradientSampler(WrapMode) { }
	
public:
	/* TCustomGradientLookUpTableSampler.Destroy */ inline __fastcall virtual ~TDiamondGradientSampler(void) { }
	
public:
	/* TCustomGradientSampler.Create */ inline __fastcall virtual TDiamondGradientSampler(TColor32Gradient* ColorGradient)/* overload */ : TCustomCenterRadiusAngleLutGradientSampler(ColorGradient) { }
	
};


class PASCALIMPLEMENTATION TXGradientSampler : public TCustomCenterRadiusAngleLutGradientSampler
{
	typedef TCustomCenterRadiusAngleLutGradientSampler inherited;
	
private:
	float FScale;
	Gr32::TFloatPoint __fastcall GetEndPoint(void);
	Gr32::TFloatPoint __fastcall GetStartPoint(void);
	void __fastcall SetEndPoint(const Gr32::TFloatPoint &Value);
	void __fastcall SetStartPoint(const Gr32::TFloatPoint &Value);
	
protected:
	virtual void __fastcall UpdateInternals(void);
	
public:
	virtual void __fastcall SimpleGradient(const Gr32::TFloatPoint &StartPoint, Gr32::TColor32 StartColor, const Gr32::TFloatPoint &EndPoint, Gr32::TColor32 EndColor);
	virtual void __fastcall SetPoints(const Gr32::TFloatPoint &StartPoint, const Gr32::TFloatPoint &EndPoint);
	virtual Gr32::TColor32 __fastcall GetSampleFloat(float X, float Y);
	__property Gr32::TFloatPoint StartPoint = {read=GetStartPoint, write=SetStartPoint};
	__property Gr32::TFloatPoint EndPoint = {read=GetEndPoint, write=SetEndPoint};
public:
	/* TCustomCenterRadiusAngleLutGradientSampler.Create */ inline __fastcall virtual TXGradientSampler(Gr32::TWrapMode WrapMode)/* overload */ : TCustomCenterRadiusAngleLutGradientSampler(WrapMode) { }
	
public:
	/* TCustomGradientLookUpTableSampler.Destroy */ inline __fastcall virtual ~TXGradientSampler(void) { }
	
public:
	/* TCustomGradientSampler.Create */ inline __fastcall virtual TXGradientSampler(TColor32Gradient* ColorGradient)/* overload */ : TCustomCenterRadiusAngleLutGradientSampler(ColorGradient) { }
	
};


class PASCALIMPLEMENTATION TLinearGradientSampler : public TXGradientSampler
{
	typedef TXGradientSampler inherited;
	
public:
	/* TCustomCenterRadiusAngleLutGradientSampler.Create */ inline __fastcall virtual TLinearGradientSampler(Gr32::TWrapMode WrapMode)/* overload */ : TXGradientSampler(WrapMode) { }
	
public:
	/* TCustomGradientLookUpTableSampler.Destroy */ inline __fastcall virtual ~TLinearGradientSampler(void) { }
	
public:
	/* TCustomGradientSampler.Create */ inline __fastcall virtual TLinearGradientSampler(TColor32Gradient* ColorGradient)/* overload */ : TXGradientSampler(ColorGradient) { }
	
};


class PASCALIMPLEMENTATION TXYGradientSampler : public TCustomCenterRadiusAngleLutGradientSampler
{
	typedef TCustomCenterRadiusAngleLutGradientSampler inherited;
	
private:
	float FScale;
	
protected:
	virtual void __fastcall UpdateInternals(void);
	
public:
	virtual Gr32::TColor32 __fastcall GetSampleFloat(float X, float Y);
public:
	/* TCustomCenterRadiusAngleLutGradientSampler.Create */ inline __fastcall virtual TXYGradientSampler(Gr32::TWrapMode WrapMode)/* overload */ : TCustomCenterRadiusAngleLutGradientSampler(WrapMode) { }
	
public:
	/* TCustomGradientLookUpTableSampler.Destroy */ inline __fastcall virtual ~TXYGradientSampler(void) { }
	
public:
	/* TCustomGradientSampler.Create */ inline __fastcall virtual TXYGradientSampler(TColor32Gradient* ColorGradient)/* overload */ : TCustomCenterRadiusAngleLutGradientSampler(ColorGradient) { }
	
};


class PASCALIMPLEMENTATION TXYSqrtGradientSampler : public TCustomCenterRadiusAngleLutGradientSampler
{
	typedef TCustomCenterRadiusAngleLutGradientSampler inherited;
	
private:
	float FScale;
	
protected:
	virtual void __fastcall UpdateInternals(void);
	
public:
	virtual Gr32::TColor32 __fastcall GetSampleFloat(float X, float Y);
public:
	/* TCustomCenterRadiusAngleLutGradientSampler.Create */ inline __fastcall virtual TXYSqrtGradientSampler(Gr32::TWrapMode WrapMode)/* overload */ : TCustomCenterRadiusAngleLutGradientSampler(WrapMode) { }
	
public:
	/* TCustomGradientLookUpTableSampler.Destroy */ inline __fastcall virtual ~TXYSqrtGradientSampler(void) { }
	
public:
	/* TCustomGradientSampler.Create */ inline __fastcall virtual TXYSqrtGradientSampler(TColor32Gradient* ColorGradient)/* overload */ : TCustomCenterRadiusAngleLutGradientSampler(ColorGradient) { }
	
};


#pragma pack(push,4)
class PASCALIMPLEMENTATION TCustomSparsePointGradientPolygonFiller : public Gr32_polygons::TCustomPolygonFiller
{
	typedef Gr32_polygons::TCustomPolygonFiller inherited;
	
protected:
	virtual int __fastcall GetCount(void) = 0 ;
	virtual Gr32::TColor32 __fastcall GetColor(int Index) = 0 ;
	virtual Gr32::TFloatPoint __fastcall GetPoint(int Index) = 0 ;
	virtual TColor32FloatPoint __fastcall GetColorPoint(int Index) = 0 ;
	virtual void __fastcall SetColor(int Index, const Gr32::TColor32 Value) = 0 ;
	virtual void __fastcall SetColorPoint(int Index, const TColor32FloatPoint &Value) = 0 ;
	virtual void __fastcall SetPoint(int Index, const Gr32::TFloatPoint &Value) = 0 ;
	
public:
	virtual void __fastcall SetPoints(Gr32::TArrayOfFloatPoint Points) = 0 ;
	virtual void __fastcall SetColorPoints(TArrayOfColor32FloatPoint ColorPoints) = 0 /* overload */;
	virtual void __fastcall SetColorPoints(Gr32::TArrayOfFloatPoint Points, Gr32::TArrayOfColor32 Colors) = 0 /* overload */;
	__property Gr32::TColor32 Color[int Index] = {read=GetColor, write=SetColor};
	__property Gr32::TFloatPoint Point[int Index] = {read=GetPoint, write=SetPoint};
	__property TColor32FloatPoint ColorPoint[int Index] = {read=GetColorPoint, write=SetColorPoint};
	__property int Count = {read=GetCount, nodefault};
public:
	/* TObject.Create */ inline __fastcall TCustomSparsePointGradientPolygonFiller(void) : Gr32_polygons::TCustomPolygonFiller() { }
	/* TObject.Destroy */ inline __fastcall virtual ~TCustomSparsePointGradientPolygonFiller(void) { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TBarycentricGradientPolygonFiller : public TCustomSparsePointGradientPolygonFiller
{
	typedef TCustomSparsePointGradientPolygonFiller inherited;
	
protected:
	System::StaticArray<TColor32FloatPoint, 3> FColorPoints;
	System::StaticArray<Gr32::TFloatPoint, 2> FDists;
	virtual int __fastcall GetCount(void);
	virtual Gr32::TColor32 __fastcall GetColor(int Index);
	virtual Gr32::TFloatPoint __fastcall GetPoint(int Index);
	virtual TColor32FloatPoint __fastcall GetColorPoint(int Index);
	virtual void __fastcall SetColor(int Index, const Gr32::TColor32 Value);
	virtual void __fastcall SetColorPoint(int Index, const TColor32FloatPoint &Value);
	virtual void __fastcall SetPoint(int Index, const Gr32::TFloatPoint &Value);
	virtual Gr32_polygons::TFillLineEvent __fastcall GetFillLine(void);
	HIDESBASE void __fastcall FillLine(Gr32::PColor32 Dst, int DstX, int DstY, int Length, Gr32::PColor32 AlphaValues);
	__classmethod Gr32::TColor32 __fastcall Linear3PointInterpolation(Gr32::TColor32 A, Gr32::TColor32 B, Gr32::TColor32 C, float WeightA, float WeightB, float WeightC);
	
public:
	virtual void __fastcall BeginRendering(void);
	virtual void __fastcall SetPoints(Gr32::TArrayOfFloatPoint Points);
	virtual void __fastcall SetColorPoints(TArrayOfColor32FloatPoint ColorPoints)/* overload */;
	virtual void __fastcall SetColorPoints(Gr32::TArrayOfFloatPoint Points, Gr32::TArrayOfColor32 Colors)/* overload */;
public:
	/* TObject.Create */ inline __fastcall TBarycentricGradientPolygonFiller(void) : TCustomSparsePointGradientPolygonFiller() { }
	/* TObject.Destroy */ inline __fastcall virtual ~TBarycentricGradientPolygonFiller(void) { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TCustomArbitrarySparsePointGradientPolygonFiller : public TCustomSparsePointGradientPolygonFiller
{
	typedef TCustomSparsePointGradientPolygonFiller inherited;
	
private:
	TArrayOfColor32FloatPoint FColorPoints;
	
protected:
	virtual int __fastcall GetCount(void);
	virtual Gr32::TColor32 __fastcall GetColor(int Index);
	virtual TColor32FloatPoint __fastcall GetColorPoint(int Index);
	virtual Gr32::TFloatPoint __fastcall GetPoint(int Index);
	virtual void __fastcall SetColor(int Index, const Gr32::TColor32 Value);
	virtual void __fastcall SetColorPoint(int Index, const TColor32FloatPoint &Value);
	virtual void __fastcall SetPoint(int Index, const Gr32::TFloatPoint &Value);
	
public:
	virtual void __fastcall Add(const Gr32::TFloatPoint &Point, Gr32::TColor32 Color)/* overload */;
	virtual void __fastcall Add(const TColor32FloatPoint &ColorPoint)/* overload */;
	virtual void __fastcall SetColorPoints(TArrayOfColor32FloatPoint ColorPoints)/* overload */;
	virtual void __fastcall SetColorPoints(Gr32::TArrayOfFloatPoint Points, Gr32::TArrayOfColor32 Colors)/* overload */;
	virtual void __fastcall SetPoints(Gr32::TArrayOfFloatPoint Points);
	virtual void __fastcall Clear(void);
public:
	/* TObject.Create */ inline __fastcall TCustomArbitrarySparsePointGradientPolygonFiller(void) : TCustomSparsePointGradientPolygonFiller() { }
	/* TObject.Destroy */ inline __fastcall virtual ~TCustomArbitrarySparsePointGradientPolygonFiller(void) { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TGourandShadedDelaunayTrianglesPolygonFiller : public TCustomArbitrarySparsePointGradientPolygonFiller
{
	typedef TCustomArbitrarySparsePointGradientPolygonFiller inherited;
	
	
private:
	typedef System::DynamicArray<TBarycentricGradientSampler*> _TGourandShadedDelaunayTrianglesPolygonFiller__1;
	
	
private:
	Gr32_vectorutils::TArrayOfTriangleVertexIndices FTriangles;
	_TGourandShadedDelaunayTrianglesPolygonFiller__1 FBarycentric;
	
protected:
	virtual Gr32_polygons::TFillLineEvent __fastcall GetFillLine(void);
	void __fastcall FillLine3(Gr32::PColor32 Dst, int DstX, int DstY, int Count, Gr32::PColor32 AlphaValues);
	HIDESBASE void __fastcall FillLine(Gr32::PColor32 Dst, int DstX, int DstY, int Count, Gr32::PColor32 AlphaValues);
	
public:
	virtual void __fastcall BeginRendering(void);
public:
	/* TObject.Create */ inline __fastcall TGourandShadedDelaunayTrianglesPolygonFiller(void) : TCustomArbitrarySparsePointGradientPolygonFiller() { }
	/* TObject.Destroy */ inline __fastcall virtual ~TGourandShadedDelaunayTrianglesPolygonFiller(void) { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TCustomGradientPolygonFiller : public Gr32_polygons::TCustomPolygonFiller
{
	typedef Gr32_polygons::TCustomPolygonFiller inherited;
	
private:
	TColor32Gradient* FGradient;
	bool FOwnsGradient;
	Gr32::TWrapMode FWrapMode;
	Gr32::TWrapProc FWrapProc;
	void __fastcall SetWrapMode(const Gr32::TWrapMode Value);
	
protected:
	void __fastcall GradientColorsChangedHandler(System::TObject* Sender);
	void __fastcall FillLineNone(Gr32::PColor32 Dst, int DstX, int DstY, int Length, Gr32::PColor32 AlphaValues);
	void __fastcall FillLineSolid(Gr32::PColor32 Dst, int DstX, int DstY, int Length, Gr32::PColor32 AlphaValues);
	virtual void __fastcall GradientFillerChanged(void);
	virtual void __fastcall WrapModeChanged(void);
	
public:
	__fastcall TCustomGradientPolygonFiller(void)/* overload */;
	__fastcall virtual TCustomGradientPolygonFiller(TColor32Gradient* ColorGradient)/* overload */;
	__fastcall virtual ~TCustomGradientPolygonFiller(void);
	__property TColor32Gradient* Gradient = {read=FGradient};
	__property Gr32::TWrapMode WrapMode = {read=FWrapMode, write=SetWrapMode, nodefault};
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TCustomGradientLookupTablePolygonFiller : public TCustomGradientPolygonFiller
{
	typedef TCustomGradientPolygonFiller inherited;
	
private:
	bool FLUTNeedsUpdate;
	bool FOwnsLUT;
	TColor32LookupTable* FGradientLUT;
	bool FUseLookUpTable;
	bool __fastcall GetLUTNeedsUpdate(void);
	void __fastcall SetUseLookUpTable(const bool Value);
	void __fastcall SetGradientLUT(TColor32LookupTable* const Value);
	
protected:
	virtual void __fastcall GradientFillerChanged(void);
	virtual void __fastcall UseLookUpTableChanged(void);
	void __fastcall LookUpTableChangedHandler(System::TObject* Sender);
	__property bool LookUpTableNeedsUpdate = {read=GetLUTNeedsUpdate, nodefault};
	
public:
	__fastcall TCustomGradientLookupTablePolygonFiller(void)/* overload */;
	__fastcall virtual TCustomGradientLookupTablePolygonFiller(TColor32LookupTable* LookupTable)/* overload */;
	__fastcall virtual ~TCustomGradientLookupTablePolygonFiller(void);
	__property TColor32LookupTable* GradientLUT = {read=FGradientLUT, write=SetGradientLUT};
	__property bool UseLookUpTable = {read=FUseLookUpTable, write=SetUseLookUpTable, nodefault};
public:
	/* TCustomGradientPolygonFiller.Create */ inline __fastcall virtual TCustomGradientLookupTablePolygonFiller(TColor32Gradient* ColorGradient)/* overload */ : TCustomGradientPolygonFiller(ColorGradient) { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TCustomLinearGradientPolygonFiller : public TCustomGradientLookupTablePolygonFiller
{
	typedef TCustomGradientLookupTablePolygonFiller inherited;
	
private:
	float FIncline;
	Gr32::TFloatPoint FStartPoint;
	Gr32::TFloatPoint FEndPoint;
	void __fastcall SetStartPoint(const Gr32::TFloatPoint &Value);
	void __fastcall SetEndPoint(const Gr32::TFloatPoint &Value);
	void __fastcall UpdateIncline(void);
	
protected:
	void __fastcall EndPointChanged(void);
	void __fastcall StartPointChanged(void);
	
public:
	virtual void __fastcall SimpleGradient(const Gr32::TFloatPoint &StartPoint, Gr32::TColor32 StartColor, const Gr32::TFloatPoint &EndPoint, Gr32::TColor32 EndColor);
	void __fastcall SimpleGradientX(const float StartX, Gr32::TColor32 StartColor, const float EndX, Gr32::TColor32 EndColor);
	void __fastcall SimpleGradientY(const float StartY, Gr32::TColor32 StartColor, const float EndY, Gr32::TColor32 EndColor);
	virtual void __fastcall SetPoints(const Gr32::TFloatPoint &StartPoint, const Gr32::TFloatPoint &EndPoint);
	__property Gr32::TFloatPoint StartPoint = {read=FStartPoint, write=SetStartPoint};
	__property Gr32::TFloatPoint EndPoint = {read=FEndPoint, write=SetEndPoint};
public:
	/* TCustomGradientLookupTablePolygonFiller.Create */ inline __fastcall TCustomLinearGradientPolygonFiller(void)/* overload */ : TCustomGradientLookupTablePolygonFiller() { }
	/* TCustomGradientLookupTablePolygonFiller.Create */ inline __fastcall virtual TCustomLinearGradientPolygonFiller(TColor32LookupTable* LookupTable)/* overload */ : TCustomGradientLookupTablePolygonFiller(LookupTable) { }
	/* TCustomGradientLookupTablePolygonFiller.Destroy */ inline __fastcall virtual ~TCustomLinearGradientPolygonFiller(void) { }
	
public:
	/* TCustomGradientPolygonFiller.Create */ inline __fastcall virtual TCustomLinearGradientPolygonFiller(TColor32Gradient* ColorGradient)/* overload */ : TCustomGradientLookupTablePolygonFiller(ColorGradient) { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TLinearGradientPolygonFiller : public TCustomLinearGradientPolygonFiller
{
	typedef TCustomLinearGradientPolygonFiller inherited;
	
private:
	float __fastcall ColorStopToScanLine(int Index, int Y);
	
protected:
	virtual Gr32_polygons::TFillLineEvent __fastcall GetFillLine(void);
	void __fastcall FillLineNegative(Gr32::PColor32 Dst, int DstX, int DstY, int Length, Gr32::PColor32 AlphaValues);
	void __fastcall FillLinePositive(Gr32::PColor32 Dst, int DstX, int DstY, int Length, Gr32::PColor32 AlphaValues);
	void __fastcall FillLineVertical(Gr32::PColor32 Dst, int DstX, int DstY, int Length, Gr32::PColor32 AlphaValues);
	void __fastcall FillLineVerticalExtreme(Gr32::PColor32 Dst, int DstX, int DstY, int Length, Gr32::PColor32 AlphaValues);
	void __fastcall FillLineVerticalPad(Gr32::PColor32 Dst, int DstX, int DstY, int Length, Gr32::PColor32 AlphaValues);
	void __fastcall FillLineVerticalPadExtreme(Gr32::PColor32 Dst, int DstX, int DstY, int Length, Gr32::PColor32 AlphaValues);
	void __fastcall FillLineVerticalWrap(Gr32::PColor32 Dst, int DstX, int DstY, int Length, Gr32::PColor32 AlphaValues);
	void __fastcall FillLineHorizontalPadPos(Gr32::PColor32 Dst, int DstX, int DstY, int Length, Gr32::PColor32 AlphaValues);
	void __fastcall FillLineHorizontalPadNeg(Gr32::PColor32 Dst, int DstX, int DstY, int Length, Gr32::PColor32 AlphaValues);
	void __fastcall FillLineHorizontalWrapNeg(Gr32::PColor32 Dst, int DstX, int DstY, int Length, Gr32::PColor32 AlphaValues);
	void __fastcall FillLineHorizontalWrapPos(Gr32::PColor32 Dst, int DstX, int DstY, int Length, Gr32::PColor32 AlphaValues);
	virtual void __fastcall UseLookUpTableChanged(void);
	virtual void __fastcall WrapModeChanged(void);
	
public:
	__fastcall virtual TLinearGradientPolygonFiller(TColor32Gradient* ColorGradient)/* overload */;
	__fastcall virtual TLinearGradientPolygonFiller(TColor32Gradient* ColorGradient, bool UseLookupTable)/* overload */;
	virtual void __fastcall BeginRendering(void);
public:
	/* TCustomGradientLookupTablePolygonFiller.Create */ inline __fastcall TLinearGradientPolygonFiller(void)/* overload */ : TCustomLinearGradientPolygonFiller() { }
	/* TCustomGradientLookupTablePolygonFiller.Create */ inline __fastcall virtual TLinearGradientPolygonFiller(TColor32LookupTable* LookupTable)/* overload */ : TCustomLinearGradientPolygonFiller(LookupTable) { }
	/* TCustomGradientLookupTablePolygonFiller.Destroy */ inline __fastcall virtual ~TLinearGradientPolygonFiller(void) { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TCustomRadialGradientPolygonFiller : public TCustomGradientLookupTablePolygonFiller
{
	typedef TCustomGradientLookupTablePolygonFiller inherited;
	
private:
	Gr32::TFloatRect FEllipseBounds;
	void __fastcall SetEllipseBounds(const Gr32::TFloatRect &Value);
	
protected:
	virtual void __fastcall EllipseBoundsChanged(void) = 0 ;
	
public:
	__property Gr32::TFloatRect EllipseBounds = {read=FEllipseBounds, write=SetEllipseBounds};
public:
	/* TCustomGradientLookupTablePolygonFiller.Create */ inline __fastcall TCustomRadialGradientPolygonFiller(void)/* overload */ : TCustomGradientLookupTablePolygonFiller() { }
	/* TCustomGradientLookupTablePolygonFiller.Create */ inline __fastcall virtual TCustomRadialGradientPolygonFiller(TColor32LookupTable* LookupTable)/* overload */ : TCustomGradientLookupTablePolygonFiller(LookupTable) { }
	/* TCustomGradientLookupTablePolygonFiller.Destroy */ inline __fastcall virtual ~TCustomRadialGradientPolygonFiller(void) { }
	
public:
	/* TCustomGradientPolygonFiller.Create */ inline __fastcall virtual TCustomRadialGradientPolygonFiller(TColor32Gradient* ColorGradient)/* overload */ : TCustomGradientLookupTablePolygonFiller(ColorGradient) { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TRadialGradientPolygonFiller : public TCustomRadialGradientPolygonFiller
{
	typedef TCustomRadialGradientPolygonFiller inherited;
	
private:
	Gr32::TFloatPoint FCenter;
	Gr32::TFloatPoint FRadius;
	float FRadScale;
	float FRadXInv;
	void __fastcall SetCenter(const Gr32::TFloatPoint &Value);
	void __fastcall SetRadius(const Gr32::TFloatPoint &Value);
	void __fastcall UpdateEllipseBounds(void);
	void __fastcall UpdateRadiusScale(void);
	
protected:
	virtual Gr32_polygons::TFillLineEvent __fastcall GetFillLine(void);
	virtual void __fastcall EllipseBoundsChanged(void);
	void __fastcall FillLinePad(Gr32::PColor32 Dst, int DstX, int DstY, int Length, Gr32::PColor32 AlphaValues);
	void __fastcall FillLineRepeat(Gr32::PColor32 Dst, int DstX, int DstY, int Length, Gr32::PColor32 AlphaValues);
	void __fastcall FillLineReflect(Gr32::PColor32 Dst, int DstX, int DstY, int Length, Gr32::PColor32 AlphaValues);
	
public:
	__fastcall TRadialGradientPolygonFiller(const Gr32::TFloatPoint &Radius)/* overload */;
	__fastcall TRadialGradientPolygonFiller(const Gr32::TFloatPoint &Radius, const Gr32::TFloatPoint &Center)/* overload */;
	virtual void __fastcall BeginRendering(void);
	__property Gr32::TFloatPoint Radius = {read=FRadius, write=SetRadius};
	__property Gr32::TFloatPoint Center = {read=FCenter, write=SetCenter};
public:
	/* TCustomGradientLookupTablePolygonFiller.Create */ inline __fastcall TRadialGradientPolygonFiller(void)/* overload */ : TCustomRadialGradientPolygonFiller() { }
	/* TCustomGradientLookupTablePolygonFiller.Create */ inline __fastcall virtual TRadialGradientPolygonFiller(TColor32LookupTable* LookupTable)/* overload */ : TCustomRadialGradientPolygonFiller(LookupTable) { }
	/* TCustomGradientLookupTablePolygonFiller.Destroy */ inline __fastcall virtual ~TRadialGradientPolygonFiller(void) { }
	
public:
	/* TCustomGradientPolygonFiller.Create */ inline __fastcall virtual TRadialGradientPolygonFiller(TColor32Gradient* ColorGradient)/* overload */ : TCustomRadialGradientPolygonFiller(ColorGradient) { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TSVGRadialGradientPolygonFiller : public TCustomRadialGradientPolygonFiller
{
	typedef TCustomRadialGradientPolygonFiller inherited;
	
private:
	Gr32::TFloatPoint FOffset;
	Gr32::TFloatPoint FRadius;
	Gr32::TFloatPoint FCenter;
	Gr32::TFloatPoint FFocalPt;
	float FVertDist;
	Gr32::TFloatPoint FFocalPointNative;
	void __fastcall SetFocalPoint(const Gr32::TFloatPoint &Value);
	void __fastcall InitMembers(void);
	
protected:
	virtual Gr32_polygons::TFillLineEvent __fastcall GetFillLine(void);
	virtual void __fastcall EllipseBoundsChanged(void);
	void __fastcall FillLineEllipse(Gr32::PColor32 Dst, int DstX, int DstY, int Length, Gr32::PColor32 AlphaValues);
	
public:
	__fastcall TSVGRadialGradientPolygonFiller(const Gr32::TFloatRect &EllipseBounds)/* overload */;
	__fastcall TSVGRadialGradientPolygonFiller(const Gr32::TFloatRect &EllipseBounds, const Gr32::TFloatPoint &FocalPoint)/* overload */;
	virtual void __fastcall BeginRendering(void);
	void __fastcall SetParameters(const Gr32::TFloatRect &EllipseBounds)/* overload */;
	void __fastcall SetParameters(const Gr32::TFloatRect &EllipseBounds, const Gr32::TFloatPoint &FocalPoint)/* overload */;
	__property Gr32::TFloatPoint FocalPoint = {read=FFocalPointNative, write=SetFocalPoint};
public:
	/* TCustomGradientLookupTablePolygonFiller.Create */ inline __fastcall TSVGRadialGradientPolygonFiller(void)/* overload */ : TCustomRadialGradientPolygonFiller() { }
	/* TCustomGradientLookupTablePolygonFiller.Create */ inline __fastcall virtual TSVGRadialGradientPolygonFiller(TColor32LookupTable* LookupTable)/* overload */ : TCustomRadialGradientPolygonFiller(LookupTable) { }
	/* TCustomGradientLookupTablePolygonFiller.Destroy */ inline __fastcall virtual ~TSVGRadialGradientPolygonFiller(void) { }
	
public:
	/* TCustomGradientPolygonFiller.Create */ inline __fastcall virtual TSVGRadialGradientPolygonFiller(TColor32Gradient* ColorGradient)/* overload */ : TCustomRadialGradientPolygonFiller(ColorGradient) { }
	
};

#pragma pack(pop)

//-- var, const, procedure ---------------------------------------------------
extern DELPHI_PACKAGE TColor32FloatPoint __fastcall Color32FloatPoint(Gr32::TColor32 Color, const Gr32::TFloatPoint &Point)/* overload */;
extern DELPHI_PACKAGE TColor32FloatPoint __fastcall Color32FloatPoint(Gr32::TColor32 Color, float X, float Y)/* overload */;
extern DELPHI_PACKAGE TColor32GradientStop __fastcall Color32GradientStop(float Offset, Gr32::TColor32 Color)/* overload */;
}	/* namespace Gr32_colorgradients */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_GR32_COLORGRADIENTS)
using namespace Gr32_colorgradients;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// Gr32_colorgradientsHPP
