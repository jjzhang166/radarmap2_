﻿// CodeGear C++Builder
// Copyright (c) 1995, 2016 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'GR32_Geometry.pas' rev: 31.00 (Windows)

#ifndef Gr32_geometryHPP
#define Gr32_geometryHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>
#include <SysInit.hpp>
#include <System.Math.hpp>
#include <System.Types.hpp>
#include <GR32.hpp>

//-- user supplied -----------------------------------------------------------

namespace Gr32_geometry
{
//-- forward type declarations -----------------------------------------------
//-- type declarations -------------------------------------------------------
enum DECLSPEC_DENUM TLinePos : unsigned char { lpStart, lpEnd, lpBoth, lpNeither };

//-- var, const, procedure ---------------------------------------------------
static const System::Extended CRad01 = 1.745329E-02;
static const System::Extended CRad30 = 5.235988E-01;
static const System::Extended CRad45 = 7.853982E-01;
static const System::Extended CRad60 = 1.047198E+00;
static const System::Extended CRad90 = 1.570796E+00;
static const System::Extended CRad180 = 3.141593E+00;
static const System::Extended CRad270 = 4.712389E+00;
static const System::Extended CRad360 = 6.283185E+00;
static const System::Extended CDegToRad = 1.745329E-02;
static const System::Extended CRadToDeg = 5.729578E+01;
extern DELPHI_PACKAGE Gr32::TFloatPoint __fastcall Average(const Gr32::TFloatPoint &V1, const Gr32::TFloatPoint &V2)/* overload */;
extern DELPHI_PACKAGE float __fastcall CrossProduct(const Gr32::TFloatPoint &V1, const Gr32::TFloatPoint &V2)/* overload */;
extern DELPHI_PACKAGE float __fastcall Dot(const Gr32::TFloatPoint &V1, const Gr32::TFloatPoint &V2)/* overload */;
extern DELPHI_PACKAGE float __fastcall Distance(const Gr32::TFloatPoint &V1, const Gr32::TFloatPoint &V2)/* overload */;
extern DELPHI_PACKAGE float __fastcall SqrDistance(const Gr32::TFloatPoint &V1, const Gr32::TFloatPoint &V2)/* overload */;
extern DELPHI_PACKAGE Gr32::TFloatPoint __fastcall GetPointAtAngleFromPoint(const Gr32::TFloatPoint &Pt, const float Dist, const float Radians)/* overload */;
extern DELPHI_PACKAGE float __fastcall GetAngleOfPt2FromPt1(const Gr32::TFloatPoint &Pt1, const Gr32::TFloatPoint &Pt2)/* overload */;
extern DELPHI_PACKAGE Gr32::TFloatPoint __fastcall GetUnitVector(const Gr32::TFloatPoint &Pt1, const Gr32::TFloatPoint &Pt2)/* overload */;
extern DELPHI_PACKAGE Gr32::TFloatPoint __fastcall GetUnitNormal(const Gr32::TFloatPoint &Pt1, const Gr32::TFloatPoint &Pt2)/* overload */;
extern DELPHI_PACKAGE Gr32::TFloatPoint __fastcall OffsetPoint(const Gr32::TFloatPoint &Pt, float DeltaX, float DeltaY)/* overload */;
extern DELPHI_PACKAGE Gr32::TFloatPoint __fastcall OffsetPoint(const Gr32::TFloatPoint &Pt, const Gr32::TFloatPoint &Delta)/* overload */;
extern DELPHI_PACKAGE Gr32::TFloatRect __fastcall OffsetRect(const Gr32::TFloatRect &Rct, const float DeltaX, const float DeltaY)/* overload */;
extern DELPHI_PACKAGE Gr32::TFloatRect __fastcall OffsetRect(const Gr32::TFloatRect &Rct, const Gr32::TFloatPoint &Delta)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfFloatPoint __fastcall Shorten(const Gr32::TArrayOfFloatPoint Pts, float Delta, TLinePos LinePos)/* overload */;
extern DELPHI_PACKAGE bool __fastcall PointInPolygon(const Gr32::TFloatPoint &Pt, const Gr32::TArrayOfFloatPoint Pts)/* overload */;
extern DELPHI_PACKAGE bool __fastcall SegmentIntersect(const Gr32::TFloatPoint &P1, const Gr32::TFloatPoint &P2, const Gr32::TFloatPoint &P3, const Gr32::TFloatPoint &P4, /* out */ Gr32::TFloatPoint &IntersectPoint)/* overload */;
extern DELPHI_PACKAGE float __fastcall PerpendicularDistance(const Gr32::TFloatPoint &P, const Gr32::TFloatPoint &P1, const Gr32::TFloatPoint &P2)/* overload */;
extern DELPHI_PACKAGE Gr32::TFixedPoint __fastcall Average(const Gr32::TFixedPoint &V1, const Gr32::TFixedPoint &V2)/* overload */;
extern DELPHI_PACKAGE Gr32::TFixed __fastcall CrossProduct(const Gr32::TFixedPoint &V1, const Gr32::TFixedPoint &V2)/* overload */;
extern DELPHI_PACKAGE Gr32::TFixed __fastcall Dot(const Gr32::TFixedPoint &V1, const Gr32::TFixedPoint &V2)/* overload */;
extern DELPHI_PACKAGE Gr32::TFixed __fastcall Distance(const Gr32::TFixedPoint &V1, const Gr32::TFixedPoint &V2)/* overload */;
extern DELPHI_PACKAGE Gr32::TFixed __fastcall SqrDistance(const Gr32::TFixedPoint &V1, const Gr32::TFixedPoint &V2)/* overload */;
extern DELPHI_PACKAGE Gr32::TFixedPoint __fastcall GetPointAtAngleFromPoint(const Gr32::TFixedPoint &Pt, const float Dist, const float Radians)/* overload */;
extern DELPHI_PACKAGE float __fastcall GetAngleOfPt2FromPt1(const Gr32::TFixedPoint &Pt1, const Gr32::TFixedPoint &Pt2)/* overload */;
extern DELPHI_PACKAGE Gr32::TFloatPoint __fastcall GetUnitVector(const Gr32::TFixedPoint &Pt1, const Gr32::TFixedPoint &Pt2)/* overload */;
extern DELPHI_PACKAGE Gr32::TFloatPoint __fastcall GetUnitNormal(const Gr32::TFixedPoint &Pt1, const Gr32::TFixedPoint &Pt2)/* overload */;
extern DELPHI_PACKAGE Gr32::TFixedPoint __fastcall OffsetPoint(const Gr32::TFixedPoint &Pt, Gr32::TFixed DeltaX, Gr32::TFixed DeltaY)/* overload */;
extern DELPHI_PACKAGE Gr32::TFixedPoint __fastcall OffsetPoint(const Gr32::TFixedPoint &Pt, float DeltaX, float DeltaY)/* overload */;
extern DELPHI_PACKAGE Gr32::TFixedPoint __fastcall OffsetPoint(const Gr32::TFixedPoint &Pt, const Gr32::TFixedPoint &Delta)/* overload */;
extern DELPHI_PACKAGE Gr32::TFixedPoint __fastcall OffsetPoint(const Gr32::TFixedPoint &Pt, const Gr32::TFloatPoint &Delta)/* overload */;
extern DELPHI_PACKAGE Gr32::TFixedRect __fastcall OffsetRect(const Gr32::TFixedRect &Rct, const Gr32::TFixed DeltaX, const Gr32::TFixed DeltaY)/* overload */;
extern DELPHI_PACKAGE Gr32::TFixedRect __fastcall OffsetRect(const Gr32::TFixedRect &Rct, const Gr32::TFixedPoint &Delta)/* overload */;
extern DELPHI_PACKAGE Gr32::TFixedRect __fastcall OffsetRect(const Gr32::TFixedRect &Rct, const float DeltaX, const float DeltaY)/* overload */;
extern DELPHI_PACKAGE Gr32::TFixedRect __fastcall OffsetRect(const Gr32::TFixedRect &Rct, const Gr32::TFloatPoint &Delta)/* overload */;
extern DELPHI_PACKAGE Gr32::TArrayOfFixedPoint __fastcall Shorten(const Gr32::TArrayOfFixedPoint Pts, float Delta, TLinePos LinePos)/* overload */;
extern DELPHI_PACKAGE bool __fastcall PointInPolygon(const Gr32::TFixedPoint &Pt, const Gr32::TFixedPoint *Pts, const int Pts_High)/* overload */;
extern DELPHI_PACKAGE bool __fastcall SegmentIntersect(const Gr32::TFixedPoint &P1, const Gr32::TFixedPoint &P2, const Gr32::TFixedPoint &P3, const Gr32::TFixedPoint &P4, /* out */ Gr32::TFixedPoint &IntersectPoint)/* overload */;
extern DELPHI_PACKAGE Gr32::TFixed __fastcall PerpendicularDistance(const Gr32::TFixedPoint &P, const Gr32::TFixedPoint &P1, const Gr32::TFixedPoint &P2)/* overload */;
extern DELPHI_PACKAGE System::Types::TPoint __fastcall Average(const System::Types::TPoint &V1, const System::Types::TPoint &V2)/* overload */;
extern DELPHI_PACKAGE int __fastcall CrossProduct(const System::Types::TPoint &V1, const System::Types::TPoint &V2)/* overload */;
extern DELPHI_PACKAGE int __fastcall Dot(const System::Types::TPoint &V1, const System::Types::TPoint &V2)/* overload */;
extern DELPHI_PACKAGE float __fastcall Distance(const System::Types::TPoint &V1, const System::Types::TPoint &V2)/* overload */;
extern DELPHI_PACKAGE int __fastcall SqrDistance(const System::Types::TPoint &V1, const System::Types::TPoint &V2)/* overload */;
extern DELPHI_PACKAGE System::Types::TPoint __fastcall OffsetPoint(const System::Types::TPoint &Pt, int DeltaX, int DeltaY)/* overload */;
extern DELPHI_PACKAGE System::Types::TPoint __fastcall OffsetPoint(const System::Types::TPoint &Pt, const System::Types::TPoint &Delta)/* overload */;
extern DELPHI_PACKAGE float __fastcall PerpendicularDistance(const System::Types::TPoint &P, const System::Types::TPoint &P1, const System::Types::TPoint &P2)/* overload */;
}	/* namespace Gr32_geometry */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_GR32_GEOMETRY)
using namespace Gr32_geometry;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// Gr32_geometryHPP
