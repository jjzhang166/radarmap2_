﻿// CodeGear C++Builder
// Copyright (c) 1995, 2016 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'GR32_Dsgn_Misc.pas' rev: 31.00 (Windows)

#ifndef Gr32_dsgn_miscHPP
#define Gr32_dsgn_miscHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>
#include <SysInit.hpp>
#include <Winapi.Windows.hpp>
#include <DesignIntf.hpp>
#include <DesignEditors.hpp>
#include <ToolsAPI.hpp>
#include <System.Classes.hpp>
#include <System.TypInfo.hpp>
#include <GR32_Containers.hpp>

//-- user supplied -----------------------------------------------------------

namespace Gr32_dsgn_misc
{
//-- forward type declarations -----------------------------------------------
class DELPHICLASS TCustomClassProperty;
class DELPHICLASS TKernelClassProperty;
class DELPHICLASS TResamplerClassProperty;
//-- type declarations -------------------------------------------------------
#pragma pack(push,4)
class PASCALIMPLEMENTATION TCustomClassProperty : public Designeditors::TClassProperty
{
	typedef Designeditors::TClassProperty inherited;
	
private:
	bool __fastcall HasSubProperties(void);
	
protected:
	__classmethod virtual Gr32_containers::TClassList* __fastcall GetClassList();
	virtual void __fastcall SetClassName(const System::UnicodeString CustomClass) = 0 ;
	virtual System::TObject* __fastcall GetObject(void) = 0 ;
	
public:
	virtual Designintf::TPropertyAttributes __fastcall GetAttributes(void);
	virtual void __fastcall GetValues(System::Classes::TGetStrProc Proc);
	virtual void __fastcall SetValue(const System::UnicodeString Value)/* overload */;
	virtual System::UnicodeString __fastcall GetValue(void);
public:
	/* TPropertyEditor.Create */ inline __fastcall virtual TCustomClassProperty(const Designintf::_di_IDesigner ADesigner, int APropCount) : Designeditors::TClassProperty(ADesigner, APropCount) { }
	/* TPropertyEditor.Destroy */ inline __fastcall virtual ~TCustomClassProperty(void) { }
	
	/* Hoisted overloads: */
	
public:
	inline void __fastcall  SetValue(const System::WideString Value){ Designeditors::TPropertyEditor::SetValue(Value); }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TKernelClassProperty : public TCustomClassProperty
{
	typedef TCustomClassProperty inherited;
	
protected:
	__classmethod virtual Gr32_containers::TClassList* __fastcall GetClassList();
	virtual void __fastcall SetClassName(const System::UnicodeString CustomClass);
	virtual System::TObject* __fastcall GetObject(void);
public:
	/* TPropertyEditor.Create */ inline __fastcall virtual TKernelClassProperty(const Designintf::_di_IDesigner ADesigner, int APropCount) : TCustomClassProperty(ADesigner, APropCount) { }
	/* TPropertyEditor.Destroy */ inline __fastcall virtual ~TKernelClassProperty(void) { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TResamplerClassProperty : public TCustomClassProperty
{
	typedef TCustomClassProperty inherited;
	
protected:
	__classmethod virtual Gr32_containers::TClassList* __fastcall GetClassList();
	virtual void __fastcall SetClassName(const System::UnicodeString CustomClass);
	virtual System::TObject* __fastcall GetObject(void);
public:
	/* TPropertyEditor.Create */ inline __fastcall virtual TResamplerClassProperty(const Designintf::_di_IDesigner ADesigner, int APropCount) : TCustomClassProperty(ADesigner, APropCount) { }
	/* TPropertyEditor.Destroy */ inline __fastcall virtual ~TResamplerClassProperty(void) { }
	
};

#pragma pack(pop)

//-- var, const, procedure ---------------------------------------------------
}	/* namespace Gr32_dsgn_misc */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_GR32_DSGN_MISC)
using namespace Gr32_dsgn_misc;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// Gr32_dsgn_miscHPP
