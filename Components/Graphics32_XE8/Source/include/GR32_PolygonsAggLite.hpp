﻿// CodeGear C++Builder
// Copyright (c) 1995, 2016 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'GR32_PolygonsAggLite.pas' rev: 31.00 (Windows)

#ifndef Gr32_polygonsaggliteHPP
#define Gr32_polygonsaggliteHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>
#include <SysInit.hpp>
#include <System.Types.hpp>
#include <GR32.hpp>
#include <GR32_Polygons.hpp>
#include <GR32_Transforms.hpp>
#include <System.Classes.hpp>

//-- user supplied -----------------------------------------------------------

namespace Gr32_polygonsagglite
{
//-- forward type declarations -----------------------------------------------
class DELPHICLASS TPolygonRenderer32AggLite;
//-- type declarations -------------------------------------------------------
class PASCALIMPLEMENTATION TPolygonRenderer32AggLite : public Gr32_polygons::TPolygonRenderer32
{
	typedef Gr32_polygons::TPolygonRenderer32 inherited;
	
protected:
	void __fastcall Render(void * CellsPtr, int MinX, int MaxX);
	
public:
	virtual void __fastcall PolygonFS(const Gr32::TArrayOfFloatPoint Points, const Gr32::TFloatRect &ClipRect)/* overload */;
	virtual void __fastcall PolyPolygonFS(const Gr32::TArrayOfArrayOfFloatPoint Points, const Gr32::TFloatRect &ClipRect)/* overload */;
public:
	/* TPolygonRenderer32.Create */ inline __fastcall TPolygonRenderer32AggLite(Gr32::TBitmap32* Bitmap, Gr32_polygons::TPolyFillMode Fillmode)/* overload */ : Gr32_polygons::TPolygonRenderer32(Bitmap, Fillmode) { }
	
public:
	/* TThreadPersistent.Destroy */ inline __fastcall virtual ~TPolygonRenderer32AggLite(void) { }
	
	/* Hoisted overloads: */
	
public:
	inline void __fastcall  PolygonFS(const Gr32::TArrayOfFloatPoint Points){ Gr32_polygons::TPolygonRenderer32::PolygonFS(Points); }
	inline void __fastcall  PolygonFS(const Gr32::TArrayOfFloatPoint Points, const Gr32::TFloatRect &ClipRect, Gr32_transforms::TTransformation* Transformation){ Gr32_polygons::TCustomPolygonRenderer::PolygonFS(Points, ClipRect, Transformation); }
	inline void __fastcall  PolyPolygonFS(const Gr32::TArrayOfArrayOfFloatPoint Points){ Gr32_polygons::TPolygonRenderer32::PolyPolygonFS(Points); }
	inline void __fastcall  PolyPolygonFS(const Gr32::TArrayOfArrayOfFloatPoint Points, const Gr32::TFloatRect &ClipRect, Gr32_transforms::TTransformation* Transformation){ Gr32_polygons::TCustomPolygonRenderer::PolyPolygonFS(Points, ClipRect, Transformation); }
	
};


//-- var, const, procedure ---------------------------------------------------
extern DELPHI_PACKAGE void __fastcall PolyPolygonFS_AggLite(Gr32::TBitmap32* Bitmap, const Gr32::TArrayOfArrayOfFloatPoint Points, Gr32::TColor32 Color, Gr32_polygons::TPolyFillMode FillMode = (Gr32_polygons::TPolyFillMode)(0x0), Gr32_transforms::TTransformation* Transformation = (Gr32_transforms::TTransformation*)(0x0))/* overload */;
extern DELPHI_PACKAGE void __fastcall PolygonFS_AggLite(Gr32::TBitmap32* Bitmap, const Gr32::TArrayOfFloatPoint Points, Gr32::TColor32 Color, Gr32_polygons::TPolyFillMode FillMode = (Gr32_polygons::TPolyFillMode)(0x0), Gr32_transforms::TTransformation* Transformation = (Gr32_transforms::TTransformation*)(0x0))/* overload */;
extern DELPHI_PACKAGE void __fastcall PolyPolygonFS_AggLite(Gr32::TBitmap32* Bitmap, const Gr32::TArrayOfArrayOfFloatPoint Points, Gr32_polygons::TCustomPolygonFiller* Filler, Gr32_polygons::TPolyFillMode FillMode = (Gr32_polygons::TPolyFillMode)(0x0), Gr32_transforms::TTransformation* Transformation = (Gr32_transforms::TTransformation*)(0x0))/* overload */;
extern DELPHI_PACKAGE void __fastcall PolygonFS_AggLite(Gr32::TBitmap32* Bitmap, const Gr32::TArrayOfFloatPoint Points, Gr32_polygons::TCustomPolygonFiller* Filler, Gr32_polygons::TPolyFillMode FillMode = (Gr32_polygons::TPolyFillMode)(0x0), Gr32_transforms::TTransformation* Transformation = (Gr32_transforms::TTransformation*)(0x0))/* overload */;
extern DELPHI_PACKAGE void __fastcall PolyPolygonFS_AggLite(Gr32::TBitmap32* Bitmap, const Gr32::TArrayOfArrayOfFloatPoint Points, const System::Types::TRect &ClipRect, Gr32::TColor32 Color, Gr32_polygons::TPolyFillMode FillMode = (Gr32_polygons::TPolyFillMode)(0x0), Gr32_transforms::TTransformation* Transformation = (Gr32_transforms::TTransformation*)(0x0))/* overload */;
extern DELPHI_PACKAGE void __fastcall PolygonFS_AggLite(Gr32::TBitmap32* Bitmap, const Gr32::TArrayOfFloatPoint Points, const System::Types::TRect &ClipRect, Gr32::TColor32 Color, Gr32_polygons::TPolyFillMode FillMode = (Gr32_polygons::TPolyFillMode)(0x0), Gr32_transforms::TTransformation* Transformation = (Gr32_transforms::TTransformation*)(0x0))/* overload */;
extern DELPHI_PACKAGE void __fastcall PolyPolygonFS_AggLite(Gr32::TBitmap32* Bitmap, const Gr32::TArrayOfArrayOfFloatPoint Points, const System::Types::TRect &ClipRect, Gr32_polygons::TCustomPolygonFiller* Filler, Gr32_polygons::TPolyFillMode FillMode = (Gr32_polygons::TPolyFillMode)(0x0), Gr32_transforms::TTransformation* Transformation = (Gr32_transforms::TTransformation*)(0x0))/* overload */;
extern DELPHI_PACKAGE void __fastcall PolygonFS_AggLite(Gr32::TBitmap32* Bitmap, const Gr32::TArrayOfFloatPoint Points, const System::Types::TRect &ClipRect, Gr32_polygons::TCustomPolygonFiller* Filler, Gr32_polygons::TPolyFillMode FillMode = (Gr32_polygons::TPolyFillMode)(0x0), Gr32_transforms::TTransformation* Transformation = (Gr32_transforms::TTransformation*)(0x0))/* overload */;
extern DELPHI_PACKAGE void __fastcall PolyPolylineFS_AggLite(Gr32::TBitmap32* Bitmap, const Gr32::TArrayOfArrayOfFloatPoint Points, Gr32::TColor32 Color, bool Closed = false, float StrokeWidth = 1.000000E+00f, Gr32_polygons::TJoinStyle JoinStyle = (Gr32_polygons::TJoinStyle)(0x0), Gr32_polygons::TEndStyle EndStyle = (Gr32_polygons::TEndStyle)(0x0), float MiterLimit = 4.000000E+00f, Gr32_transforms::TTransformation* Transformation = (Gr32_transforms::TTransformation*)(0x0))/* overload */;
extern DELPHI_PACKAGE void __fastcall PolyPolylineFS_AggLite(Gr32::TBitmap32* Bitmap, const Gr32::TArrayOfArrayOfFloatPoint Points, Gr32_polygons::TCustomPolygonFiller* Filler, bool Closed = false, float StrokeWidth = 1.000000E+00f, Gr32_polygons::TJoinStyle JoinStyle = (Gr32_polygons::TJoinStyle)(0x0), Gr32_polygons::TEndStyle EndStyle = (Gr32_polygons::TEndStyle)(0x0), float MiterLimit = 4.000000E+00f, Gr32_transforms::TTransformation* Transformation = (Gr32_transforms::TTransformation*)(0x0))/* overload */;
extern DELPHI_PACKAGE void __fastcall PolylineFS_AggLite(Gr32::TBitmap32* Bitmap, const Gr32::TArrayOfFloatPoint Points, Gr32::TColor32 Color, bool Closed = false, float StrokeWidth = 1.000000E+00f, Gr32_polygons::TJoinStyle JoinStyle = (Gr32_polygons::TJoinStyle)(0x0), Gr32_polygons::TEndStyle EndStyle = (Gr32_polygons::TEndStyle)(0x0), float MiterLimit = 4.000000E+00f, Gr32_transforms::TTransformation* Transformation = (Gr32_transforms::TTransformation*)(0x0))/* overload */;
extern DELPHI_PACKAGE void __fastcall PolylineFS_AggLite(Gr32::TBitmap32* Bitmap, const Gr32::TArrayOfFloatPoint Points, Gr32_polygons::TCustomPolygonFiller* Filler, bool Closed = false, float StrokeWidth = 1.000000E+00f, Gr32_polygons::TJoinStyle JoinStyle = (Gr32_polygons::TJoinStyle)(0x0), Gr32_polygons::TEndStyle EndStyle = (Gr32_polygons::TEndStyle)(0x0), float MiterLimit = 4.000000E+00f, Gr32_transforms::TTransformation* Transformation = (Gr32_transforms::TTransformation*)(0x0))/* overload */;
extern DELPHI_PACKAGE void __fastcall DashLineFS_AggLite(Gr32::TBitmap32* Bitmap, const Gr32::TArrayOfFloatPoint Points, const Gr32::TArrayOfFloat Dashes, Gr32::TColor32 Color, bool Closed = false, float Width = 1.000000E+00f)/* overload */;
extern DELPHI_PACKAGE void __fastcall DashLineFS_AggLite(Gr32::TBitmap32* Bitmap, const Gr32::TArrayOfFloatPoint Points, const Gr32::TArrayOfFloat Dashes, Gr32::TColor32 FillColor, Gr32::TColor32 StrokeColor, bool Closed, float Width, float StrokeWidth = 2.000000E+00f)/* overload */;
extern DELPHI_PACKAGE void __fastcall DashLineFS_AggLite(Gr32::TBitmap32* Bitmap, const Gr32::TArrayOfFloatPoint Points, const Gr32::TArrayOfFloat Dashes, Gr32_polygons::TCustomPolygonFiller* Filler, bool Closed = false, float Width = 1.000000E+00f)/* overload */;
extern DELPHI_PACKAGE void __fastcall DashLineFS_AggLite(Gr32::TBitmap32* Bitmap, const Gr32::TArrayOfFloatPoint Points, const Gr32::TArrayOfFloat Dashes, Gr32_polygons::TCustomPolygonFiller* Filler, Gr32::TColor32 StrokeColor, bool Closed, float Width, float StrokeWidth = 2.000000E+00f)/* overload */;
}	/* namespace Gr32_polygonsagglite */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_GR32_POLYGONSAGGLITE)
using namespace Gr32_polygonsagglite;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// Gr32_polygonsaggliteHPP
