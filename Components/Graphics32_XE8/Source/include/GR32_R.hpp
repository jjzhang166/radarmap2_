﻿// CodeGear C++Builder
// Copyright (c) 1995, 2016 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'GR32_R.dpk' rev: 31.00 (Windows)

#ifndef Gr32_rHPP
#define Gr32_rHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// (rtl)
#include <SysInit.hpp>
#include <GR32.hpp>
#include <GR32_Backends.hpp>
#include <GR32_Backends_Generic.hpp>
#include <GR32_Backends_VCL.hpp>
#include <GR32_Bindings.hpp>
#include <GR32_Blend.hpp>
#include <GR32_Brushes.hpp>
#include <GR32_ColorGradients.hpp>
#include <GR32_ColorPicker.hpp>
#include <GR32_Containers.hpp>
#include <GR32_ExtImage.hpp>
#include <GR32_Filters.hpp>
#include <GR32_Geometry.hpp>
#include <GR32_Image.hpp>
#include <GR32_Layers.hpp>
#include <GR32_LowLevel.hpp>
#include <GR32_Math.hpp>
#include <GR32_MicroTiles.hpp>
#include <GR32_OrdinalMaps.hpp>
#include <GR32_Paths.hpp>
#include <GR32_Polygons.hpp>
#include <GR32_Polygons_old.hpp>
#include <GR32_PolygonsAggLite.hpp>
#include <GR32_RangeBars.hpp>
#include <GR32_Rasterizers.hpp>
#include <GR32_RepaintOpt.hpp>
#include <GR32_Resamplers.hpp>
#include <GR32_System.hpp>
#include <GR32_Transforms.hpp>
#include <GR32_VectorMaps.hpp>
#include <GR32_VectorUtils.hpp>
#include <GR32_VPR.hpp>
#include <GR32_Text_VCL.hpp>
#include <GR32_XPThemes.hpp>
#include <Winapi.Windows.hpp>	// (rtl)
#include <Winapi.PsAPI.hpp>	// (rtl)
#include <System.Character.hpp>	// (rtl)
#include <System.Internal.ExcUtils.hpp>	// (rtl)
#include <System.SysUtils.hpp>	// (rtl)
#include <System.VarUtils.hpp>	// (rtl)
#include <System.Variants.hpp>	// (rtl)
#include <System.Rtti.hpp>	// (rtl)
#include <System.TypInfo.hpp>	// (rtl)
#include <System.Math.hpp>	// (rtl)
#include <System.Generics.Defaults.hpp>	// (rtl)
#include <System.Classes.hpp>	// (rtl)
#include <System.TimeSpan.hpp>	// (rtl)
#include <System.DateUtils.hpp>	// (rtl)
#include <System.IOUtils.hpp>	// (rtl)
#include <System.Win.Registry.hpp>	// (rtl)
#include <Vcl.Graphics.hpp>	// (vcl)
#include <System.Actions.hpp>	// (rtl)
#include <Vcl.ActnList.hpp>	// (vcl)
#include <System.HelpIntfs.hpp>	// (rtl)
#include <System.SyncObjs.hpp>	// (rtl)
#include <Winapi.UxTheme.hpp>	// (rtl)
#include <Vcl.GraphUtil.hpp>	// (vcl)
#include <Vcl.StdCtrls.hpp>	// (vcl)
#include <Winapi.ShellAPI.hpp>	// (rtl)
#include <Vcl.Printers.hpp>	// (vcl)
#include <Vcl.Clipbrd.hpp>	// (vcl)
#include <Vcl.ComCtrls.hpp>	// (vcl)
#include <Vcl.Dialogs.hpp>	// (vcl)
#include <Vcl.ExtCtrls.hpp>	// (vcl)
#include <Vcl.Themes.hpp>	// (vcl)
#include <System.AnsiStrings.hpp>	// (rtl)
#include <System.Win.ComObj.hpp>	// (rtl)
#include <Winapi.FlatSB.hpp>	// (rtl)
#include <Vcl.Forms.hpp>	// (vcl)
#include <Vcl.Menus.hpp>	// (vcl)
#include <Winapi.MsCTF.hpp>	// (rtl)
#include <Vcl.Controls.hpp>	// (vcl)

//-- user supplied -----------------------------------------------------------

namespace Gr32_r
{
//-- forward type declarations -----------------------------------------------
//-- type declarations -------------------------------------------------------
//-- var, const, procedure ---------------------------------------------------
}	/* namespace Gr32_r */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_GR32_R)
using namespace Gr32_r;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// Gr32_rHPP
