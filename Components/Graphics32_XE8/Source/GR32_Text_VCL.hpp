﻿// CodeGear C++Builder
// Copyright (c) 1995, 2016 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'GR32_Text_VCL.pas' rev: 31.00 (Windows)

#ifndef Gr32_text_vclHPP
#define Gr32_text_vclHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>
#include <SysInit.hpp>
#include <Winapi.Windows.hpp>
#include <System.Types.hpp>
#include <GR32.hpp>
#include <GR32_Paths.hpp>

//-- user supplied -----------------------------------------------------------

namespace Gr32_text_vcl
{
//-- forward type declarations -----------------------------------------------
//-- type declarations -------------------------------------------------------
enum DECLSPEC_DENUM TTextHinting : unsigned char { thNone, thNoHorz, thHinting };

typedef System::StaticArray<tagKERNINGPAIR, 1> TKerningPairArray;

typedef TKerningPairArray *PKerningPairArray;

//-- var, const, procedure ---------------------------------------------------
static const System::Int8 DT_LEFT = System::Int8(0x0);
static const System::Int8 DT_CENTER = System::Int8(0x1);
static const System::Int8 DT_RIGHT = System::Int8(0x2);
static const System::Int8 DT_VCENTER = System::Int8(0x4);
static const System::Int8 DT_BOTTOM = System::Int8(0x8);
static const System::Int8 DT_WORDBREAK = System::Int8(0x10);
static const System::Int8 DT_SINGLELINE = System::Int8(0x20);
static const System::Int8 DT_JUSTIFY = System::Int8(0x3);
static const System::Int8 DT_HORZ_ALIGN_MASK = System::Int8(0x3);
extern DELPHI_PACKAGE void __fastcall TextToPath(HFONT Font, Gr32_paths::TCustomPath* Path, const Gr32::TFloatRect &ARect, const System::WideString Text, unsigned Flags = (unsigned)(0x0));
extern DELPHI_PACKAGE Gr32::TArrayOfArrayOfFloatPoint __fastcall TextToPolyPolygon(HFONT Font, const Gr32::TFloatRect &ARect, const System::WideString Text, unsigned Flags = (unsigned)(0x0));
extern DELPHI_PACKAGE Gr32::TFloatRect __fastcall MeasureTextDC(HDC DC, const Gr32::TFloatRect &ARect, const System::WideString Text, unsigned Flags = (unsigned)(0x0))/* overload */;
extern DELPHI_PACKAGE Gr32::TFloatRect __fastcall MeasureText(HFONT Font, const Gr32::TFloatRect &ARect, const System::WideString Text, unsigned Flags = (unsigned)(0x0));
extern DELPHI_PACKAGE void __fastcall SetHinting(TTextHinting Value);
extern DELPHI_PACKAGE TTextHinting __fastcall GetHinting(void);
}	/* namespace Gr32_text_vcl */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_GR32_TEXT_VCL)
using namespace Gr32_text_vcl;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// Gr32_text_vclHPP
