﻿// CodeGear C++Builder
// Copyright (c) 1995, 2016 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'GR32_Transforms.pas' rev: 31.00 (Windows)

#ifndef Gr32_transformsHPP
#define Gr32_transformsHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>
#include <SysInit.hpp>
#include <Winapi.Windows.hpp>
#include <System.SysUtils.hpp>
#include <System.Classes.hpp>
#include <GR32.hpp>
#include <GR32_VectorMaps.hpp>
#include <GR32_Rasterizers.hpp>
#include <System.Types.hpp>

//-- user supplied -----------------------------------------------------------

namespace Gr32_transforms
{
//-- forward type declarations -----------------------------------------------
class DELPHICLASS ETransformError;
class DELPHICLASS ETransformNotImplemented;
class DELPHICLASS TTransformation;
class DELPHICLASS TNestedTransformation;
class DELPHICLASS T3x3Transformation;
class DELPHICLASS TAffineTransformation;
class DELPHICLASS TProjectiveTransformation;
class DELPHICLASS TTwirlTransformation;
class DELPHICLASS TBloatTransformation;
class DELPHICLASS TDisturbanceTransformation;
class DELPHICLASS TFishEyeTransformation;
class DELPHICLASS TPolarTransformation;
class DELPHICLASS TPathTransformation;
class DELPHICLASS TRemapTransformation;
//-- type declarations -------------------------------------------------------
#pragma pack(push,4)
class PASCALIMPLEMENTATION ETransformError : public System::Sysutils::Exception
{
	typedef System::Sysutils::Exception inherited;
	
public:
	/* Exception.Create */ inline __fastcall ETransformError(const System::UnicodeString Msg) : System::Sysutils::Exception(Msg) { }
	/* Exception.CreateFmt */ inline __fastcall ETransformError(const System::UnicodeString Msg, const System::TVarRec *Args, const int Args_High) : System::Sysutils::Exception(Msg, Args, Args_High) { }
	/* Exception.CreateRes */ inline __fastcall ETransformError(NativeUInt Ident)/* overload */ : System::Sysutils::Exception(Ident) { }
	/* Exception.CreateRes */ inline __fastcall ETransformError(System::PResStringRec ResStringRec)/* overload */ : System::Sysutils::Exception(ResStringRec) { }
	/* Exception.CreateResFmt */ inline __fastcall ETransformError(NativeUInt Ident, const System::TVarRec *Args, const int Args_High)/* overload */ : System::Sysutils::Exception(Ident, Args, Args_High) { }
	/* Exception.CreateResFmt */ inline __fastcall ETransformError(System::PResStringRec ResStringRec, const System::TVarRec *Args, const int Args_High)/* overload */ : System::Sysutils::Exception(ResStringRec, Args, Args_High) { }
	/* Exception.CreateHelp */ inline __fastcall ETransformError(const System::UnicodeString Msg, int AHelpContext) : System::Sysutils::Exception(Msg, AHelpContext) { }
	/* Exception.CreateFmtHelp */ inline __fastcall ETransformError(const System::UnicodeString Msg, const System::TVarRec *Args, const int Args_High, int AHelpContext) : System::Sysutils::Exception(Msg, Args, Args_High, AHelpContext) { }
	/* Exception.CreateResHelp */ inline __fastcall ETransformError(NativeUInt Ident, int AHelpContext)/* overload */ : System::Sysutils::Exception(Ident, AHelpContext) { }
	/* Exception.CreateResHelp */ inline __fastcall ETransformError(System::PResStringRec ResStringRec, int AHelpContext)/* overload */ : System::Sysutils::Exception(ResStringRec, AHelpContext) { }
	/* Exception.CreateResFmtHelp */ inline __fastcall ETransformError(System::PResStringRec ResStringRec, const System::TVarRec *Args, const int Args_High, int AHelpContext)/* overload */ : System::Sysutils::Exception(ResStringRec, Args, Args_High, AHelpContext) { }
	/* Exception.CreateResFmtHelp */ inline __fastcall ETransformError(NativeUInt Ident, const System::TVarRec *Args, const int Args_High, int AHelpContext)/* overload */ : System::Sysutils::Exception(Ident, Args, Args_High, AHelpContext) { }
	/* Exception.Destroy */ inline __fastcall virtual ~ETransformError(void) { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION ETransformNotImplemented : public System::Sysutils::Exception
{
	typedef System::Sysutils::Exception inherited;
	
public:
	/* Exception.Create */ inline __fastcall ETransformNotImplemented(const System::UnicodeString Msg) : System::Sysutils::Exception(Msg) { }
	/* Exception.CreateFmt */ inline __fastcall ETransformNotImplemented(const System::UnicodeString Msg, const System::TVarRec *Args, const int Args_High) : System::Sysutils::Exception(Msg, Args, Args_High) { }
	/* Exception.CreateRes */ inline __fastcall ETransformNotImplemented(NativeUInt Ident)/* overload */ : System::Sysutils::Exception(Ident) { }
	/* Exception.CreateRes */ inline __fastcall ETransformNotImplemented(System::PResStringRec ResStringRec)/* overload */ : System::Sysutils::Exception(ResStringRec) { }
	/* Exception.CreateResFmt */ inline __fastcall ETransformNotImplemented(NativeUInt Ident, const System::TVarRec *Args, const int Args_High)/* overload */ : System::Sysutils::Exception(Ident, Args, Args_High) { }
	/* Exception.CreateResFmt */ inline __fastcall ETransformNotImplemented(System::PResStringRec ResStringRec, const System::TVarRec *Args, const int Args_High)/* overload */ : System::Sysutils::Exception(ResStringRec, Args, Args_High) { }
	/* Exception.CreateHelp */ inline __fastcall ETransformNotImplemented(const System::UnicodeString Msg, int AHelpContext) : System::Sysutils::Exception(Msg, AHelpContext) { }
	/* Exception.CreateFmtHelp */ inline __fastcall ETransformNotImplemented(const System::UnicodeString Msg, const System::TVarRec *Args, const int Args_High, int AHelpContext) : System::Sysutils::Exception(Msg, Args, Args_High, AHelpContext) { }
	/* Exception.CreateResHelp */ inline __fastcall ETransformNotImplemented(NativeUInt Ident, int AHelpContext)/* overload */ : System::Sysutils::Exception(Ident, AHelpContext) { }
	/* Exception.CreateResHelp */ inline __fastcall ETransformNotImplemented(System::PResStringRec ResStringRec, int AHelpContext)/* overload */ : System::Sysutils::Exception(ResStringRec, AHelpContext) { }
	/* Exception.CreateResFmtHelp */ inline __fastcall ETransformNotImplemented(System::PResStringRec ResStringRec, const System::TVarRec *Args, const int Args_High, int AHelpContext)/* overload */ : System::Sysutils::Exception(ResStringRec, Args, Args_High, AHelpContext) { }
	/* Exception.CreateResFmtHelp */ inline __fastcall ETransformNotImplemented(NativeUInt Ident, const System::TVarRec *Args, const int Args_High, int AHelpContext)/* overload */ : System::Sysutils::Exception(Ident, Args, Args_High, AHelpContext) { }
	/* Exception.Destroy */ inline __fastcall virtual ~ETransformNotImplemented(void) { }
	
};

#pragma pack(pop)

typedef System::StaticArray<System::StaticArray<float, 3>, 3> TFloatMatrix;

typedef System::StaticArray<System::StaticArray<Gr32::TFixed, 3>, 3> TFixedMatrix;

typedef System::StaticArray<float, 3> TVector3f;

typedef System::StaticArray<int, 3> TVector3i;

class PASCALIMPLEMENTATION TTransformation : public Gr32::TNotifiablePersistent
{
	typedef Gr32::TNotifiablePersistent inherited;
	
private:
	Gr32::TFloatRect FSrcRect;
	void __fastcall SetSrcRect(const Gr32::TFloatRect &Value);
	
protected:
	bool TransformValid;
	virtual void __fastcall PrepareTransform(void);
	virtual void __fastcall ReverseTransformInt(int DstX, int DstY, /* out */ int &SrcX, /* out */ int &SrcY);
	virtual void __fastcall ReverseTransformFixed(Gr32::TFixed DstX, Gr32::TFixed DstY, /* out */ Gr32::TFixed &SrcX, /* out */ Gr32::TFixed &SrcY);
	virtual void __fastcall ReverseTransformFloat(float DstX, float DstY, /* out */ float &SrcX, /* out */ float &SrcY);
	virtual void __fastcall TransformInt(int SrcX, int SrcY, /* out */ int &DstX, /* out */ int &DstY);
	virtual void __fastcall TransformFixed(Gr32::TFixed SrcX, Gr32::TFixed SrcY, /* out */ Gr32::TFixed &DstX, /* out */ Gr32::TFixed &DstY);
	virtual void __fastcall TransformFloat(float SrcX, float SrcY, /* out */ float &DstX, /* out */ float &DstY);
	
public:
	virtual void __fastcall Changed(void);
	virtual bool __fastcall HasTransformedBounds(void);
	Gr32::TFloatRect __fastcall GetTransformedBounds(void)/* overload */;
	virtual Gr32::TFloatRect __fastcall GetTransformedBounds(const Gr32::TFloatRect &ASrcRect)/* overload */;
	virtual System::Types::TPoint __fastcall ReverseTransform(const System::Types::TPoint &P)/* overload */;
	virtual Gr32::TFixedPoint __fastcall ReverseTransform(const Gr32::TFixedPoint &P)/* overload */;
	virtual Gr32::TFloatPoint __fastcall ReverseTransform(const Gr32::TFloatPoint &P)/* overload */;
	virtual System::Types::TPoint __fastcall Transform(const System::Types::TPoint &P)/* overload */;
	virtual Gr32::TFixedPoint __fastcall Transform(const Gr32::TFixedPoint &P)/* overload */;
	virtual Gr32::TFloatPoint __fastcall Transform(const Gr32::TFloatPoint &P)/* overload */;
	__property Gr32::TFloatRect SrcRect = {read=FSrcRect, write=SetSrcRect};
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~TTransformation(void) { }
	
public:
	/* TObject.Create */ inline __fastcall TTransformation(void) : Gr32::TNotifiablePersistent() { }
	
};


typedef System::TMetaClass* TTransformationClass;

class PASCALIMPLEMENTATION TNestedTransformation : public TTransformation
{
	typedef TTransformation inherited;
	
public:
	TTransformation* operator[](int Index) { return this->Items[Index]; }
	
private:
	System::Classes::TList* FItems;
	System::Classes::TPersistent* FOwner;
	int __fastcall GetCount(void);
	TTransformation* __fastcall GetItem(int Index);
	void __fastcall SetItem(int Index, TTransformation* const Value);
	
protected:
	virtual void __fastcall PrepareTransform(void);
	virtual void __fastcall ReverseTransformFixed(Gr32::TFixed DstX, Gr32::TFixed DstY, /* out */ Gr32::TFixed &SrcX, /* out */ Gr32::TFixed &SrcY);
	virtual void __fastcall ReverseTransformFloat(float DstX, float DstY, /* out */ float &SrcX, /* out */ float &SrcY);
	virtual void __fastcall TransformFixed(Gr32::TFixed SrcX, Gr32::TFixed SrcY, /* out */ Gr32::TFixed &DstX, /* out */ Gr32::TFixed &DstY);
	virtual void __fastcall TransformFloat(float SrcX, float SrcY, /* out */ float &DstX, /* out */ float &DstY);
	
public:
	__fastcall TNestedTransformation(void);
	__fastcall virtual ~TNestedTransformation(void);
	TTransformation* __fastcall Add(TTransformationClass ItemClass);
	void __fastcall Clear(void);
	void __fastcall Delete(int Index);
	TTransformation* __fastcall Insert(int Index, TTransformationClass ItemClass);
	__property System::Classes::TPersistent* Owner = {read=FOwner};
	__property int Count = {read=GetCount, nodefault};
	__property TTransformation* Items[int Index] = {read=GetItem, write=SetItem/*, default*/};
};


class PASCALIMPLEMENTATION T3x3Transformation : public TTransformation
{
	typedef TTransformation inherited;
	
protected:
	TFloatMatrix FMatrix;
	TFloatMatrix FInverseMatrix;
	TFixedMatrix FFixedMatrix;
	TFixedMatrix FInverseFixedMatrix;
	virtual void __fastcall PrepareTransform(void);
	virtual void __fastcall ReverseTransformFixed(Gr32::TFixed DstX, Gr32::TFixed DstY, /* out */ Gr32::TFixed &SrcX, /* out */ Gr32::TFixed &SrcY);
	virtual void __fastcall ReverseTransformFloat(float DstX, float DstY, /* out */ float &SrcX, /* out */ float &SrcY);
	virtual void __fastcall TransformFixed(Gr32::TFixed SrcX, Gr32::TFixed SrcY, /* out */ Gr32::TFixed &DstX, /* out */ Gr32::TFixed &DstY);
	virtual void __fastcall TransformFloat(float SrcX, float SrcY, /* out */ float &DstX, /* out */ float &DstY);
	
public:
	__property TFloatMatrix Matrix = {read=FMatrix};
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~T3x3Transformation(void) { }
	
public:
	/* TObject.Create */ inline __fastcall T3x3Transformation(void) : TTransformation() { }
	
};


class PASCALIMPLEMENTATION TAffineTransformation : public T3x3Transformation
{
	typedef T3x3Transformation inherited;
	
private:
	TFloatMatrix *FStack;
	int FStackLevel;
	
public:
	__fastcall virtual TAffineTransformation(void);
	virtual Gr32::TFloatRect __fastcall GetTransformedBounds(const Gr32::TFloatRect &ASrcRect)/* overload */;
	void __fastcall Push(void);
	void __fastcall Pop(void);
	void __fastcall Clear(void)/* overload */;
	void __fastcall Clear(const TFloatMatrix &BaseMatrix)/* overload */;
	void __fastcall Rotate(float Alpha)/* overload */;
	void __fastcall Rotate(float Cx, float Cy, float Alpha)/* overload */;
	void __fastcall Skew(float Fx, float Fy);
	void __fastcall Scale(float Sx, float Sy)/* overload */;
	void __fastcall Scale(float Value)/* overload */;
	void __fastcall Translate(float Dx, float Dy);
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~TAffineTransformation(void) { }
	
	/* Hoisted overloads: */
	
public:
	inline Gr32::TFloatRect __fastcall  GetTransformedBounds(void){ return TTransformation::GetTransformedBounds(); }
	
};


class PASCALIMPLEMENTATION TProjectiveTransformation : public T3x3Transformation
{
	typedef T3x3Transformation inherited;
	
private:
	System::StaticArray<float, 4> FQuadX;
	System::StaticArray<float, 4> FQuadY;
	void __fastcall SetX0(float Value);
	void __fastcall SetX1(float Value);
	void __fastcall SetX2(float Value);
	void __fastcall SetX3(float Value);
	void __fastcall SetY0(float Value);
	void __fastcall SetY1(float Value);
	void __fastcall SetY2(float Value);
	void __fastcall SetY3(float Value);
	
protected:
	virtual void __fastcall PrepareTransform(void);
	virtual void __fastcall ReverseTransformFixed(Gr32::TFixed DstX, Gr32::TFixed DstY, /* out */ Gr32::TFixed &SrcX, /* out */ Gr32::TFixed &SrcY);
	virtual void __fastcall ReverseTransformFloat(float DstX, float DstY, /* out */ float &SrcX, /* out */ float &SrcY);
	virtual void __fastcall TransformFixed(Gr32::TFixed SrcX, Gr32::TFixed SrcY, /* out */ Gr32::TFixed &DstX, /* out */ Gr32::TFixed &DstY);
	virtual void __fastcall TransformFloat(float SrcX, float SrcY, /* out */ float &DstX, /* out */ float &DstY);
	
public:
	virtual Gr32::TFloatRect __fastcall GetTransformedBounds(const Gr32::TFloatRect &ASrcRect)/* overload */;
	
__published:
	// [SKIPPED] __property float X0 = {read=FQuadX[0], write=SetX0};
	// [SKIPPED] __property float X1 = {read=FQuadX[1], write=SetX1};
	// [SKIPPED] __property float X2 = {read=FQuadX[2], write=SetX2};
	// [SKIPPED] __property float X3 = {read=FQuadX[3], write=SetX3};
	// [SKIPPED] __property float Y0 = {read=FQuadY[0], write=SetY0};
	// [SKIPPED] __property float Y1 = {read=FQuadY[1], write=SetY1};
	// [SKIPPED] __property float Y2 = {read=FQuadY[2], write=SetY2};
	// [SKIPPED] __property float Y3 = {read=FQuadY[3], write=SetY3};
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~TProjectiveTransformation(void) { }
	
public:
	/* TObject.Create */ inline __fastcall TProjectiveTransformation(void) : T3x3Transformation() { }
	
	/* Hoisted overloads: */
	
public:
	inline Gr32::TFloatRect __fastcall  GetTransformedBounds(void){ return TTransformation::GetTransformedBounds(); }
	
};


class PASCALIMPLEMENTATION TTwirlTransformation : public TTransformation
{
	typedef TTransformation inherited;
	
private:
	float Frx;
	float Fry;
	float FTwirl;
	void __fastcall SetTwirl(const float Value);
	
protected:
	virtual void __fastcall PrepareTransform(void);
	virtual void __fastcall ReverseTransformFloat(float DstX, float DstY, /* out */ float &SrcX, /* out */ float &SrcY);
	
public:
	__fastcall virtual TTwirlTransformation(void);
	virtual Gr32::TFloatRect __fastcall GetTransformedBounds(const Gr32::TFloatRect &ASrcRect)/* overload */;
	
__published:
	__property float Twirl = {read=FTwirl, write=SetTwirl};
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~TTwirlTransformation(void) { }
	
	/* Hoisted overloads: */
	
public:
	inline Gr32::TFloatRect __fastcall  GetTransformedBounds(void){ return TTransformation::GetTransformedBounds(); }
	
};


class PASCALIMPLEMENTATION TBloatTransformation : public TTransformation
{
	typedef TTransformation inherited;
	
private:
	float FBloatPower;
	float FBP;
	float FPiW;
	float FPiH;
	void __fastcall SetBloatPower(const float Value);
	
protected:
	virtual void __fastcall PrepareTransform(void);
	virtual void __fastcall ReverseTransformFloat(float DstX, float DstY, /* out */ float &SrcX, /* out */ float &SrcY);
	virtual void __fastcall TransformFloat(float DstX, float DstY, /* out */ float &SrcX, /* out */ float &SrcY);
	
public:
	__fastcall virtual TBloatTransformation(void);
	
__published:
	__property float BloatPower = {read=FBloatPower, write=SetBloatPower};
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~TBloatTransformation(void) { }
	
};


class PASCALIMPLEMENTATION TDisturbanceTransformation : public TTransformation
{
	typedef TTransformation inherited;
	
private:
	float FDisturbance;
	void __fastcall SetDisturbance(const float Value);
	
protected:
	virtual void __fastcall ReverseTransformFloat(float DstX, float DstY, /* out */ float &SrcX, /* out */ float &SrcY);
	
public:
	virtual Gr32::TFloatRect __fastcall GetTransformedBounds(const Gr32::TFloatRect &ASrcRect)/* overload */;
	
__published:
	__property float Disturbance = {read=FDisturbance, write=SetDisturbance};
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~TDisturbanceTransformation(void) { }
	
public:
	/* TObject.Create */ inline __fastcall TDisturbanceTransformation(void) : TTransformation() { }
	
	/* Hoisted overloads: */
	
public:
	inline Gr32::TFloatRect __fastcall  GetTransformedBounds(void){ return TTransformation::GetTransformedBounds(); }
	
};


class PASCALIMPLEMENTATION TFishEyeTransformation : public TTransformation
{
	typedef TTransformation inherited;
	
private:
	float Frx;
	float Fry;
	float Faw;
	float Fsr;
	float Sx;
	float Sy;
	float FMinR;
	
protected:
	virtual void __fastcall PrepareTransform(void);
	virtual void __fastcall ReverseTransformFloat(float DstX, float DstY, /* out */ float &SrcX, /* out */ float &SrcY);
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~TFishEyeTransformation(void) { }
	
public:
	/* TObject.Create */ inline __fastcall TFishEyeTransformation(void) : TTransformation() { }
	
};


class PASCALIMPLEMENTATION TPolarTransformation : public TTransformation
{
	typedef TTransformation inherited;
	
private:
	Gr32::TFloatRect FDstRect;
	float FPhase;
	float Sx;
	float Sy;
	float Cx;
	float Cy;
	float Dx;
	float Dy;
	float Rt;
	float Rt2;
	float Rr;
	float Rcx;
	float Rcy;
	void __fastcall SetDstRect(const Gr32::TFloatRect &Value);
	void __fastcall SetPhase(const float Value);
	
protected:
	virtual void __fastcall PrepareTransform(void);
	virtual void __fastcall TransformFloat(float SrcX, float SrcY, /* out */ float &DstX, /* out */ float &DstY);
	virtual void __fastcall ReverseTransformFloat(float DstX, float DstY, /* out */ float &SrcX, /* out */ float &SrcY);
	
public:
	__property Gr32::TFloatRect DstRect = {read=FDstRect, write=SetDstRect};
	__property float Phase = {read=FPhase, write=SetPhase};
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~TPolarTransformation(void) { }
	
public:
	/* TObject.Create */ inline __fastcall TPolarTransformation(void) : TTransformation() { }
	
};


class PASCALIMPLEMENTATION TPathTransformation : public TTransformation
{
	typedef TTransformation inherited;
	
	
private:
	struct DECLSPEC_DRECORD _TPathTransformation__1
	{
	public:
		float Dist;
		float RecDist;
	};
	
	
	typedef System::DynamicArray<_TPathTransformation__1> _TPathTransformation__2;
	
	
private:
	float FTopLength;
	float FBottomLength;
	Gr32::TArrayOfFloatPoint FBottomCurve;
	Gr32::TArrayOfFloatPoint FTopCurve;
	_TPathTransformation__2 FTopHypot;
	_TPathTransformation__2 FBottomHypot;
	void __fastcall SetBottomCurve(const Gr32::TArrayOfFloatPoint Value);
	void __fastcall SetTopCurve(const Gr32::TArrayOfFloatPoint Value);
	
protected:
	float rdx;
	float rdy;
	virtual void __fastcall PrepareTransform(void);
	virtual void __fastcall TransformFloat(float SrcX, float SrcY, /* out */ float &DstX, /* out */ float &DstY);
	
public:
	__fastcall virtual ~TPathTransformation(void);
	__property Gr32::TArrayOfFloatPoint TopCurve = {read=FTopCurve, write=SetTopCurve};
	__property Gr32::TArrayOfFloatPoint BottomCurve = {read=FBottomCurve, write=SetBottomCurve};
public:
	/* TObject.Create */ inline __fastcall TPathTransformation(void) : TTransformation() { }
	
};


class PASCALIMPLEMENTATION TRemapTransformation : public TTransformation
{
	typedef TTransformation inherited;
	
private:
	Gr32_vectormaps::TVectorMap* FVectorMap;
	Gr32::TFixedPoint FScalingFixed;
	Gr32::TFloatPoint FScalingFloat;
	Gr32::TFixedPoint FCombinedScalingFixed;
	Gr32::TFloatPoint FCombinedScalingFloat;
	Gr32::TFixedPoint FSrcTranslationFixed;
	Gr32::TFixedPoint FSrcScaleFixed;
	Gr32::TFixedPoint FDstTranslationFixed;
	Gr32::TFixedPoint FDstScaleFixed;
	Gr32::TFloatPoint FSrcTranslationFloat;
	Gr32::TFloatPoint FSrcScaleFloat;
	Gr32::TFloatPoint FDstTranslationFloat;
	Gr32::TFloatPoint FDstScaleFloat;
	Gr32::TFixedPoint FOffsetFixed;
	System::Types::TPoint FOffsetInt;
	Gr32::TFloatRect FMappingRect;
	Gr32::TFloatPoint FOffset;
	void __fastcall SetMappingRect(const Gr32::TFloatRect &Rect);
	void __fastcall SetOffset(const Gr32::TFloatPoint &Value);
	
protected:
	virtual void __fastcall PrepareTransform(void);
	virtual void __fastcall ReverseTransformInt(int DstX, int DstY, /* out */ int &SrcX, /* out */ int &SrcY);
	virtual void __fastcall ReverseTransformFloat(float DstX, float DstY, /* out */ float &SrcX, /* out */ float &SrcY);
	virtual void __fastcall ReverseTransformFixed(Gr32::TFixed DstX, Gr32::TFixed DstY, /* out */ Gr32::TFixed &SrcX, /* out */ Gr32::TFixed &SrcY);
	
public:
	__fastcall virtual TRemapTransformation(void);
	__fastcall virtual ~TRemapTransformation(void);
	virtual bool __fastcall HasTransformedBounds(void);
	virtual Gr32::TFloatRect __fastcall GetTransformedBounds(const Gr32::TFloatRect &ASrcRect)/* overload */;
	void __fastcall Scale(float Sx, float Sy);
	__property Gr32::TFloatRect MappingRect = {read=FMappingRect, write=SetMappingRect};
	__property Gr32::TFloatPoint Offset = {read=FOffset, write=SetOffset};
	__property Gr32_vectormaps::TVectorMap* VectorMap = {read=FVectorMap, write=FVectorMap};
	/* Hoisted overloads: */
	
public:
	inline Gr32::TFloatRect __fastcall  GetTransformedBounds(void){ return TTransformation::GetTransformedBounds(); }
	
};


//-- var, const, procedure ---------------------------------------------------
extern DELPHI_PACKAGE TFloatMatrix IdentityMatrix;
extern DELPHI_PACKAGE bool FullEdge;
extern DELPHI_PACKAGE System::ResourceString _RCStrReverseTransformationNotImplemented;
#define Gr32_transforms_RCStrReverseTransformationNotImplemented System::LoadResourceString(&Gr32_transforms::_RCStrReverseTransformationNotImplemented)
extern DELPHI_PACKAGE System::ResourceString _RCStrForwardTransformationNotImplemented;
#define Gr32_transforms_RCStrForwardTransformationNotImplemented System::LoadResourceString(&Gr32_transforms::_RCStrForwardTransformationNotImplemented)
extern DELPHI_PACKAGE System::ResourceString _RCStrTopBottomCurveNil;
#define Gr32_transforms_RCStrTopBottomCurveNil System::LoadResourceString(&Gr32_transforms::_RCStrTopBottomCurveNil)
extern DELPHI_PACKAGE void __fastcall Adjoint(TFloatMatrix &M);
extern DELPHI_PACKAGE float __fastcall Determinant(const TFloatMatrix &M);
extern DELPHI_PACKAGE void __fastcall Scale(TFloatMatrix &M, float Factor);
extern DELPHI_PACKAGE void __fastcall Invert(TFloatMatrix &M);
extern DELPHI_PACKAGE TFloatMatrix __fastcall Mult(const TFloatMatrix &M1, const TFloatMatrix &M2);
extern DELPHI_PACKAGE TVector3f __fastcall VectorTransform(const TFloatMatrix &M, const TVector3f &V);
extern DELPHI_PACKAGE Gr32::TArrayOfArrayOfFixedPoint __fastcall TransformPoints(Gr32::TArrayOfArrayOfFixedPoint Points, TTransformation* Transformation);
extern DELPHI_PACKAGE void __fastcall Transform(Gr32::TCustomBitmap32* Dst, Gr32::TCustomBitmap32* Src, TTransformation* Transformation)/* overload */;
extern DELPHI_PACKAGE void __fastcall Transform(Gr32::TCustomBitmap32* Dst, Gr32::TCustomBitmap32* Src, TTransformation* Transformation, const System::Types::TRect &DstClip)/* overload */;
extern DELPHI_PACKAGE void __fastcall Transform(Gr32::TCustomBitmap32* Dst, Gr32::TCustomBitmap32* Src, TTransformation* Transformation, Gr32_rasterizers::TRasterizer* Rasterizer)/* overload */;
extern DELPHI_PACKAGE void __fastcall Transform(Gr32::TCustomBitmap32* Dst, Gr32::TCustomBitmap32* Src, TTransformation* Transformation, Gr32_rasterizers::TRasterizer* Rasterizer, const System::Types::TRect &DstClip)/* overload */;
extern DELPHI_PACKAGE void __fastcall SetBorderTransparent(Gr32::TCustomBitmap32* ABitmap, const System::Types::TRect &ARect);
extern DELPHI_PACKAGE void __fastcall RasterizeTransformation(Gr32_vectormaps::TVectorMap* Vectormap, TTransformation* Transformation, const System::Types::TRect &DstRect, Gr32_vectormaps::TVectorCombineMode CombineMode = (Gr32_vectormaps::TVectorCombineMode)(0x0), Gr32_vectormaps::TVectorCombineEvent CombineCallback = 0x0);
extern DELPHI_PACKAGE TFixedMatrix __fastcall FixedMatrix(const TFloatMatrix &FloatMatrix)/* overload */;
extern DELPHI_PACKAGE TFloatMatrix __fastcall FloatMatrix(const TFixedMatrix &FixedMatrix)/* overload */;
}	/* namespace Gr32_transforms */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_GR32_TRANSFORMS)
using namespace Gr32_transforms;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// Gr32_transformsHPP
