//---------------------------------------------------------------------------

#ifndef MainH
#define MainH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include "GR32_Image.hpp"
#include <Vcl.ExtCtrls.hpp>
#include "G32_ProgressBar.hpp"
#include "GR32_Polygons_old.hpp"
#include "GR32_Polygons.hpp"
#include "GR32_Lines.hpp"
#include <ImageButton.h>
#include "MyThreads.h"
#include <Vcl.Imaging.pngimage.hpp>
#include <Vcl.Menus.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TImage32 *Image321;
	TG32_ProgressBar *G32_ProgressBar1;
	TTimer *Timer1;
	TImageButton *ImageButton1;
	TEdit *Edit1;
	TButton *Button1;
	void __fastcall Timer1Timer(TObject *Sender);
	void __fastcall ImageButton1Click(TObject *Sender);
	void __fastcall Button1Click(TObject *Sender);
private:	// User declarations
	TTimerThread *thrd;
	void __fastcall ProgressIt();
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
    __fastcall ~TForm1();
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
