//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "GR32_Image"
#pragma link "G32_ProgressBar"
#pragma link "ImageButton"
#pragma resource "*.dfm"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
	thrd=NULL;
}
//---------------------------------------------------------------------------

__fastcall TForm1::~TForm1()
{
    if(thrd)
	{
		thrd->OnTimer=NULL;
		thrd->AskForTerminate();
		if(thrd->ForceTerminate()) delete thrd;
	}
}

void __fastcall TForm1::ProgressIt()
{
	G32_ProgressBar1->Position++;
	if(G32_ProgressBar1->Position>=G32_ProgressBar1->Max)
	{
		G32_ProgressBar1->Position=G32_ProgressBar1->Min;
		ImageButton1->Enabled=true;
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Timer1Timer(TObject *Sender)
{
	ProgressIt();
}
//---------------------------------------------------------------------------

void DrawLine32(TBitmap32 *Bmp, TColor32 Color,
		TColor32 Border, int Width, bool InRect, bool Shadow, int x1, int y1, int x2, int y2);

void __fastcall TForm1::ImageButton1Click(TObject *Sender)
{
	TPolygon32 *poly;
	TArrayOfFixedPoint fxp;

	poly=new TPolygon32();
	poly->Clear();
	poly->Closed=true;
	poly->Add(FixedPoint(400, 45));
	poly->Add(FixedPoint(408, 65));
	poly->Add(FixedPoint(430, 68));
	poly->Add(FixedPoint(415, 82));
	poly->Add(FixedPoint(419, 104));
	poly->Add(FixedPoint(400, 93));

	fxp.Length=6;
	fxp[0]=FixedPoint(400, 45);
	fxp[1]=FixedPoint(408, 65);
	fxp[2]=FixedPoint(430, 68);
	fxp[3]=FixedPoint(415, 82);
	fxp[4]=FixedPoint(419, 104);
	fxp[5]=FixedPoint(400, 93);

	Image321->Bitmap->BeginUpdate();
	poly->DrawFill(Image321->Bitmap, Color32(255, 255, 255, 128), NULL);

//	Gr32_polygons::PolylineXS(Image321->Bitmap, fxp, Color32(0, 255, 0, 255));

//    ShowMessage(System::CompilerVersion);

	DrawLine32(Image321->Bitmap, clGreen32, Color32(0, 64, 0, 255), 20, true, true, 100, 200, 700, 200);

	Image321->Bitmap->EndUpdate();
	Image321->Bitmap->Changed();

	delete poly;

	if(thrd)
	{
		thrd->OnTimer=NULL;
		thrd->AskForTerminate();
		if(thrd->ForceTerminate()) delete thrd;
	}
	thrd=new TTimerThread(100);
	thrd->OnTimer=&ProgressIt;
	thrd->Start();
}
//---------------------------------------------------------------------------

void DrawLine32(TBitmap32 *Bmp, TColor32 Color, TColor32 Border, int Width,
	bool Shadow, bool InRect, int X1, int Y1, int X2, int Y2)
{
	TLine32 *Line32;
	TFixedPoint LineFP[2];
	TColor32Entry ce;
	TColor32 Colors[2];

	Line32=new TLine32();
	try
	{
		Line32->EndStyle=esRounded;
		Line32->JoinStyle=jsRounded;
		LineFP[0]=FixedPoint(X1, Y1);
		LineFP[1]=FixedPoint(X2, Y2);
		Line32->SetPoints(LineFP, 1);
		if(Shadow)
		{
			Line32->Translate(Width/2, Width/2);
			Line32->Draw(Bmp, Width, Color32(64, 64, 64, 128));
		}
		Line32->SetPoints(LineFP, 1);
		if(InRect)
		{
			ce.ARGB=Color;
			if(ce.R>127) ce.R=255;
			else ce.R+=127;
			if(ce.G>127) ce.G=255;
			else ce.G+=127;
			if(ce.B>127) ce.B=255;
			else ce.B+=127;
			Colors[1]=ce.ARGB;
			Colors[0]=Color;
		}
		else
		{
			ce.ARGB=Color;
			ce.A=(Byte)((float)ce.A*0.35);
			Colors[1]=ce.ARGB;
			ce.R=(Byte)((float)ce.R*0.5);
			ce.G=(Byte)((float)ce.G*0.5);
			ce.B=(Byte)((float)ce.B*0.5);
			Colors[0]=ce.ARGB;

			ce.ARGB=Border;
			ce.A=(Byte)((float)ce.A*0.35);
			Border=ce.ARGB;
		}
		Line32->DrawGradient(Bmp, Width, Colors, 2, 90, Border);
	}
	__finally
	{
		delete Line32;
	}
}

void __fastcall TForm1::Button1Click(TObject *Sender)
{
	AnsiString str="";
	char Buf[32000];

	for(int i=0; i<100000; i++)
		str+=Edit1->Text.rs_str(Buf, 32000);

	//ShowMessage(str);
}
//---------------------------------------------------------------------------

