//---------------------------------------------------------------------------

#include <vcl.h>

#pragma hdrstop

#include "ImageButton.h"
#pragma package(smart_init)
//---------------------------------------------------------------------------
// ValidCtrCheck is used to assure that the components created do not have
// any pure virtual functions.
//

static inline void ValidCtrCheck(TImageButton *)
{
	new TImageButton(NULL);
}
//---------------------------------------------------------------------------

__fastcall TImageButton::TImageButton(TComponent* Owner)
	: TImage(Owner)
{
	tmp=Picture;
	FHotPicture=new Graphics::TPicture();//NULL;
	FDisabledPicture=new Graphics::TPicture();//NULL;
	FPressedPicture=new Graphics::TPicture();//NULL;
	FNormalPicture=new Graphics::TPicture();
	groupindex=0;
	togglebutton=false;
	AutoSize=true;
	Transparent=true;
	FState=ibsNormal;
	FDown=false;
	FShowCaption=false;
	FCaption="";
	FVerticalAligment=ibvCenter;
	FHorizontalAligment=ibhCenter;
	Width=32;
	Height=32;
	OnMouseDown=OnMouseLButtonDown;
	OnMouseUp=OnMouseLButtonUp;
	Refresh();
}
//---------------------------------------------------------------------------
__fastcall TImageButton::~TImageButton()
{
	Picture=tmp;
	delete FHotPicture;
	delete FDisabledPicture;
	delete FPressedPicture;
	delete FNormalPicture;
}

void __fastcall TImageButton::SetDown(bool value)
{
	POINT p;

	if(FDown!=value)
	{
		FDown=value;
		if(Enabled)
		{
			if(FDown) FState=ibsPressed;
			else
			{
				GetCursorPos(&p);
				try
				{
					if(Parent && p.x>=ClientOrigin.x && p.x<(ClientOrigin.x+Width) &&
						p.y>=ClientOrigin.y && p.y<(ClientOrigin.y+Height)) FState=ibsHot;
					else FState=ibsNormal;
				}
				catch(...)
				{
					FState=ibsNormal;
				}
			}
			Refresh();
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TImageButton::SetEnabled(bool Value)
{
	POINT p;

	if(TImage::Enabled!=Value)
	{
		TImage::SetEnabled(Value);
		if(!Value) FState=ibsDisabled;
		else if(FDown) FState=ibsPressed;
		else
		{
			GetCursorPos(&p);
			try
			{
				if(Parent && p.x>=ClientOrigin.x && p.x<(ClientOrigin.x+Width) &&
					p.y>=ClientOrigin.y && p.y<(ClientOrigin.y+Height)) FState=ibsHot;
				else FState=ibsNormal;
			}
			catch(...)
			{
				FState=ibsNormal;
			}
		}
		Refresh();
	}
}
//---------------------------------------------------------------------------

void __fastcall TImageButton::SetState(TImageButtonState value)
{
	//if(FState!=value)
	//{
		FState=value;
		if(FState==ibsPressed) FDown=true;
		else if(FState==ibsNormal) FDown=false;
		if(FState!=ibsDisabled && !Enabled) Enabled=true;
		Refresh();
	//}
}
//---------------------------------------------------------------------------

void __fastcall TImageButton::OnMouseEnter(TMessage& Message)
{
	if(Enabled) State=ibsHot;
}
//---------------------------------------------------------------------------

void __fastcall TImageButton::OnMouseLeave(TMessage& Message)
{
	if(Enabled)
	{
		if(FDown) State=ibsPressed;
		else State=ibsNormal;
	}
}
//---------------------------------------------------------------------------

//void __fastcall TImageButton::OnMouseLButtonDown(TMessage& Message)
void __fastcall TImageButton::OnMouseLButtonDown(System::TObject* Sender, TMouseButton Button, Classes::TShiftState Shift, int X, int Y)
{
	if(Enabled)
	{
		FState=ibsPressed;
		Refresh();
/*		if(OnMouseDown)
		{
			POINT p;

			GetCursorPos(&p);
			//(OnMouseDown)(this, mbLeft, TShiftState() << ssLeft, p.x-ClientOrigin.x, p.y-ClientOrigin.y);
		}*/
	}
}
//---------------------------------------------------------------------------

//void __fastcall TImageButton::OnMouseLButtonUp(TMessage& Message)
void __fastcall TImageButton::OnMouseLButtonUp(System::TObject* Sender, TMouseButton Button, Classes::TShiftState Shift, int X, int Y)
{
	if(Enabled)
	{
		if(FDown) FState=ibsPressed;
		else FState=ibsHot;
		Refresh();
		/*if(OnMouseUp)
		{
			POINT p;

			GetCursorPos(&p);
			//(OnMouseUp)(this, mbLeft, TShiftState() << ssLeft, p.x-ClientOrigin.x, p.y-ClientOrigin.y);
		}
		if(OnClick) (OnClick)(this); */
	}
}
//---------------------------------------------------------------------------

void __fastcall TImageButton::Refresh()
{
	TPicture *g=NULL;

	if(Picture)
	{
		switch(FState)
		{
			case ibsNormal:
				if(FNormalPicture->Graphic && !FNormalPicture->Graphic->Empty) g=FNormalPicture;
				else g=tmp;
				break;
			case ibsHot:
				if(FHotPicture->Graphic && !FHotPicture->Graphic->Empty) g=FHotPicture;
				else if(!togglebutton)
				{
					if(FNormalPicture->Graphic && !FNormalPicture->Graphic->Empty) g=FNormalPicture;
					else g=tmp;
				}
				else g=tmp;
				break;
			case ibsPressed:
				if(FPressedPicture->Graphic && !FPressedPicture->Graphic->Empty) g=FPressedPicture;
				else if(!togglebutton)
				{
					if(FHotPicture->Graphic && !FHotPicture->Graphic->Empty) g=FHotPicture;
					else if(FNormalPicture->Graphic && !FNormalPicture->Graphic->Empty) g=FNormalPicture;
					else g=tmp;
				}
				else g=tmp;
				break;
			case ibsDisabled:
				if(FDisabledPicture->Graphic && !FDisabledPicture->Graphic->Empty) g=FDisabledPicture;
				else if(!togglebutton)
				{
					if(FNormalPicture->Graphic && !FNormalPicture->Graphic->Empty) g=FNormalPicture;
					else g=tmp;
				}
				else g=tmp;
				break;
		}
		if(g && g->Graphic && !g->Graphic->Empty)
		{
			//Canvas->Draw(0, 0, g);
			Picture->Assign(g);
		}
		/*if(FShowCaption && FCaption!=NULL && Picture->Bitmap && Picture->Bitmap->Canvas)
		{
			TFont *fnt=new TFont();
			int tw, th, x, y;

			try
			{
				try
				{
					fnt->Assign(Picture->Bitmap->Canvas->Font);
					Picture->Bitmap->Canvas->Font->Assign(Font);
					tw=Picture->Bitmap->Canvas->TextWidth(FCaption);
					th=Picture->Bitmap->Canvas->TextHeight(FCaption);
					switch(FHorizontalAligment)
					{
						case ibvLeft: x=FHorizontalMargin; break;
						case ibvRight: x=Width-FHorizontalMargin-tw; break;
						case ibvCenter: x=(Width-tw)>>1; break;
					}
					switch(FVerticalAligment)
					{
						case ibvLeft: y=FVerticalMargin; break;
						case ibvRight: y=Height-FVerticalMargin-th; break;
						case ibvCenter: y=(Height-th)>>1; break;
					}
					//Picture->Bitmap->Canvas->TextOutA(x, y, FCaption);
					Picture->Bitmap->Canvas->Rectangle(x, y, x+tw, y+th);
					Picture->Bitmap->Canvas->Font->Assign(fnt);
				}
				catch (...) {}
			}
			__finally
			{
				delete fnt;
			}
		}*/
	}
}
//---------------------------------------------------------------------------

bool __fastcall TImageButton::GroupToggle()
{
	if(Enabled && Owner)
	{
		int z = Owner->ComponentCount;
		String classname = ClassName();

		for(int i = 0; i < z; i++)
		{
			if(Owner->Components[i] && Owner->Components[i]->ClassName() == classname &&
				((TImageButton*)(Owner->Components[i]))->GroupIndex == groupindex &&
				Owner->Components[i] != this)
					((TImageButton*)(Owner->Components[i]))->Down = false;
		}
		Down = true;
	}
	return Down;
}
//---------------------------------------------------------------------------

namespace Imagebutton
{
	void __fastcall PACKAGE Register()
	{
		TComponentClass classes[1] = {__classid(TImageButton)};
		RegisterComponents(L"Additional", classes, 0);
	}
}
//---------------------------------------------------------------------------
