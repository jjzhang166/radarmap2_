//---------------------------------------------------------------------------

#ifndef ImageButtonH
#define ImageButtonH
//---------------------------------------------------------------------------
#include <SysUtils.hpp>
#include <Classes.hpp>
#include <Controls.hpp>
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------

#pragma warning( pop )

enum TImageButtonState {ibsNormal, ibsHot, ibsPressed, ibsDisabled};
enum TImageButtonVerticalAligment {ibvLeft, ibvCenter, ibvRight};
enum TImageButtonHorizontalAligment {ibhTop, ibhCenter, ibhBottom};

//disable warning W8118 (iip) Inline member function in Package class, function body in the header file, in other words...
//Full warnings list: http://docwiki.embarcadero.com/RADStudio/Seattle/en/Message_Options
#pragma warn -8118
class PACKAGE TImageButton : public TImage
{
private:
	Graphics::TPicture* tmp;
	Graphics::TPicture* FNormalPicture;
	Graphics::TPicture* FHotPicture;
	Graphics::TPicture* FDisabledPicture;
	Graphics::TPicture* FPressedPicture;
	bool togglebutton;
	int groupindex;
	TImageButtonState FState;
	bool FDown;
	bool FShowCaption;
	UnicodeString FCaption;
	TImageButtonVerticalAligment FVerticalAligment;
	TImageButtonHorizontalAligment FHorizontalAligment;
	int FVerticalMargin;
	int FHorizontalMargin;
	void __fastcall SetNormalPicture(Graphics::TPicture* value) {FNormalPicture->Assign(value); Refresh();}
	void __fastcall SetHotPicture(Graphics::TPicture* value) {FHotPicture->Assign(value); Refresh();}
	void __fastcall SetDisabledPicture(Graphics::TPicture* value) {FDisabledPicture->Assign(value); Refresh();}
	void __fastcall SetPressedPicture(Graphics::TPicture* value) {FPressedPicture->Assign(value); Refresh();}
	void __fastcall SetState(TImageButtonState value);
	void __fastcall SetDown(bool value);
	void __fastcall SetShowCaption(bool value) {if(FShowCaption!=value) {FShowCaption=value; if(FCaption!=NULL && FCaption!="") Refresh();}}
	void __fastcall SetCaption(UnicodeString value) {if(FCaption!=value) {FCaption=value; Refresh();}}
	void __fastcall SetVerticalAligment(TImageButtonVerticalAligment value) {if(FVerticalAligment!=value) {FVerticalAligment=value; if(FCaption!=NULL && FCaption!="") Refresh();}}
	void __fastcall SetHorizontalAligment(TImageButtonHorizontalAligment value) {if(FHorizontalAligment!=value) {FHorizontalAligment=value; if(FCaption!=NULL && FCaption!="") Refresh();}}
	void __fastcall SetVerticalMargin(int value) {if(FVerticalMargin!=value) {FVerticalMargin=value; if(FCaption!=NULL && FCaption!="") Refresh();}}
	void __fastcall SetHorizontalMargin(int value) {if(FHorizontalMargin!=value) {FHorizontalMargin=value; if(FCaption!=NULL && FCaption!="") Refresh();}}
	void __fastcall OnMouseEnter(TMessage& Message);
	void __fastcall OnMouseLeave(TMessage& Message);
	//void __fastcall OnMouseLButtonDown(TMessage& Message);
	//void __fastcall OnMouseLButtonUp(TMessage& Message);
	void __fastcall OnMouseLButtonDown(System::TObject* Sender, TMouseButton Button, Classes::TShiftState Shift, int X, int Y);
	void __fastcall OnMouseLButtonUp(System::TObject* Sender, TMouseButton Button, Classes::TShiftState Shift, int X, int Y);
//#pragma warn -8027  //W8027 Functions containing %s are not expanded inline
BEGIN_MESSAGE_MAP
	MESSAGE_HANDLER(CM_MOUSEENTER,TMessage,OnMouseEnter);
	MESSAGE_HANDLER(CM_MOUSELEAVE,TMessage,OnMouseLeave);
	//MESSAGE_HANDLER(WM_LBUTTONDOWN,TMessage,OnMouseLButtonDown);
	//MESSAGE_HANDLER(WM_LBUTTONUP,TMessage,OnMouseLButtonUp);
END_MESSAGE_MAP(TImage)
//#pragma warn +8027/**/
protected:
	void __fastcall SetEnabled(bool Value);
public:
	__fastcall TImageButton(TComponent* Owner);
	__fastcall ~TImageButton();
	void __fastcall Refresh();

	bool __fastcall Toggle() {if(Enabled) Down=!Down; return Down;}
	bool __fastcall GroupToggle();
__published:
	__property Graphics::TPicture* PictureNormal = {read=FNormalPicture, write=SetNormalPicture};
	__property Graphics::TPicture* PictureHot = {read=FHotPicture, write=SetHotPicture};
	__property Graphics::TPicture* PictureDisabled = {read=FDisabledPicture, write=SetDisabledPicture};
	__property Graphics::TPicture* PicturePressed = {read=FPressedPicture, write=SetPressedPicture};
	__property bool ToggleButton = {read=togglebutton, write=togglebutton};
	__property TImageButtonState State = {read=FState, write=SetState};
	__property bool Down = {read=FDown, write=SetDown};
	__property int GroupIndex = {read=groupindex, write=groupindex, default=0};
	/*__property bool ShowCaption = {read=FShowCaption, write=SetShowCaption};
	__property UnicodeString Caption = {read=FCaption, write=SetCaption};
	__property TImageButtonVerticalAligment CaptionVerticalAligment = {read=FVerticalAligment, write=SetVerticalAligment};
	__property TImageButtonHorizontalAligment CaptionHorizontalAligment = {read=FHorizontalAligment, write=SetHorizontalAligment};
	__property int CaptionVerticalMargin = {read=FVerticalMargin, write=SetVerticalMargin};
	__property int CaptionHorizontalMargin = {read=FHorizontalMargin, write=SetHorizontalMargin};
	__property Font;*/
};
#pragma warn +8118
//---------------------------------------------------------------------------
#endif
