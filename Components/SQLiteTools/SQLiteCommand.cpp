// -----------------------------------------------------------------------------
// SQLiteCommand.cpp
// -----------------------------------------------------------------------------
#pragma hdrstop

#include "SQLiteTools.h"
// #include "stdinc.h"

#pragma package(smart_init)

// -------- Constructor / Destructor -------------------------------------------
__fastcall SQLiteCommand::SQLiteCommand(SQLiteConnection *pConnect, const char *sql) {
	const char *tail = NULL;

	this->dbConnect = pConnect;
	this->refsCount = 0;

	if (sqlite3_prepare_v2(this->dbConnect->DataBase, sql, -1, &this->stmt, &tail) != SQLITE_OK) {
		throw SQLiteDbError(this->dbConnect);
	}

	this->columnCount = sqlite3_column_count(this->stmt);
	this->FillColumnInfo();
}

__fastcall SQLiteCommand::SQLiteCommand(SQLiteConnection *pConnect, const AnsiString &sql) {
	const char *tail = NULL;

	this->dbConnect = pConnect;
	this->refsCount = 0;

	if (sqlite3_prepare_v2(this->dbConnect->DataBase, sql.c_str(), sql.Length(), &this->stmt, &tail) != SQLITE_OK) {
		throw SQLiteDbError(this->dbConnect);
	}

	this->columnCount = sqlite3_column_count(this->stmt);
	this->FillColumnInfo();
}

__fastcall SQLiteCommand::~SQLiteCommand() {
	sqlite3_finalize(this->stmt);

	if (this->columns) {
		delete[]this->columns;
	}
}
// -----------------------------------------------------------------------------

// -------- FillColumnInfo -----------------------------------------------------
//void __fastcall SQLiteCommand::FillColumnInfo() {
//	const char* src_ptr = NULL;
//	char* trg_ptr;
//	int name_len = 0;
//
//	this->columnCount = sqlite3_column_count(this->stmt);
//	this->columns = new const char*[this->columnCount];
//
//	for (int i = 0; i < this->columnCount; i++) {
//		src_ptr = sqlite3_column_name(stmt, i);
//		name_len = strlen(src_ptr);
//		trg_ptr = (char *) malloc(name_len + 1);
//		this->columns[i] = strcpy(trg_ptr, src_ptr);
//	}
//}

void __fastcall SQLiteCommand::FillColumnInfo() {
	this->columnCount = sqlite3_column_count(this->stmt);
	this->columns = NULL;

	if (this->columnCount > 0) {
		this->columns = new AnsiString[this->columnCount];

		for (int i = 0; i < this->columnCount; i++) {
			this->columns[i] = sqlite3_column_name(stmt, i);
		}
	}
}
// -----------------------------------------------------------------------------

// -------- GetBindIndexByName -------------------------------------------------
int __fastcall SQLiteCommand::GetBindIndexByName(const char *prmName) {
	int index = sqlite3_bind_parameter_index(this->stmt, prmName);

	if (index > 0) {
		return index;
	}
	else {
		AnsiString str = "";
		AnsiString msg = str.sprintf("Parameter %s - not found", prmName);
		throw SQLiteDbError(msg);
	}
}
// -----------------------------------------------------------------------------


// -------- Reset --------------------------------------------------------------
void __fastcall SQLiteCommand::Reset() {

	if (sqlite3_reset(this->stmt) != SQLITE_OK) {
		throw SQLiteDbError(this->dbConnect);
	}
}
// -----------------------------------------------------------------------------

// -------- ClearBindings ------------------------------------------------------
void __fastcall SQLiteCommand::ClearBindings() {

	if (sqlite3_clear_bindings(this->stmt) != SQLITE_OK) {
		throw SQLiteDbError(this->dbConnect);
	}
}
// -----------------------------------------------------------------------------

// -------- Bind (NULL) --------------------------------------------------------
void __fastcall SQLiteCommand::Bind(int index) {
	if (sqlite3_bind_null(this->stmt, index) != SQLITE_OK) {
		throw SQLiteDbError(dbConnect);
	}
}
// -----------------------------------------------------------------------------

// -------- Bind (int) ---------------------------------------------------------
void __fastcall SQLiteCommand::Bind(int index, int data) {

	if (sqlite3_bind_int(this->stmt, index, data) != SQLITE_OK) {
		throw SQLiteDbError(dbConnect);
	}
}
// -----------------------------------------------------------------------------

// -------- Bind (long long) ---------------------------------------------------
void __fastcall SQLiteCommand::Bind(int index, long long data) {

	if (sqlite3_bind_int64(this->stmt, index, data) != SQLITE_OK) {
		throw SQLiteDbError(dbConnect);
	}
}
// -----------------------------------------------------------------------------

// -------- Bind (double) ------------------------------------------------------
void __fastcall SQLiteCommand::Bind(int index, double data) {

	if (sqlite3_bind_double(this->stmt, index, data) != SQLITE_OK) {
		throw SQLiteDbError(dbConnect);
	}
}
// -----------------------------------------------------------------------------

// -------- Bind (string) ------------------------------------------------------
void __fastcall SQLiteCommand::Bind(int index, const char *data, int datalen, sqlite3_destructor_type p_destructor_type) {

	if (sqlite3_bind_text(this->stmt, index, data, datalen, p_destructor_type) != SQLITE_OK) {
		throw SQLiteDbError(dbConnect);
	}
}

void __fastcall SQLiteCommand::Bind(int index, const AnsiString &data, sqlite3_destructor_type p_destructor_type) {

	if (sqlite3_bind_text(this->stmt, index, data.c_str(), data.Length(), p_destructor_type) != SQLITE_OK) {
		throw SQLiteDbError(dbConnect);
	}
}
// -----------------------------------------------------------------------------

// -------- Bind (blob) --------------------------------------------------------
void __fastcall SQLiteCommand::Bind(int index, const void *data, int datalen, sqlite3_destructor_type p_destructor_type) {

	if (sqlite3_bind_blob(this->stmt, index, data, datalen, p_destructor_type) != SQLITE_OK) {
		throw SQLiteDbError(dbConnect);
	}
}
// -----------------------------------------------------------------------------


// -------- BindNullByName -----------------------------------------------------
void __fastcall SQLiteCommand::BindNullByName(const char *prmName) {

	this->Bind(this->GetBindIndexByName(prmName));
}

// -------- BindIntByName ------------------------------------------------------
void __fastcall SQLiteCommand::BindIntByName(const char *prmName, int data) {

	this->Bind(this->GetBindIndexByName(prmName), data);
}
 // -----------------------------------------------------------------------------

void __fastcall SQLiteCommand::BindIntByName(const char *prmName, const AnsiString &data) {

	this->BindIntByName(prmName, data.ToInt());
}
// -----------------------------------------------------------------------------

// -------- BindInt64ByName ----------------------------------------------------
void __fastcall SQLiteCommand::BindInt64ByName(const char *prmName, long long data) {

	this->Bind(this->GetBindIndexByName(prmName), data);
}

void __fastcall SQLiteCommand::BindInt64ByName(const char *prmName, const AnsiString &data) {

	this->BindInt64ByName(prmName, StrToInt64(data));
}
// -----------------------------------------------------------------------------

// -------- BindDoubleByName ---------------------------------------------------
void __fastcall SQLiteCommand::BindDoubleByName(const char *prmName, double data) {

	this->Bind(this->GetBindIndexByName(prmName), data);
}

void __fastcall SQLiteCommand::BindDoubleByName(const char *prmName, const AnsiString &data) {

	this->BindDoubleByName(prmName, data.ToDouble());
}
// -----------------------------------------------------------------------------

// -------- BindStringByName ---------------------------------------------------
void __fastcall SQLiteCommand::BindStringByName(const char *prmName, const char* data) {

	this->Bind(this->GetBindIndexByName(prmName), data, -1, SQLITE_TRANSIENT);
}

void __fastcall SQLiteCommand::BindStringByName(const char *prmName, const AnsiString &data) {

	this->BindStringByName(prmName, data.c_str());
}
// -----------------------------------------------------------------------------


// -------- ExecuteReader ------------------------------------------------------
SQLiteReader __fastcall SQLiteCommand::ExecuteReader() {

	return SQLiteReader(this);
}
// -----------------------------------------------------------------------------

// -------- ExecuteReaderFreeCommand -------------------------------------------
SQLiteReader __fastcall SQLiteCommand::ExecuteReaderFreeCommand() {

	return SQLiteReader(this, true);
}
// -----------------------------------------------------------------------------

// -------- ExecuteNonQuery ----------------------------------------------------
void __fastcall SQLiteCommand::ExecuteNonQuery() {

	this->ExecuteReader().Read();
}
// -----------------------------------------------------------------------------

// -------- CheckNoDataFound ---------------------------------------------------
void __fastcall SQLiteCommand::CheckNoDataFound() {

	if (this->noDataFound) {
		throw SQLiteDbError("nothing to read");
	}
}
// -----------------------------------------------------------------------------

// -------- ExecuteInt ---------------------------------------------------------
int __fastcall SQLiteCommand::ExecuteInt() {
	SQLiteReader reader = this->ExecuteReader();

	this->noDataFound = !reader.Read();
	this->CheckNoDataFound();

	return reader.GetInt(0);
}
// -----------------------------------------------------------------------------

// -------- ExecuteInt64 -------------------------------------------------------
long long __fastcall SQLiteCommand::ExecuteInt64() {
	SQLiteReader reader = this->ExecuteReader();

	this->noDataFound = !reader.Read();
	this->CheckNoDataFound();

	return reader.GetInt64(0);
}
// -----------------------------------------------------------------------------

// -------- ExecuteInt64_NoThrow -----------------------------------------------
long long __fastcall SQLiteCommand::ExecuteInt64_NoThrow() {
	SQLiteReader reader = this->ExecuteReader();

	this->noDataFound = !reader.Read();

	if (this->noDataFound) {
		return 0;
	}
	else {
		return reader.GetInt64(0);
	}
}
// -----------------------------------------------------------------------------

// -------- ExecuteDouble ------------------------------------------------------
double __fastcall SQLiteCommand::ExecuteDouble() {
	SQLiteReader reader = this->ExecuteReader();

	this->noDataFound = !reader.Read();
	this->CheckNoDataFound();

	return reader.GetDouble(0);
}
// -----------------------------------------------------------------------------

// -------- ExecuteString ------------------------------------------------------
AnsiString __fastcall SQLiteCommand::ExecuteString() {
	SQLiteReader reader = this->ExecuteReader();

	this->noDataFound = !reader.Read();
	this->CheckNoDataFound();

	return reader.GetString(0);
}
// -----------------------------------------------------------------------------
