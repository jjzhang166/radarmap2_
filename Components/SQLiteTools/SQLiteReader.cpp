// -----------------------------------------------------------------------------
// SQLiteReader.cpp
// -----------------------------------------------------------------------------
#pragma hdrstop

#include "SQLiteTools.h"
// #include "stdinc.h"

#pragma package(smart_init)

// -------- Constructor / Destructor -------------------------------------------
__fastcall SQLiteReader::SQLiteReader() : cmd(NULL), needFreeSQLiteCommand(false) {
}

__fastcall SQLiteReader::SQLiteReader(const SQLiteReader &copy) {

	this->needFreeSQLiteCommand = copy.needFreeSQLiteCommand;
	this->cmd = copy.cmd;

	if (this->cmd) {
		++this->cmd->refsCount;
	}
}

__fastcall SQLiteReader::SQLiteReader(SQLiteCommand *cmd, bool needFreeComd) {

	this->needFreeSQLiteCommand = needFreeComd;
	this->cmd = cmd;
	++this->cmd->refsCount;
}

__fastcall SQLiteReader::~SQLiteReader() {
	this->Close();
}
// -----------------------------------------------------------------------------

// -------- operator= ----------------------------------------------------------
SQLiteReader& __fastcall SQLiteReader:: operator = (const SQLiteReader & copy) {
	this->Close();

	this->cmd = copy.cmd;
	if (this->cmd) {
		++this->cmd->refsCount;
	}
	return *this;
}
// -----------------------------------------------------------------------------

// -------- Read ---------------------------------------------------------------
bool __fastcall SQLiteReader::Read() {
	if (!this->cmd) {
		throw SQLiteDbError("reader is closed");
	}

	switch(sqlite3_step(this->cmd->stmt)) {
	case SQLITE_ROW:
		return true;
	case SQLITE_DONE:
		return false;
	default:
		throw SQLiteDbError(this->cmd->dbConnect);
	}
}
// -----------------------------------------------------------------------------

// -------- Reset --------------------------------------------------------------
void __fastcall SQLiteReader::Reset() {
	if (!this->cmd) {
		throw SQLiteDbError("reader is closed");
	}

	if (sqlite3_reset(this->cmd->stmt) != SQLITE_OK) {
		throw SQLiteDbError(this->cmd->dbConnect);
	}
}
// -----------------------------------------------------------------------------

// -------- Close --------------------------------------------------------------
void __fastcall SQLiteReader::Close() {

	if (this->cmd) {

		if (--this->cmd->refsCount == 0) {
			sqlite3_reset(this->cmd->stmt);

			if (this->needFreeSQLiteCommand) {
				delete this->cmd;
			}
		}
		this->cmd = NULL;
	}
}
// -----------------------------------------------------------------------------

// -------- CheckReader --------------------------------------------------------
void __fastcall SQLiteReader::CheckReader(int p_index) {

	if (!this->cmd) {
		throw SQLiteDbError("reader is closed");
	}

	if (p_index > cmd->columnCount - 1) {
		throw SQLiteDbError("index out of range");
	}
}
// -----------------------------------------------------------------------------

// -------- GetColumnIndex -----------------------------------------------------
int __fastcall SQLiteReader::GetColumnIndex(const AnsiString &pColumnName) {

	if (!this->cmd) {
		throw SQLiteDbError("reader is closed");
	}

	if (this->cmd->columns) {
		for (int i = 0; i < this->cmd->ResultColumnCount; i++) {
			if (pColumnName == this->cmd->columns[i]) {
				return i;
			}
		}
	}
	throw SQLiteDbError(pColumnName + " - column not found");
}
// -----------------------------------------------------------------------------

// -------- GetInt -------------------------------------------------------------
bool __fastcall SQLiteReader::IsNull(int index) {

	this->CheckReader(index);
	return (SQLITE_NULL == sqlite3_column_type(this->cmd->stmt, index));
}

bool __fastcall SQLiteReader::IsNull(const char *pColumnName) {

	return (SQLITE_NULL == sqlite3_column_type(this->cmd->stmt, this->GetColumnIndex(pColumnName)));
}

bool __fastcall SQLiteReader::IsNull(const AnsiString &pColumnName) {

	return (SQLITE_NULL == sqlite3_column_type(this->cmd->stmt, this->GetColumnIndex(pColumnName)));
}
// -----------------------------------------------------------------------------

// -------- GetInt -------------------------------------------------------------
int __fastcall SQLiteReader::GetInt(int index) {

	this->CheckReader(index);
	return sqlite3_column_int(this->cmd->stmt, index);
}

int __fastcall SQLiteReader::GetInt(const char *pColumnName) {

	return sqlite3_column_int(this->cmd->stmt, this->GetColumnIndex(pColumnName));
}

int __fastcall SQLiteReader::GetInt(const AnsiString &pColumnName) {

	return sqlite3_column_int(this->cmd->stmt, this->GetColumnIndex(pColumnName));
}
// -----------------------------------------------------------------------------

// -------- GetInt64 -----------------------------------------------------------
long long __fastcall SQLiteReader::GetInt64(int index) {

	this->CheckReader(index);
	return sqlite3_column_int64(this->cmd->stmt, index);
}

long long __fastcall SQLiteReader::GetInt64(const char *pColumnName) {

	return sqlite3_column_int64(this->cmd->stmt, this->GetColumnIndex(pColumnName));
}

long long __fastcall SQLiteReader::GetInt64(const AnsiString &pColumnName) {

	return sqlite3_column_int64(this->cmd->stmt, this->GetColumnIndex(pColumnName));
}
// -----------------------------------------------------------------------------

// -------- GetDouble ----------------------------------------------------------
double __fastcall SQLiteReader::GetDouble(int index) {

	this->CheckReader(index);
	return sqlite3_column_double(this->cmd->stmt, index);
}

double __fastcall SQLiteReader::GetDouble(const char *pColumnName) {

	return sqlite3_column_double(this->cmd->stmt, this->GetColumnIndex(pColumnName));
}

double __fastcall SQLiteReader::GetDouble(const AnsiString &pColumnName) {

	return sqlite3_column_double(this->cmd->stmt, this->GetColumnIndex(pColumnName));
}
// -----------------------------------------------------------------------------

// -------- getString ----------------------------------------------------------
AnsiString __fastcall SQLiteReader::GetString(int index) {

	this->CheckReader(index);

	const int l_size = sqlite3_column_bytes(this->cmd->stmt, index);

	if (l_size) { // [+]PPA
		 return AnsiString((const char*)sqlite3_column_text(this->cmd->stmt, index), l_size);
	}
	else {
		return "";
	}
}

AnsiString __fastcall SQLiteReader::GetString(const char *pColumnName) {

	return (const char*)sqlite3_column_text(this->cmd->stmt, this->GetColumnIndex(pColumnName));
}

AnsiString __fastcall SQLiteReader::GetString(const AnsiString &pColumnName) {

	return (const char*)sqlite3_column_text(this->cmd->stmt, this->GetColumnIndex(pColumnName));
}
// -----------------------------------------------------------------------------

// -------- GetBlob ------------------------------------------------------------
bool __fastcall SQLiteReader::GetBlob(int index, void* p_result, int p_size) {

	this->CheckReader(index);

	const int l_size = sqlite3_column_bytes(this->cmd->stmt, index);

	if (l_size == p_size) {
		const void* l_blob = sqlite3_column_blob(this->cmd->stmt, index);

		if (l_blob) {
			memcpy(p_result, l_blob, p_size);
			return true;
		}
	}
	return false;
}

bool __fastcall SQLiteReader::GetBlob(const char *pColumnName, void* p_result, int p_size) {

	return this->GetBlob(this->GetColumnIndex(pColumnName), p_result, p_size);
}

bool __fastcall SQLiteReader::GetBlob(const AnsiString &pColumnName, void* p_result, int p_size) {

	return this->GetBlob(this->GetColumnIndex(pColumnName), p_result, p_size);
}
// -----------------------------------------------------------------------------


// -------- GetBlob ------------------------------------------------------------
//void __fastcall SQLiteReader::GetBlob(int index, std::vector<uint8_t>& p_result) {
//	this->CheckReader(index);
//
//	const int l_size = sqlite3_column_bytes(this->cmd->stmt, index);
//	p_result.resize(l_size);
//
//	if (l_size) {
//		memcpy(&p_result[0], sqlite3_column_blob(this->cmd->stmt, index), l_size);
//	}
//	return;
//}

// -------- GetColumnName ------------------------------------------------------
AnsiString __fastcall SQLiteReader::GetColumnName(int index) {

	this->CheckReader(index);
	return sqlite3_column_name(this->cmd->stmt, index);
}
// -----------------------------------------------------------------------------
