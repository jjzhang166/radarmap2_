// -----------------------------------------------------------------------------
// SQLiteConnection.cpp
// -----------------------------------------------------------------------------
#pragma hdrstop

#include "SQLiteTools.h"

#pragma package(smart_init)

// -------- Constructor / Destructor -------------------------------------------
__fastcall SQLiteConnection::~SQLiteConnection() {

	if (this->transaction) {
		delete this->transaction;
	}
	if (this->db) {
		sqlite3_close(this->db);
	}
}
// -----------------------------------------------------------------------------

// -------- CheckDbOpen --------------------------------------------------------
void __fastcall SQLiteConnection::CheckDbOpen() {

	if (!this->db) {
		throw SQLiteDbError("database is not open");
	}
}

// -------- Open ---------------------------------------------------------------
void __fastcall SQLiteConnection::Open(const char *dbName) {

	if (this->db) {
		sqlite3_close(this->db);
		this->db = NULL;
	}

	if (sqlite3_open(dbName, &this->db) != SQLITE_OK) {
		// throw SQLiteDbError("unable to open database", db);
		throw SQLiteDbError((const)this->db);
	}
}
// -----------------------------------------------------------------------------

// -------- Close --------------------------------------------------------------
void __fastcall SQLiteConnection::Close() {

	if (this->db) {
		if (sqlite3_close(this->db) != SQLITE_OK) {
			throw SQLiteDbError(this);
		}
		this->db = NULL;
	}
}
// -----------------------------------------------------------------------------

// -------- GetLastInsertId ----------------------------------------------------
long long __fastcall SQLiteConnection::GetLastInsertId() {

	this->CheckDbOpen();
	return sqlite3_last_insert_rowid(this->db);
}
// -----------------------------------------------------------------------------

// -------- SetBusyTimeout -----------------------------------------------------
void __fastcall SQLiteConnection::SetBusyTimeout(int ms) {

	this->CheckDbOpen();

	if (sqlite3_busy_timeout(this->db, ms) != SQLITE_OK) {
		throw SQLiteDbError(this);
	}
}
// -----------------------------------------------------------------------------

// -------- GetDbName ----------------------------------------------------------
AnsiString __fastcall SQLiteConnection::GetDbName() {
	AnsiString db_name = "";

	if (this->db) {
		const char * f_name = sqlite3_db_filename(this->db, "main");

		if (NULL != f_name) {
			db_name += f_name;
		}
		this->db = NULL;
	}
	return db_name;
}
// -----------------------------------------------------------------------------

// -------- ExecuteNonQuery ----------------------------------------------------
const char * __fastcall SQLiteConnection::ExecuteNonQuery(const char *sql) {

	this->CheckDbOpen();
	SQLiteCommand(this, sql).ExecuteNonQuery();

	return sql;
}

void __fastcall SQLiteConnection::ExecuteNonQuery(const AnsiString &sql) {

	this->CheckDbOpen();
	SQLiteCommand(this, sql.c_str()).ExecuteNonQuery();
}
// -----------------------------------------------------------------------------

// -------- ExecuteInt ---------------------------------------------------------
int __fastcall SQLiteConnection::ExecuteInt(const char *sql) {

	this->CheckDbOpen();
	return SQLiteCommand(this, sql).ExecuteInt();
}

int __fastcall SQLiteConnection::ExecuteInt(const AnsiString &sql) {

	this->CheckDbOpen();
	return SQLiteCommand(this, sql).ExecuteInt();
}
// -----------------------------------------------------------------------------

// -------- ExecuteInt64 -------------------------------------------------------
long long __fastcall SQLiteConnection::ExecuteInt64(const char *sql) {

	this->CheckDbOpen();
	return SQLiteCommand(this, sql).ExecuteInt64();
}

long long __fastcall SQLiteConnection::ExecuteInt64(const AnsiString &sql) {

	this->CheckDbOpen();
	return SQLiteCommand(this, sql).ExecuteInt64();
}
// -----------------------------------------------------------------------------

// -------- ExecuteString ------------------------------------------------------
AnsiString __fastcall SQLiteConnection::ExecuteString(const char *sql) {

	this->CheckDbOpen();
	return SQLiteCommand(this, sql).ExecuteString();
}

AnsiString __fastcall SQLiteConnection::ExecuteString(const AnsiString &sql) {

	this->CheckDbOpen();
	return SQLiteCommand(this, sql).ExecuteString();
}
// -----------------------------------------------------------------------------

// -------- ExecuteDouble ------------------------------------------------------
double __fastcall SQLiteConnection::ExecuteDouble(const char *sql) {

	this->CheckDbOpen();
	return SQLiteCommand(this, sql).ExecuteDouble();
}

double __fastcall SQLiteConnection::ExecuteDouble(const AnsiString &sql) {

	this->CheckDbOpen();
	return SQLiteCommand(this, sql).ExecuteDouble();
}
// -----------------------------------------------------------------------------


// -------- BeginTransaction ---------------------------------------------------
void __fastcall SQLiteConnection::BeginTransaction() {

	if (NULL == this->transaction) {
		SQLiteTransaction *tr = new SQLiteTransaction(this, true);

		if (tr->IsBeginTransaction) {
			this->transaction = tr;
		}
		else {
			delete tr;
		}
	} else {
		throw SQLiteDbError("Previous transaction is not closed");
	}
}
// -----------------------------------------------------------------------------

// -------- Commit -------------------------------------------------------------
void __fastcall SQLiteConnection::Commit() {

	if (NULL != this->transaction) {
		this->transaction->Commit();
		delete this->transaction;
		this->transaction = NULL;
	}
	else {
		throw SQLiteDbError("Transaction is not started");
	}
}
// -----------------------------------------------------------------------------

// -------- Rollback -----------------------------------------------------------
void __fastcall SQLiteConnection::Rollback() {

	if (NULL != this->transaction) {
		this->transaction->Rollback();
		delete this->transaction;
		this->transaction = NULL;
	}
	else {
		throw SQLiteDbError("Transaction is not started");
	}
}
// -----------------------------------------------------------------------------

