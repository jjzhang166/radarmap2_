//------------------------------------------------------------------------------
//		SQLiteTransaction.cpp
//------------------------------------------------------------------------------
#pragma hdrstop

#include "SQLiteTools.h"

#pragma package(smart_init)

//-------- Constructor / Destructor --------------------------------------------
__fastcall SQLiteTransaction::SQLiteTransaction(SQLiteConnection *pConnect, bool start) {
	this->dbConnect = pConnect;
	this->isBeginTransaction = start;

	if (start) {
		this->Begin();
	}
}

__fastcall SQLiteTransaction::~SQLiteTransaction() {

	if (isBeginTransaction) {
		try {
			this->Rollback();
		}
		catch(...) {
			return;
		}
	}
}
// -----------------------------------------------------------------------------

//-------- Begin ---------------------------------------------------------------
void __fastcall SQLiteTransaction::Begin() {

	if (dbConnect->GetAutocommit()) {
		this->dbConnect->ExecuteNonQuery("begin;");
		this->isBeginTransaction = true;
	}
	else {
		this->isBeginTransaction = false;
	}
}
// -----------------------------------------------------------------------------

//-------- Commit --------------------------------------------------------------
void __fastcall SQLiteTransaction::Commit() {

	if (this->isBeginTransaction) {
		this->dbConnect->ExecuteNonQuery("commit;");
	}
	this->isBeginTransaction = false;
}
// -----------------------------------------------------------------------------

//-------- Rollback ------------------------------------------------------------
void __fastcall SQLiteTransaction::Rollback() {

	if(this->isBeginTransaction)
	{
		this->dbConnect->ExecuteNonQuery("rollback;");
	}
	this->isBeginTransaction=false;
}
// -----------------------------------------------------------------------------

