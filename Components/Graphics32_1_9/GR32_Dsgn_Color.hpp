// CodeGear C++Builder
// Copyright (c) 1995, 2009 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'Gr32_dsgn_color.pas' rev: 21.00

#ifndef Gr32_dsgn_colorHPP
#define Gr32_dsgn_colorHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member functions
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <Sysinit.hpp>	// Pascal unit
#include <Classes.hpp>	// Pascal unit
#include <Sysutils.hpp>	// Pascal unit
#include <Consts.hpp>	// Pascal unit
#include <Designintf.hpp>	// Pascal unit
#include <Designeditors.hpp>	// Pascal unit
#include <Vcleditors.hpp>	// Pascal unit
#include <Windows.hpp>	// Pascal unit
#include <Registry.hpp>	// Pascal unit
#include <Graphics.hpp>	// Pascal unit
#include <Dialogs.hpp>	// Pascal unit
#include <Forms.hpp>	// Pascal unit
#include <Gr32.hpp>	// Pascal unit
#include <Gr32_image.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Gr32_dsgn_color
{
//-- type declarations -------------------------------------------------------
struct TColorEntry;
typedef TColorEntry *PColorEntry;

struct TColorEntry
{
	
public:
	SmallString<31>  Name;
	Gr32::TColor32 Color;
};


class DELPHICLASS TColorManager;
class PASCALIMPLEMENTATION TColorManager : public Classes::TList
{
	typedef Classes::TList inherited;
	
public:
	__fastcall virtual ~TColorManager(void);
	void __fastcall AddColor(const System::UnicodeString AName, Gr32::TColor32 AColor);
	void __fastcall EnumColors(Classes::TGetStrProc Proc);
	Gr32::TColor32 __fastcall FindColor(const System::UnicodeString AName);
	Gr32::TColor32 __fastcall GetColor(const System::UnicodeString AName);
	System::UnicodeString __fastcall GetColorName(Gr32::TColor32 AColor);
	void __fastcall RegisterDefaultColors(void);
	void __fastcall RemoveColor(const System::UnicodeString AName);
public:
	/* TObject.Create */ inline __fastcall TColorManager(void) : Classes::TList() { }
	
};


class DELPHICLASS TColor32Property;
class PASCALIMPLEMENTATION TColor32Property : public Designeditors::TIntegerProperty
{
	typedef Designeditors::TIntegerProperty inherited;
	
public:
	virtual Designintf::TPropertyAttributes __fastcall GetAttributes(void);
	virtual System::UnicodeString __fastcall GetValue(void);
	virtual void __fastcall GetValues(Classes::TGetStrProc Proc);
	virtual void __fastcall SetValue(const System::UnicodeString Value)/* overload */;
public:
	/* TPropertyEditor.Create */ inline __fastcall virtual TColor32Property(const Designintf::_di_IDesigner ADesigner, int APropCount) : Designeditors::TIntegerProperty(ADesigner, APropCount) { }
	/* TPropertyEditor.Destroy */ inline __fastcall virtual ~TColor32Property(void) { }
	
	
/* Hoisted overloads: */
	
public:
	inline void __fastcall  SetValue(const System::WideString Value){ Designeditors::TPropertyEditor::SetValue(Value); }
	
};


//-- var, const, procedure ---------------------------------------------------
extern PACKAGE TColorManager* ColorManager;
extern PACKAGE void __fastcall RegisterColor(const System::UnicodeString AName, Gr32::TColor32 AColor);
extern PACKAGE void __fastcall UnregisterColor(const System::UnicodeString AName);

}	/* namespace Gr32_dsgn_color */
using namespace Gr32_dsgn_color;
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// Gr32_dsgn_colorHPP
