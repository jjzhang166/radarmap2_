// Borland C++ Builder
// Copyright (c) 1995, 1999 by Borland International
// All rights reserved

// (DO NOT EDIT: machine generated header) 'MainUnit.pas' rev: 5.00

#ifndef MainUnitHPP
#define MainUnitHPP

#pragma delphiheader begin
#pragma option push -w-
#pragma option push -Vx
#include <GR32_Polygons.hpp>	// Pascal unit
#include <GR32_Layers.hpp>	// Pascal unit
#include <GR32_Image.hpp>	// Pascal unit
#include <GR32.hpp>	// Pascal unit
#include <ExtCtrls.hpp>	// Pascal unit
#include <StdCtrls.hpp>	// Pascal unit
#include <Dialogs.hpp>	// Pascal unit
#include <Forms.hpp>	// Pascal unit
#include <Controls.hpp>	// Pascal unit
#include <Graphics.hpp>	// Pascal unit
#include <Classes.hpp>	// Pascal unit
#include <SysUtils.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <System.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Mainunit
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TFormPolygons;
class PASCALIMPLEMENTATION TFormPolygons : public Forms::TForm 
{
	typedef Forms::TForm inherited;
	
__published:
	Gr32_image::TBitmap32List* BitmapList;
	Stdctrls::TButton* btNewLine;
	Stdctrls::TCheckBox* cbAntialiased;
	Stdctrls::TScrollBar* FillAlpha;
	Gr32_image::TImage32* Image;
	Stdctrls::TLabel* lbFillOpacity;
	Stdctrls::TLabel* lbLineOpacity;
	Stdctrls::TLabel* lbOutlineThickness;
	Stdctrls::TLabel* lbOutlineThicknessValue;
	Stdctrls::TScrollBar* LineAlpha;
	Stdctrls::TScrollBar* LineThickness;
	Stdctrls::TMemo* Memo1;
	Stdctrls::TMemo* Memo2;
	Extctrls::TPanel* Panel1;
	Stdctrls::TCheckBox* Pattern;
	Extctrls::TRadioGroup* rgAntialiasMode;
	Extctrls::TRadioGroup* rgFillMode;
	Stdctrls::TCheckBox* ThickOutline;
	void __fastcall FormCreate(System::TObject* Sender);
	void __fastcall FormDestroy(System::TObject* Sender);
	void __fastcall ImageMouseDown(System::TObject* Sender, Controls::TMouseButton Button, Classes::TShiftState 
		Shift, int X, int Y, Gr32_layers::TCustomLayer* Layer);
	void __fastcall ImageResize(System::TObject* Sender);
	void __fastcall ParamsChanged(System::TObject* Sender);
	void __fastcall btNewLineClick(System::TObject* Sender);
	void __fastcall ThicknessChanged(System::TObject* Sender);
	
private:
	Gr32_polygons::TPolygon32* Polygon;
	Gr32_polygons::TPolygon32* Outline;
	bool UseOutlinePoly;
	float LineSize;
	void __fastcall Build(void);
	void __fastcall Draw(void);
public:
	#pragma option push -w-inl
	/* TCustomForm.Create */ inline __fastcall virtual TFormPolygons(Classes::TComponent* AOwner) : Forms::TForm(
		AOwner) { }
	#pragma option pop
	#pragma option push -w-inl
	/* TCustomForm.CreateNew */ inline __fastcall virtual TFormPolygons(Classes::TComponent* AOwner, int 
		Dummy) : Forms::TForm(AOwner, Dummy) { }
	#pragma option pop
	#pragma option push -w-inl
	/* TCustomForm.Destroy */ inline __fastcall virtual ~TFormPolygons(void) { }
	#pragma option pop
	
public:
	#pragma option push -w-inl
	/* TWinControl.CreateParented */ inline __fastcall TFormPolygons(HWND ParentWindow) : Forms::TForm(
		ParentWindow) { }
	#pragma option pop
	
};


//-- var, const, procedure ---------------------------------------------------
extern PACKAGE TFormPolygons* FormPolygons;

}	/* namespace Mainunit */
#if !defined(NO_IMPLICIT_NAMESPACE_USE)
using namespace Mainunit;
#endif
#pragma option pop	// -w-
#pragma option pop	// -Vx

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// MainUnit
